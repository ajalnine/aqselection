﻿using System.Collections.ObjectModel;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TechnologyDataLibrary;

namespace TechnologyDataProcessor
{
    public partial class MainWindow : Window
    {
        raportEntities Raport;
        CollectionViewSource GroupViewSource;
        CollectionViewSource ParametersViewSource;
        CollectionViewSource SmeltsViewSource;
        ObservableCollection<Technology_GetMethodsForSmelts> SmeltsView;
        ObservableCollection<Technology_GetMethodsForGroups> GroupsView;
        ObservableCollection<Technology_GetMethodsForParameters> ParametersView;
        ProcessData PD;
        private delegate void GuiAction();
        private int MeltCount;
        private DateTime StartTime;

        public MainWindow()
        {
            InitializeComponent();
            Raport = new raportEntities();
            PD = new ProcessData();
            PD.NewMessage += new ProcessData.NewMessageDelegate(PD_NewMessage);
            PD.MeltCounted += new ProcessData.ProcessDelegate(PD_MeltCounted);
            PD.MeltProgress += new ProcessData.ProcessDelegate(PD_MeltProgress);
        }

        void PD_MeltProgress(object o, ProcessMessageEventArgs e)
        {
            MeltProgressBar.Dispatcher.BeginInvoke(new GuiAction(() =>
            {
                MeltProgressBar.Value = e.Number;
                MeltProgressText.Text = String.Format("Обработано {0} из {1}, осталось: {2}", e.Number, MeltCount, new TimeSpan((DateTime.Now - StartTime).Ticks * (MeltCount-e.Number) / e.Number).ToString());
            }));
        }

        void PD_MeltCounted(object o, ProcessMessageEventArgs e)
        {
            MeltCount = e.Number;
            StartTime = DateTime.Now;
            MeltProgressBar.Dispatcher.BeginInvoke(new GuiAction(() =>
            {
                MeltProgressBar.Maximum = e.Number;
            }));
        }

        void PD_NewMessage(object o, NewMessageEventArgs e)
        {
            LogBox.Dispatcher.BeginInvoke(new GuiAction(() =>
            {
                LogBox.Text += e.Message + "\r\n";
            }));
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SmeltsView = new ObservableCollection<Technology_GetMethodsForSmelts>();
            GroupsView = new ObservableCollection<Technology_GetMethodsForGroups>();
            ParametersView = new ObservableCollection<Technology_GetMethodsForParameters>();

            GroupViewSource = ((CollectionViewSource)this.Resources["GroupViewSource"]);
            GroupViewSource.Source = GroupsView;
            GroupViewSource.SortDescriptions.Add(new System.ComponentModel.SortDescription(){ PropertyName="Name"});
            foreach (var g in Raport.Technology_GetMethodsForGroups) GroupsView.Add(g);
            
            ParametersViewSource = ((CollectionViewSource)this.Resources["ParametersViewSource"]);
            ParametersViewSource.Source = ParametersView;
            ParametersViewSource.SortDescriptions.Add(new System.ComponentModel.SortDescription() { PropertyName = "Name" });

            if (Raport.Technology_GetMethodsForSmelts.Count() == 0)
            {
                Raport.Technology_GetMethodsForSmelts.AddObject(new Technology_GetMethodsForSmelts());
                Raport.SaveChanges();
            }
            SmeltsView.Add(Raport.Technology_GetMethodsForSmelts.First());
            SmeltsViewSource = ((CollectionViewSource)this.Resources["SmeltsViewSource"]);
            SmeltsViewSource.Source = SmeltsView;

            if (Raport.Technology_GetMethodsForGroups.Count() > 0) GroupSelector.SelectedIndex = 0;
        }

        private void GroupSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Technology_GetMethodsForGroups SelectedGroup = GroupSelector.SelectedItem as Technology_GetMethodsForGroups;
            ParametersView.Clear();
            if (SelectedGroup != null) foreach (var p in SelectedGroup.Technology_GetMethodsForParameters) ParametersView.Add(p);
            AddNewParameterButton.IsEnabled = SelectedGroup != null;
            DeleteGroupButton.IsEnabled = SelectedGroup != null;
            if (Raport.Technology_GetMethodsForParameters.Count() > 0) ParameterSelector.SelectedIndex = 0;
        }

        private void ParameterSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeleteParameterButton.IsEnabled = ParameterSelector.SelectedItem != null;
        }
        
        private void AddNewGroupButton_Click(object sender, RoutedEventArgs e)
        {
            Technology_GetMethodsForGroups NewGroup = new Technology_GetMethodsForGroups();
            NewGroup.Name = NewGroupName.Text;
            Raport.Technology_GetMethodsForGroups.AddObject(NewGroup);
            Raport.SaveChanges();
            GroupsView.Add(NewGroup);
            GroupSelector.SelectedItem = NewGroup;
            GroupSelector.ScrollIntoView(GroupSelector.SelectedItem);
        }

        private void AddNewParameterButton_Click(object sender, RoutedEventArgs e)
        {
            Technology_GetMethodsForGroups SelectedGroup = GroupSelector.SelectedItem as Technology_GetMethodsForGroups;
            Technology_GetMethodsForParameters NewParameter = new Technology_GetMethodsForParameters();
            NewParameter.Name = NewParameterName.Text;
            SelectedGroup.Technology_GetMethodsForParameters.Add(NewParameter);
            Raport.SaveChanges();
            ParametersView.Add(NewParameter);
            ParameterSelector.SelectedItem = NewParameter;
            ParameterSelector.ScrollIntoView(ParameterSelector.SelectedItem);
        }
        
        private void DeleteParameterButton_Click(object sender, RoutedEventArgs e)
        {
            int Index = ParameterSelector.SelectedIndex;
            if (Index > 0) Index--;
            Technology_GetMethodsForGroups SelectedGroup = GroupSelector.SelectedItem as Technology_GetMethodsForGroups;
            Technology_GetMethodsForParameters SelectedParameter = ParameterSelector.SelectedItem as Technology_GetMethodsForParameters;
            Raport.Technology_GetMethodsForParameters.DeleteObject(SelectedParameter);
            Raport.SaveChanges();
            ParametersView.Remove(SelectedParameter);
            ParameterSelector.SelectedIndex = Index;
            ParameterSelector.ScrollIntoView(ParameterSelector.SelectedItem);
        }

        private void DeleteGroupButton_Click(object sender, RoutedEventArgs e)
        {
            int Index = GroupSelector.SelectedIndex;
            if (Index > 0) Index--;
            Technology_GetMethodsForGroups SelectedGroup = GroupSelector.SelectedItem as Technology_GetMethodsForGroups;
            Raport.Technology_GetMethodsForGroups.DeleteObject(SelectedGroup);
            Raport.SaveChanges();
            GroupsView.Remove(SelectedGroup);
            GroupSelector.SelectedIndex = Index;
            GroupSelector.ScrollIntoView(GroupSelector.SelectedItem);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Raport.SaveChanges();
            Raport.AcceptAllChanges();
        }

        private void StartProcessButton_Click(object sender, RoutedEventArgs e)
        {
            LogBox.Text = string.Empty;
            List<Technology_GetMethodsForGroups> GroupIds = null;
            if (MoveGroupSelector.SelectedItems!=null && MoveGroupSelector.SelectedItems.Count>0) GroupIds = (from m in MoveGroupSelector.SelectedItems.Cast<Technology_GetMethodsForGroups>() select m).ToList();
            PD.Process(From.SelectedDate, To.SelectedDate, ConfigurationManager.ConnectionStrings["raportConnectionString"].ConnectionString, true, GroupIds);
        }
    }
}
