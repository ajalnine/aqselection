﻿using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;

namespace SLConstructor_Stability
{
    public class StabilityParametersList : SelectionParametersList
    {
        public StabilityParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();

            DateField = " tbltesthead.test_date";

            DetailsJoin = " inner join tbltests on tbltests.test_id=tbltesthead.test_id ";
            
            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector =  ColumnTypes.Discrete,
                Name = "Марка",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark 
                                INNER JOIN 
                                (
	                                select distinct id_cmark from tbltesthead @DetailsJoin @PreviousFilter group by id_cmark
                                ) as t1 on t1.id_cmark=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck("Контроль") + @" AND  @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "TblTestHead.id_cmark",
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

           var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                Name = "НТД контроля",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                select distinct id_ntd from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd
                                ) as t1 on t1.id_ntd=G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "TblTestHead.id_ntd",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            
            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Discrete,
                Name = "Стан",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct LL__MILL_ID as id, LL__MILL_ID as name from tbltesthead @DetailsJoin @PreviousFilter group by LL__MILL_ID having LL__MILL_ID is not null order by LL__MILL_ID ",
                Field = "LL__MILL_ID",
                ResultedFilterField = "tbltesthead.LL__MILL_ID",
                IsNumber = true,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.Discrete,
                Name = "Толщина",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct thk as id, thk as name from tbltesthead @DetailsJoin @PreviousFilter group by thk having thk is not null order by tbltesthead.thk",
                Field = "thk",
                ResultedFilterField = "tbltesthead.thk",
                IsNumber = true,
                NumericSelectionAvailable = true,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                Name = "Профиль",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct prof as id, prof as name from tbltesthead @DetailsJoin @PreviousFilter group by prof having prof is not null order by tbltesthead.prof",
                Field = "prof",
                ResultedFilterField = "tbltesthead.prof",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd6 = new SelectionParameterDescription
            {
                ID = 6,
                Selector = ColumnTypes.Discrete,
                Name = "Группа параметров",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, paramtype as name from tbl_paramtype 
                                INNER JOIN 
                                (
	                                select distinct id_paramtype from tbltesthead @DetailsJoin @PreviousFilter and id_paramtype is not null 
                                ) as t1 on t1.id_paramtype=tbl_paramtype.id 
                                where " + Security.GetOldParametersCheck() + " AND  @AdditionalFilter order by paramtype",
                Field = "paramtype",
                ResultedFilterField = "tbltesthead.id_paramtype",
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd7 = new SelectionParameterDescription
            {
                ID = 7,
                Selector = ColumnTypes.Parameters,
                Name = "Параметры и значения",
                EnableDetailsJoin = true,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
                                create table #tquery
                                (number int IDENTITY(1,1), pt_id int, name nvarchar(max), paramtype nvarchar(max), IsNumber bit, Parameter_ID int)

                                insert into #tquery 
                                select distinct tblTestHead.id_paramtype as pt_id, tbl_Parameters.DisplayParameterName,
                                tbl_Paramtype.Paramtype as Name, tbl_Parameters.IsNumber, tbl_parameters.id from tblTestHead 
                                inner join tbl_Paramtype on tbl_Paramtype.id = tblTestHead.id_paramtype
                                inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                                inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter  @PreviousFilter and @AdditionalFilter order by tbl_Paramtype.Paramtype, tbl_Parameters.DisplayParameterName

                                select t1.number as id, t1.name as name, t1.pt_id, t1.paramtype, t1.IsNumber, cast (case when t2.paramtype=t1.paramtype then 0 else 1 end as bit) as GroupChanged, cast (case when t2.name=t1.name and t2.paramtype=t1.paramtype then 0 else 1 end as bit) as ParameterChanged, t1.Parameter_ID from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 

                                drop table #tquery
                                ",
                Field = "DisplayParameterName",
                ResultedFilterField = "tblTests.id_parameter",
                OptionalField = "tblTests.value",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd8 = new SelectionParameterDescription
            {
                ID = 8,
                Selector = ColumnTypes.Discrete,
                Name = "Цех / материал",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct melttype as id, melttype as name from tbltesthead @DetailsJoin @PreviousFilter and melttype is not null",
                Field = "tbltesthead.melttype",
                ResultedFilterField = "melttype",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd9 = new SelectionParameterDescription
            {
                ID = 9,
                Selector = ColumnTypes.Discrete,
                Name = "Аттестация",
                EnableDetailsJoin = true,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd 
	                                select distinct attestation as id , (case when attestation=1 then 'Аттестационные' else 'Вне аттестации' end) as name from tbltesthead inner join tbltests on tbltests.test_id=tbltesthead.test_id  @PreviousFilter group by attestation having attestation is not null",
                Field = "attestation",
                ResultedFilterField = "TblTests.attestation",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };
           
            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
            ParametersList.Add(spd6);
            ParametersList.Add(spd7);
            ParametersList.Add(spd8);
            ParametersList.Add(spd9);
        }
    }
}
