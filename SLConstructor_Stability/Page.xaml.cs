﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using AQControlsLibrary;
using AQConstructorsLibrary;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;
using AQAnalysisLibrary;

namespace SLConstructor_Stability
{
    public partial class Page : IAQModule
    {
        #region Переменные

        public void InitSignaling(CommandCallBackDelegate ccbd) { }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;
        private List<Step> _steps;
        private string _tableName;
        
        private ParametersList _parametersList;
        private FieldListCombination _fieldListCombination;
        private Filters _filters;
        private string _sql = string.Empty;
        private int _selectionTypeNumber;
        private bool _addGroupName;
        private bool _showFvc;
        private bool _exchangeDates;
        public LimitsMode CurrentLimitsMode = LimitsMode.Reference;
        public LimitsShowMode CurrentLimitsShowMode = LimitsShowMode.None;
        private readonly StabilityParametersList _conditionStability = new StabilityParametersList();
        private readonly AQModuleDescription _aqmd;
        #endregion

        public Page()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            ColumnsPresenter.SelectionParameters = _conditionStability;
            InitializeCustomDateRangePanel();
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
        }

        private void InitializeCustomDateRangePanel()
        {
            var g = ColumnsPresenter.GetDateRangeCustomPanel();
            g.HorizontalAlignment = HorizontalAlignment.Center;
            var sp = Resources["DateRangeModeSelector"] as StackPanel;
            Resources.Remove("DateRangeModeSelector");
            g.Children.Add(sp);
        }

        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery();
        }

        private void RefreshQuery()
        {
            RefreshFlags();
            RefreshFilters();
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;
            var task = new Task(new[] { "Формирование списка полей", "Слияние полей" }, "Проверка условий", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cs.BeginGetTestsParametersList(_filters.ParametersFilter, _filters.ParametersFilter, GetTestsParametersListDone, task);
        }

        private void RefreshFlags()
        {
            _selectionTypeNumber = SelectionType.SelectedIndex;
            if (AddGroupNameCheckBox.IsChecked != null) _addGroupName = AddGroupNameCheckBox.IsChecked.Value;
            if (ShowFieldCombineEditorCheckBox.IsChecked != null) _showFvc = ShowFieldCombineEditorCheckBox.IsChecked.Value;
        }

        private void RefreshFilters()
        {
            _filters = ColumnsPresenter.GetFilters();
        }
        private void GetTestsParametersListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetTestsParametersList(iar);
            _parametersList = t.ParametersList;
            if (t.Success)
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                task.SetMessage(t.Message);
                _cs.BeginGetTestsCorrectionList(_filters.ParametersFilter, _parametersList, _selectionTypeNumber == 1, GetTestsCorrectionListDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() => ColumnsPresenter.EnableFinishSelection(true));
        }

        private void GetTestsCorrectionListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            FieldListCombineTaskResult t = _cs.EndGetTestsCorrectionList(iar);
            _fieldListCombination = t.FieldListCombination;
            WorkParametersList.ParametersList = _parametersList;
            if (t.Success)
            {
                task.SetState(1, TaskState.Ready);
                task.SetMessage(t.Message);
            }
            else
            {
                task.SetState(1, TaskState.Error);
                task.SetMessage(t.Message);
            }

            Dispatcher.BeginInvoke(delegate
            {
                EnableSaveAndExport(true);
                RefreshQueryButton.IsEnabled = true;
                ColumnsPresenter.EnableFinishSelection(true);
                MainFieldCombineView.PL = _parametersList;
                MainFieldCombineView.FLC = _fieldListCombination;
                MainFieldCombineView.Refresh();
                MainFieldCombineView.Visibility = _showFvc ? Visibility.Visible : Visibility.Collapsed;
            });
        }

        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery();
        }

        #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(delegate
            {
                XlsxExportButton.IsEnabled = allow;
                PreviewButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
                SaveCalculationButton.IsEnabled = allow;
            });
        }

        private void RegisterDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _conditionStability.DateField = "tbltesthead.RegisterDate";
            _exchangeDates = true;
            if (ColumnsPresenter == null) return;
            ColumnsPresenter.RefreshFirstFilter();
            RefreshFilters();
        }

        private void TestDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _conditionStability.DateField = "tbltesthead.test_date";
            _exchangeDates = false;
            if (ColumnsPresenter == null) return;
            ColumnsPresenter.RefreshFirstFilter();
            RefreshFilters();
        }

        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }


        #region Детализация по запросу
        private void EmptyTableSelectedProcessor(object o, EmptyTableSelectedEventArgs e)
        {
            var da = o as DataAnalysis;
            if (da == null) return;
            var filter = string.Format("{0} AND ({1})", e.EmptyTable.Tag, _filters.SaveFilter);
            var name = e.EmptyTable.TableName;
            var parameter = e.EmptyTable.ColumnNames[0];
            var sql = StabilitySelects.GetSingleParameterSQL(filter, parameter, _exchangeDates);
            if (e.StartProgress != null) e.StartProgress();
            var calculationService = Services.GetCalculationService();
            calculationService.BeginFillDataWithSQLNamed(sql, ColumnsPresenter.From, ColumnsPresenter.To, name, iar =>
            {
                var result = calculationService.EndFillDataWithSQLNamed(iar);
                if (result.Success)
                {
                    Dispatcher.BeginInvoke(() => calculationService.BeginGetTablesSLCompatible(result.TaskDataGuid, iar2 =>
                    {
                        var tables = calculationService.EndGetTablesSLCompatible(iar2);
                        if (tables.Count == 1) e.TableLoadedCallBack(tables[0]);
                    }, null)
                        );
                }
                else
                {
                    Dispatcher.BeginInvoke(da.ResetAppendedTables);
                }
                if (e.StopProgress != null) e.StopProgress();

            }, null);
        }

        private void TableViewSelectionChangedProcessor(object sender, DataAnalysisSelectionChangedEventArgs e)
        {
            var da = sender as DataAnalysis;

            if (da == null || !e.SourceTable.TableName.StartsWith("Стабильность") || e.AddedItems.Count == 0) return;
            da.ResetAppendedTables();
            var record = ((SLDataRow)e.AddedItems[0]).Row;
            var newEmptyTable = CreateEmptyTable(record);
            da.AppendTablesToSources(new List<SLDataTable> { newEmptyTable });
        }

        private SLDataTable CreateEmptyTable(List<object> record)
        {
            string name, filter, field;
            if (MarkNtdMillProfile.IsChecked != null && MarkNtdMillProfile.IsChecked.Value)
            {
                name = record[0] + ", " + record[1];
                name += !String.IsNullOrEmpty(record[2].ToString()) ? ", " + record[2] : String.Empty;
                name += !String.IsNullOrEmpty(record[3].ToString()) ? ", Стан:" + record[3] : string.Empty;
                name += ", " + record[4 + (_addGroupName ? 1 : 0)];

                filter = string.Format("Common_Mark.Mark='{0}'", record[0]);
                filter += string.Format(" AND Common_NTD.NTD='{0}'", record[1]);
                filter += !String.IsNullOrEmpty(record[2].ToString()) ? string.Format(" AND tbltesthead.thk={0}", record[2]) : " AND tbltesthead.thk is null ";
                filter += !String.IsNullOrEmpty(record[3].ToString()) ? string.Format(" AND tbltesthead.LL__MILL_ID={0}", record[3]) : " AND tbltesthead.LL__MILL_ID is null ";
                filter += " AND tbl_Parameters.DisplayParameterName='" + record[4 + (_addGroupName ? 1 : 0)] + "'";

                field = record[4 + (_addGroupName ? 1 : 0)].ToString();
            }
            else if (MarkNtdMill.IsChecked != null && MarkNtdMill.IsChecked.Value)
            {
                name = record[0] + ", " + record[1];
                name += !String.IsNullOrEmpty(record[2].ToString()) ? ", Стан:" + record[2] : string.Empty;
                name += ", " + record[3 + (_addGroupName ? 1 : 0)];

                filter = "Common_Mark.Mark='" + record[0] + "'";
                filter += " AND Common_NTD.NTD='" + record[1] + "'";
                filter += !String.IsNullOrEmpty(record[2].ToString()) ? " AND tbltesthead.LL__MILL_ID=" + record[2] : " AND tbltesthead.LL__MILL_ID is null ";
                filter += " AND tbl_Parameters.DisplayParameterName='" + record[3 + (_addGroupName ? 1 : 0)] + "'";

                field = record[3 + (_addGroupName ? 1 : 0)].ToString();
            }
            else
            {
                name = record[0] + ", " + record[1] + ", " + record[2 + (_addGroupName ? 1 : 0)];

                filter = "Common_Mark.Mark='" + record[0] + "'";
                filter += " AND Common_NTD.NTD='" + record[1] + "'";
                filter += " AND tbl_Parameters.DisplayParameterName='" + record[2 + (_addGroupName ? 1 : 0)] + "'";

                field = record[2 + (_addGroupName ? 1 : 0)].ToString();
            }

            var newEmptyTable = new SLDataTable { TableName = name, Tag = filter, ColumnNames = new List<string> { field }, DataTypes = new List<string> { "Double" } };
            return newEmptyTable;
        }
        #endregion
        public class StatisticDescription
        {
            public bool MinEnabled;
            public bool MaxEnabled;
            public bool CountEnabled;
            public bool AverageEnabled;
            public bool StdevEnabled;
            public bool AmplitudeEnabled;
        }
        public enum LimitsMode { AutoAttestation, Reference }

        [Flags]
        public enum LimitsShowMode { None = 0, Mark = 1, Inlude = 2, OnlyBad = 4 }
    }
}