﻿using ApplicationCore;

namespace SLConstructor_Stability
{
    public static class StabilitySelects
    {
        public static string GetStabilitySQL(string sqlFilter, bool addGroupName, bool sort, bool groupBySize, bool groupByMill)
        {

            const string minLimitsField = " tblTests.NTDMin ";
            const string maxLimitsField = " tblTests.NTDMax ";
            
            var sqlOther = @"SELECT max(Common_Mark.mark) as 'Марка',
                                max(Common_NTD.ntd) as 'НТД', ";
            if (groupBySize) sqlOther += @" cast(replace(tblTestHead.thk,',','.') as float) as 'Толщина', ";
            
            if (groupByMill) sqlOther += @"tblTestHead.LL__MILL_ID as 'Стан', ";

            if (addGroupName) sqlOther+= @"tbl_Paramtype.Paramtype as 'Группа параметров', ";

            sqlOther += @"tbl_parameters.DisplayParameterName as 'Параметр',
                count(*) as 'n',
                max(dbo.ToNumeric(" + minLimitsField + @")) as 'НГ',
                min(dbo.ToNumeric(" + maxLimitsField + @")) as 'ВГ',
                round(dbo.NormalCD( max(dbo.ToNumeric(" + minLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),4)as 'Pнг',
                round(1- dbo.NormalCD(min(dbo.ToNumeric(" + maxLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),4) as 'Pвг',
                dbo.GetScaleRG(dbo.NormalCD( max(dbo.ToNumeric(" + minLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),0.005,0.05) as '#Pнг_Цвет',
                dbo.GetScaleRG(1- dbo.NormalCD(min(dbo.ToNumeric(" + maxLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),0.005,0.05) as '#Pвг_Цвет',
                round(case when stdev(dbo.ToNumeric(tblTests.value)) <>0 then (min(dbo.ToNumeric(" + maxLimitsField + @")) -max(dbo.ToNumeric(" + minLimitsField + @"))) / (6 * stdev(dbo.ToNumeric(tblTests.value))) else null end,4) as 'Cp',
                round(case when stdev(dbo.ToNumeric(tblTests.value)) <>0 then (avg(dbo.ToNumeric(tblTests.value)) - max(dbo.ToNumeric(" + minLimitsField + @"))) / (6 * stdev(dbo.ToNumeric(tblTests.value))) else null end,4)  as 'Cpl',
                round(case when stdev(dbo.ToNumeric(tblTests.value)) <>0 then (min(dbo.ToNumeric(" + maxLimitsField + @")) - avg(dbo.ToNumeric(tblTests.value))) / (6 * stdev(dbo.ToNumeric(tblTests.value))) else null end,4)  as 'Cpu',
                round(avg(dbo.ToNumeric(tblTests.value)),5) as 'Среднее',
                round(stdev(dbo.ToNumeric(tblTests.value)),5) as 'СКО',
                case when avg(dbo.ToNumeric(tblTests.value))<>0 then round(stdev(dbo.ToNumeric(tblTests.value))/avg(dbo.ToNumeric(tblTests.value)),5) else null end as 'Квар',
                round(cast(sum(case when dbo.ToNumeric(tblTests.value)<dbo.ToNumeric(" + minLimitsField + @") then 1 else 0 end) as float)/cast(count(*) as float)*100,2) as 'Ниже НГ, %',
                round(cast(sum(case when dbo.ToNumeric(tblTests.value)>dbo.ToNumeric(" + maxLimitsField + @") then 1 else 0 end) as float)/cast(count(*) as float)*100,2) as 'Выше ВГ, %',
                round(cast(sum(case when (dbo.ToNumeric(tblTests.value)<=dbo.ToNumeric(" + maxLimitsField + @") or " + maxLimitsField + " is null) and (dbo.ToNumeric(tblTests.value)>=dbo.ToNumeric(" + minLimitsField + @") or " + minLimitsField + @" is null ) then 1 else 0 end) as float)/cast(count(*) as float)*100,2) as 'В границах, %',
                sum(case when dbo.ToNumeric(tblTests.value)<dbo.ToNumeric(" + minLimitsField + @") then 1 else 0 end) as 'Ниже НГ, набл.',
                sum(case when dbo.ToNumeric(tblTests.value)>dbo.ToNumeric(" + maxLimitsField + @") then 1 else 0 end) as 'Выше ВГ, набл.',
                sum(case when (dbo.ToNumeric(tblTests.value)<=dbo.ToNumeric(" + maxLimitsField + @") or " + maxLimitsField + " is null) and (dbo.ToNumeric(tblTests.value)>=dbo.ToNumeric(" + minLimitsField + @") or " + minLimitsField + @" is null ) then 1 else 0 end) as 'В границах, набл.',
                max(Common_NTD_1.ntd) as 'НТД 1', 
                max(Common_NTD_2.ntd) as 'НТД 2', 
                max(Common_NTD_3.ntd) as 'НТД 3',
                min(tbltesthead.test_date) as 'Начало периода', max(tbltesthead.test_date) as 'Конец периода'
                FROM tblTestHead INNER JOIN
                tblTests ON tblTestHead.test_id = tblTests.test_id and (tbltesthead.num=tbltests.samplenumber  or tbltesthead.num is null) INNER JOIN
                Common_Mark ON tblTestHead.id_cmark = Common_Mark.id left outer JOIN
                Common_NTD ON tblTestHead.id_ntd = Common_NTD.id INNER JOIN
                tbl_Paramtype ON tblTestHead.id_paramtype = tbl_Paramtype.id INNER JOIN
                tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND tblTests.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                Common_NTD AS Common_NTD_3 ON tblTestHead.id_ntd_melt3 = Common_NTD_3.id LEFT OUTER JOIN
                Common_NTD AS Common_NTD_2 ON tblTestHead.id_ntd_melt2 = Common_NTD_2.id LEFT OUTER JOIN
                Common_NTD AS Common_NTD_1 ON tblTestHead.id_ntd_melt1 = Common_NTD_1.id LEFT OUTER JOIN 
                tbl_results on tbl_results.id=tbltesthead.result LEFT OUTER JOIN
                Tests_Machines on Tests_Machines.id = tbltests.id_machine LEFT OUTER JOIN
                Tests_Testers on Tests_Testers.id = tbltests.id_tester
                where  " + Security.GetMarkSQLCheck("Контроль") + " AND " + Security.GetOldParametersCheck() + " AND " + sqlFilter + @" group by tbltesthead.id_cmark, tblTestHead.id_ntd, ";
                if (groupByMill) sqlOther += " tblTestHead.LL__MILL_ID, ";
                if (groupBySize) sqlOther += " tblTestHead.thk, ";
                sqlOther += @" tbl_Paramtype.Paramtype, tbl_parameters.DisplayParameterName ";
    
                return sqlOther + ((sort) ? @" order by
                isnull(dbo.NormalCD(max(dbo.ToNumeric(" + minLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),0)+ 
                isnull(1-dbo.NormalCD(min(dbo.ToNumeric(" + maxLimitsField + @")), avg(dbo.ToNumeric(tblTests.value)), stdev(dbo.ToNumeric(tblTests.value))),0) desc" : "");
        }

        public static string GetSingleParameterSQL(string sqlFilter, string parameter, bool exchangeDates)
        {

            const string minLimitsField = " Requirements.min ";
            const string maxLimitsField = " Requirements.max ";

            var sqlOther = @"SELECT tblTestHead.melt as 'Плавка', tblTestHead.part as 'Партия'"+
                (!exchangeDates ? ", tblTestHead.test_date as 'Дата испытания'" : ", tblTestHead.registerdate as 'Дата выплавки'") +
                @", tbltesthead.num as 'Номер образца', tblTesthead.onrs as 'Разливка',
                dbo.ToNumeric(" + minLimitsField + @") as '"+parameter +@"_НГ',
                dbo.ToNumeric(value) as '" + parameter + @"',
                dbo.ToNumeric(" + maxLimitsField + @") as '" + parameter + @"_ВГ'
                FROM tblTestHead INNER JOIN
                tblTests ON tblTestHead.test_id = tblTests.test_id and (tbltesthead.num=tbltests.samplenumber  or tbltesthead.num is null) INNER JOIN
                Common_Mark ON tblTestHead.id_cmark = Common_Mark.id left outer JOIN
                Common_NTD ON tblTestHead.id_ntd = Common_NTD.id INNER JOIN
                tbl_Paramtype ON tblTestHead.id_paramtype = tbl_Paramtype.id INNER JOIN
                tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND tblTests.id_parameter = tbl_Parameters.id
                LEFT OUTER JOIN Requirements on Requirements.MarkID = tblTestHead.id_cmark and Requirements.NtdID = tblTestHead.id_ntd and Requirements.ParamTypeID = tblTestHead.id_paramtype 
                                                                                  and Requirements.ParamID = tblTests.id_parameter and Requirements.IsSafe = 1 
                                                                                  and (Requirements.SizeMin is null or Requirements.SizeMin>=tblTestHead.thk) 
                                                                                  and (Requirements.SizeMax is null or Requirements.SizeMax<=tblTestHead.thk) 
                where  " + Security.GetMarkSQLCheck("Контроль") + " AND " + Security.GetOldParametersCheck() + " AND " + sqlFilter;

            return sqlOther;
        }
    }
}