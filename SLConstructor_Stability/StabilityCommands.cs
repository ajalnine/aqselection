﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQAnalysisLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Stability
{
    public partial class Page
    {
        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();

            PreviewButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                    {
                        var tables = _cas.EndGetTablesSLCompatible(i);
                        if (tables == null) return;
                        task.AdvanceProgress();
                        task.SetMessage("Данные готовы");
                        var ca = new Dictionary<string, object>
                    {
                        {"From", ColumnsPresenter.From}, 
                        {"To", ColumnsPresenter.To}, 
                        {"SourceData", tables},
                        {"Calculation", _steps},
                        {"Description", _filters.Description},
                        {"New", true},
                        {"TableViewSelectionChanged", new DataAnalysisSelectionChangedDelegate(TableViewSelectionChangedProcessor)},
                        {"EmptyTableSelectedProcessor", new EmptyTableSelectedDelegate(EmptyTableSelectedProcessor)}
                    };
                        Dispatcher.BeginInvoke(() =>
                        {
                            PreviewButton.IsEnabled = true;
                            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
                }

            }, task);
        }

        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            XlsxExportButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, i =>
                    {
                        var sr = _cas.EndGetAllResultsInXlsx(i);
                        if (sr.Success)
                        {
                            task.AdvanceProgress();
                            task.SetMessage("Данные готовы");
                            Dispatcher.BeginInvoke(() =>
                            {
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                    @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" +
                                    HttpUtility.UrlEncode("Отчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" +
                                    HttpUtility.UrlEncode("application/msexcel")));
                                XlsxExportButton.IsEnabled = true;
                            });
                        }
                        else
                        {
                            task.SetState(1, TaskState.Error);
                            task.SetMessage(t.Message);
                            Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                        }
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                }

            }, task);
        }

        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            var signalArguments = new Dictionary<string, object> { { "SQL", _sql } };
            const string name = "Стабильность";
            signalArguments.Add("Name", name);
            signalArguments.Add("From", ColumnsPresenter.From);
            signalArguments.Add("To", ColumnsPresenter.To);
            signalArguments.Add("Calculation", _steps);
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        private void SaveCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            var signalArguments = new Dictionary<string, object> { { "SQL", _sql } };
            const string name = "Стабильность";
            signalArguments.Add("Name", name);
            signalArguments.Add("From", ColumnsPresenter.From);
            signalArguments.Add("To", ColumnsPresenter.To);
            signalArguments.Add("Calculation", _steps);
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation, signalArguments, null);
        }

        private void SetupCalculation()
        {
            RefreshFilters();
            switch (_selectionTypeNumber)
            {
                case 0:
                    _sql = StabilitySelects.GetStabilitySQL(_filters.SaveFilter, _addGroupName,
                        true, MarkNtdMillProfile.IsChecked != null && MarkNtdMillProfile.IsChecked.Value,
                        MarkNtdMill.IsChecked != null && (MarkNtdMillProfile.IsChecked != null && (MarkNtdMillProfile.IsChecked.Value || MarkNtdMill.IsChecked.Value)));
                    _tableName = "Стабильность";
                    break;
            }
            Services.GetAQService().BeginWriteLog("Calc: Stability, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);

            _steps = StepFactory.CreateSQLQuery(_sql, _tableName);
        }
    }
}
