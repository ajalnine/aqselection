﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Core
{
    public static class Signal
    {
        private static readonly Dictionary<Guid, AQModuleDescription> AQModuleCatalogue;
        public static IAQModule WorkPlace;
        public static int SignalCounter;
        public static int AppendSignalCounter;

        static Signal()
        {
            AQModuleCatalogue = new Dictionary<Guid,AQModuleDescription>();
        }
        
        public static void Send(AQModuleDescription senderDescription, AQModuleDescription receiverDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (!AQModuleCatalogue.Keys.Contains(receiverDescription.UniqueID) && WorkPlace.GetAQModuleDescription().UniqueID!=receiverDescription.UniqueID)
            {
                commandCallBack?.Invoke(commandType, commandArgument, null);
            }
            else
            {
                receiverDescription.Instance.ProcessSignal(senderDescription, commandType, commandArgument, commandCallBack);
            }
            SignalCounter++;
            if (commandType == Command.InsertQuery || commandType == Command.InsertCalculation) AppendSignalCounter++;
        }

        public static void AttachAQModule(AQModuleDescription aqmd)
        {
            if (AQModuleCatalogue.Keys.Contains(aqmd.UniqueID))return;
            AQModuleCatalogue.Add(aqmd.UniqueID, aqmd);
        }

        public static void ClearAQModules()
        {
            AQModuleCatalogue.Clear();
        }

        public static void DetachAQModule(AQModuleDescription aqmd)
        {
            if (!AQModuleCatalogue.Keys.Contains(aqmd.UniqueID))return;
            AQModuleCatalogue.Remove(aqmd.UniqueID);
        }

        public static IEnumerable<AQModuleDescription>GetLoadedAQModules()
        {
            return AQModuleCatalogue.Values;
        }

        public static IEnumerable<AQModuleDescription>GetLoadedAQModules(Type moduleType)
        {
            return AQModuleCatalogue.Values.Where(a=>a.ModuleType.Name == moduleType.Name);
        }
    }

    public class AQModuleDescription
    {
        public string Name;
        public string URL;
        public IAQModule Instance;
        public Type ModuleType;
        public Control Container;
        public Guid UniqueID;
        public RenderTargetBitmap Thumbnail;
        public string ShortDescription;
        public object CustomData;

        public AQModuleDescription ()
        {
            UniqueID = Guid.NewGuid();
        }
    }

    public enum Command { Load, Unload, InsertStoredSelection, InsertCalculation, InsertStoredCalculation, InsertQuery, InsertTable, Renew, InsertSlideElement, ReopenInAnalisys, PasteToAnalysis, InsertTablesToAnalysis, Clear, InsertAndSaveCalculation, StartNewAnalysis, SendTableToAnalysis };
    
    public delegate void CommandCallBackDelegate(Command commandType, object commandArgument, object result);
}
