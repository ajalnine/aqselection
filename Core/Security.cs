﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Core
{
    public static class Security
    {
        public static bool UserIsKnown;
        public static bool RolesIsKnown;

        private static string _currentuser;
        public static string CurrentUser
        {
            get
            {
                return _currentuser;
            }
            set
            {
                _currentuser = value;
                UserIsKnown = true;
            }
        }

        public static string CurrentTitle { get; set; }

        public static string CurrentWorkPlace { get; set; }

        private static List<string> _roles;
        public static List<string> Roles
        {
            get
            {
                return _roles;
            }
            set
            {
                _roles = value;
                RolesIsKnown = true;
            }
        }

        public static string CurrentUserName;

        public static List<string> AvailableMarkRoles = (new[] { "Новые марки", "Нерж.", "Рельс.", "ШХ." }).ToList();

        public static List<string> AvailableGroupRoles = (new [] { "Выплавка", "Контроль", "Прокатка" }).ToList();

        public static List<string> AvailableCommonRoles = (new [] { "Администратор", "Т31_Администратор", "Т31_Редактирование", "Т31_АДО", "Т31_ЦЭСТ", "Т31_ЛПО", "Т31_ОМиТ", "Т31_ОНМКиД", "Т31_ОРС", "Т31_СЭМО", "Т31_СО", "Т31_СОРПО", "Т31_УОР", "Правка_параметров", "Просмотр_ТР", "Выборки", "Послябный_контроль", "SQL" }).ToList();

        public static List<string> AvailableRoles = AvailableCommonRoles.Union(AvailableGroupRoles.Union(AvailableMarkRoles)).ToList();

        static Security()
        {
            Roles = new List<string>();
        }

        public static string GetMarkSQLCheck(string requiresRole)
        {
            var check = " (Common_Mark.RequiresRole is NULL ";
            if (IsInRole(requiresRole) || string.IsNullOrEmpty(requiresRole))
            {
                check = AvailableMarkRoles.Where(IsInRole).Aggregate(check, (current, role) => current + (" OR Common_Mark.RequiresRole='" + role + "' "));
            }
            check += ")";
            return check;
        }

        public static string GetParametersCheck()
        {
            var check = AvailableGroupRoles.Where(IsInRole).Aggregate(" (Common_ParameterTypes.RequiresRole is NULL ", (current, role) => current + (" OR Common_ParameterTypes.RequiresRole='" + role + "' "));
            check += ")";
            return check;
        }

        public static string GetOldParametersCheck()
        {
            var check = AvailableGroupRoles.Where(IsInRole).Aggregate(" (tbl_paramtype.RequiresRole is NULL ", (current, role) => current + (" OR tbl_paramtype.RequiresRole='" + role + "' "));
            check += ")";
            return check;
        }

        public static List<string> DecryptRoleString(string encryptedRoles, string keyWord)
        {
            if (encryptedRoles == null || keyWord == null) return new List<string>();
            byte[] encryptBytes = Convert.FromBase64String(encryptedRoles);

            Rfc2898DeriveBytes db = new Rfc2898DeriveBytes(keyWord, new byte[] { 0x1, 0x9, 0x7, 0x3, 0x1, 0x9, 0x5, 0x1 });
            Aes aes = new AesManaged();
            aes.Key = db.GetBytes(aes.LegalKeySizes[0].MaxSize / 8);
            aes.IV = db.GetBytes(aes.BlockSize / 8);

            MemoryStream decryptStream = new MemoryStream();
            CryptoStream decryptor = new CryptoStream(decryptStream, aes.CreateDecryptor(), CryptoStreamMode.Write);
            try
            {
                decryptor.Write(encryptBytes, 0, encryptBytes.Length);
                decryptor.Flush();
                decryptor.Close();

                byte[] decryptBytes = decryptStream.ToArray();
                string decryptedString = Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);

                return decryptedString.Split(';').ToList();
            }
            catch
            {
                return new List<string>();
            }
        }

        public static string EncryptRoleString(List<string> decryptedRoles, string keyWord)
        {
            var toEncrypt = String.Empty;
            var isFirst = true;
            foreach (string s in decryptedRoles)
            {
                if (!isFirst) toEncrypt += ";";
                isFirst = false;
                toEncrypt += s;
            }
            var encryptData = Encoding.UTF8.GetBytes(toEncrypt);

            var db = new Rfc2898DeriveBytes(keyWord, new byte[] { 0x1, 0x9, 0x7, 0x3, 0x1, 0x9, 0x5, 0x1 });
            Aes aes = new AesManaged();
            aes.Key = db.GetBytes(aes.LegalKeySizes[0].MaxSize / 8);
            aes.IV = db.GetBytes(aes.BlockSize / 8);

            MemoryStream encryptStream = new MemoryStream();
            CryptoStream encryptor = new CryptoStream(encryptStream, aes.CreateEncryptor(), CryptoStreamMode.Write);

            encryptor.Write(encryptData, 0, encryptData.Length);
            encryptor.Flush();
            encryptor.Close();
            byte[] encryptBytes = encryptStream.ToArray();
            return Convert.ToBase64String(encryptBytes);
        }

        public static bool IsInRole(string role)
        {
            if (Roles == null || Roles.Count == 0) return false;
            return Roles.Contains(role);
        }

        public static bool IsChief()
        {
            if (CurrentTitle == null) return true;
            return (CurrentTitle.ToLower().Contains("начальн")
                || CurrentTitle.ToLower().Contains("мастер")
                || CurrentTitle.ToLower().Contains("главный")
                || CurrentTitle.ToLower().Contains("директор"));
        }

        public static bool IsWoman()
        {
            if (CurrentUserName == null) return true;
            return CurrentUserName.Trim().EndsWith("а");
        }
    }
}
