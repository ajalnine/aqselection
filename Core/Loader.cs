﻿using System.Windows;

namespace Core
{
    public static class Loader
    {
        public static Size MainTabSize;
        public delegate void SizeChangedDelegate(object o, TabSizeChangedEventArgs e);
        public static event SizeChangedDelegate TabControlSizeChanged;

        static Loader()
        {
            MainTabSize = new Size(0, 0);
        }

        public static void TabSizeChanged(object sender, Size newSize)
        {
            MainTabSize = newSize;
            TabControlSizeChanged?.Invoke(sender, new TabSizeChangedEventArgs(){NewSize = newSize});
        }
    }

    public class TabSizeChangedEventArgs
    {
        public Size NewSize{get;set;}
    }
}
