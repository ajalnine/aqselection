﻿using System;
using System.Data.Services.Client;
using System.ServiceModel;
using System.Xml;
using Core.AQServiceReference;
using Core.CalculationServiceReference;
using Core.ConstructorServiceReference;
using Core.AQDataServiceReference;
using Core.AQCatalogueDataServiceReference;
using System.Configuration;

namespace Core
{
    public static class Services
    {
        public static string Source
        {
            get { return ConfigurationManager.AppSettings["WebRoot"]; }
        }

        public static raportEntities GetRaportEntities()
        {
            var svc = new raportEntities(new Uri(Source + "Services/AQDataService.svc"));
            svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
            return svc;
        }

        public static catalogueEntities GetCatalogueEntities()
        {
            var svc = new catalogueEntities(new Uri(Source + "Services/AQCatalogueDataService.svc"));
            svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
            return svc;
        }

        public static CalculationService GetCalculationService()
        {
            var address = new EndpointAddress(new Uri(Source + "Services/CalculationService.svc"));
            var binding = GetBasicHttpBinding();
            var proxy = new CalculationServiceClient(binding, address);
            return proxy;
        }

        public static ConstructorService GetConstructorService()
        {
            var address = new EndpointAddress(new Uri(Source + "Services/ConstructorService.svc"));
            var binding = GetBasicHttpBinding();
            var proxy = new ConstructorServiceClient(binding, address);
            return proxy;
        }
        
        public static AQService GetAQService()
        {
            var address = new EndpointAddress(new Uri(Source + "Services/AQService.svc"));
            var binding = GetBasicHttpBinding();
            var proxy = new AQServiceClient(binding, address);
            return proxy;
        }

        private static BasicHttpBinding GetBasicHttpBinding()
        {
            TimeSpan ts = new TimeSpan(1, 0, 0);
            BasicHttpBinding binding = new BasicHttpBinding
            {
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647,
                OpenTimeout = ts,
                ReceiveTimeout = ts,
                SendTimeout = ts,
                CloseTimeout = ts,
                TransferMode = TransferMode.Buffered,
                ReaderQuotas = XmlDictionaryReaderQuotas.Max 
            };
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport = new HttpTransportSecurity { ClientCredentialType = HttpClientCredentialType.Windows };
            binding.Security.Message = new BasicHttpMessageSecurity { ClientCredentialType = BasicHttpMessageCredentialType.UserName };
            return binding;
        }
    }
}
