﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using ApplicationCore;
using ApplicationCore.AQServiceReference;

namespace SLParametersEditor
{
    public partial class Page : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals; 
        
        private readonly AQService _aqs;
        private ObservableCollection<ParameterGroupDescription> _originalGroups;
        private ObservableCollection<EditableParameterGroupDescription> _groups;
        private EditableParameterGroupDescription _selectedGroup;
        private FilterMode _currentFilterMode = FilterMode.All;
        private GroupSortMode _currentGroupSortMode = GroupSortMode.Position;
        private ParametersEditMode _currentParametersEditMode = ParametersEditMode.Reorder;
        private readonly List<ParametersCacheItem> _parametersCache;
        private ObservableCollection<string> _parametersDisplayNamesForReorder;
        private ParametersCacheItem _currentParameters;
        private bool _groupsChanged;
        private bool _parametersChanged;
        private readonly AQModuleDescription _aqmd;
        public Page()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            _parametersCache = new List<ParametersCacheItem>();
            _aqs = Services.GetAQService();
            _aqs.BeginGetParameterGroups(GetParameterGroupsDone, new object());
            ReflectRights();
        }

        private void ReflectRights()
        {
            var canEdit = Security.IsInRole("Правка_параметров");
            RightsTextBox.Text = string.Format("Сохранение изменений {0}", (canEdit ? "разрешено" : "запрещено"));
            SaveButton.IsEnabled = canEdit;
        }

        #region Отображение данных

        public void GetParameterGroupsDone(IAsyncResult iar)
        {
            _originalGroups = new ObservableCollection<ParameterGroupDescription>();
            foreach(var g in _aqs.EndGetParameterGroups(iar))_originalGroups.Add(g);
            UpdateGroupsData(_currentGroupSortMode);
            GroupSelector.Dispatcher.BeginInvoke(() =>
            {
                SetGroupsSelector();
                if (GroupSelector.Items.Count <= 0) return;
                GroupSelector.SelectedIndex = 0;
                GroupSelector.UpdateLayout();
            });
        }

        private void GroupSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GroupSelector.SelectedItem == null) return;
            _selectedGroup = ((EditableParameterGroupDescription)GroupSelector.SelectedItem);
            var checkCache  = (from p in _parametersCache where p.GroupID == _selectedGroup.ID select p).ToList();
            var existsInCache = checkCache != null && checkCache.Count() == 1;
            GroupName.Text = _selectedGroup.Name;
            ParametersChangedMark.Visibility = (_selectedGroup.CustomFlag) ? Visibility.Visible : Visibility.Collapsed;
            if (!existsInCache)
            {
                _aqs.BeginGetParameters(_selectedGroup.ID, GetParametersDone, new object());
            }
            else
            {
                _currentParameters = checkCache.Single();
                SetParameterSelector();
            }
        }

        public void GetParametersDone(IAsyncResult iar)
        {
            var loadedParameters = new ObservableCollection<ParameterDescription>();
            foreach (var p in _aqs.EndGetParameters(iar))loadedParameters.Add(p);

            var parameters = new ObservableCollection<EditableParameterDescription>();
            
            _currentParameters = new ParametersCacheItem(_selectedGroup.ID, parameters, loadedParameters);

            UpdateParametersData(_currentParameters, _currentFilterMode);
            _parametersCache.Add(_currentParameters);

            ParameterSelector.Dispatcher.BeginInvoke(SetParameterSelector);
        }

        public void UpdateParametersData(ParametersCacheItem pci, FilterMode fm)
        {
            if (pci == null || pci.OriginalParameters == null) return;
            pci.Parameters = new ObservableCollection<EditableParameterDescription>();
            foreach (var i in pci.OriginalParameters)
            {
                EditableParameterDescription epd;
                switch (fm)
                {
                    case FilterMode.All:
                         epd = new EditableParameterDescription(i);
                        pci.Parameters.Add(epd);
                        epd.PropertyChanged += epd_PropertyChanged;
                        break;
                    case FilterMode.New:
                        if (i.DisplayName.Trim().ToLower() == "новый параметр")
                        {
                            epd = new EditableParameterDescription(i);
                            pci.Parameters.Add(epd);
                            epd.PropertyChanged += epd_PropertyChanged;
                        }
                        break;
                    case FilterMode.ExceptNew:
                        if (i.DisplayName.Trim().ToLower() != "новый параметр")
                        {
                            epd = new EditableParameterDescription(i);
                            pci.Parameters.Add(epd);
                            epd.PropertyChanged += epd_PropertyChanged;
                        }
                        break;
                }
            }
        }

        public void UpdateGroupsData(GroupSortMode gsm)
        {
            if (_originalGroups == null) return;
            _groups = new ObservableCollection<EditableParameterGroupDescription>();
            var source = (gsm == GroupSortMode.Abc) ? _originalGroups.AsQueryable().OrderBy(o => o.Name) : _originalGroups.AsQueryable().OrderBy( o=> o.Position);
            foreach (var i in source)
            {
                var epgd = new EditableParameterGroupDescription(i);
                epgd.PropertyChanged += epgd_PropertyChanged;
                _groups.Add(epgd);
            }
        }

        void epgd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CustomFlag") return; 
            var epgd = sender as EditableParameterGroupDescription;
            if (epgd != null)
            {
                epgd.CustomFlag = true;
                epgd.OriginalParameterGroupDescription.CustomFlag = true;
                var changedGroup = (from g in _groups where g.ID == epgd.ID select g).SingleOrDefault();
                if (changedGroup != null)
                {
                    changedGroup.CustomFlag = true;
                    changedGroup.OriginalParameterGroupDescription.CustomFlag = true;
                }
            }
            _parametersChanged = true;
        }

        void epd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CustomFlag") return;
            var epd = sender as EditableParameterDescription;
            if (epd != null)
            {
                epd.CustomFlag = true;
                epd.OriginalParameterDescription.CustomFlag = true;
                var changedGroup = (from g in _groups where g.ID == epd.TypeID select g).SingleOrDefault();
                if (changedGroup != null)
                {
                    changedGroup.CustomFlag = true;
                    changedGroup.OriginalParameterGroupDescription.CustomFlag = true;
                }
                _parametersChanged = true;
                ParametersChangedMark.Visibility = Visibility.Visible;
                SaveButton.IsEnabled = true;
                epd.OriginalParameterDescription.DisplayName = epd.DisplayName;
                epd.OriginalParameterDescription.IsNumber = epd.IsNumber;
            }
            RefreshHasNewParameterInformation();
        }

        public void SetParameterSelector()
        {
            if (_currentParameters == null) return;
            ((ParametersCacheItemHelper)Resources["ParametersCacheItemHelper"]).CurrentParameters = _currentParameters;
            ((DisplayNames)Resources["DisplayNames"]).DisplayParameters = _currentParameters.DisplayParameters;
            ParameterSelector.ItemsSource = _currentParameters.Parameters;
            ParameterSelector.UpdateLayout();
            if (_groups.Count > 0)
            {
                ParameterSelector.SelectedIndex = 0;
                ParameterSelector.UpdateLayout();
            }
            if (ParametersOrderEditor != null && _currentParametersEditMode == ParametersEditMode.Reorder)
            {
                _parametersDisplayNamesForReorder = new ObservableCollection<string>(); 
                _parametersDisplayNamesForReorder.PopulateFrom((from p in _currentParameters.Parameters orderby p.Position select p.DisplayName).Distinct().ToList());
                ParametersOrderEditor.ItemsSource = _parametersDisplayNamesForReorder;
            }
        }

        public void SetGroupsSelector()
        {
            if (_groups == null) return;
            GroupSelector.ItemsSource = null;
            GroupSelector.UpdateLayout();
            GroupSelector.ItemsSource = _groups;
            GroupSelector.UpdateLayout();
        }
        #endregion

        #region Редактирование отображаемого имени

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var newValue = ((TextBox)sender).Text;
            var displayParameters = ((DisplayNames)Resources["DisplayNames"]).DisplayParameters;
            if (!displayParameters.Contains(newValue)) displayParameters.Add(newValue);
            ParameterSelector.UpdateLayout();
        }

        private void CopyName_Click(object sender, RoutedEventArgs e)
        {
            var mib = (TextImageButtonBase)sender;
            var id = (int)mib.Tag;
            var currentParameter = (from p in _currentParameters.Parameters where p.ID == id select p).SingleOrDefault();
            var displayParameters = ((DisplayNames)Resources["DisplayNames"]).DisplayParameters;
            if (currentParameter != null && !displayParameters.Contains(currentParameter.Name)) displayParameters.Add(currentParameter.Name);
            if (currentParameter != null) currentParameter.DisplayName = currentParameter.Name;
        }

        private void RefreshHasNewParameterInformation()
        {
            var newParametersCount = (from p in _currentParameters.Parameters where p.DisplayName.ToLower().Contains("новый параметр") select p.ID).Count();
            _selectedGroup.HasNewParameters = (newParametersCount != 0);
            _selectedGroup.OriginalParameterGroupDescription.HasNewParameters = _selectedGroup.HasNewParameters;
        }
        #endregion

        #region Изменение режимов

        private void ModeIsCombo_Checked(object sender, RoutedEventArgs e)
        {
            if (ParameterSelector!= null && ParameterSelector.Columns != null)
            {
                if (ParameterSelector.Columns.Count > 2)
                {
                    ParameterSelector.Columns[1].Visibility = Visibility.Visible;
                    ParameterSelector.Columns[2].Visibility = Visibility.Collapsed;
                }
            }
            if (ModeIsNew!=null) ModeIsNew.IsEnabled = true;
            if (ModeIsExceptNew != null) ModeIsExceptNew.IsEnabled = true;
            if (ParameterSelector != null) ParameterSelector.Visibility = Visibility.Visible;
            if (ReorderParameterPanel != null) ReorderParameterPanel.Visibility = Visibility.Collapsed;
            if (ParametersOrderEditor != null) ParametersOrderEditor.Visibility = Visibility.Collapsed;
            _currentParametersEditMode = ParametersEditMode.Combo;
        }

        private void ModeIsText_Checked(object sender, RoutedEventArgs e)
        {
            if (ParameterSelector != null && ParameterSelector.Columns != null)
            {
                if (ParameterSelector.Columns.Count > 2)
                {
                    ParameterSelector.Columns[2].Visibility = Visibility.Visible;
                    ParameterSelector.Columns[1].Visibility = Visibility.Collapsed;
                }
            }
            if (ModeIsNew != null) ModeIsNew.IsEnabled = true;
            if (ModeIsExceptNew != null) ModeIsExceptNew.IsEnabled = true;
            if (ParameterSelector != null) ParameterSelector.Visibility = Visibility.Visible;
            if (ReorderParameterPanel != null) ReorderParameterPanel.Visibility = Visibility.Collapsed;
            if (ParametersOrderEditor != null) ParametersOrderEditor.Visibility = Visibility.Collapsed;
            _currentParametersEditMode = ParametersEditMode.Text;
        }

        private void ModeIsOrder_Checked(object sender, RoutedEventArgs e)
        {
            if (ModeIsNew != null) ModeIsNew.IsEnabled = false;
            if (ModeIsExceptNew != null) ModeIsExceptNew.IsEnabled = false;
            if (ParameterSelector != null) ParameterSelector.Visibility = Visibility.Collapsed;
            if (ReorderParameterPanel!=null) ReorderParameterPanel.Visibility = Visibility.Visible;
            
            if (ParametersOrderEditor != null)
            {
                _parametersDisplayNamesForReorder = new ObservableCollection<string>();
                ParametersOrderEditor.Visibility = Visibility.Visible;
                _parametersDisplayNamesForReorder.PopulateFrom((from p in _currentParameters.Parameters orderby p.Position select p.DisplayName).Distinct().ToList());
                ParametersOrderEditor.ItemsSource = _parametersDisplayNamesForReorder;

            }
            _currentParametersEditMode = ParametersEditMode.Reorder;
        }
        
        private void ModeIsAll_Checked(object sender, RoutedEventArgs e)
        {
            _currentFilterMode = FilterMode.All;
            UpdateParametersData(_currentParameters, _currentFilterMode);
            SetParameterSelector();
            if(ModeIsOrder!=null) ModeIsOrder.IsEnabled = true;
        }

        private void ModeIsNew_Checked(object sender, RoutedEventArgs e)
        {
            _currentFilterMode = FilterMode.New;
            UpdateParametersData(_currentParameters, _currentFilterMode);
            SetParameterSelector();
            if (ModeIsOrder != null) ModeIsOrder.IsEnabled = false;
        }

        private void ModeIsExceptNew_Checked(object sender, RoutedEventArgs e)
        {
            _currentFilterMode = FilterMode.ExceptNew;
            UpdateParametersData(_currentParameters, _currentFilterMode);
            SetParameterSelector();
            if (ModeIsOrder != null) ModeIsOrder.IsEnabled = false;
        }

        private void SortModeIsABC_Checked(object sender, RoutedEventArgs e)
        {
            if (GroupSelector == null) return;
            _selectedGroup = ((EditableParameterGroupDescription)GroupSelector.SelectedItem); 
            _currentGroupSortMode = GroupSortMode.Abc;
            GroupSelector.SelectionChanged -= GroupSelector_SelectionChanged;
            UpdateGroupsData(_currentGroupSortMode);
            SetGroupsSelector();
            if (_selectedGroup == null) return;
            var newSelected = (from g in _groups where g.ID == _selectedGroup.ID select g).Single();
            GroupSelector.SelectedIndex = _groups.IndexOf(newSelected);
            GroupSelector.UpdateLayout();
            GroupSelector.SelectionChanged += GroupSelector_SelectionChanged;
            ReorderGroupPanel.Visibility = Visibility.Collapsed;
        }

        private void SortModeIsPosition_Checked(object sender, RoutedEventArgs e)
        {
            if (GroupSelector == null) return; 
            _selectedGroup = ((EditableParameterGroupDescription)GroupSelector.SelectedItem); 
            _currentGroupSortMode = GroupSortMode.Position;
            GroupSelector.SelectionChanged -= GroupSelector_SelectionChanged; 
            UpdateGroupsData(_currentGroupSortMode);
            SetGroupsSelector();
            if (_selectedGroup == null) return; 
            var newSelected = (from g in _groups where g.ID == _selectedGroup.ID select g).Single();
            GroupSelector.SelectedIndex = _groups.IndexOf(newSelected);
            GroupSelector.UpdateLayout();
            GroupSelector.SelectionChanged += GroupSelector_SelectionChanged;
            ReorderGroupPanel.Visibility = Visibility.Visible;
        }
        #endregion

        #region Изменение порядка групп

        private void BottomGroup_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = GroupSelector.SelectedIndex;
            var newIndex = GroupSelector.Items.Count -1;
            MoveGroup(oldIndex, newIndex);
        }

        private void DownGroup_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = GroupSelector.SelectedIndex;
            var newIndex = (oldIndex == GroupSelector.Items.Count - 1)? 0: oldIndex + 1;
            MoveGroup(oldIndex, newIndex);
         }

        private void UpGroup_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = GroupSelector.SelectedIndex;
            var newIndex = (oldIndex == 0) ? GroupSelector.Items.Count - 1 : oldIndex - 1;
            MoveGroup(oldIndex, newIndex);
        }

        private void TopGroup_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = GroupSelector.SelectedIndex;
            const int newIndex = 0;
            MoveGroup(oldIndex, newIndex);
        }

        private void MoveGroup(int oldIndex, int newIndex)
        {
            var itemToMove = _groups[oldIndex];
            _groups.RemoveAt(oldIndex);
            _groups.Insert(newIndex, itemToMove);
            GroupSelector.UpdateLayout();
            GroupSelector.SelectedIndex = newIndex;
            GroupSelector.UpdateLayout();
            GroupSelector.ScrollIntoView(itemToMove);

            for (var i = 0; i < _groups.Count; i++)
            {
                _groups[i].Position = i;
                _groups[i].OriginalParameterGroupDescription.Position = i;
            }
            _groupsChanged = true;
            GroupsChangedMark.Visibility = Visibility.Visible;
            SaveButton.IsEnabled = true;
        }
        #endregion
        
        #region Изменение порядка параметров

        private void BottomParameter_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = ParametersOrderEditor.SelectedIndex;
            var newIndex = ParametersOrderEditor.Items.Count - 1;
            MoveParameter(oldIndex, newIndex);
        }

        private void DownParameter_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = ParametersOrderEditor.SelectedIndex;
            var newIndex = (oldIndex == ParametersOrderEditor.Items.Count - 1) ? 0 : oldIndex + 1;
            MoveParameter(oldIndex, newIndex);
        }

        private void UpParameter_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = ParametersOrderEditor.SelectedIndex;
            var newIndex = (oldIndex == 0) ? ParametersOrderEditor.Items.Count - 1 : oldIndex - 1;
            MoveParameter(oldIndex, newIndex);
        }

        private void TopParameter_Click(object sender, RoutedEventArgs e)
        {
            var oldIndex = ParametersOrderEditor.SelectedIndex;
            const int newIndex = 0;
            MoveParameter(oldIndex, newIndex);
        }

        private void MoveParameter(int oldIndex, int newIndex)
        {
            if (ParametersOrderEditor.SelectedItem == null) return;
            string itemToMove = _parametersDisplayNamesForReorder[oldIndex];
            _parametersDisplayNamesForReorder.RemoveAt(oldIndex);
            _parametersDisplayNamesForReorder.Insert(newIndex, itemToMove);
            ParametersOrderEditor.UpdateLayout();
            ParametersOrderEditor.SelectedIndex = newIndex;
            ParametersOrderEditor.UpdateLayout();
            ParametersOrderEditor.ScrollIntoView(itemToMove);

            var position = 0;
            foreach (var t in _parametersDisplayNamesForReorder)
            {
                var t1 = t;
                var toPositionChange = from p in _currentParameters.Parameters where p.DisplayName==t1 select p;
                foreach (var p in toPositionChange)
                {
                    p.Position = position;
                    p.OriginalParameterDescription.Position = position;
                    position++;
                }
            }
            
            _groupsChanged = true;
            GroupsChangedMark.Visibility = Visibility.Visible;
            SaveButton.IsEnabled = true;
        }
        #endregion

        #region Сохранение изменений

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var task = new Task(new[] { "Запись групп", "Запись параметров" }, "Сохранение изменений", TaskPanel);
            task.SetState(0, TaskState.Processing);
            task.SetMessage(string.Empty);
            var changedGroups = GetChangedGroups();
            if (changedGroups.Count > 0)
            {
                _aqs.BeginUpdateParameterGroups(changedGroups.ToList(), UpdateParameterGroupsDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                var changedParameters = GetChangedParameters();
                if (changedParameters.Count > 0) _aqs.BeginUpdateParameters(changedParameters.ToList(), UpdateParametersDone, task);
                else
                {
                    task.SetState(1, TaskState.Ready);
                    SaveDone();
                }
            }
        }

        private void UpdateParameterGroupsDone(IAsyncResult iar)
        {
            var result = _aqs.EndUpdateParameterGroups(iar);
            var task = (Task)iar.AsyncState;
            if (result)
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                var changedParameters = GetChangedParameters();
                if (changedParameters.Count > 0) _aqs.BeginUpdateParameters(changedParameters.ToList(), UpdateParametersDone, task);
                else
                {
                    task.SetState(1, TaskState.Ready);
                    SaveDone();
                }
            }
            else task.SetState(0, TaskState.Error);
        }

        private void UpdateParametersDone(IAsyncResult iar)
        {
            var result = _aqs.EndUpdateParameters(iar);
            var task = (Task)iar.AsyncState;
            if (result)
            {
                task.SetState(1, TaskState.Ready);
                SaveDone();
            }
            else
            {
                task.SetState(1, TaskState.Error);
            }
        }

        private void SaveDone()
        {
            Dispatcher.BeginInvoke(() =>
            {
                foreach (var g in (from g in _groups where g.OriginalParameterGroupDescription.CustomFlag select g))
                {
                    var changedParameters =
                        (from p in _parametersCache where p.GroupID == g.ID select p.Parameters).Single();

                    foreach (
                        var p in (from c in changedParameters where c.OriginalParameterDescription.CustomFlag select c))
                    {
                        p.CustomFlag = false;
                        p.OriginalParameterDescription.CustomFlag = false;
                    }
                }
                foreach (var a in _groups)
                {
                    a.CustomFlag = false;
                    a.OriginalParameterGroupDescription.CustomFlag = false;
                }
                GroupsChangedMark.Visibility = Visibility.Collapsed;
                ParametersChangedMark.Visibility = Visibility.Collapsed;
                SaveButton.IsEnabled = false;
            });
        }

        private ObservableCollection<ParameterGroupUpdate> GetChangedGroups()
        {
            var result = new ObservableCollection<ParameterGroupUpdate>();

            if (_groupsChanged)
            {
                foreach (var g in _groups)
                {
                    var pgu = new ParameterGroupUpdate
                    {
                        ID = g.OriginalParameterGroupDescription.ID,
                        Position = g.OriginalParameterGroupDescription.Position
                    };
                    result.Add(pgu);
                }
            }
            return result;
        }

        private ObservableCollection<ParameterUpdate> GetChangedParameters()
        {
            var result = new ObservableCollection<ParameterUpdate>();
            if (!_parametersChanged) return result;
            foreach (var g in (from g in _groups where g.OriginalParameterGroupDescription.CustomFlag select g))
            {
                var changedParameters = (from p in _parametersCache where p.GroupID==g.ID select p.Parameters).Single();

                foreach (var p in (from c in changedParameters where c.OriginalParameterDescription.CustomFlag select c))
                {
                    var pu = new ParameterUpdate
                    {
                        ID = p.OriginalParameterDescription.ID,
                        IsNumber = p.OriginalParameterDescription.IsNumber,
                        DisplayName = p.OriginalParameterDescription.DisplayName,
                        Position = p.OriginalParameterDescription.Position
                    };
                    result.Add(pu);
                }
            }
            return result;
        }
        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }
    }

    #region Вспомогательные классы
    public class ParametersCacheItem
    {
        public int GroupID;
        public ObservableCollection<EditableParameterDescription> Parameters;
        public ObservableCollection<ParameterDescription> OriginalParameters;
        public bool Changed;
        public ObservableCollection<String> DisplayParameters { get { return new ObservableCollection<string>().PopulateFrom((from p in OriginalParameters select p.DisplayName).Distinct()); } }

        public ParametersCacheItem(int groupid, ObservableCollection<EditableParameterDescription> parameters, ObservableCollection<ParameterDescription> originalparameters)
        {
            Changed = false;
            GroupID = groupid;
            Parameters = parameters;
            OriginalParameters = originalparameters;
        }
    }

    public class DisplayNames
    {
        public ObservableCollection<String> DisplayParameters {get;set;}
    }

    public class ParametersCacheItemHelper
    {
        public ParametersCacheItem CurrentParameters { get; set; }
    }

    public static class LinqHelper
    {
        public static ObservableCollection<T> PopulateFrom<T>(this ObservableCollection<T> collection, IEnumerable<T> range)
        {
            foreach (var x in range)
            {
                collection.Add(x);
            }
            return collection;
        }
    }

    public class EditableParameterDescription: INotifyPropertyChanged
    {
        private int _id;
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged("ID");
            }
        }
        
        private int _typeid;
        public int TypeID
        {
            get
            {
                return _typeid;
            }
            set
            {
                _typeid = value;
                NotifyPropertyChanged("TypeID");
            }
        }

        private int _position;
        public int Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                NotifyPropertyChanged("Position");
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private string _displayname;
        public string DisplayName
        {
            get
            {
                return _displayname;
            }
            set
            {
                _displayname = value;
                NotifyPropertyChanged("DisplayName");
            }
        }

        private bool _isnumber;
        public bool IsNumber
        {
            get
            {
                return _isnumber;
            }
            set
            {
                _isnumber = value;
                NotifyPropertyChanged("IsNumber");
            }
        }

        private bool _customflag;
        public bool CustomFlag
        {
            get
            {
                return _customflag;
            }
            set
            {
                _customflag = value;
                NotifyPropertyChanged("CustomFlag");
            }
        }

        public ParameterDescription OriginalParameterDescription { get; set; }

        public EditableParameterDescription(ParameterDescription pd)
        {
            ID = pd.ID;
            IsNumber = pd.IsNumber;
            Name = pd.Name;
            Position = pd.Position;
            TypeID = pd.TypeID;
            DisplayName = pd.DisplayName;
            CustomFlag = pd.CustomFlag;
            OriginalParameterDescription = pd;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }  
    }

    public class EditableParameterGroupDescription : INotifyPropertyChanged
    {
        private int _id;
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged("ID");
            }
        }

        public int Position { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool _customflag;
        public bool CustomFlag
        {
            get
            {
                return _customflag;
            }
            set
            {
                _customflag = value;
                NotifyPropertyChanged("CustomFlag");
            }
        }

        private bool _hasnewparameters;
        public bool HasNewParameters
        {
            get
            {
                return _hasnewparameters;
            }
            set
            {
                _hasnewparameters = value;
                NotifyPropertyChanged("HasNewParameters");
            }
        }

        public ParameterGroupDescription OriginalParameterGroupDescription { get; set; }

        public EditableParameterGroupDescription(ParameterGroupDescription pd)
        {
            ID = pd.ID;
            Name = pd.Name;
            Position = pd.Position;
            CustomFlag = pd.CustomFlag;
            HasNewParameters = pd.HasNewParameters;
            OriginalParameterGroupDescription = pd;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public enum FilterMode {All, New, ExceptNew }
    public enum GroupSortMode { Abc, Position }
    public enum ParametersEditMode { Combo, Text, Reorder }

    public class CustomFlagToForegroundValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            object o = ((bool)value) ? "#FFE00000": "Black";
            return o;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class BooleanToVisibilityValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            object o = ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
            return o;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    #endregion
}