﻿using System.Linq;
using System.Windows;

namespace SLParametersEditor
{
    public partial class ParameterItem
    {
        public string  DisplayParameterName
        {
            get { return (string )GetValue(DisplayParameterNameProperty); }
            set { SetValue(DisplayParameterNameProperty, value); }
        }

        public static readonly DependencyProperty DisplayParameterNameProperty =
            DependencyProperty.Register("DisplayParameterName", typeof(string ), typeof(ParameterItem), new PropertyMetadata(string.Empty, DisplayParameterName_Changed));

        public ParametersCacheItem Parameters
        {
            get { return (ParametersCacheItem)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public static readonly DependencyProperty ParametersProperty =
            DependencyProperty.Register("Parameters", typeof(ParametersCacheItem), typeof(ParameterItem), new PropertyMetadata(null, Parameters_Changed));

        public ParameterItem()
        {
            InitializeComponent();
        }

        public static void DisplayParameterName_Changed(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ParameterItem)o).ConstructControl();
        }

        public static void Parameters_Changed(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((ParameterItem)o).ConstructControl();
        }
        
        public void ConstructControl()
        {
            if (DisplayParameterName == null || Parameters == null) return;
            CombinedParametersNames.ItemsSource = (from p in Parameters.Parameters where p.DisplayName==DisplayParameterName orderby p.Position select p.Name).ToList();
            ParameterName.Text = DisplayParameterName;
            DividerPanel.Children.Clear();
            if (CombinedParametersNames.Items.Count > 1)
            {
                DividerPanel.Children.Add(new Bracket());
                DividerPanel.VerticalAlignment = VerticalAlignment.Stretch;
            }
            else
            {
                DividerPanel.Children.Add(new Line());
                DividerPanel.VerticalAlignment = VerticalAlignment.Center;
            }
        }
    }
}
