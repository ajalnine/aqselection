﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using System.Data.Services.Client;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.EntityDataServiceReference;
using AQControlsLibrary;
using AQBasicControlsLibrary;

namespace SLT31Editor
{
    public partial class MainPage : UserControl, IAQModule
    {
        #region Свойства и поля
        CommandCallBackDelegate ReadySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals; 
        
        raportEntities svcContext;
        DataServiceQuery<T31_Departments> T31_DepartmentsQuery;
        DataServiceQuery<T31_Instructions> T31_InstructionsQuery;
        DataServiceQuery<T31_Orders> T31_OrdersQuery;
        DataServiceQuery<T31_Directions> T31_DirectionsQuery;
        DataServiceQuery<T31_Settings> T31_SettingsQuery;
        DataServiceQuery<T31_UserActions> T31_UserActionsQuery;
        ObservableCollection<T31_Instructions> Instructions = new ObservableCollection<T31_Instructions>();
        ObservableCollection<T31_Orders> Orders = new ObservableCollection<T31_Orders>();
        ObservableCollection<T31_Directions> Directions = new ObservableCollection<T31_Directions>();
        ObservableCollection<T31_UserActions> UserActions = new ObservableCollection<T31_UserActions>();

        AQService aqs;
        ComboBox DepartmentFilter;
        RoleInfo MainRoleInfo;
        ObservableCollection<T31_Settings> Settings = new ObservableCollection<T31_Settings>();
        int DepartmentID = 255;
        bool OrdersLoaded = false;
        bool DirectionsLoaded = false;
        bool DepartmentsLoaded = false;
        bool UserActionsLoaded = false;
        bool ReportPageConstructed = false;
        int SelectedInstruction = 0;
        int SelectedOrder = 0;
        int SelectedDirection = 0;
        bool IsEditionEnabled; 
        bool IsInstructionsEnabled;
        bool IsOrdersEnabled;
        bool IsDirectionsEnabled;
        string CurrentCode = string.Empty;

        #endregion

        #region Инициализация
        public MainPage()
        {
            InitializeComponent();
            svcContext = new raportEntities(new Uri("/AQSelection/Services/EntityDataService.svc", UriKind.Relative));
            svcContext.MergeOption = MergeOption.NoTracking;
            aqs = Services.GetAQService();
        }

        private void EnableNavigation(bool IsEnabled)
        {
            MainTabControl.IsEnabled = true;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tc = sender as TabControl;
            switch (tc.SelectedIndex)
            {
                case 1:
                    if (!OrdersLoaded) FullRefreshOrders();
                    OrdersLoaded = true;
                    break;
                case 2:
                    if (!DirectionsLoaded) FullRefreshDirections();
                    DirectionsLoaded = true;
                    break;
                case 4:
                    if (!ReportPageConstructed) ConstructReportPage();
                    ReportPageConstructed = true;
                    break;
                case 5:
                    if (!UserActionsLoaded) FullRefreshUserActions();
                    UserActionsLoaded = true;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Инструкции - загрузка

        private void FullRefreshInstructions()
        {
            EnableNavigation(false);
            InstructionsPager.CurrentPage = 0;
            if (DepartmentID!=255)
            {
                svcContext.BeginExecute<int>(new Uri("T31_InstructionsCountByDepartment?ID=" + DepartmentID.ToString(), UriKind.Relative), T31_InstructionsCountQueryComplete, new object());
            }
            else
            {
                svcContext.BeginExecute<int>(new Uri("T31_InstructionsCount", UriKind.Relative), T31_InstructionsCountQueryComplete, new object());
            }
        }

        private void T31_InstructionsCountQueryComplete(IAsyncResult iar)
        {
            InstructionsPager.TotalRecords = svcContext.EndExecute<int>(iar).First();
            this.Dispatcher.BeginInvoke(delegate()
            {
                RefreshInstructions();
            });
        }


        private void InstructionsPager_CurrentPageChanged(object o, EventArgs e)
        {
            SelectedInstruction = 0;
            RefreshInstructions();
        }

        private void RefreshInstructions()
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                EnableNavigation(false); 
                svcContext = new raportEntities(new Uri("/AQSelection/Services/EntityDataService.svc", UriKind.Relative));
                string FilterText;
                if (DepartmentID != 255)
                {
                    FilterText = String.Format("T31_Departments/id eq {0} and IsDeleted eq false", DepartmentID);
                }
                else
                {
                    FilterText = "IsDeleted eq false";
                }
                T31_InstructionsQuery = svcContext.T31_Instructions.AddQueryOption("$orderby", "Position").AddQueryOption("$top", InstructionsPager.PageSize.ToString()).AddQueryOption("$skip", (InstructionsPager.PageSize * InstructionsPager.CurrentPage).ToString()).Expand("T31_Departments").AddQueryOption("$filter", FilterText);
                T31_InstructionsQuery.BeginExecute(T31_InstructionsQueryCompleted, new object());
            });
        }


        private void T31_InstructionsQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Instructions.Clear();
                IEnumerable<T31_Instructions> I = T31_InstructionsQuery.EndExecute(iar);
                foreach (T31_Instructions item in I)
                {
                    Instructions.Add(item);
                    if (item.T31_Departments != null) svcContext.Detach(item.T31_Departments);
                    svcContext.Detach(item);
                }
                InstructionsDataGrid.ItemsSource = null;
                InstructionsDataGrid.UpdateLayout();
                InstructionsDataGrid.ItemsSource = Instructions;
                InstructionsDataGrid.UpdateLayout();
                InstructionsDataGrid.SelectedIndex = SelectedInstruction;
                if (InstructionsDataGrid.SelectedItem != null)
                {
                    InstructionsDataGrid.ScrollIntoView(InstructionsDataGrid.SelectedItem, InstructionsDataGrid.Columns[0]);
                    InstructionsDataGrid.UpdateLayout();
                }
                EnableNavigation(true);
            });
        }

        #endregion

        #region Приказы - загрузка

        private void FullRefreshOrders()
        {
            EnableNavigation(false);
            svcContext.BeginExecute<int>(new Uri("T31_OrdersCount", UriKind.Relative), T31_OrdersCountQueryComplete, new object());
        }

        private void T31_OrdersCountQueryComplete(IAsyncResult iar)
        {
            OrdersPager.TotalRecords = svcContext.EndExecute<int>(iar).First();
            this.Dispatcher.BeginInvoke(delegate()
            {
                RefreshOrders();
            });
        }
        
        private void OrdersPager_CurrentPageChanged(object o, EventArgs e)
        {
            SelectedOrder = 0;
            RefreshOrders();
        }

        private void RefreshOrders()
        {
            EnableNavigation(false); 
            this.Dispatcher.BeginInvoke(delegate()
            {
                svcContext = new raportEntities(new Uri("/AQSelection/Services/EntityDataService.svc", UriKind.Relative));
                string FilterText = "IsDeleted eq false";
                T31_OrdersQuery = svcContext.T31_Orders.AddQueryOption("$orderby", "Position").AddQueryOption("$top", OrdersPager.PageSize.ToString()).AddQueryOption("$skip", (OrdersPager.PageSize * OrdersPager.CurrentPage).ToString()).AddQueryOption("$filter", FilterText);
                T31_OrdersQuery.BeginExecute(T31_OrdersQueryCompleted, new object());
            });
        }


        private void T31_OrdersQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Orders.Clear();
                IEnumerable<T31_Orders> O = T31_OrdersQuery.EndExecute(iar);
                foreach (T31_Orders item in O)
                {
                    Orders.Add(item);
                }
                OrdersDataGrid.ItemsSource = null;
                OrdersDataGrid.UpdateLayout();
                OrdersDataGrid.ItemsSource = Orders;
                OrdersDataGrid.UpdateLayout();
                OrdersDataGrid.SelectedIndex = SelectedOrder;
                OrdersDataGrid.ScrollIntoView(OrdersDataGrid.SelectedItem, OrdersDataGrid.Columns[0]);
                OrdersDataGrid.UpdateLayout(); 
                EnableNavigation(true);
            });
        }

        #endregion

        #region Распоряжения - загрузка
        
        private void FullRefreshDirections()
        {
            EnableNavigation(false);
            svcContext.BeginExecute<int>(new Uri("T31_DirectionsCount", UriKind.Relative), T31_DirectionsCountQueryComplete, new object());
        }

        private void T31_DirectionsCountQueryComplete(IAsyncResult iar)
        {
            DirectionsPager.TotalRecords = svcContext.EndExecute<int>(iar).First();
            this.Dispatcher.BeginInvoke(delegate()
            {
                RefreshDirections();
            });
        }

        private void DirectionsPager_CurrentPageChanged(object o, EventArgs e)
        {
            SelectedDirection = 0;
            RefreshDirections();
        }

        private void RefreshDirections()
        {
            EnableNavigation(false);
            
            this.Dispatcher.BeginInvoke(delegate()
            {
                svcContext = new raportEntities(new Uri("/AQSelection/Services/EntityDataService.svc", UriKind.Relative));
                string FilterText = "IsDeleted eq false";
                T31_DirectionsQuery = svcContext.T31_Directions.AddQueryOption("$orderby", "Position").AddQueryOption("$top", DirectionsPager.PageSize.ToString()).AddQueryOption("$skip", (DirectionsPager.PageSize * DirectionsPager.CurrentPage).ToString()).AddQueryOption("$filter", FilterText);
                T31_DirectionsQuery.BeginExecute(T31_DirectionsQueryCompleted, new object());
            });
        }

        private void T31_DirectionsQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Directions.Clear();
                IEnumerable<T31_Directions> D = T31_DirectionsQuery.EndExecute(iar);
                foreach (T31_Directions item in D)
                {
                    Directions.Add(item);
                }
                DirectionsDataGrid.ItemsSource = null;
                DirectionsDataGrid.UpdateLayout();
                DirectionsDataGrid.ItemsSource = Directions;
                DirectionsDataGrid.UpdateLayout();
                DirectionsDataGrid.SelectedIndex = SelectedDirection;
                DirectionsDataGrid.ScrollIntoView(DirectionsDataGrid.SelectedItem, DirectionsDataGrid.Columns[0]);
                DirectionsDataGrid.UpdateLayout();
                EnableNavigation(true);
            });
        }

        #endregion 

        #region Действия пользователей - загрузка
        #region Распоряжения - загрузка

        private void FullRefreshUserActions()
        {
            EnableNavigation(false);
            svcContext.BeginExecute<int>(new Uri("T31_UserActionsCount", UriKind.Relative), T31_UserActionsCountQueryComplete, new object());
        }

        private void T31_UserActionsCountQueryComplete(IAsyncResult iar)
        {
            UserActionsPager.TotalRecords = svcContext.EndExecute<int>(iar).First();
            this.Dispatcher.BeginInvoke(delegate()
            {
                RefreshUserActions();
            });
        }
        private void UserActionsPager_CurrentPageChanged(object o, EventArgs e)
        {
            RefreshUserActions();
        }

        private void RefreshUserActions()
        {
            EnableNavigation(false);

            this.Dispatcher.BeginInvoke(delegate()
            {
                svcContext = new raportEntities(new Uri("/AQSelection/Services/EntityDataService.svc", UriKind.Relative));
                T31_UserActionsQuery = svcContext.T31_UserActions.AddQueryOption("$orderby", "Date desc").AddQueryOption("$top", UserActionsPager.PageSize.ToString()).AddQueryOption("$skip", (UserActionsPager.PageSize * UserActionsPager.CurrentPage).ToString());
                T31_UserActionsQuery.BeginExecute(T31_UserActionsQueryCompleted, new object());
            });
        }

        private void T31_UserActionsQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                UserActions.Clear();
                IEnumerable<T31_UserActions> U = T31_UserActionsQuery.EndExecute(iar);
                foreach (T31_UserActions item in U)
                {
                    UserActions.Add(item);
                } 
                UserActionsDataGrid.ItemsSource = null;
                UserActionsDataGrid.UpdateLayout();
                UserActionsDataGrid.ItemsSource = UserActions;
                UserActionsDataGrid.UpdateLayout();
                EnableNavigation(true);
            });
        }

        #endregion 

        #endregion

        #region Инструкции - редактирование

        private void InstructionsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            
            if (InstructionsDataGrid.SelectedItem == null) return;

            T31_Instructions i = InstructionsDataGrid.SelectedItem as T31_Instructions;
            i.T31_Departments = cb.SelectedItem as T31_Departments;
            
            svcContext.AttachTo("T31_Departments", i.T31_Departments);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            try
            {
                svcContext.AttachTo("T31_Instructions", i);
            }
            catch { }
            svcContext.UpdateObject(i);
            svcContext.SetLink(i, "T31_Departments", i.T31_Departments);
            WriteAction("ТИ", "Подразделение", i.Code, i.T31_Departments.DepartmentName);
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void InstructionsTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (InstructionsDataGrid.SelectedItem == null) return;
            TextBox tb = sender as TextBox;
            
            T31_Instructions i = InstructionsDataGrid.SelectedItem as T31_Instructions;
            try
            {
                svcContext.AttachTo("T31_Instructions", i);
            }
            catch { }
            i.GetType().GetProperty(tb.Name).SetValue(i, tb.Text, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(InstructionsDataGrid.SelectedItem);
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void InstructionsSaveChangesCompleted(IAsyncResult iar)
        {
            DataServiceResponse dsr = svcContext.EndSaveChanges(iar);
            T31_Instructions i = InstructionsDataGrid.SelectedItem as T31_Instructions;
            if (i != null && i.T31_Departments != null)
            {
                svcContext.DetachLink(i, "T31_Departments", i.T31_Departments);
                svcContext.Detach(i.T31_Departments);
                svcContext.Detach(i);
            }
        }

        private void InsertInstruction_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            T31_Instructions t = new T31_Instructions() { Position = (int)mib.Tag + 1, ChangedAt = DateTime.Now, ChangedBy = "...", IsDeleted = false };
            int index = Instructions.IndexOf(InstructionsDataGrid.SelectedItem as T31_Instructions);
            svcContext.AddObject("T31_Instructions", t);
            InstructionsPager.TotalRecords++;
            SelectedInstruction = index;
            WriteAction("ТИ", "Новая инструкция", "", "");
            svcContext.BeginExecute<string>(new Uri("T31_InstructionsIncrementPosition?PositionBefore=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), InstructionsIncrementPositionComplete, index);
            
        }

        private void InstructionsIncrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index > InstructionsPager.PageSize - 2)
            {
                SelectedInstruction = 0;
                InstructionsPager.CurrentPage++;
            }
            else
            {
                SelectedInstruction++;
                this.Dispatcher.BeginInvoke(delegate()
                {
                    RefreshInstructions();
                });
            }
        }

        private void DeleteInstruction_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            
            int index = Instructions.IndexOf(InstructionsDataGrid.SelectedItem as T31_Instructions);
            InstructionsPager.TotalRecords--;
            (InstructionsDataGrid.SelectedItem as T31_Instructions).IsDeleted = true;
            svcContext.AttachTo("T31_Instructions", InstructionsDataGrid.SelectedItem as T31_Instructions);
            svcContext.UpdateObject(InstructionsDataGrid.SelectedItem);
            SelectedInstruction = index;
            WriteAction("ТИ", "Удаление инструкции", (InstructionsDataGrid.SelectedItem as T31_Instructions).Code, "");
            svcContext.BeginExecute<string>(new Uri("T31_InstructionsDecrementPosition?PositionCurrent=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), InstructionsDecrementPositionComplete, index); 
        }

        private void InstructionsDecrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index == 0)
            {
                SelectedInstruction = InstructionsPager.PageSize - 1;
                InstructionsPager.CurrentPage--;
            }
            else
            {
                SelectedInstruction--;
                this.Dispatcher.BeginInvoke(delegate()
                {
                    RefreshInstructions();
                });
            }
        }

        private void InstructionsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            int i = dg.Columns.IndexOf(dg.CurrentColumn);
            if (dg.SelectedIndex != -1)
            {
                SelectedInstruction = dg.SelectedIndex;
                CurrentCode = (InstructionsDataGrid.SelectedItem as T31_Instructions).Code;
            }
            if (i != 0 && i < 6 && IsEditionEnabled && IsInstructionsEnabled) dg.BeginEdit();
            
            dg.UpdateLayout();
        }
        #endregion

        #region Приказы - редактирование

        private void Order_Click(object sender, RoutedEventArgs e)
        {
            if (OrdersDataGrid.SelectedItem == null) return;
            CheckBox arb = sender as CheckBox;

            T31_Orders i = OrdersDataGrid.SelectedItem as T31_Orders;
            try
            {
                svcContext.AttachTo("T31_Orders", i);
            }
            catch { }
            i.GetType().GetProperty(arb.Tag.ToString()).SetValue(i, arb.IsChecked, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(OrdersDataGrid.SelectedItem);
            WriteAction("Т", arb.Tag.ToString(), i.Code, (arb.IsChecked.HasValue)?((arb.IsChecked.Value)?"V":"X"):"Нет значения"); 
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, OrdersSaveChangesCompleted, new object());
        }

        private void OrdersTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (OrdersDataGrid.SelectedItem == null) return;
            TextBox tb = sender as TextBox;

            T31_Orders i = OrdersDataGrid.SelectedItem as T31_Orders;
            try
            {
                svcContext.AttachTo("T31_Orders", i);
            }
            catch { }
            i.GetType().GetProperty(tb.Name).SetValue(i, tb.Text, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(OrdersDataGrid.SelectedItem);
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void OrdersDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OrdersDataGrid.SelectedItem == null) return;
            DatePicker dp = sender as DatePicker;

            T31_Orders i = OrdersDataGrid.SelectedItem as T31_Orders;
            try
            {
                svcContext.AttachTo("T31_Orders", i);
            }
            catch { }
            i.GetType().GetProperty(dp.Name).SetValue(i, dp.SelectedDate, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(OrdersDataGrid.SelectedItem);
            WriteAction("Т", "Дата", i.Code, dp.SelectedDate.ToString());
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void OrdersSaveChangesCompleted(IAsyncResult iar)
        {
            DataServiceResponse dsr = svcContext.EndSaveChanges(iar);
            T31_Orders i = OrdersDataGrid.SelectedItem as T31_Orders;
            if (i != null)
            {
                svcContext.Detach(i);
            }
        }
        
        private void InsertOrder_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            T31_Orders t = new T31_Orders() { Position = (int)mib.Tag + 1, ChangedAt = DateTime.Now, ChangedBy = "...", IsDeleted = false };
            int index = Orders.IndexOf(OrdersDataGrid.SelectedItem as T31_Orders);
            SelectedOrder = index;
            svcContext.AddObject("T31_Orders", t);
            OrdersPager.TotalRecords++;
            WriteAction("Т", "Новый приказ", "", "");
            svcContext.BeginExecute<string>(new Uri("T31_OrdersIncrementPosition?PositionBefore=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), OrdersIncrementPositionComplete, index);
        }

        private void OrdersIncrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, OrdersSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index > OrdersPager.PageSize - 2)
            {
                SelectedOrder = 0;
                OrdersPager.CurrentPage++;
            }
            else
            {
                SelectedOrder++;
                this.Dispatcher.BeginInvoke(delegate()
                { 
                    RefreshOrders(); 
                });
            }
        }

        private void DeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;

            int index = Orders.IndexOf(OrdersDataGrid.SelectedItem as T31_Orders);
            SelectedOrder = index;
            OrdersPager.TotalRecords--;
            (OrdersDataGrid.SelectedItem as T31_Orders).IsDeleted = true;
            try
            {
                svcContext.AttachTo("T31_Orders", OrdersDataGrid.SelectedItem as T31_Orders);
            }
            catch { }
            svcContext.UpdateObject(OrdersDataGrid.SelectedItem);
            WriteAction("Т", "Удаление приказа", (OrdersDataGrid.SelectedItem as T31_Orders).Code, "");
            svcContext.BeginExecute<string>(new Uri("T31_OrdersDecrementPosition?PositionCurrent=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), OrdersDecrementPositionComplete, index);
        }

        private void OrdersDecrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, OrdersSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index == 0)
            {
                SelectedOrder = OrdersPager.PageSize - 1;
                OrdersPager.CurrentPage--;
            }
            else
            {
                SelectedOrder--;
                this.Dispatcher.BeginInvoke(delegate()
                {
                    RefreshOrders();
                });
            }
        }

        private void OrdersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            int i = dg.Columns.IndexOf(dg.CurrentColumn);
            if (dg.SelectedIndex != -1)
            {
                SelectedOrder = dg.SelectedIndex;
                CurrentCode = (OrdersDataGrid.SelectedItem as T31_Orders).Code;
            }
            if (i != 0 && i <= 3 && IsEditionEnabled && IsOrdersEnabled) dg.BeginEdit();
            dg.UpdateLayout();
        }
        #endregion

        #region Распоряжения - редактирование

        private void Direction_Click(object sender, RoutedEventArgs e)
        {
            if (DirectionsDataGrid.SelectedItem == null) return;
            CheckBox arb = sender as CheckBox;

            T31_Directions i = DirectionsDataGrid.SelectedItem as T31_Directions;
            try
            {
                svcContext.AttachTo("T31_Directions", i);
            }
            catch { }
            i.GetType().GetProperty(arb.Tag.ToString()).SetValue(i, arb.IsChecked, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(DirectionsDataGrid.SelectedItem);
            WriteAction("ТР", arb.Tag.ToString(), i.Code, (arb.IsChecked.HasValue) ? ((arb.IsChecked.Value) ? "V" : "X") : "Нет значения"); 
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, DirectionsSaveChangesCompleted, new object());
        }

        private void DirectionsTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DirectionsDataGrid.SelectedItem == null) return;
            TextBox tb = sender as TextBox;

            T31_Directions i = DirectionsDataGrid.SelectedItem as T31_Directions;
            try
            {
                svcContext.AttachTo("T31_Directions", i);
            }
            catch { }
            i.GetType().GetProperty(tb.Name).SetValue(i, tb.Text, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(DirectionsDataGrid.SelectedItem);
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void DirectionsDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DirectionsDataGrid.SelectedItem == null) return;
            DatePicker dp = sender as DatePicker;

            T31_Directions i = DirectionsDataGrid.SelectedItem as T31_Directions;
            try
            {
                svcContext.AttachTo("T31_Directions", i);
            }
            catch { }
            i.GetType().GetProperty(dp.Name).SetValue(i, dp.SelectedDate, null);
            i.ChangedAt = DateTime.Now;
            i.ChangedBy = MainRoleInfo.UserName;
            svcContext.UpdateObject(DirectionsDataGrid.SelectedItem);
            WriteAction("ТР", "Дата", i.Code, dp.SelectedDate.ToString());
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, InstructionsSaveChangesCompleted, new object());
        }

        private void DirectionsSaveChangesCompleted(IAsyncResult iar)
        {
            DataServiceResponse dsr = svcContext.EndSaveChanges(iar);
            T31_Directions i = DirectionsDataGrid.SelectedItem as T31_Directions;
            if (i != null)
            {
                svcContext.Detach(i);
            }
        }

        private void InsertDirection_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            T31_Directions t = new T31_Directions() { Position = (int)mib.Tag + 1, ChangedAt = DateTime.Now, ChangedBy = "...", IsDeleted = false };
            int index = Directions.IndexOf(DirectionsDataGrid.SelectedItem as T31_Directions);
            SelectedDirection = index;
            svcContext.AddObject("T31_Directions", t);
            DirectionsPager.TotalRecords++;
            WriteAction("ТР", "Новое распоряжение", "", "");
            svcContext.BeginExecute<string>(new Uri("T31_DirectionsIncrementPosition?PositionBefore=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), DirectionsIncrementPositionComplete, index);
        }

        private void DirectionsIncrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, DirectionsSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index > DirectionsPager.PageSize - 2)
            {
                SelectedDirection = 0;
                DirectionsPager.CurrentPage++;
            }
            else
            {
                SelectedDirection++;
                this.Dispatcher.BeginInvoke(delegate()
                {
                    RefreshDirections();
                });
            }
        }

        private void DeleteDirection_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;

            int index = Directions.IndexOf(DirectionsDataGrid.SelectedItem as T31_Directions);
            SelectedDirection = index; 
            DirectionsPager.TotalRecords--;
            (DirectionsDataGrid.SelectedItem as T31_Directions).IsDeleted = true;
            try
            {
                svcContext.AttachTo("T31_Directions", DirectionsDataGrid.SelectedItem as T31_Directions);
            }
            catch { }
            svcContext.UpdateObject(DirectionsDataGrid.SelectedItem);
            WriteAction("ТР", "Удаление распоряжения", (DirectionsDataGrid.SelectedItem as T31_Directions).Code, "");
            svcContext.BeginExecute<string>(new Uri("T31_DirectionsDecrementPosition?PositionCurrent=" + (((int)(mib.Tag)) + 1).ToString(), UriKind.Relative), DirectionsDecrementPositionComplete, index);
        }

        private void DirectionsDecrementPositionComplete(IAsyncResult iar)
        {
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, DirectionsSaveChangesCompleted, new object());
            int index = (int)iar.AsyncState;
            if (index == 0)
            {
                SelectedDirection = DirectionsPager.PageSize - 1;
                DirectionsPager.CurrentPage--;
            }
            else
            {
                SelectedDirection--;
                this.Dispatcher.BeginInvoke(delegate()
                {
                    RefreshDirections();
                });
            }
        }

        private void DirectionsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            int i = dg.Columns.IndexOf(dg.CurrentColumn);
            if (dg.SelectedIndex != -1)
            {
                SelectedDirection = dg.SelectedIndex;
                CurrentCode = (DirectionsDataGrid.SelectedItem as T31_Directions).Code;
            }
            if (i != 0 && i <= 3 && IsEditionEnabled && IsDirectionsEnabled) dg.BeginEdit();
            dg.UpdateLayout();
        }
        
        #endregion

        #region Загрузка справочника подразделений, настроек и авторизация

        private void DepartmentFilter_Loaded(object sender, RoutedEventArgs e)
        {
            if (DepartmentsLoaded) return;
            ComboBox cb = sender as ComboBox;
            DepartmentFilter = cb;
            aqs.BeginGetRoles(GetRolesComplete, new object());
        }

        private void GetRolesComplete(IAsyncResult iar)
        {
            MainRoleInfo = aqs.EndGetRoles(iar);
            this.Dispatcher.BeginInvoke(delegate()
            {
                T31_SettingsQuery = svcContext.T31_Settings;
                T31_SettingsQuery.BeginExecute(T31_SettingsQueryCompleted, new object());
            });
        }

        private void T31_SettingsQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Settings.Clear();
                IEnumerable<T31_Settings> S = T31_SettingsQuery.EndExecute(iar);
                foreach (T31_Settings item in S)
                {
                    Settings.Add(item);
                }
                SettingsDataGrid.ItemsSource = null;
                SettingsDataGrid.UpdateLayout();
                SettingsDataGrid.ItemsSource = Settings;
                SettingsDataGrid.UpdateLayout(); 
                
                UpdateInterfaceBySettings();
                UpdateInterfaceByRoles();
                T31_DepartmentsQuery = svcContext.T31_Departments.AddQueryOption("$orderby", "DepartmentName");
                T31_DepartmentsQuery.BeginExecute(T31_DepartmentsQueryCompleted, new object());
            });
        }

        private void Setting_Click(object sender, RoutedEventArgs e)
        {
            if (SettingsDataGrid.SelectedItem == null) return;
            CheckBox arb = sender as CheckBox;

            T31_Settings i = SettingsDataGrid.SelectedItem as T31_Settings;
            try
            {
                svcContext.AttachTo("T31_Settings", i);
            }
            catch { }
            i.IsAllowed = arb.IsChecked;
            svcContext.UpdateObject(SettingsDataGrid.SelectedItem);
            svcContext.BeginSaveChanges(SaveChangesOptions.Batch, SettingsSaveChangesCompleted, new object());
        }

        private void SettingsSaveChangesCompleted(IAsyncResult iar)
        {
            DataServiceResponse dsr = svcContext.EndSaveChanges(iar);
            T31_Settings i = SettingsDataGrid.SelectedItem as T31_Settings;
            if (i != null)
            {
                svcContext.Detach(i);
            }
            this.Dispatcher.BeginInvoke(delegate()
            {
                UpdateInterfaceBySettings();
                UpdateInterfaceByRoles();
            });
        }
        
        private void T31_DepartmentsQueryCompleted(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                (this.Resources["T31Departments"] as Departments).DepartmentsCollection = new ObservableCollection<T31_Departments>();
                (this.Resources["T31Departments"] as Departments).DepartmentsCollectionWithNull = new ObservableCollection<T31_Departments>();
                (this.Resources["T31Departments"] as Departments).DepartmentsCollectionWithNull.Add(new T31_Departments() { id = 255, DepartmentName = "Все" });

                foreach (T31_Departments item in T31_DepartmentsQuery.EndExecute(iar))
                {
                    (this.Resources["T31Departments"] as Departments).DepartmentsCollection.Add(item);
                    (this.Resources["T31Departments"] as Departments).DepartmentsCollectionWithNull.Add(item);

                }
                (this.Resources["DepartmentValueConverter"] as DepartmentValueConverter).Departments = (this.Resources["T31Departments"] as Departments).DepartmentsCollection;

                DepartmentFilter.ItemsSource = (this.Resources["T31Departments"] as Departments).DepartmentsCollectionWithNull;
                DepartmentFilter.UpdateLayout();
                DepartmentFilter.SelectedIndex = 0;
                DepartmentFilter.UpdateLayout();
                DepartmentsLoaded = true;
            });
        }

        private void DepartmentFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            if (cb.SelectedItem != null)
            {
                DepartmentID = (cb.SelectedItem as T31_Departments).id;
                FullRefreshInstructions();
            }
        }
        #endregion
             
        #region Отчеты

        private string[] FieldNames = new string[] { "ADO", "SO", "ORS", "SEMO", "SorPO", "LPO", "OMIT", "ONMKID", "CEST", "UOR" };
        private string[] FieldCaptions = new string[] { "АДО", "СО", "ОРС", "СЭМО", "СорПО", "ЛПО", "ОМиТ", "ОНМКиД", "ЦЭСТ", "УОР" };
        private string[] ReportCaptions = new string[] { "Утвержденные распоряжения", "Отмененные распоряжения", "Утвержденные приказы", "Отмененные приказы" };
        private string[] ReportCodes = new string[] { "pr", "dr", "pt", "dt" };

        private void ConstructReportPage()
        {
            for (int i = 0; i < 1+ReportCaptions.Length; i++) ReportsByDepartmentPlaceholder.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < 1 + FieldCaptions.Length; i++) ReportsByDepartmentPlaceholder.ColumnDefinitions.Add(new ColumnDefinition());
            ReportsByDepartmentPlaceholder.ColumnDefinitions[0].Width = new GridLength(200);

            for (int i = 0; i < ReportCaptions.Length; i++)
            {
                TextBlock tb = new TextBlock() { Text = ReportCaptions[i], HorizontalAlignment = System.Windows.HorizontalAlignment.Left, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(3)};
                b.Child = tb;
                Grid.SetColumn(b, 0);
                Grid.SetRow(b, i + 1);
                ReportsByDepartmentPlaceholder.Children.Add(b);
            }
            for (int i = 0; i < FieldCaptions.Length; i++)
            {
                TextBlock tb = new TextBlock() { Text = FieldCaptions[i], HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(1) };
                b.Child = tb;
                Grid.SetColumn(b, i + 1);
                Grid.SetRow(b, 0);
                ReportsByDepartmentPlaceholder.Children.Add(b);
            }
            for (int i = 0; i < ReportCaptions.Length; i++)
            {
                string Code = ReportCodes[i]; 
                
                for (int j = 0; j < FieldCaptions.Length; j++)
                {
                    StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                    TextImageButtonBase mb1 = new TextImageButtonBase() { ImageUrl = "/AQSelection/Images/word16.png", Tag=Code+"w"+j.ToString() };
                    mb1.Style = this.Resources["MiniImageButton"] as Style;
                    mb1.Click += ReportMB_Click;
                    mb1.Margin = new Thickness(5);
                    TextImageButtonBase mb2 = new TextImageButtonBase() { ImageUrl = "/AQSelection/Images/excel16.png", Tag = Code + "x" + j.ToString() };
                    mb2.Style = this.Resources["MiniImageButton"] as Style;
                    mb2.Click += ReportMB_Click;
                    mb2.Margin = new Thickness(5); 
                    sp.Children.Add(mb1);
                    sp.Children.Add(mb2);
                    Border b = new Border(){ BorderThickness= new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(1)};
                    b.Child = sp;
                    Grid.SetColumn(b, j + 1);
                    Grid.SetRow(b, i + 1);
                    ReportsByDepartmentPlaceholder.Children.Add(b);
                }
            }


            AddNewCommonReport("a", "Полный список распоряжений");
            AddNewCommonReport("b", "Полный список приказов");
            AddNewCommonReport("i", "Полный список инструкций");
            AddNewCommonReport("j", "Действующие инструкции");
            AddNewCommonReport("k", "Удаленные инструкции");
            AddNewCommonReport("c", "Конфликты распоряжений");
            AddNewCommonReport("d", "Конфликты приказов");
            AddNewCommonReport("e", "Необработанные распоряжения");
            AddNewCommonReport("f", "Необработанные приказы");
            AddNewCommonReport("g", "Обработанные распоряжения без отметок (возможные ошибки)");
            AddNewCommonReport("h", "Обработанные приказы без отметок (возможные ошибки)");
        }

        private void ReportMB_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            bool IsApproved = mib.Tag.ToString().Substring(0, 1) == "p";
            bool IsDirection = mib.Tag.ToString().Substring(1, 1) == "r";
            bool IsWord = mib.Tag.ToString().Substring(2, 1) == "w";
            int Number = Int16.Parse(mib.Tag.ToString().Substring(3, 1));
            string Caption = FieldCaptions[Number];
            string Name = FieldNames[Number];

            string ReportName = (IsApproved) ? "Утвержденные " : "Отмененные ";
            ReportName += (IsDirection) ? "распоряжения " : "приказы ";
            ReportName += "отдела " + Caption;

            string SQL = "select position as 'п/п', Code as 'Обозначение', Name as 'Наименование', EmitDate as 'Дата выпуска' from ";
            SQL += (IsDirection) ? "t31_directions " : "t31_orders ";
            SQL += " where isdeleted<>1 and ";
            SQL += Name + "=" + ((IsApproved) ? "1 " : "0 ");
            SQL += "order by position";
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru" + ((IsWord) ? "/t31/ReportDocx.aspx" : "/t31/ReportXlsx.aspx") + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }

        private void AddNewCommonReport(string Code, string Caption)
        {
            CommonReportsPlaceholder.RowDefinitions.Add(new RowDefinition());

            StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
            TextImageButtonBase mb1 = new TextImageButtonBase() { ImageUrl = "/AQSelection/Images/word16.png", Tag = Code + "w" };
            mb1.Style = this.Resources["MiniImageButton"] as Style;
            mb1.Click += CommonReportMB_Click;
            mb1.Margin = new Thickness(5,2,5,2);
            TextImageButtonBase mb2 = new TextImageButtonBase() { ImageUrl = "/AQSelection/Images/excel16.png", Tag = Code + "x" };
            mb2.Style = this.Resources["MiniImageButton"] as Style; 
            mb2.Click += CommonReportMB_Click;
            mb2.Margin = new Thickness(5,2,5,2);
            sp.Children.Add(mb1);
            sp.Children.Add(mb2);
            Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(2,0,0,0) };
            b.Child = sp;
            Grid.SetColumn(b, 1);
            Grid.SetRow(b, CommonReportsPlaceholder.RowDefinitions.Count - 1);
            CommonReportsPlaceholder.Children.Add(b);

            TextBlock tb = new TextBlock() { Text = Caption, HorizontalAlignment = System.Windows.HorizontalAlignment.Left, VerticalAlignment = System.Windows.VerticalAlignment.Center };
            Border b2 = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(2,0,0,0) };
            b2.Child = tb;
            Grid.SetColumn(b2, 0);
            Grid.SetRow(b2, CommonReportsPlaceholder.RowDefinitions.Count - 1);
            CommonReportsPlaceholder.Children.Add(b2);
        }

        private void CommonReportMB_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            bool IsWord = mib.Tag.ToString().Substring(1, 1) == "w";
            string Req = mib.Tag.ToString().Substring(0, 1);
            string ReportName = "Ошибка", SQL = "select 'Ошибка в запросе' as 'Ошибка'";

            switch (Req)
            {
                case "a":
                    ReportName = "Полный список распоряжений";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ОНМКиД',[CEST] as 'ЦЭСТ',[UOR] as 'УОР',[EmitDate] as 'Дата выпуска' from t31_directions where isdeleted<>1 order by position";
                    break;
                case "b":
                    ReportName = "Полный список приказов";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ОНМКиД',[CEST] as 'ЦЭСТ',[UOR] as 'УОР',[EmitDate] as 'Дата выпуска' from t31_orders where isdeleted<>1 order by position";
                    break;
                case "i":
                    ReportName = "Полный список инструкций";
                    SQL = "SELECT [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,[IsDeleted] as 'Удалено' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId order by position";
                    break;
                case "j":
                    ReportName = "Действующие инструкции";
                    SQL = "SELECT [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,[IsDeleted] as 'Удалено' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId where isdeleted=0 order by position";
                    break;
                case "k":
                    ReportName = "Удаленные инструкции";
                    SQL = "SELECT [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,[IsDeleted] as 'Удалено' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId where isdeleted=1 order by position";
                    break;
                case "c":
                    ReportName = "Конфликты распоряжений";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ОНМКиД',[CEST] as 'ЦЭСТ',[UOR] as 'УОР',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_directions where (ado=1 or so=1 or ors=1 or semo=1 or sorpo=1 or lpo=1 or omit=1 or onmkid=1 or cest=1 or uor=1) and (ado=0 or so=0 or ors=0 or semo=0 or sorpo=0 or lpo=0 or omit=0 or onmkid=0 or cest=0 or uor=0) and isdeleted<>1 order by position";
                    break;
                case "d":
                    ReportName = "Конфликты приказов";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ОНМКиД',[CEST] as 'ЦЭСТ',[UOR] as 'УОР',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_orders where (ado=1 or so=1 or ors=1 or semo=1 or sorpo=1 or lpo=1 or omit=1 or onmkid=1 or cest=1 or uor=1) and (ado=0 or so=0 or ors=0 or semo=0 or sorpo=0 or lpo=0 or omit=0 or onmkid=0 or cest=0 or uor=0) and isdeleted<>1 order by position";
                    break;
                case "e":
                    ReportName = "Необработанные распоряжения";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_directions where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and isdeleted<>1 order by position";
                    break;
                case "f":
                    ReportName = "Необработанные приказы";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_orders where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and isdeleted<>1 order by position";
                    break;
                case "g":
                    ReportName = "Обработанные распоряжения без отметок (возможные ошибки)";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_directions where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and changedby is not null and isdeleted<>1 order by position";
                    break;
                case "h":
                    ReportName = "Обработанные приказы без отметок (возможные ошибки)";
                    SQL = "select [Position] as '№ п/п',[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_orders where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and changedby is not null and isdeleted<>1 order by position";
                    break;
                default:
                    break;

            }
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru" + ((IsWord) ? "/t31/ReportDocx.aspx" : "/t31/ReportXlsx.aspx") + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }

        private void InstructionsReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru/t31/GetInstructions.aspx?FirstPage=" + InstructionsFromPage.Text);
            HtmlPage.Window.Navigate(Url); 
        }

        private void OrdersReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru/t31/GetOrders.aspx?FirstPage=" + OrdersFromPage.Text);
            HtmlPage.Window.Navigate(Url); 
        }

        private void DirectionsReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru/t31/GetDirections.aspx?FirstPage=" + DirectionsFromPage.Text);
            HtmlPage.Window.Navigate(Url); 
        }

        private void InstructionsFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            InstructionsReport.IsEnabled=( int.TryParse(tb.Text, out Page) || tb.Text.Trim()==string.Empty);
        }

        private void OrdersFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            OrdersReport.IsEnabled = (int.TryParse(tb.Text, out Page) || tb.Text.Trim() == string.Empty);
        }

        private void DirectionsFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            DirectionsReport.IsEnabled = (int.TryParse(tb.Text, out Page) || tb.Text.Trim() == string.Empty);
        }
        #endregion

        #region Управление правами

        private void UpdateInterfaceByRoles()
        {
            bool IsAdmin = IsInRole("t31_admins");
            bool IsUser = IsInRole("t31_editors") && (IsRightSet("EditionEnabled")).Value;
            (MainTabControl.Items[5] as TabItem).IsEnabled = IsAdmin;
            RightsForAcceptRejectBoxes rfarb1 = this.Resources["RightsForAcceptRejectBoxes1"] as RightsForAcceptRejectBoxes;
            foreach (PropertyInfo p in rfarb1.GetType().GetProperties())
            {
                if (IsAdmin && IsOrdersEnabled && IsEditionEnabled) p.SetValue(rfarb1, true, null);
                else
                {
                    if (!IsUser) p.SetValue(rfarb1, false, null);
                    else p.SetValue(rfarb1, IsInRole("t31_" + p.Name) && IsUser && IsOrdersEnabled && IsEditionEnabled, null);
                }
            }
            RightsForAcceptRejectBoxes rfarb2 = this.Resources["RightsForAcceptRejectBoxes2"] as RightsForAcceptRejectBoxes;
            foreach (PropertyInfo p in rfarb2.GetType().GetProperties())
            {
                if (IsAdmin && IsDirectionsEnabled && IsEditionEnabled) p.SetValue(rfarb2, true, null);
                else
                {
                    if (!IsUser) p.SetValue(rfarb2, false, null);
                    else p.SetValue(rfarb2, IsInRole("t31_" + p.Name) && IsUser && IsEditionEnabled && IsDirectionsEnabled, null);
                }
            }
        }

        private void UpdateInterfaceBySettings()
        {
            IsEditionEnabled = (IsRightSet("EditionEnabled")).Value;
            IsInstructionsEnabled = (IsRightSet("InstructionsEnabled")).Value;
            IsOrdersEnabled = (IsRightSet("OrdersEnabled")).Value;
            IsDirectionsEnabled = (IsRightSet("DirectionsEnabled")).Value;
            bool IsUser = (IsRightSet("EditionEnabled")).Value && (IsInRole("t31_admins") || IsInRole("t31_editors"));  
            RightsForAddDelete rfad = this.Resources["RightsForAddDelete"] as RightsForAddDelete;

            rfad.InstructionDeleteEnabled = IsEditionEnabled && IsInstructionsEnabled && IsRightSet("InstructionDeleteEnabled").Value;
            rfad.InstructionNewEnabled = IsEditionEnabled && IsInstructionsEnabled && IsRightSet("InstructionNewEnabled").Value;
            foreach (DataGridColumn dgc in InstructionsDataGrid.Columns) dgc.IsReadOnly = !(IsEditionEnabled && IsInstructionsEnabled && IsUser);

            rfad.OrdersDeleteEnabled = IsEditionEnabled && IsOrdersEnabled && IsRightSet("OrdersDeleteEnabled").Value;
            rfad.OrdersNewEnabled = IsEditionEnabled && IsOrdersEnabled && IsRightSet("OrdersNewEnabled").Value;
            foreach (DataGridColumn dgc in OrdersDataGrid.Columns) dgc.IsReadOnly = !(IsEditionEnabled && IsOrdersEnabled && IsUser);

            rfad.DirectionsDeleteEnabled = IsEditionEnabled && IsDirectionsEnabled && IsRightSet("DirectionsDeleteEnabled").Value;
            rfad.DirectionsNewEnabled = IsEditionEnabled && IsDirectionsEnabled && IsRightSet("DirectionsNewEnabled").Value;
            foreach (DataGridColumn dgc in DirectionsDataGrid.Columns) dgc.IsReadOnly = !(IsEditionEnabled && IsDirectionsEnabled && IsUser);

            InstructionsDataGrid.UpdateLayout();
            DirectionsDataGrid.UpdateLayout();
            OrdersDataGrid.UpdateLayout();
            RightsTextBox.Text = ((IsInRole("t31_admins")) ? "Администратор: " : "Пользователь: ") + MainRoleInfo.UserName + "; " + "редактирование " + ((IsUser) ? "разрешено" : "запрещено");
        }
        
        private bool IsInRole(string Role)
        {
            if (MainRoleInfo.Roles != null)
            {
                return (from m in MainRoleInfo.Roles select m.ToLower()).Contains(Role.ToLower());
            }
            return false;
        }

        private bool? IsRightSet(string Right)
        {
            return (from s in Settings where s.Code.ToLower() == Right.ToLower() select s.IsAllowed).Single();
        }
        #endregion

        #region Запись действий пользователя
        private void InstructionsTextBox_LostFocus(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            WriteAction("ТИ", tb.Name, CurrentCode, tb.Text);
        }
        private void OrdersTextBox_LostFocus(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            WriteAction("Т", tb.Name, CurrentCode, tb.Text);
        }
        private void DirectionsTextBox_LostFocus(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;

            WriteAction("ТР", tb.Name, CurrentCode, tb.Text);
        }

        private void WriteAction(string part, string field, string code, string newValue)
        {
            string userName = MainRoleInfo.UserName;
            T31_UserActions t = new T31_UserActions() { Code=code, Field = field, Part=part, NewValue=newValue, UserName=userName, Date=DateTime.Now};
            svcContext.AddObject("T31_UserActions", t);
        }
        #endregion

        private void RefreshUserActionsButton_Click(object sender, RoutedEventArgs e)
        {
            FullRefreshUserActions();
        }

        private void ReportToXlsxUserActionsButton_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            string ReportName = "Действия пользователей";
            string SQL = "select Date as 'Дата', UserName as 'Пользователь', Part as 'Приложение', Field as 'Поле', NewValue as 'Новое значение', Code as 'Обозначение документа' from T31_UserActions order by date desc";
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru/t31/ReportXlsx.aspx" + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }
    
        public AQModuleDescription GetAQModuleDescription()
        {
            return new AQModuleDescription() {Instance = this as IAQModule, ModuleType = this.GetType(), Name = this.GetType().Name, UniqueID = this.GetHashCode()};
        }

        public void ProcessSignal(AQModuleDescription SenderDescription, Command CommandType, object CommandArgument, CommandCallBackDelegate CommandCallBack)
        {
            if (CommandCallBack != null) CommandCallBack.Invoke(CommandType, CommandArgument, null);
        }
    }

    #region Вспомогательные классы

    public class Departments
    {
        public ObservableCollection<T31_Departments> DepartmentsCollection {get;set;}
        public ObservableCollection<T31_Departments> DepartmentsCollectionWithNull { get; set; }
    }

    public class RightsForAcceptRejectBoxes : INotifyPropertyChanged
    {
        private bool ado;
        public bool ADO { get { return ado; } set { ado = value; NotifyPropertyChanged("ADO"); } }
        private bool so;
        public bool SO { get { return so; } set { so = value; NotifyPropertyChanged("SO"); } }
        private bool ors;
        public bool ORS { get { return ors; } set { ors = value; NotifyPropertyChanged("ORS"); } }
        private bool semo;
        public bool SEMO { get { return semo; } set { semo = value; NotifyPropertyChanged("SEMO"); } }
        private bool sorpo;
        public bool SorPO { get { return sorpo; } set { sorpo = value; NotifyPropertyChanged("SorPO"); } }
        private bool lpo;
        public bool LPO { get { return lpo; } set { lpo = value; NotifyPropertyChanged("LPO"); } }
        private bool omit;
        public bool OMIT { get { return omit; } set { omit = value; NotifyPropertyChanged("OMIT"); } }
        private bool onmkid;
        public bool ONMKID { get { return onmkid; } set { onmkid = value; NotifyPropertyChanged("ONMKID"); } }
        private bool cest;
        public bool CEST { get { return cest; } set { cest = value; NotifyPropertyChanged("CEST"); } }
        private bool uor;
        public bool UOR { get { return uor; } set { uor = value; NotifyPropertyChanged("UOR"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class RightsForAddDelete : INotifyPropertyChanged
    {
        private bool ide;
        public bool InstructionDeleteEnabled { get { return ide; } set { ide = value; NotifyPropertyChanged("InstructionDeleteEnabled"); } }
        private bool ine;
        public bool InstructionNewEnabled { get { return ine; } set { ine = value; NotifyPropertyChanged("InstructionNewEnabled"); } }
        private bool ode;
        public bool OrdersDeleteEnabled { get { return ode; } set { ode = value; NotifyPropertyChanged("OrdersDeleteEnabled"); } }
        private bool one;
        public bool OrdersNewEnabled { get { return one; } set { one = value; NotifyPropertyChanged("OrdersNewEnabled"); } }
        private bool dde;
        public bool DirectionsDeleteEnabled { get { return dde; } set { dde = value; NotifyPropertyChanged("DirectionsDeleteEnabled"); } }
        private bool dne;
        public bool DirectionsNewEnabled { get { return dne; } set { dne = value; NotifyPropertyChanged("DirectionsNewEnabled"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class DepartmentValueConverter : System.Windows.Data.IValueConverter
    {
        public ObservableCollection<T31_Departments> Departments;
        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            T31_Departments v = value as T31_Departments;
            if (value == null) return null;
            return Departments.Where(s => s.id == v.id ).First(); 
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    public class DateTimeToStringValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime? v = value as DateTime?;
            if (!v.HasValue) return null;
            return v.Value.ToString("dd.MM.yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime v;
            if (DateTime.TryParse(value.ToString(), out v)) return v;
            else return null;
        }
    }

    public class BooleanToVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool? v = value as bool?;
            if (v.HasValue && v.Value)return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    #endregion
}