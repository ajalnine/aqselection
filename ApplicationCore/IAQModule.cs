﻿using System;

namespace ApplicationCore
{
    public interface IAQModule
    {
        event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        AQModuleDescription GetAQModuleDescription();
        void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack);
        void InitSignaling(CommandCallBackDelegate CommandCallBack);
    }

    public delegate void ReadyForAcceptSignalsDelegate(object o, ReadyForAcceptSignalsEventArg e);

    public class ReadyForAcceptSignalsEventArg : EventArgs
    {
        public CommandCallBackDelegate CommandCallBack;
    }
}
