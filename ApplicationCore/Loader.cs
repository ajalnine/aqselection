﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Resources;
using System.Windows.Browser;
using System.Windows.Media.Imaging;
using System.IO;
using System.Reflection;

namespace ApplicationCore
{
    public static class Loader
    {
        private static Dictionary<string, StreamResourceInfo> XAPCache;
        private static Dictionary<string, Assembly> AssemblyCache;
        public static Size MainTabSize;
        public delegate void SizeChangedDelegate(object o, TabSizeChangedEventArgs e);
        public static event SizeChangedDelegate TabControlSizeChanged;

        public static Canvas InvisibleCanvas;

        static Loader()
        {
            XAPCache = new Dictionary<string, StreamResourceInfo>();
            AssemblyCache = new Dictionary<string, Assembly>();
            MainTabSize = new Size(0, 0);
        }

        public static void TabSizeChanged(object sender, Size NewSize)
        {
            MainTabSize = NewSize;
            if (TabControlSizeChanged!=null)TabControlSizeChanged.Invoke(sender, new TabSizeChangedEventArgs(){NewSize = NewSize}); 
        }
        
        public static void BeginLoadModule(string ModuleName, ModuleLoadedCallbackDelegate ModuleLoadedCallBack)
        {
            if (XAPCache.Keys.Contains(ModuleName)) ModuleLoadedCallBack.Invoke(XAPCache[ModuleName]);
            else
            {
                WebClient client = new WebClient();
                client.Headers["Pragma"] = "no-cache";
                client.Headers["Expires"] = "0";
                client.Headers["Cache-Control"] = "no-cache";
                client.OpenReadCompleted += new OpenReadCompletedEventHandler(Module_Loaded);
                client.OpenReadAsync(new Uri(ModuleName + ".xap"+"?Code="+Guid.NewGuid().ToString(), UriKind.Relative), new ModuleID() { Name = ModuleName, CallBack = ModuleLoadedCallBack });
            }
        }

        private static void Module_Loaded(object sender, OpenReadCompletedEventArgs e)
        {
            ModuleID ProcessedID = (ModuleID)e.UserState;
            if (e.Error != null) ProcessedID.CallBack.Invoke(null);
            else
            {
                StreamResourceInfo sri = new StreamResourceInfo(e.Result, null);
                if (!XAPCache.Keys.Contains(ProcessedID.Name))XAPCache.Add(ProcessedID.Name.ToString(), sri);
                if (ProcessedID.CallBack!=null) ProcessedID.CallBack.Invoke(sri);
            }
        }

        public static void BeginLoadAssembly(string ModuleName, string AssemblyName, AssemblyLoadedCallbackDelegate AssemblyLoadedCallBack, object userState)
        {
            if (AssemblyCache.Keys.Contains(AssemblyName)) AssemblyLoadedCallBack.Invoke(AssemblyCache[ModuleName], userState);
            else
            {
                if (XAPCache.Keys.Contains(ModuleName))
                {
                    StreamResourceInfo sr4dll = Application.GetResourceStream(XAPCache[AssemblyName], new Uri(AssemblyName + ".dll", UriKind.Relative));
                    AssemblyPart Dll = new AssemblyPart();
                    Assembly LoadedAssembly = Dll.Load(sr4dll.Stream);
                    if (!AssemblyCache.Keys.Contains(AssemblyName)) AssemblyCache.Add(AssemblyName, LoadedAssembly);
                    if (AssemblyLoadedCallBack!=null) AssemblyLoadedCallBack(LoadedAssembly, userState);
                }
                else
                {
                    WebClient client = new WebClient();
                    AssemblyID AID = new AssemblyID() { Name = AssemblyName, ModName = ModuleName, CallBack = AssemblyLoadedCallBack, LoadCommandUserState = userState };
                    client.OpenReadCompleted += new OpenReadCompletedEventHandler(ModuleForAssembly_Loaded);
                    client.OpenReadAsync(new Uri(ModuleName + ".xap", UriKind.Relative), AID);
                }
            }
        }

        private static void ModuleForAssembly_Loaded(object sender, OpenReadCompletedEventArgs e)
        {
            AssemblyID ProcessedID = (AssemblyID)e.UserState;
            if (e.Error != null) ProcessedID.CallBack.Invoke(null, e.UserState);
            else
            {
                StreamResourceInfo sri = new StreamResourceInfo(e.Result, null);
                if (!XAPCache.Keys.Contains(ProcessedID.Name)) XAPCache.Add(ProcessedID.Name.ToString(), sri);
                BeginLoadAssembly(ProcessedID.ModName, ProcessedID.Name, ProcessedID.CallBack, ProcessedID.LoadCommandUserState);
            }
        }
    }

    public delegate void ModuleLoadedCallbackDelegate(StreamResourceInfo sri);
    public delegate void AssemblyLoadedCallbackDelegate(Assembly a, object UserState);

    public class ModuleID
    {
        public string Name{get; set;}
        public ModuleLoadedCallbackDelegate CallBack { get; set;}
    }

    public class AssemblyID
    {
        public string Name { get; set; }
        public string ModName { get; set; }
        public object LoadCommandUserState { get; set; }
        public AssemblyLoadedCallbackDelegate CallBack { get; set; }
    }
    
    public class TabSizeChangedEventArgs
    {
        public Size NewSize{get;set;}
    }
}
