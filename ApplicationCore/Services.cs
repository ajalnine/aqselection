﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ConstructorServiceReference;

namespace ApplicationCore
{
    public static class Services
    {
        public static ReportingDuplexServiceClient GetReportingService()
        {
            EndpointAddress address = new EndpointAddress(new Uri(HtmlPage.Document.DocumentUri, "Services/ReportingService.svc"));
            PollingDuplexHttpBinding binding = GetPollingDuplexHttpBinding();
            ReportingDuplexServiceClient proxy = new ReportingDuplexServiceClient(binding, address);
            return proxy;
        }

        public static CalculationService GetCalculationService()
        {
            EndpointAddress address = new EndpointAddress(new Uri(HtmlPage.Document.DocumentUri, "Services/CalculationService.svc"));
            BasicHttpBinding binding = GetBasicHttpBinding();
            CalculationService proxy = new ChannelFactory<CalculationService>(binding, address).CreateChannel();
            ((IContextChannel)proxy).OperationTimeout = new TimeSpan(2, 0, 0); 
            return proxy;
        }

        public static ConstructorService GetConstructorService()
        {
            EndpointAddress address = new EndpointAddress(new Uri(HtmlPage.Document.DocumentUri, "Services/ConstructorService.svc"));
            BasicHttpBinding binding = GetBasicHttpBinding();
            ConstructorService proxy = new ChannelFactory<ConstructorService>(binding, address).CreateChannel();
            ((IContextChannel)proxy).OperationTimeout = new TimeSpan(2, 0, 0);
            return proxy;
        }

        public static AQService GetAQService()
        {
            EndpointAddress address = new EndpointAddress(new Uri(HtmlPage.Document.DocumentUri, "Services/AQService.svc"));
            BasicHttpBinding binding = GetBasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            AQService proxy = new ChannelFactory<AQService>(binding, address).CreateChannel();
            ((IContextChannel)proxy).OperationTimeout = new TimeSpan(2, 0, 0);
            return proxy;
        }

        private static BasicHttpBinding GetBasicHttpBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxBufferSize = 2147483647;
            binding.MaxReceivedMessageSize = 2147483647;
            TimeSpan ts = new TimeSpan(1, 0, 0);
            binding.OpenTimeout = ts;
            binding.ReceiveTimeout = ts;
            binding.SendTimeout = ts;
            binding.CloseTimeout = ts;
            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.TransferMode = TransferMode.Buffered;
            return binding;
        }
        
        private static CustomBinding GetCustomBinding()
        {
            HttpTransportBindingElement httpTransport = new HttpTransportBindingElement();
            httpTransport.MaxReceivedMessageSize = 2147483647;
            httpTransport.MaxBufferSize = 2147483647;
            httpTransport.TransferMode = TransferMode.StreamedResponse;
            BinaryMessageEncodingBindingElement binary = new BinaryMessageEncodingBindingElement();
            
            CustomBinding binding = new CustomBinding(binary, httpTransport);
            TimeSpan ts = new TimeSpan(1, 0, 0);
            binding.OpenTimeout = ts;
            binding.ReceiveTimeout = ts;
            binding.SendTimeout = ts;
            binding.CloseTimeout = ts;
            
            return binding;
        }

        private static PollingDuplexHttpBinding GetPollingDuplexHttpBinding()
        {
            PollingDuplexHttpBinding binding = new PollingDuplexHttpBinding(PollingDuplexMode.MultipleMessagesPerPoll);
            binding.MaxBufferSize = 2147483647;
            binding.MaxReceivedMessageSize = 2147483647;
            binding.UseTextEncoding = false;

            TimeSpan ts = new TimeSpan(1, 0, 0);
            binding.OpenTimeout = ts;
            binding.ReceiveTimeout = ts;
            binding.SendTimeout = ts;
            binding.CloseTimeout = ts;
            return binding;
        }
    }
}
