﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore
{
    public static class Extentions
    {
        public static string CapitalizeFirst(this string input)
        {
            return input.First().ToString().ToUpper() + string.Join("", input.Skip(1));
        }
    }
}
