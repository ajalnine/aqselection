﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using OpenRiaServices.DomainServices;
using OpenRiaServices.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore;
using AQSelection;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using ApplicationCore.AQServiceReference;
using OpenRiaServices.Controls;

namespace SLDocuments
{
    public partial class MainPage : UserControl, IAQModule
    {
        public T31_Instructions CurrentInstruction;
        public T31_Orders CurrentOrder;
        public T31_Directions CurrentDirection;
        
        private bool IsEditionEnabled;
        
        private byte? CurrentDepartment = null;
        private bool ReloadOrders = true;
        private bool ReloadDirections = true;
        private bool ReportPageConstructed = false;
        private AQService aqs;
        private readonly AQModuleDescription _aqmd;

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tc = sender as TabControl;
            switch (tc.SelectedIndex)
            {
                case 4:
                    if (!ReportPageConstructed) ConstructReportPage();
                    ReportPageConstructed = true;
                    break;
                default:
                    break;
            }
        }

        #region Обработка сигналов
        CommandCallBackDelegate ReadySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }
        #endregion

        #region Инструкции

        #region События загрузки данных

        private void t31_InstructionsDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {

            if (e.HasError)
            {
                System.Windows.MessageBox.Show(e.Error.ToString(), "Ошибка загрузки", System.Windows.MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            if (CurrentInstruction != null && t31_InstructionsDomainDataSource.DataView.IndexOf(CurrentInstruction) != -1)
            {
                t31_InstructionsDomainDataSource.DataView.MoveCurrentTo(CurrentInstruction);
                t31_InstructionsDataGrid.ScrollIntoView(CurrentInstruction, null);
            }
            else
            {
                t31_InstructionsDomainDataSource.DataView.MoveCurrentToFirst();
                try
                {
                    t31_InstructionsDataGrid.ScrollIntoView(t31_InstructionsDomainDataSource.DataView[0], null);
                }
                catch { }
            }
            NewInstructionButton.IsEnabled = true;
        }
        private void t31_DepartmentsDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            ((DepartmentValueConverter)this.Resources["DepartmentValueConverter"]).DepartmentSource = ((DomainDataSource)sender);
            if (e.HasError)
            {
                System.Windows.MessageBox.Show(e.Error.ToString(), "Load Error", System.Windows.MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            var Departments = ((DomainDataSource)sender).DataView.Cast<T31_Departments>().AsQueryable();
            DepartsmentFilterSelector.Children.Clear();
            foreach (var d in Departments)
            {
                RadioButton rd = new RadioButton() { GroupName = "DS", IsChecked = false, Width = 70, Style = this.Resources["RadioButtonBorderStyle"] as Style, Content = new TextBlock() { Text = d.DepartmentName, VerticalAlignment = System.Windows.VerticalAlignment.Center }, VerticalContentAlignment = System.Windows.VerticalAlignment.Center };
                rd.Checked += new RoutedEventHandler(rd_Checked);
                rd.Tag = d.id;
                rd.Name = Guid.NewGuid().ToString();
                DepartsmentFilterSelector.Children.Add(rd);
            }
        }

        #endregion
        
        void rd_Checked(object sender, RoutedEventArgs e)
        {
            if (t31_InstructionsDomainDataSource == null) return;
            int di = Int16.Parse(((RadioButton)sender).Tag.ToString());
            t31_InstructionsDomainDataSource.FilterDescriptors.Clear();
            
            FilterDescriptor fd1 = new FilterDescriptor() { PropertyPath = "IsDeleted", Operator = FilterOperator.IsNotEqualTo, Value = true };
            t31_InstructionsDomainDataSource.FilterDescriptors.Add(fd1);

            CurrentDepartment = (di != -1) ? (byte?)di : null;
            developerIdTextBox.IsEnabled = (di == -1);
            NewInstructionButton.IsEnabled = false;
            if (di != -1)
            {
                FilterDescriptor fd2 = new FilterDescriptor() { PropertyPath = "DeveloperId", Operator = FilterOperator.IsEqualTo, Value = di };
                t31_InstructionsDomainDataSource.FilterDescriptors.Add(fd2);
            }
            if (t31_InstructionsDomainDataSource.CanLoad) t31_InstructionsDomainDataSource.Load();
            
        }
        
        private void CancelInstructionEdit_Button(object sender, RoutedEventArgs e)
        {
            t31_InstructionsDomainDataSource.RejectChanges();
            EditInstructionsWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_InstructionsDataGrid.IsEnabled = true;
        }

        private void SaveInstructionButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentInstruction.Code == null || CurrentInstruction.Code.Length ==0)
            {
                MessageBox.Show("Поле 'Обозначение' не заполнено");
                return;
            }
            if (CurrentInstruction.Name == null || CurrentInstruction.Name.Length == 0)
            {
                MessageBox.Show("Поле 'Наименование' не заполнено");
                return;
            }
            CurrentInstruction.ChangedBy = Security.CurrentUserName;
            CurrentInstruction.ChangedAt = DateTime.Now;

            NewInstructionButton.IsEnabled = true;
            EditInstructionsWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_InstructionsDataGrid.IsEnabled = true;
            t31_InstructionsDataGrid.ScrollIntoView(t31_InstructionsDomainDataSource.DataView[0], null);
            t31_InstructionsDomainDataSource.SubmitChanges();
            ToLog(CurrentInstruction.Code, "ТИ", "Сохранение", CurrentInstruction.Name + ";" + CurrentInstruction.Changes);
        }

        private void DeleteInstruction_Click(object sender, RoutedEventArgs e)
        {
            NewInstructionButton.IsEnabled = true;
            int ci = (int)((TextImageButtonBase)sender).Tag;
            var ToDelete = t31_InstructionsDomainDataSource.DataView.Cast<T31_Instructions>().Where(i => i.ID == ci).SingleOrDefault();
            MessageBoxResult mbr = MessageBox.Show("Удалить инструкцию " + ToDelete.Name + "?", "Удаление инструкции", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.OK)
            {
                ToDelete.IsDeleted = true;
                t31_InstructionsDomainDataSource.SubmitChanges();

                ToLog(ToDelete.Code, "ТИ", "Удаление", ToDelete.Name + ";" + CurrentInstruction.Changes);
                CurrentInstruction = null;
            }
        }

        private void NewInstruction_Click(object sender, RoutedEventArgs e)
        {
            EditInstructionsWindow.Visibility = System.Windows.Visibility.Visible;
            t31_InstructionsDataGrid.IsEnabled = false; 
            T31_Instructions NewInstruction = new T31_Instructions();
            
            NewInstruction.ChangedBy = Security.CurrentUserName;
            NewInstruction.ChangedAt = DateTime.Now;
            NewInstruction.IsDeleted = false;
            NewInstruction.DeveloperId = (byte)((CurrentDepartment.HasValue) ? CurrentDepartment.Value : 1);
            t31_InstructionsDomainDataSource.DataView.Add(NewInstruction);
            t31_InstructionsDomainDataSource.DataView.MoveCurrentTo(NewInstruction);
            CurrentInstruction = NewInstruction;
        }
        
        private void EditInstruction_Click(object sender, RoutedEventArgs e)
        {
            t31_InstructionsDataGrid.IsEnabled = false;
            CurrentInstruction = t31_InstructionsDomainDataSource.DataView.Cast<T31_Instructions>().Where(i => i.ID == ((int)((TextImageButtonBase)sender).Tag)).SingleOrDefault();
            EditInstructionsWindow.Visibility = System.Windows.Visibility.Visible;
        }

        private void t31_InstructionsDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            t31_InstructionsDomainDataSource.Load();
        }

        #endregion

        #region Приказы

        #region События загрузки данных

        private void t31_OrdersDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {

            if (e.HasError)
            {
                System.Windows.MessageBox.Show(e.Error.ToString(), "Ошибка загрузки", System.Windows.MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            NewOrderButton.IsEnabled = true;
            t31_OrdersDataGrid.IsEnabled = true;
            if (CurrentOrder != null) t31_OrdersDataGrid.ScrollIntoView(CurrentOrder, null);
        }
        
        #endregion  
        
        private void CancelOrderEdit_Button(object sender, RoutedEventArgs e)
        {
            t31_OrdersDomainDataSource.RejectChanges();
            EditOrdersWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_OrdersDataGrid.IsEnabled = true;
        }

        private void SaveOrderButton_Click(object sender, RoutedEventArgs e)
        {
            int? Pos = GetNumberPart(CurrentOrder.Code); //(dbo.ParseDigitsToNumeric)
            if (Pos.HasValue && CurrentOrder.EmitDate.HasValue) Pos = Pos * 10000 + CurrentOrder.EmitDate.Value.Year;
            if (String.IsNullOrWhiteSpace(CurrentOrder.Code))
            {
                MessageBox.Show("Поле 'Обозначение' не заполнено");
                return;
            }

            if (!Pos.HasValue)
            {
                MessageBox.Show("Поле 'Обозначение' должно содержать номер документа");
                return;
            } 
            
            if (CurrentOrder.Name == null || CurrentOrder.Name.Length == 0)
            {
                MessageBox.Show("Поле 'Наименование' не заполнено");
                return;
            }

            if (CurrentOrder.EmitDate == null)
            {
                MessageBox.Show("Поле 'Дата выпуска' не заполнено");
                return;
            }
            
            int c;
            if (Int32.TryParse(CurrentOrder.Code, out c)) CurrentOrder.Code = "Т-" + CurrentOrder.Code;
            
            CurrentOrder.Position = Pos;
            NewOrderButton.IsEnabled = true;
            CurrentOrder.ChangedBy = Security.CurrentUserName;
            CurrentOrder.ChangedAt = DateTime.Now;
            EditOrdersWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_OrdersDataGrid.IsEnabled = true;
            t31_OrdersDataGrid.ScrollIntoView(t31_OrdersDomainDataSource.DataView[0], null);
            ReloadOrders = true;
            t31_OrdersDomainDataSource.SubmitChanges();
            ToLog(CurrentOrder.Code + (CurrentOrder.EmitDate.HasValue ? " " + CurrentOrder.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "Т", "Сохранение", CurrentOrder.Name);
        }

        private void DeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            NewOrderButton.IsEnabled = true;
            int ci = (int)((TextImageButtonBase)sender).Tag;
            var ToDelete = t31_OrdersDomainDataSource.DataView.Cast<T31_Orders>().Where(i => i.ID == ci).SingleOrDefault();
            MessageBoxResult mbr = MessageBox.Show("Удалить приказ " + ToDelete.Name + "?", "Удаление приказа", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.OK)
            {
                ToLog(ToDelete.Code + (ToDelete.EmitDate.HasValue ? " " + ToDelete.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "Т", "Удаление", ToDelete.Name);
                ToDelete.IsDeleted = true;
                ReloadOrders = true; 
                t31_OrdersDomainDataSource.SubmitChanges();
                CurrentOrder = null;
            }
        }

        private void NewOrder_Click(object sender, RoutedEventArgs e)
        {
            EditOrdersWindow.Visibility = System.Windows.Visibility.Visible;
            t31_OrdersDataGrid.IsEnabled = false;
            T31_Orders NewOrder = new T31_Orders();
            NewOrder.ChangedBy = Security.CurrentUserName;
            NewOrder.ChangedAt = DateTime.Now;
            NewOrder.IsDeleted = false;
            t31_OrdersDomainDataSource.DataView.Add(NewOrder);
            t31_OrdersDomainDataSource.DataView.MoveCurrentTo(NewOrder);
            CurrentOrder = NewOrder;
        }

        private void EditOrder_Click(object sender, RoutedEventArgs e)
        {
            t31_OrdersDataGrid.IsEnabled = false;
            CurrentOrder = t31_OrdersDomainDataSource.DataView.Cast<T31_Orders>().Where(i => i.ID == ((int)((TextImageButtonBase)sender).Tag)).SingleOrDefault();
            EditOrdersWindow.Visibility = System.Windows.Visibility.Visible;
        }

        private void t31_OrdersDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
           if (ReloadOrders) t31_OrdersDomainDataSource.Load();
           else t31_OrdersDataGrid.IsEnabled = true;
        }

        private void OrderCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb != null)
            {
                int id = (int)cb.Tag;
                T31_Orders c = t31_OrdersDomainDataSource.DataView.Cast<T31_Orders>().Where(i => i.ID == id).SingleOrDefault();
                switch (cb.Name.ToUpper())
                {
                    case "ADO":
                        c.ADO = cb.IsChecked;
                        break;
                    case "SO":
                        c.SO = cb.IsChecked;
                        break;
                    case "ORS":
                        c.ORS = cb.IsChecked;
                        break;
                    case "SEMO":
                        c.SEMO = cb.IsChecked;
                        break;
                    case "SORPO":
                        c.SorPO = cb.IsChecked;
                        break;
                    case "LPO":
                        c.LPO = cb.IsChecked;
                        break;
                    case "ONMKID":
                        c.ONMKID = cb.IsChecked;
                        break;
                    case "OMIT":
                        c.OMIT = cb.IsChecked;
                        break;
                    case "CEST":
                        c.CEST = cb.IsChecked;
                        break;
                    case "UOR":
                        c.UOR = cb.IsChecked;
                        break;
                    default:
                        break;
                }
                c.ChangedBy = Security.CurrentUserName;
                c.ChangedAt = DateTime.Now;
                                    
                ReloadOrders = false;
                t31_OrdersDataGrid.IsEnabled = false;
                t31_OrdersDomainDataSource.SubmitChanges();
                ToLog(c.Code + (c.EmitDate.HasValue ?  " " + c.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "Т", cb.Name.ToUpper(), (cb.IsChecked.HasValue) ? cb.IsChecked.ToString() : "Нет отметки");
            }
        }

        #endregion

        #region Распоряжения

        #region События загрузки данных

        private void t31_DirectionsDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {

            if (e.HasError)
            {
                System.Windows.MessageBox.Show(e.Error.ToString(), "Ошибка загрузки", System.Windows.MessageBoxButton.OK);
                e.MarkErrorAsHandled();
            }
            NewDirectionButton.IsEnabled = true;
            t31_DirectionsDataGrid.IsEnabled = true;
            if (CurrentDirection != null) t31_DirectionsDataGrid.ScrollIntoView(CurrentDirection, null);
        }

        #endregion

        private void CancelDirectionEdit_Button(object sender, RoutedEventArgs e)
        {
            t31_DirectionsDomainDataSource.RejectChanges();
            EditDirectionsWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_DirectionsDataGrid.IsEnabled = true;
        }

        private void SaveDirectionButton_Click(object sender, RoutedEventArgs e)
        {
            int? Pos = GetNumberPart(CurrentDirection.Code); //(dbo.ParseDigitsToNumeric)
            if (Pos.HasValue && CurrentDirection.EmitDate.HasValue) Pos = Pos * 10000 + CurrentDirection.EmitDate.Value.Year;
            if (String.IsNullOrWhiteSpace(CurrentDirection.Code))
            {
                MessageBox.Show("Поле 'Обозначение' не заполнено");
                return;
            }
            
            if (!Pos.HasValue)
            {
                MessageBox.Show("Поле 'Обозначение' должно содержать номер документа");
                return;
            }
            if (CurrentDirection.Name == null || CurrentDirection.Name.Length == 0)
            {
                MessageBox.Show("Поле 'Наименование' не заполнено");
                return;
            }
            if (CurrentDirection.EmitDate == null)
            {
                MessageBox.Show("Поле 'Дата выдачи' не заполнено");
                return;
            }
            int c;
            if (Int32.TryParse(CurrentDirection.Code, out c)) CurrentDirection.Code = "ТР-" + CurrentDirection.Code;

            CurrentDirection.Position = Pos;
            CurrentDirection.ChangedBy = Security.CurrentUserName;
            CurrentDirection.ChangedAt = DateTime.Now;
            
            NewDirectionButton.IsEnabled = true;
            EditDirectionsWindow.Visibility = System.Windows.Visibility.Collapsed;
            t31_DirectionsDataGrid.IsEnabled = true;
            t31_DirectionsDataGrid.ScrollIntoView(t31_DirectionsDomainDataSource.DataView[0], null);
            ReloadDirections = true;
            t31_DirectionsDomainDataSource.SubmitChanges();
            ToLog(CurrentDirection.Code + (CurrentDirection.EmitDate.HasValue ? " " + CurrentDirection.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "ТР", "Сохранение", CurrentDirection.Name);
        }

        private void DeleteDirection_Click(object sender, RoutedEventArgs e)
        {
            NewDirectionButton.IsEnabled = true;
            int ci = (int)((TextImageButtonBase)sender).Tag;
            var ToDelete = t31_DirectionsDomainDataSource.DataView.Cast<T31_Directions>().Where(i => i.ID == ci).SingleOrDefault();
            MessageBoxResult mbr = MessageBox.Show("Удалить распоряжение " + ToDelete.Name + "?", "Удаление распоряжения", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.OK)
            {
                ToLog(ToDelete.Code + (ToDelete.EmitDate.HasValue ? " " + ToDelete.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "ТР", "Удаление", ToDelete.Name); 
                ToDelete.IsDeleted = true;
                ReloadDirections = true;
                t31_DirectionsDomainDataSource.SubmitChanges();
                CurrentDirection = null;
            }
        }

        private void NewDirection_Click(object sender, RoutedEventArgs e)
        {
            EditDirectionsWindow.Visibility = System.Windows.Visibility.Visible;
            t31_DirectionsDataGrid.IsEnabled = false;
            T31_Directions NewDirection = new T31_Directions();
            NewDirection.ChangedBy = Security.CurrentUserName;
            NewDirection.ChangedAt = DateTime.Now;
            NewDirection.IsDeleted = false;
            t31_DirectionsDomainDataSource.DataView.Add(NewDirection);
            t31_DirectionsDomainDataSource.DataView.MoveCurrentTo(NewDirection);
            CurrentDirection = NewDirection;
        }

        private void EditDirection_Click(object sender, RoutedEventArgs e)
        {
            t31_DirectionsDataGrid.IsEnabled = false;
            CurrentDirection = t31_DirectionsDomainDataSource.DataView.Cast<T31_Directions>().Where(i => i.ID == ((int)((TextImageButtonBase)sender).Tag)).SingleOrDefault();
            EditDirectionsWindow.Visibility = System.Windows.Visibility.Visible;
        }

        private void t31_DirectionsDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            if (ReloadDirections) t31_DirectionsDomainDataSource.Load();
            else t31_DirectionsDataGrid.IsEnabled = true;
        }

        private void DirectionCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            if (cb != null)
            {
                int id = (int)cb.Tag;
                T31_Directions c = t31_DirectionsDomainDataSource.DataView.Cast<T31_Directions>().Where(i => i.ID == id).SingleOrDefault();
                switch (cb.Name.ToUpper())
                {
                    case "ADO":
                        c.ADO = cb.IsChecked;
                        break;
                    case "SO":
                        c.SO = cb.IsChecked;
                        break;
                    case "ORS":
                        c.ORS = cb.IsChecked;
                        break;
                    case "SEMO":
                        c.SEMO = cb.IsChecked;
                        break;
                    case "SORPO":
                        c.SorPO = cb.IsChecked;
                        break;
                    case "LPO":
                        c.LPO = cb.IsChecked;
                        break;
                    case "ONMKID":
                        c.ONMKID = cb.IsChecked;
                        break;
                    case "OMIT":
                        c.OMIT = cb.IsChecked;
                        break;
                    case "CEST":
                        c.CEST = cb.IsChecked;
                        break;
                    case "UOR":
                        c.UOR = cb.IsChecked;
                        break;
                    default:
                        break;
                }
                c.ChangedBy = Security.CurrentUserName;
                c.ChangedAt = DateTime.Now;
                ReloadDirections = false;
                t31_DirectionsDataGrid.IsEnabled = false;
                t31_DirectionsDomainDataSource.SubmitChanges();
                ToLog(c.Code + (c.EmitDate.HasValue ? " " + c.EmitDate.Value.ToString("dd.MM.yyyy") : String.Empty), "ТР", cb.Name.ToUpper(), (cb.IsChecked.HasValue) ? cb.IsChecked.ToString() : "Нет отметки");
            }
        }

        #endregion

        #region Отчеты

        private string[] FieldNames = new string[] { "ADO", "SO", "ORS", "SEMO", "SorPO", "LPO", "OMIT", "ONMKID", "CEST", "UOR" };
        private string[] FieldCaptions = new string[] { "АДО", "СО", "ОРС", "СЭМО", "СорПО", "ЛПО", "ОМиТ", "ЦНКМ", "ЦЭСТ", "ОГО" };
        private string[] ReportCaptions = new string[] { "Утвержденные распоряжения", "Отмененные распоряжения", "Утвержденные приказы", "Отмененные приказы" };
        private string[] ReportCodes = new string[] { "pr", "dr", "pt", "dt" };

        private void ConstructReportPage()
        {
            for (int i = 0; i < 1 + ReportCaptions.Length; i++) ReportsByDepartmentPlaceholder.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < 1 + FieldCaptions.Length; i++) ReportsByDepartmentPlaceholder.ColumnDefinitions.Add(new ColumnDefinition());
            ReportsByDepartmentPlaceholder.ColumnDefinitions[0].Width = new GridLength(200);

            for (int i = 0; i < ReportCaptions.Length; i++)
            {
                TextBlock tb = new TextBlock() { Text = ReportCaptions[i], HorizontalAlignment = System.Windows.HorizontalAlignment.Left, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(3) };
                b.Child = tb;
                Grid.SetColumn(b, 0);
                Grid.SetRow(b, i + 1);
                ReportsByDepartmentPlaceholder.Children.Add(b);
            }
            for (int i = 0; i < FieldCaptions.Length; i++)
            {
                TextBlock tb = new TextBlock() { Text = FieldCaptions[i], HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(1) };
                b.Child = tb;
                Grid.SetColumn(b, i + 1);
                Grid.SetRow(b, 0);
                ReportsByDepartmentPlaceholder.Children.Add(b);
            }
            for (int i = 0; i < ReportCaptions.Length; i++)
            {
                string Code = ReportCodes[i];

                for (int j = 0; j < FieldCaptions.Length; j++)
                {
                    StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
                    TextImageButtonBase mb1 = new TextImageButtonBase() { ImageUrl = "/AQResources;component/Images/word16.png", Tag = Code + "w" + j.ToString() };
                    mb1.Style = this.Resources["MiniImageButton"] as Style;
                    mb1.Click += ReportMB_Click;
                    mb1.Margin = new Thickness(5);
                    TextImageButtonBase mb2 = new TextImageButtonBase() { ImageUrl = "/AQResources;component/Images/excel16.png", Tag = Code + "x" + j.ToString() };
                    mb2.Style = this.Resources["MiniImageButton"] as Style;
                    mb2.Click += ReportMB_Click;
                    mb2.Margin = new Thickness(5);
                    sp.Children.Add(mb1);
                    sp.Children.Add(mb2);
                    Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(1) };
                    b.Child = sp;
                    Grid.SetColumn(b, j + 1);
                    Grid.SetRow(b, i + 1);
                    ReportsByDepartmentPlaceholder.Children.Add(b);
                }
            }


            AddNewCommonReport("a", "Полный список распоряжений");
            AddNewCommonReport("b", "Полный список приказов");
            AddNewCommonReport("i", "Полный список инструкций");
            AddNewCommonReport("l", "Действующие распоряжения");
            AddNewCommonReport("m", "Действующие приказы");
            AddNewCommonReport("j", "Действующие инструкции");
            AddNewCommonReport("k", "Удаленные инструкции");
            AddNewCommonReport("c", "Конфликты распоряжений");
            AddNewCommonReport("d", "Конфликты приказов");
            AddNewCommonReport("e", "Необработанные распоряжения");
            AddNewCommonReport("f", "Необработанные приказы");
            AddNewCommonReport("g", "Обработанные распоряжения без отметок (возможные ошибки)");
            AddNewCommonReport("h", "Обработанные приказы без отметок (возможные ошибки)");
        }

        private void ReportMB_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            bool IsApproved = mib.Tag.ToString().Substring(0, 1) == "p";
            bool IsDirection = mib.Tag.ToString().Substring(1, 1) == "r";
            bool IsWord = mib.Tag.ToString().Substring(2, 1) == "w";
            int Number = Int16.Parse(mib.Tag.ToString().Substring(3, 1));
            string Caption = FieldCaptions[Number];
            string Name = FieldNames[Number];

            string ReportName = (IsApproved) ? "Утвержденные " : "Отмененные ";
            ReportName += (IsDirection) ? "распоряжения " : "приказы ";
            ReportName += "отдела " + Caption;

            string SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position), Code as 'Обозначение', Name as 'Наименование', EmitDate as 'Дата выпуска' from ";
            SQL += (IsDirection) ? "t31_directions " : "t31_orders ";
            SQL += " where isdeleted<>1 and ";
            SQL += Name + "=" + ((IsApproved) ? "1 " : "0 ");
            SQL += "order by position";
            Uri Url = new Uri(HtmlPage.Document.DocumentUri, ((IsWord) ? "/AQSelection/T31Reports/ReportDocx.aspx" : "/AQSelection/T31Reports/ReportXlsx.aspx") + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }

        private void ChangesReport_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            bool IsOrder = mib.Tag.ToString().Substring(0, 1) == "T";
            bool IsProlonged = mib.Tag.ToString().Substring(1, 1) == "P";
            bool IsWord = mib.Tag.ToString().Substring(2, 1) == "W";

            string ReportName = (IsProlonged) ? "Утвержденные " : "Отмененные ";
            ReportName += (IsOrder) ? "приказы" : "распоряжения";
            string Part = IsOrder? "Т" : "ТР";
            string Table = IsOrder ? " T31_Orders " : " T31_Directions ";
            string Compare = IsProlonged ? " ='True' " : " <>'True' ";
            string AdditionalFields = IsWord ? "" : ", ma.UserName as 'Автор изменения', convert(nvarchar, ma.Date, 104) as 'Дата изменения'";
            DateTime from = MainDateRangePicker.From.Value;
            DateTime to = MainDateRangePicker.To.Value;

            string SQL = @"select d.code as 'Обозначение', convert(nvarchar, d.emitdate, 104) as 'Дата выпуска', d.Name as 'Наименование' " + AdditionalFields + @" 
                            from(select code, count(*) as Changes, MIN(date) as mindate, MAX(date) as maxdate from t31_useractions where part='" + Part + "' and Date>='" + from.ToString("yyyy-MM-dd") + "' and Date<'" + to.AddDays(1).ToString("yyyy-MM-dd") + @"' group by Code) as dates
                            inner join T31_UserActions as mi on mi.Date = mindate
                            inner join T31_UserActions as ma on ma.Date = maxdate
                            left outer join " + Table + @" as d on (d.Code %2B ' ' %2B CONVERT(nvarchar, EmitDate, 104)) = dates.code
                            where (mi.NewValue<>ma.NewValue or dates.Changes=1) and mi.NewValue in ('True','False','Нет отметки')
                             and ma.NewValue in ('True','False','Нет отметки') and d.Code is not null and d.EmitDate is not null and d.Name is not null
                             and ma.NewValue " + Compare ;

            Uri Url = new Uri(HtmlPage.Document.DocumentUri, ((IsWord) ? "/AQSelection/T31Reports/ReportDocx.aspx" : "/AQSelection/T31Reports/ReportXlsx.aspx") + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);

        }

        private void AddNewCommonReport(string Code, string Caption)
        {
            CommonReportsPlaceholder.RowDefinitions.Add(new RowDefinition());

            StackPanel sp = new StackPanel() { Orientation = Orientation.Horizontal, HorizontalAlignment = System.Windows.HorizontalAlignment.Center, VerticalAlignment = System.Windows.VerticalAlignment.Center };
            TextImageButtonBase mb1 = new TextImageButtonBase() { ImageUrl = "/AQResources;component/Images/word16.png", Tag = Code + "w" };
            mb1.Style = this.Resources["MiniImageButton"] as Style;
            mb1.Click += CommonReportMB_Click;
            mb1.Margin = new Thickness(5, 2, 5, 2);
            TextImageButtonBase mb2 = new TextImageButtonBase() { ImageUrl = "/AQResources;component/Images/excel16.png", Tag = Code + "x" };
            mb2.Style = this.Resources["MiniImageButton"] as Style;
            mb2.Click += CommonReportMB_Click;
            mb2.Margin = new Thickness(5, 2, 5, 2);
            sp.Children.Add(mb1);
            sp.Children.Add(mb2);
            Border b = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(2, 0, 0, 0) };
            b.Child = sp;
            Grid.SetColumn(b, 1);
            Grid.SetRow(b, CommonReportsPlaceholder.RowDefinitions.Count - 1);
            CommonReportsPlaceholder.Children.Add(b);

            TextBlock tb = new TextBlock() { Text = Caption, HorizontalAlignment = System.Windows.HorizontalAlignment.Left, VerticalAlignment = System.Windows.VerticalAlignment.Center };
            Border b2 = new Border() { BorderThickness = new Thickness(0.5), BorderBrush = new SolidColorBrush(Colors.LightGray), Margin = new Thickness(-0.5), Padding = new Thickness(2, 0, 0, 0) };
            b2.Child = tb;
            Grid.SetColumn(b2, 0);
            Grid.SetRow(b2, CommonReportsPlaceholder.RowDefinitions.Count - 1);
            CommonReportsPlaceholder.Children.Add(b2);
        }

        private void CommonReportMB_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            bool IsWord = mib.Tag.ToString().Substring(1, 1) == "w";
            string Req = mib.Tag.ToString().Substring(0, 1);
            string ReportName = "Ошибка", SQL = "select 'Ошибка в запросе' as 'Ошибка'";

            switch (Req)
            {
                case "a":
                    ReportName = "Полный список распоряжений";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ЦНКМ',[CEST] as 'ЦЭСТ',[UOR] as 'ОГО',[EmitDate] as 'Дата выпуска' from t31_directions where isdeleted<>1 order by position";
                    break;
                case "b":
                    ReportName = "Полный список приказов";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ЦНКМ',[CEST] as 'ЦЭСТ',[UOR] as 'ОГО',[EmitDate] as 'Дата выпуска' from t31_orders where isdeleted<>1 order by position";
                    break;
                case "l":
                    ReportName = "Действующие распоряжения";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_directions where  (ado <>0 or ado is null) and (so<>0 or so is null) and (ors<>0 or ors is null) and (semo <>0 or semo is null) and (sorpo<>0 or sorpo is null) and (lpo <>0 or lpo is null) and (omit <>0 or omit is null) and (onmkid<>0 or onmkid is null) and (cest <>0 or cest is null) and (uor<>0 or uor is null) and isdeleted<>1 order by position";
                    break;
                case "m":
                    ReportName = "Действующие приказы";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_orders where  (ado <>0 or ado is null) and (so<>0 or so is null) and (ors<>0 or ors is null) and (semo <>0 or semo is null) and (sorpo<>0 or sorpo is null) and (lpo <>0 or lpo is null) and (omit <>0 or omit is null) and (onmkid<>0 or onmkid is null) and (cest <>0 or cest is null) and (uor<>0 or uor is null) and isdeleted<>1 order by position";
                    break;

                case "i":
                    ReportName = "Полный список инструкций";
                    SQL = "SELECT [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,~[IsDeleted] as 'Действует' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId order by position";
                    break;
                case "j":
                    ReportName = "Действующие инструкции";
                    SQL = "SELECT [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,~[IsDeleted] as 'Действует' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId where isdeleted=0 order by position";
                    break;
                case "k":
                    ReportName = "Удаленные инструкции";
                    SQL = "SELECT [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[Changes] as 'Действующие изменения',[DepartmentUser] as 'Подразделение - пользователь',[DepartmentName] as 'Подразделение - разработчик',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' ,~[IsDeleted] as 'Действует' FROM [raport].[dbo].[T31_Instructions] as i inner join T31_Departments as d on d.id = i.DeveloperId where isdeleted=1 order by position";
                    break;
                case "c":
                    ReportName = "Конфликты распоряжений";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ЦНКМ',[CEST] as 'ЦЭСТ',[UOR] as 'ОГО',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_directions where (ado=1 or so=1 or ors=1 or semo=1 or sorpo=1 or lpo=1 or omit=1 or onmkid=1 or cest=1 or uor=1) and (ado=0 or so=0 or ors=0 or semo=0 or sorpo=0 or lpo=0 or omit=0 or onmkid=0 or cest=0 or uor=0) and isdeleted<>1 order by position";
                    break;
                case "d":
                    ReportName = "Конфликты приказов";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[ADO] as 'АДО',[SO] as 'СО',[ORS] as 'ОРС',[SEMO] as 'СЭМО',[SorPO] as 'СорПО',[LPO] as 'ЛПО',[OMIT] as 'ОМиТ',[ONMKID] as 'ЦНКМ',[CEST] as 'ЦЭСТ',[UOR] as 'ОГО',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_orders where (ado=1 or so=1 or ors=1 or semo=1 or sorpo=1 or lpo=1 or omit=1 or onmkid=1 or cest=1 or uor=1) and (ado=0 or so=0 or ors=0 or semo=0 or sorpo=0 or lpo=0 or omit=0 or onmkid=0 or cest=0 or uor=0) and isdeleted<>1 order by position";
                    break;
                case "e":
                    ReportName = "Необработанные распоряжения";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_directions where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and isdeleted<>1 order by position";
                    break;
                case "f":
                    ReportName = "Необработанные приказы";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска' from t31_orders where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and isdeleted<>1 order by position";
                    break;
                case "g":
                    ReportName = "Обработанные распоряжения без отметок (возможные ошибки)";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_directions where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and changedby is not null and isdeleted<>1 order by position";
                    break;
                case "h":
                    ReportName = "Обработанные приказы без отметок (возможные ошибки)";
                    SQL = "select [п/п] = ROW_NUMBER() OVER (ORDER BY Position),[Code] as 'Обозначение',[Name] as 'Наименование',[EmitDate] as 'Дата выпуска',[ChangedBy] as 'Изменено',[ChangedAt] as 'Дата изменения' from t31_orders where (ado is null and so is null and ors is null and semo is null and sorpo is null and lpo is null and omit is null and onmkid is null and cest is null and uor is null) and changedby is not null and isdeleted<>1 order by position";
                    break;
                default:
                    break;

            }
            Uri Url = new Uri(HtmlPage.Document.DocumentUri, ((IsWord) ? "/AQSelection/T31Reports/ReportDocx.aspx" : "/AQSelection/T31Reports/ReportXlsx.aspx") + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }

        private void InstructionsReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri(HtmlPage.Document.DocumentUri, @"/AQSelection/T31Reports/GetInstructions.aspx?FirstPage=" + InstructionsFromPage.Text);
            HtmlPage.Window.Navigate(Url);
        }

        private void OrdersReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri(HtmlPage.Document.DocumentUri, @"/AQSelection/T31Reports/GetOrders.aspx?FirstPage=" + OrdersFromPage.Text);
            HtmlPage.Window.Navigate(Url);
        }

        private void DirectionsReport_Click(object sender, RoutedEventArgs e)
        {
            Uri Url = new Uri(HtmlPage.Document.DocumentUri, @"/AQSelection/T31Reports/GetDirections.aspx?FirstPage=" + DirectionsFromPage.Text);
            HtmlPage.Window.Navigate(Url);
        }

        private void InstructionsFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            InstructionsReport.IsEnabled = (int.TryParse(tb.Text, out Page) || tb.Text.Trim() == string.Empty);
        }

        private void OrdersFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            OrdersReport.IsEnabled = (int.TryParse(tb.Text, out Page) || tb.Text.Trim() == string.Empty);
        }

        private void DirectionsFromPage_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            int Page;
            DirectionsReport.IsEnabled = (int.TryParse(tb.Text, out Page) || tb.Text.Trim() == string.Empty);
        }
        #endregion
        
        #region Администрирование

        private void SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            t31_SettingsDomainDataSource.SubmitChanges();
        }

        private void RefreshUserActionsButton_Click(object sender, RoutedEventArgs e)
        {
            t31_UserActionsDomainDataSource.Load();
        }

        private void ReportToXlsxUserActionsButton_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            string ReportName = "Действия пользователей";
            string SQL = "select top 1000 Date as 'Дата', UserName as 'Пользователь', Part as 'Приложение', Field as 'Поле', NewValue as 'Новое значение', Code as 'Обозначение документа' from T31_UserActions order by date desc";
            Uri Url = new Uri("http://itc-serv01.chmk.mechelgroup.ru/t31/ReportXlsx.aspx" + "?ReportName=" + HttpUtility.UrlEncode(ReportName) + "&SQL=" + HttpUtility.UrlEncode(SQL));
            HtmlPage.Window.Navigate(Url);
        }

        private void t31_SettingsDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            ReflectSettingsOnInterface();
        }
        private void t31_SettingsDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            ReflectSettingsOnInterface();
            MainTabControl.Visibility = System.Windows.Visibility.Visible;
        }
        #endregion

        #region Запись действий пользователей

        private void ToLog(string Doc, string App, string Field, string Value)
        {
            T31_UserActions u = new T31_UserActions();
            u.Code = Doc;
            u.Date = DateTime.Now;
            u.Field = Field;
            u.Part = App;
            u.NewValue = Value;
            u.UserName = Security.CurrentUserName;
            t31_UserActionsDomainDataSource.DataView.Add(u);
            t31_UserActionsDomainDataSource.SubmitChanges();
        }

        #endregion

        #region Управление правами

        private void ReflectSettingsOnInterface()
        {
            UpdateInterfaceBySettings();
            UpdateInterfaceByRoles();
        }

        private void UpdateInterfaceByRoles()
        {
            bool IsAdmin = Security.IsInRole("Т31_Администратор");
            bool IsUser = Security.IsInRole("Т31_Редактирование") && (IsRightSet("EditionEnabled")).Value;
            (MainTabControl.Items[5] as TabItem).IsEnabled = IsAdmin;
            RightsForAcceptRejectBoxes rfarb1 = this.Resources["RightsForAcceptRejectBoxesOrders"] as RightsForAcceptRejectBoxes;
            SetDepartmentRights(IsAdmin, IsUser, rfarb1);
            RightsForAcceptRejectBoxes rfarb2 = this.Resources["RightsForAcceptRejectBoxesDirections"] as RightsForAcceptRejectBoxes;
            SetDepartmentRights(IsAdmin, IsUser, rfarb2);
        }

        private void SetDepartmentRights(bool IsAdmin, bool IsUser, RightsForAcceptRejectBoxes rfab)
        {
            rfab.ADO = (Security.IsInRole("Т31_АДО") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.CEST = (Security.IsInRole("Т31_ЦЭСТ") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.LPO = (Security.IsInRole("Т31_ЛПО") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.OMIT = (Security.IsInRole("Т31_ОМиТ") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.ONMKID = (Security.IsInRole("Т31_ОНМКиД") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.ORS = (Security.IsInRole("Т31_ОРС") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.SEMO = (Security.IsInRole("Т31_СЭМО") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.SO = (Security.IsInRole("Т31_СО") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.SorPO = (Security.IsInRole("Т31_СОРПО") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.UOR = (Security.IsInRole("Т31_УОР") && IsUser && IsEditionEnabled) || (IsAdmin && IsEditionEnabled);
            rfab.GetTR = (Security.IsInRole("Просмотр_ТР"));
        }

        private void UpdateInterfaceBySettings()
        {
            IsEditionEnabled = (IsRightSet("EditionEnabled")).Value;
            bool IsUser = (IsRightSet("EditionEnabled")).Value && (Security.IsInRole("Т31_Администратор") || Security.IsInRole("Т31_Редактирование"));
            RightsForAddDelete rfad = this.Resources["RightsForAddDelete"] as RightsForAddDelete;

            rfad.InstructionDeleteEnabled = IsEditionEnabled  && IsRightSet("InstructionDeleteEnabled").Value;
            rfad.InstructionNewEnabled = IsEditionEnabled && IsRightSet("InstructionNewEnabled").Value;
            rfad.InstructionEditEnabled = IsEditionEnabled && IsRightSet("InstructionEditEnabled").Value;

            rfad.OrdersDeleteEnabled = IsEditionEnabled && IsRightSet("OrdersDeleteEnabled").Value;
            rfad.OrdersNewEnabled = IsEditionEnabled && IsRightSet("OrdersNewEnabled").Value;
            rfad.OrdersEditEnabled = IsEditionEnabled && IsRightSet("OrdersEditEnabled").Value;

            rfad.DirectionsDeleteEnabled = IsEditionEnabled && IsRightSet("DirectionsDeleteEnabled").Value;
            rfad.DirectionsNewEnabled = IsEditionEnabled && IsRightSet("DirectionsNewEnabled").Value;
            rfad.DirectionsEditEnabled = IsEditionEnabled && IsRightSet("DirectionsEditEnabled").Value;

            t31_InstructionsDataGrid.UpdateLayout();
            t31_DirectionsDataGrid.UpdateLayout();
            t31_OrdersDataGrid.UpdateLayout();
            RightsTextBox.Text = ((Security.IsInRole("Т31_Администратор")) ? "Администратор Т31: " : "Пользователь Т31: ") + Security.CurrentUserName + "; " + "редактирование " + ((IsUser) ? "разрешено" : "запрещено");
        }

        private bool? IsRightSet(string Right)
        {
            if (t31_SettingsDomainDataSource == null || t31_SettingsDomainDataSource.Data.Cast<T31_Settings>().Count()==0) return false;
            return (from s in t31_SettingsDomainDataSource.DataView.Cast<T31_Settings>() where s.Code.ToLower() == Right.ToLower() select s.IsAllowed).Single();
        }
        #endregion
        
        private int? GetNumberPart(string Code)
        {
            if (Code == null) return null;
            int Result = 0;
            string n = Regex.Replace(Code, @"[^0-9]*", "");
            if (!Int32.TryParse(n, out Result)) return null;
            return Result;
        }

        #region Выдача ТР

        private void GetDirection_Click(object sender, RoutedEventArgs e)
        {
            int ci = (int)((TextImageButtonBase)sender).Tag;
            var ToEmit = t31_DirectionsDomainDataSource.DataView.Cast<T31_Directions>().Where(i => i.ID == ci).SingleOrDefault();
            
            if (ToEmit != null)
            {
                string StartsWith = ToEmit.Code + " от " + ToEmit.EmitDate.Value.ToString("dd.MM.yyyy");
                
                bool Disposed = false;
                if (ToEmit.ADO.HasValue && ToEmit.ADO.Value == false) Disposed = true;
                if (ToEmit.CEST.HasValue && ToEmit.CEST.Value == false) Disposed = true;
                if (ToEmit.LPO.HasValue && ToEmit.LPO.Value == false) Disposed = true;
                if (ToEmit.OMIT.HasValue && ToEmit.OMIT.Value == false) Disposed = true;
                if (ToEmit.ONMKID.HasValue && ToEmit.ONMKID.Value == false) Disposed = true;
                if (ToEmit.ORS.HasValue && ToEmit.ORS.Value == false) Disposed = true;
                if (ToEmit.SEMO.HasValue && ToEmit.SEMO.Value == false) Disposed = true;
                if (ToEmit.SO.HasValue && ToEmit.SO.Value == false) Disposed = true;
                if (ToEmit.SorPO.HasValue && ToEmit.SorPO.Value == false) Disposed = true;
                if (ToEmit.UOR.HasValue && ToEmit.UOR.Value == false) Disposed = true;

                if (!ToEmit.ADO.HasValue && 
                    !ToEmit.CEST.HasValue && 
                    !ToEmit.LPO.HasValue && 
                    !ToEmit.OMIT.HasValue && 
                    !ToEmit.ONMKID.HasValue && 
                    !ToEmit.ORS.HasValue && 
                    !ToEmit.SEMO.HasValue && 
                    !ToEmit.SO.HasValue && 
                    !ToEmit.SorPO.HasValue && 
                    !ToEmit.UOR.HasValue && (ToEmit.EmitDate.HasValue && ToEmit.EmitDate.Value.Year < DateTime.Now.Year)) Disposed = true;
                
                MessageBoxResult mbr = MessageBoxResult.OK;
                //if (Disposed) mbr = MessageBox.Show(StartsWith + " по приказу Т-31 не действует. Открыть просмотр ?", "Предупреждение", MessageBoxButton.OKCancel);
                if (mbr == MessageBoxResult.Cancel) return;
                
                aqs = Services.GetAQService();
                aqs.BeginGetTR(StartsWith, (i) => 
                {
                    try
                    {
                        string Guid = aqs.EndGetTR(i);
                        if (String.IsNullOrEmpty(Guid)) this.Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Скан " + StartsWith + " не найден. "+ Guid); });
                        else this.Dispatcher.BeginInvoke(delegate() 
                        {
                            HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(Guid) + "&FName=" + HttpUtility.UrlEncode(StartsWith + ".pdf") + "&ContentType=" + HttpUtility.UrlEncode("application/pdf"))); 
                        });
                    }
                    catch { this.Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Скан " + StartsWith + " не найден, ошибка при поиске. "); }); }
                }, null);
            }
        }

        #endregion

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1 + " ";
        }

        private void UnlockEditButton_Click(object sender, RoutedEventArgs e)
        {
            List<string> NotAllow = new List<String>() { "OrdersDeleteEnabled", "DirectionsDeleteEnabled"};
            foreach (var x in t31_SettingsDomainDataSource.Data.Cast<T31_Settings>())
            {
                x.IsAllowed = !NotAllow.Contains(x.Code);
            }
            t31_SettingsDomainDataSource.SubmitChanges();
            ReflectSettingsOnInterface();
        }

        private void LockEditButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var x in t31_SettingsDomainDataSource.Data.Cast<T31_Settings>())
            {
                x.IsAllowed = false;
            }
            t31_SettingsDomainDataSource.SubmitChanges();
            ReflectSettingsOnInterface();
        }

        private void ResetAllButton_Click(object sender, RoutedEventArgs e)
        {
            var LastStarts = t31_UserActionsDomainDataSource.Data.Cast<T31_UserActions>().Where(a => a.Part == "Новый период").Select(a => a.Date.Value.Year);

            if (LastStarts.Any() && LastStarts.Max() == DateTime.Now.Year)
            {
                MessageBox.Show("Период редактирования может быть начат только один раз в году.");
                return;
            }
            
            if (DateTime.Now.Month < 12)
            {
                MessageBox.Show("Период редактирования может быть начат только в декабре");
                return;
            }
                        
            MessageBoxResult mbr = MessageBox.Show(@"Будут удалены все ТР и Т с пометкой отмены. Будет установлена отметка редактирования в прошлом периоде для продленных ТР и Т (зеленый квадрат). Будут сброшены все отметки продления ТР и Т. Продолжить?", "Внимание!", MessageBoxButton.OKCancel);

            if (mbr == MessageBoxResult.Cancel) return;

            t31_DirectionsDomainDataSource.Load();
            t31_OrdersDomainDataSource.Load();
            var year = DateTime.Now.Year;
            foreach (var o in t31_OrdersDomainDataSource.Data.Cast<T31_Orders>())
            {
                if (((o.ADO.HasValue && !o.ADO.Value) ||
                    (o.CEST.HasValue && !o.CEST.Value) ||
                    (o.LPO.HasValue && !o.LPO.Value) ||
                    (o.OMIT.HasValue && !o.OMIT.Value) ||
                    (o.ONMKID.HasValue && !o.ONMKID.Value) ||
                    (o.ORS.HasValue && !o.ORS.Value) ||
                    (o.SEMO.HasValue && !o.SEMO.Value) ||
                    (o.SO.HasValue && !o.SO.Value) ||
                    (o.SorPO.HasValue && !o.SorPO.Value) ||
                    (o.UOR.HasValue && !o.UOR.Value)) ||
                    (
                        (!o.ADO.HasValue &&
                        !o.CEST.HasValue &&
                        !o.LPO.HasValue &&
                        !o.OMIT.HasValue &&
                        !o.ONMKID.HasValue &&
                        !o.ORS.HasValue &&
                        !o.SEMO.HasValue &&
                        !o.SO.HasValue &&
                        !o.SorPO.HasValue &&
                        !o.UOR.HasValue) &&
                        (!o.EmitDate.HasValue || o.EmitDate.Value.Year < year)
                    ))
                {
                    o.IsDeleted = true;
                    o.ChangedAt = DateTime.Now;
                    o.ChangedBy = Security.CurrentUserName;
                }
                else
                {
                    o.OldADO = o.ADO;
                    o.OldCEST = o.CEST;
                    o.OldLPO = o.LPO;
                    o.OldOMIT = o.OMIT;
                    o.OldONMKID = o.ONMKID;
                    o.OldORS = o.ORS;
                    o.OldSEMO = o.SEMO;
                    o.OldSO = o.SO;
                    o.OldSorPO = o.SorPO;
                    o.OldUOR = o.UOR;

                    o.ADO = null;
                    o.CEST = null;
                    o.LPO = null;
                    o.OMIT = null;
                    o.ONMKID = null;
                    o.ORS = null;
                    o.SEMO = null;
                    o.SO = null;
                    o.SorPO = null;
                    o.UOR = null;

                    o.ChangedAt = DateTime.Now;
                    o.ChangedBy = Security.CurrentUserName;
                }
            }

            t31_OrdersDomainDataSource.SubmitChanges();
            
            foreach (var o in t31_DirectionsDomainDataSource.Data.Cast<T31_Directions>())
            {
                if (((o.ADO.HasValue && !o.ADO.Value) ||
                    (o.CEST.HasValue && !o.CEST.Value) ||
                    (o.LPO.HasValue && !o.LPO.Value) ||
                    (o.OMIT.HasValue && !o.OMIT.Value) ||
                    (o.ONMKID.HasValue && !o.ONMKID.Value) ||
                    (o.ORS.HasValue && !o.ORS.Value) ||
                    (o.SEMO.HasValue && !o.SEMO.Value) ||
                    (o.SO.HasValue && !o.SO.Value) ||
                    (o.SorPO.HasValue && !o.SorPO.Value) ||
                    (o.UOR.HasValue && !o.UOR.Value)) ||
                    (
                        (!o.ADO.HasValue &&
                        !o.CEST.HasValue &&
                        !o.LPO.HasValue &&
                        !o.OMIT.HasValue &&
                        !o.ONMKID.HasValue &&
                        !o.ORS.HasValue &&
                        !o.SEMO.HasValue &&
                        !o.SO.HasValue &&
                        !o.SorPO.HasValue &&
                        !o.UOR.HasValue) &&
                        (!o.EmitDate.HasValue || o.EmitDate.Value.Year < year)
                    ))
                {
                    o.IsDeleted = true;
                    o.ChangedAt = DateTime.Now;
                    o.ChangedBy = Security.CurrentUserName;
                }
                else
                {
                    o.OldADO = o.ADO;
                    o.OldCEST = o.CEST;
                    o.OldLPO = o.LPO;
                    o.OldOMIT = o.OMIT;
                    o.OldONMKID = o.ONMKID;
                    o.OldORS = o.ORS;
                    o.OldSEMO = o.SEMO;
                    o.OldSO = o.SO;
                    o.OldSorPO = o.SorPO;
                    o.OldUOR = o.UOR;

                    o.ADO = null;
                    o.CEST = null;
                    o.LPO = null;
                    o.OMIT = null;
                    o.ONMKID = null;
                    o.ORS = null;
                    o.SEMO = null;
                    o.SO = null;
                    o.SorPO = null;
                    o.UOR = null;

                    o.ChangedAt = DateTime.Now;
                    o.ChangedBy = Security.CurrentUserName;
                }
            }

            t31_DirectionsDomainDataSource.SubmitChanges();

            ToLog("", "Новый период", "", "Начало периода");

        }
    }

    #region Конвертеры
    public class DepartmentValueConverter : System.Windows.Data.IValueConverter
    {
        public DomainDataSource DepartmentSource;
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || DepartmentSource == null) return null;
            return DepartmentSource.Data.Cast<T31_Departments>().Where(s => s.id == (byte)value).First().DepartmentName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    public class LastYearsConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime? v = value as DateTime?;
            if (v.HasValue) return ((v.Value.Year >= 2002)? Visibility.Visible : Visibility.Collapsed);
            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class BooleanToVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool? v = value as bool?;
            if (v.HasValue && v.Value) return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class DateTimeToStringValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime? v = value as DateTime?;
            if (!v.HasValue) return null;
            return v.Value.ToString("dd.MM.yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime v;
            if (DateTime.TryParseExact(value.ToString(), "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out v)) return v;
            else return null;
        }
    }
    #endregion

    #region Классы управления правами

    public class RightsForAcceptRejectBoxes : INotifyPropertyChanged
    {
        private bool ado;
        public bool ADO { get { return ado; } set { ado = value; NotifyPropertyChanged("ADO"); } }
        private bool so;
        public bool SO { get { return so; } set { so = value; NotifyPropertyChanged("SO"); } }
        private bool ors;
        public bool ORS { get { return ors; } set { ors = value; NotifyPropertyChanged("ORS"); } }
        private bool semo;
        public bool SEMO { get { return semo; } set { semo = value; NotifyPropertyChanged("SEMO"); } }
        private bool sorpo;
        public bool SorPO { get { return sorpo; } set { sorpo = value; NotifyPropertyChanged("SorPO"); } }
        private bool lpo;
        public bool LPO { get { return lpo; } set { lpo = value; NotifyPropertyChanged("LPO"); } }
        private bool omit;
        public bool OMIT { get { return omit; } set { omit = value; NotifyPropertyChanged("OMIT"); } }
        private bool onmkid;
        public bool ONMKID { get { return onmkid; } set { onmkid = value; NotifyPropertyChanged("ONMKID"); } }
        private bool cest;
        public bool CEST { get { return cest; } set { cest = value; NotifyPropertyChanged("CEST"); } }
        private bool uor;
        public bool UOR { get { return uor; } set { uor = value; NotifyPropertyChanged("UOR"); } }
        private bool gettr;
        public bool GetTR { get { return gettr; } set { gettr = value; NotifyPropertyChanged("GetTR"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class RightsForAddDelete : INotifyPropertyChanged
    {
        private bool ide;
        public bool InstructionDeleteEnabled { get { return ide; } set { ide = value; NotifyPropertyChanged("InstructionDeleteEnabled"); } }
        private bool ine;
        public bool InstructionNewEnabled { get { return ine; } set { ine = value; NotifyPropertyChanged("InstructionNewEnabled"); } }
        private bool ie;
        public bool InstructionEditEnabled { get { return ie; } set { ie = value; NotifyPropertyChanged("InstructionEditEnabled"); } }
        private bool ode;
        public bool OrdersDeleteEnabled { get { return ode; } set { ode = value; NotifyPropertyChanged("OrdersDeleteEnabled"); } }
        private bool one;
        public bool OrdersNewEnabled { get { return one; } set { one = value; NotifyPropertyChanged("OrdersNewEnabled"); } }
        private bool oe;
        public bool OrdersEditEnabled { get { return oe; } set { oe = value; NotifyPropertyChanged("OrdersEditEnabled"); } }
        private bool dde;
        public bool DirectionsDeleteEnabled { get { return dde; } set { dde = value; NotifyPropertyChanged("DirectionsDeleteEnabled"); } }
        private bool dne;
        public bool DirectionsNewEnabled { get { return dne; } set { dne = value; NotifyPropertyChanged("DirectionsNewEnabled"); } }
        private bool de;
        public bool DirectionsEditEnabled { get { return de; } set { de = value; NotifyPropertyChanged("DirectionsEditEnabled"); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    #endregion
    }
}
