﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AQMathClasses
{
    public static partial class AQMath
    {

        public static double MatrixDeterminant(double[,] matrix)
        {
            double det = 1;
            int n = (int)Math.Sqrt(matrix.Length);
            double[][] a = new double[n][];

            double[][] b = new double[1][];
            for (int i=0; i<n; i++)a[i] = new double[n];
            b[0] = new double[n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    a[i][j] = matrix[i,j];
                }
            }
            
            for (int i = 0; i < n; ++i)
            {
                int k = i;
                for (int j = i + 1; j < n; ++j) if (Math.Abs(a[j][i]) > Math.Abs(a[k][i])) k = j;
                if (Math.Abs(a[k][i]) < double.Epsilon)
                {
                    det = 0;
                    break;
                }
                b[0] = a[i];
                a[i] = a[k];
                a[k] = b[0];
                if (i != k) det = -det;
                det *= a[i][i];
                for (int j = i + 1; j < n; ++j) a[i][j] /= a[i][i];
                for (int j = 0; j < n; ++j) if ((j != i) && (Math.Abs(a[j][i]) > double.Epsilon)) for (k = i + 1; k < n; ++k) a[j][k] -= a[i][k]*a[j][i];
            }
            return det;
        }

        public static double[,] MatrixPenroseInvert(double[,] a)
        {
            var n = a.GetLength(0);
            var v = MatrixV0(a);
            var i = MatrixI(n);
            var i2 = MatrixScalarMultiple(i, 2);
            double[,] newv, va, s;
            double r = double.PositiveInfinity;
            var iteration = 0;
            var d = new List<double>();
            do
            {
                va = MatrixMultiply(v, a);
                s = MatrixScalarMultiple(va, -1);
                newv = MatrixMultiply(MatrixAdd(i2, s), v);
                d.Clear();
                for (int k = 0; k < n; k++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        d.Add( Math.Abs( v[k,j] - newv[k, j]));
                    }
                }
                r = d.Max();
                v = newv;
                iteration++;
            } while ( r > 0.0001);
            return newv;
        }

        public static double[,] MatrixAdd(double[,] a, double[,] b)
        {
            var n = a.GetLength(0);
            var m = a.GetLength(1);
            var result = new double[n, m];
            for (int i = 0; i< n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    result[i, j] = a[i, j] + b[i, j];
                }
            }
            return result;
        }

        public static double[,] MatrixV0(double[,] a)
        {
            return MatrixScalarMultiple(MatrixTranspose(a), 0.001); ;
        }

        public static double[,] MatrixScalarMultiple(double[,] a, double x)
        {
            var n = a.GetLength(0);
            var m = a.GetLength(1);
            var result = new double[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    result[i, j] = a[i, j] * x;
                }
            }
            return result;
        }

        public static double MatrixMaximumRowSumNorm(double[,] a)
        {
            var n = a.GetLength(0);
            var m = a.GetLength(1);
            var rowsum = new List<double>();

            for (int i = 0; i < n; i++)
            {
                var s = 0.0d;
                for (int j = 0; j < m; j++)
                {
                    s += Math.Abs(a[i, j]);
                }
                rowsum.Add(s);
            }
            return rowsum.Max();
        }

        public static double[,] MatrixI(int n)
        {
            var result = new double[n, n];
            for (int i=0; i< n;i++)
            {
                result[i, i] = 1;
            }
            return result;
        }

        /*
        public static double[,] MatrixPenroseInvert(double[,] a)
        {
            var nc = a.GetLength(0);
            var nr = a.GetLength(1);
            var u = new double[nc, nc];
            var mr = nr;                                                //      subroutine ginv2(a, u, aflag, atemp, mr, nr, nc)
            var aflag = new double[nc];
            var atemp = new double[nc];                                 //      dimension  a(mr, nc), u(nc, nc), aflag(nc), atemp(nc)
            for (var i = 0; i < nc; i++)                                //      do 10 i # 1, nc
            {
                for (var j = 0; j < nc; j++)                            //	    do 5  j # 1, nc
                {
                    u[i, j] = 0;                                        //  5   u(i, j) # 0.0
                }
                u[i, i] = 1;                                            //  10  u(i, i) # 1.0
            }
            var fac = Dot(mr, nr, a, 0, 0);                             //  	fac # dot (mr, nr, a, 1, 1)
            fac = 1.0 / Math.Sqrt(fac);
            for (int i = 0; i < nr; i++)                                //  	do 15 i # 1, nr
            {
                a[i, 0] *= fac;                                         //  15  a(i, 1) # a(i, 1) * fac
            }
            for (int i = 0; i < nc; i++)                                //  	do 20 i # 1, nc
            {
                u[i, 0] *= fac;                                         //  20  u(I, 1) # u(i, 1) * fac
            }
            aflag[0] = 1.0;
            var tol = double.Epsilon;                                   //  	tol # (10. * 0.5 ** n) **2

            for (int j = 1; j < nc; j++)                                //  	do 100 j # 2, nc
            {

                var dot1 = Dot(mr, nr, a, j, j);                        //  	dot1 # dot(mr, nr, a, j, j)
                var jm1 = j - 1;
                for (int l = 0; l < 2; l++)                             //      do 50 l # 1, 2
                {
                    for (int k = 0; k < jm1; k++)                       //  	do 30 k # 1, jm1
                    {
                        atemp[k] = Dot(mr, nr, a, j, k);                //  30  atemp(k) # dot(mr, nr, a, j, k)
                    }
                    for (int k = 0; k < jm1; k++)                       //  	do 45 k # 1, jm1
                    {
                        for (int i = 0; i < nr; i++)                    //  	do 35 i # 1, nr
                        {
                            a[i, j] -= atemp[k] * a[i, k] * aflag[k];   //  35  a(i, j) # a(i,j) - atemp(k) * a(i,k ) * aflag(k)
                        }
                        for (int i = 0; i < nc; i++)                    //  	do 40 i # 1, nc
                        {
                            u[i, j] -= atemp[k] * u[i, k];              //  40  u(i, j) # u(i,j) - atemp(k) * u(i,k)
                        }
                    }                                                   //  45  continue

                }                                                       //  50  continue
                var dot2 = Dot(mr, nr, a, j, j);                        //      dot2 # dot(mr,nr, a, j, j)
                if ((dot2 / dot1) < tol)                                //  	if ((dot2 / dot1) - tol) 55, 55, 70 \\ <0, =0, >0
                {
                    for (int i = 0; i < jm1; i++)                       //  55  do 60 i # 1, jm1
                    {
                        atemp[i] = 0.0d;                                //  	atemp(i) # 0.0
                        for (int k = 0; k < i; k++)                     //  	do 60 k # 1, I
                        {
                            atemp[i] += u[k, i] * u[k, j];              //  60  atemp(i) # atemp(i) + u(k, i) * u (k, j)   
                        }
                    }
                    for (int i = 0; i< nr; i++)                         //  	do 65 i # 1, nr
                    {
                        a[i, j] = 0.0d;                                 //  	a(i, j) # 0.0
                        for (int k=0; k< jm1; k++)                      //  	do 65 k # 1, jm1
                        {
                            a[i, j] -= a[i, k] * atemp[k] * aflag[k];   //  65  a(i, j) # a(i,j) - a(i,k) * atemp(k) * aflag(k)
                        }
                    }

                    aflag[j] = 0.0d;                                    //  	aflag(j) # 0.0
                    fac = Dot(nc, nc, u, j, j);                         //  	fac # dot(nc, nc, u, j ,j)
                    fac = 1.0d / Math.Sqrt(fac);                        //  	fac # 1.0 / sqrt(fac)
                }
                else                                                    //  	goto 75
                {
                    aflag[j] = 1.0d;                                    //  70  aflag(j) # 1.0
                    fac = 1.0d / Math.Sqrt(dot2);                       //  	fac # 1.0 / sqrt(dot2)
                }
                for (int i = 0; i < nr; i++)                            //  75  do 80 i # 1, nr
                {
                    a[i,j] *= fac;                                      //  80  a(i, j) # a(i,j) * fac
                }
                for (int i = 0; i < nc; i++)                            //  	do 85 i # 1, nc
                {
                    u[i, j] *= fac;                                     //  85  u(i, j) # u(i,j) * fac
                }
            }

            for (int j = 0; j < nc; j++)                                //      do 130 j # 1, nc
            {
                for (int i = 0; i < nr; i++)                            //  	do 130 i # 1, nr
                {
                    fac = 0.0d;                                         //  	fac # 0.0
                    for (int k = j; k < nc; k++)                        //  	do 120 k # j, nc
                    {
                        fac += a[i, k] * u[j, k];                       //  120 fac # fac + a(i,k) * u (j,k)
                    }

                    a[i, j] = fac;                                      //  130 a(i, j) # fac
                }
            }
            return a;
        }

        public static double Dot(int mr, int nr, double[,] a, int jc, int kc)
        {
            var dot = 0.0;
            for (int i = 0; i< nr; i++)
            {
                dot += a[i, jc] * a[i, kc];
            }
            return dot;
        }
        */

        public static double[][] MatrixInvert(double[][] A)
        {
            int n = A.Length;
            //e will represent each column in the identity matrix
            double[] e;
            //x will hold the inverse matrix to be returned
            double[][] x = new double[n][];
            for (int i = 0; i < n; i++)
            {
                x[i] = new double[A[i].Length];
            }
            /*
            * solve will contain the vector solution for the LUP decomposition as we solve
            * for each vector of x.  We will combine the solutions into the double[][] array x.
            * */
            double[] solve;

            //Get the LU matrix and P matrix (as an array)
            Tuple<double[][], int[]> results = LUPDecomposition(A);

            double[][] LU = results.Item1;
            int[] P = results.Item2;

            /*
            * Solve AX = e for each column ei of the identity matrix using LUP decomposition
            * */
            for (int i = 0; i < n; i++)
            {
                e = new double[A[i].Length];
                e[i] = 1;
                solve = LUPSolve(LU, P, e);
                for (int j = 0; j < solve.Length; j++)
                {
                    x[j][i] = solve[j];
                }
            }
            return x;
        }

        public static double[] LUPSolve(double[][] LU, int[] pi, double[] b)
        {
            int n = LU.Length - 1;
            double[] x = new double[n + 1];
            double[] y = new double[n + 1];
            double suml = 0;
            double sumu = 0;
            double lij = 0;

            /*
            * Solve for y using formward substitution
            * */
            for (int i = 0; i <= n; i++)
            {
                suml = 0;
                for (int j = 0; j <= i - 1; j++)
                {
                    /*
                    * Since we've taken L and U as a singular matrix as an input
                    * the value for L at index i and j will be 1 when i equals j, not LU[i][j], since
                    * the diagonal values are all 1 for L.
                    * */
                    if (i == j)
                    {
                        lij = 1;
                    }
                    else
                    {
                        lij = LU[i][j];
                    }
                    suml = suml + (lij * y[j]);
                }
                y[i] = b[pi[i]] - suml;
            }
            //Solve for x by using back substitution
            for (int i = n; i >= 0; i--)
            {
                sumu = 0;
                for (int j = i + 1; j <= n; j++)
                {
                    sumu = sumu + (LU[i][j] * x[j]);
                }
                x[i] = (y[i] - sumu) / LU[i][i];
            }
            return x;
        }

        public static Tuple<double[][], int[]> LUPDecomposition(double[][] A)
        {
            int n = A.Length - 1;
            /*
            * pi represents the permutation matrix.  We implement it as an array
            * whose value indicates which column the 1 would appear.  We use it to avoid 
            * dividing by zero or small numbers.
            * */
            int[] pi = new int[n + 1];
            double p = 0;
            int kp = 0;
            int pik = 0;
            int pikp = 0;
            double aki = 0;
            double akpi = 0;

            //Initialize the permutation matrix, will be the identity matrix
            for (int j = 0; j <= n; j++)
            {
                pi[j] = j;
            }

            for (int k = 0; k <= n; k++)
            {
                /*
                * In finding the permutation matrix p that avoids dividing by zero
                * we take a slightly different approach.  For numerical stability
                * We find the element with the largest 
                * absolute value of those in the current first column (column k).  If all elements in
                * the current first column are zero then the matrix is singluar and throw an
                * error.
                * */
                p = 0;
                for (int i = k; i <= n; i++)
                {
                    if (Math.Abs(A[i][k]) > p)
                    {
                        p = Math.Abs(A[i][k]);
                        kp = i;
                    }
                }
                if (p == 0)
                {
                    throw new Exception("singular matrix");
                }
                /*
                * These lines update the pivot array (which represents the pivot matrix)
                * by exchanging pi[k] and pi[kp].
                * */
                pik = pi[k];
                pikp = pi[kp];
                pi[k] = pikp;
                pi[kp] = pik;

                /*
                * Exchange rows k and kpi as determined by the pivot
                * */
                for (int i = 0; i <= n; i++)
                {
                    aki = A[k][i];
                    akpi = A[kp][i];
                    A[k][i] = akpi;
                    A[kp][i] = aki;
                }

                /*
                    * Compute the Schur complement
                    * */
                for (int i = k + 1; i <= n; i++)
                {
                    A[i][k] = A[i][k] / A[k][k];
                    for (int j = k + 1; j <= n; j++)
                    {
                        A[i][j] = A[i][j] - (A[i][k] * A[k][j]);
                    }
                }
            }
            return Tuple.Create(A, pi);
        }

        public static double[][] MatrixMultiply(double[][] A, double[][] B)
        {
            double[][] C = new double[A.Length][];
            for (int i = 0; i < A.GetLength(0); i++)
            {
                C[i] = new double[B[0].Length];
            }

            if (A[0].Length != B.Length)
            {
                throw new Exception("Incompatable Dimensions");
            }
            else
            {

                for (int i = 0; i <= A.Length - 1; i++)
                {
                    for (int j = 0; j <= B[0].Length - 1; j++)
                    {
                        C[i][j] = 0;
                        for (int k = 0; k <= A[0].Length - 1; k++)
                        {
                            C[i][j] = C[i][j] + A[i][k] * B[k][j];
                        }
                    }
                }
            }

            return C;
        }

        public static double[,] MatrixMultiply(double[,] A, double[,] B)
        {
            double[,] C = new double[A.GetLength(0), B.GetLength(0)];
            
            for (int i = 0; i <= A.GetLength(0) - 1; i++)
            {
                for (int j = 0; j <= B.GetLength(0) - 1; j++)
                {
                    C[i,j] = 0;
                    for (int k = 0; k <= A.GetLength(0) - 1; k++)
                    {
                        C[i,j] += A[i,k] * B[k,j];
                    }
                }
            }
        return C;
        }

        public static double[,] MatrixTranspose(double[,] A)
        {
            //Get the number of rows of A
            int n = A.GetLength(0);
            //Get the number of columns of A
            int m = A.GetLength(1);

            //The new matrix will be mXn
            double[,] Atranspose = new double[m,n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Atranspose[j,i] = A[i,j];
                }
            }
            return Atranspose;
        }
    }
}
