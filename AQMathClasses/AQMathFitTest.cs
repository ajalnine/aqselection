﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        #region Критерий Шапиро-Уилка

        public static int MinOfTwo(int a, int b)
        {
            return ((a) > (b) ? (b) : (a));
        }

        public static Double? ShapiroWilkW(List<object> X)
        {
            double[] c1 = { 0, .221157, -.147981, -2.07119, 4.434685, -2.706056 };
            double[] c2 = { 0, .042981, -.293762, -1.752461, 5.682633, -3.582633 };
           
            List<double> a = new List<double>();
            int j, i1, nn2;

            double ssassx, summ2, ssumm2;
            double a1, a2, an, sa, sx, xx, w1;
            double fac, asa, ssa, sax, rsn, ssx, xsx, xi;

            double w = 0;

            List<Double> x = (from xd in X.OfType<Double>() orderby xd select xd).ToList();
            int n = x.Count();
            if (n < 3) return Double.NaN;

            double range = x[n - 1] - x[0];
            for (int i = 0; i <= n; i++)a.Add(0);
            an = n;
            nn2 = n / 2;

            if (n == 3)
            {
                a[1] = .70710678;
            }
            else
            {
                summ2 = 0;
                for (int i = 1; i <= n; ++i)
                {
                    a[i] = (float)NormalZbyP((i - 0.375) / (n + 0.25)).Value;
                    summ2 += a[i] * a[i];
                }
                summ2 *= 2;
                ssumm2 = Math.Sqrt(summ2);
                rsn = 1 / Math.Sqrt(an);
                a1 = poly(c1, 6, rsn) - a[1] / ssumm2;

                /* Normalize a[] */
                if (n > 5)
                {
                    i1 = 3;
                    a2 = -a[2] / ssumm2 + poly(c2, 6, rsn);
                    fac = Math.Sqrt((summ2 - 2 * (a[1] * a[1]) - 2 * (a[2] * a[2])) / (1 - 2 * (a1 * a1) - 2 * (a2 * a2)));
                    a[2] = a2;
                }
                else
                {
                    i1 = 2;
                    fac = Math.Sqrt((summ2 - 2 * (a[1] * a[1])) / (1 - 2 * (a1 * a1)));
                }

                a[1] = a1;
                for (int i = i1; i <= nn2; ++i)
                {
                    a[i] /= -fac;
                }
            }
            xx = x[0] / range;
            sx = xx;
            sa = -a[1];
            j = n - 1;
            for (int i = 1; i < n; --j)
            {
                xi = x[i] / range;
                sx += xi;
                ++i;
                if (i != j) sa += Math.Sign(i - j) * a[MinOfTwo(i, j)];
                xx = xi;
            }

            sa /= n;
            sx /= n;
            ssa = ssx = sax = 0;
            j = n - 1;
            for (int i = 0; i < n; ++i, --j)
            {
                if (i != j)
                    asa = Math.Sign(i - j) * a[1 + MinOfTwo(i, j)] - sa;
                else
                    asa = -sa;
                xsx = x[i] / range - sx;
                ssa += asa * asa;
                ssx += xsx * xsx;
                sax += asa * xsx;
            }

            ssassx = Math.Sqrt(ssa * ssx);
            w1 = (ssassx - sax) * (ssassx + sax) / (ssa * ssx);
            w = 1 - w1;
            return w;
        }

        public static Double? ShapiroWilkP(List<object> X)
        {
            double m, s;
            double[] g = { -2.273, .459 };
            double[] c3 = { .544, -.39978, .025054, -6.714e-4 };
            double[] c4 = { 1.3822, -.77857, .062767, -.0020322 };
            double[] c5 = { -1.5861, -.31082, -.083751, .0038915 };
            double[] c6 = { -.4803, -.082676, .0030302 };

            double w = ShapiroWilkW(X).Value;
            int n = X.OfType<Double>().Count();
            double y = Math.Log(1 - w);
            double lnx = Math.Log(n);
            if (n == 3)
            {
                double pi6 = 1.90985931710274;
                double stqr = 1.04719755119660;
                double pw = pi6 * (Math.Asin(Math.Sqrt(w)) - stqr);
                if (pw < 0) pw = 0;
                return pw;
            }
            
            if (n <= 11) 
            {
        	    double gamma = poly(g, 2, n);
        	    if (y >= gamma) 
                {
                    return 1e-99;
                }
            	y = -Math.Log(gamma - y);
            	m = poly(c3, 4, n);
            	s = Math.Exp(poly(c4, 4, n));
            } 
            else 
            {
        	    m = poly(c5, 4, lnx);
        	    s = Math.Exp(poly(c6, 3, lnx));
            }
            return 1-NormalCD(y, m, s).Value;
        }
        
        static double poly(double[] cc, int nord, double x)
        {
            double p = 0;
            if (nord > 1) 
            {
        	    p = x * cc[nord-1];
        	    for (int j = nord - 2; j > 0; j--)
                {
                    p = (p + cc[j]) * x;
                }
            }
        	return cc[0] + p;
        }

        #endregion

        #region Критерий Андерсона-Дарлинга

        public static double AndersonDarlingA(List<object> X)
        {
            List<Double> x = (from xd in X.OfType<Double>() orderby xd select xd).ToList();
            int n = x.Count();
            double Mean = AggregateMean(X).Value;
            double StDev = AggregateStDev(X).Value;

            double a = 0;
            for (int i = 1; i <= n; i++)
            {
                double f1 = NormalCD(x[i - 1], Mean, StDev).Value;
                double f2 = NormalCD(x[n - i], Mean, StDev).Value;
                if (f2 == 1 || f1 == 0) continue;
                double currentA = ((2 * (double)i - 1) / n) * (Math.Log(f1) + Math.Log(1 - f2));
                a += currentA;
            }
            a = -n - a;
            double a2 = a * (1 + 0.75 / n + 2.25 / ((double)n * n));

            return a2;
        }

        public static double AndersonDarlingP(List<object> X)
        {
            double a = AndersonDarlingA(X);

            if (a < 13 && a >= 0.6) return Math.Exp(1.2937 - 5.709 * a + 0.0186 * a * a);
            if (a < 0.6 && a >= 0.34) return Math.Exp(0.9177 - 4.279 * a - 1.38 * a * a);
            if (a < 0.34 && a >= 0.2) return 1 - Math.Exp(-8.318 + 42.796 * a - 59.938 * a * a);
            return 1 - Math.Exp(-13.436 + 101.14 * a - 223.73 * a * a);
        }

        #endregion

        #region Критерий Колмогорова

        public static double KolmogorovD(List<object> X)
        {
            List<Double> x = (from xd in X.OfType<Double>() orderby xd select xd).ToList();
            double n = x.Count();
            List<Double> Diff = new List<double>();
            double Mean = AggregateMean(X).Value;
            double StDev = AggregateStDev(X).Value;

            for (double i = 1; i <= n; i++)
            {
                double f = NormalCD(x[(int)i-1], Mean, StDev).Value;
                Diff.Add(Math.Abs(i / n - f));
                Diff.Add(Math.Abs((i-1) / n - f));
            }

            return Diff.Max();
        }

        
        private static List<double> MatrixMultiply(List<double> A, List<double> B)
        { 
            double s;
            List<double> C = new List<double>();
            int m = (int)Math.Sqrt((double)A.Count());

            for(int i = 0; i < m; i++)
            {
                for(int j = 0; j < m; j++)
                {
                    s=0;
                    for(int k = 0; k < m; k++)
                    {
                        s+= A[i*m+k] * B[k*m+j]; 
                    }
                    C.Add(s);
                }
            }
            return C;
        }

        private static List<double> MatrixPower(List<double> A, int n)
        {
            List<double> B = (from a in A select a).ToList();
            if (n == 1) return B;

            List<double> C = MatrixPower(B, n / 2);
            List<double> D = MatrixMultiply(C, C);
            if (n % 2 == 1)
            {
                return MatrixMultiply(B, D);
            }
            else
            {
                return D;
            }
        }
       
        public static double KolmogorovP(List<object> X)
        {
            List<Double> x = (from xd in X.OfType<Double>() orderby xd select xd).ToList();
            int n = x.Count();
            double d = KolmogorovD(X);
            return 1-K(n,d);
        }

        public static bool LillieforsTest(List<object> X)
        {
            List<Double> x = (from xd in X.OfType<Double>() orderby xd select xd).ToList();
            int n = x.Count();
            double d = KolmogorovD(X);
            return 0.886 / Math.Sqrt((double)n) > d;
        }

        private static double K(int n, double d)
        {
            double s = d * d * n;
            if (s > 7.24 || (s > 3.76 && n > 99)) return 1 - 2 * Math.Exp(-(2.000071 + .331 / Math.Sqrt(n) + 1.409 / n) * s);
            int k = (int)((double)n * d) + 1;
            int m = 2 * k - 1;
            double h = (double)k - (double)n * d;

            List<double> H = new List<double>();
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    H.Add((i - j + 1 < 0) ? 0 : 1);
                }
            }

            for (int i = 0; i < m; i++)
            {
                H[i * m] -= Math.Pow(h, i + 1);
                H[(m - 1) * m + i] -= Math.Pow(h, (m - i));
            }

            H[(m - 1) * m] += (2 * h - 1 > 0) ? Math.Pow(2 * h - 1, m) : 0;

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (i - j + 1 > 0)
                    {
                        for (int g = 1; g <= i - j + 1; g++)
                        {
                            H[i * m + j] /= g;
                        }
                    }
                }
            }
            List<double> X = MatrixPower(H, n);
            s = X[(k - 1) * m + k - 1];
            for (int i = 1; i <= n; i++)
            {
                s *= (double)i/(double)n;
            }
            return s;
        }
        #endregion
    }
}