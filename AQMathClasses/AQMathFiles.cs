﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        public static string SearchFile(string rootPath, string startsWith, string extention)
        {
            try {
                var found = Directory.EnumerateFiles(rootPath, startsWith + @"*." + extention, SearchOption.AllDirectories).FirstOrDefault();
                return found;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
