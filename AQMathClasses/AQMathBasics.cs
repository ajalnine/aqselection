﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        static double OldNormalRND;
        static bool UseLastRND = false;

        public static Double? Sinh(Double? a)
        {
            if (!a.HasValue) return null;
            double x = a.Value;
            return 0.5 * (Math.Exp(x) - Math.Exp(-x));
        }

        public static Double? Cosh(Double? a)
        {
            if (!a.HasValue) return null;
            double x = a.Value;
            return 0.5 * (Math.Exp(x) + Math.Exp(-x));
        }

        public static Double? Tanh(Double? a)
        {
            if (!a.HasValue) return null;
            double x = a.Value;
            return Sinh(x)/Cosh(x);
        }

        public static Double? Coth(Double? a)
        {
            if (!a.HasValue) return null;
            double x = a.Value;
            return 1/Tanh(x);
        }

        public static Double Rnd()
        {
            return Rand.NextDouble();
        }

        public static Double? RndNormal(Double? Mean, Double? Stdev)
        {
            if (!Mean.HasValue || !Stdev.HasValue) return null;
            double x1, x2, w, y1;

            if (UseLastRND)		       
	        {
		        y1 = OldNormalRND;
		        UseLastRND = false;
	        }
	        else
	        {
		        do {
			        x1 = 2.0 * Rnd() - 1.0;
			        x2 = 2.0 * Rnd() - 1.0;
			        w = x1 * x1 + x2 * x2;
		        } while ( w >= 1.0 );

		        w = Math.Sqrt( (-2.0 * Math.Log( w, Math.E ) ) / w );
		        y1 = x1 * w;
                OldNormalRND = x2 * w;
                UseLastRND = true;
	        }

	        return( Mean.Value + y1 * Stdev.Value );
        }

        public static Double? Abs(Double? a) { return (a.HasValue) ? (Double?)Math.Abs(a.Value) : null; }
        public static Double? Acos(Double? a) { return (a.HasValue) ? (Double?)Math.Acos(a.Value) : null; }
        public static Double? Asin(Double? a) { return (a.HasValue) ? (Double?)Math.Asin(a.Value) : null; }
        public static Double? Atan(Double? a) { return (a.HasValue) ? (Double?)Math.Atan(a.Value) : null; }
        public static Double? Cos(Double? a) { return (a.HasValue) ? (Double?)Math.Cos(a.Value) : null; }
        public static Double? PI(Double? a) { return Math.PI; }
        public static Double? E(Double? a) { return Math.E; }
        public static Double? Exp(Double? a) { return (a.HasValue) ? (Double?)Math.Exp(a.Value) : null; }
        public static Double? Floor(Double? a) { return (a.HasValue) ? (Double?)Math.Floor(a.Value) : null; }
        public static Double? Ceiling(Double? a) { return (a.HasValue) ? (Double?)Math.Ceiling(a.Value) : null; }
        public static Double? Log10(Double? a) { return (a.HasValue) ? (Double?)Math.Log10(a.Value) : null; }
        public static Double? Sign(Double? a) { return (a.HasValue) ? (Double?)Math.Sign(a.Value) : null; }
        public static Double? Sin(Double? a) { return (a.HasValue) ? (Double?)Math.Sin(a.Value) : null; }
        public static Double? Sqrt(Double? a) { return (a.HasValue) ? (Double?)Math.Sqrt(a.Value) : null; }
        public static Double? Tan(Double? a) { return (a.HasValue) ? (Double?)Math.Tan(a.Value) : null; }
        public static Double? IEEEReminder(Double? a, Double? b) { return (a.HasValue && b.HasValue) ? (Double?)Math.IEEERemainder(a.Value, b.Value) : null; }
        public static Double? Log(Double? a, Double? b) { return (a.HasValue && b.HasValue) ? (Double?)Math.Log(a.Value, b.Value) : null; }
        public static Double? Pow(Double? a, Double? b) 
        { 
            if (!a.HasValue || !b.HasValue) return null;
            if (a.Value==0) return 0;
            return Math.Pow(a.Value, b.Value); 
        }

        public static Double? Round(Double? a, Int32? b) { return (a.HasValue && b.HasValue) ? (Double?)Math.Round(a.Value, (int)b.Value) : null; }
        public static Double? Round(Double? a, Double b) { return (a.HasValue) ? (Double?)Math.Round(a.Value, (int)b) : null; }

        public static Double? FloorToList(Double? a, List<object> list)
        {
            if (!a.HasValue) return null;
            return list.OfType<double>().Where(v => v <= a.Value).Max();
        }

        public static Double? CeilingToList(Double? a, List<object> list)
        {
            if (!a.HasValue) return null;
            var result = list.OfType<double>().Where(v => v >= a.Value);
            return result.Any() ? (double?)result.Min() : null;
        }

        public static Double? CeilingToList(Double? a, params object[] list)
        {
            if (!a.HasValue) return null;
            var result = list.OfType<double>().Where(v => v >= a.Value);
            return result.Any() ? (double?)result.Min() : null;
        }

        public static Double? SmartRound(Double? value, int level)
        {
            if (!value.HasValue || double.IsNaN(value.Value) || double.IsInfinity(value.Value)) return null;
            var v = Math.Abs(value.Value);
            var log = Math.Log10(v);
            return log < 0 
                ? Math.Sign(value.Value) * Math.Round(v, (int) (-log + level <=15 ? (-log + level) : 15)) 
                : Math.Sign(value.Value) * Math.Round(v, log > level 
                ? log - level<=15 ? (int)(log - level): 15 
                : level - log<=15 ? (int)(level - log): 15);
        }
    }
}