﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        public static DateTime? Today()
        {
            return DateTime.Now - DateTime.Now.TimeOfDay;
        }

        public static DateTime? Now()
        {
            return DateTime.Now;
        }

        public static DateTime? Date(object o)
        {
            if (o == null) return null;
            DateTime dt;
            if (DateTime.TryParse(o.ToString(), out dt)) return dt;
            else return null;
        }

        public static TimeSpan? Time(object o)
        {
            if (o == null) return null;
            TimeSpan ts;
            if (TimeSpan.TryParse(o.ToString(), out ts)) return ts;
            else return null;
        }

        public static TimeSpan? Second()
        {
            return new TimeSpan(0,0,1);
        }

        public static TimeSpan? Minute()
        {
            return new TimeSpan(0, 1, 0);
        }

        public static TimeSpan? Hour()
        {
            return new TimeSpan(1, 0, 0);
        }

        public static TimeSpan? Day()
        {
            return new TimeSpan(1, 0, 0, 0);
        }

        public static double? DayOfYear(DateTime? o)
        {
            return o?.DayOfYear;
        }

        public static double? DayOfMonth(DateTime? o)
        {
            return o?.Day;
        }

        public static double? Month(DateTime? o)
        {
            return o?.Month;
        }

        public static double? Year(DateTime? o)
        {
            return o?.Year;
        }

        public static int? WeekOfYear(DateTime? o)
        {
            if (!(o is DateTime?)) return null;

            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;

            return cal.GetWeekOfYear(o.Value, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
        }

        public static string DayOfWeek(DateTime? o)
        {
            if (o as DateTime? == null) return null;
            return Enum.GetName(typeof(DayOfWeek), (o as DateTime?).Value.DayOfWeek);
        }

        public static Double? DateDifferenceInMinutes(DateTime? From, DateTime? To)
        {
            if (From as DateTime? == null || To as DateTime? == null) return null;
            return (To.Value - From.Value).TotalMinutes;
        }

        public static Double? DateDifferenceInSeconds(DateTime? From, DateTime? To)
        {
            if (From as DateTime? == null || To as DateTime? == null) return null;
            return (To.Value - From.Value).TotalSeconds;
        }

        public static Double? DateDifferenceInHours(DateTime? From, DateTime? To)
        {
            if (From as DateTime? == null || To as DateTime? == null) return null;
            return (To.Value - From.Value).TotalHours;
        }

        public static Double? DateDifferenceInDays(DateTime? From, DateTime? To)
        {
            if (From as DateTime? == null || To as DateTime? == null) return null;
            return (To.Value - From.Value).TotalDays;
        }

        public static string MinutesToTimeString(Double? Minutes)
        {
            if(!Minutes.HasValue)return null;
            int MinutesPart = (int)Math.Floor(Minutes.Value);
            int SecondsPart = (int)((Minutes.Value - MinutesPart)*60);
            TimeSpan ts = new TimeSpan(0, 0, MinutesPart, SecondsPart);
            return ts.ToString();
        }

        public static string DateTimeToDateString(DateTime? o)
        {
            if (o as DateTime? == null) return null;
            return o.Value.ToString("dd.MM.yyyy");
        }

        public static string DateTimeToFormattedString(DateTime? o, string format)
        {
            if (o as DateTime? == null) return null;
            return o.Value.ToString(format);
        }

        public static string DateTimeToDateString(DateTime? o, string format)
        {
            if (o as DateTime? == null) return null;
            return o.Value.ToString(format);
        }

        public static string DateTimeToTimeString(DateTime? o)
        {
            if (o as DateTime? == null) return null;
            return o.Value.TimeOfDay.ToString();
        }

        public static double? GetBrigadeNumberByDateTime(DateTime? o)
        {
            if (o as DateTime? == null) return null;
            DateTime Base = new DateTime(2010, 1, 1, 8, 0, 0);
            TimeSpan Difference = o.Value - Base;
            if (Difference.Ticks < 0) return null;
            double Variant = (Math.Floor( Difference.TotalDays) % 4) * 2 + ((Difference.Hours < 12) ? 0 : 1);
            double[] Bridgades = new double[] { 4, 2, 1, 4, 3, 1, 2, 3 };
            return Bridgades[(int)Variant];
        }

        public static DateTime? ParseDateTimeDMY(string o)
        {
            return ParseDateTimeFormatted(o, "dd.MM.yyyy HH:mm:ss");
        }

        public static DateTime? ParseDateTimeYMD(string o)
        {
            return ParseDateTimeFormatted(o, @"yyyy/MM/dd HH:mm:ss");
        }

        public static DateTime? RoundDateTimeToFloor(DateTime? o, string part)
        {
            if (string.IsNullOrEmpty(part)) return o;
            if (o == null) return null;
            var d = o.Value;
            var c = part[0];
            switch (c)
            {
                case 's':
                    return new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
                case 'm':
                    return new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, 0);
                case 'H':
                    return new DateTime(d.Year, d.Month, d.Day, d.Hour, 0, 0);
                case 'd':
                    return new DateTime(d.Year, d.Month, d.Day);
                case 'M':
                    return new DateTime(d.Year, d.Month, 1);
                case 'Y':
                case 'y':
                    return new DateTime(d.Year, 1, 1);
            }
            return o;
        }

        public static DateTime? ParseDateTimeFormatted(string o, string f)
        {
            if (o as string == null) return null;
            DateTime Result = DateTime.Now;
            if (DateTime.TryParseExact(o, f, new CultureInfo("ru-RU"), System.Globalization.DateTimeStyles.AllowWhiteSpaces, out Result)) return Result;
            else return null;
        }

        public static DateTime? ParseDateTimeAuto(string o)
        {
            if (o as string == null) return null;
            DateTime Result = DateTime.Now;
            string[] Formats1 = new string[] { "dd.MM.yyyy HH:mm:ss", "dd.MM.yyyy HH:mm", "dd.MM.yyyy", "d.MM.yyyy HH:mm:ss", "dd.M.yyyy HH:mm:ss", "d.M.yyyy HH:mm:ss"};
            string[] Formats2 = new string[] { @"yyyy-MM-dd HH:mm:ss", @"yyyy-MM-dd HH:mm", @"yyyy-M-dd HH:mm:ss", @"yyyy-MM-d HH:mm:ss", @"yyyy-M-d HH:mm:ss", @"yyyy-MM-dd" };
            if (DateTime.TryParseExact(o, Formats1, new CultureInfo("ru-RU"), System.Globalization.DateTimeStyles.AllowWhiteSpaces, out Result)) return Result;
            else if (DateTime.TryParseExact(o, Formats2, new CultureInfo("ru-RU"), System.Globalization.DateTimeStyles.AllowWhiteSpaces, out Result)) return Result;
            else return null;
        }
    }
}