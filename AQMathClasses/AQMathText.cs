﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        public static string Text(object o)
        {
            if (o==null)return null;
            return o.ToString();
        }

        public static double? ParseNumber(object o)
        {
            if (o == null) return null;
            double result;
            if (double.TryParse(o.ToString().Replace('.', ','), out result)) return result;
            else if (double.TryParse(o.ToString(), out result)) return result;
            else return null;
        }
        
        public static bool? ContainsSubstring(object o, string s)
        {
            if (o == null || s == null) return null;
            return o.ToString().Contains(s);
        }

        public static bool? ContainsSubstringCI(object o, string s)
        {
            if (o == null || s == null) return null;
            return o.ToString().ToLower().Contains(s.ToLower());
        }

        public static bool ContainsSubstringAndNonEmptyCI(object o, string s)
        {
            if (o == null || s == null) return false;
            return o.ToString().ToLower().Contains(s.ToLower());
        }

        public static bool? ContainsSubstringOrEmptyCI(object o, string s)
        {
            if (o == null || s == null || string.IsNullOrWhiteSpace(o.ToString())) return true;
            return o.ToString().ToLower().Contains(s.ToLower());
        }

        public static List<object> SplitText(string data, string Divider)
        {
            if (data == null) return null;
            if (data == String.Empty) return new List<object>();
            string[] Div = new string[] { Divider };
            return (from d in data.Split(Div, StringSplitOptions.RemoveEmptyEntries) select d.Trim()).Cast<object>().ToList();
        }

        public static string SQLFilterFromStrings(string data)
        {
            List<object> l = ListFromStrings(data);
            if (l == null || l.Count == 0) return String.Empty;
            bool IsFirst = true;
            string Result = "(";
            foreach (var v in l)
            {
                if (!IsFirst) Result += ",";
                Result += "'" + v.ToString().Replace("'", "''") + "'";
                IsFirst = false;
            }
            Result += ")";
            return Result;
        }
        public static string Concatenate(List<object> data, string divider)
        {
            var r = string.Empty;
            if (data == null) return null;
            for (var index = 0; index < data.Count; index++)
            {
                r += data[index].ToString();
                if (index<data.Count-1) r += divider;
            }
            return r;
        }

        public static string ConcatenateDistinct(List<object> data, string divider)
        {
            var r = string.Empty;
            if (data == null) return null;
            var distinctData = data.Distinct().OrderBy(a=>a).ToList();
            for (var index = 0; index < distinctData.Count; index++)
            {
                if (string.IsNullOrWhiteSpace(distinctData[index].ToString())) continue;
                r += distinctData[index].ToString();
                if (index < distinctData.Count - 1) r += divider;
            }
            return r;
        }

        public static string Substring(string arg, double? from, double? length)
        {
            if (arg == null) return null;
            if (!from.HasValue) from = 0;
            if (from > arg.Length - 1 || from < 0 || length < 0) return string.Empty;
            if (!length.HasValue || from + length > arg.Length) length = arg.Length - from;
            return arg.Substring((int)from.Value, (int)length.Value);
        }

        public static string Last(string arg, double? length)
        {
            if (arg == null) return null;
            if (!length.HasValue) length = arg.Length;
            if (length > arg.Length - 1 || length < 0) return arg;
            return arg.Substring(arg.Length - (int)length.Value, (int)length.Value);
        }

        public static string Replace(string arg, string toFind, string toReplace)
        {
            return arg?.Replace(toFind, toReplace);
        }

        public static string Trim(string arg)
        {
            return arg?.Trim();
        }

        public static string First(string arg, double? length)
        {
            if (arg == null) return null;
            if (!length.HasValue) length = arg.Length;
            if (length > arg.Length - 1 || length < 0) return arg;
            return arg.Substring(0, (int)length.Value);
        }

        public static string Skip(string arg, double? length)
        {
            if (arg == null) return null;
            if (!length.HasValue) length = arg.Length;
            if (length > arg.Length - 1 || length < 0) return string.Empty;
            return arg.Substring((int)length.Value, arg.Length - (int)length.Value);
        }

        public static string Truncate(string arg, double? length)
        {
            if (arg == null) return null;
            if (!length.HasValue) length = arg.Length;
            if (length > arg.Length - 1 || length < 0) return string.Empty;
            return arg.Substring(0, arg.Length - (int)length.Value);
        }

        public static string SQLFilterFromMeltList(string List, string Field)
        {
            MatchCollection mc = Regex.Matches(List + ";", @"([\w]*?)([\d]*)(-([\d]*))?[^\w-\n]+");
            string Oven = string.Empty;
            string Number = string.Empty;
            string EndRange = string.Empty;
            string OutputDiscreteFilter = " (" + Field + " in (";
            string OutputRangeFilter = "(";

            bool IsFirstDiscrete = true;
            bool IsFirstRange = true;

            foreach (Match m in mc)
            {
                string CurrentOven = m.Groups[1].Success ? m.Groups[1].Captures[0].Value : string.Empty;
                if (CurrentOven != string.Empty) Oven = CurrentOven;

                string CurrentNumber = m.Groups[2].Success ? m.Groups[2].Captures[0].Value : string.Empty;
                if (CurrentNumber != string.Empty)
                {
                    if (Number.Length >= CurrentNumber.Length)
                    {
                        Number = Number.Substring(0, Number.Length - CurrentNumber.Length) + CurrentNumber;
                    }
                    else Number = CurrentNumber;
                }

                string CurrentEndRange = m.Groups[4].Success ? m.Groups[4].Captures[0].Value : string.Empty;
                if (CurrentEndRange != string.Empty)
                {
                    if (Number.Length >= CurrentEndRange.Length)
                    {
                        EndRange = Number.Substring(0, Number.Length - CurrentEndRange.Length) + CurrentEndRange;
                    }
                    else EndRange = CurrentEndRange;
                }
                else EndRange = string.Empty;

                if (Oven != string.Empty && Number != string.Empty)
                {
                    if (EndRange != string.Empty)
                    {

                        if (int.Parse(EndRange) > int.Parse(Number))
                        {
                            if (!IsFirstRange) OutputRangeFilter += " OR ";
                            string CurrentItemFilter = "((dbo.ToNumeric(substring(" + Field + "," + (Oven.Length + 1).ToString() + ", LEN(" + Field + "))) between " + Number.ToString() + " and " + EndRange.ToString() + @")
                                and substring(" + Field + ",1," + Oven.Length.ToString() + ")='" + Oven + "')";

                            IsFirstRange = false;
                            OutputRangeFilter += CurrentItemFilter;
                        }
                    }
                    else
                    {
                        if (!IsFirstDiscrete) OutputDiscreteFilter += ",";
                        IsFirstDiscrete = false;
                        OutputDiscreteFilter += "'" + Oven + Number + "'";
                    }
                }
            }
            OutputRangeFilter += ")";
            OutputDiscreteFilter += "))";
            string And = (!IsFirstDiscrete && !IsFirstRange) ? string.Empty : " and ";
            if (IsFirstRange)
            {
                if (IsFirstDiscrete) return string.Empty;
                else return And + OutputDiscreteFilter;
            }
            else
            {
                if (IsFirstDiscrete) return And + OutputRangeFilter;
                else return And + " (" + OutputRangeFilter + " OR " + OutputDiscreteFilter + ")";
            }
        }
    }
}