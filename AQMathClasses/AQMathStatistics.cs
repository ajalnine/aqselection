﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        public static Double? Moment2(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;

            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            Double? Result = null;
            if (Avg == null) return null;
            if (Cnt == 0) return 0;
            foreach (Double dr in m)
            {
                double a;
                if (double.TryParse(dr.ToString(), out a))
                {
                    if (Result == null)
                    {
                        Result = (Double?)Pow((double)a - (double)Avg.Value, 2);

                    }
                    else
                    {
                        Result += (Double?)Pow((double)a - (double)Avg.Value, 2);
                    }
                }
            }
            return Result;
        }
        
        public static Double? Moment3(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;

            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            Double? Result = null;
            if (Avg == null) return null;
            if (Cnt == 0) return 0;

            foreach (Double dr in m)
            {
                double a;
                if (double.TryParse(dr.ToString(), out a))
                {
                    if (Result == null)
                    {
                        Result = (Double?)Pow((double)a - (double)Avg.Value, 3);
                    }
                    else
                    {
                        Result += (Double?)Pow((double)a - (double)Avg.Value, 3);
                    }
                }
            }
            
            return Result;
        }

        public static Double? Moment4(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;

            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            Double? Result = null;
            if (Avg == null) return null;
            if (Cnt == 0) return 0;
            foreach (Double dr in m)
            {
                double a;
                if (double.TryParse(dr.ToString(), out a))
                {
                    if (Result == null)
                    {
                        Result = (Double?)Pow((double)a - (double)Avg.Value, 4);

                    }
                    else
                    {
                        Result += (Double?)Pow((double)a - (double)Avg.Value, 4);
                    }
                }
            }
            return Result;
        }

        public static Double? Kurtosis(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            Double? m2 = Moment2(data);
            if (!m2.HasValue) return null;
            Double? m4 = Moment4(data);
            if (!m4.HasValue) return null;
            int n = AggregateCount(data);
            Double? stdev = AggregateStDev(data);
            if (!stdev.HasValue) return null;
            return  (n * (n + 1) * m4.Value - 3 * m2.Value * m2.Value * (n - 1)) / ((n - 1) * (n - 2) * (n - 3) * Math.Pow(stdev.Value, 4 ));
        }

        public static Double? Skewness(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;

            Double? stdev = AggregateStDev(data);
            if (!stdev.HasValue) return null;
            Double? m2 = Moment2(data);
            if (!m2.HasValue) return null;
            Double? m3 = Moment3(data);
            if (!m3.HasValue) return null;
            int n = AggregateCount(data);
            return n * m3 / ((n - 1) * (n - 2) * Math.Pow(stdev.Value, 3 ));
        }

        public static Double? SkewnessStdErr(List<object> data)
        {
            double n = (double)AggregateCount(data);
            return Math.Sqrt((6 * n* (n - 1)) / ((n - 2) * (n + 1) * (n + 3)));
        }

        public static Double? KurtosisStdErr(List<object> data)
        {
            Double? SES = SkewnessStdErr(data);
            if (!SES.HasValue) return null;
            double n = (double)AggregateCount(data);
            return 2 * SES.Value * Math.Sqrt((n*n-1)/((n-3)*(n+5)));
        }

        public static int AggregateValidCases(params List<object>[] data)
        {
            if (data == null) return 0;
            int AllNumber = data[0].Count();
            int Count = 0;
            for (int i = 0; i < AllNumber; i++)
            {
                bool AllHasValues = true;
                foreach (var d in data)
                {
                    if (d[i] == null || d[i] == DBNull.Value)
                    {
                        AllHasValues = false;
                        break;
                    }
                }
                if (AllHasValues) Count++;
            }
            return Count;
        }

        public static Double? Gamma(Double? X)
        {
            if (!X.HasValue)return null;
            double x = X.Value;
            
            double[] a = {676.5203681218835, -1259.139216722289, 771.3234287757674, -176.6150291498386, 12.50734324009056, -0.1385710331296526, 0.9934937113930748e-05, 0.1659470187408462e-06};
        	double lnsqrt2pi = 0.9189385332046727;
            if (x < 0 ) return Double.NaN;
        	double lngamma = 0;
        	double tmp = x + 7;
        	
            foreach (double d in a.Reverse())
            {
                lngamma += d/tmp;
                tmp--;
            }
            lngamma += 0.9999999999995183;
            Double? result = Math.Exp(Math.Log(lngamma) + lnsqrt2pi - (x + 6.5) + (x - 0.5) * Math.Log(x + 6.5));
         	return result;
        }

        public static Double? GammaIncomplete(Double? K, Double? X)
        {
            if (!X.HasValue || !K.HasValue) return null;
            double y = X.Value;
            double p = K.Value;
            double Precision = 1e-9;

            double igamma = 0;
            if(y < 0 || p < 0 ) return Double.NaN;
            double arg = p*Math.Log(y)- Math.Log(Gamma(p+1).Value)-y;
            double f = Math.Exp(arg);
            if(f==0) return 1;
            double c = 1;
            igamma = 1;
            double a = p;
            while(c/igamma > Precision)
            {
                a++;
                c *= y / a;
                igamma +=c;
            }
            igamma *=f;
            return igamma;
        }

        public static Double? Beta(Double? A, Double? B)
        {
            if (!A.HasValue || !B.HasValue) return null;
            double a = A.Value;
            double b = B.Value;
            if (a < 100 && b < 100)
            {
                double? Ga = Gamma(a);
                double? Gb = Gamma(b);
                double? Gab = Gamma(a + b);
                return Ga*Gb/Gab;
            }
            var sum = 0d;
            var step = Math.PI/2/100;
            for (double i = 0; i <= Math.PI/2; i += step)
            {
                var v = Math.Pow(Math.Sin(i), 2*b - 1)*Math.Pow(Math.Cos(i), 2*a - 1);
                sum += v * step;
            }
            return 2 * sum;
        }
      
        public static Double? BetaIncomplete(Double? p, Double? q, Double? x)
        {
            if (!p.HasValue || !q.HasValue || !x.HasValue) return null;
            if (double.IsNaN(x.Value))return null;

            double beta = Beta(q, p).Value;
            double betalog = Math.Log(beta);

            double precision = 1e-7;
            bool indx;
            int ns;
            double pp, psq, qq, rx, temp, term, xx, ai, betain, cx;

            betain = x.Value;
            if ( p <= 0 || q <= 0 ) return Double.NaN;
            if ( x <= 0 || x > 1 ) return Double.NaN;
            if ( x==0 || x==1) return betain;
            
            psq = p.Value + q.Value;
            cx = 1 - x.Value;
            if ( p < psq * x )
            {
                xx = cx;
                cx = x.Value;
                pp = q.Value;
                qq = p.Value;
                indx = true;
            }
            else
            {
                xx = x.Value;
                pp = p.Value;
                qq = q.Value;
                indx = false;
            }
            
            term = 1;
            ai = 1;
            betain = 1;
            ns = (int)( qq + cx * psq);
            rx = xx / cx;
            temp = qq - ai;
            if ( ns == 0 ) rx = xx;
            while(true)
            {
                term = term * temp * rx / ( pp + ai );
                betain = betain + term;
                temp = Math.Abs( term );
                if (temp <= precision && temp <= precision * betain)
                {
                    betain = betain * Math.Exp ( pp * Math.Log (xx) + ( qq - 1) * Math.Log (cx) - betalog ) / pp;
                    if ( indx ) betain = 1 - betain;
                    return betain;
                }
                ai++;
                ns--;

                if ( ns >= 0)
                {
                    temp = qq - ai;
                    if ( ns == 0 ) rx=xx;
                }
                else
                {
                    temp = psq;
                    psq++;
                }
            }
        }

        public static Double? Factorial(Double? a)
        {
            if (!a.HasValue) return null;
            double x = a.Value;
            return Gamma(x + 1);
        }
        
        public static Double? Erf(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            double c = .564189583547756;
            double[] a = { 7.7105849500132e-5,-.00133733772997339, .0323076579225834,.0479137145607681,.128379167095513 };
            double[] b = { .00301048631703895,.0538971687740286, .375795757275549 };
            double[] p = { -1.36864857382717e-7,.564195517478974, 7.21175825088309,43.1622272220567,152.98928504694, 339.320816734344,451.918953711873,300.459261020162 };
            double[] q = { 1,12.7827273196294,77.0001529352295, 277.585444743988,638.980264465631,931.35409485061, 790.950925327898,300.459260956983 };
            double[] r = { 2.10144126479064,26.2370141675169, 21.3688200555087,4.6580782871847,.282094791773523 };
            double[] s = { 94.153775055546,187.11481179959,  99.0191814623914,18.0124575948747 };

            double ret_val;
            double t, x2, ax, bot, top;

            ax = Math.Abs(x);
            if (ax <= 0.5) 
            {
	            t = x * x;
	            top = (((a[0] * t + a[1]) * t + a[2]) * t + a[3]) * t + a[4] + 1.0;
	            bot = ((b[0] * t + b[1]) * t + b[2]) * t + 1.0;
            	return x * (top / bot);
            }
    
            if (ax <= 4)
            { 
                top = ((((((p[0] * ax + p[1]) * ax + p[2]) * ax + p[3]) * ax + p[4]) * ax + p[5]) * ax + p[6]) * ax + p[7];
	            bot = ((((((q[0] * ax + q[1]) * ax + q[2]) * ax + q[3]) * ax + q[4]) * ax + q[5]) * ax + q[6]) * ax + q[7];
	            ret_val = 0.5 - Math.Exp(-x * x) * top / bot + 0.5;
	            if (x < 0.0) return -ret_val;
	            else return ret_val;
            }

            if (ax >= 5.8) 
            {
	            return x > 0 ? 1 : -1;
            }

            x2 = x * x;
            t = 1.0 / x2;
            top = (((r[0] * t + r[1]) * t + r[2]) * t + r[3]) * t + r[4];
            bot = (((s[0] * t + s[1]) * t + s[2]) * t + s[3]) * t + 1.0;
            t = (c - top / (x2 * bot)) / ax;
            ret_val = 0.5 - Math.Exp(-x2) * t + 0.5;
            if (x < 0.0) return -ret_val;
            else return ret_val;
        }

        public static Double? OperativeForRegression(double? x, double? stDevOfResiduals, double? mean, double? stDevOfResult, double? ch)
        {
            if (!x.HasValue || !stDevOfResiduals.HasValue || !mean.HasValue || !stDevOfResult.HasValue || !ch.HasValue)
                return null;
            var k = Math.Pow(stDevOfResiduals.Value / stDevOfResult.Value, 2);
            var disp = x + k*mean.Value - ch.Value*(1 + k);
            var div = stDevOfResiduals.Value*Math.Sqrt(1 + k);
            var m = disp/div;
            return NormalCD(m,0,1);
        }

        public static Double? CustomerRiskForRegressionLeft(double? stDevOfResiduals, double? meanOfResult, double? stDevOfResult, double? mean, double? stdev, double? ch)
        {
            if (!stDevOfResiduals.HasValue || !meanOfResult.HasValue || !stDevOfResult.HasValue || !ch.HasValue) return null;
            var res = 0.0d;
            var n= 1000.0d;
            var minusinf = ch.Value - stdev.Value*20;
            var step = (ch.Value - minusinf)/n;
            var prev = 0.0d;

            for (var i = minusinf; i <= ch.Value; i += step)
            {
                var a1 = OperativeForRegression(i, stDevOfResiduals, mean, stDevOfResult, ch).Value;
                var b1 = NormalPD(i, mean, stdev).Value;
                var q = b1 * a1;
                res += Math.Abs((q + prev) /2 * (step));
                prev = q;
            }
            return res;
        }

        public static Double? CustomerRiskForRegressionRight(double? stDevOfResiduals, double? meanOfResult, double? stDevOfResult, double? mean, double? stdev, double? ch)
        {
            if (!stDevOfResiduals.HasValue || !meanOfResult.HasValue || !stDevOfResult.HasValue || !ch.HasValue) return null;

            var res = 0.0d;
            var n = 1000.0d;
            var plusinf = ch.Value + stdev.Value * 20;
            var step = (plusinf - ch.Value) / n;
            var prev = 0.0d;

            for (var i = ch.Value; i <= plusinf; i += step)
            {
                var a1 = OperativeForRegression(i, stDevOfResiduals, mean, stDevOfResult, ch).Value;
                var b1 = NormalPD(i, mean, stdev).Value;
                var q = b1 * a1;
                res += Math.Abs((q + prev) / 2 * (step));
                prev = q;
            }
            return res;
        }

        public static Double? OperativeForP2P(double? z, double? s0, double? ch, double? samples)
        {
            if (!z.HasValue || !s0.HasValue || !ch.HasValue || !samples.HasValue)
                return null;
            var m = (z-ch) / s0;
            var f = NormalCD(m, 0, 1);
            if (!f.HasValue) return null;
            return Math.Pow(f.Value, samples.Value)*(1 + Math.Pow(f.Value, samples.Value) - Math.Pow(f.Value, samples.Value * 2));
        }

        public static Double? CustomerRiskForP2PLeft(double? s0, double? mean, double? stdev, double? ch, double? samples)
        {
            if (!s0.HasValue || !ch.HasValue || !samples.HasValue) return null;
            var res = 0.0d;
            var n = 1000.0d;
            var minusinf = ch.Value - s0.Value * 20;
            var step = (ch.Value - minusinf) / n;
            var prev = 0.0d;

            for (var i = minusinf; i <= ch.Value; i += step)
            {
                var a1 = OperativeForP2P(i, s0, ch, samples).Value;
                var b1 = NormalPD(i, mean, stdev).Value;
                var q = b1 * a1;
                res += Math.Abs((q + prev) / 2 * (step));
                prev = q;
            }
            return res;
        }
        
        public static Double? CustomerRiskForP2PRight(double? s0, double? mean, double? stdev, double? ch, double? samples)
        {
            if (!s0.HasValue || !ch.HasValue || !samples.HasValue) return null;
            var res = 0.0d;
            var n = 1000.0d;
            var plusinf = ch.Value + stdev.Value * 20;
            var step = (plusinf - ch.Value) / n;
            var prev = 0.0d;

            for (var i = ch.Value; i <= plusinf; i += step)
            {
                var a1 = OperativeForP2P(i, s0, ch, samples).Value;
                var b1 = NormalPD(i, mean, stdev).Value;
                var q = b1 * a1;
                res += Math.Abs((q + prev) / 2 * (step));
                prev = q;
            }
            return res;
        }
    }
}