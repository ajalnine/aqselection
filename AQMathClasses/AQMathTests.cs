﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AQMathClasses
{
    static partial class AQMath
    {
        public static double? TTestWelchP(List<object> group1, List<object> group2)
        {
            return TTestWelch(group1, group2).Item2;
        }

        public static Tuple<double?, double?> TTestWelch(List<object> group1, List<object> group2)
        {
            var m1 = AQMath.AggregateMean(group1);
            var m2 = AQMath.AggregateMean(group2);
            var s1 = AQMath.AggregateStDev(group1);
            var s2 = AQMath.AggregateStDev(group2);
            if (!m1.HasValue || !m2.HasValue || !s1.HasValue || !s2.HasValue) return null;

            var mean1 = m1.Value;
            var mean2 = m2.Value;
            var stdev1 = s1.Value;
            var stdev2 = s2.Value;
            var n1 = (double)AQMath.AggregateCount(group1);
            var n2 = (double)AQMath.AggregateCount(group2);

            var s = stdev1 * stdev1 / n1 + stdev2 * stdev2 / n2;
            var sd = Math.Sqrt(s);
            var t = (mean1 - mean2) / sd;
            var df = s * s / (Math.Pow(stdev1 * stdev1 / n1, 2) / (n1 - 1) + Math.Pow(stdev2 * stdev2 / n2, 2) / (n2 - 1));
            var tp = StudentCD(t, df);
            var p = tp > 0.5 ? 2*( 1 - tp) : 2 * tp;
            return new Tuple<double?, double?>(t, p);
        }

        public static double? TTestP(List<object> group1, List<object> group2)
        {
            return TTest(group1, group2).Item2;
        }

        public static Tuple<double?, double?> TTest(List<object> group1, List<object> group2)
        {
            var m1 = AQMath.AggregateMean(group1);
            var m2 = AQMath.AggregateMean(group2);
            var s1 = AQMath.AggregateStDev(group1);
            var s2 = AQMath.AggregateStDev(group2);
            if (!m1.HasValue || !m2.HasValue || !s1.HasValue || !s2.HasValue) return null;

            var mean1 = m1.Value;
            var mean2 = m2.Value;
            var stdev1 = s1.Value;
            var stdev2 = s2.Value;
            var n1 = (double)AQMath.AggregateCount(group1);
            var n2 = (double)AQMath.AggregateCount(group2);

            var df = n1 + n2 - 2;
            var sp = Math.Sqrt((stdev1 * stdev1 * (n1 - 1) + stdev2 * stdev2 * ( n2 - 1 )) / (df));

            var sd = sp * Math.Sqrt(1/n1 + 1/n2);
            var t = (mean1 - mean2) / sd;
            if (sd == 0) return null;
            var tp = StudentCD(t, df);
            var p = tp > 0.5 ? 2 * (1 - tp) : 2 * tp;
            return new Tuple<double?, double?>(t, p);
        }

        public static double? UTestP(List<object> group1, List<object> group2)
        {
            return UTest(group1, group2).Item2;
        }

        public static Tuple<double?, double?> UTest(List<object> group1, List<object> group2)
        {
            var g1 = group1.OfType<double>().Select(a => new { s = 1, value = a}).ToList();
            var g2 = group2.OfType<double>().Select(a => new { s = 2, value = a}).ToList();
            g1.AddRange(g2);

            var ranked = g1.OrderBy(a => a.value).Select((current, i) => new Ranked {Value = current.value, Rank = i + 1.0d, Sample = current.s}).ToList();
            var tied = (from r in ranked group r by r.Value).Select(a => new {Key = a.Key, Group = a.ToList()}).ToList();
            var tieCount = new List<double>();

            foreach (var t in tied)
            {
                var toTie = t.Group;
                tieCount.Add(toTie.Count);
                if (toTie.Count <= 1.0d) continue;
                var tiedValue = AggregateMean(toTie.Select(a => a.Rank).Cast<object>().ToList());
                if (!tiedValue.HasValue) continue;
                foreach (var tt in toTie) tt.Rank = tiedValue.Value;
            }

            var sample1 = ranked.Where(a => a.Sample == 1).ToList();
            var sample2 = ranked.Where(a => a.Sample == 2).ToList();
            var n1 = (double)sample1.Count;
            var n2 = (double)sample2.Count;
            var n = n1 + n2;

            var R1 = tied.Sum(a => a.Group.Sum(b => b.Sample == 1 ? b.Rank : 0));
            var R2 = tied.Sum(a => a.Group.Sum(b => b.Sample == 2 ? b.Rank : 0));
            
            var U1 = R1 - n1 * (n1 + 1.0d) / 2.0d;
            var U2 = R2 - n2 * (n2 + 1.0d) / 2.0d;
            var U = U1 < U2 ? U1 : U2;
            var mU = n1 * n2 / 2.0d;

            var tieSum = tieCount.Sum(t => (t * t * t - t) / (n * (n - 1.0d)));
            var s = Math.Sqrt((n1 * n2 / 12.0d) * ((n + 1.0d) - tieSum));
            var z = (U - mU + 0.5d) / s;
            var tp = NormalCD(z, 0.0d, 1.0d);
            var p = tp > 0.5 ? 2 * (1 - tp) : 2 * tp;
            return new Tuple<double?, double?>(z, p);
        }

        public class Ranked
        {
            public double Value { get; set; }
            public double Rank  { get; set; }
            public int Sample { get; set; }
        }
    }
}
