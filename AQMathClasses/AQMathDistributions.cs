﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {

        #region Normal
        public static Double? NormalPD(Double? X, Double? Mean, Double? stdev)
        {
            if (!X.HasValue || !Mean.HasValue || !stdev.HasValue) return null;
            return (1/(stdev.Value * Math.Sqrt(2*Math.PI))) * Math.Exp(-(Math.Pow(X.Value-Mean.Value,2))/(2*Math.Pow(stdev.Value,2)));
        }

        public static Double? NormalCD(Double? X, Double? Mean, Double? stdev)
        {
            if (!X.HasValue || !Mean.HasValue || !stdev.HasValue) return null;
            if (stdev.Value == 0) return (Mean > X) ? 0 : 1;
            return 0.5 * (1 + Erf((X.Value - Mean.Value) / (stdev.Value * Math.Sqrt(2))));
        }

        public static Double? NormalZbyP(Double? P)
        {
           	double SPLIT1 = 0.425, SPLIT2 = 5, CONST1 = 0.180625, CONST2 = 1.6;
            double A0 = 3.3871328727963666080;
            double A1 = 1.3314166789178437745E+2;
        	double A2 = 1.9715909503065514427E+3;
        	double A3 = 1.3731693765509461125E+4;
        	double A4 = 4.5921953931549871457E+4;
        	double A5 = 6.7265770927008700853E+4;
        	double A6 = 3.3430575583588128105E+4;
        	double A7 = 2.5090809287301226727E+3;
        	double B1 = 4.2313330701600911252E+1;
        	double B2 = 6.8718700749205790830E+2;
        	double B3 = 5.3941960214247511077E+3;
        	double B4 = 2.1213794301586595867E+4;
        	double B5 = 3.9307895800092710610E+4;
        	double B6 = 2.8729085735721942674E+4;
        	double B7 = 5.2264952788528545610E+3;
        	double C0 = 1.42343711074968357734;
            double C1 = 4.63033784615654529590;
            double C2 = 5.76949722146069140550;
            double C3 = 3.64784832476320460504;
            double C4 = 1.27045825245236838258;
            double C5 = 2.41780725177450611770E-1;
            double C6 = 2.27238449892691845833E-2;
            double C7 = 7.74545014278341407640E-4;
            double D1 = 2.05319162663775882187;
            double D2 = 1.67638483018380384940;
            double D3 = 6.89767334985100004550E-1;
            double D4 = 1.48103976427480074590E-1;
            double D5 = 1.51986665636164571966E-2;
            double D6 = 5.47593808499534494600E-4;
            double D7 = 1.05075007164441684324E-9;
        	double E0 = 6.65790464350110377720;
            double E1 = 5.46378491116411436990;
            double E2 = 1.78482653991729133580;
            double E3 = 2.96560571828504891230E-1;
            double E4 = 2.65321895265761230930E-2;
            double E5 = 1.24266094738807843860E-3;
            double E6 = 2.71155556874348757815E-5;
            double E7 = 2.01033439929228813265E-7;
            double F1 = 5.99832206555887937690E-1;
            double F2 = 1.36929880922735805310E-1;
            double F3 = 1.48753612908506148525E-2;
            double F4 = 7.86869131145613259100E-4;
            double F5 = 1.84631831751005468180E-5;
            double F6 = 1.42151175831644588870E-7;
            double F7 = 2.04426310338993978564E-15;
            double Q = P.Value - 0.5;
            double R;
        	  if (Math.Abs(Q) <= SPLIT1)
              {
        	       R = CONST1 - Q * Q;
        	       return Q * (((((((A7 * R + A6) * R + A5) * R + A4) * R + A3) * R + A2) * R + A1) * R + A0) / (((((((B7 * R + B6) * R + B5) * R + B4) * R + B3) * R + B2) * R + B1) * R + 1);
        	  }
        	  else
              {
        	     if(Q < 0) R = P.Value;
        	     else  R = 1 - P.Value;
        	  
        	    if (R < 0) return 0;
        	    
        	    R = Math.Sqrt(-Math.Log(R));
        	    double result;
                if (R <= SPLIT2)
                {
                   R -= CONST2;
        	       result =  (((((((C7 * R + C6) * R + C5) * R + C4) * R + C3) * R + C2) * R + C1) * R + C0) /  (((((((D7 * R + D6) * R + D5) * R + D4) * R + D3) * R + D2) * R + D1) * R + 1);
        	    }
                else
                {
        	        R -= SPLIT2;
                    result = (((((((E7 * R + E6) * R + E5) * R + E4) * R + E3) * R + E2) * R + E1) * R + E0) / (((((((F7 * R + F6) * R + F5) * R + F4) * R + F3) * R + F2) * R + F1) * R + 1);
        	    }
        	    if (Q < 0)return -result;
                else return result;
        	}
        }

        public static Double? NormalCDInverse(Double? X, Double? Mean, Double? stdev)
        {
            if (!X.HasValue || !Mean.HasValue || !stdev.HasValue) return null;
            return 1 - NormalCD(X, Mean, stdev);
        }

        public static Double? NormalCDBetween(Double? X, Double? X2, Double? Mean, Double? stdev)
        {
            if (!X2.HasValue || !X.HasValue || !Mean.HasValue || !stdev.HasValue) return null;
            if (X.Value < X2.Value) return NormalCD(X2, Mean, stdev) - NormalCD(X, Mean, stdev);
            else return 1 - NormalCD(X, Mean, stdev) + NormalCD(X2, Mean, stdev);
        }
        #endregion

        #region Chi Square
        public static Double? ChiSquarePD(Double? X, Double? n)
        {
            if (!X.HasValue || !n.HasValue) return null;
            if (X.Value <= 0) return 0;
            return (1 / (Pow(2, n.Value / 2) * Gamma(n / 2))) * Pow(X.Value, 0.5 * n.Value - 1) * Math.Exp(-0.5 * X.Value);
        }

        public static Double? ChiSquareCD(Double? X, Double? n)
        {
            if (!X.HasValue || !n.HasValue) return null;
            if (X.Value <= 0) return 0;
            return GammaIncomplete(n.Value / 2, X.Value / 2);
        }
        #endregion

        #region Student
        public static Double? StudentPD(Double? X, Double? N)
        {
            if (!X.HasValue || !N.HasValue) return null;
            double n = N.Value;
            double x = X.Value;
            return (Pow(1+x*x/n,-0.5*(n+1)))/(Math.Sqrt(n)*Beta(0.5,n*0.5));
        }

        public static Double? StudentCD(Double? X, Double? N)
        {
            if (!X.HasValue || !N.HasValue) return null;
            double n = N.Value;
            double x = X.Value;
            if (n < 100)
            {
                double a = (x + Math.Sqrt(x * x + n)) / (2 * Math.Sqrt(x * x + n));
                return BetaIncomplete(0.5 * n, 0.5 * n, a);
            }
            return NormalCD(x, 0, 1);
        }

        public static Double? StudentTbyP(Double? Pt, Double? Ndf)
        {
            if (!Pt.HasValue || !Ndf.HasValue) return null;
            double p = Pt.Value;
            double ndf = Ndf.Value;
            if (p <= 0 || p >= 1 || ndf < 1) return null; // error argument
            const double eps = 1e-12;
            const double M_PI_2 = 1.570796326794896619231321691640; // pi/2
            bool neg;
            const bool lower_tail = false;

            double P,q,prob,a,b,c,d,y,x;
            if((lower_tail && p > 0.5) || (!lower_tail && p < 0.5)) {
               neg = false;
               P = 2 * (lower_tail ? (1 - p) : p);
             }
             else {
               neg = true;
               P = 2 * (lower_tail ? p : (1 - p));
             }

             if(Math.Abs(ndf - 2) < eps) {   /* df ~= 2 */
               q = Math.Sqrt(2 / (P * (2 - P)) - 2);
             }
             else if (ndf < 1 + eps) {   /* df ~= 1 */
               prob = P * M_PI_2;
               q = Math.Cos(prob)/Math.Sin(prob);
             }
             else {      /*-- usual case;  including, e.g.,  df = 1.1 */
               a = 1 / (ndf - 0.5);
               b = 48 / (a * a);
               c = ((20700 * a / b - 98) * a - 16) * a + 96.36;
               d = ((94.5 / (b + c) - 3) / b + 1) * Math.Sqrt(a * M_PI_2) * ndf;
               y = Math.Pow(d * P, 2 / ndf);
               if (y > 0.05 + a) {
                 /* Asymptotic inverse expansion about normal */
                   x = NormalZbyP(0.5 * P).Value;
                 y = x * x;
                 if (ndf < 5)
                   c += 0.3 * (ndf - 4.5) * (x + 0.6);
                 c = (((0.05 * d * x - 5) * x - 7) * x - 2) * x + b + c;
                 y = (((((0.4 * y + 6.3) * y + 36) * y + 94.5) / c - y - 3) / b + 1) * x;
                 y = a * y * y;
                 if (y > 0.002)/* FIXME: This cutoff is machine-precision dependent*/
                   y = Math.Exp(y) - 1;
                 else { /* Taylor of    e^y -1 : */
                   y = (0.5 * y + 1) * y;
                 }
               }
               else {
                 y = ((1 / (((ndf + 6) / (ndf * y) - 0.089 * d - 0.822)
                     * (ndf + 2) * 3) + 0.5 / (ndf + 4))
                     * y - 1) * (ndf + 1) / (ndf + 2) + 1 / y;
               }
               q = Math.Sqrt(ndf * y);
             }
             if(neg) q = -q;
             return q;
        }
        #endregion

        #region Beta
        public static Double? BetaPD(Double? X, Double? A, Double? B)
        {
            if (!X.HasValue || !A.HasValue || !B.HasValue) return null;
            double? beta = Beta(A, B);
            if (!beta.HasValue) return null;
            return 1/beta * Math.Pow(X.Value, A.Value - 1) * Math.Pow(1 - X.Value, B.Value - 1);
        }

        public static Double? BetaCD(Double? X, Double? A, Double? B)
        {
            if (!X.HasValue || !A.HasValue || !B.HasValue) return null;
            return BetaIncomplete(A,B,X);
        }
        #endregion

        #region Gamma
        public static Double? GammaPD(Double? X, Double? A)
        {
            if (!X.HasValue || !A.HasValue) return null;
            double? gamma = Gamma(A);
            if (!gamma.HasValue) return null;
            return 1 / gamma * Math.Pow(X.Value, A.Value - 1) * Math.Exp(- X.Value);
        }

        public static Double? GammaCD(Double? X, Double? A)
        {
            if (!X.HasValue || !A.HasValue) return null;
            return GammaIncomplete(A, X)/Gamma(A);
        }
        #endregion

        #region Fischer
        public static Double? FisherPD(Double? X, Double? M, Double? N)
        {
            if (!X.HasValue || !N.HasValue || !M.HasValue) return null;
            double m = M.Value;
            double n = N.Value;
            double x = X.Value;
            return Gamma((n+m)/2)* Pow(m,0.5*m)*Pow(n,0.5*n)*Pow(x,0.5*n-1)*Pow(m+n*x, -0.5*(n+m))/(Gamma(n/2)*Gamma(m/2));
        }

        public static Double? FisherCD(Double? X, Double? M, Double? N)
        {
            if (!X.HasValue || !N.HasValue || !M.HasValue) return null;
            double m = M.Value;
            double n = N.Value;
            double x = X.Value;
            var l2 = (2 * n + m * x / 3 + m - 2) / (2 * n + 4 * m * x / 3);
            var r = ChiSquareCD(l2 * m * x, m);
            return r;
            
            double a = n*x/(m + n*x);
            return BetaIncomplete(0.5*n, 0.5*m, a);
        }
        #endregion

        #region Poisson
        public static Double? PoissonPD(Double? X, Double? Lambda)
        {
            if (!X.HasValue || !Lambda.HasValue) return null;
            return Math.Exp(-Lambda.Value) * Math.Pow(Lambda.Value, X.Value) / Factorial(X);
        }

        public static Double? PoissonCD(Double? X, Double? Lambda)
        {
            if (!X.HasValue || !Lambda.HasValue) return null;
            if (X.Value >= 0)
            {
                return (Gamma(Math.Floor(X.Value+1))* (1 - GammaIncomplete(Math.Floor(X.Value+1), Lambda.Value))) / Factorial(Math.Floor(X.Value)).Value;
            }
            else
            {
                Double s = 0;
                for (int i = 0; i <= X.Value; i++)
                {
                    s += Math.Pow(Lambda.Value, i)/Factorial(i).Value;
                }
                return Math.Exp(-Lambda.Value) * s;
            }
        }
        #endregion

        #region Weibull

        public static Double? WeibullPD(Double? X, Double? Lambda, Double? K)
        {
            if (!X.HasValue || !Lambda.HasValue || !K.HasValue) return null;
            if (X.Value >= 0)
            {
                return (K.Value / Lambda.Value) * Math.Pow((X.Value / Lambda.Value), K.Value - 1) * Math.Exp(-Math.Pow(X.Value/Lambda.Value, K.Value));
            }
            else return 0;
        }

        public static Double? WeibullCD(Double? X, Double? Lambda, Double? K)
        {
            return 1 - Math.Exp(-Math.Pow(X.Value / Lambda.Value, K.Value));
        }

        public static Double? WeibullGetKFromData(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (!m.Any()) return null;

            var k = 1d;

            double previousK;
            var count = 0;
            do
            {
                previousK = k;
                var fk = WeibullGetF(m, k);
                var fkd = WeibullGetFDerivate(m, k);
                k -= fk / fkd;
                count++;
                if (count > 1000) break;
            } while (Math.Abs( previousK - k)>1e-3 );

            return k;
        }

        private static double WeibullGetF(List<double> list, double k)
        {
            var sumXKlnX = list.Select(a => 
            a!=0
            ? Math.Pow(a, k) * Math.Log(a, Math.E)
            : 0).Sum();

            var sumXK = list.Select(a => Math.Pow(a, k)).Sum();

            var sumlnX = list.Select(a =>
            a!=0 
            ? Math.Log(a, Math.E)
            : 0 ).Sum();

            return sumXKlnX/sumXK - 1.0d / k - sumlnX/list.Count;
        } 

        private static double WeibullGetFDerivate(List<double> list, double k)
        {
            var sum1 = list.Select(x =>
            x!=0 
            ? Math.Pow(x, k) * Math.Log(x, Math.E) * Math.Log(x, Math.E) 
            : 0).Sum();

            var sumXK = list.Select(x => 
            x!=0 
            ? Math.Pow(x, k)
            :0).Sum();

            var sumXKlnX = list.Select(x =>
            x!=0
            ? Math.Pow(x, k) * Math.Log(x, Math.E)
            : 0).Sum();

            return (sum1 * sumXK - sumXKlnX * sumXKlnX) / (sumXK * sumXK) + 1 / (k * k);
        }

        public static double? WeibullGetLambdaFromData(List<object> data, double? k)
        {
            if (!k.HasValue) return null;
            var m = data.OfType<double>().ToList();
            if (!m.Any()) return null;

            return Math.Pow(m.Select(a => Math.Pow(a, k.Value)).Sum() / m.Count, 1 / k.Value);
        }

        #endregion
    }
}