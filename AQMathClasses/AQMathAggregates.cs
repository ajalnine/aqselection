﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace AQMathClasses
{
    public static partial class AQMath
    {
        public static Double? AggregateMin(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            return m.Min();
        }
        public static Double? AggregateMax(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null; 
            return m.Max();
        }

        public static DateTime? AggregateMinDate(List<object> data)
        {
            var m = data.OfType<DateTime>().ToList();
            if (m.Count == 0) return null;
            return m.Min();
        }
        public static DateTime? AggregateMaxDate(List<object> data)
        {
            var m = data.OfType<DateTime>().ToList();
            if (m.Count == 0) return null;
            return m.Max();
        }

        public static Double? AggregateRange(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            return m.Max() - m.Min();
        }

        public static Double? AggregateMean(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            return m.Sum() / m.Count;
        }
        public static Double? AggregateMedian(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            return AggregatePercentile(data,50);
        }

        public static object AggregateFromFirst(List<object> data, Double? skip)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count -1 < skip) return null;
            return data[(int)(skip??0)];
        }

        public static object AggregateFromLast(List<object> data, Double? skip)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count - 1 < skip) return null;
            return data[m.Count - 1 - (int)(skip??0)];
        }

        public static Double? AggregateIQR(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            return AggregatePercentile(data, 75) - AggregatePercentile(data, 25);
        }

        public static Double? AggregatePercentile(List<object> data, Double? percent)
        {
            if (!percent.HasValue) return null;
            var m = data.OfType<Double>().ToList();
            var d = m.OrderBy(a => a);
            var n = m.Count;
            if (n == 0) return null;
            if (n == 1) return (Double?)m.Single();
            int minIndex = (int) Math.Floor((n - 1) * percent.Value / 100d);
            if (percent!=50 || n % 2 == 1) return (Double?) d.ElementAt(minIndex);
            return ((Double?)d.ElementAt(minIndex)+(Double?)d.ElementAt(minIndex+1))/2;
        }

        public static Double? AggregatePercentileInc(List<object> data, Double? percent)
        {
            if (!percent.HasValue) return null;
            var m = data.OfType<Double>().ToList();
            var d = m.OrderBy(a => a);
            var n = m.Count;
            if (n == 0) return null;
            if (n == 1) return (Double?)m.Single();
            if (percent <= 0) return m.First();
            if (percent >= 100) return m.Last();
            double index = ((double)n - 1.0d) * (percent.Value / 100d);
            double k = index % 1;
            int start = (int)Math.Floor(index);
            return (Double?)d.ElementAt(start) + ((Double?)d.ElementAt(start + 1 ) - (Double?)d.ElementAt(start)) * k;
        }

        public static Double? PercentInRange(List<object> data, Double? RangeStart, Double? RangeEnd)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            int c= m.Count;
            if (RangeStart.HasValue) c -= m.Where(a=> a < RangeStart.Value).Count();
            if (RangeEnd.HasValue) c -= m.Where(a => a > RangeEnd.Value).Count();
            return (Double?) ((double)c)/(double)m.Count*100;
        }

        public static object CheckInRange(object value, object rangeStart, object rangeEnd)
        {
            if (value == null) return null;

            if (value is double)
            {
                double? RangeStart = rangeStart as double?;
                double? RangeEnd = rangeEnd as double?;
                double? Value = value as double?;

                if (!RangeStart.HasValue && !RangeEnd.HasValue) return value;
                if (!RangeStart.HasValue && RangeEnd.HasValue)
                {
                    return RangeStart.Value <= Value.Value ? value : null;
                }
                if (RangeStart.HasValue && !RangeEnd.HasValue)
                {
                    return RangeEnd.Value >= Value.Value ? value : null;
                }
                if (RangeStart != null && RangeEnd != null)
                {
                    return RangeEnd.Value >= Value.Value && RangeStart.Value <= Value.Value ? value : null;
                }
            }

            if (value is DateTime)
            {
                DateTime? RangeStart = rangeStart as DateTime?;
                DateTime? RangeEnd = rangeEnd as DateTime?;
                DateTime? Value = value as DateTime?;

                if (!RangeStart.HasValue && !RangeEnd.HasValue) return value;
                if (!RangeStart.HasValue && RangeEnd.HasValue)
                {
                    return RangeStart.Value <= Value.Value ? value : null;
                }
                if (RangeStart.HasValue && !RangeEnd.HasValue)
                {
                    return RangeEnd.Value >= Value.Value ? value : null;
                }
                if (RangeStart != null && RangeEnd != null)
                {
                    return RangeEnd.Value >= Value.Value && RangeStart.Value <= Value.Value ? value : null;
                }
            }

            if (value is TimeSpan)
            {
                TimeSpan? RangeStart = rangeStart as TimeSpan?;
                TimeSpan? RangeEnd = rangeEnd as TimeSpan?;
                TimeSpan? Value = value as TimeSpan?;

                if (!RangeStart.HasValue && !RangeEnd.HasValue) return value;
                if (!RangeStart.HasValue && RangeEnd.HasValue)
                {
                    return RangeStart.Value <= Value.Value ? value : null;
                }
                if (RangeStart.HasValue && !RangeEnd.HasValue)
                {
                    return RangeEnd.Value >= Value.Value ? value : null;
                }
                if (RangeStart != null && RangeEnd != null)
                {
                    return RangeEnd.Value >= Value.Value && RangeStart.Value <= Value.Value ? value : null;
                }
            }

            if (value is int)
            {
                int? RangeStart = rangeStart as int?;
                int? RangeEnd = rangeEnd as int?;
                int? Value = value as int?;

                if (!RangeStart.HasValue && !RangeEnd.HasValue) return value;
                if (!RangeStart.HasValue && RangeEnd.HasValue)
                {
                    return RangeStart.Value <= Value.Value ? value : null;
                }
                if (RangeStart.HasValue && !RangeEnd.HasValue)
                {
                    return RangeEnd.Value >= Value.Value ? value : null;
                }
                if (RangeStart != null && RangeEnd != null)
                {
                    return RangeEnd.Value >= Value.Value && RangeStart.Value <= Value.Value ? value : null;
                }
            }

            if (value is string || value is bool)
            {
                string RangeStart = rangeStart?.ToString();
                string RangeEnd = rangeEnd?.ToString();
                string Value = value?.ToString();
                if (Value == RangeStart || Value == RangeEnd) return value;
            }

            return null;
        }
        
        public static Double? AggregateSum(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return m.Sum();
        }

        public static Double? AggregateStDev(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            
            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            if (Avg == null) return null;
            if (Cnt <= 1) return 0;
            Double? m2 = Moment2(data);
            if (m2.HasValue) return Math.Sqrt(m2.Value / (Cnt - 1));
            else return null;
        }

        public static Double? AggregateStDevRangeEstimation(List<object> data)
        {
            var m = data.OfType<Double>().ToList();
            if (m.Count == 0) return null;
            
            double r = 0;

            for (var i = 0; i < m.Count - 1; i++)
                r += Math.Abs(m[i + 1] - m[i]);

            return r / m.Count;
        }


        public static Double? AggregateStDevPopulation(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;

            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            if (Avg == null) return null;
            if (Cnt <= 1) return 0;
            Double? m2 = Moment2(data);
            if (m2.HasValue) return Math.Sqrt(m2.Value / Cnt);
            else return null;
        }

        public static Double? AggregateVariance(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            Double? X = AggregateStDev(data).Value;
            if (!X.HasValue) return null;
            return Pow(X.Value, 2);
        }

        public static int AggregateCount(List<object> data)
        {
            return data?.Count(s => s != null && s.ToString() != "") ?? 0;
        }

        public static int AggregateMinIntervalBetween(List<object> data, object value)
        {
            if (data == null || value == null) return 0;
            var prev = 0;
            var count = int.MaxValue;
            for (var j=0; j<data.Count; j++)
            {
                if (data[j] != value || j == 0) continue;
                var c = j - prev;
                if (c < count) count = c;
                prev = j;
            }
            return count;
        }

        public static int AggregateContinuousCountOfValues(List<object> data, object value)
        {
            if (data == null || !data.Contains(value)) return 0;
            var maxCount = 0;
            var count = 0;
            var previous = value;
            for (int i = 0; i<data.Count; i++)
            {
                if (data[i] == previous && data[i]==value) count++;
                else
                {
                    if (maxCount < count) maxCount = count;
                    previous = data[i];
                    count = 1;
                }
            }
            if (maxCount < count) maxCount = count;
            return maxCount;
        }

        public static int AggregateDistinctCount(List<object> data)
        {
            if (data == null) return 0;
            else return data.Where(s => s != null && s.ToString() != "").Distinct().Count();
        }

        public static int AggregateCountOfValues(List<object> data, object value)
        {
            if (data == null) return 0;
            if (value == null) return data.Count(s => s == null);
            return data.Count(s => s!=null && s.ToString() == value.ToString());
        }

        public static double AggregateCountOfValuesInPercent(List<object> data, object value)
        {
            if (data == null) return 0;
            var count = (double)data.Count(s => s != null && s.ToString() != "");
            if (value == null) return data.Count(s => s == null) / count * 100.0d;
            return data.Count(s => s != null && s.ToString() == value.ToString()) / count * 100.0d;
        }

        public static List<object> List(params object[] Data)
        {
            return Data.ToList();
        }

        public static List<object> Distinct(List<object> Data)
        {
            return Data.Distinct().ToList();
        }

        public static List<object> NumberSequence(double? from, double? step, double? count)
        {
            if (!from.HasValue || !count.HasValue || !step.HasValue) return null;

            var res = new List<object>();
            for (var i = 0;  i < count.Value; i ++)
            {
                res.Add(from.Value + i * step.Value);
            }
            return res;
        }

        public static List<object> DateSequence(DateTime? from, TimeSpan? step, double? count)
        {
            if (!from.HasValue || !count.HasValue || !step.HasValue) return null;

            var res = new List<object>();
            var dt = from.Value;
            for (var i = 0; i < count.Value; i++)
            {
                res.Add(dt);
                dt += step.Value;
            }
            return res;
        }
        
        public static List<object> DistinctNoEmptyStrings(List<object> Data)
        {
            return Data.Where(a=>a is string && !string.IsNullOrWhiteSpace(a.ToString())).Select(a=>(object)a.ToString().Trim(' ', '\t', '\r', '\n')).OrderBy(a=>a).Distinct().ToList();
        }

        public static object CheckList(object value, params object[] Data)
        {
            return Data.Contains(value) ? value : null;
        }

        public static object ReplaceList(object value, object newValue, params object[] Data)
        {
            return Data.Contains(value) ? newValue : value;
        }

        public static List<object> ListFromStrings(string data)
        {
            if (data == null) return null;
            if (data == String.Empty) return new List<object>();
            var div = new[] { "\r\n", "\r", "\n", ",", ";" };
            return (from d in data.Split(div, StringSplitOptions.RemoveEmptyEntries) select d.Trim()).Cast<object>().ToList();
        }

        public static List<object> ListFromQuotedStrings(string data)
        {
            if (data == null) return null;
            if (data == String.Empty) return new List<object>();
            var div = new[] { "," };
            return (from d in data.Split(div, StringSplitOptions.RemoveEmptyEntries) select d.Trim()).Select(a=>a.Substring(1, a.Length-2)).Cast<object>().ToList();
        }

        public static Boolean? Exists(List<object> Data, object Value)
        {
            if (Data == null) return null;
            return Data.Contains(Value);
        }
    }
}