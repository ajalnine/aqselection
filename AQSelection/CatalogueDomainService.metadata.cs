﻿
namespace AQSelection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using OpenRiaServices.DomainServices.Hosting;
    using OpenRiaServices.DomainServices.Server;


    // The MetadataTypeAttribute identifies AQ_CatalogueDataMetadata as the class
    // that carries additional metadata for the AQ_CatalogueData class.
    [MetadataTypeAttribute(typeof(AQ_CatalogueData.AQ_CatalogueDataMetadata))]
    public partial class AQ_CatalogueData
    {

        // This class allows you to attach custom attributes to properties
        // of the AQ_CatalogueData class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class AQ_CatalogueDataMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private AQ_CatalogueDataMetadata()
            {
            }

            public AQ_CatalogueRevisions AQ_CatalogueRevisions { get; set; }

            public Nullable<DateTime> AttachedAt { get; set; }

            public string AttachedBy { get; set; }

            public byte[] BinaryData { get; set; }

            public string BinaryDataMIME { get; set; }

            public string DataDescription { get; set; }

            public string DataItemName { get; set; }

            public int id { get; set; }

            public Nullable<bool> IsDeleted { get; set; }

            public Nullable<int> RevisionID { get; set; }

            public string XmlData { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies AQ_CatalogueItemsMetadata as the class
    // that carries additional metadata for the AQ_CatalogueItems class.
    [MetadataTypeAttribute(typeof(AQ_CatalogueItems.AQ_CatalogueItemsMetadata))]
    public partial class AQ_CatalogueItems
    {

        // This class allows you to attach custom attributes to properties
        // of the AQ_CatalogueItems class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class AQ_CatalogueItemsMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private AQ_CatalogueItemsMetadata()
            {
            }

            public ICollection<AQ_CatalogueItems> AQ_CatalogueItems1 { get; set; }

            public AQ_CatalogueItems AQ_CatalogueItems2 { get; set; }

            public ICollection<AQ_CatalogueRevisions> AQ_CatalogueRevisions { get; set; }

            public int id { get; set; }

            public Nullable<bool> IsDeleted { get; set; }

            public string Name { get; set; }

            public Nullable<int> ParentId { get; set; }

            public string Type { get; set; }
        }
    }

    // The MetadataTypeAttribute identifies AQ_CatalogueRevisionsMetadata as the class
    // that carries additional metadata for the AQ_CatalogueRevisions class.
    [MetadataTypeAttribute(typeof(AQ_CatalogueRevisions.AQ_CatalogueRevisionsMetadata))]
    public partial class AQ_CatalogueRevisions
    {

        // This class allows you to attach custom attributes to properties
        // of the AQ_CatalogueRevisions class.
        //
        // For example, the following marks the Xyz property as a
        // required property and specifies the format for valid values:
        //    [Required]
        //    [RegularExpression("[A-Z][A-Za-z0-9]*")]
        //    [StringLength(32)]
        //    public string Xyz { get; set; }
        internal sealed class AQ_CatalogueRevisionsMetadata
        {

            // Metadata classes are not meant to be instantiated.
            private AQ_CatalogueRevisionsMetadata()
            {
            }

            public ICollection<AQ_CatalogueData> AQ_CatalogueData { get; set; }

            public AQ_CatalogueItems AQ_CatalogueItems { get; set; }

            public Nullable<int> CalculationID { get; set; }

            public int id { get; set; }

            public Nullable<int> Revision { get; set; }

            public string RevisionAuthor { get; set; }

            public Nullable<DateTime> RevisionDate { get; set; }

            public string RevisionDescription { get; set; }
        }
    }
}
