﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DeleteTables:ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);

            DataSet newDataStorage;
            if (_currentParameters.TablesTransformation != null && _currentParameters.TablesTransformation.Any())
            {
                newDataStorage = new DataSet();
                var variables = dataStorage.Tables["Переменные"];
                newDataStorage.Tables.Add(variables.Copy());
                
                foreach (var table in _currentParameters.TablesTransformation)
                {
                    if (_currentParameters.TablesToDelete.Contains(table.OldTableName)) continue;

                    var dt = dataStorage.Tables[table.OldTableName];
                    var newdt = dt.Copy();
                    newdt.TableName = table.NewTableName;
                    if (newDataStorage.Tables.Contains(newdt.TableName)) newDataStorage.Tables.Remove(newdt.TableName);
                    newDataStorage.Tables.Add(newdt);
                    dt.CloneAllPropertiesTo(newdt);
                }
            }
            else
            {
                foreach (var p in _currentParameters.TablesToDelete)
                {
                    if (dataStorage.Tables.IndexOf(p) != -1) dataStorage.Tables.Remove(p);
                }
                newDataStorage = dataStorage;
            }

            return new StepProcessResult { Success = true, ResultedDataSet = newDataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<string> TablesToDelete;
            public ObservableCollection<NewTableData> TablesTransformation;
        }

        public class NewTableData
        {
            public string OldTableName;
            public string NewTableName;
        }
    }
}
