﻿using System.Data;

namespace AQSelection.Calculators
{
    interface ICalculator
    {
        StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo);
    }

    public class StepProcessResult
    {
        public bool Success;
        public DataSet ResultedDataSet;
        public string Message;
    }
}
