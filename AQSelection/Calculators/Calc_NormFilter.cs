﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
    public class Calc_NormFilter : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            if (_currentParameters.Case)
            {
                List<DataRow> toDeleteCases = new List<DataRow>();
                if (_currentParameters.All)
                {
                    foreach (DataRow r in source.Rows)
                    {
                        if (ProcessCaseAll(r) ^ _currentParameters.Include) toDeleteCases.Add(r);
                    }
                }
                else
                {
                    foreach (DataRow r in source.Rows)
                    {
                        if (ProcessCaseEvery(r) ^ _currentParameters.Include) toDeleteCases.Add(r);
                    }
                }
                foreach (DataRow c in toDeleteCases)
                {
                    source.Rows.Remove(c);
                }
            }
            else
            {
                foreach (DataRow r in source.Rows)
                {
                    foreach (DataColumn column in source.Columns)
                    {
                        if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#") || (_currentParameters.Field!= null && column.ColumnName!= _currentParameters.Field)) continue;
                        if (r[column] == DBNull.Value) continue;
                        var minName = "#" + column.ColumnName + "_НГ";
                        var maxName = "#" + column.ColumnName + "_ВГ";
                        double? minValue = r.Table.Columns.Contains(minName)
                            ? r[minName] != DBNull.Value
                                ? (double?)r[minName]
                                : null
                            : null;
                        double? maxValue = r.Table.Columns.Contains(maxName)
                            ? r[maxName] != DBNull.Value
                                ? (double?)r[maxName]
                                : null
                            : null;

                        var check = true;
                        if (minValue.HasValue) check &= ((double?)r[column] >= minValue);
                        if (maxValue.HasValue) check &= ((double?)r[column] <= maxValue);
                        if (!_currentParameters.Include) check = !check;
                        if (!_currentParameters.InRange) check = !check;
                        if (!check) r[column] = DBNull.Value;
                    }
                }
            }
            return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
        }

        private bool ProcessCaseAll(DataRow r)
        {
            if (_currentParameters.InRange)
            {
                var check = true;
                var columns = _currentParameters.Field == " Любое поле" ? r.Table.Columns.Cast<DataColumn>() 
                                                                        :from s in r.Table.Columns.Cast<DataColumn>() where s.ColumnName == _currentParameters.Field select s;
                foreach (DataColumn column in columns)
                {
                    if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#") && (_currentParameters.Field != null && column.ColumnName != _currentParameters.Field)) continue;
                    if (r[column] == DBNull.Value) continue;
                    var minName = "#" + column.ColumnName + "_НГ";
                    var maxName = "#" + column.ColumnName + "_ВГ";
                    double? minValue = r.Table.Columns.Contains(minName)
                        ? r[minName] != DBNull.Value
                            ? (double?)r[minName]
                            : null
                        : null;
                    double? maxValue = r.Table.Columns.Contains(maxName)
                        ? r[maxName] != DBNull.Value
                            ? (double?)r[maxName]
                            : null
                        : null;


                    if (minValue.HasValue && r[column] != DBNull.Value)
                        check &= ((double?)r[column] >= minValue);
                    if (maxValue.HasValue && r[column] != DBNull.Value)
                        check &= ((double?)r[column] <= maxValue);
                }
                return check;
            }
            else
            {
                var check = true;
                var columns = _currentParameters.Field == " Любое поле" ? r.Table.Columns.Cast<DataColumn>()
                    : from s in r.Table.Columns.Cast<DataColumn>() where s.ColumnName == _currentParameters.Field select s;
                var nonEmpty = false;
                foreach (DataColumn column in columns)
                {
                    if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#") && (_currentParameters.Field != null && column.ColumnName != _currentParameters.Field)) continue;
                    if (r[column] == DBNull.Value) continue;
                    var minName = "#" + column.ColumnName + "_НГ";
                    var maxName = "#" + column.ColumnName + "_ВГ";
                    double? minValue = r.Table.Columns.Contains(minName)
                        ? r[minName] != DBNull.Value
                            ? (double?)r[minName]
                            : null
                        : null;
                    double? maxValue = r.Table.Columns.Contains(maxName)
                        ? r[maxName] != DBNull.Value
                            ? (double?)r[maxName]
                            : null
                        : null;


                    if (minValue.HasValue && r[column] != DBNull.Value)
                    {
                        check &= ((double?)r[column] < minValue);
                        nonEmpty = true;
                    }
                    if (maxValue.HasValue && r[column] != DBNull.Value)
                    {
                        check &= ((double?)r[column] > maxValue);
                        nonEmpty = true;
                    }
                }
                if (!nonEmpty) return false;
                return check;
            }
        }

        private bool ProcessCaseEvery(DataRow r)
        {
            var columns = _currentParameters.Field == " Любое поле" ? r.Table.Columns.Cast<DataColumn>()
                : from s in r.Table.Columns.Cast<DataColumn>() where s.ColumnName == _currentParameters.Field select s;

            foreach (DataColumn column in columns)
            {
                if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#") && (_currentParameters.Field != null && column.ColumnName != _currentParameters.Field)) continue;
                if (r[column] == DBNull.Value) continue;
                var minName = "#" + column.ColumnName + "_НГ";
                var maxName = "#" + column.ColumnName + "_ВГ";
                double? minValue = r.Table.Columns.Contains(minName)
                    ? r[minName]!=DBNull.Value 
                        ? (double?)r[minName] 
                        : null 
                    : null;
                double? maxValue = r.Table.Columns.Contains(maxName)
                    ? r[maxName] != DBNull.Value
                        ? (double?)r[maxName]
                        : null
                    : null;

                var inRange = true;
                if (minValue.HasValue) inRange &= r[column] != DBNull.Value && ((double?)r[column] >= minValue);
                if (maxValue.HasValue) inRange &= r[column] != DBNull.Value && ((double?)r[column] <= maxValue);
                if (inRange && _currentParameters.InRange) return true;
                if (!inRange && !_currentParameters.InRange) return true;
            }
            return false;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public bool Include;
            public bool Case;
            public bool All;
            public bool InRange;
            public string Field;
        }
    }
}
