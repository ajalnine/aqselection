﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
    // ReSharper disable once InconsistentNaming
    public class Calc_ExpressionFields : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];
            var insertPosition = processingDataTable.Columns.IndexOf(_currentParameters.InsertAfterField) + 1;
            foreach (var expressionfield in _currentParameters.ExpressionFields)
            {
                var column = GetExpressionColumn(processingDataTable, expressionfield.FieldName, expressionfield.ExpressionType, insertPosition++);
                var index = processingDataTable.Columns.IndexOf(column);
                ProcessRule(expressionfield, index, processingDataTable);
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessRule(ExpressionFieldDescription rule, int index, DataTable table)
        {
            var ime = AQExpression.CompileExpression(rule.Code);
            if (ime == null) return;
            foreach (DataRow dr in table.Rows)
            {
                var result = ime.GetResult(dr);
                if (result != null)
                {
                    dr[index] = result;
                }
            }
        }

        private static DataColumn GetExpressionColumn(DataTable processingDataTable, string fieldName, string fieldType, int index)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == fieldName);
            if (column != null) return column;
            var dt = Type.GetType("System." + fieldType);
            var dc = new DataColumn
            {
                ColumnName = fieldName,
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            dc.SetOrdinal(index);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string InsertAfterField;
            public ObservableCollection<ExpressionFieldDescription> ExpressionFields;
        }
        public class ExpressionFieldDescription
        {
            public string FieldName;
            public string ExpressionType;
            public string Expression;
            public string Code;
        }
    }
}