﻿using System;
using System.Globalization;
using System.IO;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_SetRanges : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];

            foreach (var r in _currentParameters.Ranges)
            {
                if (!(r.Min.HasValue && double.IsNaN(r.Min.Value)))
                {
                    var column = GetRangeColumn(source, "#" + r.Parameter + "_НГ");
                    var value = r.Min;
                    ProcessColumn(column, value, r.Group, _currentParameters.GroupField);
                }
                if (!(r.Max.HasValue && double.IsNaN(r.Max.Value)))
                {
                    var column = GetRangeColumn(source, "#" + r.Parameter + "_ВГ");
                    var value = r.Max;
                    ProcessColumn(column, value, r.Group, _currentParameters.GroupField);
                }
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessColumn(DataColumn column, double? value, string group, string groupField)
        {
            var source = column.Table;
            if (groupField == "Нет")
            {
                foreach (DataRow row in source.Rows)
                {
                    row[column] = value.HasValue ? (object)value.Value : DBNull.Value;
                }
            }
            else
            {
                foreach (DataRow row in source.Rows)
                {
                    if (!string.IsNullOrEmpty(group) && row[groupField].ToString() != group) continue;
                    row[column] = value.HasValue ? (object)value.Value : DBNull.Value;
                }
            }
        }

        private static DataColumn GetRangeColumn(DataTable processingDataTable, string fieldName)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == fieldName);
            if (column != null) return column;
            var dt = Type.GetType("System.Double");
            var dc = new DataColumn
            {
                ColumnName = fieldName,
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string GroupField;
            public string Groups;
            public ObservableCollection<Range> Ranges;
        }

        public class Range
        {
            public double? Min;
            public double? Max;
            public string Parameter;
            public string Group;
        }
    }
}
