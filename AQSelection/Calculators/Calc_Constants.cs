﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Constants : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables["Переменные"];
            var columns = source.Columns.OfType<DataColumn>().Select(a => a.ColumnName).ToList();
            foreach (var c in _currentParameters.Constants)
            {
                if (columns.Contains(c.VariableName)) continue;
                var newdc = new DataColumn { ColumnName = c.VariableName, DataType = Type.GetType("System." + c.VariableType) };
                source.Columns.Add(newdc);
                foreach (DataRow dr in source.Rows)
                {
                    var value = c.Value ?? DBNull.Value;
                    if (c.VariableType == "TimeSpan" && c.Value!=null) value = new TimeSpan(((DateTime) c.Value).Ticks);
                    dr[c.VariableName] = value;
                }
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
       
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }
        public class Parameters
        {
            public ObservableCollection<ConstantDescription> Constants;
        }
        public class ConstantDescription
        {
            public string VariableName;
            public string VariableType;
            public object Value;
        }
    }
}
