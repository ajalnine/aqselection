﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using MySql;
using MySql.Data.MySqlClient;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DataSourceSQL: ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var variables = dataStorage.Tables["Переменные"];

            var from = DateTime.Parse(variables.Rows[0]["От"].ToString());
            var to = DateTime.Parse(variables.Rows[0]["До"].ToString());
            var connectionString = _currentParameters.ConnectionString ?? System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            var csl = connectionString.ToLower();
            var isMySQL = csl.Contains("server") && csl.Contains("uid") && csl.Contains("pwd") && csl.Contains("database");
            if (isMySQL) return MySQLStepProcess(dataStorage, @from, to, variables, connectionString);
            return MSSQLStepProcess(dataStorage, @from, to, variables, connectionString);
        }

        private StepProcessResult MSSQLStepProcess(DataSet dataStorage, DateTime @from, DateTime to, DataTable variables,
            string connectionString)
        {
            var sql = GetSQLPrepend(@from, to, variables) + _currentParameters.SQL;

            var replaces = Regex.Matches(sql, @"(\{@([a-zA-z0-9а-яА-Я]*)([^\{\}]*)\})");

            sql = replaces.Cast<Match>().Aggregate(sql,
                (current, r) => current.Replace(r.Groups[0].Value,
                    variables.Rows[0][r.Groups[2].Value.ToString(CultureInfo.InvariantCulture)].ToString()));
            var clsql = sql.ToLower();
            var sconn = new SqlConnection(connectionString);
            sconn.Open();
            try
            {
                var temporaryTables = new List<string>();
                foreach (DataTable dt in dataStorage.Tables)
                {
                    var tempName = "[#" + dt.TableName.ToLower() + "]";
                    if (!clsql.Contains(tempName)) continue;

                    CreateTemporaryTable(dt, sconn);
                    temporaryTables.Add(tempName);
                }

                var sc2 = new SqlCommand(sql, sconn);
                var sda = new SqlDataAdapter(sc2) {SelectCommand = {CommandTimeout = 10000}};
                var data = new DataTable {TableName = _currentParameters.TableName};
                sda.Fill(0, int.MaxValue, data);
                dataStorage.Tables.Add(data);

                foreach (var t in temporaryTables)
                {
                    DropTemporaryTable(t, sconn);
                }
            }
            catch
            {
                sconn.Close();
                return new StepProcessResult {Success = false, ResultedDataSet = null};
            }

            return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
        }

        private StepProcessResult MySQLStepProcess(DataSet dataStorage, DateTime @from, DateTime to, DataTable variables,
            string connectionString)
        {
            var sql = GetMySQLPrepend(@from, to, variables) + _currentParameters.SQL;

            var replaces = Regex.Matches(sql, @"(\{@([a-zA-z0-9а-яА-Я]*)([^\{\}]*)\})");

            sql = replaces.Cast<Match>().Aggregate(sql,
                (current, r) => current.Replace(r.Groups[0].Value,
                    variables.Rows[0][r.Groups[2].Value.ToString(CultureInfo.InvariantCulture)].ToString()));
            var clsql = sql.ToLower();
            var sconn = new MySqlConnection(connectionString);
            sconn.Open();
            try
            {
                
                var sc2 = new MySqlCommand(sql, sconn);
                var sda = new MySqlDataAdapter(sc2) { SelectCommand = { CommandTimeout = 10000 } };
                var data = new DataTable { TableName = _currentParameters.TableName };
                sda.Fill(0, int.MaxValue, data);
                dataStorage.Tables.Add(data);
            }
            catch
            {
                sconn.Close();
                return new StepProcessResult { Success = false, ResultedDataSet = null };
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void DropTemporaryTable(string s, SqlConnection sconn)
        {
            if (!s.StartsWith("[#")) return;
            var sc = new SqlCommand("DROP TABLE "+s, sconn);
            sc.ExecuteNonQuery();
        }

        private void DropTemporaryTable(string s, MySqlConnection sconn)
        {
            if (!s.StartsWith("[#")) return;
            var sc = new MySqlCommand("DROP TABLE " + s, sconn);
            sc.ExecuteNonQuery();
        }

        private void CreateTemporaryTable(DataTable dt, SqlConnection sconn)
        {
            var sc = new SqlCommand(CreateTableScript(dt), sconn);
            sc.ExecuteNonQuery();

            var sbc = new SqlBulkCopy(sconn) {DestinationTableName = "[#" + dt.TableName + "]"};
            sbc.WriteToServer(dt);
            sbc.Close();
        }
        /*
        private void CreateTemporaryTable(DataTable dt, MySqlConnection sconn)
        {
            var sc = new MySqlCommand(CreateTableScript(dt), sconn);
            sc.ExecuteNonQuery();

            var sbc = new MySqlBulkLoader(sconn);
            sbc.TableName = "[#" + dt.TableName + "]";
            sbc.WriteToServer(dt);
        }
        */
        private string CreateTableScript(DataTable dt)
        {
            var res = "\r\nCREATE TABLE [#" + dt.TableName + "] (";
            var isFirst = true;
            foreach (DataColumn f in dt.Columns)
            {
                if (!isFirst) res += " , ";
                string datatype;
                switch (f.DataType.ToString().ToLower())
                {
                    case "int32":
                    case "system.int32":
                        datatype = " int ";
                        break;
                    case "double":
                    case "system.double":
                        datatype = " numeric(17,6) ";
                        break;
                    case "boolean":
                    case "system.boolean":
                        datatype = " bit ";
                        break;
                    case "timespan":
                    case "system.timespan":
                    case "datetime":
                    case "system.datetime":
                        datatype = " datetime ";
                        break;
                    default:
                        datatype = " nvarchar(max) ";
                        break;
                }

                res += "[" + f.ColumnName + "] " + datatype;
                isFirst = false;
            }

            res += ")\r\n";
            return res;
        }

        protected string GetSQLPrepend(DateTime @from, DateTime to, DataTable variables)
        {
            var sqlPrepend =
                $@"set dateformat ymd
declare @date1 datetime 
declare @date2 datetime
set @date1='{
                    @from.ToString("yyyy-MM-dd")}'  set @date2='{to.ToString("yyyy-MM-dd")}' 
";

            if (variables == null || variables.Columns.Count <= 0) return sqlPrepend;
            foreach (DataColumn i in variables.Columns)
            {
                string sqlType;
                string sqlValue;
                switch (i.DataType.Name)
                {
                    case "String":
                        sqlType = "nvarchar(MAX)";
                        sqlValue = "'"+variables.Rows[0][i].ToString().Replace("'", "''")+"'";
                        break;
                    case "Double":
                    case "Int32":
                        sqlType = "numeric(16,8)";
                        sqlValue = variables.Rows[0][i].ToString().Replace(",",".");
                        break;
                    case "Boolean":
                    case "bool":
                        sqlType = "bit";
                        sqlValue = (variables.Rows[0][i].ToString().ToLower()=="true") ? "1" : "0";
                        break;
                    case "DateTime":
                        sqlType = "datetime";
                        sqlValue = "'"+((DateTime)variables.Rows[0][i]).ToString("yyyy-MM-dd")+"'";
                        break;
                    case "TimeSpan":
                        sqlType = "datetime";
                        sqlValue = "'"+((TimeSpan)variables.Rows[0][i]).ToString("HH:mm:ss")+"'";
                        break;
                    default:
                        return null;
                }
                var name = i.ColumnName.Replace(" ", "_").Replace("\\", "_");
                sqlPrepend += "declare @" + name + " " + sqlType + "\r\n";
                sqlPrepend += "set @" + name + "=" + sqlValue + "\r\n";
            }

            return sqlPrepend;
        }

        private static string GetMySQLPrepend(DateTime @from, DateTime to, DataTable variables)
        {
            var sqlPrepend =
                 $@"set @date1='{from.ToString("yyyy-MM-dd")}';  set @date2='{to.ToString("yyyy-MM-dd")}';";

            if (variables == null || variables.Columns.Count <= 0) return sqlPrepend;
            foreach (DataColumn i in variables.Columns)
            {
                string sqlType;
                string sqlValue;
                switch (i.DataType.Name)
                {
                    case "String":
                        sqlValue = "'" + variables.Rows[0][i].ToString().Replace("'", "''") + "'";
                        break;
                    case "Double":
                    case "Int32":
                        sqlValue = variables.Rows[0][i].ToString().Replace(",", ".");
                        break;
                    case "Boolean":
                    case "bool":
                        sqlValue = (variables.Rows[0][i].ToString().ToLower() == "true") ? "1" : "0";
                        break;
                    case "DateTime":
                        sqlValue = "'" + ((DateTime)variables.Rows[0][i]).ToString("yyyy-MM-dd") + "'";
                        break;
                    case "TimeSpan":
                        sqlValue = "'" + ((TimeSpan)variables.Rows[0][i]).ToString("HH:mm:ss") + "'";
                        break;
                    default:
                        return null;
                }
                var name = i.ColumnName.Replace(" ", "_").Replace("\\", "_");
                sqlPrepend += "set @" + name + ":=" + sqlValue + ";\r\n";
            }

            return sqlPrepend;
        }
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string SQL;
            public string TableName;
            public string ConnectionString;
        }
    }
}