﻿using System;
using System.Globalization;
using System.IO;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_SplitTable : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            var fieldIndex = source.Columns.IndexOf(_currentParameters.CategoryFieldName);
            var datatype = source.Columns[fieldIndex].DataType.ToString();
            if (_currentParameters.Categories.Count == 0)
            {
                var fields = (from r in source.AsEnumerable() select r[fieldIndex].ToString().Trim()).Distinct();
                foreach(var f in fields)
                {
                    _currentParameters.Categories.Add(f);
                }
            }
            var stringCompare = datatype.Contains("String");

            foreach (var s in _currentParameters.Categories.Where(a=>a!=String.Empty))
            {
                var dv = source.DefaultView;
                var items = s.Split('|');
                
                var filter = String.Empty;
                var isFirst = true;
                foreach (var i in items)
                {
                    if (stringCompare)
                    {
                        if (!isFirst) filter += " OR ";
                        filter += "Trim([" + _currentParameters.CategoryFieldName + "]) LIKE '" + i.Replace("'", "''") + "'";
                    }
                    else
                    {
                        var comparison = "=";
                        if (i.StartsWith("<")) comparison = " ";
                        if (i.StartsWith(">")) comparison = " ";
                        if (i.StartsWith("<=")) comparison = " ";
                        if (i.StartsWith(">=")) comparison = " ";
                        if (i.StartsWith("<>")) comparison = " ";
                        if (!isFirst) filter +=  comparison=="=" ?  " OR " : " AND ";
                        filter += "[" + _currentParameters.CategoryFieldName + "] " + comparison + i.ToString(CultureInfo.InvariantCulture) ;
                    }
                    isFirst = false;
                }
                var categoryTableName = s.Replace("|", " ; ").Replace("%", "~").Trim();
                dv.RowFilter = filter;
                var destination = dv.ToTable(_currentParameters.OutputTablePrefix + categoryTableName);
                if (_currentParameters.RemoveCategory) destination.Columns.Remove(_currentParameters.CategoryFieldName);
                source.CloneAllPropertiesTo(destination);
                if (dataStorage.Tables.Contains(destination.TableName))dataStorage.Tables.Remove(destination.TableName);
                dataStorage.Tables.Add(destination);
            }
            if (_currentParameters.RemoveTable) dataStorage.Tables.Remove(source);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string CategoryFieldName;
            public string OutputTablePrefix;
            public ObservableCollection<string> Categories;
            public bool RemoveCategory;
            public bool RemoveTable;
        }
    }
}
