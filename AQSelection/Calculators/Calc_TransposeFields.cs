﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_TransposeFields : ICalculator
    {
        private Parameters _currentParameters;
        private DataTable _source, _destination;
        private List<string> _fixedFields; 
        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            _source = dataStorage.Tables[_currentParameters.TableName];
            _destination = new DataTable();
            
            var usedFields = new List<string>();
            var sourceFields = _source.Columns.OfType<DataColumn>().Select(a=>a.ColumnName).ToList();
            foreach (var g in _currentParameters.TransposeGroups)
            {
                if (!g.DeleteSourceFields) continue;
                usedFields.AddRange(g.FieldList);
            }
            _fixedFields = sourceFields.Where(a => !usedFields.Contains(a)).ToList();

            ConstructDestinationColumns();

            foreach (DataRow row in _source.Rows)
            {
                ProcessRow(row);
            }

            _destination.TableName = _currentParameters.TableName;
            dataStorage.Tables.Remove(_source);
            _source.CloneTablePropertiesTo(_destination);
            dataStorage.Tables.Add(_destination);
            return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
        }

        private void ProcessRow(DataRow row)
        {
            var destinationRowNumber = _currentParameters.TransposeGroups
                .Select(g => (int) AggregateRow.Count(row, g.FieldList)).Concat(new[] {0}).Max();
            for (var i = 0; i < destinationRowNumber; i++)
            {
                var newRow = _destination.NewRow();
                foreach (var fixedField in _fixedFields)
                {
                    newRow[fixedField] = row[fixedField];
                }
                foreach (var g in _currentParameters.TransposeGroups)
                {
                    var groupSource = g.FieldList.Select(f => new { value = row[f], name = f }).Where(v => v.value != DBNull.Value).ToList();
                    if (groupSource.Count <= i) continue;
                    newRow[g.ValueName] = groupSource[i].value;
                    newRow[g.CategoryName] = groupSource[i].name;
                }
                _destination.Rows.Add(newRow);
            }
        }

        private void ConstructDestinationColumns()
        {
            foreach (var fixedField in _fixedFields)
            {
                var c = _source.Columns[fixedField];
                var destinationColumn = new DataColumn {ColumnName = c.ColumnName, DataType = c.DataType};
                c.CloneColumnPropertiesTo(destinationColumn);
                _destination.Columns.Add(destinationColumn);
            }

            foreach (var g in _currentParameters.TransposeGroups)
            {
                var c = _source.Columns[g.FieldList[0]];
                var destinationColumn = new DataColumn {ColumnName = g.CategoryName, DataType = typeof (string)};
                c.CloneColumnPropertiesTo(destinationColumn);
                _destination.Columns.Add(destinationColumn);
                var destinationColumn2 = new DataColumn {ColumnName = g.ValueName, DataType = c.DataType};
                c.CloneColumnPropertiesTo(destinationColumn2);
                _destination.Columns.Add(destinationColumn2);
            }
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof (Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters) xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<TransposeGroup> TransposeGroups;
            public string TableName;
        }

        public class TransposeGroup
        {
            public string CategoryName;
            public string ValueName;
            public bool DeleteSourceFields;
            public ObservableCollection<string> FieldList;
        }
    }
}
