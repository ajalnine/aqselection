﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DataSourceCalculation:ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var variables = dataStorage.Tables["Переменные"];

            var from = DateTime.Parse(variables.Rows[0]["От"].ToString());
            var to = DateTime.Parse(variables.Rows[0]["До"].ToString());

            var result = CatalogueHelper.GetCalculationByID(_currentParameters.CurrentCalculationID);

            var calculationDataSet = new DataSet();
            var e = new DataTable("Переменные");
            calculationDataSet.Tables.Add(e);
            e.Columns.Add(new DataColumn("От", typeof(DateTime)));
            e.Columns.Add(new DataColumn("До", typeof(DateTime)));
            e.Rows.Add(from, to);

            foreach (var s in result.Where(a=>a.Group!="Параметры"))
            {
                var className = s.Calculator;
                var t = Type.GetType("AQSelection.Calculators." + className);
                if (t == null) continue;
                var calculator = Activator.CreateInstance(t) as ICalculator;
                if (calculator == null) continue;
                var spr = calculator.ProcessData(calculationDataSet, s);
                calculationDataSet = spr.ResultedDataSet;
                if (!spr.Success) return new StepProcessResult{ ResultedDataSet = null,  Success = false};
            }

            foreach (DataTable dt in calculationDataSet.Tables)
            {
                if (dt.TableName == "Переменные") continue;
                var s = dt.Clone();
                s.TableName = GetAlias(s.TableName);
                dataStorage.Tables.Add(s);
                dt.CloneAllPropertiesTo(s);
                foreach(DataRow r in dt.Rows)
                {
                    s.ImportRow(r);
                }
            }

            var newNames = calculationDataSet.Tables["Переменные"].Columns;
            var newValues = calculationDataSet.Tables["Переменные"].Rows[0];
            var oldNames = dataStorage.Tables["Переменные"].Columns;
            
            foreach (DataColumn dc in newNames)
            {
                var name = dc.ToString();
                if (!oldNames.Contains(name))
                {
                    dataStorage.Tables["переменные"].Columns.Add(new DataColumn { ColumnName = dc.ColumnName, DataType = dc.DataType });
                }
                dataStorage.Tables["Переменные"].Rows[0][dc.ColumnName] = newValues[dc.ColumnName];
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }


        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }
        
        public string GetAlias(string name)
        {
            return (from s in _currentParameters.TableAliases where s.Name == name select s.AliasName).SingleOrDefault();
        }

        public class Parameters
        {
            public int CurrentCalculationID;
            public List<Alias> TableAliases;
        }

        public class Alias
        {
            public string Name;
            public string AliasName;
        }
    }
}

