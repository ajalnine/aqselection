﻿using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_FormatBorders : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];

            foreach (DataColumn c in processingDataTable.Columns)
            {
                c.ExtendedProperties.Add("ChangeBorder", _currentParameters.ChangeBordered.Contains(c.ColumnName));
                c.ExtendedProperties.Add("LeftBorder", _currentParameters.LeftBordered.Contains(c.ColumnName));
                c.ExtendedProperties.Add("RightBorder", _currentParameters.RightBordered.Contains(c.ColumnName));
                c.ExtendedProperties.Add("HorizontalBorder", _currentParameters.HorizontalBordered.Contains(c.ColumnName));
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public List<string> LeftBordered;
            public List<string> RightBordered;
            public List<string> HorizontalBordered;
            public List<string> ChangeBordered;
        }
    }
}

