﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DataSourceWebService: ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var variables = dataStorage.Tables["Переменные"];

            try
            {
                var handler = new HttpClientHandler();
                using (var client = new HttpClient(handler))
                {
                    XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
                    XNamespace aqns = _currentParameters.MethodNamespace;
                    XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                    XNamespace wsa = "http://www.w3.org/2005/08/addressing";
                    var methodParameters =
                        _currentParameters.VariableMapping.Select(
                            a => new XElement(a.ParameterName,
                            variables.Rows[0][a.VariableName.StartsWith("@") ? a.VariableName.Substring(1, a.VariableName.Length-1) : a.VariableName ].ToString())).ToList();

                    var soapAction = _currentParameters.SOAPAction ?? _currentParameters.Method;
                    var soapRequest = new XDocument(
                        new XDeclaration("1.0", "UTF-8", "no"),
                        new XElement(ns + "Envelope",
                            new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                            new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                            new XAttribute(XNamespace.Xmlns + "soap", ns),
                            new XAttribute(XNamespace.Xmlns + "wsa", wsa),
                           _currentParameters.SOAPAction !=null ? new XElement(ns + "Header", new XElement(wsa+"Action", new XAttribute("mustUnderstand", 1), soapAction))
                           : new XElement(ns + "Header"),
                            new XElement(ns + "Body",new XElement((aqns??string.Empty) + _currentParameters.Method, methodParameters)
                            )
                        ));

                    var request = new HttpRequestMessage
                    {
                        RequestUri = new Uri(_currentParameters.Service),
                        Method = HttpMethod.Post,
                        Content = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml")
                    };

                    client.Timeout = new TimeSpan(0, 15, 0);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                    request.Headers.Add("SOAPAction", soapAction);

                    if (!string.IsNullOrEmpty(_currentParameters.UserName))
                    {
                        var byteArray = Encoding.ASCII.GetBytes(_currentParameters.UserName + ":" + _currentParameters.Password);
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    }
                    else handler.UseDefaultCredentials = true;

                    var response = client.SendAsync(request).Result;
                    var responseBodyAsText = response.Content.ReadAsStringAsync().Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        return new StepProcessResult { Success = false, ResultedDataSet = null };
                    }

                    var doc = XDocument.Parse(responseBodyAsText);

                    var soapresult = aqns != null ? doc.Descendants(aqns + _currentParameters.MethodResponse).SingleOrDefault()
                                                  : doc.Descendants(_currentParameters.MethodResponse).SingleOrDefault();
                    if (soapresult != null)
                    {
                        foreach (var variable in _currentParameters.OutputVariables)
                        {
                            var column = GetColumn(variables, variable.Alias.StartsWith("@") 
                                ? variable.Alias.Substring(1, variable.Alias.Length - 1) 
                                : variable.Alias , variable.DataType);

                            var element = soapresult.Descendants(variable.Name).SingleOrDefault()?.Value;
                            if (string.IsNullOrEmpty(element)) variables.Rows[0][column] = null;
                            else
                            {
                                switch (variable.DataType)
                                {
                                    case "Double":
                                        variables.Rows[0][column] = double.Parse(element);
                                        break;
                                    case "String":
                                        variables.Rows[0][column] = element;
                                        break;
                                    case "Boolean":
                                        variables.Rows[0][column] = bool.Parse(element);
                                        break;
                                    case "DateTime":
                                        variables.Rows[0][column] = DateTime.Parse(element);
                                        break;
                                    case "TimeSpan":
                                        variables.Rows[0][column] = TimeSpan.Parse(element);
                                        break;
                                }
                            }
                        }

                        foreach (var table in _currentParameters.OutputTables)
                        {
                            var dt = new DataTable {TableName = table.Alias};
                            dataStorage.Tables.Add(dt);
                            var tableResult = soapresult.Descendants().Where(a=>a.Name.LocalName==table.TableName).ToList();

                            var rows = tableResult.Any()
                                ? tableResult.Descendants().Where(a => a.Name.LocalName == table.RowName).ToList()
                                : soapresult.Descendants().Where(a => a.Name.LocalName == table.RowName).ToList();
                            if (!rows.Any()) continue;

                            foreach (var r in rows)
                            {
                                var row = dt.NewRow();
                                foreach (var cellValue in table.RowItems)
                                {
                                    var column = GetColumn(dt, cellValue.Name, cellValue.DataType);

                                    var element = r.Descendants().SingleOrDefault(a => a.Name.LocalName == cellValue.Name)?.Value;
                                    if (string.IsNullOrEmpty(element)) row[column] = DBNull.Value;
                                    else
                                    {
                                        switch (cellValue.DataType)
                                        {
                                            case "Double":
                                                row[column] = double.Parse(element, NumberFormatInfo.InvariantInfo);
                                                break;
                                            case "String":
                                                row[column] = element;
                                                break;
                                            case "Boolean":
                                                row[column] = bool.Parse(element);
                                                break;
                                            case "DateTime":
                                                row[column] = DateTime.Parse(element);
                                                break;
                                            case "TimeSpan":
                                                row[column] = TimeSpan.Parse(element);
                                                break;
                                        }
                                    }
                                }
                                dt.Rows.Add(row);
                            }
                        }
                    }
                    else if (_currentParameters.OutputTables.Any() || _currentParameters.OutputVariables.Any()) return new StepProcessResult { Success = false, ResultedDataSet = null };
                }
            }
            catch(Exception e)
            {
                return new StepProcessResult {Success = false, ResultedDataSet = null, Message = e.Message};
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private static DataColumn GetColumn(DataTable processingDataTable, string fieldName, string fieldType)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == fieldName);
            if (column != null) return column;
            var dt = Type.GetType("System." + fieldType);
            var dc = new DataColumn
            {
                ColumnName = fieldName,
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string Wsdl;
            public string Wsdlurl;
            public string UserName;
            public string Password;
            public string Method;
            public string Service;
            public string MethodResponse;
            public string MethodNamespace;
            public string SOAPAction;
            public List<string> Operations;
            public List<VariableMap> VariableMapping;
            public List<VariableOutput> OutputVariables;
            public List<TableOutput> OutputTables;
        }

        public class VariableOutput
        {
            public string Name;
            public string Alias;
            public string DataType;
        }

        public class FieldOutput
        {
            public string Name;
            public string DataType;
        }

        public class TableOutput
        {
            public string TableName { get; set; }
            public string Alias { get; set; }
            public string RowName { get; set; }
            public List<FieldOutput> RowItems { get; set; }
        }

        public class VariableMap
        {
            public string VariableName;
            public string ParameterName;
        }
    }
}