﻿using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
    public class Calc_DataSourceTable:ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            if ((_currentParameters.Data==null || _currentParameters.Data.Count==0) && !string.IsNullOrEmpty(_currentParameters.TSV))
            {
                SetDataByText(_currentParameters.TSV);
            }
            
            var destination = new DataTable();
            destination.TableName = stepInfo.Outputs == null ? _currentParameters.TableName : stepInfo.Outputs.First().Name;
            dataStorage.Tables.Add(destination);

            for (int i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                string typeName = _currentParameters.DataTypes[i];
                Type columnType = Type.GetType("System."+typeName);
                destination.Columns.Add(new DataColumn { ColumnName = _currentParameters.Data[0].RowData[i].Value.ToString(), DataType = columnType});

                var isColored = false;
                for (var j = 1; j < _currentParameters.Data.Count; j++)
                {
                    if (_currentParameters.Data[j].RowData[i].BG == null) continue;
                    isColored = true;
                    break;
                }
                if (!isColored) continue;
                destination.Columns.Add(new DataColumn { ColumnName = "#" + _currentParameters.Data[0].RowData[i].Value + "_Цвет", DataType = typeof(string)});
            }

            for (var j = 1; j < _currentParameters.Data.Count; j++)
            {
                
                var dr = destination.Rows.Add();
                var i = 0;
                foreach (var sourceColumn in _currentParameters.Data[0].RowData)
                {
                    var cell = _currentParameters.Data[j].RowData[i];
                    if (!string.IsNullOrEmpty(cell.Expression)) continue;
                    var dataColumnName = sourceColumn.Value.ToString();
                    var dataIndex = destination.Columns.IndexOf(destination.Columns[dataColumnName]);
                    var colorColumnName = string.Format("#{0}_Цвет", dataColumnName);
                    if (destination.Columns.Contains(colorColumnName))
                    {
                        var destinationColorIndex = destination.Columns.IndexOf(destination.Columns[colorColumnName]);
                        var color = destinationColorIndex >= 0 ? _currentParameters.Data[j].RowData[i].BG : DBNull.Value;
                        dr[destinationColorIndex] = color;
                    }

                    string value = cell.Value?.ToString();

                    switch (destination.Columns[dataIndex].DataType.Name)
                    {
                        case "Double":
                            var separator = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToCharArray()[0];
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)Double.Parse(value.Replace(',', separator).Replace('.', separator)));
                            break;
                        case "DateTime":
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)DateTime.Parse(value));
                            break;
                        case "TimeSpan":
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)TimeSpan.Parse(value));
                            break;
                        case "Boolean":
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)Boolean.Parse(value));
                            break;
                        case "Int32":
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)Int32.Parse(value));
                            break;
                        default:
                            dr[dataIndex] = (string.IsNullOrWhiteSpace(value) ? DBNull.Value : (object)value); break;
                    }
                    i++;
                }
            }

            for (var j = 1; j < _currentParameters.Data.Count; j++)
            {
                var i = 0;
                var dr = destination.Rows[j - 1];
                foreach (var sourceColumn in _currentParameters.Data[0].RowData)
                {
                    var dataColumnName = sourceColumn.Value.ToString();
                    var dataIndex = destination.Columns.IndexOf(destination.Columns[dataColumnName]);
                    var columnCode = sourceColumn.Code;
                    var columnExpression = sourceColumn.Expression;
                    var cell = _currentParameters.Data[j].RowData[i];
                    if (columnCode == null && cell.Code == null) continue;
                    if (columnCode == null)
                    {
                        dr[dataIndex] = ProcessCellExpression(cell.Code, cell.Expression, dr);
                    }
                    else
                    {
                        dr[dataIndex] = string.IsNullOrEmpty(cell.Code)
                        ? ProcessCellExpression(columnCode, columnExpression, dr)
                        : ProcessCellExpression(cell.Code, cell.Expression, dr);
                    }
                    i++;
                }
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void SetDataByText(string textData)
        {
            var insertedRows = textData.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (insertedRows.Count<2) insertedRows = textData.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var height = insertedRows.Count;
            var width = insertedRows[0].Split(new[] { "\t" }, StringSplitOptions.None).Count();
            for (var i = 0; i < width; i++) AddNewColumn();
            for (var i = 0; i < height; i++) AddNewRow();
            
            for (var i = 0; i < height; i++)
            {
                var row = insertedRows[i];
                var values = row.Split(new[] { "\t" }, StringSplitOptions.None);
                for (var j = 0; j < width; j++)
                {
                    _currentParameters.Data[i].RowData[j].Value = values[j];
                }
            }
        }
        private void AddNewColumn()
        {
            foreach (var row in _currentParameters.Data)
            {
                var c = new Cell { };
                row.RowData.Add(c);
            }
        }
        private void AddNewRow()
        {
            var r = new Row { RowData = new ObservableCollection<Cell>() };
            for (var index = 0; index < _currentParameters.DataTypes.Count; index++)
            {
                r.RowData.Add(new Cell { Value = String.Empty });
            }
            _currentParameters.Data.Add(r);
        }


        private Dictionary<string, AQMathClasses.IMathExpression> expressionCache = new Dictionary<string, AQMathClasses.IMathExpression>();

        private object ProcessCellExpression(string code, string expression, DataRow dr)
        {
            if (expressionCache.ContainsKey(expression)) return expressionCache[expression].GetResult(dr);
            var ime = AQExpression.CompileExpression(code);
            expressionCache.Add(expression, ime);
            if (ime == null) return "##Error";
            return ime.GetResult(dr);
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public List<string> DataTypes;
            public List<string> ColumnNames;
            public List<string> ColoredColumns;
            public List<Double> ColumnWidth;
            public string TableName;
            public ObservableCollection<Row> Data;
            public string TSV;
        }

        public class Row
        {
            public ObservableCollection<Cell> RowData;
        }

        public class Cell
        {
            public object Value;
            public object BG;
            public string Code;
            public string Expression;
            public string ExpressionDataType;
        }
    }
}
