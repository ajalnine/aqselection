﻿using System;
using System.IO;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Variable : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables["Переменные"];

            var ime = AQExpression.CompileExpression(_currentParameters.Code);
            if (ime==null)return new StepProcessResult {Success = false, ResultedDataSet = dataStorage };

            var newdc = new DataColumn { ColumnName = _currentParameters.VariableName, DataType = Type.GetType("System." + _currentParameters.VariableType) };
            source.Columns.Add(newdc);
            
            foreach (DataRow dr in source.Rows)
            {
                var result =  ime.GetResult(dr);
                try
                {
                    dr[_currentParameters.VariableName] = result ?? DBNull.Value;
                }
                catch
                {
                    return new StepProcessResult { Success = false, ResultedDataSet = dataStorage };
                }
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
       
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string Expression;
            public string Code;
            public string VariableName;
            public string VariableType;
        }
    }
}
