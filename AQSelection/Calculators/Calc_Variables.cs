﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Variables : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables["Переменные"];

            foreach (var variable in _currentParameters.Variables)
            {
                var column = GetVariableColumn(processingDataTable, variable.VariableName, variable.VariableType);
                var index = processingDataTable.Columns.IndexOf(column);
                ProcessRule(variable, index, processingDataTable);
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessRule(VariableDescription rule, int index, DataTable table)
        {
            var ime = AQExpression.CompileExpression(rule.Code);
            if (ime == null) return;
            foreach (DataRow dr in table.Rows)
            {
                var result = ime.GetResult(dr);
                if (result != null)
                {
                    dr[index] = result;
                }
            }
        }

        private static DataColumn GetVariableColumn(DataTable processingDataTable, string fieldName, string fieldType)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName== fieldName);
            if (column != null) return column;
            var dt = Type.GetType("System." + fieldType);
            var dc = new DataColumn
            {
                ColumnName = fieldName,
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<VariableDescription> Variables;
        }

        public class VariableDescription
        {
            public string VariableName;
            public string VariableType;
            public string Expression;
            public string Code;
        }
    }
}