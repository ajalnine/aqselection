﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_JoinTables:ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            
            var destination = JoinTables(dataStorage.Tables[_currentParameters.FirstTable], dataStorage.Tables[_currentParameters.SecondTable], 
                       _currentParameters.FirstKey, _currentParameters.SecondKey, 
                       _currentParameters.JoinType == "FULL" || _currentParameters.JoinType == "LEFT", 
                       _currentParameters.JoinType == "FULL" || _currentParameters.JoinType == "RIGHT");

            destination.TableName = _currentParameters.FirstTable;
            dataStorage.Tables.Remove(_currentParameters.FirstTable);
            if (_currentParameters.RemoveRightTable) dataStorage.Tables.Remove(_currentParameters.SecondTable);
            if (_currentParameters.RemoveRightKey)
            {
                var resultedSecondKey = GetAlias(_currentParameters.SecondKey);
                destination.Columns.Remove(resultedSecondKey);
            }
            dataStorage.Tables.Add(destination);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private DataTable JoinTables(DataTable leftTable, DataTable rightTable, string leftKey, string rightKey, bool outerForLeft, bool outerForRight)
        {
            var destination = new DataTable();

            foreach (DataColumn dc in leftTable.Columns)
            {
                var destinationColumn = new DataColumn {ColumnName = dc.ColumnName, DataType = dc.DataType};
                dc.CloneColumnPropertiesTo(destinationColumn);
                destination.Columns.Add(destinationColumn);
            }
            
            foreach (DataColumn dc in rightTable.Columns)
            {
                var destinationColumn = new DataColumn {ColumnName = GetAlias(dc.ColumnName), DataType = dc.DataType};
                dc.CloneColumnPropertiesTo(destinationColumn);
                destination.Columns.Add(destinationColumn);
            }
            var leftTableRows = leftTable.AsEnumerable();
            var rightTableRows = rightTable.AsEnumerable();

            var leftKeyValues = GetKeys(leftTableRows, leftKey).ToList();
            var rightKeyValues = GetKeys(rightTableRows, rightKey).ToList();

            var commonKeyValues = leftKeyValues.Intersect(rightKeyValues).ToList();
            var nullValues = new List<object> {null};
            var leftOnlyKeyValues = leftKeyValues.Except(commonKeyValues).Concat(nullValues).ToList();
            var rightOnlyKeyValues = rightKeyValues.Except(commonKeyValues).Concat(nullValues).ToList();

            var common = (from l in leftTableRows
                         join r in rightTableRows
                         on l.Field<object>(leftKey) equals r.Field<object>(rightKey)
                         where l.Field<object>(leftKey) != null && r.Field<object>(rightKey) != null
                         select CreateResultRow(l, r, destination)).ToList();
            
            if (outerForLeft)
            {
                var outerLeft = from l in leftTableRows
                                where leftOnlyKeyValues.Contains(l.Field<object>(leftKey))
                                select CreateResultRow(l, null, destination);
                common.AddRange(outerLeft);    
            }

            if (outerForRight)
            {
                var outerRight = from r in rightTableRows
                                 where rightOnlyKeyValues.Contains(r.Field<object>(rightKey))
                                 select CreateResultRow(null, r, destination);
                common.AddRange(outerRight);
            }
            
            foreach (var c in common)
            {
                destination.Rows.Add(c);
            }
            destination.Columns[leftKey].ColumnName = "_SortingColumn1ax";
            destination.Columns[GetAlias(rightKey)].ColumnName = "_SortingColumn2by";
            destination.DefaultView.Sort = "_SortingColumn1ax, _SortingColumn2by";
            var result = destination.DefaultView.ToTable();
            result.Columns["_SortingColumn1ax"].ColumnName = leftKey;
            result.Columns["_SortingColumn2by"].ColumnName = GetAlias(rightKey);
            leftTable.CloneTablePropertiesTo(result);
            rightTable.CloneTablePropertiesTo(result);
            return result;
        }

        public IEnumerable<object> GetKeys(IEnumerable<DataRow> t, string keyName)
        {
            return (from f in t select f.Field<object>(keyName)).Distinct();
        }

        public DataRow CreateResultRow(DataRow left, DataRow right, DataTable destinationTable)
        {
            var newRow = destinationTable.NewRow();
            if (left != null)
            {
                foreach (DataColumn c in left.Table.Columns)
                {
                    newRow[c.ColumnName] = left[c.ColumnName];
                }
            }
            if (right != null)
            {
                foreach (DataColumn c in right.Table.Columns)
                {
                    newRow[GetAlias(c.ColumnName)] = right[c.ColumnName];
                }
            }
            return newRow;
        }

        public string GetAlias(string name)
        {
            if (_currentParameters.SecondTableAliases == null) return name;
            var n = (from f in _currentParameters.SecondTableAliases where f.OriginalName == name select f.AliasName).ToList();
            return n.Any() ? n.Single() : name;
        }

        public class Parameters
        {
            public string FirstTable;
            public string FirstKey;
            public string SecondTable;
            public string SecondKey;
            public List<Alias> SecondTableAliases;
            public string JoinType;
            public bool RemoveRightTable;
            public bool RemoveRightKey;
        }

        public class Alias
        {
            public string OriginalName;
            public string AliasName;
        }
    }
}