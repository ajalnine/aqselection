﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Color = System.Windows.Media.Color;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_MarkNorm : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];

            var columnNames = processingDataTable.Columns.OfType<DataColumn>()
                                            .Select(a => a.ColumnName)
                                            .Where(a => (!a.StartsWith("#") && !a.EndsWith("_Цвет"))).ToList();
            foreach (var c in columnNames) GetColorColumn(processingDataTable, c);

            if (_currentParameters.AllRow)
            {
                
                foreach (DataRow dr in processingDataTable.Rows)
                {
                    var inRange = ProcessCaseAll(dr);
                    foreach (var c in columnNames)
                    {
                        if (_currentParameters.MarkInRange && inRange) dr[string.Format("#{0}_Цвет", c)] =  _currentParameters.ColorInRange.ToString();
                        if (_currentParameters.MarkOutRange && !inRange) dr[string.Format("#{0}_Цвет", c)] = _currentParameters.ColorOutRange.ToString();
                    }
                }
            }
            else
            {
                foreach (DataRow r in processingDataTable.Rows)
                {
                    foreach (DataColumn column in processingDataTable.Columns)
                    {
                        if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#")) continue;
                        if (r[column] == DBNull.Value) continue;
                        var minName = "#" + column.ColumnName + "_НГ";
                        var maxName = "#" + column.ColumnName + "_ВГ";
                        double? minValue = r.Table.Columns.Contains(minName)
                            ? r[minName] != DBNull.Value
                                ? (double?)r[minName]
                                : null
                            : null;
                        double? maxValue = r.Table.Columns.Contains(maxName)
                            ? r[maxName] != DBNull.Value
                                ? (double?)r[maxName]
                                : null
                            : null;

                        var check = true;
                        var c = column.ColumnName;
                        if (minValue.HasValue) check &= ((double?)r[column] >= minValue);
                        if (maxValue.HasValue) check &= ((double?)r[column] <= maxValue);
                        var rangeExists = minValue.HasValue || maxValue.HasValue;
                        if (_currentParameters.MarkInRange && check && rangeExists) r[string.Format("#{0}_Цвет", c)] = _currentParameters.ColorInRange.ToString();
                        if (_currentParameters.MarkOutRange && !check) r[string.Format("#{0}_Цвет", c)] = _currentParameters.ColorOutRange.ToString();
                    }
                }
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private bool ProcessCaseAll(DataRow r)
        {
            var allInRange = true;
            var anyOutRange = false;
            foreach (DataColumn column in r.Table.Columns)
            {
                if (column.DataType != typeof(double) || column.ColumnName.StartsWith("#")) continue;
                if (r[column] == DBNull.Value) continue;
                var minName = "#" + column.ColumnName + "_НГ";
                var maxName = "#" + column.ColumnName + "_ВГ";
                double? minValue = r.Table.Columns.Contains(minName)
                    ? r[minName] != DBNull.Value
                        ? (double?)r[minName]
                        : null
                    : null;
                double? maxValue = r.Table.Columns.Contains(maxName)
                    ? r[maxName] != DBNull.Value
                        ? (double?)r[maxName]
                        : null
                    : null;


                if (minValue.HasValue && r[column] != DBNull.Value)
                    allInRange &= ((double?)r[column] >= minValue);
                if (maxValue.HasValue && r[column] != DBNull.Value)
                    allInRange &= ((double?)r[column] <= maxValue);
                if (minValue.HasValue && r[column] != DBNull.Value)
                {
                    anyOutRange |= ((double?)r[column] < minValue);
                }
                if (maxValue.HasValue && r[column] != DBNull.Value)
                {
                    anyOutRange |= ((double?)r[column] > maxValue);
                }
            }
            return allInRange && !anyOutRange;
        }
        
        private static DataColumn GetColorColumn(DataTable processingDataTable, string fieldName)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == (fieldName + "_Цвет") || a.ColumnName == ("#" + fieldName + "_Цвет"));
            if (column != null) return column;
            var dc = new DataColumn
            {
                ColumnName = "#" + fieldName + "_Цвет",
                DataType = typeof(String)
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public Color ColorInRange;
            public Color ColorOutRange;
            public bool MarkInRange;
            public bool MarkOutRange;
            public bool AllRow;
        }
    }
}