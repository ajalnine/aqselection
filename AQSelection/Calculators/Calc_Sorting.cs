﻿using System.Globalization;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Sorting : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];

            var isFirst = true;
            var sortString = string.Empty;
            
            var oldColumns = new List<string>();
            
            var count = 0;
            for (var i=0; i<source.Columns.Count; i++)
            {
                var oldName = source.Columns[i].ColumnName;
                oldColumns.Add(oldName);
                var newName = "Name" + count.ToString(CultureInfo.InvariantCulture);
                source.Columns[i].ColumnName = newName;
                count++;
            }
            
            foreach (var s in _currentParameters.SortOrder)
            {
                if (!isFirst) sortString += ",";
                sortString += "Name" + oldColumns.IndexOf(s);
                sortString += (_currentParameters.IsAscending) ? "" : " DESC";
                isFirst = false;
            }

            source.DefaultView.Sort = sortString;
            var destination = source.DefaultView.ToTable();
            destination.TableName = _currentParameters.TableName;

            for (var i = 0; i < source.Columns.Count; i++)
            {
                destination.Columns[i].ColumnName = oldColumns[i];
            }
            
            source.CloneAllPropertiesTo(destination);
            dataStorage.Tables.Remove(source);
            dataStorage.Tables.Add(destination);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<string> SortOrder;
            public bool IsAscending;
        }
    }
}
