﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_CopyTable:ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);

            foreach (var p in _currentParameters.Copies.Where(c=>c.NeedToCopy))
            {
                var source = dataStorage.Tables[p.OriginalName];

                var destination = new DataTable();

                foreach (DataColumn column in source.Columns)
                {
                    destination.Columns.Add(new DataColumn { ColumnName = column.ColumnName, DataType = column.DataType });
                }
                foreach (DataRow row in source.Rows) destination.ImportRow(row);
                
                destination.TableName = p.CopyName;
                source.CloneAllPropertiesTo(destination);
                dataStorage.Tables.Add(destination);
            }
            return new StepProcessResult {Success = true, ResultedDataSet = dataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public List<CopyTableData> Copies;
        }

        public class CopyTableData
        {
            public string OriginalName { get; set; }
            public string CopyName { get; set; }
            public bool NeedToCopy { get; set; }
        }
    }
}
