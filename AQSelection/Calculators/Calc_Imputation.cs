﻿using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using System.Linq.Dynamic;
using AQMathClasses;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Imputation : ICalculator
    {
        Parameters _currentParameters;

        public delegate void InputeColumnDelegate(Imputation i, DataTable source);

        private Dictionary<ImputationMethod, InputeColumnDelegate> _inputer = new Dictionary
            <ImputationMethod, InputeColumnDelegate>
        {
            {ImputationMethod.None, InputationNone },
            {ImputationMethod.Avg, InputationAvg },
            {ImputationMethod.Med, InputationMed },
            {ImputationMethod.First, InputationFirst },
            {ImputationMethod.Last, InputationLast },
            {ImputationMethod.Int, InputationInt },
            {ImputationMethod.Zero, InputationZero },
            {ImputationMethod.HotDeck, InputationHotDeck }
        };

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            
            foreach (var col in _currentParameters.ImputationFields)
            {
                _inputer[col.Method].Invoke(col, source);
            }
            return new StepProcessResult{ Success = true, ResultedDataSet = dataStorage };
        }

        private static void InputationNone(Imputation i, DataTable source)
        {

        }

        private static void InputationAvg(Imputation i, DataTable source)
        {
            var data = source.AsEnumerable().Where(r=>r[i.FieldName]!=null && r[i.FieldName] != DBNull.Value).Select(r => r.Field<double>(i.FieldName)).OfType<object>().ToList();
            var avg = AQMath.AggregateMean(data);
            foreach (DataRow row in source.Rows)
            {
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value) row[i.FieldName] = avg;
            }
        }

        private static void InputationMed(Imputation i, DataTable source)
        {
            var data = source.AsEnumerable().Where(r => r[i.FieldName] != null && r[i.FieldName] != DBNull.Value).Select(r => r.Field<double>(i.FieldName)).OfType<object>().ToList();
            var med = AQMath.AggregateMedian(data);
            foreach (DataRow row in source.Rows)
            {
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value) row[i.FieldName] = med;
            }
        }

        private static void InputationFirst(Imputation i, DataTable source)
        {
            object firstValue = DBNull.Value;
            foreach (DataRow row in source.Rows)
            {
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value) row[i.FieldName] = firstValue;
                else firstValue = row[i.FieldName];
            }
        }

        private static void InputationLast(Imputation i, DataTable source)
        {
            object lastValue = DBNull.Value;
            for(var c = source.Rows.Count - 1; c>=0; c--)
            {
                var row = source.Rows[c];
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value) row[i.FieldName] = lastValue;
                else lastValue = row[i.FieldName];
            }
        }

        private static void InputationInt(Imputation i, DataTable source)
        {
            if (source.Columns[i.FieldName].DataType.Name == "DateTime")
            {
                var firstDate = DateTime.Now;
                var firstDatePosition = 0;
                var inDateWindow = false;
                for (var c = 0; c < source.Rows.Count; c++)
                {
                    var row = source.Rows[c];
                    if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value)
                    {
                        inDateWindow = true;
                    }
                    else
                    {
                        if (inDateWindow)
                        {
                            var lastDate = (DateTime) row[i.FieldName];
                            for (var j = firstDatePosition; j <= c; j++)
                            {
                                var range = (double) (lastDate - firstDate).Ticks;
                                var newval = range / (double)((double)c - (double)firstDatePosition) * ((double)j - (double)firstDatePosition); ;
                                source.Rows[j][i.FieldName] = firstDate + new TimeSpan((long)newval);
                            }
                        }
                        firstDate = (DateTime)row[i.FieldName];
                        firstDatePosition = c;
                        inDateWindow = false;
                    }
                }
                return;
            }
          
            var firstValue = 0.0d;
            var firstPosition = 0;
            var inWindow = false;
            for (var c = 0; c< source.Rows.Count; c++)
            {
                var row = source.Rows[c];
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value)
                {
                    inWindow = true;
                }
                else
                {
                    if (inWindow)
                    {
                        var lastValue = (double)row[i.FieldName];
                        for (var j = firstPosition; j <= c; j++)
                        {
                            source.Rows[j][i.FieldName] = firstValue + (lastValue - firstValue) / (double)((double)c - (double)firstPosition) * ((double)j - (double)firstPosition);
                        }
                    }
                    firstValue = (double)row[i.FieldName];
                    firstPosition = c;
                    inWindow = false;
                }
            }
        }

        private static void InputationZero(Imputation i, DataTable source)
        {
            foreach (DataRow row in source.Rows)
            {
                if (row[i.FieldName] == null || row[i.FieldName] == DBNull.Value) row[i.FieldName] = 0d;
            }
        }

        private static void InputationHotDeck(Imputation i, DataTable source)
        {
            InputationAvg(i, source);
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public List<Imputation> ImputationFields;
            public string TableName;
        }

        public enum ImputationMethod
        {
            None, Avg, Med, First, Last, Zero, Int, HotDeck
        }

        public class Imputation
        {
            public string FieldName;
            public ImputationMethod Method;
            public bool NAN;
            public bool NAND;
        }
    }
}

