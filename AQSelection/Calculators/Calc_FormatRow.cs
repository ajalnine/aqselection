﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Color = System.Windows.Media.Color;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_FormatRow : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];

            foreach (var rule in _currentParameters.Rules)
            {
                ProcessRule(rule, processingDataTable);
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessRule(RowFormatRule rule, DataTable table)
        {
            var ime = AQExpression.CompileExpression(rule.Code);
            if (ime == null) return;
            var columnNames =   table.Columns.OfType<DataColumn>()
                                            .Select(a => a.ColumnName)
                                            .Where(a => (!a.StartsWith("#") && !a.EndsWith("_Цвет"))).ToList();
            
            foreach (var c in columnNames) GetColorColumn(table, c);
            
            foreach (DataRow dr in table.Rows)
            {
                var result = ime.GetResult(dr);
                if (result == null || !(Boolean) result) continue;
                foreach (var c in columnNames) dr[string.Format("#{0}_Цвет", c)] = rule.Color.ToString();
            }
        }

        private static DataColumn GetColorColumn(DataTable processingDataTable, string fieldName)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == (fieldName + "_Цвет") || a.ColumnName == ("#" + fieldName + "_Цвет"));
            if (column != null) return column;
            var dc = new DataColumn
            {
                ColumnName = "#" + fieldName + "_Цвет",
                DataType = typeof(String)
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<RowFormatRule> Rules;
        }

        public class RowFormatRule
        {
            public string Expression;
            
            public string Code;

            public Color Color;
        }
    }
}