﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Collections.ObjectModel;
using Color = System.Windows.Media.Color;
using System.Linq;
using System.Data;
using System.Linq.Dynamic;
using System.Xml;
using System.Xml.Serialization;
using AQMathClasses;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Layers : ICalculator
    {
        Parameters _currentParameters;
        private DataTable _source;
        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            _source = dataStorage.Tables[_currentParameters.TableName];
            switch (_currentParameters.Operation)
            {
                case InteractiveOperations.CutInNewTable:
                    var rows1 = GetRowsForLayer();
                    var newTable1 = GetNewTableForLayers();
                    foreach (var row in rows1)
                    {
                        newTable1.ImportRow(row);
                        _source.Rows.Remove(row);
                    }
                    dataStorage.Tables.Add(newTable1);
                    break;

                case InteractiveOperations.CopyToNewTable:
                    var rows2 = GetRowsForLayer();
                    var newTable2 = GetNewTableForLayers();
                    foreach (var row in rows2)
                    {
                        newTable2.ImportRow(row);
                    }
                    dataStorage.Tables.Add(newTable2);
                    break;

                case InteractiveOperations.DeleteCase:
                    var rows3 = GetRowsForLayer();
                    foreach (var row in rows3)
                    {
                        _source.Rows.Remove(row);
                    }
                    break;

                case InteractiveOperations.SetLabel:
                    var labelColumn = GetLabelColumn(_currentParameters.YField);
                    var rows4 = GetRowsForLayer();
                    foreach (var row in rows4)
                    {
                        row[labelColumn] = _currentParameters.OptionalCommandArgument ?? DBNull.Value;
                    }
                    break;

                case InteractiveOperations.CombineLayers:
                    var layerColumn = GetColumn(_currentParameters.YField);
                    if (layerColumn == null) break;

                    var rows7 = GetRowsForLayer();
                    foreach (var row in rows7)
                    {
                        row[layerColumn] = _currentParameters.OptionalCommandArgument ?? DBNull.Value;
                    }
                    break;

                case InteractiveOperations.SetColor:
                    var rows5 = GetRowsForLayer();
                    var colorColumn = GetColorColumn(_currentParameters.YField);

                    switch (_currentParameters.ColorMode)
                    {
                        case ColorMode.SetForValue:
                            foreach (var row in rows5)
                            {
                                row[colorColumn] = _currentParameters.Color.ToString();
                            }
                            break;

                        case ColorMode.ResetFromValues:
                            foreach (var row in rows5)
                            {
                                row[colorColumn] = DBNull.Value;
                            }
                            break;

                        case ColorMode.SetForCase:
                            var columns = new List<string>();
                            foreach(DataColumn cl in _source.Columns)columns.Add(cl.ColumnName);
                                foreach (var c in columns)
                                {
                                    var fieldColorColumn = GetColorColumn(c);
                                    if (c.EndsWith("_Цвет"))continue;
                                    foreach (var row in rows5)
                                    {
                                        row[fieldColorColumn] = _currentParameters.Color.ToString();
                                    }
                                }
                            
                            break;

                        case ColorMode.ResetFromCases:
                            var columns2 = new List<string>();
                            foreach (DataColumn cl in _source.Columns) columns2.Add(cl.ColumnName);
                            foreach (var c in columns2)
                            {
                                if (c.EndsWith("_Цвет")) continue;
                                var fieldColorColumn = GetColorColumn(c);
                                foreach (var row in rows5)
                                {
                                    row[fieldColorColumn] = DBNull.Value;
                                }
                            }
                            break;
                    }
                    break;
                case InteractiveOperations.DeleteValue:
                    var rows8 = GetRowsForLayer();
                    foreach (var row in rows8)
                    {
                        row[_currentParameters.YField] = DBNull.Value;
                    }
                    break;
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
        
        private List<DataRow> GetRowsForLayer()
        {
            var res = new List<DataRow>();
            if (_source.Columns[_currentParameters.LayerField].DataType == typeof(double))
            {
                foreach (DataRow row in _source.Rows)
                {
                    if (row[_currentParameters.LayerField] == DBNull.Value)
                    {
                        if (_currentParameters.Layers.Contains(null)) res.Add(row);
                    }

                    else
                    {
                        foreach (var l in _currentParameters.Layers)
                        {
                            if (Math.Abs((double)row[_currentParameters.LayerField] - (double)l)<1e-10)res.Add(row);
                        }
                    }
                }
            }
            else
            {
                var layerNames = _currentParameters.Layers.Select(a => a.ToString()).ToList();
                foreach (DataRow row in _source.Rows)
                {
                    if (row[_currentParameters.LayerField] == DBNull.Value)
                    {
                        if (_currentParameters.Layers.Contains(null) || _currentParameters.Layers.Contains(string.Empty)) res.Add(row);
                    }

                    else
                    {
                        if (layerNames.Contains(row[_currentParameters.LayerField]?.ToString())) res.Add(row);
                    }
                }
            }

            return res;
        }

        private DataColumn GetColorColumn(string columnName)
        {
            var column = _source.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == (columnName + "_Цвет") || a.ColumnName == ("#" + columnName + "_Цвет"));
            if (column != null) return column;
            var dc = new DataColumn
            {
                ColumnName = "#" + columnName + "_Цвет",
                DataType = typeof(String)
            };
            _source.Columns.Add(dc);
            return dc;
        }

        private  DataColumn GetLabelColumn(string columnName)
        {
            var column = _source.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == (columnName + "_Метка") || a.ColumnName == ("#" + columnName + "_Метка"));
            if (column != null) return column;
            var dc = new DataColumn
            {
                ColumnName = "#" + columnName + "_Метка",
                DataType = typeof(String)
            };
            _source.Columns.Add(dc);
            return dc;
        }

        private DataColumn GetColumn(string columnName)
        {
            return _source.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == columnName);
        }

        private DataTable GetNewTableForLayers()
        {
            var destination = new DataTable();
            var tableName = _currentParameters.OptionalCommandArgument?.ToString();
            foreach (DataColumn column in _source.Columns)
            {
                destination.Columns.Add(new DataColumn { ColumnName = column.ColumnName, DataType = column.DataType });
            }
            
            destination.TableName = tableName;
            _source.CloneAllPropertiesTo(destination);
            return destination;
        }
        

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string LayerField;
            public string YField;
            public InteractiveOperations Operation;
            public object OptionalCommandArgument;
            public List<object> Layers;
            public ColorMode ColorMode;
            public Color Color;
        }

        public enum ColorMode
        {
            SetForCase, SetForValue, ResetFromCases, ResetFromValues
        }

        [Flags]
        public enum InteractiveOperations
        {
            None = 0,
            DeleteValue = 1,
            DeleteCase = 2,
            CutInNewTable = 4,
            CopyToNewTable = 8,
            SetColor = 0x10,
            SetColorForRow = 0x20,
            SetLabel = 0x40,
            SplitLayers = 0x80,
            DeleteColumn = 0x100,
            SetFilter = 0x200,
            DeleteFilter = 0x400,
            RemoveFilters = 0x800,
            CombineLayers = 0x1000,
            Outliers = 0x2000,
            OutOfRange = 0x4000,
            All = 0xffff
        }
    }
}
