﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_CheckExpression : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];

            var ime = AQExpression.CompileExpression(_currentParameters.Code);
            if (ime==null)return new StepProcessResult {Success = false, ResultedDataSet = dataStorage };
            
            var toDelete = (from DataRow dr in source.Rows let result = ime.GetResult(dr) where result == null || !(Boolean) result select dr).ToList();
            foreach (var dr in toDelete)
            {
                source.Rows.Remove(dr);
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string Expression;
            public string Code;
            public string InsertAfter;
            public string FieldName;
            public string FieldType;
        }
    }
}
