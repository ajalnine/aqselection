﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web;
using AQMathClasses;

namespace AQSelection.Calculators
{
    public static class AQExpression
    {
        public static IMathExpression CompileExpression(string expression)
        {
            var methodName = "ExpressionClass" + DateTime.Now.Ticks;
            var src = string.Format(@"
            using System;
            using System.Data;
            using AQMathClasses;
              
            public class {0}:IMathExpression
            {{
                public object GetResult(DataRow dr)
                {{
                    DataRow row = dr;
            {1}
                }}
            }}", methodName, expression);
            var cp = new Microsoft.CSharp.CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } });
            var p = new CompilerParameters { GenerateInMemory = true, GenerateExecutable = false };
            p.ReferencedAssemblies.Add("system.dll");
            p.ReferencedAssemblies.Add("system.xml.dll");
            p.ReferencedAssemblies.Add("system.data.dll");
            p.ReferencedAssemblies.Add(HttpContext.Current.Server.MapPath(@"\AQSelection\bin\aqmathclasses.dll"));
            var cr = cp.CompileAssemblyFromSource(p, new[] { src });
            if (cr.Errors.Count != 0 || cr.CompiledAssembly == null) return null;
            var objType = cr.CompiledAssembly.GetType(methodName);
            try
            {
                if (objType != null)
                {
                    return (IMathExpression)Activator.CreateInstance(objType);
                }
            }
            catch
            {
                return null;
            }
            return null;
        }
    }
}