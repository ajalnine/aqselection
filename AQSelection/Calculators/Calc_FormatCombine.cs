﻿using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_FormatCombine : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];

            foreach (DataColumn c in processingDataTable.Columns)
            {
                c.ExtendedProperties.Add("CellCombined", _currentParameters.ColumnsToCombine.Contains(c.ColumnName));
            }
            if (_currentParameters.EnableHierarchySubdivision) processingDataTable.ExtendedProperties.Add("EnableHierarchySubdivision", true);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public List<string> ColumnsToCombine;
            public bool EnableHierarchySubdivision;
        }
    }
}

