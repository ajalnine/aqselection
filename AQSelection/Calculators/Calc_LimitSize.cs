﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using AQMathClasses;
using System.Xml;
using System.Xml.Serialization;
using System.Linq.Dynamic;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_LimitSize : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            var destination = ConstructDestination(source);

            if (_currentParameters.Grouping != null && _currentParameters.Grouping.Count > 0)
            {
                var rows = source.AsEnumerable().AsQueryable();

                var isFirst = true;

                var groupString = "new (";
                foreach (var e in _currentParameters.Grouping)
                {
                    var index = source.Columns.IndexOf(e);
                    if (!isFirst) groupString += ",";
                    groupString += string.Format("ItemArray[{0}] as gr{0}", index);
                    isFirst = false;
                }
                groupString += ")";

                var selected = rows.GroupBy(groupString, "it").Select("new(ToList() as d)");

                foreach (var row in selected)
                {
                    var group = row.GetType().GetProperty("d").GetValue(row, null) as List<DataRow>;
                    ProcessSingleGroup(group, destination);
                }
            }
            else
            {
                var fullTable = source.AsEnumerable().ToList();
                ProcessSingleGroup(fullTable, destination);
            }
            dataStorage.Tables.Remove(source);
            dataStorage.Tables.Add(destination);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessSingleGroup( IList<DataRow> @group, DataTable destination)
        {
            
            var n = _currentParameters.Size;
            if (n < 1) return;
            if (@group.Count < n)
            {
                if (_currentParameters.DeleteSmallGroups) return;
                foreach (var t in @group) destination.ImportRow(t);
                return;
            }
            
            switch (_currentParameters.Mode)
            {
                case "First":
                    for (int i = 0; i < n; i++) destination.ImportRow((@group[i]));
                    break;

                case "Last":
                    for (int i = @group.Count - n; i < @group.Count; i++) destination.ImportRow((@group[i]));
                    break;

                case "Drop":
                    if (@group.Count==n) for (int i = 0; i < n; i++) destination.ImportRow((@group[i]));
                    break;

                case "Eq":
                    for (double i = 0; i < (double)@group.Count; i+=(double)(@group.Count-1)/(double)(n-1)) destination.ImportRow((@group[(int)Math.Round(i,0)]));
                    break;

                case "Rnd":
                    var r = new List<int>();
                    var count = 0;
                    while (count < n)
                    {
                        var rnd = (int)Math.Round((@group.Count-1) * AQMath.Rnd(), 0);
                        if (r.Contains(rnd)) continue;
                        r.Add(rnd);
                        count++;
                    }
                    foreach (int i in r.OrderBy(a => a))
                    {
                        destination.ImportRow((@group[i]));
                    }
                    break;

                case "Z":
                    var groupValues = (from x in @group select x[_currentParameters.ZField]).ToList();
                    var aggregateMean = AQMath.AggregateMean(groupValues);
                    if (aggregateMean == null) break;
                    
                    var mean = aggregateMean.Value;
                    var aggregateStDev = AQMath.AggregateStDev(groupValues);
                    if (aggregateStDev == null) break;
                    
                    var stDev = aggregateStDev.Value;
                    var startData = (from g in @group select new RowsWeights { Dr = g }).ToList();
                    var values = (from g in @group select (double)g[_currentParameters.ZField]).ToList();

                    var zValues = new Dictionary<double, double>();

                    foreach (var s in values.Distinct())
                    {
                        var normalCd = AQMath.NormalCD(s, mean, stDev);
                        if (normalCd != null) zValues.Add(s,  normalCd.Value);
                    }
                    foreach (var sd in startData)
                    {
                        sd.Value = (double)sd.Dr[_currentParameters.ZField];
                    }
                        
                    for (int i = startData.Count-1; i >= n; i--)
                    {
                        var toDelete = startData[0];
                        foreach (var x in startData)
                        {
                            var max = Double.NegativeInfinity;
                            for (int y = 0; y < startData.Count; y++)
                            {
                                if (y == i) continue;
                                var current = Math.Abs(y / ((double)startData.Count - 1) - zValues[startData[y].Value]);
                                if (current>max)max = current;
                            }
                            x.D = max;
                            if (toDelete.D < max)toDelete = x;
                        }

                        startData.Remove(toDelete);
                    }
                    foreach (var res in startData.Select(g => g.Dr))
                    {
                        destination.ImportRow(res);
                    }
                    
                    break;
            }
        }

        private static DataTable ConstructDestination(DataTable source)
        {
            var destination = new DataTable {TableName = source.TableName};

            foreach (DataColumn dc in source.Columns)
            {
                destination.Columns.Add(new DataColumn { DataType = dc.DataType, ColumnName = dc.ColumnName });
            }
            source.CloneAllPropertiesTo(destination);
            return destination;
        }


        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public List<string> Grouping;
            public int Size;
            public bool DeleteSmallGroups;
            public string Mode;
            public string ZField;
        }

        private class RowsWeights
        {
            public DataRow Dr;
            public double D;
            public double Value;
        }
    }
}
