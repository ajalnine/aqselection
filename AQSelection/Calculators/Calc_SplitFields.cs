﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_SplitFields : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            var destination = new DataTable {TableName = _currentParameters.TableName};

            foreach (DataColumn c in source.Columns)
            {
                var sfd = GetFieldData(c.ColumnName);
                if (sfd == null)
                {
                    var destinationColumn = new DataColumn {ColumnName = c.ColumnName, DataType = c.DataType};
                    c.CloneColumnPropertiesTo(destinationColumn);
                    destination.Columns.Add(destinationColumn);
                }
                else
                {
                    for (var i = 1; i <= sfd.Number; i++ )
                    {
                        var destinationColumn = sfd.IsDouble
                            ? new DataColumn {ColumnName = c.ColumnName + "_" + i, DataType = typeof (Double)}
                            : new DataColumn {ColumnName = c.ColumnName + "_" + i, DataType = c.DataType};
                        c.CloneColumnPropertiesTo(destinationColumn);
                        destination.Columns.Add(destinationColumn);
                    }
                }
            }

            foreach (DataRow sr in source.Rows)
            {
                var dr = destination.Rows.Add();

                foreach (DataColumn c in source.Columns)
                {
                    var sfd = GetFieldData(c.ColumnName);
                    if (sfd == null)
                    {
                        dr[c.ColumnName] = sr[c.ColumnName];
                    }
                    else
                    {
                        FillSplitData(c.ColumnName, sfd, sr[c.ColumnName].ToString(), dr);
                    }
                }
            }
            source.CloneTablePropertiesTo(destination);
            dataStorage.Tables.Remove(source);
            dataStorage.Tables.Add(destination);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        public void FillSplitData(string fieldName, SplitFieldData sfd, string value, DataRow dr)
        {
            if (!sfd.IsDouble)
            {
                var v = value.Split(sfd.Divider.ToCharArray()[0]);
                var i = 1;
                while (i <= v.Count() && i <= sfd.Number)
                {
                    var name = fieldName + "_" + i;
                    dr[name] = v[i - 1];
                    i++;
                }
            }
            else
            {
                if (!sfd.ProcessErrors)
                {
                    var v = value.Split(sfd.Divider.ToCharArray()[0]);
                    var i = 1;
                    var j = 1;
                    while (i <= v.Count() && i <= sfd.Number)
                    {
                        var name = fieldName + "_" + j;
                        Decimal r;
                        var val = Regex.Replace(v[i - 1], "[,.]", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                        if (Decimal.TryParse(val, out r))
                        {
                            dr[name] = r;
                            j++;
                        }
                        i++;
                    }
                }
                else
                {
                    var preV = TrySplit(value);
                    var v = ReSplit(preV);
                    var i = 1;
                    var j = 1;
                    while (i <= v.Count() && i <= sfd.Number)
                    {
                        var name = fieldName + "_" + j;
                        Decimal r;
                        var val = Regex.Replace(v[i - 1].Trim(), "[,.]", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                        if (Decimal.TryParse(val, out r))
                        {
                            dr[name] = r;
                            j++;
                        }
                        i++;
                    }
                }
            }
        }

        static List<string> TrySplit(string value)
        {
            var result = new List<string>();
            var divs = Regex.Replace(value, @"[0-9.\w]*", string.Empty);
            var c = divs.ToCharArray();
            if (c.Length > 0)
            {
                switch (c.GroupBy(a => a).Count())
                {
                    case 1:
                        result.AddRange(value.Split(c[0])
                            .Select(val => Regex.Replace(val.Trim(), "[,.]", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)));
                        return result;

                    case 0:
                        return result;

                    default:
                        var m = Regex.Matches(value, @"([0-9.]*)");
                        result.AddRange(from object val in m 
                                        select Regex.Replace(val.ToString().Trim(), "[,.]", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) into vn where vn != string.Empty select vn);
                        return result;
                }
            }
            result.Add(value);
            return result;
        }

        static List<string> ReSplit(List<string> v)
        {
            if (v.Count == 0) return v;
            var numbers = new List<decimal>();
            var i = 1;
            while (i <= v.Count())
            {
                Decimal r;
                var val = Regex.Replace(v[i - 1].Trim(), "[,.]", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                if (Decimal.TryParse(val, out r))
                {
                    numbers.Add(r);
                }
                i++;
            }
            if (numbers.Count == 0) return v;
            var min = numbers.Min().ToString(CultureInfo.InvariantCulture).Length;
            var max = numbers.Max().ToString(CultureInfo.InvariantCulture).Length;
            if ((min / (double)max) > 0.5)
            {
                return v;
            }
            var result = new List<string>();
            foreach (var n in numbers)
            {
                if (n.ToString(CultureInfo.InvariantCulture).Length / min >= 2)
                {
                    result.Add(n.ToString(CultureInfo.InvariantCulture).Substring(0, n.ToString(CultureInfo.InvariantCulture).Length / 2));
                    result.Add(n.ToString(CultureInfo.InvariantCulture).Substring(n.ToString(CultureInfo.InvariantCulture).Length / 2));
                }
                else
                {
                    result.Add(n.ToString(CultureInfo.InvariantCulture));
                }
            }
            return result;
        }

        public SplitFieldData GetFieldData(string fieldName)
        {
            return (from s in _currentParameters.SplitFields where s.FieldName == fieldName select s).SingleOrDefault();
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<SplitFieldData> SplitFields;
            public string TableName;
        }

        public class SplitFieldData
        {
            public string FieldName;
            public string Divider;
            public int Number;
            public bool ProcessErrors;
            public bool IsDouble;
        }
    }
}
