﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Linq.Dynamic;
using DataTable = System.Data.DataTable;
using AQMathClasses;

namespace AQSelection.Calculators
{
    // ReSharper disable once InconsistentNaming
    public class Calc_Aggregates : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];
            var insertPosition = processingDataTable.Columns.IndexOf(_currentParameters.InsertAfterField) + 1;
            foreach (var expressionfield in _currentParameters.AggregateFields)
            {
                CreateExpressionColumn(processingDataTable, expressionfield.FieldName, expressionfield.ExpressionType, insertPosition++);
            }

            dataStorage.Tables.Remove(processingDataTable);

            var destination = new DataTable();
            var groupTable  = new DataTable();
            foreach (DataColumn column in processingDataTable.Columns)
            {
                destination.Columns.Add(new DataColumn { ColumnName = column.ColumnName, DataType = column.DataType });
                groupTable.Columns.Add(new DataColumn { ColumnName = column.ColumnName, DataType = column.DataType });
            }
            destination.TableName = processingDataTable.TableName;
            groupTable.TableName = processingDataTable.TableName;
            processingDataTable.CloneAllPropertiesTo(destination);
            processingDataTable.CloneAllPropertiesTo(groupTable);
            dataStorage.Tables.Add(groupTable);
            var rows = processingDataTable.AsEnumerable().AsQueryable();

            var isFirst = true;
            var groupString = "new (";
            foreach (var e in _currentParameters.GroupingFields)
            {
                var index = processingDataTable.Columns.IndexOf(e);
                if (!isFirst) groupString += ",";
                groupString += string.Format("ItemArray[{0}] as gr{0}", index);
                isFirst = false;
            }
            groupString += ")";

            var selected = rows.GroupBy(groupString, "it").Select("new(ToList() as d)");
            var expressions = new Dictionary<string, IMathExpression>();
            foreach (var expressionfield in _currentParameters.AggregateFields)
            {
                var ime = AQExpression.CompileExpression(expressionfield.Code);
                if (ime != null) expressions.Add(expressionfield.FieldName, ime);
            }
            
            foreach (var rowGroup in selected)
            {
                groupTable.Rows.Clear();

                var group = rowGroup.GetType().GetProperty("d").GetValue(rowGroup, null) as List<DataRow>;
                if (group == null) continue;

                foreach (var row in group) groupTable.ImportRow(row);

                foreach (var expressionfield in _currentParameters.AggregateFields)
                {
                    if (!expressions.ContainsKey(expressionfield.FieldName)) continue;
                    var index = processingDataTable.Columns.IndexOf(expressionfield.FieldName);
                    ProcessAggregateField(expressionfield, index, groupTable, expressions[expressionfield.FieldName]);
                }

                foreach (DataRow resultRow in groupTable.Rows) destination.ImportRow(resultRow);
            }
            dataStorage.Tables.Remove(groupTable);
            dataStorage.Tables.Add(destination);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessAggregateField(AggregateFieldDescription rule, int index, DataTable table, IMathExpression ime)
        {
            if (ime == null) return;
            foreach (DataRow dr in table.Rows) dr[index] = ime.GetResult(dr)?? DBNull.Value;
        }

        private static void CreateExpressionColumn(DataTable processingDataTable, string fieldName, string fieldType, int index)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == fieldName);
            if (column != null) return;
            var dt = Type.GetType("System." + fieldType);
            var dc = new DataColumn
            {
                ColumnName = fieldName,
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            dc.SetOrdinal(index);
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string InsertAfterField;
            public ObservableCollection<AggregateFieldDescription> AggregateFields;
            public ObservableCollection<String> GroupingFields;
        }
        public class AggregateFieldDescription
        {
            public string FieldName;
            public string ExpressionType;
            public string Expression;
            public string Code;
        }
    }
}