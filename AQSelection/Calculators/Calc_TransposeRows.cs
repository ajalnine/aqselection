﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Linq.Dynamic;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_TransposeRows : ICalculator
    {
        private Parameters _currentParameters;
        private DataTable _source, _destination, _grouped;
        private List<string> _fixedFields;
        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            _source = dataStorage.Tables[_currentParameters.TableName];
            _destination = new DataTable();

            FillValuesForNoTableMode(); 
            
            var sourceFields = _source.Columns.OfType<DataColumn>().Select(a => a.ColumnName).ToList();
            
            var usedFields = new List<string>();
            foreach (var g in _currentParameters.TransposeGroups)
            {
                usedFields.AddRange(g.ValuesList.Split(new[] { "\r", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).Select(v => g.Prefix + v));
                usedFields.AddRange(g.ValuesList.Split(new[] { "\r", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).Select(v => "#" + g.Prefix + v + "_Цвет"));
                if (!g.DeleteSourceFields) continue;
                usedFields.Add(g.ValueField);
                usedFields.Add("#" + g.ValueField + "_Цвет");
                usedFields.Add(g.CategoryField);
            } 
            
            _fixedFields = sourceFields.Where(a => !usedFields.Contains(a)).ToList();

            ConstructDestinationColumns();

            foreach (DataRow row in _source.Rows)
            {
                ProcessRow(row);
            }

            _destination.TableName = _currentParameters.TableName;
            dataStorage.Tables.Remove(_source);
            _source.CloneTablePropertiesTo(_destination);
            dataStorage.Tables.Add(_destination);
            if (!_currentParameters.EnableGrouping) return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };

            GroupDataByUnusedFields();

            dataStorage.Tables.Remove(_destination);
            _destination.CloneTablePropertiesTo(_grouped);
            dataStorage.Tables.Add(_grouped);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void GroupDataByUnusedFields()
        {
            _grouped = new DataTable {TableName = _currentParameters.TableName};
            ConstructGroupedColumns();

            var rows = _destination.AsEnumerable().AsQueryable();

            var isFirst = true;
            var groupingFields = _fixedFields;
            var groupString = "new (";
            foreach (var e in groupingFields)
            {
                var index = _destination.Columns.IndexOf(e);
                if (!isFirst) groupString += ",";
                groupString += string.Format("ItemArray[{0}] as gr{0}", index);
                isFirst = false;
            }
            groupString += ")";

            var selected = rows.GroupBy(groupString, "it").Select("new(ToList() as d)");

            foreach (var row in selected)
            {
                var group = row.GetType().GetProperty("d").GetValue(row, null) as List<DataRow>;
                var dr = _grouped.Rows.Add();
                var m = 0;
                foreach (DataColumn gf in _grouped.Columns)
                {
                    var dataType = _destination.Columns[gf.ColumnName].DataType.ToString().ToLower();
                    var i = _destination.Columns.IndexOf(_destination.Columns[gf.ColumnName]);
                    dr[m] = AggregateColumn.First(@group, i, dataType);
                    m++;
                }
            }
        }

        private void FillValuesForNoTableMode()
        {
            foreach (var g in _currentParameters.TransposeGroups.Where(a=>a!=null))
            {
                if (g.Mode != TransposeMode.NoTable) continue;
                var transposeGroup = g;
                var values =
                    _source.Rows.OfType<DataRow>()
                        .Select(a => a[transposeGroup.CategoryField])
                        .Distinct()
                        .Where(a => a != null && !string.IsNullOrEmpty(a.ToString()));
                g.ValuesList = string.Empty;
                var isFirst = true;
                foreach (var v in values)
                {
                    if (!isFirst) g.ValuesList += "\r\n";
                    g.ValuesList += v;
                    isFirst = false;
                }
            }
        }

        private void ProcessRow(DataRow row)
        {
            var newRow = _destination.NewRow();
            foreach (var fixedField in _fixedFields)
            {
                newRow[fixedField] = row[fixedField];
            }
            
            foreach (var g in _currentParameters.TransposeGroups)
            {
                var fields = g.ValuesList.Split(new[] {"\r", "\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var f in fields)
                {
                    if (_destination.Columns.Contains(g.Prefix + f)
                        && row[g.CategoryField] != null
                        && row[g.CategoryField].ToString().Trim() == f.Trim())
                    {
                        newRow[g.Prefix + f] = row[g.ValueField];
                        var colorName = "#" + g.Prefix + f + "_Цвет";
                        var sourceColorName = "#" + g.ValueField + "_Цвет";
                        if (_destination.Columns.Contains(colorName) && _source.Columns.Contains(sourceColorName))newRow[colorName] = row[sourceColorName];
                    }
                }    
            }
            _destination.Rows.Add(newRow);
        }

        private void ConstructDestinationColumns()
        {
            foreach (var fixedField in _fixedFields)
            {
                var c = _source.Columns[fixedField];
                var destinationColumn = new DataColumn { ColumnName = c.ColumnName, DataType = c.DataType };
                c.CloneColumnPropertiesTo(destinationColumn);
                _destination.Columns.Add(destinationColumn);
            }

            foreach (var g in _currentParameters.TransposeGroups)
            {
                var sourceColumn = _source.Columns[g.ValueField];
                var fields = g.ValuesList.Split(new[] { "\r", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                
                foreach (var f in fields)
                {
                    var destinationColumn = new DataColumn { ColumnName = g.Prefix + f, DataType = sourceColumn.DataType };
                    sourceColumn.CloneColumnPropertiesTo(destinationColumn);
                    _destination.Columns.Add(destinationColumn);
                }

                if (_source.Columns.Contains("#" + sourceColumn + "_Цвет"))
                {
                    foreach (var f in fields)
                    {
                        var destinationColumn = new DataColumn { ColumnName = "#"+ g.Prefix + f + "_Цвет", DataType = typeof(string) };
                        _destination.Columns.Add(destinationColumn);
                    }
                }
            }
        }

        private void ConstructGroupedColumns()
        {
            foreach (DataColumn e in _destination.Columns)
            {
                var groupedColumn = new DataColumn { ColumnName = e.ColumnName, DataType = e.DataType };
                e.CloneColumnPropertiesTo(groupedColumn);
                _grouped.Columns.Add(groupedColumn);
            }
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<TransposeGroup> TransposeGroups;
            public string TableName;
            public bool EnableGrouping;
        }

        public class TransposeGroup
        {
            public TransposeMode Mode;
            public string CategoryField;
            public string ValueField;
            public string Prefix;
            public string ValuesList;
            public bool DeleteSourceFields;
        }
    }
    public enum TransposeMode
    {
        Auto, NoTable, Manual
    }
}
