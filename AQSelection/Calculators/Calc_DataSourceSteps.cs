﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DataSourceSteps: ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);

            foreach (var step in _currentParameters.CalculationDescription.StepData)
            {
                if (step.Group == "Параметры") continue;
                var t = Type.GetType("AQSelection.Calculators." + step.Calculator);
                if (t == null) return new StepProcessResult { Success = false, ResultedDataSet = dataStorage };
                var calculator = Activator.CreateInstance(t) as ICalculator;
                if (calculator==null) return new StepProcessResult { Success = false, ResultedDataSet = dataStorage };
                var s = calculator.ProcessData(dataStorage, step);
                if (!s.Success) return new StepProcessResult { Success = false, ResultedDataSet = dataStorage };
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }
        public class Parameters
        {
            public CalculationDescription CalculationDescription;
        }

        public class CalculationDescription
        {
            public int ID;
            public string Name;
            public string Parameters;
            public int TargetID = -1;
            public string TargetName = string.Empty;
            public bool CanBeDeleted = true;
            public List<Step> StepData;
            public List<DataItem> Results;
        }
    }
}