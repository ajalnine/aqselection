﻿using System;
using System.IO;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_ExpressionField : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];

            var ime = AQExpression.CompileExpression(_currentParameters.Code);
            if (ime==null)return new StepProcessResult {Success = false, ResultedDataSet = dataStorage };
            
            var indexOfFieldBefore = source.Columns[_currentParameters.InsertAfter].Ordinal;
            var newdc = new DataColumn { ColumnName = _currentParameters.FieldName, DataType = Type.GetType("System." + _currentParameters.FieldType) };
            source.Columns.Add(newdc);
            newdc.SetOrdinal(indexOfFieldBefore + 1);
            
            foreach (DataRow dr in source.Rows)
            {
                var result =  ime.GetResult(dr);
                try
                {
                    dr[_currentParameters.FieldName] = result ?? DBNull.Value;
                }
                catch
                {
                    return new StepProcessResult { Success = false, ResultedDataSet = dataStorage };
                }
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string Expression;
            public string Code;
            public string InsertAfter;
            public string FieldName;
            public string FieldType;
        }
    }
}
