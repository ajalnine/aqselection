﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_UnionTables : ICalculator
    {
        Parameters _currentParameters;
        bool _appendSourceName;
        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            _appendSourceName = !string.IsNullOrWhiteSpace(_currentParameters.SourceTableNameField);
            var sources = new Dictionary<string, DataTable>();
            if (_currentParameters.UseList)
            {
                foreach (var s in _currentParameters.ToUnion)
                {
                    sources.Add(s, dataStorage.Tables[s]);
                }
            }
            else
            {
                foreach (var s in dataStorage.Tables.Cast<DataTable>()
                        .Where(s => s.TableName.StartsWith(_currentParameters.NameStartsWith)))
                {
                    sources.Add(s.TableName, s);
                }
            }
            if (sources.Count == 0) return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };

            var destination = ConstructDestination(sources);
            
            foreach (var dt in sources.Values)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    var newrow = destination.NewRow();
                    var isEmpty = true;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var value = dr[dc.ColumnName];
                        if (value != DBNull.Value) isEmpty = false;
                        if (destination.Columns[dc.ColumnName].DataType.Name == "String" ) newrow[dc.ColumnName] = value.ToString();
                        else newrow[dc.ColumnName] = value;
                    }
                    if (_appendSourceName) newrow[_currentParameters.SourceTableNameField] = dt.TableName;
                    if (!isEmpty) destination.Rows.Add(newrow);
                }
            }
            
            dataStorage.Tables.Add(destination);
            if (!_currentParameters.DeleteSources) return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
            foreach (var dt in sources.Values)
            {
                dataStorage.Tables.Remove(dt);
            }
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }


        private DataTable ConstructDestination(Dictionary<string,DataTable> sources)
        {
            var resultFields = new List<DataField>();
            var destination = new DataTable { TableName = _currentParameters.ResultTableName };

            foreach (var i in sources.Values)
            {
                foreach (DataColumn f in i.Columns)
                {
                    if (resultFields.All(a => a.Name != f.ColumnName))
                    {
                        resultFields.Add(new DataField { Name = f.ColumnName, DataType = f.DataType.ToString() });
                    }
                    else
                    {
                        var field = resultFields.Single(a => a.Name == f.ColumnName);
                        if (field.DataType != f.DataType.ToString()) field.DataType = "System.String";
                    }
                }
            }
            
            foreach (var dc in resultFields)
            {
                destination.Columns.Add(new DataColumn { DataType = Type.GetType(dc.DataType), ColumnName = dc.Name });
            }
            if (_appendSourceName)
            {
                destination.Columns.Add(new DataColumn { DataType = typeof(string), ColumnName = _currentParameters.SourceTableNameField });
            }

            foreach (var i in sources.Values)
            {
                i.CloneAllPropertiesTo(destination);
            }
            return destination;
        }


        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public bool UseList;
            public List<string> ToUnion;
            public string NameStartsWith;
            public string ResultTableName;
            public bool DeleteSources;
            public string SourceTableNameField;
        }
    }
}
