﻿using System.Linq;
using System.IO;
using System.Collections.ObjectModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_ChangeFields : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];

            var existingNames = source.Columns.OfType<DataColumn>().Select(a => a.ColumnName);
            var usedNames =  _currentParameters.FieldTransformation.Select(a => a.OldFieldName);
            var toDelete = existingNames.Except(usedNames).ToList();

            foreach (var f in toDelete)
            {
                source.Columns.Remove(f);
            }

            var ordinal = 0;

            foreach (var f in usedNames)
            {
                source.Columns[f].SetOrdinal(ordinal);
                ordinal++;
            }

            foreach (var f in _currentParameters.FieldTransformation)
            {
                source.Columns[f.OldFieldName].ColumnName = f.NewFieldName;
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<string> DeletedFields;
            public ObservableCollection<NewTableData> FieldTransformation;
        }

         public class NewTableData
         {
             public string OldFieldName;
             public string NewFieldName;
         }
    }
}
