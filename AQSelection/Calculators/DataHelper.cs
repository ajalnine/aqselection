﻿using System.Data;
using System.Linq;

namespace AQSelection.Calculators
{
    public static class DataHelper
    {
        public static void CloneTablePropertiesTo(this DataTable source, DataTable destination)
        {
            if (source == null || destination == null) return;
            foreach (var key in source.ExtendedProperties.Keys)
            {
                if (destination.ExtendedProperties.ContainsKey(key))
                {
                    if (destination.ExtendedProperties[key] is bool && source.ExtendedProperties[key] is bool) destination.ExtendedProperties[key] = (bool)source.ExtendedProperties[key] | (bool)destination.ExtendedProperties[key];
                    else destination.ExtendedProperties[key] = source.ExtendedProperties[key];
                }
                else
                {
                    destination.ExtendedProperties.Add(key, source.ExtendedProperties[key]);
                }
            }
        }

        public static void CloneAllPropertiesTo(this DataTable source, DataTable destination)
        {
            if (source == null || destination == null) return;
            
            source.CloneTablePropertiesTo(destination);
            
            foreach (var column in source.Columns.Cast<DataColumn>().Where(column => destination.Columns.Contains(column.ColumnName)))
            {
                column.CloneColumnPropertiesTo(destination.Columns[column.ColumnName]);
            }
        }

        public static void CloneColumnPropertiesTo(this DataColumn source, DataColumn destination)
        {
            if (source == null || destination == null) return;
            foreach (var key in source.ExtendedProperties.Keys)
            {
                if (destination.ExtendedProperties.ContainsKey(key))
                {
                    if (destination.ExtendedProperties[key] is bool && source.ExtendedProperties[key] is bool) destination.ExtendedProperties[key] = (bool)source.ExtendedProperties[key] | (bool)destination.ExtendedProperties[key];
                    else destination.ExtendedProperties[key] = source.ExtendedProperties[key];
                }
                else
                {
                    destination.ExtendedProperties.Add(key, source.ExtendedProperties[key]);
                }
            }
        }
    }
}