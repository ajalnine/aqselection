﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_CombineFields : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            var destination = new DataTable {TableName = source.TableName};
            source.CloneTablePropertiesTo(destination);
            
            var processedFields = new List<string>();
            
            var operationInfo = new Dictionary<string, ResultingField>();

            foreach (DataColumn dc in source.Columns)
            {
                if (processedFields.Contains(dc.ColumnName)) continue;
                var currentGroup = GetFieldGroup(dc.ColumnName);

                if (currentGroup == null)
                {
                    var destinationColumn = new DataColumn {ColumnName = dc.ColumnName, DataType = dc.DataType};
                    destination.Columns.Add(destinationColumn);
                    dc.CloneColumnPropertiesTo(destinationColumn);
                    operationInfo.Add(dc.ColumnName, new ResultingField { Operation = AggregateType.Single, SourceForSingle = dc.ColumnName, SourceGroup = null });
                }
                else
                {
                    var dataType = dc.DataType;

                    var a1 = currentGroup.EmitMax ? 1 : 0;
                    var a2 = currentGroup.EmitMin ? 1 : 0;
                    var a3 = currentGroup.EmitAverage ? 1 : 0;
                    var a4 = currentGroup.EmitSum ? 1 : 0;
                    var a5 = currentGroup.EmitFirst ? 1 : 0;
                    var a6 = currentGroup.EmitCount ? 1 : 0;
                    var a7 = currentGroup.EmitLast ? 1 : 0;
                    var a8 = currentGroup.EmitRange ? 1 : 0;
                    var a9 = currentGroup.EmitList ? 1 : 0;
                    if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 != 1)
                    {
                        if (currentGroup.EmitMin)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Min",
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Min", new ResultingField { Operation = AggregateType.Min, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitMax)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Max",
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Max", new ResultingField { Operation = AggregateType.Max, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitRange)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Размах",
                                DataType = typeof (Double)
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Размах", new ResultingField { Operation = AggregateType.Range, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitAverage)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Среднее",
                                DataType = typeof (Double)
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Среднее", new ResultingField { Operation = AggregateType.Average, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitSum)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Сумма",
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            operationInfo.Add(currentGroup.Name + "_Сумма", new ResultingField { Operation = AggregateType.Sum, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitFirst)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Первое",
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            operationInfo.Add(currentGroup.Name + "_Первое", new ResultingField { Operation = AggregateType.First, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitLast)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Последнее",
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            operationInfo.Add(currentGroup.Name + "_Последнее", new ResultingField { Operation = AggregateType.Last, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitCount)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Число",
                                DataType = typeof (Int32)
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Число", new ResultingField { Operation = AggregateType.Count, SourceGroup = currentGroup });
                        }
                        if (currentGroup.EmitList)
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name + "_Список",
                                DataType = typeof(String)
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                            operationInfo.Add(currentGroup.Name + "_Список", new ResultingField { Operation = AggregateType.List, SourceGroup = currentGroup });
                        }
                    }
                    else
                    {
                        var operationType = AggregateType.Single;
                        if (currentGroup.EmitMin) operationType = AggregateType.Min;
                        else if (currentGroup.EmitMax) operationType = AggregateType.Max;
                        else if (currentGroup.EmitRange) operationType = AggregateType.Range;
                        else if (currentGroup.EmitAverage) operationType = AggregateType.Average;
                        else if (currentGroup.EmitSum) operationType = AggregateType.Sum;
                        else if (currentGroup.EmitFirst) operationType = AggregateType.First;
                        else if (currentGroup.EmitLast) operationType = AggregateType.Last;
                        else if (currentGroup.EmitCount) operationType = AggregateType.Count;
                        if (currentGroup.EmitList)
                        {
                            operationType = AggregateType.List;
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name,
                                DataType = typeof(String)
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                        }
                        else
                        {
                            var destinationColumn = new DataColumn
                            {
                                ColumnName = currentGroup.Name,
                                DataType = dataType
                            };
                            destination.Columns.Add(destinationColumn);
                            dc.CloneColumnPropertiesTo(destinationColumn);
                        }
                        operationInfo.Add(currentGroup.Name, new ResultingField { Operation = operationType, SourceGroup = currentGroup });
                    }
                    processedFields = processedFields.Concat(currentGroup.CombinedFields).ToList();
                }
            }
            foreach (DataRow r in source.Rows)
            {
                var d = destination.Rows.Add();
                foreach (var k in operationInfo.Keys)
                {
                    ProcessOperation(r, d, operationInfo[k], k);
                }
            }

            dataStorage.Tables.Remove(source);
            dataStorage.Tables.Add(destination);
            
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private static void ProcessOperation(DataRow sourceRow, DataRow destinationRow, ResultingField operation, string destinationField)
        {
            switch (operation.Operation)
            {
                case AggregateType.Single:
                    destinationRow[destinationField] = sourceRow[operation.SourceForSingle];
                    break;
                case AggregateType.Min:
                    destinationRow[destinationField] = AggregateRow.Min(sourceRow, operation.SourceGroup.CombinedFields);
                    break;
                case AggregateType.Max:
                    destinationRow[destinationField] = AggregateRow.Max(sourceRow, operation.SourceGroup.CombinedFields); 
                    break;
                case AggregateType.Range:
                    destinationRow[destinationField] = AggregateRow.Range(sourceRow, operation.SourceGroup.CombinedFields);
                    break;
                case AggregateType.Sum:
                    destinationRow[destinationField] = AggregateRow.Sum(sourceRow, operation.SourceGroup.CombinedFields); 
                    break;
                case AggregateType.Average:
                    destinationRow[destinationField] = AggregateRow.Average(sourceRow, operation.SourceGroup.CombinedFields); 
                    break;
                case AggregateType.First:
                    destinationRow[destinationField] = AggregateRow.First(sourceRow, operation.SourceGroup.CombinedFields); 
                    break;
                case AggregateType.Last:
                    destinationRow[destinationField] = AggregateRow.Last(sourceRow, operation.SourceGroup.CombinedFields);
                    break;
                case AggregateType.List:
                    destinationRow[destinationField] = AggregateRow.List(sourceRow, operation.SourceGroup.CombinedFields);
                    break;
                case AggregateType.Count:
                    destinationRow[destinationField] = AggregateRow.Count(sourceRow, operation.SourceGroup.CombinedFields); 
                    break;
            }
        }

        private class ResultingField
        {
            public FieldGroupData SourceGroup;
            public string SourceForSingle;
            public AggregateType Operation;
        }

        private enum AggregateType {Min, Max, Range, Average, Sum, Count, First, Last, Single, List};

        private FieldGroupData GetFieldGroup(string fieldName)
        {
            return _currentParameters.Groups.FirstOrDefault(g => g.CombinedFields.Contains(fieldName));
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public List<FieldGroupData> Groups;
            public FieldGroupData UnfinishedGroup;
            public string TableName;
        }

        public class FieldGroupData 
        {
            public List<string> CombinedFields;
            public string Name;
            public bool EmitFirst;
            public bool EmitLast;
            public bool EmitList;
            public bool EmitSum;
            public bool EmitRange;
            public bool EmitAverage;
            public bool EmitMin;
            public bool EmitMax;
            public bool EmitCount;
            public bool DisableAvg;
            public bool DisableSum;
        }
    }
}
