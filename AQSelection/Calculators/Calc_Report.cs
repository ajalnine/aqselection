﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Report: ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var appendedTables = new DataSet();
            foreach (DataTable t in dataStorage.Tables)
            {
                if (t.TableName.StartsWith("$"))appendedTables.Tables.Add(t.Copy());
            }
            foreach (DataTable todel in appendedTables.Tables)dataStorage.Tables.Remove(todel.TableName);

            var res = dataStorage;

            for (var i=0; i<= _currentParameters.InternalSteps.Count; i++)
            {
                var tablesToAppend = _currentParameters.NewTables.Where(a => a.PositionOfOperation == i);
                foreach (var table in tablesToAppend)
                {
                    if (appendedTables.Tables.Contains($"${i.ToString("000000")}" + table.NewTable.TableName))
                    {
                        var t = appendedTables.Tables[$"${i.ToString("000000")}" + table.NewTable.TableName];
                        t.TableName = table.NewTable.TableName;
                        appendedTables.Tables.Remove(t);
                        res.Tables.Add(t);
                    }
                }

                if (i < _currentParameters.InternalSteps.Count)
                {
                    var step = _currentParameters.InternalSteps[i];
                    var t = Type.GetType("AQSelection.Calculators." + step.Calculator);
                    if (t == null) return new StepProcessResult {Success = false, ResultedDataSet = res};
                    var calculator = Activator.CreateInstance(t) as ICalculator;
                    if (calculator == null) return new StepProcessResult {Success = false, ResultedDataSet = res};
                    var s = calculator.ProcessData(res, step);
                    if (!s.Success) return new StepProcessResult {Success = false, ResultedDataSet = res};
                    res = s.ResultedDataSet;
                }
            }
            
            return new StepProcessResult { Success = true, ResultedDataSet = res };
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public List<Step> InternalSteps;
            public List<GuidStepPair> TotalOperations;
            public List<OperationIndex> TableGeneratingOperations;
            public List<OperationIndex> ChartGeneratingOperations;
            public List<SerializableAnalysisResult> SARs;
            public List<NewTableIndex> NewTables;
            public string SelectedTable;
            public bool IsInvertedPanel;
            public bool LastOperationTableAdded;
            public ColorScales DefaultColorScale;
        }

        public class SerializableAnalysisResult
        {
            public bool OnlyInnerSteps;
            public Step AnalysisOperation;
            public string UsedTable;
            public List<string> UsedFields;
            public List<SLDataTable> InnerTables;
            public SlideElementDescription SED;
            public string IconSerializable;
            public ColorScales UsedColorScale;
            public bool IsInverted;
            public DateTime? From;
            public DateTime? To;
            public Guid AnalysisGuid;
        }

        public class GuidStepPair
        {
            public Step Operation;
            public Guid Guid;
        }

        public class OperationIndex
        {
            public int PositionOfOperation;
            public Guid Guid;
        }

        public class NewTableIndex
        {
            public int PositionOfOperation;
            public SLDataTable NewTable;
        }

        [Flags]
        public enum ColorScales
        {
            HSV2 = 1,
            HSV = 2,
            Blue = 3,
            Red = 4,
            RedGreen = 5,
            Gray = 6,
            Metal = 7,
            Warm = 8,
            Cold = 9,
            Protanopia = 10,
            Reversed = 0x100
        }
    }
}