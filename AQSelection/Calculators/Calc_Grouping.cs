﻿using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.Linq.Dynamic;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Grouping : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            var destination = ConstructDestination(source);

            var rows = source.AsEnumerable().AsQueryable();

            var isFirst = true;
            var groupingFields = _currentParameters.GroupingFields.Where(a => a.IsGrouping);
            string groupString;
            if (groupingFields.Any())
            {
                groupString = "new (";
                foreach (var e in groupingFields)
                {
                    var index = source.Columns.IndexOf(e.FieldName);
                    if (!isFirst) groupString += ",";
                    groupString += string.Format("ItemArray[{0}] as gr{0}", index);
                    isFirst = false;
                }
                groupString += ")";
            }
            else groupString = "new (1 as dumbGroup)";

            var selected = rows.GroupBy(groupString, "it").Select("new(ToList() as d)");

            foreach (var row in selected)
            {
                var group = row.GetType().GetProperty("d").GetValue(row, null) as List<DataRow>;
                var dr = destination.Rows.Add();
                var m = 0;
                foreach (var gf in _currentParameters.GroupingFields)
                {
                    var dataType = source.Columns[gf.FieldName].DataType.ToString().ToLower();
                    var i = source.Columns.IndexOf(source.Columns[gf.FieldName]);
                    if (gf.IsGrouping)
                    {
                        dr[m] = AggregateColumn.First(@group, i, dataType);
                        m++;
                        if (gf.EmitCount)
                        {
                            dr[m] = AggregateColumn.Count(@group, i, dataType);
                            m++;
                        } 
                    }
                    else
                    {
                        if (gf.EmitMin)
                        {
                            dr[m] = AggregateColumn.Min(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitMax)
                        {
                            dr[m] = AggregateColumn.Max(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitRange)
                        {
                            dr[m] = AggregateColumn.Range(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitAvg)
                        {
                            dr[m] = AggregateColumn.Average(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitMed)
                        {
                            dr[m] = AggregateColumn.Median(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitSum)
                        {
                            dr[m] = AggregateColumn.Sum(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitStdev)
                        {
                            dr[m] = AggregateColumn.Stdev(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitFirst)
                        {
                            dr[m] = AggregateColumn.First(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitLast)
                        {
                            dr[m] = AggregateColumn.Last(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitCount)
                        {
                            dr[m] = AggregateColumn.Count(@group, i, dataType);
                            m++;
                        }
                        if (gf.EmitList)
                        {
                            dr[m] = AggregateColumn.List(@group, i, dataType);
                            m++;
                        }
                    }
                }
            }
            
            dataStorage.Tables.Remove(source);
            dataStorage.Tables.Add(destination);
            return new StepProcessResult{ Success = true, ResultedDataSet = dataStorage };
        }

        private DataTable ConstructDestination(DataTable source)
        {
            var destination = new DataTable {TableName = source.TableName};
            source.CloneTablePropertiesTo(destination);

            foreach (var gf in _currentParameters.GroupingFields)
            {
                var dc = source.Columns[gf.FieldName];
                if (gf.IsGrouping)
                {
                    var destinationColumn = new DataColumn {DataType = dc.DataType, ColumnName = dc.ColumnName};
                    destination.Columns.Add(destinationColumn);
                    dc.CloneColumnPropertiesTo(destinationColumn);
                    if (gf.EmitCount) destination.Columns.Add(new DataColumn { DataType = dc.DataType, ColumnName = dc.ColumnName + "_Число" });
                }
                else
                {
                    var a1 = gf.EmitMax ? 1 : 0;
                    var a2 = gf.EmitMin ? 1 : 0;
                    var a3 = gf.EmitAvg ? 1 : 0;
                    var a4 = gf.EmitSum ? 1 : 0;
                    var a5 = gf.EmitStdev ? 1 : 0;
                    var a6 = gf.EmitCount ? 1 : 0;
                    var a7 = gf.EmitList ? 1 : 0;
                    var a8 = gf.EmitRange ? 1 : 0;
                    var a9 = gf.EmitFirst ? 1 : 0;
                    var a10 = gf.EmitLast ? 1 : 0;
                    var a11 = gf.EmitMed ? 1 : 0;

                    if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10 + a11 != 1)
                    {
                        if (gf.EmitMin)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType,
                                ColumnName = dc.ColumnName + "_Min"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitMax)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType,
                                ColumnName = dc.ColumnName + "_Max"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitRange)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_Размах"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitAvg)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_Среднее"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitMed)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_Медиана"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitSum)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_Сумма"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitStdev)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_СКО"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitFirst)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType,
                                ColumnName = dc.ColumnName + "[0]"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitLast)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType,
                                ColumnName = dc.ColumnName + "[n]"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitCount)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName + "_Число"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        if (gf.EmitList)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (String),
                                ColumnName = dc.ColumnName + "_Список"
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                    }
                    else
                    {
                        if (gf.EmitMin)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType, 
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitMax)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType, 
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitRange)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitAvg)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitMed)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitSum)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitStdev)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitFirst)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType, 
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitLast)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = dc.DataType, 
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitCount)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (Double),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                        else if (gf.EmitList)
                        {
                            var destinationColumn = new DataColumn
                            {
                                DataType = typeof (String),
                                ColumnName = dc.ColumnName
                            };
                            dc.CloneColumnPropertiesTo(destinationColumn); 
                            destination.Columns.Add(destinationColumn);
                        }
                    }
                }
            }
            return destination;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

       public class Parameters
        {
            public List<GroupTableData> GroupingFields;
            public string TableName;
            public ObservableCollection<string> GroupOrder;
        }

       public class GroupTableData 
       {
           public string FieldName;
           public bool IsGrouping;
           public bool EmitMin;
           public bool EmitMax;
           public bool EmitRange;
           public bool EmitAvg;
           public bool EmitMed;
           public bool EmitSum;
           public bool EmitStdev;
           public bool EmitCount;
           public bool EmitList;
           public bool EmitFirst;
           public bool EmitLast;
           public bool DisableAvg;
       }
    }
}

