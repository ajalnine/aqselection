﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Color = System.Windows.Media.Color;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_FormatScale : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];

            foreach (var rule in _currentParameters.Rules)
            {
                var dataColumn = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName== rule.FieldName);
                if (dataColumn == null) continue;
                var colorColumn = GetColorColumn(processingDataTable, rule.FieldName);
                var dataIndex = processingDataTable.Columns.IndexOf(dataColumn);
                var colorIndex = processingDataTable.Columns.IndexOf(colorColumn);
                ProcessRule(rule, dataIndex, colorIndex, processingDataTable);
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessRule(ScaleRule rule, int dataIndex, int colorIndex, DataTable table)
        {
            var rows = table.Rows.OfType<DataRow>().ToList();
            var dataType = table.Columns[dataIndex].DataType.ToString().ToLower();
            object min, max, avg;
            switch (rule.Mode)
            {
                case ScaleRuleMode.MinMax:

                    switch (dataType)
                    {
                        case "system.int32":
                        case "system.double":
                            min = AggregateColumn.Min(rows, dataIndex, dataType);
                            max = AggregateColumn.Max(rows, dataIndex, dataType);
                            avg = AggregateColumn.Average(rows, dataIndex, dataType);
                            foreach (var row in rows)
                            {
                                if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                                var c = GetColor(rule, (double)row[dataIndex], (double)min, (double)max, (double)avg);
                                if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                            }
                            return;

                        case "system.decimal":
                            min = AggregateColumn.Min(rows, dataIndex, dataType);
                            max = AggregateColumn.Max(rows, dataIndex, dataType);
                            avg = AggregateColumn.Average(rows, dataIndex, dataType);
                            foreach (var row in rows)
                            {
                                if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                                var c = GetColor(rule, Decimal.ToDouble((decimal)row[dataIndex]), 
                                                                 Decimal.ToDouble((decimal)min), 
                                                                 Decimal.ToDouble((decimal)max), 
                                                                 Decimal.ToDouble((decimal)avg));
                                if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                            }
                            return;

                        case "system.boolean":
                            foreach (var row in rows)
                            {
                                if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                                var c = GetColor(rule, (double)row[dataIndex]>0 ? 1.0d : 0.0d, 0.0d, 0.0d, 0.1d);
                                if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                            }
                            return;

                        case "system.dateTime":
                        case "system.timeSpan":
                            min = AggregateColumn.Min(rows, dataIndex, dataType);
                            max = AggregateColumn.Max(rows, dataIndex, dataType);
                            avg = AggregateColumn.Average(rows, dataIndex, dataType);
                            foreach (var row in rows)
                            {
                                if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                                var c = GetColor(rule, ((DateTime)row[dataIndex]).Ticks, 
                                                                 ((DateTime)min).Ticks,
                                                                 ((DateTime)max).Ticks, 
                                                                 ((DateTime)avg).Ticks);
                                if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                            }
                            return;

                        case "system.string":
                            var list = AggregateColumn.OrderedStringData(rows, dataIndex, dataType);
                            min = 0.0d;
                            max = (double)list.Count;
                            avg = (double)list.Count / 2;
                            foreach (var row in rows)
                            {
                                if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                                var c = GetColor(rule, list.IndexOf(row[dataIndex].ToString()), (double)min, (double)max, (double)avg);
                                if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                            }
                            return;
                    }
                    break;

                case ScaleRuleMode.Number:
                    min = rule.Min.HasValue ? rule.Min : AggregateColumn.Min(rows, dataIndex, dataType);
                    max = rule.Max.HasValue ? rule.Max : AggregateColumn.Max(rows, dataIndex, dataType);
                    avg = rule.Med.HasValue ? rule.Med : ((double)min + (double)max) / 2;
                    foreach (var row in rows)
                    {
                        if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                        var c = GetColor(rule, (double)row[dataIndex], (double)min, (double)max, (double)avg);
                        if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                    }
                    break;

                case ScaleRuleMode.Range:
                    min = AggregateColumn.Min(rows, dataIndex, dataType);
                    max = AggregateColumn.Max(rows, dataIndex, dataType);
                    
                    var ll = table.Columns.OfType<DataColumn>().FirstOrDefault(a => a.ColumnName.EndsWith(table.Columns[dataIndex].ColumnName + "_НГ"));
                    var llindex = ll!=null ? table.Columns.IndexOf(ll) : -1;
                    var ul = table.Columns.OfType<DataColumn>().FirstOrDefault(a => a.ColumnName.EndsWith(table.Columns[dataIndex].ColumnName + "_ВГ"));
                    var ulindex = ul!=null ? table.Columns.IndexOf(ul) : -1;
                    foreach (var row in rows)
                    {
                        if (row[dataIndex] == null || row[dataIndex] == DBNull.Value) continue;
                        if ((llindex < 0 || row[llindex] == null || row[llindex] == DBNull.Value) &&
                            (ulindex < 0 || row[ulindex] == null || row[ulindex] == DBNull.Value)) continue;

                        var rowmin = llindex < 0 || row[llindex] == null || row[llindex] == DBNull.Value ? (double?) min : (double?) row[llindex];
                        var rowmax = ulindex < 0 || row[ulindex] == null || row[ulindex] == DBNull.Value ? (double?) max : (double?) row[ulindex];
                        var rowavg = (rowmax + rowmin)/2;
                        var c = GetColor(rule, (double)row[dataIndex], (double)rowmin, (double)rowmax, (double)rowavg);
                        if (!string.IsNullOrEmpty(c)) row[colorIndex] = c;
                    }
                    break;
            }
        }

        private string GetColor(ScaleRule rule, double value, double min, double max, double avg)
        {
            if (value < min) return rule.Extended ? rule.Color1.ToString() : null;
            if (value > max) return rule.Extended ? rule.Color3.ToString() : null;
            return value < avg
                ? GetInterpolatedColor(rule.Color1, rule.Color2, (value - min) / (avg - min))
                : (value > avg
                    ? GetInterpolatedColor(rule.Color2, rule.Color3, (value - avg) / (max - avg))
                    : rule.Color2.ToString());
        }

        private string GetInterpolatedColor(Color color1, Color color2, double factor)
        {
            var a = color1.A + (color2.A - color1.A) * factor;
            var r = color1.R + (color2.R - color1.R) * factor;
            var g = color1.G + (color2.G - color1.G) * factor;
            var b = color1.B + (color2.B - color1.B) * factor;
            return Color.FromArgb((byte)a, (byte)r, (byte)g, (byte)b).ToString();
        }

        private static DataColumn GetColorColumn(DataTable processingDataTable, string fieldName)
        {
            var column = processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == ("#" + fieldName + "_Цвет") || a.ColumnName == (fieldName + "_Цвет"));
            if (column != null) return column;
            var dc = new DataColumn
            {
                ColumnName = "#" + fieldName + "_Цвет",
                DataType = typeof(String)
            };
            processingDataTable.Columns.Add(dc);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<ScaleRule> Rules;
        }

        public class ScaleRule
        {
            public string FieldName;

            public double? Min;
            
            public double? Max;

            public double? Med;

            public Color Color1;

            public Color Color2;
            
            public Color Color3;

            public ScaleRuleMode Mode;

            public bool Extended;
        }

        public enum ScaleRuleMode
        {
            MinMax, Range, Number
        }
    }
}