﻿using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_DataSourceSequence : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            
            var ime = AQExpression.CompileExpression(_currentParameters.Code);
            if (ime==null)return new StepProcessResult {Success = false, ResultedDataSet = dataStorage };

            var table = new DataTable {TableName = _currentParameters.TableName};
            dataStorage.Tables.Add(table);

            var newdc = new DataColumn { ColumnName = _currentParameters.SequenceName, DataType = Type.GetType("System." + _currentParameters.SequenceType) };
            table.Columns.Add(newdc);

            var res = ime.GetResult(dataStorage.Tables["Переменные"]?.Rows[0]);
            var list = res as IList;
            if (list != null)
            {
                var result = list;
                foreach (var r in result)
                {
                    var dr = table.NewRow();
                    dr[_currentParameters.SequenceName] = r ?? DBNull.Value;
                    table.Rows.Add(dr);
                }
            }
            else
            {
                var dr = table.NewRow();
                dr[_currentParameters.SequenceName] = res ?? DBNull.Value;
                table.Rows.Add(dr);
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }
       
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string Expression;
            public string Code;
            public string SequenceName;
            public string TableName;
            public string SequenceType;
        }
    }
}
