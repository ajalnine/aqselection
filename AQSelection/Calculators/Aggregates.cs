﻿using AQMathClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace AQSelection.Calculators
{
    public static class AggregateColumn
    {
        public static object Min(List<DataRow> data, int index, string dataType)
        {
            IComparable result = null;
            foreach (var a in (from dr in data where dr.ItemArray[index] != null select dr.ItemArray[index]).OfType<IComparable>())
            {
                if (result == null)
                {
                    result = a;
                }
                else if (a.CompareTo(result) == -1) result = a;
            }
            return (result == null) ? DBNull.Value : result as object;
        }

        public static object Max(List<DataRow> data, int index, string dataType)
        {
            IComparable result = null;
            foreach (var dr in data)
            {
                if (dr.ItemArray[index] == null) continue;
                var a = dr.ItemArray[index] as IComparable;
                if (a == null) continue;
                if (result == null)
                {
                    result = a;
                }
                else if (a.CompareTo(result) == 1) result = a;
            }
            return (result == null) ? DBNull.Value : result as object;
        }

        public static object Range(List<DataRow> data, int index, string dataType)
        {
            var min = Min(data, index, dataType) as double?;
            var max = Max(data, index, dataType) as double?;
            if (min.HasValue && max.HasValue) return max.Value - min.Value;
            return DBNull.Value;
        }

        public static object Average(List<DataRow> data, int index, string dataType)
        {
            Double? result = null;
            var count = 0;
            foreach (var dr in data.Where(dr => dr.ItemArray[index] != null))
            {
                double a;

                if (!double.TryParse(dr.ItemArray[index].ToString(), out a)) continue;
                if (result == null)
                {
                    result = a;
                }
                else
                {
                    result += a;
                }
                count++;
            }
            if (count == 0) return DBNull.Value;
            return (result == null) ? DBNull.Value : result / count as object;
        }

        public static object Median(List<DataRow> data, int index, string dataType)
        {
            var m = new List<double>();
            foreach (var dr in data.Where(dr => dr.ItemArray[index] != null))
            {
                double a;

                if (double.TryParse(dr.ItemArray[index].ToString(), out a))
                {
                    m.Add(a);
                }
            }
            var d = m.OrderBy(a => a);
            var n = m.Count();
            if (n == 0) return DBNull.Value;
            if (n == 1) return m.Single();
            var minIndex = (int)Math.Floor((n - 1) * 50 / 100d);
            if (n % 2 == 1) return d.ElementAt(minIndex);
            return (d.ElementAt(minIndex) + d.ElementAt(minIndex + 1)) / 2;
        }

        public static object Stdev(List<DataRow> data, int index, string dataType)
        {
            var avg = Average(data, index, dataType) as Double?;
            var cnt = 0;
            Double? result = null;
            if (avg == null) return DBNull.Value;
            if ((int)((double)Count(data, index, dataType)) <= 1) return 0;
            foreach (var dr in data.Where(dr => dr.ItemArray[index] != null))
            {
                double a;
                if (!double.TryParse(dr.ItemArray[index].ToString(), out a)) continue;
                if (result == null)
                {
                    result = Math.Pow(a - avg.Value, 2);
                }
                else
                {
                    result += Math.Pow(a - avg.Value, 2);
                }
                cnt++;
            }
            if (result != null) return Math.Sqrt(result.Value / (cnt - 1));
            return DBNull.Value;
        }

        public static object Sum(List<DataRow> data, int index, string dataType)
        {
            Double? result = null;
            foreach (var dr in data)
            {
                if (dr.ItemArray[index] == null) continue;
                double a;
                if (!double.TryParse(dr.ItemArray[index].ToString(), out a)) continue;
                if (result == null)
                {
                    result = a;
                }
                else
                {
                    result += a;
                }
            }
            return (result == null) ? DBNull.Value : result as object;
        }

        public static object List(List<DataRow> data, int index, string dataType)
        {
            String result = null;
            var isFirst = true;
            foreach (var dr in data)
            {
                if (dr.ItemArray[index] == null) continue;
                if (!isFirst) result += "; ";
                isFirst = false;
                result += dr.ItemArray[index].ToString();
            }
            return (result == null) ? DBNull.Value : result as object;
        }
        public static List<string> OrderedStringData(List<DataRow> data, int index, string dataType)
        {
            var result = (from dr in data where dr.ItemArray[index] != null select dr.ItemArray[index].ToString()).ToList();
            return result.OrderBy(a=>a).ToList();
        }

        public static object Count(List<DataRow> data, int index, string dataType)
        {
            double count = 0;
            foreach (var dr in data)
            {
                if (dr.ItemArray[index] != DBNull.Value) count++;
            }
            return count;
        }

        public static object First(List<DataRow> data, int index, string dataType)
        {
            for (var i = 0; i < data.Count; i++)
            {
                if (data[i][index] !=DBNull.Value ) return data[i][index];    
            }
            return DBNull.Value;
        }

        public static object Last(List<DataRow> data, int index, string dataType)
        {
            for (var i = data.Count - 1; i >= 0; i--)
            {
                if (data[i][index] != DBNull.Value) return data[i][index];
            }
            return DBNull.Value;
        }
    }

    public static class AggregateRow
    {
        public static object Min(DataRow dr, IEnumerable<string> fields)
        {
            IComparable result = null;
            foreach (var a in (from s in fields where dr.Field<object>(s) != null select dr.Field<object>(s)).OfType<IComparable>())
            {
                if (result == null)
                {
                    result = a;
                }
                else if (a.CompareTo(result) == -1) result = a;
            }
            return (result == null) ? DBNull.Value : result as object;
        }

        public static object Max(DataRow dr, IEnumerable<string> fields)
        {
            IComparable result = null;
            foreach (var a in (from s in fields where dr.Field<object>(s) != null select dr.Field<object>(s)).OfType<IComparable>())
            {
                if (result == null)
                {
                    result = a;
                }
                else if (a.CompareTo(result) == 1) result = a;
            }
            return (result == null) ? DBNull.Value : result as object;
        }

        public static object Average(DataRow dr, IEnumerable<string> fields)
        {
            Double? result = null;
            var count = 0;
            foreach (var a in fields.Where(s => dr.Field<object>(s) != null).Select(s => Double.Parse(dr.Field<object>(s).ToString())).Where(a => (double?)a != null))
            {
                if (result == null)
                {
                    result = a;
                }
                else
                {
                    result += a;
                }
                count++;
            }
            if (count == 0) return DBNull.Value;
            return (result == null) ? DBNull.Value : result / count as object;
        }

        public static object Range(DataRow dr, List<string> fields)
        {
            var min = Min(dr, fields) as Double?;
            var max = Max(dr, fields) as Double?;

            return (min == null || max == null) ? DBNull.Value : (max - min) as object;
        }

        public static object Count(DataRow dr, IEnumerable<string> fields)
        {
            return fields.Count(s => dr.Field<object>(s) != null);
        }

        public static object First(DataRow dr, IEnumerable<string> fields)
        {
            foreach (var v in fields.Select(dr.Field<object>).Where(v => v != null))
            {
                return v;
            }
            return DBNull.Value;
        }

        public static object Last(DataRow dr, IList<string> fields)
        {
            for (var i = fields.Count - 1; i >= 0; i--)
            {
                var v = dr.Field<object>(fields[i]);
                if (v != null) return v;
            }
            return DBNull.Value;
        }

        public static object List(DataRow dr, IList<string> fields)
        {
            var list = fields.Where(s => dr.Field<object>(s) != null).Select(s=>dr.Field<object>(s)).OfType<object>().ToList();
            if (!list.Any())return DBNull.Value;
            return AQMath.Concatenate(list, "; ");
        }

        public static object Sum(DataRow dr, List<string> fields)
        {
            if (dr.Table.Columns[fields.First()].DataType.Name.ToLower() == "string")
            {
                string result1 = null;
                foreach (var s in fields)
                {
                    if (dr.Field<object>(s) == null) continue;
                    var a = dr.Field<object>(s).ToString();
                    if (result1 == null)
                    {
                        result1 = a;
                    }
                    else
                    {
                        result1 += "; " + a;
                    }
                }
                return (result1 == null) ? DBNull.Value : result1 as object;
            }
            Double? result = null;
            foreach (var a in from s in fields where dr.Field<object>(s) != null select Double.Parse(dr.Field<object>(s).ToString()))
            {
                if (result == null)
                {
                    result = a;
                }
                else
                {
                    result += a;
                }
            }
            return (result == null) ? DBNull.Value : result as object;
        }
    }
}