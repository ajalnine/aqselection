﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Data;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using DataTable = System.Data.DataTable;

namespace AQSelection.Calculators
{
    // ReSharper disable once InconsistentNaming
    public class Calc_RecalcFields : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var processingDataTable = dataStorage.Tables[_currentParameters.TableName];
            foreach (var expressionfield in _currentParameters.ExpressionFields)
            {
                var column = GetExpressionColumn(processingDataTable, expressionfield.FieldName);
                var index = processingDataTable.Columns.IndexOf(column);

                if (column.DataType.ToString() == "System." + expressionfield.ExpressionType || expressionfield.ExpressionType == null)
                {
                    ProcessRule(expressionfield, index, processingDataTable);
                }
                else
                {
                    CreateExpressionColumn(processingDataTable, expressionfield.FieldName, expressionfield.ExpressionType, index);
                    ProcessRule(expressionfield, index, processingDataTable);
                    processingDataTable.Columns.Remove(column);
                    processingDataTable.Columns["tempname321123"].ColumnName = column.ColumnName;
                }
            }

            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private void ProcessRule(RecalcFieldDescription rule, int index, DataTable table)
        {
            var ime = AQExpression.CompileExpression(rule.Code);
            if (ime == null) return;
            foreach (DataRow dr in table.Rows)
            {
                 dr[index] = ime.GetResult(dr)?? DBNull.Value;
            }
        }

        private static DataColumn GetExpressionColumn(DataTable processingDataTable, string fieldName)
        {
            return processingDataTable.Columns.Cast<DataColumn>().SingleOrDefault(a => a.ColumnName == fieldName);
        }

        private static DataColumn CreateExpressionColumn(DataTable processingDataTable, string fieldName, string fieldType, int index)
        {
            var dt = Type.GetType("System." + fieldType);
            var dc = new DataColumn
            {
                ColumnName = "tempname321123",
                DataType = dt
            };
            processingDataTable.Columns.Add(dc);
            dc.SetOrdinal(index);
            return dc;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<RecalcFieldDescription> ExpressionFields;
        }

        public class RecalcFieldDescription
        {
            public string FieldName;
            public string Expression;
            public string Code;
            public string ExpressionType;
        }
    }
}