﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Linq.Dynamic;
using AQMathClasses;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
// ReSharper disable once InconsistentNaming
    public class Calc_Outliers : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var totalsource = dataStorage.Tables[_currentParameters.TableName];

            if (_currentParameters.LayerField == null)
            {
                var report = _currentParameters.GenerateReport ? ConstructReportTable(dataStorage.Tables[_currentParameters.TableName]) : null;
                var count = 1;
                if (_currentParameters.IsCyclic)
                {
                    while (FilterData(totalsource, report, count, null) > 0)
                    {
                        count++;
                        if (count > 100) return new StepProcessResult {Success = false};
                    }
                }
                else FilterData(totalsource, report, 1, null);

                if (_currentParameters.GenerateReport && report != null) dataStorage.Tables.Add(report);
                return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
            }

            var layers = totalsource.AsEnumerable().Select(a=>a.Field<object>(_currentParameters.LayerField)).Distinct().ToList();
            var dv = totalsource.DefaultView;
            var destination = dv.ToTable();
            destination.Clear();
            var totalreport = _currentParameters.GenerateReport ? ConstructReportTable(dataStorage.Tables[_currentParameters.TableName]) : null;
            var datatype = totalsource.Columns[_currentParameters.LayerField].DataType.ToString();
            var stringCompare = datatype.Contains("String");

            foreach (var l in layers)
            {
                
                var filter = stringCompare 
                    ?  "[" + _currentParameters.LayerField + "] LIKE  '" + (l ?? string.Empty).ToString().Replace("'", "''") + "'" 
                    : "[" + _currentParameters.LayerField + "] =" + l;
                dv.RowFilter = filter;
                var source = dv.ToTable();
                    
                var count = 1;
                var report = _currentParameters.GenerateReport ? ConstructReportTable(dataStorage.Tables[_currentParameters.TableName]) : null;
                if (_currentParameters.IsCyclic)
                {
                    while (FilterData(source, report, count, l?.ToString()) > 0)
                    {
                        count++;
                        if (count > 100) return new StepProcessResult { Success = false };
                    }
                }
                else FilterData(source, report, 1, l?.ToString());

                destination.Merge(source);
                if (totalreport!=null && report != null) totalreport.Merge(report);
            }
            dataStorage.Tables.Remove(totalsource);
            dataStorage.Tables.Add(destination);

            if (_currentParameters.GenerateReport && totalreport != null) dataStorage.Tables.Add(totalreport);
            return new StepProcessResult { Success = true, ResultedDataSet = dataStorage };
        }

        private bool CheckIsDataAvailable(int index, IQueryable<DataRow> source)
        {
            return source.Any(a => !string.IsNullOrEmpty(a.ItemArray[index].ToString()));
        }

        private int FilterData(DataTable source, DataTable report, int count, string layer)
        {
            var filteredCount = 0;
            var toRemoveRows = new List<DataRow>();
            var rows = source.AsEnumerable().AsQueryable();
            var fi = _currentParameters.Filtering.Where(f=>CheckIsDataAvailable(source.Columns.IndexOf(f), rows)).ToDictionary(f => f, f => new FilterInfo(source, f, _currentParameters));
            
            foreach (var dr in rows)
            {
                var toRemove = false;
                foreach (var f in fi.Keys)
                {
                    var o = fi[f];
                    var currentValue = dr[f] !=null && dr[f]!=DBNull.Value 
                        ? (double?)Convert.ToDouble(dr[f]) 
                        : null;
                    if ((currentValue == null || !(currentValue < o.DownLimit)) && !(currentValue > o.UpLimit))
                        continue;
                    filteredCount++;
                    if (_currentParameters.GenerateReport)
                    {
                        var r = report.NewRow();
                        foreach(var i in _currentParameters.IncludeInReport)
                        {
                            r[i] = dr[i];
                        }
                        if (layer!=null) r[_currentParameters.LayerField] = layer;
                        r["Поле выброса"] = f;
                        r["Значение выброса"] = currentValue;
                        r["Нижняя граница"] = o.DownLimit;
                        r["Верхняя граница"] = o.UpLimit;
                        r["Номер прохода"] = count;
                        r["Способ фильтрации"] = o.Method;
                        report.Rows.Add(r);
                    }
                    if (_currentParameters.RemoveValueOnly) dr[f] = DBNull.Value;
                    else
                    {
                        toRemove = true;
                        break;
                    }
                }
                if (toRemove && !_currentParameters.RemoveValueOnly) toRemoveRows.Add(dr);
            }
            foreach (var dr in toRemoveRows)
            {
                source.Rows.Remove(dr);
            }
            return filteredCount;
        }

        private DataTable ConstructReportTable(DataTable source)
        {
            var report = new DataTable {TableName = _currentParameters.ReportName};

            foreach (var dc in source.Columns.Cast<DataColumn>().Where(dc => _currentParameters.IncludeInReport.Contains(dc.ColumnName)))
            {
                report.Columns.Add(new DataColumn { DataType = dc.DataType, ColumnName = dc.ColumnName });
            }

            if (_currentParameters.LayerField != null)
            {
                report.Columns.Add(new DataColumn { ColumnName = _currentParameters.LayerField, DataType = typeof(String) });
            }

            report.Columns.Add(new DataColumn { ColumnName = "Поле выброса", DataType = typeof(String) });
            report.Columns.Add(new DataColumn { ColumnName = "Значение выброса", DataType = typeof(Double) });
            report.Columns.Add(new DataColumn { ColumnName = "Нижняя граница", DataType = typeof(Double) });
            report.Columns.Add(new DataColumn { ColumnName = "Верхняя граница", DataType = typeof(Double) });
            report.Columns.Add(new DataColumn { ColumnName = "Номер прохода", DataType = typeof(Double) });
            report.Columns.Add(new DataColumn { ColumnName = "Способ фильтрации", DataType = typeof(String) });
            return report;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;

            public List<string> Filtering;
            public string FilterMethod;
            public bool IsCyclic;
            public bool RemoveValueOnly;

            public double K;
            public double KZ;

            public bool GenerateReport;
            public string ReportName;
            public List<string> IncludeInReport;
            public string LayerField;
        }

        public class FilterInfo
        {
            public bool IsNormal;
            public double? DownLimit;
            public double? UpLimit;
            public string Method;

            public FilterInfo(DataTable dt, string fieldName, Parameters p)
            {
                List<object> v;
                if (dt.Columns[fieldName].DataType == typeof(double?)) v = (from a in dt.AsEnumerable().AsQueryable() select (double?)a[fieldName]).Cast<object>().ToList();
                else v = (from a in dt.AsEnumerable().AsQueryable()
                select (a[fieldName]!=null && a[fieldName] != DBNull.Value) ? (double?)Convert.ToDouble(a[fieldName]) : null).Cast<object>().ToList();
                

                switch (p.FilterMethod)
                {
                    case "AutoK":
                        IsNormal = AQMath.KolmogorovP(v) < 0.05;
                        break;
                    case "AutoAD":
                        IsNormal = AQMath.AndersonDarlingP(v) < 0.05;
                        break;
                    case "AutoSW":
                        IsNormal = AQMath.ShapiroWilkP(v) < 0.05;
                        break;
                    case "3sigma":
                        IsNormal = true;
                        break;
                    case "1.5H":
                        IsNormal = false;
                        break;
                }

                if (IsNormal)
                {
                    var av = AQMath.AggregateMean(v);
                    var st = AQMath.AggregateStDev(v);
                    DownLimit = av - st * p.KZ;
                    UpLimit = av + st * p.KZ;
                    Method = "±" + p.KZ + "σ";
                }
                else
                {
                    var iqr = AQMath.AggregateIQR(v);
                    DownLimit = AQMath.AggregatePercentile(v, 25) - iqr * p.K;
                    UpLimit = AQMath.AggregatePercentile(v, 75) + iqr * p.K;
                    Method = "±" + p.K + "H";
                }
            }
        }
    }
}
