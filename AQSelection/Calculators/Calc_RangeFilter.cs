﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection.Calculators
{
    public class Calc_RangeFilter : ICalculator
    {
        Parameters _currentParameters;

        public StepProcessResult ProcessData(DataSet dataStorage, Step stepInfo)
        {
            _currentParameters = ParseParameters(stepInfo.ParametersXML);
            var source = dataStorage.Tables[_currentParameters.TableName];
            if (_currentParameters.Case)
            {
                List<DataRow> toDeleteCases = new List<DataRow>();
                if (_currentParameters.All)
                {
                    foreach (DataRow r in source.Rows)
                    {
                        if (ProcessCaseAll(r) ^ _currentParameters.Include) toDeleteCases.Add(r);
                    }
                }
                else
                {
                    foreach (DataRow r in source.Rows)
                    {
                        if (ProcessCaseEvery(r) ^ _currentParameters.Include) toDeleteCases.Add(r);
                    }
                }
                foreach (DataRow c in toDeleteCases)
                {
                    source.Rows.Remove(c);
                }
            }
            else
            {
                foreach (DataRow r in source.Rows)
                {
                    foreach (var range in _currentParameters.Ranges)
                    {
                        if (!string.IsNullOrEmpty(range.Group) &&
                            r[_currentParameters.GroupField].ToString() != range.Group) continue;

                        if (r[range.Parameter] == DBNull.Value)continue;
                        var check = true;
                        if (range.Min.HasValue) check &= ((double?)r[range.Parameter] >= range.Min.Value);
                        if (range.Max.HasValue) check &= ((double?)r[range.Parameter] <= range.Max.Value);
                        if (!_currentParameters.Include) check = !check;
                        if (!check) r[range.Parameter] = DBNull.Value;
                    }
                }
            }
            return new StepProcessResult {Success = true, ResultedDataSet = dataStorage};
        }

        private bool ProcessCaseAll(DataRow r)
        {
            var check = true;
            foreach (var range in _currentParameters.Ranges)
            {
                if (!string.IsNullOrEmpty(range.Group) && r[_currentParameters.GroupField].ToString() != range.Group)
                    continue;
                if (range.Min.HasValue)
                    check &= r[range.Parameter] != DBNull.Value && ((double?) r[range.Parameter] >= range.Min.Value);
                if (range.Max.HasValue)
                    check &= r[range.Parameter] != DBNull.Value && ((double?) r[range.Parameter] <= range.Max.Value);
            }
            return check;
        }

        private bool ProcessCaseEvery(DataRow r)
        {
            foreach (var range in _currentParameters.Ranges)
            {
                if (!string.IsNullOrEmpty(range.Group) && r[_currentParameters.GroupField].ToString() != range.Group) continue;
                var inRange = true;
                if (range.Min.HasValue) inRange &= r[range.Parameter] != DBNull.Value && ((double?)r[range.Parameter] >= range.Min.Value);
                if (range.Max.HasValue) inRange &= r[range.Parameter] != DBNull.Value && ((double?)r[range.Parameter] <= range.Max.Value);
                if (inRange) return true;
            }
            return false;
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string GroupField;
            public string Groups;
            public bool Include;
            public bool Case;
            public bool All;
            public ObservableCollection<Range> Ranges;
        }

        public class Range
        {
            public double? Min;
            public double? Max;
            public string Parameter;
            public string Group;
        }
    }
}
