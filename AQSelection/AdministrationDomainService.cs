﻿
using System.Configuration;
using System.DirectoryServices;
using System.Web;

namespace AQSelection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using OpenRiaServices.DomainServices.EntityFramework;
    using OpenRiaServices.DomainServices.Hosting;
    using OpenRiaServices.DomainServices.Server;

    [RequiresAuthentication]
    [EnableClientAccess()]
    public class AdministrationDomainService : DbDomainService<raportEntities>
    {

        public IQueryable<AQ_News> GetAQ_News()
        {
            return this.DbContext.AQ_News;
        }

        public void InsertAQ_News(AQ_News aQ_News)
        {
            DbEntityEntry<AQ_News> entityEntry = this.DbContext.Entry(aQ_News);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_News.Add(aQ_News);
            }
        }

        public void UpdateAQ_News(AQ_News currentAQ_News)
        {
            this.DbContext.AQ_News.AttachAsModified(currentAQ_News, this.ChangeSet.GetOriginal(currentAQ_News), this.DbContext);
        }

        public void DeleteAQ_News(AQ_News aQ_News)
        {
            DbEntityEntry<AQ_News> entityEntry = this.DbContext.Entry(aQ_News);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_News.Attach(aQ_News);
                this.DbContext.AQ_News.Remove(aQ_News);
            }
        }

        public IQueryable<AQ_UserLog> GetAQ_UserLog()
        {
            return this.DbContext.AQ_UserLog;
        }

        public void InsertAQ_UserLog(AQ_UserLog aQ_UserLog)
        {
            DbEntityEntry<AQ_UserLog> entityEntry = this.DbContext.Entry(aQ_UserLog);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_UserLog.Add(aQ_UserLog);
            }
        }

        public void UpdateAQ_UserLog(AQ_UserLog currentAQ_UserLog)
        {
            this.DbContext.AQ_UserLog.AttachAsModified(currentAQ_UserLog, this.ChangeSet.GetOriginal(currentAQ_UserLog), this.DbContext);
        }

        public void DeleteAQ_UserLog(AQ_UserLog aQ_UserLog)
        {
            DbEntityEntry<AQ_UserLog> entityEntry = this.DbContext.Entry(aQ_UserLog);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_UserLog.Attach(aQ_UserLog);
                this.DbContext.AQ_UserLog.Remove(aQ_UserLog);
            }
        }

        public IQueryable<AQ_Users> GetAQ_Users()
        {
            return this.DbContext.AQ_Users;
        }

        public void InsertAQ_Users(AQ_Users aQ_Users)
        {
            DbEntityEntry<AQ_Users> entityEntry = this.DbContext.Entry(aQ_Users);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_Users.Add(aQ_Users);
            }
        }

        public void UpdateAQ_Users(AQ_Users currentAQ_Users)
        {
            this.DbContext.AQ_Users.AttachAsModified(currentAQ_Users, this.ChangeSet.GetOriginal(currentAQ_Users), this.DbContext);
        }

        public void DeleteAQ_Users(AQ_Users aQ_Users)
        {
            DbEntityEntry<AQ_Users> entityEntry = this.DbContext.Entry(aQ_Users);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_Users.Attach(aQ_Users);
                this.DbContext.AQ_Users.Remove(aQ_Users);
            }
        }

        public IQueryable<Liquidus> GetLiquidus()
        {
            return this.DbContext.Liquidus;
        }

        public void InsertLiquidus(Liquidus liquidus)
        {
            DbEntityEntry<Liquidus> entityEntry = this.DbContext.Entry(liquidus);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.Liquidus.Add(liquidus);
            }
        }

        public void UpdateLiquidus(Liquidus currentLiquidus)
        {
            this.DbContext.Liquidus.AttachAsModified(currentLiquidus, this.ChangeSet.GetOriginal(currentLiquidus), this.DbContext);
        }

        public void DeleteLiquidus(Liquidus liquidus)
        {
            DbEntityEntry<Liquidus> entityEntry = this.DbContext.Entry(liquidus);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.Liquidus.Attach(liquidus);
                this.DbContext.Liquidus.Remove(liquidus);
            }
        }

        #region Расширения операций по пользователям

        public AQ_Users GetAQ_User(string login)
        {
            try
            {

                var result = String.IsNullOrEmpty(login)
                    ? DbContext.AQ_Users.SingleOrDefault(a => a.Login == HttpContext.Current.User.Identity.Name.ToString())
                    : DbContext.AQ_Users.SingleOrDefault(a => a.Login == login);
                if (result == null)
                {
                    result = new AQ_Users();
                    DbContext.AQ_Users.Add(result);
                    result.Login = login;
                    DbContext.SaveChanges();
                }
                return result;
            }
            catch
            {
                return null;
            }
        }

        public string GetUserName(string login)
        {
            try
            {
                var domain = login.Split('\\')[0];
                var domainController = ConfigurationManager.AppSettings["Domain"];
                domainController = domainController.Replace("[subdomain]", domain);
                var de = new DirectoryEntry(domainController);
                var ds = new DirectorySearcher(de)
                {
                    Filter = "(&(objectCategory=user)(objectClass=person)(sAMAccountName=" + login.Split('\\')[1] + "))",
                    SearchScope = SearchScope.Subtree,
                    ReferralChasing = ReferralChasingOption.All
                };
                var sr = ds.FindOne();
                return sr?.Properties["CN"][0].ToString() ?? login;
            }
            catch
            {
                return login;
            }
        }

        public void RecheckDomainUserInfo(bool writeToLog)
        {
            foreach (var user in this.DbContext.AQ_Users)
            {
                try
                {
                    FillUserInfo(writeToLog, user);
                }
                catch
                {
                }
            }
            DbContext.SaveChanges();
        }

        public void RecheckDate(bool writeToLog)
        {
            foreach (var user in this.DbContext.AQ_Users)
            {
                try
                {
                    var lastVisited = DbContext.AQ_UserLog.Where(a => a.UserName == user.Login).Max(a => a.Date);
                    user.LastUpdated = lastVisited;
                    DbContext.AQ_Users.AttachAsModified(user, DbContext);
                }
                catch
                {
                }
            }
            DbContext.SaveChanges();
        }

        private void FillUserInfo(bool writeToLog, AQ_Users user)
        {
            var lastVisited = DbContext.AQ_UserLog.Where(a => a.UserName == user.Login).Max(a => a.Date);
            user.LastUpdated = lastVisited;
            string domain = user.Login.Split('\\')[0];
            string userLogin = user.Login.Split('\\')[1];
            string domainController = ConfigurationManager.AppSettings["Domain"];
            domainController = domainController.Replace("[subdomain]", domain);
            var de = new DirectoryEntry(domainController);
            var ds = new DirectorySearcher(de)
            {
                Filter = "(&(objectCategory=user)(objectClass=person)(sAMAccountName=" + userLogin + "))",
                SearchScope = SearchScope.Subtree,
                ReferralChasing = ReferralChasingOption.All
            };
            var sr = ds.FindOne();
            var oldUserWorkplace = user.Workplace;
            var newUserWorkplace = sr.Properties["Company"][0] + "; " + sr.Properties["Department"][0] +
                                   "; " + sr.Properties["WWWHOMEPAGE"][0];
            if (oldUserWorkplace != newUserWorkplace)
            {
                user.Workplace = newUserWorkplace;
                if (writeToLog && oldUserWorkplace != null)
                    WriteToLog(user.Login,
                        $"Workplace change detected: from {oldUserWorkplace} to {newUserWorkplace}");
            }
            var oldUserTitle = user.Title;
            var newUserTitle = sr.Properties["Title"][0].ToString();
            if (oldUserTitle != newUserTitle)
            {
                user.Title = newUserTitle;
                if (writeToLog && oldUserTitle != null)
                    WriteToLog(user.Login, $"Title change detected: from {oldUserTitle} to {newUserTitle}");
            }
            user.CachedUserName = sr.Properties["CN"][0].ToString();
            DbContext.AQ_Users.AttachAsModified(user, DbContext);
            //DbContext.SaveChanges();
        }

        private void WriteToLog(string login, string message)
        {
            var logRecord = new AQ_UserLog { UserName = login, Date = DateTime.Now, Data = message };
            var log = DbContext.AQ_UserLog;
            log.Add(logRecord);
            DbContext.SaveChanges();
        }

        #endregion
        
        #region Отчеты
        public List<DayLoad> GetDayLoad(string KeyWord)
        {
            List<DayLoad> Result = new List<DayLoad>();
            var Data = this.DbContext.AQ_UserLog.Where(c => c.Data.Contains(KeyWord)).OrderBy(d => d.Date);
            AQ_UserLog first = Data.First();
            DateTime LastDate = new DateTime(first.Date.Value.Year, first.Date.Value.Month, first.Date.Value.Day);
            int Counter = 1;
            foreach (AQ_UserLog a in Data)
            {
                DateTime d = new DateTime(a.Date.Value.Year, a.Date.Value.Month, a.Date.Value.Day);
                if (LastDate != d)
                {
                    Result.Add(new DayLoad() { Date = LastDate, Load = Counter });
                    LastDate = d;
                    Counter = 1;
                }
                else Counter++;
            }
            Result.Add(new DayLoad() { Date = LastDate, Load = Counter });

            return Result;
        }

        public List<DayLoad> GetDayUserCount()
        {
            List<DayLoad> Result = new List<DayLoad>();
            var Data = this.DbContext.AQ_UserLog.Where(c => c.Data.Contains("SL Version:")).OrderBy(d => d.Date);
            AQ_UserLog first = Data.First();
            DateTime LastDate = new DateTime(first.Date.Value.Year, first.Date.Value.Month, first.Date.Value.Day);
            List<string> Users = new List<string>();
            foreach (AQ_UserLog a in Data)
            {
                DateTime d = new DateTime(a.Date.Value.Year, a.Date.Value.Month, a.Date.Value.Day);
                if (LastDate != d)
                {
                    Result.Add(new DayLoad() { Date = LastDate, Load = Users.Count });
                    LastDate = d;
                    Users.Clear();
                }
                else
                {
                    if (!Users.Contains(a.UserName)) Users.Add(a.UserName);
                }
            }
            Result.Add(new DayLoad() { Date = LastDate, Load = Users.Count });

            return Result;
        }

        public List<UserActivity> GetUserActivity()
        {
            var Data = this.DbContext.AQ_UserLog.OrderBy(d => d.Date);
            Dictionary<string, UserActivity> Result = new Dictionary<string, UserActivity>();

            foreach (AQ_UserLog a in Data)
            {
                UserActivity cu;
                if (!Result.Keys.Contains(a.UserName))
                {
                    cu = new UserActivity() { Activity = 0, FirstAppeared = a.Date.Value, Login = a.UserName, LastSLVersion = 4, Errors = 0 };
                    Result.Add(a.UserName, cu);
                }
                else cu = Result[a.UserName];
                cu.Activity++;
                cu.LastRecordDate = a.Date.Value;
                if (a.Data.Contains("SL Version")) cu.LastKnownIP = a.Data.Substring(17, a.Data.Length - 17);
                if (a.Data.StartsWith("SL5 detected") || a.Data.Contains("SL Version: 5")) cu.LastSLVersion = 5;
                if (a.Data.StartsWith("Error")) cu.Errors++;
            }

            foreach (var c in Result.Values)
            {
                var user = this.DbContext.AQ_Users.Where(a => a.Login == c.Login).FirstOrDefault();
                if (user != null) c.UserName = user.CachedUserName;
            }

            return Result.Values.Where(a => a.Login != null && a.Login.Length > 0).OrderByDescending(b => b.FirstAppeared).ToList();
        }
        #endregion
    }

    public class DayLoad
    {
        [Key]
        public DateTime Date { get; set; }
        public double Load { get; set; }
    }

    public class UserActivity
    {
        [Key]
        public string Login { get; set; }
        public string UserName { get; set; }
        public DateTime FirstAppeared { get; set; }
        public DateTime LastRecordDate { get; set; }
        public string LastKnownIP { get; set; }
        public int LastSLVersion { get; set; }
        public int Activity { get; set; }
        public int Errors { get; set; }
    }
}


