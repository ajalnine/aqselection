﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Packaging;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;


namespace AQSelection
{
	public partial class PageReportDocx : System.Web.UI.Page
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			string ReportName = Server.UrlDecode (Request.QueryString["ReportName"] ?? "Отчет");
			string FName = Server.UrlDecode (Request.QueryString["FileName"] ?? "Отчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null));

			string SQL = Server.UrlDecode (Request.QueryString["SQL"] ?? "Select 'Запрос не указан' as 'Ошибка'");

			Response.Clear ();
			Response.ContentType = "application/msword";
			Encoding ResponseEncoding = Encoding.GetEncoding (1251);
			Encoding SourceEncoding = Encoding.Default;
			Response.AddHeader ("Content-Disposition", "attachment; filename=" + Server.UrlPathEncode (Encoding.GetEncoding (1251).GetString (Encoding.Default.GetBytes (FName + ".docx"))));
			string FileName = Path.GetTempFileName ();

			using (WordprocessingDocument doc = WordprocessingDocument.Create (FileName, WordprocessingDocumentType.Document))
			{
				doc.AddMainDocumentPart ();
				doc.MainDocumentPart.Document = new Document ();
				doc.MainDocumentPart.Document.Body = new Body ();

				#region Стили
				Style a0 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a0" };
				a0.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "20" }));
				a0.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style h0 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "h0" };
				h0.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "24" }));
				h0.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style a1 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a1" };
				a1.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "22" }));
				a1.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style a2 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a2" };
				a2.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "22" }));
				a2.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });
				a2.PrependChild<ParagraphProperties> (new ParagraphProperties () { Justification = new Justification () { Val = new EnumValue<JustificationValues> (JustificationValues.Center) } });

				StyleDefinitionsPart sdp = doc.MainDocumentPart.AddNewPart<StyleDefinitionsPart> ();
				sdp.Styles = new Styles ();
				sdp.Styles.AppendChild<Style> (a0);
				sdp.Styles.AppendChild<Style> (a1);
				sdp.Styles.AppendChild<Style> (a2);
				sdp.Styles.AppendChild<Style> (h0);
				sdp.Styles.Save (sdp.GetStream ());
				#endregion

				#region Таблица
				Table table = new Table ();

				TableProperties tblProp = new TableProperties (
					new TableBorders (
						new TopBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new BottomBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new LeftBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new RightBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new InsideHorizontalBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new InsideVerticalBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) }
					),
					new TableCellMargin ()
					{
						BottomMargin = new BottomMargin () { Width = "0", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						LeftMargin = new LeftMargin () { Width = "1000", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						RightMargin = new RightMargin () { Width = "1000", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						TopMargin = new TopMargin () { Width = "0", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) }
					},
					new TableIndentation () { Width = 0, Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
					new TableLook ()
				);


				table.AppendChild<TableProperties> (tblProp);



                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
				SqlConnection sconn = new SqlConnection (connectionString);
				sconn.Open ();
				SqlCommand sc = new SqlCommand (SQL, sconn);
				sc.CommandType = System.Data.CommandType.Text;
				SqlDataReader sdr = sc.ExecuteReader ();
				int RowNumber = 1;

				while (sdr.Read ())
				{
					if (RowNumber == 1)
					{
						TableRow trHeader = new TableRow ();
						TableRowProperties trProp = new TableRowProperties (new TableHeader (), new CantSplit ());
						trHeader.PrependChild<TableRowProperties> (trProp);

						TableRow trSubHeader = new TableRow ();
						TableRowProperties trProp2 = new TableRowProperties (new TableHeader (), new CantSplit ());
						trSubHeader.PrependChild<TableRowProperties> (trProp2);

						for (uint i = 0; i < sdr.FieldCount; i++)
						{
							TableCell tcHeader = new TableCell ();
							VerticalTextAlignmentOnPage avt = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
							tcHeader.PrependChild<TableCellProperties> (new TableCellProperties (avt));
							Paragraph pHeader = new Paragraph ();
							pHeader.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a0" } });
							string Name = sdr.GetName ((int)i).ToString ();
							pHeader.AppendChild<Run> (new Run (new Text (Name)));
							tcHeader.Append (pHeader);

							TableCell tcSubHeader = new TableCell ();
							VerticalTextAlignmentOnPage avst = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
							tcSubHeader.PrependChild<TableCellProperties> (new TableCellProperties (avst));
							Paragraph pSubHeader = new Paragraph ();
							pSubHeader.PrependChild<ParagraphProperties> (new ParagraphProperties () { Justification = new Justification () { Val = new EnumValue<JustificationValues> (JustificationValues.Center) }, ParagraphStyleId = new ParagraphStyleId () { Val = "h0" } });
							pSubHeader.AppendChild<Run> (new Run (new Text ((i + 1).ToString ())));
							tcSubHeader.Append (pSubHeader);

							TableCellProperties tcp = new TableCellProperties (new Shading () { Fill = "C0C0C0", Color = "Auto", Val = new EnumValue<ShadingPatternValues> (ShadingPatternValues.Clear) });
							tcSubHeader.Append (tcp);

							trHeader.Append (tcHeader);
							trSubHeader.Append (tcSubHeader);

						}
						table.Append (trHeader);
						table.Append (trSubHeader);
						RowNumber++;
					}
					TableRow tr = new TableRow ();
					TableRowProperties trP = new TableRowProperties (new CantSplit ());
					tr.AppendChild<TableRowProperties> (trP);

					for (int i = 0; i < sdr.FieldCount; i++)
					{

						TableCell tc = new TableCell ();
						VerticalTextAlignmentOnPage v = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
						tc.Append (new TableCellProperties (v));
						Paragraph p = new Paragraph ();
						if (sdr.GetFieldType ((int)i).Name.ToLower () == "boolean")
						{
							if (!sdr.IsDBNull ((int)i))
							{
								p.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
								if (sdr[(int)i].ToString () == "True")
								{
									Run r1 = new Run (new Text ("V"));
									RunProperties rp = new RunProperties (new Color () { Val = "0000FF" });
									r1.PrependChild<RunProperties> (rp);
									p.AppendChild<Run> (r1);

									tc.Append (p);

								}
								else
								{
									Run r1 = new Run (new Text ("X"));
									RunProperties rp = new RunProperties (new Color () { Val = "FF0000" });
									r1.PrependChild<RunProperties> (rp);
									p.AppendChild<Run> (r1);

									tc.Append (p);
								}
							}
							else
								tc.Append (p);
						}
						else
						{
							p.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a1" } });
							p.AppendChild<Run> (new Run (new Text (sdr[i].ToString ())));
							tc.Append (p);
						}


						tr.Append (tc);
					}
					table.Append (tr);
					RowNumber++;
				}
				sdr.Close ();
				sc.Dispose ();
				sconn.Close ();

				#endregion

				#region Титул

				Run r4 = new Run (new Text (ReportName));
                r4.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
				Paragraph pmain4 = new Paragraph (r4);
				pmain4.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
				pmain4.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "440", Before = "0" } });
				doc.MainDocumentPart.Document.Body.Append (pmain4);

				doc.MainDocumentPart.Document.Body.Append (table);
				# endregion

				#region Колонтитул, номер страницы и параметры документа
				HeaderPart hp = doc.MainDocumentPart.AddNewPart<HeaderPart> ();
				hp.Header = new Header ();

				Paragraph n = new Paragraph ();
				Tabs t = new Tabs ();
				t.AppendChild<TabStop> (new TabStop () { Position = 4677, Val = new EnumValue<TabStopValues> (TabStopValues.Center) });
				t.AppendChild<TabStop> (new TabStop () { Position = 9322, Val = new EnumValue<TabStopValues> (TabStopValues.Right) });
				n.AppendChild<ParagraphProperties> (new ParagraphProperties (t)
				{
					FrameProperties = new FrameProperties ()
					{
						Wrap = new EnumValue<TextWrappingValues> (TextWrappingValues.Around),
						VerticalPosition = new EnumValue<VerticalAnchorValues> (VerticalAnchorValues.Text),
						HorizontalPosition = new EnumValue<HorizontalAnchorValues> (HorizontalAnchorValues.Margin),
						Y = "1",
						XAlign = new EnumValue<HorizontalAlignmentValues> (HorizontalAlignmentValues.Center)
					},
					ParagraphStyleId = new ParagraphStyleId () { Val = "a0" }
				});

				Run pn1 = new Run ();
				FieldChar fc1 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.Begin) };
				pn1.AppendChild<FieldChar> (fc1);
				n.AppendChild<Run> (pn1);

				Run pn2 = new Run ();
				FieldCode fc2 = new FieldCode () { Text = "PAGE" };
				pn2.AppendChild<FieldCode> (fc2);
				n.AppendChild<Run> (pn2);

				Run pn3 = new Run ();
				FieldChar fc3 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.Separate) };
				pn3.AppendChild<FieldChar> (fc3);
				n.AppendChild<Run> (pn3);

				Run pn4 = new Run (new Text ("12"));
				n.AppendChild<Run> (pn4);

				Run pn5 = new Run ();
				FieldChar fc5 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.End) };
				pn5.AppendChild<FieldChar> (fc5);
				n.AppendChild<Run> (pn5);

				hp.Header.Append (n);

				Run rh = new Run (new Text (ReportName));
				Paragraph n2 = new Paragraph (rh);
				n2.PrependChild<ParagraphProperties> (new ParagraphProperties ()
				{
					ParagraphStyleId = new ParagraphStyleId () { Val = "a0" }
				}
				);
				hp.Header.Append (n2);

				hp.Header.Save (hp.GetStream ());


				SectionProperties sectPr = new SectionProperties (
																		new PageSize ()
																		{
																			Width = 16838,
																			Height = 11906,
																			Code = 9,
																			Orient = new EnumValue<PageOrientationValues> (PageOrientationValues.Landscape)
																		},
																		new PageMargin ()
																		{
																			Top = 851,
																			Right = 851,
																			Bottom = 851,
																			Left = 851,
																			Header = 452,
																			Footer = 0,
																			Gutter = 0
																		},
																		new TitlePage (),
																		new HeaderReference () { Id = doc.MainDocumentPart.GetIdOfPart (hp) },
																		new PageNumberType () { Start = 1 },
																		new DocGrid () { LinePitch = 360 },
																		new Columns () { Space = "708" }
																  );
				doc.MainDocumentPart.Document.Body.AppendChild<SectionProperties> (sectPr);

				doc.MainDocumentPart.Document.Save ();
				#endregion

			}
			Response.TransmitFile (FileName);
			Response.End ();
			File.Delete (FileName);
		}
	}
}