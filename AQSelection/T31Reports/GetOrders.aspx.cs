﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Packaging;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;


namespace AQSelection
{
	public partial class PageGetOrders : System.Web.UI.Page
	{
		int FirstPage = 1;

		protected void Page_Load (object sender, EventArgs e)
		{
			if (!int.TryParse (Request.QueryString["FirstPage"], out FirstPage))
				FirstPage = 1;

			Response.Clear ();
			Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
			Encoding ResponseEncoding = Encoding.GetEncoding (1251);
			Encoding SourceEncoding = Encoding.Default;
			Response.AddHeader ("Content-Disposition", "attachment; filename=" + Server.UrlPathEncode (Encoding.GetEncoding (1251).GetString (Encoding.Default.GetBytes ("Приложение №2 (Т).docx"))));
			string FileName = Path.GetTempFileName ();

			using (WordprocessingDocument doc = WordprocessingDocument.Create (FileName, WordprocessingDocumentType.Document))
			{
				doc.AddMainDocumentPart ();
				doc.MainDocumentPart.Document = new Document ();
				doc.MainDocumentPart.Document.Body = new Body ();

				#region Стили
				Style a0 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a0" };
				a0.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "24" }));
				a0.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style h0 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "h0" };
				h0.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "24" }));
				h0.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style a1 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a1" };
				a1.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "22" }));
				a1.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });

				Style a2 = new Style () { Type = new EnumValue<StyleValues> (StyleValues.Paragraph), StyleId = "a2" };
				a2.AppendChild<RunProperties> (new RunProperties (new PrimaryStyle (), new RunFonts () { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize () { Val = "22" }));
				a2.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "30" } });
				a2.PrependChild<ParagraphProperties> (new ParagraphProperties () { Justification = new Justification () { Val = new EnumValue<JustificationValues> (JustificationValues.Center) } });

				StyleDefinitionsPart sdp = doc.MainDocumentPart.AddNewPart<StyleDefinitionsPart> ();
				sdp.Styles = new Styles ();
				sdp.Styles.AppendChild<Style> (a0);
				sdp.Styles.AppendChild<Style> (a1);
				sdp.Styles.AppendChild<Style> (a2);
				sdp.Styles.AppendChild<Style> (h0);
				sdp.Styles.Save (sdp.GetStream ());
				#endregion

				#region Таблица
				Table table = new Table ();

				TableProperties tblProp = new TableProperties (
					new TableBorders (
						new TopBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new BottomBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new LeftBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new RightBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new InsideHorizontalBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) },
						new InsideVerticalBorder () { Val = new EnumValue<BorderValues> (BorderValues.Single) }
					),
					new TableCellMargin ()
					{
						BottomMargin = new BottomMargin () { Width = "0", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						LeftMargin = new LeftMargin () { Width = "1000", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						RightMargin = new RightMargin () { Width = "1000", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
						TopMargin = new TopMargin () { Width = "0", Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) }
					},
					new TableIndentation () { Width = 0, Type = new EnumValue<TableWidthUnitValues> (TableWidthUnitValues.Dxa) },
					new TableLook ()
				);


				table.AppendChild<TableProperties> (tblProp);


				TableRow trHeader = new TableRow ();
				TableRowProperties trProp = new TableRowProperties (new TableHeader (), new CantSplit ());
				trHeader.PrependChild<TableRowProperties> (trProp);

				TableRow trSubHeader = new TableRow ();
				TableRowProperties trProp2 = new TableRowProperties (new TableHeader (), new CantSplit ());
				trSubHeader.PrependChild<TableRowProperties> (trProp2);

				string[] Columns = new string[] { "№ п/п", "№ приказа", "Дата", "Наименование документа" };

				int FieldNumber = 1;
				foreach (string s in Columns)
				{
					TableCell tcHeader = new TableCell ();
					VerticalTextAlignmentOnPage avt = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
					tcHeader.PrependChild<TableCellProperties> (new TableCellProperties (avt));
					Paragraph pHeader = new Paragraph ();
					pHeader.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a0" } });
					pHeader.AppendChild<Run> (new Run (new Text (s)));
					tcHeader.Append (pHeader);

					TableCell tcSubHeader = new TableCell ();
					VerticalTextAlignmentOnPage avst = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
					tcSubHeader.PrependChild<TableCellProperties> (new TableCellProperties (avst));
					Paragraph pSubHeader = new Paragraph ();
					pSubHeader.PrependChild<ParagraphProperties> (new ParagraphProperties () { Justification = new Justification () { Val = new EnumValue<JustificationValues> (JustificationValues.Center) }, ParagraphStyleId = new ParagraphStyleId () { Val = "a0" } });
					pSubHeader.AppendChild<Run> (new Run (new Text (FieldNumber.ToString ())));
					tcSubHeader.Append (pSubHeader);

					TableCellProperties tcp = new TableCellProperties (new Shading () { Fill = "C0C0C0", Color = "Auto", Val = new EnumValue<ShadingPatternValues> (ShadingPatternValues.Clear) });
					tcSubHeader.Append (tcp);

					trHeader.Append (tcHeader);
					trSubHeader.Append (tcSubHeader);
					FieldNumber++;
				}
				table.Append (trHeader);
				table.Append (trSubHeader);

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
				SqlConnection sconn = new SqlConnection (connectionString);
				sconn.Open ();
				SqlCommand sc = new SqlCommand ("select Code, convert(nvarchar, EmitDate, 104) as EmitDate, Name from t31_orders where not (ADO is null and SO is null and ORS is null and SEMO is null and SorPO is null and LPO is null and OMIT is null and ONMKID is null and CEST is NULL and UOR is null) and not ((ADO=0 and ADO is not null) or (SO=0 and SO is not null) or (ORS=0 and ORS is not null) or (SEMO=0 and semo is not null) or (SorPO=0 and SorPo is not null) or (LPO=0 and LPO is not null) or (OMIT=0 and OMIT is not null) or (ONMKID=0 and ONMKID is not null) or (CEST=0 and CEST is not null) or (UOR=0 and UOR is not null)) and IsDeleted=0 order by Position", sconn);
				sc.CommandType = System.Data.CommandType.Text;
				SqlDataReader sdr = sc.ExecuteReader ();
				int RowNumber = 1;
				while (sdr.Read ())
				{
					TableRow tr = new TableRow ();
					TableRowProperties trP = new TableRowProperties (new CantSplit ());
					tr.AppendChild<TableRowProperties> (trP);

					TableCell tcn = new TableCell ();
					VerticalTextAlignmentOnPage vt = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
					tcn.Append (new TableCellProperties (vt));
					Paragraph pn = new Paragraph ();
					pn.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
					pn.AppendChild<Run> (new Run (new Text (RowNumber.ToString ())));
					tcn.Append (pn);

					tr.Append (tcn);

					for (int i = 0; i < sdr.FieldCount; i++)
					{
						TableCell tc = new TableCell ();
						VerticalTextAlignmentOnPage v = new VerticalTextAlignmentOnPage () { Val = new EnumValue<VerticalJustificationValues> (VerticalJustificationValues.Center) };
						tc.Append (new TableCellProperties (v));
						Paragraph p = new Paragraph ();
						p.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a1" } });
						p.AppendChild<Run> (new Run (new Text (sdr[i].ToString ())));
						tc.Append (p);

						tr.Append (tc);
					}
					table.Append (tr);
					RowNumber++;
				}
				sdr.Close ();
				sc.Dispose ();
				sconn.Close ();

				#endregion

				#region Титул

				Paragraph pmain1 = new Paragraph (new Run (new Text ("Приложение 2")));
				pmain1.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a1" }, Indentation = new Indentation () { Left = "7200" } });
				Paragraph pmain2 = new Paragraph (new Run (new Text ("к приказу № Т-31")));
				pmain2.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a1" }, Indentation = new Indentation () { Left = "7200" } });
				Text t1 = new Text (new string (' ', 6));
				t1.SetAttribute (new OpenXmlAttribute ("xml", "space", "http://www.w3.org/XML/1998/namespace", "preserve"));
				Run firstSpace = (new Run (t1));
				firstSpace.PrependChild<RunProperties> (new RunProperties (new Underline () { Val = new EnumValue<UnderlineValues> (UnderlineValues.Single) }));

				Text t2 = new Text (new string (' ', 14));
				t2.SetAttribute (new OpenXmlAttribute ("xml", "space", "http://www.w3.org/XML/1998/namespace", "preserve"));
				Run secondSpace = (new Run (t2));
				secondSpace.PrependChild<RunProperties> (new RunProperties (new Underline () { Val = new EnumValue<UnderlineValues> (UnderlineValues.Single) }));

				Paragraph pmain3 = new Paragraph (new Run (new Text ("от ")), firstSpace, new Run (new Text ("\\"), secondSpace, new Run (new Text (DateTime.Now.Year.ToString () + " г."))));
				pmain3.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a1" }, Indentation = new Indentation () { Left = "7200" } });

				doc.MainDocumentPart.Document.Body.Append (pmain1);
				doc.MainDocumentPart.Document.Body.Append (pmain2);
				doc.MainDocumentPart.Document.Body.Append (pmain3);

				Run r4 = new Run (new Text ("П Е Р Е Ч Е Н Ь"));
                r4.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
				Paragraph pmain4 = new Paragraph (r4);
				pmain4.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
				pmain4.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "0", Before = "1000" } });
				doc.MainDocumentPart.Document.Body.Append (pmain4);

				Run r5 = new Run (new Text ("технологических приказов, действующих на"));
                r5.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
				Paragraph pmain5 = new Paragraph (r5);
				pmain5.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
				doc.MainDocumentPart.Document.Body.Append (pmain5);

				Run r6 = new Run (new Text ("ПАО «ЧМК» и ЧФ ПАО «Уралкуз»"));
                r6.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
				Paragraph pmain6 = new Paragraph (r6);
				pmain6.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
				doc.MainDocumentPart.Document.Body.Append (pmain6);

				Paragraph pmain7 = new Paragraph (new Run (new Text ("(по состоянию на " + DateTime.Now.ToShortDateString () + ")")));
				pmain7.PrependChild<ParagraphProperties> (new ParagraphProperties () { ParagraphStyleId = new ParagraphStyleId () { Val = "a2" } });
				pmain7.PrependChild<ParagraphProperties> (new ParagraphProperties () { SpacingBetweenLines = new SpacingBetweenLines () { After = "300", Before = "0" } });
				doc.MainDocumentPart.Document.Body.Append (pmain7);
				doc.MainDocumentPart.Document.Body.Append (table);
				# endregion

				#region Колонтитул, номер страницы и параметры документа
				HeaderPart hp = doc.MainDocumentPart.AddNewPart<HeaderPart> ();
				hp.Header = new Header ();

				Paragraph n = new Paragraph ();
				Tabs t = new Tabs ();
				t.AppendChild<TabStop> (new TabStop () { Position = 4677, Val = new EnumValue<TabStopValues> (TabStopValues.Center) });
				t.AppendChild<TabStop> (new TabStop () { Position = 9322, Val = new EnumValue<TabStopValues> (TabStopValues.Right) });
				n.AppendChild<ParagraphProperties> (new ParagraphProperties (t)
				{
					FrameProperties = new FrameProperties ()
					{
						Wrap = new EnumValue<TextWrappingValues> (TextWrappingValues.Around),
						VerticalPosition = new EnumValue<VerticalAnchorValues> (VerticalAnchorValues.Text),
						HorizontalPosition = new EnumValue<HorizontalAnchorValues> (HorizontalAnchorValues.Margin),
						Y = "1",
						XAlign = new EnumValue<HorizontalAlignmentValues> (HorizontalAlignmentValues.Center)
					},
					ParagraphStyleId = new ParagraphStyleId () { Val = "a0" }
				});

				Run pn1 = new Run ();
				FieldChar fc1 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.Begin) };
				pn1.AppendChild<FieldChar> (fc1);
				n.AppendChild<Run> (pn1);

				Run pn2 = new Run ();
				FieldCode fc2 = new FieldCode () { Text = "PAGE" };
				pn2.AppendChild<FieldCode> (fc2);
				n.AppendChild<Run> (pn2);

				Run pn3 = new Run ();
				FieldChar fc3 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.Separate) };
				pn3.AppendChild<FieldChar> (fc3);
				n.AppendChild<Run> (pn3);

				Run pn4 = new Run (new Text ("12"));
				n.AppendChild<Run> (pn4);

				Run pn5 = new Run ();
				FieldChar fc5 = new FieldChar () { FieldCharType = new EnumValue<FieldCharValues> (FieldCharValues.End) };
				pn5.AppendChild<FieldChar> (fc5);
				n.AppendChild<Run> (pn5);

				hp.Header.Append (n);

				Run rh = new Run (new Text ("Продолжение приложения №2"));
				Paragraph n2 = new Paragraph (rh);
				n2.PrependChild<ParagraphProperties> (new ParagraphProperties ()
				{
					ParagraphStyleId = new ParagraphStyleId () { Val = "a0" }
				}
				);
				hp.Header.Append (n2);

				hp.Header.Save (hp.GetStream ());


				SectionProperties sectPr = new SectionProperties (
																		new PageSize ()
																		{
																			Width = 11906,
																			Height = 16838,
																			Code = 9
																		},
																		new PageMargin ()
																		{
																			Top = 1134,
																			Right = 851,
																			Bottom = 851,
																			Left = 1134,
																			Header = 452,
																			Footer = 0,
																			Gutter = 0
																		},
																		new TitlePage (),
																		new HeaderReference () { Id = doc.MainDocumentPart.GetIdOfPart (hp) },
																		new PageNumberType () { Start = FirstPage },
																		new DocGrid () { LinePitch = 360 },
																		new Columns () { Space = "708" }
																  );
				doc.MainDocumentPart.Document.Body.AppendChild<SectionProperties> (sectPr);

				doc.MainDocumentPart.Document.Save ();
				#endregion

			}
			Response.TransmitFile (FileName);
			Response.End ();
			File.Delete (FileName);
		}
	}
}