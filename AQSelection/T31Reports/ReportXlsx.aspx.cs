﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Packaging;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;


namespace AQSelection
{
	public partial class PageReportXlsx : System.Web.UI.Page
	{
		protected void Page_Load (object sender, EventArgs e)
		{
			string ReportName = Server.UrlDecode (Request.QueryString["ReportName"] ?? "Отчет");
			string SQL = Server.UrlDecode (Request.QueryString["SQL"] ?? "Select 'Запрос не указан' as 'Ошибка'");
			string FName = Server.UrlDecode (Request.QueryString["FileName"] ?? "Отчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null));

			Response.Clear ();
			Response.ContentType = "application/msexcel";
			Encoding ResponseEncoding = Encoding.GetEncoding (1251);
			Encoding SourceEncoding = Encoding.Default;
			Response.AddHeader ("Content-Disposition", "attachment; filename=" + Server.UrlPathEncode (Encoding.GetEncoding (1251).GetString (Encoding.Default.GetBytes (FName + ".xlsx"))));
			string FileName = Path.GetTempFileName ();

			using (SpreadsheetDocument doc = SpreadsheetDocument.Create (FileName, SpreadsheetDocumentType.Workbook))
			{
				WorkbookPart main = doc.AddWorkbookPart ();
				main.Workbook = new Workbook ();
				WorksheetPart newWorksheetPart = main.AddNewPart<WorksheetPart> ();

				newWorksheetPart.Worksheet = new Worksheet (new SheetData ());
				Sheets sheets = new Sheets ();
				main.Workbook.AppendChild<Sheets> (sheets);
				string relationshipId = main.GetIdOfPart (newWorksheetPart);
				Sheet sheet = new Sheet ();
				sheets.AppendChild<Sheet> (sheet);
				sheet.Id = relationshipId;
				sheet.SheetId = 1;
				sheet.Name = "Отчет по Т31";
				newWorksheetPart.Worksheet.Save ();

				WorkbookStylesPart wsp = doc.WorkbookPart.AddNewPart<WorkbookStylesPart> ();
				Stylesheet ss = new Stylesheet ();
				ss.Fonts = new Fonts ();
				ss.Fonts.AppendChild<Font> (new Font () { });

				Font Red = new Font ();
				Red.AppendChild<Color> (new Color () { Rgb = "FFFF0000" });
				ss.Fonts.AppendChild<Font> (Red);

				Font Blue = new Font ();
				Blue.AppendChild<Color> (new Color () { Rgb = "FF0000FF" });
				ss.Fonts.AppendChild<Font> (Blue);

				ss.Fills = new Fills ();
				ss.Fills.AppendChild<Fill> (new Fill () { });
				ss.Borders = new Borders ();
				ss.Borders.AppendChild<Border> (new Border () { });

				ss.CellStyleFormats = new CellStyleFormats ();
				ss.CellStyleFormats.AppendChild<CellFormat> (new CellFormat () { NumberFormatId = 0, FontId = 0, BorderId = 0, FillId = 0 });
				ss.NumberingFormats = new NumberingFormats ();
				ss.NumberingFormats.AppendChild (new NumberingFormat () { FormatCode = "mm/dd/yy;@", NumberFormatId = 165 });
				ss.CellFormats = new CellFormats ();
				ss.CellFormats.AppendChild (new CellFormat () { NumberFormatId = 0, FontId = 0, BorderId = 0, FillId = 0, FormatId = 0 });
				ss.CellFormats.AppendChild (new CellFormat () { NumberFormatId = 165, FontId = 0, BorderId = 0, FillId = 0, FormatId = 1, ApplyNumberFormat = true });
				CellFormat cf1 = new CellFormat () { NumberFormatId = 0, FontId = 1, BorderId = 0, FillId = 0, FormatId = 0, ApplyAlignment = true };
				cf1.AppendChild<Alignment> (new Alignment () { Horizontal = new EnumValue<HorizontalAlignmentValues> () { Value = HorizontalAlignmentValues.Center } });
				ss.CellFormats.AppendChild (cf1);

				CellFormat cf2 = new CellFormat () { NumberFormatId = 0, FontId = 2, BorderId = 0, FillId = 0, FormatId = 0, ApplyAlignment = true };
				cf2.AppendChild<Alignment> (new Alignment () { Horizontal = new EnumValue<HorizontalAlignmentValues> () { Value = HorizontalAlignmentValues.Center } });
				ss.CellFormats.AppendChild (cf2);

				ss.CellStyles = new CellStyles ();
				ss.CellStyles.AppendChild (new CellStyle () { FormatId = 0, Name = "a0", BuiltinId = 0 });
				ss.DifferentialFormats = new DifferentialFormats ();
				wsp.Stylesheet = ss;
				ss.Save (wsp.GetStream ());

				#region Таблица

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
				SqlConnection sconn = new SqlConnection (connectionString);
				sconn.Open ();
				SqlCommand sc = new SqlCommand (SQL, sconn);
				sc.CommandType = System.Data.CommandType.Text;
				SqlDataReader sdr = sc.ExecuteReader ();
				uint RowNumber = 1;
				while (sdr.Read ())
				{
					if (RowNumber == 1)
					{
						for (uint i = 0; i < sdr.FieldCount; i++)
						{
							AddCell (sdr.GetName ((int)i), CellValues.String, i + 1, 1, newWorksheetPart, 0);
						}
						RowNumber++;
					}
					for (uint i = 0; i < sdr.FieldCount; i++)
					{
						string t = sdr.GetFieldType ((int)i).Name.ToLower ();
						switch (t)
						{
							case "int32":
								if (!sdr.IsDBNull ((int)i))
									AddCell (sdr[(int)i].ToString (), CellValues.Number, i + 1, RowNumber, newWorksheetPart, 0);
								else
									AddCell ("", CellValues.String, i + 1, RowNumber, newWorksheetPart, 0);
								break;

							case "datetime":
								if (!sdr.IsDBNull ((int)i))
								{
									DateTime a = DateTime.Parse (sdr[(int)i].ToString ());
									AddCell (a.ToOADate ().ToString ().Replace (',', '.'), CellValues.Number, i + 1, RowNumber, newWorksheetPart, 1);
								}
								else
								{
									AddCell ("", CellValues.String, i + 1, RowNumber, newWorksheetPart, 0);
								}
								break;

							case "boolean":
								if (sdr.IsDBNull ((int)i))
								{
									AddCell ("", CellValues.String, i + 1, RowNumber, newWorksheetPart, 0);
									break;
								}
								if (sdr[(int)i].ToString () == "True")
									AddCell ("V", CellValues.String, i + 1, RowNumber, newWorksheetPart, 3);
								else
									AddCell ("X", CellValues.String, i + 1, RowNumber, newWorksheetPart, 2);
								break;

							case "string":
								AddCell (sdr[(int)i].ToString (), CellValues.String, i + 1, RowNumber, newWorksheetPart, 0);
								break;

							default:
								AddCell (sdr[(int)i].ToString (), CellValues.Number, i + 1, RowNumber, newWorksheetPart, 0);
								break;
						}
					}
					AddCell ("", CellValues.String, (uint)sdr.FieldCount + 1, RowNumber, newWorksheetPart, 0);
					RowNumber++;
				}
				sdr.Close ();
				sc.Dispose ();
				sconn.Close ();

				#endregion

				newWorksheetPart.Worksheet.Save ();
				main.Workbook.Save ();
			}
			Response.TransmitFile (FileName);
			Response.End ();
			File.Delete (FileName);
		}

		private void AddCell (string text, CellValues cv, uint columnIndex, uint rowIndex, WorksheetPart worksheetPart, uint StyleIndex)
		{
			Worksheet worksheet = worksheetPart.Worksheet;
			SheetData sheetData = worksheet.GetFirstChild<SheetData> ();
			string columnName = GetColumnName (columnIndex);

			string cellReference = columnName + rowIndex;

			Row row;
			if (sheetData.Elements<Row> ().Where (r => r.RowIndex == rowIndex).Count () != 0)
			{
				row = sheetData.Elements<Row> ().Where (r => r.RowIndex == rowIndex).First ();
			}
			else
			{
				row = new Row () { RowIndex = rowIndex };
				sheetData.Append (row);
			}
			Cell newCell;

			if (row.Elements<Cell> ().Where (c => c.CellReference.Value == columnName + rowIndex).Count () > 0)
			{
				newCell = row.Elements<Cell> ().Where (c => c.CellReference.Value == cellReference).First ();
			}
			else
			{

				Cell refCell = null;
				foreach (Cell cell in row.Elements<Cell> ())
				{
					if (string.Compare (cell.CellReference.Value, cellReference, true) > 0)
					{
						refCell = cell;
						break;
					}
				}

				newCell = new Cell () { CellReference = cellReference };
				row.InsertBefore (newCell, refCell);

			}
			newCell.StyleIndex = StyleIndex;
			newCell.CellValue = new CellValue (text);
			newCell.DataType = new EnumValue<CellValues> (cv);
		}

		private string GetColumnName (uint number)
		{
			if (number > 65535)
				throw new System.Exception ();
			string Name = String.Empty;
			uint div;
			while (number > 0)
			{
				div = number % 25;
				number /= 25;
				Name = Encoding.Default.GetString (new byte[] { (byte)((Encoding.Default.GetBytes ("A"))[0] + div - 1) }) + Name;
			}
			return Name;
		}
	}
}