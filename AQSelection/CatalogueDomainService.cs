﻿
namespace AQSelection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using OpenRiaServices.DomainServices.EntityFramework;
    using OpenRiaServices.DomainServices.Hosting;
    using OpenRiaServices.DomainServices.Server;


    [RequiresAuthentication]
    [EnableClientAccess()]
    public class CatalogueDomainService : DbDomainService<raportEntities>
    {
        public IQueryable<AQ_CatalogueItems> GetAQ_CatalogueItems(string orderBy)
        {
            IQueryable<AQ_CatalogueItems> r;
            switch (orderBy)
            {
                default:
                case "Name":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenBy(a => a.Name);
                    break;
                case "RevisionAuthor":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenBy(a => a.AQ_CatalogueRevisions.OrderByDescending(b => b.RevisionDate).FirstOrDefault().RevisionAuthor);
                    break;
                case "RevisionDate":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenByDescending(a => a.AQ_CatalogueRevisions.Max(b => b.RevisionDate));
                    break;
            }
            return r;
        }

        public IQueryable<AQ_CatalogueItems> GetAQ_CatalogueItemsFiltered(string Filter, string orderBy)
        {
            IQueryable<AQ_CatalogueItems> r;
            List<string> SplittedFilter = Filter.ToLower().Split(',').Select(b => b.Trim()).ToList();
            switch (orderBy)
            {
                default:
                case "Name":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").Where(a => SplittedFilter.Contains(a.Type.ToLower().Trim())).OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenBy(a => a.Name);
                    break;
                case "RevisionAuthor":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").Where(a => SplittedFilter.Contains(a.Type.ToLower().Trim())).OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenBy(a => a.AQ_CatalogueRevisions.OrderByDescending(b => b.RevisionDate).FirstOrDefault().RevisionAuthor);
                    break;
                case "RevisionDate":
                    r = DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").Where(a => SplittedFilter.Contains(a.Type.ToLower().Trim())).OrderBy(a => a.Type.ToLower() == "folder" ? 0 : 1).ThenByDescending(a => a.AQ_CatalogueRevisions.Max(b => b.RevisionDate));
                    break;
            }
            return r;
        }

        public AQ_CatalogueItems GetAQ_CatalogueItem(int CalculationID)
        {
            return this.DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").ToList().Where(a => a.id == CalculationID).SingleOrDefault();
        }

        public AQ_CatalogueItems GetAQ_CatalogueItemNoDeleted(int CalculationID)
        {
            return this.DbContext.AQ_CatalogueItems.Where(d => d.IsDeleted == false).Include("AQ_CatalogueRevisions").Include("AQ_CatalogueItems2").ToList().Where(a => a.id == CalculationID).SingleOrDefault();
        }
        
        
        public IQueryable<AQ_CatalogueData> GetAQ_CatalogueDataList(int CalculationID, bool IncludeContent, int? RevisionNumber, bool IncludeDeleted)
        {
            AQ_CatalogueItems CurrentItem = this.DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Where(a => a.id == CalculationID).SingleOrDefault();
            if (CurrentItem == null) return null;
            AQ_CatalogueRevisions CurrentRevision = FindRevision(RevisionNumber, CurrentItem);
            if (CurrentRevision == null) return null;
            if (CurrentRevision.AQ_CatalogueData == null) return null;

            IEnumerable<AQ_CatalogueData> Intermediate;

            if (IncludeDeleted) Intermediate = this.DbContext.AQ_CatalogueData.Where(a => a.RevisionID == CurrentRevision.id);
            else Intermediate = this.DbContext.AQ_CatalogueData.Where(a => a.RevisionID == CurrentRevision.id && a.IsDeleted == true);

            if (IncludeContent) return Intermediate.AsQueryable();
            else
            {
                List<AQ_CatalogueData> t = new List<AQ_CatalogueData>();
                foreach (var a in Intermediate)
                {
                    AQ_CatalogueData aqcd = new AQ_CatalogueData();
                    aqcd.AttachedAt = a.AttachedAt;
                    aqcd.AttachedBy = a.AttachedBy;
                    aqcd.DataDescription = a.DataDescription;
                    aqcd.DataItemName = a.DataItemName;
                    aqcd.BinaryDataMIME = a.BinaryDataMIME;
                    aqcd.IsDeleted = a.IsDeleted;
                    t.Add(aqcd);
                }
                return t.AsQueryable();
            }
        }

        private static AQ_CatalogueRevisions FindRevision(int? RevisionNumber, AQ_CatalogueItems CurrentItem)
        {
            AQ_CatalogueRevisions CurrentRevision;
            if (RevisionNumber.HasValue)
            {
                CurrentRevision = CurrentItem.AQ_CatalogueRevisions.Where(a => a.Revision == RevisionNumber.Value).SingleOrDefault();
            }
            else
            {
                CurrentRevision = CurrentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();
            }
            return CurrentRevision;
        }

        public AQ_CatalogueData GetAQ_CatalogueDataSingle(int CalculationID, int? RevisionNumber, string Name)
        {
            AQ_CatalogueItems CurrentItem = this.DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Where(a => a.id == CalculationID).SingleOrDefault();
            if (CurrentItem == null) return null;
            AQ_CatalogueRevisions CurrentRevision = FindRevision(RevisionNumber, CurrentItem);

            if (CurrentRevision == null) return null;
            if (CurrentRevision.AQ_CatalogueData == null) return null;

            return this.DbContext.AQ_CatalogueData.Where(a => a.RevisionID == CurrentRevision.id && a.DataItemName == Name).SingleOrDefault();
        }
        
        [RequiresAuthentication]
        public int? DeleteAQ_CatalogueDataSingle(int ItemID, string DocumentName)
        {
            AQ_CatalogueItems CurrentItem = this.DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Where(a => a.id == ItemID).SingleOrDefault();
            if (CurrentItem == null) return null;
            AQ_CatalogueRevisions CurrentRevision = FindRevision(null, CurrentItem);

            AQ_CatalogueData CurrentData = CurrentRevision.AQ_CatalogueData.Where(a => a.DataItemName == DocumentName).SingleOrDefault();

            if (CurrentData == null) return null;
            CurrentData.IsDeleted = true;
            this.DbContext.AQ_CatalogueData.AttachAsModified(CurrentData, this.ChangeSet.GetOriginal(CurrentData), this.DbContext);
            return CurrentData.id;
        }

        public bool? CheckAQ_CatalogueItems(int ItemID)
        {
            AQ_CatalogueItems CurrentItem = this.DbContext.AQ_CatalogueItems.Where(a => a.id == ItemID).SingleOrDefault();
            if (CurrentItem == null) return null;
            return CurrentItem.IsDeleted;
        }

        public bool? CheckAQ_CatalogueData(int ItemID, string DocumentName)
        {
            AQ_CatalogueItems CurrentItem = this.DbContext.AQ_CatalogueItems.Include("AQ_CatalogueRevisions").Where(a => a.id == ItemID).SingleOrDefault();
            if (CurrentItem == null) return null;
            AQ_CatalogueRevisions CurrentRevision = FindRevision(null, CurrentItem);

            AQ_CatalogueData CurrentData = CurrentRevision.AQ_CatalogueData.Where(a => a.DataItemName == DocumentName).SingleOrDefault();

            if (CurrentData == null) return null;
            return CurrentData.IsDeleted;
        }

        public IQueryable<AQ_CatalogueData> GetAQ_CatalogueData()
        {
            return this.DbContext.AQ_CatalogueData;
        }

        public void InsertAQ_CatalogueData(AQ_CatalogueData aQ_CatalogueData)
        {
            DbEntityEntry<AQ_CatalogueData> entityEntry = this.DbContext.Entry(aQ_CatalogueData);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_CatalogueData.Add(aQ_CatalogueData);
            }
        }

        public void UpdateAQ_CatalogueData(AQ_CatalogueData currentAQ_CatalogueData)
        {
            this.DbContext.AQ_CatalogueData.AttachAsModified(currentAQ_CatalogueData, this.ChangeSet.GetOriginal(currentAQ_CatalogueData), this.DbContext);
        }

        public void DeleteAQ_CatalogueData(AQ_CatalogueData aQ_CatalogueData)
        {
            DbEntityEntry<AQ_CatalogueData> entityEntry = this.DbContext.Entry(aQ_CatalogueData);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_CatalogueData.Attach(aQ_CatalogueData);
                this.DbContext.AQ_CatalogueData.Remove(aQ_CatalogueData);
            }
        }

        public void InsertAQ_CatalogueItems(AQ_CatalogueItems aQ_CatalogueItems)
        {
            DbEntityEntry<AQ_CatalogueItems> entityEntry = this.DbContext.Entry(aQ_CatalogueItems);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_CatalogueItems.Add(aQ_CatalogueItems);
            }
        }

        public void UpdateAQ_CatalogueItems(AQ_CatalogueItems currentAQ_CatalogueItems)
        {
            this.DbContext.AQ_CatalogueItems.AttachAsModified(currentAQ_CatalogueItems, this.ChangeSet.GetOriginal(currentAQ_CatalogueItems), this.DbContext);
        }

        public void DeleteAQ_CatalogueItems(AQ_CatalogueItems aQ_CatalogueItems)
        {
            DbEntityEntry<AQ_CatalogueItems> entityEntry = this.DbContext.Entry(aQ_CatalogueItems);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_CatalogueItems.Attach(aQ_CatalogueItems);
                this.DbContext.AQ_CatalogueItems.Remove(aQ_CatalogueItems);
            }
        }

        public IQueryable<AQ_CatalogueRevisions> GetAQ_CatalogueRevisions()
        {
            return this.DbContext.AQ_CatalogueRevisions;
        }

        public void InsertAQ_CatalogueRevisions(AQ_CatalogueRevisions aQ_CatalogueRevisions)
        {
            DbEntityEntry<AQ_CatalogueRevisions> entityEntry = this.DbContext.Entry(aQ_CatalogueRevisions);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.AQ_CatalogueRevisions.Add(aQ_CatalogueRevisions);
            }
        }

        public void UpdateAQ_CatalogueRevisions(AQ_CatalogueRevisions currentAQ_CatalogueRevisions)
        {
            this.DbContext.AQ_CatalogueRevisions.AttachAsModified(currentAQ_CatalogueRevisions, this.ChangeSet.GetOriginal(currentAQ_CatalogueRevisions), this.DbContext);
        }

        public void DeleteAQ_CatalogueRevisions(AQ_CatalogueRevisions aQ_CatalogueRevisions)
        {
            DbEntityEntry<AQ_CatalogueRevisions> entityEntry = this.DbContext.Entry(aQ_CatalogueRevisions);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.AQ_CatalogueRevisions.Attach(aQ_CatalogueRevisions);
                this.DbContext.AQ_CatalogueRevisions.Remove(aQ_CatalogueRevisions);
            }
        }
    }
}


