﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace AQSelection
{
    public static class CatalogueHelper
    {
        public static List<Step> GetCalculationByID(int id)
        {
            var ce = new raportEntities();
            var item = ce.AQ_CatalogueItems.SingleOrDefault(a => a.id == id);
            if (item == null) return null;
            var currentRevision = item.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();
            var components = ce.AQ_CatalogueData.Where(a => a.RevisionID == currentRevision.id);

            var xs = new XmlSerializer(typeof(List<Step>));
            var aqCatalogueData = components.SingleOrDefault(b => b.DataItemName == "StepData");
            if (aqCatalogueData == null) return null;
            var stepDataText = aqCatalogueData.XmlData;
            var stepData = (List<Step>) xs.Deserialize(XmlReader.Create(new StringReader(stepDataText)));
            return stepData;
        }
    }
}