﻿using System;
using System.Linq;
using System.Web;

namespace AQSelection
{
    public static class Sessions
    {
        public static Guid NewItem(object o)
        {
            var g = Guid.NewGuid();
            HttpContext.Current.Application.Add(g.ToString(), o);
            return g;
        }

        public static void NewItem(string key, object o)
        {
            HttpContext.Current.Application.Add(key, o);
        }

        public static Guid NewItem(HttpContext hc, object o)
        {
            var g = Guid.NewGuid();
            hc.Application.Add(g.ToString(), o);
            return g;
        }

        public static HttpContext GetHttpContext()
        {
            return HttpContext.Current;
        }

        public static object GetItem(Guid g)
        {
            return HttpContext.Current.Application[g.ToString()];
        }

        public static object GetItem(HttpContext hc, Guid g)
        {
            return hc.Application[g.ToString()];
        }

        public static object GetItem(string s)
        {
            return HttpContext.Current.Application[s];
        }

        public static bool CheckItem(string s)
        {
            return HttpContext.Current.Application.AllKeys.Contains(s);
        }

        public static object GetItem(HttpContext hc, string s)
        {
            return hc.Application[s];
        }

        public static void UpdateItem(Guid g, object o)
        {
            HttpContext.Current.Application[g.ToString()] = o;
        }

        public static void UpdateItem(HttpContext hc, Guid g, object o)
        {
            hc.Application[g.ToString()] = o;
        }

        public static void UpdateItem(string s, object o)
        {
            HttpContext.Current.Application[s] = o;
        }

        public static void UpdateItem(HttpContext hc, string s, object o)
        {
            hc.Application[s] = o;
        }

        public static void DisposeItem(Guid g)
        {
            HttpContext.Current.Application.Remove(g.ToString());
        }

        public static void DisposeItem(HttpContext hc, Guid g)
        {
            hc.Application.Remove(g.ToString());
        }

        public static void DisposeItem(string s)
        {
            HttpContext.Current.Application.Remove(s);
        }

        public static void DisposeItem(HttpContext hc, string s)
        {
            hc.Application.Remove(s);
        }
    }
}