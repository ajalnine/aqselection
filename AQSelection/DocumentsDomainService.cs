﻿
namespace AQSelection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using OpenRiaServices.DomainServices.EntityFramework;
    using OpenRiaServices.DomainServices.Hosting;
    using OpenRiaServices.DomainServices.Server;


    [RequiresAuthentication]
    [EnableClientAccess()]
    public class DocumentsDomainService : DbDomainService<raportEntities>
    {

        public IQueryable<T31_Departments> GetT31_Departments()
        {
            return this.DbContext.T31_Departments;
        }

        public void InsertT31_Departments(T31_Departments t31_Departments)
        {
            DbEntityEntry<T31_Departments> entityEntry = this.DbContext.Entry(t31_Departments);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_Departments.Add(t31_Departments);
            }
        }

        public void UpdateT31_Departments(T31_Departments currentT31_Departments)
        {
            this.DbContext.T31_Departments.AttachAsModified(currentT31_Departments, this.ChangeSet.GetOriginal(currentT31_Departments), this.DbContext);
        }

        public void DeleteT31_Departments(T31_Departments t31_Departments)
        {
            DbEntityEntry<T31_Departments> entityEntry = this.DbContext.Entry(t31_Departments);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_Departments.Attach(t31_Departments);
                this.DbContext.T31_Departments.Remove(t31_Departments);
            }
        }

        public IQueryable<T31_Directions> GetT31_Directions()
        {
            return this.DbContext.T31_Directions;
        }

        public void InsertT31_Directions(T31_Directions t31_Directions)
        {
            DbEntityEntry<T31_Directions> entityEntry = this.DbContext.Entry(t31_Directions);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_Directions.Add(t31_Directions);
            }
        }

        public void UpdateT31_Directions(T31_Directions currentT31_Directions)
        {
            this.DbContext.T31_Directions.AttachAsModified(currentT31_Directions, this.ChangeSet.GetOriginal(currentT31_Directions), this.DbContext);
        }

        public void DeleteT31_Directions(T31_Directions t31_Directions)
        {
            DbEntityEntry<T31_Directions> entityEntry = this.DbContext.Entry(t31_Directions);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_Directions.Attach(t31_Directions);
                this.DbContext.T31_Directions.Remove(t31_Directions);
            }
        }

        public IQueryable<T31_Instructions> GetT31_Instructions()
        {
            return this.DbContext.T31_Instructions;
        }

        public void InsertT31_Instructions(T31_Instructions t31_Instructions)
        {
            DbEntityEntry<T31_Instructions> entityEntry = this.DbContext.Entry(t31_Instructions);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_Instructions.Add(t31_Instructions);
            }
        }

        public void UpdateT31_Instructions(T31_Instructions currentT31_Instructions)
        {
            this.DbContext.T31_Instructions.AttachAsModified(currentT31_Instructions, this.ChangeSet.GetOriginal(currentT31_Instructions), this.DbContext);
        }

        public void DeleteT31_Instructions(T31_Instructions t31_Instructions)
        {
            DbEntityEntry<T31_Instructions> entityEntry = this.DbContext.Entry(t31_Instructions);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_Instructions.Attach(t31_Instructions);
                this.DbContext.T31_Instructions.Remove(t31_Instructions);
            }
        }

        public IQueryable<T31_Orders> GetT31_Orders()
        {
            return this.DbContext.T31_Orders;
        }

        public void InsertT31_Orders(T31_Orders t31_Orders)
        {
            DbEntityEntry<T31_Orders> entityEntry = this.DbContext.Entry(t31_Orders);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_Orders.Add(t31_Orders);
            }
        }

        public void UpdateT31_Orders(T31_Orders currentT31_Orders)
        {
            this.DbContext.T31_Orders.AttachAsModified(currentT31_Orders, this.ChangeSet.GetOriginal(currentT31_Orders), this.DbContext);
        }

        public void DeleteT31_Orders(T31_Orders t31_Orders)
        {
            DbEntityEntry<T31_Orders> entityEntry = this.DbContext.Entry(t31_Orders);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_Orders.Attach(t31_Orders);
                this.DbContext.T31_Orders.Remove(t31_Orders);
            }
        }

        public IQueryable<T31_Settings> GetT31_Settings()
        {
            return this.DbContext.T31_Settings;
        }

        public void InsertT31_Settings(T31_Settings t31_Settings)
        {
            DbEntityEntry<T31_Settings> entityEntry = this.DbContext.Entry(t31_Settings);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_Settings.Add(t31_Settings);
            }
        }

        public void UpdateT31_Settings(T31_Settings currentT31_Settings)
        {
            this.DbContext.T31_Settings.AttachAsModified(currentT31_Settings, this.ChangeSet.GetOriginal(currentT31_Settings), this.DbContext);
        }

        public void DeleteT31_Settings(T31_Settings t31_Settings)
        {
            DbEntityEntry<T31_Settings> entityEntry = this.DbContext.Entry(t31_Settings);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_Settings.Attach(t31_Settings);
                this.DbContext.T31_Settings.Remove(t31_Settings);
            }
        }

        public IQueryable<T31_UserActions> GetT31_UserActions()
        {
            return this.DbContext.T31_UserActions;
        }

        public void InsertT31_UserActions(T31_UserActions t31_UserActions)
        {
            DbEntityEntry<T31_UserActions> entityEntry = this.DbContext.Entry(t31_UserActions);
            if ((entityEntry.State != EntityState.Detached))
            {
                entityEntry.State = EntityState.Added;
            }
            else
            {
                this.DbContext.T31_UserActions.Add(t31_UserActions);
            }
        }

        public void UpdateT31_UserActions(T31_UserActions currentT31_UserActions)
        {
            this.DbContext.T31_UserActions.AttachAsModified(currentT31_UserActions, this.ChangeSet.GetOriginal(currentT31_UserActions), this.DbContext);
        }

        public void DeleteT31_UserActions(T31_UserActions t31_UserActions)
        {
            DbEntityEntry<T31_UserActions> entityEntry = this.DbContext.Entry(t31_UserActions);
            if ((entityEntry.State != EntityState.Deleted))
            {
                entityEntry.State = EntityState.Deleted;
            }
            else
            {
                this.DbContext.T31_UserActions.Attach(t31_UserActions);
                this.DbContext.T31_UserActions.Remove(t31_UserActions);
            }
        }
    }
}


