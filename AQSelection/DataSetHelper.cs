﻿using System;
using System.Collections.Generic;
using System.Data;

namespace AQSelection
{
    public class DataSetHelper
    {
        public static Dictionary<string, string> SimplifySchema(DataTable schemaTable)
        {
            var simplifiedSchema = new Dictionary<string, string>();
            if (schemaTable != null)
            foreach (DataRow dr in schemaTable.Rows)
            {
                simplifiedSchema.Add((string)dr[0], ((Type)dr[12]).FullName);
            }
            return simplifiedSchema;
        }
    }
}