﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AQSelection.Default" %>
<%@ Import Namespace="System.Activities.DynamicUpdate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Анализ Качества</title>
    <script type="text/javascript">
        function DownloadProgress(sender, eventArgs) {
            sender.findName("ProgressArc").Point = (150 - 130 * Math.sin((1 - eventArgs.progress * 0.99999) * 2 * Math.PI)).toString() + "," + (150 - 130 * Math.cos((1 - eventArgs.progress * 0.99999) * 2 * Math.PI)).toString();
            sender.findName("ProgressArc").IsLargeArc = (eventArgs.progress >= 0.5);
        }
        function CloseWindow() {
            window.open('', '_self', ''); window.close();
        }
    </script>
    <style type="text/css">
        img {width: 100px; height:100px; margin-top:20px; padding: 5px; border-width: 1px; border-style:solid; border-color: #268F97; border-radius: 10px}
    </style>
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="must-revalidate" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-store" />
</head>
<body style="margin:0px; padding:0px; height: 100%; width: 100%; font-family:Verdana; font-size: 12px; text-align: justify" >
    <div style="margin:0px; padding:0px; position: fixed; height: 100%; width: 100%">
       <object id="SL" style="background:white; margin:0px; padding:0px" width="100%" height="100%"
                        data="data:application/x-silverlight-2," 
                        type="application/x-silverlight-2" >
                        <param name="source" value="/AQSelection/ClientBin/SLWorkPlace.xap?<%=Guid.NewGuid().ToString()%>" />
                        <param name="EnableGPUAcceleration" value="true" />
                        <param name="SplashScreenSource" value="/AQSelection/ClientBin/splash.xaml" />
                        <param name="onSourceDownloadProgressChanged" value="DownloadProgress" />
                        <param name="initParams" id="initParams" value="<%= "UID=" + User.Identity.Name+ ",CS="+ConfigurationManager.ConnectionStrings["raportConnectionString"]%>" />
           <div style="width: 50%; position: relative; margin-left: 25%; margin-top: 10px">
               
               <table width="100%" align="center" style="margin-bottom: 10px">
                   <tbody>
                   <tr height="33%" align="center">
                       <td valign="top" width="33%" >
                        <img src="/AQSelection/Images/CheckQuality.png" />
                           <p>Диагностика качества<br/>Поиск "слабых" мест</p>
                       </td>
                       <td valign="top" width="34%" >
                           <img src="/AQSelection/Images/Pareto.png" />
                           <p>Основные инструменты<br />анализа качества</p>
                       </td>
                       <td valign="top" width="33%" >
                           <img src="/AQSelection/Images/Integral.png" />
                           <p>Автоматизация<br />сложных расчетов</p>
                       </td>
                   </tr>
                   <tr height="34%" align="center">
                       <td valign="top" width="33%" >
                           <img src="/AQSelection/Images/Selection.png" />
                           <p>Формирование выборок <br />данных по технологии, <br />результатам испытаний и т.д.</p>
                       </td>
                       <td valign="top" width="34%" >
                        <img style="border-style: none" src="/AQSelection/Images/LandingImage.png" />
                           <p style="font-size:20px;color: #268F97">Анализ качества</p>
                       </td>
                       <td valign="top" width="33%" >
                           <img src="/AQSelection/Images/Calculation.png" />
                            <p>Визуальное<br />программирование<br />обработки данных</p>
                       </td>
                   </tr>
                   <tr height="33%" align="center">
                       <td valign="top" width="33%" >
                           <img src="/AQSelection/Images/Calendar.png" />
                            <p>Каталог расчетов <br />для постоянного использования</p>
                       </td>
                       <td valign="top" width="34%" >
                           <img src="/AQSelection/Images/Violin.png" />
                            <p>Современные методы<br />визуального анализа</p>
                       </td>
                       <td valign="top" width="33%" >
                           <img src="/AQSelection/Images/Presentation.png" />
                            <p>Автоматическое создание <br />презентаций и отчетов</p>
                       </td>
                   </tr>
                  </tbody>
               </table>
               <hr/>

               <table width="100%" style="margin-top: 5px">
                   <tbody>
                   <tr>
                       <td valign="top" width="50%">
                       <ul>
                           <li style="margin-bottom: 5px;">По этому адресу можно войти в <b>АС Анализ Качества</b>. </li>
                           <li style="margin-bottom: 5px;">Вы видите эту страницу потому, что на вашем компьютере <b>не установлено</b> расширение браузера: <asp:HyperLink runat="server" NavigateUrl="http://cmk-sapp64.int.mechel.corp/AQSelection/Silverlight.exe" Text="Silverlight 5"></asp:HyperLink>. Либо ваш браузер не поддерживает Silverlight, попробуйте Internet Explorer.</li>
                           <li style="margin-bottom: 5px;">Если с установкой Silverlight возникли проблемы, обращайтесь в службу поддержки пользователей по тел.:&nbsp;<b>08</b></li>
                       </ul>
                       </td>
                       <td valign="top" width="50%" >
                       <ul>
                           <li style="margin-bottom: 5px">Обработка и анализ <b>собственных</b> данных, формирование отчетов и презентаций доступны всем пользователям</li>
                           <li style="margin-bottom: 5px">Для доступа к информации по качеству и технологии оформите заявку на&nbsp<b>АС&nbspТехнолог</b></li>
                           <li style="margin-bottom: 5px">Для работы с данными по рельсовой и нержавеющей стали дополнительно требуется подключение к <b>АС Послябового учета нержавеющей и рельсовой стали</b></li>
                       </ul>
                       </td>
                   </tr>
                  </tbody>
               </table>
                  <hr/>
                     <table width="100%">
                   <tbody>
                   <tr>
                       <td valign="top" width="50%" align="left" >
                                <p style="font-size:10px;margin-left: 40px">Лаборатория анализа качества<br/>Отдела информационного обеспечения<br/>Исследовательско-технологического центра ЧМК</p>
                       </td>
                       <td valign="top" width="50%" align="right" >
                                <p style="font-size:14px;color: #268F97"><b>Тел.: 5-38-22</b></p>
                       </td>
                   </tr>
                  </tbody>
               </table>
        </div>    
        </object>
    </div>
</body>
</html>