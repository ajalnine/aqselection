﻿using System;
using System.Text;
using System.IO;

namespace AQSelection
{
    public partial class GetFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Server.UrlDecode(Request.QueryString["ReportName"] ?? "Отчет");
            var memoryStreamGuid = Server.UrlDecode(Request.QueryString["FileName"]);
            var contentType = Server.UrlDecode(Request.QueryString["ContentType"]);
            var ms = Sessions.GetItem(memoryStreamGuid) as MemoryStream;
            var fName = Server.UrlDecode(Request.QueryString["FName"] ?? "Отчет");
            if (Sessions.CheckItem(memoryStreamGuid + "-FileName"))
            {
                fName = Sessions.GetItem(memoryStreamGuid + "-FileName").ToString();
            }
            Response.Clear();
            Response.ContentType = contentType;
            if (fName != null)
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlPathEncode(Encoding.GetEncoding(1251).GetString(Encoding.Default.GetBytes(fName))));
            if (ms != null) ms.WriteTo(Response.OutputStream);
            Response.Flush();
            Sessions.DisposeItem(memoryStreamGuid);
            Response.End();
        }
    }
}
