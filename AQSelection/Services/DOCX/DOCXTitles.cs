﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using D = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        private static HeaderPart CreateDocumentColontitles(WordprocessingDocument doc)
        {
            HeaderPart hp = doc.MainDocumentPart.AddNewPart<HeaderPart>();
            hp.Header = new Header();

            Paragraph n = new Paragraph();
            Tabs t = new Tabs();
            t.AppendChild<TabStop>(new TabStop() { Position = 4677, Val = new EnumValue<TabStopValues>(TabStopValues.Center) });
            t.AppendChild<TabStop>(new TabStop() { Position = 9322, Val = new EnumValue<TabStopValues>(TabStopValues.Right) });
            n.AppendChild<ParagraphProperties>(new ParagraphProperties(t)
            {
                FrameProperties = new FrameProperties()
                {
                    Wrap = new EnumValue<TextWrappingValues>(TextWrappingValues.Around),
                    VerticalPosition = new EnumValue<VerticalAnchorValues>(VerticalAnchorValues.Text),
                    HorizontalPosition = new EnumValue<HorizontalAnchorValues>(HorizontalAnchorValues.Margin),
                    Y = "1",
                    XAlign = new EnumValue<HorizontalAlignmentValues>(HorizontalAlignmentValues.Center)
                },
                ParagraphStyleId = new ParagraphStyleId() { Val = "a0" }
            });

            Run pn1 = new Run();
            FieldChar fc1 = new FieldChar() { FieldCharType = new EnumValue<FieldCharValues>(FieldCharValues.Begin) };
            pn1.AppendChild<FieldChar>(fc1);
            n.AppendChild<Run>(pn1);

            Run pn2 = new Run();
            FieldCode fc2 = new FieldCode() { Text = "PAGE" };
            pn2.AppendChild<FieldCode>(fc2);
            n.AppendChild<Run>(pn2);

            Run pn3 = new Run();
            FieldChar fc3 = new FieldChar() { FieldCharType = new EnumValue<FieldCharValues>(FieldCharValues.Separate) };
            pn3.AppendChild<FieldChar>(fc3);
            n.AppendChild<Run>(pn3);

            Run pn4 = new Run(new Text("12"));
            n.AppendChild<Run>(pn4);

            Run pn5 = new Run();
            FieldChar fc5 = new FieldChar() { FieldCharType = new EnumValue<FieldCharValues>(FieldCharValues.End) };
            pn5.AppendChild<FieldChar>(fc5);
            n.AppendChild<Run>(pn5);

            hp.Header.Append(n);

            Run rh = new Run(new Text("Продолжение отчета"));
            Paragraph n2 = new Paragraph(rh);
            n2.PrependChild<ParagraphProperties>(new ParagraphProperties()
            {
                ParagraphStyleId = new ParagraphStyleId() { Val = "a0" }
            }
            );
            hp.Header.Append(n2);

            hp.Header.Save(hp.GetStream());
            return hp;
        }

        private static void CreateDocumentTitle(WordprocessingDocument doc)
        {
            Run r1 = new Run(new Text("О Т Ч Е Т"));
            r1.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
            Paragraph pmain1 = new Paragraph(r1);
            pmain1.PrependChild<ParagraphProperties>(new ParagraphProperties() { ParagraphStyleId = new ParagraphStyleId() { Val = "a2" } });
            pmain1.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "0", Before = "0" } });
            doc.MainDocumentPart.Document.Body.Append(pmain1);
            Run r2 = new Run(new Text("АС \"Анализ Качества\""));
            r2.PrependChild<RunProperties>(new RunProperties(new Bold() { Val = OnOffValue.FromBoolean(true) }));
            Paragraph pmain2 = new Paragraph(r2);
            pmain2.PrependChild<ParagraphProperties>(new ParagraphProperties() { ParagraphStyleId = new ParagraphStyleId() { Val = "a2" } });
            pmain2.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "200", Before = "0" } });
            doc.MainDocumentPart.Document.Body.Append(pmain2);
        }
   }
}