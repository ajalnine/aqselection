﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;


namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        private const Int64 ClientWidth = 5940425;
        private const Int64 ClientHeight = 2524760;
        
        private void CreateDocumentContent(WordprocessingDocument doc, PresentationDescription PD)
        {
            uint ImageNumber = 1;
            foreach (var s in PD.Slides)
            {
                foreach (var sp in s.SlideElements)
                {
                    switch (sp.SlideElementType)
                    {
                        case SlideElementTypes.Image:
                            AppendImageData(doc, sp.SlideData as List<byte>, ImageNumber);
                            AppendPicture(doc, sp, ImageNumber);
                            ImageNumber++;
                            break;

                    }
                }
            }
        }

        private static void AppendImageData(WordprocessingDocument doc, List<byte> PngData, uint ImageNumber)
        {
            string RelID = "PicId" + ImageNumber.ToString();
            MemoryStream PNG = new MemoryStream((PngData).ToArray<byte>());
            ImagePart imagePart1 = doc.MainDocumentPart.AddNewPart<ImagePart>("image/png", RelID);
            PNG.Position = 0;
            imagePart1.FeedData(PNG);
            PNG.Close();
        }

        private void AppendPicture(WordprocessingDocument doc, SlideElementDescription sed, uint ImageNumber)
        {
            string RelID = "PicId" + ImageNumber.ToString();
            
            Paragraph paragraph = new Paragraph();
            AddImageToParagraph(paragraph, sed, RelID, ImageNumber);

            Paragraph paragraph2 = new Paragraph();
            ParagraphProperties paragraphProperties2 = new ParagraphProperties();
            paragraphProperties2.Append(new ParagraphStyleId() { Val = "a1" });
            paragraph.Append(paragraphProperties2);
            
            AppendImageRef(ImageNumber, paragraph2, sed.Description);

            doc.MainDocumentPart.Document.Body.AppendChild(paragraph);
            doc.MainDocumentPart.Document.Body.AppendChild(paragraph2);
        }

        private static void AppendImageRef(uint ImageNumber, Paragraph paragraph2, string Name)
        {
            Run run2 = new Run();
            Text text1 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text1.Text = "Рисунок ";
            run2.Append(text1);
            
            SimpleField simpleField1 = new SimpleField() { Instruction = " SEQ Рисунок \\* ARABIC " };

            Run run3 = new Run();

            RunProperties runProperties2 = new RunProperties();
            NoProof noProof2 = new NoProof();

            runProperties2.Append(noProof2);
            Text text2 = new Text();
            text2.Text = ImageNumber.ToString();

            run3.Append(runProperties2);
            run3.Append(text2);

            simpleField1.Append(run3);

            Run run4 = new Run();

            RunProperties runProperties3 = new RunProperties();
            Languages languages2 = new Languages() { Val = "ru-RU" };

            runProperties3.Append(languages2);
            Text text3 = new Text();
            text3.Text = ". " + Name + ".";

            run4.Append(runProperties3);
            run4.Append(text3);
            BookmarkStart bookmarkStart1 = new BookmarkStart() { Name = "_Toc" + ImageNumber.ToString(), Id = ImageNumber.ToString() };
            BookmarkEnd bookmarkEnd1 = new BookmarkEnd() { Id = ImageNumber.ToString() };

            paragraph2.Append(bookmarkStart1);
            paragraph2.Append(run2);
            paragraph2.Append(simpleField1);
            paragraph2.Append(run4);
            paragraph2.Append(bookmarkEnd1);
        }

        private static void AddImageToParagraph(Paragraph paragraph2, SlideElementDescription SED,  string RelID, uint ImageNumber)
        {
            double Rel1 = SED.Height / SED.Width;
            double Rel2 = (double)ClientHeight / (double)ClientWidth;
            
            Int64 PicWidth = 0;
            Int64 PicHeight = 0;
            
            if (Rel1 > Rel2)
            {
                PicWidth = ClientWidth;
                PicHeight = (Int64)((double)ClientWidth * SED.Height / SED.Width);
            }
            else
            {
                PicWidth = (Int64)((double)ClientHeight * SED.Width / SED.Height);
                PicHeight = ClientHeight;
            }

            var element = new Drawing(
                new DW.Inline(
                         new DW.Extent() { Cx = PicWidth, Cy = PicHeight },
                         new DW.WrapTopBottom(){ DistanceFromBottom = 1000},
                         new DW.DocProperties() { Id = (UInt32Value)ImageNumber, Name = "Рисунок " + RelID },
                         new DW.NonVisualGraphicFrameDrawingProperties( new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(     new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)ImageNumber,
                                             Name = RelID+".png"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(){Embed = RelID, CompressionState = A.BlipCompressionValues.Print},
                                         new A.Stretch(new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = PicWidth, Cy = PicHeight }),
                                         new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }))
                             ) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     ));

            paragraph2.AppendChild(new Run(element));
        }
    }
}