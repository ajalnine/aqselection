﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using D = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        private static void CreateDocumentStyles(WordprocessingDocument doc)
        {
            Style a0 = new Style() { Type = new EnumValue<StyleValues>(StyleValues.Paragraph), StyleId = "a0" };
            a0.AppendChild<RunProperties>(new RunProperties(new PrimaryStyle(), new RunFonts() { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize() { Val = "24" }));
            a0.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "0", Before = "30" } });

            Style h0 = new Style() { Type = new EnumValue<StyleValues>(StyleValues.Paragraph), StyleId = "h0" };
            h0.AppendChild<RunProperties>(new RunProperties(new PrimaryStyle(), new RunFonts() { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize() { Val = "24" }));
            h0.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "0", Before = "30" } });

            Style a1 = new Style() { Type = new EnumValue<StyleValues>(StyleValues.Paragraph), StyleId = "a1" };
            a1.AppendChild<RunProperties>(new RunProperties(new PrimaryStyle(), new RunFonts() { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize() { Val = "22" }));
            a1.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "0", Before = "30" } });

            Style a2 = new Style() { Type = new EnumValue<StyleValues>(StyleValues.Paragraph), StyleId = "a2" };
            a2.AppendChild<RunProperties>(new RunProperties(new PrimaryStyle(), new RunFonts() { EastAsia = "Arial", HighAnsi = "Arial", Ascii = "Arial" }, new FontSize() { Val = "22" }));
            a2.PrependChild<ParagraphProperties>(new ParagraphProperties() { SpacingBetweenLines = new SpacingBetweenLines() { After = "0", Before = "30" } });
            a2.PrependChild<ParagraphProperties>(new ParagraphProperties() { Justification = new Justification() { Val = new EnumValue<JustificationValues>(JustificationValues.Center) } });

            StyleDefinitionsPart sdp = doc.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
            sdp.Styles = new Styles();
            sdp.Styles.AppendChild<Style>(a0);
            sdp.Styles.AppendChild<Style>(a1);
            sdp.Styles.AppendChild<Style>(a2);
            sdp.Styles.AppendChild<Style>(h0);
            sdp.Styles.Save(sdp.GetStream());
        }
   }
}