﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using D = DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        public MemoryStream CreateEmptyDOCXPackage(PresentationDescription PD)
        {
            MemoryStream MS = new MemoryStream();

            using (WordprocessingDocument doc = WordprocessingDocument.Create(MS, WordprocessingDocumentType.Document))
            {
                doc.AddMainDocumentPart();
                doc.MainDocumentPart.Document = new Document();
                doc.MainDocumentPart.Document.Body = new Body();

                CreateDocumentStyles(doc);
                CreateDocumentTitle(doc);
                CreateDocumentContent(doc, PD);
                CreateDocumentSectionProperties(doc);
                doc.MainDocumentPart.Document.Save();
            }
 
            return MS;
        }

        private static void CreateDocumentSectionProperties(WordprocessingDocument doc)
        {
            HeaderPart hp = CreateDocumentColontitles(doc);


            SectionProperties sectPr = new SectionProperties(
                                                                    new PageSize()
                                                                    {
                                                                        Width = 11906,
                                                                        Height = 16838,
                                                                        Code = 9
                                                                    },
                                                                    new PageMargin()
                                                                    {
                                                                        Top = 1134,
                                                                        Right = 851,
                                                                        Bottom = 851,
                                                                        Left = 1134,
                                                                        Header = 452,
                                                                        Footer = 0,
                                                                        Gutter = 0
                                                                    },
                                                                    new TitlePage(),
                                                                    new HeaderReference() { Id = doc.MainDocumentPart.GetIdOfPart(hp) },
                                                                    new PageNumberType() { Start = 1 },
                                                                    new DocGrid() { LinePitch = 360 },
                                                                    new Columns() { Space = "708" }
                                                                );
            doc.MainDocumentPart.Document.Body.AppendChild<SectionProperties>(sectPr);
        }
   }
}