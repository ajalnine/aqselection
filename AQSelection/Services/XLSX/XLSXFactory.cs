﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace AQSelection
{
    public  static partial class XLSXFactory
    {
        public static Guid CreateXLSXForAllTables(DataSet dataStore, string storedDataSetGuidToDelete, HttpContext hc)
        {
            var lines = 0;
            uint cellCounter = 0;
            var ms = new MemoryStream();
            int fixedColumns = 1;

            if (dataStore.Tables.Contains("Переменные"))
            {
                if (dataStore.Tables["Переменные"].Columns.Contains("Фиксированный столбец"))
                {
                    fixedColumns = (int)dataStore.Tables["Переменные"].Rows[0]["Фиксированный столбец"];
                }
            }



            using (SpreadsheetDocument doc = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart main = doc.AddWorkbookPart();
                main.Workbook = new Workbook();

                var wsp = doc.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                var ss = new Stylesheet {CellFormats = new CellFormats()};
                CreateFonts(ss);
                CreateBorders(ss);
                CreateNumberingFormats(ss);
                CreateCellStyleFormats(ss);
                ss.CellStyles = new CellStyles();
                ss.CellStyles.AppendChild(new CellStyle { FormatId = 0, Name = "a0", BuiltinId = 0 });
                ss.DifferentialFormats = new DifferentialFormats();
                var fillDictionary = CreateFills(dataStore, ss);
                var styleDictionary = new Dictionary<string, uint>();
                GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, XLSXBorder.None, 0, false, styleDictionary);
                
                var bookviews = new BookViews();
                var wv = new WorkbookView
                {
                    XWindow = 360,
                    YWindow = 555,
                    WindowWidth = 24615,
                    WindowHeight = 11190,
                    ActiveTab = 1
                };
                bookviews.AppendChild(wv);
                main.Workbook.AppendChild(bookviews);

                var sheets = new Sheets();
                main.Workbook.AppendChild(sheets);
                var totalcells = 0;
                var maxRowsInSheet = 0;
                var maxColumnsInSheet = 0;
                uint number = 1;
                uint tableNumber = 1;
                foreach (DataTable data in dataStore.Tables)
                {
                    var firstColumnIsCaseName = data.ExtendedProperties.ContainsKey("FirstColumnIsCaseName") && (bool)data.ExtendedProperties["FirstColumnIsCaseName"];
                    var newWorksheetPart = main.AddNewPart<WorksheetPart>();

                    var tableWriter = OpenXmlWriter.Create(new PackagePartStream(newWorksheetPart.GetStream()));

                    var workSheet = new Worksheet();

                    tableWriter.WriteStartElement(workSheet);
                    var index = 0;
                    var cellindex = 0;
                    var displayableColumns = data.Columns.Cast<DataColumn>().Count(d => !(d.ColumnName.StartsWith("#")));
                    var fieldTypesList = new Dictionary<string, string>();
                    var fieldIndexes = new Dictionary<string, int>();
                    var columnRequiresCellMerging = new Dictionary<string, bool>();
                    foreach (DataColumn d in data.Columns)
                    {
                        columnRequiresCellMerging.Add(d.ColumnName, d.ExtendedProperties.ContainsKey("CellCombined") && (bool)d.ExtendedProperties["CellCombined"]);
                        if (d.ColumnName.StartsWith("#"))
                        {
                            index++;
                            continue;
                        }
                        var columnDataType = d.DataType.Name.ToLower();
                        fieldTypesList.Add(d.ColumnName, columnDataType);
                        fieldIndexes.Add(d.ColumnName, index);
                        index++;
                        cellindex++;
                    }
                    //newWorksheetPart.Worksheet = new Worksheet(new SheetViews(), new Columns(), new SheetData());

                    var relationshipId = main.GetIdOfPart(newWorksheetPart);
                    var sheet = new Sheet();
                    sheets.AppendChild(sheet);
                    sheet.Id = relationshipId;
                    sheet.SheetId = number;
                    string name = data.TableName;
                    if (name.Length > 30)
                    {
                        name = name.Substring(0, 30 - tableNumber.ToString(CultureInfo.InvariantCulture).Length) + tableNumber;
                    }
                    tableNumber++;
                    sheet.Name = Regex.Replace(name, @"[:\?\*\[\]\/\\]", "_");

                    CreateSheetView(tableWriter, number, tableNumber == 3 ? fixedColumns : 1);

                    
                    
                    var colorColumnIndexes = new Dictionary<string, int>();
                    var columnHasTime = new Dictionary<string, bool>();
                    var columnHasLeftBorder = new Dictionary<string, bool>();
                    var columnHasRightBorder = new Dictionary<string, bool>();
                    var columnHasHorizontalBorder = new Dictionary<string, bool>();
                    var columnHasChangeBorder = new Dictionary<string, bool>();
                    var rowHasChangeBorder = new bool[data.Rows.Count];


                    foreach (DataColumn d in data.Columns)
                    {
                        var columnDataType = d.DataType.Name.ToLower();
                        if (columnDataType == "datetime") columnHasTime.Add(d.ColumnName, CheckIsTimeEmpty(d.ColumnName, data));
                        columnHasLeftBorder.Add(d.ColumnName, d.ExtendedProperties.ContainsKey("LeftBorder") && (bool)d.ExtendedProperties["LeftBorder"]);
                        columnHasRightBorder.Add(d.ColumnName, d.ExtendedProperties.ContainsKey("RightBorder") && (bool)d.ExtendedProperties["RightBorder"]);
                        columnHasHorizontalBorder.Add(d.ColumnName, d.ExtendedProperties.ContainsKey("HorizontalBorder") && (bool)d.ExtendedProperties["HorizontalBorder"]);
                        columnHasChangeBorder.Add(d.ColumnName, d.ExtendedProperties.ContainsKey("ChangeBorder") && (bool)d.ExtendedProperties["ChangeBorder"]);
                    }
                    CreateColumns(tableWriter, data, columnHasTime);
                    var sheetData1 = new SheetData();

                    tableWriter.WriteStartElement(sheetData1);
                    
                    number++;

                    #region Таблица

                    uint rowNumber = 1;

                    var row = new Row {RowIndex = rowNumber};
                    tableWriter.WriteStartElement(row);

                    
                    index = 0;
                    cellindex = 0;
                    foreach (DataColumn d in data.Columns)
                    {
                        if (d.ColumnName.StartsWith("#"))
                        {
                            if (d.ColumnName.EndsWith("_Цвет"))colorColumnIndexes.Add(d.ColumnName, index);
                            index++;
                            continue;
                        }
                        var headerBorder = XLSXBorder.Thin;
                        headerBorder |= XLSXBorder.Top;
                        headerBorder |= XLSXBorder.Bottom;
                        if (columnHasLeftBorder[d.ColumnName]) headerBorder |= XLSXBorder.Left;
                        if (columnHasRightBorder[d.ColumnName]) headerBorder |= XLSXBorder.Right;
                        if (index == 0) headerBorder |= XLSXBorder.Left;
                        if (cellindex == displayableColumns - 1) headerBorder |= XLSXBorder.Right;

                        var headerCell = new Cell
                        {
                            CellReference = GetColumnName((uint)cellindex) + rowNumber,
                            StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Bold, headerBorder, 2, true, styleDictionary),
                            CellValue = new CellValue(d.ColumnName),
                            DataType = new EnumValue<CellValues>(CellValues.String)
                        };

                        tableWriter.WriteElement(headerCell);

                        var columnDataType = d.DataType.Name.ToLower();
                        index++;
                        cellindex++;
                    }
                    var totalColumns = cellindex;
                    var headerCellFinal = new Cell
                    {
                        CellReference = GetColumnName((uint)cellindex) + rowNumber,
                        StyleIndex =
                                GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, XLSXBorder.None, 0, false, styleDictionary),
                        CellValue = new CellValue(String.Empty),
                        DataType = new EnumValue<CellValues>(CellValues.Number)
                    };
                    tableWriter.WriteElement(headerCellFinal);
                    tableWriter.WriteEndElement(); //row

                    //AutoFilter autoFilter1 = new AutoFilter() { Reference = "A1:" + GetColumnName((uint)cellindex - 1) + Data.Rows.Count + 1 };
                    //worksheet.Append(autoFilter1);

                    foreach (var c in columnHasChangeBorder.Where(a=>a.Value))
                    {
                        var i = fieldIndexes[c.Key];
                        string oldValue = null;
                        for (var j = 0; j < data.Rows.Count; j++)
                        {
                            var value = data.Rows[j].ItemArray[i] != null ? data.Rows[j].ItemArray[i].ToString() : null;
                            if (value != oldValue) rowHasChangeBorder[j] = true;
                            oldValue = value;
                        }
                    }

                    rowNumber++;
                    
                    foreach (DataRow r in data.Rows)
                    {
                        row = new Row {RowIndex = rowNumber};
                        tableWriter.WriteStartElement(row);
                        lines++;
                        uint cellInRowCounter = 0;
                        foreach (string columnName in fieldTypesList.Keys)
                        {
                            var aligned = columnRequiresCellMerging.ContainsKey(columnName) &&
                                            columnRequiresCellMerging[columnName];
                                
                            int i = fieldIndexes[columnName];
                            var fillId = GetFillId(columnName, colorColumnIndexes, r, fillDictionary);
                            
                            var borderId = XLSXBorder.Thin;
                            if (cellInRowCounter == 0) borderId |= XLSXBorder.Left;
                            if (cellInRowCounter == totalColumns - 1) borderId |= XLSXBorder.Right;
                            if (rowNumber == 2) borderId |= XLSXBorder.Top;
                            if (rowNumber == data.Rows.Count + 1) borderId |= XLSXBorder.Bottom;
                            if (columnHasLeftBorder[columnName]) borderId |= XLSXBorder.Left;
                            if (columnHasRightBorder[columnName]) borderId |= XLSXBorder.Right;
                            if (columnHasHorizontalBorder[columnName]) borderId |= XLSXBorder.Top | XLSXBorder.Bottom;
                            if (rowHasChangeBorder[rowNumber - 2]) borderId |= XLSXBorder.Top;

                            #region Создание ячейки данных
                            Cell cell = null;

                            if (r.ItemArray[i] == null || String.IsNullOrEmpty(r.ItemArray[i].ToString()))
                            {
                                    cell = new Cell
                                    {
                                        CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                        StyleIndex =  GetStyle(ss, 0, XLSXFont.Default, borderId, fillId, false, styleDictionary),
//                                        CellValue = new CellValue(String.Empty),
  //                                      DataType = new EnumValue<CellValues>(CellValues.String)
                                    };

                            }
                            else
                                switch (fieldTypesList[columnName])
                                {
                                    case "int32":
                                        cell = new Cell
                                        {
                                            CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                            StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0)
                                                ? GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Bold, borderId, 2, true, styleDictionary)
                                                : GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, borderId, fillId, aligned, styleDictionary),
                                            CellValue = new CellValue(r.ItemArray[i].ToString()),
                                            DataType = new EnumValue<CellValues>(CellValues.Number)
                                        };
                                        break;

                                    case "datetime":
                                        var dateString = r.ItemArray[i].ToString();
                                        DateTime dt;

                                        if (DateTime.TryParse(dateString, out dt))
                                        {
                                            var dateFormat = columnHasTime[columnName]
                                                ? XLSXNumberFormat.DateTime
                                                : XLSXNumberFormat.Date;
                                            
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0) 
                                                        ? GetStyle(ss, dateFormat, XLSXFont.Bold, borderId, 2, true, styleDictionary)
                                                        : GetStyle(ss, dateFormat, XLSXFont.Default, borderId, fillId, true, styleDictionary),
                                                    CellValue = new CellValue(dt.ToOADate().ToString(CultureInfo.InvariantCulture).Replace(',', '.')),
                                                    DataType = new EnumValue<CellValues>(CellValues.Number)
                                                };
                                        }
                                        else
                                        {
                                            
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0) 
                                                        ? GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, borderId, fillId, false, styleDictionary)
                                                        : GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Bold, borderId, 2, true, styleDictionary),
                                                    CellValue = new CellValue(String.Empty),
                                                    DataType = new EnumValue<CellValues>(CellValues.Number)
                                                };
                                        }
                                        break;

                                    case "timespan":
                                        var timeString = r.ItemArray[i].ToString();
                                        TimeSpan t;

                                        if (TimeSpan.TryParse(timeString, out t))
                                        {
                                            var dtime = (t.Days * 86400.0d + t.Hours * 3600.0d + t.Minutes * 60.0d + t.Seconds) / 86400.0d;
                                            const XLSXNumberFormat dateFormat = XLSXNumberFormat.Time;
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0)
                                                        ? GetStyle(ss, dateFormat, XLSXFont.Bold, borderId, 2, true, styleDictionary)
                                                        : GetStyle(ss, dateFormat, XLSXFont.Default, borderId, fillId, true, styleDictionary),
                                                    CellValue = new CellValue(dtime.ToString(CultureInfo.InvariantCulture).Replace(',', '.')),
                                                    DataType = new EnumValue<CellValues>(CellValues.Number)
                                                };
                                        }
                                        else
                                        {
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0)
                                                        ? GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Bold, borderId, 2, true, styleDictionary)
                                                        : GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, borderId, fillId, false, styleDictionary),
                                                    CellValue = new CellValue(String.Empty),
                                                    DataType = new EnumValue<CellValues>(CellValues.Number)
                                                };
                                        }
                                        break;

                                    case "boolean":
                                        if ((bool) r.ItemArray[i])
                                        {
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Blue, borderId, fillId, true, styleDictionary),
                                                    CellValue = new CellValue("V"),
                                                    DataType = new EnumValue<CellValues>(CellValues.String)
                                                };
                                        }
                                        else
                                                cell = new Cell
                                                {
                                                    CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                    StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Red, borderId, fillId, true, styleDictionary),
                                                    CellValue = new CellValue("X"),
                                                    DataType = new EnumValue<CellValues>(CellValues.String)
                                                };

                                        break;

                                    case "string":
                                            cell = new Cell
                                            {
                                                CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                StyleIndex = (firstColumnIsCaseName && cellInRowCounter == 0)
                                                        ? GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Bold, borderId, 2, true, styleDictionary)
                                                        : GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, borderId, fillId, aligned, styleDictionary),
                                                CellValue = new CellValue(r.ItemArray[i].ToString()),
                                                DataType = new EnumValue<CellValues>(CellValues.String)
                                            };
                                        break;

                                    default:
                                        var v = r.ItemArray[i].ToString().ToLower();
                                        switch (v)
                                        {
                                            case "nan":
                                                    cell = new Cell
                                                    {
                                                        CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                        StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Red, borderId, fillId, true, styleDictionary),
                                                        CellValue = new CellValue("NaN"),
                                                        DataType = new EnumValue<CellValues>(CellValues.String)
                                                    };
                                                break;
                                            case "бесконечность":
                                            case "infinity":
                                                
                                                    cell = new Cell
                                                    {
                                                        CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                        StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Red, borderId, fillId, true, styleDictionary),
                                                        CellValue = new CellValue("∞"),
                                                        DataType = new EnumValue<CellValues>(CellValues.String)
                                                    };
                                                break;
                                            default:
                                                
                                                    cell = new Cell
                                                    {
                                                        CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                                        StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, borderId, fillId, true, styleDictionary),
                                                        CellValue = new CellValue(v.Replace(',', '.')),
                                                        DataType = new EnumValue<CellValues>(CellValues.Number)
                                                    };
                                                break;
                                        }
                                        break;
                                }
                            #endregion
                            totalcells++;
                            tableWriter.WriteElement(cell);
                            cellInRowCounter++;
                            cellCounter++;
                        }

                        var finalCell = 
                            new Cell
                            {
                                CellReference = GetColumnName(cellInRowCounter) + rowNumber,
                                StyleIndex = GetStyle(ss, XLSXNumberFormat.Default, XLSXFont.Default, XLSXBorder.None, 0, false, styleDictionary),
                                CellValue = new CellValue(String.Empty),
                                DataType = new EnumValue<CellValues>(CellValues.Number)
                            };
                        tableWriter.WriteElement(finalCell);
                        tableWriter.WriteEndElement();// row;
                        if (maxColumnsInSheet < cellInRowCounter) maxColumnsInSheet = (int)cellInRowCounter;
                        rowNumber++;
                    }
                    tableWriter.WriteEndElement();// sheetData1;
                    CreateMergedCells(tableWriter, data, fieldTypesList.Keys, fieldIndexes, columnRequiresCellMerging);
                    tableWriter.WriteEndElement(); //workSheet
                    tableWriter.Close();
                    if (maxRowsInSheet < rowNumber) maxRowsInSheet = (int)rowNumber;
                    #endregion
                }
                wsp.Stylesheet = ss;
                ss.Save(wsp.GetStream());

                Sessions.DisposeItem(hc, storedDataSetGuidToDelete);
                Guid storedMemoryStreamGuid = Sessions.NewItem(hc, ms);
                main.Workbook.Save();
                AQService aqs = new AQService();
                aqs.WriteLog($"XLSX created with; Sheets:{dataStore.Tables.Count}; Cells: {totalcells}; Bytes:{ms.Length}; Max columns:{maxColumnsInSheet}; Max rows:{maxRowsInSheet-2}");

                return storedMemoryStreamGuid;
            }
        }

        private static bool CheckIsTimeEmpty(string columnName, DataTable data)
        {
            var i = data.Columns.IndexOf(columnName);
            foreach (DataRow r in data.Rows)
            {
                if (r.ItemArray[i] == null) continue;
                var dateString = r.ItemArray[i].ToString();
                DateTime dt;
                if (!DateTime.TryParse(dateString, out dt))continue;
                if (dt.Hour > 0 || dt.Minute > 0 || dt.Second > 0)return true;
            }
            return false;
        }

        private static uint GetFillId(string columnName, Dictionary<string, int> colorColumnIndexes, DataRow r, Dictionary<string, uint> fillDictionary)
        {
            string colorColumnName = string.Format("#{0}_Цвет", columnName);
            int colorIndex = colorColumnIndexes.ContainsKey(colorColumnName)
                ? colorColumnIndexes[colorColumnName]
                : -1;

            uint fillId = colorIndex >= 0 && r.ItemArray[colorIndex].ToString().StartsWith("#")
                ? fillDictionary[r.ItemArray[colorIndex].ToString()]
                : 0;
            return fillId;
        }

        private static void CreateColumns(OpenXmlWriter worksheetWriter, DataTable data, Dictionary<string, bool> columnHasTime)
        {
            var cols = new Columns();
            worksheetWriter.WriteStartElement(cols);

            int columnIndex = 0;
            for (var i = 0; i < data.Columns.Count; i++)
            {
                if (data.Columns[i].Caption.StartsWith("#")) continue;
                DoubleValue columnWidth;
                if (!columnHasTime.ContainsKey(data.Columns[i].Caption)) columnWidth = GetColumnWidth(data, i);
                else columnWidth = columnHasTime[data.Columns[i].Caption] ? 15.8 : 9.8;
                var c = new Column
                {
                    BestFit = true,
                    CustomWidth = true,
                    Width = columnWidth,
                    Min = (uint) (columnIndex + 1),
                    Max = (uint) (columnIndex + 1)
                };
                worksheetWriter.WriteElement(c);
                columnIndex++;
            }
            worksheetWriter.WriteEndElement();
        }

        private static void CreateSheetView(OpenXmlWriter tableWriter, uint number, int fixedColumns)
        {
            var sheetViews = new SheetViews();
            tableWriter.WriteStartElement(sheetViews);


            var sheetView1 = new SheetView
            {
                TabSelected = (number == 2),
                WorkbookViewId = 0
            };
            tableWriter.WriteStartElement(sheetView1);

            var pane1 =new Pane
            {
                HorizontalSplit = fixedColumns,
                VerticalSplit = 1,
                TopLeftCell = GetColumnName((uint)fixedColumns)+"2",
                ActivePane = new EnumValue<PaneValues>(PaneValues.TopRight),
                State = new EnumValue<PaneStateValues>(PaneStateValues.Frozen)
            };

            tableWriter.WriteElement(pane1);
            var selection1 = new Selection
            {
                Pane = new EnumValue<PaneValues>(PaneValues.TopRight)
            };
            tableWriter.WriteElement(selection1);

            tableWriter.WriteEndElement();// sheetView1;
            tableWriter.WriteEndElement();// sheetViews;
        }

        private static DoubleValue GetColumnWidth(DataTable data, int i)
        {
            if (data.DataSet.Tables.Contains("Переменные"))
            {
                if (data.DataSet.Tables["Переменные"].Columns
                    .Contains(data.TableName + "." + data.Columns[i].Caption + ".ширина столбца"))
                {
                    return DoubleValue.FromDouble(
                        (double) data.DataSet.Tables["Переменные"].Rows[0][
                            data.TableName + "." + data.Columns[i].Caption + ".ширина столбца"]);
                }
            }

            const double digitWidth = 8;
            const double digitWidthBold = 9;
            var length = (from DataRow r in data.Rows where r[i] != null && !String.IsNullOrEmpty(r[i].ToString()) 
                          select r[i].ToString().Length).Concat(new[] {0}).Max();
            var defaultWidth = Math.Truncate(((length * digitWidth / 7.0d) * 256.0d + 128.0d ) / 256.0d);
            defaultWidth = (double)decimal.Round((decimal)defaultWidth + 0.2M, 2);
            defaultWidth = Math.Min(defaultWidth, 25);
            var maxHeaderWordLength =
                data.Columns[i].ColumnName.Split(new[] {' '}).ToList().Select(a => a.Trim().Length).Max();
            var headerWidth = Math.Truncate((((maxHeaderWordLength + 1) * digitWidthBold / 7.0d) * 256.0d + 128.0d) / 256.0d);
            headerWidth = (double)decimal.Round((decimal)headerWidth + 0.2M, 2);
            headerWidth = Math.Min(headerWidth, 18);
            return Math.Max(defaultWidth, headerWidth);
        }

        private static void CreateMergedCells(OpenXmlWriter tableWriter, DataTable data, IEnumerable<string> columnNames,
            Dictionary<string, int> fieldIndexes, Dictionary<string, bool> mergingColumns)
        {

            var merged = new MergeCells();

            uint cellInRowCounter = 0;
            var enableHierarchySubdivision = data.ExtendedProperties.ContainsKey("EnableHierarchySubdivision") && (bool)data.ExtendedProperties["EnableHierarchySubdivision"];
            var borderExistance = new bool[data.Rows.Count];
            
            bool mergeFound = false;
            foreach (var columnName in columnNames)
            {

                if (mergingColumns.ContainsKey(columnName) && mergingColumns[columnName])
                {
                    var columnReference = GetColumnName(cellInRowCounter);
                    var i = fieldIndexes[columnName];

                    var rowNumber = 2;
                    string previousValue = null;
                    var oldRowNumber = 2;
                    var combinedCounter = 0;

                    foreach (DataRow r in data.Rows)
                    {
                        var cellContent = r.ItemArray[i] != null ? r.ItemArray[i].ToString() : null;
                        if (cellContent != previousValue || borderExistance[rowNumber - 2])
                        {
                            if (combinedCounter > 0)
                            {
                                var startCellReference = columnReference + oldRowNumber;
                                var endCellReference = columnReference + (rowNumber - 1);
                                if (!mergeFound) tableWriter.WriteStartElement(merged);
                                mergeFound = true;
                                var mc = new MergeCell
                                {
                                    Reference = new StringValue(startCellReference + ":" + endCellReference)
                                };
                                tableWriter.WriteElement(mc);
                            }
                            if ((cellContent != previousValue) && enableHierarchySubdivision)borderExistance[rowNumber - 2] = true;
                            oldRowNumber = rowNumber;
                            combinedCounter = 0;
                        }
                        else
                        {
                            combinedCounter++;
                        }
                        rowNumber++;
                        previousValue = cellContent;
                    }
                    if (combinedCounter > 0)
                    {
                        var startCellReference = columnReference + oldRowNumber;
                        var endCellReference = columnReference + (rowNumber - 1);
                        var mc = new MergeCell
                        {
                            Reference = new StringValue(startCellReference + ":" + endCellReference)
                        };
                        tableWriter.WriteElement(mc);
                    }
                }
                cellInRowCounter++;
            }
            if (mergeFound) tableWriter.WriteEndElement(); //mergedcells
        }

        
        private static string GetColumnName(uint number)
        {
            if (number > 65535) throw new Exception();
            if (number < 26) return Encoding.Default.GetString(new [] {(byte) (65 + number)});
            var name = String.Empty;
            number++;
            do
            {
                var div = --number%26;
                number /= 26;
                name = Encoding.Default.GetString(new [] {(byte) (65 + div)}) + name;
            } while (number > 0);
            return name;
        }
    }

    public class PackagePartStream : Stream
    {
        private readonly Stream _stream;

        private static readonly Mutex Mutex = new Mutex(false);

        public PackagePartStream(Stream stream)
        {
            _stream = stream;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _stream.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _stream.SetLength(value);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _stream.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            Mutex.WaitOne(Timeout.Infinite, false);
            _stream.Write(buffer, offset, count);
            Mutex.ReleaseMutex();
        }

        public override bool CanRead
        {
            get { return _stream.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _stream.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _stream.CanWrite; }
        }

        public override long Length
        {
            get { return _stream.Length; }
        }

        public override long Position
        {
            get { return _stream.Position; }
            set { _stream.Position = value; }
        }

        public override void Flush()
        {
            Mutex.WaitOne(Timeout.Infinite, false);
            _stream.Flush();
            Mutex.ReleaseMutex();
        }

        public override void Close()
        {
            _stream.Close();
        }

        protected override void Dispose(bool disposing)
        {
            _stream.Dispose();
        }
    }

}