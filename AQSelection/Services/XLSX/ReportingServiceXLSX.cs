﻿using System;
using System.Data;
using System.Linq;
using System.ComponentModel;

// ReSharper disable once CheckNamespace
namespace AQSelection
{
    public partial class ReportingService
    {
        #region Операции с XLSX

        public void GetAllResultsInXlsx(Guid OperationGuid, string StoredDataSetGuid)
        {
            var dataStore = Sessions.GetItem(StoredDataSetGuid) as DataSet;
            var hc = Sessions.GetHttpContext();
            var memoryStreamGuid = new Guid();
            try
            {
                memoryStreamGuid = XLSXFactory.CreateXLSXForAllTables(dataStore, StoredDataSetGuid, hc);
                ExtractFileNameFromVariables(dataStore, memoryStreamGuid);
                client.Receive(OperationGuid, new OperationResult("Файл Xlsx создан", true, OperationGuid.ToString(), memoryStreamGuid));
            }
            catch(Exception e)
            {
                client.Receive(OperationGuid, new OperationResult(e.Message, false, OperationGuid.ToString(), memoryStreamGuid)); 
            }
        }

        private static void ExtractFileNameFromVariables(DataSet DataStore, Guid xlsxGuid)
        {
            if (DataStore.Tables.Contains("Переменные"))
            {
                if (DataStore.Tables["Переменные"].Columns.Contains("FileName"))
                {
                    Sessions.NewItem(xlsxGuid + "-FileName", DataStore.Tables["Переменные"].Rows[0]["FileName"]);
                }
            }
        }
        #endregion
    }
}