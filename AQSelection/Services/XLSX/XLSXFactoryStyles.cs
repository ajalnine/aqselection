﻿using System;
using System.Collections.Generic;
using System.Data;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

// ReSharper disable once CheckNamespace
namespace AQSelection
{
    public  static partial class XLSXFactory
    {
        private enum XLSXFont
        {
            Default = 0, Red = 1, Blue = 2, Bold = 3
        }
        private enum XLSXNumberFormat
        {
            Default = 0, DateTime = 165, Date = 166, Time = 167
        }
        
        [Flags]
        private enum XLSXBorder
        {
            None=0x10, Left = 0x01, Right = 0x02, Top = 0x04, Bottom = 0x08, Thin= 0x00
        }

        private static void CreateCellStyleFormats(Stylesheet ss)
        {
            ss.CellStyleFormats = new CellStyleFormats();
            ss.CellStyleFormats.AppendChild(new CellFormat
            {
                NumberFormatId = 0,
                FontId = 0,
                BorderId = 0,
                FillId = 0
            });
        }

        private static void CreateNumberingFormats(Stylesheet ss)
        {
            ss.NumberingFormats = new NumberingFormats();
            ss.NumberingFormats.AppendChild(new NumberingFormat
            {
                FormatCode = @"dd.mm.yy\ hh:mm:ss;@",
                NumberFormatId = 165
            });
            ss.NumberingFormats.AppendChild(new NumberingFormat
            {
                FormatCode = @"dd.mm.yy;@",
                NumberFormatId = 166
            });
            ss.NumberingFormats.AppendChild(new NumberingFormat
            {
                FormatCode = @"[h]:mm:ss;@",
                NumberFormatId = 167
            });
        }

        private static Dictionary<string, uint> CreateFills(DataSet dataStore, Stylesheet ss)
        {
            ss.Fills = new Fills();
            ss.Fills.AppendChild(new Fill() );
            ss.Fills.AppendChild(new Fill() );

            ss.Fills.AppendChild(new Fill
            {
                PatternFill =
                    new PatternFill
                    {
                        PatternType = new EnumValue<PatternValues>(PatternValues.Solid),
                        ForegroundColor =
                            new ForegroundColor {Rgb = "FFC3DEE0"},
                        BackgroundColor = new BackgroundColor {Indexed = 64}
                    }
            });
            var fillDictionary = SetupFills(dataStore, ss);
            return fillDictionary;
        }

        private static void CreateFonts(Stylesheet ss)
        {
            ss.Fonts = new Fonts();
            ss.Fonts.AppendChild(new Font());

            var red = new Font();
            red.AppendChild(new Color {Rgb = "FFFF0000"});
            ss.Fonts.AppendChild(red);

            var blue = new Font();
            blue.AppendChild(new Color {Rgb = "FF0000FF"});
            ss.Fonts.AppendChild(blue);

            var bold = new Font();
            bold.AppendChild(new Bold());
            ss.Fonts.AppendChild(bold);
        }

        private static void CreateBorders(Stylesheet ss)
        {
            ss.Borders = new Borders();
            ss.Borders.AppendChild(new Border
            {
                LeftBorder = new LeftBorder(),
                RightBorder = new RightBorder(),
                TopBorder = new TopBorder(),
                BottomBorder = new BottomBorder(),
                DiagonalBorder = new DiagonalBorder()
            });
            for(var i=0; i<=0x0f; i++)CreateBorder(ss, i);
        }

        private static void CreateBorder(Stylesheet ss, int i)
        {
            ss.Borders.AppendChild(new Border
            {
                LeftBorder = new LeftBorder
                {
                    Style = ((i&0x01)==0x01) ? BorderStyleValues.Medium :  BorderStyleValues.Thin,
                    Color = new Color {Rgb = ((i&0x01)==0x01) ? "FF000000" : "FF808080"}
                },
                RightBorder = new RightBorder
                {
                    Style = ((i & 0x02) == 0x02) ? BorderStyleValues.Medium : BorderStyleValues.Thin,
                    Color = new Color { Rgb = ((i & 0x02) == 0x02) ? "FF000000" : "FF808080" }
                },
                TopBorder = new TopBorder
                {
                    Style = ((i & 0x04) == 0x04) ? BorderStyleValues.Medium : BorderStyleValues.Thin,
                    Color = new Color { Rgb = ((i & 0x04) == 0x04) ? "FF000000" : "FF808080" }
                },
                BottomBorder = new BottomBorder
                {
                    Style = ((i & 0x08) == 0x08) ? BorderStyleValues.Medium : BorderStyleValues.Thin,
                    Color = new Color { Rgb = ((i & 0x08) == 0x08) ? "FF000000" : "FF808080" }
                }
            });
        }


        private static uint GetStyle(Stylesheet ss, XLSXNumberFormat numberFormatId, XLSXFont font, XLSXBorder border, uint fillId, bool aligned, Dictionary<string, uint> styleDictionary)
        {
            var borderId = border == XLSXBorder.None ? 0 : (uint)border + 1;
            var key = ((uint)numberFormatId).ToString("x2") + ((uint)font).ToString("x2") + borderId.ToString("x2") +
                      fillId.ToString("x8") + (aligned ? "1" : "0");
            if (styleDictionary.ContainsKey(key)) return styleDictionary[key];

            var cf = new CellFormat
            {
                NumberFormatId = (uint)numberFormatId,
                FontId = (uint)font,
                BorderId = borderId,
                FillId = fillId,
                FormatId = 0,
                ApplyAlignment = true
            };
            if (numberFormatId > 0) cf.ApplyAlignment = true;
            var alignment = new Alignment
            {
                Vertical = new EnumValue<VerticalAlignmentValues> {Value = VerticalAlignmentValues.Center}
            };
            if (aligned)
            {
                alignment.Horizontal = new EnumValue<HorizontalAlignmentValues>{ Value = HorizontalAlignmentValues.Center};
                alignment.WrapText = true;
            }
            
            cf.AppendChild(alignment);
            ss.CellFormats.AppendChild(cf);
            var value = styleDictionary.Count;
            styleDictionary.Add(key, (uint)value);
            return (uint)value;
        }

        private static Dictionary<string, uint> SetupFills(DataSet dataStore, Stylesheet ss)
        {
            var fillDictionary = new Dictionary<string, uint>();
            uint fillsCounter = 3;
            foreach (DataTable t in dataStore.Tables)
            {
                int columnNumber = 0;
                foreach (DataColumn c in t.Columns)
                {
                    if (c.ColumnName.EndsWith("_Цвет"))
                    {
                        foreach (DataRow r in t.Rows)
                        {
                            string colorValue = r.ItemArray[columnNumber].ToString();
                            if (!fillDictionary.ContainsKey(colorValue))
                            {
                                fillDictionary.Add(colorValue, fillsCounter);
                                fillsCounter++;
                                ss.Fills.AppendChild(new Fill
                                {
                                    PatternFill =
                                        new PatternFill
                                        {
                                            PatternType = new EnumValue<PatternValues>(PatternValues.Solid),
                                            ForegroundColor =
                                                new ForegroundColor
                                                {
                                                    Rgb = colorValue.Replace("#", string.Empty).ToUpper()
                                                },
                                            BackgroundColor = new BackgroundColor { Indexed = 64 }
                                        }
                                });
                            }
                        }
                    }
                    columnNumber++;
                }
            }
            return fillDictionary;
        }
    }
}