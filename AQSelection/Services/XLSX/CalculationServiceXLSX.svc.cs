﻿using System.Globalization;
using System.IO;
using System;
using System.Linq;
using System.ServiceModel;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;

namespace AQSelection
{
    #region Создание XLSX
    public partial class CalculationService
    {
       #region Operation contracts
        [OperationContract]
        public StepResult GetAllResultsInXlsx(string StoredDataSetGuid)
        {
            DataSet DataStore = Sessions.GetItem(StoredDataSetGuid) as DataSet;
            Guid xlsxGuid = Guid.NewGuid();
            try
            {
                xlsxGuid = XLSXFactory.CreateXLSXForAllTables(DataStore, StoredDataSetGuid, HttpContext.Current);
                ExtractFileNameFromVariables(DataStore, xlsxGuid);
                return new StepResult("Экспорт завершен", true, xlsxGuid.ToString());
            }
            catch(Exception e)
            {
                return new StepResult(e.Message, false, xlsxGuid.ToString());
            }
        }

        private static void ExtractFileNameFromVariables(DataSet DataStore, Guid xlsxGuid)
        {
            if (DataStore.Tables.Contains("Переменные"))
            {
                if (DataStore.Tables["Переменные"].Columns.Contains("FileName"))
                {
                    Sessions.NewItem(xlsxGuid + "-FileName", DataStore.Tables["Переменные"].Rows[0]["FileName"]);
                }
            }
        }

        #endregion
    }
    #endregion
}