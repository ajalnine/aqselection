﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Linq;
using System.Web;
using System.Text;

namespace AQSelection
{
    [ServiceContract(CallbackContract = typeof(IReportingDuplexClient), SessionMode = SessionMode.Required)]
    public interface IReportingDuplexService
    {
        [OperationContract]
        void GetTablesSLCompatible(Guid OperationGuid, string StoredDataSetGuid);

        [OperationContract]
        void GetAllResultsInXlsx(Guid OperationGuid, string StoredDataSetGuid);

        [OperationContract]
        void FillDataWithSLTables(Guid OperationGuid, List<ReportDataTable> sldt);

        [OperationContract]
        void CreatePPTX(Guid OperationGuid, PresentationDescription PD);
        [OperationContract]
        void AppendPPTX(Guid OperationGuid, Guid pdGuid, PresentationDescription PD);
        [OperationContract]
        void GetPPTX(Guid OperationGuid, Guid PDGuid);

        [OperationContract]
        void CreateDOCX(Guid OperationGuid, PresentationDescription PD);
        [OperationContract]
        void AppendDOCX(Guid OperationGuid, Guid pdGuid, PresentationDescription PD);
        [OperationContract]
        void GetDOCX(Guid OperationGuid, Guid PDGuid);

        [OperationContract]
        void SendBinary(Guid OperationGuid, List<byte> Array);
        
        [OperationContract]
        void CancelOperation(Guid OperationGuid);

        [OperationContract]
        void DropData(Guid DropItemGuid);
    }

    [ServiceContract]
    public interface IReportingDuplexClient
    {
        [OperationContract(IsOneWay = true)]
        void Receive(Guid OperationGuid, OperationResult or);

        [OperationContract(IsOneWay = true)]
        void Progress(Guid OperationGuid, double NewProgress);
    }
}
