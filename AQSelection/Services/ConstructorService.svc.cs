﻿using System;
using System.Linq;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace AQSelectionConstructorService
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ConstructorService
    {
        [OperationContract]
        public ParametersListTaskResult GetTestsParametersList(string Filter, string FilterForChemistry)
        {
            ParametersList tpl = new ParametersList();

            if (Filter == String.Empty) Filter = " 1=1 ";
            if (FilterForChemistry == String.Empty) FilterForChemistry = " 1=1 ";
            tpl.ChemistryParameters = new List<ParamDescription>();
            tpl.OtherParameters = new List<ParamDescription>();
            tpl.AllParameters = new List<ParamDescription>();
            tpl.AllGroups = new List<GroupDescription>();

            tpl.DuplicatedParameters = new List<string>();
            
            string SQL = @"set dateformat ymd
                        select distinct tbl_Parameters.DisplayParameterName as param, tbl_paramtype.Paramtype, tbl_Paramtype.id, 
                        tbl_Parameters.IsNumber, tbltests.id_parameter as id_param, tbl_Parameters.ParameterName, tbl_paramtype.Position, tbl_parameters.Position from tblTestHead 
                        inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                        inner join tbl_Paramtype on tbl_Paramtype.id=tblTestHead.id_paramtype
                        inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter
                        where tblTestHead.id_paramtype<>722 and  " + Filter + @" 
                        union 
                        select distinct tbl_Parameters.DisplayParameterName as param, tbl_paramtype.Paramtype, tbl_Paramtype.id, 
                        tbl_Parameters.IsNumber, tbltests.id_parameter as id_param, tbl_Parameters.ParameterName, tbl_paramtype.Position, tbl_parameters.Position from tblTestHead 
                        inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                        inner join tbl_Paramtype on tbl_Paramtype.id=tblTestHead.id_paramtype
                        inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter
                        where tblTestHead.id_paramtype=722 and  " + FilterForChemistry + @"   order by tbl_paramtype.Position, Paramtype, tbl_parameters.Position, DisplayParameterName";

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
             SqlDataReader sdr = sc.ExecuteReader();
            
            while (sdr.Read())
             {
                 if ((from GroupDescription g in tpl.AllGroups where g.GroupID == sdr.GetInt32(2) select g).Count() == 0) tpl.AllGroups.Add(new GroupDescription(sdr.GetInt32(2), sdr.GetString(1)));
                 switch (sdr.GetInt32(2))
                 {
                     case 722:
                         tpl.HasChemistry = true;
                         tpl.ChemistryParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
                         break;
                     default:
                         tpl.HasOther = true;
                         tpl.OtherParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
                         break;
                 }
                 tpl.AllParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
             }
             sdr.Close();

             string SQL2 = @"select param from
                            (
                                select distinct tbl_parameters.DisplayParameterName as param, tbl_Paramtype.id from tblTestHead 
                                inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                                inner join tbl_Paramtype on tbl_Paramtype.id=tblTestHead.id_paramtype
                                inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter
                                where tbl_paramtype.id<>722 and " + Filter + @" 
                                union
                                select distinct tbl_parameters.DisplayParameterName as param, tbl_Paramtype.id from tblTestHead 
                                inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                                inner join tbl_Paramtype on tbl_Paramtype.id=tblTestHead.id_paramtype
                                inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter
                                where tbl_paramtype.id=722 and " + FilterForChemistry + @" 
                            ) as t
                            group by param
                            having COUNT(param)>1
                            order by param";
             SqlCommand sc2 = new SqlCommand(SQL2, sconn);
             sc2.CommandType = System.Data.CommandType.Text;
             sc2.CommandTimeout = 3000;
             SqlDataReader sdr2 = sc2.ExecuteReader();

             while (sdr2.Read())
             {
                 tpl.DuplicatedParameters.Add(sdr2[0].ToString().ToLower());
             }
             sdr2.Close();
             sconn.Close();
            if (tpl.AllParameters.Count!=0)  return new ParametersListTaskResult("Список полей получен", true,  tpl);
            else return new ParametersListTaskResult("Нет данных", false, null);
        }

        [OperationContract]
        public ParametersListTaskResult GetOELParametersList(string Filter)
        {
            ParametersList tpl = new ParametersList();
            if (Filter == String.Empty) Filter = " 1=1 ";
            tpl.AllParameters = new List<ParamDescription>();
            tpl.AllGroups = new List<GroupDescription>();
            string SQL = @"set dateformat ymd 
                        select distinct tbl_Parameters.DisplayParameterName as param, tbl_paramtype.Paramtype, tbl_Paramtype.id, 
                        tbl_Parameters.IsNumber, oel_elements.id_parameter as id_param, tbl_Parameters.ParameterName, tbl_paramtype.Position, tbl_parameters.Position from oel_head 
                        inner join oel_elements on oel_elements.id_test=oel_head.id_test 
                        inner join tbl_Paramtype on tbl_Paramtype.id=oel_head.id_paramtype
                        inner join tbl_Parameters on tbl_Parameters.id = oel_elements.id_parameter
                        where " + Filter + " and tbl_parameters.id_paramtype=722 order by tbl_paramtype.Position, Paramtype, tbl_parameters.Position, DisplayParameterName";

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
             SqlDataReader sdr = sc.ExecuteReader();
            
            while (sdr.Read())
             {
                 if ((from GroupDescription g in tpl.AllGroups where g.GroupID == sdr.GetInt32(2) select g).Count() == 0) tpl.AllGroups.Add(new GroupDescription(sdr.GetInt32(2), sdr.GetString(1)));
                 tpl.AllParameters.Add(new ParamDescription(sdr[0].ToString(), (sdr[0].ToString() == sdr[5].ToString()) ? sdr[0].ToString() : sdr[0].ToString() + "(" + sdr[5].ToString() + ")", sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
             }
             sdr.Close();
             sconn.Close();
             if (tpl.AllParameters.Count != 0) return new ParametersListTaskResult("Список полей получен", true, tpl);
             else return new ParametersListTaskResult("Нет данных", false, null);
        }

        [OperationContract]
        public ParametersListTaskResult GetTechnologyParametersList(string Filter)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            
            ParametersList tpl = new ParametersList();
            if (Filter == String.Empty) Filter = " 1=1 ";
            tpl.AllParameters = new List<ParamDescription>();
            tpl.AllGroups = new List<GroupDescription>();
            string SQL = @"set dateformat ymd 
                        select distinct Common_Parameters.DisplayParameterName as param, Common_ParameterTypes.Paramtype, Common_ParameterTypes.id, 
                        Common_Parameters.IsNumber, Technology_Values.ParameterID as id_param, Common_Parameters.ParameterName, Common_ParameterTypes.Position, Common_Parameters.Position from Technology_Smelts 
                        inner join Technology_Values on Technology_Values.SmeltID=Technology_Smelts.SmeltID 
                        inner join Common_Parameters on Common_Parameters.id = Technology_Values.ParameterID
                        inner join Common_ParameterTypes on Common_ParameterTypes.id=Common_Parameters.id_paramtype
                        where " + Filter + " order by Common_ParameterTypes.Position, Paramtype, Common_Parameters.Position, DisplayParameterName";

            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            while (sdr.Read())
            {
                if ((from GroupDescription g in tpl.AllGroups where g.GroupID == sdr.GetInt32(2) select g).Count() == 0) tpl.AllGroups.Add(new GroupDescription(sdr.GetInt32(2), sdr.GetString(1)));
                tpl.AllParameters.Add(new ParamDescription(sdr[0].ToString(), (sdr[0].ToString() == sdr[5].ToString()) ? sdr[0].ToString() : sdr[0].ToString() + "(" + sdr[5].ToString() + ")", sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
            }
            sdr.Close();
            if (tpl.AllParameters.Count == 0)
            {
                sconn.Close();
                return new ParametersListTaskResult("Нет данных", false, null);
            }
            string SQL2 = @"set dateformat ymd 
                        select distinct Technology_Values.ParameterID as id_param, Technology_Values.ProcessingNumber as Variant1, Technology_Values.OptionalCategory as Variant2 from Technology_Smelts 
                        inner join Technology_Values on Technology_Values.SmeltID=Technology_Smelts.SmeltID 
                        inner join Common_Parameters on Common_Parameters.id = Technology_Values.ParameterID
                        inner join Common_ParameterTypes on Common_ParameterTypes.id=Common_Parameters.id_paramtype
                        where " + Filter ;

            SqlCommand sc2 = new SqlCommand(SQL2, sconn);
            sc2.CommandTimeout = 3000;
            sc2.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr2 = sc2.ExecuteReader();

            while (sdr2.Read())
            {
                int ParameterID = (int)sdr2[0];
                ParameterVariantDescription PVD = new ParameterVariantDescription();
                PVD.VariatedValue1 = (sdr2[2] == DBNull.Value) ? null : (string)sdr2[2];
                PVD.VariatedValue2 = (sdr2[1] == DBNull.Value) ? null : sdr2[1].ToString();
                string Divider=(PVD.VariatedValue1!=null && PVD.VariatedValue2!=null) ? ", ":String.Empty;
                string ProcessingName = (PVD.VariatedValue2 == null) ? String.Empty : "Обр" + PVD.VariatedValue2;
                PVD.VariantName = (PVD.VariatedValue1 ?? String.Empty) + Divider + ProcessingName;
                foreach (var p in tpl.AllParameters.Where(a => a.ParamID == ParameterID)) p.ParameterVariants.Add(PVD);
            }
            sdr2.Close();

            sconn.Close();
            tpl.DuplicatedParameters = new List<string>();
            foreach (var p in tpl.AllParameters)
            {
                if ((from a in tpl.AllParameters where a.Param == p.Param select a.Param).Count() > 1)
                {
                    if (!tpl.DuplicatedParameters.Contains(p.Param)) tpl.DuplicatedParameters.Add(p.Param.ToLower());
                }
            }
            return new ParametersListTaskResult("Список полей получен", true, tpl); 
        }

        [OperationContract]
        public ParametersListTaskResult GetRequirementsParametersList(string filter)
        {
            var tpl = new ParametersList();

            if (filter == String.Empty) filter = " 1=1 ";
            tpl.ChemistryParameters = new List<ParamDescription>();
            tpl.OtherParameters = new List<ParamDescription>();
            tpl.AllParameters = new List<ParamDescription>();
            tpl.AllGroups = new List<GroupDescription>();

            tpl.DuplicatedParameters = new List<string>();

            var sql = @"set dateformat ymd 
            select distinct Common_Parameters.DisplayParameterName as param, Common_ParameterTypes.Paramtype, Common_ParameterTypes.id, 
            Common_Parameters.IsNumber, Requirements.ParamID as id_param, Common_Parameters.ParameterName, Common_ParameterTypes.Position, Common_Parameters.Position 
            from Requirements 
            INNER JOIN 
            Common_Mark ON Requirements.MarkID = Common_Mark.id left outer JOIN 
            Common_NTD ON Requirements.NtdID = Common_NTD.id INNER JOIN 
            Common_ParameterTypes ON Requirements.ParamTypeID = Common_ParameterTypes.id INNER JOIN 
            Common_Parameters ON Common_ParameterTypes.id = Common_Parameters.id_paramtype AND Requirements.ParamID = Common_Parameters.id 
            where " + filter + " order by Common_ParameterTypes.Position, Paramtype, Common_Parameters.Position, DisplayParameterName ";

            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            using (var sconn = new SqlConnection(connectionString))
            {
                sconn.Open();
                var sc = new SqlCommand(sql, sconn) {CommandTimeout = 3000, CommandType = CommandType.Text};
                var sdr = sc.ExecuteReader();

                while (sdr.Read())
                {
                    if (!(from GroupDescription g in tpl.AllGroups where g.GroupID == sdr.GetInt32(2) select g).Any()) tpl.AllGroups.Add(new GroupDescription(sdr.GetInt32(2), sdr.GetString(1)));
                    switch (sdr.GetInt32(2))
                    {
                        case 722:
                            tpl.HasChemistry = true;
                            tpl.ChemistryParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
                            break;
                        default:
                            tpl.HasOther = true;
                            tpl.OtherParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
                            break;
                    }
                    tpl.AllParameters.Add(new ParamDescription(sdr[0].ToString(), sdr[5].ToString(), sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2)));
                }
                sdr.Close();

                var sql2 = @"set dateformat ymd select param from
                (
                select distinct Common_Parameters.DisplayParameterName as param, Common_ParameterTypes.id from Requirements 
                INNER JOIN Common_ParameterTypes ON Requirements.ParamTypeID = Common_ParameterTypes.id INNER JOIN
	            Common_Parameters ON Common_ParameterTypes.id = Common_Parameters.id_paramtype AND Requirements.ParamID = Common_Parameters.id 
                where " + filter + @") as t
                group by param
                having COUNT(param)>1
                order by param";
                var sc2 = new SqlCommand(sql2, sconn) {CommandType = CommandType.Text, CommandTimeout = 3000};
                var sdr2 = sc2.ExecuteReader();

                while (sdr2.Read())
                {
                    tpl.DuplicatedParameters.Add(sdr2[0].ToString().ToLower());
                }
                sdr2.Close();
                sconn.Close();
            }
            return tpl.AllParameters.Count != 0 ? new ParametersListTaskResult("Список полей получен", true, tpl) : new ParametersListTaskResult("Нет данных", false, null);
        }

        [OperationContract]
        public ParametersListTaskResult GetPerUnitControlParametersList(string Filter)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();

            ParametersList tpl = new ParametersList();
            if (Filter == String.Empty) Filter = " 1=1 ";
            tpl.AllParameters = new List<ParamDescription>();
            tpl.AllGroups = new List<GroupDescription>();
            string SQL = @" set dateformat ymd 
                            select distinct Common_Parameters.DisplayParameterName as param, 
                            Common_ParameterTypes.Paramtype, 
                            Common_ParameterTypes.id, 
                            Common_Parameters.IsNumber, 
                            Control_Values.ParameterID as id_param, 
                            Common_Parameters.ParameterName, 
                            Common_ParameterTypes.Position, 
                            Common_Parameters.Position,
                            Control_Parts.ControlLevel 
                            from Control_Parts 
                            inner join Technology_Smelts on Technology_Smelts.SmeltID=Control_Parts.TechnologySmeltID 
                            inner join Control_Values on Control_Parts.CPN = Control_Values.CPN
                            inner join Common_Parameters on Common_Parameters.id = Control_Values.ParameterID
                            inner join Common_ParameterTypes on Common_ParameterTypes.id=Common_Parameters.id_paramtype
                            where " + Filter + " order by Common_ParameterTypes.Position, Paramtype, Common_Parameters.Position, DisplayParameterName, Control_Parts.ControlLevel ";

            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            while (sdr.Read())
            {
                if ((from GroupDescription g in tpl.AllGroups where g.GroupID == sdr.GetInt32(2) select g).Count() == 0) tpl.AllGroups.Add(new GroupDescription(sdr.GetInt32(2), sdr.GetString(1)));
                tpl.AllParameters.Add(new ParamDescription(sdr[0].ToString(), (sdr[0].ToString() == sdr[5].ToString()) ? sdr[0].ToString() : sdr[0].ToString() + "(" + sdr[5].ToString() + ")", sdr[1].ToString(), sdr.GetBoolean(3), sdr.GetInt32(4), sdr.GetInt32(2), (int)sdr.GetByte(8), sdr.GetInt32(6), sdr.GetInt32(7)));
            }
            sdr.Close();
            if (tpl.AllParameters.Count == 0)
            {
                sconn.Close();
                return new ParametersListTaskResult("Нет данных", false, null);
            }

            string SQL2 = @"set dateformat ymd 
                        select distinct Control_Values.ParameterID as id_param, Control_Values.PerBilletProcessingNumber as Variant1, Control_Parts.Aggregate as Variant2 from Control_Parts 
                        inner join Technology_Smelts on Technology_Smelts.SmeltID=Control_Parts.TechnologySmeltID 
                        inner join Control_Values on Control_Values.CPN=Control_Parts.CPN 
                        inner join Common_Parameters on Common_Parameters.id = Control_Values.ParameterID
                        inner join Common_ParameterTypes on Common_ParameterTypes.id=Common_Parameters.id_paramtype where " + Filter;

            SqlCommand sc2 = new SqlCommand(SQL2, sconn);
            sc2.CommandTimeout = 3000;
            sc2.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr2 = sc2.ExecuteReader();

            while (sdr2.Read())
            {
                int ParameterID = (int)sdr2[0];
                ParameterVariantDescription PVD = new ParameterVariantDescription();
                PVD.VariatedValue1 = (sdr2[1] == DBNull.Value) ? null : sdr2[1].ToString();
                PVD.VariatedValue2 = (sdr2[2] == DBNull.Value) ? null : sdr2[2].ToString();
                string Divider = (PVD.VariatedValue1 != null && PVD.VariatedValue2 != null) ? ", " : String.Empty;
                string ProcessingName = (PVD.VariatedValue1 == null) ? String.Empty : "№ Обр. " + PVD.VariatedValue1;
                PVD.VariantName = (PVD.VariatedValue2 ?? String.Empty) + Divider + ProcessingName;
                foreach (var p in tpl.AllParameters.Where(a => a.ParamID == ParameterID)) p.ParameterVariants.Add(PVD);
            }
            sdr2.Close();

            sconn.Close();
            tpl.DuplicatedParameters = new List<string>();
            foreach (var p in tpl.AllParameters)
            {
                if ((from a in tpl.AllParameters where a.Param == p.Param select a.Param).Count() > 1)
                {
                    if (!tpl.DuplicatedParameters.Contains(p.Param)) tpl.DuplicatedParameters.Add(p.Param.ToLower());
                }
            }
            return new ParametersListTaskResult("Список полей получен", true, tpl); 
        }

        [OperationContract]
        public FieldListCombineTaskResult GetTestsCorrectionList(string SQLFilter, ParametersList aqpl, bool IsStatistic)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL;
            SQL = @" set dateformat ymd select count(*) from (SELECT count(*) as c FROM tbltesthead INNER JOIN
                          tbltests ON tbltesthead.test_id = tbltests.test_id INNER JOIN
                          Common_ParameterTypes ON tbltesthead.id_paramtype = Common_ParameterTypes.id INNER JOIN
                          Common_Parameters ON Common_ParameterTypes.id = Common_Parameters.id_paramtype AND tbltests.id_parameter = Common_Parameters.id 
                              where " + SQLFilter + @" group by " + ((IsStatistic) ? " tbltesthead.melt" : "tbltesthead.melt, tbltesthead.meltpos_id") + ") as t";
            SqlCommand sc0 = new SqlCommand(SQL, sconn);
            sc0.CommandTimeout = 3000;
            sc0.CommandType = System.Data.CommandType.Text;
            FieldListCombination flc = new FieldListCombination();
            flc.FieldListCombined = new List<FieldList>();
            flc.TotalCount = (int)sc0.ExecuteScalar();

            return GetCombinationsFromReferences(aqpl, sconn, flc);
        }

        [OperationContract]
        public FieldListCombineTaskResult GetOELCorrectionList(string SQLFilter, ParametersList aqpl, bool PerMelt)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL;
            SQL = @" set dateformat ymd select count(*) from (SELECT count(*) as c FROM oel_head INNER JOIN
                          oel_elements ON oel_head.id_test = oel_elements.id_test INNER JOIN
                          tbl_Paramtype ON oel_head.id_paramtype = tbl_Paramtype.id INNER JOIN
                          tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND oel_elements.id_parameter = tbl_Parameters.id 
                              where " + SQLFilter + @" group by " + ((PerMelt) ? " oel_head.melt" : "oel_head.id_test") + ") as t";
            SqlCommand sc0 = new SqlCommand(SQL, sconn);
            sc0.CommandTimeout = 3000;
            sc0.CommandType = System.Data.CommandType.Text;
            FieldListCombination flc = new FieldListCombination();
            flc.FieldListCombined = new List<FieldList>();
            flc.TotalCount = (int)sc0.ExecuteScalar();

            return GetCombinationsFromReferences(aqpl, sconn, flc);
        }

        [OperationContract]
        public FieldListCombineTaskResult GetTechnologyCorrectionList(string SQLFilter, ParametersList aqpl, bool PerProcessing, bool groupByMelt)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL;
            SQL = @" set dateformat ymd select count(*) from (SELECT count(*) as c FROM Technology_Smelts 
                          INNER JOIN Technology_Values ON Technology_Smelts.SmeltID = Technology_Values.SmeltID 
                          INNER JOIN Common_Parameters ON Technology_Values.ParameterID = Common_Parameters.id 
                          INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id 
                              where " + SQLFilter + @" group by " + ((PerProcessing) 
                              ? groupByMelt 
                                ? " Technology_Smelts.SmeltNumber, Technology_Values.ProcessingNumber"
                                : " Technology_Smelts.SmeltID, Technology_Values.ProcessingNumber"
                              : groupByMelt 
                                ? " Technology_Smelts.SmeltNumber"
                                : " Technology_Smelts.SmeltID"
                                ) + ") as t";
            SqlCommand sc0 = new SqlCommand(SQL, sconn);
            sc0.CommandTimeout = 3000;
            sc0.CommandType = System.Data.CommandType.Text;
            FieldListCombination flc = new FieldListCombination();
            flc.FieldListCombined = new List<FieldList>();
            flc.TotalCount = (int)sc0.ExecuteScalar();

            return GetCombinationsFromReferences(aqpl, sconn, flc);
        }

        [OperationContract]
        public FieldListCombineTaskResult GetPerUnitControlCorrectionList(string SQLFilter, ParametersList aqpl)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL;
            SQL = @" set dateformat ymd select count(*) from (SELECT count(*) as c FROM Control_Parts 
                    INNER JOIN Technology_Smelts ON Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                    INNER JOIN Control_Values on Control_Values.CPN = Control_Parts.CPN
                    INNER JOIN Common_Parameters ON Control_Values.ParameterID = Common_Parameters.id 
                    INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id 
                    where " + SQLFilter + @" group by Control_Parts.InputMark) as t";
            SqlCommand sc0 = new SqlCommand(SQL, sconn);
            sc0.CommandTimeout = 3000;
            sc0.CommandType = System.Data.CommandType.Text;
            FieldListCombination flc = new FieldListCombination();
            flc.FieldListCombined = new List<FieldList>();
            flc.TotalCount = (int)sc0.ExecuteScalar();

            return GetCombinationsFromReferences(aqpl, sconn, flc);
        }

        private static FieldListCombineTaskResult GetCombinationsFromReferences(ParametersList aqpl, SqlConnection sconn, FieldListCombination flc)
        {
            List<FieldList> l = flc.FieldListCombined;

            string SQL2 = @"select p.id, p.id_paramtype, p.DisplayParameterName from tbl_Parameters as p
                    inner join 
                    (select id_paramtype, DisplayParameterName 
                    from tbl_Parameters where  id_paramtype in (null, " + FieldListHelper.GetAllGroupsList(aqpl) + @")
                    and DisplayParameterName in 
                    (
                    select DisplayParameterName from tbl_Parameters where id in (null, " + FieldListHelper.GetAllParametersList(aqpl) + @")
                    ) group by id_paramtype, DisplayParameterName  having COUNT(*)>1) as t 
                    on t.DisplayParameterName=p.DisplayParameterName and t.id_paramtype=p.id_paramtype
                    where p.id in (null, " + FieldListHelper.GetAllParametersList(aqpl) + @")
                    order by p.id_paramtype, p.DisplayParameterName
                    ";

            SqlCommand sc2 = new SqlCommand(SQL2, sconn);
            sc2.CommandTimeout = 3000;
            sc2.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr2 = sc2.ExecuteReader();

            string Parameter = string.Empty;
            int Group = -1;
            FieldList FL = null;
            while (sdr2.Read())
            {
                int CurrentID = sdr2.GetInt32(0);
                int CurrentGroupID = sdr2.GetInt32(1);
                string CurrentName = sdr2.GetString(2);
                if (CurrentGroupID != Group || CurrentName != Parameter)
                {
                    FL = new FieldList();
                    FL.Enabled = true;
                    FL.MainParameterID = CurrentID;
                    l.Add(FL);
                    Group = CurrentGroupID;
                    Parameter = CurrentName;
                }
                else FL.Combined.Add(CurrentID);
            }

            sdr2.Close();
            sconn.Close();

            List<int> ToDeleteUnready = new List<int>();
            foreach (FieldList fl in l)
            {
                if (fl.Combined.Count == 0)
                {
                    ToDeleteUnready.Add(fl.MainParameterID);
                }
            }

            foreach (int i in ToDeleteUnready)
            {
                FieldList f = (from FieldList fl in l where fl.MainParameterID == i select fl).Single();
                l.Remove(f);
            }
            return new FieldListCombineTaskResult("Записей:" + flc.TotalCount.ToString(), true, flc);
        }

        [OperationContract]
        private ProbeListTaskResult GetProbes(string Filter)
        {
            string SQL = @"set dateformat ymd select distinct oel_Probes.DisplayProbeName, oel_Probes.ID FROM oel_head INNER JOIN
                                  oel_elements ON oel_head.id_test = oel_elements.id_test INNER JOIN
                                  Common_Mark ON oel_head.id_cmark = Common_Mark.id left outer join
                                  Common_NTD ON oel_head.id_ntd = Common_NTD.id INNER JOIN
                                  tbl_Paramtype ON oel_head.id_paramtype = tbl_Paramtype.id INNER JOIN
                                  tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND oel_elements.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                                  oel_probes on oel_probes.id=oel_head.id_Probe LEFT OUTER JOIN
                                  oel_probetypes on oel_probetypes.id=oel_head.type
                                  where oel_probes.id is not null and " + Filter + " order by oel_probes.displayprobename";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            List<ProbeDescription> Probes = new List<ProbeDescription>();

            while (sdr.Read())
            {
                string Name = sdr[0].ToString();
                int ID = sdr.GetInt32(1);
                Probes.Add(new ProbeDescription(Name, ID));
            }
            return new ProbeListTaskResult("Список проб получен", true, Probes);
        }

        [OperationContract]
        private DefectsListTaskResult GetDefects(string Filter)
        {
            string SQL = @"set dateformat ymd select distinct DefectGroupID, DefectGroup, DefectID, Defect from Waste_Notices
                            inner join Waste_DefectGroups as WDG on Waste_Notices.DefectGroupID=WDG.id
                            inner join Waste_Defects as WD on Waste_Notices.DefectID=WD.id
                            where " + Filter + @" order by DefectGroup, Defect";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            List<DefectDescription> Defects = new List<DefectDescription>();

            while (sdr.Read())
            {
                int groupID = sdr.GetInt32(0);
                string groupName = sdr[1].ToString();
                int defectID = sdr.GetInt32(2);
                string defectName = sdr[3].ToString();
                Defects.Add(new DefectDescription(groupID, groupName, defectID, defectName));
            }
            return new DefectsListTaskResult("Список дефектов получен", true, Defects);
        }

        [OperationContract]
        private int GetRecordCount(string SQL)
        {
            string Sql = @"set dateformat YMD select count(*) as RecordNumber from (" + SQL + ") as t";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();

            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(Sql, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            int rc = (int)sc.ExecuteScalar();
            return rc;
        }

        [OperationContract]
        public List<object[]> GetRows(string SQL)
        {
            string[] RestrictedKeywords = new string[] { "Update ", "Delete ", "Modify ", "Truncate ", "exec " };
            
            foreach (string s in RestrictedKeywords)
            {
                if (SQL.ToLower().Contains(s.ToLower()))
                {
                    return null;
                }
            }

            List<object[]> RowCollection = new List<object[]>();

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();
            
            while (sdr.Read())
            {
                object[] Row = new object[sdr.FieldCount];
                for (int i = 0; i < sdr.FieldCount; i++)
                {
                    Row[i] = sdr.GetValue(i) ?? "NULL";
                }
                RowCollection.Add(Row);                
            }
            sdr.Close();
            sconn.Close();
            return RowCollection;
        }
    }

    public static class FieldListHelper
    {
        public static IEnumerable<ParamDescription> GetParametersLINQSourceByGroupID(ParametersList tpl, int GroupID, List<int> RestrictedFields)
        {
            return (from ParamDescription pd in tpl.AllParameters where pd.ParamTypeID == GroupID && !RestrictedFields.Contains(pd.ParamID) select pd);
        }

        public static string GetAllGroupsList(ParametersList tpl)
        {
            bool IsFirst = true;
            string GroupList = string.Empty;
            foreach (GroupDescription gd in tpl.AllGroups)
            {
                if (!IsFirst) GroupList += ",";
                GroupList += gd.GroupID.ToString();
                IsFirst = false;
            }
            return GroupList;
        }

        public static string GetAllParametersList(ParametersList tpl)
        {
            bool IsFirst = true;
            string ParametersList = string.Empty;
            foreach (ParamDescription pd in tpl.AllParameters)
            {
                if (!IsFirst) ParametersList += ",";
                ParametersList += pd.ParamID.ToString();
                IsFirst = false;
            }
            return ParametersList;
        }

        public static string GetParametersName(ParametersList tpl, int ID)
        {
            return (from i in tpl.AllParameters where i.ParamID == ID select i.Param).First();
        }
    }
    
    [DataContract]
    public class ParametersList
    {
        [DataMember]
        public bool HasChemistry = false;
        [DataMember]
        public bool HasOther = false;
        [DataMember]
        public List<ParamDescription> OtherParameters;
        [DataMember]
        public List<ParamDescription> ChemistryParameters;
        [DataMember]
        public List<ParamDescription> AllParameters;
        [DataMember]
        public List<string> DuplicatedParameters;
        [DataMember]
        public List<GroupDescription> AllGroups;
    }

    [DataContract]
    public class ParamDescription
    {
        [DataMember]
        public string Param { get; set; }
        [DataMember]
        public string ParamOld { get; set; }
        [DataMember]
        public string ParamType { get; set; }
        [DataMember]
        public int ParamID { get; set; }
        [DataMember]
        public int ParamTypeID { get; set; }
        [DataMember]
        public bool IsNumber { get; set; }
        [DataMember]
        public List<ParameterVariantDescription> ParameterVariants;
        [DataMember]
        public int Level { get; set; }
        [DataMember]
        public int GroupOrder { get; set; }
        [DataMember]
        public int ParameterOrder { get; set; }
        
        public ParamDescription()
        {
            Param = String.Empty;
            ParamType = String.Empty;
            IsNumber = false;
            ParamID = 0;
            ParamTypeID = 0;
            Level = 0;
            ParameterVariants = new List<ParameterVariantDescription>();
        }

        public ParamDescription(string P, string Pold, string PT, bool Number, int ParameterID, int ParameterTypeID)
        {
            Param = P;
            ParamOld = Pold;
            ParamType = PT;
            IsNumber = Number;
            ParamID = ParameterID;
            ParamTypeID = ParameterTypeID;
            ParameterVariants = new List<ParameterVariantDescription>();
        }
        
        public ParamDescription(string P, string Pold, string PT, bool Number, int ParameterID, int ParameterTypeID, List<ParameterVariantDescription> PVD)
        {
            Param = P;
            ParamOld = Pold;
            ParamType = PT;
            IsNumber = Number;
            ParamID = ParameterID;
            ParamTypeID = ParameterTypeID;
            ParameterVariants = PVD;
        }

        public ParamDescription(string P, string Pold, string PT, bool Number, int ParameterID, int ParameterTypeID, int level, int GroupPosition, int ParameterPosition)
        {
            Param = P;
            ParamOld = Pold;
            ParamType = PT;
            IsNumber = Number;
            ParamID = ParameterID;
            ParamTypeID = ParameterTypeID;
            Level = level;
            GroupOrder = GroupPosition;
            ParameterOrder = ParameterPosition;
            ParameterVariants = new List<ParameterVariantDescription>();
        }

        public ParamDescription(string P, string Pold, string PT, bool Number, int ParameterID, int ParameterTypeID, List<ParameterVariantDescription> PVD, int level)
        {
            Param = P;
            ParamOld = Pold;
            ParamType = PT;
            IsNumber = Number;
            ParamID = ParameterID;
            ParamTypeID = ParameterTypeID;
            ParameterVariants = PVD;
            Level = level;
        }
    }

    [DataContract]
    public class ParameterVariantDescription
    {
        [DataMember]
        public string VariatedField1 { get; set; }
        [DataMember]
        public string VariatedField2 { get; set; }
        [DataMember]
        public string VariatedValue1 { get; set; }
        [DataMember]
        public string VariatedValue2 { get; set; }
        [DataMember]
        public string VariantName { get; set; }
    }

    [DataContract]
    public class GroupDescription
    {
        [DataMember]
        public int GroupID { get; set; }
        [DataMember]
        public string GroupName { get; set; }

        public GroupDescription(int ID, string Name)
        {
            GroupID = ID;
            GroupName = Name;
        }
    }

    [DataContract]
    public class DefectDescription
    {
        [DataMember]
        public int DefectGroupID { get; set; }
        [DataMember]
        public string DefectGroupName { get; set; }
        [DataMember]
        public int DefectID { get; set; }
        [DataMember]
        public string DefectName { get; set; }
        public DefectDescription(int groupID, string groupName, int defectID, string defectName)
        {
            DefectGroupID = groupID;
            DefectGroupName = groupName;
            DefectID = defectID;
            DefectName = defectName;
        }
    }

    [DataContract]
    public class FieldList
    {
        [DataMember]
        public int MainParameterID 
        { 
            get { return MPID; } 
            set 
            { 
                MPID = value; 
            } 
        }
        private int MPID;
        [DataMember]
        public List<int> Combined { get; set; }
        [DataMember]
        public bool Enabled { get; set; }
        [DataMember]
        public int Hash { get; set; }
        [DataMember]
        public List<int> FullCombined 
        { 
            get 
            {
                List<int> temp = new List<int>();
                temp.Add(MainParameterID);
                return Combined.Union(temp).ToList();
            } 
        }

        public FieldList()
        {
            Combined = new List<int>();
            MainParameterID = 0;
            Enabled = false;
            Hash = GetHashCode();
        }
    }

    [DataContract]
    public class FieldListCombination
    {
        [DataMember]
        public List<FieldList> FieldListCombined = new List<FieldList>();
        [DataMember]
        public int TotalCount;
    }

    [DataContract]
    public class ProbeDescription
    {
        [DataMember]
        public string Name = string.Empty;
        [DataMember]
        public int ID = -1;

        public ProbeDescription(string name, int id)
        {
            Name = name;
            ID = id;
        }
    }

    [DataContract]
    public class ParametersListTaskResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public ParametersList ParametersList;

        public ParametersListTaskResult(string message, bool success, ParametersList pl)
        {
            Message = message;
            Success = success;
            ParametersList = pl;
        }
    }

    [DataContract]
    public class ProbeListTaskResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public List<ProbeDescription> ProbeList;

        public ProbeListTaskResult(string message, bool success, List<ProbeDescription> pl)
        {
            Message = message;
            Success = success;
            ProbeList = pl;
        }
    }

    [DataContract]
    public class DefectsListTaskResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public List<DefectDescription> DefectList;

        public DefectsListTaskResult(string message, bool success, List<DefectDescription> pl)
        {
            Message = message;
            Success = success;
            DefectList = pl;
        }
    }

    [DataContract]
    public class FieldListCombineTaskResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public FieldListCombination FieldListCombination;

        public FieldListCombineTaskResult(string message, bool success, FieldListCombination flc)
        {
            Message = message;
            Success = success;
            FieldListCombination = flc;
        }
    }
}
