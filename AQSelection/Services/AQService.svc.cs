﻿using System;
using System.Configuration;
using System.Globalization;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Security;
using System.Security.Principal;
using System.Security.Permissions;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Windows;
using System.Web;
using System.DirectoryServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Mime;

namespace AQSelection
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AQService
    {
        [OperationContract]
        public ActualityList GetActualityList()
        {
            ActualityList al = new ActualityList();
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ConnectionString.ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            try { sconn.Open(); }
            catch { return null; }

            string SQL = @"
SELECT ' ' as Name, null,0 as ord
UNION
SELECT 'Испытания' as Name, (select max(dt_upd) from Tests_head) as LastDate, 1 as ord
UNION
SELECT 'Хим. состав' as Name, (select max(test_date) from OEL_head) as LastDate, 2 as ord
UNION
SELECT 'Технология' as Name, (select max(LastUpdate) from Technology_Smelts) as LastDate, 3 as ord
UNION
SELECT 'Списание' as Name, (select max(NoticeEmitDate) from Waste_Notices) as LastDate, 4 as ord
UNION
SELECT 'Контроль' as Name, (select max(ChangeDate) from Control_Parts) as LastDate, 5 as ord
UNION
SELECT 'МНЛЗ-2' as Name, (select max(stop_date) from mnlz2_reports) as LastDate, 6 as ord
UNION
SELECT 'МНЛЗ-3' as Name, (select max(stop_date) from mnlz3_reports) as LastDate, 7 as ord
UNION
SELECT 'МНЛЗ-4' as Name, (select max(stop_date) from mnlz4_reports) as LastDate, 8 as ord
UNION
SELECT 'МНЛЗ-5' as Name, (select max(stop_date) from mnlz5_reports) as LastDate, 9 as ord
UNION
SELECT 'Стан 240' as Name, (select max([finish]) from [stan240]) as LastDate, 10 as ord
UNION
SELECT 'Стан 250-1' as Name, (select max(endtime) from [stan251]) as LastDate, 11 as ord
UNION
SELECT 'Стан 300-2' as Name, (select max(OuTime) from stan3002_CL215) as LastDate, 12 as ord
UNION
SELECT 'Стан 300-2.' as Name, (select max(DateTimePC) from stan3002_newline) as LastDate, 13 as ord
UNION
SELECT 'Операции' as Name, (select max(EndDate) from Operations) as LastDate, 14 as ord
UNION
SELECT 'УРБС' as Name, (select max(Rolling_Date) from urbs_semiproducts) as LastDate, 15 as ord
UNION
SELECT 'F3500' as Name, (select max(StartDate) from F2500_Catalogue) as LastDate, 16 as ord
UNION
SELECT 'IMS 2300' as Name, (select max(DateKey) from F2300_Catalogue) as LastDate, 17 as ord
union
SELECT 'Ковши' as Name, (select max([Время выпуска]) from
(
select max([Время выпуска]) as [Время выпуска] from ScoopsReportKKC
union
select max([Время выпуска]) as [Время выпуска] from ScoopsReportESPC
) as t), 18 as ord 
UNION
SELECT 'Отчет МНЛЗ' as Name, (SELECT max(end_time) FROM [raport].[dbo].[CCMProductivity]), 19 as ord
order by ord";
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            while (sdr.Read())
            {
                if (sdr.GetValue(1) != DBNull.Value) al.Actualities.Add(new ActualityDescription(sdr.GetValue(0).ToString(), ((DateTime?)sdr.GetValue(1)).Value.ToString("dd.MM HH:mm")));
                else al.Actualities.Add(new ActualityDescription(sdr.GetValue(0).ToString(), null));
            }
            sdr.Close();
            sconn.Close();
            return al;
        }

        [OperationContract]
        public List<ParameterGroupDescription> GetParameterGroups()
        {
            List<ParameterGroupDescription> pgd = new List<ParameterGroupDescription>();
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL = @"SELECT id, paramtype, position, cast (case when isnull(cp.NumberOfNew, 0)>0 then 'true' else 'false' end as bit) as HasNew  
            from Common_ParameterTypes  as cpt
            left outer join 
            (
            select p.id_paramtype,  COUNT(*) as NumberOfNew 
            from Common_Parameters as p 
            where p.DisplayParameterName like '%Новый параметр%' 
            group by id_paramtype) as cp on cpt.id = cp.id_paramtype
            order by position, paramtype";
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            while (sdr.Read())
            {
                pgd.Add(new ParameterGroupDescription(sdr.GetInt32(0), sdr.GetValue(1).ToString(), sdr.GetInt32(2), sdr.GetBoolean(3)));
            }
            sdr.Close();
            sconn.Close();
            return pgd;
        }

        [OperationContract]
        public bool UpdateParameterGroups(List<ParameterGroupUpdate> GroupsUpdateList)
        {
            string SQL = string.Empty;

            foreach (ParameterGroupUpdate gu in GroupsUpdateList)
            {
                SQL += "UPDATE Common_ParameterTypes SET Position='" + gu.Position + "' WHERE Id='" + gu.ID.ToString() + "'\r\n";
            }

            try
            {
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
                SqlConnection sconn = new SqlConnection(connectionString);
                sconn.Open();
                SqlCommand sc = new SqlCommand(SQL, sconn);
                sc.CommandTimeout = 3000;
                sc.CommandType = System.Data.CommandType.Text;
                sc.ExecuteNonQuery();
                sconn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        [OperationContract]
        public List<ParameterDescription> GetParameters(int ParamType)
        {
            List<ParameterDescription> pd = new List<ParameterDescription>();
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL = @"SELECT id, parametername, displayparametername, isnull(position, 10000), isnumber, id_paramtype from Common_Parameters where id_paramtype='" + ParamType.ToString() + "' order by position, displayparametername, parametername";
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();

            while (sdr.Read())
            {
                pd.Add(new ParameterDescription(sdr.GetInt32(0), sdr.GetValue(1).ToString(), sdr.GetValue(2).ToString(), sdr.GetInt32(3), sdr.GetBoolean(4), sdr.GetInt32(5)));
            }
            sdr.Close();
            sconn.Close();
            return pd;
        }

        [OperationContract]
        public bool UpdateParameters(List<ParameterUpdate> ParametersUpdateList)
        {
            string SQL = string.Empty;

            foreach (ParameterUpdate pu in ParametersUpdateList)
            {
                SQL += "UPDATE Common_Parameters SET Position='" + pu.Position + "', DisplayParameterName='" + pu.DisplayName.Replace("'", "''") + "', IsNumber='" + ((pu.IsNumber) ? "true" : "false") + "' WHERE Id='" + pu.ID.ToString() + "'\r\n";
            }

            try
            {
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
                SqlConnection sconn = new SqlConnection(connectionString);
                sconn.Open();
                SqlCommand sc = new SqlCommand(SQL, sconn);
                sc.CommandTimeout = 3000;
                sc.CommandType = System.Data.CommandType.Text;
                sc.ExecuteNonQuery();
                sconn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        [OperationContract]
        public void WriteLog(string Data)
        {
            string UserName = HttpContext.Current.User?.Identity.Name;
            string data = Data;
            try
            {
                data = data.Replace("@IP", HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
            }
            catch { }
            string SQL = "Insert into AQ_UserLog (Date, UserName, Data)  values (getdate(), '" + UserName + "', '" + data + "')";
            try
            {
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
                SqlConnection sconn = new SqlConnection(connectionString);
                sconn.Open();
                SqlCommand sc = new SqlCommand(SQL, sconn);
                sc.CommandTimeout = 3000;
                sc.CommandType = System.Data.CommandType.Text;
                sc.ExecuteNonQuery();
                sconn.Close();
            }
            catch
            {
            }
        }

        [OperationContract]
        public string GetStringFromURL(string URI, string userName, string password)
        {
            var handler = new HttpClientHandler();
            using (var client = new HttpClient(handler))
            {
                if (!string.IsNullOrEmpty(userName))
                {
                    var byteArray = Encoding.ASCII.GetBytes(userName + ":" + password);
                    client.DefaultRequestHeaders.Authorization =
                        new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                }
                else handler.UseDefaultCredentials = true;

                try
                {
                    var response = client.GetAsync(URI).Result;
                    if (!response.IsSuccessStatusCode) return null;
                    var responseContent = response.Content;
                    return responseContent.ReadAsStringAsync().Result;
                }
                catch (Exception e)
                {
                    return e.Message;
                }
            }
        }

        [OperationContract]
        public List<ThicknessFileInfo> GetThicknessData(string Filter, string FileType)
        {
            string MesPath = ConfigurationManager.AppSettings["F2500Path"];
            List<ThicknessFileInfo> result = new List<ThicknessFileInfo>();

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            string SQL = @"set dateformat ymd SELECT distinct StartDate, EndDate, Melt, Mark, Thickness, Width, ONRS,  MinThicknessDev, MaxThicknessDev
            from F2500_Catalogue
            where (" + Filter + @") order by startdate";
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            SqlDataReader sdr = sc.ExecuteReader();
            string[] dirs = Directory.GetDirectories(MesPath + @"\");
            string OldMeltNumber = null;
            int RollNumber = 1;
            while (sdr.Read())
            {
                ThicknessFileInfo tfi = new ThicknessFileInfo();
                DateTime d = sdr.GetDateTime(0);

                tfi.FileName = FileType + d.ToString("yyyyMMddHHmmss") + ".MTB";
                tfi.MeltNumber = sdr.GetString(2);
                if (tfi.MeltNumber != OldMeltNumber)
                {
                    OldMeltNumber = tfi.MeltNumber;
                    RollNumber = 1;
                }
                tfi.RollNumber = RollNumber;
                tfi.SteelName = sdr.GetString(3);
                tfi.NominalThickness = double.Parse(sdr.GetDecimal(4).ToString());
                tfi.NominalWidth = double.Parse(sdr.GetDecimal(5).ToString());
                tfi.ONRS = (sdr.GetValue(6) != DBNull.Value) ? sdr.GetString(6) : string.Empty;
                tfi.MinThicknessDev = double.Parse(sdr.GetDecimal(7).ToString());
                tfi.MaxThicknessDev = double.Parse(sdr.GetDecimal(8).ToString());
                foreach (string dir in dirs)
                {
                    string path = Path.Combine(dir, tfi.FileName);
                    if (File.Exists(path))
                    {
                        tfi.FileContent = File.ReadAllText(path);
                        result.Add(tfi);
                        break;
                    }
                }
                RollNumber++;
            }
            sdr.Close();
            sconn.Close();
            return result;
        }

        [OperationContract]
        public string GetTR(string NameStarts)
        {
            MemoryStream MS = new MemoryStream();
            string StoredMemoryStreamGuid = Sessions.NewItem(MS).ToString();
            string TRPath = ConfigurationManager.AppSettings["TRPath"];
            var found = Directory.EnumerateFiles(TRPath, NameStarts + @"*.pdf", SearchOption.AllDirectories).FirstOrDefault();
            if (found != null)
            {
                FileStream fs = new FileStream(found, FileMode.Open, FileAccess.Read);
                fs.CopyTo(MS);
                fs.Close();
                return StoredMemoryStreamGuid;
            }
            return null;
        }

        [OperationContract]
        public string SendFile(Guid OperationGuid, string filename, string contentType, string subject, string recepients)
        {
            var ms = Sessions.GetItem(OperationGuid) as MemoryStream;
            MailMessage message = new MailMessage();

            message.From = new MailAddress(ConfigurationManager.AppSettings["SMTPSender"], ConfigurationManager.AppSettings["SMTPSenderAlias"]);
            message.Body = "Это письмо отправлено автоматически из АС \"Анализ качества\"";
            message.Subject = subject;
            foreach (var rcpt in recepients.Split(','))
            {
                message.To.Add(rcpt);
            }
            Attachment data = new Attachment(ms, filename, contentType);
            ms.Position = 0;
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = DateTime.Now;
            disposition.ModificationDate = DateTime.Now;
            disposition.ReadDate = DateTime.Now;
            message.Attachments.Add(data);

            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"], 25);
            client.Credentials = CredentialCache.DefaultNetworkCredentials;

            try
            {
                client.Send(message);
                WriteLog($"Sent {filename} with size {ms.Length} to {recepients}, subject: {subject}");
            }
            catch (Exception ex)
            {
                WriteLog($"Error during mail sent: {ex.InnerException}");
            }
            return null;
        }
        [OperationContract]
        public string SendFiles(List<FileInfo> fileInfo, string subject, string recepients)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(ConfigurationManager.AppSettings["SMTPSender"], ConfigurationManager.AppSettings["SMTPSenderAlias"]);
            message.Body = "Это письмо отправлено автоматически из АС \"Анализ качества\"";
            message.Subject = subject;
            foreach (var rcpt in recepients.Split(','))
            {
                message.To.Add(rcpt);
            }

            foreach (FileInfo fi in fileInfo)
            {
                var ms = Sessions.GetItem(fi.OperationGuid) as MemoryStream;

                Attachment data = new Attachment(ms, fi.FileName, fi.ContentType);
                ms.Position = 0;
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = DateTime.Now;
                disposition.ModificationDate = DateTime.Now;
                disposition.ReadDate = DateTime.Now;
                message.Attachments.Add(data);
            }

            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"], 25);
            client.Credentials = CredentialCache.DefaultNetworkCredentials;

            try
            {
                client.Send(message);
                WriteLog($"Sent files to {recepients}, subject: {subject}");
            }
            catch (Exception ex)
            {
                WriteLog($"Error during mail sent: {ex.InnerException}");
            }
            return null;
        }
    }

    [DataContract]
    public class ActualityList
    {
        [DataMember]
        public List<ActualityDescription> Actualities;

        public ActualityList()
        {
            Actualities = new List<ActualityDescription>();
            List<object[]> RowCollection = new List<object[]>();
        }
    }

    [DataContract]
    public class ActualityDescription
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Date { get; set; }

        public ActualityDescription(string name, string date)
        {
            Name = name;
            Date = date;
        }
    }

    [DataContract]
    public class ParameterGroupDescription
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public bool CustomFlag { get; set; }
        [DataMember]
        public bool HasNewParameters { get; set; }

        public ParameterGroupDescription(int id, string name, int position, bool hasnewparameters)
        {
            ID = id;
            Name = name;
            Position = position;
            CustomFlag = false;
            HasNewParameters = hasnewparameters;
        }
    }

    [DataContract]
    public class ParameterGroupUpdate
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Position { get; set; }

        public ParameterGroupUpdate(int id, int position)
        {
            ID = id;
            Position = position;
        }
    }

    [DataContract]
    public class ParameterUpdate
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public bool IsNumber { get; set; }

        public ParameterUpdate(int id, string displayname, int position, bool isnumber)
        {
            ID = id;
            DisplayName = displayname;
            Position = position;
            IsNumber = isnumber;
        }
    }

    [DataContract]
    public class ParameterDescription
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int TypeID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public bool IsNumber { get; set; }
        [DataMember]
        public bool CustomFlag { get; set; }

        public ParameterDescription(int id, string name, string displayname, int position, bool isnumber, int typeid)
        {
            ID = id;
            TypeID = typeid;
            Name = name;
            DisplayName = displayname;
            Position = position;
            IsNumber = isnumber;
            CustomFlag = false;
        }
    }

    [DataContract]
    public class RoleInfo
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public List<string> Roles { get; set; }
    }

    [DataContract]
    public class FileInfo
    {
        [DataMember]
        public Guid OperationGuid { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public string ContentType { get; set; }
    }

    [DataContract]
    public class ThicknessFileInfo
    {
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string MeltNumber { get; set; }
        [DataMember]
        public int RollNumber { get; set; }
        [DataMember]
        public string SteelName { get; set; }
        [DataMember]
        public double NominalThickness { get; set; }
        [DataMember]
        public double MinThicknessDev { get; set; }
        [DataMember]
        public double MaxThicknessDev { get; set; }
        [DataMember]
        public double NominalWidth { get; set; }
        [DataMember]
        public string FileContent { get; set; }
        [DataMember]
        public string ONRS { get; set; }
    }
}