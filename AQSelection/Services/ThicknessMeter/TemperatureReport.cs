﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string TemperatureSqlTemplate = @"use raport set dateformat ymd

            select 
            Filter.Melt as 'Плавка', RollNumber as 'Рулон',
            cast(f2500.Offset as float) as Отступ, cast(AVG(f2500.T07) as float) as 't по центру' from f2500.dbo.Temperatures as f2500
            inner join
            (select Melt, Thickness, Width, MinThicknessDev, MaxThicknessDev, RollNumber, DateKey from F2500_Catalogue 
             left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
            where  {0}) as Filter
            on Filter.datekey = f2500.DateKey
            group by Filter.Melt, RollNumber, F2500.DateKey, Offset
            order by F2500.DateKey, RollNumber";

        private static DataSet CreateTemperatureReport(string filter, Dictionary<string, double> parameters)
        {
            var sql = String.Format(TemperatureSqlTemplate, filter);
            var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(sql);
            dataStorage.Tables[0].TableName = "Температура";
            return dataStorage;
        }
    }
}