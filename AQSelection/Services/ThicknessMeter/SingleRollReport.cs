﻿using System;
using System.Collections.Generic;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string SingleRollSqlTemplate = @"
            set dateformat ymd    
            use raport
            select TOP 1
                  DateKey as 'Дата'
                  ,Melt as 'Плавка'
                  ,RollNumber as '№ полосы'
                  ,Common_Mark.Mark +'  '+ Common_NTD.NTD + '  ' + cast(cast(Thickness as float) as varchar) +'х' + cast(cast(Width as float) as varchar) as 'Материал'
                  ,cast(cast(MinThicknessDev as float) as nvarchar)+'/'+cast(cast(MaxThicknessDev as float) as nvarchar)  as 'Допуск оператора'
                  ,cast(Thickness as float) as '#Толщина'
                  ,cast(Width as float) as '#Ширина'
                  ,RollData.RollLength as '#Длина'
                  ,cast(MinThicknessDev as float) as '#Минимальный допуск оператора'
                  ,cast(MaxThicknessDev as float) as '#Максимальный допуск оператора'
                  
              FROM F2500_Catalogue 
              left outer join Common_Mark on Common_Mark.id = F2500_Catalogue.MarkID
              left outer join Common_Ntd on Common_NTD.id = F2500_Catalogue.NtdID
              inner join (select DateKey as DK, MAX(offset) as RollLength from F2500.dbo.SurfaceThickness group by DateKey) as RollData
              on RollData.DK = F2500_Catalogue.DateKey
              where {0}

            use f2500
            select {1} 
                from SurfaceThickness
                where  {0}
                order by Offset

            use f2500
            select {2} 
                from Temperatures
                where  {0}
                group by offset
                order by Offset

            use f2500
            select offset as Отступ, width as Ширина
                from Width
                where  {0}
                order by Offset";

        private static DataSet CreateSingleRollReport(string filter, Dictionary<string, double> parameters)
        {
            var sql = String.Format(SingleRollSqlTemplate, filter, GetThicknessFieldList(), GetTemperaturesFieldList());
            var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(sql);
            string prefix = string.Empty;
            if (dataStorage.Tables[0].Rows.Count == 1)
            {
                prefix = dataStorage.Tables[0].Rows[0][1] + "-" + dataStorage.Tables[0].Rows[0][2];
            }
            dataStorage.Tables[0].TableName = prefix;
            dataStorage.Tables[1].TableName = "Карта толщин";
            dataStorage.Tables[2].TableName = "Карта t°";
            dataStorage.Tables[3].TableName = "Ширина";
            return dataStorage;
        }

        private static string GetThicknessFieldList()
        {
            var fieldList = "Cast (Offset as float) as Отступ";
            for (var i = 1; i <= 86; i++)
            {
                fieldList += String.Format(", Cast (Cell{0} as float) as Cell{0}", i.ToString("D2"));
            }
            return fieldList;
        }

        private static string GetTemperaturesFieldList()
        {
            var fieldList = "Cast (Offset as float) as Отступ";
            for (var i = 1; i <= 13; i++)
            {
                fieldList += String.Format(", Cast (AVG(T{0}) as float) as T{0}", i.ToString("D2"));
            }
            return fieldList;
        }
    }
}