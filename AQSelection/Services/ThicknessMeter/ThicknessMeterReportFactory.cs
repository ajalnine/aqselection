﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private delegate DataSet CreateReportDelegate(string filter, Dictionary<string, double> parameters);

        private readonly static Dictionary<ThicknessReportTypes, CreateReportDelegate> ReportBuilder = new Dictionary<ThicknessReportTypes, CreateReportDelegate>
            {
                {ThicknessReportTypes.Accuracy, CreateAccuracyReport},
                {ThicknessReportTypes.Width, CreateWidthReport},
                {ThicknessReportTypes.Temperature, CreateTemperatureReport},
                {ThicknessReportTypes.RollList, CreateRollListReport},
                {ThicknessReportTypes.SingleRoll, CreateSingleRollReport},
                {ThicknessReportTypes.Accuracy2300, CreateAccuracy2300Report},

            };

        public static DataSet CreateReport(ThicknessReportTypes reportType, string filter, Dictionary<string, double> parameters)
        {
            return ReportBuilder[reportType].Invoke(filter, parameters);
        }
    }
}