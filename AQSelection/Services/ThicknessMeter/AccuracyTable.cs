﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    class AccuracyTable : DataTable
    {
        private const double CellSize = 0.0198;

        private readonly int _stripCount;
        private readonly int _firstCellIndex, _lastCellIndex, _offsetIndex, _rollNumberIndex;
        private readonly int _headSize, _thicknessIndex;
        private readonly Dictionary<string, string> _schema;
        private readonly List<double> _uniqueThicknessList; 

        public AccuracyTable(string name, int stripCount, Dictionary<string, string> sourceSchema)
        {
            _schema = sourceSchema;
            _stripCount = stripCount;
            _offsetIndex = _schema.Keys.ToList().IndexOf("Offset");
            _rollNumberIndex = _schema.Keys.ToList().IndexOf("Номер рулона");
            _firstCellIndex = _schema.Keys.ToList().IndexOf("Cell01");
            _lastCellIndex = _schema.Keys.ToList().IndexOf("Cell86");
            _thicknessIndex = _schema.Keys.ToList().IndexOf("Толщина");
            _headSize = _schema.Keys.ToList().IndexOf("RollLength") - 1;
            _uniqueThicknessList = new List<double>();
            
            TableName = name;
            CreateReportSchema();
        }

        #region Построение схемы таблицы

        private void CreateReportSchema()
        {
            CloneHeaderSchema();
            Columns.Add(new DataColumn("Номер рулона", typeof(Double)));
            Columns.Add(new DataColumn("Отступ", typeof(Double)));
            Columns.Add(new DataColumn("Отклонение оси", typeof(Double)));
            CreateSchemaForStatistics();
        }

        private void CreateSchemaForStatistics()
        {
            for (var i = 1; i <= _stripCount; i++)
            {
                var postfix = _stripCount > 1 ? " " + i : String.Empty;
                Columns.Add(new DataColumn("Средняя толщина" + postfix, typeof(Double)));
                Columns.Add(new DataColumn("Размах толщины" + postfix, typeof(Double)));
                Columns.Add(new DataColumn("Толщина в центре полосы" + postfix, typeof(Double)));
                Columns.Add(new DataColumn("Разнотолщинность в центре полосы" + postfix, typeof(Double)));
            }
        }

        private void CloneHeaderSchema()
        {
            for (var i = 0; i < _headSize; i++)
            {
                var columnName = _schema.Keys.ToList()[i];
                var typeName = _schema[columnName];
                Columns.Add(new DataColumn(columnName, Type.GetType(typeName)));
            }
        } 
        #endregion

        #region Формирование строки отчета

        public void AppendAccuracyRow(SqlDataReader sdr)
        {
            var measures = GetValidMeasures(sdr);
            if (measures == null || measures.Count <= 0) return;

            var thickness = sdr.GetDouble(_thicknessIndex);
            if (!_uniqueThicknessList.Contains(thickness)) _uniqueThicknessList.Add(thickness);
            var sectionstatistics = GetSectionStatistics(measures, thickness);

            var dr = NewRow();
            CloneHeader(sdr, dr);
            dr["Отступ"] = (double)sdr.GetDecimal(_offsetIndex);
            var rollNumber = sdr.GetInt32(_rollNumberIndex);
            dr["Номер рулона"] = (double)rollNumber;
            dr["Отклонение оси"] = GetAxisDelta(measures);
            FillSectionStatistics(sectionstatistics, dr);
            Rows.Add(dr);
        }

        private void FillSectionStatistics(IEnumerable<StripStatistics> sectionstatistics, DataRow dr)
        {
            foreach (var s in sectionstatistics)
            {
                var namePostfix = _stripCount > 1 ? " " + (s.Strip + 1) : String.Empty;
                dr["Средняя толщина" + namePostfix] = s.Mean;
                dr["Размах толщины" + namePostfix] = s.Range;
                dr["Толщина в центре полосы" + namePostfix] = s.ThicknessAtCenter;
                dr["Разнотолщинность в центре полосы" + namePostfix] = s.ThicknessDifference;
            }
        }

        private void CloneHeader(SqlDataReader sdr, DataRow dr)
        {
            for (var i = 0; i < _headSize; i++)
            {
                var columnToCopy = _schema.Keys.ToList()[i];
                dr[columnToCopy] = sdr[columnToCopy];
            }
        } 
        #endregion
        
        #region Анализ сечения

        private Dictionary<int, Decimal> GetValidMeasures(IDataRecord sdr)
        {
            var measures = new Dictionary<int, Decimal>();

            var firstFound = false;
            for (var i = _firstCellIndex; i <= _lastCellIndex; i++)
            {
                var cellValue = sdr.GetDecimal(i);

                if (firstFound)
                {
                    if (cellValue == 0)
                    {
                        if (measures.Count < 10) return null; // Пропуск непредставительных сечений
                        measures.Remove(--i);
                        measures.Remove(--i);
                        measures.Remove(measures.Keys.First()); //Пропуск крайних 2х ячеек с обоих сторон
                        return measures;
                    }
                    measures.Add(i, cellValue);
                }
                else firstFound = cellValue > 0;
            }
            return null;
        }

        private decimal GetAxisDelta(Dictionary<int, decimal> measures)
        {
            return ((_lastCellIndex - _firstCellIndex) / 2 - (measures.Keys.Max() - measures.Keys.Min()) / 2) * (decimal)CellSize;
        }

        private IEnumerable<StripStatistics> GetSectionStatistics(Dictionary<int, decimal> measures, double thickness)
        {
            var sectionStatistics = new List<StripStatistics>();

            var stripSize = measures.Count / _stripCount;
            var keylist = measures.Keys.ToList();
            var outlierRange = thickness / 2;

            for (var strip = 0; strip < _stripCount; strip++)
            {
                var sum = 0.0D;
                var min = double.MaxValue;
                var max = double.MinValue;
                var count = 0.0D;

                for (var i = stripSize * strip; i < stripSize * (strip + 1); i++)
                {
                    var value = (double)measures[keylist[i]];
                    if (value < thickness - outlierRange || value > thickness + outlierRange) continue;

                    sum += value;
                    count++;
                    min = (min > value) ? value : min;
                    max = (max < value) ? value : max;
                }

                var stripCenterThickness = (double)measures[keylist[(int)(stripSize * (strip + 0.5))]];
                if (count > 0) sectionStatistics.Add(new StripStatistics
                                    {
                                        Mean = Math.Round(sum / count, 3),
                                        Range = Math.Round(max - min, 3),
                                        ThicknessAtCenter = stripCenterThickness,
                                        ThicknessDifference = Math.Abs(stripCenterThickness - thickness),
                                        Strip = strip
                                    });
            }
            return sectionStatistics;
        } 
        #endregion

        public IEnumerable<double> GetThicknesses()
        {
            return _uniqueThicknessList.OrderBy(a => a);
        }

        public class StripStatistics
        {
            public int Strip { get; set; }
            public double Mean { get; set; }
            public double Range { get; set; }
            public double ThicknessAtCenter { get; set; }
            public double ThicknessDifference { get; set; }
        }
    }
}