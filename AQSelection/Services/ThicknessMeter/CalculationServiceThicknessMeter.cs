﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.Collections.Generic;

namespace AQSelection
{
    public partial class CalculationService
    {
        [OperationContract]
        public StepResult FillDataSetWithThicknessReport(ThicknessReportTypes reportType, string filter, Dictionary<string, double> parameters)
        {
            var dataStorage = ThicknessMeterReportFactory.CreateReport(reportType, filter, parameters);
            if (dataStorage == null || dataStorage.Tables.Count == 0) return new StepResult("Нет данных", false, Sessions.NewItem(dataStorage).ToString());
            return new StepResult(LogTableSize(dataStorage), true, Sessions.NewItem(dataStorage).ToString());
        }
    }

    [DataContract(Namespace = "")]
    public enum ThicknessReportTypes
    {
        [EnumMember]
        Accuracy,
        [EnumMember]
        Width,
        [EnumMember]
        Temperature,
        [EnumMember]
        RollList,
        [EnumMember]
        SingleRoll,
        [EnumMember]
        Unknown,
        [EnumMember]
        Accuracy2300
    }
}