﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string WidthSqlTemplate = @"use raport set dateformat ymd

        declare @cl float
        declare @top float
        declare @end float

        set @cl={1}
        set @top={2}
        set @end={3}

        select 
        Filter.Melt as 'Плавка',
        cast(RollNumber as float) as 'Рулон', 
        cast(Filter.Width as float) as 'Ширина_НГ',
        cast(f2500.Offset as float) as 'Отступ', 
        cast(f2500.Width as float) as 'Ширина'
        from 
        f2500.dbo.Width as f2500
        inner join
        (select Melt, Thickness, Width, MinThicknessDev, MaxThicknessDev, RollNumber, DateKey from F2500_Catalogue 
        left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
        where {0}) as Filter
        on Filter.datekey = f2500.DateKey
        inner join
        (select Max(Offset) as RollLength, DateKey from F2500.dbo.surfacethickness group by DateKey) as Filter2
        on Filter2.datekey = f2500.DateKey
        where f2500.Offset < @top and f2500.Width between Filter.Width *(1 - @cl /100) and Filter.Width *(1 + @cl /100)
        order by F2500.DateKey, RollNumber

        select 
        Filter.Melt as 'Плавка',
        cast(RollNumber as float) as 'Рулон', 
        cast(Filter.Width as float) as 'Ширина_НГ',
        cast(f2500.Offset as float) as 'Отступ', 
        cast(f2500.Width as float) as 'Ширина'
        from f2500.dbo.Width as f2500
        inner join
        (select Melt, Thickness, Width, MinThicknessDev, MaxThicknessDev, RollNumber, DateKey from F2500_Catalogue 
        left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
        where  {0}) as Filter
        on Filter.datekey = f2500.DateKey
        inner join
        (select Max(Offset) as RollLength, DateKey from F2500.dbo.surfacethickness group by DateKey) as Filter2
        on Filter2.datekey = f2500.DateKey
        where f2500.Offset between @top and Filter2.RollLength  - @end  and f2500.Width between Filter.Width *(1 - @cl /100) and Filter.Width *(1 + @cl /100)
        order by F2500.DateKey, RollNumber

        select 
        Filter.Melt as 'Плавка',
        cast(RollNumber as float) as 'Рулон', 
        cast(Filter.Width as float) as 'Ширина_НГ',
        cast(f2500.Offset as float) as 'Отступ', 
        cast(f2500.Width as float) as 'Ширина'
        from f2500.dbo.Width as f2500
        inner join
        (select Melt, Thickness, Width, MinThicknessDev, MaxThicknessDev, RollNumber, DateKey from F2500_Catalogue 
        left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
        where {0} ) as Filter
        on Filter.datekey = f2500.DateKey
        inner join
        (select Max(Offset) as RollLength, DateKey from F2500.dbo.surfacethickness group by DateKey) as Filter2
        on Filter2.datekey = f2500.DateKey
        where f2500.Offset >= Filter2.RollLength - @end  and f2500.Width between Filter.Width *(1 - @cl /100) and Filter.Width *(1 + @cl /100)
        order by F2500.DateKey, RollNumber";

        
        private static DataSet CreateWidthReport(string filter, Dictionary<string, double> parameters)
        {
            var outlierRange = parameters["Удалять выше или ниже номинала, %"];
            var beginPartLength = parameters["Длина начального участка, м"];
            var endPartLength = parameters["Длина концевого участка, м"];

            var sql = String.Format(WidthSqlTemplate, filter, outlierRange, beginPartLength, endPartLength);

            var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(sql); 
            dataStorage.Tables[0].TableName = "Начало";
            dataStorage.Tables[1].TableName = "Середина";
            dataStorage.Tables[2].TableName = "Конец";
            
            return dataStorage;
        }
    }
}