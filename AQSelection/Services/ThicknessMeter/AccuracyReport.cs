﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string AccuracySqlTemplate = @"use raport set dateformat ymd
            select 
            Filter.Melt as 'Плавка',
            cast(Filter.Thickness as float) as 'Толщина',
            cast(Filter.Width as float) as 'Ширина',
            Filter.RollNumber as 'Номер рулона',
            RollInfo.RollLength as 'RollLength',
            f2500.*
            from f2500.dbo.SurfaceThickness as f2500
            inner join
            (
                select Melt, Thickness, Width, MinThicknessDev, MaxThicknessDev, RollNumber, DateKey from F2500_Catalogue 
                left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
                where  {0}
            ) as Filter on Filter.DateKey = f2500.DateKey
            inner join
            (
                select Max(Offset) as RollLength, DateKey from F2500.dbo.surfacethickness group by DateKey
            ) as RollInfo on RollInfo.DateKey = f2500.DateKey 
            order by Filter.DateKey, Filter.Rollnumber, f2500.offset";

        private static DataSet CreateAccuracyReport(string filter, Dictionary<string, double> parameters)
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            using (var sconn = new SqlConnection(connectionString))
            {
                sconn.Open();
                var sc = new SqlCommand(String.Format(AccuracySqlTemplate, filter), sconn) { CommandTimeout = 1000 };
                var sdr = sc.ExecuteReader();

                var sourceTableSchema = DataSetHelper.SimplifySchema(sdr.GetSchemaTable());

                var stripCount = (int)parameters["Количество штрипсов, шт"];

                var startingPart = new AccuracyTable("Начало", stripCount, sourceTableSchema);
                var middlePart = new AccuracyTable("Середина", stripCount, sourceTableSchema);
                var endPart = new AccuracyTable("Конец", stripCount, sourceTableSchema);

                var offsetCell =  sourceTableSchema.Keys.ToList().IndexOf("Offset");
                var rollLengthCell = sourceTableSchema.Keys.ToList().IndexOf("RollLength");

                var beginPartLength = parameters["Длина начального участка, м"];
                var endPartLength = parameters["Длина концевого участка, м"];

                while (sdr.Read())
                {
                    var offset = (double)sdr.GetDecimal(offsetCell);
                    var rollLength = (double)sdr.GetDecimal(rollLengthCell);

                    ((offset <= beginPartLength)
                        ? startingPart
                        : (offset > rollLength - endPartLength)
                            ? endPart
                            : middlePart).AppendAccuracyRow(sdr);
                }

                var lcl = parameters["Нижнее отклонение, мм"];
                var ucl = parameters["Верхнее отклонение, мм"];

                var startingPartLimits = new AccuracyLimitsTable("#Начало_границы", lcl, ucl, stripCount, startingPart.GetThicknesses());
                var middlePartLimits = new AccuracyLimitsTable("#Середина_границы", lcl, ucl, stripCount, middlePart.GetThicknesses());
                var endPartLimits = new AccuracyLimitsTable("#Конец_границы", lcl, ucl, stripCount, endPart.GetThicknesses());
                
                var dataStorage = new DataSet();
                dataStorage.Tables.Add(startingPart);
                dataStorage.Tables.Add(middlePart);
                dataStorage.Tables.Add(endPart);
                dataStorage.Tables.Add(startingPartLimits);
                dataStorage.Tables.Add(middlePartLimits);
                dataStorage.Tables.Add(endPartLimits);
                return dataStorage;
            }
        }
    }
}