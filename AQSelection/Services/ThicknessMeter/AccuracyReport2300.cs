﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string Accuracy2300SqlTemplate = @"use raport set dateformat ymd
            select 
            Filter.Melt as 'Плавка',
            Filter.Mark as 'Марка',
            cast(Filter.Thickness as float) as 'Толщина',
            cast(Filter.ListNumber as float) as 'Номер листа',
            ListLength.ListLength as 'Длина листа',
            f2300.[DateKey] as 'Начало прокатки',
            f2300.[Offset] as 'Отступ',
            Filter.MinThicknessDev as '#Толщина справа_НГ',
            Filter.MaxThicknessDev as '#Толщина справа_ВГ',
            Filter.MinThicknessDev as '#Толщина в центре_НГ',
            Filter.MaxThicknessDev as '#Толщина в центре_ВГ',
            Filter.MinThicknessDev as '#Толщина слева_НГ',
            Filter.MaxThicknessDev as '#Толщина слева_ВГ',
            f2300.[ThicknessRight] as 'Толщина справа',
            f2300.[ThicknessCenter] as 'Толщина в центре',
            f2300.[ThicknessLeft] as 'Толщина слева',
            ABS(f2300.[ThicknessRight] - cast(Filter.Thickness as float)) as 'Разнотолщинность справа',
            ABS(f2300.[ThicknessCenter] - cast(Filter.Thickness as float)) as 'Разнотолщинность в центре',
            ABS(f2300.[ThicknessLeft] - cast(Filter.Thickness as float)) as 'Разнотолщинность слева',
            f2300.[TemperatureRight] as 'Температура справа',
            f2300.[TemperatureCenter] as 'Температура в центре',
            f2300.[TemperatureLeft] as 'Температура слева',
            f2300.[Width] as 'Ширина'
            from f2500.dbo.[2300] as f2300
            inner join
            (
                select Melt, Thickness, MinThicknessDev, MaxThicknessDev, ListNumber, DateKey, Common_Mark.Mark from F2300_Catalogue 
                left outer join Common_Mark on F2300_Catalogue.MarkID = Common_Mark.ID
                where  {0} and IsLast=1
            ) as Filter on Filter.DateKey = f2300.DateKey
            inner join
            (
                select Max(Offset) as ListLength, DateKey from F2500.dbo.[2300] group by DateKey
            ) as ListLength on ListLength.DateKey = f2300.DateKey 
            where f2300.Offset <= ListLength - @end and f2300.Offset >= @start";

        private static DataSet CreateAccuracy2300Report(string filter, Dictionary<string, double> parameters)
        {
            var sql = String.Format(Accuracy2300SqlTemplate, filter);
            var beginPartLength = parameters["Длина начального участка, м"];
            var endPartLength = parameters["Длина концевого участка, м"];
            var vars = $@"declare @start float
                          declare @end float
                          set @start={beginPartLength.ToString(CultureInfo.InvariantCulture)}   
                          set @end={endPartLength.ToString(CultureInfo.InvariantCulture)}   
";

            var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(vars + sql);
            dataStorage.Tables[0].TableName = "2300";
            return dataStorage;
        }
    }
}