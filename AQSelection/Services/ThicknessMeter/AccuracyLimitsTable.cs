﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    class AccuracyLimitsTable : DataTable
    {
        private readonly double _lcl ;
        private readonly double _ucl;
        private readonly int _stripCount;
        private readonly IEnumerable<double> _thicknesses;

        public AccuracyLimitsTable(string name, double lcl, double ucl, int stripCount, IEnumerable<double> thicknesses)
        {
            _lcl = lcl;
            _ucl = ucl;
            _thicknesses = thicknesses;
            _stripCount = stripCount;
            TableName = name;

            CreateLimitsSchema();
            CalculateLimits();
        }

        private void CreateLimitsSchema()
        {
            for (var i = 1; i <= _stripCount; i++)
            {
                var postfix = _stripCount > 1 ? " " + i : String.Empty;
                Columns.Add(new DataColumn("Средняя толщина_НГ" + postfix, typeof(Double)));
                Columns.Add(new DataColumn("Средняя толщина_ВГ" + postfix, typeof(Double)));
                Columns.Add(new DataColumn("Размах толщины_ВГ" + postfix, typeof(Double)));
            }
        }

        private void CalculateLimits()
        {
            foreach (var s in _thicknesses)
            {
                var dr = NewRow();
                for (var i = 1; i <= _stripCount; i++)
                {
                    var postfix = _stripCount > 1 ? " " + i : String.Empty;
                    dr["Средняя толщина_НГ" + postfix]= s + _lcl;
                    dr["Средняя толщина_ВГ" + postfix] = s + _ucl;
                    dr["Размах толщины_ВГ" + postfix] = _ucl - _lcl;
                }
                Rows.Add(dr);
            }
        }
    }
}