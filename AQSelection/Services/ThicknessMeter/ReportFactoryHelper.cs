﻿using System.Data.SqlClient;
using System.Data;

namespace AQSelection
{
    public static class ReportFactoryHelper
    {
        public static DataSet FillDataSetFromSqlQuery(string sql)
        {
            var connectionString =
                System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            var dataStorage = new DataSet();
            using (var sconn = new SqlConnection(connectionString))
            {
                sconn.Open();
                var sc = new SqlCommand(sql, sconn) { CommandTimeout = 1000 };
                var sda = new SqlDataAdapter(sc);
                sda.Fill(dataStorage);
            }
            return dataStorage;
        }
    }
}