﻿using System;
using System.Collections.Generic;
using System.Data;

namespace AQSelection
{
    public static partial class ThicknessMeterReportFactory
    {
        private const string RollListSqlTemplate = @"use raport set dateformat ymd

        select Melt+'-' +cast(RollNumber as nvarchar) as 'Полоса',
        Common_Mark.Mark +'  '+ Common_NTD.NTD + '  ' + cast(cast(Thickness as float) as varchar) +'х' + cast(cast(Width as float) as varchar) as 'Материал',  datekey as '#Дата'
        from F2500_Catalogue 
        left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.id
        left outer join Common_NTD on F2500_Catalogue.NtdID = Common_NTD.id
        where {0}
        order by DateKey, RollNumber";

        private static DataSet CreateRollListReport(string filter, Dictionary<string, double> parameters)
        {
            var sql = String.Format(RollListSqlTemplate, filter);
            var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(sql);
            dataStorage.Tables[0].TableName = "Полосы";
            return dataStorage;
        }
    }
}