﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        #region Операции с PPTX
        public void GetPPTX(Guid OperationGuid, Guid PDGuid)
        {
            var PD = (PresentationDescription) Sessions.GetItem(PDGuid);
            MemoryStream MS = CreateEmptyPPTXPackage(PD);
            Guid storedMemoryStreamGuid = Sessions.NewItem(Sessions.GetHttpContext(), MS);
            AQService aqs = new AQService();
            aqs.WriteLog(
                $"Presentation PPTX created with; Slides:{PD.Slides.Count}; Bytes:{MS.Length}");
            client.Receive(OperationGuid, new OperationResult("FileReady", true, OperationGuid.ToString(), storedMemoryStreamGuid));
        }

        public void CreatePPTX(Guid OperationGuid, PresentationDescription PD)
        {
            Guid pdGuid = Sessions.NewItem(Sessions.GetHttpContext(), PD);
            client.Receive(OperationGuid, new OperationResult("Created", true, OperationGuid.ToString(), pdGuid));
        }

        public void AppendPPTX(Guid OperationGuid, Guid pdGuid, PresentationDescription PD)
        {
            var PreviousPD = (PresentationDescription)Sessions.GetItem(pdGuid);
            PreviousPD.Slides.AddRange(PD.Slides);
            client.Receive(OperationGuid, new OperationResult("Appended", true, OperationGuid.ToString(), pdGuid));
        }
        #endregion
    }
}