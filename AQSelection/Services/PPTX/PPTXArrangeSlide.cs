﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using DocumentFormat.OpenXml.Drawing;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        private const Int64 VerticalMargin = 75000;
        private const Int64 HorizontalMargin = 75000;

        private static SlideArrangeInfo CreateArrangeInfoForElement(SlideElementDescription SED, double Xoffset, double Yoffset, double WindowWidth, double WindowHeight)
        {
            SlideArrangeInfo AI = new SlideArrangeInfo();
            double ElementWidth = 0, ElementHeight = 0, ElementOffsetX = 0, ElementOffsetY = 0, rel = 0;
            
            rel = SED.Width / SED.Height;
            if (rel > WindowWidth / WindowHeight)
            {
                ElementWidth = WindowWidth;
                ElementHeight = WindowWidth * (SED.Height / SED.Width);
            }
            else
            {
                ElementWidth = WindowHeight * (SED.Width / SED.Height);
                ElementHeight = WindowHeight;
            }

            ElementOffsetX = WindowWidth / 2 - ElementWidth / 2 + Xoffset;
            ElementOffsetY = WindowHeight / 2 - ElementHeight / 2 + Yoffset;

            AI.Offset = new Offset() { X = (Int64)ElementOffsetX + HorizontalMargin, Y = (Int64)ElementOffsetY + VerticalMargin };
            AI.Size = new Extents() { Cx = (Int64)ElementWidth - HorizontalMargin * 2, Cy = (Int64)ElementHeight - VerticalMargin * 2};
            return AI;
        }
        
        private static List<SlideArrangeInfo> CreateArrangeInfos(SlideDescription SD)
        {
            List<SlideArrangeInfo> ArrangeInfos = new List<SlideArrangeInfo>();
            bool IsSingleType = (from a in SD.SlideElements select a.SlideElementType).Distinct().Count() == 1;
            double widthPart1, widthPart2, widthPart3, widthPart4;
            switch (SD.SlideElements.Count)
            {
                case 1:
                    ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight));
                    break;

                case 2:

                    double rel1 = Math.Max(SD.SlideElements[0].Width, SD.SlideElements[1].Width) / (SD.SlideElements[0].Height + SD.SlideElements[1].Height);
                    double rel2 = (SD.SlideElements[0].Width + SD.SlideElements[1].Width) / Math.Max(SD.SlideElements[0].Height, SD.SlideElements[1].Height);

                    if (IsSingleType && rel1 > rel2)
                    {
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth / 2, SlideArrangeInfo.ClientHeight));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], SlideArrangeInfo.ClientWidth / 2, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth / 2, SlideArrangeInfo.ClientHeight));
                    }
                    else
                    {
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 2));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 2, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 2));
                    }

                    break;

                case 3:

                    var aspectRatioMin = SD.SlideElements.Select(ar => ar.Width / ar.Height).Min();
                    if (aspectRatioMin > 2)
                    {
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 3));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 3, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 3));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 3 * 2, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 3));
                    }
                    else
                    {
                        widthPart1 = SD.SlideElements[0].Width/
                                            (SD.SlideElements[0].Width + SD.SlideElements[1].Width);
                        widthPart2 = 1 - widthPart1;

                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth * widthPart1, SlideArrangeInfo.ClientHeight / 2));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], SlideArrangeInfo.ClientWidth * widthPart1, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth * widthPart2, SlideArrangeInfo.ClientHeight / 2));

                        if (Math.Abs(SD.SlideElements[2].Width - SD.SlideElements[0].Width) < double.Epsilon &&
                            Math.Abs(SD.SlideElements[2].Width - SD.SlideElements[1].Width) < double.Epsilon)
                        {
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], SlideArrangeInfo.ClientWidth/4,
                                SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight/2,
                                SlideArrangeInfo.ClientWidth/2, SlideArrangeInfo.ClientHeight/2));
                        }
                        else
                        {
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], 0,
                                SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight/2,
                                SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight/2));
                        }
                    }
                    break;

                case 4:
                    if (IsSingleType)
                    {
                        var aspectRatioMin2 = SD.SlideElements.Select(ar => ar.Width / ar.Height).Min();
                        if (aspectRatioMin2 > 3)
                        {
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 4, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 4 * 2, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[3], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 4 * 3, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                        }
                        else
                        {
                            widthPart1 = SD.SlideElements[0].Width / (SD.SlideElements[0].Width + SD.SlideElements[1].Width);
                            widthPart2 = 1 - widthPart1;
                            widthPart3 = SD.SlideElements[2].Width / (SD.SlideElements[2].Width + SD.SlideElements[3].Width);
                            widthPart4 = 1 - widthPart3;

                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth * widthPart1, SlideArrangeInfo.ClientHeight / 2));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], SlideArrangeInfo.ClientWidth * widthPart1, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth * widthPart2, SlideArrangeInfo.ClientHeight / 2));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 2, SlideArrangeInfo.ClientWidth * widthPart3, SlideArrangeInfo.ClientHeight / 2));
                            ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[3], SlideArrangeInfo.ClientWidth * widthPart3, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 2, SlideArrangeInfo.ClientWidth * widthPart4, SlideArrangeInfo.ClientHeight / 2));
                        }
                    }
                    else
                    {
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[0], 0, SlideArrangeInfo.HeaderHeight, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[1], 0, SlideArrangeInfo.HeaderHeight + SlideArrangeInfo.ClientHeight / 4, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[2], 0, SlideArrangeInfo.HeaderHeight + 2 * SlideArrangeInfo.ClientHeight / 4, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                        ArrangeInfos.Add(CreateArrangeInfoForElement(SD.SlideElements[3], 0, SlideArrangeInfo.HeaderHeight + 3 * SlideArrangeInfo.ClientHeight / 4, SlideArrangeInfo.ClientWidth, SlideArrangeInfo.ClientHeight / 4));
                    }
                break;

                default:
                    
                    var r = GetOptimalRowNumber(SD.SlideElements);
                    var A = SlideArrangeInfo.ClientHeight;
                    var B = SlideArrangeInfo.ClientWidth;
                    var a = A / r;
                    var b0 = 0.0d;
                    var a0 = 0.0d;
                    var preArrange = new List<PreArrangeInfo>();
                    var row = 0;
                    foreach (var e in SD.SlideElements)
                    {
                        var b = a * e.Width / e.Height;
                        if (b0 + b > B)
                        {
                            b0 = 0;
                            a0 += a;
                            row++;
                        }
                        preArrange.Add(new PreArrangeInfo { b0 = b0, a0 = a0, a = a, b = b, sed = e, row = row });
                        b0 += b;
                    }
                    if (preArrange.Select(p => p.b).Distinct().Count() == 1)
                    {
                        var maxB = preArrange.Max(p => p.b + p.b0);
                        var maxA = preArrange.Max(p => p.a + p.a0);
                        var aOffset = (SlideArrangeInfo.ClientHeight - maxA)/2 + SlideArrangeInfo.HeaderHeight;
                        var bOffset = (SlideArrangeInfo.ClientWidth - maxB)/2;
                        foreach (var p in preArrange)
                        {
                            ArrangeInfos.Add(CreateArrangeInfoForElement(p.sed, p.b0 + bOffset, p.a0 + aOffset, p.b, p.a));
                        }
                    }
                    else
                    {
                        var maxA = preArrange.Max(p => p.a + p.a0);
                        var aOffset = (SlideArrangeInfo.ClientHeight - maxA) / 2 + SlideArrangeInfo.HeaderHeight;
                        for (int i = 0; i <= row; i++)
                        {
                            var currentArrangement = preArrange.Where(p => p.row == i).ToList();
                            var maxB = currentArrangement.Max(p => p.b + p.b0);
                            var bOffset = (SlideArrangeInfo.ClientWidth - maxB) / 2;
                            foreach (var p in currentArrangement)
                            {
                                ArrangeInfos.Add(CreateArrangeInfoForElement(p.sed, p.b0 + bOffset, p.a0 + aOffset, p.b, p.a));
                            }
                        }
                    }
                    break;
            }
            return ArrangeInfos;
        }

        private class PreArrangeInfo
        {
            public double b0, a0, b, a;
            public SlideElementDescription sed;
            public int row;
        }

        private static int GetOptimalRowNumber(List<SlideElementDescription> se)
        {
            var A = SlideArrangeInfo.ClientHeight;
            var B = SlideArrangeInfo.ClientWidth;
            Dictionary<int, double> S = new Dictionary<int, double>();

            for (int r = 1; r<= se.Count; r++)
            {
                var a = A / r;
                var b0 = 0.0d;
                var a0 = 0.0d;
                var s = 0.0d;

                foreach(var e in se)
                {
                    var b = a * e.Width / e.Height;
                    b0 += b;
                    if (b0 > B)
                    {
                        s += Math.Abs(a * (B - (b0 - b)));
                        a0 += a;
                        if (a0 > A) s = double.PositiveInfinity;
                        b0 = b;
                    }
                }
                if (b0 < B) s += a * (B - b0);
                if ((a0 + a) <= A) s += (A - (a0 + a))*B;
                else s = double.PositiveInfinity;
                S[r] = s;
            }
            var mins = S.Values.Min();
            return S.Where(a => a.Value == mins).First().Key;
        }
    }

    public class SlideArrangeInfo
    {
        public const double SlideWidth = 12192000;
        public const double SlideHeight = 6858000;
        public const double HeaderHeight = 404664;
        public const double ClientWidth = SlideWidth;
        public const double ClientHeight = SlideHeight - HeaderHeight;
        
        public Point2DType Offset;
        public PositiveSize2DType Size;
    }
}