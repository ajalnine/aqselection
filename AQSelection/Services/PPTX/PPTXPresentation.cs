﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        public MemoryStream CreateEmptyPPTXPackage(PresentationDescription PD)
        {
            MemoryStream MS = new MemoryStream();

            using (PresentationDocument package = PresentationDocument.Create(MS, PresentationDocumentType.Presentation))
            {
                PresentationPart presentationPart = package.AddPresentationPart();
                presentationPart.Presentation = new Presentation();
                
                ExtendedFilePropertiesPart extendedFilePropertiesPart1 = package.AddNewPart<ExtendedFilePropertiesPart>("rId6");
                extendedFilePropertiesPart1.Properties = new DocumentFormat.OpenXml.ExtendedProperties.Properties();

                CreatePresentationParts(presentationPart, PD);
                package.Close();
            }

            return MS;
        }

        private static void CreatePresentationParts(PresentationPart presentationPart, PresentationDescription PD)
        {
            SlideMasterIdList slideMasterIdList1 = new SlideMasterIdList(new SlideMasterId() { Id = (UInt32Value)2147483648U, RelationshipId = "rId1" });
            SlideSize slideSize1 = new SlideSize() { Cx = (Int32)SlideArrangeInfo.SlideWidth, Cy = (Int32)SlideArrangeInfo.SlideHeight, Type = SlideSizeValues.Screen16x9 };
            NotesSize notesSize1 = new NotesSize() { Cx = 121992000, Cy = 6858000 };
            DefaultTextStyle defaultTextStyle1 = new DefaultTextStyle();

            uint slideid = 256;
            SlideIdList slideIdList1 = new SlideIdList();
            SlideLayoutPart slideLayoutPart1 = null; 
            foreach (var slide in PD.Slides)
            {
                string rid = "rid" + slideid.ToString();
                slideIdList1.AppendChild<SlideId>(new SlideId() { Id = (UInt32Value)slideid, RelationshipId = rid });
                SlidePart slidePart1;
                slidePart1 = CreateSlidePart(presentationPart, slide, rid, PD);
                if (slideid == 256)
                {
                    slideLayoutPart1 = slidePart1.AddNewPart<SlideLayoutPart>("rId1");
                    slideLayoutPart1.SlideLayout = CreateSlideLayoutPart();

                    SlideMasterPart slideMasterPart1 = slideLayoutPart1.AddNewPart<SlideMasterPart>("rId1");
                    slideMasterPart1.SlideMaster = CreateSlideMasterPart(PD.ForegroundColor, PD.BlackBackground);
                    slideMasterPart1.AddPart(slideLayoutPart1, "rId1");
                    
                    ThemePart themePart1 = slideMasterPart1.AddNewPart<ThemePart>("rId5");
                    themePart1.Theme = CreateTheme();
                    presentationPart.AddPart(slideMasterPart1, "rId1");
                    presentationPart.AddPart(themePart1, "rId3");
                }
                else
                {
                    slidePart1.AddPart<SlideLayoutPart>(slideLayoutPart1);
                }
                slideid++;
            }
            presentationPart.Presentation.Append(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);
        }
    }
}