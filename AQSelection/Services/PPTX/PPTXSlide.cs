﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;

namespace AQSelection
{
    public partial class ReportingService : IReportingDuplexService
    {
        private static SlidePart CreateSlidePart(PresentationPart presentationPart, SlideDescription SD, string rid, PresentationDescription PD)
        {
            SlidePart slidePart1 = presentationPart.AddNewPart<SlidePart>(rid);
            Slide slide1 = new Slide();
            ShapeTree shapeTree1 = CreateShapeTree(slidePart1, slide1);
            string SlideTitle = String.Empty;
            int tc = 0;
            foreach (var st in (from s in SD.SlideElements where s.ShortDescription !=null && s.ShortDescription!=String.Empty  select s.ShortDescription).Distinct())
            {
                SlideTitle += st;
                if (tc > 0) SlideTitle +="; " ;
                tc++;
            }

            AppendTitleToShapeTree(SlideTitle, shapeTree1, PD);

            List<SlideArrangeInfo> ArrangeInfos = CreateArrangeInfos(SD);

            for (int i = 0; i < SD.SlideElements.Count; i++)
            {
                string elementid = "i" + rid + "e" + i;
                SlideElementDescription se = SD.SlideElements[i];
                
                switch (se.SlideElementType)
                {
                    case SlideElementTypes.Image:
                        AppendPictureToShapeTree(elementid, shapeTree1, ArrangeInfos[i]);
                        AppendImageData(se.SlideData as List<byte>, elementid, slidePart1);
                        break;

                    default:
                        break;
                }
            }
            return slidePart1;
        }

        #region Добавление элемента - изображения
        
        private static void AppendPictureToShapeTree(string pictureid, ShapeTree shapeTree1, SlideArrangeInfo AI)
        {
            P.Picture picture1 = new P.Picture();
            P.BlipFill blipFill1 = new P.BlipFill();
            D.Blip blip1 = new D.Blip() { Embed = pictureid };
            D.SourceRectangle sourceRectangle1 = new D.SourceRectangle();
            D.Stretch stretch1 = new D.Stretch();
            D.FillRectangle fillRectangle1 = new D.FillRectangle();
            stretch1.Append(fillRectangle1);
            blipFill1.Append(blip1);
            blipFill1.Append(sourceRectangle1);
            blipFill1.Append(stretch1);

            P.ShapeProperties shapeProperties1 = new P.ShapeProperties() { };

            D.Transform2D transform2D1 = new D.Transform2D();
            transform2D1.Append(AI.Offset);
            transform2D1.Append(AI.Size);

            D.PresetGeometry presetGeometry1 = new D.PresetGeometry() { Preset = D.ShapeTypeValues.Rectangle };
            D.AdjustValueList adjustValueList1 = new D.AdjustValueList();

            presetGeometry1.Append(adjustValueList1);
            D.NoFill noFill1 = new D.NoFill();

            D.SolidFill solidFill10 = new D.SolidFill();
            D.RgbColorModelHex rgbColorModelHex1 = new D.RgbColorModelHex() { Val = "FFFFFF" };

            solidFill10.Append(rgbColorModelHex1);

            shapeProperties1.Append(transform2D1);
            shapeProperties1.Append(presetGeometry1);
            shapeProperties1.Append(noFill1);
            P.NonVisualPictureProperties nonVisualPictureProperties1 = new P.NonVisualPictureProperties();
            P.NonVisualDrawingProperties nonVisualDrawingProperties2 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)1026U, Name = "", Description = "" };

            P.NonVisualPictureDrawingProperties nonVisualPictureDrawingProperties1 = new P.NonVisualPictureDrawingProperties();
            D.PictureLocks pictureLocks1 = new D.PictureLocks() { NoChangeAspect = true, NoChangeArrowheads = true };

            nonVisualPictureDrawingProperties1.Append(pictureLocks1);
            ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties2 = new ApplicationNonVisualDrawingProperties();

            nonVisualPictureProperties1.Append(nonVisualDrawingProperties2);
            nonVisualPictureProperties1.Append(nonVisualPictureDrawingProperties1);
            nonVisualPictureProperties1.Append(applicationNonVisualDrawingProperties2);

            picture1.Append(nonVisualPictureProperties1);
            picture1.Append(blipFill1);
            picture1.Append(shapeProperties1);

            shapeTree1.Append(picture1);
        }

        private static void AppendImageData(List<byte> PngData, string pictureid, SlidePart slidePart1)
        {
                    MemoryStream PNG = new MemoryStream((PngData).ToArray<byte>());
                    ImagePart imagePart1 = slidePart1.AddNewPart<ImagePart>("image/png", pictureid);
                    PNG.Position = 0;
                    imagePart1.FeedData(PNG);
                    PNG.Close();
        }

        #endregion

        #region Создание Shape

        private static P.NonVisualGroupShapeProperties CreateNonVisualGroupShapeProperties()
        {
            P.NonVisualGroupShapeProperties nonVisualGroupShapeProperties1 = new P.NonVisualGroupShapeProperties();
            P.NonVisualDrawingProperties nonVisualDrawingProperties1 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" };
            P.NonVisualGroupShapeDrawingProperties nonVisualGroupShapeDrawingProperties1 = new P.NonVisualGroupShapeDrawingProperties();
            ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties1 = new ApplicationNonVisualDrawingProperties();
            nonVisualGroupShapeProperties1.Append(nonVisualDrawingProperties1);
            nonVisualGroupShapeProperties1.Append(nonVisualGroupShapeDrawingProperties1);
            nonVisualGroupShapeProperties1.Append(applicationNonVisualDrawingProperties1);
            return nonVisualGroupShapeProperties1;
        }

        private static GroupShapeProperties CreateGroupShapeProperties()
        {
            GroupShapeProperties groupShapeProperties1 = new GroupShapeProperties();
            D.TransformGroup transformGroup1 = new D.TransformGroup();
            D.Offset offset1 = new D.Offset() { X = 0L, Y = 0L };
            D.Extents extents1 = new D.Extents() { Cx = 0L, Cy = 0L };
            D.ChildOffset childOffset1 = new D.ChildOffset() { X = 0L, Y = 0L };
            D.ChildExtents childExtents1 = new D.ChildExtents() { Cx = 0L, Cy = 0L };
            transformGroup1.Append(offset1);
            transformGroup1.Append(extents1);
            transformGroup1.Append(childOffset1);
            transformGroup1.Append(childExtents1);
            groupShapeProperties1.Append(transformGroup1);
            return groupShapeProperties1;
        }
    
        private static ShapeTree CreateShapeTree(SlidePart slidePart1, Slide slide1)
        {
            CommonSlideData commonSlideData1 = new CommonSlideData();
            ShapeTree shapeTree1 = new ShapeTree();
            commonSlideData1.Append(shapeTree1);
            slide1.Append(commonSlideData1);
            slidePart1.Slide = slide1;
            P.NonVisualGroupShapeProperties nonVisualGroupShapeProperties1 = CreateNonVisualGroupShapeProperties();
            GroupShapeProperties groupShapeProperties1 = CreateGroupShapeProperties();
            shapeTree1.Append(nonVisualGroupShapeProperties1);
            shapeTree1.Append(groupShapeProperties1);
            return shapeTree1;
        }
        
        #endregion
    }
}