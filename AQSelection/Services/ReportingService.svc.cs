﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;

namespace AQSelection
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public partial class ReportingService : IReportingDuplexService
    {
        public Dictionary<Guid, BackgroundWorker> ActiveWorkers;
        IReportingDuplexClient client;
        
        public ReportingService()
        {
            ActiveWorkers = new Dictionary<Guid, BackgroundWorker>();
            client = OperationContext.Current.GetCallbackChannel<IReportingDuplexClient>();
        }
        
        #region Операции с SLTable

        public void FillDataWithSLTables(Guid OperationGuid, List<ReportDataTable> sldt)
        {
            DataSet DataStorage = new DataSet();
            Guid StoredDataTableGuid = Guid.NewGuid();

            BackgroundWorker bw = new BackgroundWorker() { WorkerSupportsCancellation = true };
            ActiveWorkers.Add(OperationGuid, bw);
            HttpContext hc = Sessions.GetHttpContext();

            bw.DoWork += (o, e) =>
            {
                foreach (ReportDataTable t in sldt)
                {
                    if (!bw.CancellationPending)
                    {
                        DataTable dt = new DataTable();
                        dt.TableName = t.TableName;

                        for (int i = 0; i < t.ColumnNames.Count; i++)
                        {

                            var DT = t.DataTypes[i] != null
                            ?   Type.GetType((!t.DataTypes[i].StartsWith("System.")
                                    ? "System."
                                    : String.Empty) + t.DataTypes[i].Replace("Decimal", "Double"))
                            : typeof(string);
                            DataColumn dc = new DataColumn() { ColumnName = t.ColumnNames[i], DataType = DT };
                            dt.Columns.Add(dc);
                        }

                        foreach (var r in t.Table)
                        {
                            DataRow dr = dt.Rows.Add();
                            for (int j = 0; j < dr.ItemArray.Count(); j++)
                            {
                                object v = r.Row[j];
                                if (v!=null && v.ToString()!=String.Empty)dr.SetField(j, v.ToString());
                            }
                        }
                        var extendedProperties = t.CustomProperties;
                        if (extendedProperties != null)
                        {
                            foreach (var v in extendedProperties)
                            {
                                if (v != null)
                                {
                                    if (!v.Key.Contains("#@#")) dt.ExtendedProperties.Add(v.Key, v.Value);
                                    else
                                    {
                                        var property = v.Key.Split(new[] { "#@#" }, StringSplitOptions.None);
                                        dt.Columns[property[0]].ExtendedProperties.Add(property[1], v.Value);
                                    }
                                }
                            }
                        }
                        DataStorage.Tables.Add(dt);
                    }
                    else
                    {
                        DataStorage = null;
                        e.Cancel = true;
                        e.Result = null;
                    }
                }
            };

            bw.RunWorkerCompleted += (o, e) =>
            {
                if (!e.Cancelled)
                {
                    StoredDataTableGuid = Sessions.NewItem(hc, DataStorage);
                    if (ActiveWorkers.Keys.Contains(OperationGuid)) ActiveWorkers.Remove(OperationGuid);
                    client.Receive(OperationGuid, new OperationResult("Таблица заполнена", true, OperationGuid.ToString(), StoredDataTableGuid.ToString()));
                }
                else
                {
                    client.Receive(OperationGuid, new OperationResult("Операция отменена", false, OperationGuid.ToString(), null));
                }
            };

            bw.RunWorkerAsync();
        }

        public void GetTablesSLCompatible(Guid OperationGuid, string StoredDataSetGuid)
        {
            AQService aqs = new AQService();
            aqs.WriteLog("Calculation Data readed");

            DataSet Data = Sessions.GetItem(StoredDataSetGuid) as DataSet;
            List<ReportDataTable> SLTables = new List<ReportDataTable>();
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (o, e) =>
            {
                foreach (DataTable dt in Data.Tables)
                {
                    ReportDataTable t = new ReportDataTable();
                    t.TableName = dt.TableName;
                    t.ColumnNames = new List<string>();
                    t.DataTypes = new List<string>();
                    t.Table = new List<ReportDataRow>();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        t.ColumnNames.Add(dc.Caption);
                        t.DataTypes.Add(dc.DataType.ToString());
                    }

                    foreach (DataRow dr in dt.Rows)
                    {
                        ReportDataRow r = new ReportDataRow();
                        r.Row = new List<object>();
                        foreach (var a in dr.ItemArray)
                        {
                            r.Row.Add(a.ToString());
                        }
                        t.Table.Add(r);
                    }
                    if (dt.ExtendedProperties.Count > 0)
                    {
                        t.CustomProperties = new List<SLExtendedProperty>();
                        foreach (var p in dt.ExtendedProperties.Keys)
                        {
                            t.CustomProperties.Add(new SLExtendedProperty { Key = p.ToString(), Value = dt.ExtendedProperties[p] });
                        }
                        foreach (DataColumn dc in dt.Columns)
                        {
                            foreach (var ck in dc.ExtendedProperties.Keys)
                            {
                                t.CustomProperties.Add(new SLExtendedProperty { Key = dc.ColumnName + "#@#" + ck, Value = dc.ExtendedProperties[ck] });
                            }
                        }
                    }
                    SLTables.Add(t);
                }
            };
            bw.RunWorkerCompleted += (o, e) =>
            {
                if (!e.Cancelled)
                {
                    if (ActiveWorkers.Keys.Contains(OperationGuid)) ActiveWorkers.Remove(OperationGuid);
                    client.Receive(OperationGuid, new OperationResult("Данные сформированы", true, OperationGuid.ToString(), SLTables));
                }
                else
                {
                    client.Receive(OperationGuid, new OperationResult("Операция отменена", false, OperationGuid.ToString(), null));
                } 
            };
            bw.RunWorkerAsync();
        }

        #endregion

        #region Вспомогательные операции

        public void SendBinary(Guid OperationGuid, List<byte> Array)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(Array.ToArray(), 0, Array.Count());
            client.Receive(OperationGuid, new OperationResult("Файл создан", true, OperationGuid.ToString(), Sessions.NewItem(ms).ToString()));
        }
        
        public void DropData(Guid DropItemGuid)
        {
            Sessions.DisposeItem(DropItemGuid);
        }

        public void CancelOperation(Guid OperationGuid)
        {
            if (ActiveWorkers.Keys.Contains(OperationGuid))
            {
                ActiveWorkers[OperationGuid].CancelAsync();
                ActiveWorkers.Remove(OperationGuid);
            }
        }

        #endregion
    }

    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string TaskDataGuid { get; set; }

        [DataMember]
        public object ResultData { get; set; }
        
        public OperationResult(string message, bool success, string taskDataGuid)
        {
            Message = message;
            Success = success;
            TaskDataGuid = taskDataGuid;
        }
        
        public OperationResult(string message, bool success, string taskDataGuid, object resultData)
        {
            Message = message;
            Success = success;
            TaskDataGuid = taskDataGuid;
            ResultData = resultData;
        }
    }

    [DataContract]
    public class ReportDataRow
    {
        [DataMember]
        public List<object> Row;
    }

    [DataContract]
    public class ReportDataTable
    {
        [DataMember]
        public List<ReportDataRow> Table;

        [DataMember]
        public string SelectionString;

        [DataMember]
        public List<string> ColumnNames;

        [DataMember]
        public List<string> DataTypes;

        [DataMember]
        public string TableName;

        [DataMember]
        public List<SLExtendedProperty> CustomProperties;
        public ReportDataTable()
        {
            Table = new List<ReportDataRow>();
        }

        public void AddRow(ReportDataRow row)
        {
            Table.Add(row);
        }
    }

    #region Данные для презентаций

    [KnownType(typeof(byte[]))]
    [KnownType(typeof(List<byte>))]
    [DataContract]
    public class SlideElementDescription
    {
        [DataMember]
        public SlideElementTypes SlideElementType { get; set; }
        [DataMember]
        public object SlideData { get; set; } //|Image - List<byte> PNG	| Table - ReportingTable |Text | Chart - null
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string ShortDescription { get; set; }
        [DataMember]
        public bool BreakSlide { get; set; }
        [DataMember]
        public double Width { get; set; }
        [DataMember]
        public double Height { get; set; }
    }

    [DataContract]
    public class SlideDescription
    {
        [DataMember]
        public List<SlideElementDescription> SlideElements { get; set; }
    }

    [DataContract]
    public class PresentationDescription
    {
        [DataMember]
        public string ForegroundColor { get; set; }
        [DataMember]
        public bool BlackBackground { get; set; }
        [DataMember]
        public List<SlideDescription> Slides { get; set; }
    }

    [DataContract]
    public enum SlideElementTypes
    {
        [EnumMember]
        Chart,
        [EnumMember]
        Image,
        [EnumMember]
        Text,
        [EnumMember]
        Table
    }

    #endregion

}
