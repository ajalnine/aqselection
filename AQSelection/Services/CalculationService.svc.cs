﻿using System.CodeDom.Compiler;
using System;
using System.Collections;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using AQSelection.Calculators;
using AQMathClasses;
using MySql;
using MySql.Data.MySqlClient;

namespace AQSelection
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class CalculationService
    {
        private SqlDataReader GetReaderForSql(string SQL, SqlConnection conn)
        {
            SqlCommand sc = new SqlCommand(SQL, conn);
            sc.CommandTimeout = 3000;
            sc.CommandType = CommandType.Text;
            return sc.ExecuteReader();
        }

        #region Создание расчета из запроса SQL

        [OperationContract]
        public QueryResult GetQueryResults(string QuerySQL, string TableName, List<InputDescription> InputData, string connectionString)
        {
            try
            {
                var cs = connectionString ?? System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
                if (!string.IsNullOrEmpty(connectionString) && connectionString.Contains("itc-serv01")) cs = System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();////
                var csl = cs.ToLower();
                var isMySQL = csl.Contains("server") && csl.Contains("uid") && csl.Contains("pwd") && csl.Contains("database");
                if (isMySQL) return MySQLQueryResults(QuerySQL, TableName, InputData, cs);
                return MSSQLQueryResults(QuerySQL, TableName, InputData, cs);
            }
            catch(Exception e)
            {
                return new QueryResult(e.Message, false, null);
            }
        }

        private static QueryResult MSSQLQueryResults(string QuerySQL, string TableName, List<InputDescription> InputData, string cs)
        {
            var conn = new SqlConnection(cs);
            conn.Open();
            if (conn.State != ConnectionState.Open) return null;

            var result = new List<DataItem>();
            var table = new DataItem
            {
                DataItemType = DataType.Selection,
                Name = TableName,
                TableName = TableName,
                Fields = new List<DataField>()
            };
            result.Add(table);

            var sql = GetSQLPrefix(InputData, QuerySQL) + "\r\n" + QuerySQL;

            var replaces = Regex.Matches(sql, @"(\{@([a-zA-z0-9а-яА-Я]*)([^\{\}]*)\})");

            foreach (Match r in replaces)
            {
                var name = r.Groups[0].Value.Replace("@", String.Empty)
                    .Replace("{", string.Empty)
                    .Replace("}", string.Empty);
                var v = InputData.Where((a => a.Name == name)).Select(a => a.Value).ToList();
                if (v.Any() && v.First() != null)
                    sql = sql.Replace(r.Groups[0].Value,
                        (string.IsNullOrEmpty(v.First().ToString()) || v.First().ToString() == "''")
                            ? "(NULL)"
                            : v.First().ToString());
            }

            try
            {
                var exc = "exec sp_describe_first_result_set N'" + sql.Replace("'", "''") + "'";
                var command = new SqlCommand(exc, conn);
                var sdr = command.ExecuteReader();
                var schema = new DataTable();
                schema.Load(sdr);

                var indexOfColumnName = schema.Columns.IndexOf("name");
                var indexOfDataType = schema.Columns.IndexOf("system_type_name");

                foreach (DataRow f in schema.Rows)
                {
                    table.Fields.Add(new DataField
                    {
                        Name = f[indexOfColumnName].ToString(),
                        DataType = SqlTypeToType(f[indexOfDataType].ToString()).ToString().Replace("System.", "")
                    });
                }

                sdr.Close();
                return new QueryResult(string.Empty, true, result);
            }
            catch (Exception)
            {
                var command = new SqlCommand(sql, conn);
                var sdr = command.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.SingleRow |
                                                CommandBehavior.SingleResult);
                var schema = sdr.GetSchemaTable();
                if (schema == null) return null;

                var indexOfColumnName = schema.Columns.IndexOf("ColumnName");
                var indexOfDataType = schema.Columns.IndexOf("DataType");

                foreach (DataRow f in schema.Rows)
                {
                    table.Fields.Add(new DataField
                    {
                        Name = f[indexOfColumnName].ToString(),
                        DataType = GetCompatibleDataType(f[indexOfDataType].ToString().Replace("System.", ""))
                    });
                }

                sdr.Close();
                return new QueryResult(string.Empty, true, result);
            }
        }

        private static QueryResult MySQLQueryResults(string QuerySQL, string TableName, List<InputDescription> InputData, string cs)
        {
            var conn = new MySqlConnection(cs);
            conn.Open();
            if (conn.State != ConnectionState.Open) return null;

            var result = new List<DataItem>();
            var table = new DataItem
            {
                DataItemType = DataType.Selection,
                Name = TableName,
                TableName = TableName,
                Fields = new List<DataField>()
            };
            result.Add(table);

            var sql = GetMySQLPrefix(InputData, QuerySQL) + "\r\n" + QuerySQL;

            var replaces = Regex.Matches(sql, @"(\{@([a-zA-z0-9а-яА-Я]*)([^\{\}]*)\})");

            foreach (Match r in replaces)
            {
                var name = r.Groups[0].Value.Replace("@", String.Empty)
                    .Replace("{", string.Empty)
                    .Replace("}", string.Empty);
                var v = InputData.Where((a => a.Name == name)).Select(a => a.Value).ToList();
                if (v.Any() && v.First() != null)
                    sql = sql.Replace(r.Groups[0].Value,
                        (string.IsNullOrEmpty(v.First().ToString()) || v.First().ToString() == "''")
                            ? "(NULL)"
                            : v.First().ToString());
            }

            var command = new MySqlCommand(sql, conn);
            var sdr = command.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.SingleRow |
                                            CommandBehavior.SingleResult);
            var schema = sdr.GetSchemaTable();
            if (schema == null) return null;

            var indexOfColumnName = schema.Columns.IndexOf("ColumnName");
            var indexOfDataType = schema.Columns.IndexOf("DataType");

            foreach (DataRow f in schema.Rows)
            {
                table.Fields.Add(new DataField
                {
                    Name = f[indexOfColumnName].ToString(),
                    DataType = GetCompatibleDataType(f[indexOfDataType].ToString().Replace("System.", ""))
                });
            }

            sdr.Close();
            return new QueryResult(string.Empty, true, result);
        }

        public static Type SqlTypeToType(string type)
        {
            string[] tokens = type.Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
            string typeFamily = tokens[0].ToLowerInvariant();
            string size = tokens.Length > 1 ? tokens[1] : string.Empty;

            switch (typeFamily)
            {
                case "bigint":
                    return typeof(long);
                case "binary":
                    return size == "1" ? typeof(byte) : typeof(byte[]);
                case "bit":
                    return typeof(bool);
                case "char":
                    return size == "1" ? typeof(char) : typeof(string);
                case "datetime":
                case "datetime2":
                case "smalldatetime":
                    return typeof(DateTime);
                case "decimal":
                case "numeric":
                case "float":
                case "money":
                case "int":
                    return typeof(double);
                case "image":
                    return typeof(byte[]);
                case "nchar":
                    return size == "1" ? typeof(char) : typeof(string);
                case "ntext":
                    return typeof(string);
                case "nvarchar":
                    return typeof(string);
                case "real":
                    return typeof(double);
                case "uniqueidentifier":
                    return typeof(Guid);
                case "smallint":
                    return typeof(short);
                case "smallmoney":
                    return typeof(decimal);
                case "sql_variant":
                    return typeof(object);
                case "text":
                    return typeof(string);
                case "time":
                    return typeof(TimeSpan);
                case "tinyint":
                    return typeof(byte);
                case "varbinary":
                    return typeof(byte[]);
                case "varchar":
                    return typeof(string);
                case "variant":
                    return typeof(string);
                case "xml":
                    return typeof(string);
                default:
                    throw new ArgumentException(string.Format("There is no .Net type specified for mapping T-SQL type '{0}'.", type));
            }
        }

        private static string GetCompatibleDataType(string typeName)
        {
            return typeName == "Decimal" ? "Double" : typeName;
        }

        private static string GetSQLPrefix(List<InputDescription> inputData, string sql)
        {
            var prefix = @" 
declare @date1 datetime 
declare @date2 datetime
set @date1=getdate() 
set @date2=getdate() ";
            if (inputData != null && inputData.Count > 0)
            {
                foreach (var i in inputData)
                {
                    string SQLType = String.Empty;
                    string SQLValue = String.Empty;

                    switch (i.Type)
                    {
                        case "String":
                            SQLType = "nvarchar(MAX)";
                            SQLValue = "'abc'";
                            break;
                        case "Double":
                        case "Int32":
                            SQLType = "numeric(16,8)";
                            SQLValue = "1";
                            break;
                        case "Boolean":
                        case "bool":
                            SQLType = "bit";
                            SQLValue = "1";
                            break;
                        case "DateTime":
                            SQLType = "datetime";
                            SQLValue = "getdate()";
                            break;
                        case "TimeSpan":
                            SQLType = "datetime";
                            SQLValue = "getdate()";
                            break;
                    }

                    prefix += "declare @" + i.Name.Replace(" ","_") + " " + SQLType + "\r\n";
                    prefix += "set @" + i.Name.Replace(" ", "_") + "=" + SQLValue + "\r\n";
                }
            }
            if (sql.ToLower().Contains("with")) prefix += ";";
            return prefix;
        }


        private static string GetMySQLPrefix(List<InputDescription> inputData, string sql)
        {
            var prefix = @" 
            set @date1:=now();
            set @date2:=now(); ";
            if (inputData != null && inputData.Count > 0)
            {
                foreach (var i in inputData)
                {
                    string SQLType = String.Empty;
                    string SQLValue = String.Empty;

                    switch (i.Type)
                    {
                        case "String":
                            SQLValue = "'abc'";
                            break;
                        case "Double":
                        case "Int32":
                            SQLValue = "1";
                            break;
                        case "Boolean":
                        case "bool":
                            SQLValue = "1";
                            break;
                        case "DateTime":
                            SQLValue = "now()";
                            break;
                        case "TimeSpan":
                            SQLValue = "now()";
                            break;
                    }

                    prefix += "set @" + i.Name.Replace(" ", "_") + ":=" + SQLValue + ";\r\n";
                }
            }
            return prefix;
        }
        #endregion

        #region Выполнение расчета

        [OperationContract]
        public StepResult InitCalculation(DateTime From, DateTime To, List<InputDescription> UserInput)
        {
            DataSet DataStore = new DataSet();
            DataTable e = new DataTable("Переменные");
            DataStore.Tables.Add(e);
            ProcessUserInput(From, To, UserInput, e);
            string StoredDataSetGuid = Sessions.NewItem(DataStore).ToString();
            return new StepResult("Расчет инициализирован", true, StoredDataSetGuid);
        }

        [OperationContract]
        public StepResult ExecuteStep(string StoredDataSetGuid, Step StepData)
        {
            string ClassName = StepData.Calculator;
            string Name = StepData.Name;
            if (StepData.Group == "Параметры") return new StepResult(Name, true, StoredDataSetGuid);
            Type t = Type.GetType("AQSelection.Calculators." + ClassName);
            ICalculator calculator = Activator.CreateInstance(t) as ICalculator;
            DataSet DataStorage = Sessions.GetItem(StoredDataSetGuid) as DataSet;
            StepProcessResult SPR = calculator.ProcessData(DataStorage, StepData);
            DataStorage = SPR.ResultedDataSet;
            Sessions.UpdateItem(StoredDataSetGuid, DataStorage);
            return new StepResult(SPR.Success ? Name: SPR.Message, SPR.Success, StoredDataSetGuid);
        }

        [OperationContract]
        public StepResult ExecuteCalculation(int CalculationID, DateTime From, DateTime To,
            List<InputDescription> UserInput)
        {
            List<Step> calculationSteps = CatalogueHelper.GetCalculationByID(CalculationID);
            var DataStorage = StepResult(From, To, UserInput, calculationSteps);
            return new StepResult("Расчет", true, Sessions.NewItem(DataStorage).ToString());
        }

        [OperationContract]
        public List<SLDataTable> GetCalculationResult(int CalculationID, DateTime From, DateTime To, List<InputDescription> UserInput)
        {
            List<Step> calculationSteps = CatalogueHelper.GetCalculationByID(CalculationID);
            var DataStorage = StepResult(From, To, UserInput, calculationSteps);
            return GetSLTablesFromDataSet(DataStorage);
        }

        [OperationContract]
        public StepResult ExecuteSteps(DateTime From, DateTime To, List<InputDescription> UserInput, List<Step> CalculationSteps)
        {
            var DataStorage = StepResult(From, To, UserInput, CalculationSteps);
            return new StepResult("Расчет", true, Sessions.NewItem(DataStorage).ToString());
        }
        private DataSet StepResult(DateTime From, DateTime To, List<InputDescription> UserInput, List<Step> calculationSteps)
        {
            DataSet DataStorage = new DataSet();
            DataTable e = new DataTable("Переменные");
            DataStorage.Tables.Add(e);
            ProcessUserInput(From, To, UserInput, e);
            foreach (Step s in calculationSteps)
            {
                if (s.Group != "Параметры")
                {
                    string ClassName = s.Calculator;
                    string Name = s.Name;
                    Type t = Type.GetType("AQSelection.Calculators." + ClassName);
                    ICalculator calculator = Activator.CreateInstance(t) as ICalculator;
                    StepProcessResult SPR = calculator.ProcessData(DataStorage, s);
                    DataStorage = SPR.ResultedDataSet;
                    if (!SPR.Success)
                    {
                        return DataStorage;
                    }
                }
            }
            return DataStorage;
        }

        private void ProcessUserInput(DateTime From, DateTime To, List<InputDescription> UserInput, DataTable Variables)
        {
            Variables.Columns.Add(new DataColumn("От", typeof (DateTime)));
            Variables.Columns.Add(new DataColumn("До", typeof (DateTime)));
            DataRow dr = Variables.NewRow();
            dr["От"] = From;
            dr["До"] = To;

            if (UserInput != null)
            {
                foreach (var c in UserInput)
                {
                    Variables.Columns.Add(new DataColumn(c.Name, Type.GetType("System." + c.Type)));
                    dr[c.Name] = c.Value;
                }
            }
            Variables.Rows.Add(dr);
        }

        #endregion

        #region Экспорт данных

        [OperationContract]
        public StepResult FillDataWithSQL(string SQL, DateTime From, DateTime To)
        {
            return FillDataSetWithSQLNamed(SQL, From, To, new List<string>() {"Данные"});
        }

        [OperationContract]
        public StepResult FillDataWithSQLNamed(string SQL, DateTime From, DateTime To, string TableName)
        {
            return FillDataSetWithSQLNamed(SQL, From, To, new List<string>() {TableName});
        }

        [OperationContract]
        public StepResult FillDataSetWithSQLNamed(string sql, DateTime From, DateTime To, List<string> TableNames)
        {
            var connectionString =
                System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            var fullSql = GetSQLPrepend(From, To) + sql;
            var sconn = new SqlConnection(connectionString);
            sconn.Open();

            var testResult = SQLIsValid(sql);

            if (testResult != null) return new StepResult(testResult, false, null);
            try
            {
                var dataStorage = ReportFactoryHelper.FillDataSetFromSqlQuery(fullSql);
                if (dataStorage.Tables.Count == 0) return new StepResult("Нет данных", false, null);
                NameTables(TableNames, dataStorage);
                var userMessage = LogTableSize(dataStorage);
                return new StepResult(userMessage, true, Sessions.NewItem(dataStorage).ToString());
            }
            catch (SystemException e)
            {
                sconn.Close();
                return new StepResult(e.Message, false, null);
            }
        }

        private static void NameTables(List<string> TableNames, DataSet DataStorage)
        {
            if (DataStorage.Tables.Count == TableNames.Count)
            {
                int i = 0;
                foreach (var TableName in TableNames)
                {
                    DataStorage.Tables[i].TableName = String.IsNullOrEmpty(TableName)
                        ? "Таблица_" + (++i).ToString()
                        : TableName;
                }
            }
            else
            {
                int i = 0;
                foreach (DataTable dataTable in DataStorage.Tables)
                {
                    dataTable.TableName = "Таблица_" + (++i).ToString();
                }
            }
        }

        private static string LogTableSize(DataSet DataStorage)
        {
            AQService aqs = new AQService();

            int TableCount = DataStorage.Tables.Count;
            int RowCount = 0;
            int ColumnCount = 0;

            foreach (DataTable dt in DataStorage.Tables)
            {
                RowCount += dt.Rows.Count;
                ColumnCount += dt.Columns.Count;
            }

            string UserMessage = String.Format("Таблиц: {0}, Записей: {1}, Полей: {2}", TableCount, RowCount,
                ColumnCount);
            string LogMessage = String.Format("Data written; Tables: {0}, Records: {1}, Columns: {2}", TableCount,
                RowCount,
                ColumnCount);

            aqs.WriteLog(LogMessage);
            return UserMessage;
        }

        [OperationContract]
        public int GetScalar(string SQL)
        {
            string CheckResult = SQLIsValid(SQL);
            if (!String.IsNullOrEmpty(CheckResult)) throw (new Exception(CheckResult));

            string connectionString =
                System.Configuration.ConfigurationManager.ConnectionStrings["raportConnectionString"].ToString();
            SqlConnection sconn = new SqlConnection(connectionString);
            sconn.Open();
            SqlCommand sc = new SqlCommand(SQL, sconn);
            sc.CommandTimeout = 3000;
            sc.CommandType = System.Data.CommandType.Text;
            int Scalar = (int) sc.ExecuteScalar();
            sconn.Close();
            return Scalar;
        }

        [OperationContract]
        public StepResult FillDataWithSLTables(List<SLDataTable> sldt)
        {
            DataSet DataStorage = new DataSet();
            InsertSLDataTablesToDataSet(sldt, DataStorage);
            var storedDataTableGuid = Sessions.NewItem(DataStorage);
            var aqs = new AQService();
            aqs.WriteLog("Raw data written");
            return new StepResult("Таблица заполнена", true, storedDataTableGuid.ToString());
        }

        [OperationContract]
        public StepResult AppendSLTables(string storedDataSetGuid, List<SLDataTable> sldt)
        {
            DataSet DataStorage = Sessions.GetItem(storedDataSetGuid) as DataSet;
            foreach (var s in sldt) s.TableName = s.TableName; //Таблицы скрываются на клиенте для возможности добавлять в расчет синхронно с операциями в анализе, переименование в Calc_Report
            InsertSLDataTablesToDataSet(sldt, DataStorage);
            Sessions.UpdateItem(storedDataSetGuid, DataStorage);
            return new StepResult("Таблицы добавлены", true, storedDataSetGuid);
        }

        private static void InsertSLDataTablesToDataSet(List<SLDataTable> sldt, DataSet DataStorage)
        {
            foreach (SLDataTable t in sldt)
            {
                var dt = new DataTable();
                dt.TableName = t.TableName;

                for (var i = 0; i < t.ColumnNames.Count; i++)
                {
                    var name = t.ColumnNames[i];
                    var dc = new DataColumn()
                    {
                        ColumnName = name,
                        DataType = Type.GetType(t.DataTypes[i].StartsWith("System.") ? t.DataTypes[i] : "System." + t.DataTypes[i])
                    };
                    dt.Columns.Add(dc);
                }
                var extendedProperties = t.CustomProperties;
                if (extendedProperties != null)
                {
                    foreach (var v in extendedProperties.Where(a=>a!=null))
                    {
                        if (!v.Key.Contains("#@#")) dt.ExtendedProperties.Add(v.Key, v.Value);
                        else
                        {
                            var property = v.Key.Split(new[] { "#@#" }, StringSplitOptions.None);
                            dt.Columns[property[0]].ExtendedProperties.Add(property[1], v.Value);
                        }
                    }
                }

                foreach (var r in t.Table)
                {
                    DataRow dr = dt.Rows.Add();
                    for (int j = 0; j < dr.ItemArray.Count(); j++)
                    {
                        var value = string.IsNullOrEmpty(r.Row[j]?.ToString()) ? DBNull.Value : r.Row[j];
                        dr.SetField(j, value);
                    }
                }
                if (DataStorage.Tables.Contains(dt.TableName)) DataStorage.Tables.Remove(dt.TableName);
                DataStorage.Tables.Add(dt);
            }
        }


        protected string GetSQLPrepend(DateTime @from, DateTime to)
        {
            string sqlPrepend = string.Format(@"set dateformat ymd
declare @date1 datetime 
declare @date2 datetime
set @date1='{0}'  set @date2='{1}' 
", @from.ToString("yyyy-MM-dd"), to.ToString("yyyy-MM-dd"));
            return sqlPrepend;
        }

        protected string SQLIsValid(string SQL)
        {
            var restrictedKeywords = new[]
            {"update ", "insert ", "create ", "delete ", "drop ", "modify ", "truncate ", "exec "};
            var requiredKeywords = new string[] {};

            foreach (var s in restrictedKeywords)
            {
                if (SQL.ToLower().Contains(s))
                {
                    return "Запрос не должен содержать ключевое слово " + s;
                }
            }

            foreach (string s in requiredKeywords)
            {
                if (!SQL.ToLower().Contains(s.ToLower()))
                {
                    return "Отсутствует обязательное выражение " + s;
                }
            }
            return null;
        }

        [OperationContract]
        public List<SLDataTable> GetTablesSLCompatible(string storedDataSetGuid)
        {
            var aqs = new AQService();
            aqs.WriteLog("Calculation Data readed");

            var Data = Sessions.GetItem(storedDataSetGuid) as DataSet;
            return GetSLTablesFromDataSet(Data);

        }

        private List<SLDataTable> GetSLTablesFromDataSet(DataSet Data)
        {
            var slTables = new List<SLDataTable>();

            foreach (DataTable dt in Data.Tables)
            {
                var t = new SLDataTable();
                t.TableName = dt.TableName;
                t.ColumnNames = new List<string>();
                t.DataTypes = new List<string>();
                t.Table = new List<SLDataRow>();
                if (dt.ExtendedProperties.Count > 0)
                {
                    t.CustomProperties = new List<SLExtendedProperty>();
                    foreach (var p in dt.ExtendedProperties.Keys)
                    {
                        t.CustomProperties.Add(new SLExtendedProperty { Key = p.ToString(), Value = dt.ExtendedProperties[p] });
                    }
                    foreach (DataColumn dc in dt.Columns)
                    {
                        foreach (var ck in dc.ExtendedProperties.Keys)
                        {
                            t.CustomProperties.Add(new SLExtendedProperty { Key = dc.ColumnName + "#@#" + ck, Value = dc.ExtendedProperties[ck] });
                        }
                    }
                }
                foreach (DataColumn dc in dt.Columns)
                {
                    t.ColumnNames.Add(dc.Caption);
                    var typeName = dc.DataType.ToString();
                    if (typeName.EndsWith("Decimal")) typeName = "Double";
                    t.DataTypes.Add(typeName);
                }

                foreach (DataRow dr in dt.Rows)
                {
                    SLDataRow r = new SLDataRow();
                    r.Row = new List<object>();
                    foreach (var a in dr.ItemArray)
                    {
                        if (a is Double?) r.Row.Add((Double?)a);
                        else if (a is Int32?) r.Row.Add((Int32?)a);
                        else if (a is DateTime?) r.Row.Add((DateTime?)a);
                        else if (a is Decimal?) r.Row.Add(decimal.ToDouble((Decimal)a));
                        else r.Row.Add(a.ToString());
                    }
                    t.Table.Add(r);
                }
                slTables.Add(t);
            }
            return slTables;
        }
        #endregion

        #region Проверка выражений

        [OperationContract]
        public StepResult CheckExpression(string Expression, List<DataItem> DataStructure, string CurrentTableName)
        {
            string src = @"
            using System;
            using System.Data;
            using AQMathClasses;
              
            public class ExpressionClass:IMathExpression
            {
                public object GetResult(DataRow dr)
                {
                    DataRow row=dr;
            " + Expression + @"
                }
            }";
            Microsoft.CSharp.CSharpCodeProvider cp = new Microsoft.CSharp.CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } });
            CompilerParameters p = new CompilerParameters(){ GenerateInMemory = true, GenerateExecutable = false, };
            p.ReferencedAssemblies.Add("system.dll");
            p.ReferencedAssemblies.Add("system.xml.dll");
            p.ReferencedAssemblies.Add("system.data.dll");
            p.ReferencedAssemblies.Add(HttpContext.Current.Server.MapPath(@"\AQSelection\bin\aqmathclasses.dll"));
            CompilerResults cr = cp.CompileAssemblyFromSource(p, new string[] { src });
            
            string Errors = string.Empty;
            foreach (CompilerError ce in cr.Errors)
            {
                Errors += ce.ErrorText + "(" + ce.Column.ToString() + ";" + (ce.Line - 10).ToString() + ")" + "\r\n";
            }

            if (cr.Errors.Count == 0 && cr.CompiledAssembly != null)
            {
                var objType = cr.CompiledAssembly.GetType("ExpressionClass");
                try
                {
                    if (objType != null)
                    {
                        var calculator = (IMathExpression)Activator.CreateInstance(objType);
                        var testData = GetTestData(DataStructure);
                        var x = calculator.GetResult(testData.Tables[CurrentTableName].Rows[0]);
                        var y = calculator.GetResult(testData.Tables[CurrentTableName].Rows[1]);
                        if (x == null && y == null) return new StepResult("null", true, "null");
                        var t = (x==null) ? y?.GetType().Name : x?.GetType().Name;
                        var res = (x == null) ? y.ToString() : x.ToString();
                        if (!t.Contains("List")) return new StepResult(t, true, res);
                        var list = (IList) y;
                        if (list.OfType<double>().Any()) t = "Double";
                        else if (list.OfType<DateTime>().Any()) t = "DateTime";
                        else if (list.OfType<TimeSpan>().Any()) t = "TimeSpan";
                        else if (list.OfType<bool>().Any()) t = "Boolean";
                        else if (list.OfType<string>().Any()) t = "String";
                        return new StepResult(t, true, y.ToString());
                    }
                }
                catch(Exception e)
                {
                    return new StepResult(Errors, false, e.Message);
                }
            }
            return new StepResult(Errors, false, string.Empty);
        }

        private DataSet GetTestData(List<DataItem> TestData)
        {
            DataSet DataStorage = new DataSet();
            foreach (DataItem di in TestData)
            {
                DataTable dt = new DataTable();
                dt.TableName = di.Name;
                foreach (DataField df in di.Fields)
                {
                    if (dt.Columns.Contains(df.Name)) continue;
                    DataColumn dc = new DataColumn();
                    dc.ColumnName = df.Name;
                    var t = df.DataType.StartsWith("System.") ? df.DataType : "System." + df.DataType;
                    dc.DataType = Type.GetType(t);
                    dt.Columns.Add(dc);
                }
                DataRow dr = dt.Rows.Add();
                DataRow dr1 = dt.Rows.Add();
                foreach (DataField df in di.Fields)
                {
                    switch (df.DataType.Replace("System.", "").ToLower()) 
                    {
                        case "datetime":
                             dr[df.Name] = DateTime.Now;
                             dr1[df.Name] = DateTime.Now;
                             break;
                        case "timespan":
                             dr[df.Name] = TimeSpan.Zero;
                             dr1[df.Name] = TimeSpan.Zero;
                             break;
                        case "string":
                             dr[df.Name] = "2010-07-28 00:06:45";
                             dr1[df.Name] = "abc";
                             break;
                        case "boolean":
                        case "bool":
                             dr[df.Name] = true;
                             dr1[df.Name] = true;
                             break;
                        case "int":
                        case "int32":
                        case "int64":
                             dr[df.Name] = -1;
                             dr1[df.Name] = -1;
                             break;
                        case "uint":
                        case "uint32":
                        case "uint64":
                             dr[df.Name] = 1;
                             dr1[df.Name] = 1;
                             break;
                        default:
                             dr[df.Name] = 1e-10;
                             dr1[df.Name] = 1e-10;
                             break;
                    }
                }
                DataStorage.Tables.Add(dt);
            }
            return DataStorage;
        }

        [OperationContract]
        public void DropData(string DropItemGuid)
        {
            Sessions.DisposeItem(DropItemGuid);
        }
        #endregion
    }

    #region Классы обмена данными, совместимые с клиентскими

    [DataContract(Namespace = "AQSelection")]
    public class InputDescription
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public object Value { get; set; }
        [DataMember]
        public string Type { get; set; }
    }
     
    [DataContract(Namespace = "AQSelection")]
    public class Step
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string ParametersXML { get; set; }
        [DataMember]
        public List<DataItem> Inputs { get; set; }
        [DataMember]
        public List<DataItem> Outputs { get; set; }
        [DataMember]
        public List<string> DeletedItems { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public string EditorAssemblyPath { get; set; }
        [DataMember]
        public string Calculator { get; set; }
    }

    [DataContract(Namespace = "AQSelection")]
    public class DataItem
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DataType DataItemType { get; set; }
        [DataMember]
        public List<DataField> Fields { get; set; }
        [DataMember]
        public string TableName { get; set; }
    }

    [DataContract(Namespace = "AQSelection")]
    public enum DataType {
        [EnumMember]
        Selection,
        [EnumMember]
        Variables
    }

    [DataContract(Namespace = "AQSelection")]
    public class DataField
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DataType { get; set; }
    }

    [DataContract(Namespace = "AQSelection")]
    public class StepResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public string TaskDataGuid;

        public StepResult(string message, bool success, string taskDataGuid)
        {
            Message = message;
            Success = success;
            TaskDataGuid = taskDataGuid;
        }
    }

    [DataContract(Namespace = "AQSelection")]
    public class QueryResult
    {
        [DataMember]
        public string Message;

        [DataMember]
        public bool Success;

        [DataMember]
        public List<DataItem> Fields;

        public QueryResult(string message, bool success, List<DataItem> fields)
        {
            Message = message;
            Success = success;
            Fields = fields;
        }
    }

    [DataContract(Namespace = "http://www.w3.org/2001/XMLSchema", Name = "SLDataRow")]
    public class SLDataRow
    {
        [DataMember]
        public List<object> Row;
    }

    [DataContract(Namespace = "http://www.w3.org/2001/XMLSchema", Name = "SLExtendedProperty")]
    public class SLExtendedProperty
    {
        [DataMember]
        public string Key;
        [DataMember]
        public object Value;
    }

    [DataContract(Namespace = "", Name = "SLDataTable")]
    public class SLDataTable
    {
        [DataMember]
        public List<SLDataRow> Table;

        [DataMember]
        public List<SLDataRow> UnfilteredTable;

        [DataMember]
        public bool IsFiltered;

        [DataMember]
        public List<SLDataFilter> Filters;

        [DataMember]
        public string SelectionString;
        
        [DataMember]
        public List<string> ColumnNames;

        [DataMember]
        public List<object> ColumnTags;

        [DataMember]
        public List<object> RowTags;

        [DataMember]
        public List<string> DataTypes;

        [DataMember]
        public string TableName;

        [DataMember]
        public string TableKey;

        [DataMember]
        public string Header;

        [DataMember]
        public string Tag;
        
        [DataMember]
        public List<SLExtendedProperty> CustomProperties;
        
        public SLDataTable()
        {
            Table = new List<SLDataRow>();
            IsFiltered = false;
            TableKey = Guid.NewGuid().ToString();
        }

        public void AddRow(SLDataRow row)
        {
            Table.Add(row);
        }
    }

    [DataContract(Namespace = "", Name = "SLDataFilter")]
    public class SLDataFilter
    {
        [DataMember]
        public List<SLDataFilterItem> FilterItems;

        [DataMember]
        public string ColumnName;
    }

    [DataContract(Namespace = "", Name = "SLDataFilterItem")]
    public class SLDataFilterItem
    {
        [DataMember]
        public bool Included;

        [DataMember]
        public string Caption;

        [DataMember]
        public object Value;
    }

    #endregion
}