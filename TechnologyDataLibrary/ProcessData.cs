﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Threading;

namespace TechnologyDataLibrary
{
    public class ProcessData
    {
        public delegate void NewMessageDelegate(object o, NewMessageEventArgs e);
        public event NewMessageDelegate NewMessage;
        
        public delegate void ProcessDelegate(object o, ProcessMessageEventArgs e);
        public event ProcessDelegate MeltCounted;
        public event ProcessDelegate MeltProgress;

        public static DateTime SFrom, STo;
        private static DateTime StartDate;
        private raportEntities Raport;
        private SqlConnection Conn;
        private List<SmeltDefinition> SmeltToProcess;
        private List<Technology_GetMethodsForGroups> GroupList = null;
        private List<ParametersCacheItem> ParametersCache = null;

        public void Process(DateTime? From, DateTime? To, string SQLConnectionString, bool NewThread, List<Technology_GetMethodsForGroups> groupList)
        {
            GroupList = groupList;
            if (!From.HasValue) From = DateTime.Now.AddDays(-7);
            if (!To.HasValue) To = DateTime.Now;
            Say(String.Format("Обработка за период {0} - {1}", From.Value.ToShortDateString(), To.Value.ToShortDateString()));
            Say(String.Format("ConnectionString: {0}", SQLConnectionString));
            Conn = new SqlConnection(SQLConnectionString);

            Say(String.Format("Время начала обработки: {0}", DateTime.Now.ToLongTimeString()));

            StartDate = DateTime.Now;
            SFrom = From.Value;
            STo = To.Value;
            SmeltToProcess = new List<SmeltDefinition>();
            if (NewThread)
            {
                ThreadStart ts = new ThreadStart(DataTransform);
                ts.BeginInvoke(new AsyncCallback(ProcessFinishedCallBack), new object());
            }
            else
            {
                DataTransform();
                ProcessFinishedCallBack(null);
            }
        }

        private void ProcessFinishedCallBack(IAsyncResult iar)
        {
            Say(String.Format("Время выполнения: {0} сек.", (DateTime.Now - StartDate).TotalMilliseconds/1000));
        }

        private void DataTransform()
        {
            Raport = new raportEntities();
            Raport.CommandTimeout = 1500;
            Technology_GetMethodsForSmelts SQLForSmelts = Raport.Technology_GetMethodsForSmelts.FirstOrDefault();
            if (SQLForSmelts == null)
            {
                Say(String.Format("SQL для списка плавок не задан."));
                Thread.CurrentThread.Abort();
            }
            Conn.Open();
            if (GroupList == null || GroupList.Count == 0)
            {
                Say("Удаление данных за указанный период");
                SqlCommand sc1 = new SqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED delete from Technology_Smelts where RegisterDate between @from and @To", Conn);
                sc1.CommandTimeout = 1500;
                sc1.Parameters.Add(new SqlParameter() { ParameterName = "@From", Value = SFrom});
                sc1.Parameters.Add(new SqlParameter() { ParameterName = "@To", Value = STo});
                sc1.ExecuteNonQuery();
            }
            Say("Кэширование справочника параметров: " + DateTime.Now.ToLongTimeString());
                
            ParametersCache = new List<ParametersCacheItem>();
            foreach (var p in Raport.Common_Parameters)
            {
                ParametersCacheItem pci = new ParametersCacheItem() { ParameterName = p.ParameterName, GroupID = p.id_paramtype.Value, ParameterID = p.id, IsNumber = p.IsNumber.Value };
                ParametersCache.Add(pci);
            }

            Say("Формирование списка плавок ЭСПЦ-6: " + DateTime.Now.ToLongTimeString());
            try
            {
                SqlDataReader sdr3 = GetReader(SQLForSmelts.GetMethodESPC6);
                while (sdr3.Read())
                {
                    ProcessSmelt(sdr3, WorkShops.ESPC6);
                }
                sdr3.Close();
                Raport.SaveChanges();
            }
            catch (Exception e)
            {
                Say("Не удалось получить данные ЭСПЦ-6" + e.Message);
                throw (new Exception());
            }

            Say("Формирование списка плавок ККЦ: " + DateTime.Now.ToLongTimeString());
            try
            {
                SqlDataReader sdr1 = GetReader(SQLForSmelts.GetMethodKKC);
                while (sdr1.Read())
                {
                    ProcessSmelt(sdr1, WorkShops.KKC);
                }
                sdr1.Close();
                Raport.SaveChanges();
            }
            catch(Exception e)
            {
                Say("Не удалось получить данные ККЦ:" + e.Message);
                throw (new Exception());
            }

            Say("Формирование списка плавок ЭСПЦ-2: " + DateTime.Now.ToLongTimeString());
            try
            {
                SqlDataReader sdr2 = GetReader(SQLForSmelts.GetMethodESPC2);
                while (sdr2.Read())
                {
                    ProcessSmelt(sdr2, WorkShops.ESPC2);
                }
                sdr2.Close();
                Raport.SaveChanges();
            }
            catch (Exception e)
            {
                Say("Не удалось получить данные ЭСПЦ-2" + e.Message);
                throw (new Exception());
            }
            Say("Список плавок получен по всем цехам: " + DateTime.Now.ToLongTimeString());
            if (MeltCounted != null) MeltCounted.Invoke(this, new ProcessMessageEventArgs() { Number = SmeltToProcess.Count });
            Say("Чтение параметров, в обработке плавок: " + SmeltToProcess.Count);
            int i = 0;
            foreach (SmeltDefinition sd in SmeltToProcess)
            {
                Say($"Плавка: {sd.Smelt.SmeltNumber}, ID: {sd.Smelt.SmeltID}, OID: {sd.Smelt.OriginalSmeltID}, Цех: {sd.Smelt.Technology_Workshops.Workshop} ({sd.WorkShop})");

                ProcessSmeltGroups(sd.Smelt, sd.WorkShop);
                i++;
                if (MeltProgress != null) MeltProgress.Invoke(this, new ProcessMessageEventArgs() { Number = i });
                Raport.SaveChanges();
                Raport.AcceptAllChanges();
            }

        
            Say("Данные сохранены: " + DateTime.Now.ToLongTimeString());
            Finalize();
            Say("Постообработка данных выполнена: " + DateTime.Now.ToLongTimeString());
        }

        private SqlDataReader GetReader(string SQL)
        {
            SqlCommand sc1 = new SqlCommand(SQL, Conn);
            sc1.Parameters.Add(new SqlParameter() { ParameterName = "From", Value = SFrom });
            sc1.Parameters.Add(new SqlParameter() { ParameterName = "To", Value = STo });
            sc1.CommandTimeout = 1500;
            SqlDataReader sdr1 = sc1.ExecuteReader();
            return sdr1;
        }

        private SqlDataReader GetSmeltReader(string SQL, decimal SmeltID)
        {
            SqlCommand sc1 = new SqlCommand(SQL, Conn);
            sc1.Parameters.Add(new SqlParameter() { ParameterName = "SmeltID", Value = SmeltID});
            sc1.CommandTimeout = 1500; 
            SqlDataReader sdr1 = sc1.ExecuteReader();
            return sdr1;
        }

        private void Finalize()
        {
            var SQL = @"WITH t2 (smeltnumber, markid) AS

            (
                SELECT smeltnumber, max(markid)
            FROM Technology_Smelts
            GROUP BY smeltnumber
                having DATEDIFF(d, min(registerdate), max(registerdate)) < 100
                )

            UPDATE Technology_Smelts SET MarkID = t2.markid
            FROM Technology_Smelts
            INNER JOIN t2 ON t2.smeltnumber = Technology_Smelts.smeltnumber
            where Technology_Smelts.MarkID is null";
            SqlCommand sc1 = new SqlCommand(SQL, Conn) {CommandTimeout = 1500};
            sc1.ExecuteNonQuery();
        }

        private void ProcessSmelt(SqlDataReader sdr, WorkShops WorkShop)
        {
            Decimal? OriginalSmeltID = (Decimal)sdr["OriginalSmeltID"];
            if (!OriginalSmeltID.HasValue) return;
            byte? WorkShopID = GetOrCreateWorkShopID(sdr["WorkShop"].ToString());
            if (!WorkShopID.HasValue) return;
            Technology_Smelts CurrentSmelt = new Technology_Smelts() { OriginalSmeltID = OriginalSmeltID.Value, WorkShopID = WorkShopID };
            Raport.Technology_Smelts.AddObject(CurrentSmelt);
            
            if (sdr["Mark"]!=DBNull.Value) CurrentSmelt.MarkID = GetOrCreateMarkID((string)sdr["Mark"]);
            if (sdr["RequestedMark"] != DBNull.Value) CurrentSmelt.RequestedMarkID = GetOrCreateMarkID((string)sdr["RequestedMark"]);
            if (sdr["Ntd"]!=DBNull.Value) CurrentSmelt.NtdID = GetOrCreateNtdID((string)sdr["Ntd"]);
            if (sdr["TechnologyNtd"]!=DBNull.Value) CurrentSmelt.TechnologyNtdID = GetOrCreateNtdID((string)sdr["TechnologyNtd"]);
            if (sdr["BottlingType"]!= DBNull.Value) CurrentSmelt.BottlingTypeID = GetOrCreateBottlingTypeID((string)sdr["BottlingType"]);
            if (sdr["RegisterDate"]!=DBNull.Value) CurrentSmelt.RegisterDate = (DateTime?)sdr["RegisterDate"];
            int ScoopN;
            if (int.TryParse(sdr["ScoopNumber"].ToString(), out ScoopN)) CurrentSmelt.ScoopNumber = ScoopN;
            if (sdr["SmeltNumber"]!=DBNull.Value) CurrentSmelt.SmeltNumber = (string)sdr["SmeltNumber"];
            SmeltToProcess.Add(new SmeltDefinition() { Smelt = CurrentSmelt, WorkShop = WorkShop });
        }

        private void ProcessSmeltGroups(Technology_Smelts CurrentSmelt, WorkShops WorkShop)
        {
            List<Technology_GetMethodsForGroups> Groups;
            if (GroupList==null) Groups = Raport.Technology_GetMethodsForGroups.ToList();
            else Groups = GroupList;

            foreach (var g in Groups)
            {
                int GroupID = GetOrCreateGroupID(g.Name);
                switch (WorkShop)
                {
                    case WorkShops.KKC:
                        if (g.GetMethodKKC != null && g.GetMethodKKC.Trim() != string.Empty) ProcessGroup(CurrentSmelt, g.GetMethodKKC, GroupID);
                        foreach (Technology_GetMethodsForParameters p in g.Technology_GetMethodsForParameters.Where(a =>
                            a.GetMethodKKC != null && a.GetMethodKKC.Trim() != string.Empty))
                        {
                            Say($"{CurrentSmelt} : {p.Name}");
                            ProcessParameter(CurrentSmelt, p.GetMethodKKC, p.Name, GroupID);
                        }
                        break;
                    case WorkShops.ESPC2:
                        if (g.GetMethodESPC2 != null && g.GetMethodESPC2.Trim() != string.Empty) ProcessGroup(CurrentSmelt, g.GetMethodESPC2, GroupID);
                        foreach (Technology_GetMethodsForParameters p in g.Technology_GetMethodsForParameters.Where(a =>
                            a.GetMethodESPC2 != null && a.GetMethodESPC2.Trim() != string.Empty))
                        {
                            Say($"{CurrentSmelt} : {p.Name}"); Say($"{CurrentSmelt} : {p.Name}");
                            ProcessParameter(CurrentSmelt, p.GetMethodESPC2, p.Name, GroupID);
                        }
                        break;
                    case WorkShops.ESPC6:
                        if (g.GetMethodESPC6 != null && g.GetMethodESPC6.Trim() != string.Empty) ProcessGroup(CurrentSmelt, g.GetMethodESPC6, GroupID);
                        foreach (Technology_GetMethodsForParameters p in g.Technology_GetMethodsForParameters.Where(a =>
                            a.GetMethodESPC6 != null && a.GetMethodESPC6.Trim() != string.Empty))
                        {
                            Say($"{CurrentSmelt} : {p.Name}");
                            ProcessParameter(CurrentSmelt, p.GetMethodESPC6, p.Name, GroupID);
                        }
                        break;
                    default:
                        break;
                }
            }
            if (GroupList == null)
            {
                Raport.SaveChanges();
                Raport.AcceptAllChanges();
            }
        }

        private void ProcessGroup(Technology_Smelts CurrentSmelt, string SQL, int GroupID)
        {
            bool? MultipleParametersInRow = null;
            SqlDataReader GroupReader = GetSmeltReader(SQL, CurrentSmelt.OriginalSmeltID);
            while (GroupReader.Read())
            {
                if (MultipleParametersInRow == null) MultipleParametersInRow = CheckMethod(GroupReader);
                if (MultipleParametersInRow.Value)
                {
                    byte PN;
                    byte? ProcessingNumber = null;
                    if (GroupReader[0] != DBNull.Value)
                    {
                        if (byte.TryParse(GroupReader[0].ToString(), out PN)) ProcessingNumber = PN;
                    }
                    int Start = 1;
                    string OptionalCategory = null;
                    if (GroupReader.GetName(1) == "OptionalCategory")
                    {
                        Start = 2;
                        OptionalCategory = (GroupReader[1] != DBNull.Value) ? GroupReader[1].ToString() : null;
                    }
                    for (int i = Start; i < GroupReader.FieldCount; i++)
                    {
                        if (GroupReader[i] != DBNull.Value)
                        {
                            bool IsNumber;
                            decimal Number;
                            IsNumber = decimal.TryParse(GroupReader[i].ToString(), out Number);
                            string ParameterName = GroupReader.GetName(i);
                            int ParameterID = GetOrCreateParameterID(ParameterName, GroupID, IsNumber);
                            Technology_Values CurrentValue = new Technology_Values() { SmeltID = CurrentSmelt.SmeltID, ParameterID = ParameterID, ProcessingNumber = ProcessingNumber, OptionalCategory = OptionalCategory };
                            CurrentSmelt.Technology_Values.Add(CurrentValue);
                            CurrentValue.Value = GroupReader[i].ToString();
                        }
                    }
                }
                else
                {
                    bool IsNumber;
                    decimal Number;
                    IsNumber = decimal.TryParse(GroupReader["Value"].ToString(), out Number);
                    if (GroupReader["ParameterName"] != DBNull.Value && GroupReader["Value"] != DBNull.Value)
                    {
                        string ParameterName = GroupReader["ParameterName"].ToString();
                        int ParameterID = GetOrCreateParameterID(ParameterName, GroupID, IsNumber);
                        byte? ProcessingNumber = (GroupReader["ProcessingNumber"] != DBNull.Value) ? ((byte?)GroupReader["ProcessingNumber"]) : null;
                        string OptionalCategory = (GroupReader["OptionalCategory"] != DBNull.Value) ? ((string)GroupReader["OptionalCategory"]) : null;  
                        Technology_Values CurrentValue = new Technology_Values() {SmeltID = CurrentSmelt.SmeltID, ParameterID = ParameterID, ProcessingNumber = ProcessingNumber, OptionalCategory = OptionalCategory};
                        CurrentSmelt.Technology_Values.Add(CurrentValue);
                        if (GroupReader["Value"] != DBNull.Value) CurrentValue.Value = GroupReader["Value"].ToString();
                        if (GroupReader["ValueGotAt"] != DBNull.Value) CurrentValue.ValueGotAt = (DateTime?)GroupReader["ValueGotAt"];
                    }
                }
            }
            GroupReader.Close();
        }

        private bool CheckMethod(SqlDataReader sdr)
        {
            return (!(sdr.FieldCount == 5 && sdr.GetName(0) == "ParameterName"));
        }

        private void ProcessParameter(Technology_Smelts CurrentSmelt, string SQL, string ParameterName, int GroupID)
        {
            SqlDataReader GroupReader = GetSmeltReader(SQL, CurrentSmelt.OriginalSmeltID);
            bool? IsNumber = null;
            decimal Number;
            
            while (GroupReader.Read())
            {
                Say($"{CurrentSmelt} : {ParameterName}");
                if (GroupReader["Value"] != DBNull.Value)
                {
                    if (!IsNumber.HasValue)IsNumber = decimal.TryParse(GroupReader["Value"].ToString(), out Number);
                    
                    int ParameterID = GetOrCreateParameterID(ParameterName, GroupID, IsNumber.Value);
                    byte? ProcessingNumber = (GroupReader["ProcessingNumber"] != DBNull.Value) ? ((byte?)GroupReader["ProcessingNumber"]) : null;
                    Technology_Values CurrentValue = Raport.Technology_Values.Where(a => a.SmeltID == CurrentSmelt.SmeltID && a.ParameterID == ParameterID && a.ProcessingNumber == ProcessingNumber).SingleOrDefault();
                    if (CurrentValue == null)
                    {
                        CurrentValue = new Technology_Values() { SmeltID = CurrentSmelt.SmeltID, ParameterID = ParameterID, ProcessingNumber = ProcessingNumber };
                        Raport.Technology_Values.AddObject(CurrentValue);
                    }
                    if (GroupReader["OptionalCategory"] != DBNull.Value) CurrentValue.OptionalCategory = (string)GroupReader["OptionalCategory"];
                    if (GroupReader["Value"] != DBNull.Value) CurrentValue.Value = GroupReader["Value"].ToString();
                    if (GroupReader["ValueGotAt"] != DBNull.Value) CurrentValue.ValueGotAt = (DateTime?)GroupReader["ValueGotAt"];
                }
            }
            GroupReader.Close();
        }

        #region Заполнение справочников

        private byte? GetOrCreateWorkShopID(string WorkShop)
        {
            if (WorkShop == null) return null;
            var W = Raport.Technology_Workshops.Where(a => a.Workshop == WorkShop).SingleOrDefault();
            if (W==null)
            {
                Technology_Workshops TW = new Technology_Workshops() { Workshop = WorkShop };
                Raport.Technology_Workshops.AddObject(TW);
                Raport.SaveChanges();
                Say("Новый цех: " + WorkShop);
                return TW.id;
            }
            return W.id;
        }

        private int? GetOrCreateMarkID(string Mark)
        {
            if (Mark == null) return null;
            var M = Raport.Common_Mark.Where(a => a.Mark == Mark).SingleOrDefault();
            if (M==null)
            {
                Common_Mark CM = new Common_Mark() { Mark = Mark, RequiresRole = "Новые марки" };
                Raport.Common_Mark.AddObject(CM);
                Raport.SaveChanges();
                Say("Новая марка: " + Mark);
                return CM.id;
            }
            return M.id;
        }

        private int? GetOrCreateNtdID(string Ntd)
        {
            if (Ntd == null) return null;
            var N = Raport.Common_NTD.Where(a => a.ntd == Ntd).SingleOrDefault();
            if (N==null)
            {
                Common_NTD CN = new Common_NTD() { ntd = Ntd};
                Raport.Common_NTD.AddObject(CN);
                Raport.SaveChanges();
                Say("Новый НТД: " + Ntd);
                return CN.id;
            }
            return N.id;
        }

        private byte? GetOrCreateBottlingTypeID(string BottlingType)
        {
            if (BottlingType == null) return null;
            var B = Raport.Technology_BottlingTypes.Where(a => a.BottlingType == BottlingType).SingleOrDefault();
            if (B == null)
            {
                Technology_BottlingTypes TB = new Technology_BottlingTypes() { BottlingType = BottlingType};
                Raport.Technology_BottlingTypes.AddObject(TB);
                Raport.SaveChanges();
                Say("Новый тип разливки: " + BottlingType);
                return TB.id;
            }
            return B.id;
        }

        private int GetOrCreateGroupID(string GroupName)
        {
            var P = Raport.Common_ParameterTypes.Where(a => a.Paramtype == GroupName).SingleOrDefault();
            if (P == null)
            {
                Common_ParameterTypes CP = new Common_ParameterTypes() { Paramtype = GroupName, Position=10000 };
                Raport.Common_ParameterTypes.AddObject(CP);
                Raport.SaveChanges();
                Say("Новая группа: " + GroupName);
                return CP.id;
            }
            return P.id;
        }

        private int GetOrCreateParameterID(string ParameterName, int GroupID, bool IsNumber)
        {
            var a = (from p in ParametersCache where p.ParameterName.ToLower().Trim() == ParameterName.ToLower().Trim() && p.GroupID == GroupID  select p).SingleOrDefault();
            
            if (a == null)
            {
                Common_Parameters CP = new Common_Parameters() { id_paramtype = GroupID, ParameterName = ParameterName, DisplayParameterName = ParameterName, IsNumber = IsNumber, Position = 10000 };
                Raport.Common_Parameters.AddObject(CP);
                Raport.SaveChanges();
                Say("Новый параметр: " + ParameterName);
                ParametersCacheItem pci = new ParametersCacheItem() { ParameterName = CP.ParameterName, GroupID = CP.id_paramtype.Value, ParameterID = CP.id, IsNumber = CP.IsNumber.Value };
                ParametersCache.Add(pci);
                return CP.id;
            }
            else return a.ParameterID;
        }
        #endregion

        private void Say(string Text)
        {
            if (NewMessage!=null)NewMessage.Invoke(this, new NewMessageEventArgs(){ Message=Text});
        }
    }

    public class NewMessageEventArgs : EventArgs
    {
        public string Message { get; set; }
    }

    public class ProcessMessageEventArgs : EventArgs
    {
        public int Number { get; set; }
    }

    public class ParametersCacheItem
    {
        public string ParameterName { get; set; }
        public int GroupID { get; set; }
        public bool IsNumber { get; set; }
        public int ParameterID { get; set; }
    }

    public class SmeltDefinition
    {
        public Technology_Smelts Smelt {get; set; }
        public WorkShops WorkShop { get; set; }
    }
    public enum WorkShops {KKC, ESPC2, ESPC6 };
}