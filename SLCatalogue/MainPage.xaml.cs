﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using ApplicationCore;
using AQCatalogueLibrary;

namespace SLCatalogue
{
    public partial class MainPage : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private Rect _selectedItemRectangle;
        private double _oldScrollHorizontalOffset;
        private double _oldScrollVerticalOffset;
        private DoubleAnimation _popupHorizontalOffsetAnimation;
        private DoubleAnimation _popupVerticalOffsetAnimation;
        private DoubleAnimation _connectorEndXAnimation;
        private DoubleAnimation _connectorEndYAnimation;
        private Storyboard _popupStoryBoard;
        private bool _popupBlocked;
        private bool _isInitialized = false;
        private readonly AQModuleDescription _aqmd;

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name }; 
            CatalogueOperations.CreateDomainContext();
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var sv = Parent as ScrollViewer;
            if (sv != null)
            {
                Height = sv.ViewportHeight - 10;
                Width = sv.ViewportWidth - 10; 
                Observable.FromEvent<SizeChangedEventArgs>(sv, "SizeChanged").Throttle(new TimeSpan(1000000)).Subscribe(ev => Dispatcher.BeginInvoke(() =>
                {
                    Height = sv.ViewportHeight;
                    Width = sv.ViewportWidth;
                }));
            }
            InitAnimation();
        }

        private void InitAnimation()
        {
            UpdateLayout();
            _popupStoryBoard = new Storyboard();
            _popupHorizontalOffsetAnimation = new DoubleAnimation {SpeedRatio = 5};
            _popupVerticalOffsetAnimation = new DoubleAnimation {SpeedRatio = 5};
            _connectorEndXAnimation = new DoubleAnimation {SpeedRatio = 5};
            _connectorEndYAnimation = new DoubleAnimation {SpeedRatio = 5};
            _popupStoryBoard.Children.Add(_popupHorizontalOffsetAnimation);
            _popupStoryBoard.Children.Add(_popupVerticalOffsetAnimation);
            _popupStoryBoard.Children.Add(_connectorEndXAnimation);
            _popupStoryBoard.Children.Add(_connectorEndYAnimation);

            Storyboard.SetTarget(_popupVerticalOffsetAnimation, ItemProcessorPopup);
            Storyboard.SetTarget(_popupHorizontalOffsetAnimation, ItemProcessorPopup);
            Storyboard.SetTargetProperty(_popupVerticalOffsetAnimation, new PropertyPath("VerticalOffset"));
            Storyboard.SetTargetProperty(_popupHorizontalOffsetAnimation, new PropertyPath("HorizontalOffset"));

            Storyboard.SetTarget(_connectorEndXAnimation, Connector);
            Storyboard.SetTarget(_connectorEndYAnimation, Connector);
            Storyboard.SetTargetProperty(_connectorEndXAnimation, new PropertyPath("X2"));
            Storyboard.SetTargetProperty(_connectorEndYAnimation, new PropertyPath("Y2"));
        }

        private void MainCatalogue_ItemSelected(object o, ItemOperationEventArgs e)
        {
            var isAnimationRequired = ItemProcessorPopup.IsOpen;
            ItemProcessorPopup.IsOpen = !_popupBlocked;
            Connector.Visibility = _popupBlocked? Visibility.Collapsed : Visibility.Visible;
            _selectedItemRectangle = e.ItemRectangle;
            ItemProcessorPlaceHolder.Children.Clear();
            var processor = CatalogueHelper.GetItemProcessorByType(e.ItemType);
            processor.InitializeProcessor(e.ItemID);
            processor.CommandEmitted += Processor_CommandEmitted;
            ItemProcessorPlaceHolder.Children.Add(processor as UserControl);
            RefreshItemProcessor(isAnimationRequired);
        }

        void Processor_CommandEmitted(object o, CommandEventArgs e)
        {
            ICommandAcceptor ica = CatalogueHelper.GetCommandAcceptorByType(e.Command);
            if (ica.PopupMustBeClosed)
            {
                ItemProcessorPopup.IsOpen = false;
                _popupBlocked = true;
            }
            var reopen = ica.PopupCanBeReopened;
            ica.ExecuteCommand(TaskPanel, UserControlPlaceHolder, e, this, () => 
            {
                if (_popupBlocked)
                {
                    _popupBlocked = false;
                    ItemProcessorPopup.IsOpen = reopen;
                }
            });
        }

        private void MainCatalogue_FolderSelected(object o, ItemOperationEventArgs e)
        {
            ItemProcessorPopup.IsOpen = false;
            Connector.Visibility = Visibility.Collapsed;
        }

        private void RefreshItemProcessor(bool doAnimation)
        {
            if (Math.Abs(_selectedItemRectangle.Height) < 1e-7)
            {
                Connector.Visibility = Visibility.Collapsed;
                ItemProcessorPopup.IsOpen = false;
            }
            else
            {
                SetupAnimation(doAnimation);
            }
        }

        private void SetupAnimation(bool doAnimation)
        {
            ItemProcessor.UpdateLayout();
            Connector.X1 = _selectedItemRectangle.Right + 20 - InternalScrollViewer.HorizontalOffset;
            Connector.Y1 = _selectedItemRectangle.Top + 5 + _selectedItemRectangle.Height / 2 - InternalScrollViewer.VerticalOffset;
            
            var destinationX = _selectedItemRectangle.Right - InternalScrollViewer.HorizontalOffset + 100;
            _popupHorizontalOffsetAnimation.To = destinationX;
            _connectorEndXAnimation.To = destinationX;

            var destinationY = _selectedItemRectangle.Top - InternalScrollViewer.VerticalOffset + _selectedItemRectangle.Height / 2 - 80;
            if (destinationY < 0) destinationY = 0;
            if (destinationY > InternalScrollViewer.ViewportHeight - ItemProcessor.ActualHeight) destinationY = InternalScrollViewer.ViewportHeight - ItemProcessor.ActualHeight;
            
            _popupVerticalOffsetAnimation.To = destinationY;
            _connectorEndYAnimation.To = destinationY + ItemProcessor.ActualHeight / 2;

            if (!doAnimation)
            {
                _popupHorizontalOffsetAnimation.From = destinationX;
                _popupVerticalOffsetAnimation.From = destinationY;
                _connectorEndXAnimation.From = destinationX;
                _connectorEndYAnimation.From = destinationY + ItemProcessor.ActualHeight / 2;
            }
            else
            {
                _popupHorizontalOffsetAnimation.From = null;
                _popupVerticalOffsetAnimation.From = null;
                _connectorEndXAnimation.From = null;
                _connectorEndYAnimation.From = null;
            }
            _popupStoryBoard.Begin();
        }

        private void MainCatalogue_SelectedItemGeometryPossibleChanged(object o, ItemOperationEventArgs e)
        {
            _selectedItemRectangle = e.ItemRectangle;
            RefreshItemProcessor(true);
        }

        private void InternalScrollViewer_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateLayout();
            if (Math.Abs(InternalScrollViewer.HorizontalOffset - _oldScrollHorizontalOffset) > 1e-8 || Math.Abs(InternalScrollViewer.VerticalOffset - _oldScrollVerticalOffset) > 1e-8)
            {
                SetScrollOffset();
            }
        }

        private void SetScrollOffset()
        {
            _oldScrollHorizontalOffset = InternalScrollViewer.HorizontalOffset;
            _oldScrollVerticalOffset = InternalScrollViewer.VerticalOffset;
            RefreshItemProcessor(true);
        }

        private void InternalScrollViewer_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            InternalScrollViewer.UpdateLayout();
            var dt = new DispatcherTimer { Interval = new TimeSpan(100000) };
            dt.Tick += (o, ev) => 
            { 
                SetScrollOffset(); 
                dt.Stop(); 
            };
            dt.Start();
        }

        private void ItemProcessorPopup_Closed(object sender, EventArgs e)
        {
            Connector.Visibility = Visibility.Collapsed;
        }

        private void Catalogue_Loaded(object sender, EventArgs e)
        {
            if (!_isInitialized) MainCatalogue.RenewData();
            _isInitialized = true;
        }

        private void ItemProcessorPopup_Opened(object sender, EventArgs e)
        {
            Connector.Visibility = Visibility.Visible;
        }

        private void Order_Click(object sender, EventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb == null) return;
            MainCatalogue.PreselectRequired = true;
            MainCatalogue.ItemOrder = rb.Name;
            ItemProcessorPopup.IsOpen = false;
            Connector.Visibility = Visibility.Collapsed;
            RefreshItemProcessor(true);
        }

        private void Search_Click(object sender, EventArgs e)
        {
            var n = MainCatalogue.Search(Search.Text);
            if (n == 0)
            {
                Found.Text = $"Нет совпадений";
                return;
            }
            if (n>4 && n<20)
            {
                Found.Text = $"Найдено {n} записей";
                return;
            }
            var last = n % 10;
            switch (n)
            {
                case 1:
                    Found.Text = $"Найдена {n} запись";
                    break;
                case 2:
                case 3:
                case 4:
                    Found.Text = $"Найдены {n} записи";
                    break;
                default:
                    Found.Text = $"Найдено {n} записей";
                    break;
            }
            Found.Visibility = Visibility.Visible;
        }

        private void ResetSearch_Click(object sender, EventArgs e)
        {
            MainCatalogue.ResetSearch();
            Search.Text = string.Empty;
            Found.Text = string.Empty;
            Found.Visibility = Visibility.Collapsed;
        }

        private void TaskPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RefreshItemProcessor(true);
        }
    }
}