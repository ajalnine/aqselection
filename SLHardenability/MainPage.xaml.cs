﻿//Вставить обновление пределов после редактирования исходного ХС
//Изменить целевую функцию

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using System.Windows.Threading;
using AQChartLibrary;
using AQControlsLibrary;
using ApplicationCore;
using AQCalculationsLibrary;
using AQMathClasses;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLHardenability
{
    public partial class MainPage : UserControl, IAQModule
    {
        CommandCallBackDelegate ReadySignalCallBack;
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private List<SLDataTable> Results;
        private BackgroundWorker CalculationWorker;
        private BackgroundWorker OptimizationWorker;
        private DispatcherTimer dt;
        private double[] MetricPointList = new double[]{1, 1.5, 3, 4.5, 5, 6, 7, 7.5, 9, 10, 10.5, 11, 12, 13, 13.5, 15, 18, 20, 21, 24, 25, 27, 30, 33, 35, 39, 40, 45, 50, 51};
        private double[] InchesPointList = new double[]{1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20, 22, 24, 28, 32};
        private bool ModeIsNonBoronFlag = true;
        private bool ModeIsMetricFlag = true;
        private bool ModeIsOptimization = false;
        private ReportingDuplexServiceClient rdsc;
        private Guid CurrentOperation = new Guid();
        private string CurrentDimension = string.Empty;
        private SLDataTable TableForChart = new SLDataTable();
        private SLDataTable HardnessTable = new SLDataTable();
        private bool ProcessingEnabled = true;
        private readonly AQModuleDescription _aqmd;

        private delegate Double? HardenabilityCalculator(Double? Point, Double? C, Double? DI); 
        public InteractiveParameters p
        {
            get { return (InteractiveParameters)GetValue(pProperty); }
            set { SetValue(pProperty, value); }
        }

        public static readonly DependencyProperty pProperty =
            DependencyProperty.Register("p", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        public InteractiveParameters optimal
        {
            get { return (InteractiveParameters)GetValue(optimalProperty); }
            set { SetValue(optimalProperty, value); }
        }

        public static readonly DependencyProperty optimalProperty =
            DependencyProperty.Register("optimal", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        public InteractiveParameters constraints
        {
            get { return (InteractiveParameters)GetValue(ConstraintsProperty); }
            set { SetValue(ConstraintsProperty, value); }
        }

        public static readonly DependencyProperty ConstraintsProperty =
            DependencyProperty.Register("constraints", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));
        
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            p = InteractiveParameters.GetClone(this.Resources["ChemistryParameters"] as InteractiveParameters);
            optimal = InteractiveParameters.GetClone(this.Resources["OptimalChemistryParameters"] as InteractiveParameters);
            constraints = new InteractiveParameters();
            RefreshFlags();
            InitCalculationWorker();
            InitOptimizationWorker();

            foreach (var a in p.Children.Union(optimal.Children))
            {
                a.PropertyChanged += new PropertyChangedEventHandler(ParametersChanged);
            }

            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(100);
            dt.Tick += (o, e) =>
            {
                if (!CalculationWorker.IsBusy)
                {
                    CalculationWorker.RunWorkerAsync(new object[]{p, optimal, constraints});
                    dt.Stop();
                }
            };
            DoCalculation();
        }

        void ParametersChanged(object sender, PropertyChangedEventArgs e)
        {
            OptimizationResult.Text = String.Empty;
            if (e.PropertyName.Contains("Value") && ProcessingEnabled)
            {
                RefreshFlags();
                DoCalculation();
            }
        }

        private void DoCalculation()
        {
            if (CalculationWorker == null) return;
            if (CalculationWorker.IsBusy)
            {
                CalculationWorker.CancelAsync();
                dt.Start();
            }
            else CalculationWorker.RunWorkerAsync(new object[] { p, optimal, constraints });
        }

        private void InitCalculationWorker()
        {
            CalculationWorker = new BackgroundWorker();

            CalculationWorker.DoWork += (o, e) =>
            {
                    ExportToXlsxEx.Dispatcher.BeginInvoke(delegate() { ExportToXlsxEx.IsEnabled = false; RefreshFlags(); });
                    InteractiveParameters p = (e.Argument as object[])[0] as InteractiveParameters;
                    InteractiveParameters optimal = (e.Argument as object[])[1] as InteractiveParameters;
                    InteractiveParameters constraints = (e.Argument as object[])[2] as InteractiveParameters;

                    HardenabilityCalculator hc = (ModeIsNonBoronFlag) ?
                        ((ModeIsMetricFlag) ? new HardenabilityCalculator(AQMath.GetHardenabilityNonBoronMetricWithGrain) :
                                              new HardenabilityCalculator(AQMath.GetHardenabilityNonBoronInchesWithGrain)) :
                        ((ModeIsMetricFlag) ? new HardenabilityCalculator(AQMath.GetHardenabilityBoronMetricWithGrain) :
                                              new HardenabilityCalculator(AQMath.GetHardenabilityBoronInchesWithGrain));

                    double[] Points = (ModeIsMetricFlag) ? MetricPointList : InchesPointList;

                    double? DIMin = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(p.GetValue("C"), p.GetValue("Mn"), p.GetValue("Si"), p.GetValue("Ni"), p.GetValue("Cr"), p.GetValue("Mo"), p.GetValue("Cu"), p.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно")) :
                                                         AQMath.HGetDIBoron(p.GetValue("C"), p.GetValue("Mn"), p.GetValue("Si"), p.GetValue("Ni"), p.GetValue("Cr"), p.GetValue("Mo"), p.GetValue("Cu"), p.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно"));
                    double? DIMax = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(p.GetValue2("C"), p.GetValue2("Mn"), p.GetValue2("Si"), p.GetValue2("Ni"), p.GetValue2("Cr"), p.GetValue2("Mo"), p.GetValue2("Cu"), p.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно")) :
                                                         AQMath.HGetDIBoron(p.GetValue2("C"), p.GetValue2("Mn"), p.GetValue2("Si"), p.GetValue2("Ni"), p.GetValue2("Cr"), p.GetValue2("Mo"), p.GetValue2("Cu"), p.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно"));

                    PrepareTables(p, Points, ModeIsOptimization);
                    
                if (!DIMin.HasValue || double.IsNaN(DIMin.Value) || !DIMax.HasValue || double.IsNaN(DIMax.Value))
                    {
                        this.Dispatcher.BeginInvoke(delegate() { Comment.Text = "Ошибка вычисления DI"; HardnessChart.Visibility = System.Windows.Visibility.Collapsed; });
                        return;
                    }
                    if (DIMin.Value > 177.5 || DIMax.Value > 177.5)
                    {
                        this.Dispatcher.BeginInvoke(delegate() { Comment.Text = "DI больше допустимого значения 177.5 мм"; HardnessChart.Visibility = System.Windows.Visibility.Collapsed; });
                        return;
                    }
                    this.Dispatcher.BeginInvoke(delegate() { Comment.Text = "DI= " + Math.Round(DIMin.Value,2) + ".." + Math.Round(DIMax.Value,2);});

                    double? DIOptMin = null, DIOptMax = null;

                    #region Расчет для суженных пределов
                    if (ModeIsOptimization)
                    {
                        DIOptMin = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(optimal.GetValue("C"), optimal.GetValue("Mn"), optimal.GetValue("Si"), optimal.GetValue("Ni"), optimal.GetValue("Cr"), optimal.GetValue("Mo"), optimal.GetValue("Cu"), optimal.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно")) :
                                                             AQMath.HGetDIBoron(optimal.GetValue("C"), optimal.GetValue("Mn"), optimal.GetValue("Si"), optimal.GetValue("Ni"), optimal.GetValue("Cr"), optimal.GetValue("Mo"), optimal.GetValue("Cu"), optimal.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно"));
                        DIOptMax = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(optimal.GetValue2("C"), optimal.GetValue2("Mn"), optimal.GetValue2("Si"), optimal.GetValue2("Ni"), optimal.GetValue2("Cr"), optimal.GetValue2("Mo"), optimal.GetValue2("Cu"), optimal.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно")) :
                                                             AQMath.HGetDIBoron(optimal.GetValue2("C"), optimal.GetValue2("Mn"), optimal.GetValue2("Si"), optimal.GetValue2("Ni"), optimal.GetValue2("Cr"), optimal.GetValue2("Mo"), optimal.GetValue2("Cu"), optimal.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно"));

                        if (!DIOptMin.HasValue || double.IsNaN(DIOptMin.Value) || !DIOptMax.HasValue || double.IsNaN(DIOptMax.Value))
                        {
                            this.Dispatcher.BeginInvoke(delegate() { Comment2.Text = "Ошибка вычисления DI"; HardnessChart.Visibility = System.Windows.Visibility.Collapsed; });
                            return;
                        }
                        if (DIOptMin.Value > 177.5 || DIOptMax.Value > 177.5)
                        {
                            this.Dispatcher.BeginInvoke(delegate() { Comment2.Text = "DI больше допустимого значения 177.5 мм"; HardnessChart.Visibility = System.Windows.Visibility.Collapsed; });
                            return;
                        }
                        this.Dispatcher.BeginInvoke(delegate() { Comment2.Text = "DIсуж.= " + Math.Round(DIOptMin.Value,2) + ".." + Math.Round(DIOptMax.Value,2); });
                         
                    }
                    #endregion

                    SLDataRow Min = GetNewRow("Исходный min");
                    SLDataRow OptMin = GetNewRow("Расчет min");
                    SLDataRow Max = GetNewRow("Исходный max");
                    SLDataRow OptMax = GetNewRow("Расчет max");

                    
                    foreach (double i in Points)
                    {
                        SLDataRow Point = GetNewRow(i);
                        CalculatePoint(p.GetValue("C"), hc, DIMin.Value, Min, i, Point);
                        var CurrentConstraint = constraints.Children.Where(a => a.Caption == i.ToString()).SingleOrDefault();
                        /// Здесь должна быть привязка параметра к таблице
                        double? MinPointConstraint = (CurrentConstraint!=null ? (double?)CurrentConstraint.CurrentValue: null);
                        double? MaxPointConstraint = (CurrentConstraint != null ? (double?)CurrentConstraint.CurrentValue2 : null);
                        if (ModeIsOptimization)
                        {
                            Point.Row.Add(CurrentConstraint); 
                            CalculatePoint(optimal.GetValue("C"), hc, DIOptMin.Value, OptMin, i, Point);
                        }
                        CalculatePoint(p.GetValue2("C"), hc, DIMax.Value, Max, i, Point);
                        if (ModeIsOptimization)
                        {
                            Point.Row.Add(CurrentConstraint);
                            CalculatePoint(optimal.GetValue2("C"), hc, DIOptMax.Value, OptMax, i, Point);
                        }
                        TableForChart.Table.Add(Point);
                        if (CheckCancelCalculation(o, e)) return;
                    }

                    FinalizeTables(p, optimal, Min, OptMin, Max, OptMax);
            };

            CalculationWorker.RunWorkerCompleted += (o, e) =>
            {
                this.Dispatcher.BeginInvoke(delegate()
                {
                    ExportToXlsxEx.IsEnabled = true;
                    HardnessChart.ChartData = CreateChartDescription(ModeIsOptimization);
                    HardnessChart.DataTables = Results;
                    try { HardnessChart.MakeChart(); }
                    catch { }
                });
            };

            CalculationWorker.WorkerSupportsCancellation = true;
        }

        private void FinalizeTables(InteractiveParameters p, InteractiveParameters optimal, SLDataRow Min, SLDataRow OptMin, SLDataRow Max, SLDataRow OptMax)
        {
            foreach (InputParameter a in p.Children)
            {
                Max.Row.Add(a.CurrentValue2);
                Min.Row.Add(a.CurrentValue);
            }
            if (ModeIsOptimization)
            {
                foreach (InputParameter a in optimal.Children)
                {
                    OptMax.Row.Add(a.CurrentValue2);
                    OptMin.Row.Add(a.CurrentValue);
                }
                OptMax.Row.Add(null);
                OptMin.Row.Add(null);
            }
            HardnessTable.Table.Add(Min);
            if (ModeIsOptimization)
            {
                HardnessTable.Table.Add(OptMin);
            }
            HardnessTable.Table.Add(Max);
            if (ModeIsOptimization)
            {
                HardnessTable.Table.Add(OptMax);
            }
        }

        private static SLDataRow GetNewRow(object Data)
        {
            SLDataRow TableRow;
            TableRow = new SLDataRow();
            TableRow.Row = new List<object>();
            TableRow.Row.Add(Data);
            return TableRow;
        }

        private static void CalculatePoint(double C, HardenabilityCalculator hc, double DI, SLDataRow Row, double i, SLDataRow Point)
        {
            Double? value = hc(i, C, DI);
            if (value.HasValue && !Double.IsNaN(value.Value))
            {
                double val = Math.Round(value.Value, 1);
                Row.Row.Add(val);
                Point.Row.Add(val);
            }
            else
            {
                Row.Row.Add(null);
                Point.Row.Add(null);
            }
        }

        private void PrepareTables(InteractiveParameters p, double[] Points, bool IsDetailed)
        {
            Results = new List<SLDataTable>();
            TableForChart = new SLDataTable();
            HardnessTable = new SLDataTable();
            Results.Add(HardnessTable);
            Results.Add(TableForChart);

            string Suffix = ((ModeIsMetricFlag) ? "мм, " : "1⁄16\", ") + ((ModeIsNonBoronFlag) ? "не" : String.Empty) + "бор.";
            CurrentDimension = Suffix;
            HardnessTable.TableName = "Прокаливаемость " + Suffix;
            TableForChart.TableName = "Полоса прокаливаемости " + Suffix;

            HardnessTable.DataTypes = new List<string>();
            HardnessTable.ColumnNames = new List<string>();
            TableForChart.DataTypes = new List<string>();
            TableForChart.ColumnNames = new List<string>();

            this.Dispatcher.BeginInvoke(delegate() { Comment.Text = String.Empty; Comment2.Text = String.Empty; HardnessChart.Visibility = System.Windows.Visibility.Visible; });
            TableForChart.ColumnNames.Add("Точка");
            TableForChart.DataTypes.Add("Double");
            TableForChart.ColumnNames.Add("Твердость Min");
            TableForChart.DataTypes.Add("Double");
            if (IsDetailed)
            {
                TableForChart.ColumnNames.Add("Требование, Min");
                TableForChart.DataTypes.Add("InputParameter");
                TableForChart.ColumnNames.Add("Расчет, Min");
                TableForChart.DataTypes.Add("Double");
            }
            TableForChart.ColumnNames.Add("Твердость Max");
            TableForChart.DataTypes.Add("Double");
            if (IsDetailed)
            {
                TableForChart.ColumnNames.Add("Требование, Max");
                TableForChart.DataTypes.Add("InputParameter");
                TableForChart.ColumnNames.Add("Расчет, Max");
                TableForChart.DataTypes.Add("Double");
            }
            HardnessTable.ColumnNames.Add("Хим. состав");
            HardnessTable.DataTypes.Add("String");

            foreach (double i in Points)
            {
                HardnessTable.ColumnNames.Add(i.ToString());
                HardnessTable.DataTypes.Add("Double");
            }

            foreach (InputParameter a in p.Children)
            {
                HardnessTable.ColumnNames.Add(a.Caption);
                HardnessTable.DataTypes.Add("Double");
            }
            HardnessTable.Table = new List<SLDataRow>();
            TableForChart.Table = new List<SLDataRow>();
           
        }

        private ChartDescription CreateChartDescription(bool Detailed)
        {
            ChartDescription cd = new ChartDescription();
            cd.ChartTitle = "Полоса прокаливаемости " + CurrentDimension;
            cd.ChartSeries = new List<ChartSeriesDescription>();
            
            ChartSeriesDescription csdMin = new ChartSeriesDescription();
            csdMin.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
            csdMin.XColumnName = "Точка";
            csdMin.XIsNominal = true;
            csdMin.XAxisTitle = "Точка";
            csdMin.YColumnName = "Твердость Min";
            csdMin.YAxisTitle = "Твердость";
            csdMin.SeriesTitle = csdMin.YColumnName;
            csdMin.SeriesType = ChartSeriesType.LineXY;
            csdMin.LineColor = "#FF4A7EBB";
            csdMin.LineSize = 2;
            cd.ChartSeries.Add(csdMin);
            
            if (Detailed)
            {
                ChartSeriesDescription csdLimitMin = new ChartSeriesDescription();
                csdLimitMin.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
                csdLimitMin.XColumnName = "Точка";
                csdLimitMin.XIsNominal = true;
                csdLimitMin.XAxisTitle = "Точка";
                csdLimitMin.YColumnName = "Требование, Min";
                csdLimitMin.YAxisTitle = "Твердость";
                csdLimitMin.SeriesTitle = csdLimitMin.YColumnName;
                csdLimitMin.SeriesType = ChartSeriesType.EditableTableOnly;
                csdLimitMin.LineColor = "#ffa00000";
                csdLimitMin.LineSize = 2;
                csdLimitMin.Tag = "CurrentValue";
                
                ChartSeriesDescription csdOptimalMin = new ChartSeriesDescription();
                csdOptimalMin.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
                csdOptimalMin.XColumnName = "Точка";
                csdOptimalMin.XIsNominal = true;
                csdOptimalMin.XAxisTitle = "Точка";
                csdOptimalMin.YColumnName = "Расчет, Min";
                csdOptimalMin.YAxisTitle = "Твердость";
                csdOptimalMin.SeriesTitle = csdOptimalMin.YColumnName;
                csdOptimalMin.SeriesType = ChartSeriesType.LineXY;
                csdOptimalMin.LineColor = "#FF0A0E9B";
                csdOptimalMin.LineSize = 2;
                cd.ChartSeries.Add(csdLimitMin);
                cd.ChartSeries.Add(csdOptimalMin);
            }

            ChartSeriesDescription csdMax = new ChartSeriesDescription();
            csdMax.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
            csdMax.XColumnName = "Точка";
            csdMax.XIsNominal = true;
            csdMax.XAxisTitle = "Точка";
            csdMax.YAxisTitle = "Твердость";
            csdMax.YColumnName = "Твердость Max";
            csdMax.SeriesTitle = csdMax.YColumnName;
            csdMax.SeriesType = ChartSeriesType.LineXY;
            csdMax.LineColor = "#FF98B954";
            csdMax.LineSize = 2;
            cd.ChartSeries.Add(csdMax);

            if (Detailed)
            {
                ChartSeriesDescription csdLimitMax = new ChartSeriesDescription();
                csdLimitMax.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
                csdLimitMax.XColumnName = "Точка";
                csdLimitMax.XIsNominal = true;
                csdLimitMax.XAxisTitle = "Точка";
                csdLimitMax.YAxisTitle = "Твердость";
                csdLimitMax.YColumnName = "Требование, Max";
                csdLimitMax.SeriesTitle = csdLimitMax.YColumnName;
                csdLimitMax.SeriesType = ChartSeriesType.EditableTableOnly;
                csdLimitMax.LineColor = "#ffa00000";
                csdLimitMax.LineSize = 2;
                csdLimitMax.Tag = "CurrentValue2";

                ChartSeriesDescription csdOptimalMax = new ChartSeriesDescription();
                csdOptimalMax.SourceTableName = "Полоса прокаливаемости " + CurrentDimension;
                csdOptimalMax.XColumnName = "Точка";
                csdOptimalMax.XIsNominal = true;
                csdOptimalMax.XAxisTitle = "Точка";
                csdOptimalMax.YAxisTitle = "Твердость";
                csdOptimalMax.YColumnName = "Расчет, Max";
                csdOptimalMax.SeriesTitle = csdOptimalMax.YColumnName;
                csdOptimalMax.SeriesType = ChartSeriesType.LineXY;
                csdOptimalMax.LineColor = "#FF586904";
                csdOptimalMax.LineSize = 2;
                cd.ChartSeries.Add(csdLimitMax);
                cd.ChartSeries.Add(csdOptimalMax);
            }

            return cd;
        }

        private bool CheckCancelCalculation(object o, DoWorkEventArgs e)
        {
            if (((BackgroundWorker)o).CancellationPending)
            {
                e.Cancel = true;
                e.Result = null;
                return true;
            }
            return false;
        }

        private void ExportToXlsxEx_Click(object sender, RoutedEventArgs e)
        {
            Task task = new Task(new string[] { "Формирование результата", "Формирование файла Xlsx" }, "Экспорт в Xlsx", TaskPanel);
            task.StartTask();

            Guid Task1Guid = Guid.NewGuid();
            Guid Task2Guid = Guid.NewGuid();
            rdsc = Services.GetReportingService();
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived").Where((o1, e1) => o1.EventArgs.OperationGuid == Task1Guid).Subscribe(x =>
            {
                OperationResult sr = x.EventArgs.or;
                if (sr.Success)
                {
                    task.AdvanceProgress();
                    CurrentOperation = Task2Guid;
                    rdsc.GetAllResultsInXlsxAsync(Task2Guid, (string)sr.ResultData);
                }
                else
                {
                    task.StopTask(sr.Message);
                    if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                }
            });

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived").Where((o1, e1) => o1.EventArgs.OperationGuid == Task2Guid).Subscribe(x =>
            {
                OperationResult sr = x.EventArgs.or;
                if (sr.Success)
                {
                    task.AdvanceProgress();
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.ResultData.ToString()) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                    });
                }
                else
                {
                    task.StopTask(sr.Message);
                    if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                }
            });

            List<ReportDataTable> r = new List<ReportDataTable>();
            foreach (var a in Results)
            {
                ReportDataTable rdt = new ReportDataTable();
                rdt.ColumnNames = a.ColumnNames;
                rdt.DataTypes = new List<string>();
                foreach (var d in a.DataTypes)
                {
                    if (d != "InputParameter") rdt.DataTypes.Add(d);
                    else rdt.DataTypes.Add("Double");
                }
                rdt.SelectionString = a.SelectionString;
                rdt.Table = new List<ReportDataRow>();
                foreach (var dr in a.Table)
                {
                    ReportDataRow rdr = new ReportDataRow();
                    rdr.Row = new List<object>();
                    for (int index = 0; index<dr.Row.Count; index++)
                    {
                        object b = dr.Row[index];
                        if (b!=null && b.GetType().ToString() == "AQCalculationsLibrary.InputParameter") 
                        {
                            InputParameter i = ((InputParameter)b);
                            string name = rdt.ColumnNames[index];
                            bool IsMax = name.Contains("Max");
                            rdr.Row.Add((IsMax)?i.CurrentValue2:i.CurrentValue);
                        }
                        else rdr.Row.Add(b);
                    }
                    rdt.Table.Add(rdr);
                }
                rdt.TableName = a.TableName;
                r.Add(rdt);
            }
            CurrentOperation = Task1Guid;
            rdsc.FillDataWithSLTablesAsync(Task1Guid, r);

        }

        #region Обработка сигналов

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription SenderDescription, Command CommandType, object CommandArgument, CommandCallBackDelegate CommandCallBack)
        {
            if (CommandCallBack != null) CommandCallBack.Invoke(CommandType, CommandArgument, null);
        }

        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }

        #endregion

        #region Обработка смены режима

        private void ModeIsChanged(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            if (HardnessChart != null)
            {
                HardnessChart.ClearSelection();
                constraints.Children.Clear();
            }
            DoCalculation();
        }

        private void RefreshFlags()
        {
            if (ModeIsMetric == null || ModeIsNonBoron == null) return;
            ModeIsMetricFlag = ModeIsMetric.IsChecked.Value;
            ModeIsNonBoronFlag = ModeIsNonBoron.IsChecked.Value;
        }

        private void ModePanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HardnessChart != null)
            {
                ModeIsOptimization = ModePanel.SelectedIndex == 1;
                HardnessChart.Selectable = ModeIsOptimization;
                HardnessChart.GetCustomSurface().Visibility = ModeIsOptimization ? Visibility.Visible : Visibility.Collapsed;
                OptimizationsPanel.Visibility = ModeIsOptimization ? Visibility.Visible : Visibility.Collapsed;
                if (ModeIsOptimization) 
                {
                    RefreshOptimalChemistryStartValues();
                    RefreshConstraintRanges();
                }
                if (ModePanel.SelectedIndex < 2) DoCalculation();
            }
        }

        private void RefreshOptimalChemistryStartValues()
        {
            ProcessingEnabled = false;
            var Source = p.GroupedParameters.Where(a => a.GroupName == "Химический состав").Single().ParametersInGroup;
            var Optimal = optimal.GroupedParameters.Where(a => a.GroupName == "Суженный химический состав").Single().ParametersInGroup;
            foreach (InputParameter s in Source)
            {
                InputParameter o = Optimal.Where(a => a.Caption == s.Caption).Single();
                o.MinValue = s.CurrentValue;
                o.MaxValue = s.CurrentValue2;
                if (o.MinValue == o.MaxValue)
                {
                    o.Unlocked = false;
                    o.Unlocked2 = false;
                }
                o.MinAllowedValue = s.MinAllowedValue;
                o.MaxAllowedValue = s.MaxAllowedValue;
                o.CurrentValue = Math.Min(Math.Max(o.CurrentValue, o.MinValue), o.MaxValue);
                o.CurrentValue2 = Math.Max(Math.Min(o.CurrentValue2, o.MaxValue), o.MinValue);
            }
            ProcessingEnabled = true;
        }

        private void HardnessChart_AxisValueSelectionChanged(object o, AxisValueSelectionEventArgs e)
        {
            if (e.IsChecked)
            {
                InputParameter ip = new InputParameter();
                ip.MinValue = (double)HardnessChart.LeftAxisRange.DisplayMinimum;
                ip.MaxValue = (double)HardnessChart.LeftAxisRange.DisplayMaximum;
                ip.Step = 0.5;
                ip.CurrentValue = (double)HardnessChart.LeftAxisRange.BoundSeries.Where(a => a.SeriesName == "Твердость Min").Single().SeriesData[e.SelectedIndex];
                ip.CurrentValue2 = (double)HardnessChart.LeftAxisRange.BoundSeries.Where(a => a.SeriesName == "Твердость Max").Single().SeriesData[e.SelectedIndex];
                ip.MinAllowedValue = ip.CurrentValue;
                ip.MaxAllowedValue = ip.CurrentValue2;
                ip.Caption = e.SelectedLabel;
                ip.ControlOffset = e.SelectedLabelOffset - 15;
                ip.Unlocked = true;
                ip.Unlocked2 = true;
                ip.StrongCheckIsEnabled = false;
                ip.PropertyChanged += new PropertyChangedEventHandler(Constraints_PropertyChanged);
                constraints.Children.Add(ip);
            }
            else
            {
                var a = (from c in constraints.Children where c.Caption==e.SelectedLabel select c).SingleOrDefault();
                if (a!=null) constraints.Children.Remove(a);
            }
            OptimizationResult.Text = String.Empty;
            DoCalculation();
        }

        private void RefreshConstraintRanges()
        {
            if (constraints == null || constraints.Children.Count == 0) return;
            foreach (var c in constraints.Children)
            {
                c.MaxValue = (double)HardnessChart.LeftAxisRange.DisplayMaximum;
                c.MinValue = (double)HardnessChart.LeftAxisRange.DisplayMinimum;
            }
        }

        void Constraints_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            string A = e.PropertyName;
            OptimizationResult.Text = String.Empty;
        }

        #endregion

        #region Оптимизация

        private void DoOptimize()
        {
            if (OptimizationWorker == null) return;
            if (OptimizationWorker.IsBusy)
            {
                OptimizationWorker.CancelAsync();
                dt.Start();
            }
            else OptimizationWorker.RunWorkerAsync(new object[] { p, optimal, constraints });
        }

        private void InitOptimizationWorker()
        {
            OptimizationWorker = new BackgroundWorker();

            OptimizationWorker.DoWork += (o, e) =>
            {
                ExportToXlsxEx.Dispatcher.BeginInvoke(delegate()
                {
                    OptimizationResult.Text = "Поиск решения...";
                    OptimizationResult.UpdateLayout();
                });
                InteractiveParameters p = (e.Argument as object[])[0] as InteractiveParameters;
                InteractiveParameters opt = (e.Argument as object[])[1] as InteractiveParameters;
                InteractiveParameters constraints = (e.Argument as object[])[2] as InteractiveParameters;
                List<double> FitnessStory = new List<double>();
                List<InteractiveParameters> Swarm = new List<InteractiveParameters>();
                for (int i = 0; i < 80; i++)
                {

                    InteractiveParameters ip = InteractiveParameters.PSOGetParticle(opt);
                    Swarm.Add(ip);
                }

                do
                {
                    double BestFit = (from s in Swarm select s.PSOBestParticleFitness).Max();
                    InteractiveParameters BestParticle = (from s in Swarm where s.PSOBestParticleFitness == BestFit select s).First();
                    FitnessStory.Add(BestFit);

                    if (FitnessStory.Count > 100)
                    {
                        double epsilon = (double)AQMath.AggregateStDev(FitnessStory.TakeLast(11).Cast<object>().ToList());
                        if (FitnessStory.Count > 1e3 || epsilon < 1e-8)
                        {
                            this.Dispatcher.BeginInvoke(delegate()
                            {
                                InteractiveParameters.PSOUnlockParticle(BestParticle, true);
                                optimal = BestParticle;
                                foreach (var a in optimal.Children)
                                {
                                    a.PropertyChanged += new PropertyChangedEventHandler(ParametersChanged);
                                }
                                if (FitnessStory.Count < 1e3) OptimizationResult.Text = "Решение найдено. (50х" + FitnessStory.Count.ToString() + ")";
                                else OptimizationResult.Text = "Нет решения";
                            });
                        return;
                        }
                    }
                    foreach (var s in Swarm)
                    {
                        s.PSOStep(BestParticle, GetPointPenalty(s, constraints, p));
                    }
                }
                while (true);
                
            };

            OptimizationWorker.RunWorkerCompleted += (o, e) =>
            {
                this.Dispatcher.BeginInvoke(delegate()
                {
                    DoCalculation();
                    PSOStart.IsChecked = false;
                });
            };

            OptimizationWorker.WorkerSupportsCancellation = true;
        }

        private void PSOStart_Click(object sender, RoutedEventArgs e)
        {
            DoOptimize();
        }

        private double GetPointPenalty(InteractiveParameters pt, InteractiveParameters constraints, InteractiveParameters p)
        {
            if (constraints.Children.Count==0) return 0;
            double f = 0;

            HardenabilityCalculator hc = (ModeIsNonBoronFlag) ?
                        ((ModeIsMetricFlag) ? new HardenabilityCalculator(AQMath.GetHardenabilityNonBoronMetricWithGrain) :
                                              new HardenabilityCalculator(AQMath.GetHardenabilityNonBoronInchesWithGrain)) :
                        ((ModeIsMetricFlag) ? new HardenabilityCalculator(AQMath.GetHardenabilityBoronMetricWithGrain) :
                                              new HardenabilityCalculator(AQMath.GetHardenabilityBoronInchesWithGrain));

                    double[] Points = (ModeIsMetricFlag) ? MetricPointList : InchesPointList;

                    double? DIMin = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(pt.GetValue("C"), pt.GetValue("Mn"), pt.GetValue("Si"), pt.GetValue("Ni"), pt.GetValue("Cr"), pt.GetValue("Mo"), pt.GetValue("Cu"), pt.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно")) :
                                                         AQMath.HGetDIBoron(pt.GetValue("C"), pt.GetValue("Mn"), pt.GetValue("Si"), pt.GetValue("Ni"), pt.GetValue("Cr"), pt.GetValue("Mo"), pt.GetValue("Cu"), pt.GetValue("V"), ModeIsMetricFlag, p.GetValue("Зерно"));
                    double? DIMax = ModeIsNonBoronFlag ? AQMath.HGetDINonBoron(pt.GetValue2("C"), pt.GetValue2("Mn"), pt.GetValue2("Si"), pt.GetValue2("Ni"), pt.GetValue2("Cr"), pt.GetValue2("Mo"), pt.GetValue2("Cu"), pt.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно")) :
                                                         AQMath.HGetDIBoron(pt.GetValue2("C"), pt.GetValue2("Mn"), pt.GetValue2("Si"), pt.GetValue2("Ni"), pt.GetValue2("Cr"), pt.GetValue2("Mo"), pt.GetValue2("Cu"), pt.GetValue2("V"), ModeIsMetricFlag, p.GetValue2("Зерно"));


            foreach (var c in constraints.Children)
            {
                double ConstraintMin = c.CurrentValue;
                double ConstraintMax = c.CurrentValue2;
                
                double Range = ConstraintMax - ConstraintMin;
                double Point = double.Parse(c.Caption);
                double? Min = hc(Point, pt.GetValue("C"), DIMin.Value);
                double? Max = hc(Point, pt.GetValue2("C"), DIMax.Value);
                if (!Min.HasValue) return 100;
                if (!Max.HasValue) return 100;
                double f1 = Math.Max(ConstraintMin - Min.Value, 0);
                double f2 = Math.Max(Max.Value - ConstraintMax, 0); 
                f += Math.Max(f1, f2);
            }
            return f ;
        }

        #endregion

        private void HardnessChart_Loaded(object sender, RoutedEventArgs e)
        {
            HardnessChart.UpdateLayout();
            Grid c = HardnessChart.GetCustomSurface();
            ListBox ip = new ListBox();
            c.Children.Add(ip);
            ip.ItemTemplate = (DataTemplate)this.Resources["ConstraintItemTemplate"];
            ip.Template = (ControlTemplate)this.Resources["ConstraintTemplate"];
            ip.ItemsPanel = (ItemsPanelTemplate)this.Resources["ConstraintPanelTemplate"];
            ip.ItemContainerStyle = (Style)this.Resources["ConstraintItemStyle"];
            ip.ItemsSource = constraints.Children;
            StackPanel sp = (StackPanel)this.Resources["OptimizationsPanel"];
            this.Resources.Remove("OptimizationsPanel");
            if (sp!=null)c.Children.Add(sp);
        }

        private void NewSlideButton_Click(object sender, RoutedEventArgs e)
        {
            Grid g = HardnessChart.GetCustomSurface();
            //g.Opacity = 0;
            UpdateLayout();
            WriteableBitmap wb = new WriteableBitmap(HardnessChart, new ScaleTransform() { ScaleX = 2, ScaleY = 2 });
            WriteableBitmap icon = new WriteableBitmap(HardnessChart, new ScaleTransform() { ScaleX = 0.5, ScaleY = 0.5 });
            //g.Opacity = 1;
            UpdateLayout();
            SlideElementDescription sed = new SlideElementDescription() { BreakSlide = true, Description = "Полоса прокаливаемости", SlideElementType = SlideElementTypes.Image, Width = wb.PixelWidth, Height = wb.PixelHeight };

            Dictionary<string, object> SignalArguments = new Dictionary<string, object>();
            SignalArguments.Add("Thumbnail", wb);
            SignalArguments.Add("Icon", wb);
            SignalArguments.Add("SlideElement", sed);
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertSlideElement, SignalArguments, null);
        }
    }
    #region Конвертеры 

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class MinValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class MinAllowedValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.MinAllowedValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class MaxAllowedValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.MaxAllowedValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    #endregion
}