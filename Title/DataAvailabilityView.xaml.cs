﻿using System;
using System.Windows;

namespace Title
{
    public partial class DataAvailabilityView
    {
        public delegate void ClickDelegate(object source, RoutedEventArgs e);

        public event ClickDelegate Click;
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(DataAvailabilityView), new PropertyMetadata(String.Empty, DescriptionPropertyChanged));

        public string LockMessageText
        {
            get { return (string)GetValue(LockMessageTextProperty); }
            set { SetValue(LockMessageTextProperty, value); }
        }

        public static readonly DependencyProperty LockMessageTextProperty =
            DependencyProperty.Register("LockMessageText", typeof(string), typeof(DataAvailabilityView), new PropertyMetadata(String.Empty, LockMessageTextPropertyChanged));

        public bool Locked
        {
            get { return (bool)GetValue(LockedProperty); }
            set { SetValue(LockedProperty, value); }
        }

        public static readonly DependencyProperty LockedProperty =
            DependencyProperty.Register("Locked", typeof(bool), typeof(DataAvailabilityView), new PropertyMetadata(false, LockedPropertyChanged));

        public DataAvailabilityView()
        {
            InitializeComponent();
        }

        public static void DescriptionPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((DataAvailabilityView)o);
            c.TitleButton.Text = e.NewValue.ToString();
        }

        public static void LockedPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((DataAvailabilityView)o);
            c.LockedIcon.Visibility = ((bool)e.NewValue) ? Visibility.Visible : Visibility.Collapsed;
            c.LockMessagePlace.Visibility = !String.IsNullOrWhiteSpace(c.LockMessage.Text) ? c.LockedIcon.Visibility : Visibility.Collapsed;
            c.TitleButton.IsEnabled = !(bool)e.NewValue;

        }
        public static void LockMessageTextPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((DataAvailabilityView)o);
            c.LockMessage.Text = e.NewValue.ToString();
            c.LockMessagePlace.Visibility = !String.IsNullOrWhiteSpace(c.LockMessage.Text) ? c.LockedIcon.Visibility : Visibility.Collapsed;
        }

        private void TitleButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Click!=null)Click.Invoke(this, e);
        }
    }
}
