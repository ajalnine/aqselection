﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Core;
using BasicControlsLibrary;
using Core.CalculationServiceReference;

namespace Title
{
    public partial class MainPage : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        readonly DispatcherTimer _dt = new DispatcherTimer{Interval = new TimeSpan(0,0,0,0,300)};
        private bool _scrollFixed = true;
        private readonly AQModuleDescription _aqmd;

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            Loader.TabControlSizeChanged += Loader_TabControlSizeChanged;
            _dt.Tick += dt_Tick;
            FillHexPanel();
        }

        private void FillHexPanel()
        {
            
            for (var x = 0; x < 200; x += 70)
            {
                for (var y = 0; y < 200; y += 20)
                {
                    var offset = x + 35*((y/20) & 1);
                    var color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.1 + ((double)(offset + y)) / 700.0f));
                    var color2 = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.1 + ((double)(offset + y)) / 700.0f));
                    color.A = 0x30;
                    var newHex = new Hexagon
                    {
                        Highlight = color,
                        Stroke = color2
                    };

                    HexPanel.Children.Insert(0, newHex);
                    Canvas.SetLeft (newHex, offset);
                    Canvas.SetTop  (newHex, y);
                }
            }
        }

        void dt_Tick(object sender, EventArgs e)
        {
            _scrollFixed = false;
            _dt.Stop();
        }

        void Loader_TabControlSizeChanged(object o, TabSizeChangedEventArgs e)
        {
            Cont.Width = (e.NewSize.Width>5)? e.NewSize.Width-5 : 0;
            Cont.Height = (e.NewSize.Height>25)? e.NewSize.Height-25 : 0;
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            commandCallBack?.Invoke(commandType, commandArgument, null);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            MainNewsPresenter.RefreshNews();
        }

        private void MainNewsPresenter_NewsLoaded(object o, EventArgs e)
        {
            _scrollFixed = true;
        }

        private void RewindNews()
        {
            NewsHider.ScaleX = 1;
            NewsHider.ScaleY = 1;
            MainNewsPresenter.Rewind();
        }

        private void MainNewsPresenter_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.Renew, null, null);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            if (Math.Abs(Loader.MainTabSize.Width) < 1e-8 || Math.Abs(Loader.MainTabSize.Height) < 1e-8 )return;
            Cont.Width = Loader.MainTabSize.Width - 8;
            Cont.Height = Loader.MainTabSize.Height - 25;
        }

        private void MainNewsPresenter_OnLayoutUpdated(object sender, EventArgs e)
        {
            if (_scrollFixed)RewindNews();
            _dt.Start();
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Math.Abs(Loader.MainTabSize.Width) < 1e08 || Math.Abs(Loader.MainTabSize.Height) < 1e-8) return;
            Cont.Width = Loader.MainTabSize.Width - 8;
            Cont.Height = Loader.MainTabSize.Height - 25;
        }
    }

    public class RightsForNewsEdit : INotifyPropertyChanged
    {
        private Visibility _showeditbuttons;
        public Visibility ShowEditButtons { get { return _showeditbuttons; } set { _showeditbuttons = value; NotifyPropertyChanged("ShowEditButtons"); } }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
