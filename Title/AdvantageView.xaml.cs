﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Title
{
    public partial class AdvantageView
    {
        public delegate void ClickDelegate(object source, RoutedEventArgs e);

        public event ClickDelegate Click;
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(AdvantageView), new PropertyMetadata(String.Empty, DescriptionPropertyChanged));

        public string ImageURI
        {
            get { return (string)GetValue(ImageURIProperty); }
            set { SetValue(ImageURIProperty, value); }
        }

        public static readonly DependencyProperty ImageURIProperty =
            DependencyProperty.Register("ImageURI", typeof(string), typeof(AdvantageView), new PropertyMetadata(String.Empty, ImageURIPropertyChanged));

        public string LockMessageText
        {
            get { return (string)GetValue(LockMessageTextProperty); }
            set { SetValue(LockMessageTextProperty, value); }
        }

        public static readonly DependencyProperty LockMessageTextProperty =
            DependencyProperty.Register("LockMessageText", typeof(string), typeof(AdvantageView), new PropertyMetadata(String.Empty, LockMessageTextPropertyChanged));

        public bool Locked
        {
            get { return (bool)GetValue(LockedProperty); }
            set { SetValue(LockedProperty, value); }
        }

        public static readonly DependencyProperty LockedProperty =
            DependencyProperty.Register("Locked", typeof(bool), typeof(AdvantageView), new PropertyMetadata(false, LockedPropertyChanged));

        public AdvantageView()
        {
            InitializeComponent();
        }

        public static void DescriptionPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((AdvantageView)o);
            c.TitleTextBlock.Text = e.NewValue.ToString();
        }

        public static void ImageURIPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((AdvantageView) o);
            var uri = new Uri(@"/Resources;component/Images/Landing/" + e.NewValue, UriKind.RelativeOrAbsolute);
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = uri;
            image.EndInit();
            c.Icon.Source = image;
            
        }

        public static void LockedPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((AdvantageView)o);
            c.LockedIcon.Visibility = ((bool)e.NewValue) ? Visibility.Visible : Visibility.Collapsed;
            c.LockMessagePlace.Visibility = !String.IsNullOrWhiteSpace(c.LockMessage.Text) ? c.LockedIcon.Visibility : Visibility.Collapsed;
            c.TitleButton.IsEnabled = !(bool) e.NewValue;
        }
        public static void LockMessageTextPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((AdvantageView)o);
            c.LockMessage.Text = e.NewValue.ToString();
            c.LockMessagePlace.Visibility = !String.IsNullOrWhiteSpace(c.LockMessage.Text) ? c.LockedIcon.Visibility : Visibility.Collapsed;
        }

        private void TitleButton_OnClick(object sender, RoutedEventArgs e)
        {
            Click?.Invoke(this, e);
        }

        private void TitleButton_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (Click != null) return;
            TitleButton.Visibility = Visibility.Collapsed;
            TitleButton.IsEnabled = false;
        }
    }
}
