﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using BasicControlsLibrary;
using Core;

namespace Title
{
    public partial class About
    {
        public About()
        {
            InitializeComponent();
            
            Advantage1.Description = "Поиск слабых мест";
            Advantage1.ImageURI = "CheckQuality.png";
            Advantage1.Tag = "SLConstructor_Stability.Page";
            Advantage1.Click += NavigationButtonNoRoles_Click;
            Advantage2.Description = "Развитие";
            Advantage2.ImageURI = "Progress.png";
            Advantage3.Description = "Оперативность исследований";
            Advantage3.ImageURI = "Calculation.png";
            Advantage3.Tag = "SLConstructor_Calculations.Page";
            Advantage3.Click += NavigationButtonNoRoles_Click;
            Advantage4.Description = "Доступ к информации";
            Advantage4.ImageURI = "Access.png";
            Advantage5.Description = "Современный визуальный анализ";
            Advantage5.ImageURI = "Violin.png";
            Advantage5.Tag = "Signal.StartNewAnalysis";
            Advantage5.Click += NavigationButtonNoRoles_Click;
            Advantage6.Description = "Решение специальных задач";
            Advantage6.ImageURI = "Integral.png";
            
            if (!Security.IsInRole("Выборки"))
            {
                Advantage1.Locked = true;
                Advantage1.LockMessageText = "Подключитесь к АС Технолог";
                Advantage4.Locked = true;
                Advantage4.LockMessageText = "Подключитесь к АС Технолог";
                
                foreach (var c in RadialPanel.Children.OfType<DataAvailabilityView>().Where(a => RadialHierarchicalPanel.GetChildOf(a) == "DataSources"))
                {
                    c.Locked = true;
                    c.LockMessageText = string.Empty;
                }
            }

            if (!Security.IsInRole("Послябный_контроль"))
            {
                var b = RadialPanel.Children.OfType<DataAvailabilityView>().SingleOrDefault(a => a.Description == "Контроль");
                if (b == null) return;
                b.Locked = true;
                b.LockMessageText = "Подключитесь к АС Послябового учета";
            }
        }
        private void UpdateLinks()
        {
            if (LinksLayer != null && LinksLayer.Children.Count > 0) LinksLayer.Children.Clear();
            foreach (var element in RadialPanel.Children.OfType<UIElement>())
            {
                var from = RadialHierarchicalPanel.GetCenter(element);
                var id = RadialHierarchicalPanel.GetChildOf(element);
                if (id == null) continue; 
                var parent = RadialPanel.Children.OfType<UIElement>().SingleOrDefault(a => RadialHierarchicalPanel.GetID(a) == id);
                if (parent == null) continue;
                var to = RadialHierarchicalPanel.GetCenter(parent);
                var level = RadialHierarchicalPanel.GetLevel(parent);
                var thickness = (4.0d - level)/2;
                DrawLink(to, from, Orientation.Horizontal, thickness);
            }
        }

        
        private void DrawLink(Point from, Point to, Orientation orientation, double thickness)
        {
            var p = new Path(); //268F97 
            var startPoint = (orientation == Orientation.Horizontal) ? new Point(to.X > from.X ? 0 : 1, 0) : new Point(0, to.Y > from.Y ? 1 : 0);
            var endPoint = (orientation == Orientation.Horizontal) ? new Point(to.X > from.X ? 1 : 0, 0) : new Point(0, to.Y > from.Y ? 0 : 1);
            
            p.Stroke = new LinearGradientBrush
            {
                StartPoint = startPoint, EndPoint = endPoint,
                GradientStops = new GradientStopCollection
                {
                    new GradientStop{Color = Color.FromArgb(0x00,0xff,0xff,0xff), Offset = 0},
                    new GradientStop{Color = Color.FromArgb(0x5F,0x26,0x8f,0x97), Offset = 0.03},
                    new GradientStop{Color = Color.FromArgb(0x7F,0x8f,0x26,0x97), Offset = 0.7},
                    new GradientStop{Color = Color.FromArgb(0x00,0xff,0xff,0xff), Offset = 0.9},
                    new GradientStop{Color = Color.FromArgb(0x00,0xff,0xff,0xff), Offset = 1}
                }
            };
            p.StrokeThickness = thickness;
            p.StrokeEndLineCap = PenLineCap.Triangle;

            p.UseLayoutRounding = true;
            var pg = new PathGeometry();
            p.Data = pg;
            var pf = new PathFigure {IsClosed = false, IsFilled = false, StartPoint = from};
            pg.Figures = new PathFigureCollection {pf};
            var ls3 = new BezierSegment();
            if (orientation == Orientation.Horizontal)
            {
                ls3.Point3 = new Point(to.X, to.Y);
                ls3.Point2 = new Point(from.X + (to.X - from.X) * 0.2, to.Y);
                ls3.Point1 = new Point(from.X, to.Y + (to.Y - from.Y) * 0.2);
            }
            else
            {
                ls3.Point3 = new Point(to.X, to.Y);
                ls3.Point2 = new Point(to.X, from.Y - (to.Y - from.Y) * 0.6);
                ls3.Point1 = new Point(from.X, to.Y * 1.1);
            }
            pf.Segments = new PathSegmentCollection {ls3};

            Panel.SetZIndex(p, 0);
            LinksLayer.Children.Add(p);
        }

        private void Layout_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateLinks();
        }

        private void NavigationButton_Click(object sender, RoutedEventArgs e)
        {
            if (Security.Roles.Any(a => a == "Выборки"))
            {
                var ca = new Dictionary<string, object>();
                var dataAvailabilityView = sender as FrameworkElement;
                if (dataAvailabilityView != null)
                {
                    var uri = dataAvailabilityView.Tag.ToString();
                    ca.Add("URI", uri);
                }
                Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.Load, ca, null);
            }
        }
        private void NavigationButtonNoRoles_Click(object sender, RoutedEventArgs e)
        {
            var ca = new Dictionary<string, object>();
            var dataAvailabilityView = sender as FrameworkElement;
            if (dataAvailabilityView == null) return;
            var tag = dataAvailabilityView.Tag.ToString();

            if (tag.StartsWith("Signal"))
            {
                var command = (Command)Enum.Parse(typeof (Command), tag.Split(new []{'.'}, StringSplitOptions.None)[1], true);
                Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), command, ca, null);
            }
            else
            {
                ca.Add("URI", tag);
                Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.Load, ca, null);
            }
        }
    }
}
