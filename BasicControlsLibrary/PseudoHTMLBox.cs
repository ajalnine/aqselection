﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Core;

namespace BasicControlsLibrary
{
    public class PseudoHTMLBox : UserControl
    {
        public delegate void PicturesLoadedDelegate(object o, EventArgs e);

        public event PicturesLoadedDelegate PicturesLoaded;
        private int _totalPictures, _picturesReady;
        private int _paragraphs;
        public string Source
        {
            get { return (string)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(string), typeof(PseudoHTMLBox), new PropertyMetadata(String.Empty, SourcePropertyChangedCallback));

        public string DateHeader
        {
            get { return (string)GetValue(DateHeaderProperty); }
            set { SetValue(DateHeaderProperty, value); }
        }
        public static readonly DependencyProperty DateHeaderProperty =
            DependencyProperty.Register("DateHeader", typeof(string), typeof(PseudoHTMLBox), new PropertyMetadata(String.Empty, HeaderPropertyChangedCallback));


        private static void SourcePropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sourceUc = o as PseudoHTMLBox;
            sourceUc?.RefreshLayout(e.NewValue.ToString(), sourceUc.DateHeader);
        }

        private static void HeaderPropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

            var sourceUc = o as PseudoHTMLBox;

            DateTime date;
            DateTime.TryParseExact(e.NewValue.ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.AllowWhiteSpaces, out date);
            sourceUc?.RefreshLayout(sourceUc.Source, date.ToShortDateString());
        }

        public PseudoHTMLBox()
        {
            Content = new StackPanel{ Orientation = Orientation.Vertical, Background = new SolidColorBrush(Colors.Transparent) };
        }

        public void RefreshLayout(string html, string header)
        {
            ((StackPanel)Content).Children.Clear();
            if (header != string.Empty)
            {
                var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    VerticalAlignment = VerticalAlignment.Center
                };
                var r = new Run
                {
                    Text = header,
                    Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0x26, 0x8f, 0x97)),
                    FontSize = 12
                };
                tb.Inlines.Add(r);
                ((StackPanel)Content).Children.Add(tb);
            }
            var rtb = new RichTextBox
            {
                IsReadOnly = true,
                BorderThickness = new Thickness(0),
                Background = new SolidColorBrush(Colors.Transparent)
            };
            rtb.Document.Blocks.Clear();
            ((StackPanel)Content).Children.Add(rtb);
            ParseHTML(html, rtb);
        }

        private void ParseHTML(string html, RichTextBox rtb)
        {
            var wp = StartNewLine(rtb);
            html = html.Replace("<li>", "<br />● ").Replace("\r","<br />");
            var isBold = false;
            var isItalic = false;
            var isInHyperlink = false;
            var isRed = false;
            Hyperlink currentHref = new Hyperlink();

            MatchCollection m = Regex.Matches(html, @"(<[^<>]+>)");
            if (m.Count != 0)
            {
                int pos = 0;
                int pos2 = 0;

                foreach (Match match in m)
                {
                    foreach (Capture c in match.Captures)
                    {
                        pos2 = c.Index;
                        if (!isInHyperlink) AddText(wp, html.Substring(pos, pos2 - pos), isBold, isItalic, isRed);
                        else AddHyperlinkText(currentHref, html.Substring(pos, pos2 - pos));
                        string token = c.Value.Replace(" ", "").ToLower();
                        switch (token)
                        {
                            case "<b>":
                                isBold = true;
                                break;
                            case @"</b>":
                                isBold = false;
                                break;
                            case "<red>":
                                isRed = true;
                                break;
                            case @"</red>":
                                isRed = false;
                                break;
                            case "<i>":
                                isItalic = true;
                                break;
                            case @"</i>":
                                isItalic = false;
                                break;
                            case "<br/>":
                                wp = StartNewLine(rtb);
                                break;
                            case "</a>":
                                isInHyperlink  = false;
                                break;
                            default:
                                if (c.Value.Contains("src=")) AddImage(wp, c.Value);
                                else if (c.Value.Contains("href="))
                                {
                                    currentHref = new Hyperlink
                                    {
                                        NavigateUri = new Uri(GetURIForLink(c.Value)),
                                        TargetName = "_blank"
                                    };
                                    isInHyperlink = true;
                                    wp.Inlines.Add(currentHref);
                                }
                                break;
                        }
                        pos = pos2 + c.Length;
                        pos2 = pos;
                    }
                }
                if (pos2 < html.Length)
                {
                    AddText(wp, html.Substring(pos2, html.Length - pos2), false, false, false);
                }
            }
            else
            {
                AddText(wp, html, false, false, false);
            }
            if (_totalPictures == 0) PicturesLoaded?.Invoke(this, new EventArgs());
        }

        private Paragraph StartNewLine(RichTextBox rtb)
        {
            var p = new Paragraph { TextAlignment = TextAlignment.Left, LineHeight = 14, LineStackingStrategy = LineStackingStrategy.BlockLineHeight, FontSize = 12 };
            rtb.Document.Blocks.Add(p);
            p.Inlines.Clear();
            p.Margin = new Thickness(0);
            return p;
        }

        private void AddText(Paragraph wp, string text, bool isBold, bool isItalic, bool isRed)
        {
            var r = new Run
            {
                Text = text,
                FontWeight = (isBold) ? FontWeights.Bold : FontWeights.Light,
                FontStyle = (isItalic) ? FontStyles.Italic : FontStyles.Normal,
                Foreground = isRed ? new SolidColorBrush(Colors.Red) : Foreground
            };
            wp.Inlines.Add(r);
        }

        private void AddHyperlinkText(Hyperlink currentHref, string text)
        {
            Run r = new Run {Text = text};
            currentHref.Inlines.Add(r);
        }

        private void AddImage(Paragraph wp, string tokenCode)
        {
            var m = Regex.Match(tokenCode, "img src=\"(.+)\"");
            if (m.Groups.Count <= 1) return;
            var uri = m.Groups[1].Value;
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(Services.Source + uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            _totalPictures++;
            var i = new Image();
            i.BeginInit();
            i.Source = bi;
            var iuic = new InlineUIContainer();
            wp.Inlines.Add(iuic);
            iuic.Child = i;
            i.SizeChanged += ImageOnLoaded;
            i.EndInit();
        }

        private void ImageOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            _picturesReady++;
            var i = (sender as Image);
            var image = i?.Source;
            if (image==null)return;
            i.Width = (Width < image.Width)? image.Width : Width;
            i.Height = image.Height;
            if (_totalPictures == _picturesReady) PicturesLoaded?.Invoke(this, new EventArgs());
        }

        private string GetURIForLink(string tokenCode)
        {
            Match m = Regex.Match(tokenCode, "a href=\"(.+)\"");
            if (m.Groups.Count > 1)
            {
                string uri = m.Groups[1].Value;
                return uri;
            }
            return null;
        }
    }
}
