﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace BasicControlsLibrary
{
    public class TextImageButtonBase : Button
    {
        readonly DispatcherTimer _clickTimer;

        public delegate void ButtonAutoClick(object sender, RoutedEventArgs e);
        public event ButtonAutoClick AutoClick;
        public delegate void ButtonClick(object sender, RoutedEventArgs e);
        public new event ButtonClick Click;
        private float _tickNumber;
        
        public string ImageUrl
        {
            get { return (string)GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); }
        }

        public static readonly DependencyProperty ImageUrlProperty =
            DependencyProperty.Register("ImageUrl", typeof(string), typeof(TextImageButtonBase), new PropertyMetadata(String.Empty, delegate(DependencyObject o, DependencyPropertyChangedEventArgs args) { ((TextImageButtonBase)o).ImageUrlChanged(args.NewValue.ToString()); }));

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(TextImageButtonBase), new PropertyMetadata(null));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(TextImageButtonBase), new PropertyMetadata(String.Empty));

        public TextImageButtonBase()
        {
            _clickTimer = new DispatcherTimer();
            _clickTimer.Tick += ClickTimer_Tick;
        }
        
        private void ImageUrlChanged(string newValue)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            var img = new BitmapImage();
            img.BeginInit();
            img.UriSource = new Uri(newValue, UriKind.Relative);
            img.EndInit();
            ImageSource = img;
        }

        private void ClickTimer_Tick(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(delegate
            {
                _clickTimer.Interval = (Math.Abs(_tickNumber) < 1e-8)? new TimeSpan(0, 0, 0, 0, 100):new TimeSpan(0, 0, 0, 0, (int)(100 / (1.0f + _tickNumber)));

                AutoClick?.Invoke(this, new RoutedEventArgs());
                _tickNumber += 0.1f;
                if (_tickNumber > 2) _tickNumber = 2;
            }));
        }
      
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            e.Handled = false;
            AutoClick?.Invoke(this, new RoutedEventArgs());
            Click?.Invoke(this, new RoutedEventArgs());
            _clickTimer.Interval = new TimeSpan(0, 0, 0, 0, 400);
            _tickNumber = 0;
            _clickTimer.Start();
        }
        
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);
            e.Handled = false;
            _clickTimer.Stop();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            _clickTimer.Stop();
            base.OnMouseLeave(e);
        }
    }
}
