﻿using System;
using System.Windows;

namespace BasicControlsLibrary
{
    public partial class VerticalTextBlock 
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(VerticalTextBlock), new PropertyMetadata(String.Empty));

        public VerticalTextBlock()
        {
            InitializeComponent();
        }
    }
}
