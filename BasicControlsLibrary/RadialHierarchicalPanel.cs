﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BasicControlsLibrary
{
    public class RadialHierarchicalPanel : Panel
    {
        private  Dictionary<int, double> _maxLevelDiameters;

        private double _aspectRatio;
        
        public static string GetChildOf(DependencyObject obj)
        {
            return (string)obj.GetValue(ChildOfProperty);
        }

        public static void SetChildOf(DependencyObject obj, string value)
        {
            obj.SetValue(ChildOfProperty, value);
        }

        public static readonly DependencyProperty ChildOfProperty =
            DependencyProperty.RegisterAttached("ChildOf", typeof(string), typeof(RadialHierarchicalPanel), new PropertyMetadata(null, PropertyChanged));

        public static Point GetCenter(DependencyObject obj)
        {
            return (Point)obj.GetValue(CenterProperty);
        }

        public static void SetCenter(DependencyObject obj, Point value)
        {
            obj.SetValue(CenterProperty, value);
        }

        public static readonly DependencyProperty CenterProperty =
            DependencyProperty.RegisterAttached("Center", typeof(Point), typeof(RadialHierarchicalPanel), new PropertyMetadata(new Point(0, 0), PropertyChanged));


        
        public static string GetID(DependencyObject obj)
        {
            return (string)obj.GetValue(IDProperty);
        }

        public static void SetID(DependencyObject obj, string value)
        {
            obj.SetValue(IDProperty, value);
        }

        public static readonly DependencyProperty IDProperty =
            DependencyProperty.RegisterAttached("ID", typeof(string), typeof(RadialHierarchicalPanel), new PropertyMetadata(null, PropertyChanged));

        
        public static double GetRadialMargin(DependencyObject obj)
        {
            return (double)obj.GetValue(RadialMarginProperty);
        }

        public static void SetRadialMargin(DependencyObject obj, double value)
        {
            obj.SetValue(RadialMarginProperty, value);
        }

        public static readonly DependencyProperty RadialMarginProperty =
            DependencyProperty.RegisterAttached("RadialMargin", typeof(double), typeof(RadialHierarchicalPanel), new PropertyMetadata(0.0d, PropertyChanged));

        public static double GetAngleOffset(DependencyObject obj)
        {
            return (double)obj.GetValue(AngleOffsetProperty);
        }

        public static void SetAngleOffset(DependencyObject obj, double value)
        {
            obj.SetValue(AngleOffsetProperty, value);
        }


        public static readonly DependencyProperty AngleOffsetProperty =
            DependencyProperty.RegisterAttached("AngleOffset", typeof(double), typeof(RadialHierarchicalPanel), new PropertyMetadata(0.0d, PropertyChanged));



        public static double GetAngle(DependencyObject obj)
        {
            return (double)obj.GetValue(AngleProperty);
        }

        public static void SetAngle(DependencyObject obj, double value)
        {
            obj.SetValue(AngleProperty, value);
        }

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.RegisterAttached("Angle", typeof(double), typeof(RadialHierarchicalPanel), new PropertyMetadata(0.0d, PropertyChanged));
        

        public static int GetLevel(DependencyObject obj)
        {
            return (int)obj.GetValue(LevelProperty);
        }

        public static void SetLevel(DependencyObject obj, int value)
        {
            obj.SetValue(LevelProperty, value);
        }

        public static readonly DependencyProperty LevelProperty =
            DependencyProperty.RegisterAttached("Level", typeof(int), typeof(RadialHierarchicalPanel), new PropertyMetadata(0, PropertyChanged));
       
        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element == null) return;

            var rhp = VisualTreeHelper.GetParent(d) as RadialHierarchicalPanel;
            rhp?.InvalidateMeasure();
        }
        // Для нулевого уровня - если один - в центре, если несколько - вокруг, весь круг используется, для четного - стартовый угол 0.5 от числа, для нечетного - вверху всегда один.
        // Для первого уровня - по минимуму относительно середины группы и родителя + AngleOffset
        // Для второго уровня - аналогично относительно середины группы и родителя + AngleOffset

        protected override Size MeasureOverride(Size availableSize)
        {
            RecalcLevels();
            var maxLevel = Children.OfType<UIElement>().Max(a => GetLevel(a));
            var zeroLevelCount = Children.OfType<UIElement>().Count(a => GetLevel(a) == 0);
            GetMaxLevelDiameters(availableSize, maxLevel, true);

            if (zeroLevelCount > 1)
            {
                var totals = _maxLevelDiameters.Sum(a => a.Value);
                return new Size(Math.Max(totals, availableSize.Width), Math.Max(totals, availableSize.Height));
            }
            var levelZeroDiameter = _maxLevelDiameters.Single(a => a.Key == 0).Value;
            var otherDiameter = _maxLevelDiameters.Where(a => a.Key > 0).Sum(a=>a.Value);
            return new Size(Math.Max(levelZeroDiameter / 2 + otherDiameter, availableSize.Width), 
                            Math.Max(levelZeroDiameter / 2 + otherDiameter, availableSize.Height));
        }

        private void GetMaxLevelDiameters(Size availableSize, int maxLevel, bool measure)
        {
            _maxLevelDiameters = new Dictionary<int, double>();

            for (var i = 0; i <= maxLevel; i++)
            {
                var levelElements = Children.OfType<UIElement>().Where(a => GetLevel(a) == i).ToList();
                var diameter = 0.0d;
                foreach (var e in levelElements)
                {
                    if (measure) e.Measure(availableSize);
                    var d = GetRadialMargin(e) * 2 + 2 * Math.Sqrt(Math.Pow(e.DesiredSize.Width/2, 2) + Math.Pow(e.DesiredSize.Height/2, 2));
                    if (diameter < d) diameter = d;
                }
                _maxLevelDiameters.Add(i, diameter);
            }
        }

        private void RecalcLevels()
        {
            foreach (var element in Children.OfType<UIElement>())
            {
                var level = 0;
                var e = element;
                var childOfID = GetChildOf(e);
                while (childOfID != null)
                {
                    childOfID = GetChildOf(e);
                    if (childOfID == null) break;
                    level++;
                    e = Children.OfType<UIElement>().Single(a => GetID(a) == childOfID);
                }
                SetLevel(element, level);
            }
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            RecalcLevels();
            var zeroLevelElements = Children.OfType<UIElement>().Where(a => GetLevel(a) == 0).ToList();
            _aspectRatio = Math.Abs(finalSize.Height) < 1e-7 ? 1 : finalSize.Width / finalSize.Height * 0.8;

            var angle = 0.0d;
            var angleStep = Math.PI * 2.0d / zeroLevelElements.Count;
            
            if (zeroLevelElements.Count > 1)
            {
                var diameterZeroLevel = _maxLevelDiameters[0] / 2;
                foreach (var zle in zeroLevelElements)
                {
                    SetAngle(zle, angle + GetAngleOffset(zle));
                    angle += angleStep;
                    var newCenter = new Point(finalSize.Width / 2 + diameterZeroLevel * Math.Cos(GetAngle(zle)) * _aspectRatio, finalSize.Height / 2 + diameterZeroLevel * Math.Sin(GetAngle(zle)));
                    SetCenter(zle, newCenter);        
                    SetChildAngles(zle, finalSize, _maxLevelDiameters[0], false);
                }
            }
            else
            {
                var zle = zeroLevelElements.First(); 
                SetAngle(zle , angle);
                SetCenter(zle, new Point(finalSize.Width / 2, finalSize.Height / 2));
                SetChildAngles(zle, finalSize, _maxLevelDiameters[0] / 2, true);
            }

            foreach (var child in Children.OfType<UIElement>())
            {
                var center = GetCenter(child);
                if (double.IsNaN(center.X) || double.IsNaN(center.Y)) child.Arrange(new Rect(0, 0, 0, 0));
                else
                {
                    var rect = new Rect(center.X - child.DesiredSize.Width/2, center.Y - child.DesiredSize.Height/2,
                        child.DesiredSize.Width, child.DesiredSize.Height);
                    child.Arrange(rect);
                }
            }
            
            return finalSize;
        }

        private void SetChildAngles(UIElement zle, Size finalSize, double previousRingDiameter, bool zeroLevelIsSIngle)
        {
            var id = GetID(zle);
            if (id == null) return;
            var childs = Children.OfType<UIElement>().Where(a =>  GetChildOf(a)==id).ToList();
            if (childs.Count == 0) return;
            var startangle = GetAngle(zle);
            var childLevel = GetLevel(childs.First());
            var radial = previousRingDiameter + _maxLevelDiameters[childLevel]/2;
            var count = (double)childs.Count;
            double angleStep, totalAngle, angle;

            if (zeroLevelIsSIngle)
            {
                angleStep =  Math.PI * 2.0d / count;
                totalAngle = Math.Abs(angleStep * count);
                angle = startangle - totalAngle / 2;
            }
            else
            {
                angleStep = Math.Atan((_maxLevelDiameters[childLevel] / 2) / radial);
                totalAngle = Math.Abs(angleStep * (count - 1));
                angle = startangle - totalAngle / 2;
            }
            
            foreach (var child in childs)
            {
                SetAngle(child, angle + GetAngleOffset(child));
                angle += angleStep;
                var newCenter = new Point(finalSize.Width / 2 + radial * Math.Cos(GetAngle(child)) * _aspectRatio, finalSize.Height / 2 + radial * Math.Sin(GetAngle(child)));
                SetCenter(child, newCenter);
                SetChildAngles(child, finalSize, previousRingDiameter + _maxLevelDiameters[childLevel], false);
            }
        }
    }
}
