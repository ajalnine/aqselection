﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace BasicControlsLibrary
{
    public class TreeViewExtended : TreeView
    {
        public delegate void DragFinishedDelegate(object sender, DragFinishedEventArgs e);
        public delegate void DragCandidateDelegate(object sender, DragCandidateEventArgs e);
        public event EventHandler<TreeViewEventArgs> ContainerPrepared;
        public event EventHandler<TreeViewEventArgs> SelectedItemGeometryPossiblyChanged;
        public event EventHandler<MouseButtonEventArgs> OnMouseLeftButtonDownOnItem;
        public event EventHandler<MouseButtonEventArgs> OnMouseLeftButtonDoubleClick;
        public event EventHandler<MouseButtonEventArgs> OnMouseRightButtonDownOnItem;
        public event EventHandler<MouseButtonEventArgs> OnMouseLeftButtonUpOnItem;
        public event EventHandler<MouseButtonEventArgs> OnMouseRightButtonUpOnItem;
        public event EventHandler<MouseEventArgs> OnMouseEnterItem;
        public event EventHandler<MouseEventArgs> OnMouseLeaveItem;
        public event EventHandler<MouseEventArgs> OnMouseMoveOverItem;
        public event EventHandler<MouseEventArgs> DragStarted;
        public event DragFinishedDelegate DragFinished;
        public event DragCandidateDelegate DragCandidateChanged;
        public delegate void ContextMenuCallDelegate(object sender, ContextMenuCallEventArgs e);
        public event ContextMenuCallDelegate ContextMenuCall;

        private List<object> itemsToDelayExpand = new List<object>();
        private object SelectedItemDelayed = null;

        private bool LeftMouseButtonPressed = false;
        private bool ItemDragging = false;
        private TreeViewItemExtended DraggingItem = null;
        private TreeViewItemExtended ItemOnWhichMouseOverDuringDrag = null;
        private DispatcherTimer DragTimer = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 0, 0, 300) };
        private Point LastProcessedPoint;

        public TreeViewExtended()
        {
            this.DefaultStyleKey = typeof(TreeView);
            DragTimer.Tick += new EventHandler(DragTimer_Tick);
        }

        public void CancelDrag()
        {
            ItemDragging = false;
            DraggingItem = null;
            ItemOnWhichMouseOverDuringDrag = null;
            LeftMouseButtonPressed = false;
            DragTimer.Stop();
        }

        void DragTimer_Tick(object sender, EventArgs e)
        {
            if (ItemOnWhichMouseOverDuringDrag != null) ItemOnWhichMouseOverDuringDrag.IsExpanded = true;
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeViewExtended;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeViewItemExtended();
        }

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            TreeViewItemExtended treeViewItemExtended = (TreeViewItemExtended)element;
            treeViewItemExtended.ParentTreeView = this;
            base.PrepareContainerForItemOverride(element, item);
            InvokeContainerPrepared(treeViewItemExtended, item);
        }

        internal void InvokeContainerPrepared(TreeViewItemExtended sender, object item)
        {
            if (ContainerPrepared != null) ContainerPrepared(sender, new TreeViewEventArgs(sender, item));
            if (itemsToDelayExpand.Contains(item))
            {
                sender.IsExpanded = true;
                itemsToDelayExpand.Remove(item);
            }
        }

        public void ExpandDelayItems(List<object> ItemsCorrespondingToTreeViewItemsToExpand)
        {
            itemsToDelayExpand.AddRange(ItemsCorrespondingToTreeViewItemsToExpand);
            foreach (object itemtoTryAndExpand in ItemsCorrespondingToTreeViewItemsToExpand)
            {
                TreeViewItem treeViewItem = this.ContainerFromItem(itemtoTryAndExpand);
                if (treeViewItem != null)
                {
                    treeViewItem.IsExpanded = true;
                    itemsToDelayExpand.Remove(itemtoTryAndExpand);
                }
            }
        }

        public void SetSelectedItem(object SelectedItem, List<object> SelectedItemParents)
        {
            ContainerPrepared += new EventHandler<TreeViewEventArgs>(ContainerPrepared_LookForSelectedItem);
            SelectedItemDelayed = SelectedItem;
            ExpandDelayItems(SelectedItemParents);
        }

        public void StartRename()
        {
            if (this.SelectedItem!=null) (this.ContainerFromItem(this.SelectedItem) as TreeViewItemExtended).StartRename();
        }

        private void ContainerPrepared_LookForSelectedItem(object sender, TreeViewEventArgs e)
        {
            if (e.Item == SelectedItemDelayed)
            {
                e.Container.IsSelected = true;
                SelectedItemDelayed = null;
                ContainerPrepared -= new EventHandler<TreeViewEventArgs>(ContainerPrepared_LookForSelectedItem);
            }
        }

        internal void InvokeOnMouseLeftButtonDown(TreeViewItemExtended sender, MouseButtonEventArgs e)
        {
            LeftMouseButtonPressed = true;
            sender.IsSelected = true;
            e.Handled = true;
            if (OnMouseLeftButtonDownOnItem != null) OnMouseLeftButtonDownOnItem(sender, e);
        }

        internal void InvokeOnMouseLeftButtonDoubleClick(TreeViewItemExtended sender, MouseButtonEventArgs e)
        {
            if (sender==e.OriginalSource) sender.IsExpanded = !sender.IsExpanded;
            sender.IsSelected = true;
            e.Handled = true;
            if (OnMouseLeftButtonDoubleClick != null) OnMouseLeftButtonDoubleClick(sender, e);
        }

        internal void InvokeOnMouseRightButtonDown(TreeViewItemExtended sender, MouseButtonEventArgs e)
        {
            if (OnMouseRightButtonDownOnItem != null) OnMouseRightButtonDownOnItem(sender, e);
            if (ContextMenuCall != null)
            {
                ContextMenuCallEventArgs te = new ContextMenuCallEventArgs();
                te.SelectedItem = sender;
                te.CallPoint = e.GetPosition(this);
                e.Handled = true;
                sender.IsSelected = true;
                ContextMenuCall(this, te);
            }
        }
        
        internal void InvokeOnMouseLeftButtonUp(TreeViewItemExtended sender, MouseButtonEventArgs e)
        {
            LeftMouseButtonPressed = false;
            if (DragFinished!=null)DragFinished(sender, new DragFinishedEventArgs(DraggingItem, sender, e));
            ItemDragging = false;
            DraggingItem = null;
            DragTimer.Stop();
            if (OnMouseLeftButtonUpOnItem != null) OnMouseLeftButtonUpOnItem(sender, e);
        }

        internal void InvokeOnMouseRightButtonUp(TreeViewItemExtended sender, MouseButtonEventArgs e)
        {
            if (OnMouseRightButtonUpOnItem != null) OnMouseRightButtonDownOnItem(sender, e);
        }

        internal void InvokeOnMouseEnterItem(TreeViewItemExtended sender, MouseEventArgs e)
        {
            if (ItemDragging)
            {
                ItemOnWhichMouseOverDuringDrag = sender;
                DragTimer.Start();
                Point MousePoint = e.GetPosition(sender.ParentTreeView);
                if (DragCandidateChanged != null && MousePoint!= LastProcessedPoint)
                {
                    DragCandidateChanged(sender, new DragCandidateEventArgs(DraggingItem, sender, e));
                    LastProcessedPoint = MousePoint;
                }
            }
            if (OnMouseEnterItem != null) OnMouseEnterItem(sender, e);
        }

        internal void InvokeOnMouseLeaveItem(TreeViewItemExtended sender, MouseEventArgs e)
        {
            if (ItemDragging)
            {
                ItemOnWhichMouseOverDuringDrag = null;
                DragTimer.Stop();
                DragCandidateChanged(sender, new DragCandidateEventArgs(DraggingItem, null, e));
            }
            if (OnMouseLeaveItem != null) OnMouseLeaveItem(sender, e);
        }

        internal void InvokeOnMouseMoveOverItem(TreeViewItemExtended sender, MouseEventArgs e)
        {
            if (LeftMouseButtonPressed && ItemDragging == false)
            {
                ItemDragging = true;
                DraggingItem = sender;
                if (DragStarted != null) DragStarted(sender, e);
            }
            if (ItemDragging)
            {
                ItemOnWhichMouseOverDuringDrag = sender;
                DragTimer.Start();
                Point MousePoint = e.GetPosition(sender.ParentTreeView);
                if (DragCandidateChanged != null && MousePoint != LastProcessedPoint)
                {
                    DragCandidateChanged(sender, new DragCandidateEventArgs(DraggingItem, sender, e));
                    LastProcessedPoint = MousePoint;
                }
            }
            if (OnMouseMoveOverItem != null) OnMouseMoveOverItem(sender, e);
        }

        internal void InvokeSelectedItemGeometryPossiblyChanged(TreeViewItemExtended sender, EventArgs e)
        {
            this.UpdateLayout();
            if (SelectedItemGeometryPossiblyChanged != null) SelectedItemGeometryPossiblyChanged(sender, new TreeViewEventArgs((TreeViewItemExtended)TreeViewWorkarounds.ContainerFromItem(this, this.SelectedItem), this.SelectedItem));
        }
    }
    
    public class TreeViewItemExtended : TreeViewItem
    {
        public TreeViewExtended ParentTreeView { set; get; }
        public TreeViewItemExtended ParentTreeViewItem { internal set; get; }

        public delegate void EditModeDelegate(object o, EventArgs e);
        public event EditModeDelegate EditModeStarted;
        public event EventHandler<MouseButtonEventArgs> OnMouseLeftButtonDownOnItem;
        public event EventHandler<MouseButtonEventArgs> OnMouseLeftButtonUpOnItem;

        public TreeViewItemExtended()
        {
            DefaultStyleKey = typeof(TreeViewItem);
        }

        public void StartRename()
        {
            if (EditModeStarted != null)
            {
                EditModeStarted(this, new EventArgs());
            }
        }

        protected override void OnCollapsed(RoutedEventArgs e)
        {
            base.OnCollapsed(e);
            ParentTreeView.InvokeSelectedItemGeometryPossiblyChanged(this, new EventArgs());
        }

        protected override void OnExpanded(RoutedEventArgs e)
        {
            base.OnExpanded(e);
            ParentTreeView.InvokeSelectedItemGeometryPossiblyChanged(this, new EventArgs());
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeViewExtended;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeViewItemExtended();
        }

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            TreeViewItemExtended treeViewItemExtended = (TreeViewItemExtended)element;
            treeViewItemExtended.ParentTreeView = this.ParentTreeView;
            treeViewItemExtended.ParentTreeViewItem = this;
            base.PrepareContainerForItemOverride(element, item);
            ParentTreeView.InvokeContainerPrepared(treeViewItemExtended, item);
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (e.ClickCount==2) ParentTreeView.InvokeOnMouseLeftButtonDoubleClick(this, e);
            else if (OnMouseLeftButtonDownOnItem != null) OnMouseLeftButtonDownOnItem(this, e);
            ParentTreeView.InvokeOnMouseLeftButtonDown(this, e);
            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            ParentTreeView.InvokeOnMouseRightButtonDown(this, e);
            base.OnMouseRightButtonDown(e);
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (OnMouseLeftButtonUpOnItem != null) OnMouseLeftButtonUpOnItem(this, e);
            ParentTreeView.InvokeOnMouseLeftButtonUp(this, e);
            base.OnMouseLeftButtonUp(e);
        }

        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            ParentTreeView.InvokeOnMouseRightButtonUp(this, e);
            base.OnMouseRightButtonUp(e);
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            ParentTreeView.InvokeOnMouseEnterItem(this, e);
            base.OnMouseEnter(e);
        }
        
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            ParentTreeView.InvokeOnMouseLeaveItem(this, e);
            base.OnMouseLeave(e);
        }

        protected override void  OnMouseMove(MouseEventArgs e)
        {
            ParentTreeView.InvokeOnMouseMoveOverItem(this, e);
            base.OnMouseMove(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.F2 && this.EditModeStarted != null)
            {
                this.EditModeStarted(this, new EventArgs());
                e.Handled = true;
            }
            else base.OnKeyDown(e);
        }
    }

    public class TreeViewEventArgs : EventArgs
    {
        public TreeViewItemExtended Container { get; set; }
        public object Item { get; set; }

        public TreeViewEventArgs(TreeViewItemExtended container, object item)
        {
            Container = container;
            Item = item;
        }
    }

    public class DragFinishedEventArgs : EventArgs
    {
        public TreeViewItemExtended Source { get; set; }
        public TreeViewItemExtended Destination { get; set; }
        public MouseButtonEventArgs MouseButtonEventArgs { get; set; }
        
        public DragFinishedEventArgs(TreeViewItemExtended source, TreeViewItemExtended destination, MouseButtonEventArgs e)
        {
            Source = source;
            Destination = destination;
            MouseButtonEventArgs = e;
        }
    }

    public class DragCandidateEventArgs : EventArgs
    {
        public TreeViewItemExtended Source { get; set; }
        public TreeViewItemExtended Destination { get; set; }
        public MouseEventArgs MouseButtonEventArgs { get; set; }

        public DragCandidateEventArgs(TreeViewItemExtended source, TreeViewItemExtended destination, MouseEventArgs e)
        {
            Source = source;
            Destination = destination;
            MouseButtonEventArgs = e;
        }
    }

    public class ContextMenuCallEventArgs : EventArgs
    {
        public TreeViewItemExtended SelectedItem { get; set; }
        public Point CallPoint { get; set; }
    }

    public static class TreeViewWorkarounds
    {
        public static TreeViewItem ContainerFromItem(this TreeView treeView, object item)
        {
            TreeViewItem containerThatMightContainItem = (TreeViewItem)treeView.ItemContainerGenerator.ContainerFromItem(item);
            if (containerThatMightContainItem != null) return containerThatMightContainItem;
            else return ContainerFromItem(treeView.ItemContainerGenerator, treeView.Items, item);
        }

        private static TreeViewItem ContainerFromItem(ItemContainerGenerator parentItemContainerGenerator, ItemCollection itemCollection, object item)
        {
            foreach (object curChildItem in itemCollection)
            {
                TreeViewItem parentContainer = (TreeViewItem)parentItemContainerGenerator.ContainerFromItem(curChildItem);
                if (parentContainer == null) return null;
                TreeViewItem containerThatMightContainItem = (TreeViewItem)parentContainer.ItemContainerGenerator.ContainerFromItem(item);
                if (containerThatMightContainItem != null) return containerThatMightContainItem;
                TreeViewItem recursionResult = ContainerFromItem(parentContainer.ItemContainerGenerator, parentContainer.Items, item);
                if (recursionResult != null) return recursionResult;
            }
            return null;
        }

        public static object ItemFromContainer(this TreeView treeView, TreeViewItem container)
        {
            TreeViewItem itemThatMightBelongToContainer = (TreeViewItem)treeView.ItemContainerGenerator.ItemFromContainer(container);
            if (itemThatMightBelongToContainer != null) return itemThatMightBelongToContainer;
            else return ItemFromContainer(treeView.ItemContainerGenerator, treeView.Items, container);
        }

        private static object ItemFromContainer(ItemContainerGenerator parentItemContainerGenerator, ItemCollection itemCollection, TreeViewItem container)
        {
            foreach (object curChildItem in itemCollection)
            {
                TreeViewItem parentContainer = (TreeViewItem)parentItemContainerGenerator.ContainerFromItem(curChildItem);
                if (parentContainer == null) return null;
                TreeViewItem itemThatMightBelongToContainer = (TreeViewItem)parentContainer.ItemContainerGenerator.ItemFromContainer(container);
                if (itemThatMightBelongToContainer != null) return itemThatMightBelongToContainer;
                TreeViewItem recursionResult = ItemFromContainer(parentContainer.ItemContainerGenerator, parentContainer.Items, container) as TreeViewItem;
                if (recursionResult != null) return recursionResult;
            }
            return null;
        }

        public static TreeViewItem FindContainer(this TreeView treeView, Predicate<TreeViewItem> condition)
        {
            return FindContainer(treeView.ItemContainerGenerator, treeView.Items, condition);
        }

        private static TreeViewItem FindContainer(ItemContainerGenerator parentItemContainerGenerator, ItemCollection itemCollection, Predicate<TreeViewItem> condition)
        {
            foreach (object curChildItem in itemCollection)
            {
                TreeViewItem containerThatMightMeetTheCondition = (TreeViewItem)parentItemContainerGenerator.ContainerFromItem(curChildItem);
                if (containerThatMightMeetTheCondition == null) return null;
                if (condition(containerThatMightMeetTheCondition)) return containerThatMightMeetTheCondition;
                TreeViewItem recursionResult = FindContainer(containerThatMightMeetTheCondition.ItemContainerGenerator, containerThatMightMeetTheCondition.Items, condition);
                if (recursionResult != null) return recursionResult;
            }
            return null;
        }
    }
}