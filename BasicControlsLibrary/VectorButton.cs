﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace BasicControlsLibrary
{
    public class VectorButton:Button
    {
        public Geometry PathDataOutline
        {
            get { return (Geometry)GetValue(PathDataOutlineProperty); }
            set { SetValue(PathDataOutlineProperty, value); }
        }

        public static readonly DependencyProperty PathDataOutlineProperty =
            DependencyProperty.Register("PathDataOutline", typeof(Geometry), typeof(VectorButton), new PropertyMetadata(null));

        public Geometry PathDataChrome
        {
            get { return (Geometry)GetValue(PathDataChromeProperty); }
            set { SetValue(PathDataChromeProperty, value); }
        }

        public static readonly DependencyProperty PathDataChromeProperty =
            DependencyProperty.Register("PathDataChrome", typeof(Geometry), typeof(VectorButton), new PropertyMetadata(null));
        
       
    }
}
