﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Technology
{
    public partial class MainPage
    {
        private const string TableName = "Технология";

        private readonly Dictionary<SelectionTypes, int> _fixedColumnsDictionary = new Dictionary<SelectionTypes, int>
            {
                {SelectionTypes.LinePerMelt, 6},
                {SelectionTypes.LinePerMeltRelative, 6},
                {SelectionTypes.LinePerCard, 6},
                {SelectionTypes.LinePerProbe, 7},
                {SelectionTypes.LinePerProcessing, 7},
            };

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            PreviewButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                    {
                        var tables = _cas.EndGetTablesSLCompatible(i);
                        if (tables == null)
                        {
                            task.SetState(0, TaskState.Error);
                            Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
                        }
                        task.AdvanceProgress();
                        task.SetMessage("Данные готовы");
                        var ca = new Dictionary<string, object>
                        {
                            {"From", ColumnsPresenter.From}, 
                            {"To", ColumnsPresenter.To}, 
                            {"SourceData", tables},
                            {"Calculation", _steps},
                            {"Description", _filters.Description},
                            {"New", true},
                        };
                        Dispatcher.BeginInvoke(() =>
                        {
                            PreviewButton.IsEnabled = true;
                            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
                }
            }, task);
        }

        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            XlsxExportButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт Xlsx", TaskPanel);
            task.SetState(0, TaskState.Processing);
            task.CustomData = sender;
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, iar2 =>
                    {
                        var t2 = _cas.EndGetAllResultsInXlsx(iar2);
                        task.SetState(1, TaskState.Ready);
                        task.SetMessage(t2.Message);
                        Dispatcher.BeginInvoke(() =>
                        {
                            HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}", 
                                HttpUtility.UrlEncode(t2.TaskDataGuid), 
                                HttpUtility.UrlEncode(string.Format("Отчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null))), 
                                HttpUtility.UrlEncode("application/msexcel"))));
                            XlsxExportButton.IsEnabled = true;
                        });
                    }, task);
                }
                else
                {
                    _cas.BeginDropData(t.TaskDataGuid, null, null);
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                }    
            }, task);
        }

        private void SaveQueryButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", TableName},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation, signalArguments, null);
        }


        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", TableName},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        private void CreateCalculation()
        {
            RefreshFlags();
            RefreshFilters(false);
            _currentSQL = TechnologySelects.GetTechnologySQL(_filters, _parametersList, _fieldListCombination, _selectionType, _addGroupName);
            Services.GetAQService().BeginWriteLog("Calc: Technology, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);
            _steps = StepFactory.CreateSQLQuery(_currentSQL, TableName);
            if (_selectionType == SelectionTypes.LinePerMelt || _selectionType == SelectionTypes.LinePerMeltRelative)
            {
                _steps.Add(StepFactory.CreateGrouping(TableName, TechnologyCalculationFactory.GetGroupDataForMeltSelectionType(_parametersList, _fieldListCombination, _selectionType, _addGroupName)));
            }
            AppendDescriptionStep();
            if (_formatted) AppendFormatSteps();
            if (_colored) AppendScales();
        }

        private void AppendDescriptionStep()
        {
            _steps.Add(StepFactory.CreateConstants(new List<ConstantDescription>
            {
                new ConstantDescription{VariableName = "Описание", VariableType = "String", Value = _filters.Description},
                new ConstantDescription{VariableName = "Автор", VariableType = "String", Value = Security.CurrentUser},
                new ConstantDescription{VariableName = "Фиксированный столбец", VariableType = "Int32", Value =  _formatted ? _fixedColumnsDictionary[_selectionType] : 1}
            }));
        }

        private void AppendFormatSteps()
        {
            _steps.Add(StepFactory.CreateChangeFields(TableName, TechnologyCalculationFactory.GetChangeFieldsFormattedOutput(_parametersList, _fieldListCombination, _selectionType, _addGroupName)));
            _steps.Add(StepFactory.CreateSorting(TableName, TechnologyCalculationFactory.GetSortingFieldsFormattedOutput(_parametersList, _fieldListCombination, _selectionType, _addGroupName), true));
            _steps.Add(StepFactory.CreateFormatCombine(TableName, TechnologyCalculationFactory.GetCombineFieldsFormattedOutput(_parametersList, _fieldListCombination, _selectionType, _addGroupName), true));

            _steps.Add(StepFactory.CreateFormatBorders(TableName,
                  TechnologyCalculationFactory.GetRightBordersFormattedOutput(),
                  TechnologyCalculationFactory.GetLeftBordersFormattedOutput(_parametersList, _fieldListCombination, _selectionType, _addGroupName),
                  TechnologyCalculationFactory.GetHorizontalBordersFormattedOutput(),
                  TechnologyCalculationFactory.GetChangeBordersFormattedOutput(_selectionType)));
        }

        private void AppendScales()
        {
            _steps.Add(StepFactory.CreateFormatScale(TableName, TechnologyCalculationFactory.GetScaleRulesFormattedOutput(_parametersList, _fieldListCombination, _selectionType, _addGroupName)));
        }
    }
}
