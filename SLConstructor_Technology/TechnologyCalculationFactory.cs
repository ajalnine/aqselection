﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;

namespace SLConstructor_Technology
{
    public static class TechnologyCalculationFactory
    {
        public static List<NewTableData> GetChangeFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Цех", NewFieldName = "Цех"}, 
                new NewTableData{OldFieldName = "Марка полученная", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
                new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Дата", NewFieldName = "Дата"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"} 
            });

            switch (selectionType)
            {
                case SelectionTypes.LinePerProcessing:
                    result.Add(new NewTableData { OldFieldName = "Обработка", NewFieldName = "Обработка" });
                    break;
                case SelectionTypes.LinePerProbe:
                    result.Add(new NewTableData { OldFieldName = "Проба", NewFieldName = "Проба" });
                    break;
            }
            
            foreach (var k in aqpl.AllParameters)
            {
                var category = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (", " + k.ParamType) : string.Empty);

                if (k.ParameterVariants.Count == 0)
                {
                    result.Add(new NewTableData { OldFieldName = k.Param + category, NewFieldName = k.Param + category });
                }
                else
                {
                    string field;
                    switch (selectionType)
                    {
                        case SelectionTypes.LinePerMelt:
                        case SelectionTypes.LinePerMeltRelative:
                        case SelectionTypes.LinePerCard:
                            foreach (var pvd in k.ParameterVariants)
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : "(" + pvd.VariantName + ")");
                                result.Add(new NewTableData { OldFieldName = field, NewFieldName = field });
                            }
                            break;

                        case SelectionTypes.LinePerProcessing:
                            foreach (var probe in k.ParameterVariants.Select(a => a.VariatedValue1).Distinct())
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(probe) ? String.Empty : "(" + probe + ")");
                                result.Add(new NewTableData { OldFieldName = field, NewFieldName = field });
                            }
                            break;

                        case SelectionTypes.LinePerProbe:
                            foreach (var processing in k.ParameterVariants.Select(a => a.VariatedValue2).Distinct())
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(processing) ? String.Empty : "(" + processing + ")");
                                result.Add(new NewTableData { OldFieldName = field, NewFieldName = field });
                            }
                            break;
                    }
                }
            }
            return result;
        }

        public static List<GroupTableData> GetGroupDataForMeltSelectionType(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<GroupTableData>();

            result.AddRange(new List<GroupTableData>
            {
                new GroupTableData{FieldName = "Плавка", IsGrouping = true},
                new GroupTableData{FieldName = "Цех", EmitLast = true},
                new GroupTableData{FieldName = "Дата", EmitLast = true},
                new GroupTableData{FieldName = "Марка полученная", EmitLast = true},
                new GroupTableData{FieldName = "Марка заданная", EmitLast = true},
                new GroupTableData{FieldName = "Марка сертификата", EmitLast = true},
                new GroupTableData{FieldName = "НТД", EmitLast = true},
                new GroupTableData{FieldName = "НТД технологии", EmitLast = true},
                new GroupTableData{FieldName = "Ковш", EmitLast = true},
                new GroupTableData{FieldName = "Разливка", EmitLast = true},
                new GroupTableData{FieldName = "Вес годного", EmitLast = true},
            });
            
            foreach (var k in aqpl.AllParameters)
            {
                var category = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (", " + k.ParamType) : string.Empty);

                if (k.ParameterVariants.Count == 0)
                {
                    result.Add(new GroupTableData { FieldName = k.Param + category, EmitLast = true});
                }
                else
                {
                    foreach (var pvd in k.ParameterVariants)
                    {
                        var field = k.Param + category + (string.IsNullOrEmpty(pvd.VariantName) ? string.Empty : "(" + pvd.VariantName + ")");
                        result.Add(new GroupTableData { FieldName = field, EmitLast = true});
                        if (TechnologySelects.CheckIsRelativeRequired(k) && selectionType == SelectionTypes.LinePerMeltRelative)
                        {
                            field = k.Param + " кг/т" + category + (string.IsNullOrEmpty(pvd.VariantName) ? string.Empty : "(" + pvd.VariantName + ")");
                            result.Add(new GroupTableData { FieldName = field, EmitLast = true });
                        }
                    }
                }
            }
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
                "Плавка",
            });
            switch (selectionType)
            {
                case SelectionTypes.LinePerProcessing:
                    result.Add("Обработка");
                    break;
                case SelectionTypes.LinePerProbe:
                    result.Add("Проба");
                    break;
            }
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
                "Плавка",
            });

            return result;
        }

        public static List<string> GetRightBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата"
            });
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
                "Плавка",
            });
            var oldGroup = string.Empty;
            foreach (var k in aqpl.AllParameters)
            {
                var category = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (", " + k.ParamType) : string.Empty);

                if (k.ParamType == oldGroup)continue;
                oldGroup = k.ParamType;
                
                if (k.ParameterVariants.Count == 0) result.Add(k.Param + category);
                else
                {
                    string field;
                    switch (selectionType)
                    {
                        case SelectionTypes.LinePerMelt:
                        case SelectionTypes.LinePerMeltRelative:
                        case SelectionTypes.LinePerCard:
                            var pvd = k.ParameterVariants.First();
                            field = k.Param + category + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : "(" + pvd.VariantName + ")");
                            result.Add(field);
                            break;

                        case SelectionTypes.LinePerProcessing:
                            var probe = k.ParameterVariants.Select(a => a.VariatedValue1).Distinct().First();
                            field = k.Param + category + (string.IsNullOrEmpty(probe) ? String.Empty : "(" + probe + ")");
                            result.Add(field);
                            break;

                        case SelectionTypes.LinePerProbe:
                            var processing = k.ParameterVariants.Select(a => a.VariatedValue2).Distinct().First();
                            field = k.Param + category + (string.IsNullOrEmpty(processing) ? String.Empty : "(" + processing + ")");
                            result.Add(field);
                            break;
                    }
                }
            }
            return result;
        }

        public static List<string> GetHorizontalBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
            });

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput(SelectionTypes selectionType)
        {
            var result = new List<string>();
            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
            });
            switch (selectionType)
            {
                case SelectionTypes.LinePerProcessing:
                case SelectionTypes.LinePerProbe:
                    result.Add("Плавка");
                    break;
            }
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName)
        {
            var result = new List<ScaleRule>();


            foreach (var k in aqpl.AllParameters.Where(a=>a.IsNumber))
            {
                var category = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (", " + k.ParamType) : string.Empty);

                if (k.ParameterVariants.Count == 0)
                {
                    result.Add(new ScaleRule
                            {
                                Mode = ScaleRuleMode.MinMax,
                                FieldName = k.Param + category,
                                Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                            });
                }
                else
                {
                    string field;
                    switch (selectionType)
                    {
                        case SelectionTypes.LinePerMelt:
                        case SelectionTypes.LinePerMeltRelative:
                        case SelectionTypes.LinePerCard:
                            foreach (var pvd in k.ParameterVariants)
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : "(" + pvd.VariantName + ")");
                                result.Add(new ScaleRule
                                {
                                    Mode = ScaleRuleMode.MinMax,
                                    FieldName = field,
                                    Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                    Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                    Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                                });
                            }
                            break;

                        case SelectionTypes.LinePerProcessing:
                            foreach (var probe in k.ParameterVariants.Select(a => a.VariatedValue1).Distinct())
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(probe) ? String.Empty : "(" + probe + ")");
                                result.Add(new ScaleRule
                                {
                                    Mode = ScaleRuleMode.MinMax,
                                    FieldName = field,
                                    Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                    Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                    Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                                });
                            }
                            break;

                        case SelectionTypes.LinePerProbe:
                            foreach (var processing in k.ParameterVariants.Select(a => a.VariatedValue2).Distinct())
                            {
                                field = k.Param + category + (string.IsNullOrEmpty(processing) ? String.Empty : "(" + processing + ")");
                                result.Add(new ScaleRule
                                {
                                    Mode = ScaleRuleMode.MinMax,
                                    FieldName = field,
                                    Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                    Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                    Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                                });
                            }
                            break;
                    }
                }
            }
            return result;
        }
    }
}