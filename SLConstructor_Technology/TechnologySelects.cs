﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;

namespace SLConstructor_Technology
{
    public static class TechnologySelects
    {
        private static List<int> RelativePossibleParamTypes = new List<int>
        {
            771, 772, 773, 770, 756, 757, 812, 755, 753, 758, 754, 781, 780, 774, 765, 767, 816, 764, 766, 769, 768, 760, 759
        };

        public static string GetTechnologySQL(Filters filters, ParametersList aqpl, FieldListCombination flc, SelectionTypes currentSelectionType, bool addGroupName)
        {
            var sqlOther = string.Empty;
            var havingOther = string.Empty;

            if (aqpl.AllParameters.Count <= 0) return $"{sqlOther}{" ORDER BY max(Technology_Smelts.SmeltNumber) "}";
            sqlOther = @"SELECT max(Technology_Smelts.SmeltNumber) as 'Плавка', 
                                max(Technology_WorkShops.WorkShop) as 'Цех',
                                max(Technology_Smelts.RegisterDate) as 'Дата', 
                                max(Common_Mark.mark) as 'Марка полученная', 
                                max(Common_Mark2.mark) as 'Марка заданная', 
                                max(Common_Mark3.mark) as 'Марка сертификата', 
                                max(Common_NTD.ntd) as 'НТД', 
                                max(Common_NTD2.ntd) as 'НТД технологии', 
                                max(Technology_Smelts.ScoopNumber) as 'Ковш', 
                                max(Technology_BottlingTypes.BottlingType) as 'Разливка', 
                                round(max(Technology_Smelts.Suitable),0) as 'Вес годного'";
            switch (currentSelectionType)
            {
                case SelectionTypes.LinePerProcessing:
                    sqlOther += ", max(Technology_Values.ProcessingNumber) as 'Обработка' ";
                    break;
                case SelectionTypes.LinePerProbe:
                    sqlOther += ", max(Technology_Values.OptionalCategory) as 'Проба' ";
                    break;
            }
                
            foreach (var k in aqpl.AllParameters)
            {
                var category = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (", "+ k.ParamType ) : "");

                if (k.ParameterVariants.Count == 0)
                {
                    ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlOther, ref havingOther, k, " IS NULL ", " IS NULL ", category, currentSelectionType);
                }
                else
                {
                    string categoryName;
                    string processingCompare;
                    string probeCompare;
                    switch (currentSelectionType)
                    {
                        case SelectionTypes.LinePerMelt:
                        case SelectionTypes.LinePerMeltRelative:
                        case SelectionTypes.LinePerCard:
                            foreach (var pvd in k.ParameterVariants)
                            {
                                probeCompare = (pvd.VariatedValue1 != null)?"='"+pvd.VariatedValue1+"' ":" IS NULL ";
                                processingCompare = (pvd.VariatedValue2 != null) ? $"='{pvd.VariatedValue2}' "
                                    : " IS NULL ";
                                categoryName = category + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : "(" + pvd.VariantName + ")");
                                ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlOther, ref havingOther, k, probeCompare, processingCompare, categoryName, currentSelectionType);
                            }
                            break;

                        case SelectionTypes.LinePerProcessing:
                            foreach (var probe in k.ParameterVariants.Select(a=>a.VariatedValue1).Distinct())
                            {
                                processingCompare = string.Empty;
                                probeCompare = (probe != null) ? $"='{probe}' " : " IS NULL ";
                                categoryName = category + (string.IsNullOrEmpty(probe) ? String.Empty : "(" + probe + ")");
                                ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlOther, ref havingOther, k, probeCompare, processingCompare, categoryName, currentSelectionType);
                            }
                            break;
                                
                        case SelectionTypes.LinePerProbe:
                            foreach (var processing in k.ParameterVariants.Select(a=>a.VariatedValue2).Distinct())
                            {
                                processingCompare = (processing != null) ? "='" + processing + "' " : " IS NULL ";
                                probeCompare = string.Empty;
                                categoryName = category + (string.IsNullOrEmpty(processing) ? String.Empty : "(" + processing + ")");
                                ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlOther, ref havingOther, k, probeCompare, processingCompare, categoryName, currentSelectionType);
                            }
                            break;
                    }
                }
            }
                
            sqlOther += @" FROM Technology_Smelts 
                                  INNER JOIN Technology_Values ON Technology_Smelts.SmeltID = Technology_Values.SmeltID
                                  LEFT OUTER JOIN Common_Mark ON Technology_Smelts.MarkID = Common_Mark.id 
                                  LEFT OUTER JOIN Common_NTD ON Technology_Smelts.NtdID = Common_NTD.id 
                                  LEFT OUTER JOIN Common_Mark as Common_Mark2 ON Technology_Smelts.RequestedMarkID = Common_Mark2.id 
                                  LEFT OUTER JOIN Common_Mark as Common_Mark3 ON Technology_Smelts.CertMarkID = Common_Mark3.id 
                                  LEFT OUTER JOIN Common_NTD as Common_NTD2 ON Technology_Smelts.TechnologyNtdID = Common_NTD2.id 
                                  LEFT OUTER JOIN Technology_WorkShops ON Technology_Smelts.WorkShopID = Technology_WorkShops.ID
                                  LEFT OUTER JOIN Technology_BottlingTypes ON Technology_Smelts.BottlingTypeID = Technology_BottlingTypes.ID
                                  INNER JOIN Common_Parameters ON Common_Parameters.id=Technology_Values.ParameterID
                                  INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id
                                  WHERE " + Security.GetMarkSQLCheck("Выплавка") + " AND "
                        + Security.GetMarkSQLCheck("Выплавка").Replace("Common_Mark", "Common_Mark2") + " AND "
                        + Security.GetMarkSQLCheck("Выплавка").Replace("Common_Mark", "Common_Mark3") + " AND "
                        + Security.GetParametersCheck() + " AND "
                        + filters.SaveFilter;

            switch (currentSelectionType)
            {
                case SelectionTypes.LinePerCard:
                    sqlOther += " GROUP BY Technology_Smelts.SmeltID ";
                    if (havingOther.Length > 0) sqlOther += " HAVING " + havingOther;
                    sqlOther += " ORDER BY Technology_Smelts.SmeltID ";
                    break;
                case SelectionTypes.LinePerMelt:
                case SelectionTypes.LinePerMeltRelative:
                    sqlOther += " GROUP BY Technology_Smelts.SmeltID";
                    if (havingOther.Length > 0) sqlOther += " HAVING " + havingOther;
                    sqlOther += " ORDER BY Technology_Smelts.SmeltID ";
                    break;
                case SelectionTypes.LinePerProcessing:
                    sqlOther += " GROUP BY Technology_Smelts.SmeltID, Technology_Values.ProcessingNumber";
                    if (havingOther.Length > 0) sqlOther += " HAVING " + havingOther;
                    sqlOther += " ORDER BY Technology_Smelts.SmeltID ";
                    break;
                case SelectionTypes.LinePerProbe:
                    sqlOther += " GROUP BY Technology_Smelts.SmeltID, Technology_Values.OptionalCategory ";
                    if (havingOther.Length > 0) sqlOther += " HAVING " + havingOther;
                    sqlOther += " ORDER BY Technology_Smelts.SmeltID ";
                    break;
            } 
                
            return sqlOther;
        }


        public static bool CheckIsRelativeRequired(ParamDescription k)
        {
            return RelativePossibleParamTypes.Contains(k.ParamTypeID);
        }

        private static void ConstructSingleParameterSQL(string valueFilter, FieldListCombination flc, ref string sqlOther, ref string havingOther, ParamDescription k, string probe, string processingNumber, string fieldSubName, SelectionTypes currentSelectionType)
        {
            if (k.IsNumber)
            {
                if (!FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID))
                {
                    if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerProcessing:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.OptionalCategory {probe}  then  dbo.ToNumeric(Technology_Values.Value) else null end) as \'{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerProbe:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.ProcessingNumber {processingNumber} then  dbo.ToNumeric(Technology_Values.Value) else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerCard:
                            case SelectionTypes.LinePerMelt:
                            case SelectionTypes.LinePerMeltRelative:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.OptionalCategory {probe} and Technology_Values.ProcessingNumber {processingNumber} then  dbo.ToNumeric(Technology_Values.Value) else null end) as '{k.Param}{fieldSubName}' \r\n";
                                if (CheckIsRelativeRequired(k) && currentSelectionType == SelectionTypes.LinePerMeltRelative)
                                {
                                    sqlOther +=
                                        $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.OptionalCategory {probe} and Technology_Values.ProcessingNumber {processingNumber} then  round(dbo.ToNumeric(Technology_Values.Value)/Technology_Smelts.Suitable*1000, 5) else null end) as '{k.Param + " кг/т"}{fieldSubName}' \r\n";
                                }

                                break;
                        } 
                    }
                    else
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerProcessing:
                                sqlOther +=
                                    $",max(case when Technology_Values.OptionalCategory {probe}  then dbo.MultipleToNumeric(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerProbe:
                                sqlOther +=
                                    $",max(case when Technology_Values.ProcessingNumber {processingNumber}  then dbo.MultipleToNumeric(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerCard:
                            case SelectionTypes.LinePerMelt:
                            case SelectionTypes.LinePerMeltRelative:
                                sqlOther +=
                                    $",max(case when Technology_Values.ProcessingNumber {processingNumber} and Technology_Values.OptionalCategory {probe}  then dbo.MultipleToNumeric(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                        } 
                    }
                }
                var hs = FilterUtility.GetValueFilterString(valueFilter, k.ParamID);
                if (string.IsNullOrEmpty(hs)) return;
                if (havingOther != String.Empty) havingOther += " AND ";
                havingOther += hs;
            }
            else
            {
                if (!FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID))
                {
                    if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerProcessing:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.OptionalCategory {probe} then Technology_Values.Value else null end) as '{k.Param}{fieldSubName}'\r\n";
                                break;
                            case SelectionTypes.LinePerProbe:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and  Technology_Values.ProcessingNumber {processingNumber}  then Technology_Values.Value else null end) as '{k.Param}{fieldSubName}'\r\n";
                                break;
                            case SelectionTypes.LinePerCard:
                            case SelectionTypes.LinePerMelt:
                            case SelectionTypes.LinePerMeltRelative:
                                sqlOther +=
                                    $",max(case when Technology_Values.ParameterID='{k.ParamID}' and Technology_Values.OptionalCategory {probe} and  Technology_Values.ProcessingNumber {processingNumber}  then Technology_Values.Value else null end) as '{k.Param}{fieldSubName}'\r\n";
                                break;
                        } 
                    }
                    else
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerProcessing:
                                sqlOther +=
                                    $",max(case when Technology_Values.OptionalCategory {probe}  then dbo.MultipleToString(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerProbe:
                                sqlOther +=
                                    $",max(case when Technology_Values.ProcessingNumber {processingNumber}  then dbo.MultipleToString(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                            case SelectionTypes.LinePerCard:
                            case SelectionTypes.LinePerMelt:
                            case SelectionTypes.LinePerMeltRelative:
                                sqlOther +=
                                    $",max(case when Technology_Values.ProcessingNumber {processingNumber} and Technology_Values.OptionalCategory {probe}  then dbo.MultipleToString(Technology_Values.Value, Technology_Values.ParameterID , '{FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID)}') else null end) as '{k.Param}{fieldSubName}' \r\n";
                                break;
                        }
                    }
                }
            }
        }
    }
}