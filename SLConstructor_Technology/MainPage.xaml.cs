﻿using System;
using System.Collections.Generic;
using System.Windows;
using AQControlsLibrary;
using AQConstructorsLibrary;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLConstructor_Technology
{
    public partial class MainPage : IAQModule
    {
        #region Переменные

        public void InitSignaling(CommandCallBackDelegate ccbd) {
        }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;

        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;
        private readonly AQModuleDescription _aqmd;
        
        private ParametersList _parametersList;
        private FieldListCombination _fieldListCombination;
        private Filters _filters;
        private string _currentSQL = string.Empty;
        private bool _showFvc;
        private bool _addGroupName;
        private bool _formatted;
        private bool _colored;
        private SelectionTypes _selectionType = SelectionTypes.LinePerMelt;
        private List<Step> _steps;
        
        #endregion

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name }; 
            ColumnsPresenter.SelectionParameters = new TechnologyParametersList();
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
        }

        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery(true);
        }

        private void RefreshQuery(bool full)
        {
            RefreshFlags();
            RefreshFilters(full);
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;
            var task = new Task(new[] { "Формирование списка полей", "Слияние полей" }, "Проверка условий", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cs.BeginGetTechnologyParametersList(_filters.ParametersFilter, GetTechnologyParametersListDone, task);
        }

        private void RefreshFlags()
        {
            if (RowIsMelt.IsChecked != null && RowIsMelt.IsChecked.Value) _selectionType = SelectionTypes.LinePerMelt;
            if (RowIsMeltRelative.IsChecked != null && RowIsMeltRelative.IsChecked.Value) _selectionType = SelectionTypes.LinePerMeltRelative;
            if (RowIsProcessing.IsChecked != null && RowIsProcessing.IsChecked.Value) _selectionType = SelectionTypes.LinePerProcessing;
            if (RowIsProbe.IsChecked != null && RowIsProbe.IsChecked.Value) _selectionType = SelectionTypes.LinePerProbe;
            if (RowIsCard.IsChecked != null && RowIsCard.IsChecked.Value) _selectionType = SelectionTypes.LinePerCard;
            _showFvc = ShowFieldCombineEditorCheckBox.IsChecked != null && ShowFieldCombineEditorCheckBox.IsChecked.Value;
            _addGroupName = AddGroupNameCheckBox.IsChecked != null && AddGroupNameCheckBox.IsChecked.Value;
            _formatted = NoFormatButton.IsChecked != null && !NoFormatButton.IsChecked.Value;
            _colored = ScaleButton.IsChecked != null && ScaleButton.IsChecked.Value;
        }

        private void RefreshFilters(bool full)
        {
            _filters = full ? ColumnsPresenter.GetFilters() : ColumnsPresenter.UpdateFilters(_filters);
        }

        private void GetTechnologyParametersListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetTechnologyParametersList(iar);
            _parametersList = t.ParametersList;
            if (t.Success)
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                task.SetMessage(t.Message);
                _cs.BeginGetTechnologyCorrectionList(_filters.ParametersFilter, _parametersList, _selectionType == SelectionTypes.LinePerProcessing, _selectionType == SelectionTypes.LinePerMelt, GetTechnologyCorrectionListDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() => ColumnsPresenter.EnableFinishSelection(true));
        }

        private void GetTechnologyCorrectionListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetTechnologyCorrectionList(iar);
            _fieldListCombination = t.FieldListCombination;
            WorkParametersList.ParametersList = _parametersList;
            if (t.Success)
            {
                task.SetState(1, TaskState.Ready);
                task.SetMessage(t.Message);
            }
            else
            {
                task.SetState(1, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() =>
            {
                EnableSaveAndExport(true);
                RefreshQueryButton.IsEnabled = true;
                ColumnsPresenter.EnableFinishSelection(true);
                MainFieldCombineView.PL = _parametersList;
                MainFieldCombineView.FLC = _fieldListCombination;
                MainFieldCombineView.Refresh();
                MainFieldCombineView.Visibility = _showFvc ? Visibility.Visible : Visibility.Collapsed;
            });
        }

        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery(false);
        }

        #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(() =>
            {
                XlsxExportButton.IsEnabled = allow;
                PreviewButton.IsEnabled = allow;
                SaveQueryButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
            });
        }

        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }
    }
    public enum SelectionTypes {LinePerMelt, LinePerMeltRelative, LinePerProcessing, LinePerProbe, LinePerCard };
}
