﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQCatalogueLibrary;
using AQChartViewLibrary;
using AQControlsLibrary;
using AQSelection;

namespace SLReport
{
    public partial class MainPage : IAQModule
    {
        private CalculationService _cs;
        private readonly AQModuleDescription _aqmd;
        CommandCallBackDelegate _readySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { _readySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool _readyForAcceptSignalsProcessed;
        private int _slideReadyCounter = 0;
        private CalculationDescription _presentedCalculationDescription;

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name, URL = "SLReport.MainPage" };
            Loader.TabControlSizeChanged += Loader_TabControlSizeChanged;
        }

        private void Loader_TabControlSizeChanged(object o, TabSizeChangedEventArgs e)
        {
            LayoutRoot.Width =  e.NewSize.Width;
            MainSlidePanel.Height = Loader.MainTabSize.Height - 30;
            SlidePanelBorder.MaxWidth = LayoutRoot.Width - 80;
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            _aqmd.ShortDescription = _presentedCalculationDescription?.Name;
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object o, CommandCallBackDelegate commandCallBack)
        {
            switch (commandType)
            {
                case Command.InsertReport:

                    var ca = o as Dictionary<string, object>;

                    List<Step> sourceSteps = null;
                    Step operation = null;
                    DateTime? from = null, to = null;
                    var description = string.Empty;
                    List<InputDescription> parameters = null;
                    CalculationDescription cd = null;

                    if (ca != null)
                    {
                        if (ca.ContainsKey("From")) from = (DateTime) ca["From"];
                        if (ca.ContainsKey("To")) to = (DateTime) ca["To"];
                        if (ca.ContainsKey("Calculation")) sourceSteps = (List<Step>) ca["Calculation"];
                        if (ca.ContainsKey("Description")) description = (string) ca["Description"];
                        if (ca.ContainsKey("Parameters")) parameters = (List<InputDescription>) ca["Parameters"];
                        if (ca.ContainsKey("CD")) cd = (CalculationDescription) ca["CD"];
                    }

                    CreateReport(description, sourceSteps, cd, from, to, parameters);
                    break;
            }
            commandCallBack?.Invoke(commandType, o, null);
        }

        private void CreateReport(string description, List<Step> sourceSteps, CalculationDescription cd, DateTime? @from,
            DateTime? to, List<InputDescription> parameters)
        {
            MainSlidePanel.Clear();
            MainSlidePanel.UpdateLayout();
            MainSlidePanel.SetDescription(description);
            var stepNames = sourceSteps?.Select(a => a.Name).ToArray().Concat(new[] {"Прием данных"}).ToArray();

            var task = new Task(stepNames, "Формирование отчета", TaskPanel);

            var reporter = new Reporter(_cs, task, cd, true, @from, to, null);
            reporter.AttachToSlidePanel(MainSlidePanel);
            MainSlidePanel.EnableButtons(false);
            _cs.BeginInitCalculation(@from.Value, to.Value, parameters,
                iar =>
                {
                    var sr = _cs.EndInitCalculation(iar);
                    task = (Task) iar.AsyncState;
                    task.CustomData = sr.TaskDataGuid;

                    try
                    {
                        reporter.DoServerSteps(() =>
                        {
                            var taskGuid = task.CustomData.ToString();
                            task.SetState(task.CurrentStep, TaskState.Processing);
                            Dispatcher.BeginInvoke(() => _cs.BeginGetTablesSLCompatible(taskGuid, i =>
                            {
                                var data = _cs.EndGetTablesSLCompatible(i);
                                if (data == null) return;
                                task.AdvanceProgress();
                                task.SetMessage("Данные готовы");
                                Dispatcher.BeginInvoke(() =>
                                {
                                    TaskPanel.Children.Clear();
                                    _slideReadyCounter = MainSlidePanel.InternalSlideCollection.Count;
                                    foreach (var msp in MainSlidePanel.InternalSlideCollection) msp.SourceData = data;
                                    MainSlidePanel.EnableButtons(true);
                                    reporter.DetachFromSlidePanel();
                                    Services.GetAQService().BeginWriteLog("Report created in SLReport: " + cd.Name, null, null);
                                    AllSlidesReady();
                                });
                            }, task));
                        });
                    }
                    catch (Exception)
                    {
                        task.SetState(task.CurrentStep, TaskState.Error);
                        task.SetMessage("Ошибка");
                        reporter.DetachFromSlidePanel();
                    }
                }, task);
        }

        private void AutoOpenReport(int reportId)
        {
            CatalogueOperations.GetItem(reportId, (e, s) =>
            {
                var CD = CatalogueOperations.ItemToCalculationDescription(e.Item);
                string from, to, days;
                HtmlPage.Document.QueryString.TryGetValue("From", out from);
                HtmlPage.Document.QueryString.TryGetValue("To", out to);
                HtmlPage.Document.QueryString.TryGetValue("Days", out days);
                var From = string.IsNullOrEmpty(from) ? DateTime.Now - TimeSpan.FromDays(7) : DateTime.ParseExact(from, "dd.MM.yyyy", null);
                var To = string.IsNullOrEmpty(to) ? DateTime.Now : DateTime.ParseExact(to, "dd.MM.yyyy", null);
                var Days = string.IsNullOrEmpty(days) ? 0 : int.Parse(days);
                if (Days >0 ) From = To - TimeSpan.FromDays(Days);
                CreateReport(e.Item.RevisionDescription, CD.StepData, CD, From , To, null);
            }, null);
        }

        private void LayoutRoot_OnLoaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Width = Loader.MainTabSize.Width;
            MainSlidePanel.Height = Loader.MainTabSize.Height - 30;
            LayoutRoot.Visibility = Visibility.Visible;
            if (_readyForAcceptSignalsProcessed) return;
            _cs = Services.GetCalculationService();

            ReadyForAcceptSignals?.Invoke(this, new ReadyForAcceptSignalsEventArg { CommandCallBack = _readySignalCallBack });
            _readyForAcceptSignalsProcessed = true;

            var openReportId = string.Empty;
            HtmlPage.Document.QueryString.TryGetValue("OpenReportID", out openReportId);
            var reportId = -1;
            if (int.TryParse(openReportId, out reportId)) AutoOpenReport(reportId);
        }

        private void AutoSendReport(string auto)
        {
            switch (auto)
            {
                case "PPTX":
                    MainSlidePanel.SendPPTX();
                    break;
                case "DOCX":
                    MainSlidePanel.SendDOCX();
                    break;
                case "XLSX":
                    MainSlidePanel.SendXLSX();
                    break;
                case "PPTXXLSX":
                    MainSlidePanel.SendPPTXXLSX();
                    break;
            }
        }

        private void MainSlidePanel_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            TaskPanel.Width = LayoutRoot.ActualWidth - MainSlidePanel.Width / 2 - 40;
        }

        private void AllSlidesReady()
        {
            var auto = "";
            if (HtmlPage.Document.QueryString.TryGetValue("Auto", out auto))
            {
                DispatcherTimer _timer = new DispatcherTimer();
                _timer.Interval = TimeSpan.FromSeconds(1);
                _timer.Tick += (s, d) =>
                {
                    if (MainSlidePanel.InternalSlideCollection.All(a => a.IsReady))
                    {
                        AutoSendReport(auto);
                        _timer.Stop();
                    }
                };
                _timer.Start();
            }
        }
    }
}
