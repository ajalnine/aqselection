﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static List<Step> CreateDeleteTable(string tableName)
        {
            var currentParameters = new DeleteParameters {TablesToDelete = new List<string> {tableName}};

            return TransformTables(currentParameters);
        }

        public static IEnumerable<Step> CreateRenameTable(IEnumerable<SLDataTable> sourceTables, string tableName,
            string newName)
        {
            var currentParameters = new DeleteParameters {TablesTransformation = new List<NewTableData>()};

            foreach (var c in sourceTables)
            {
                currentParameters.TablesTransformation.Add(c.TableName == tableName
                    ? new NewTableData {OldTableName = c.TableName, NewTableName = newName}
                    : new NewTableData {OldTableName = c.TableName, NewTableName = c.TableName});
            }

            return TransformTables(currentParameters);
        }

        public static IEnumerable<Step> CreateRenameTables(IEnumerable<SLDataTable> sourceTables,
            Dictionary<string, string> tableName)
        {
            var currentParameters = new DeleteParameters {TablesTransformation = new List<NewTableData>()};

            foreach (var c in sourceTables)
            {
                currentParameters.TablesTransformation.Add(tableName.ContainsKey(c.TableName)
                    ? new NewTableData {OldTableName = c.TableName, NewTableName = tableName[c.TableName]}
                    : new NewTableData {OldTableName = c.TableName, NewTableName = c.TableName});
            }

            return TransformTables(currentParameters);
        }

        public static IEnumerable<Step> CreateReorderTables(IEnumerable<string> orderedNames)
        {
            var currentParameters = new DeleteParameters {TablesTransformation = new List<NewTableData>()};

            foreach (var s in orderedNames)
            {
                currentParameters.TablesTransformation.Add(new NewTableData {OldTableName = s, NewTableName = s});
            }
            return TransformTables(currentParameters);
        }

        private static List<Step> TransformTables(DeleteParameters currentParameters)
        {
            var steps = new List<Step>();
            var xs = new XmlSerializer(typeof (DeleteParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_DeleteTables",
                EditorAssemblyPath = "SLCalc_DeleteTables",
                Group = "Преобразование",
                Name = "Управление таблицами",
                ImagePath = "/AQResources;component/Images/Modules/DeleteTable.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("DeleteParameters", "Parameters");
            steps.Add(currentStep);
            Counter++;
            return steps;
        }
    }
    public class DeleteParameters
    {
        public List<string> TablesToDelete;
        public List<NewTableData> TablesTransformation;
    }
}
