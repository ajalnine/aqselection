﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateRecalcFields(string tableName, List<RecalcFieldDescription> expressionFields)
        {
            var currentParameters = new RecalcFieldsParameters
            {
                TableName = tableName,
                ExpressionFields = expressionFields
            };
            var xs = new XmlSerializer(typeof(RecalcFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_RecalcFields",
                EditorAssemblyPath = "SLCalc_RecalcFields",
                Group = "Расчеты",
                Name = "Изменение группы полей",
                ImagePath = "/AQResources;component/Images/Modules/RecalcFields.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("RecalcFieldsParameters", "Parameters");
            return currentStep;
        }

        public static bool AppendRecalcFields(Step stepToAppend, string tableName, List<RecalcFieldDescription> expressionFields)
        {
            var xmlToChange = stepToAppend.ParametersXML.Replace("Parameters", "RecalcFieldsParameters");

            var xd = new XmlSerializer(typeof(RecalcFieldsParameters));
            var sr = new StringReader(xmlToChange);
            var oldParameters = (RecalcFieldsParameters)xd.Deserialize(XmlReader.Create(sr));
            if (oldParameters.TableName != tableName) return false;
            oldParameters.ExpressionFields.AddRange(expressionFields);
            var xs = new XmlSerializer(typeof(RecalcFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), oldParameters);
            stepToAppend.ParametersXML = sb.ToString().Replace("RecalcFieldsParameters", "Parameters");
            return true;
        }
    }


    public class RecalcFieldsParameters
    {
        public string TableName;
        public List<RecalcFieldDescription> ExpressionFields;
    }
    public class RecalcFieldDescription
    {
        public string FieldName;
        public string Expression;
        public string Code;
    }
}
