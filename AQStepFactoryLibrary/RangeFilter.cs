﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary
{
    public static partial class StepFactory
    {
        public static Step CreateRangeFilter(string tableName, string groupField, string groups, List<Range> ranges, bool include, bool Case, bool all)
        {
            var currentParameters = new RangeFilterParameters
            {
                TableName = tableName,
                GroupField = groupField,
                Groups = groups,
                Ranges = new ObservableCollection<Range>(ranges),
                Include = include,
                Case = Case,
                All = all
            };
            var xs = new XmlSerializer(typeof(RangeFilterParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_RangeFilter",
                EditorAssemblyPath = "SLCalc_RangeFilter",
                Group = "Фильтрация",
                Name = "Числовой фильтр",
                ImagePath = "/AQResources;component/Images/Modules/RangeFilter.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("RangeFilterParameters", "Parameters");
            return currentStep;
        }
    }
    public class RangeFilterParameters
    {
        public string TableName;
        public string GroupField;
        public string Groups;
        public bool Include;
        public bool Case;
        public bool All;
        public ObservableCollection<Range> Ranges;
    }
}
