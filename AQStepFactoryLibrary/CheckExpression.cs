﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateCheckExpression(string tableName, string fieldName, string fieldType, string expression, string code, string insertAfter)
        {
            var currentParameters = new CheckExpressionParameters
            {
                TableName = tableName,
                FieldName = fieldName,
                FieldType = fieldType,
                InsertAfter = insertAfter,
                Expression = expression,
                Code = code
            };
            var xs = new XmlSerializer(typeof(CheckExpressionParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_CheckExpression",
                EditorAssemblyPath = "SLCalc_CheckExpression",
                Group = "Фильтрация",
                Name = "Проверка выражений",
                ImagePath = "/AQResources;component/Images/Modules/CheckExpression.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("CheckExpressionParameters", "Parameters");
            return currentStep;
        }
    }
    public class CheckExpressionParameters
    {
        public string TableName;
        public string Expression;
        public string Code;
        public string InsertAfter;
        public string FieldName;
        public string FieldType;
    }
}
