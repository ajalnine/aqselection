﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateLayerOperationCutIntoTable(string tableName, string layerField, string argument, List<object> layers)
        {
            return CreateLayerOperation(tableName, layerField, null, SetLayerInteractiveOperations.CutInNewTable, argument, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationCopyIntoTable(string tableName, string layerField, string argument, List<object> layers)
        {
            return CreateLayerOperation(tableName, layerField, null, SetLayerInteractiveOperations.CopyToNewTable, argument, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationDeleteCases(string tableName, string layerField, List<object> layers)
        {
            return CreateLayerOperation(tableName, layerField, null, SetLayerInteractiveOperations.DeleteCase, null, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationDeleteValues(string tableName, string layerField, string yField, List<object> layers)
        {
            return CreateLayerOperation(tableName, layerField, yField, SetLayerInteractiveOperations.DeleteValue, null, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationSetLabel(string tableName, string layerField, string yField, List<object> layers, string label)
        {
            return CreateLayerOperation(tableName, layerField, yField, SetLayerInteractiveOperations.SetLabel, label, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationCombineLayers(string tableName, string layerField, string yField, List<object> layers, object newValue)
        {
            return CreateLayerOperation(tableName, layerField, yField, SetLayerInteractiveOperations.CombineLayers, newValue, layers, ColorMode.SetForValue, Colors.Transparent);
        }

        public static Step CreateLayerOperationSetColor(string tableName, string layerField, string yField, List<object> layers, Color color, ColorMode colorMode)
        {
            return CreateLayerOperation(tableName, layerField, yField, SetLayerInteractiveOperations.SetColor, null, layers, colorMode, color);
        }
        
        private static Step CreateLayerOperation(string tableName, string layerField, string yField, SetLayerInteractiveOperations command,  object argument, List<object> layers, ColorMode colorMode, Color color)
        {
            var currentParameters = new SetLayersParameters
            {
                TableName = tableName,
                LayerField = layerField,
                Layers = layers,
                Operation = command,
                OptionalCommandArgument = argument,
                ColorMode = colorMode,
                YField = yField,
                Color = color
            };
            var xs = new XmlSerializer(typeof(SetLayersParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Layers",
                EditorAssemblyPath = "SLCalc_Layers",
                Group = "Расчеты",
                Name = "Операции над слоями",
                ImagePath = "/AQResources;component/Images/Modules/Layers.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SetLayersParameters", "Parameters");
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SetLayerInteractiveOperations", "InteractiveOperations");
            return currentStep;
        }
    }
    public class SetLayersParameters
    {
        public string TableName;
        public string LayerField;
        public string YField;
        public SetLayerInteractiveOperations Operation;
        public object OptionalCommandArgument;
        public List<object> Layers;
        public ColorMode ColorMode;
        public Color Color;
    }

    public enum ColorMode
    {
        SetForCase, SetForValue, ResetFromCases, ResetFromValues
    }

    [Flags]
    public enum SetLayerInteractiveOperations
    {
        None = 0,
        DeleteValue = 1,
        DeleteCase = 2,
        CutInNewTable = 4,
        CopyToNewTable = 8,
        SetColor = 0x10,
        SetColorForRow = 0x20,
        SetLabel = 0x40,
        SplitLayers = 0x80,
        DeleteColumn = 0x100,
        SetFilter = 0x200,
        DeleteFilter = 0x400,
        RemoveFilters = 0x800,
        CombineLayers = 0x1000,
        Outliers = 0x2000,
        OutOfRange = 0x4000,
        All = 0xffff
    }
}
