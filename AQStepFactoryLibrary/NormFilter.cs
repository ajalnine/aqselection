﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary
{
    public static partial class StepFactory
    {
        public static Step CreateNormFilter(string tableName, bool include, bool Case, bool all, bool inRanges, string field)
        {
            var currentParameters = new NormFilterParameters
            {
                TableName = tableName,
                Include = include,
                Case = Case,
                All = all,
                InRanges = inRanges,
                Field = field
            };
            var xs = new XmlSerializer(typeof(NormFilterParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_NormFilter",
                EditorAssemblyPath = "SLCalc_NormFilter",
                Group = "Фильтрация",
                Name = "Фильтр границ",
                ImagePath = "/AQResources;component/Images/Modules/NormFilter.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("NormFilterParameters", "Parameters");
            return currentStep;
        }
    }

    public class NormFilterParameters
    {
        public string TableName;
        public bool Include;
        public bool Case;
        public bool All;
        public bool InRanges;
        public string Field;
    }
}
