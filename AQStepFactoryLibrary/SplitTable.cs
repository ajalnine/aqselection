﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateSplitTable(string tableName, string categoryFieldName, string outputTablePrefix, List<string> categories, bool removeCategory, bool removeTable)
        {
            var currentParameters = new SplitTableParameters
            {
                TableName = tableName, 
                Categories = categories,
                CategoryFieldName = categoryFieldName,
                OutputTablePrefix = outputTablePrefix,
                RemoveCategory = removeCategory,
                RemoveTable = removeTable
            };
            var xs = new XmlSerializer(typeof(SplitTableParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_SplitTable",
                EditorAssemblyPath = "SLCalc_SplitTable",
                Group = "Преобразование",
                Name = "Деление таблицы по значению поля",
                ImagePath = "/AQResources;component/Images/Modules/SplitTable.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SplitTableParameters", "Parameters");
            return currentStep;
        }
    }

    public class SplitTableParameters
    {
        public string TableName;
        public string CategoryFieldName;
        public string OutputTablePrefix;
        public List<string> Categories;
        public bool RemoveCategory;
        public bool RemoveTable;
    }
}
