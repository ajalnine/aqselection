﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static List<Step> CreateTable(SLDataTable sldt)
        {
            var steps = new List<Step>();
            var currentStep = new Step();
            var currentParameters = new TableParameters
            {
                TableName = sldt.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColoredColumns = new List<string>(),
                Data = new List<Row>(),
                ColumnWidth = new List<double>()
            };

            var outputColumnNames = sldt.ColumnNames.Where(a => !a.EndsWith("Цвет")).ToList();
            currentParameters.ColumnNames = outputColumnNames;
            var rnames = new Row {RowData = new List<Cell>()};

            if (!sldt.ColumnNames.Any(a => a.EndsWith("Цвет")))
            {
                bool columnFirst = true;

                var raw = new StringBuilder();
                foreach (var columnName in outputColumnNames)
                {
                    var i = sldt.ColumnNames.IndexOf(columnName);
                    var dataType = sldt.DataTypes[i];
                    currentParameters.DataTypes.Add(dataType.Replace("System.", String.Empty));
                    currentParameters.ColumnWidth.Add(70);
                    if (!columnFirst) raw.Append("\t");
                    raw.Append(columnName);
                    columnFirst = false;
                }
                string value;
                foreach (var row in sldt.Table)
                {
                    columnFirst = true;
                    raw.Append("\r\n");
                    foreach (var c in row.Row)
                    {
                        if (!columnFirst) raw.Append("\t");
                        value = c != null ? c.ToString() : string.Empty;
                        columnFirst = false;
                        raw.Append(c);
                    }
                }
                currentParameters.TSV = raw.ToString();
                currentParameters.Data = null;
            }
            else
            {

                foreach (var columnName in outputColumnNames)
                {
                    var i = sldt.ColumnNames.IndexOf(columnName);
                    var dataType = sldt.DataTypes[i];
                    currentParameters.DataTypes.Add(dataType.Replace("System.", String.Empty));
                    currentParameters.ColumnWidth.Add(70);
                    var c = new Cell { Value = columnName };
                    rnames.RowData.Add(c);
                }
                currentParameters.Data.Add(rnames);

                foreach (var row in sldt.Table)
                {
                    var newRow = new Row { RowData = new List<Cell>() };
                    foreach (var columnName in outputColumnNames)
                    {
                        var i = sldt.ColumnNames.IndexOf(columnName);
                        var colorIndex = sldt.ColumnNames.IndexOf("#" + columnName + "_Цвет");
                        var value = row.Row[i] != null ? row.Row[i].ToString() : String.Empty;
                        var color = (colorIndex >= 0 && row.Row[i] != null) ? row.Row[colorIndex]?.ToString() : null;
                        var c = new Cell { Value = value, BG = color };
                        newRow.RowData.Add(c);
                    }
                    currentParameters.Data.Add(newRow);
                }
            }

            var xs = new XmlSerializer(typeof (TableParameters));
            var sb = new StringBuilder();
            var ws = new XmlWriterSettings {NewLineHandling = NewLineHandling.Entitize};
            xs.Serialize(XmlWriter.Create(sb, ws), currentParameters);
            currentStep.ParametersXML = sb.ToString();
            currentStep.Calculator = "Calc_DataSourceTable";
            currentStep.EditorAssemblyPath = "SLCalc_DataSourceTable";
            currentStep.Group = "Источники данных";
            currentStep.Name = "Таблица";
            currentStep.ImagePath = "/AQResources;component/Images/Modules/DataSourceTable.png";
            currentStep.Inputs = null;
            currentStep.Outputs = null;
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("TableParameters", "Parameters");
            steps.Add(currentStep);
            Counter++;
            return steps;
        }

        public static Step CreateTable(TableParameters tableParameters)
        {
            var currentStep = new Step();
            var xs = new XmlSerializer(typeof(TableParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), tableParameters);
            currentStep.ParametersXML = sb.ToString();
            currentStep.Calculator = "Calc_DataSourceTable";
            currentStep.EditorAssemblyPath = "SLCalc_DataSourceTable";
            currentStep.Group = "Источники данных";
            currentStep.Name = "Таблица";
            currentStep.ImagePath = "/AQResources;component/Images/Modules/DataSourceTable.png";
            currentStep.Inputs = null;
            currentStep.Outputs = null;
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("TableParameters", "Parameters");
            Counter++;
            return currentStep;
        }
    }
    
    public class TableParameters
    {
        public List<string> DataTypes;
        public string TableName;
        public List<string> ColumnNames;
        public List<string> ColoredColumns;
        public List<Double> ColumnWidth;
        public List<Row> Data;
        public string TSV;
    }

    public class Row
    {
        public List<Cell> RowData;
    }

    public class Cell
    {
        public object Value { get; set; }
        public object BG { get; set; }
        public string Code { get; set; }
        public string Expression { get; set; }
        public string DataType { get; set; }
    }
}
