﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateTransposeRows(string tableName, List<TransposeGroup> transposeGroups)
        {
            var currentParameters = new TransposeRowsParameters
            {
                TableName = tableName,
                TransposeGroups = transposeGroups
            };
            var xs = new XmlSerializer(typeof(TransposeRowsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_TransposeRows",
                EditorAssemblyPath = "SLCalc_TransposeRows",
                Group = "Преобразование",
                Name = "Транспонирование записей",
                ImagePath = "/AQResources;component/Images/Modules/TransposeRows.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("TransposeRowsParameters", "Parameters");
            return currentStep;
        }
    }

    public class TransposeRowsParameters
    {
        public List<TransposeGroup> TransposeGroups;
        public string TableName;
    }

    public class TransposeGroup
    {
        public TransposeMode Mode;
        public string CategoryField;
        public string ValueField;
        public string Prefix;
        public string ValuesList;
        public bool DeleteSourceFields;
    }
    public enum TransposeMode
    {
        Auto, NoTable, Manual
    }
}
