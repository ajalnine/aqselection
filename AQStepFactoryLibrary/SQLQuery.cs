﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static List<Step> CreateSQLQuery(string sql, string name)
        {
            var steps = new List<Step>();
            var currentParameters = new QueryParameters {SQL = sql, TableName = name};
            var xs = new XmlSerializer(typeof (QueryParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_DataSourceSQL",
                EditorAssemblyPath = "SLCalc_DataSourceSQL",
                Group = "Источники данных",
                Name = "Запрос SQL",
                ImagePath = "/AQResources;component/Images/Modules/DataSource.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("QueryParameters", "Parameters");
            steps.Add(currentStep);
            Counter++;
            return steps;
        }
    }

    public class QueryParameters
    {
        public string SQL;
        public string TableName;
    }
}
