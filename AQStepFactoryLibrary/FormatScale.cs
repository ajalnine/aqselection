﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateFormatScale(string tableName, List<ScaleRule> scaleRules)
        {
            var currentParameters = new FormatScaleParameters {TableName = tableName, Rules = scaleRules};
            var xs = new XmlSerializer(typeof (FormatScaleParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_FormatScale",
                EditorAssemblyPath = "SLCalc_FormatScale",
                Group = "Формат",
                Name = "Цветовые шкалы",
                ImagePath = "/AQResources;component/Images/Modules/ColorScales.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("FormatScaleParameters", "Parameters");
            return currentStep;
        }
    }

    public class FormatScaleParameters
    {
        public string TableName;
        public List<ScaleRule> Rules;
    }

    public class ScaleRule
    {
        public string FieldName;

        public double? Min;

        public double? Max;

        public double? Med;

        public Color Color1;

        public Color Color2;

        public Color Color3;

        public ScaleRuleMode Mode;

        public bool Extended;
    }

    public enum ScaleRuleMode
    {
        MinMax, Range, Number
    }
}
