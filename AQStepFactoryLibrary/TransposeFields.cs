﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateTransposeFields(string tableName, List<TransposeGroupFields> transposeGroups)
        {
            var currentParameters = new TransposeFieldsParameters
            {
                TableName = tableName,
                TransposeGroups = transposeGroups
            };
            var xs = new XmlSerializer(typeof(TransposeFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_TransposeFields",
                EditorAssemblyPath = "SLCalc_TransposeFields",
                Group = "Преобразование",
                Name = "Транспонирование полей",
                ImagePath = "/AQResources;component/Images/Modules/TransposeFields.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("TransposeFieldsParameters", "Parameters").Replace("TransposeGroupFields", "TransposeGroups");
            return currentStep;
        }
    }
    public class TransposeFieldsParameters
    {
        public List<TransposeGroupFields> TransposeGroups;
        public string TableName;
    }
    public class TransposeGroupFields
    {
        public string CategoryName;
        public string ValueName;
        public bool DeleteSourceFields;
        public List<string> FieldList;
    }
}
