﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateMarkNorm(string tableName, Color colorInRange, Color colorOutRange, bool allRow, bool markInRange, bool markOutRange)
        {
            var currentParameters = new MarkNormParameters {TableName = tableName, ColorInRange = colorInRange, ColorOutRange = colorOutRange, AllRow = allRow, MarkInRange = markInRange, MarkOutRange = markOutRange };
            var xs = new XmlSerializer(typeof (FormatScaleParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_MarkNorm",
                EditorAssemblyPath = "SLCalc_MarkNorm",
                Group = "Формат",
                Name = "Отметка по границам",
                ImagePath = "/AQResources;component/Images/Modules/MarkNorm.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("MarkNormParameters", "Parameters");
            return currentStep;
        }
    }

    public class MarkNormParameters
    {
        public string TableName;
        public Color ColorInRange;
        public Color ColorOutRange;
        public bool MarkInRange;
        public bool MarkOutRange;
        public bool AllRow;
    }
}
