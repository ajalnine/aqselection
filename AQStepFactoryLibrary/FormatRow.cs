﻿using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateFormatRow(string tableName, List<RowFormatRule> rules)
        {
            var currentParameters = new FormatRowParameters
            {
                TableName = tableName,
                Rules = rules
            };
            var xs = new XmlSerializer(typeof(FormatRowParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_FormatRow",
                EditorAssemblyPath = "SLCalc_FormatRow",
                Group = "Формат",
                Name = "Цвет строки",
                ImagePath = "/AQResources;component/Images/Modules/ConditionRow.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("FormatRowParameters", "Parameters");
            return currentStep;
        }
    }

    public class FormatRowParameters
    {
        public string TableName;
        public List<RowFormatRule> Rules;
    }

    public class RowFormatRule
    {
        public string Expression;

        public string Code;

        public Color Color;
    }
}
