﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateCopyTable(List<CopyTableData> copyTableData)
        {
            var currentParameters = new CopyTableParameters {Copies = copyTableData};
            var xs = new XmlSerializer(typeof (CopyTableParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_CopyTable",
                EditorAssemblyPath = "SLCalc_CopyTable",
                Group = "Преобразование",
                Name = "Копирование таблиц",
                ImagePath = "/AQResources;component/Images/Modules/CopyTable.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("CopyTableParameters", "Parameters");
            return currentStep;
        }

    }

    public class CopyTableParameters
    {
        public List<CopyTableData> Copies;
    }

    public class CopyTableData
    {
        public string OriginalName { get; set; }
        public string CopyName { get; set; }
        public bool NeedToCopy { get; set; }
    }
}
