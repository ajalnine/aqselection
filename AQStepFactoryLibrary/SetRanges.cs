﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateSetRanges(string tableName, string groupField, string groups, List<Range> ranges)
        {
            var currentParameters = new SetRangesParameters
            {
                TableName = tableName, 
                GroupField = groupField,
                Groups = groups,
                Ranges = new ObservableCollection<Range>(ranges)
            };
            var xs = new XmlSerializer(typeof(SetRangesParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_SetRanges",
                EditorAssemblyPath = "SLCalc_SetRanges",
                Group = "Расчеты",
                Name = "Границы параметров",
                ImagePath = "/AQResources;component/Images/Modules/SetRanges.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SetRangesParameters", "Parameters");
            return currentStep;
        }
    }
    public class SetRangesParameters
    {
        public string TableName;
        public string GroupField;
        public string Groups;
        public ObservableCollection<Range> Ranges;
    }

    public class Range
    {
        public double? Min;
        public double? Max;
        public string Parameter;
        public string Group;
    }
}
