﻿using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateFormatCondition(string tableName, List<FormatRule> rules)
        {
            var currentParameters = new FormatConditionParameters
            {
                TableName = tableName,
                Rules = rules
            };
            var xs = new XmlSerializer(typeof(FormatConditionParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_FormatCondition",
                EditorAssemblyPath = "SLCalc_FormatCondition",
                Group = "Формат",
                Name = "Цвет ячейки",
                ImagePath = "/AQResources;component/Images/Modules/Condition.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("FormatConditionParameters", "Parameters");
            return currentStep;
        }
    }

    public class FormatConditionParameters
    {
        public string TableName;
        public List<FormatRule> Rules;
    }

    public class FormatRule
    {
        public string FieldName;
        public string Expression;
        public string Code;
        public Color Color;
    }
}
