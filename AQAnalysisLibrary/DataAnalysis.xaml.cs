﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public delegate void DataAnalysisSelectionChangedDelegate(object sender, DataAnalysisSelectionChangedEventArgs e);
        
    public delegate void EmptyTableSelectedDelegate(object o, EmptyTableSelectedEventArgs e);
    public partial class DataAnalysis
    {
        #region Поля и свойства компонента

        public delegate void DataProcessDelegate(object sender, TaskStateEventArgs e);
        
        private readonly AQService _aqs;
        public AQModuleDescription ParentAqmd { get; set; }

        private readonly CalculationService _cas;
        private ReportingDuplexServiceClient _rdsc;
        private SLDataTable _selectedTable;

        public event DataAnalysisSelectionChangedDelegate TableViewSelectionChanged;
        public event EmptyTableSelectedDelegate EmptyTableSelected;

        #endregion

        public DataAnalysis()
        {
            InitializeComponent();

            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (_cas == null) _cas = Services.GetCalculationService();
            if (_aqs == null) _aqs = Services.GetAQService();
        }
        
        public void ClosePopups()
        {
            CustomWindow.IsOpen = false;
            ((FilterDispatcher) Resources["MainFilterDispatcher"]).HideFilters();
        }

        private void DataSetPresenter_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (!(Parent is ChildWindow)) return;
            var p = (ChildWindow) Parent;
            if (e.Key == Key.PageDown)
            {
                p.Margin = new Thickness(0, Root.Margin.Top + 100, 0, 0);
                p.UpdateLayout();
            }
            if (e.Key == Key.PageUp)
            {
                p.Margin = new Thickness(0, Root.Margin.Top - 100, 0, 0);
                p.UpdateLayout();
            }
        }

        private void MainDataGrid_OnPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            var grid = e.EditingElement as Grid;
            if (grid?.Children == null || grid.Children.Count <= 1) return;
            var textBox = grid.Children[1] as TextBox;
            textBox?.SelectAll();
        }

        private void ShowDescriptionButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(string.IsNullOrEmpty(Description) ? "Описание исходных данных отсутствует" : Description, "Об исходных данных", MessageBoxButton.OK);
        }

        private void Panel_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
           // ClosePopups();
        }
    }
}