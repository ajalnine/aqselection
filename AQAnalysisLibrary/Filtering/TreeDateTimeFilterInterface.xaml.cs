﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQReportingLibrary;

namespace AQAnalysisLibrary.Filtering
{
    public partial class TreeDateTimeFilterInterface : IFilterInterface
    {
        private readonly SLDataTable _sldt;
        private readonly string _columnName;
        private readonly string _colorColumnName;
        private readonly List<object> _unfilteredValues;
        private readonly List<object> _unfilteredColorValues;
        private List<SLDataFilterItem> _data = new List<SLDataFilterItem>();
        private List<SLDataFilterItem> _colordata = new List<SLDataFilterItem>();
        public event FilterChangedDelegate FilterChanged;

        public TreeDateTimeFilterInterface(SLDataTable sldt, string columnName)
        {
            _sldt = sldt;
            _columnName = columnName;
            _colorColumnName = "#" + _columnName + "_Цвет";
            _unfilteredValues = _sldt.GetDistinctUnfilteredWithNulls(_columnName).ToList();
            _unfilteredColorValues =  _sldt.GetDistinctUnfilteredWithNulls(_colorColumnName).ToList();
            InitializeComponent();
            CreateFilterPanel();
            CreateColorFilter();
        }

        private void CreateFilterPanel()
        {
            if (_sldt.Filters != null && _sldt.Filters.Any(a => a.ColumnName == _columnName))
            {
                _data = _sldt.Filters.Single(a => a.ColumnName == _columnName).FilterItems;
                var sourceDistinctData = _sldt.GetDistinctWithNulls(_columnName).ToList();
                foreach (var d in _data)
                {
                    d.Included = sourceDistinctData.Contains(d.Value);
                }
            }
            else
            {
                var sourceDistinctData = _sldt.GetDistinctWithNulls(_columnName);
                var d = sourceDistinctData.OrderBy(a => a is DateTime ? a : DateTime.MinValue).ToList();

                _data =
                    d.Select(
                        a =>
                            new SLDataFilterItem
                            {
                                Value = a,
                                Caption =
                                    string.IsNullOrEmpty(a?.ToString())
                                        ? " Нет значения"
                                        : ((DateTime) a).ToString("dd.MM.yyyy"),
                                Included = true
                            }).ToList();
            }
            var hasNulls = _data.Any(a => string.IsNullOrEmpty(a.Value?.ToString()));
            var years = _data.Where(a => a.Value is DateTime).Select(a => ((DateTime) a.Value).Year).Distinct();
            var initData =
                years.Select(
                    a =>
                        new DateItem
                        {
                            Caption = a.ToString(),
                            Parent = null,
                            DatePart = DatePart.Year,
                            Data = _data.Where(b => b.Value is DateTime && ((DateTime) b.Value).Year == a).ToList(),
                        }).ToList();
        

           var items = hasNulls 
           ? new List<DateItem> {new DateItem {Caption = " Нет значения", Parent = null, DatePart = DatePart.Day, Data = _data.Where(a => string.IsNullOrEmpty(a.Value?.ToString())).ToList(), State = _data.Single(a => string.IsNullOrEmpty(a.Value?.ToString())).Included}} 
           : new List<DateItem>();
            items.AddRange(initData);
            FilterContent.ItemsSource = items;
        }

        private void CreateColorFilter()
        {
            _colordata = _sldt.CreateFilter(_colorColumnName);
            ColorFilter.ItemsSource = _colordata;
        }

        private void All_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(true);
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(false);
        }
       
        private void Unfilter_OnClick(object sender, RoutedEventArgs e)
        {
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Unfilter });
        }

        private void DelFilter_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.RemoveFilter(_columnName);
            _sldt.RemoveFilter(_colorColumnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Cancelled });
        }

        private void OrderByAsc_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.OrderByAsc(_columnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void OrderByDesc_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.OrderByDesc(_columnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.StoreFilter(_data, _columnName, _unfilteredValues.Count);
            _sldt.StoreFilter(_colordata, _colorColumnName, _unfilteredColorValues.Count);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.RemoveFilter(_columnName);
            CreateFilterPanel();
            _sldt.RemoveFilter(_colorColumnName);
            CreateColorFilter();
        }

        private void SetChecked(bool Checked)
        {
            foreach (var d in FilterContent.ItemsSource)
            {
                ((DateItem) d).State = Checked;
            }
        }
    }

    public class DateItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _caption;
        private DatePart _datepart;
        public List<SLDataFilterItem> Data;
        public DateItem Parent;

        public List<DateItem> SubItems
        {
            get
            {
                switch (DatePart)
                {
                    case DatePart.Year:
                        var month = Data.Where(a => a.Value is DateTime).Select(a => ((DateTime) a.Value).Month).Distinct();
                        var monthData =
                            month.Select(
                                a =>
                                    new DateItem
                                    {
                                        Caption = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(a),
                                        Parent = this,
                                        DatePart = DatePart.Month,
                                        Data = Data.Where(b => b.Value is DateTime && ((DateTime) b.Value).Month == a).ToList(),

                                    }).ToList();
                        return monthData;

                    case DatePart.Month:
                        var days = Data.Where(a => a.Value is DateTime).Select(a => ((DateTime)a.Value).Day).Distinct();
                        var daysData =
                            days.Select(
                                a =>
                                    new DateItem
                                    {
                                        Caption = a.ToString(),
                                        Parent = this,
                                        DatePart = DatePart.Day,
                                        Data = Data.Where(b => b.Value is DateTime && ((DateTime)b.Value).Day == a).ToList(),
                                    }).ToList();
                        return daysData;

                    default:
                        return null;
                }
            }
        } 
        public bool? State
        {
            get
            {
                if (DatePart == DatePart.Day) return Data.Single().Included;
                var c = Data.Where(a => a.Included).ToList();
                if (c.Count == 0) return false;
                if (c.Count == Data.Count) return true;
                return null;
            }
            set
            {
                if (DatePart == DatePart.Day && value != null)
                {
                    Data.Single().Included = value.Value;
                    OnPropertyChanged(nameof(State));
                }
                else
                {
                    if (SubItems != null)
                    {
                        foreach (var s in SubItems)
                        {
                            s.State = value;
                            s.OnPropertyChanged(nameof(s.State));
                            
                        }
                    }
                }
                var d = this;
                while (d.Parent != null)
                {
                    d.Parent?.OnPropertyChanged(nameof(Parent.State));
                    d = d.Parent;
                }
                OnPropertyChanged(nameof(SubItems));
            }
        }

        public DatePart DatePart
        {
            get { return _datepart; }
            set
            {
                _datepart = value;
                OnPropertyChanged(nameof(DatePart));
            }
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                OnPropertyChanged(nameof(Caption));
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public enum DatePart { Year, Month, Day}
}
