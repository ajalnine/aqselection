﻿using System;
using System.Linq;
using System.Windows;
using AQAnalysisLibrary.Filtering;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQReportingLibrary;

namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        private void MainFilterDispatcher_OnFilterRequest(object o, FilterRequestEventArg e)
        {
            switch (e.FilterCommand)
            {
                case FilterCommand.Edit:

                    if (e.FilterControl.InDataGrid)
                    {
                        var filterSize = 200;
                        CustomWindow.IsOpen = true;
                        CustomWindow.Width = filterSize;
                        CustomWindow.MaxHeight = filterSize*3;
                        var transform = e.FilterControl.TransformToVisual(this);
                        var point = transform.Transform(new Point(e.FilterControl.ActualWidth + 6, 0));
                        SetFilterInterfaceToCustomWindow(e.FilterControl.ColumnName);
                        CustomWindow.UpdateLayout();
                        CustomWindow.VerticalOffset = 30;
                        CustomWindow.HorizontalOffset = Math.Abs(point.X) - filterSize < 0
                            ? 0
                            : Math.Abs(point.X) - filterSize > this.ActualWidth
                                ? this.ActualWidth - filterSize
                                : Math.Abs(point.X) - filterSize;
                    }
                    else
                    {
                        var filterSize = 200;
                        CustomWindow.IsOpen = true;
                        CustomWindow.Width = filterSize;
                        CustomWindow.MaxHeight = filterSize * 3;
                        var transform = e.FilterControl.TransformToVisual(this);
                        var point = transform.Transform(new Point(e.FilterControl.ActualWidth + 6, 0));
                        SetFilterInterfaceToCustomWindow(e.FilterControl.ColumnName);
                        CustomWindow.UpdateLayout();
                        CustomWindow.VerticalOffset = Math.Abs(point.Y) + CustomWindow.ActualHeight > this.ActualHeight ? this.ActualHeight - CustomWindow.ActualHeight : Math.Abs(point.Y);
                        CustomWindow.HorizontalOffset = Math.Abs(point.X);
                    }
                    break;

                case FilterCommand.Close:
                    CustomWindow.IsOpen = false;
                    break;
            }
        }

        private void SetFilterInterfaceToCustomWindow(string columnName)
        {
            if (!_selectedTable.IsFiltered) _selectedTable.StartFiltering();
            var dataType = _selectedTable.GetDataType(columnName);
            IFilterInterface i = null;
            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    i = new DiscreteFilterInterface(_selectedTable, columnName, false);
                    CustomWindow.Child = (DiscreteFilterInterface)i;
                    break;

                case "system.bool":
                case "bool":
                    i = new DiscreteFilterInterface(_selectedTable, columnName, false);
                    CustomWindow.Child = (DiscreteFilterInterface)i;
                    break;

                case "system.datetime":
                case "datetime":
                    var t = _selectedTable.GetDistinct(columnName).OfType<DateTime>().ToList();
                    if (t.All(a => a.Millisecond == 0 && a.Second == 0 && a.Minute == 0 && a.Hour == 0))
                    {
                        i = new TreeDateTimeFilterInterface(_selectedTable, columnName);
                        CustomWindow.Child = (TreeDateTimeFilterInterface)i;
                    }
                    else
                    {
                        i = new DiscreteDateTimeFilterInterface(_selectedTable, columnName);
                        CustomWindow.Child = (DiscreteDateTimeFilterInterface)i;
                    }
                    break;

                case "system.timespan":
                case "timespan":
                    i = new DiscreteFilterInterface(_selectedTable, columnName, false);
                    CustomWindow.Child = (DiscreteFilterInterface)i;
                    break;

                case "system.int":
                case "int":
                case "system.double":
                case "double":
                    i = new DiscreteFilterInterface(_selectedTable, columnName, true);
                    CustomWindow.Child = (DiscreteFilterInterface)i;
                    break;
            }
            if (i == null) return;
            i.FilterChanged += I_FilterChanged;
        }

        private void I_FilterChanged(object o, FilterChangedEventArgs e)
        {
            switch (e.Result)
            {
                case FilterResult.Unfilter:
                    _selectedTable.StopFiltering();
                    ((FilterDispatcher)Resources["MainFilterDispatcher"]).ResetFiltered();
                    RefreshDatagrid();
                    break;

                case FilterResult.Changed:
                    _selectedTable.ApplyFilters();
                    SetAsFilteredMark();
                    RefreshDatagrid();
                    break;

                case FilterResult.Cancelled:
                    break;
            }
            ClosePopups();
        }

        private void SetAsFilteredMark()
        {
            ((FilterDispatcher) Resources["MainFilterDispatcher"])?.SetAsFiltered(
                _selectedTable?.Filters?.Select(a => a.ColumnName).ToList());
            ((FilterDispatcher)Resources["MainFilterDispatcher"])?.SetAsFiltered(
                _selectedTable?.Filters?.Select(a => "#" + a.ColumnName + "_Цвет").ToList());
        }

        private void ResetSubscription()
        {
            ((FilterDispatcher)Resources["MainFilterDispatcher"])?.ClearSubscription();
        }
    }
}