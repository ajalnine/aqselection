﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQReportingLibrary;

namespace AQAnalysisLibrary.Filtering
{
    public partial class DiscreteFilterInterface : IFilterInterface
    {
        private readonly bool _isNumeric;
        private readonly SLDataTable _sldt;
        private readonly string _columnName;
        private readonly string _colorColumnName;
        private readonly List<object> _unfilteredValues;
        private readonly List<object> _unfilteredColorValues;
        private List<SLDataFilterItem> _data = new List<SLDataFilterItem>();
        private List<SLDataFilterItem> _colordata = new List<SLDataFilterItem>();
        public event FilterChangedDelegate FilterChanged;
        public DiscreteFilterInterface(SLDataTable sldt, string columnName, bool withNumericSelection)
        {
            _isNumeric = withNumericSelection;
            _sldt = sldt;
            _columnName = columnName;
            _colorColumnName = "#" + _columnName + "_Цвет";
            _unfilteredValues = _sldt.GetDistinctUnfilteredWithNulls(_columnName).ToList();
            _unfilteredColorValues = _sldt.GetDistinctUnfilteredWithNulls(_colorColumnName).ToList();

            InitializeComponent();

            NumericFilterPanel.Visibility = withNumericSelection ? Visibility.Visible : Visibility.Collapsed;

            CreateFilterPanel();
            CreateColorFilter();
        }

        private void CreateFilterPanel()
        {
            _data = _sldt.CreateFilter(_columnName);
            FilterContent.ItemsSource = _data;
        }
        private void CreateColorFilter()
        {
            _colordata = _sldt.CreateFilter(_colorColumnName);
            ColorFilter.ItemsSource = _colordata;
        }

        private void All_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(true, false);
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(false, false);
        }

        private void Plus_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(true, true);
        }

        private void Minus_OnClick(object sender, RoutedEventArgs e)
        {
            SetChecked(false, true);
        }

        private void Unfilter_OnClick(object sender, RoutedEventArgs e)
        {
            FilterChanged?.Invoke(this, new FilterChangedEventArgs {Result = FilterResult.Unfilter});
        }

        private void DelFilter_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.RemoveFilter(_columnName);
            _sldt.RemoveFilter(_colorColumnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Cancelled });
        }

        private void OrderByAsc_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.OrderByAsc(_columnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void OrderByDesc_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.OrderByDesc(_columnName);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.StoreFilter(_data, _columnName, _unfilteredValues.Count);
            _sldt.StoreFilter(_colordata, _colorColumnName, _unfilteredColorValues.Count);
            FilterChanged?.Invoke(this, new FilterChangedEventArgs { Result = FilterResult.Changed });
        }

        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            _sldt.RemoveFilter(_columnName);
            CreateFilterPanel();
            _sldt.RemoveFilter(_colorColumnName);
            CreateColorFilter();
        }

        private void SetChecked(bool Checked, bool range)
        {
            if (FilterContent.ItemsSource == null) return;
            if (((List<SLDataFilterItem>)FilterContent.ItemsSource).Count == 0) return;
            IQueryable toCheck;
            if (range)
            {
                double min, max ;
                if (!double.TryParse(FilterMin.Text, out min)) min = double.NegativeInfinity;
                if (!double.TryParse(FilterMax.Text, out max)) max = double.PositiveInfinity;
                toCheck = (from a in ((List<SLDataFilterItem>)FilterContent.ItemsSource) where !string.IsNullOrEmpty(a.Value?.ToString())   && double.Parse(a.Value.ToString()) >= min && double.Parse(a.Value.ToString()) <= max select a).AsQueryable();
            }
            else
            {
                toCheck = (from a in ((List<SLDataFilterItem>)FilterContent.ItemsSource) select a).AsQueryable();
            }

            FilterContent.ItemsSource = null;
            foreach (var item in toCheck)
            {
                ((SLDataFilterItem)item).Included = Checked;
            }
            FilterContent.ItemsSource = _data;
            FilterContent.Focus();
            FilterContent.UpdateLayout();
        }
    }
}
