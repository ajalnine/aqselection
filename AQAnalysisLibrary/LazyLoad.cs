﻿using System;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        private void BeginLoadEmptyTable()
        {
            var e = new EmptyTableSelectedEventArgs
                {
                    EmptyTable = _selectedTable,
                    TableLoadedCallBack = LoadedTableCallBack,
                    StartProgress = MainTableSelector.StartLoadTableProgress,
                    StopProgress = MainTableSelector.StopLoadTableProgress
                };
            EmptyTableSelected?.Invoke(this, e);
        }
        
        private void LoadedTableCallBack(SLDataTable loadedTable)
        {
            _selectedTable = loadedTable;

            var source = _dataTables.SingleOrDefault(a => a.TableName == loadedTable.TableName);
            _dataTables.Remove(source);
            _dataTables.Add(loadedTable);

            var appendedSource = _appendedTables.SingleOrDefault(a => a.TableName == loadedTable.TableName);
            _appendedTables.Remove(appendedSource);
            _appendedTables.Add(loadedTable);
            Dispatcher.BeginInvoke(() => 
            {
                SetTableInfo();
                UpdateButtonsState();
                EnableChartView();
                DoAnalysis();
            });
        }
    }

    public class EmptyTableSelectedEventArgs : EventArgs
    {
        public SLDataTable EmptyTable { get; set; }
        public TableLoadedCallBackDelegate TableLoadedCallBack { get; set; }
        public Action StartProgress { get; set; }
        public Action StopProgress { get; set; }
    }

    public delegate void TableLoadedCallBackDelegate(SLDataTable loadedTable);
}