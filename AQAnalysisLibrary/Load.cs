﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQStepFactoryLibrary;
using System.IO.IsolatedStorage;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        #region Загрузка данных

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof (string), typeof (DataAnalysis),
                                        new PropertyMetadata(String.Empty));

        private Step _currentChartStep;
        private List<SLDataTable> _dataTables;
        private DateTime? _from;
        private bool _hasAttachedCalculation;
        private List<Step> _sourceSteps;
        private DateTime? _to;

        public string Description
        {
            get { return (string) GetValue(DescriptionProperty); }
            private set { SetValue(DescriptionProperty, value); }
        }
        
        public void LoadFromClipboard(string text)
        {
            var fromBuffer = ClipboardDataHelper.GetSLDataTableFromClipboard(text, "Таблица" + _pasteCounter);
            ViewType.SelectedIndex = IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisTab") ? (int)IsolatedStorageSettings.ApplicationSettings["AnalysisTab"] : 0;
            
            if (fromBuffer == null)
            {
                StartEmptyAnalisys();
                return;
            }

            if (_dataTables == null)
            {
                MainChartView.Reset();
                Description = string.Empty;
                _dataTables = new List<SLDataTable>();
                _sourceSteps = new List<Step>();
            }
            _dataTables.Add(fromBuffer);
            _sourceSteps.AddRange(StepFactory.CreateTable(fromBuffer));
            _hasAttachedCalculation = true;
            _pasteCounter++;
            BeginAnalysis(fromBuffer.TableName);
        }

        public void StartEmptyAnalisys()
        {
            BeginAnalysis(null);
        }
        
        public void LoadDataWithCalculation(List<SLDataTable> newTables, List<Step> newCalculation, Step operation,
                                             DateTime? from, DateTime? to, string desc, bool preselect = true)
        {
            if (_dataTables == null)
            {
                MainChartView.Reset();
                Description = string.Empty;
                _dataTables = new List<SLDataTable>();
            }
            if (_sourceSteps == null) _sourceSteps = new List<Step>();
            if (from.HasValue) _from = from;
            if (to.HasValue) _to = to;
            if (!String.IsNullOrEmpty(desc)) Description = desc;
            if (operation != null) _currentChartStep = operation;
            if (_currentChartStep != null) ViewType.SelectedIndex = 1;
            else ViewType.SelectedIndex = IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisTab") ? (int)IsolatedStorageSettings.ApplicationSettings["AnalysisTab"] : 0;

            _hasAttachedCalculation = newCalculation != null || _sourceSteps.Count > 0;
            
            SafeAppendNewData(newTables, newCalculation);
            if (preselect)
            {
                if (_currentChartStep != null)
                {
                    MainChartView.DoAnalysis(_dataTables, null, _sourceSteps, operation, _from, _to, desc); 
                }
                else BeginAnalysis(newTables[newTables.Count > 1 ? 1 : 0].TableName);
            }
            else
            {
                BeginAnalysis(_selectedTable.TableName);
            }
        }

        private void SafeAppendNewData(List<SLDataTable> newTables, IEnumerable<Step> newCalculation)
        {
            if (newTables == null) return;
            var intersection = _dataTables.Select(a => a.TableName).Intersect(newTables.Select(a => a.TableName)).ToList();
            if (!intersection.Any())
            {
                _dataTables.InsertRange(_dataTables.Count, newTables);
                if (newCalculation != null) _sourceSteps.InsertRange(_sourceSteps.Count, newCalculation);
            }
            else
            {
                var renames = new Dictionary<string, string>();
                foreach (var repeatingName in intersection)
                {
                    var renamingTable = newTables.SingleOrDefault(a => a.TableName == repeatingName);
                    if (renamingTable == null || _dataTables.Contains(renamingTable)) continue;
                    var newName = $"{renamingTable.TableName} {(_dataTables.Select(a => a.TableName).Count(a => a.StartsWith(renamingTable.TableName)) + 1)}";
                    renamingTable.TableName = newName;
                    renames.Add(repeatingName, newName);
                    _dataTables.Add(renamingTable);
                }
                var renameOperation = StepFactory.CreateRenameTables(newTables, renames);
                if (newCalculation == null) return;
                foreach (var step in newCalculation)
                {
                    if (!_sourceSteps.Contains(step))_sourceSteps.Add(step);
                }
                _sourceSteps.AddRange(renameOperation);
            }
        }
        #endregion

        public void Reset()
        {
            MainChartView.Reset();
            Description = string.Empty;
            _dataTables = new List<SLDataTable>();
            _sourceSteps = new List<Step>();
            Description = string.Empty;
            _currentChartStep = null;
            CustomTableDetailsPanel.Children.Clear();
            CustomTableDetailsPanel.Visibility = Visibility.Collapsed;
        }
    }
}