﻿using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        private void SetColorMarkOnTable(ColorChangedEventArgs e)
        {
            if ( _selectedTable?.Table == null || !_selectedTable.Table.Any() || MainDataGrid.SelectedItems==null) return;
            
            if (e.TableMarkMode == TableMarkMode.Clear)
            {
                DeleteAllColorData();
                return;
            }

            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            if (x < 0 || y < 0 ) return;

            if (MainDataGrid.SelectedItems.Count <= 1)
            {
                var selectedRow = MainDataGrid.SelectedItems.OfType<SLDataRow>().FirstOrDefault();
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                        var column = $"#{MainDataGrid.CurrentColumn.Header}_Цвет";
                        _selectedTable.FillCell(column, e.ColorString, "System.String", selectedRow);
                        break;
                    case TableMarkMode.Row:
                        var columns = _selectedTable.ColumnNames.Where(a => !a.StartsWith("#")).Select(a=> $"#{a}_Цвет").ToList();
                        _selectedTable.FillRow(columns, e.ColorString, "System.String", selectedRow);
                        break;
                    case TableMarkMode.Column:
                        var column2 = $"#{MainDataGrid.CurrentColumn.Header}_Цвет";
                        _selectedTable.FillColumn(column2, e.ColorString, "System.String");
                        break;
                }
            }
            else
            {
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                    case TableMarkMode.Column:
                        var column = $"#{MainDataGrid.CurrentColumn.Header}_Цвет";
                        foreach (var s in MainDataGrid.SelectedItems)
                        {
                            _selectedTable.FillCell(column, e.ColorString, "System.String", (SLDataRow)s);
                        }
                        break;
                    case TableMarkMode.Row:
                        var columns = _selectedTable.ColumnNames.Where(a => !a.StartsWith("#")).Select(a=> $"#{a}_Цвет").ToList();
                        foreach (var s in MainDataGrid.SelectedItems)
                        {
                            _selectedTable.FillRow(columns, e.ColorString, "System.String", (SLDataRow)s);
                        }
                        break;
                }
            }
            MakeTable();
        }

        private void DeleteAllColorData()
        {
            var mr = MessageBox.Show($"Удалить цветовую разметку {_selectedTable.TableName} ?",
                "Подтверждение",
                MessageBoxButton.OKCancel);
            if (mr == MessageBoxResult.Cancel) return;

            var colorColumns = _selectedTable.ColumnNames.Where(a => a.EndsWith("_Цвет")).ToList();
            foreach (var cc in colorColumns) _selectedTable.DeleteColumn(cc);
            MakeTable();
        }
    }
}
