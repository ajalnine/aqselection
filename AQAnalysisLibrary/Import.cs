﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQStepFactoryLibrary;
using AQCalculationsLibrary;
// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        #region Импорт таблицы из буфера

        private static int _pasteCounter = 1;

        private void PasteButton_Click(object sender, RoutedEventArgs e)
        {
            var imported = GetSLDataTableFromClipboard();
            if (imported != null)
            {
                if (_dataTables==null)_dataTables = new List<SLDataTable>();
                if (_sourceSteps==null)_sourceSteps = new List<Step>();
                while (_dataTables.Select(a => a.TableName).Contains(imported.TableName)) imported.TableName += "_";
                _dataTables.Add(imported);
                _sourceSteps.AddRange(StepFactory.CreateTable(imported));
                _pasteCounter++;
            }
            else return;
            BeginAnalysis(imported.TableName);
        }

        private SLDataTable GetSLDataTableFromClipboard()
        {
            if (Clipboard.ContainsText())
            {
                return ClipboardDataHelper.GetSLDataTableFromClipboard(Clipboard.GetText(), "Таблица" + _pasteCounter);
            }
            MessageBox.Show("Буфер обмена не содержит данных.", "Ошибка вставки данных", MessageBoxButton.OK);
            return null;
        }
        #endregion
    }
}