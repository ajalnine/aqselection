﻿
using System;
using System.Windows;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQChartViewLibrary;
using AQControlsLibrary;
using AQReportingLibrary;


// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        private void SetButtonsVisibility()
        {
            if (ViewType.SelectedIndex == 1)
            {
                CopyButton.Visibility = Visibility.Visible;
                TableToCalcButton.Visibility = Visibility.Visible;
                TablesToCalcButton.Visibility = Visibility.Visible;
                CalcToCalcButton.Visibility = Visibility.Visible;
                XlsxButton.Visibility = Visibility.Collapsed;
                NewSlideButton.Visibility = Visibility.Visible;
                DeleteButton.Visibility = Visibility.Visible;
                TableToAnalysisButton.Visibility = Visibility.Visible;
                PasteButton.Visibility = Visibility.Visible;
                MainTableCellsColorSelector.Visibility = Visibility.Collapsed;
            }
            else
            {
                MainTableCellsColorSelector.Visibility = Visibility.Visible; 
                CopyButton.Visibility = Visibility.Visible;
                TableToCalcButton.Visibility = Visibility.Visible;
                TablesToCalcButton.Visibility = Visibility.Visible;
                TableToAnalysisButton.Visibility = Visibility.Visible; 
                CalcToCalcButton.Visibility = Visibility.Visible;
                XlsxButton.Visibility = Visibility.Visible;
                NewSlideButton.Visibility = Visibility.Collapsed;
                DeleteButton.Visibility = Visibility.Visible;
                PasteButton.Visibility = Visibility.Visible;
            }
        }

        private void UpdateButtonsState()
        {
            CalcToCalcButton.IsEnabled = _selectedTable != null && _hasAttachedCalculation;
            CopyButton.IsEnabled = _selectedTable != null;
            ClearButton.IsEnabled = _selectedTable != null;
            TableToCalcButton.IsEnabled = _selectedTable != null;
            TableToAnalysisButton.IsEnabled = _selectedTable != null;
            XlsxButton.IsEnabled = _selectedTable != null;
            NewSlideButton.IsEnabled = _selectedTable != null;
            DeleteButton.IsEnabled = _selectedTable != null;
            EmptyDataSet.Visibility = _selectedTable != null ? Visibility.Collapsed : Visibility.Visible;
        }


        private void MainDataGrid_OnLayoutUpdated(object sender, EventArgs e)
        {
            SetAsFilteredMark();
        }

        private void MainChartView_OnInteractiveCommandProcessed(object o, InteractiveCommandProcessedEventArgs e)
        {
            if (e.Result.TablesCreated) MainTableSelector.CreateTableSelectors(_dataTables, MainChartView.GetSelectedTable());
        }

        private void MainChartView_OnReportInteractiveRename(object o, ReportInteractiveRenameEventArgs e)
        {
            foreach (var ro in e.RenameOperations)
            {
                if (ro.RenameTarget == ReportRenameTarget.Table)
                {
                    MainTableSelector.ChangeTableName(ro.OldName, ro.NewName);
                }
                if (ro.RenameTarget == ReportRenameTarget.Field)
                {
                     ((FilterDispatcher) Resources["MainFilterDispatcher"]).RenameColumn(ro.OldName, ro.NewName);
                }
            }
        }

        private void MainChartView_OnInternalTablesReadyForExport(object o, InternalTablesReadyForExportEventArgs e)
        {
            InternalTablesExportPanel.Visibility = Visibility.Visible;
            _internalReportData = e.InternalTables;
        }

        private void MainChartView_OnAnalysisStarted(object o, AnalysisStartedEventArgs e)
        {
            InternalTablesExportPanel.Visibility = Visibility.Collapsed;
            _internalReportData = null;
        }

        private void MainChartView_OnAnalysisFinished(object o, AnalysisReadyEventArgs e)
        {

        }

        private void MainChartView_OnSelectedTableChanged(object o, SelectedTableChangedEventArgs e)
        {
            _selectedTable = e.SelectedTable;
            MainTableSelector.CreateTableSelectors(_dataTables, MainChartView.GetSelectedTable());
        }
    }
}