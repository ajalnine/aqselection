﻿using System.Windows;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        #region Удаление таблицы

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (_selectedTable == null) return;
            var mbr = MessageBox.Show("Удалить таблицу \"" + _selectedTable.TableName + "\" ?",
                "Удаление", MessageBoxButton.OKCancel);
            if (mbr != MessageBoxResult.OK) return;
            var index = _dataTables.IndexOf(_selectedTable);
            _dataTables.Remove(_selectedTable);
            _sourceSteps.AddRange(StepFactory.CreateDeleteTable(_selectedTable.TableName));
            index--;
            if (index < 0) index = _dataTables.Count - 1;
            if (index < 0)
            {
                MainDataGrid.ItemsSource = null;
                MainDataGrid.Columns.Clear();
                _selectedTable = null;
                _dataTables = null;
                _sourceSteps = null;
                ViewType.SelectedIndex = 0;
                StartEmptyAnalisys();
            }
            else BeginAnalysis(_dataTables[index].TableName);
        }
        #endregion
   }
}