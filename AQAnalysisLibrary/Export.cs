﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQControlsLibrary;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        #region Экспорт всех таблиц в Xlsx

        private void ExportToXlsxEx_Click(object sender, RoutedEventArgs e)
        {
            Guid task1Guid = SetupExportToXlsxService();
            _rdsc.FillDataWithSLTablesAsync(task1Guid, ConvertSLTablesToReportTables(_dataTables));
        }

        private Guid SetupExportToXlsxService()
        {
            var task = new Task(new[] { "Компоновка данных", "Формирование файла Xlsx" }, "Экспорт в Xlsx", TaskPanel);
            task.StartTask();

            var task1Guid = Guid.NewGuid();
            var task2Guid = Guid.NewGuid();
            _rdsc = Services.GetReportingService();
            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              task.AdvanceProgress();
                              _rdsc.GetAllResultsInXlsxAsync(task2Guid, (string)sr.ResultData);
                          }
                          else
                          {
                              task.StopTask(sr.Message);
                              if (sr.ResultData != null) _rdsc.DropDataAsync((Guid)sr.ResultData);
                          }
                      });

            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task2Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              task.AdvanceProgress();
                              Dispatcher.BeginInvoke(
                                  () => HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                      $@"GetFile.aspx?FileName={HttpUtility.UrlEncode(sr.ResultData.ToString())}&FName={
                                          HttpUtility.UrlEncode($"Расчет {DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)}.xlsx")}&ContentType={
                                          HttpUtility.UrlEncode("application/msexcel")}")));
                          }
                          else
                          {
                              task.StopTask(sr.Message);
                              if (sr.ResultData != null) _rdsc.DropDataAsync((Guid)sr.ResultData);
                          }
                      });
            return task1Guid;
        }

        private List<ReportDataTable> ConvertSLTablesToReportTables(IEnumerable<SLDataTable> tables)
        {
            var result = new List<ReportDataTable>();
            foreach (SLDataTable a in tables)
            {
                var rdt = new ReportDataTable
                    {
                        ColumnNames = a.ColumnNames,
                        DataTypes = (from x in a.DataTypes select x?.Replace("System.", String.Empty)).ToList(),
                        Table = new List<ReportDataRow>(),
                    };
                if (a.CustomProperties!= null)
                {
                    rdt.CustomProperties = new List<ApplicationCore.ReportingServiceReference.SLExtendedProperty>();
                    foreach(var e in a.CustomProperties)
                    {
                        if (e != null) rdt.CustomProperties.Add(new ApplicationCore.ReportingServiceReference.SLExtendedProperty() { Key = e.Key, Value = e.Value });
                        else rdt.CustomProperties.Add(null);
                    }
                }
                foreach (SLDataRow b in a.Table)
                {
                    var rdr = new ReportDataRow {Row = b.Row};
                    for (var i = 0; i < b.Row.Count; i++) if (rdr.Row[i] == DBNull.Value) rdr.Row[i] = null;
                    rdt.Table.Add(rdr);
                }
                rdt.TableName = a.TableName;
                rdt.SelectionString = a.SelectionString;
                result.Add(rdt);
            }
            return result;
        }

        #endregion

        #region Передача

        private void TableToCalcButton_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object> {{"Table", _selectedTable}};
            if (_from.HasValue) signalArguments.Add("From", _from.Value);
            if (_to.HasValue) signalArguments.Add("To", _to.Value);
            Signal.Send(ParentAqmd, Signal.WorkPlace.GetAQModuleDescription(),
                        Command.InsertTable, signalArguments, null);
        }

        private void TablesToCalcButton_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object> { { "Tables", _dataTables } };
            if (_from.HasValue) signalArguments.Add("From", _from.Value);
            if (_to.HasValue) signalArguments.Add("To", _to.Value);
            Signal.Send(ParentAqmd, Signal.WorkPlace.GetAQModuleDescription(),
                        Command.InsertTables, signalArguments, null);
        }

        private void CalcToCalcButton_Click(object sender, RoutedEventArgs e)
        {
            
            var signalArguments = new Dictionary<string, object>
            {
                {
                    "Calculation", MainTableSelector.GetOrderMode() != TableOrderMode.Unordered
                        ? _sourceSteps.Union(AppendReorderTableStep()).ToList()
                        : _sourceSteps
                }
            };
            if (_from.HasValue) signalArguments.Add("From", _from.Value);
            if (_to.HasValue) signalArguments.Add("To", _to.Value);
            Signal.Send(ParentAqmd, Signal.WorkPlace.GetAQModuleDescription(),
                        Command.InsertCalculation, signalArguments, null);
                        
        }

        private IEnumerable<Step> AppendReorderTableStep()
        {
            return StepFactory.CreateReorderTables(MainTableSelector.GetOrderedNames());
        }

        private void TableToAnalysisButton_Click(object sender, RoutedEventArgs e)
        {
            var tableToSend = _selectedTable.GetClone();

            var signalArguments = new Dictionary<string, object> { { "Table", tableToSend } };
            Signal.Send(ParentAqmd, Signal.WorkPlace.GetAQModuleDescription(),
                        Command.SendTableToAnalysis, signalArguments, null);
        }

        #endregion

        #region Экспорт слайда

        private void NewSlideButton_Click(object sender, RoutedEventArgs e)
        {
            var res = MainChartView.GetAnalisysResult();
            var signalArguments = new Dictionary<string, object>
                {
                    {"Operation", res.AnalysisOperation},
                    {"Calculation", res.InnerSteps},
                    //{"SourceGuid", },
                    {"Thumbnail", res.ThumbNail},
                    {"Icon", res.Icon},
                    {"SlideElement", res.SED},
                    {"SourceData", _dataTables},
                    {"ColorScale", res.UsedColorScale},
                    {"Inverted", res.IsInverted}
                };
            if (_from.HasValue) signalArguments.Add("From", _from.Value);
            if (_to.HasValue) signalArguments.Add("To", _to.Value);
            Signal.Send(ParentAqmd, Signal.WorkPlace.GetAQModuleDescription(), Command.InsertSlideElement, signalArguments, null);
        }

        #endregion

        #region Копирование таблицы в буфер обмена

        private List<SLDataTable> _internalReportData;

        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            string textData = string.Empty;
            for (int i = 0; i < _selectedTable.ColumnNames.Count; i++)
            {
                if (i > 0) textData += "\t";
                object o = _selectedTable.ColumnNames[i];
                if (o != null) textData += o.ToString();
            }

            foreach (SLDataRow s in _selectedTable.Table)
            {
                textData += "\r\n";
                for (int i = 0; i < s.Row.Count; i++)
                {
                    if (i > 0) textData += "\t";
                    object o = s.Row[i];
                    if (o != null) textData += o.ToString();
                }
            }
            Clipboard.SetText(textData);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            if (_dataTables == null || _dataTables.Count == 0) return;
            var mr = MessageBox.Show("Удалить все таблицы?", "", MessageBoxButton.OKCancel);
            if (mr != MessageBoxResult.OK) return;
            ClearData(true);
        }

        public void ClearData(bool showWindow)
        {
            MainChartView.Reset();
            Description = string.Empty;
            _dataTables = new List<SLDataTable>();
            _sourceSteps = new List<Step>();
            _selectedTable = null;
            _hasAttachedCalculation = false;
            _pasteCounter = 1;
            BeginAnalysis(null);
        }

        #endregion

        private void AddInternalTablesToAnalysisDataButton_Click(object sender, RoutedEventArgs e)
        {
            var steps = _internalReportData.Select(a => StepFactory.CreateTable(a).SingleOrDefault()).ToList();
            LoadDataWithCalculation(_internalReportData, steps, null, null, null, null, false);
        }

        private void ExportInternalTablesToXlsxButton_Click(object sender, RoutedEventArgs e)
        {
            Guid task1Guid = SetupExportToXlsxService();
            _rdsc.FillDataWithSLTablesAsync(task1Guid, ConvertSLTablesToReportTables(_internalReportData));
        }

        private void DeleteRow_Click(object sender, RoutedEventArgs e)
        {
            if (MainDataGrid.SelectedItems.Count > 1)
            {
                var all = MainDataGrid.ItemsSource.Cast<object>().ToList();

                var toDelete = (from object s in MainDataGrid.SelectedItems where all.IndexOf(s) != 0 select _selectedTable.Table[all.IndexOf(s)]).ToList();

                foreach (var t in toDelete)
                {
                    _selectedTable.Table.Remove(t);
                }
            }
            else if (MainDataGrid.SelectedIndex > 0) _selectedTable.Table.RemoveAt(MainDataGrid.SelectedIndex);
            RefreshDatagrid();
        }
    }
}