﻿using System;
using System.Globalization;
using System.Linq;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using AQChartLibrary;
using AQControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQAnalysisLibrary
{
    public partial class DataAnalysis
    {
        private void MainTableSelector_OnSelectedTableChanged(object o, SelectedTableChangedEventArgs e)
        {
            _currentChartStep = null;
            AnalyzeTable(e.SelectedTable.TableName);
            MainTableSelector.Focus();
        }

        private void BeginAnalysis(string showTableName)
        {
            if (_dataTables == null || _dataTables.Count == 0) NoTableMode();
            else AnalyzeTable(MainTableSelector.CreateTableSelectors(_dataTables, !string.IsNullOrEmpty(showTableName)
                ? _dataTables.FirstOrDefault(a => a.TableName == showTableName)
                : _dataTables[GetDefaultTableIndex()]));
        }

        private void AnalyzeTable(string tableName)
        {
            ShowPeriod();
            SelectTable(tableName);
            if (_selectedTable.Table == null || _selectedTable.ColumnNames == null || _selectedTable.DataTypes == null)
            {
                BeginLoadEmptyTable();
                return;
            }
            SetTableInfo();
            UpdateButtonsState();
            EnableChartView();
            DoAnalysis();
        }

        private void ViewType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewType == null) return;
            IsolatedStorageSettings.ApplicationSettings["AnalysisTab"] = ViewType.SelectedIndex;
            SetButtonsVisibility();
            ClosePopups();
            if (_selectedTable != null) DoAnalysis();
        }

        private void DoAnalysis()
        {
            if (ViewType.SelectedIndex == 0) MakeTable();
            else MainChartView.DoAnalysis(_dataTables, _selectedTable, _sourceSteps, _currentChartStep, _from, _to, Description);
        }

        private void SetTableInfo()
        {
            TableInfo.Text = _selectedTable == null ? string.Empty : $"n={_selectedTable.Table.Count}, m={_selectedTable.ColumnNames.Count}";
        }

        private void SelectTable(string tableName)
        {
            var sldt = (from d in _dataTables where d.TableName == tableName select d).First();
            _selectedTable = sldt;
            _to = sldt.GetMaximumDate();
            _from = sldt.GetMinimumDate();
        }

        private void NoTableMode()
        {
            UpdateButtonsState();
            DisableChartView();
            MainDataGrid.Columns.Clear();
            MainDataGrid.ItemsSource = null;
            MainDataGrid.UpdateLayout();
            TableInfo.Text = string.Empty;
        }

        
        private void ShowPeriod()
        {
            var subName = _hasAttachedCalculation ? " расчета" : string.Empty;
            ViewerHeader.Text = _from.HasValue && _to.HasValue
                                    ? $"Данные{subName} за период {_from.Value.ToShortDateString()} - {_to.Value.ToShortDateString()}."
                                    : $"Данные{subName}";
        }

        private int GetDefaultTableIndex()
        {
            var defaultTable = 0;
            if (_dataTables == null) return -1;
            if (_dataTables.Count > 1 && _dataTables[0].TableName == "Переменные") defaultTable++;
            var tableNames = _dataTables.Select(a => a.TableName).ToList();
            if (MainChartView.GetSelectedTable() == null)
            {
                var previousExisted = tableNames.IndexOf(_selectedTable.TableName);
                if (previousExisted >= 0) defaultTable = previousExisted;
            }
            else
            {
                var requiredTable = tableNames.IndexOf(MainChartView.GetSelectedTable().TableName);
                if (requiredTable >= 0) defaultTable = requiredTable;
            }
            return defaultTable;
        }

        private void DisableChartView()
        {
            ViewType.SelectedIndex = 0;
            ChartTab.IsEnabled = false;
        }

        private void EnableChartView()
        {
            ChartTab.IsEnabled = true;
        }

        private void MakeTable()
        {
            ResetSubscription();
            MainDataGrid.Columns.Clear();
            foreach (var s in _selectedTable.ColumnNames.Where(a => !a.StartsWith("#")))
            {
                var i = _selectedTable.ColumnNames.IndexOf(s);
                var dgc = new DataGridTemplateColumn
                {
                        Header = s,
                        HeaderStyle = (Style)Resources["DataGridColumnFilteredHeader"],
                        CanUserSort = true,
                        SortMemberPath = "Row[" + i.ToString(CultureInfo.InvariantCulture) + "]"
                    };
                var bindingText = "{Binding Row[" + i.ToString(CultureInfo.InvariantCulture) + "]}";
                var bindingTextEdit = "{Binding Row[" + i.ToString(CultureInfo.InvariantCulture) + "], Converter={StaticResource DataTypeValidator" + i + "},ConverterParameter=" + _selectedTable.DataTypes[i] + ", Mode=TwoWay, NotifyOnValidationError=True, ValidatesOnExceptions=True,ValidatesOnDataErrors=True, ValidatesOnNotifyDataErrors=True}";
                var bindingBG = "#00000000";
                if (_selectedTable.ColumnNames.Contains("#" + s + "_Цвет"))
                {
                    var j = _selectedTable.ColumnNames.IndexOf("#" + s + "_Цвет");
                    bindingBG = "{Binding Path=Row[" + j + "], Converter={StaticResource BGColorConverter" + i + "}}";
                }
                var template =
                    string.Format(
                        @"<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' xmlns:this='clr-namespace:AQAnalysisLibrary;assembly=AQAnalysisLibrary'>
                    <Grid MinHeight='18'>
                    <Grid.Resources>
                        <this:BGColorConverter x:Key='BGColorConverter{2}' />
                    </Grid.Resources>
                    <Rectangle Fill='{1}'>
                    </Rectangle><TextBlock Text='{0}' Margin='2' /></Grid></DataTemplate>",
                        bindingText, bindingBG, i);
                var templateEdit =
                   string.Format(
                       @"<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml' xmlns:this='clr-namespace:AQAnalysisLibrary;assembly=AQAnalysisLibrary'>
                    <Grid>
                    <Grid.Resources>
                        <this:BGColorConverter x:Key='BGColorConverter{2}' />
                        <this:DataTypeValidator x:Key='DataTypeValidator{2}' />
                    </Grid.Resources>
                    <Rectangle Opacity='0.5' Fill='{1}'>
                    </Rectangle><TextBox Text='{0}' BorderThickness='0' Margin='0' /></Grid></DataTemplate>",
                       bindingTextEdit, bindingBG, i);
                dgc.CellTemplate = (DataTemplate)XamlReader.Load(template);
                dgc.CellEditingTemplate = (DataTemplate)XamlReader.Load(templateEdit);
                MainDataGrid.Columns.Add(dgc);
            }
            MainDataGrid.CanUserSortColumns = true;
            MainDataGrid.FrozenColumnCount = 1;
            MainDataGrid.ItemsSource = _selectedTable.Table;
        }

        private void RefreshDatagrid()
        {
            if (MainDataGrid == null) return;
            MainDataGrid.ItemsSource = _selectedTable.Table;
            SetTableInfo();
            if (MainDataGrid.SelectedItem != null)
            {
                MainDataGrid.ScrollIntoView(
                    _selectedTable.Table[Math.Min(MainDataGrid.SelectedIndex, _selectedTable.Table.Count - 1)],
                    MainDataGrid.CurrentColumn);
            }
        }
        
        private void MainDataGrid_OnCurrentCellChanged(object sender, EventArgs e)
        {
            ClosePopups();
            if (MainDataGrid.SelectedItems.Count > 1) return;
            MainDataGrid.BeginEdit();
        }

        private void MainDataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1) + "  ";
        }

        private void MainTableCellsColorSelector_OnColorChanged(object sender, ColorChangedEventArgs e)
        {
            SetColorMarkOnTable(e);
        }
        
    }
    public class DataTypeValidator : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.ToString()==String.Empty) return null;
            switch (parameter.ToString())
            {
                case "Double":
                case "System.Double":
                    double doubleResult;
                    var isDouble = double.TryParse(value.ToString(), out doubleResult);
                    if (isDouble) return doubleResult;
                    MessageBox.Show("Ошибка ввода. Это числовое поле");
                    return null;
                    
                case "DateTime":
                case "System.DateTime":
                    DateTime dateTimeResult;
                    var isDateTime = DateTime.TryParseExact(value.ToString(), "MMMM yyyy", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime = !isDateTime && DateTime.TryParse(value.ToString(), out dateTimeResult);
                    if (isDateTime) return dateTimeResult;
                    MessageBox.Show("Ошибка ввода. Это поле типа Дата/Время");
                    return null;

                case "TimeSpan":
                case "System.TimeSpan":
                    TimeSpan timeSpanResult;
                    var isTimeSpan = TimeSpan.TryParse(value.ToString(), out timeSpanResult);
                    if (isTimeSpan) return timeSpanResult;
                    MessageBox.Show("Ошибка ввода. Это поле типа Время");
                    return null;

                case "Int32":
                case "System.Int32":
                    Int32 int32Result;
                    var isInt32 = Int32.TryParse(value.ToString(), out int32Result);
                    if (isInt32) return int32Result;
                    MessageBox.Show("Ошибка ввода. Это целочисленное поле");
                    return null;

                case "Boolean":
                case "System.Boolean":
                    Boolean booleanResult;
                    var isBoolean = Boolean.TryParse(value.ToString(), out booleanResult);
                    if (isBoolean) return booleanResult;
                    MessageBox.Show("Ошибка ввода. Это двоичное поле");
                    return null;

                default:
                    return value.ToString();
            }
        }
    }

    public class BGColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return new SolidColorBrush(Colors.Transparent);
            var c = value.ToString();
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);

            var cu = System.Convert.ToInt32(c.Replace("#", "0x"), 16);
            Brush fill = new SolidColorBrush(Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff),
                                                   (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff)));
            return fill;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class FGColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? new SolidColorBrush(Colors.Black) : new SolidColorBrush(Colors.Blue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}