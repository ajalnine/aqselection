﻿using System;
using System.Windows;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace SLWorkPlace
{
    public partial class SystemMap
    {
        public delegate void NavigateDelegate(object sender, SystemMapEventArgs e);
        public event NavigateDelegate Navigate;
        public SystemMap()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            AQButton.IsChecked = false;
        }

        private void VectorButton_Click(object sender, RoutedEventArgs e)
        {
            if (Navigate != null)
            {
                var vb = sender as VectorButton;
                if (vb != null) Navigate.Invoke(this, new SystemMapEventArgs { NavigateTo = vb.Tag.ToString() });
            }
            AQButton.IsChecked = false;
        }
    }

    public class SystemMapEventArgs : EventArgs
    {
        public string NavigateTo { get; set; }
    }
}
