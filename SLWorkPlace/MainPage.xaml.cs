﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Reflection;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQBasicControlsLibrary;
using AQChartViewLibrary;
using AQControlsLibrary;

namespace SLWorkPlace
{
    public partial class MainPage : IAQModule, IServiceProvider
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool _initialized = false;        
        private string _initName;
        private string _assemblyName;
        private readonly AQService _aqs;
        private readonly ImageSource _newCalculation;
        private readonly AQModuleDescription _aqmd;
        private readonly Dictionary<Guid, AQModuleDescription> _boundModules;
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name }; 
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            try
            {
                gd.Clear(Microsoft.Xna.Framework.Color.White);
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }
            
            _newCalculation = new BitmapImage {UriSource = new Uri("/AQResources;component/Images/Catalogue/Calculation.png", UriKind.Relative)}; 
            _boundModules = new Dictionary<Guid, AQModuleDescription>();
            Signal.WorkPlace = this;
            XnaContent.AppContentManager = new ContentManager(this) ;
            _aqs = Services.GetAQService();
        }

        #region Загрузка модулей

        private void AddTask(string assemblyName, string toInstance, CommandCallBackDelegate commandCallBack)
        {
            _initName = toInstance;
            _assemblyName = assemblyName;
            var name = $"{ assemblyName + "."+toInstance}, {assemblyName}, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            var loadedPage = (UserControl)Activator.CreateInstance(Type.GetType(name));

            var ti = new TabItem
            {
                Style = Resources["TaskTabItemStyle"] as Style,
                Padding = new Thickness(0),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Stretch
            };
            ti.MouseLeave += ti_MouseLeave;
            MainPanel.Items.Add(ti);
            MainPanel.SelectedItem = ti;

            var aqModule = loadedPage as IAQModule;
            if (aqModule != null)
            {
                var aqmd = aqModule.GetAQModuleDescription();
                var mti = GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]);
                loadedPage.HorizontalAlignment = HorizontalAlignment.Left;
                loadedPage.VerticalAlignment = VerticalAlignment.Top;
                loadedPage.Loaded += LoadedPage_Loaded;
                var signalledModule = ((IAQModule)loadedPage);
                if (commandCallBack != null) signalledModule.InitSignaling(commandCallBack as CommandCallBackDelegate);
                signalledModule.ReadyForAcceptSignals += MainPage_ReadyForAcceptSignals;

                Signal.AttachAQModule(aqmd);
                aqmd.Container = ti;
                var fill = new SolidColorBrush(Color.FromArgb(255,
                    (byte)(255.0d - (255.0d - mti.ColorMark.R) / 64.0d),
                    (byte)(255.0d - (255.0d - mti.ColorMark.G) / 64.0d),
                    (byte)(255.0d - (255.0d - mti.ColorMark.B) / 64.0d)));

                if (!mti.NoScroll)
                {
                    var internalSv = new ScrollViewer
                    {
                        HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                        VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                        Margin = new Thickness(0),
                        Padding = new Thickness(1),
                        Content = loadedPage,
                        Background = fill
                    };
                    ti.Content = internalSv;
                    Dispatcher.BeginInvoke(() => internalSv.ScrollToVerticalOffset(0));
                }
                else
                {
                    ti.Content = loadedPage;
                    loadedPage.Background = fill;
                }

                ti.Tag = aqmd;
                ti.Background = new SolidColorBrush(mti.ColorMark);

                var tpi = new TaskPanelItem { HighLight = Colors.Transparent, Text = mti.Caption, HorizontalAlignment = HorizontalAlignment.Right, TabToClose = ti, Closeable = mti.Closeable };
                ti.Header = tpi;

                Observable.FromEvent<EventArgs>(tpi, "CloseClick").Subscribe(x =>
                {
                    var taskPanelItem = x.Sender as TaskPanelItem;
                    if (taskPanelItem == null) return;
                    var t = taskPanelItem.TabToClose;

                    var aqmdClosing = (t.Tag as AQModuleDescription);
                    Signal.DetachAQModule(aqmdClosing);
                    if (aqmdClosing != null &&
                        (_boundModules != null && _boundModules.ContainsValue(aqmdClosing)))
                    {
                        var toDelete = _boundModules.Where(b => b.Value == aqmdClosing).Select(b => b.Key).ToList();
                        foreach (var d in toDelete)
                        {
                            _boundModules.Remove(d);
                        }
                        _boundModules.Remove(aqmdClosing.UniqueID);
                    }
                    MainPanel.Items.Remove(t);
                });
            }

            _aqs.BeginWriteLog("Load:" + assemblyName, null, new object());
        }

        private void InitApplication()
        {
            string initString;
            LoadByUri(
                HtmlPage.Document.QueryString.TryGetValue("Init", out initString)
                    ? initString
                    : "SLTitle.MainPage", null);
            var sl5 = Application.Current.Host.IsVersionSupported("5.0")
                ? "5"
                : Application.Current.Host.IsVersionSupported("4.0") ? "4" : "old";
            Loader.TabSizeChanged(this, new Size(MainPanel.ActualWidth, MainPanel.ActualHeight));
            _aqs.BeginWriteLog(string.Format("SL Version: {0};IP=@IP", sl5), null, new object());
        }

        private void LoadByUri(string uri, CommandCallBackDelegate commandCallBack)
        {
            var initValues = uri.Split('.');
            AddTask(initValues[0], initValues[1], commandCallBack);
        }

        private void MainMenu_XAPSelected(object o, AssemblySelectedEventArgs e)
        {
            _initName = e.ClassName;
            _assemblyName = e.Assembly;
            AddTask(e.Assembly, e.ClassName, null);
        }

        void ti_MouseLeave(object sender, MouseEventArgs e)
        {
            RefreshThumbnail();
        }

        void LoadedPage_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshThumbnail();
        }

        private void RefreshThumbnail()
        {
            var st = MainPanel.SelectedItem as TabItem;
            if (st == null) return;
            var aqmd = st.Tag as AQModuleDescription;
            if (aqmd == null) return;
            aqmd.Thumbnail = new WriteableBitmap(aqmd.Instance as UIElement, new ScaleTransform { ScaleX = 0.3, ScaleY = 0.3 });
            var i = new Image{ Source = aqmd.Thumbnail };
            var tt = new ToolTip { Style = Resources["ThumbnailStyle"] as Style, Content = i, HorizontalOffset = 0, VerticalOffset = 10 };
            var uiElement = st.Header as UIElement;
            if (uiElement != null) uiElement.SetValue(ToolTipService.ToolTipProperty, tt);
        }

        private MenuTreeItem GetMenuTreeItemByAssemblyName(string assemblyName)
        {
            var mtr = Resources["MenuTreeData"] as MenuTreeRoot;
            if (mtr != null) return mtr.Children.Select(mti => FindMenuTreeItemFrom(mti, assemblyName)).FirstOrDefault(mt => mt != null);
            return null;
        }

        private MenuTreeItem FindMenuTreeItemFrom(MenuTreeItem mti, string assemblyName)
        {
            return mti.Assembly == assemblyName 
                ? mti 
                : mti.Children.Select(mt => FindMenuTreeItemFrom(mt, assemblyName)).FirstOrDefault(m => m != null);
        }

        #endregion
        
        #region "Expose"

        private void ThumbnailsSelection(object o, RoutedEventArgs e)
        {
            ThumbView.Children.Clear();
            
            foreach (var i in MainPanel.Items)
            {
                var ti = i as TabItem;
                if (ti == null) continue;
                var aqmd = ti.Tag as AQModuleDescription;
                if (aqmd != null && aqmd.Thumbnail == null) return;
                if (aqmd == null) continue;
                var mti = GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]);
                var b = new TextImageButtonBase {Style=Resources["ThumbButton"] as Style, Text = mti.Caption, Width = aqmd.Thumbnail.PixelWidth+20, Height = aqmd.Thumbnail.PixelHeight+20, ImageSource = aqmd.Thumbnail, Tag = ti, Margin = new Thickness(10), MaxHeight = 400 };
                b.Click += b_Click;
                ThumbView.Children.Add(b);
            }
            ThumbPanel.Visibility = Visibility.Visible;
            ThumbView.UpdateLayout();
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null) MainPanel.SelectedItem = button.Tag as TabItem;
            HideTaskSelection();
        }

        private void HideTaskSelection()
        {
            ThumbView.Children.Clear();
            ThumbPanel.Visibility = Visibility.Collapsed;
            BackAQButton.IsChecked = false;
        }

        private void BackAQButton_Click(object sender, RoutedEventArgs e)
        {
            HideTaskSelection();
        }


        #endregion

        #region Обработка сигналов

        void MainPage_ReadyForAcceptSignals(object o, ReadyForAcceptSignalsEventArg e)
        {
            var ccbd = e.CommandCallBack;
            if (ccbd != null) ccbd.Invoke(Command.Load, null, o);
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            switch (commandType)
            {
                case Command.Renew:
                    SetScroller(0);
                    return;

                case Command.SetScroller:
                    SetScroller((double)commandArgument);
                    return;

                case Command.InsertSlideElement:

                    if (MainSlidePanel != null) SendSlideToSlidePanel(commandArgument);
                    return;

                case Command.Load:

                    var ca = commandArgument as Dictionary<string, object>;
                    var uri = string.Empty;
                    if (ca != null && ca.ContainsKey("URI")) uri = (String)ca["URI"];

                    LoadByUri(uri, (t, a, r) =>
                    {
                        if (commandCallBack!=null) commandCallBack.Invoke(commandType, commandArgument, r);
                    });
                    return;


                case Command.InsertStoredCalculation:
                    LoadByUri("SLConstructor_Calculations.Page", (t, a, r) =>
                    {
                        var loadedCalculationConstructor = r as IAQModule;
                        if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), Command.InsertStoredCalculation, commandArgument, null);
                    });
                    return;

                case Command.PasteToAnalysis:
                    var c = commandArgument as Dictionary<string, object> ?? new Dictionary<string, object>();
                    c.Add("Text", Clipboard.GetText());
                    SendCommandToAnalisys(c, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                case Command.PasteToCalculation:
                    var c2 = commandArgument as Dictionary<string, object> ?? new Dictionary<string, object>();
                    c2.Add("Text", Clipboard.GetText());
                    SendCommandToCalculation(c2, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                case Command.InsertTablesToAnalysis:
                case Command.ReopenInAnalisys:
                case Command.SendTableToAnalysis:
                    SendCommandToAnalisys(commandArgument, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                case Command.StartNewAnalysis:
                    LoadByUri("SLAnalysis.MainPage", (t, a, r) =>
                    {
                        var loadedCalculationConstructor = r as IAQModule;
                        if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), commandType, commandArgument, null);
                    });
                    break;
                case Command.Clear:
                    CloseAllModules();
                    break;

                case Command.InsertReport:
                    SendCommandToReport(commandArgument, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                default:
                    SendCommandToCalculation(commandArgument, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    return;
            }
        }

        private void SetScroller(double offset)
        {
            var tab2 = MainPanel.SelectedItem as TabItem;
            if (tab2 != null)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    var sv = (tab2.Content as ScrollViewer);
                    if (sv == null) return;
                    sv.InvalidateScrollInfo();
                    sv.ScrollToVerticalOffset(offset);
                    sv.UpdateLayout();
                });
            }
        }

        private void CloseAllModules()
        {
            Signal.ClearAQModules();
            MainPanel.Items.Clear();
            MainSlidePanel.Clear();
            InitApplication();
        }

        private void SendSlideToSlidePanel(object commandArgument)
        {
            var ca = commandArgument as Dictionary<string, object>;
            Step operation = null;
            List<Step> sourceSteps = null;
            var sourceCalcGuid = Guid.NewGuid();
            WriteableBitmap wb = null, icon = null;
            ColorScales cs = ColorScales.HSV;
            SlideElementDescription sed = null;
            List<SLDataTable> sourceData = null;
            bool inv = false;

            DateTime? from = null, to = null;
            if (ca != null)
            {
                if (ca.ContainsKey("From")) from = (DateTime) ca["From"];
                if (ca.ContainsKey("To")) to = (DateTime) ca["To"];

                if (ca.ContainsKey("Operation")) operation = (Step) ca["Operation"];
                if (ca.ContainsKey("Calculation")) sourceSteps = (List<Step>) ca["Calculation"];
                if (ca.ContainsKey("SourceGuid")) sourceCalcGuid = (Guid) ca["SourceGuid"];
                if (ca.ContainsKey("Thumbnail")) wb = (WriteableBitmap) ca["Thumbnail"];
                if (ca.ContainsKey("Icon")) icon = (WriteableBitmap)ca["Icon"];
                if (ca.ContainsKey("SlideElement")) sed = (SlideElementDescription) ca["SlideElement"];
                if (ca.ContainsKey("SourceData")) sourceData = (List<SLDataTable>) ca["SourceData"];
                if (ca.ContainsKey("ColorScale")) cs = (ColorScales)ca["ColorScale"];
                if (ca.ContainsKey("Inverted")) inv = (bool)ca["Inverted"];
            }

            OpenSlidePanel();
            var ar = new AnalysisResult {SED = sed, InnerSteps = sourceSteps, InnerTables = null, AnalysisGuid = sourceCalcGuid, AnalysisOperation = operation,
                ThumbNail = wb, Icon = icon, UsedColorScale = cs, IsInverted = inv, From = from, To = to, OnlyInnerSteps = false, AvailableData = sourceData, UsedTable = null, UsedFields = null};
            MainSlidePanel.InsertAnalysisResultElement(ar);
        }

        private void SendCommandToCalculation(object commandArgument, Command c, AQModuleDescription senderDescription)
        {
            var calcs = (from t in MainPanel.Items let aqmd = ((AQModuleDescription)((TabItem)t).Tag) where aqmd.UniqueID != senderDescription.UniqueID && GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]).Caption.Trim() == "Расчёты" select t).ToList();
            if (!calcs.Any())
            {
                LoadByUri("SLConstructor_Calculations.Page", (t, a, r) =>
                {
                    var loadedCalculationConstructor = r as IAQModule;
                    if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), c, commandArgument, null);
                });
            }
            else SelectModule(calcs.Cast<TabItem>(), commandArgument, c, "Новый расчет", "Выбор расчета для вставки запроса", "SLConstructor_Calculations.Page");
        }

        private void SendCommandToReport(object commandArgument, Command c, AQModuleDescription senderDescription)
        {
            LoadByUri("SLReport.MainPage", (t, a, r) =>
            {
                var loadedReportViewer = r as IAQModule;
                if (loadedReportViewer != null) Signal.Send(GetAQModuleDescription(), loadedReportViewer.GetAQModuleDescription(), c, commandArgument, null);
            });
        }

        private void SendCommandToAnalisys(object commandArgument, Command c, AQModuleDescription senderDescription)
        {
            var loadedmodules = (from t in MainPanel.Items
                let aqmd = ((AQModuleDescription) ((TabItem) t).Tag)
                where GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]).Caption?.Trim() == "Анализ"
                && aqmd?.UniqueID!=senderDescription?.UniqueID
                select t).ToList();

            var ca = commandArgument as Dictionary<string, object>;
            var onlyNew = false;
            if (ca!=null && ca.ContainsKey("New")) onlyNew = (bool)ca["New"];
            if (senderDescription!=null && _boundModules.ContainsKey(senderDescription.UniqueID))
            {
                Signal.Send(GetAQModuleDescription(), _boundModules[senderDescription.UniqueID], c, commandArgument, null);
                MainPanel.SelectedItem = _boundModules[senderDescription.UniqueID].Container; 
                return;
            }
            
            if (!loadedmodules.Any() || onlyNew)
            {
                LoadByUri("SLAnalysis.MainPage", (t, a, r) =>
                {
                    var loadedAnalysis = r as IAQModule;
                    if (loadedAnalysis == null) return;
                    if (onlyNew && senderDescription!=null)_boundModules.Add(senderDescription.UniqueID, ((IAQModule)r).GetAQModuleDescription());
                    Signal.Send(GetAQModuleDescription(), loadedAnalysis.GetAQModuleDescription(), c, commandArgument, null);
                });
            }
            else SelectModule(loadedmodules.Cast<TabItem>(), commandArgument, c, "Новый анализ", "Выбор анализа для вставки данных", "SLAnalysis.MainPage");
        }

        private void SelectModule(IEnumerable<TabItem> tis, object commandArgument, Command currentCommand, string nameOfNewModule, string title, string newModuleURI)
        {
            ModuleView.Children.Clear();
            SelectModuleWindow.Tag = commandArgument;
            SelectModuleWindow.Title = title;
            var newModuleButton = new TextImageButtonBase { Style = Resources["ThumbButton"] as Style, Text = nameOfNewModule, Width = 100, Height = 100, ImageSource = _newCalculation, Margin = new Thickness(10) };
            newModuleButton.Click += modulebutton_Click; 
            newModuleButton.Tag = new AQModuleDescription { CustomData = currentCommand, Name = "NewModule", URL = newModuleURI}; 
            ModuleView.Children.Add(newModuleButton);
            
            foreach (var ti in tis)
            {
                var aqmd = ti.Tag as AQModuleDescription;
                if (aqmd != null && aqmd.Thumbnail == null) return;
                if (aqmd == null) continue;
                var calcbutton = new TextImageButtonBase { Style = Resources["ThumbButton"] as Style, Text = aqmd.ShortDescription, Width = aqmd.Thumbnail.PixelWidth + 10, Height = aqmd.Thumbnail.PixelHeight + 10, ImageSource = aqmd.Thumbnail, Tag = ti, Margin = new Thickness(10), MaxHeight = 400 };
                calcbutton.Tag = aqmd;
                aqmd.CustomData = currentCommand;
                calcbutton.Click += modulebutton_Click; 
                ModuleView.Children.Add(calcbutton);
            }
            ModuleView.UpdateLayout();
            SelectModuleWindow.Visibility = Visibility.Visible;
        }


        void modulebutton_Click(object sender, RoutedEventArgs e)
        {
            SelectModuleWindow.Visibility = Visibility.Collapsed;
            var aqmd = ((TextImageButtonBase)sender).Tag as AQModuleDescription;
            if (aqmd == null) return;
            if (aqmd.Name != "NewModule")
            {
                Signal.Send(GetAQModuleDescription(), aqmd, (Command)aqmd.CustomData, SelectModuleWindow.Tag, null);
                MainPanel.SelectedItem = aqmd.Container;
            }
            else
            {
                LoadByUri(aqmd.URL, (t, a, r) =>
                {
                    var loadedCalculationConstructor = r as IAQModule;
                    if (loadedCalculationConstructor == null) return;
                    Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), (Command)aqmd.CustomData, SelectModuleWindow.Tag, null);
                });
            }
        }

        private void SelectCalculationWindow_Closing(object sender, CancelEventArgs e)
        {
            ModuleView.Children.Clear();
            SelectModuleWindow.Visibility = Visibility.Collapsed;
            SelectModuleWindow.Tag = null;
        }

        #endregion

        #region Панель слайдов

        private void OpenSlidesPanelButton_Click(object sender, RoutedEventArgs e)
        {
            OpenSlidePanel();
        }

        private void OpenSlidePanel()
        {
            RightPanelHiddenView.Visibility = Visibility.Collapsed;
            RightPanelOpenView.Visibility = Visibility.Visible;
            if (RightPanel.ActualWidth < 130)
            {
                SlidePanelColumnDefinition.Width = new GridLength(175);
            }
        }

        private void CloseSlidesPanelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseSlidePanel();
        }

        private void CloseSlidePanel()
        {
            RightPanelHiddenView.Visibility = Visibility.Visible;
            RightPanelOpenView.Visibility = Visibility.Collapsed; 
            SlidePanelColumnDefinition.Width = new GridLength(12);
        }

        #endregion 

        private void RightPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Math.Abs(e.NewSize.Width - 12) < 1e-8)
            {
                RightPanelHiddenView.Visibility = Visibility.Visible;
                RightPanelOpenView.Visibility = Visibility.Collapsed; 
            }
            else
            {
                RightPanelHiddenView.Visibility = Visibility.Collapsed;
                RightPanelOpenView.Visibility = Visibility.Visible;
            }
        }

        private void MainPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Loader.TabSizeChanged(this, new Size(MainPanel.ActualWidth, MainPanel.ActualHeight));
        }

        private void DrawingSurface_OnDraw(object sender, DrawEventArgs e)
        {
        }

        public object GetService(Type serviceType)
        {
            return GraphicsDeviceManager.Current.GraphicsDevice;
        }

        private void FrameworkElement_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_initialized || sender == null || Math.Abs(((FrameworkElement) sender).ActualWidth) < 1e-8 ||
                Math.Abs(((FrameworkElement) sender).ActualHeight) < 1e-8) return;

            InitApplication();
            _initialized = true;
        }
    }
}