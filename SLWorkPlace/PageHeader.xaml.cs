﻿using System.Globalization;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Threading;
using ApplicationCore;
using ApplicationCore.AQServiceReference;


namespace SLWorkPlace
{
    public partial class PageHeader 
    {
        public event RoutedEventHandler AqClick;
        private ActualityList _currentActualityList;
        private readonly AQService _aqs;
        
        public PageHeader()
        {
            InitializeComponent();

            if (Security.CurrentUserName != null)
            {
                var chiefAppend =  (Security.IsChief() ? "+" : string.Empty);
                UserHeader.Text = "Пользователь" + chiefAppend;
                var refreshActualityTimer = new DispatcherTimer {Interval = new TimeSpan(0, 10, 0)};
                refreshActualityTimer.Tick += RefreshActualityTimer_Tick;
                refreshActualityTimer.Start();
                UserName.Text = Security.CurrentUserName;
                var s = string.Empty;
                var isFirst = true;
                foreach (var r in Security.Roles.Where(a => a != "Администратор" && a != String.Empty))
                {
                    if (!isFirst) s += "; ";
                    isFirst = false;
                    s += r;
                }
                UserRoles.Text = s;
                if (s.Length > 0) UserRoles.Visibility = Visibility.Visible;
                else UserHeader.Text = "Гость" + chiefAppend;

                if (Security.IsInRole("Администратор")) UserHeader.Text = "Администратор" + chiefAppend;
                var isMale = Security.CurrentUserName.Trim().ToLower().EndsWith("ч");
                Female.Visibility = isMale ? Visibility.Collapsed : Visibility.Visible;
                Male.Visibility = isMale ? Visibility.Visible : Visibility.Collapsed;
            }
            _aqs = Services.GetAQService();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateActuality();
        }

        private void RefreshActualityTimer_Tick(object sender, EventArgs e)
        {
            UpdateActuality();
        }

        private void UpdateActuality()
        {
            _aqs.BeginGetActualityList(GetActualityListDone, null);
        }

        private void GetActualityListDone(IAsyncResult iar)
        {
            try
            {
                ActualityList al = _aqs.EndGetActualityList(iar);
                bool dataChanged = false;
                if (_currentActualityList != null)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        if (al.Actualities[i].Date != _currentActualityList.Actualities[i].Date) dataChanged = true;
                    }
                }
                if (_currentActualityList == null || dataChanged)
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        if (_currentActualityList != null) NewDataStoryboard.Begin();
                        _currentActualityList = al;
                        ActualityView.ItemsSource = null;
                        ActualityView.UpdateLayout();
                        ActualityView.ItemsSource = _currentActualityList?.Actualities;
                        ActualityView.UpdateLayout();
                    });
                }
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch
            { }
        }

        private void AQButton_Click(object sender, RoutedEventArgs e)
        {
            if (AqClick != null) AqClick.Invoke(this, e );
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Выйти из системы ?", String.Empty, MessageBoxButton.OKCancel);
            if (mbr== MessageBoxResult.OK) HtmlPage.Window.Navigate(new Uri("http://itc-serv01.chmk.mechelgroup.ru/"));
        }

        private void PasteToAnalysisButton_Click(object sender, RoutedEventArgs e)
        {
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.PasteToAnalysis, null, null);
        }

        private void PasteToCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.PasteToCalculation, null, null);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Будут закрыты все модули и удалены все слайды. Продолжить ?", String.Empty, MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.OK) Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.Clear, null, null);
        }

        private void ChartPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            Loader.InvisibleCanvas = sender as Canvas;
        }
    }

    public class ActualityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return new SolidColorBrush(Colors.Transparent);
            DateTime p = DateTime.ParseExact(value.ToString(), "dd.MM HH:mm", null);
            Color c = (DateTime.Now - p).Days >= 2 ? Color.FromArgb(0xe0, 0x96, 0x53, 0x26) : Color.FromArgb(0x50, 0x00, 0x00, 0x00);
            return new SolidColorBrush(c);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
