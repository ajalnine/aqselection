﻿using System.Collections.Generic;
using System.Windows;
using ApplicationCore.ReportingServiceReference;
using AQBasicControlsLibrary;
using AQControlsLibrary;

namespace SLWorkPlace
{
    public partial class MainPage
    {
        private void ExportPPTX_Click(object sender, RoutedEventArgs e)
        {
            var pd = CreatePresentationDescription();

            PPTXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            CommonTasks.CreatePPTX(this, pd, "Презентация ", () => Dispatcher.BeginInvoke(() =>
            {
                PPTXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        private void ExportDOCX_Click(object sender, RoutedEventArgs e)
        {
            var pd = CreatePresentationDescription();

            DOCXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            CommonTasks.CreateDOCX(this, pd, "Отчет ", () => Dispatcher.BeginInvoke(() =>
            {
                DOCXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }
        
        private PresentationDescription CreatePresentationDescription()
        {
            var color = ColorConvertor.GetDefaultColorForScale(MainSlidePanel.GetLastColorScale()).Substring(3, 6);
            var pd = new PresentationDescription { Slides = new List<SlideDescription>(), ForegroundColor = color, BlackBackground = MainSlidePanel.IsInverted()};
            var sd = new SlideDescription { SlideElements = new List<SlideElementDescription>() };
            foreach (var csed in MainSlidePanel.InternalSlideCollection)
            {
                sd.SlideElements.Add(csed.SED);
                if (!csed.IsContinuous)
                {
                    pd.Slides.Add(sd);
                    sd = new SlideDescription { SlideElements = new List<SlideElementDescription>() };
                }
            }
            if (sd.SlideElements.Count > 0) pd.Slides.Add(sd);
            return pd;
        }
        
        private void ResetButtonStates()
        {
            EnableButtons(MainSlidePanel.InternalSlideCollection.Count != 0);
        }
        
        public void EnableButtons(bool enable)
        {
            DeleteAll.IsEnabled = enable;
            ExportPPTX.IsEnabled = enable;
            ExportDOCX.IsEnabled = enable;
            CreateCalc.IsEnabled = enable;
        }
        
        private void CreateCalc_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Позже будет реализовано формирование расчета для построения графиков автоматически");
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Удалить все слайды ?", "Предупреждение", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel) return;
            ResetButtonStates();
            MainSlidePanel.ResetInternalSlidesCollection();
        }

        private void MainSlidePanel_OnSlideCollectionChanged(object o, SlideCollectionChangedEventArgs e)
        {
            ResetButtonStates();
        }
    }
}
