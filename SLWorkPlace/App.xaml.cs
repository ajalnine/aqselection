﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ApplicationCore;

namespace SLWorkPlace
{
    public partial class App 
    {
        public App()
        {
            Startup += Application_Startup;
            Exit += Application_Exit;
            UnhandledException += Application_UnhandledException;
            
            InitializeComponent();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            var ip = e.InitParams.ContainsKey("UID")? e.InitParams["UID"] : null;
            var cs = e.InitParams.ContainsKey("CS") ? e.InitParams["CS"] : null;
            Connection.ConnectionString = cs;
            SLAdministration.CheckRoles.ReadRolesAsync(ip, () => { RootVisual = new MainPage(); });
        }
        
        private void Application_Exit(object sender, EventArgs e)
        {

        }

        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!System.Diagnostics.Debugger.IsAttached)
            {

                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(() => ReportError(e));
            }
        }

        private void ReportError(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");
                Services.GetAQService().BeginWriteLog(errorMsg, null, null);
                //MessageBox.Show(errorMsg, "Ошибка", MessageBoxButton.OK);
            }
// ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }
        }
    }
}
