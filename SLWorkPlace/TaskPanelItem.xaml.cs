﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SLWorkPlace
{
    public partial class TaskPanelItem
    {
        public delegate void ClickDelegate(object o, EventArgs e);
        public event ClickDelegate CloseClick;

        public TabItem TabToClose {get;set;}

        private string _text;
        public string Text 
        {
            get { return _text; }
            set {_text = value; SelectButton.Text = value;}
        }

        private Color _highlight;
        public Color HighLight
        {
            get
            {
                return _highlight;
            }
            set 
            {
                _highlight = value;
                TaskItemHighLight.Background = new SolidColorBrush { Color = value };
            }
        }

        private bool _closeable;
        public bool Closeable
        {
            get
            {
                return _closeable;
            }
            set
            {
                _closeable = value;
                CloseButton.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
            }
        }


        public TaskPanelItem()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            if (CloseClick != null) CloseClick.Invoke(this, new EventArgs());
        }
    }
}
