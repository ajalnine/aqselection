﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ApplicationCore;

// ReSharper disable once CheckNamespace
namespace SLWorkPlace
{
    [TemplateVisualState(Name = "Expanded", GroupName="PopupState")]
    [TemplateVisualState(Name = "Collapsed", GroupName="PopupState")]
    [TemplateVisualState(Name = "Normal", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "MouseOver", GroupName = "CommonStates")]
    [TemplateVisualState(Name = "Pressed", GroupName = "CommonStates")]
    public class MenuItem : Control
    {
        public Visibility ExpandableIconVisibility
        {
            get { return (Visibility)GetValue(ExpandableIconVisibilityProperty); }
            set { SetValue(ExpandableIconVisibilityProperty, value); }
        }

        public static readonly DependencyProperty ExpandableIconVisibilityProperty =
            DependencyProperty.Register("ExpandableIconVisibility", typeof(Visibility), typeof(MenuItem), new PropertyMetadata(Visibility.Collapsed));

        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.Register("Caption", typeof(string), typeof(MenuItem), new PropertyMetadata(String.Empty));

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register("IsExpanded", typeof(bool), typeof(MenuItem), new PropertyMetadata(false));

        public SolidColorBrush ColorMarkerBrush
        {
            get { return (SolidColorBrush)GetValue(ColorMarkerBrushProperty); }
            set { SetValue(ColorMarkerBrushProperty, value); }
        }

        public static readonly DependencyProperty ColorMarkerBrushProperty =
            DependencyProperty.Register("ColorMarkerBrush", typeof(SolidColorBrush), typeof(MenuItem), new PropertyMetadata(new SolidColorBrush(Colors.White)));

        public double ContainerOffsetX
        {
            get { return (double)GetValue(ContainerOffsetXProperty); }
            set { SetValue(ContainerOffsetXProperty, value); }
        }

        public static readonly DependencyProperty ContainerOffsetXProperty =
            DependencyProperty.Register("ContainerOffsetX", typeof(double), typeof(MenuItem), new PropertyMetadata(0.0d));

        public double ContainerOffsetY
        {
            get { return (double)GetValue(ContainerOffsetYProperty); }
            set { SetValue(ContainerOffsetYProperty, value); }
        }

        public static readonly DependencyProperty ContainerOffsetYProperty =
            DependencyProperty.Register("ContainerOffsetY", typeof(double), typeof(MenuItem), new PropertyMetadata(0.0d));

        public ObservableCollection<MenuItem> SubItems
        {
            get { return (ObservableCollection<MenuItem>)GetValue(SubItemsProperty); }
            set { SetValue(SubItemsProperty, value); }
        }

        public static readonly DependencyProperty SubItemsProperty =
            DependencyProperty.Register("SubItems", typeof(ObservableCollection<MenuItem>), typeof(MenuItem), new PropertyMetadata(null));

        public MenuItem ParentItem {get; set;}
        public MenuTreeItem TreeDataItem {get;set;}
        public int Level {get;set;}
        public Menu RootMenu { get; set; }
        public Style SubItemStyle { get; set; }

        private void ConstructPopup()
        {
            if (TreeDataItem.Children == null || Security.Roles == null) return;
            SubItems = new ObservableCollection<MenuItem>();
            Caption = TreeDataItem.Caption;
            ExpandableIconVisibility = (string.IsNullOrEmpty(TreeDataItem.Assembly) && TreeDataItem.Children.Count > 0) ? Visibility.Visible : Visibility.Collapsed;
            UpdateLayout();
            foreach (var s in TreeDataItem.Children.Where(a=>!a.IsHidden))
            {
                var allRolesExists = true; 
                if (!string.IsNullOrEmpty(s.RequiredRoles))
                {
                    var roles = s.RequiredRoles.Split(';').ToList();
                    if (roles.Any(v => !Security.Roles.Contains(v)))
                    {
                        allRolesExists = false;
                    }
                }
                if (!allRolesExists) continue;
                var menuItem = new MenuItem
                {
                    Style = SubItemStyle,
                    SubItemStyle = SubItemStyle,
                    ParentItem = this,
                    TreeDataItem = s,
                    Level = Level + 1,
                    RootMenu = RootMenu
                };
                UpdateLayout();
                menuItem.ColorMarkerBrush = new SolidColorBrush(s.ColorMark);
                SubItems.Add(menuItem);
            }
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            HideThisLevelPopups();
            if (TreeDataItem.Children.Any())
            {
                IsExpanded = true;

                var c = GetMenuItemOffset();
                ContainerOffsetX = c.X;
                ContainerOffsetY = c.Y;
                ConstructPopup();
                VisualStateManager.GoToState(this, "Expanded", true);
            }
            RootMenu.ActiveItem = this;
            base.OnMouseEnter(e);
            VisualStateManager.GoToState(this, "MouseOver", true);
        }

        private Point GetMenuItemOffset()
        {
            return Level != 0 ? new Point(RenderSize.Width -5 , 0) : new Point(-10, RenderSize.Height);
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            RootMenu.ActiveItem = null;
            VisualStateManager.GoToState(this, "Normal", true);
            base.OnMouseLeave(e);
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(this, "Pressed", true); 
            base.OnMouseLeftButtonDown(e);
            if (TreeDataItem.UnderConstruction) MessageBox.Show($"Раздел \"{TreeDataItem.Caption}\" находится в состоянии разработки.", "Сообщение", MessageBoxButton.OK);
            else if (!String.IsNullOrEmpty(TreeDataItem.Assembly) || !String.IsNullOrEmpty(TreeDataItem.NavigateURL) || !String.IsNullOrEmpty(TreeDataItem.Signal))
            {
                RootMenu.HideAllPopups();
                RootMenu.MenuAccepted(this);
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(this, "MouseOver", true); 
            base.OnMouseLeftButtonUp(e);
        }

        public override void OnApplyTemplate()
        {
            ConstructPopup();
            base.OnApplyTemplate();
        }

        public void HideThisLevelPopups()
        {
            if (ParentItem == null)
            {
                RootMenu.HideAllPopups();
            }
            else  foreach (var m in ParentItem.SubItems) m.IsExpanded = false;
        }

        public void HideSelfAndChildPopups()
        {
            if (SubItems == null) return;
            foreach (var m in SubItems) m.HideSelfAndChildPopups();
            IsExpanded = false;
            VisualStateManager.GoToState(this, "Collapsed", true);
        }
    }
}
