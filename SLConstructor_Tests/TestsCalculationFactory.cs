﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;
using ApplicationCore.CalculationServiceReference;
using System;

namespace SLConstructor_Tests
{
    public static partial class TestsCalculationFactory
    {
        public static List<GroupTableData> GetGroupDescriptions(bool addGroupName, ParametersList aqpl, FieldListCombination flc, Page.StatisticDescription sd)
        {
            var result = new List<GroupTableData>();

            result.AddRange(new List<GroupTableData> 
            { 
                new GroupTableData{ IsGrouping = true, FieldName = "Плавка"}, 
                new GroupTableData{ IsGrouping = true, FieldName = "Марка"},
                new GroupTableData{ IsGrouping = true, FieldName = "Цех"},
                new GroupTableData{ IsGrouping = true, FieldName = "Дата выплавки"},
                new GroupTableData{ IsGrouping = true, FieldName = "Разливка"},
                new GroupTableData{ IsGrouping = true, FieldName = "НТД"},
            });

            result.AddRange(from k in aqpl.OtherParameters.Union(aqpl.ChemistryParameters)
                            where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID) && k.IsNumber
                            let parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? string.Format(" ({0})", k.ParamType) : string.Empty)
                            select k.Param + parameterDisplayName into fieldName
                            select new GroupTableData
                            {
                                FieldName = fieldName,
                                EmitMin = sd.MinEnabled,
                                EmitMax = sd.MaxEnabled,
                                EmitAvg = sd.AverageEnabled,
                                EmitRange = sd.AmplitudeEnabled,
                                EmitCount = sd.CountEnabled
                            });
            return result;
        }

        public static List<NewTableData> GetChangeFieldsFormattedOutput(bool addGroupName, ParametersList aqpl, FieldListCombination flc, LimitsShowMode limitsShowMode)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Марка", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
//                new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Стан", NewFieldName = "Стан"}, 
                new NewTableData{OldFieldName = "Толщина", NewFieldName = "Толщина"}, 
                new NewTableData{OldFieldName = "Дата выплавки", NewFieldName = "Дата выплавки"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"},
                new NewTableData{OldFieldName = "Партия", NewFieldName = "Партия"},
            });
            if (aqpl.HasOther) result.Add(new NewTableData { OldFieldName = "Тип исп.", NewFieldName = "Тип испытания" });
            if (aqpl.HasOther) result.Add(new NewTableData { OldFieldName = "Номер образца", NewFieldName = "Номер образца" });
            result.Add(new NewTableData { OldFieldName = "Дата испытания", NewFieldName = "Дата испытания" }); 
            result.Add(new NewTableData { OldFieldName = "Результат исп.", NewFieldName = "Результат исп." });
            foreach (var k in aqpl.OtherParameters.Union(aqpl.ChemistryParameters))
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "");
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude) result.Add(new NewTableData { NewFieldName = k.Param + parameterDisplayName + "_НГ", OldFieldName = k.Param + parameterDisplayName + "_НГ" });
                result.Add(new NewTableData { NewFieldName = k.Param + parameterDisplayName, OldFieldName = k.Param + parameterDisplayName });
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude) result.Add(new NewTableData { NewFieldName = k.Param + parameterDisplayName + "_ВГ", OldFieldName = k.Param + parameterDisplayName + "_ВГ" });
            }
            if (aqpl.HasOther) result.Add(new NewTableData { OldFieldName = "Комментарии проч.", NewFieldName = "Комментарии проч." });
            if (aqpl.HasOther) result.Add(new NewTableData { OldFieldName = "Литер исп.", NewFieldName = "Литер исп." });

            if ((limitsShowMode & LimitsShowMode.Mark) != LimitsShowMode.Mark) return result;

            result.AddRange(from k in aqpl.OtherParameters.Union(aqpl.ChemistryParameters) 
                            where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID) 
                            let parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "") 
                            select new NewTableData {NewFieldName = "#" + k.Param + parameterDisplayName + "_Цвет", OldFieldName = "#" + k.Param + parameterDisplayName + "_Цвет"});

            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(ParametersList aqpl)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
        //        "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки",
                "Плавка",
                "Партия"
            });

            if (aqpl.HasOther) result.Add("Тип испытания");
            result.Add("Дата испытания");
            if (aqpl.HasOther) result.Add("Номер образца");
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(bool addGroupName, ParametersList aqpl, FieldListCombination flc, LimitsShowMode limitsShowMode)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
       //         "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки",
                "Плавка",
                "Партия"
            });
            if (aqpl.HasOther) result.Add("Тип испытания");
            foreach (var k in aqpl.ChemistryParameters)
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "");
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude) result.Add(k.Param + parameterDisplayName + "_НГ");
                result.Add(k.Param + parameterDisplayName);
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude) result.Add(k.Param + parameterDisplayName + "_ВГ");
            }
            return result;
        }

        public static List<string> GetRightBordersFormattedOutput(ParametersList aqpl)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
      //          "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки",
                "Плавка",
                "Партия"
            });
            if (aqpl.HasOther) result.Add("Тип испытания");
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(bool addGroupName, ParametersList aqpl, FieldListCombination flc, LimitsShowMode limitsShowMode)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
    //            "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки", 
                "Плавка",
                "Партия"
            });
            if (aqpl.HasOther) result.Add("Тип испытания");
            foreach (var k in aqpl.ChemistryParameters)
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "");
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude)
                {
                    result.Add(k.Param + parameterDisplayName + "_НГ");
                    break;
                }
                result.Add(k.Param + parameterDisplayName);
                break;
            }
            var group = string.Empty;
            foreach (var k in aqpl.OtherParameters)
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "");
                if (k.ParamType == group) continue;
                if ((limitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude)
                {
                    result.Add(k.Param + parameterDisplayName + "_НГ");
                    group = k.ParamType;
                }
                else
                {
                    result.Add(k.Param + parameterDisplayName);
                    group = k.ParamType;
                }
            }
            if (aqpl.HasOther) result.Add("Комментарии проч.");
            return result;
        }
        
        public static List<string> GetHorizontalBordersFormattedOutput(ParametersList aqpl)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
      //          "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки", 
                "Плавка",
                "Партия"
            });
            if (aqpl.HasOther) result.Add("Тип испытания");

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput(ParametersList aqpl)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
       //         "Разливка",
                "Стан",
                "Толщина",
                "Дата выплавки",
                "Плавка",
                "Партия"
            });
            if (aqpl.HasOther) result.Add("Тип испытания");
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(bool addGroupName, ParametersList aqpl, FieldListCombination flc, LimitsShowMode limitsShowMode)
        {
            var mode = (limitsShowMode != LimitsShowMode.None) ? ScaleRuleMode.Range : ScaleRuleMode.MinMax;
            return (from k in aqpl.OtherParameters.Union(aqpl.ChemistryParameters).Where(a=>a.IsNumber)
                where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                let parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "")
                select new ScaleRule
                {
                    FieldName = k.Param + parameterDisplayName, Mode = mode, Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10), Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                }).ToList();
        }
    }
}