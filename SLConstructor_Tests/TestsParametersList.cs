﻿using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;

namespace SLConstructor_Tests
{
    public class TestsParametersList : SelectionParametersList
    {
        public TestsParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();

            DateField = " tbltesthead.test_date";

            DetailsJoin = " inner join tbltests on tbltests.test_id=tbltesthead.test_id ";
            
            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector =  ColumnTypes.Discrete,
                Name = "Марка",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark 
                                INNER JOIN 
                                (
	                                select distinct id_cmark from tbltesthead @DetailsJoin @PreviousFilter group by id_cmark
                                ) as t1 on t1.id_cmark=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck("Контроль") + @" AND  @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "TblTestHead.id_cmark",
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                Name = "НТД",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                select distinct id_ntd from 
	                                (
		                                select distinct id_ntd from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd
		                                union
		                                select distinct id_ntd_melt1 from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd_melt1
		                                union
		                                select distinct id_ntd_melt2 from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd_melt2
		                                union
		                                select distinct id_ntd_melt3 from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd_melt3
	                                ) as G2
                                ) as t1 on t1.id_ntd=G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "TblTestHead.id_ntd|TblTestHead.id_ntd_melt1|TblTestHead.id_ntd_melt2|TblTestHead.id_ntd_melt3",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Discrete,
                Name = "НТД контроля",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                select distinct id_ntd from tblTestHead @DetailsJoin @PreviousFilter group by id_ntd
                                ) as t1 on t1.id_ntd=G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "TblTestHead.id_ntd",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            
            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.Discrete,
                Name = "Стан",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct LL__MILL_ID as id, LL__MILL_ID as name from tbltesthead @DetailsJoin @PreviousFilter and LL__MILL_ID is not null order by LL__MILL_ID ",
                Field = "LL__MILL_ID",
                ResultedFilterField = "tbltesthead.LL__MILL_ID",
                IsNumber = true,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                Name = "Толщина",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct thk as id, thk as name from tbltesthead @DetailsJoin @PreviousFilter and thk is not null order by tbltesthead.thk",
                Field = "thk",
                ResultedFilterField = "tbltesthead.thk",
                IsNumber = true,
                NumericSelectionAvailable = true,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd6 = new SelectionParameterDescription
            {
                ID = 6,
                Selector = ColumnTypes.Discrete,
                Name = "Профиль",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct prof as id, prof as name from tbltesthead @DetailsJoin @PreviousFilter and prof is not null order by tbltesthead.prof",
                Field = "prof",
                ResultedFilterField = "tbltesthead.prof",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd7 = new SelectionParameterDescription
            {
                ID = 7,
                Selector = ColumnTypes.Discrete,
                Name = "Группа параметров",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, paramtype as name from tbl_paramtype 
                                INNER JOIN 
                                (
	                                select distinct id_paramtype from tbltesthead @DetailsJoin @PreviousFilter and id_paramtype is not null 
                                ) as t1 on t1.id_paramtype=tbl_paramtype.id 
                                where " + Security.GetOldParametersCheck() + " AND @AdditionalFilter order by paramtype",
                Field = "paramtype",
                ResultedFilterField = "tbltesthead.id_paramtype",
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd8 = new SelectionParameterDescription
            {
                ID = 8,
                Selector =  ColumnTypes.Parameters,
                Name = "Параметры и значения",
                EnableDetailsJoin = true,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
                                create table #tquery
                                (number int IDENTITY(1,1), pt_id int, name nvarchar(max), paramtype nvarchar(max), IsNumber bit, Parameter_ID int)

                                insert into #tquery 
                                select distinct tblTestHead.id_paramtype as pt_id, tbl_Parameters.DisplayParameterName,
                                tbl_Paramtype.Paramtype as Name, tbl_Parameters.IsNumber, tbl_parameters.id from tblTestHead 
                                inner join tbl_Paramtype on tbl_Paramtype.id = tblTestHead.id_paramtype
                                inner join tbltests on tbltests.test_id=tbltesthead.test_id 
                                inner join tbl_Parameters on tbl_Parameters.id = tblTests.id_parameter  @PreviousFilter and @AdditionalFilter order by paramtype, DisplayParameterName

                                select t1.number as id, t1.name as name, t1.pt_id, t1.paramtype, t1.IsNumber, cast (case when t2.paramtype=t1.paramtype then 0 else 1 end as bit) as GroupChanged, cast (case when t2.name=t1.name and t2.paramtype=t1.paramtype then 0 else 1 end as bit) as ParameterChanged, t1.Parameter_ID from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 

                                drop table #tquery
                                ",
                Field = "DisplayParameterName",
                ResultedFilterField = "tblTests.id_parameter",
                OptionalField = "tblTests.value",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd9 = new SelectionParameterDescription
            {
                ID = 9,
                Selector = ColumnTypes.Discrete,
                Name = "Цех / материал",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct melttype as id, melttype as name from tbltesthead @DetailsJoin @PreviousFilter and melttype is not null",
                Field = "tbltesthead.melttype",
                ResultedFilterField = "melttype",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd10 = new SelectionParameterDescription
            {
                ID = 10,
                Selector = ColumnTypes.Discrete,
                Name = "Способ разливки",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct onrs as id, onrs as name from tbltesthead @DetailsJoin @PreviousFilter and onrs is not null",
                Field = "tbltesthead.onrs",
                ResultedFilterField = "onrs",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            var spd11 = new SelectionParameterDescription
            {
                ID = 11,
                Selector = ColumnTypes.MeltList,
                Name = "Номер плавки",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct melt , melttype as melttype from tbltesthead @DetailsJoin @PreviousFilter @AdditionalFilter and melt is not null  and melttype is not null order by melttype, melt

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null  order by t2.melttype, t2.melt

                                drop table #tquery
                                ",
                Field = "tbltesthead.melt",
                ResultedFilterField = "melt",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
                /*
                 @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct melt , max(melttype) as melttype from oel_head @DetailsJoin @PreviousFilter @AdditionalFilter group by melt having melt is not null and max(melttype) is not null order by max(melttype), melt

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null

                                drop table #tquery
                                "
                */
            };

            var spd12 = new SelectionParameterDescription
            {
                ID = 12,
                Selector = ColumnTypes.Discrete,
                Name = "Результат",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct result as id, tbl_results.ResultName as name from tbltesthead @DetailsJoin left outer join tbl_results on tbl_results.id=tbltesthead.result @PreviousFilter and tbl_results.ResultName is not null",
                Field = "tbltesthead.result",
                ResultedFilterField = "result",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd13 = new SelectionParameterDescription
            {
                ID = 13,
                Selector = ColumnTypes.Discrete,
                Name = "Испытательная машина",
                EnableDetailsJoin = true,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select id, machinename as name from Tests_Machines AS M 
                                INNER JOIN 
                                (
	                                select distinct id_machine from tbltesthead inner join tbltests on tbltests.test_id=tbltesthead.test_id  @PreviousFilter group by id_machine
                                ) as t1 on t1.id_machine=M.id 
                                where @AdditionalFilter 
                                order by machinename
                                ",
                Field = "machinename",
                ResultedFilterField = "TblTests.id_machine",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd14 = new SelectionParameterDescription
            {
                ID = 14,
                Selector = ColumnTypes.Discrete,
                Name = "Лаборант",
                EnableDetailsJoin = true,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select id, testername as name from Tests_Testers AS T 
                                INNER JOIN 
                                (
	                                select distinct id_tester from tbltesthead inner join tbltests on tbltests.test_id=tbltesthead.test_id  @PreviousFilter group by id_tester
                                ) as t1 on t1.id_tester=T.id 
                                where @AdditionalFilter 
                                order by testername
                                ",
                Field = "testername",
                ResultedFilterField = "TblTests.id_tester",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd15 = new SelectionParameterDescription
            {
                ID = 15,
                Selector = ColumnTypes.Text,
                Name = "Комментарий",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = string.Empty,
                Field = "Comments",
                ResultedFilterField = "TblTestHead.Comments",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd16 = new SelectionParameterDescription
            {
                ID = 16,
                Selector = ColumnTypes.Discrete,
                Name = "Тип испытания",
                EnableDetailsJoin = true,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd 
                                select distinct isnull(id_status, '') as id, isnull(Status, ' Пустые') as name from tbltesthead 
                                inner join tbltests on tbltests.test_id=tbltesthead.test_id   
                                left outer join Tests_Statuses on Tests_Statuses.id=tbltests.id_status
                                 @PreviousFilter 
                                order by name",
                Field = "status",
                ResultedFilterField = "TblTests.id_status",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };

            var spd17 = new SelectionParameterDescription
            {
                ID = 17,
                Selector = ColumnTypes.Discrete,
                Name = "Аттестация",
                EnableDetailsJoin = true,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd 
	                                select distinct attestation as id , (case when attestation=1 then 'Аттестационные' else 'Вне аттестации' end) as name from tbltesthead inner join tbltests on tbltests.test_id=tbltesthead.test_id  @PreviousFilter group by attestation having attestation is not null",
                Field = "attestation",
                ResultedFilterField = "TblTests.attestation",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };
            var spd18 = new SelectionParameterDescription
            {
                ID = 18,
                Selector = ColumnTypes.Discrete,
                Name = "Литер",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct liter as id, liter as name from tbltesthead @DetailsJoin @PreviousFilter and liter is not null",
                Field = "tbltesthead.liter",
                ResultedFilterField = "liter",
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = true
            };
            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
            ParametersList.Add(spd6);
            ParametersList.Add(spd7);
            ParametersList.Add(spd8);
            ParametersList.Add(spd9);
            ParametersList.Add(spd10);
            ParametersList.Add(spd11);
            ParametersList.Add(spd12);
            ParametersList.Add(spd13);
            ParametersList.Add(spd14);
            ParametersList.Add(spd15);
            ParametersList.Add(spd16);
            ParametersList.Add(spd17);
            ParametersList.Add(spd18);
        }
    }

}
