﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQControlsLibrary;
using AQConstructorsLibrary;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLConstructor_Tests
{
    public enum LimitsMode { AutoAttestation, Reference, UserInput }

    [Flags]
    public enum LimitsShowMode { None = 0, Mark = 1, Inlude = 2, OnlyBad = 4 }

    public partial class Page : IAQModule
    {
        #region Переменные

        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;
        private readonly AQModuleDescription _aqmd;
        private List<Step> _steps;
        private StatisticDescription _statisticDescription;
        private ParametersList _parametersList;
        private FieldListCombination _fieldListCombination;
        private Filters _filters;
        private bool _formatted;
        private bool _colored;
        private bool _statisticIncluded;
        private string _sql = string.Empty;
        private bool _addGroupName, _showFvc, _exchangeDates, _expandDatesForChemistry;
        private LimitsMode _currentLimitsMode = LimitsMode.Reference;
        private LimitsShowMode _currentLimitsShowMode = LimitsShowMode.None;
        private readonly TestsParametersList _conditionList = new TestsParametersList();

        #endregion

        public Page()
        {
            InitializeComponent();
             _aqmd = new AQModuleDescription {Instance = this, ModuleType = GetType(), Name = GetType().Name};
            ColumnsPresenter.SelectionParameters = _conditionList;
            InitializeCustomDateRangePanel(); 
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
            ReflectCurrentStatisticToUI(_statisticDescription);
        }

        private void InitializeCustomDateRangePanel()
        {
            var g = ColumnsPresenter.GetDateRangeCustomPanel();
            g.HorizontalAlignment = HorizontalAlignment.Center;
            var sp = Resources["DateRangeModeSelector"] as StackPanel;
            Resources.Remove("DateRangeModeSelector");
            g.Children.Add(sp);
        }
        
        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery();
        }
        
        private void RefreshQuery()
        {
            RefreshFlags(); 
            RefreshFilters();
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;
            var task = new Task(new[] { "Формирование списка полей", "Слияние полей" }, "Проверка условий", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cs.BeginGetTestsParametersList(_filters.ParametersFilter, _filters.ParametersFilter, GetTestsParametersListDone, task);
        }

        private void RefreshFlags()
        {
            if (AddGroupNameCheckBox.IsChecked != null) _addGroupName = AddGroupNameCheckBox.IsChecked.Value;
            if (ShowFieldCombineEditorCheckBox.IsChecked != null) _showFvc = ShowFieldCombineEditorCheckBox.IsChecked.Value;
            if (ExpandDatesForChemistryCheckBox.IsChecked != null) _expandDatesForChemistry = ExpandDatesForChemistryCheckBox.IsChecked.Value;
            if (NoFormatButton.IsChecked != null) _formatted = !NoFormatButton.IsChecked.Value;
            if (ScaleButton.IsChecked != null) _colored = ScaleButton.IsChecked.Value;
            if (DisableStatistic.IsChecked != null) _statisticIncluded = !DisableStatistic.IsChecked.Value;
        }

        private void RefreshFilters()
        {
            _filters = ColumnsPresenter.GetFilters();
        }

        private void GetTestsParametersListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetTestsParametersList(iar);
            _parametersList = t.ParametersList; 
            if (t.Success)
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                task.SetMessage(t.Message);
                _cs.BeginGetTestsCorrectionList(_filters.ParametersFilter, _parametersList, true, GetTestsCorrectionListDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() => ColumnsPresenter.EnableFinishSelection(true));
        }

        private void GetTestsCorrectionListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            FieldListCombineTaskResult t = _cs.EndGetTestsCorrectionList(iar);
            _fieldListCombination = t.FieldListCombination;
            WorkParametersList.ParametersList = _parametersList;
            if (t.Success)
            {
                task.SetState(1, TaskState.Ready);
                task.SetMessage(t.Message);
            }
            else
            {
                task.SetState(1, TaskState.Error);
                task.SetMessage(t.Message);
            }

            Dispatcher.BeginInvoke(delegate
            {
                EnableSaveAndExport(true);
                RefreshQueryButton.IsEnabled = true;
                ColumnsPresenter.EnableFinishSelection(true);
                MainFieldCombineView.PL = _parametersList;
                MainFieldCombineView.FLC = _fieldListCombination;
                MainFieldCombineView.Refresh();
                MainFieldCombineView.Visibility = _showFvc ? Visibility.Visible : Visibility.Collapsed;
            });
        }

        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery();
        }

    #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(delegate
            {
                XlsxExportButton.IsEnabled = allow;
                PreviewButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
                SaveCalculationButton.IsEnabled = allow;
            });
        }

        private void RegisterDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _conditionList.DateField = "tbltesthead.RegisterDate";
            _exchangeDates = true;
            if (ColumnsPresenter == null) return;
            ColumnsPresenter.RefreshFirstFilter();
            RefreshFilters();
        }

        private void TestDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _conditionList.DateField = "tbltesthead.test_date";
            _exchangeDates = false;
            if (ColumnsPresenter == null) return;
            ColumnsPresenter.RefreshFirstFilter();
            RefreshFilters();
        }

        private void LimitsMode_Checked(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb == null || rb.Tag == null) return;
            _currentLimitsMode = (LimitsMode)Enum.Parse(typeof(LimitsMode), rb.Tag.ToString(), true);
        }
        private void LimitsShowMode_Click(object sender, RoutedEventArgs e)
        {
            RefreshCurrentLimitsShowMode();
            if (_currentLimitsShowMode != LimitsShowMode.None)DisableStatistics();
        }

        private void DisableStatistics()
        {
            MinToggleButton.IsChecked = false;
            MaxToggleButton.IsChecked = false;
            AvgToggleButton.IsChecked = false;
            RangeToggleButton.IsChecked = false;
            CountToggleButton.IsChecked = false;
            DisableStatistic.IsChecked = true;
            RefreshStatisticDescription();
        }
        private void DisableShowLimits()
        {
            ColorLimits.IsChecked = false;
            IncludeLimits.IsChecked = false;
            OnlyBad.IsChecked = false;
            RefreshCurrentLimitsShowMode();
        }

        private void RefreshCurrentLimitsShowMode()
        {
            _currentLimitsShowMode = LimitsShowMode.None;
            _currentLimitsShowMode |= ColorLimits.IsChecked != null && ColorLimits.IsChecked.Value ? LimitsShowMode.Mark : 0;
            _currentLimitsShowMode |= IncludeLimits.IsChecked != null && IncludeLimits.IsChecked.Value
                ? LimitsShowMode.Inlude
                : 0;
            _currentLimitsShowMode |= OnlyBad.IsChecked != null && OnlyBad.IsChecked.Value ? LimitsShowMode.OnlyBad : 0;
        }

        private void RefreshStatisticDescription()
        {
            var sd = new StatisticDescription
            {
                MinEnabled = MinToggleButton.IsChecked != null && MinToggleButton.IsChecked.Value,
                MaxEnabled = MaxToggleButton.IsChecked != null && MaxToggleButton.IsChecked.Value,
                AverageEnabled = AvgToggleButton.IsChecked != null && AvgToggleButton.IsChecked.Value,
                AmplitudeEnabled = RangeToggleButton.IsChecked != null && RangeToggleButton.IsChecked.Value,
                CountEnabled = CountToggleButton.IsChecked != null && CountToggleButton.IsChecked.Value
            };
            _statisticDescription = sd;
        }

        private void ReflectCurrentStatisticToUI(StatisticDescription currentStatisticDescription)
        {
            if (currentStatisticDescription == null) return;
            DisableStatistic.IsChecked = !(currentStatisticDescription.MinEnabled ||
                                           currentStatisticDescription.MaxEnabled ||
                                           currentStatisticDescription.AverageEnabled ||
                                           currentStatisticDescription.AmplitudeEnabled ||
                                           currentStatisticDescription.CountEnabled);
            
            MinToggleButton.IsChecked = currentStatisticDescription.MinEnabled;
            MaxToggleButton.IsChecked =currentStatisticDescription.MaxEnabled;
            AvgToggleButton.IsChecked = currentStatisticDescription.AverageEnabled;
            RangeToggleButton.IsChecked = currentStatisticDescription.AmplitudeEnabled;
            CountToggleButton.IsChecked = currentStatisticDescription.CountEnabled;
        }

        private void StatisticToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ToggleButton && (sender as ToggleButton).Name == "DisableStatistic")
            {
                DisableStatistics();
            }
            else
            {
                DisableStatistic.IsChecked = false;
                DisableShowLimits();
            }
            
            RefreshStatisticDescription();
        }


        #endregion

        public AQModuleDescription GetAQModuleDescription(){ return _aqmd; }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }
    }
}