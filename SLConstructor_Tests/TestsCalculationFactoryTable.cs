﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;
using ApplicationCore.CalculationServiceReference;
using System;

namespace SLConstructor_Tests
{
    public static partial class TestsCalculationFactory
    {
        private const string TableName = "Результаты испытаний";
        public static TableParameters GetNormStatisticsTable(bool addGroupName, ParametersList parametersList, FieldListCombination fieldListCombination)
        {
            var tp = new TableParameters
            {
                TableName = "Проверка соответствия",
                ColoredColumns = new List<string>(),
                ColumnNames = new List<string>(),
                DataTypes = new List<string>(),
                ColumnWidth = new List<double>(),
                Data = new List<Row>()
            };

            var parameterNames = GetParametersNames(parametersList, fieldListCombination, addGroupName);

            tp.ColumnNames.Add("Параметр");
            tp.ColumnWidth.Add(70);
            tp.DataTypes.Add("String");

            Row headerRow = new Row { RowData = new List<Cell>()};
            headerRow.RowData.Add(new Cell { Value = "Параметры" });

            Row countRow = new Row { RowData = new List<Cell>() };
            countRow.RowData.Add(new Cell { Value = "Число испытаний" });
            Row goodRow = new Row { RowData = new List<Cell>() };
            goodRow.RowData.Add(new Cell { Value = "Уд." });
            Row badRow = new Row { RowData = new List<Cell>() };
            badRow.RowData.Add(new Cell { Value = "Неуд." });
            Row goodPercentRow = new Row { RowData = new List<Cell>() };
            goodPercentRow.RowData.Add(new Cell { Value = "Уд. %" });
            Row badPercentRow = new Row { RowData = new List<Cell>() };
            badPercentRow.RowData.Add(new Cell { Value = "Неуд. %" });
            foreach (var p in parameterNames)
            {
                tp.ColumnNames.Add(p);
                tp.ColumnWidth.Add(70);
                tp.DataTypes.Add("Double");
                headerRow.RowData.Add(new Cell { Value = p });

                countRow.RowData.Add(new Cell
                {
                    Value = String.Format("=Count([{0}].[{1}])", TableName, p),
                    Expression = String.Format("Count([{0}].[{1}])", TableName, p),
                    Code = String.Format("var Result=(AQMath.AggregateCount(AQMath.GetFieldValues<String>(AQMath.GetTable(\"{0}\",row),\"{1}\"))); return Result; ", TableName, p),
                    DataType = "Double"
                });

                goodRow.RowData.Add(new Cell
                {
                    Value = String.Format("=CountOfValues([{0}].[#{1}_Цвет], \"#FF30C010\")", TableName, p),
                    Expression = String.Format("CountOfValues([{0}].[#{1}_Цвет], \"#FF30C010\")", TableName, p),
                    Code = String.Format("var Result=(AQMath.AggregateCountOfValues(AQMath.GetFieldValues<String>(AQMath.GetTable(\"{0}\",row),\"#{1}_Цвет\"), \"#FF30C010\"));return Result;", TableName, p),
                    DataType = "Double"
                });

                badRow.RowData.Add(new Cell
                {
                    Value = String.Format("=CountOfValues([{0}].[#{1}_Цвет], \"#FFEF6060\")", TableName, p),
                    Expression = String.Format("CountOfValues([{0}].[#{1}_Цвет], \"#FFEF6060\")", TableName, p),
                    Code = String.Format("var Result=(AQMath.AggregateCountOfValues(AQMath.GetFieldValues<String>(AQMath.GetTable(\"{0}\",row),\"#{1}_Цвет\"), \"#FFEF6060\"));return Result;", TableName, p),
                    DataType = "Double"
                });

                goodPercentRow.RowData.Add(new Cell
                {
                    Value = String.Format("=Round(CountOfValuesInPercent([{0}].[#{1}_Цвет], \"#FF30C010\"),2)", TableName, p),
                    Expression = String.Format("Round(CountOfValuesInPercent([{0}].[#{1}_Цвет], \"#FF30C010\"),2)", TableName, p),
                    Code = String.Format("var Result=(AQMath.Round(AQMath.AggregateCountOfValuesInPercent(AQMath.GetFieldValues<String>(AQMath.GetTable(\"{0}\",row),\"#{1}_Цвет\"), \"#FF30C010\"),2.0));return Result;", TableName, p),
                    DataType = "Double"
                });
                badPercentRow.RowData.Add(new Cell
                {
                    Value = String.Format("=Round(CountOfValuesInPercent([{0}].[#{1}_Цвет], \"#FFEF6060\"),2)", TableName, p),
                    Expression = String.Format("Round(CountOfValuesInPercent([{0}].[#{1}_Цвет], \"#FFEF6060\"),2)", TableName, p),
                    Code = String.Format("var Result=(AQMath.Round(AQMath.AggregateCountOfValuesInPercent(AQMath.GetFieldValues<String>(AQMath.GetTable(\"{0}\",row),\"#{1}_Цвет\"), \"#FFEF6060\"),2.0));return Result; ", TableName, p),
                    DataType = "Double"
                });
            }
            tp.Data.AddRange(new[] { headerRow, countRow, goodRow, badRow, goodPercentRow, badPercentRow });
            return tp;
        }

        private static List<string> GetParametersNames(ParametersList aqpl, FieldListCombination flc, bool addGroupName)
        {
            var result = new List<string>();
            foreach (var k in aqpl.OtherParameters.Union(aqpl.ChemistryParameters))
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : "");
                result.Add(k.Param + parameterDisplayName);
            }
            return result;
        }
    }
}