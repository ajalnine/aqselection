﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Tests
{
    public partial class Page
    {
        private const string TableName = "Результаты испытаний";

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            
            PreviewButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                    {
                        var tables = _cas.EndGetTablesSLCompatible(i);
                        if (tables == null) return;
                        task.AdvanceProgress();
                        task.SetMessage("Данные готовы");
                        var ca = new Dictionary<string, object>
                    {
                        {"From", ColumnsPresenter.From}, 
                        {"To", ColumnsPresenter.To}, 
                        {"SourceData", tables},
                        {"Calculation", _steps},
                        {"Description", _filters.Description},
                        {"New", true},
                    };
                        Dispatcher.BeginInvoke(() =>
                        {
                            PreviewButton.IsEnabled = true;
                            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
                }

            }, task);
        }

        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            XlsxExportButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, i =>
                    {
                        var sr = _cas.EndGetAllResultsInXlsx(i);
                        if (sr.Success)
                        {
                            task.AdvanceProgress();
                            task.SetMessage("Данные готовы");
                            Dispatcher.BeginInvoke(() =>
                            {
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                    @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" +
                                    HttpUtility.UrlEncode("Отчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" +
                                    HttpUtility.UrlEncode("application/msexcel")));
                                XlsxExportButton.IsEnabled = true;
                            });
                        }
                        else
                        {
                            task.SetState(1, TaskState.Error);
                            task.SetMessage(t.Message);
                            Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                        }
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                }

            }, task);
        }

        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            var signalArguments = new Dictionary<string, object> { { "SQL", _sql } };
            const string name = TableName;
            signalArguments.Add("Name", name);
            signalArguments.Add("From", ColumnsPresenter.From);
            signalArguments.Add("To", ColumnsPresenter.To);
            signalArguments.Add("Calculation", _steps);
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        private void SaveCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            SetupCalculation();
            var signalArguments = new Dictionary<string, object> { { "SQL", _sql } };
            const string name = TableName;
            signalArguments.Add("Name", name);
            signalArguments.Add("From", ColumnsPresenter.From);
            signalArguments.Add("To", ColumnsPresenter.To);
            signalArguments.Add("Calculation", _steps);
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation, signalArguments, null);
        }

        private void SetupCalculation()
        {
            RefreshFilters();
            _sql = TestsSelect.GetCorrectedSQL(_filters, _addGroupName, 
                _currentLimitsMode, _currentLimitsShowMode, _parametersList, 
                _fieldListCombination, _exchangeDates, _expandDatesForChemistry);

            Services.GetAQService().BeginWriteLog("Calc: Tests, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);
            _steps = StepFactory.CreateSQLQuery(_sql, TableName);
            AppendDescriptionStep();
            if ((_currentLimitsShowMode & LimitsShowMode.Mark) == LimitsShowMode.Mark) AppendNormStep();
            if (_statisticIncluded) AppendStatisticsStep();
            if (_formatted) AppendFormatSteps();
            if (_colored) AppendScales();
        }

        private void AppendDescriptionStep()
        {
            _steps.Add(StepFactory.CreateConstants(new List<ConstantDescription>
            {
                new ConstantDescription{VariableName = "Описание", VariableType = "String", Value = _filters.Description},
                new ConstantDescription{VariableName = "Автор", VariableType = "String", Value = Security.CurrentUser},
                new ConstantDescription{VariableName = "Поле даты", VariableType = "String", Value = _exchangeDates ? "Дата выплавки" : "Дата испытания"},
                new ConstantDescription{VariableName = "Фиксированный столбец", VariableType = "Int32", Value =  _formatted ? 6 : 1}
            }));
        }

        private void AppendNormStep()
        {
            var calculatingTable = TestsCalculationFactory.GetNormStatisticsTable(_addGroupName, _parametersList, _fieldListCombination);
            _steps.Add(StepFactory.CreateTable(calculatingTable));
        }

        private void AppendStatisticsStep()
        {
            _steps.Add(StepFactory.CreateCopyTable(
                new List<CopyTableData>
                {
                    new CopyTableData
                    {
                        OriginalName = TableName, 
                        CopyName = "Статистика", 
                        NeedToCopy = true
                    }
                }));

            var grouping = TestsCalculationFactory.GetGroupDescriptions(_addGroupName, _parametersList, _fieldListCombination, _statisticDescription);
            _steps.Add(StepFactory.CreateGrouping("Статистика", grouping));
        }

        private void AppendFormatSteps()
        {
            _steps.Add(StepFactory.CreateChangeFields(TableName, TestsCalculationFactory.GetChangeFieldsFormattedOutput(_addGroupName, _parametersList, _fieldListCombination, _currentLimitsShowMode)));
            _steps.Add(StepFactory.CreateSorting(TableName, TestsCalculationFactory.GetSortingFieldsFormattedOutput(_parametersList), true));
            _steps.Add(StepFactory.CreateFormatCombine(TableName, TestsCalculationFactory.GetCombineFieldsFormattedOutput(_addGroupName, _parametersList, _fieldListCombination, _currentLimitsShowMode), true));
            _steps.Add(StepFactory.CreateFormatBorders(TableName,
                TestsCalculationFactory.GetRightBordersFormattedOutput(_parametersList),
                TestsCalculationFactory.GetLeftBordersFormattedOutput(_addGroupName, _parametersList, _fieldListCombination, _currentLimitsShowMode),
                TestsCalculationFactory.GetHorizontalBordersFormattedOutput(_parametersList),
                TestsCalculationFactory.GetChangeBordersFormattedOutput(_parametersList)));
        }

        private void AppendScales()
        {
            _steps.Add(StepFactory.CreateFormatScale(TableName, TestsCalculationFactory.GetScaleRulesFormattedOutput(_addGroupName, _parametersList, _fieldListCombination, _currentLimitsShowMode)));
        }
        public class StatisticDescription
        {
            public bool MinEnabled;
            public bool MaxEnabled;
            public bool CountEnabled;
            public bool AverageEnabled;
            public bool AmplitudeEnabled;
        }
    }
}
