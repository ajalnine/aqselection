﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;

namespace SLConstructor_Tests
{
    public static class TestsSelect
    {
        #region Create Combined Select

        public static string GetCorrectedSQL(Filters filters, bool addGroupName, LimitsMode currentLimitsMode, LimitsShowMode currentLimitsShowMode, 
                                      ParametersList aqpl, FieldListCombination flc, bool exchangeDates,  bool expandChemistryDates)
        {
            if (!aqpl.HasChemistry && !aqpl.HasOther) return String.Empty;

            var sqlChemistry = String.Empty;
            var sqlOther = String.Empty;
            var valueChemistry = String.Empty;
            var valueOther = String.Empty;
            var colorsChemistry = String.Empty;
            var colorsOther = String.Empty;
            var fieldsChemistry = String.Empty;
            var fieldsOther = String.Empty;
            var filterChemistry = String.Empty;
            var filterOther = String.Empty;
            var joinExpression = String.Empty;
            var joinEndOfExpression = String.Empty;
            var whereExpression = String.Empty;
            var sortExpression = String.Empty;
            var minLimitsField = string.Empty;
            var maxLimitsField = string.Empty;
            
            var sortDate = !exchangeDates ? @"[Дата испытания]" : "[Дата выплавки]";

            switch (currentLimitsMode)
            {
                case LimitsMode.AutoAttestation:
                    minLimitsField = " tblTests.min ";
                    maxLimitsField = " tblTests.max ";
                    break;

                case LimitsMode.Reference:
                    minLimitsField = " tblTests.NTDMin ";
                    maxLimitsField = " tblTests.NTDMax ";
                    break;

                case LimitsMode.UserInput:
                    minLimitsField = " tblTests.UserMin ";
                    maxLimitsField = " tblTests.UserMax ";
                    break;
            }

            if (aqpl.HasChemistry)
            {
                var filter = aqpl.HasOther ? filters.ChemistrySaveFilter : filters.SaveFilter;
                valueChemistry = CreateChemistryValueFilter(addGroupName, aqpl, filters.GenericValueFilterInfo);
                sqlChemistry = CreateChemistrySelectPart(filter, addGroupName, currentLimitsShowMode, aqpl, flc,
                    exchangeDates, minLimitsField, maxLimitsField, expandChemistryDates);
                fieldsChemistry = CreateChemistryFieldsList(addGroupName, aqpl, flc, exchangeDates,
                    (currentLimitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude);
                colorsChemistry = ((currentLimitsShowMode & LimitsShowMode.Mark) == LimitsShowMode.Mark)
                    ? CreateColorsChemistry(addGroupName, aqpl, flc)
                    : String.Empty;
                filterChemistry = ((currentLimitsShowMode & LimitsShowMode.OnlyBad) == LimitsShowMode.OnlyBad)
                    ? CreateRangeFilterChemistry(addGroupName, aqpl)
                    : String.Empty;
                if (!aqpl.HasOther) whereExpression = CreateWhereExpression(valueChemistry, filterChemistry);
                sortExpression = " order by c.[Плавка_], c." + sortDate;
            }

            if (aqpl.HasOther)
            {
                valueOther = CreateOtherValueFilter(addGroupName, aqpl, filters.GenericValueFilterInfo);
                sqlOther = CreateOtherSelectPart(filters, addGroupName, currentLimitsShowMode, aqpl, flc,
                    exchangeDates, minLimitsField, maxLimitsField);
                fieldsOther = CreateOtherFieldsList(addGroupName, aqpl, flc, exchangeDates,
                    (currentLimitsShowMode & LimitsShowMode.Inlude) == LimitsShowMode.Inlude); ////
                colorsOther = ((currentLimitsShowMode & LimitsShowMode.Mark) == LimitsShowMode.Mark)
                    ? CreateColorsOther(addGroupName, aqpl, flc)
                    : String.Empty;
                filterOther = ((currentLimitsShowMode & LimitsShowMode.OnlyBad) == LimitsShowMode.OnlyBad)
                    ? CreateRangeFilterOther(addGroupName, aqpl)
                    : String.Empty;
                if (!aqpl.HasChemistry) whereExpression = CreateWhereExpression(valueOther, filterOther);
                sortExpression = " order by m.[Плавка], m.[Толщина], m." + sortDate + ", m.[test_id], m.[Номер образца]";
            }

            if (aqpl.HasOther && aqpl.HasChemistry)
            {
                whereExpression = CreateCommonWhereExpression(valueChemistry, valueOther, filterChemistry, filterOther);
                joinExpression = (!String.IsNullOrEmpty(valueChemistry) || (filters.GenericValueFilterInfo != null && filters.GenericValueFilterInfo.Count > 0))
                    ? "inner join"
                    : "left outer join";
                joinEndOfExpression = "on c.[Плавка_]=m.[Плавка]";
            }

            return string.Format(@"select {0} from {1} {2} "
                , fieldsOther + fieldsChemistry + colorsOther + colorsChemistry
                , sqlOther + joinExpression + sqlChemistry + joinEndOfExpression
                , whereExpression + sortExpression);
        }

        #endregion

        #region Value Filters

        private static string CreateChemistryValueFilter(bool addGroupName, ParametersList aqpl, List<ValueFilterInfo> vfi)
        {
            return CreateValueFilter(addGroupName, aqpl, aqpl.ChemistryParameters, vfi, "c");
        }
        private static string CreateOtherValueFilter(bool addGroupName, ParametersList aqpl, List<ValueFilterInfo> vfi)
        {
            return CreateValueFilter(addGroupName, aqpl, aqpl.OtherParameters, vfi, "m");
        }

        private static string CreateValueFilter(bool addGroupName, ParametersList aqpl, IList<ParamDescription> parameters, List<ValueFilterInfo> vfi, string tablePrefix)
        {
            var valueFilter = String.Empty;
            var counter = 0;
            for (var index = 0; index < parameters.Count; index++)
            {
                var k = parameters[index];
                var parameterName = string.Format("{0}{1}", k.Param, ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : String.Empty));
                var condition = (from v in vfi where v.ParameterID == k.ParamID select v).SingleOrDefault();
                if (condition == null) continue;

                if (counter > 0) valueFilter += " AND ";
                counter++;
                if (condition.Min.HasValue) valueFilter += tablePrefix +".[" + parameterName + "]>=" + condition.Min.Value;
                if (condition.Max.HasValue)
                {
                    if (condition.Min.HasValue) valueFilter += " AND ";
                    valueFilter += tablePrefix + ".[" + parameterName + "]<=" + condition.Max.Value;
                }
            }
            return valueFilter;
        }
        #endregion

        #region Where Expressions

        private static string CreateWhereExpression(string value, string filter)
        {
            if (String.IsNullOrEmpty(value) && String.IsNullOrEmpty(filter)) return String.Empty;
            var whereExpression = "WHERE ";
            var hc = false;
            if (!String.IsNullOrEmpty(value))
            {
                whereExpression += "(" + value + ")";
                hc = true;
            }
            if (!String.IsNullOrEmpty(filter))
            {
                if (hc) whereExpression += " AND ";
                whereExpression += "(" + filter + ")";
            }
            return whereExpression;
        }

        private static string CreateCommonWhereExpression(string valueChemistry, string valueOther,string filterChemistry, string filterOther)
        {
            if ((String.IsNullOrEmpty(valueChemistry) && String.IsNullOrEmpty(valueOther)) &&
                String.IsNullOrEmpty(filterChemistry) && String.IsNullOrEmpty(filterOther)) return String.Empty;
            var whereExpression = "WHERE ";

            var hc = false;

            if (!String.IsNullOrEmpty(valueChemistry))
            {
                whereExpression += "(" + valueChemistry + ")";
                hc = true;
            }
            if (!String.IsNullOrEmpty(valueOther))
            {
                if (hc) whereExpression += " AND ";
                whereExpression += "(" + valueOther + ")";
                hc = true;
            }
            if (!String.IsNullOrEmpty(filterOther) && !String.IsNullOrEmpty(filterChemistry))
            {
                if (hc) whereExpression += " AND ";
                whereExpression += "((" + filterOther + ")";
                whereExpression += " OR ";
                whereExpression += "(" + filterChemistry + "))";
            }
            else
            {
                if (!String.IsNullOrEmpty(filterOther))
                {
                    if (hc) whereExpression += " AND ";
                    whereExpression += "(" + filterOther + ")";
                    hc = true;
                }
                if (!String.IsNullOrEmpty(filterChemistry))
                {
                    if (hc) whereExpression += " AND ";
                    whereExpression += "(" + filterChemistry + ")";
                }
            }
            return whereExpression;
        }

        #endregion

        #region Range Filters

        private static string CreateRangeFilterOther(bool addGroupName, ParametersList aqpl)
        {
            var filter = "(";

            var isFirst = true;
            foreach (var k in aqpl.OtherParameters)
            {
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                    ? (" (" + k.ParamType + ")")
                    : "");
                var operatingName = k.Param + parameterDisplayName;
                if (!isFirst) filter += " OR ";
                filter += string.Format(@"( m.[{0}] < m.[{0}_НГ] OR m.[{0}] > m.[{0}_ВГ] ) ", operatingName);
                isFirst = false;
            }
            filter += ") ";
            return filter;
        }

        private static string CreateRangeFilterChemistry(bool addGroupName, ParametersList aqpl)
        {
            var filter = "(";

            var isFirst = true;
            foreach (var k in aqpl.ChemistryParameters)
            {
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                    ? (" (" + k.ParamType + ")")
                    : String.Empty);
                var operatingName = k.Param + parameterDisplayName;
                if (!isFirst) filter += " OR ";
                filter += string.Format(@"( c.[{0}] < c.[{0}_НГ] OR c.[{0}] > c.[{0}_ВГ] ) ", operatingName);
                isFirst = false;
            }
            filter += ") ";
            return filter;
        }

        #endregion

        #region Field Lists

        private static string CreateChemistryFieldsList(bool addGroupName, ParametersList aqpl, FieldListCombination flc,
            bool exchangeDates, bool includeLimitsFields)
        {
            var fields = String.Empty;
            if (!aqpl.HasOther)
            {
                fields += " c.[Плавка_] as 'Плавка', c.[Марка], c.[Цех_] as 'Цех', c.[Партия]";

                if (!exchangeDates) fields += @", c.[Дата испытания], c.[Дата выплавки]";
                else fields += @", c.[Дата выплавки], c.[Дата испытания]";

                fields +=
                    @", c.[Профиль], c.[Толщина], c.[Стан], c.[Результат исп.], c.[Извещение], c.[Разливка], 
                    c.[Комментарии хим.], c.[Литер хим.], c.[НТД], c.[НТД1], c.[НТД2], c.[НТД3] ";
            }
            foreach (var k in aqpl.ChemistryParameters)
            {

                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                    ? (" (" + k.ParamType + ")")
                    : "");

                if (includeLimitsFields) fields += ", c.[" + k.Param + parameterDisplayName + "_НГ] ";

                fields += ", c.[" + k.Param + parameterDisplayName + "]";

                if (includeLimitsFields) fields += ", c.[" + k.Param + parameterDisplayName + "_ВГ] \r\n";
                else fields += "\r\n";
            }

            return fields;
        }

        private static string CreateOtherFieldsList(bool addGroupName, ParametersList aqpl, FieldListCombination flc,
            bool exchangeDates, bool includeLimitsFields)
        {
            var fields = @" m.[Плавка], m.[Марка], m.[Цех], m.[Партия] ";
            if (!exchangeDates) fields += @", m.[Дата испытания], m.[Дата выплавки]";
            else fields += @", m.[Дата выплавки], m.[Дата испытания]";


            fields += @", m.[Профиль], m.[Толщина], m.[Стан], m.[Результат исп.], m.[Тип исп.]  
                                ,m.[Аттестация], m.[Извещение],m.[Разливка], m.[Комментарии проч.], m.[Литер исп.]
                                ,m.[НТД], m.[НТД1], m.[НТД2], m.[НТД3], m.[Лаборант], m.[Номер образца]";

            if ((from i in aqpl.AllParameters where i.ParamTypeID == 662 select i).Any())
            {
                fields += ", m.[Испытательная машина] \r\n";
            }

            foreach (var k in aqpl.OtherParameters)
            {

                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                    ? (" (" + k.ParamType + ")")
                    : "");

                if (includeLimitsFields) fields += ", m.[" + k.Param + parameterDisplayName + "_НГ] ";

                fields += ", m.[" + k.Param + parameterDisplayName + "]";

                if (includeLimitsFields) fields += ", m.[" + k.Param + parameterDisplayName + "_ВГ] \r\n";
                else fields += "\r\n";
            }

            return fields;
        }
        #endregion

        #region Color Fields

        private static string CreateColorsChemistry(bool addGroupName, ParametersList aqpl, FieldListCombination flc)
        {
            return (from k in aqpl.ChemistryParameters
                where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                let parameterDisplayName =
                    ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                        ? (" (" + k.ParamType + ")")
                        : String.Empty)
                select k.Param + parameterDisplayName).Aggregate(string.Empty, (current, operatingName) 
                    => current + string.Format(@",case when c.[{0}] < c.[{0}_НГ] OR c.[{0}] > c.[{0}_ВГ] THEN '#FFEF6060' when (c.[{0}] is null or (c.[{0}_НГ] is null and c.[{0}_НГ] is null)) then null ELSE '#FF30C010' END AS '#{0}_Цвет' ", operatingName));
        }

        private static string CreateColorsOther(bool addGroupName, ParametersList aqpl, FieldListCombination flc)
        {
            return (from k in aqpl.OtherParameters
                where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                let parameterDisplayName =
                    ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                        ? (" (" + k.ParamType + ")")
                        : String.Empty)
                select k.Param + parameterDisplayName).Aggregate(string.Empty, (current, operatingName) 
                    => current + string.Format (@",case when m.[{0}] < m.[{0}_НГ] OR m.[{0}] > m.[{0}_ВГ] THEN '#FFEF6060' when (m.[{0}] is null or (m.[{0}_НГ] is null and m.[{0}_НГ] is null)) then null ELSE '#FF30C010' END AS '#{0}_Цвет' ", operatingName));
        }

        #endregion

        #region Create Partial Selects

        private static string CreateOtherSelectPart(Filters filters, bool addGroupName,
            LimitsShowMode currentLimitsShowMode, ParametersList aqpl, FieldListCombination flc, bool exchangeDates,
            string minLimitsField, string maxLimitsField)
        {
            var sql =
                @"( SELECT max(tblTestHead.melt) as 'Плавка', max(tblTestHead.melttype) as 'Цех' , max(tblTestHead.part) as 'Партия', ";
            if (!exchangeDates) sql += @"max(tblTestHead.test_date) as 'Дата испытания', 
                                                 max(tblTestHead.RegisterDate) as 'Дата выплавки', ";
            else sql += @"max(tblTestHead.RegisterDate) as 'Дата выплавки', 
                                   max(tblTestHead.test_date) as 'Дата испытания', ";

            sql += @"max(Common_Mark.mark) as 'Марка', tblTestHead.test_id,
                                tblTestHead.prof as 'Профиль', 
                                cast(replace(tblTestHead.thk,',','.') as float) as 'Толщина', 
                                tblTestHead.LL__MILL_ID as 'Стан', 
                                max(tbl_results.resultname) as 'Результат исп.',  
                                max(Tests_Statuses.status) as 'Тип исп.',  
                                max(case when tblTests.Attestation=1 then 'Да' else 'Нет' end) as 'Аттестация',  
                                tblTestHead.inf as 'Извещение', 
                                max(tblTestHead.onrs) as 'Разливка',
                                max(tblTestHead.Comments) as 'Комментарии проч.',
                                max(tblTestHead.Liter) as 'Литер исп.',
                                max(Common_NTD.ntd) as 'НТД', 
                                max(Common_NTD_1.ntd) as 'НТД1',
                                max(Common_NTD_2.ntd) as 'НТД2',
                                max(Common_NTD_3.ntd) as 'НТД3', 
                                max(Tests_Testers.TesterName) as 'Лаборант',
                                tblTestHead.num as 'Номер образца' ";

            if ((from i in aqpl.AllParameters where i.ParamTypeID == 662 select i).Any())
                sql += ",max(Tests_Machines.MachineName) as 'Испытательная машина'\r\n";


            sql = aqpl.OtherParameters.Aggregate(sql, (current, k) => current + GetParameterLine(addGroupName, currentLimitsShowMode, aqpl, flc, minLimitsField, maxLimitsField, k));


            sql += string.Format(@" FROM tblTestHead INNER JOIN
                                  tblTests ON tblTestHead.test_id = tblTests.test_id and (tbltesthead.num=tbltests.samplenumber  or tbltesthead.num is null) INNER JOIN
                                  Common_Mark ON tblTestHead.id_cmark = Common_Mark.id left outer JOIN
                                  Common_NTD ON tblTestHead.id_ntd = Common_NTD.id INNER JOIN
                                  tbl_Paramtype ON tblTestHead.id_paramtype = tbl_Paramtype.id INNER JOIN
                                  tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND tblTests.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_3 ON tblTestHead.id_ntd_melt3 = Common_NTD_3.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_2 ON tblTestHead.id_ntd_melt2 = Common_NTD_2.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_1 ON tblTestHead.id_ntd_melt1 = Common_NTD_1.id LEFT OUTER JOIN 
                                  tbl_results on tbl_results.id=tbltesthead.result LEFT OUTER JOIN
                                  Tests_Machines on Tests_Machines.id = tbltests.id_machine LEFT OUTER JOIN
                                  Tests_Statuses on Tests_Statuses.id = tbltests.id_status LEFT OUTER JOIN
                                  Tests_Testers on Tests_Testers.id = tbltests.id_tester where {0} AND {1} and tbltesthead.id_paramtype<>722
                                  group by tblTestHead.inf, tblTests.Attestation, Tests_Statuses.Status, tbltesthead.meltpos_id,  tbltesthead.LL__MILL_ID, 
                                           tblTestHead.num, tblTestHead.result, tblTestHead.thk, tblTestHead.prof, tbltesthead.test_id, 
                                           Tests_Machines.id, Tests_Testers.id) as m ", "(" + Security.GetMarkSQLCheck("Контроль") + " AND " + Security.GetOldParametersCheck() + ")",
                filters.SaveFilter);
            return sql;
        }

        private static string CreateChemistrySelectPart(string chemistryFilter, bool addGroupName,
            LimitsShowMode currentLimitsShowMode, ParametersList aqpl, FieldListCombination flc, bool exchangeDates,
            string minLimitsField, string maxLimitsField, bool expandChemistryDates)
        {
            var sql = @"( SELECT max(tblTestHead.melt) as 'Плавка_', max(tblTestHead.melttype) as 'Цех_' ";

            if (!aqpl.HasOther)
            {
                sql += @" ,max(Common_Mark.mark) as 'Марка', 
                                        max(tblTestHead.part) as 'Партия', ";

                sql += !exchangeDates
                    ? @"max(tblTestHead.test_date) as 'Дата испытания', 
                                        max(tblTestHead.RegisterDate) as 'Дата выплавки', "
                    : @"max(tblTestHead.RegisterDate) as 'Дата выплавки', 
                                           max(tblTestHead.test_date) as 'Дата испытания', ";

                sql += @" max(tblTestHead.prof) as 'Профиль', 
                                        max(cast(replace(tblTestHead.thk,',','.') as float)) as 'Толщина', 
                                        max(tblTestHead.LL__MILL_ID) as 'Стан', 
                                        max(tbl_results.resultname) as 'Результат исп.', 
                                        max(tblTestHead.inf) as 'Извещение', 
                                        max(tblTestHead.onrs) as 'Разливка',
                                        max(tblTestHead.Comments) as 'Комментарии хим.',
                                        max(tblTestHead.Liter) as 'Литер хим.',
                                        max(Common_NTD.ntd) as 'НТД', 
                                        max(Common_NTD_1.ntd) as 'НТД1',
                                        max(Common_NTD_2.ntd) as 'НТД2',
                                        max(Common_NTD_3.ntd) as 'НТД3'";
            }

            sql = aqpl.ChemistryParameters.Aggregate(sql, (current, k) => current + GetParameterLine(addGroupName, currentLimitsShowMode, aqpl, flc, minLimitsField, maxLimitsField, k));

            if (aqpl.HasOther && !exchangeDates && expandChemistryDates)
                chemistryFilter = chemistryFilter.Replace("@date1", "dateadd(m,-3, @date1)");

            sql += string.Format(@" FROM tblTestHead INNER JOIN
                                  tblTests ON tblTestHead.test_id = tblTests.test_id INNER JOIN
                                  Common_Mark ON tblTestHead.id_cmark = Common_Mark.id left outer JOIN
                                  Common_NTD ON tblTestHead.id_ntd = Common_NTD.id INNER JOIN
                                  tbl_Paramtype ON tblTestHead.id_paramtype = tbl_Paramtype.id INNER JOIN
                                  tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND tblTests.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_3 ON tblTestHead.id_ntd_melt3 = Common_NTD_3.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_2 ON tblTestHead.id_ntd_melt2 = Common_NTD_2.id LEFT OUTER JOIN
                                  Common_NTD AS Common_NTD_1 ON tblTestHead.id_ntd_melt1 = Common_NTD_1.id LEFT OUTER JOIN 
                                  tbl_results on tbl_results.id=tbltesthead.result 
                                  where {0} AND {1} and tbltesthead.id_paramtype=722 group by tbltesthead.melt) as c ",
                "(" + Security.GetMarkSQLCheck("Контроль") + " AND " + Security.GetOldParametersCheck() + ")", chemistryFilter);
            return sql;
        }

        #endregion

        #region Create Parameter Line

        private static string GetParameterLine(bool addGroupName, LimitsShowMode currentLimitsShowMode,
            ParametersList aqpl,
            FieldListCombination flc, string minLimitsField, string maxLimitsField, ParamDescription k)
        {
            var parameterDisplayName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param))
                ? (" (" + k.ParamType + ")")
                : "");
            var parameterLine = String.Empty;
            if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) return parameterLine;
            if (k.IsNumber)
            {
                if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                {
                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(case when tbl_Parameters.id='{0}' then  dbo.ToNumeric({1}) else null end) as '{2}{3}_НГ'\r\n",
                                k.ParamID, minLimitsField, k.Param, parameterDisplayName);

                    parameterLine +=
                        string.Format(
                            ",max(case when tbl_Parameters.id='{0}' then  dbo.ToNumeric(tblTests.value) else null end) as '{1}{2}'\r\n",
                            k.ParamID, k.Param, parameterDisplayName);

                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(case when tbl_Parameters.id='{0}' then  dbo.ToNumeric({1}) else null end) as '{2}{3}_ВГ'\r\n",
                                k.ParamID, maxLimitsField, k.Param, parameterDisplayName);
                }
                else
                {
                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(dbo.MultipleToNumeric({0}, tbl_Parameters.id , '{1}')) as '{2}{3}_НГ'\r\n",
                                minLimitsField, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID),
                                k.Param,
                                parameterDisplayName);

                    parameterLine +=
                        string.Format(
                            ",max(dbo.MultipleToNumeric(tblTests.value, tbl_Parameters.id , '{0}')) as '{1}{2}'\r\n",
                            FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param,
                            parameterDisplayName);

                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(dbo.MultipleToNumeric({0}, tbl_Parameters.id , '{1}')) as '{2}{3}_ВГ'\r\n",
                                maxLimitsField, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID),
                                k.Param,
                                parameterDisplayName);
                }
            }
            else
            {
                if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                {
                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(case when tbl_Parameters.id='{0}' then {1} else null end) as '{2}{3}_НГ'\r\n",
                                k.ParamID, minLimitsField, k.Param, parameterDisplayName);

                    parameterLine +=
                        string.Format(
                            ",max(case when tbl_Parameters.id='{0}' then tblTests.value else null end) as '{1}{2}'\r\n",
                            k.ParamID, k.Param, parameterDisplayName);

                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(case when tbl_Parameters.id='{0}' then {1} else null end) as '{2}{3}_ВГ'\r\n",
                                k.ParamID, maxLimitsField, k.Param, parameterDisplayName);
                }
                else
                {
                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(dbo.MultipleToString({0}, tbl_Parameters.id , '{1}')) as '{2}{3}_НГ'\r\n",
                                minLimitsField, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID),
                                k.Param,
                                parameterDisplayName);

                    parameterLine +=
                        string.Format(
                            ",max(dbo.MultipleToString(tblTests.value, tbl_Parameters.id , '{0}')) as '{1}{2}'\r\n",
                            FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param,
                            parameterDisplayName);

                    if (currentLimitsShowMode != LimitsShowMode.None)
                        parameterLine +=
                            string.Format(
                                ",max(dbo.MultipleToString({0}, tbl_Parameters.id , '{1}')) as '{2}{3}_ВГ'\r\n",
                                maxLimitsField, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID),
                                k.Param,
                                parameterDisplayName);
                }
            }
            return parameterLine;
        }
        #endregion
    }
}