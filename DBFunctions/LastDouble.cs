﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

[Serializable]
[SqlUserDefinedAggregate(Format.UserDefined, MaxByteSize=8000, IsInvariantToDuplicates=false, IsInvariantToNulls=false, IsInvariantToOrder=false, IsNullIfEmpty=true )]
public class LastDouble: IBinarySerialize
{
    public void Init()
    {
        doubleValue = SqlDouble.Null;
    }

    public void Accumulate(SqlDouble Value)
    {
        if (!Value.IsNull)
        {
            doubleValue = Value;
        }
    }

    public void Merge(LastDouble Value)
    {
        if (!Value.doubleValue.IsNull)
        {
            doubleValue = Value.doubleValue;
        }
    }

    public SqlDouble Terminate()
    {
        return doubleValue;
    }

    public void Read(BinaryReader r)
    {
        var isempty = r.ReadBoolean();
        doubleValue = r.ReadDouble();
        if (isempty) doubleValue = SqlDouble.Null;
    }

    public void Write(BinaryWriter w)
    {
        w.Write(doubleValue.IsNull);
        if (!doubleValue.IsNull)w.Write((double)doubleValue);
        else w.Write(0);
    }

    private SqlDouble doubleValue;
}
