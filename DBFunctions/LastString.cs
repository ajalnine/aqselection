﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

[Serializable]
[SqlUserDefinedAggregate(Format.UserDefined, MaxByteSize=8000, IsInvariantToDuplicates=false, IsInvariantToNulls=false, IsInvariantToOrder=false, IsNullIfEmpty=true )]
public class LastString: IBinarySerialize
{
    public void Init()
    {
        stringValue = SqlString.Null;
    }

    public void Accumulate(SqlString Value)
    {
        if (!Value.IsNull)
        {
            stringValue = Value;
        }
    }

    public void Merge(LastString Value)
    {
        if (!Value.stringValue.IsNull)
        {
            stringValue = Value.stringValue;
        }
    }

    public SqlString Terminate()
    {
        return stringValue;
    }

    public void Read(BinaryReader r)
    {
        var isempty = r.ReadBoolean();
        stringValue = r.ReadString();
        if (isempty) stringValue = SqlString.Null;
    }

    public void Write(BinaryWriter w)
    {
        w.Write(stringValue.IsNull);
        w.Write(!stringValue.IsNull ? stringValue.ToString() : string.Empty);
    }

    private SqlString stringValue;
}
