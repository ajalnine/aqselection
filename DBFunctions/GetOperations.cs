﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [Microsoft.SqlServer.Server.SqlFunction(DataAccess=DataAccessKind.Read, 
                                            FillRowMethodName="FillParameterData",
                                            TableDefinition="ParameterName nvarchar(50), ParameterValue nvarchar(50), ProcessingNumber tinyint, OptionalCategory nvarchar(50), ValueGotAt datetime")]
    
    public static IEnumerable GetOperations(SqlDecimal KeyValue,  SqlString Command)
    {
        List<ParameterData> Readed = new List<ParameterData>();

        using (SqlConnection connection = new SqlConnection("context connection=true")) 
        {
            connection.Open();

            using (SqlCommand sqlCommand = new SqlCommand(Command.ToString(), connection)) 
            {
                SqlParameter SmeltID = sqlCommand.Parameters.Add("@SmeltID", SqlDbType.Decimal);
                SmeltID.Value = KeyValue;

                using (SqlDataReader OperationsReader = sqlCommand.ExecuteReader())
                {
                    while (OperationsReader.Read()) 
                    {
                        ParameterData pd = new ParameterData();
                        pd.ParameterName = OperationsReader.GetSqlString(0);
                        pd.ParameterValue = OperationsReader.GetSqlString(1);
                        Readed.Add(pd);
                    }
                }
            }
        }

        List<SqlString> Names = new List<SqlString>();
        foreach (ParameterData r in Readed)
        {
            if (!Names.Contains(r.ParameterName)) Names.Add(r.ParameterName);
        }
        
        foreach (SqlString n in Names)
        {
            List<ParameterData> p = new List<ParameterData>(); 
            foreach (ParameterData s in Readed)
            {
                if (s.ParameterName == n) p.Add(s);
            }
            
            for (int i = 0; i < p.Count; i++)
            {
                p[i].ProcessingNumber = (SqlByte)i;
            }
        }

        return Readed;
   }

    public static void FillParameterData(object ResultObj, out SqlString parametername, out SqlString parametervalue, out SqlByte processingnumber, out SqlString optionalcategory, out SqlDateTime valuegotat)
    {
       ParameterData pd= (ParameterData)ResultObj;
       parametername = pd.ParameterName;
       parametervalue = pd.ParameterValue;
       processingnumber = pd.ProcessingNumber;
       optionalcategory = pd.OptionalCategory;
       valuegotat = pd.ValueGotAt;
    }

    public class ParameterData
    {
        public SqlString ParameterName;
        public SqlString ParameterValue;
        public SqlByte ProcessingNumber;
        public SqlString OptionalCategory;
        public SqlDateTime ValueGotAt;
    }
};

