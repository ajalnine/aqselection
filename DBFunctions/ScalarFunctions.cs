﻿using System;
using System.Text.RegularExpressions;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [SqlFunction]
    public static SqlString GetSplitted(SqlString Input, SqlString SplitBy, SqlInt32 Number)
    {
        var splitted = Input.ToString().Split(new String[] { SplitBy.ToString() }, StringSplitOptions.RemoveEmptyEntries);
        if (splitted.Length - 1 < Number) return null;
        var Result = splitted[(Int32)Number];
        return Result.Trim();
    }

    [SqlFunction]
    public static SqlBoolean IsStrongNumeric(SqlString Input)
    {
        decimal temp;
        return decimal.TryParse(Input.ToString(), out temp);
    }
    
    [SqlFunction]
    public static SqlDouble ToNumeric(SqlString Input)
    {
        double temp;
        bool result = double.TryParse(Input.ToString().Replace('.',','), out temp);
        if (result) return new SqlDouble(temp);
        else return new SqlDouble();
    }

    [SqlFunction]
    public static SqlDouble ParseDigitsToNumeric(SqlString Input)
    {
        if (Input.IsNull) return new SqlDouble();
        int Result = 0;
        string n = Regex.Replace(Input.Value, @"[^0-9]*", "");
        if (!Int32.TryParse(n, out Result)) return new SqlDouble();
        return new SqlDouble(Result);
    }

    [SqlFunction]
    public static SqlDouble MultipleToNumeric(SqlString Input, SqlInt32 ParamID, SqlString ParamIDList)
    {
        foreach (SqlString p in ParamIDList.ToString().Split(','))
        {
            if (p == ParamID.ToString())
            {
                double temp;
                bool result = double.TryParse(Input.ToString().Replace('.', ','), out temp);
                if (result) return new SqlDouble(temp);
            }
        }
        return new SqlDouble();
    }

    [SqlFunction]
    public static SqlString MultipleToString(SqlString Input, SqlInt32 ParamID, SqlString ParamIDList)
    {
        foreach (SqlString p in ParamIDList.ToString().Split(','))
        {
            if (p == ParamID.ToString()) return new SqlString(Input.ToString());
        }
        return new SqlString();
    }
    
    [SqlFunction]
    public static SqlString GetSplittedValue(SqlString Input, SqlInt32 Index, SqlString Divider)
    {
        string[] array = Input.ToString().Split(Divider.ToString()[0]);
        if (Index >= array.Length) return new SqlString();
        else return array[(int)Index].Trim();
    }

    [SqlFunction]
    public static SqlDouble SingleToNumeric(SqlString Input, SqlInt32 ParamID, SqlInt32 SingleParamID)
    {
        if (SingleParamID == ParamID)
        {
            double temp;
            bool result = double.TryParse(Input.ToString().Replace('.', ','), out temp);
            if (result) return new SqlDouble(temp);
        }
        return new SqlDouble();
    }

    [SqlFunction]
    public static SqlString GetScaleRG(SqlDouble input, SqlDouble start, SqlDouble end)
    {
        double value;
        if (input.IsNull) return "#FFFFFFFF";
        var min = start.IsNull ? 0 : start.Value;
        var max = end.IsNull ? 1 : end.Value;
        if (Math.Abs(max - min) < 1e-8) return "#FFFFFFFF";
        if (input.Value < min) value = 0;
        else if (input.Value > max) value = 1;
        else value = (input.Value - min) / (max - min);

        return new SqlString(value <= 0.5
        ? "#FF" + Convert.ToByte(0x30 + value * 0x19c).ToString("X") + Convert.ToByte(0xc0 + value * 0x7e).ToString("X") + Convert.ToByte(0x10).ToString("X")
        : "#FF" + Convert.ToByte(0x11f - value * 0x40).ToString("X") + Convert.ToByte(0x1bf - value * 0x19e).ToString("X") + Convert.ToByte(0x10).ToString("X"));
    }
    
    [SqlFunction]
    public static SqlDouble NormalCD(SqlDouble X, SqlDouble Mean, SqlDouble stdev)
    {
        if (X.IsNull || Mean.IsNull || stdev.IsNull) return new SqlDouble();
        if (stdev.Value == 0) return (Mean > X)? 0: 1;
        return new SqlDouble(0.5 * (1 + Erf((X.Value - Mean.Value) / (stdev.Value * Math.Sqrt(2)))).Value);
    }

    public static Double? Erf(Double? X)
    {
        if (!X.HasValue) return null;
        double x = X.Value;
        double c = .564189583547756;
        double[] a = { 7.7105849500132e-5, -.00133733772997339, .0323076579225834, .0479137145607681, .128379167095513 };
        double[] b = { .00301048631703895, .0538971687740286, .375795757275549 };
        double[] p = { -1.36864857382717e-7, .564195517478974, 7.21175825088309, 43.1622272220567, 152.98928504694, 339.320816734344, 451.918953711873, 300.459261020162 };
        double[] q = { 1, 12.7827273196294, 77.0001529352295, 277.585444743988, 638.980264465631, 931.35409485061, 790.950925327898, 300.459260956983 };
        double[] r = { 2.10144126479064, 26.2370141675169, 21.3688200555087, 4.6580782871847, .282094791773523 };
        double[] s = { 94.153775055546, 187.11481179959, 99.0191814623914, 18.0124575948747 };

        double ret_val;
        double t, x2, ax, bot, top;

        ax = Math.Abs(x);
        if (ax <= 0.5)
        {
            t = x * x;
            top = (((a[0] * t + a[1]) * t + a[2]) * t + a[3]) * t + a[4] + 1.0;
            bot = ((b[0] * t + b[1]) * t + b[2]) * t + 1.0;
            return x * (top / bot);
        }

        if (ax <= 4)
        {
            top = ((((((p[0] * ax + p[1]) * ax + p[2]) * ax + p[3]) * ax + p[4]) * ax + p[5]) * ax + p[6]) * ax + p[7];
            bot = ((((((q[0] * ax + q[1]) * ax + q[2]) * ax + q[3]) * ax + q[4]) * ax + q[5]) * ax + q[6]) * ax + q[7];
            ret_val = 0.5 - Math.Exp(-x * x) * top / bot + 0.5;
            if (x < 0.0) return -ret_val;
            else return ret_val;
        }

        if (ax >= 5.8)
        {
            return x > 0 ? 1 : -1;
        }

        x2 = x * x;
        t = 1.0 / x2;
        top = (((r[0] * t + r[1]) * t + r[2]) * t + r[3]) * t + r[4];
        bot = (((s[0] * t + s[1]) * t + s[2]) * t + s[3]) * t + 1.0;
        t = (c - top / (x2 * bot)) / ax;
        ret_val = 0.5 - Math.Exp(-x2) * t + 0.5;
        if (x < 0.0) return -ret_val;
        else return ret_val;
    }
};

