﻿using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace DBFunctions
{
    public partial class UserDefinedFunctions
    {
        [SqlFunction]
        public static SqlString GetNameFromSid(SqlBinary binaryForm)
        {
            var sid = new System.Security.Principal.SecurityIdentifier(binaryForm.Value, 0);
            if (!sid.IsValidTargetType(typeof(System.Security.Principal.NTAccount))) return sid.Value;
            var ntAccount = (System.Security.Principal.NTAccount)sid.Translate(typeof(System.Security.Principal.NTAccount));
            return ntAccount.Value;
        }
    }
}
