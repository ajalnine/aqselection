﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

[Serializable]
[SqlUserDefinedAggregate(Format.UserDefined, MaxByteSize=8000, IsInvariantToDuplicates=true, IsInvariantToNulls=true, IsInvariantToOrder=true, IsNullIfEmpty=true)]
public class Concatenate: IBinarySerialize
{
    public void Init()
    {
        ReturnString = new StringBuilder();
    }

    public void Accumulate(SqlString Value)
    {
        if (!Value.IsNull)
        {
            if (ReturnString.Length > 0) ReturnString.Append("; ");
            ReturnString.Append(Value.ToString());
        }
    }

    public void Merge(Concatenate Group)
    {
        if (ReturnString.Length > 0) ReturnString.Append("; ");
        ReturnString.Append(Group.ReturnString.ToString());
    }

    public SqlString Terminate()
    {
        return ReturnString.ToString();
    }

    public void Read(BinaryReader r)
    {
        ReturnString = new StringBuilder(r.ReadString());
    }

    public void Write(BinaryWriter w)
    {
        w.Write(ReturnString.ToString());
    }

    private StringBuilder ReturnString;
}
