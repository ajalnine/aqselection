﻿using System.Collections.Generic;
using AQControlsLibrary;
using ApplicationCore;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;
using AQAnalysisLibrary;

namespace SLThicknessAnalyzer
{
    public partial class MainPage
    {
        private void InitializeUserCommands()
        {
            _viewDataUserCommand = new UserCommand
                (work:(c, a) =>
                    {
                        var cs = Services.GetCalculationService();
                        var t = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных"}, "Расчет", TaskPanel);
                        var filters = ColumnsPresenter.GetFilters();

                        t.StartTask();
                        cs.BeginFillDataSetWithThicknessReport(CurrentReportMode, filters.WorkFilter + "AND" + Security.GetMarkSQLCheck("Прокатка"), InteractiveParameters.GetValues(CurrentParameters), iar =>
                        {
                            var result = cs.EndFillDataSetWithThicknessReport(iar);

                            if (result.Success)
                            {
                                t.AdvanceProgress();
                                t.SetMessage(result.Message);

                                Dispatcher.BeginInvoke(
                                    () => cs.BeginGetTablesSLCompatible(result.TaskDataGuid, i =>
                                    {
                                        var tables = cs.EndGetTablesSLCompatible(i);
                                        if (tables == null) return;
                                        t.AdvanceProgress();
                                        t.SetMessage("Данные готовы");
                                        c.Success(); 
                                        Dispatcher.BeginInvoke(() =>
                                        {
                                            var ca = new Dictionary<string, object>
                                            {
                                                {"From", ColumnsPresenter.From}, 
                                                {"To", ColumnsPresenter.To}, 
                                                {"SourceData", tables},
                                                {"Description",
                                                    $"Данные толщиномера: {InteractiveParameters.GetDescriptions(CurrentParameters)}"
                                                },
                                                {"TableViewSelectionChanged", new DataAnalysisSelectionChangedDelegate(TableViewSelectionChangedProcessor)},
                                                {"New", true},
                                                {"ResetAnalysis", true},
                                            };
                                            
                                            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                                            

                                        });
                                        
                                    }, t));
                            }
                            else
                            {
                                t.StopTask(result.Message);
                                c.Failed();
                            }
                        }, null);
                    }
                , success: (c, a) => _moduleStateDispatcher.GoState(ModuleStates.UserReady) 
                , failed:(c, a) => _moduleStateDispatcher.GoState(ModuleStates.Error));


            _precheckDataUserCommand = new UserCommand
                (work: (c, a) =>
                {
                    var cs = Services.GetCalculationService();
                    var t = new Task(new[] { "Поиск данных" }, "Поиск данных", TaskPanel);
                    var filters = ColumnsPresenter.GetFilters();

                    t.StartTask();

                    var sql =
                        $@"set dateformat ymd SELECT count(*) as Count from F2500_Catalogue 
left outer join Common_Mark on F2500_Catalogue.MarkID = Common_Mark.ID
where ({
                            filters.WorkFilter}) and ({Security.GetMarkSQLCheck("Прокатка")})";
            
                    cs.BeginGetScalar(sql, iar =>
                    {
                        _rollCount = cs.EndGetScalar(iar);

                        if (_rollCount > 0)
                        {
                            t.AdvanceProgress();
                            t.SetMessage(_rollCount + " рулонов");
                            c.Success();
                        }
                        else
                        {
                            t.StopTask("Нет данных");
                            c.Failed();
                        }
                    }   , null);

                }
                , success: (c, a) =>_moduleStateDispatcher.GoState(ModuleStates.UserReady)
                , failed: (c, a) => _moduleStateDispatcher.GoState(ModuleStates.Error));

            _viewSingleRollCommand = new UserCommand
                (work: (c, a) =>
                    {
                        var cs = Services.GetCalculationService();
                        var filter = $"DateKey='{_currentRollDateKey.ToString(@"yyyy/MM/dd HH:mm:ss")}'";

                        cs.BeginFillDataSetWithThicknessReport(ThicknessReportTypes.SingleRoll, filter, null,iar =>
                                        {
                                            var result = cs.EndFillDataSetWithThicknessReport(iar);
                                            if (result.Success)
                                            {
                                                Dispatcher.BeginInvoke(() =>cs.BeginGetTablesSLCompatible(result.TaskDataGuid,iar2 =>
                                                        {
                                                            _currentRollInfo=cs.EndGetTablesSLCompatible(iar2);
                                                            if (_currentRollInfo.Count !=4)c.Failed();
                                                            c.Success();
                                                        }, null)
                                                    );
                                            }
                                            else
                                            {
                                                c.Failed();
                                            }
                                        }, null);
                    }
                 , success: (c, a) => Dispatcher.BeginInvoke(CreateRollChart)
                 , failed: (c, a) => Dispatcher.BeginInvoke(() =>
                 {
                     _attachedDataAnalysis?.SetCustomTableDetailViewer(null);
                 }));
        }

        private void CreateRollChart()
        {
            if (_attachedDataAnalysis == null) return;

            var v = _attachedDataAnalysis.GetCustomTableDetailViewer();
            if (v == null)
            {
                var rc = new RollChart();
                _attachedDataAnalysis.SetCustomTableDetailViewer(rc);
                rc.SetRollView(_currentRollInfo, CurrentParameters);
            }
            else
            {
                var chart = v as RollChart;
                chart?.SetRollView(_currentRollInfo, CurrentParameters);
            }
            _attachedDataAnalysis.ResetAppendedTables();
            _attachedDataAnalysis.AppendTablesToSources(_currentRollInfo);
        }
    }
}
