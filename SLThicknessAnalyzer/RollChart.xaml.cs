﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using SLThicknessAnalyzer.RollView;
using Xna = Microsoft.Xna.Framework;

namespace SLThicknessAnalyzer
{
    public partial class RollChart
    {
        private static List<SLDataTable> _source;
        private string _chartDescription;
        private string _currentTextureName;
        private Xna.Color _currentBackground;
        private InteractiveParameters _interactiveParameters;
        private RollView.RollView _rollView;
        private bool _isLoaded;
        private bool _lightEnabled;

        public RollChart()
        {
            InitializeComponent();
            _currentTextureName = TextureFactory.GetTextureNames().FirstOrDefault();
            _currentBackground = Xna.Color.Black;
            _lightEnabled = true;
            LightImage.Source = new BitmapImage {UriSource = new Uri("/AQResources;component/Images/LightBulb.png", UriKind.Relative)};
        }

        private void LayoutRoot_OnLoaded(object sender, RoutedEventArgs e)
        {
            _chartDescription = GetChartDescription();
            SetChartTitle();
            SetupTextureSelectionPanel();
            if (!_isLoaded) RenewRollView();
            _isLoaded = true;
        }

        private void RenewRollView()
        {
            UpdateLayout();
            _rollView = new RollView.RollView(_source, _interactiveParameters) {Height = ActualWidth * 0.4 / 0.7};
            _rollView.SetViewMode(_currentTextureName, _currentBackground, _lightEnabled);
            _chartDescription = GetChartDescription();
            SetChartTitle();
            RollViewContainer.Children.Clear();
            RollViewContainer.Children.Add(_rollView);
        }

        public void SetRollView(List<SLDataTable> source, InteractiveParameters ip)
        {
            _source = source;
            _interactiveParameters = ip;
            if (_isLoaded) RenewRollView();
        }

        private void SetChartTitle()
        {
            ChartTitle.Text = "Карта толщин полосы";

            if (_source[0] == null || _source[0].Table.Count != 1) return;

            ChartSubtitle.Text = _chartDescription;
        }

        private string GetChartDescription()
        {
            if (_source[0].Table.Count == 0) return string.Empty;
            var chartComment = string.Empty;
            var count = 0;
            var visiblecount = 0;
            foreach (var columnName in _source[0].ColumnNames)
            {
                if (!columnName.StartsWith("#"))
                {
                    if (count != 0)
                    {
                        chartComment += (visiblecount % 3 == 0) ? "\r\n" : "; ";
                    }
                    chartComment += columnName + ": " + _source[0].Table[0].Row[count];
                    visiblecount++;
                }
                count++;
            }
            return chartComment;
        }

/*
        private void NewSlideButton_Click(object sender, RoutedEventArgs e)
        {
            var wb = new WriteableBitmap(ChartPanel, new System.Windows.Media.ScaleTransform { ScaleX = 2, ScaleY = 2 });
            var sed = new SlideElementDescription
            {
                BreakSlide = true,
                Description = _chartDescription,
                ShortDescription = _chartDescription,
                SlideElementType = SlideElementTypes.Image,
                Width = wb.PixelWidth,
                Height = wb.PixelHeight
            };

            var signalArguments = new Dictionary<string, object>
                {
                    {"Thumbnail", wb},
                    {"SlideElement", sed},
                };
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(),
                        Command.InsertSlideElement, signalArguments, null);
        }
*/

        private void SetupTextureSelectionPanel()
        {
            TextureSelectionPanel.Children.Clear();
            SetupTextureSelectionButtons();
            SetupBackgroundSelectionButtons();
        }

        private void SetupTextureSelectionButtons()
        {
            var isFirst = true;
            foreach (var textureName in TextureFactory.GetTextureNames())
            {
                var tb = new TextBlock {Text = textureName, Margin = new Thickness(10, 0, 10, 0)};
                var rb = new RadioButton
                    {
                        GroupName = "TextureSelectorGroup",
                        Content = tb,
                        Style = (Style) Resources["RadioButtonBorderStyle"]
                    };
                rb.Click += rb_Click;
                if (isFirst) rb.IsChecked = true;
                TextureSelectionPanel.Children.Add(rb);
                isFirst = false;
            }
        }

        private void SetupBackgroundSelectionButtons()
        {
            var rb1 = CreateBackgroundSelectorButton(Colors.Black);
            rb1.IsChecked = true;
            var rb2 = CreateBackgroundSelectorButton(Colors.Gray);
            var rb3 = CreateBackgroundSelectorButton(Colors.White);
            TextureSelectionPanel.Children.Add(rb1);
            TextureSelectionPanel.Children.Add(rb2);
            TextureSelectionPanel.Children.Add(rb3);
        }

        private RadioButton CreateBackgroundSelectorButton(Color c)
        {
            var xnaColor = new Microsoft.Xna.Framework.Color(c.R, c.G, c.B);
            var rbbackground = new RadioButton
                {
                    GroupName = "BackgroundSelectorGroup",
                    Content = new Rectangle {Width = 20, Height = 10, 
                        Fill = new SolidColorBrush(c), 
                        Margin = new Thickness(10, 2, 10, 2), 
                        StrokeThickness = 1, 
                        Stroke = new SolidColorBrush(Colors.Black)},
                    Style = (Style) Resources["RadioButtonBorderStyle"],
                    Tag = xnaColor
                };
            rbbackground.Click += rbbackground_Click;
            return rbbackground;
        }

        void rbbackground_Click(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb == null) return;
            _currentBackground = (Microsoft.Xna.Framework.Color)rb.Tag;
            RefreshRollView();
        }

        void rb_Click(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb == null) return;
            _currentTextureName = ((TextBlock)rb.Content).Text;
            RefreshRollView();
        }

        private void LightButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toggleButton = sender as ToggleButton;
            if (toggleButton?.IsChecked != null) _lightEnabled = toggleButton.IsChecked.Value;
            RefreshRollView();
        }

        private void RefreshRollView()
        {
            _rollView.SetViewMode(_currentTextureName, _currentBackground, _lightEnabled);
        }
    }
}
