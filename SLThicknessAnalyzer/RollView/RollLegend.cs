﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SLThicknessAnalyzer.RollView
{
    public class RollLegend
    {
        private readonly ColorScale _currentColorScale;

        public RollLegend(ColorScale colorScale)
        {
            _currentColorScale = colorScale;
        }

        public void DrawLegend(SpriteFont font1, GraphicsDevice gd, SpriteBatch spriteBatch)
        {
            if (_currentColorScale == null) return;
            var stops = (_currentColorScale.IsDiscrete) ? (IEnumerable<ColorScaleStop>) _currentColorScale.GetColorScaleStops() : _currentColorScale.GetColorScaleStops().OrderByDescending(a => a.Level);
            var fontSize = font1.MeasureString("Ш");
            var startPosition = new Vector2(28, gd.Viewport.Height - fontSize.Y * stops.Count() - 10);
            var regionSize = CalcRegionSize(font1, fontSize, stops);

            DrawRectangle(gd, spriteBatch, new Color(0f, 0f, 0f, 0.5f), 0, (int)startPosition.Y - 10, (int)regionSize.X + 40, (int)regionSize.Y + 20);
            DrawStopNames(font1, spriteBatch, startPosition, stops, fontSize);
            if (!_currentColorScale.IsDiscrete) DrawScale(gd, startPosition, fontSize, regionSize, spriteBatch);
            else DrawDiscrete(gd, startPosition, fontSize, spriteBatch);
        }

        private static Vector2 CalcRegionSize(SpriteFont font1, Vector2 fontSize, IEnumerable<ColorScaleStop> stops)
        {
            var colorScaleStops = stops as IList<ColorScaleStop> ?? stops.ToList();
            var regionHeight = fontSize.Y*colorScaleStops.Count;
            var maxWidth =
                (from line in colorScaleStops
                 select "-" + line.Name
                 into label select font1.MeasureString(label)
                 into lineSize select lineSize.X).Concat(new[] {0f}).Max();

            var regionSize = new Vector2(maxWidth, regionHeight);
            return regionSize;
        }

        private void DrawStopNames(SpriteFont font1, SpriteBatch spriteBatch, Vector2 startPosition, IEnumerable<ColorScaleStop> stops, Vector2 fontSize)
        {
            var position = startPosition;

            foreach (var line in stops)
            {
                var label = "-" + line.Name;
                spriteBatch.DrawString(font1, label, position, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                position.Y += fontSize.Y;
            }
        }

        private void DrawDiscrete(GraphicsDevice gd, Vector2 startPosition, Vector2 fontSize, SpriteBatch spriteBatch)
        {
            var offset = (int)startPosition.Y;

            foreach (var s in _currentColorScale.GetColorScaleStops())
            {
                var color = Color.Multiply(s.Color, 1.5f);
                DrawRectangle(gd, spriteBatch, color, 8, offset, 16, (int)fontSize.Y);
                offset += (int)fontSize.Y;
            }
        }

        private void DrawScale(GraphicsDevice gd, Vector2 startPosition, Vector2 fontSize, Vector2 regionSize, SpriteBatch spriteBatch)
        {
            var lineOffset = fontSize.Y / 2;
            var scale = new Texture2D(gd, 0x1, 0x100);
            var colors = new Color[0x100];

            for (var i = 0; i < 0x100; i++)
            {
                var value = _currentColorScale.MaxValue - i*(_currentColorScale.MaxValue - _currentColorScale.MinValue)/0x100;
                var color = Color.Multiply(_currentColorScale.GetColor(value), 1.5f);
                colors[i] = color;
            }
            scale.SetData(colors);
            var textureDestination = new Rectangle(8, (int) (startPosition.Y + lineOffset), 16,
                                                   (int) (regionSize.Y - lineOffset*2));
            spriteBatch.Draw(scale, textureDestination, Color.White);

            DrawRectangle(gd, spriteBatch, Color.Multiply(_currentColorScale.GetColor(_currentColorScale.MaxValue), 1.5f), 8, (int) (startPosition.Y), 16, (int) (lineOffset));
            DrawRectangle(gd, spriteBatch, Color.Multiply(_currentColorScale.GetColor(_currentColorScale.MinValue), 1.5f), 8, (int) (startPosition.Y + regionSize.Y - lineOffset), 16, (int) (lineOffset));
        }

        private static void DrawRectangle(GraphicsDevice gd, SpriteBatch spriteBatch, Color c, int x, int y, int width, int height)
        {
            var scaleBegin = new Texture2D(gd, 0x1, 0x1);
            scaleBegin.SetData(new[] { c });
            var textureDestination = new Rectangle(x, y, width, height);
            spriteBatch.Draw(scaleBegin, textureDestination, Color.White);
        }
    }
}