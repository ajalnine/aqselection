﻿using System;
using Microsoft.Xna.Framework;

namespace SLThicknessAnalyzer.RollView
{
    public class ColorScaleStop : IComparable<ColorScaleStop>
    {
        public float Level { get; set; }
        public Color Color { get; set; }
        public string Name { get; set; }

        public int CompareTo(ColorScaleStop other)
        {
            return Math.Sign(Level - other.Level);
        }

        public new string ToString()
        {
            return Name;
        }
    }
}