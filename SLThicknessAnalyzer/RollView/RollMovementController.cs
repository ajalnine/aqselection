﻿using System.Windows.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SLThicknessAnalyzer.RollView
{
    public class RollMovementController : MovementController
    {
        private const float BlockedRotationRadius = 70;
        private const float AutoRotationSpeed = 3;
        private const float DeltaZSpeed = 18;
        private const float DistanceSpeed = 100;
        private const float DefaultDistance = 5;

        public RollMovementController(Control control, Movement min, Movement max) : base(control, min, max)
        {
        }

        protected override Movement GetAutoMovement(float time)
        {
            var currentMovement = new Movement
                {
                    Rotation = time / AutoRotationSpeed,
                    DeltaZ = 0,
                    Distance = DefaultDistance
                };
            return currentMovement;
        }

        protected override Movement GetDeltaMovement()
        {
            var currentMouseState = Mouse.GetState();
            var currentMousePosition = new Vector2(currentMouseState.X, currentMouseState.Y);
            var currentMouseWheelPosition = currentMouseState.ScrollWheelValue;
            
            var movement = new Movement();
            if (currentMouseState.LeftButton == ButtonState.Pressed && OldMouseButtonState == ButtonState.Pressed)
            {
                var oldVector = OldMousePosition - Center;
                var newVector = currentMousePosition - Center;
                var moveVector = newVector - oldVector;
                movement.Rotation = (newVector.Length() > BlockedRotationRadius) ? VectorExtensions.GetRotation(newVector, oldVector) : 0;
                movement.DeltaZ = (moveVector != Vector2.Zero) ? VectorExtensions.GetZAxisOffset(CurrentMovement.Rotation, movement.Rotation, moveVector) / DeltaZSpeed : 0;
            }
            movement.Distance = (currentMouseWheelPosition - OldMouseWheelPosition) / DistanceSpeed;
            
            OldMousePosition = currentMousePosition;
            OldMouseButtonState = currentMouseState.LeftButton;
            OldMouseWheelPosition = currentMouseWheelPosition;
            return movement;
        }
    }
}
