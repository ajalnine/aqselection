﻿using ApplicationCore.CalculationServiceReference;
using System.Windows.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SLThicknessAnalyzer.RollView
{
    public class Thickness3D
    {
        private readonly SLDataTable _thicknessTable;
        private readonly float _nominalThickness;
        private const int SkipEdge = 0;

        public Thickness3D(SLDataTable thicknessTable, SLDataTable descriptionTable)
        {
            _thicknessTable = thicknessTable;
            var descriptionTable1 = descriptionTable;
            _nominalThickness = (float)((double)descriptionTable1.Table[0].Row[descriptionTable1.ColumnNames.IndexOf("#Толщина")]);
        }
        
        public VertexBuffer GetVertexBuffer(float xsize, float ysize, float zsize)
        {
            var tolerance = - GetNominalThickness() * 0.5f;
            var heightMap = GetHeightMap(xsize, ysize, zsize);
            var normalMap = VectorExtensions.HeightMapToTriangleNormals(heightMap);
            var smoothNormalMap = VectorExtensions.SmoothTriangleNormals(normalMap);
            var vertices = XNAHelper.CreateLandscapeVertices(heightMap, smoothNormalMap, tolerance, SkipEdge);

            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            if (vertices.Count == 0) return null;
            var vb = new VertexBuffer(gd, VertexPositionNormalTexture.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);
            vb.SetData(0, vertices.ToArray(), 0, vertices.Count, 0);
            return vb;
        }

        private Vector3[,] GetHeightMap(float xsize, float ysize, float sharpness)
        {
            var sections = _thicknessTable.Table.Count;
            var cells = _thicknessTable.ColumnNames.Count - 1;

            var heightMap = new Vector3[cells, sections];
            var stepx = xsize / (cells - 1);
            var rollLength = (float)((double)_thicknessTable.Table[sections - 1].Row[0]);

            for (var y = 0; y < sections; y++)
            {
                var offset = (float)((double)_thicknessTable.Table[y].Row[0]);
                var y1 = ysize * offset / rollLength;
                for (var x = 0; x < cells; x++)
                {
                    var x1 = x * stepx;
                    var cellThickness = (float)((double)_thicknessTable.Table[y].Row[x + 1]);
                    var height = sharpness * (cellThickness - _nominalThickness);
                    heightMap[x, y] = new Vector3(x1 - xsize / 2, height, y1 - ysize / 2);
                }
            }
            return heightMap;
        }

        public float GetNominalThickness()
        {
            return _nominalThickness;
        }
    }
}
