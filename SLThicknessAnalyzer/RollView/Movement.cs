﻿namespace SLThicknessAnalyzer.RollView
{
    public struct Movement
    {
        public float Rotation { get; set; }
        public float DeltaZ { get; set; }
        public float Distance { get; set; }

        public static Movement operator +(Movement movement, Movement delta)
        {
            return new Movement
                {
                    Rotation = movement.Rotation + delta.Rotation,
                    DeltaZ = movement.DeltaZ + delta.DeltaZ,
                    Distance = movement.Distance + delta.Distance
                };
        }

        public static Movement operator -(Movement movement, Movement delta)
        {
            return new Movement
                {
                    Rotation = movement.Rotation - delta.Rotation,
                    DeltaZ = movement.DeltaZ - delta.DeltaZ,
                    Distance = movement.Distance - delta.Distance
                };
        }

        public void Limit(Movement min, Movement max)
        {
            DeltaZ = (DeltaZ > max.DeltaZ) ? max.DeltaZ : (DeltaZ < min.DeltaZ) ? min.DeltaZ : DeltaZ;
            Distance = (Distance > max.Distance) ? max.Distance : (Distance < min.Distance) ? min.Distance : Distance;
        }
    }
}