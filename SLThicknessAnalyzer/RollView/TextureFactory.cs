﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using System.Windows.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SLThicknessAnalyzer.RollView
{
    public static class TextureFactory
    {
        public delegate Texture2D TextureFactoryMethodDelegate(List<SLDataTable> source, InteractiveParameters ip, ColorScale colorScale);

        private static readonly Dictionary<string, TextureFactoryMethodDelegate> Textures = new Dictionary<string, TextureFactoryMethodDelegate>
                {
                    {"Участки полосы", CreateRangeTexture},
                    {"Отклонение толщины", CreateDeviationTexture},
                    {"Температуры", CreateTemperaturesTexture}
                };

        public static Texture2D GetTexture(string textureName, List<SLDataTable> source, InteractiveParameters ip, ColorScale colorScale)
        {
            return Textures.ContainsKey(textureName) ? Textures[textureName].Invoke(source, ip, colorScale) : null;
        }

        public static List<string> GetTextureNames()
        {
            return Textures.Keys.ToList();
        }

        private static Texture2D CreateRangeTexture(List<SLDataTable> source, InteractiveParameters interactiveParameters, ColorScale colorScale)
        {
            const int textureHeight = 0x100;
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            var sections = source[1].Table.Count;
            var texture = new Texture2D(gd, 1, textureHeight, false, SurfaceFormat.Color);
            var colors = new Color[textureHeight];

            var begin = interactiveParameters.GetValue("Длина начального участка, м");
            var end = interactiveParameters.GetValue("Длина концевого участка, м");

            var rollLength = (double)source[1].Table[sections - 1].Row[0];

            for (var i = 0; i < textureHeight; i++)
            {
                var j = (int)(sections / (double)textureHeight * i);
                var offset = (double)source[1].Table[j].Row[0];
                var part = (offset < begin) ? 1 : (offset < rollLength - end) ? 2 : 3;
                colors[i] = colorScale.GetColor(part);
            }

            texture.SetData(colors);
            return texture;
        }

        private static Texture2D CreateDeviationTexture(List<SLDataTable> source, InteractiveParameters interactiveParameters, ColorScale colorScale)
        {
            return TableToMap2D(colorScale, source[1], 0x80, 0x200, 1);
        }

        private static Texture2D CreateTemperaturesTexture(List<SLDataTable> source, InteractiveParameters interactiveParameters, ColorScale colorScale)
        {
            return TableToMap2D(colorScale, source[2], 0x20, 0x80, 1); 
        }

        private static Texture2D TableToMap2D(ColorScale colorScale, SLDataTable sldt, int textureWidth, int textureHeight, int fieldOffset)
        {
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            var sections = sldt.Table.Count;
            var cells = sldt.ColumnNames.Count - fieldOffset;

            var texture = new Texture2D(gd, textureWidth, textureHeight, false, SurfaceFormat.Color);
            var colors = new Color[textureHeight * textureWidth];

            for (var y = 0; y < textureHeight; y++)
            {
                var j = (int) (sections / (double) textureHeight * y);
                for (var x = 0; x < textureWidth; x++)
                {
                    var i = (int) (cells / (double) textureWidth * x);
                    var value = (float)(double) sldt.Table[j].Row[i + fieldOffset];
                    colors[x + y * textureWidth] = colorScale.GetColor(value);
                }
            }
            texture.SetData(colors);
            return texture;
        }
    }
}
