﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQCalculationsLibrary;
using System.Windows.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SLThicknessAnalyzer.RollView
{
    public partial class RollView
    {
        const float Xsize = 3f;
        const float Ysize = 9f;
        const float Sharp = 3f;
        private static readonly Movement MinMovement = new Movement { DeltaZ = -Ysize / 2, Distance = 2 };
        private static readonly Movement MaxMovement = new Movement { DeltaZ = Ysize / 2, Distance = 10 };

        private static SLDataTable ThicknessTable => _source[1];

        private static SLDataTable DescriptionTable => _source[0];

        private float _aspectRatio;
        
        private static List<SLDataTable> _source;
        private static InteractiveParameters _interactiveParameters;
        private static string _textureName;
        private bool _lightEnabled;
        private VertexBuffer _vertexBuffer;
        private Texture2D _currentTexture;
        private ColorScale _currentColorScale;
        private Color _currentBackground;
        private MovementController _movementController;
        private RollLegend _rollLegend;

        public RollView(List<SLDataTable> source, InteractiveParameters ip)
        {
            _source = source;
            _interactiveParameters = ip;
            InitializeComponent();
        }

        private void LayoutRoot_OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateLayout();
            _aspectRatio = (float)RenderSize.Width / (float)RenderSize.Height;
            SetupScene();
        }

        private void SetupScene()
        {
            _vertexBuffer = (new Thickness3D(ThicknessTable, DescriptionTable)).GetVertexBuffer(Xsize, Ysize, Sharp);
            _currentColorScale = ColorScaleFactory.GetColorScale(_textureName, _source, _interactiveParameters);
            _currentTexture = TextureFactory.GetTexture(_textureName, _source, _interactiveParameters, _currentColorScale);
            _rollLegend = new RollLegend(_currentColorScale);
            _movementController = new RollMovementController(this, MinMovement, MaxMovement);
        }

        private void DrawingSurface_OnDraw(object sender, DrawEventArgs e)
        {
            SetupGraphicsDevice();
            SetupRenderEffect(e);
            if (DrawScene())DrawOverlays();
            else DrawNoDataMessage();
            e.InvalidateSurface();
        }

        private void SetupGraphicsDevice()
        {
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            gd.Clear(_currentBackground);
            gd.RasterizerState = new RasterizerState { MultiSampleAntiAlias = true, CullMode = CullMode.None, FillMode = FillMode.Solid};
        }

        private void SetupRenderEffect(DrawEventArgs e)
        {
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            var basicEffect = new BasicEffect(gd);
            if (_lightEnabled)
            {
                basicEffect.EnableDefaultLighting();
                basicEffect.LightingEnabled = true;
            }
            basicEffect.Texture = _currentTexture;
            basicEffect.TextureEnabled = true;
            var movement = _movementController.GetMovement((float)e.TotalTime.TotalSeconds);
            basicEffect.World = Matrix.CreateTranslation(0, 0, movement.DeltaZ) * Matrix.CreateRotationY(movement.Rotation);
            basicEffect.View = Matrix.CreateLookAt(new Vector3(0, movement.Distance/1.3f, movement.Distance), Vector3.Zero, Vector3.Up);
            basicEffect.Projection = Matrix.CreatePerspectiveFieldOfView(0.85f, _aspectRatio, 0.01f, 1000.0f);
            basicEffect.CurrentTechnique.Passes[0].Apply();
        }

        private bool DrawScene()
        {
            if (_vertexBuffer == null) return false;
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            gd.SetVertexBuffer(_vertexBuffer);
            try
            {
                gd.DrawPrimitives(PrimitiveType.TriangleList, 0, _vertexBuffer.VertexCount/3);
            }
            catch
            {
                // ignored
            }
            return true;
        }

        private void DrawOverlays()
        {
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            var font1 = XnaContent.AppContentManager.Load<SpriteFont>(".\\Content\\Arial10");
            var spriteBatch = new SpriteBatch(gd);
            spriteBatch.Begin();
            _rollLegend.DrawLegend(font1, gd, spriteBatch);
            spriteBatch.End();
            gd.DepthStencilState = new DepthStencilState { DepthBufferEnable = true };
        }

        private void DrawNoDataMessage()
        {
            var gd = GraphicsDeviceManager.Current.GraphicsDevice;
            var font1 = XnaContent.AppContentManager.Load<SpriteFont>(".\\Content\\Arial10");
            var spriteBatch = new SpriteBatch(gd);
            spriteBatch.Begin();
            var position = new Vector2(28, gd.Viewport.Height - font1.MeasureString("Ш").Y - 10);
            spriteBatch.DrawString(font1, "Нет данных", position, Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            spriteBatch.End();
            gd.DepthStencilState = new DepthStencilState { DepthBufferEnable = true };
        }
        
        public void SetViewMode(string textureName, Color background, bool lightEnabled)
        {
            _textureName = textureName;
            _currentColorScale = ColorScaleFactory.GetColorScale(_textureName, _source, _interactiveParameters);
            _currentTexture = TextureFactory.GetTexture(_textureName, _source, _interactiveParameters, _currentColorScale);
            _rollLegend = new RollLegend(_currentColorScale);
            _lightEnabled = lightEnabled;
            _currentBackground = background;
        }

        private void DrawingSurface_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Math.Abs(e.PreviousSize.Height) < 1e-8 || Math.Abs(e.PreviousSize.Width - e.NewSize.Width) < 1e-8) return;
            Height = e.NewSize.Width / (7f / 4f);
            ((DrawingSurface) sender).Height = Height;
            ((DrawingSurface)sender).UpdateLayout();
        }
    }
}
