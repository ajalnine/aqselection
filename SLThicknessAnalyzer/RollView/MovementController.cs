﻿using System.Windows.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SLThicknessAnalyzer.RollView
{
    public abstract class MovementController
    {
        protected static Vector2 OldMousePosition;
        protected static Vector2 Center;
        protected static ButtonState OldMouseButtonState;
        protected static int OldMouseWheelPosition;
        protected static Movement MinConstraint;
        protected static Movement MaxConstraint;
        
        protected Movement CurrentMovement;
        protected bool AutoRotateDisabled;

        protected MovementController(Control control, Movement min, Movement max)
        {
            Mouse.RootControl = control;
            MinConstraint = min;
            MaxConstraint = max;
            Center = new Vector2((float)control.RenderSize.Width / 2, (float)control.RenderSize.Height / 2);
            CurrentMovement = new Movement();
        }

        public Movement GetMovement(float time)
        {
            UpdateAutoRotateStatus();
            if (AutoRotateDisabled)
            {
                CurrentMovement += GetDeltaMovement();
            }
            else
            {
                CurrentMovement = GetAutoMovement(time);
            }
            CurrentMovement.Limit(MinConstraint, MaxConstraint);
            return CurrentMovement; 
        }

        protected abstract Movement GetDeltaMovement();
        protected abstract Movement GetAutoMovement(float time);

        private void UpdateAutoRotateStatus()
        {
            if (Mouse.GetState().LeftButton == ButtonState.Pressed) AutoRotateDisabled = true;
        }
    }
}
