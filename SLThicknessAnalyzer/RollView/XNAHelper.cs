﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SLThicknessAnalyzer.RollView
{
    public class XNAHelper
    {
        public static List<VertexPositionNormalTexture> CreateLandscapeVertices(Vector3[,] heightMap, Vector3[,] smoothNormalMap, float tolerance, int skipEdge)
        {
            var vertices = new List<VertexPositionNormalTexture>();
            var cells = heightMap.GetLength(0);
            var sections = heightMap.GetLength(1);

            var ustep = (float)1 / cells;
            var vstep = (float)1 / sections;

            for (var x = 0; x < cells - 1; x++)
            {
                var u = x * ustep;

                for (var y = skipEdge; y < sections - 1 - skipEdge; y++)
                {
                    var v = y * vstep;

                    var a = heightMap[x, y];
                    var b = heightMap[x, y + 1];
                    var c = heightMap[x + 1, y + 1];
                    var d = heightMap[x + 1, y];

                    var an = smoothNormalMap[x, y];
                    var bn = smoothNormalMap[x, y + 1];
                    var cn = smoothNormalMap[x + 1, y + 1];
                    var dn = smoothNormalMap[x + 1, y];

                    var at = new Vector2(u, v);
                    var bt = new Vector2(u, v + vstep);
                    var ct = new Vector2(u + ustep, v + vstep);
                    var dt = new Vector2(u + ustep, v);

                    if (a.Y > tolerance && b.Y > tolerance && c.Y > tolerance)
                    {
                        vertices.Add(new VertexPositionNormalTexture(a, an, at));
                        vertices.Add(new VertexPositionNormalTexture(b, bn, bt));
                        vertices.Add(new VertexPositionNormalTexture(c, cn, ct));
                    } 

                    if (a.Y > tolerance && d.Y > tolerance && c.Y > tolerance)
                    {
                        vertices.Add(new VertexPositionNormalTexture(c, cn, ct));
                        vertices.Add(new VertexPositionNormalTexture(d, dn, dt));
                        vertices.Add(new VertexPositionNormalTexture(a, an, at));
                    }
                }
            }
            return vertices;
        }
    }
}