﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using Microsoft.Xna.Framework;

namespace SLThicknessAnalyzer.RollView
{
    public static class ColorScaleFactory
    {
        public delegate ColorScale ColorScaleFactoryMethodDelegate(List<SLDataTable> source, InteractiveParameters ip);

        private static readonly Dictionary<string, ColorScaleFactoryMethodDelegate> ColorScales = new Dictionary<string, ColorScaleFactoryMethodDelegate>
                {
                    {"Участки полосы", GetRangeColorScale},
                    {"Отклонение толщины", GetDeviationColorScale},
                    {"Температуры", GetTemperatureColorScale}
                };

        public static ColorScale GetColorScale(string scaleName, List<SLDataTable> source, InteractiveParameters ip)
        {
            return ColorScales.ContainsKey(scaleName) ? ColorScales[scaleName].Invoke(source, ip) : null;
        }

        private static ColorScale GetRangeColorScale(IList<SLDataTable> source, InteractiveParameters interactiveParameters)
        {
            var rangeColorScale = new ColorScale(new[]
                {
                    new ColorScaleStop {Level = 1, Color = new Color(0.4f, 0.2f, 0.2f), Name = "Начало"},
                    new ColorScaleStop {Level = 2, Color = new Color(0.3f, 0.3f, 0.3f), Name = "Середина"},
                    new ColorScaleStop {Level = 3, Color = new Color(0.4f, 0.3f, 0.2f), Name = "Конец"}
                }) {IsDiscrete = true};
            return rangeColorScale;
        }

        private static ColorScale GetDeviationColorScale(IList<SLDataTable> source, InteractiveParameters interactiveParameters)
        {
            var nominalThickness = (float) (double) source[0].Table[0].Row[source[0].ColumnNames.IndexOf("#Толщина")];
            var operatorMin = (float) (double) source[0].Table[0].Row[source[0].ColumnNames.IndexOf("#Минимальный допуск оператора")];
            var operatorMax = (float) (double) source[0].Table[0].Row[source[0].ColumnNames.IndexOf("#Максимальный допуск оператора")];
            var max = (float) interactiveParameters.GetValue("Верхнее отклонение, мм");
            var min = (float) interactiveParameters.GetValue("Нижнее отклонение, мм");

            var colors = new[]
                {
                    new Color(0.5f, 0.0f, 0.5f),
                    new Color(0.0f, 0.2f, 0.5f),
                    new Color(0.0f, 0.5f, 0.0f),
                    new Color(0.4f, 0.5f, 0.0f),
                    new Color(0.5f, 0.0f, 0.0f)
                };

            var values = new List<ColorScaleStop>
                {
                    new ColorScaleStop {Level = nominalThickness  + operatorMin, Name = "Min оператора"},
                    new ColorScaleStop {Level = nominalThickness  + min, Name = "Min исследователя"},
                    new ColorScaleStop {Level = nominalThickness, Name = "Номинал"},
                    new ColorScaleStop {Level = nominalThickness  + max, Name = "Max исследователя"},
                    new ColorScaleStop {Level = nominalThickness  + operatorMax, Name = "Max оператора"}
                };
            values.Sort();

            var count = 0;
            foreach (var colorScaleStop in values)
            {
                colorScaleStop.Color = colors[count];
                count++;
            }
            return new ColorScale(values);
        }

        private static ColorScale GetTemperatureColorScale(IList<SLDataTable> source, InteractiveParameters interactiveParameters)
        {
            var temperatureColorScale = new ColorScale(new[]
                {
                    new ColorScaleStop {Level = 500, Color = new Color(0.0f, 0.0f, 0.0f), Name = "500 C"},
                    new ColorScaleStop {Level = 600, Color = new Color(0.1f, 0.0f, 0.0f), Name = "600 C"},
                    new ColorScaleStop {Level = 700, Color = new Color(0.4f, 0.0f, 0.0f), Name = "700 C"},
                    new ColorScaleStop {Level = 800, Color = new Color(0.7f, 0.2f, 0.0f), Name = "800 C"},
                    new ColorScaleStop {Level = 900, Color = new Color(0.7f, 0.7f, 0.0f), Name = "900 C"},
                    new ColorScaleStop {Level = 1000, Color = new Color(1.0f, 1.0f, 1.0f), Name = "1000 C"}
                }) { IsDiscrete = false };
            return temperatureColorScale;
        }
    }
}
