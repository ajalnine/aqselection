﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace SLThicknessAnalyzer.RollView
{
    public static class VectorExtensions
    {
        public static float GetRotation(Vector2 newVector, Vector2 oldVector)
        {
            var deltaangle = Math.Atan(newVector.X / newVector.Y) - Math.Atan(oldVector.X / oldVector.Y);

            deltaangle = (deltaangle > Math.PI / 2) ?
            Math.PI - deltaangle
            : (deltaangle < -Math.PI / 2) ?
            -Math.PI + deltaangle
            : deltaangle;

            return (float)deltaangle;
        }

        public static float GetZAxisOffset(float currentAngle, float currentAngleDelta, Vector2 moveVector)
        {
            var directionVector = Vector3.Transform(Vector3.UnitZ, Matrix.CreateRotationY(currentAngle + currentAngleDelta));
            var moveVector3 = new Vector3(moveVector.X, 0, moveVector.Y);
            directionVector.Normalize();
            moveVector3.Normalize();
            return Vector3.Dot(moveVector3, directionVector);
        }

        public static Vector3[,] HeightMapToTriangleNormals(Vector3[,] heightMap)
        {
            var cells = heightMap.GetLength(0);
            var sections = heightMap.GetLength(1);

            var normalMap = new Vector3[(cells - 1) * 2, sections - 1];

            for (var y = 0; y < sections - 1; y++)
            {
                for (var x = 0; x < cells - 1; x++)
                {
                    var a = heightMap[x, y];
                    var b = heightMap[x, y + 1];
                    var c = heightMap[x + 1, y + 1];
                    var d = heightMap[x + 1, y];
                    normalMap[x * 2, y] = Vector3.Cross(a - b, a - c);
                    normalMap[x * 2 + 1, y] = Vector3.Cross(a - c, a - d);
                }
            }
            return normalMap;
        }

        public static Vector3[,] SmoothTriangleNormals(Vector3[,] normalMap)
        {
            var cells = normalMap.GetLength(0) / 2 + 1;
            var sections = normalMap.GetLength(1) + 1;

            var smoothNormalMap = new Vector3[cells, sections];
            for (var y = 0; y < sections; y++)
            {
                for (var x = 0; x < cells; x++)
                {
                    var surroundingNormals = new List<Vector3>();
                    if (x < cells - 1 && y < sections - 1) surroundingNormals.Add(normalMap[2 * x, y]);
                    if (x < cells - 1 && y < sections - 1) surroundingNormals.Add(normalMap[2 * x + 1, y]);
                    if (x > 0 && y < sections - 1) surroundingNormals.Add(normalMap[2 * (x - 1) + 1, y]);
                    if (x > 0 && y > 0) surroundingNormals.Add(normalMap[2 * (x - 1), y - 1]);
                    if (x > 0 && y > 0) surroundingNormals.Add(normalMap[2 * (x - 1) + 1, y - 1]);
                    if (x < cells - 1 && y > 0) surroundingNormals.Add(normalMap[2 * x, y - 1]);

                    var xn = surroundingNormals.Select(n => n.X).Average();
                    var yn = surroundingNormals.Select(n => n.Y).Average();
                    var zn = surroundingNormals.Select(n => n.Z).Average();
                    smoothNormalMap[x, y] = new Vector3(xn, yn, zn);
                }
            }
            return smoothNormalMap;
        }
    }
}
