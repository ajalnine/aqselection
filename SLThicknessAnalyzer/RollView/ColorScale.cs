﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace SLThicknessAnalyzer.RollView
{
    public class ColorScale
    {
        private readonly List<ColorScaleStop> _colorScaleStops;
        public bool IsDiscrete { get; set; }
        public float MinValue => _colorScaleStops.First().Level;
        public float MaxValue => _colorScaleStops.Last().Level;

        public ColorScale()
        {
            _colorScaleStops = new List<ColorScaleStop>();
        }

        public ColorScale(IEnumerable<ColorScaleStop> colorScaleStops)
        {
            _colorScaleStops = colorScaleStops.OrderBy(a=>a.Level).ToList();
        }

        public Color GetColor(float value)
        {
            if (!_colorScaleStops.Any()) return Color.Transparent;

            var count = _colorScaleStops.Count;
            if (count == 1) return _colorScaleStops[0].Color;
            var first = _colorScaleStops.First();
            if (value < first.Level) return first.Color;
            
            for (var i = 0; i < count - 1; i++)
            {
                if (!(_colorScaleStops[i].Level <= value) || !(_colorScaleStops[i + 1].Level > value)) continue;

                if (!IsDiscrete)
                {
                    var k = (value - _colorScaleStops[i].Level)/
                            (_colorScaleStops[i + 1].Level - _colorScaleStops[i].Level);
                    return Color.Lerp(_colorScaleStops[i].Color, _colorScaleStops[i + 1].Color, k);
                }
                return (_colorScaleStops[i].Color);
            }
            return _colorScaleStops.Last().Color;
        }
        
        public void Add(ColorScaleStop colorScaleStop)
        {
            _colorScaleStops.Add(colorScaleStop);
            _colorScaleStops.Sort();
        }

        public void Clear()
        {
            _colorScaleStops.Clear();
        }

        public List<ColorScaleStop> GetColorScaleStops()
        {
            return _colorScaleStops;
        }
    }
}