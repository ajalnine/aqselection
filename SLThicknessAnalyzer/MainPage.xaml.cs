﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AQConstructorsLibrary;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using System.Windows.Graphics;
using AQAnalysisLibrary;


namespace SLThicknessAnalyzer
{
    public partial class MainPage : IAQModule
    {
        public enum ModuleStates
        {
            UserReady,
            ApplicationBusy,
            Error
        };

        private StateDispatcher<ModuleStates> _moduleStateDispatcher;
        private UserCommand _viewDataUserCommand;
        private UserCommand _precheckDataUserCommand;
        private UserCommand _viewSingleRollCommand;
        private int _rollCount;
        private DataAnalysis _attachedDataAnalysis;
        private List<SLDataTable> _currentRollInfo ;
        private DateTime _currentRollDateKey;
        private readonly AQModuleDescription _aqmd;
        private ThicknessReportTypes CurrentReportMode 
        {
            get
            {
                if (ReportSelector?.SelectedItem == null) return ThicknessReportTypes.Unknown;
                string modeName = ((TabItem) ReportSelector.SelectedItem).Name;
                ThicknessReportTypes reportMode;
                if (!Enum.TryParse(modeName, true, out reportMode)) throw new Exception("Ошибка имени отчета");
                return reportMode;
            }
        }

        #region Dependency properties

        public InteractiveParameters CurrentParameters
        {
            get { return (InteractiveParameters)GetValue(CurrentParametersProperty); }
            set { SetValue(CurrentParametersProperty, value); }
        }

        public static readonly DependencyProperty CurrentParametersProperty =
            DependencyProperty.Register("CurrentParameters", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));
        
        #endregion

        private readonly Dictionary<RenderModeReason, string> _capsMessages = new Dictionary<RenderModeReason, string>
            {
                {
                    RenderModeReason.Not3DCapable,
                    "Видеокарта не поддерживает аппаратное ускорение 3D, поэтому отображение карты толщин недоступно."
                },
                {
                    RenderModeReason.SecurityBlocked,
                    "Аппаратное 3D ускорение запрещено настройками безопасности Silverlight."
                },
                {
                    RenderModeReason.TemporarilyUnavailable,
                    "Аппаратное 3D ускорение временно недоступно, возможно другая программа занимает ресурсы видеокарты."
                },
            };

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            InitializeStateDispatcher();
            InitializeUserCommands();
            var caps = GraphicsDeviceManager.Current.RenderModeReason;
            if (caps != RenderModeReason.Normal)
            {
                RollList.IsEnabled = false;
                CapsLabel.Text = _capsMessages[caps];
            }
            ColumnsPresenter.SelectionParameters = new SelectionThicknessParametersList();
        }

        private void InitializeStateDispatcher()
        {
            _moduleStateDispatcher = new StateDispatcher<ModuleStates>();
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.ApplicationBusy,  () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = false; }));
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.UserReady,        () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = true; }));
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.Error,            () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = false; }));
        }

        #region Обработка сигналов IAQModule

        CommandCallBackDelegate _readySignalCallBack;
        private bool _readyForAcceptSignalsProcessed;
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        
        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            commandCallBack?.Invoke(commandType, commandArgument, null);
        }

        public void InitSignaling(CommandCallBackDelegate ccbd) { _readySignalCallBack = ccbd; }
        
        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReadyForAcceptSignals != null && !_readyForAcceptSignalsProcessed)
            {
                ReadyForAcceptSignals.Invoke(this, new ReadyForAcceptSignalsEventArg { CommandCallBack = _readySignalCallBack });
            }
            _readyForAcceptSignalsProcessed = true;
        }

        #endregion

        #region Установка числовых параметров по режиму отчета

        private void ReportSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentReportMode != ThicknessReportTypes.RollList)
            {
                _attachedDataAnalysis?.SetCustomTableDetailViewer(null);
            }
            UpdateCurrentParameters();
        }

        private void ReportSelector_OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateCurrentParameters();
        }

        private void UpdateCurrentParameters()
        {
            if (ReportSelector?.SelectedItem == null) return;
            CurrentParameters = Resources[CurrentReportMode.ToString()] as InteractiveParameters;
        }
        #endregion
        
        #region Состояния интерфейса и исполнение команд

        private void ColumnsPresenter_ConstructionNotCompleted(object o, ConstructionNotCompletedEventArgs e)
        {
            _moduleStateDispatcher.GoState(ModuleStates.ApplicationBusy);
        }

        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            _moduleStateDispatcher.GoState(ModuleStates.UserReady); 
            _precheckDataUserCommand.Execute();
        }

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckReportPossibility())
            {
                _moduleStateDispatcher.GoState(ModuleStates.ApplicationBusy);
                _viewDataUserCommand.Execute();
            }
        }

        private void TableViewSelectionChangedProcessor(object sender, DataAnalysisSelectionChangedEventArgs e)
        {
            if (CurrentReportMode != ThicknessReportTypes.RollList || e.AddedItems.Count != 1) return;
            _attachedDataAnalysis = e.SourceAnalysis;
            var slDataRow = e.AddedItems[0] as SLDataRow;
            var possibleDate = slDataRow?.Row[slDataRow.Row.Count - 1];
            if (possibleDate is DateTime)
            {
                _currentRollDateKey = (DateTime)possibleDate;
                _viewSingleRollCommand.Execute();
            }
        }
        #endregion

        private bool CheckReportPossibility()
        {
            if (CurrentReportMode == ThicknessReportTypes.Accuracy)
            {
                var strips = CurrentParameters.GetValue("Количество штрипсов, шт");
                var maxCount = 1100 - strips * 100;
                if (maxCount >= _rollCount) return true;
                
                var mbr =
                    MessageBox.Show(
                        $"Рекомендуемое число рулонов для обработки - не более {maxCount}. При расчете по {_rollCount} рулонам возможно аварийное прекращение работы системы. Продолжить?", "Внимание", MessageBoxButton.OKCancel);
                return mbr == MessageBoxResult.OK;
            }
            return true;
        }

        
    }

    #region Конвертеры

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    #endregion
}
