﻿using System;
using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;

namespace SLThicknessAnalyzer
{
    public class SelectionThicknessParametersList : SelectionParametersList
    {
        public SelectionThicknessParametersList()
        {
            DateField = " F2500_Catalogue.StartDate";

            DetailsJoin = String.Empty;

            ParametersList = new List<SelectionParameterDescription>
                {

                    new SelectionParameterDescription
                        {
                            ID = 9,
                            Selector = ColumnTypes.Discrete,
                            EnableDetailsJoin = false,
                            Name = "Марка F2500",
                            SearchAvailable = true,
                            SQLTemplate = @"set dateformat ymd 
	                                select distinct Mark as id, Mark as name from F2500_Catalogue 
                                    @PreviousFilter and @AdditionalFilter group by Mark
                                order by Mark
                                ",
                            Field = "Mark",
                            ResultedFilterField = "F2500_Catalogue.Mark",
                            LastOnly = false,
                            IsNumber = false,
                            NumericSelectionAvailable = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                        new SelectionParameterDescription
                        {
                            ID = 0,
                            Selector = ColumnTypes.Discrete,
                            EnableDetailsJoin = false,
                            Name = "Марка сертификата",
                            SearchAvailable = true,
                            SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark 
                                INNER JOIN 
                                (
	                                select distinct MarkID from F2500_Catalogue 
                                    @DetailsJoin
                                    @PreviousFilter group by MarkID
                                ) as t1 on t1.MarkID=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck("Прокатка") + @" AND @AdditionalFilter 
                                order by mark
                                ",
                            Field = "mark",
                            ResultedFilterField = "F2500_Catalogue.MarkID",
                            LastOnly = false,
                            IsNumber = false,
                            NumericSelectionAvailable = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                   new SelectionParameterDescription
                       {
                            ID = 2,
                            Selector = ColumnTypes.Discrete,
                            EnableDetailsJoin = false,
                            Name = "НТД",
                            SearchAvailable = true,
                            SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS N 
                                INNER JOIN 
                                (
	                                select distinct NtdID from F2500_Catalogue 
                                    @DetailsJoin
                                    @PreviousFilter group by NtdID
                                ) as t1 on t1.NtdID=N.id 
                                where @AdditionalFilter 
                                order by ntd
                                ",
                            Field = "ntd",
                            ResultedFilterField = "F2500_Catalogue.NtdID",
                            LastOnly = false,
                            IsNumber = false,
                            NumericSelectionAvailable = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                    new SelectionParameterDescription
                        {
                            ID = 3,
                            Selector = ColumnTypes.Discrete,
                            Name = "Толщина",
                            EnableDetailsJoin = false,
                            SearchAvailable = false,
                            SQLTemplate =
                                @"set dateformat ymd select distinct Thickness as id, Thickness as name from F2500_Catalogue @PreviousFilter group by Thickness having Thickness is not null order by Thickness",
                            Field = "Thickness",
                            ResultedFilterField = "F2500_Catalogue.Thickness",
                            IsNumber = true,
                            NumericSelectionAvailable = true,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = true
                        },

                    new SelectionParameterDescription
                        {
                            ID = 4,
                            Selector = ColumnTypes.Discrete,
                            Name = "Ширина",
                            EnableDetailsJoin = false,
                            SearchAvailable = false,
                            SQLTemplate =
                                @"set dateformat ymd select distinct Width as id, Width as name from F2500_Catalogue @PreviousFilter group by Width having Width is not null order by Width",
                            Field = "Width",
                            ResultedFilterField = "F2500_Catalogue.Width",
                            IsNumber = true,
                            NumericSelectionAvailable = true,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = true
                        },

                    new SelectionParameterDescription
                        {
                            ID = 5,
                            Selector = ColumnTypes.Discrete,
                            Name = "Цех",
                            EnableDetailsJoin = false,
                            SearchAvailable = false,
                            SQLTemplate =
                                @"set dateformat ymd select distinct melttype as id, melttype as name from F2500_Catalogue @PreviousFilter  and  @AdditionalFilter group by melttype having melttype is not null",
                            Field = "F2500_Catalogue.melttype",
                            ResultedFilterField = "melttype",
                            IsNumber = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                    new SelectionParameterDescription
                        {
                            ID = 6,
                            Selector = ColumnTypes.Discrete,
                            Name = "Способ разливки",
                            EnableDetailsJoin = false,
                            SearchAvailable = false,
                            SQLTemplate =
                                @"set dateformat ymd select distinct onrs as id, onrs as name from F2500_Catalogue @PreviousFilter  and  @AdditionalFilter group by onrs having onrs is not null",
                            Field = "F2500_Catalogue.onrs",
                            ResultedFilterField = "onrs",
                            IsNumber = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                    new SelectionParameterDescription
                        {
                            ID = 7,
                            Selector = ColumnTypes.MeltList,
                            Name = "Номер плавки",
                            EnableDetailsJoin = false,
                            SearchAvailable = true,
                            SQLTemplate = @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct melt , max(melttype) as melttype from F2500_Catalogue @PreviousFilter @AdditionalFilter group by melt having melt is not null  and max(melttype) is not null  order by max(melttype), melt

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null

                                drop table #tquery
                                ",
                            Field = "F2500_Catalogue.melt",
                            ResultedFilterField = "melt",
                            IsNumber = false,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = false
                        },

                    new SelectionParameterDescription
                        {
                            ID = 8,
                            Selector = ColumnTypes.Discrete,
                            Name = "№ рулона",
                            EnableDetailsJoin = false,
                            SearchAvailable = false,
                            SQLTemplate =
                                @"set dateformat ymd select distinct RollNumber as id, RollNumber as name from F2500_Catalogue @DetailsJoin @PreviousFilter group by RollNumber having RollNumber is not null order by F2500_Catalogue.RollNumber",
                            Field = "RollNumber",
                            ResultedFilterField = "F2500_Catalogue.RollNumber",
                            IsNumber = true,
                            NumericSelectionAvailable = true,
                            IncludeInDescription = true,
                            IgnoreFilterForChemistry = true
                        }
                };
        }
    }
}
