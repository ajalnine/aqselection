﻿using System;
using System.Collections.Generic;
using System.Windows;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQAnalysisLibrary;
using AQStepFactoryLibrary;

namespace SLAnalysis
{
    public partial class MainPage : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd)
        {
            _readySignalCallBack = ccbd;
        }

        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;

        private bool _readyForAcceptSignalsProcessed;
        private CommandCallBackDelegate _readySignalCallBack;
        private readonly AQModuleDescription _aqmd;
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription
            {
                Instance = this,
                ModuleType = GetType(),
                Name = GetType().Name,
                URL = "SLAnalysis.MainPage"
            };
            MainDataAnalysis.ParentAqmd = _aqmd;
            Loader.TabControlSizeChanged += Loader_TabControlSizeChanged;
        }

        private void Loader_TabControlSizeChanged(object o, TabSizeChangedEventArgs e)
        {
            Cont.Width = (e.NewSize.Width > 10) ? e.NewSize.Width - 10 : 0;
            Cont.Height = (e.NewSize.Height > 30) ? e.NewSize.Height - 30 : 0;
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument,
            CommandCallBackDelegate commandCallBack)
        {

            var ca = commandArgument as Dictionary<string, object>;
            switch (commandType)
            {
                case Command.PasteToAnalysis:
                    if (ca != null && ca.ContainsKey("Text"))
                    {
                        MainDataAnalysis.LoadFromClipboard(ca["Text"].ToString());
                    }
                    break;

                case Command.InsertTablesToAnalysis:
                case Command.ReopenInAnalisys:
                    List<SLDataTable> sourceData = null;
                    List<Step> sourceSteps = null;
                    Step operation = null;
                    DateTime? from = null, to = null;
                    var description = string.Empty;
                    bool reset = false;
                    if (ca != null)
                    {
                        if (ca.ContainsKey("From")) from = (DateTime)ca["From"];
                        if (ca.ContainsKey("To")) to = (DateTime)ca["To"];
                        if (ca.ContainsKey("SourceData")) sourceData = (List<SLDataTable>)ca["SourceData"];
                        if (ca.ContainsKey("Calculation")) sourceSteps = (List<Step>)ca["Calculation"];
                        if (ca.ContainsKey("Description")) description = (string)ca["Description"];
                        if (ca.ContainsKey("Operation")) operation = (Step)ca["Operation"];
                        if (ca.ContainsKey("TableViewSelectionChanged"))MainDataAnalysis.TableViewSelectionChanged += (DataAnalysisSelectionChangedDelegate)ca["TableViewSelectionChanged"];
                        if (ca.ContainsKey("EmptyTableSelectedProcessor")) MainDataAnalysis.EmptyTableSelected += (EmptyTableSelectedDelegate)ca["EmptyTableSelectedProcessor"];
                        if (ca.ContainsKey("ResetAnalysis")) reset = (bool)ca["ResetAnalysis"];
                    }
                    if (reset)
                    {
                        MainDataAnalysis.Reset();
                    }
                    MainDataAnalysis.LoadDataWithCalculation(sourceData, sourceSteps, operation, from, to, description); 
                    break;

                case Command.StartNewAnalysis:
                    MainDataAnalysis.StartEmptyAnalisys();
                    break;    

                case Command.SendTableToAnalysis:
                    SLDataTable table;
                    if (ca != null && ca.ContainsKey("Table")) table = (SLDataTable)ca["Table"];
                    else break;
                    var step = StepFactory.CreateTable(table);
                    MainDataAnalysis.LoadDataWithCalculation(new List<SLDataTable>{table}, new List<Step>(step), null, null, null, null); 
                    break;
            }
        }

        private void Cont_OnLoaded(object sender, RoutedEventArgs e)
        {
            Cont.Width = Loader.MainTabSize.Width - 10;
            Cont.Height = Loader.MainTabSize.Height - 30;

            if (_readyForAcceptSignalsProcessed) return;
            if (ReadyForAcceptSignals != null) ReadyForAcceptSignals.Invoke(this, new ReadyForAcceptSignalsEventArg { CommandCallBack = _readySignalCallBack });
            _readyForAcceptSignalsProcessed = true;
        }
    }
}