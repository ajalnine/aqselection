﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        private void ShowPopup(string axisTag, FixedAxis axis)
        {
            FixedAxisRangeEditor.IsOpen = true;
            FixedAxisRangeEditor.Tag = axisTag;
            AxisName.Text = string.Format("Параметры оси {0}:", axisTag);
            FixedAxisRangeEditor.VerticalOffset = _mousePosition.Y;
            FixedAxisRangeEditor.HorizontalOffset = _mousePosition.X;
            FixedAxisRangeEditor.DataContext = axis;
        }

        #region Объединение фиксации из UI и автоматически рассчитанных значений из графика

        void chart_AxisRangeReady(object o, Chart.AxisRangeReadyEventArgs e)
        {
            RenewFixedAxises(e);
        }

        private void RenewFixedAxises(Chart.AxisRangeReadyEventArgs e)
        {
            if (_currentFixedAxises == null) _currentFixedAxises = new FixedAxises();
            if (_currentFixedAxises.FixedAxisCollection == null) _currentFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X"), e.BottomAxisRange, "X");
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X2"), e.TopAxisRange, "X2");
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y"), e.LeftAxisRange, "Y");
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y2"), e.RightAxisRange, "Y2");
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Z"), e.ZAxisRange, "Z");
            MoveAxisRangeDataToFixedAxis(_currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "W"), e.WAxisRange, "W");
        }

        private void MoveAxisRangeDataToFixedAxis(FixedAxis axis, AxisRange ar, string name)
        {
            if (!(ar.DisplayMinimum is double) || !(ar.DisplayMaximum is double)) return; //Заглушка для пропуска DateTime

            if (axis == null)
            {
                axis = new FixedAxis {Axis = name};
                _currentFixedAxises.FixedAxisCollection.Add(axis);
            }
            if (!axis.AxisMinFixed && !axis.AxisMinLocked) axis.AxisMin = (double?)ar.DisplayMinimum;
            if (!axis.AxisMaxFixed && !axis.AxisMaxLocked) axis.AxisMax = (double?)ar.DisplayMaximum;
            if (!axis.StepFixed && !axis.StepLocked) axis.Step = ar.Step;
        }
        #endregion

        #region Объединение фиксации из параметров операции и UI
        private void SetFixedAxis(Chart chart)
        {
            chart.AxisRangeReady += chart_AxisRangeReady;
            MergeFixedAxis(chart.FixedAxises); //Первоначальные ограничения графика объединяются с ограничениями из кэша
            chart.FixedAxises = _currentFixedAxises;
        }

        private void MergeFixedAxis(FixedAxises chartSpecificAxises)
        {
            if (_currentFixedAxises == null) _currentFixedAxises = new FixedAxises();
            if (_currentFixedAxises.FixedAxisCollection == null) _currentFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MergeFixedAxisData(chartSpecificAxises, "X");
            MergeFixedAxisData(chartSpecificAxises, "X2");
            MergeFixedAxisData(chartSpecificAxises, "Y");
            MergeFixedAxisData(chartSpecificAxises, "Y2");
            MergeFixedAxisData(chartSpecificAxises, "Z");
            MergeFixedAxisData(chartSpecificAxises, "W");
        }
        private void MergeFixedAxisData(FixedAxises chartSpecificAxises, string axisname)
        {
            if (chartSpecificAxises == null) return;
            var source = chartSpecificAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == axisname);
            var destination = _currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == axisname);

            if (source != null)
            {
                if (destination != null)
                {
                    if (source.AxisMinLocked)
                    {
                        destination.AxisMin = source.AxisMin;
                        destination.AxisMinFixed = source.AxisMinFixed;
                    }
                    if (source.AxisMaxLocked)
                    {
                        destination.AxisMax = source.AxisMax;
                        destination.AxisMaxFixed = source.AxisMaxFixed;
                    }
                    if (source.StepLocked)
                    {
                        destination.Step = source.Step;
                        destination.StepFixed = source.StepFixed;
                    }
                    destination.AxisMinLocked = source.AxisMinLocked;
                    destination.AxisMaxLocked = source.AxisMaxLocked;
                    destination.StepLocked = source.StepLocked;
                }
                else
                {
                    _currentFixedAxises.FixedAxisCollection.Add(source);
                }
            }
        }
        #endregion

        #region События интерфейса
        void irc_AxisClicked(object o, AxisEventArgs e)
        {
            var axisTag = e.Tag.ToString(CultureInfo.InvariantCulture);

            if (_currentFixedAxises == null || _currentFixedAxises.FixedAxisCollection == null ||
                _currentFixedAxises.FixedAxisCollection.All(a => a.Axis != axisTag)) return;
            var fixedAxis = _currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == axisTag);
            if (fixedAxis == null || fixedAxis.AxisMin == null || fixedAxis.AxisMax == null) return;

            ShowPopup(axisTag, fixedAxis);
        }
        
        private void FixedAxisOK_OnClick(object sender, RoutedEventArgs e)
        {
            AxisMinTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            AxisMaxTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            StepTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            var fixedAxis = _currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == FixedAxisRangeEditor.Tag.ToString());
            if (fixedAxis!=null && fixedAxis.IsValid)
            {
                MakeReportChart();
                FixedAxisRangeEditor.IsOpen = false;
            }
        }
        private void ChartPanel_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            FixedAxisRangeEditor.IsOpen = false;
        }
        private void Fixed_OnBindingValidationError(object sender, ValidationErrorEventArgs e)
        {
            var fixedAxis = _currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == FixedAxisRangeEditor.Tag.ToString());
            if (fixedAxis != null) fixedAxis.IsValid = false;
            FixedAxisRangeEditor.IsOpen = true;
        }
        private void ResetFixationButton_OnClick(object sender, RoutedEventArgs e)
        {
            _currentFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MakeReportChart();
            FixedAxisRangeEditor.IsOpen = false;
        }
        #endregion

        private void FixedTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var t = sender as TextBox;
            if (t != null && (!t.Text.EndsWith(".") && t.Text != "0" && !t.Text.EndsWith(",")))
            {
                var be = t.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
            }
        }
    }
}