﻿using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public static class SLDataTableExtentions
    {
        public static SLDataTable GetClone(this SLDataTable sourceTable)
        {
            var clone = new SLDataTable
            {
                Tag = sourceTable.Tag,
                TableName = sourceTable.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                CustomProperties = sourceTable.CustomProperties,
                SelectionString = sourceTable.SelectionString,
                Table = new List<SLDataRow>()
            };
            if (sourceTable.DataTypes != null) clone.DataTypes.AddRange(sourceTable.DataTypes);
            if (sourceTable.ColumnNames != null) clone.ColumnNames.AddRange(sourceTable.ColumnNames);
            if (sourceTable.ColumnTags != null) clone.ColumnTags.AddRange(sourceTable.ColumnTags);
            foreach (var row in sourceTable.Table)
            {
                var r = new SLDataRow { Row = new List<object>() };
                if (row.Row!=null)r.Row.AddRange(row.Row);
                clone.Table.Add(r);
            }
            return clone;
        }
        
        public static void FillCell(this SLDataTable table, string columnName, object value, string dataType, int rowIndex)
        {
            var index = GetColumn(table, columnName, dataType);
            table.Table[rowIndex].Row[index] = value;
        }
        public static void FillCell(this SLDataTable table, string columnName, object value, string dataType, SLDataRow rowToFill)
        {
            var index = GetColumn(table, columnName, dataType);
            rowToFill.Row[index] = value;
        }
        
        public static void FillCells(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, int startRow, int endRow)
        {
            foreach (var index in columnList.Select(column => GetColumn(table, column, commonDataType)))
            {
                for (var i = startRow; i <= endRow; i++)
                {
                    table.Table[i].Row[index] = value;
                }
            }
        }

        public static void FillCells(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, List<SLDataRow> rowsToFill)
        {
            foreach (var index in columnList.Select(column => GetColumn(table, column, commonDataType)))
            {
                foreach (var r in rowsToFill)
                {
                    r.Row[index] = value;
                }
            }
        }

        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType)
        {
            var index = GetColumn(table, columnName, dataType);
                foreach (var row in table.Table)
                {
                    row.Row[index] = value;
                }
        }
        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType, int startRow, int endRow)
        {
            var index = GetColumn(table, columnName, dataType);
            for (var i=startRow; i<= endRow; i++)
            {
                table.Table[i].Row[index] = value;
            }
        }
        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType, List<SLDataRow> rowsToFill)
        {
            var index = GetColumn(table, columnName, dataType);
            foreach (var r in rowsToFill)
            {
                r.Row[index] = value;
            }
        }
        public static void FillRow(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, int rowIndex)
        {
            foreach (var column in columnList)
            {
                var index = GetColumn(table, column, commonDataType);
                table.Table[rowIndex].Row[index] = value;
            }
        }
        public static void FillRow(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, SLDataRow rowToFill)
        {
            foreach (var column in columnList)
            {
                var index = GetColumn(table, column, commonDataType);
                rowToFill.Row[index] = value;
            }
        }
        private static int GetColumn(this SLDataTable table, string columnName, string dateType)
        {
            if (table.ColumnNames.Contains(columnName)) return table.ColumnNames.IndexOf(columnName);
            table.ColumnNames.Add(columnName);
            table.DataTypes.Add(dateType);
            if (table.ColumnTags != null) table.ColumnTags.Add(null);
            foreach (var r in table.Table)
            {
                switch (dateType)
                {
                    case "System.String":
                    case "String":
                        r.Row.Add(string.Empty);
                        break;
                    default:
                        r.Row.Add(null);
                        break;
                }
            }
            return table.ColumnNames.Count - 1;
        }
        public static void DeleteColumn(this SLDataTable table, string  columnName)
        {
            var index = table.ColumnNames.IndexOf(columnName);
            if (index < 0) return;
            table.ColumnNames.RemoveAt(index);
            table.DataTypes.RemoveAt(index);
            if (table.ColumnTags != null && table.ColumnTags.Count > index)
            {
                table.ColumnTags.RemoveAt(index);
            }
            foreach (var r in table.Table) r.Row.RemoveAt(index);
        }
    }
}
