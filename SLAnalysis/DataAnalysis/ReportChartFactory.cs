﻿using System.Collections.Generic;
using AQControlsLibrary.ReportChart;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public partial class DataAnalysis
    {
        private IReportChart GetReportChartByUI()
        {
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.ChartIsDynamic:
                    return new ReportChartDynamic(new ReportChartDynamicParameters
                    {
                        Table = _selectedTable.TableName,
                        Fields = new List<string> {MainFieldsSelector.YField},
                        DateField = MainFieldsSelector.XField,
                        RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                        SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                        UserLimits = MainRangeModeSelector.GetUserLimits(),
                        ColorField = MainFieldsSelector.ZField,
                        MarkerSizeField = MainFieldsSelector.WField,
                        LabelField = MainFieldsSelector.VField,
                        ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                        MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                        MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                        LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                        FixedAxisesDescription = _currentFixedAxises
                    });

                case ReportChartType.ChartIsCard:
                    var cardType = MainCardTypeSelector.GetCurrentCardType();
                    switch (cardType)
                    {
                        case ReportChartCardType.CardTypeIsTrends:
                        case ReportChartCardType.CardTypeIsGraphics:
                            return new ReportChartCard(new ReportChartCardParameters
                                {
                                    CardType = cardType,
                                    GroupField = MainFieldsSelector.ZField,
                                    DateField = MainFieldsSelector.VField,
                                    Series = ReportChartSeriesType.None,
                                    Table = _selectedTable.TableName,
                                    Fields = new List<string> { MainFieldsSelector.YField, MainFieldsSelector.XField },
                                    SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                                    RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                                    UserLimits = MainRangeModeSelector.GetUserLimits(),
                                    ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                                    MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                                    MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                                    LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                                    FixedAxisesDescription = _currentFixedAxises
                                });

                        default:
                            return new ReportChartCard(new ReportChartCardParameters
                                {
                                    CardType = cardType,
                                    GroupField = MainFieldsSelector.ZField,
                                    DateField = MainFieldsSelector.VField,
                                    Series = MainSeriesCheckSelector.GetCurrentChartSeriesType(),
                                    Table = _selectedTable.TableName,
                                    SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                                    Fields = new List<string> { MainFieldsSelector.YField },
                                    RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                                    UserLimits = MainRangeModeSelector.GetUserLimits(),
                                    ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                                    MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                                    MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                                    LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                                    FixedAxisesDescription = _currentFixedAxises
                                });
                    }

                case ReportChartType.ChartIsFreq:
                    var p =new ReportChartHistogramParameters
                        {
                            Table = _selectedTable.TableName,
                            Fields = new List<string> { MainFieldsSelector.YField, MainFieldsSelector.XField },
                            YMode = MainFrequencyParameterSelector.GetCurrentYMode(),
                            AlignMode = MainFrequencyParameterSelector.GetCurrentAlignMode(),
                            DetailLevel = MainFrequencyParameterSelector.GetCurrentDetailLevel(),
                            Step = MainFrequencyParameterSelector.GetCurrentStep(),
                            ColorField = MainFieldsSelector.ZField,
                            RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                            UserLimits = MainRangeModeSelector.GetUserLimits(),
                            ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                            MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                            MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                            UserAlignValue = MainFrequencyParameterSelector.GetCurrentUserAlignValue(),
                            FixedAxisesDescription = _currentFixedAxises,
                            LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                            FitMode = MainFitSelector.GetCurrentFit(),
                            HistogramType = MainHistogramTypeSelector.GetCurrentHistogramType(),
                            Aggregate = MainAggregateSelector.GetCurrentAggregate()
                        };
                    switch (p.HistogramType)
                    {
                        case ReportChartHistogramType.Pareto:
                            return new ReportChartPareto(p);
                        case ReportChartHistogramType.Stratification:
                            return new ReportChartStratification(p);
                        default:
                            return new ReportChartFreq(p);
                    }
                        
                case ReportChartType.ChartIsGroupCard:
                    return new ReportChartGroups(new ReportChartGroupsParameters
                        {
                            Table = _selectedTable.TableName,
                            Fields = new List<string> {MainFieldsSelector.YField},
                            GroupField = MainFieldsSelector.ZField,
                            Mode = MainGroupModeSelector.GetCurrentGroupMode(),
                            ShowOutliers = MainGroupParametersSelector.GetCurrentOutliers(),
                            OnlySignificant = false,
                            RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                            Sorted = MainGroupParametersSelector.GetCurrentSorted(),
                            UserLimits = MainRangeModeSelector.GetUserLimits(),
                            ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                            MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                            MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                            Center = MainGroupParametersSelector.GetCurrentGroupCenter(),
                            LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                            FixedAxisesDescription = _currentFixedAxises
                        });

                case ReportChartType.ChartIsScatterPlot:
                    return new ReportChartScatterPlot(new ReportChartScatterPlotParameters
                        {
                            Table = _selectedTable.TableName,
                            Fields = new List<string> {MainFieldsSelector.YField},
                            FieldX = MainFieldsSelector.XField,
                            RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                            SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                            UserLimits = MainRangeModeSelector.GetUserLimits(),
                            ColorField = MainFieldsSelector.ZField,
                            MarkerSizeField = MainFieldsSelector.WField,
                            LabelField = MainFieldsSelector.VField,
                            ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                            MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                            MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                            LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                            FixedAxisesDescription = _currentFixedAxises
                        });

                case ReportChartType.ChartIsPP:
                    return new ReportChartProbabilityPlot(new ReportChartProbabilityPlotParameters
                    {
                        Table = _selectedTable.TableName,
                        Fields = new List<string> { MainFieldsSelector.YField },
                        ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                        MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                        MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                        LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode()
                    });
                default:
                    return null;
            }
        }
    }
}