﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQDataAnalysis.ReportChart;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        private string _currentChartDescription = string.Empty;
        private string _currentShortChartDescription = string.Empty;
        
        private void MakeReportChart()
        {
            if (_dataTables == null) { SetNoDataMessage(); return; }

            var irc = GetReportChartByUI();
            if (irc == null) { SetNoDataMessage(); return; }

            SetDescriptions(irc);

            _currentChartStep = irc.GetStep();
            var c = irc.BuildCharts(_dataTables);
            if (c == null) SetNoDataMessage();
            else
            {
                irc.ReportChartChanged += ReportChartChanged;
                irc.UiReflectChanges += IrcUiReflectChanges;
                irc.AxisClicked += irc_AxisClicked;
                irc.ReportInteractiveRename += irc_ReportInteractiveRename;
                ChartPanel.Children.Clear();

                foreach (var chart in c)
                {
                    SetFixedAxis(chart);
                    ChartPanel.Children.Add(chart);
                    chart.MakeChart();
                }
            }
        }

        private void SetDescriptions(IReportChart irc)
        {
            _currentShortChartDescription = Description;
            _currentChartDescription = irc.GetChartDescription();
            AppendPeriodToDescription();
        }

        private void SetNoDataMessage()
        {
            ChartPanel.Children.Clear();

            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 50,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = "Нет данных",
                TextAlignment = TextAlignment.Center,
                Margin = new Thickness(0, 150, 0, 100)
            };
            ChartPanel.Children.Add(tb);
        }

        private void AppendPeriodToDescription()
        {
            if (!_from.HasValue || !_to.HasValue) return;
            var periodDescription = string.Format("; Период: {0} - {1}", _from.Value.ToShortDateString(), _to.Value.ToShortDateString());
            _currentShortChartDescription += periodDescription;
            _currentChartDescription += periodDescription;
        }
    }
}