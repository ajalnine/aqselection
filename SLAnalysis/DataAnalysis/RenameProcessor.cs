﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary.ReportChart;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public static class RenameProcessor
    {
        public static ReportInteractiveRenameEventArgs DefaultRenameProcess(ChartRenameEventArgs e, SLDataTable selectedTable)
        {
            var re = new ReportInteractiveRenameEventArgs { RenameOperations = new List<ReportRenameOperation>() };
            foreach (var ro in e.RenameOperations)
            {
                var rename = new ReportRenameOperation { OldName = ro.OldName, NewName = ro.NewName, Table = selectedTable.TableName };

                switch (ro.ChartItemType)
                {
                    case ChartRenameItemType.Table:
                        if (rename.OldName != selectedTable.TableName) continue;
                            rename.RenameTarget = ReportRenameTarget.Table;
                        break;
                    case ChartRenameItemType.Cathegory:
                        rename.RenameTarget = ReportRenameTarget.Cathegory;
                        break;
                    case ChartRenameItemType.Field:
                        switch (ro.TableName)
                        {
                            case "Пределы":
                                rename.RenameTarget = ReportRenameTarget.Range;
                                break;
                            default:
                                if (!selectedTable.ColumnNames.Contains(rename.OldName))continue;
                                rename.RenameTarget = ReportRenameTarget.Field;
                                break;
                        }
                        break;
                }
                re.RenameOperations.Add(rename);
            }
            var duplicateCleaned = new List<ReportRenameOperation>();
            foreach (var ro in re.RenameOperations.Where(ro => !duplicateCleaned.Any(a => a.OldName == ro.OldName && a.RenameTarget == ro.RenameTarget && a.Table == ro.Table)))
            {
                duplicateCleaned.Add(ro);
            }
            re.RenameOperations = duplicateCleaned;
            return re;
        }
    }
}
       