﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public partial class DataAnalysis
    {
        private List<SLDataTable> _appendedTables;
        private void MainDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var extendedEventArgs = new DataAnalysisSelectionChangedEventArgs(_selectedTable, this, e.RemovedItems, e.AddedItems);
            if (TableViewSelectionChanged != null) TableViewSelectionChanged.Invoke(this, extendedEventArgs);
        }

        public void SetCustomTableDetailViewer(UIElement c)
        {
            if (CustomTableDetailsPanel.Children.Any())CustomTableDetailsPanel.Children.Clear();
            if (c == null)
            {
                CustomTableDetailsPanel.Visibility = Visibility.Collapsed;
                return;
            }
            CustomTableDetailsPanel.Visibility = Visibility.Visible;
            CustomTableDetailsPanel.Children.Add(c);
        }

        public UIElement GetCustomTableDetailViewer()
        {
            return CustomTableDetailsPanel.Children.FirstOrDefault();
        }

        public void AppendTablesToSources(List<SLDataTable> newTables)
        {
            foreach (var slDataTable in newTables)
            {
                _dataTables.Add(slDataTable);
            }
            _appendedTables = newTables;
            SetupTableView(false, null);
        }

        public void ResetAppendedTables()
        {
            if (_appendedTables == null) return;
            foreach (var appendedTable in _appendedTables)
            {
                _dataTables.Remove(appendedTable);
            }
            _appendedTables = null;
            SetupTableView(false, null);
        }
    }

    public class DataAnalysisSelectionChangedEventArgs : SelectionChangedEventArgs
    {
        public SLDataTable SourceTable { get; set; }

        public DataAnalysis SourceAnalysis { get; set; }
        public DataAnalysisSelectionChangedEventArgs(SLDataTable sourceTable, DataAnalysis sourceAnalysis, IList removedItems, IList addedItems) : base(removedItems, addedItems)
        {
            SourceTable = sourceTable;
            SourceAnalysis = sourceAnalysis;

        }
    }
}