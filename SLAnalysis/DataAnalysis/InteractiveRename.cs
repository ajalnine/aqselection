﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQDataAnalysis.ReportChart;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        void irc_ReportInteractiveRename(object reportchart, ReportInteractiveRenameEventArgs e)
        {
            if (e.RenameOperations == null || e.RenameOperations.Count == 0) return;
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Range)) MainRangeModeSelector.RenameRange(e.RenameOperations);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Field)) RenameFieldsInSelectedTable(e);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Table)) RenameTable(e);
        }

        private void RenameFieldsInSelectedTable(ReportInteractiveRenameEventArgs reportInteractiveRenameEventArgs)
        {
            var renames = new Dictionary<string, string>();
            foreach (var ro in reportInteractiveRenameEventArgs.RenameOperations
                .Where(a => a.RenameTarget == ReportRenameTarget.Field && a.Table == _selectedTable.TableName))
            {
                while (_selectedTable.ColumnNames.Select(a => a.Trim().ToLower()).Contains(ro.NewName.Trim().ToLower()))ro.NewName += "_";
                var renameFieldIndex =
                    _selectedTable.ColumnNames.Select(a => a.Trim().ToLower())
                        .ToList()
                        .IndexOf(ro.OldName.Trim().ToLower());
                if (renameFieldIndex > -1)
                {
                    var oldname = _selectedTable.ColumnNames[renameFieldIndex];
                    renames.Add(ro.NewName, oldname);
                    _selectedTable.ColumnNames[renameFieldIndex] = ro.NewName;
                }
            }
            _sourceSteps.AddRange(StepFactory.CreateRenameFields(_selectedTable, renames));
            MakeDataSetStructure();
            MainFieldsSelector.RenameFields(reportInteractiveRenameEventArgs.RenameOperations);
        }

        private void RenameTable(ReportInteractiveRenameEventArgs e)
        {
            var tableRename = e.RenameOperations.SingleOrDefault(a => a.RenameTarget == ReportRenameTarget.Table);
            if (tableRename == null) return;
            if (_selectedTable.TableName == tableRename.OldName)
            {
                while (_dataTables.Select(a => a.TableName.Trim().ToLower()).Contains(tableRename.NewName.Trim().ToLower())) tableRename.NewName += "_";
                _sourceSteps.AddRange(StepFactory.CreateRenameTable(_dataTables, _selectedTable.TableName, tableRename.NewName));
                _selectedTable.TableName = tableRename.NewName;
            }
            ChangeTableName(tableRename.OldName, tableRename.NewName);
            if (ViewType.SelectedIndex == 0) return;
            MakeReportChart();
        }

        private IEnumerable<Step> AppendReorderTableStep()
        {
            return StepFactory.CreateReorderTables(GetOrderedNames());
        }
    }
}
       