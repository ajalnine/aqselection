﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        #region Импорт таблицы из буфера

        private static int _pasteCounter = 1;

        private void PasteButton_Click(object sender, RoutedEventArgs e)
        {
            var imported = GetSLDataTableFromClipboard();
            if (imported != null)
            {
                if (_dataTables==null)_dataTables = new List<SLDataTable>();
                if (_sourceSteps==null)_sourceSteps = new List<Step>();
                while (_dataTables.Select(a => a.TableName).Contains(imported.TableName)) imported.TableName += "_";
                _dataTables.Add(imported);
                _sourceSteps.AddRange(StepFactory.CreateTable(imported));
                _pasteCounter++;
            }
            else return;
            if (DataLoaded != null) DataLoaded(this, new TaskStateEventArgs(_currentTaskState));
            SetupTableView(true, imported.TableName);
            if (DataProcessed != null) DataProcessed(this, new TaskStateEventArgs(_currentTaskState));
        }

        private SLDataTable GetSLDataTableFromClipboard()
        {
            if (Clipboard.ContainsText())
            {
                return GetSLDataTableFromClipboard(Clipboard.GetText());
            }
            MessageBox.Show("Буфер обмена не содержит данных.", "Ошибка вставки данных", MessageBoxButton.OK);
            return null;
        }

        private SLDataTable GetSLDataTableFromClipboard(string textData)
        {
            if (!string.IsNullOrEmpty(textData))
            {
                var insertedRows = textData.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                var height = insertedRows.Count;
                if (height <= 1)
                {
                    MessageBox.Show("Буфер обмена не содержит табличных данных.", "Ошибка вставки данных",
                    MessageBoxButton.OK);
                    return null;
                }
                var headers = insertedRows[0].Split(new[] { "\t" }, StringSplitOptions.None).ToList();

                if (headers.Any(String.IsNullOrWhiteSpace))
                {
                    MessageBox.Show("Таблица не должна содержать пустые заголовки", "Ошибка вставки данных",
                        MessageBoxButton.OK);
                    return null;
                }
                if (headers.Count != headers.Distinct().Count())
                {
                    MessageBox.Show("Таблица не должна содержать повторяющиеся заголовки", "Ошибка вставки данных",
                        MessageBoxButton.OK);
                    return null;
                }

                var data = new List<Row>();
                var dataTypes = new List<string>();

                foreach (var row in insertedRows.Skip(1))
                {
                    var r = new Row { RowData = new List<string>() };
                    r.RowData.AddRange(row.Split(new[] { "\t" }, StringSplitOptions.None));
                    data.Add(r);
                }

                for (int i = 0; i < headers.Count; i++)
                {
                    dataTypes.Add(GetDataTypeForColumn(data, i));
                }
                var sldt = ConvertStringsToSLDT(data, headers, dataTypes);
                return sldt;
            }
            MessageBox.Show("Буфер обмена не содержит данных.", "Ошибка вставки данных", MessageBoxButton.OK);
            return null;
        }

        private SLDataTable ConvertStringsToSLDT(IEnumerable<Row> data, List<string> headers, List<string> dataTypes)
        {
            if (data == null) throw new ArgumentNullException("data");
            var destination = new SLDataTable
            {
                ColumnNames = headers,
                DataTypes = dataTypes,
                TableName = "Таблица" + _pasteCounter,
                Tag = null,
                SelectionString = String.Empty,
                Table = new List<SLDataRow>()
            };

            foreach (var t in data)
            {
                var sldr = new SLDataRow{Row = new List<object>()};
                destination.Table.Add(sldr);

                for (var i = 0; i < headers.Count; i++)
                {
                    var value = t.RowData[i].Trim();
                    switch (dataTypes[i])
                    {
                        case "Double":
                            sldr.Row.Add(((value == string.Empty) ? DBNull.Value : (object)Double.Parse(value.Replace('.', NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToCharArray()[0]))));
                            break;
                        case "DateTime":
                            sldr.Row.Add(((value == string.Empty) ? DBNull.Value : (object)DateTime.Parse(value)));
                            break;
                        case "Boolean":
                            sldr.Row.Add(((value == string.Empty) ? DBNull.Value : (object)Boolean.Parse(value)));
                            break;
                        case "Int32":
                            sldr.Row.Add(((value == string.Empty) ? DBNull.Value : (object)Int32.Parse(value)));
                            break;
                        default:
                            sldr.Row.Add(((value == string.Empty) ? DBNull.Value : (object)value));
                            break;
                    }
                }
            }
            return destination;
        }

        private string GetDataTypeForColumn(IList<Row> data, int j)
        {
            if (data == null) throw new ArgumentNullException("data");
            var recordsNumber = data.Count;
            var isDouble = true;
            var isBoolean = true;
            var isDateTime = true;
            var isInt32 = true;
            var isTimeSpan = true;
            var hasValues = false;

            for (var i = 1; i < recordsNumber; i++)
            {
                var value = data[i].RowData[j];
                if (string.IsNullOrEmpty(value)) continue;
                if (isDouble)
                {
                    Double doubleResult;
                    isDouble = Double.TryParse(value, out doubleResult) || Double.TryParse(value.Replace('.', ','), out doubleResult) || Double.TryParse(value.Replace(',', '.'), out doubleResult);
                    hasValues = true;
                }
                if (isDateTime)
                {
                    DateTime dateTimeResult;
                    isDateTime = DateTime.TryParseExact(value, "MMMM yyyy", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime = !isDateTime && DateTime.TryParse(value, out dateTimeResult);
                }
                if (isTimeSpan)
                {
                    TimeSpan timeSpanResult;
                    isTimeSpan = TimeSpan.TryParse(value, out timeSpanResult);
                }
                if (isInt32)
                {
                    Int32 int32Result;
                    isInt32 = Int32.TryParse(value, out int32Result);
                }
                if (isBoolean)
                {
                    Boolean booleanResult;
                    isBoolean = Boolean.TryParse(value, out booleanResult);
                }
            }
            if (!hasValues) return "String";
            if (isDouble) return "Double";
            if (isBoolean) return "Boolean";
            if (isDateTime) return "DateTime";
            if (isTimeSpan) return "TimeSpan";
            if (isInt32) return "Int32";
            return "String";
        }

        public class Row
        {
            public List<string> RowData;
        }

        #endregion
    }
}