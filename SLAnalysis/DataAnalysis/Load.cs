﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        #region Загрузка данных

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof (string), typeof (DataAnalysis),
                                        new PropertyMetadata(String.Empty));

        private Step _currentChartStep;
        private List<SLDataTable> _dataTables;
        private DateTime? _from;
        private bool _hasAttachedCalculation;
        private List<Step> _sourceSteps;
        private DateTime? _to;
        private bool _isSignalProcessingFirstTime;

        public string Description
        {
            get { return (string) GetValue(DescriptionProperty); }
            private set { SetValue(DescriptionProperty, value); }
        }
        
        public void LoadFromClipboard(string text)
        {
            var fromBuffer = GetSLDataTableFromClipboard(text);
            if (fromBuffer == null) return;
            if (_dataTables == null)
            {
                ChartPanel.Children.Clear();
                ResetPreselection();
                Description = string.Empty;
                _dataTables = new List<SLDataTable>();
                _sourceSteps = new List<Step>();
            }
            _dataTables.Add(fromBuffer);
            _sourceSteps.AddRange(StepFactory.CreateTable(fromBuffer));
            _hasAttachedCalculation = true;
            _pasteCounter++;

            if (DataLoaded != null) DataLoaded(this, new TaskStateEventArgs(_currentTaskState));
            SetupTableView(true, fromBuffer.TableName);
            if (DataProcessed != null) DataProcessed(this, new TaskStateEventArgs(_currentTaskState));
        }

        public void StartEmptyAnalisys()
        {
            SetupTableView(true, null);
        }
        
        public void LoadDataWithCalculation(List<SLDataTable> newTables, List<Step> newCalculation, Step operation,
                                             DateTime? from, DateTime? to, string desc)
        {
            if (_dataTables == null)
            {
                ChartPanel.Children.Clear();
                ResetPreselection();
                Description = string.Empty;
                _dataTables = new List<SLDataTable>();
            }
            if (_sourceSteps == null) _sourceSteps = new List<Step>();
            if (from.HasValue) _from = from;
            if (to.HasValue) _to = to;
            if (!String.IsNullOrEmpty(desc)) Description = desc;
            if (operation != null) _currentChartStep = operation;
            
            _hasAttachedCalculation = newCalculation != null || _sourceSteps.Count > 0;
            
            SafeAppendNewData(newTables, newCalculation);
            
            if (_currentChartStep != null)
            {
                ResetPreselection();
                SetUIbyStep(operation);
                _isSignalProcessingFirstTime = true;
            }
            if (DataLoaded != null) DataLoaded(this, new TaskStateEventArgs(_currentTaskState));
            SetupTableView(true, newTables[newTables.Count>1 ? 1:0].TableName);
            if (DataProcessed != null) DataProcessed(this, new TaskStateEventArgs(_currentTaskState));
        }

        private void SafeAppendNewData(List<SLDataTable> newTables, IEnumerable<Step> newCalculation)
        {
            if (newTables == null) return;
            var intersection = _dataTables.Select(a => a.TableName).Intersect(newTables.Select(a => a.TableName)).ToList();
            if (!intersection.Any())
            {
                _dataTables.InsertRange(_dataTables.Count, newTables);
                if (newCalculation != null) _sourceSteps.InsertRange(_sourceSteps.Count, newCalculation);
            }
            else
            {
                var renames = new Dictionary<string, string>();
                foreach (var repeatingName in intersection)
                {
                    var renamingTable = newTables.SingleOrDefault(a => a.TableName == repeatingName);
                    if (renamingTable == null) continue;
                    var newName = string.Format("{0} {1}", renamingTable.TableName, (_dataTables.Select(a => a.TableName).Count(a => a.StartsWith(renamingTable.TableName)) + 1));
                    renamingTable.TableName = newName;
                    renames.Add(repeatingName, newName);
                }
                var renameOperation = StepFactory.CreateRenameTables(newTables, renames);
                _dataTables.InsertRange(_dataTables.Count, newTables);
                if (newCalculation == null) return;
                _sourceSteps.InsertRange(_sourceSteps.Count, newCalculation);
                _sourceSteps.AddRange(renameOperation);
            }
        }
        #endregion

        public void Reset()
        {
            ChartPanel.Children.Clear();
            ResetPreselection();
            Description = string.Empty;
            _dataTables = new List<SLDataTable>();
            _sourceSteps = new List<Step>();
            Description = string.Empty;
            _currentChartStep = null;
            CustomTableDetailsPanel.Children.Clear();
            CustomTableDetailsPanel.Visibility = Visibility.Collapsed;
        }
    }
}