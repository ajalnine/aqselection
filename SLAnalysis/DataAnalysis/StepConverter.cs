﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary.ReportChart
{
    public static class StepConverter
    {
        public static Step GetStep<T>(T p, string name)
        {
            var xs = new XmlSerializer(typeof (T));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), p);
            var s = new Step
                {
                    Calculator = null,
                    ParametersXML = sb.ToString().Replace(typeof (T).Name, "Parameters"),
                    Group = "Графики",
                    Name = name
                };
            s.Calculator = "Calc_" + typeof (T).Name.Replace("Parameters", String.Empty);
            return s;
        }

        public static T GetParameters<T>(Step s)
        {
            var xs = new XmlSerializer(typeof (T));
            var sr = new StringReader(s.ParametersXML.Replace("Parameters", typeof (T).Name));
            var p = (T) xs.Deserialize(XmlReader.Create(sr));
            return p;
        }
    }
}