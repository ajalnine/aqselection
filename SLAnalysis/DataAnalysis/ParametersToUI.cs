﻿using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using AQDataAnalysis.ReportChart;
using ApplicationCore.CalculationServiceReference;

namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        public string StepTableToSelect = null;
        public string StepXFieldToSelect = null;
        public string StepYFieldToSelect = null;
        public string StepZFieldToSelect = null;
        public string StepWFieldToSelect = null;
        public string StepVFieldToSelect = null;

        public void SetUIbyStep(Step s)
        {
            if (s == null) return;
            switch (s.Calculator.Substring(5, s.Calculator.Length - 5))
            {
                case "ReportChartDynamic":
                    var p1 = DeserializeParameters<ReportChartDynamicParameters>(s);
                    StepTableToSelect = p1.Table;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsDynamic);
                    MainRangeModeSelector.SetCurrentRangeType(p1.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p1.UserLimits);
                    MainSmoothnessSelector.SetCurrentSmoothness(p1.SmoothMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p1.LayoutMode);

                    StepYFieldToSelect = p1.Fields.FirstOrDefault();
                    StepXFieldToSelect = p1.DateField;
                    StepZFieldToSelect = p1.ColorField;
                    StepWFieldToSelect = p1.MarkerSizeField;
                    StepVFieldToSelect = p1.LabelField;
                    MainMarkerSelector.SetCurrentColorScale(p1.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p1.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p1.MarkerSizeMode);
                    
                    _currentFixedAxises = p1.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsDynamic] = _currentFixedAxises;
                    return;

                case "ReportChartCard":
                    var p2 = DeserializeParameters<ReportChartCardParameters>(s);
                    StepTableToSelect = p2.Table;
                    StepYFieldToSelect = p2.Fields.FirstOrDefault();
                    StepVFieldToSelect = p2.DateField;
                    if ((p2.CardType == ReportChartCardType.CardTypeIsTrends || p2.CardType == ReportChartCardType.CardTypeIsGraphics) && p2.Fields.Count > 1)
                    {
                        StepXFieldToSelect = p2.Fields.Skip(1).FirstOrDefault();
                    }
                    StepZFieldToSelect = p2.GroupField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsCard);
                    MainSeriesCheckSelector.SetCurrentChartSeriesType(p2.Series);
                    MainCardTypeSelector.SetCurrentCardType(p2.CardType);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p2.LayoutMode);
                    MainRangeModeSelector.SetCurrentRangeType(p2.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p2.UserLimits);
                    MainMarkerSelector.SetCurrentColorScale(p2.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p2.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p2.MarkerSizeMode);
                    _currentFixedAxises = p2.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsCard] = _currentFixedAxises;
                    return;

                case "ReportChartGroups":
                    var p3 = DeserializeParameters<ReportChartGroupsParameters>(s);
                    StepTableToSelect = p3.Table;
                    StepYFieldToSelect = p3.Fields.FirstOrDefault();
                    StepZFieldToSelect = p3.GroupField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsGroupCard);
                    MainGroupModeSelector.SetCurrentGroupMode(p3.Mode);
                    MainGroupParametersSelector.SetCurrentGroupParameters(p3.Center, p3.ShowOutliers, p3.Sorted);
                    MainRangeModeSelector.SetCurrentRangeType(p3.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p3.UserLimits);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p3.LayoutMode);
                    MainMarkerSelector.SetCurrentColorScale(p3.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p3.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p3.MarkerSizeMode);
                    _currentFixedAxises = p3.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsGroupCard] = _currentFixedAxises;
                    return;

                case "ReportChartHistogram":
                    var p4 = DeserializeParameters<ReportChartHistogramParameters>(s);
                    StepTableToSelect = p4.Table;
                    StepYFieldToSelect = p4.Fields.FirstOrDefault();
                    StepXFieldToSelect = p4.Fields.Count > 1 ? p4.Fields[1] : p4.Fields[0];
                    StepZFieldToSelect = p4.ColorField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsFreq);
                    MainFrequencyParameterSelector.SetCurrentAlignMode(p4.AlignMode);
                    MainFrequencyParameterSelector.SetCurrentYMode(p4.YMode);
                    MainFrequencyParameterSelector.SetDetailLevel(p4.DetailLevel);
                    MainFrequencyParameterSelector.SetCurrentStep(p4.Step);
                    MainFrequencyParameterSelector.SetCurrentUserAlignValue(p4.UserAlignValue);
                    MainRangeModeSelector.SetCurrentRangeType(p4.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p4.UserLimits);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p4.LayoutMode);
                    MainMarkerSelector.SetCurrentColorScale(p4.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p4.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p4.MarkerSizeMode);
                    MainHistogramTypeSelector.SetCurrentHistogramType(p4.HistogramType);
                    _currentFixedAxises = p4.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsFreq] = _currentFixedAxises;
                    MainFitSelector.SetCurrentFit(p4.FitMode);
                    MainAggregateSelector.SetCurrentAggregate(p4.Aggregate);
                    return;

                case "ReportChartScatterPlot":
                    var p5 = DeserializeParameters<ReportChartScatterPlotParameters>(s);
                    StepTableToSelect = p5.Table;
                    StepXFieldToSelect = p5.FieldX;
                    StepYFieldToSelect = p5.Fields.FirstOrDefault();
                    StepZFieldToSelect = p5.ColorField;
                    StepWFieldToSelect = p5.MarkerSizeField;
                    StepVFieldToSelect = p5.LabelField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsScatterPlot);
                    MainRangeModeSelector.SetCurrentRangeType(p5.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p5.UserLimits);
                    MainSmoothnessSelector.SetCurrentSmoothness(p5.SmoothMode);
                    MainMarkerSelector.SetCurrentColorScale(p5.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p5.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p5.MarkerSizeMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p5.LayoutMode);
                    _currentFixedAxises = p5.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsScatterPlot] = _currentFixedAxises;
                    return;

                case "ReportChartProbabilityPlot":
                    var p6 = DeserializeParameters<ReportChartProbabilityPlotParameters>(s);
                    StepTableToSelect = p6.Table;
                    StepYFieldToSelect = p6.Fields.FirstOrDefault();
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsPP);
                    MainMarkerSelector.SetCurrentColorScale(p6.ColorScale);
                    MainMarkerSelector.SetCurrentMarkerMode(p6.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p6.MarkerSizeMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p6.LayoutMode);
                    _currentFixedAxises = p6.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ChartIsPP] = _currentFixedAxises;
                    return;
            }
        }

        private static T DeserializeParameters<T>(Step s)
        {
            var xs = new XmlSerializer(typeof (T));
            var sr = new StringReader(s.ParametersXML.Replace("Parameters", typeof (T).Name));
            return (T) xs.Deserialize(XmlReader.Create(sr));
        }
    }
}