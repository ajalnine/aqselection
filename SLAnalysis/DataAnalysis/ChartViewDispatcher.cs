﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQDataAnalysis.ReportChart;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        #region События интерфейса

        private FixedAxises _currentFixedAxises;

        private void MainFieldsSelector_FieldsChanged(object sender, FieldsChangedEventArgs e)
        {
            if (e.ChangedField == ChangedFieldType.Renew) return;
            if (e.ChangedField == ChangedFieldType.Flip) MainRangeModeSelector.FlipUserRanges();
            CheckLimits();
            if ((e.YField != null || e.XField != null) && ViewType.SelectedIndex == 1)
            {
                RefreshFieldMarks();
                MakeReportChart();
            }
            FixedAxisRangeEditor.IsOpen = false;
        }

        private void MainChartTypeSelector_ChartTypeChanged(object sender, ChartTypeChangedEventArgs e)
        {
            SetupBestOptions();
            _currentFixedAxises = _fixedAxisesCache.ContainsKey(e.ChartType) ? _fixedAxisesCache[e.ChartType] : null;
            RefreshChartPanelVisibility();
            MakeReportChart();
            FixedAxisRangeEditor.IsOpen = false;
        }

        private void MainMarkerSelector_OnMarkerModeChanged(object sender, MarkerChangedEventArgs e)
        {
            MakeReportChart();
        }

        private void RangeModeSelector_RangeModeChanged(object sender, RangeModeChangedEventArgs e)
        {
            RefreshChartPanelVisibility();
            MakeReportChart();
        }

        private void SeriesCheckSelector_SeriesCheckChanged(object sender, SeriesCheckChangedEventArgs e)
        {
            MakeReportChart();
        }

        private void MainFitSelector_OnFitChanged(object sender, FitChangedEventArgs e)
        {
            MakeReportChart();
        }
        private void CardTypeSelector_CardTypeChanged(object sender, CardTypeChangedEventArgs e)
        {
            SetupBestOptions();
            RefreshChartPanelVisibility();
            MakeReportChart();
        }

        private void MainHistogramTypeSelector_OnHistogramTypeChanged(object sender, HistogramTypeChangedEventArgs e)
        {
            SetupBestOptions();
            RefreshChartPanelVisibility();
            MakeReportChart();
        }

        private void MainAggregateSelector_OnAggregateChanged(object sender, AggregateChangedEventArgs e)
        {
            RefreshChartPanelVisibility();
            MakeReportChart();
        }
        private void SmoothnessSelector_SmoothnessChanged(object sender, SmoothnessChangedEventArgs e)
        {
            MakeReportChart();
        }

        private void GroupModeSelector_GroupModeChanged(object sender, GroupModeChangedEventArgs e)
        {
            MakeReportChart();
        }

        private void GroupParametersSelector_GroupParametersChanged(object sender, GroupParametersChangedEventArgs e)
        {
            MakeReportChart();
        }

        private void MainLayoutModeSelector_OnLayoutModeChanged(object sender, LayoutModeChangedEventArgs e)
        {
            MakeReportChart();
        }
        private void FrequencyParameterSelector_FrequencyParametersChanged(object sender, FrequencyParametersChangedEventArgs e)
        {
            MakeReportChart();
            FixedAxisRangeEditor.IsOpen = false;
            CheckStep(e);
        }

        private void CheckStep(FrequencyParametersChangedEventArgs e)
        {
            var xFixed = _currentFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X");
            if (xFixed == null || !xFixed.AxisMaxFixed || !xFixed.AxisMinFixed || !e.Step.HasValue) return;
            var steps = ((xFixed.AxisMax - xFixed.AxisMin) / (e.Step.Value / Math.Pow(2, e.DetailLevel) * 4)).Value;
            MainFrequencyParameterSelector.ShowExclamation(Math.Abs(steps - Math.Round(steps)) > 1e-7);
        }

        private void ReportChartChanged(object o, EventArgs e)
        {
            ClearMarks();
            MakeReportChart();
            RefreshFieldMarks();
        }

        private void RefreshFieldMarks()
        {
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.ChartIsScatterPlot:
                    MarkCorrelatedFields();
                    break;
                case ReportChartType.ChartIsPP:
                    MarkNormalFields();
                    break;
            }
        }
        #endregion

        private void ViewType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewType == null) return;
            SetButtonsVisibility();
            FixedAxisRangeEditor.IsOpen = false;
            if (_isSignalProcessingFirstTime || ViewType.SelectedIndex != 1) return;
            Data.LDRCache.Clear();
            if (_hasClosed) RefreshParametersUI();
            RefreshChartPanelVisibility();
            MakeReportChart();
        }

        private void RefreshParametersUI()
        {
            MainRangeModeSelector.RefreshUI();
            MainSeriesCheckSelector.RefreshUI();
            MainSmoothnessSelector.RefreshUI();
            MainGroupModeSelector.RefreshUI();
            MainGroupParametersSelector.RefreshUI();
            MainCardTypeSelector.RefreshUI();
            MainChartTypeSelector.RefreshUI();
            MainMarkerSelector.RefreshUI();
            MainFrequencyParameterSelector.RefreshUI();
            MainFitSelector.RefreshUI();
            MainLayoutModeSelector.RefreshUI();
            MainHistogramTypeSelector.RefreshUI();
            MainAggregateSelector.RefreshUI();
        }

        private void SetupBestOptions()
        {
            if (_isSignalProcessingFirstTime) return;
            ClearMarks();
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.ChartIsDynamic:
                    MainRangeModeSelector.SetCurrentRangeType(ReportChartRangeType.RangeIsNTD);
                    MainSmoothnessSelector.SetCurrentSmoothness(ReportChartDynamicSmoothMode.DynamicSmoothLowess);
                    break;

                case ReportChartType.ChartIsCard:
                    MainRangeModeSelector.SetCurrentRangeType(MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends || MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends
                        ? ReportChartRangeType.RangeIsNone
                        : ReportChartRangeType.RangeIsSlide);
                    break;

                case ReportChartType.ChartIsFreq:
                    MainRangeModeSelector.SetCurrentRangeType(ReportChartRangeType.RangeIsNTD);
                    MainFitSelector.SetCurrentFit(ReportChartFitMode.FitGauss | ReportChartFitMode.FitKDE);
                    break;

                case ReportChartType.ChartIsGroupCard:
                    MainRangeModeSelector.SetCurrentRangeType(ReportChartRangeType.RangeIsNTD);
                    break;

                case ReportChartType.ChartIsScatterPlot:
                    MainRangeModeSelector.SetCurrentRangeType(ReportChartRangeType.RangeIsNone);
                    var smoothnessMode = MainSmoothnessSelector.GetCurrentSmoothness();
                    if (smoothnessMode != ReportChartDynamicSmoothMode.DynamicSmoothLine && smoothnessMode != ReportChartDynamicSmoothMode.DynamicSmoothIntervals)
                    MainSmoothnessSelector.SetCurrentSmoothness(ReportChartDynamicSmoothMode.DynamicSmoothLine);
                    break;
            }
        }

        private void RefreshChartPanelVisibility()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();
            if (MainSeriesCheckSelector == null) return;
            MainSeriesCheckSelector.Visibility = (rct == ReportChartType.ChartIsCard &&
                                                  MainCardTypeSelector.GetCurrentCardType() ==
                                                  ReportChartCardType.CardTypeIsX &&
                                                  new [] { ReportChartRangeType.RangeIsSlide, ReportChartRangeType.RangeIsR, ReportChartRangeType.RangeIsSigmas }.Contains(MainRangeModeSelector.GetCurrentRangeType()))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainCardTypeSelector.Visibility = (rct == ReportChartType.ChartIsCard)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainFrequencyParameterSelector.Visibility = (rct == ReportChartType.ChartIsFreq && (MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Frequencies || MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Stratification))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainAggregateSelector.Visibility = (rct == ReportChartType.ChartIsFreq && MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Pareto)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainHistogramTypeSelector.Visibility = (rct == ReportChartType.ChartIsFreq)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainGroupModeSelector.Visibility = (rct == ReportChartType.ChartIsGroupCard)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainGroupParametersSelector.Visibility = (rct == ReportChartType.ChartIsGroupCard)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainSmoothnessSelector.Visibility = (rct == ReportChartType.ChartIsDynamic ||
                                                 rct == ReportChartType.ChartIsScatterPlot)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainRangeModeSelector.Visibility = (rct == ReportChartType.ChartIsPP ||
                    (
                        rct == ReportChartType.ChartIsCard && (MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends || 
                                                           MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsGraphics)
                                                           ||
                                                           rct == ReportChartType.ChartIsFreq && (MainHistogramTypeSelector.GetCurrentHistogramType()==ReportChartHistogramType.Pareto)
                    )
                ) 
                ? Visibility.Collapsed
                : Visibility.Visible;
            MainRangeModeSelector.XY = rct == ReportChartType.ChartIsScatterPlot;
            MainMarkerSelector.Visibility = Visibility.Visible;
            MainFitSelector.Visibility = (rct == ReportChartType.ChartIsFreq && MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Frequencies)
                ? Visibility.Visible
                : Visibility.Collapsed;
            

            switch (rct)
            {
                case ReportChartType.ChartIsCard:
                    if (MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends || MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsGraphics)
                    {
                        MainFieldsSelector.SetFieldsHeaders("Y2", "Y1", "Имя", null, "↑↓");
                        MainSmoothnessSelector.Visibility = MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsGraphics ? Visibility.Collapsed : Visibility.Visible;
                        MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                    }
                    else
                    {
                        MainFieldsSelector.SetFieldsHeaders(null, "Y", null, null, "↑↓");
                    }
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                    break;
                    
                case ReportChartType.ChartIsDynamic:
                    MainFieldsSelector.SetFieldsHeaders("↑↓", "Y", "Цвет", "Размер", "Метка");
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.DateTime, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.Double | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                    if (!_isSignalProcessingFirstTime)MainFieldsSelector.SetZWVToStart(); //Для того, чтобы при передаче расчета с панели слайдов не сбрасывались поля маркера.
                    break;
                    
                case ReportChartType.ChartIsFreq:
                    switch (MainHistogramTypeSelector.GetCurrentHistogramType())
                    {
                        case ReportChartHistogramType.Pareto:
                            switch (MainAggregateSelector.GetCurrentAggregate())
                            {
                                case ReportChartAggregate.N:
                                case ReportChartAggregate.Percent:
                                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", null, null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                                default:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Параметр", null, null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                            }
                            break;
                        default:
                            MainFieldsSelector.SetFieldsHeaders(null, "Параметр", "Цвет", null, null);
                            MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                            break;
                    }
                    break;

                case ReportChartType.ChartIsGroupCard:
                    MainFieldsSelector.SetFieldsHeaders(null, "Y", "Группа", null, null);
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                    break;

                case ReportChartType.ChartIsScatterPlot:
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.Double | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                    MainFieldsSelector.SetFieldsHeaders("X", "Y", "Цвет", "Размер", "Метка");
                    if (!_isSignalProcessingFirstTime)MainFieldsSelector.SetZWVToStart(); //Для того, чтобы при передаче расчета с панели слайдов не сбрасывались поля маркера.
                    break;

                case ReportChartType.ChartIsPP:
                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", null, null, null);
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                    break;
            }
            _isSignalProcessingFirstTime = false;
        }
        
        private void IrcUiReflectChanges(object o, UIReflectChangesEventArgs e)
        {
            if (e.UserLimits.Table.Count>0) MainRangeModeSelector.SetupRangesForAB(e.UserLimits);
            if (e.Step.HasValue && e.StepIsAuto.HasValue) MainFrequencyParameterSelector.DisplayStep(e.StepIsAuto.Value, e.Step.Value);
        }

        private void SetButtonsVisibility()
        {
            if (ViewType.SelectedIndex == 1)
            {
                CopyButton.Visibility = Visibility.Collapsed;
                TableToCalcButton.Visibility = Visibility.Collapsed;
                CalcToCalcButton.Visibility = Visibility.Visible;
                XlsxButton.Visibility = Visibility.Collapsed;
                NewSlideButton.Visibility = Visibility.Visible;
                DeleteButton.Visibility = Visibility.Collapsed;
                TableToAnalysisButton.Visibility = Visibility.Collapsed;
                PasteButton.Visibility = Visibility.Collapsed;
                MainTableCellsColorSelector.Visibility = Visibility.Collapsed;
            }
            else
            {
                MainTableCellsColorSelector.Visibility = Visibility.Visible; 
                CopyButton.Visibility = Visibility.Visible;
                TableToCalcButton.Visibility = Visibility.Visible;
                TableToAnalysisButton.Visibility = Visibility.Visible; 
                CalcToCalcButton.Visibility = Visibility.Visible;
                XlsxButton.Visibility = Visibility.Visible;
                NewSlideButton.Visibility = Visibility.Collapsed;
                DeleteButton.Visibility = Visibility.Visible;
                PasteButton.Visibility = Visibility.Visible;
            }
        }

        private void CheckLimits()
        {
            BlockPointPlots(_selectedTable.Table.Count > 20000);
            CheckDateForDynamic();
        }

        private void BlockPointPlots(bool isBlocked)
        {
            if (isBlocked)MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsFreq);
            MainChartTypeSelector.Enable(ReportChartType.ChartIsDynamic, !isBlocked);
            MainChartTypeSelector.Enable(ReportChartType.ChartIsCard, !isBlocked);
            MainChartTypeSelector.Enable(ReportChartType.ChartIsScatterPlot, !isBlocked);
            MainChartTypeSelector.Enable(ReportChartType.ChartIsGroupCard, !isBlocked);
        }

        private void CheckDateForDynamic()
        {
            if (DataHelper.GetFirstDateIndex(_selectedTable) >= 0) return;
            if (MainChartTypeSelector.GetCurrentChartType() == ReportChartType.ChartIsDynamic)
                MainChartTypeSelector.SetCurrentChartType(ReportChartType.ChartIsFreq);
            MainChartTypeSelector.Enable(ReportChartType.ChartIsDynamic, false);
        }
    }
}