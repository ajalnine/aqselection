﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public partial class DataAnalysis
    {
        private TableOrderMode _currentTableOrderMode  = TableOrderMode.Unordered;
        private readonly Dictionary<TableOrderMode, string> _tableModeLabels = 
            new Dictionary<TableOrderMode, string> {{TableOrderMode.Unordered, String.Empty}, {TableOrderMode.Abc, "A..Z"}, {TableOrderMode.Reversed, "Z..A"}}; 
        private string CreateTableSelectors(int defaultTable)
        {
            if (defaultTable == -1 || _dataTables.Count==0) return null;
            OrderModeTextBlock.Text = _tableModeLabels[_currentTableOrderMode];
            var tableToSelect = _dataTables[defaultTable].TableName;
            var orderedNameSource = GetOrderedNames();
            foreach (var s in orderedNameSource)
            {
                var erb = GetTableSelectorButton(s);
                erb.IsChecked = s == tableToSelect;
                erb.Click += SelectorButton_Click;
                SelectorPanel.Children.Add(erb);
            }
            return tableToSelect;
        }

        private IEnumerable<string> GetOrderedNames()
        {
            var nameSource = (from d in _dataTables select d.TableName).Where(a => !a.StartsWith("#")).ToList();
            var orderedNameSource = nameSource;
            switch (_currentTableOrderMode)
            {
                case TableOrderMode.Abc:
                    orderedNameSource = nameSource.OrderBy(a => a).ToList();
                    break;
                case TableOrderMode.Reversed:
                    orderedNameSource = nameSource.OrderByDescending(a => a).ToList();
                    break;
            }
            return orderedNameSource;
        }

        private void ChangeTableName(string oldName, string newName)
        {
            var renamingButton = (from s in SelectorPanel.Children let radioButton = s as RadioButton
                                  where Equals(radioButton.Tag, oldName)
                                    select radioButton).SingleOrDefault();
            if (renamingButton == null) return;

            var grid = renamingButton.Content as Grid;
            if (grid == null) return;

            var tb = grid.Children[0] as TextBlock;
            if (tb == null || tb.Text.Trim().ToLower() != oldName.Trim().ToLower()) return;
            tb.Text = newName;
            renamingButton.Tag = newName;
        }

        private RadioButton GetTableSelectorButton(string s)
        {
            var g = new Grid();
            g.Children.Add(new TextBlock
                        {
                            Text = XmlConvert.DecodeName(s),
                            VerticalAlignment = VerticalAlignment.Center,
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Padding = new Thickness(3)
                        });

            var r = new Rectangle
                {
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    Stroke = new SolidColorBrush(Colors.Red),
                    StrokeThickness = 2,
                    Visibility = Visibility.Collapsed,
                    StrokeDashArray = new DoubleCollection {2, 2}
                };

            var animation = new DoubleAnimation
                {
                    Duration = new Duration(new TimeSpan(0,0,0,2)),
                    From = 200,
                    To = 0,
                    RepeatBehavior = RepeatBehavior.Forever
                };
            var sb = new Storyboard { Duration = new Duration(new TimeSpan(0, 0, 0, 2))};
            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, r);
            Storyboard.SetTargetProperty(animation, new PropertyPath("StrokeDashOffset"));
            r.Resources.Add(s, sb);
            g.Children.Add(r);
            sb.Begin();
            
            return new RadioButton
                {
                    Style = Resources["RadioButtonBorderStyle"] as Style,
                    Content = g,
                    Background = new SolidColorBrush(Colors.Transparent),
                    Tag = s,
                    GroupName = "0"
                    ,Height = 24
                    ,VerticalAlignment = VerticalAlignment.Center
                    ,Margin = new Thickness(5, 0, 0, 0)
                };
        }

        private void SelectorButton_Click(object sender, RoutedEventArgs e)
        {
            var erb = (RadioButton) sender;
            ShowTable(erb.Tag.ToString());
        }

        private RadioButton GetCurrentSelectorButton()
        {
            return (from s in SelectorPanel.Children let radioButton = s as RadioButton where radioButton.IsChecked != null 
                        && (radioButton != null && (s is RadioButton && radioButton.IsChecked.Value)) select s)
                    .SingleOrDefault() as RadioButton;
        }

        private void ClearSelectorPanel()
        {
            SelectorPanel.Children.Clear();
            SelectorPanel.Visibility = (_dataTables == null || _dataTables.Count == 1)
                                           ? Visibility.Collapsed
                                           : Visibility.Visible;
            ReorderTablesButton.Visibility = SelectorPanel.Visibility;
            OrderModeTextBlock.Visibility = SelectorPanel.Visibility;
        }
        private void ReorderTablesButton_OnClick(object sender, RoutedEventArgs e)
        {
            int mode = (int) _currentTableOrderMode + 1;
            if (mode > 2) mode = 0;
            _currentTableOrderMode = (TableOrderMode) mode;
            ClearSelectorPanel();
            CreateTableSelectors(_dataTables.IndexOf(_selectedTable));
        }
    }

    public enum TableOrderMode
    {
        Unordered = 0, Abc = 1, Reversed = 2
    }
}