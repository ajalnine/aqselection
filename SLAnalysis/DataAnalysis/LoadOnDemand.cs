﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public partial class DataAnalysis
    {
        private void BeginLoadEmptyTable()
        {
            var e = new EmptyTableSelectedEventArgs
                {
                    EmptyTable = _selectedTable,
                    TableLoadedCallBack = LoadedTableCallBack,
                    StartProgress = StartLoadTableProgress,
                    StopProgress = StopLoadTableProgress
                };
            if (EmptyTableSelected != null) EmptyTableSelected.Invoke(this, e);
        }

        private void StartLoadTableProgress()
        {
            Dispatcher.BeginInvoke(delegate 
            { 
                SelectorPanel.IsHitTestVisible = false;
                SelectorPanel.Opacity = 0.8;
                var rb = GetCurrentSelectorButton();
                var grid = rb.Content as Grid;
                if (grid == null) return;
                var rectangle = grid.Children[1] as Rectangle;
                if (rectangle != null) rectangle.Visibility = Visibility.Visible;
            });
        }

        private void StopLoadTableProgress()
        {
            Dispatcher.BeginInvoke(delegate
            {
                SelectorPanel.IsHitTestVisible = true;
                SelectorPanel.Opacity = 1;
                var rb = GetCurrentSelectorButton();
                var grid = rb.Content as Grid;
                if (grid == null) return;
                var rectangle = grid.Children[1] as Rectangle;
                if (rectangle != null) rectangle.Visibility = Visibility.Collapsed;
            });
        }

        private void LoadedTableCallBack(SLDataTable loadedTable)
        {
            _selectedTable = loadedTable;

            var source = _dataTables.SingleOrDefault(a => a.TableName == loadedTable.TableName);
            _dataTables.Remove(source);
            _dataTables.Add(loadedTable);

            var appendedSource = _appendedTables.SingleOrDefault(a => a.TableName == loadedTable.TableName);
            _appendedTables.Remove(appendedSource);
            _appendedTables.Add(loadedTable);
            
            _lastSelectedTable = _selectedTable.TableName;

            SetupDataSet();
        }
    }

    public class EmptyTableSelectedEventArgs : EventArgs
    {
        public SLDataTable EmptyTable { get; set; }
        public TableLoadedCallBackDelegate TableLoadedCallBack { get; set; }
        public Action StartProgress { get; set; }
        public Action StopProgress { get; set; }
    }

    public delegate void TableLoadedCallBackDelegate(SLDataTable loadedTable);
}