﻿using System.Linq;
using System.Windows;
using AQControlsLibrary.ReportChart;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public partial class DataAnalysis
    {
        private void SetColorMarkOnTable(ColorChangedEventArgs e)
        {
            if ( _selectedTable == null || _selectedTable.Table == null || !_selectedTable.Table.Any() || MainDataGrid.SelectedItems==null) return;
            
            if (e.TableMarkMode == TableMarkMode.Clear)
            {
                DeleteAllColorData();
                return;
            }

            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            if (x < 0 || y < 0 ) return;

            if (MainDataGrid.SelectedItems.Count <= 1)
            {
                var selectedRow = MainDataGrid.SelectedItems.OfType<SLDataRow>().FirstOrDefault();
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                        var column = string.Format("#{0}_Цвет", MainDataGrid.CurrentColumn.Header);
                        _selectedTable.FillCell(column, e.ColorString, "System.String", selectedRow);
                        break;
                    case TableMarkMode.Row:
                        var columns = _selectedTable.ColumnNames.Where(a => !a.StartsWith("#")).Select(a=>string.Format("#{0}_Цвет", a)).ToList();
                        _selectedTable.FillRow(columns, e.ColorString, "System.String", selectedRow);
                        break;
                    case TableMarkMode.Column:
                        var column2 = string.Format("#{0}_Цвет", MainDataGrid.CurrentColumn.Header);
                        _selectedTable.FillColumn(column2, e.ColorString, "System.String");
                        break;
                }
            }
            else
            {
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                    case TableMarkMode.Column:
                        var column = string.Format("#{0}_Цвет", MainDataGrid.CurrentColumn.Header);
                        foreach (var s in MainDataGrid.SelectedItems)
                        {
                            _selectedTable.FillCell(column, e.ColorString, "System.String", (SLDataRow)s);
                        }
                        break;
                    case TableMarkMode.Row:
                        var columns = _selectedTable.ColumnNames.Where(a => !a.StartsWith("#")).Select(a=>string.Format("#{0}_Цвет", a)).ToList();
                        foreach (var s in MainDataGrid.SelectedItems)
                        {
                            _selectedTable.FillRow(columns, e.ColorString, "System.String", (SLDataRow)s);
                        }
                        break;
                }
            }
            MakeDataSetStructure();
        }

        private void DeleteAllColorData()
        {
            var mr = MessageBox.Show(string.Format("Удалить цветовую разметку {0} ?", _selectedTable.TableName),
                "Подтверждение",
                MessageBoxButton.OKCancel);
            if (mr == MessageBoxResult.Cancel) return;

            var colorColumns = _selectedTable.ColumnNames.Where(a => a.EndsWith("_Цвет")).ToList();
            foreach (var cc in colorColumns) _selectedTable.DeleteColumn(cc);
            MakeDataSetStructure();
        }
    }
}
