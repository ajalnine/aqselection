﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AQDataAnalysis.ReportChart;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;

// ReSharper disable once CheckNamespace
namespace AQDataAnalysis
{
    public delegate void DataAnalysisSelectionChangedDelegate(object sender, DataAnalysisSelectionChangedEventArgs e);
        
    public delegate void EmptyTableSelectedDelegate(object o, EmptyTableSelectedEventArgs e);
    public partial class DataAnalysis
    {
        #region Поля и свойства компонента

        public delegate void DataProcessDelegate(object sender, TaskStateEventArgs e);
        
        public static readonly DependencyProperty DisposeDataAfterClosingProperty =
            DependencyProperty.Register("DisposeDataAfterClosing", typeof (bool), typeof (DataAnalysis),
                                        new PropertyMetadata(true));

        private readonly AQService _aqs;
        public AQModuleDescription ParentAqmd { get; set; }

        private readonly CalculationService _cas;
        private string _fileName;
        private ReportingDuplexServiceClient _rdsc;
        private Dictionary<ReportChartType, FixedAxises> _fixedAxisesCache;
        private Point _mousePosition;

        public bool DisposeDataAfterClosing
        {
            get { return (bool) GetValue(DisposeDataAfterClosingProperty); }
            set { SetValue(DisposeDataAfterClosingProperty, value); }
        }

        public event DataProcessDelegate DataLoaded;
        public event DataProcessDelegate DataProcessed;
        public event DataProcessDelegate Finished;
        public event DataAnalysisSelectionChangedDelegate TableViewSelectionChanged;
        public event EmptyTableSelectedDelegate EmptyTableSelected;

        #endregion

        public DataAnalysis()
        {
            InitializeComponent();
            InitFixedAxisesCache();
            MouseMove += Window_MouseMove;
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (_cas == null) _cas = Services.GetCalculationService();
            if (_aqs == null) _aqs = Services.GetAQService();
        }

        void Window_MouseMove(object sender, MouseEventArgs e)
        {
            _mousePosition = e.GetPosition(this);
        }
        
        private void InitFixedAxisesCache()
        {
            _fixedAxisesCache = new Dictionary<ReportChartType, FixedAxises>
            {
                {ReportChartType.ChartIsCard, new FixedAxises()},
                {ReportChartType.ChartIsDynamic, new FixedAxises()},
                {ReportChartType.ChartIsFreq, new FixedAxises()},
                {ReportChartType.ChartIsGroupCard, new FixedAxises()},
                {ReportChartType.ChartIsPP, new FixedAxises()},
                {ReportChartType.ChartIsScatterPlot, new FixedAxises()}
            };
        }

        public void CloseAnalysis()
        {
            FixedAxisRangeEditor.IsOpen = false;
            CustomTableDetailsPanel.Visibility = Visibility.Collapsed;
            _hasClosed = true; 
            if (Parent is ChildWindow) DisposeCachesAndData();
            if (Finished != null) Finished(this, new TaskStateEventArgs(_currentTaskState));
        }

        private void DisposeCachesAndData()
        {
            Data.LDRCache.Clear();
            if (_currentFixedAxises != null)
                _currentFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            ChartPanel.Children.Clear();
            ResetAppendedTables();
            CustomTableDetailsPanel.Children.Clear();
            if (MainDataGrid != null && MainDataGrid.ItemsSource != null)
            {
                MainDataGrid.Dispatcher.BeginInvoke(() =>
                {
                    MainDataGrid.ItemsSource = null;
                    MainDataGrid.Columns.Clear();
                });
            }
            if (DisposeDataAfterClosing)
            {
                _cas.BeginDropData(_fileName, null, null);
                _fileName = null;
            }
            _selectedTable = null;
            _dataTables = null;
            _sourceSteps = null;
        }

        private void DataSetPresenter_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (!(Parent is ChildWindow)) return;
            var p = Parent as ChildWindow;
            if (e.Key == Key.PageDown)
            {
                p.Margin = new Thickness(0, Root.Margin.Top + 100, 0, 0);
                p.UpdateLayout();
            }
            if (e.Key == Key.PageUp)
            {
                p.Margin = new Thickness(0, Root.Margin.Top - 100, 0, 0);
                p.UpdateLayout();
            }
        }

        private void MainDataGrid_OnPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            var grid = e.EditingElement as Grid;
            if (grid == null || grid.Children == null || grid.Children.Count <= 1) return;
            var textBox = grid.Children[1] as TextBox;
            if (textBox == null) return;
            textBox.SelectAll();
        }
    }
}