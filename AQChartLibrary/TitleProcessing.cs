﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{

    public delegate void InteractiveRenameDelegate(object source, ChartRenameEventArgs e);

    public partial class Chart 
    {
        public event InteractiveRenameDelegate InteractiveRename;

        private string _textBeforeEditing;
        private List<InlineDescription> _richTextBeforeEditing;

        #region Legend processing
        private void ChartLegend_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            _textBeforeEditing = s != null ? s.Text : string.Empty;
        }

        private void ChartLegend_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            ChartLegendCommon_LostFocus(sender, ChartRenameItemType.Field);
        }

        private void ChartLegendCommon_LostFocus(object sender, ChartRenameItemType crit)
        {
            var s = sender as TextBox;
            var textAfterEditing = s != null ? s.Text : string.Empty;
            if (textAfterEditing == _textBeforeEditing) return;
            var description = s?.Tag as ChartSeriesDescription;
            var cs = description != null ? ((ChartSeriesDescription) s.Tag) : null;
            var e = new ChartRenameEventArgs
            {
                RenameOperations = new List<ChartRenameOperation>
                        {
                            new ChartRenameOperation
                            {
                                OldName =  _textBeforeEditing.Trim(),
                                NewName = textAfterEditing.Trim(),
                                ChartItemType = crit,
                                TableName = (s != null && cs?.SourceTableName!=null) ? cs?.SourceTableName : null,
                                LayerField = cs?.ZColumnName?.Trim()
                            }
                        }
            };

            InvokeRenameEvent(e);
        }

        private void ChartLegendSimple_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            ChartLegendCommon_LostFocus(sender, ChartRenameItemType.Layer);
        }


        #endregion

        #region Chart titles processing

        private void ChartTitle_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            _textBeforeEditing = s != null ? GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;
            _richTextBeforeEditing = s != null ? GetInlinesFromRichTextBox(s.Blocks) : null;
        }


        private void ChartTitle_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            var textAfterEditing = s != null ? GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;
            if (textAfterEditing == _textBeforeEditing) return;
            textAfterEditing = textAfterEditing.Replace(">=", "≥").Replace("<=", "≤");
            var renameOperations = new List<ChartRenameOperation>();

            if (textAfterEditing == _textBeforeEditing)
            {
                renameOperations.Add(new ChartRenameOperation
                {
                    ChartItemType = ChartRenameItemType.Table,
                    OldName = SourceName,
                    NewName = SourceName,
                    TableName = SourceName
                });
                InvokeRenameEvent(new ChartRenameEventArgs { RenameOperations = renameOperations });
                return;
            }

            var match = Regex.Match(textAfterEditing, GetRegexPatternForTitleProcessing(_richTextBeforeEditing));
            var firstOrDefault = _richTextBeforeEditing.FirstOrDefault(a => a.IsBold);
            var possibleTable = string.Empty;
            if (firstOrDefault != null)possibleTable = firstOrDefault.Text;
            
            var possibleFields =_richTextBeforeEditing.Where(a => a.IsBold).Skip(1).Select(a => new { Field = a.Text, Table = possibleTable });
            
            var fieldNames =       ChartData.ChartSeries.Select(a => new { Field = a.XColumnName, Table = a.SourceTableName })
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.YColumnName, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.ZColumnName, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.VColumnName, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.WColumnName, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.XAxisTitle, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.YAxisTitle, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.ZAxisTitle, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.VAxisTitle, Table = a.SourceTableName }))
                            .Union(ChartData.ChartSeries.Select(a => new { Field = a.WAxisTitle, Table = a.SourceTableName }))
                            .Union(possibleFields)
                            .Where(a => !String.IsNullOrEmpty(a.Field)).ToList();

            var candidates = _richTextBeforeEditing.Where(a => a.IsBold).Select(a => a.Text).ToList();
            for (int index = 0; index < candidates.Count; index++)
            {
                var r = candidates[index];
                var groupname = "A" + (index + 1);
                if (match.Groups[groupname].Captures.Count == 0) continue;
                var newnameGroup = match.Groups[groupname].Captures[match.Groups[groupname].Captures.Count - 1];
                if (!match.Groups[groupname].Success || newnameGroup.Value == r) continue;
                var oldname = r;
                if (SourceName.Replace(">=", "≥").Replace("<=", "≤") == oldname)
                {
                    renameOperations.Add(new ChartRenameOperation
                    {
                        ChartItemType = ChartRenameItemType.Table,
                        OldName = SourceName,
                        NewName = newnameGroup.Value,
                        TableName = oldname
                    });
                }
                else
                {
                    var foundFields = fieldNames.Where(a => a.Field == r || a.Field.Replace(">=", "≥").Replace("<=", "≤") == r).ToList();
                    renameOperations.AddRange(foundFields.Select(foundField => new ChartRenameOperation
                    {
                        ChartItemType = ChartRenameItemType.Field,
                        OldName = foundField.Field.Replace(">=", "≥").Replace("<=", "≤"),
                        NewName = newnameGroup.Value,
                        TableName = foundField.Table
                    }));
                }
            }
            renameOperations.Reverse();
            var duplicateCleaned = new List<ChartRenameOperation>();
            foreach (var ro in renameOperations
                .Where(ro => !duplicateCleaned
                    .Any(a=>a.OldName == ro.OldName && a.ChartItemType == ro.ChartItemType && a.TableName == ro.TableName)))
            {
                duplicateCleaned.Add(ro);
            }

            if (duplicateCleaned.Count > 0) InvokeRenameEvent(new ChartRenameEventArgs { RenameOperations = duplicateCleaned });
            else PartialRefresh();
        }

        private void ChartSubTitle_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            _textBeforeEditing = s != null ? GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;

        }
        private void ChartSubTitle_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            var textAfterEditing = s != null ? GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;
            textAfterEditing = textAfterEditing.Replace(">=", "≥").Replace("<=", "≤");

            if (textAfterEditing == _textBeforeEditing) return;

            AxisTitleProcessing(textAfterEditing);
        }

        #endregion

        #region Axis processing
        private void ChartAxisTitle_GotFocus(object sender, RoutedEventArgs e)
        {
            var s = sender as TextBox;
            _textBeforeEditing = s != null ? s.Text : string.Empty;

        }
        private void ChartAxisTitle_LostFocus(object sender, RoutedEventArgs e)
        {
            var s = sender as TextBox;
            var textAfterEditing = s != null ? s.Text : string.Empty;
            textAfterEditing = textAfterEditing.Replace(">=", "≥").Replace("<=", "≤");

            if (textAfterEditing == _textBeforeEditing) return;

            AxisTitleProcessing(textAfterEditing);
        }

        private void AxisTitleProcessing(string textAfterEditing)
        {
            var renameOperations = new List<ChartRenameOperation>();
            var fieldNames = ChartData.ChartSeries.Select(a => new {Field = a.XAxisTitle, Table = a.SourceTableName})
                .Union(ChartData.ChartSeries.Select(a => new {Field = a.YAxisTitle, Table = a.SourceTableName}))
                .Union(ChartData.ChartSeries.Select(a => new {Field = a.ZAxisTitle, Table = a.SourceTableName}))
                .Union(ChartData.ChartSeries.Select(a => new {Field = a.VAxisTitle, Table = a.SourceTableName}))
                .Union(ChartData.ChartSeries.Select(a => new {Field = a.WAxisTitle, Table = a.SourceTableName}))
                .Where(a => !String.IsNullOrEmpty(a.Field))
                .ToList();
            var foundFields =
                (from fieldname in fieldNames where _textBeforeEditing.Contains(fieldname.Field) select fieldname).ToList();

            if (foundFields.Count == 1 && foundFields[0].Field == _textBeforeEditing)
            {
                renameOperations.Add(new ChartRenameOperation
                {
                    ChartItemType = ChartRenameItemType.Field,
                    OldName = _textBeforeEditing,
                    NewName = textAfterEditing,
                    TableName = foundFields[0].Table
                });
            }
            else
            {
                int index = 1;
                var uniquefieldnames = foundFields.Select(a => a.Field).Distinct().ToList();
                var regextemp = uniquefieldnames.Aggregate(_textBeforeEditing, (current, a) => current.Replace(a, @"(?<" + "A" + index++ + @">.+)"));
                regextemp +="$";

                var match = Regex.Match(textAfterEditing, regextemp);

                for (int i = 0; i < uniquefieldnames.Count; i++)
                {
                    var r = uniquefieldnames[i];
                    var groupname = "A" + (i + 1);
                    var newnameGroup =
                        match.Groups[groupname].Captures[match.Groups[groupname].Captures.Count - 1];
                    if (!match.Groups[groupname].Success || newnameGroup.Value == r) continue;
                    var oldname = r;
                    var newname = newnameGroup.Value;
                    if (newname == oldname) continue;
                    if (newname.StartsWith("(") && newname.EndsWith(")"))
                        newname = newname.Substring(1, newname.Length - 2);
                    renameOperations.AddRange(foundFields.Where(a => a.Field == r)
                        .Select(a => a.Table).Distinct()
                        .Select(table => new ChartRenameOperation
                    {
                        ChartItemType = ChartRenameItemType.Field, OldName = oldname, NewName = newname, TableName = table
                    }));
                }
            }

            var duplicateCleaned = new List<ChartRenameOperation>();
            foreach (var ro in renameOperations
                .Where(ro => !duplicateCleaned
                    .Any(a => a.OldName == ro.OldName && a.ChartItemType == ro.ChartItemType && a.TableName == ro.TableName)))
            {
                duplicateCleaned.Add(ro);
            }

            if (duplicateCleaned.Count > 0) InvokeRenameEvent(new ChartRenameEventArgs { RenameOperations = duplicateCleaned });
            else PartialRefresh();
        }

        private void ChartVerticalAxisTitle_GotFocus(object sender, RoutedEventArgs e)
        {
            var s = sender as VerticalTextBox;
            _textBeforeEditing = s != null ? s.Text : string.Empty;
        }
        private void ChartVerticalAxisTitle_LostFocus(object sender, RoutedEventArgs e)
        {
            var s = sender as VerticalTextBox;
            var textAfterEditing = s != null ? s.Text : string.Empty;
            if (textAfterEditing == _textBeforeEditing) return;

            AxisTitleProcessing(textAfterEditing);
        }

        #endregion

        #region Cathegory processing
        private void ChartCathegoryTitle_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            _textBeforeEditing = s != null ? s.Text : string.Empty;
        }
        private void ChartCathegoryTitle_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            var textAfterEditing = s != null ? s.Text : string.Empty;
            textAfterEditing = textAfterEditing.Replace(">=", "≥").Replace("<=", "≤");

            if (textAfterEditing == _textBeforeEditing) return;
            var e = new ChartRenameEventArgs
            {
                RenameOperations = new List<ChartRenameOperation>
                        { 
                            new ChartRenameOperation
                            {
                                OldName = _textBeforeEditing,
                                NewName = textAfterEditing,
                                ChartItemType = ChartRenameItemType.Cathegory,
                                TableName = string.Empty
                            }
                        }
            };
            if (e.RenameOperations[0].TableName != null) InvokeRenameEvent(e);
        }

        #endregion

        private void InvokeRenameEvent(ChartRenameEventArgs e)
        {
            PartialRefresh();
            if (e.RenameOperations[0].TableName != null && InteractiveRename != null)
            {
                InteractiveRename(this, e);
            }
        }

        public static string GetPlainTextFromRichTextBox(IEnumerable<Block> textSource)
        {
            return textSource.OfType<Paragraph>()
                .SelectMany(block => block.Inlines.OfType<Run>())
                .Aggregate(string.Empty, (current, inline) => current + inline.Text);
        }

        public static List<InlineDescription> GetInlinesFromRichTextBox(IEnumerable<Block> textSource)
        {
            return textSource.OfType<Paragraph>()
                .SelectMany(block => block.Inlines.OfType<Run>())
                .Select(a=>new InlineDescription{Text =  a.Text, IsBold = a.FontWeight== FontWeights.Bold})
                .Where(b=>!String.IsNullOrEmpty(b.Text))
                .ToList();
        }

        public static string GetRegexPatternForTitleProcessing(List<InlineDescription> id)
        {
            int index = 1;
            return id.Where(a => a != null)
                .Aggregate(string.Empty, (current, inlineDescription) => current + (inlineDescription.IsBold ? @"(?<" + "A" + index++ + @">.+)" : Regex.Escape(inlineDescription.Text))) + "$";
        }
        
    }
    public enum ChartRenameItemType
    {
        Table,
        Field,
        Cathegory,
        Layer
    }
    public class ChartRenameEventArgs
    {
        public List<ChartRenameOperation> RenameOperations;
    }

    public class ChartRenameOperation
    {
        public ChartRenameItemType ChartItemType { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public string TableName { get; set; }
        public string LayerField { get; set; }
    }

    public class FoundText 
    {
        public string Text;
        public string TableName;
        public bool IsBold;
    }
    
    public class InlineDescription
    {
        public string Text;
        public bool IsBold;
    }
}
