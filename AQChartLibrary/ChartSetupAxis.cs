﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {

        public AxisRange BottomAxisRange;
        public AxisRange LeftAxisRange;
        public AxisRange RightAxisRange;
        public AxisRange TopAxisRange;
        public AxisRange ZAxisRange;
        public AxisRange WAxisRange;
        public AxisRange VAxisRange;

        public delegate void AxisRangeReadyDelegate(object o, AxisRangeReadyEventArgs e);

        public event AxisRangeReadyDelegate AxisRangeReady;

        private void SetupSeriesRanges()
        {
            var xAxisAr = new List<SeriesRangeInfo>();
            var yAxisAr = new List<SeriesRangeInfo>();
            var zAxisAr = new List<SeriesRangeInfo>();
            var wAxisAr = new List<SeriesRangeInfo>();
            var vAxisAr = new List<SeriesRangeInfo>();

            
            RightAxisRange = new AxisRange(VerticalAxisSymmetry) {Axis = AxisType.Y, IsSecondary = true};
            LeftAxisRange = new AxisRange(VerticalAxisSymmetry) { Axis = AxisType.Y};
            TopAxisRange = new AxisRange(HorizontalAxisSymmetry) {Axis = AxisType.X, IsSecondary = true};
            BottomAxisRange = new AxisRange(HorizontalAxisSymmetry) { Axis = AxisType.X};
            ZAxisRange = new AxisRange(false) {Axis = AxisType.Z};
            WAxisRange = new AxisRange(false) { Axis = AxisType.W };
            VAxisRange = new AxisRange(false) { Axis = AxisType.V };

            if (FixedAxises != null && FixedAxises.FixedAxisCollection != null)
            {
                BottomAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a=>a.Axis=="X");
                TopAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X2");
                LeftAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y");
                RightAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y2");
                ZAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Z");
                WAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "W");
                VAxisRange.FixedAxisDescription = FixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "V");
            }
            if (DataTables == null) return;

            BindSeriesToDirections(xAxisAr, yAxisAr, zAxisAr, wAxisAr, vAxisAr);

            var nominalXCount = xAxisAr.Count(a => a.IsNominal);
            var numericXCount = xAxisAr.Count - nominalXCount;

            if (nominalXCount > 0) BindSeriesToNominalAxises(xAxisAr, BottomAxisRange, TopAxisRange);
            if (numericXCount > 0) BindSeriesToNumericAxises(xAxisAr, BottomAxisRange, TopAxisRange);
            if (yAxisAr.Count > 0) BindSeriesToNumericAxises(yAxisAr, LeftAxisRange, RightAxisRange);
            if (zAxisAr.Count > 0) BindSeriesToVirtualZAxis(zAxisAr);
            if (wAxisAr.Count > 0) BindSeriesToVirtualWAxis(wAxisAr);
            if (vAxisAr.Count > 0) BindSeriesToVirtualVAxis(vAxisAr);
        }

        private void BindSeriesToNumericAxises(List<SeriesRangeInfo> axisAr, AxisRange primaryAxisRange, AxisRange secondaryAxisRange)
        {
            var names = (from n in axisAr select n.SourceColumnName).Distinct().ToList();
            if (names.Count == 0) return;
            if (names.Count == 1)
            {
                var seriesRangeInfo = axisAr.FirstOrDefault();
                BindAllSeriesToSingleAxis(axisAr, seriesRangeInfo != null && seriesRangeInfo.IsSecondary ? secondaryAxisRange : primaryAxisRange);
                return;
            }
            var nameGroups = axisAr.GroupBy(a => a.SourceColumnName).ToList();

            var ranges = nameGroups.Select(a => a.Max(b => b.Range)).ToList();
            var mins =   nameGroups.Select(a => a.Min(b => b.Min)).ToList();
            var maxs =   nameGroups.Select(a => a.Max(b => b.Max)).ToList();

            var secondaryAxisRequired = axisAr.Any(a => a.IsSecondary);
            var r = Math.Log10((maxs.Min() + ranges.Max()) / (mins.Min() + ranges.Min()));
            var singleAxis = !((!(r <= 0.3) && EnableSecondaryAxises) || secondaryAxisRequired);

            if (singleAxis) //Все на одной оси
            {
                BindAllSeriesToSingleAxis(axisAr, primaryAxisRange);
            }
            else
            {
                if (!secondaryAxisRequired)
                {
                    var tx = Math.Pow(10, r/2)*ranges.Min();
                    foreach (var a in (from x in nameGroups where x.Select(c => c.Range).Max() > tx select x))
                    {
                        foreach (var seriesRangeInfo in a)
                        {
                            primaryAxisRange.BoundSeries.Add(seriesRangeInfo);
                            seriesRangeInfo.BoundAxis = primaryAxisRange;
                        }
                    }
                    foreach (var a in (from x in nameGroups where x.Select(c => c.Range).Max() <= tx select x))
                    {
                        foreach (var seriesRangeInfo in a)
                        {
                            secondaryAxisRange.BoundSeries.Add(seriesRangeInfo);
                            seriesRangeInfo.BoundAxis = secondaryAxisRange;
                        }
                    }
                }
                else
                {
                    foreach (var a in (from x in axisAr where !x.IsSecondary select x))
                    {
                            primaryAxisRange.BoundSeries.Add(a);
                            a.BoundAxis = primaryAxisRange;
                    }
                    foreach (var a in (from x in axisAr where x.IsSecondary select x))
                    {
                        secondaryAxisRange.BoundSeries.Add(a);
                        a.BoundAxis = secondaryAxisRange;
                    }
                }
            }
        }

        private static void BindAllSeriesToSingleAxis(IEnumerable<SeriesRangeInfo> axisAr, AxisRange primaryAxisRange)
        {
            foreach (var a in axisAr)
            {
                primaryAxisRange.BoundSeries.Add(a);
                a.BoundAxis = primaryAxisRange;
            }
        }

        private void BindSeriesToNominalAxises(List<SeriesRangeInfo> xAxisAr, AxisRange primaryAxisRange, AxisRange secondaryAxisRange)
        {
            var uniqueColumns = (from x in xAxisAr where x.IsNominal select x.SourceColumnName).Distinct().ToList();
            if (uniqueColumns.Count > 2) throw new Exception("Более двух номинальных шкал не поддерживается");

            if (uniqueColumns.Count == 1)
            {
                foreach (var xa in from x in xAxisAr where x.IsNominal select x)
                {
                    xa.BoundAxis = primaryAxisRange;
                    primaryAxisRange.BoundSeries.Add(xa);
                }
            }

            if (uniqueColumns.Count() != 2) return;
            if (!EnableSecondaryAxises) throw new Exception("Требуется дополнительная ось, но она заблокирована");

            var b1 = (from x in xAxisAr where x.IsNominal && uniqueColumns.First() == x.SourceColumnName select x).Single();
            primaryAxisRange.BoundSeries.Add(b1);
            b1.BoundAxis = primaryAxisRange;
            var b2 = (from x in xAxisAr where x.IsNominal && uniqueColumns.Skip(1).First() == x.SourceColumnName select x).Single();
            secondaryAxisRange.BoundSeries.Add(b2);
            b2.BoundAxis = secondaryAxisRange;
        }

        private void BindSeriesToVirtualZAxis(IEnumerable<SeriesRangeInfo> zAxisAr)
        {
            foreach (var a in zAxisAr)
            {
                ZAxisRange.BoundSeries.Add(a);
                a.BoundAxis = ZAxisRange;
            }
        }
        private void BindSeriesToVirtualWAxis(IEnumerable<SeriesRangeInfo> wAxisAr)
        {
            foreach (var a in wAxisAr)
            {
                WAxisRange.BoundSeries.Add(a);
                a.BoundAxis = WAxisRange;
            }
        }

        private void BindSeriesToVirtualVAxis(IEnumerable<SeriesRangeInfo> vAxisAr)
        {
            foreach (var a in vAxisAr)
            {
                VAxisRange.BoundSeries.Add(a);
                a.BoundAxis = VAxisRange;
            }
        }

        private void BindSeriesToDirections(List<SeriesRangeInfo> xAxisAr, List<SeriesRangeInfo> yAxisAr, List<SeriesRangeInfo> zAxisAr, List<SeriesRangeInfo> wAxisAr, List<SeriesRangeInfo> vAxisAr)
        {
            foreach (var series in ChartData.ChartSeries)
            {
                var chartTable = DataTables.SingleOrDefault(a => a != null && a.TableName == series.SourceTableName);
                if (chartTable == null) continue;
                if (series.XColumnName != null)
                    xAxisAr.Add(new SeriesRangeInfo
                        {
                            SourceSeries = series,
                            AxisTitle = series.XAxisTitle,
                            SeriesName = series.SeriesTitle,
                            SourceColumnName = series.XColumnName,
                            SourceTable = chartTable,
                            IsNominal = series.XIsNominal,
                            DataType = chartTable.GetDataType(series.XColumnName),
                            ColorColumnName = series.ColorColumnName,
                            MarkerSizeColumnName = series.MarkerSizeColumnName,
                            MarkerLabelColumnName = series.LabelColumnName
                        });
                if (series.YColumnName != null)
                    yAxisAr.Add(new SeriesRangeInfo
                        {
                            SourceSeries = series,
                            AxisTitle = series.YAxisTitle,
                            SeriesName = series.SeriesTitle,
                            SourceColumnName = series.YColumnName,
                            SourceTable = chartTable,
                            IsNominal = series.YIsNominal,
                            IsSecondary = series.YIsSecondary,
                            DataType = chartTable.GetDataType(series.YColumnName),
                            ColorColumnName = series.ColorColumnName,
                            MarkerSizeColumnName = series.MarkerSizeColumnName,
                            MarkerLabelColumnName = series.LabelColumnName
                        });
                if (series.ZColumnName != null)
                    zAxisAr.Add(new SeriesRangeInfo
                        {
                            SourceSeries = series,
                            AxisTitle = series.ZAxisTitle,
                            SeriesName = series.SeriesTitle,
                            SourceColumnName = series.ZColumnName,
                            SourceTable = chartTable,
                            IsNominal = false,
                            DataType = chartTable.GetDataType(series.ZColumnName),
                            ColorColumnName = series.ColorColumnName,
                            MarkerSizeColumnName = series.MarkerSizeColumnName,
                            MarkerLabelColumnName = series.LabelColumnName
                        });
                if (series.WColumnName != null)
                    wAxisAr.Add(new SeriesRangeInfo
                    {
                        SourceSeries = series,
                        AxisTitle = series.WAxisTitle,
                        SeriesName = series.SeriesTitle,
                        SourceColumnName = series.WColumnName,
                        SourceTable = chartTable,
                        IsNominal = false,
                        DataType = chartTable.GetDataType(series.WColumnName),
                        ColorColumnName = series.ColorColumnName,
                        MarkerSizeColumnName = series.MarkerSizeColumnName,
                        MarkerLabelColumnName = series.LabelColumnName
                    });
                if (series.VColumnName != null)
                    vAxisAr.Add(new SeriesRangeInfo
                    {
                        SourceSeries = series,
                        AxisTitle = series.VAxisTitle,
                        SeriesName = series.SeriesTitle,
                        SourceColumnName = series.VColumnName,
                        SourceTable = chartTable,
                        IsNominal = false,
                        DataType = chartTable.GetDataType(series.VColumnName),
                        ColorColumnName = series.ColorColumnName,
                        MarkerSizeColumnName = series.MarkerSizeColumnName,
                        MarkerLabelColumnName = series.LabelColumnName
                    });
            }
        }

        private void SetupAxis()
        {
            var showBorder = ShowHorizontalGridlines || ShowVerticalAxises || ShowHorizontalGridlines || ShowVerticalGridlines;
            var leftBorder = showBorder ? 1 : 0;
            var rightBorder = showBorder ? 1 : 0;
            var bottomBorder = showBorder ? 1 : 0;
            var topBorder = showBorder ? 1 : 0;
            RightAxis.EnableCellCondense = EnableCellCondense;
            LeftAxis.EnableCellCondense = EnableCellCondense;
            TopAxis.EnableCellCondense = EnableCellCondense;
            BottomAxis.EnableCellCondense = EnableCellCondense;
            ChartBorder.BorderThickness = new Thickness(leftBorder, topBorder, rightBorder, bottomBorder);

            if (TopAxisRange.BoundSeries.Count > 0)
            {
                UpAxisTitle.Visibility =  Visibility.Visible;
                UpAxisTitle.Text = (from x in TopAxisRange.BoundSeries select x.AxisTitle).Distinct().GetConcatenated("; ");
                TopAxis.IsNominal = TopAxisRange.IsNominal;
                TopAxis.DataLabels = TopAxisRange.GetLabels();
                TopAxis.Visibility = ShowHorizontalAxises ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                UpAxisTitle.Visibility = Visibility.Collapsed;
                TopAxis.Visibility = Visibility.Collapsed;
            }
            if (BottomAxisRange.BoundSeries.Count > 0)
            {
                BottomAxisTitle.Visibility =  Visibility.Visible;
                BottomAxisTitle.Text = (from x in BottomAxisRange.BoundSeries select x.AxisTitle).Distinct().GetConcatenated("; ");
                BottomAxis.IsNominal = BottomAxisRange.IsNominal ||
                                       BottomAxisRange.BoundSeries.Any(a => a.DataType == "String");
                BottomAxis.DataLabels = BottomAxisRange.GetLabels();
                BottomAxis.Visibility = ShowHorizontalAxises ? Visibility.Visible : Visibility.Collapsed;
                BottomAxisTitle.Visibility = ShowHorizontalAxises ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                BottomAxisTitle.Visibility = Visibility.Collapsed;
                BottomAxis.Visibility = Visibility.Collapsed;
            }

            if (RightAxisRange.BoundSeries.Count > 0)
            {
                RightAxisTitle.Visibility = Visibility.Visible;
                RightAxis.IsNominal = RightAxisRange.IsNominal || RightAxisRange.BoundSeries.Any(a => a.DataType == "String");
                RightAxis.DataLabels = RightAxisRange.GetLabels();
                RightAxis.Visibility = ShowVerticalAxises ? Visibility.Visible : Visibility.Collapsed;
                RightAxisTitle.Visibility = ShowVerticalAxises ? Visibility.Visible : Visibility.Collapsed;
                string text = (RightAxisRange.BoundSeries.Count > 1)
                                  ? (from x in RightAxisRange.BoundSeries select x.AxisTitle).Distinct().GetConcatenated("\r\n")
                                  : RightAxisRange.BoundSeries.Single().SeriesName;
                RightAxisTitle.Text = text;
            }
            else
            {
                RightAxisTitle.Visibility = Visibility.Collapsed;
                RightAxis.Visibility = Visibility.Collapsed;
            }

            if (LeftAxisRange.BoundSeries.Count > 0)
            {
                LeftAxisTitle.Visibility = Visibility.Visible;
                LeftAxis.IsNominal = LeftAxisRange.IsNominal || LeftAxisRange.BoundSeries.Any(a => a.DataType == "String");
                LeftAxis.DataLabels = LeftAxisRange.GetLabels();
                LeftAxis.Visibility = ShowVerticalAxises ? Visibility.Visible : Visibility.Collapsed;
                LeftAxisTitle.Visibility = ShowVerticalAxises ? Visibility.Visible : Visibility.Collapsed;

                string text = (LeftAxisRange.BoundSeries.Count > 1)
                                  ? (from x in LeftAxisRange.BoundSeries select x.AxisTitle).Distinct().GetConcatenated("\r\n")
                                  : LeftAxisRange.BoundSeries.Single().AxisTitle;
                LeftAxisTitle.Text = text;
            }
            else
            {
                LeftAxisTitle.Visibility = Visibility.Collapsed;
                LeftAxis.Visibility = Visibility.Collapsed;
            }

            if (DoubleVerticalAxis && RightAxisRange.BoundSeries.Count == 0 && LeftAxisRange.BoundSeries.Count > 0)
            {
                RightAxisRange.BoundSeries = LeftAxisRange.BoundSeries;
                RightAxisRange.IsNominal = LeftAxisRange.IsNominal || LeftAxisRange.BoundSeries.Any(a => a.DataType == "String");
                RightAxis.DataLabels = LeftAxisRange.GetLabels();
                RightAxis.IsNominal = LeftAxis.IsNominal;
                RightAxis.Visibility = ShowVerticalAxises ? Visibility.Visible : Visibility.Collapsed;
                RightAxisRange.FixedAxisDescription = LeftAxisRange.FixedAxisDescription;
                RightAxisRange.Axis = LeftAxisRange.Axis;
                RightAxisRange.HideNegative = LeftAxisRange.HideNegative;
                RightAxisRange.IsSymmetrical = LeftAxisRange.IsSymmetrical;
                RightAxisRange.IsValid = LeftAxisRange.IsValid;
                RightAxisRange.RealSteps = LeftAxisRange.RealSteps;
                RightAxisRange.SourceMinimum = LeftAxisRange.SourceMinimum;
                RightAxisRange.SourceMaximum = LeftAxisRange.SourceMaximum;
                RightAxisRange.DisplayStep = LeftAxisRange.DisplayStep;
                RightAxisRange.PartStep = LeftAxisRange.PartStep;
                RightAxisRange.DataType = LeftAxisRange.DataType;
                RightAxis.Tag = LeftAxis.Tag;
            }
        }

        private void SetupAxisRanges()
        {
            var maxLabelsOnVerticalAxis = (int)Math.Floor(Height / 40);
            var maxLabelsOnHorizontalAxis = (int)Math.Floor(Width / 30);

            BottomAxisRange.HideNegative = HideNegativeX;
            TopAxisRange.HideNegative = HideNegativeX;
            RightAxisRange.Setup(maxLabelsOnVerticalAxis);
            LeftAxisRange.Setup(maxLabelsOnVerticalAxis);
            TopAxisRange.Setup(maxLabelsOnHorizontalAxis);
            BottomAxisRange.Setup(maxLabelsOnHorizontalAxis);
            ZAxisRange.Setup(7);
            WAxisRange.Setup(7);
            VAxisRange.Setup(7);
            if (AxisRangeReady != null)
            {
                AxisRangeReady.Invoke(this, new AxisRangeReadyEventArgs
                {
                    RightAxisRange = RightAxisRange,
                    LeftAxisRange = LeftAxisRange,
                    BottomAxisRange = BottomAxisRange,
                    TopAxisRange = TopAxisRange,
                    ZAxisRange = ZAxisRange,
                    WAxisRange = WAxisRange,
                    VAxisRange = VAxisRange,
                });
            }
        }

        public class AxisRangeReadyEventArgs
        {
            public AxisRange RightAxisRange;
            public AxisRange LeftAxisRange;
            public AxisRange BottomAxisRange;
            public AxisRange TopAxisRange;
            public AxisRange ZAxisRange;
            public AxisRange WAxisRange;
            public AxisRange VAxisRange;
        }
    }
}