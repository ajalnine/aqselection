﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private Brush _invertedMinorGridLinesBrush, _minorGridLinesBrush, _majorGridLinesBrush, _invertedMajorGridlinesBrush;
        private double _minorGridLinesThickness, _majorGridLinesThickness;
        private void DrawGridLines()
        {
            GridLinesSurface.Children.Clear();
            RightHelperGridLines.Children.Clear();
            TopHelperGridLines.Children.Clear();
            BottomHelperGridLines.Children.Clear();
            LeftHelperGridLines.Children.Clear();
            var h = GridLinesSurface.ActualHeight;
            var w = GridLinesSurface.ActualWidth;
            if (ContrastGridLines)
            {
                _invertedMinorGridLinesBrush = new SolidColorBrush{ Color = Color.FromArgb(0xff, 0x50, 0x50, 0x50) };
                _minorGridLinesBrush = new SolidColorBrush { Color = Color.FromArgb(255, 0x80, 0x80, 0x80) };
                _invertedMajorGridlinesBrush = new SolidColorBrush { Color = Color.FromArgb(255, 0xff, 0xff, 0xff) };
                _majorGridLinesBrush = new SolidColorBrush { Color = Color.FromArgb(255, 0x0, 0x0, 0x0) };

                _minorGridLinesThickness = 0.5;
                _majorGridLinesThickness = 0.8;
            }
            else
            {
                _invertedMinorGridLinesBrush = new SolidColorBrush { Color = Color.FromArgb(0xff, 0x40, 0x40, 0x40) };
                _minorGridLinesBrush = new SolidColorBrush { Color = Color.FromArgb(255, 230, 230, 230) };
                _invertedMajorGridlinesBrush = new SolidColorBrush { Color = Color.FromArgb(255, 0x80, 0x80, 0x80) };
                _majorGridLinesBrush = defaultGridBrush;

                _minorGridLinesThickness = 0.4;
                _majorGridLinesThickness = 0.5;
            }

            if (RightAxisRange.IsValid) DrawGridLines(w, h, RightAxisRange.RealSteps, RightAxisRange.IsNominal, Orientation.Horizontal, GridLinesSurface);
            if (LeftAxisRange.IsValid) DrawGridLines(w, h, LeftAxisRange.RealSteps, LeftAxisRange.IsNominal, Orientation.Horizontal, GridLinesSurface);
            if (TopAxisRange.IsValid) DrawGridLines(w, h, TopAxisRange.RealSteps, TopAxisRange.IsNominal, Orientation.Vertical, GridLinesSurface);
            if (BottomAxisRange.IsValid)DrawGridLines(w, h, BottomAxisRange.RealSteps, BottomAxisRange.IsNominal, Orientation.Vertical, GridLinesSurface);
            if (!ShowHelperGridlines) return;
            if (_rightHelperExists) DrawGridLines(RightHelperSurface.ActualWidth, RightHelperSurface.ActualHeight, RightAxisRange.RealSteps, RightAxisRange.IsNominal, Orientation.Horizontal, RightHelperGridLines);
            if (_leftHelperExists) DrawGridLines(LeftHelperSurface.ActualWidth, LeftHelperSurface.ActualHeight, LeftAxisRange.RealSteps, LeftAxisRange.IsNominal, Orientation.Horizontal, LeftHelperGridLines);
            if (_topHelperExists) DrawGridLines(TopHelperSurface.ActualWidth, TopHelperSurface.ActualHeight, Math.Abs(TopAxisRange.RealSteps) < 1e-8 ? BottomAxisRange.RealSteps : TopAxisRange.RealSteps, TopAxisRange.IsNominal, Orientation.Vertical, TopHelperGridLines);
            if (_bottomHelperExists) DrawGridLines(BottomHelperSurface.ActualWidth, BottomHelperSurface.ActualHeight, BottomAxisRange.RealSteps, BottomAxisRange.IsNominal, Orientation.Vertical, BottomHelperGridLines);

            if (_topHelperExists) TopHelperBorder.BorderThickness = new Thickness(1, 1, 1, 1);
            if (_bottomHelperExists) BottomHelperBorder.BorderThickness = new Thickness(1, 1, 1, 1);
            if (_leftHelperExists) LeftHelperBorder.BorderThickness = new Thickness(1, 1, 1, 1);
            if (_rightHelperExists) RightHelperBorder.BorderThickness = new Thickness(1, 1, 1, 1);
            
        }

        private void DrawGridLines(double width, double height, double number, bool isNominal, Orientation linesOrientation, Canvas panel)
        {
            if (Math.Abs(number) < 1e-8) return;
            double step;

            double  rightMajorTickOffset = 0.0d,
                    rightMinorTickOffset = 0.0d,
                    leftMajorTickOffset = 0.0d,
                    leftMinorTickOffset = 0.0d,
                    topMinorTickOffset = 0.0d,
                    topMajorTickOffset = 0.0d,
                    bottomMinorTickOffset = 0.0d,
                    bottomMajorTickOffset = 0.0d;

            if (LeftAxis.Visibility == Visibility.Visible && ShowTickMarks)
            {
                leftMajorTickOffset = 3.0d;
                leftMinorTickOffset = 2.0d;
            }

            if (RightAxis.Visibility == Visibility.Visible && ShowTickMarks)
            {
                rightMajorTickOffset = 3.0d;
                rightMinorTickOffset = 2.0d;
            }

            if (TopAxis.Visibility == Visibility.Visible && ShowTickMarks)
            {
                topMajorTickOffset = 3.0d;
                topMinorTickOffset = 2.0d;
            }

            if (BottomAxis.Visibility == Visibility.Visible && ShowTickMarks)
            {
                bottomMajorTickOffset = 3.0d;
                bottomMinorTickOffset = 2.0d;
            }

            switch (linesOrientation)
            {
                case Orientation.Vertical:
                    if (ShowVerticalGridlines)
                    {
                        step = width / number;
                        if (step > 0)
                        {
                            for (double x = 0; Math.Round(x, 0) <= Math.Round(width, 0); x += step)
                            {
                                if (x >= 0 && step > 5)
                                    panel.Children.Add(new Line
                                    {
                                        X1 = x,
                                        X2 = x,
                                        Y1 = -topMajorTickOffset,
                                        Y2 = height + bottomMajorTickOffset,
                                        Stroke = Inverted ? _invertedMajorGridlinesBrush
                                                              : _majorGridLinesBrush,
                                        StrokeThickness = _majorGridLinesThickness,
                                        UseLayoutRounding = true
                                    });
                                if (step > 40 && !isNominal && !HideMinorGridLines && Math.Round(x, 0) < Math.Round(width, 0))
                                {
                                    for (double x2 = x + step / NumberOfMinorVerticalGridLines;
                                         Math.Round(x2, 0) < Math.Round(x + step, 0);
                                         x2 += step / NumberOfMinorVerticalGridLines)
                                    {
                                        panel.Children.Add(new Line
                                        {
                                            X1 = x2,
                                            X2 = x2,
                                            Y1 = -topMinorTickOffset,
                                            Y2 = height + bottomMinorTickOffset,
                                            Stroke = Inverted ? _invertedMinorGridLinesBrush
                                                              : _minorGridLinesBrush ,
                                            StrokeThickness = _minorGridLinesThickness,
                                            UseLayoutRounding = true
                                        });
                                    }
                                }
                            }
                        }
                    }
                    break;

                case Orientation.Horizontal:
                    if (ShowHorizontalGridlines)
                    {
                        step = height / number;
                        if (step > 0)
                        {
                            for (double x = 0; Math.Round(x, 0) <= Math.Round(height, 0); x += step)
                            {
                                if (x >= 0 && step > 5)
                                    panel.Children.Add(new Line
                                    {
                                        X1 = -leftMajorTickOffset,
                                        X2 = width + rightMajorTickOffset,
                                        Y1 = x,
                                        Y2 = x,
                                        Stroke = Inverted ? _invertedMajorGridlinesBrush
                                                              : _majorGridLinesBrush,
                                        StrokeThickness = _majorGridLinesThickness,
                                        UseLayoutRounding = true
                                    });
                                if (step > 40 && !HideMinorGridLines && Math.Round(x, 0) < Math.Round(height, 0))
                                {
                                    for (double x2 = x + step / NumberOfMinorHorizontalGridLines;
                                         Math.Round(x2, 0) <= Math.Round(x + step * 4 / NumberOfMinorHorizontalGridLines, 0);
                                         x2 += step / NumberOfMinorHorizontalGridLines)
                                    {
                                        panel.Children.Add(new Line
                                        {
                                            X1 = -leftMinorTickOffset,
                                            X2 = width + rightMinorTickOffset,
                                            Y1 = x2,
                                            Y2 = x2,
                                            Stroke = Inverted ? _invertedMinorGridLinesBrush
                                                              : _minorGridLinesBrush,
                                            StrokeThickness = _minorGridLinesThickness,
                                            UseLayoutRounding = true
                                        });
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void DrawZero(Canvas panel)
        {
            var h = panel.ActualHeight;
            var w = panel.ActualWidth;

            if (LeftAxisRange.DataType == "Double" && ShowHorizontalZero)
            {
                var y = LeftAxisRange.TransformValue(0.0d, LeftAxisRange.BoundSeries.FirstOrDefault()) ?? 0.0d;
                if (y > 0 && y < 1)
                    panel.Children.Add(new Line
                    {
                        X1 = 0.0d,
                        X2 = w,
                        Y1 = h - h * y,
                        Y2 = h - h * y,
                        Stroke = defaultGridBrush,
                        StrokeThickness = 1,
                        UseLayoutRounding = true
                    });
            }
            if (RightAxisRange.DataType == "Double" && ShowHorizontalZero)
            {
                var y = RightAxisRange.TransformValue(0.0d, RightAxisRange.BoundSeries.FirstOrDefault()) ?? 0.0d;
                if (y > 0 && y < 1)
                    panel.Children.Add(new Line
                    {
                        X1 = 0.0d,
                        X2 = w,
                        Y1 = h - h * y,
                        Y2 = h - h * y,
                        Stroke = defaultGridBrush,
                        StrokeThickness = 1,
                        UseLayoutRounding = true
                    });
            }
            if (BottomAxisRange.DataType == "Double" && ShowVerticalZero)
            {
                var x = BottomAxisRange.TransformValue(0.0d, BottomAxisRange.BoundSeries.FirstOrDefault()) ?? 0.0d;
                if (x > 0 && x < 1)
                    panel.Children.Add(new Line
                    {
                        X1 = w * x,
                        X2 = w * x,
                        Y1 = 0,
                        Y2 = h,
                        Stroke = defaultGridBrush,
                        StrokeThickness = 1,
                        UseLayoutRounding = true
                    });
            }
            if (TopAxisRange.DataType == "Double" && ShowVerticalZero)
            {
                var x = TopAxisRange.TransformValue(0.0d, TopAxisRange.BoundSeries.FirstOrDefault()) ?? 0.0d;
                if (x > 0 && x < 1)
                    panel.Children.Add(new Line
                    {
                        X1 = w * x,
                        X2 = w * x,
                        Y1 = 0,
                        Y2 = h,
                        Stroke = defaultGridBrush,
                        StrokeThickness = 1,
                        UseLayoutRounding = true
                    });
            }
        }
    }
}