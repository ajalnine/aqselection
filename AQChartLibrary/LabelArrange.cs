﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace AQChartLibrary
{
    public class PieLabelArrange : LabelArrange
    {
        #region Pie

        public class PieLabelArrangeDescription
        {
            public int Key;
            public int ConstraintKey;
            public double CircleCenterX;
            public double CircleCenterY;
            public double circleMaxRadius;
            public double circleMinRadius;
            public double angle;
            public double angleRange;
            public string LabelText;
            public Brush LabelBrush;
            public Brush InvertedLabelBrush;
            public bool WhiteBG;
            public double FontSize;
            public double? MaxWidth;
            public bool AllowInternal;
            public bool AllowColorChange;
            public bool OnlyInternal;
        }
        
        public void AddPieGraph(PieLabelArrangeDescription plad)
        {
            if (double.IsNaN(plad.FontSize)) return;

            var node = new LabelPlaces();
            node.Key = plad.Key;
            node.ConstraintKey = plad.ConstraintKey;

            Grid container = new Grid();
            var tb = new TextBlock
            {
                Text = plad.LabelText,
                FontSize = plad.FontSize,
                Foreground = plad.LabelBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };
            if (plad.MaxWidth.HasValue) tb.MaxWidth = plad.MaxWidth.Value;
            tb.UpdateLayout();
            if (plad.WhiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = Chart.GetWhiteBackGround(!plad.WhiteBG),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                container.Children.Add(rect);
            }
            container.Children.Add(tb);
            node.sourceObject = container;


            Grid container2 = new Grid();
            var tb2 = new TextBlock
            {
                Text = plad.LabelText,
                FontSize = plad.FontSize,
                Foreground = plad.InvertedLabelBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };
            if (plad.MaxWidth.HasValue) tb2.MaxWidth = plad.MaxWidth.Value;
            tb2.UpdateLayout();
            if (plad.WhiteBG)
            {
                var rect2 = new Rectangle
                {
                    Fill = Chart.GetColorBackGround(((SolidColorBrush)plad.LabelBrush).Color),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                container2.Children.Add(rect2);
            }
            container2.Children.Add(tb2);
            node.invertedSourceObject = container2;
            
            node.Places = new List<LabelPlace>();

            var outRX = tb.ActualWidth / 2 + 2;
            var outRY = tb.ActualHeight / 2 + 2;

            var sinA = Math.Sin(plad.angle);
            var cosA = Math.Cos(plad.angle);
            var inOffset = new Point(plad.CircleCenterX + plad.circleMaxRadius * sinA, plad.CircleCenterY - plad.circleMaxRadius * cosA);
            var outOffset = new Point(outRX * sinA * 1.3, outRY * cosA * 1.3);
            var outOffsetEx1 = new Point(outRX * sinA * 2.5, outRY * cosA * 2.5);
            var outOffsetEx2 = new Point(outRX * sinA * 5, outRY * cosA * 4);
            var outOffsetEx3 = new Point(outRX * sinA * 7.5, outRY * cosA * 6);
            
            var surfaceX = inOffset.X + outOffset.X;
            var surfaceY = inOffset.Y - outOffset.Y;
            var surfaceXEx1 = inOffset.X + outOffsetEx1.X;
            var surfaceYEx1 = inOffset.Y - outOffsetEx1.Y;
            var surfaceXEx2 = inOffset.X + outOffsetEx2.X;
            var surfaceYEx2 = inOffset.Y - outOffsetEx2.Y;
            var surfaceXEx3 = inOffset.X + outOffsetEx3.X;
            var surfaceYEx3 = inOffset.Y - outOffsetEx3.Y;

            if (plad.AllowInternal)
            {
                var inOffsetArea = new Point(plad.CircleCenterX +  (plad.circleMinRadius + plad.circleMaxRadius) * sinA * 0.5,
                    plad.CircleCenterY - (plad.circleMinRadius + plad.circleMaxRadius) * cosA *0.5);
                var surfaceXEx4 = inOffsetArea.X;
                var surfaceYEx4 = inOffsetArea.Y;
                
                node.Places.Add(new LabelPlace
                {
                    Penalty = plad.OnlyInternal ? 1 : 10,
                    x1 = surfaceXEx4 - outRX,
                    x2 = surfaceXEx4 + outRX,
                    y1 = surfaceYEx4 - outRY,
                    y2 = surfaceYEx4 + outRY,
                    AnchorX = inOffsetArea.X,
                    AnchorY = inOffsetArea.Y,
                    LineRequired = false,
                    LineBrush = plad.LabelBrush,
                    Inverse = true
                });
            }

            if (!plad.OnlyInternal)
            {
                node.Places.Add(new LabelPlace { Penalty = 1, x1 = surfaceX - outRX, x2 = surfaceX + outRX, y1 = surfaceY - outRY, y2 = surfaceY + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineBrush = plad.LabelBrush });
                node.Places.Add(new LabelPlace { Penalty = 2, x1 = surfaceXEx1 - outRX, x2 = surfaceXEx1 + outRX, y1 = surfaceYEx1 - outRY, y2 = surfaceYEx1 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineRequired = true, LineBrush = plad.LabelBrush });
                node.Places.Add(new LabelPlace { Penalty = 4, x1 = surfaceXEx2 - outRX, x2 = surfaceXEx2 + outRX, y1 = surfaceYEx2 - outRY, y2 = surfaceYEx2 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineRequired = true, LineBrush = plad.LabelBrush });
                node.Places.Add(new LabelPlace { Penalty = 7, x1 = surfaceXEx3 - outRX, x2 = surfaceXEx3 + outRX, y1 = surfaceYEx3 - outRY, y2 = surfaceYEx3 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineRequired = true, LineBrush = plad.LabelBrush });

                var angle2 = plad.angle - plad.angleRange / 2;
                var inOffset2 = new Point(plad.CircleCenterX + plad.circleMaxRadius * Math.Sin(angle2),
                    plad.CircleCenterY - plad.circleMaxRadius * Math.Cos(angle2));
                var outOffset2 = new Point(outRX * Math.Sin(angle2) * 1.3, outRY * Math.Cos(angle2) * 1.3);
                var surfaceX2 = inOffset2.X + outOffset2.X;
                var surfaceY2 = inOffset2.Y - outOffset2.Y;
                node.Places.Add(new LabelPlace
                {
                    Penalty = 5, x1 = surfaceX2 - outRX, x2 = surfaceX2 + outRX, y1 = surfaceY2 - outRY,
                    y2 = surfaceY2 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y
                });

                var outOffset4 = new Point(outRX * Math.Sin(angle2) * 2.5, outRY * Math.Cos(angle2) * 2.5);
                var surfaceX4 = inOffset2.X + outOffset4.X;
                var surfaceY4 = inOffset2.Y - outOffset4.Y;
                node.Places.Add(new LabelPlace
                {
                    Penalty = 5, x1 = surfaceX4 - outRX, x2 = surfaceX4 + outRX, y1 = surfaceY4 - outRY,
                    y2 = surfaceY4 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineRequired = true,
                    LineBrush = plad.LabelBrush
                });

                var angle3 = plad.angle + plad.angleRange / 2;
                var inOffset3 = new Point(plad.CircleCenterX + plad.circleMaxRadius * Math.Sin(angle3),
                    plad.CircleCenterY - plad.circleMaxRadius * Math.Cos(angle3));
                var outOffset3 = new Point(outRX * Math.Sin(angle3) * 1.3, outRY * Math.Cos(angle3) * 1.3);
                var surfaceX3 = inOffset3.X + outOffset3.X;
                var surfaceY3 = inOffset3.Y - outOffset3.Y;
                node.Places.Add(new LabelPlace
                {
                    Penalty = 5, x1 = surfaceX3 - outRX, x2 = surfaceX3 + outRX, y1 = surfaceY3 - outRY,
                    y2 = surfaceY3 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y
                });

                var outOffset5 = new Point(outRX * Math.Sin(angle3) * 2.5, outRY * Math.Cos(angle3) * 2.5);
                var surfaceX5 = inOffset2.X + outOffset5.X;
                var surfaceY5 = inOffset2.Y - outOffset5.Y;
                node.Places.Add(new LabelPlace
                {
                    Penalty = 5, x1 = surfaceX5 - outRX, x2 = surfaceX5 + outRX, y1 = surfaceY5 - outRY,
                    y2 = surfaceY5 + outRY, AnchorX = inOffset.X, AnchorY = inOffset.Y, LineRequired = true,
                    LineBrush = plad.LabelBrush
                });
            }

            if (node != null) AddGraph(node);
        }
    }
    #endregion

    #region Columns

    public enum SmallLabelOverwork
    {
        SmallDefault, SmallOver, SmallSkip
    }

    public class ColumnLabelArrangeDescription
    {
        public int Key;
        public int ConstraintKey;
        public double x;
        public double y;
        public double Width;
        public double Height;
        public string LabelText;
        public Brush LabelBrush;
        public Brush InvertedLabelBrush;
        public double FontSize;
        public double? MaxWidth;
        public bool AllowInternal;
        public bool AllowColorChange;
        public bool NegativeColumns;
        public bool WhiteBG;
        public SmallLabelOverwork SmallOverwork;
    }
    

    public class ColumnLabelArrange : LabelArrange
    {
        public void AddColumnGraph(ColumnLabelArrangeDescription clad)
        {
            if (double.IsNaN(clad.FontSize)) return;

            var node = new LabelPlaces();
            node.Key = clad.Key;
            node.ConstraintKey = clad.ConstraintKey;

            Grid container = new Grid();
            var tb = new TextBlock
            {
                Text = clad.LabelText,
                FontSize = clad.FontSize,
                Foreground = clad.InvertedLabelBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsHitTestVisible = false,
            };
            tb.UpdateLayout();
            var maxRequired = clad.MaxWidth.HasValue && tb.ActualWidth > clad.MaxWidth.Value;
            if (maxRequired)
            {
                tb.Width = clad.MaxWidth.Value;
            }
            tb.UpdateLayout();
            var rect = new Rectangle
            {
                Fill = Chart.GetWhiteBackGround(!clad.WhiteBG),
                Width = (maxRequired) ? clad.MaxWidth.Value : tb.ActualWidth,
                Height = tb.ActualHeight + 2,
                Margin = new Thickness(-4, -1, -4, -1),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsHitTestVisible = false
            };
            container.Children.Add(rect);
            container.Children.Add(tb);
            node.sourceObject = container;
            container.UpdateLayout();
            if (tb.ActualHeight > clad.Height && clad.AllowInternal &&
                clad.SmallOverwork == SmallLabelOverwork.SmallSkip) return;

            Grid container2 = new Grid();
            var tb2 = new TextBlock
            {
                Text = clad.LabelText,
                FontSize = clad.FontSize,
                Foreground = clad.LabelBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsHitTestVisible = false
            };
            if (maxRequired)
            {
                tb2.Width = clad.MaxWidth.Value;
            }
            tb2.UpdateLayout();
            container2.Children.Add(tb2);
            node.invertedSourceObject = container2;
            container2.UpdateLayout();
            node.Places = new List<LabelPlace>();

            var outRX = maxRequired? clad.MaxWidth.Value /2 + 1 : tb.ActualWidth / 2 + 1;
            var outRY = tb.ActualHeight / 2 + 1;

            if (clad.AllowInternal || clad.SmallOverwork == SmallLabelOverwork.SmallDefault)
            {
                
                node.Places.Add(new LabelPlace { Penalty = 1, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2 - outRY, y2 = clad.y + clad.Height / 2 + outRY, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                if (clad.SmallOverwork == SmallLabelOverwork.SmallOver)
                {
                    if (clad.Height / tb.ActualHeight > 3)
                    {
                        node.Places.Add(new LabelPlace
                        {
                            Penalty = 4, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX,
                            y1 = clad.y + clad.Height / 4 - outRY, y2 = clad.y + clad.Height / 4 + outRY,
                            AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2,
                            LineBrush = clad.LabelBrush, Inverse = true
                        });
                        node.Places.Add(new LabelPlace
                        {
                            Penalty = 4, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX,
                            y1 = clad.y + 3 * clad.Height / 4 - outRY, y2 = clad.y + 3 * clad.Height / 4 + outRY,
                            AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2,
                            LineBrush = clad.LabelBrush, Inverse = true
                        });
                    }

                    node.Places.Add(new LabelPlace
                    {
                        Penalty = 5, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX,
                        y1 = clad.y - outRY * 2, y2 = clad.y, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y,
                        LineBrush = clad.LabelBrush, Inverse = !clad.AllowColorChange
                    });
                }
            }
            else
            {
                if (clad.NegativeColumns)
                {
                    node.Places.Add(new LabelPlace { Penalty = 1, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y, y2 = clad.y + outRY * 2, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush });
                    node.Places.Add(new LabelPlace { Penalty = 2, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + outRY * 2, y2 = clad.y + outRY * 4, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 3, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + outRY * 4, y2 = clad.y + outRY * 6, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 4, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + outRY * 6, y2 = clad.y + outRY * 8, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 5, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + outRY * 8, y2 = clad.y + outRY * 10, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 6, x1 = clad.x + clad.Width / 2 - outRX * 2, x2 = clad.x + clad.Width / 2, y1 = clad.y + outRY * 0.5, y2 = clad.y + outRY * 2.5, AnchorX = clad.x + clad.Width / 4, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 6, x1 = clad.x + clad.Width / 2, x2 = clad.x + clad.Width / 2 + 2 * outRX, y1 = clad.y + outRY * 0.5, y2 = clad.y + outRY * 2.5, AnchorX = clad.x + 3 * clad.Width / 4, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 10, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2 - outRY, y2 = clad.y + clad.Height / 2 + outRY, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                    node.Places.Add(new LabelPlace { Penalty = 15, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2 - 2 * outRY, y2 = clad.y + clad.Height / 2, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                    node.Places.Add(new LabelPlace { Penalty = 15, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2, y2 = clad.y + clad.Height / 2 + 2 * outRY, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                }
                else
                {
                    node.Places.Add(new LabelPlace { Penalty = 1, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y - outRY * 2, y2 = clad.y, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush });
                    node.Places.Add(new LabelPlace { Penalty = 2, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y - outRY * 4, y2 = clad.y - outRY * 2, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 3, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y - outRY * 6, y2 = clad.y - outRY * 4, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 4, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y - outRY * 8, y2 = clad.y - outRY * 6, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 5, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y - outRY * 10, y2 = clad.y - outRY * 8, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 6, x1 = clad.x + clad.Width / 2 - outRX * 2, x2 = clad.x + clad.Width / 2, y1 = clad.y - outRY * 2.5, y2 = clad.y - outRY * 0.5, AnchorX = clad.x + clad.Width / 4, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 6, x1 = clad.x + clad.Width / 2, x2 = clad.x + clad.Width / 2 + 2 * outRX, y1 = clad.y - outRY * 2.5, y2 = clad.y - outRY * 0.5, AnchorX = clad.x + 3 * clad.Width / 4, AnchorY = clad.y, LineBrush = clad.LabelBrush, LineRequired = true });
                    node.Places.Add(new LabelPlace { Penalty = 10, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2 - outRY, y2 = clad.y + clad.Height / 2 + outRY, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                    node.Places.Add(new LabelPlace { Penalty = 15, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2, y2 = clad.y + clad.Height / 2 + 2 * outRY, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                    node.Places.Add(new LabelPlace { Penalty = 15, x1 = clad.x + clad.Width / 2 - outRX, x2 = clad.x + clad.Width / 2 + outRX, y1 = clad.y + clad.Height / 2 - 2 * outRY, y2 = clad.y + clad.Height / 2, AnchorX = clad.x + clad.Width / 2, AnchorY = clad.y + clad.Height / 2, LineBrush = clad.LabelBrush, Inverse = true });
                }
            }
            AddGraph(node);
        }
        #endregion

        public override void RenderPlaces(Panel linesSurface, Panel labelsSurface)
        {
            GetBestPlaces(linesSurface.ActualWidth, linesSurface.ActualHeight);
            foreach (var g in graph)
            {
                var p = g.SelectedPlace;
                if (p == null) continue;
                var o = p.Inverse ? (Grid)g.invertedSourceObject : (Grid)g.sourceObject;
                if (p.LineRequired)
                {
                    linesSurface.Children.Add(new Line { X1 = p.x1, Y1 = p.y2, X2 = p.x2, Y2= p.y2, StrokeThickness = 0.5, Stroke = p.LineBrush });
                    linesSurface.Children.Add(new Line { X1 = p.AnchorX, Y1 = p.AnchorY, X2 = p.x2, Y2 = p.y2, StrokeThickness = 0.5, Stroke = p.LineBrush });
                }
                labelsSurface.Children.Add(o);
                Canvas.SetLeft(o, p.x1);
                Canvas.SetTop(o, p.y1);
            }
        }
    }

    public class LabelArrange
    {
        protected List<LabelPlaces> graph = new List<LabelPlaces>();
        protected List<LabelConstraint> constraints = new List<LabelConstraint>();

        protected void GetBestPlaces(double screenConstraintX, double screenConstraintY)
        {
            foreach (var g in graph)
            {
                foreach (var p in g.Places)
                {
                    foreach (var c in constraints) if (c.Key != g.ConstraintKey) LabelPlace.CheckRectangularConstraint(p, c);
                    LabelPlace.CheckScreenConstraint(p, screenConstraintX, screenConstraintY);
                }
            }

            for (int pass = 0; pass < 8; pass++)
            {
                for (var x = 0; x < graph.Count; x++)
                {
                    for (var y = x; y < graph.Count; y++)
                    {
                        if (x == y) continue;
                        for (int i = 0; i < graph[x].Places.Count; i++)
                        {
                            for (int j = 0; j < graph[y].Places.Count; j++)
                            {
                                LabelPlace.CheckOverlapWith(graph[x].Places[i], graph[y].Places[j]);
                            }
                        }
                    }
                }
                RemoveBadPlaces();
            }
            
            foreach (var g in graph)
            {
                g.SelectedPlace = g.Places.OrderBy(a => a.Overlaps.Count > 0 ? a.Overlaps.Max() : 0).ThenBy(a => a.Penalty).FirstOrDefault();
            }
        }

        public virtual void RenderPlaces(Panel linesSurface, Panel labelsSurface)
        {
            GetBestPlaces(linesSurface.ActualWidth, linesSurface.ActualHeight);
            foreach (var g in graph)
            {
                var p = g.SelectedPlace;
                if (p == null) continue;
                var o = p.Inverse? (Grid)g.invertedSourceObject :(Grid)g.sourceObject;
                if (p.LineRequired)
                {
                    linesSurface.Children.Add(new Line { X1 = p.AnchorX, Y1 = p.AnchorY, X2 = (p.x1 + p.x2) / 2, Y2 = (p.y1 + p.y2) / 2, StrokeThickness = 0.5, Stroke = p.LineBrush });
                }
                labelsSurface.Children.Add(o);
                Canvas.SetLeft(o, p.x1);
                Canvas.SetTop(o, p.y1);
            }
        }

        public void AddConstraint(LabelConstraint lc)
        {
            constraints.Add(lc);
        }

        public void AddGraph(LabelPlaces lp)
        {
            graph.Add(lp);
        }

        private void RemoveBadPlaces()
        {
            foreach (var g in graph)
            {
                var bad = g.Places.OrderByDescending(a => a.Overlaps.Count > 0 ? a.Overlaps.Max() : 0).ThenByDescending(a=>a.Penalty).FirstOrDefault();
                foreach (var b in bad.OverlapList)
                {
                    if (b != null)
                    {
                        var i = b.OverlapList.IndexOf(bad);
                        if (i >= 0)
                        {
                            b.OverlapList.RemoveAt(i);
                            b.Overlaps.RemoveAt(i);
                        }
                    }
                }
                if (g.Places.Count>1) g.Places.Remove(bad);
            }
        }
    }

    public class LabelPlaces
    {
        public object sourceObject;
        public object invertedSourceObject;
        public List<LabelPlace> Places = new List<LabelPlace>();
        public LabelPlace SelectedPlace;
        public int Key;
        public int ConstraintKey;
    }

    public class LabelConstraint
    {
        public double x1, y1, x2, y2;
        public int Key;
    }

    public class LabelPlace
    {
        public double x1, y1, x2, y2;
        public double AnchorX, AnchorY;
        public bool LineRequired = false;
        public Size LabelSize;
        public double Penalty;
        public bool Inverse;
        public List<LabelPlace> OverlapList = new List<LabelPlace>();
        public List<double> Overlaps = new List<double>();
        public Brush LineBrush;

        public static void CheckOverlapWith(LabelPlace one, LabelPlace two)
        {
            if (one.y1 > two.y2 || one.y2 < two.y1 || one.x1 > two.x2 || one.x2 < two.x1) return;

            double ox1 = 0, oy1 = 0, ox2 = 0, oy2 = 0;
            if (one.x1 >= two.x1 && one.x2 <= two.x2)
            {
                ox1 = 1;
                ox2 = (one.x2 - one.x1) / (two.x2 - two.x1);
            }
            if (one.y1 >= two.y1 && one.y2 <= two.y2)
            {
                oy1 = 1;
                oy2 = (one.y2 - one.y1) / (two.y2 - two.y1);
            }
            if (one.x1 <= two.x1 && one.x2 >= two.x2)
            {
                ox1 = (two.x2 - two.x1) / (one.x2 - one.x1);
                ox2 = 1;
            }
            if (one.y1 <= two.y1 && one.y2 >= two.y2)
            {
                oy1 = (two.y2 - two.y1) / (one.y2 - one.y1);
                oy2 = 1;
            }
            if (one.x1 <= two.x1 && one.x2 <= two.x2)
            {
                ox1 = (one.x2 - two.x1) / (one.x2 - one.x1);
                ox2 = (one.x2 - two.x1) / (two.x2 - two.x1);
            }
            if (one.y1 <= two.y1 && one.y2 <= two.y2)
            {
                oy1 = (one.y2 - two.y1) / (one.y2 - one.y1);
                oy2 = (one.y2 - two.y1) / (two.y2 - two.y1);
            }
            if (one.x1 >= two.x1 && one.x2 >= two.x2)
            {
                ox1 = (two.x2 - one.x1) / (one.x2 - one.x1);
                ox2 = (two.x2 - one.x1) / (two.x2 - two.x1);
            }
            if (one.y1 >= two.y1 && one.y2 >= two.y2)
            {
                oy1 = (two.y2 - one.y1) / (one.y2 - one.y1);
                oy2 = (two.y2 - one.y1) / (two.y2 - two.y1);
            }

            var o1 = Math.Abs(ox1 * oy1);
            var o2 = Math.Abs(ox2 * oy2);

            if (o1 > 0)
            {
                one.OverlapList.Add(two);
                one.Overlaps.Add(o1);
            }
            if (o2 > 0)
            {
                two.OverlapList.Add(one);
                two.Overlaps.Add(o2);
            }
        }

        public static void CheckRectangularConstraint(LabelPlace one, LabelConstraint two)
        {
            if (one.y1 > two.y2 || one.y2 < two.y1 || one.x1 > two.x2 || one.x2 < two.x1) return;

            double ox1 = 0, oy1 = 0;
            if (one.x1 >= two.x1 && one.x2 <= two.x2)
            {
                ox1 = 1;
            }
            if (one.y1 >= two.y1 && one.y2 <= two.y2)
            {
                oy1 = 1;
            }
            if (one.x1 <= two.x1 && one.x2 >= two.x2)
            {
                ox1 = (two.x2 - two.x1) / (one.x2 - one.x1);
            }
            if (one.y1 <= two.y1 && one.y2 >= two.y2)
            {
                oy1 = (two.y2 - two.y1) / (one.y2 - one.y1);
            }
            if (one.x1 <= two.x1 && one.x2 <= two.x2)
            {
                ox1 = (one.x2 - two.x1) / (one.x2 - one.x1);
            }
            if (one.y1 <= two.y1 && one.y2 <= two.y2)
            {
                oy1 = (one.y2 - two.y1) / (one.y2 - one.y1);
            }
            if (one.x1 >= two.x1 && one.x2 >= two.x2)
            {
                ox1 = (two.x2 - one.x1) / (one.x2 - one.x1);
            }
            if (one.y1 >= two.y1 && one.y2 >= two.y2)
            {
                oy1 = (two.y2 - one.y1) / (one.y2 - one.y1);
            }

            var o1 = Math.Abs(ox1 * oy1);

            if (o1 > 0)
            {
                one.OverlapList.Add(null);
                one.Overlaps.Add(o1);
                one.Penalty += 10;
            }
        }

        public static void CheckScreenConstraint(LabelPlace one, double screenWidth, double screenHeight)
        {
            double ox = 0, oy = 0;
            if (one.x1 >= 0 && one.x2 <= screenWidth)
            {
                ox = 1;
            }
            if (one.y1 >= 0 && one.y2 <= screenHeight)
            {
                oy = 1;
            }
            if (ox * oy == 1) return;

            if (one.x1 <= 0 && one.x2 >= screenWidth)
            {
                ox = (screenWidth) / (one.x2 - one.x1);
            }
            if (one.y1 <= 0 && one.y2 >= screenHeight)
            {
                oy = (screenHeight) / (one.y1 - one.y2);
            }
            if (one.x1 <= 0 && one.x2 <= screenWidth)
            {
                ox = (one.x2) / (one.x2 - one.x1);
            }
            if (one.y1 <= 0 && one.y2 <= screenHeight)
            {
                oy = (one.y2) / (one.y1 - one.y2);
            }
            if (one.x1 >= 0 && one.x2 >= screenWidth)
            {
                ox = (screenWidth - one.x1) / (one.x2 - one.x1);
            }
            if (one.y1 >= 0 && one.y2 >= screenHeight)
            {
                oy = (screenHeight - one.y2) / (one.y1 - one.y2);
            }
            if ((one.x1 <= 0 && one.x2 <= 0) || (one.x2 >= screenWidth && one.x1 >= screenWidth))
            {
                ox = 0;
            }
            if ((one.y1 <= 0 && one.y2 <= 0) || (one.y2 >= screenHeight && one.y1 >= screenHeight))
            {
                oy = 0;
            }

            var o1 = Math.Abs(ox * oy);

            if (o1 < 1)
            {
                one.OverlapList.Add(null);
                one.Overlaps.Add((1-o1)*10);
                one.Penalty += 10;
            }
        }
    }
}
