﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQMathClasses;

namespace AQChartLibrary
{
    public static class SLDataReportingExtentions
    {
        #region Вспомогательные операции для расчетов 

        public static bool IsNominal(this SLDataTable sldt, string columnName)
        {
            switch (GetDataType(sldt, columnName))
            {
                case "System.Double":
                case "Double":
                    return false;

                case "System.Int32":
                case "Int32":
                    return false;

                case "System.String":
                case "String":
                    return true;

                case "System.DateTime":
                case "DateTime":
                    return false;

                case "System.TimeSpan":
                case "TimeSpan":
                    return false;

                case "InputParameter":
                    return true;

                case "GroupDescription":
                    return true;

                default:
                    return true;
            }
        }

        public static string GetFieldTypeDescription(string sr)
        {
            string typeDescription;
            switch (sr.ToLower())
            {
                case "datetime":
                    typeDescription = "01.01.2015";
                    break;
                case "timespan":
                    typeDescription = "08:00:00";
                    break;
                case "string":
                    typeDescription = "Abc";
                    break;
                case "boolean":
                case "bool":
                    typeDescription = "1/0";
                    break;
                default:
                    typeDescription = "123";
                    break;
            }
            return typeDescription;
        }

        public static string GetFieldTypeName(string sr)
        {
            if (sr == null) return "Текст";
            string typeDescription;
            switch (sr.ToLower())
            {
                case "datetime":
                    typeDescription = "Дата и время";
                    break;
                case "timespan":
                    typeDescription = "Время";
                    break;
                case "string":
                    typeDescription = "Текст";
                    break;
                case "boolean":
                case "bool":
                    typeDescription = "Двоичное значение";
                    break;
                default:
                    typeDescription = "Число";
                    break;
            }
            return typeDescription;
        }

        public static bool CheckDataType(string type, object value)
        {
            if (type == null) return false;
            DateTime oDateTime;
            TimeSpan oTimeSpan;
            bool oBoolean;
            double oDouble;
            switch (type.ToLower())
            {
                case "datetime":
                    return DateTime.TryParse(value?.ToString(), out oDateTime);
                case "timespan":
                    return TimeSpan.TryParse(value?.ToString(), out oTimeSpan);
                case "string":
                    return true;
                case "boolean":
                case "bool":
                    return bool.TryParse(value?.ToString(), out oBoolean);
                default:
                    return double.TryParse(value?.ToString(), out oDouble);
            }
        }

        public static string GetFieldType(string sr)
        {
            string type;
            switch (sr)
            {
                case "01.01.2015":
                    type = "DateTime";
                    break;
                case "08:00:00":
                    type = "TimeSpan";
                    break;
                case "Abc":
                    type = "String";
                    break;
                case "1/0":
                    type = "Boolean";
                    break;
                case "Неизвестен":
                    type = null;
                    break;
                default:
                    type = "Double";
                    break;
            }
            return type;
        }

        public static List<object> GetColumnData(this SLDataTable sldt, string columnName)
        {
            var i = sldt.ColumnNames.IndexOf(columnName);
            return i == -1 ? new List<object>() : (from r in sldt.Table select r.Row[i]).ToList<object>();
        }
        public static List<object> GetColumnDataUnfiltered(this SLDataTable sldt, string columnName)
        {
            var i = sldt.ColumnNames.IndexOf(columnName);
            return i == -1 ? new List<object>() : (from r in sldt.UnfilteredTable select r.Row[i]).ToList<object>();
        }

        public static string GetDataType(this SLDataTable sldt, string columnName)
        {
            int i = sldt.ColumnNames.IndexOf(columnName);
            return i == -1 ? null : sldt.DataTypes[i];
        }

        public static string GetConcatenated(this IEnumerable<string> list, string divider)
        {
            var result = String.Empty;
            var isFirst = true;
            foreach (var o in list.Where(a => a != null))
            {
                if (!isFirst) result += divider;
                result += o;
                isFirst = false;
            }
            return result;
        }

        public static double GetApproximateRange(this SLDataTable sldt, string columnName)
        {
            IEnumerable<object> data = (GetColumnData(sldt, columnName).Where(a => a != null)).ToList();
            if (!data.Any()) return 0;
            var T = GetDataType(sldt, columnName);
            switch (T)
            {
                case "System.Double":
                case "Double":
                    if (data.Cast<Double>().Any(Double.IsNaN)) return Double.NaN;
                    return data.Cast<Double>().Max() - data.Cast<Double>().Min();
                
                case "System.Int32":
                case "Int32":
                    return data.Cast<Int32>().Max() - data.Cast<Int32>().Min();

                case "System.String":
                case "String":
                    return Double.NaN;

                case "System.DateTime":
                case "DateTime":
                    return (data.Cast<DateTime>().Max() - data.Cast<DateTime>().Min()).TotalMilliseconds*1e5;

                case "System.TimeSpan":
                case "TimeSpan":
                    return (data.Cast<TimeSpan>().Max() - data.Cast<TimeSpan>().Min()).TotalMilliseconds*1e10;

                case "InputParameter":
                    return Double.NaN;


                case "GroupDescription":
                    return (from d in data.Cast<GroupDescription>() select d.Max()).Max() -
                           (from d in data.Cast<GroupDescription>() select d.Min()).Min();

                default:
                    try
                    {
                        return data.Cast<Double>().Max() - data.Cast<Double>().Min();
                    }
                    catch
                    {
                        return Double.NaN;
                    }
            }
        }

        public static double GetApproximateMinimum(this SLDataTable sldt, string columnName)
        {
            IEnumerable<object> data = GetColumnData(sldt, columnName).Where(a => a != null).ToList();
            if (!data.Any()) return 0;
            switch (GetDataType(sldt, columnName))
            {
                case "System.Double":
                case "Double":
                    if (data.Where(a=>a is Double).Cast<Double>().Any(Double.IsNaN)) return Double.NaN;
                    return data.Where(a=>a is Double).Cast<Double>().Min();

                case "System.Int32":
                case "Int32":
                    return data.Where(a => a is Int32).Cast<Int32>().Min();

                case "System.String":
                case "String":
                    return 0;

                case "System.DateTime":
                case "DateTime":
                    return data.Where(a => a is DateTime).Cast<DateTime>().Min().Ticks * 1e5;

                case "System.TimeSpan":
                case "TimeSpan":
                    return data.Where(a => a is TimeSpan).Cast<TimeSpan>().Min().TotalMilliseconds * 1e10;

                case "InputParameter":
                    return Double.NaN;

                case "GroupDescription":
                    return (from d in data.Cast<GroupDescription>() select d.Min()).Min();

                default:
                    try
                    {
                        return data.Where(a=>a is Double).Cast<Double>().Min();
                    }
                    catch
                    {
                        return Double.NaN;
                    }
            }
        }
        
        public static double Max(this GroupDescription gd)
        {
            if (gd.Center == null) return Double.NaN;
            var max = gd.Center.Value;
            if (gd.IndexedData != null && gd.IndexedData.Count > 0) return gd.IndexedData.Select(a => a.Value).Max();
            if (gd.BoxMax > max && gd.BoxMax != null) max = gd.BoxMax.Value;
            if (gd.WhiskerMax > max && gd.WhiskerMax != null) max = gd.WhiskerMax.Value;
            if (gd.Extremes != null && gd.Extremes.Count > 0)
            {
                var maxExtreme = gd.Extremes.Select(a => a.Value).Max();
                if (maxExtreme > max) max = maxExtreme;
            }
            if (gd.Outliers == null || gd.Outliers.Count <= 0) return max;
            var maxOutlier = gd.Outliers.Select(a => a.Value).Max();
            if (maxOutlier > max) max = maxOutlier;
            return max;
        }

        public static double Min(this GroupDescription gd)
        {
            if (gd.Center == null) return Double.NaN;
            var min = gd.Center.Value;
            if (gd.IndexedData != null && gd.IndexedData.Count > 0) return gd.IndexedData.Select(a=>a.Value).Min();
            if (gd.BoxMin < min && gd.BoxMin != null) min = gd.BoxMin.Value;
            if (gd.WhiskerMin < min && gd.WhiskerMin != null) min = gd.WhiskerMin.Value;
            if (gd.Extremes != null && gd.Extremes.Count > 0)
            {
                var minExtreme = gd.Extremes.Select(a => a.Value).Min();
                if (minExtreme < min) min = minExtreme;
            }
            if (gd.Outliers == null || gd.Outliers.Count <= 0) return min;
            var minOutlier = gd.Outliers.Select(a => a.Value).Min();
            if (minOutlier < min) min = minOutlier;
            return min;
        }

        public static double GetApproximateMaximum(this SLDataTable sldt, string columnName)
        {
            IEnumerable<object> data = (GetColumnData(sldt, columnName).Where(a => a != null)).ToList();
            if (!data.Any()) return 0;
            switch (GetDataType(sldt, columnName))
            {
                case "System.Double":
                case "Double":
                    if (data.Where(a=> a is Double).Cast<Double>().Any(Double.IsNaN)) return Double.NaN;
                    return data.Where(a => a is Double).Cast<Double>().Max();
                
                case "System.Int32":
                case "Int32":
                    return data.Where(a => a is Int32).Cast<Int32>().Max();

                case "System.String":
                case "String":
                    return data.Where(a => a is String && !String.IsNullOrEmpty(a.ToString())).Distinct().Count() - 1;

                case "System.DateTime":
                case "DateTime":
                    return data.Where(a => a is DateTime).Cast<DateTime>().Max().Ticks * 1e5;

                case "System.TimeSpan":
                case "TimeSpan":
                    return data.Where(a => a is TimeSpan).Cast<TimeSpan>().Max().TotalMilliseconds * 1e10;

                case "InputParameter":
                    return Double.NaN;

                case "GroupDescription":
                    return (from d in data.Cast<GroupDescription>() select d.Max()).Max();

                default:
                    try
                    {
                        return data.Where(a => a is Double).Cast<Double>().Max();
                    }
                    catch
                    {
                        return Double.NaN;
                    }
            }
        }
        public static List<object> GetLayerList(this SLDataTable sldt, string layerName)
        {
            return GetColumnData(sldt, layerName).Distinct().ToList();
        }

        public static double GetDistinctCount(this SLDataTable sldt, string columnName)
        {
            return (GetColumnData(sldt, columnName).Where(a => a != null)).Distinct().Count();
        }

        public static IEnumerable<object> GetDistinct(this SLDataTable sldt, string columnName)
        {
            return GetColumnData(sldt, columnName).Where(a => a != null).Distinct();
        }

        public static IEnumerable<object> GetDistinctWithNulls(this SLDataTable sldt, string columnName)
        {
            return GetColumnData(sldt, columnName).Distinct();
        }
        public static IEnumerable<object> GetDistinctUnfilteredWithNulls(this SLDataTable sldt, string columnName)
        {
            return GetColumnDataUnfiltered(sldt, columnName).Distinct();
        }

        public static DateTime? GetMinimumDate(this SLDataTable sldt)
        {
            var datefields = GetDateFields(sldt);
            if (datefields == null || datefields.Count == 0) return null;
            var mindate = DateTime.MaxValue;

            foreach (var c in datefields)
            {
                var dates = sldt.GetColumnData(c).Where(a => a is DateTime).Cast<DateTime>();
                var dateTimes = dates as IList<DateTime> ?? dates.ToList();
                if (!dateTimes.Any()) continue;
                var md = dateTimes.Min();
                if (mindate > md) mindate = md;
            }
            return mindate != DateTime.MaxValue ? (DateTime?) mindate : null;
        }

        public static DateTime? GetMaximumDate(this SLDataTable sldt)
        {
            var datefields = GetDateFields(sldt);
            if (datefields == null || datefields.Count == 0) return null;
            var maxdate = DateTime.MinValue;

            foreach (var c in datefields)
            {
                var dates = sldt.GetColumnData(c).Where(a => a is DateTime).Cast<DateTime>();
                var dateTimes = dates as IList<DateTime> ?? dates.ToList();
                if (!dateTimes.Any()) continue;
                var md = dateTimes.Max();
                if (maxdate < md) maxdate = md;
            }
            return maxdate != DateTime.MinValue ? (DateTime?)maxdate : null;
        }

        public static List<string> GetDateFields(SLDataTable sldt)
        {
            var fields = new List<string>();
            for (var i = 0; i < sldt.DataTypes.Count; i++)
            {
                if (sldt.DataTypes[i].ToLower().Contains("datetime"))fields.Add(sldt.ColumnNames[i]);
            }
            return fields;
        }
        #endregion

        #region Выбор строк по условию

        public static List<int> GetRowsForValueFromField(this SLDataTable sldt, string fieldName, object value)
        {
            var f = sldt.ColumnNames.IndexOf(fieldName);
            var dt = sldt.DataTypes[f].ToLower().Split('.').LastOrDefault();
            var result = new List<int>();
            int count = 0;


            switch (dt)
            {
                case "datetime":
                    DateTime v;
                    if (!DateTime.TryParse(value?.ToString(), out v)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((DateTime)r.Row[f]).Ticks == v.Ticks) result.Add(count);
                        count++;
                    }
                    break;
                case "double":
                    double d;
                    if (!double.TryParse(value?.ToString(), out d)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && (Math.Abs((double)r.Row[f] - d) < 1e-10)) result.Add(count);
                        count++;
                    }
                    break;
                case "timespan":
                    TimeSpan t;
                    if (!TimeSpan.TryParse(value?.ToString(), out t)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((TimeSpan)r.Row[f]).Ticks == t.Ticks) result.Add(count);
                        count++;
                    }
                    break;
                default:
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && r.Row[f]?.ToString() == value?.ToString()) result.Add(count);
                        count++;
                    }
                    break;
            }

            return result;
        }

        public static List<object> GetRowDataForValueFromField(this SLDataTable sldt, string dataFieldName, object value, string fieldName)
        {
            var f = sldt.ColumnNames.IndexOf(fieldName);
            var n = sldt.ColumnNames.IndexOf(dataFieldName);
            var result = new List<object>();
            var dt = sldt.DataTypes[f].ToLower().Split('.').LastOrDefault();

            switch (dt)
            {
                case "datetime":
                    DateTime v;
                    if (!DateTime.TryParse(value?.ToString(), out v)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((DateTime)r.Row[f]).Ticks == v.Ticks) result.Add(r.Row[n]);
                    }
                    break;
                case "double":
                    double d;
                    if (!double.TryParse(value?.ToString(), out d)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && (Math.Abs((double)r.Row[f] - d) < 1e-10)) result.Add(r.Row[n]);
                    }
                    break;
                case "timespan":
                    TimeSpan t;
                    if (!TimeSpan.TryParse(value?.ToString(), out t)) break;
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((TimeSpan)r.Row[f]).Ticks == t.Ticks) result.Add(r.Row[n]);
                    }
                    break;
                default:
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && r.Row[f]?.ToString() == value?.ToString()) result.Add(r.Row[n]);
                    }
                    break;
            }
            return result;
        }
        
        public static List<int> GetRowsInRangeFromField(this SLDataTable sldt, string fieldName, double from, double to, bool rightIncluded)
        {
            var f = sldt.ColumnNames.IndexOf(fieldName);
            var result = new List<int>();
            int count = 0;

            if (rightIncluded)
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[f] is double && (double)r.Row[f] > from && (((double)r.Row[f] <= to) || Math.Abs((double)r.Row[f] - to) < 1e-10)) result.Add(count);
                    count++;
                }
            }
            else
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[f] is double && (double)r.Row[f] >= from && (((double)r.Row[f] < to) || Math.Abs((double)r.Row[f] - from) < 1e-10)) result.Add(count);
                    count++;
                }
            }
            return result;
        }

        public static List<int> GetRowsInRangeFromFieldAndLayer(this SLDataTable sldt, string fieldName, string layerField, object layer, double from, double to, bool rightIncluded)
        {
            var f = sldt.ColumnNames.IndexOf(fieldName);
            var g = sldt.ColumnNames.IndexOf(layerField);
            var result = new List<int>();
            int count = 0;

            if (rightIncluded)
            {
                if (layer != null && layer != DBNull.Value && layer?.ToString() != "#")
                {
                    foreach (var r in sldt.Table)
                    {
                        if ((r.Row[f] is double && (double)r.Row[f] > from && (double)r.Row[f] <= to ) && (r.Row[g] != null && r.Row[g] != DBNull.Value) && r.Row[g].ToString() == layer.ToString()) result.Add(count);
                        count++;
                    }
                }
                else
                {
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] is double && (double)r.Row[f] > from && (((double)r.Row[f] <= to)) && (r.Row[g] == null || r.Row[g] == DBNull.Value || r.Row[g]?.ToString() == string.Empty)) result.Add(count);
                        count++;
                    }
                }
            }
            else
            {
                if (layer != null && layer != DBNull.Value && layer?.ToString() != "#")
                {
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] is double && (double)r.Row[f] >= from && (((double)r.Row[f] < to)) && (r.Row[g] != null && r.Row[g] != DBNull.Value) && r.Row[g].ToString() == layer.ToString()) result.Add(count);
                        count++;
                    }
                }
                else
                {
                    foreach (var r in sldt.Table)
                    {
                        if (r.Row[f] is double && (double)r.Row[f] >= from && (((double)r.Row[f] < to)) && (r.Row[g] == null || r.Row[g] == DBNull.Value || r.Row[g]?.ToString() == string.Empty)) result.Add(count);
                        count++;
                    }
                }
            }
            return result;
        }

        public static List<int> GetRowsForValueFromFieldAndLayer(this SLDataTable sldt, string fieldName, object value, string layerField, object layer)
        {
            var f = sldt.ColumnNames.IndexOf(fieldName);
            var f2 = sldt.ColumnNames.IndexOf(layerField);

            if (f2 == -1) return GetRowsForValueFromField(sldt, fieldName, value);

            var result = new List<int>();
            int count = 0;
            var dt = sldt.DataTypes[f].ToLower().Split('.').LastOrDefault();
            var dl = sldt.DataTypes[f2].ToLower().Split('.').LastOrDefault();

            if (layer != null && layer != DBNull.Value && !string.IsNullOrEmpty(layer?.ToString()) && layer?.ToString() != "#")
            {
                var selectedForLayer = new List<int>();

                switch (dl)
                {
                    case "datetime":
                        DateTime v;
                        if (!DateTime.TryParse(layer?.ToString(), out v)) break;
                        foreach (var r in sldt.Table)
                        {
                            if (r.Row[f2] != null && !string.IsNullOrEmpty(r.Row[f2]?.ToString()) && ((DateTime)r.Row[f2]).Ticks == v.Ticks) selectedForLayer.Add(count);
                            count++;
                        }
                        break;
                    case "double":
                        double d;
                        if (!double.TryParse(layer?.ToString(), out d)) break;
                        foreach (var r in sldt.Table)
                        {
                            if (r.Row[f2] != null && !string.IsNullOrEmpty(r.Row[f2]?.ToString()) && (Math.Abs((double)r.Row[f2] - d) < 1e-10)) selectedForLayer.Add(count);
                            count++;
                        }
                        break;
                    case "timespan":
                        TimeSpan t;
                        if (!TimeSpan.TryParse(layer?.ToString(), out t)) break;
                        foreach (var r in sldt.Table)
                        {
                            if (r.Row[f2] != null && !string.IsNullOrEmpty(r.Row[f2]?.ToString()) && ((TimeSpan)r.Row[f2]).Ticks == t.Ticks) selectedForLayer.Add(count);
                            count++;
                        }
                        break;
                    default:
                        foreach (var r in sldt.Table)
                        {
                            if (r.Row[f2] != null && !string.IsNullOrEmpty(r.Row[f2]?.ToString()) && r.Row[f2]?.ToString() == layer?.ToString()) selectedForLayer.Add(count);
                            count++;
                        }
                        break;
                }

                switch (dt)
                {
                    case "datetime":
                        DateTime v;
                        if (!DateTime.TryParse(value?.ToString(), out v)) break;
                        foreach (var r in selectedForLayer)
                        {
                            if (sldt.Table[r].Row[f] != null && !string.IsNullOrEmpty(sldt.Table[r].Row[f]?.ToString()) && ((DateTime)sldt.Table[r].Row[f]).Ticks == v.Ticks) result.Add(r);
                        }
                        break;
                    case "double":
                        double d;
                        if (!double.TryParse(value?.ToString(), out d)) break;
                        foreach (var r in selectedForLayer)
                        {
                            if (sldt.Table[r].Row[f] != null && !string.IsNullOrEmpty(sldt.Table[r].Row[f]?.ToString()) && (Math.Abs((double)sldt.Table[r].Row[f] - d) < 1e-10)) result.Add(r);
                        }
                        break;
                    case "timespan":
                        TimeSpan t;
                        if (!TimeSpan.TryParse(value?.ToString(), out t)) break;
                        foreach (var r in selectedForLayer)
                        {
                            if (sldt.Table[r].Row[f] != null && !string.IsNullOrEmpty(sldt.Table[r].Row[f]?.ToString()) && ((TimeSpan)sldt.Table[r].Row[f]).Ticks == t.Ticks) result.Add(r);
                        }
                        break;
                    default:
                        foreach (var r in selectedForLayer)
                        {
                            if (sldt.Table[r].Row[f] != null && !string.IsNullOrEmpty(sldt.Table[r].Row[f]?.ToString()) && sldt.Table[r].Row[f]?.ToString() == value?.ToString()) result.Add(r);
                        }
                        break;
                }
            }
            else
            {
                switch (dt)
                {
                    case "datetime":
                        DateTime v;
                        if (!DateTime.TryParse(value?.ToString(), out v)) break;
                        foreach (var r in sldt.Table)
                        {
                            if ((r.Row[f2] == null || r.Row[f2] == DBNull.Value || r.Row[f2]?.ToString() == string.Empty) && r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((DateTime)r.Row[f]).Ticks == v.Ticks) result.Add(count);
                            count++;
                        }
                        break;
                    case "double":
                        double d;
                        if (!double.TryParse(value?.ToString(), out d)) break;
                        foreach (var r in sldt.Table)
                        {
                            if ((r.Row[f2] == null || r.Row[f2] == DBNull.Value || r.Row[f2]?.ToString() == string.Empty) && r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && (Math.Abs((double)r.Row[f] - d) < 1e-10)) result.Add(count);
                            count++;
                        }
                        break;
                    case "timespan":
                        TimeSpan t;
                        if (!TimeSpan.TryParse(value?.ToString(), out t)) break;
                        foreach (var r in sldt.Table)
                        {
                            if ((r.Row[f2] == null || r.Row[f2] == DBNull.Value || r.Row[f2]?.ToString() == string.Empty) && r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && ((TimeSpan)r.Row[f]).Ticks == t.Ticks) result.Add(count);
                            count++;
                        }
                        break;
                    default:
                        foreach (var r in sldt.Table)
                        {
                            if ((r.Row[f2] == null || r.Row[f2] == DBNull.Value || r.Row[f2]?.ToString() == string.Empty) && r.Row[f] != null && !string.IsNullOrEmpty(r.Row[f]?.ToString()) && r.Row[f]?.ToString() == value?.ToString()) result.Add(count);
                            count++;
                        }
                        break;
                }
            }
            return result;
        }

        public static List<int> GetRowsForAllValuesFromLayer(this SLDataTable sldt, string layerField, object layer)
        {
            var f = sldt.ColumnNames.IndexOf(layerField);
            var result = new List<int>();
            int count = 0;

            if (layer != null && layer != DBNull.Value && layer?.ToString() !="#")
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[f] != null && r.Row[f].ToString() == layer.ToString()) result.Add(count);
                    count++;
                }
            }
            else
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[f] == null || r.Row[f] == DBNull.Value || r.Row[f]?.ToString() == string.Empty) result.Add(count);
                    count++;
                }
            }
            return result;
        }

        public static List<int> GetRowsForValueFromFieldLayerAndColor(this SLDataTable sldt, string fieldName, object value, string layerField, object layer, string colorFieldName, object color)
        {
            var f2 = sldt.ColumnNames.IndexOf(layerField);
            var f3 = sldt.ColumnNames.IndexOf(colorFieldName);

            if (f2 == -1 || f3 == -1) return GetRowsForValueFromField(sldt, fieldName, value);

            var intermediateResult = GetRowsForValueFromFieldAndLayer(sldt, fieldName, value, layerField, layer);
            var result = new List<int>();
            foreach (var r in intermediateResult)
            {
                if (color != null)
                {
                    if (sldt.Table[r].Row[f3] != null && !string.IsNullOrEmpty(sldt.Table[r].Row[f3]?.ToString()) && sldt.Table[r].Row[f3]?.ToString() == color?.ToString()) result.Add(r);
                }
                else if (sldt.Table[r].Row[f3] == null || string.IsNullOrEmpty(sldt.Table[r].Row[f3]?.ToString())) result.Add(r);
            }
            return result;
        }
        
        public static List<int> GetRowsForAllValuesFromLayers(this SLDataTable sldt, string layerField, List<object> layers)
        {
            var result = new List<int>();
            if (layers == null)
            {
                for(var i = 0; i< sldt.Table.Count; i++)result.Add(i);
            }
            else
            {
                foreach (var l in layers) result.AddRange(GetRowsForValueFromField(sldt, layerField, l));
            }
            return result;
        }

        public static List<int> GetRowsForAllValues(this SLDataTable sldt)
        {
            var result = new List<int>();
            int count = 0;

            foreach (var r in sldt.Table)
            {
                result.Add(count);
                count++;
            }
            return result;
        }

        public static List<int> GetRowsForAllValuesFromGroupAndLayer(this SLDataTable sldt, string layerField, object layer, string groupField, object group)
        {
            var z = sldt.ColumnNames.IndexOf(layerField);
            var g = sldt.ColumnNames.IndexOf(groupField);
            var result = new List<int>();

            int count = 0;

            if (group != null && layer != null && layer?.ToString()!="#")
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[g] != null && r.Row[g].ToString() == group?.ToString() && r.Row[z] != null && r.Row[z]?.ToString() == layer?.ToString()) result.Add(count);
                    count++;
                }
            }
            if (group == null && layer != null && layer?.ToString() != "#")
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[g] == null && r.Row[z] != null && r.Row[z]?.ToString() == layer?.ToString()) result.Add(count);
                    count++;
                }
            }
            if (group != null && (layer == null || layer?.ToString()=="#"))
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[g] != null && r.Row[g].ToString() == group?.ToString() && (r.Row[z] == null || r.Row[z]==DBNull.Value || r.Row[z]?.ToString() == string.Empty)) result.Add(count);
                    count++;
                }
            }
            if (group == null && (layer == null || layer?.ToString() == "#"))
            {
                foreach (var r in sldt.Table)
                {
                    if (r.Row[g] == null && (r.Row[z] == null || r.Row[z] == DBNull.Value || r.Row[z]?.ToString() == string.Empty)) result.Add(count);
                    count++;
                }
            }
            return result;
        }
        #endregion

        #region Обработка строк

        public static void DeleteValues(this SLDataTable sldt, string fieldName, List<int> rowList)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);

            foreach (var row in rowList)
            {
                sldt.Table[row].Row[index] = null;
            }
        }


        public static void DeleteCases(this SLDataTable sldt, string fieldName, List<int> rowList)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);
            var rows = rowList.OrderByDescending(a => a);

            foreach (var row in rows)
            {
                var r = sldt.Table[row];
                sldt.Table.Remove(r);
                if (sldt.IsFiltered)
                {
                    sldt.UnfilteredTable.Remove(r);
                }
            }
        }

        public static SLDataTable CopyToNewTable(this SLDataTable sldt, string fieldName, List<int> rowList)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);
            var result = sldt.CloneMetaData();

            foreach (var row in rowList)
            {
                result.Table.Add(sldt.Table[row].Clone());
            }
            return result;
        }

        public static SLDataTable CutToNewTable(this SLDataTable sldt, string fieldName, List<int> rowList)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);
            SLDataTable result = sldt.CloneMetaData();
            var rows = rowList.OrderByDescending(a => a);

            foreach (var row in rows)
            {
                result.Table.Add(sldt.Table[row].Clone());
                sldt.Table.RemoveAt(row);
            }
            return result;
        }

        public static void SetColor(this SLDataTable sldt, string fieldName, string destinationField, List<int> rowList, string color)
        {
            var colorName = "#" + destinationField + "_Цвет";
            sldt.SetTextField(fieldName, rowList, color, colorName);
        }

        public static void SetFieldValue(this SLDataTable sldt, string fieldName, List<int> rowList, object value)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);

            if (index == -1)
            {
                sldt.ColumnNames.Add(fieldName);
                sldt.ColumnTags?.Add(null);
                sldt.DataTypes.Add(value.GetType().Name.Split('.').LastOrDefault());

                if (!sldt.IsFiltered)
                {
                    for (var i = 0; i < sldt.Table.Count; i++)
                    {
                        sldt.Table[i].Row.Add(rowList.Contains(i) ? value : null);
                    }
                }
                else
                {
                    for (var i = 0; i < sldt.UnfilteredTable.Count; i++)
                    {
                        sldt.UnfilteredTable[i].Row.Add(null);
                    }
                    foreach (var r in rowList)
                    {
                        sldt.Table[r].Row[index] = value;
                    }
                }
            }
            else
            {
                foreach (var r in rowList)
                {
                    sldt.Table[r].Row[index] = value;
                }
            }
        }

        public static void SetColorForRow(this SLDataTable sldt, List<int> rowList, string color)
        {
            var fields = sldt.ColumnNames.ToList();
            foreach (var fieldName in fields)
            {
                var colorName = "#" + fieldName + "_Цвет";
                sldt.SetTextField(fieldName, rowList, color, colorName);
            }
        }

        public static void SetLabel(this SLDataTable sldt, string fieldName, string destinationField, List<int> rowList, string label)
        {
            var colorName = "#" + destinationField + "_Метка";
            sldt.SetTextField(fieldName, rowList, label, colorName);
        }

        private static void SetTextField(this SLDataTable sldt, string fieldName, List<int> rowList, string newfieldvalue, string newfield)
        {
            var index = sldt.ColumnNames.IndexOf(fieldName);
            var indexColor = sldt.ColumnNames.IndexOf(newfield);

            if (indexColor == -1)
            {
                sldt.ColumnNames.Add(newfield);
                sldt.ColumnTags?.Add(null);
                sldt.DataTypes.Add("String");

                if (!sldt.IsFiltered)
                {
                    for (var i = 0; i < sldt.Table.Count; i++)
                    {
                        sldt.Table[i].Row.Add(rowList.Contains(i) ? newfieldvalue : null);
                    }
                }
                else
                {
                    for (var i = 0; i < sldt.UnfilteredTable.Count; i++)
                    {
                        sldt.UnfilteredTable[i].Row.Add(null);
                    }
                    indexColor = sldt.ColumnNames.IndexOf(newfield);
                    foreach (var r in rowList)
                    {
                        sldt.Table[r].Row[indexColor] = newfieldvalue;
                    }
                }
            }
            else
            {
                foreach (var r in rowList)
                {
                     sldt.Table[r].Row[indexColor] = newfieldvalue;
                }
            }
        }
        
        public static SLDataRow Clone(this SLDataRow sldr)
        {
            var row = new SLDataRow() { Row = new List<object>()};
            
            foreach (var value in sldr.Row)
            {
                row.Row.Add(value);
            }
            return row;
        }

        private static SLDataTable CloneMetaData(this SLDataTable sldt)
        {
            return new SLDataTable
            {
                ColumnNames = sldt.ColumnNames?.ToList(),
                ColumnTags = sldt.ColumnTags?.ToList(),
                CustomProperties = sldt.CustomProperties?.ToList(),
                DataTypes = sldt.DataTypes?.ToList(),
                Header = sldt.Header,
                RowTags = sldt.RowTags?.ToList(),
                SelectionString = sldt.SelectionString,
                TableKey = sldt.TableKey,
                TableName = sldt.TableName,
                IsFiltered = sldt.IsFiltered,
                UnfilteredTable = new List<SLDataRow>(),
                Filters = new List<SLDataFilter>(),
                Tag = sldt.Tag,
                Table = new List<SLDataRow>()
            };
        }
        #endregion

        #region Операции над слоями

        public static SLDataTable ExtractLayer(this SLDataTable sldt, string layerName, object Z)
        {
            var index = sldt.ColumnNames.IndexOf(layerName);
            SLDataTable result = sldt.CloneMetaData();

            if (index < 0) return result;

            foreach (var row in sldt.Table)
            {
                if (Z == null && row.Row[index] == null) result.Table.Add(row);
                else if (row.Row[index]?.ToString() == Z?.ToString()) result.Table.Add(row);
            }

            return result;
        }

        public static SLDataTable ExtractLayers(this SLDataTable sldt, string layerName, List<object> layers)
        {
            var index = sldt.ColumnNames.IndexOf(layerName);
            SLDataTable result = sldt.CloneMetaData();

            if (index < 0) return result;

            var z = layers.Where(a => a != null).Select(a => a.ToString()).ToList();
            var hasNulls = layers.Any(a => a == null);
            foreach (var row in sldt.Table)
            {
                if (hasNulls && row.Row[index] == null) result.Table.Add(row);
                else if (z.Contains(row.Row[index].ToString())) result.Table.Add(row);
            }
            return result;
        }

        public static List<SLDataTable> SplitByLayers(this SLDataTable sldt, string layerName, List<object> layers = null)
        {
            var z = layers ?? sldt.GetLayerList(layerName);
            var index = sldt.ColumnNames.IndexOf(layerName);
            var resultCache = new Dictionary<string, SLDataTable>();
            foreach (var layer in z)
            {
                var layerTable = sldt.CloneMetaData();
                if (layerTable.TableName.ToLower().StartsWith("таблица"))
                {
                    if (layer != null) layerTable.TableName = layerName + ": " + layer;
                    else layerTable.TableName = layerName + ": нет значения";
                }
                else
                {
                    if (layer != null) layerTable.TableName += "; " + layerName + ": " + layer;
                    else layerTable.TableName += "; " + layerName + ": нет значения";
                }

                layerTable.IsFiltered = false;
                layerTable.UnfilteredTable = null;

                resultCache.Add(layer.ToString() ?? "##Нет значения", layerTable);
            }

            foreach (var row in sldt.Table)
            {
                var layer = (row.Row[index] ?? "##Нет значения").ToString();
                if (resultCache.ContainsKey(layer)) resultCache[layer].Table.Add(row);
            }

            return resultCache.Values.ToList();
        }

        public static void RemoveOutOfRanges(this SLDataTable sldt, string layerName)
        {
            var index = sldt.ColumnNames.IndexOf(layerName);
            var LRName = "#" + layerName + "_НГ";
            var HRName = "#" + layerName + "_ВГ";
            var LRindex = sldt.ColumnNames.IndexOf(LRName);
            var HRindex = sldt.ColumnNames.IndexOf(HRName);
            
            foreach (var r in sldt.Table)
            {
                if (r.Row[index] == null || string.IsNullOrEmpty(r.Row[index]?.ToString())) continue;
                if (LRindex >= 0 && r.Row[LRindex] != null && !string.IsNullOrEmpty(r.Row[LRindex]?.ToString()) && (double)r.Row[index] < (double)r.Row[LRindex]) r.Row[index] = null;
                else if (HRindex >= 0 && r.Row[HRindex] != null && !string.IsNullOrEmpty(r.Row[HRindex]?.ToString()) && (double)r.Row[index] > (double)r.Row[HRindex]) r.Row[index] = null;
            }
        }

        public static void RemoveOutliers(this SLDataTable sldt, string layerName)
        {
            if (sldt.GetDataType(layerName).ToLower().Split('.').LastOrDefault() != "double") return;
            var index = sldt.ColumnNames.IndexOf(layerName);
            var deleted = true;
            while (deleted)
            {
                deleted = false;
                var d = sldt.GetColumnData(layerName);
                var sigma = AQMath.AggregateStDev(d);
                var avg = AQMath.AggregateMean(d);
                var lr = avg - 3 * sigma;
                var hr = avg + 3 * sigma;

                foreach (var r in sldt.Table)
                {
                    if (r.Row[index] == null || string.IsNullOrEmpty(r.Row[index]?.ToString())) continue;
                    if ((double) r.Row[index] < lr)
                    {
                        r.Row[index] = null;
                        deleted = true;
                    }
                    else if ((double) r.Row[index] > hr)
                    {
                        r.Row[index] = null;
                        deleted = true;
                    }
                }
            }
        }

        public static void SetColorForLayers(this SLDataTable sldt, string layerName, List<object> layers, string color)
        {
            var colorName = "#" + layerName + "_Цвет";
            var index = sldt.ColumnNames.IndexOf(layerName);
            var indexColor = sldt.ColumnNames.IndexOf(colorName);
            var z = layers.Where(a => a != null).Select(a => a.ToString()).ToList();
            var hasNulls = layers.Any(a => a == null);

            if (indexColor == -1)
            {
                sldt.ColumnNames.Add(colorName);
                sldt.ColumnTags?.Add(null);
                sldt.DataTypes.Add("String");
                if (sldt.IsFiltered)
                {
                    foreach (var r in sldt.UnfilteredTable)
                    {
                        r.Row.Add(null);
                    }
                    foreach (var r in sldt.Table)
                    {
                        if ((hasNulls && r.Row[index] == null) || (z.Contains(r.Row[index].ToString()))) r.Row[indexColor] = color;
                    }
                }
                else
                {
                    foreach (var r in sldt.Table)
                    {
                        if ((hasNulls && r.Row[index] == null) || (z.Contains(r.Row[index].ToString()))) r.Row.Add(color);
                        else r.Row.Add(null);
                    }
                }
            }
            else
            {
                foreach (var r in sldt.Table)
                {
                    if ((hasNulls && r.Row[index] == null) || (z.Contains(r.Row[index].ToString()))) r.Row[indexColor] = color;
                }
            }
        }

        #endregion
    }
}