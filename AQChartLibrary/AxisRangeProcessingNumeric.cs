﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class AxisRange
    {
        private void ProcessNumericAxis(object sourceMinimum, object sourceMaximum, double maxDesiredSteps)
        {
            if (sourceMinimum == null) return;
            if (FixedAxisDescription == null || (!FixedAxisDescription.StepFixed && !FixedAxisDescription.AxisMaxFixed && !FixedAxisDescription.AxisMinFixed))
            {
                if (IsSymmetrical) AutoProcessSymmetricalNumericAxis(sourceMinimum, sourceMaximum, maxDesiredSteps);
                else AutoProcessNumericAxis(sourceMinimum, sourceMaximum, maxDesiredSteps);
                return;
            }
            
            if (FixedAxisDescription.StepFixed)
            {
                if (FixedAxisDescription.AxisMaxFixed && FixedAxisDescription.AxisMinFixed)
                {
                    FullFixedProcessNumericAxis();
                }
                else StepFixedProcessNumericAxis(sourceMinimum, sourceMaximum);
            }
            else
            {
                if (FixedAxisDescription.AxisMaxFixed && FixedAxisDescription.AxisMinFixed)
                {
                    RangeFixedProcessNumericAxis();
                }
                else if (!FixedAxisDescription.AxisMaxFixed && FixedAxisDescription.AxisMinFixed)
                {
                    RangeMinFixedProcessNumericAxis(sourceMaximum, maxDesiredSteps);
                }
                else
                {
                    RangeMaxFixedProcessNumericAxis(sourceMinimum, maxDesiredSteps);
                }
            }
            _orderedAxisLabels = _displayedLabels.OrderBy(a => a).ToList();
        }

        private void StepFixedProcessNumericAxis(object sourceMinimum, object sourceMaximum)
        {
            var fixedStep = FixedAxisDescription.Step;
            if (Math.Abs(fixedStep) < Double.Epsilon || Math.Abs((double)sourceMinimum - (double)sourceMaximum)<Double.Epsilon)
            {
                SetSinglePointRange((double)sourceMaximum);
                return;
            }

            var anker = (double)sourceMinimum;
            var sMin = (double) sourceMinimum;
            var sMax = (double) sourceMaximum;
            var lowRange = Math.Ceiling((anker - sMin)/fixedStep)*fixedStep;
            DisplayMinimum = FixedAxisDescription.AxisMinFixed ? FixedAxisDescription.AxisMin : anker - lowRange;
            var highRange = Math.Ceiling((sMax - anker)/fixedStep)*fixedStep;
            DisplayMaximum = FixedAxisDescription.AxisMaxFixed ? FixedAxisDescription.AxisMax : anker + highRange;

            if (DisplayMaximum != null&& DisplayMinimum != null)
            {
                var steps = Math.Round(((double) DisplayMaximum - (double) DisplayMinimum)/fixedStep, 1);
                RealSteps = steps;
            }
           
            if (!FixedAxisDescription.AxisMaxFixed)
            {
                RealSteps++;
                if (DisplayMaximum != null) DisplayMaximum = (double) DisplayMaximum + fixedStep;
            }

            if (DisplayMinimum != null && (Math.Abs((double) DisplayMinimum) > Double.Epsilon && !FixedAxisDescription.AxisMinFixed))
            {
                RealSteps++;
                DisplayMinimum = (double) DisplayMinimum - fixedStep;
            }
            if (!(Math.Abs(RealSteps) > Double.Epsilon))
            {
                if (DisplayMinimum != null) SetSinglePointRange((double)DisplayMinimum);
            }
            else
            {
                _displayedLabels = new List<string>();

                for (var i = 0; i <= RealSteps; i++)
                {
                    if (DisplayMinimum != null)
                    {
                        var label = Math.Round((Double)DisplayMinimum + i * fixedStep, 6);
                        if (label < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                        else if (label < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(label).ToString(CultureInfo.InvariantCulture));
                        else _displayedLabels.Add(label.ToString(CultureInfo.InvariantCulture));
                    }
                }
                DisplayStep = fixedStep;
                PartStep = 1.0d / RealSteps;
            }
        }

        private void RangeFixedProcessNumericAxis()
        {
            DisplayMinimum = FixedAxisDescription.AxisMin;
            DisplayMaximum = FixedAxisDescription.AxisMax;
            if (DisplayMaximum == null || DisplayMinimum == null) return;
            var optimalStep = Math.PI;

            for (double i = 14; i > 3; i--)
            {
                var step = ((double) DisplayMaximum - (double) DisplayMinimum)/i;
                if (step.ToString(CultureInfo.InvariantCulture).Length < optimalStep.ToString(CultureInfo.InvariantCulture).Length)
                {
                    optimalStep = step;
                }
            }

            if (Math.Abs(optimalStep - Math.PI)<Double.Epsilon )
            {
                optimalStep = ((double)DisplayMaximum - (double)DisplayMinimum) / 10;
            }

            RealSteps = Math.Round(((double)DisplayMaximum - (double)DisplayMinimum) / optimalStep, 9);

            _displayedLabels = new List<string>();
            if (!(Math.Abs(RealSteps) > double.Epsilon)) SetSinglePointRange((double)DisplayMinimum);
            else
            {
                for (int i = 0; i <= RealSteps; i++)
                {
                    var label = Math.Round((Double)DisplayMinimum + i * optimalStep, 6);
                    if (label < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                    else if (label < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(label).ToString(CultureInfo.InvariantCulture));
                    else _displayedLabels.Add(label.ToString(CultureInfo.InvariantCulture));
                }
                DisplayStep = optimalStep;
                PartStep = 1 / RealSteps;
            }
        }

        private void FullFixedProcessNumericAxis()
        {
            DisplayMinimum = FixedAxisDescription.AxisMin;
            DisplayMaximum = FixedAxisDescription.AxisMax;
            if (DisplayMaximum == null || DisplayMinimum == null) return; 
            RealSteps = Math.Round(((double)DisplayMaximum - (double)DisplayMinimum) / FixedAxisDescription.Step, 9);
            _displayedLabels = new List<string>();

            for (int i = 0; i <= RealSteps; i++)
            {
                var label = Math.Round((Double)DisplayMinimum + i * FixedAxisDescription.Step, 6);
                if (label < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                else if (label < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(label).ToString(CultureInfo.InvariantCulture));
                else _displayedLabels.Add(label.ToString(CultureInfo.InvariantCulture));
            }
            DisplayStep = FixedAxisDescription.Step;
            PartStep = 1.0d / RealSteps;
        }

        private void RangeMinFixedProcessNumericAxis(object sourceMaximum, double maxDesiredSteps)
        {
            double degree;
            double ceil = 0;
            double floor = 0;
            var optimalStep = Math.PI;
            var sMin = FixedAxisDescription.AxisMin;
            var sMax = sourceMaximum;

            if (sMin.HasValue && Math.Abs(sMin.Value - (double)sMax) < 1e-8)
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double) sMax)) - 1)/2;
                if (Math.Abs(degree) > 1e-8)
                {
                    ceil = Math.Ceiling((double) sMax/degree)*degree;
                    floor = Math.Floor((double) sMax/degree)*degree;
                }
            }
            else
            {
                if (sMin.HasValue)
                {
                    degree = Math.Pow(10, Math.Ceiling(Math.Log10((double) sMax - (double) sMin)) - 1)/2;
                    ceil = Math.Ceiling((double) sMax/degree)*degree;
                    floor = (double) sMin;
                }
            }

            var amplitude = (ceil - floor);

            var degree2 = Math.Pow(10, Math.Ceiling(Math.Log10(amplitude/maxDesiredSteps)))/2;
            if (Math.Abs(degree2) < 1e-8) SetSinglePointRange(floor);
            else
            {
                var availableStep = Math.Ceiling(amplitude/(degree2)/maxDesiredSteps)*(degree2);

                DisplayMinimum = floor;
                DisplayMaximum = Math.Ceiling(ceil/availableStep) * availableStep;

                for (double i = 14; i > 3; i--)
                {
                    var ostep = ((double)DisplayMaximum - (double)DisplayMinimum) / i;
                    if (ostep.ToString(CultureInfo.InvariantCulture).Length < optimalStep.ToString(CultureInfo.InvariantCulture).Length)
                    {
                        optimalStep = ostep;
                    }
                }

                RealSteps = Math.Round(((double) DisplayMaximum - (double) DisplayMinimum)/optimalStep, 0);
                _displayedLabels = new List<string>();
                var step = Math.Round(optimalStep, 6);
                DisplayMaximum = (double) DisplayMinimum + RealSteps * step;
                if (Axis != AxisType.Z)
                {
                    RealSteps++;
                    DisplayMaximum = (double) DisplayMaximum + step;
                }
                if (step > 0)
                {
                    for (var i = Math.Round((double) DisplayMinimum, 6);
                        Math.Round(i, 6) <= Math.Round((double) DisplayMaximum, 6);
                        i += step)
                    {
                        var v = Math.Round(i, 6);
                        if (v < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                        else if (v < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(v).ToString(CultureInfo.InvariantCulture));
                        else _displayedLabels.Add(v.ToString(CultureInfo.InvariantCulture));
                    }
                }
                DisplayStep = step;
                PartStep = 1.0d / RealSteps;
            }
        }

        private void RangeMaxFixedProcessNumericAxis(object sourceMinimum, double maxDesiredSteps)
        {
            double degree;
            double ceil = 0;
            double floor = 0;
            var optimalStep = Math.PI;

            var sMin = sourceMinimum;
            var sMax = FixedAxisDescription.AxisMax;

            if (sMax.HasValue && Math.Abs((double)sMin - (double)sMax) < 1e-8)
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sMax)) - 1) / 2;
                if (Math.Abs(degree) > 1e-8)
                {
                    ceil = Math.Ceiling((double)sMax / degree) * degree;
                    floor = Math.Floor((double)sMax / degree) * degree;
                }
            }
            else
            {
                if (sMax.HasValue)
                {
                    degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sMax - (double)sMin)) - 1) / 2;
                    ceil = sMax.Value;
                    floor = Math.Floor((double)sMin / degree) * degree;
                }
            }
            var amplitude = (ceil - floor);

            var degree2 = Math.Pow(10, Math.Ceiling(Math.Log10(amplitude / maxDesiredSteps))) / 2;
            if (!(Math.Abs(degree2) > Double.Epsilon)) SetSinglePointRange(floor);
            else 
            {
                var availableStep = Math.Ceiling(amplitude / (degree2) / maxDesiredSteps) * (degree2);

                DisplayMinimum = Math.Floor(floor / availableStep) * availableStep;
                DisplayMaximum = ceil;
                for (double i = 14; i > 3; i--)
                {
                    var ostep = ((double)DisplayMaximum - (double)DisplayMinimum) / i;
                    if (ostep.ToString(CultureInfo.InvariantCulture).Length < optimalStep.ToString(CultureInfo.InvariantCulture).Length)
                    {
                        optimalStep = ostep;
                    }
                }

                RealSteps = Math.Round(((double)DisplayMaximum - (double)DisplayMinimum) / optimalStep, 0);
                double step = Math.Round(optimalStep, 6);

                DisplayMinimum = (double) DisplayMaximum - RealSteps * step;
                _displayedLabels = new List<string>();
                
                if (Math.Abs((Double)DisplayMinimum) > Double.Epsilon && Axis != AxisType.Z)
                {
                    RealSteps++;
                    DisplayMinimum = (double) DisplayMinimum - step;
                }

                if (step > 0)
                {
                    for (var i = Math.Round((double)DisplayMinimum, 6);
                        Math.Round(i, 6) <= Math.Round((double)DisplayMaximum, 6);
                        i += step)
                    {
                        double v = Math.Round(i, 6);
                        if (v < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                        else if (v < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(v).ToString(CultureInfo.InvariantCulture));
                        else _displayedLabels.Add(v.ToString(CultureInfo.InvariantCulture));
                    }
                }
                DisplayStep = step;
                PartStep = 1.0d / RealSteps;
            }
        }

        private void AutoProcessNumericAxis(object sourceMinimum, object sourceMaximum, double maxDesiredSteps)
        {
            double degree;
            double ceil = 0;
            double floor = 0;

            if (Math.Abs((double)sourceMinimum - (double)sourceMaximum) < 1e-6)
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sourceMaximum)) - 1) / 2;
                if (Math.Abs(degree) > 1e-8)
                {
                    ceil = Math.Ceiling((double)sourceMaximum / degree) * degree;
                    floor = Math.Floor((double)sourceMaximum / degree) * degree;
                }
            }
            else
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sourceMaximum - (double)sourceMinimum)) - 1) / 2;
                ceil = Math.Ceiling((double)sourceMaximum / degree) * degree;
                floor = Math.Floor((double)sourceMinimum / degree) * degree;
            }

            var amplitude = (ceil - floor);

            var degree2 = Math.Pow(10, Math.Ceiling(Math.Log10(amplitude / maxDesiredSteps))) / 2;
            if (!(Math.Abs(degree2) > Double.Epsilon)) SetSinglePointRange(floor);
            else
            {
                var availableStep = Math.Ceiling(amplitude/(degree2)/maxDesiredSteps)*(degree2);

                DisplayMinimum = Math.Floor(floor/availableStep)*availableStep;
                DisplayMaximum = Math.Ceiling(ceil/availableStep)*availableStep;

                RealSteps = Math.Round(((double) DisplayMaximum - (double) DisplayMinimum)/availableStep, 0);
                _displayedLabels = new List<string>();
                var step = Math.Round(availableStep, 6);

                if (Axis != AxisType.Z)
                {
                    RealSteps++;
                    DisplayMaximum = (double) DisplayMaximum + step;

                    if (Math.Abs((double) DisplayMinimum) > 1e-15)
                    {
                        RealSteps++;
                        DisplayMinimum = (double) DisplayMinimum - step;
                    }
                }

                if (step > 0)
                {
                    for (var i = Math.Round((double) DisplayMinimum, 6);
                        Math.Round(i, 6) <= Math.Round((double) DisplayMaximum, 6);
                        i += step)
                    {
                        var v = Math.Round(i, 6);
                        if (v < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                        else if (v < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(v).ToString(CultureInfo.InvariantCulture));
                        else _displayedLabels.Add(v.ToString(CultureInfo.InvariantCulture));
                    }
                }
                DisplayStep = step;
                PartStep = 1 / RealSteps;
            }
        }

        private void AutoProcessSymmetricalNumericAxis(object sourceMinimum, object sourceMaximum, double maxDesiredSteps)
        {
            double degree;
            double ceil = 0;
            double floor = 0;

            
            var optimalStep = Math.PI;
            var sMin = 0.0d;
            var sMax = Math.Max(Math.Abs((double)sourceMinimum), Math.Abs((double)sourceMaximum)); ;

            if (Math.Abs((double)sMax) < 1e-8)
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sMax)) - 1) / 2;
                if (Math.Abs(degree) > 1e-8)
                {
                    ceil = Math.Ceiling((double)sMax / degree) * degree;
                    floor = Math.Floor((double)sMax / degree) * degree;
                }
            }
            else
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10((double)sMax - (double)sMin)) - 1) / 2;
                ceil = Math.Ceiling((double)sMax / degree) * degree;
                floor = (double)sMin;
            }

            var amplitude = (ceil - floor);

            var degree2 = Math.Pow(10, Math.Ceiling(Math.Log10(amplitude / maxDesiredSteps))) / 2;
            if (Math.Abs(degree2) < 1e-8) SetSinglePointRange(floor);
            else
            {
                var availableStep = Math.Ceiling(amplitude / (degree2) / maxDesiredSteps) * (degree2);

                DisplayMaximum = Math.Ceiling(ceil / availableStep) * availableStep;
                

                for (double i = 14; i > 3; i--)
                {
                    var ostep = ((double)DisplayMaximum) / i;
                    if (ostep.ToString(CultureInfo.InvariantCulture).Length < optimalStep.ToString(CultureInfo.InvariantCulture).Length)
                    {
                        optimalStep = ostep;
                    }
                }

                _displayedLabels = new List<string>();
                var step = Math.Round(optimalStep, 6);
                DisplayMaximum = (double)DisplayMaximum + step;
                DisplayMinimum = -(double)DisplayMaximum;
                RealSteps = Math.Round(((double)DisplayMaximum - (double)DisplayMinimum) / optimalStep, 0);
                if (step > 0)
                {
                    for (var i = Math.Round((double)DisplayMinimum, 6);
                        Math.Round(i, 6) <= Math.Round((double)DisplayMaximum, 6);
                        i += step)
                    {
                        var v = Math.Round(i, 6);
                        if (v < 0 && HideNegative) _displayedLabels.Add(string.Empty);
                        else if (v < 0 && IsSymmetrical) _displayedLabels.Add(Math.Abs(v).ToString(CultureInfo.InvariantCulture));
                        else _displayedLabels.Add(v.ToString(CultureInfo.InvariantCulture));
                    }
                }
                DisplayStep = step;
                PartStep = 1.0d / RealSteps;
            }
        }


        private void SetSinglePointRange(double floor)
        {
            RealSteps = 1;

            if (Math.Abs(floor) < Double.Epsilon)
            {
                DisplayMinimum = 0.0d;
                DisplayMaximum = 1.0d;
                _displayedLabels = new List<string> {"0", "1"};
                DisplayStep = 1;
                PartStep = 1;
            }
            else
            {
                DisplayMinimum = 0.0d;
                DisplayMaximum = floor;
                _displayedLabels = new List<string> {"0", Math.Round(floor, 6).ToString(CultureInfo.InvariantCulture)};
                DisplayStep = floor;
                PartStep = 1;
            }
        }
    }
}