﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class AxisRange
    {
        private void ProcessDateAxis(Double maxDesriredSteps)
        {

            var smin = (from x in BoundSeries where x.SeriesData != null && x.DataType != "InputParameter" select x.SeriesData.Where(a=>a is DateTime).Min());
            var smax = (from x in BoundSeries where x.SeriesData != null && x.DataType != "InputParameter" select x.SeriesData.Where(a => a is DateTime).Max());

            object min = smin.Min();
            object max = smax.Max();
            if (min == null && max == null) return;

            SourceMinimum = (DateTime?) min ?? max;
            SourceMaximum = (DateTime?) max ?? min;
            ProcessDateOrTimeAxis(maxDesriredSteps);
        }

        private void ProcessDateOrTimeAxis(Double maxDesiredSteps)
        {
            var fixedDescription = FixedAxisDescription;

            var ymin = ((DateTime)SourceMinimum).Year - 1;
            var Mmin = ((DateTime)SourceMinimum).Month - 1;
            var dmin = ((DateTime)SourceMinimum).Day - 1;
            var Hmin = ((DateTime)SourceMinimum).Hour;
            var mmin = ((DateTime)SourceMinimum).Minute;
            var smin = ((DateTime)SourceMinimum).Second;

            var ymax = ((DateTime)SourceMaximum).Year - 1;
            var Mmax = ((DateTime)SourceMaximum).Month - 1;
            var dmax = ((DateTime)SourceMaximum).Day - 1;
            var Hmax = ((DateTime)SourceMaximum).Hour;
            var mmax = ((DateTime)SourceMaximum).Minute;
            var smax = ((DateTime)SourceMaximum).Second;

            var range = new DateTime(0);
            range = range.AddYears(ymax).AddYears(-ymin);
            range = range.AddMonths(Mmax).AddMonths(-Mmin);
            range = range.AddDays(dmax).AddDays(-dmin);
            range = range.AddHours(Hmax).AddHours(-Hmin);
            range = range.AddMinutes(mmax).AddMinutes(-mmin);
            range = range.AddSeconds(smax).AddSeconds(-smin);

            var sMin = double.Parse((ymin * 12 + Mmin).ToString("00000000") + dmin.ToString("00") + Hmin.ToString("00") +
                             mmin.ToString("00") + smin.ToString("00"));
            var sMax = double.Parse((ymax * 12 + Mmax).ToString("00000000") + dmax.ToString("00") + Hmax.ToString("00") +
                             mmax.ToString("00") + smax.ToString("00"));
            var sRange = double.Parse(((range.Year - 1) * 12 + range.Month - 1).ToString("00000000") +
                             (range.Day - 1).ToString("00") + range.Hour.ToString("00") + range.Minute.ToString("00") +
                             range.Second.ToString("00"));
            double degree, ceil, floor;

            if (Math.Abs(sMin - sMax) < 1e-8)
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10(sMax)) - 1);
                ceil = Math.Ceiling(sMax / degree) * degree;
                floor = Math.Floor(sMax / degree) * degree;
            }
            else
            {
                degree = Math.Pow(10, Math.Ceiling(Math.Log10(sRange)-1) - 1);
                ceil = Math.Ceiling(sMax / degree) * degree;
                floor = Math.Floor(sMin / degree) * degree;
            }

            var cs = ceil.ToString("0000000000000000");
            var Mceil = int.Parse(cs.Substring(0, 8));
            var dceil = int.Parse(cs.Substring(8, 2));
            var Hceil = int.Parse(cs.Substring(10, 2));
            var mceil = int.Parse(cs.Substring(12, 2));
            var sceil = int.Parse(cs.Substring(14, 2));

            var fs = floor.ToString("0000000000000000");
            var Mfloor = int.Parse(fs.Substring(0, 8));
            var dfloor = int.Parse(fs.Substring(8, 2));
            var Hfloor = int.Parse(fs.Substring(10, 2));
            var mfloor = int.Parse(fs.Substring(12, 2));
            var sfloor = int.Parse(fs.Substring(14, 2));

            var amp = new DateTime(0);
            amp = amp.AddYears((int)Math.Floor(Mceil/12)).AddYears((int)Math.Floor(-Mfloor/12));
            amp = amp.AddMonths(Mceil%12).AddMonths(-Mfloor%12);
            amp = amp.AddDays(dceil).AddDays(-dfloor ); ///// AddDays(-dfloor-1 )
            amp = amp.AddHours(Hceil).AddHours(-Hfloor);
            amp = amp.AddMinutes(mceil).AddMinutes(-mfloor);
            amp = amp.AddSeconds(sceil).AddSeconds(-sfloor);

            var amplitude =double.Parse(((amp.Year - 1) * 12 + amp.Month - 1).ToString("00000000") +
                             (amp.Day - 1).ToString("00") + amp.Hour.ToString("00") + amp.Minute.ToString("00") +
                             amp.Second.ToString("00"));
            

            var degree2 = Math.Pow(10, Math.Ceiling(Math.Log10(amplitude/maxDesiredSteps)));
            var availableStep = Math.Ceiling(amplitude/degree2/maxDesiredSteps)*degree2;
            
            var dispmax = new DateTime(0);
            var dispmin = new DateTime(0);

            dispmax = dispmax.AddYears((int)Math.Floor(Mceil / 12));
            dispmax = dispmax.AddMonths(Mceil%12);
            dispmax = dispmax.AddDays(dceil);
            dispmax = dispmax.AddHours(Hceil);
            dispmax = dispmax.AddMinutes(mceil);
            dispmax = dispmax.AddSeconds(sceil);

            dispmin = dispmin.AddYears((int)Math.Floor(Mfloor / 12));
            dispmin = dispmin.AddMonths(Mfloor%12);
            dispmin = dispmin.AddDays(dfloor);
            dispmin = dispmin.AddHours(Hfloor);
            dispmin = dispmin.AddMinutes(mfloor);
            dispmin = dispmin.AddSeconds(sfloor);

            if (FixedAxisDescription != null)
            {
                if (FixedAxisDescription.AxisMaxFixed && FixedAxisDescription.AxisMaxDate.HasValue) dispmax = FixedAxisDescription.AxisMaxDate.Value;
                if (FixedAxisDescription.AxisMinFixed && FixedAxisDescription.AxisMinDate.HasValue) dispmin = FixedAxisDescription.AxisMinDate.Value;
            }
            DisplayMaximum = dispmax;
            DisplayMinimum = dispmin;

            _displayedLabels = new List<string>();

            RealSteps = Math.Ceiling(amplitude / availableStep);
            var minticks = ((DateTime)DisplayMinimum).Ticks;
            var maxticks = ((DateTime)DisplayMaximum).Ticks;
            if (dispmax.Hour == 0 && dispmin.Hour == 0 && dispmax.Minute == 0 && dispmin.Minute == 0 && dispmax.Second == 0 && dispmin.Second == 0 && dispmax !=dispmin)
            {
                var roundSteps = RealSteps;
                do
                {
                    var currentDate = new DateTime(minticks + (long)(((maxticks - minticks)) * 1 / roundSteps));
                    if (currentDate.Hour == 0 && currentDate.Minute == 0 && currentDate.Second == 0)
                    {
                        RealSteps = roundSteps;
                        break;
                    }
                    roundSteps++;
                } while (roundSteps <= maxDesiredSteps);
            }
            DisplayStep = availableStep;
            PartStep = 1.0d / RealSteps;
            for (var i = 0; i <= RealSteps; i++)
            {
                var v = (i)/RealSteps;
                var currentDate = new DateTime(minticks + (long) (((maxticks - minticks)) * v));
                if (!string.IsNullOrEmpty(FixedAxisDescription?.Format)) _displayedLabels.Add(currentDate.ToString(FixedAxisDescription.Format));
                else
                {
                    if (Mceil == Mfloor && dceil == dfloor)
                        _displayedLabels.Add(currentDate.ToString("HH:mm:ss"));
                    else if (currentDate.Hour == 0 && currentDate.Minute == 0 && currentDate.Second == 0)
                        _displayedLabels.Add(currentDate.ToString("dd.MM.yyyy"));
                    else _displayedLabels.Add(currentDate.ToString("dd.MM.yyyy HH:mm:ss"));
                }
            }
            _orderedAxisLabels = _displayedLabels.OrderBy(a => a).ToList();
        }

        private void ProcessTimeAxis()
        {
            var min = (TimeSpan) (from x in BoundSeries where x.DataType != "InputParameter" select x.SeriesData.Min()).Min();
            var max = (TimeSpan) (from x in BoundSeries where x.DataType != "InputParameter" select x.SeriesData.Max()).Max();

            SourceMinimum = new DateTime(1, 1, 1, min.Hours, min.Minutes, min.Seconds);
            SourceMaximum = new DateTime(1, 1, 1, max.Hours, max.Minutes, max.Seconds);

            ProcessDateOrTimeAxis(20);
        }
    }
}