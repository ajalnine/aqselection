﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQBasicControlsLibrary;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart 
    {
        private Panel GetLineXYLegendRow(ChartSeriesDescription cs)
        {
            if (cs.SeriesTitle.StartsWith("#")) return null;
            var scale = _sriX?.SourceSeries?.ColorScale ?? _sriY?.SourceSeries?.ColorScale ?? _sriZ?.SourceSeries?.ColorScale ?? ColorScales.HSV2;


            Panel sp = ChartData.ChartLegendType == ChartLegend.Right
                ? (Panel)new StackPanel {Orientation = Orientation.Vertical, Margin = new Thickness(0, 0, 0, 0), VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Left}
                : (Panel)new WrapPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(20, 0, 20, 0), VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center};

            if (cs.ZColumnName == null && cs.WColumnName == null)
            {
                return(GetGenericLegendRow(cs));
            }
            
            if (cs.ZColumnName != null && (cs.WColumnName != cs.ZColumnName))
            {
                var source = ZAxisRange.BoundSeries.FirstOrDefault(a => a.SourceColumnName == cs.ZColumnName);

                if (source == null) return sp;

                var tb = GetAxisTitleForLegend(cs.ZColumnName, cs);

                sp.Children.Add(tb);
                var values = source.SeriesData.Distinct();
                var valuesCount = values.Count();
                if (valuesCount > 0)
                {
                    var isText = ZAxisRange.DataType.ToLower().Contains("string");
                    if ((valuesCount <= 24 && isText) || (valuesCount <= 10 && !isText) || (scale & ColorScales.BW) == ColorScales.BW)
                        GetColorDiscreteXYLegend(source, sp, cs, GetLayerBrushes());
                    else GetColorScaleXYLegend(sp, source);
                }
            }
            if (cs.SeriesType == ChartSeriesType.StairsOnX || cs.SeriesType == ChartSeriesType.StairsOnY || cs.SeriesType == ChartSeriesType.Circles) return sp;
            if (cs.WColumnName != null)
            {
                var tb = GetAxisTitleForLegend(cs.WColumnName, cs);
                sp.Children.Add(tb);
                CreateMarkerShapeLegend(cs, sp); 
            }

            if (cs.VColumnName != null)
            {
                var tb = GetAxisTitleForLegend(cs.VColumnName, cs);
                sp.Children.Add(tb);
            }
            return sp;
        }
        
        private void Ug_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
            AxisClicked.Invoke(this, new AxisEventArgs {Tag="Z"});
        }

        private bool labelIsDate, _labelHasTime, _labelIsYear, _labelIsMonthAndYear, _labelIsMonth;
        
        private void CheckIsGroupHasTime(IEnumerable<object> orderedlabels)
        {
            if (!labelIsDate) return;
            var g = orderedlabels.Select(d => (DateTime?) d).ToList();
            _labelHasTime = labelIsDate && g.Any(date => date.HasValue && (date.Value.TimeOfDay.Ticks>0));
            _labelIsMonth = !_labelHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1));
            _labelIsYear = !_labelHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1 && date.Value.Month == 1));
            _labelIsMonthAndYear = !_labelHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1)) && g.Where(date => date.HasValue).Select(a=>a.Value.Year).Distinct().Count()>1 && g.Where(date => date.HasValue).Select(a => a.Value.Month).Distinct().Count() > 1; 
        }

        private TextBox GetAxisTitleForLegend(string axisName, ChartSeriesDescription cs)
        {
            var tb = new TextBox
            {
                AcceptsReturn = true,
                BorderThickness = new Thickness(0),
                Foreground = Inverted ? new SolidColorBrush(Colors.White) : defaultForegroundBrush,
                Text = axisName,
                MaxWidth = 100,
                TextWrapping = TextWrapping.Wrap,
                FontSize = 9 * GlobalFontCoefficient,
                Tag = cs,
                Margin = new Thickness(10,0,3,0),
                Background = new SolidColorBrush(Colors.Transparent),
                VerticalAlignment = VerticalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center
            };
            tb.LostFocus += ChartLegend_LostFocus;
            tb.GotFocus += ChartLegend_GotFocus;
            return tb;
        }

        private void OnAxisClicked(object o, AxisEventArgs axisEventArgs)
        {
               if (AxisClicked != null) AxisClicked.Invoke(o, axisEventArgs);
        }

        private  string ProcessTime(object item)
        {
            if (!(item is DateTime)) return item?.ToString();
            var groupName = (_labelHasTime || !labelIsDate)
                ? item.ToString() :
                _labelIsMonthAndYear ? ((DateTime)item).ToString("yyyy MMMM") :
                _labelIsYear ? ((DateTime)item).ToString("yyyy") :
                _labelIsMonth ? ((DateTime)item).ToString("MMMM") : ((DateTime)item).ToString("dd.MM.yyyy");
            return groupName;
        }
    }
}