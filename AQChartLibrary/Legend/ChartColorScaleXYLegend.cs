﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

namespace AQChartLibrary
{
    public partial class Chart
    {
        private void GetColorScaleXYLegend(Panel sp, SeriesRangeInfo source)
        {
            var b = new LinearGradientBrush();
            var c = new LinearGradientBrush();
            var top = ColorConvertor.ColorEnd[source.SourceSeries.ColorScaleMode];
            var bottom = ColorConvertor.ColorOffset[source.SourceSeries.ColorScaleMode];
            var st = 1 / ColorConvertor.ColorPart[source.SourceSeries.ColorScaleMode];
            var partStep = ZAxisRange.PartStep * (top - bottom);
            var mm = _sriX?.SourceSeries?.MarkerMode ?? _sriY?.SourceSeries?.MarkerMode ??
                     _sriZ?.SourceSeries?.MarkerMode ?? MarkerModes.SemiTransparent;
            if (mm == MarkerModes.Ring) mm = MarkerModes.Solid;

            if (ZAxisRange.IsDiscrete && ZAxisRange.RealSteps < 50)
            {
                var space = 1 / ZAxisRange.RealSteps / 20;
                for (double a = bottom; a <= top; a += partStep)
                {
                    var gb1 = new GradientStop { Offset = (a - bottom) / (top - bottom), Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a)) };
                    var gc1 = new GradientStop { Offset = (a - bottom) / (top - bottom), Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a)) };
                    b.GradientStops.Add(gb1);
                    c.GradientStops.Add(gc1);
                    var gb2 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom) - space, Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a)) };
                    var gc2 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom) - space, Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a)) };
                    b.GradientStops.Add(gb2);
                    c.GradientStops.Add(gc2);
                    var gb3 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom) - space, Color = ((SolidColorBrush)defaultBackgroundBrush).Color };
                    var gc3 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom) - space, Color = ((SolidColorBrush)defaultBackgroundBrush).Color };
                    b.GradientStops.Add(gb3);
                    c.GradientStops.Add(gc3);
                    var gb4 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom), Color = ((SolidColorBrush)defaultBackgroundBrush).Color };
                    var gc4 = new GradientStop { Offset = (a - bottom + partStep) / (top - bottom), Color = ((SolidColorBrush)defaultBackgroundBrush).Color };
                    b.GradientStops.Add(gb4);
                    c.GradientStops.Add(gc4);
                }
            }
            else
            {
                for (double a = top; a >= 0; a -= 0.05)
                {
                    var gb = new GradientStop { Offset = a * st, Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a + bottom)) };
                    var gc = new GradientStop { Offset = a * st, Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(source.SourceSeries.ColorScale, a + bottom)) };
                    b.GradientStops.Add(gb);
                    c.GradientStops.Add(gc);
                }
            }
            
            if (ChartData.ChartLegendType == ChartLegend.Right)
            {
                var spscale = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(15, 5, 0, 5) };
                b.StartPoint = new Point(0, 1);
                b.EndPoint = new Point(0, 0);
                c.StartPoint = new Point(0, 1);
                c.EndPoint = new Point(0, 0);

                c.Opacity = GetStrokeOpacity(mm) * 3;
                b.Opacity = GetFillOpacity(mm) * 3;

                spscale.Children.Add(new Rectangle
                {
                    Stroke = c,
                    Fill = b,
                    Width = 8,
                    Height = (ZAxisRange.IsDiscrete && ZAxisRange.RealSteps <= 12) ? ZAxisRange.RealSteps * 20 : 120,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(0, 0, 5, 0),
                    StrokeThickness = 1
                });

                if (!source.DataType.Contains("String"))
                {
                    var n = ZAxisRange.GetLabels().Count;
                    var k = n <= 10 ? 8.0d : 8.0d / n * 10.0d;
                    var ca = new ChartAxis
                    {
                        AxisOrientation = Orientation.Vertical,
                        IsNominal = ZAxisRange.IsNominal,
                        IsPrimary = ZAxisRange.IsSecondary,
                        MinWidth = 10,
                        DataLabels = ZAxisRange.GetLabels(),
                        EnableCellCondense = true,
                        Margin = new Thickness(0, 0, 0, 0),
                        FontSize = k * GlobalFontCoefficient,
                        Tag = "Z",
                        LabelColor = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black)
                    };
                    spscale.Children.Add(ca);
                    ca.AxisClicked += OnAxisClicked;
                }
                sp.Children.Add(spscale);
            }
            else
            {
                var spscale = new StackPanel { Orientation = Orientation.Vertical, VerticalAlignment = VerticalAlignment.Center, Margin = new Thickness(20,0,20,0)};
                b.StartPoint = new Point(0, 0);
                b.EndPoint = new Point(1, 0);
                c.StartPoint = new Point(0, 0);
                c.EndPoint = new Point(1, 0);

                c.Opacity = GetStrokeOpacity(mm) * 3;
                b.Opacity = GetFillOpacity(mm) * 3;
                var v = (ZAxisRange.IsDiscrete && ZAxisRange.RealSteps <= 12) ? ZAxisRange.RealSteps * 20 : 120;
                spscale.Children.Add(new Rectangle
                {
                    Stroke = c,
                    Fill = b,
                    Width = v,
                    Height = 8,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(0, 0, 0, 0),
                    StrokeThickness = 1
                });

                if (!source.DataType.Contains("String"))
                {
                    var n = ZAxisRange.GetLabels().Count;
                    var k = n <= 10 ? 8.0d : 8.0d / n * 10.0d;
                    var ca = new ChartAxis
                    {
                        AxisOrientation = Orientation.Horizontal,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Top,
                        IsNominal = ZAxisRange.IsNominal,
                        IsPrimary = true,
                        MinHeight = 10,
                        Width = v,
                        DataLabels = ZAxisRange.GetLabels(),
                        EnableCellCondense = true,
                        Margin = new Thickness(0, 0, 0, 0),
                        FontSize = k * GlobalFontCoefficient,
                        Tag = "Z",
                        LabelColor = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black)
                    };
                    spscale.Children.Add(ca);
                    ca.AxisClicked += OnAxisClicked;
                }
                sp.Children.Add(spscale);
            }
        }
    }
}
