﻿using System;
using System.Linq;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void CreateBoxWhiskerLegend(Panel legendPanel)
        {
            var box =
                (from c in ChartData.ChartSeries where c.SeriesType == ChartSeriesType.BoxWhiskers select c)
                    .FirstOrDefault();
            if (box == null) return;
            var bwl = new BoxWhiskerLegend
            {
                Mode = box.Options,
                Stroke = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(box.ColorScale)),
            };
            var fillBrush = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(box.ColorScale));
                fillBrush.Opacity = GetFillOpacity(box.MarkerMode);
            bwl.Fill = fillBrush;
            bwl.FontSize = 7 * GlobalFontCoefficient;
            legendPanel.Children.Add(bwl);
        }
   }
}