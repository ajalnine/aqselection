﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using System;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private Panel GetGenericLegendRow(ChartSeriesDescription cs)
        {
            bool wide = false;
            if (cs != null)
            {
                var serie = _sriX ?? _sriY ?? _sriZ;
                wide = serie!=null && (serie.SourceSeries.ColorScale & ColorScales.BW) == ColorScales.BW;
            }

            if (cs.SeriesTitle.StartsWith("#")) return null;
            var color = (cs.LineColor != null)
                ? cs.LineColor
                : (cs.MarkerColor != null)
                    ? cs.MarkerColor
                    : (cs.FillColor != null)
                        ? cs.FillColor
                        : ColorConvertor.GetDefaultColorForScale(cs.ColorScale);
            var stroke = ConvertStringToStroke(color);
            stroke.Opacity = GetStrokeOpacity(_sriX?.SourceSeries?.MarkerMode ?? _sriY?.SourceSeries?.MarkerMode ?? _sriZ?.SourceSeries?.MarkerMode ?? MarkerModes.SemiTransparent);
            var fill = ConvertStringToStroke(color);
            fill.Opacity = GetFillOpacity(_sriX?.SourceSeries?.MarkerMode ?? _sriY?.SourceSeries?.MarkerMode ?? _sriZ?.SourceSeries?.MarkerMode ?? MarkerModes.SemiTransparent);

            var sp = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 0, 0) };
            
            sp.Children.Add(new Rectangle
            {
                Stroke = stroke,
                Fill = fill,
                Width = wide ? 12: 6,
                Margin = ChartData.ChartLegendType == ChartLegend.Right
                    ? new Thickness(5, 0, 5, 0)
                    : new Thickness(1, 0, 5, 0),
                Height = wide ? 12 : 6,
                VerticalAlignment = VerticalAlignment.Center,
            });
            
            var tb = new TextBox
            {
                AcceptsReturn = true,
                BorderThickness = new Thickness(0),
                Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black),
                Text = cs.SeriesTitle,
                MaxWidth = 100,
                TextWrapping = TextWrapping.Wrap,
                FontSize = 8 * GlobalFontCoefficient,
                Margin = ChartData.ChartLegendType == ChartLegend.Right
                    ? new Thickness(1, 0, 5, 0)
                    : new Thickness(1, 0, 10, 0),
                Tag = cs,
                Background = new SolidColorBrush(Colors.Transparent),
                VerticalAlignment = VerticalAlignment.Center
            };
            tb.LostFocus += ChartLegend_LostFocus;
            tb.GotFocus += ChartLegend_GotFocus;

            sp.Children.Add(tb);
            return sp;
        }
    }
}