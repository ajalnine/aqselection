﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;

namespace AQChartLibrary
{
    public partial class Chart
    {
        private void GetColorDiscreteXYLegend(SeriesRangeInfo source, Panel sp, ChartSeriesDescription cs, Dictionary<object, Brushes> layerBrushes)
        {
            List<object> orderedlabels;
            var zformat = FixedAxises?.FixedAxisCollection?.SingleOrDefault(a => a.Axis == "Z")?.Format;
            bool datetimeProcessed = false;
            switch (source.DataType)
            {
                case "Double":
                    orderedlabels = source.SeriesData.Distinct().Where(a => a is double).OrderBy(a => a).ToList();
                    break;
                case "Int":
                    orderedlabels = source.SeriesData.Distinct().Where(a => a is int).OrderBy(a => a).ToList();
                    break;
                case "DateTime":
                    orderedlabels = source.SeriesData.Distinct().Where(a => a is DateTime).OrderBy(a => a).ToList();
                    break;
                case "TimeSpan":
                    orderedlabels = source.SeriesData.Distinct().Where(a => a is TimeSpan).OrderBy(a => a).ToList();
                    break;
                default:
                    double dummy;
                    DateTime dummyDT;
                    TimeSpan dummyTS;
                    if (source.SeriesData.Distinct().All(a => double.TryParse(a?.ToString(), out dummy) || a?.ToString() == " Прочие" || string.IsNullOrEmpty(a?.ToString()) || a?.ToString() == "#" || a == DBNull.Value))
                    {
                        var all = source.SeriesData.Distinct().ToList();
                        if (ReverseOrderedLegendDiscreteItems)
                        {
                            var orderedpart = all.Where(a => a != null && a != DBNull.Value && a?.ToString() != "#" && double.TryParse(a?.ToString(), out dummy)).OrderByDescending(a => double.Parse(a?.ToString())).ToList();
                            var other = all.Except(orderedpart).OrderByDescending(a => a)?.ToList();
                            orderedlabels = orderedpart.Union(other).ToList();
                        }
                        else
                        {
                            var orderedpart = all.Where(a => a != null && a != DBNull.Value && a?.ToString() != "#" && double.TryParse(a?.ToString(), out dummy)).OrderBy(a => double.Parse(a?.ToString())).ToList();
                            var other = all.Except(orderedpart).OrderBy(a => a)?.ToList();
                            orderedlabels = orderedpart.Union(other).ToList();
                        }
                    }
                    else if (source.SeriesData.Distinct().All(a => DateTime.TryParse(a?.ToString(), out dummyDT) || a?.ToString() == " Прочие" || string.IsNullOrEmpty(a?.ToString()) || a?.ToString() == "#" || a == DBNull.Value))
                    {
                        var all = source.SeriesData.Distinct().ToList();
                        var allDates = new List<Tuple<DateTime, object>>();
                        foreach (var v in all)
                        {
                            if (string.IsNullOrEmpty(v?.ToString()) || v == DBNull.Value || v?.ToString() == "#" || !DateTime.TryParse(v?.ToString(), CultureInfo.CurrentCulture, DateTimeStyles.None, out dummyDT)) continue;
                            allDates.Add(new Tuple<DateTime, object>( dummyDT, v));
                        }

                        var orderedpart = ReverseOrderedLegendDiscreteItems
                            ? allDates.OrderByDescending(a => a.Item1).Select(a => new Tuple<string, object>(a.Item1.ToString(zformat, CultureInfo.CurrentCulture), a.Item2)).ToList()
                            : allDates.OrderBy(a => a.Item1).Select(a => new Tuple<string, object>(a.Item1.ToString(zformat, CultureInfo.CurrentCulture), a.Item2)).ToList();
                        var other = all.Except(orderedpart.Select(a=>a.Item2)).OrderByDescending(a => a).Select(a=> new Tuple<string, object>(a?.ToString(), a)).ToList();
                        orderedlabels = orderedpart.Union(other).Select(a=>(object)a).ToList();
                        datetimeProcessed = true;
                    }
                    else if (source.SeriesData.Distinct().All(a => TimeSpan.TryParse(a?.ToString(), out dummyTS) || a?.ToString() == " Прочие" || string.IsNullOrEmpty(a?.ToString()) || a?.ToString() == "#" || a == DBNull.Value))
                    {
                        var all = source.SeriesData.Distinct().ToList();
                        if (ReverseOrderedLegendDiscreteItems)
                        {
                            var orderedpart =
                                all.Where(a =>
                                        a != null && a != DBNull.Value && a?.ToString() != "#" &&
                                        TimeSpan.TryParse(a?.ToString(), out dummyTS))
                                    .OrderByDescending(a => TimeSpan.Parse(a?.ToString())).ToList();
                            var other = all.Except(orderedpart).OrderByDescending(a => a)?.ToList();
                            orderedlabels = orderedpart.Union(other).ToList();
                        }
                        else
                        {
                            var orderedpart =
                                all.Where(a =>
                                        a != null && a != DBNull.Value && a?.ToString() != "#" &&
                                        TimeSpan.TryParse(a?.ToString(), out dummyTS))
                                    .OrderBy(a => TimeSpan.Parse(a?.ToString())).ToList();
                            var other = all.Except(orderedpart).OrderBy(a => a)?.ToList();
                            orderedlabels = orderedpart.Union(other).ToList();
                        }
                    }
                    else
                    {
                        if (ReverseOrderedLegendDiscreteItems)
                        {
                            orderedlabels = source.SeriesData.Distinct().Where(a => a != null && a != DBNull.Value && a?.ToString() != "#").OrderByDescending(a => a).ToList();
                        }
                        else
                        {
                            orderedlabels = source.SeriesData.Distinct().Where(a => a != null && a != DBNull.Value && a?.ToString() != "#").OrderBy(a => a).ToList();
                        }
                    }
                    break;
            }

            Panel ug;
            var count = orderedlabels.Count;
            if (ChartData.ChartLegendType == ChartLegend.Right)
            {
                ug = new UniformGrid();
                if (count > 12 && count <= 24)
                {
                    ((UniformGrid)ug).Columns = 2;
                    if (count > 12) ((UniformGrid)ug).Rows = (int)Math.Ceiling((double)count / 2.0d);
                }
                else
                {
                    ((UniformGrid)ug).Rows = count > 12 ? 12 : count;
                    if (count > 12) ((UniformGrid)ug).Columns = (int)Math.Ceiling((double)count / 12.0d);
                }
            }
            else
            {
                ug = new WrapPanel { Orientation = Orientation.Horizontal, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Left };
            }

            ug.MouseLeftButtonDown += Ug_MouseLeftButtonDown;
            if (!datetimeProcessed)
            {
                foreach (var item in orderedlabels)
                {
                    if (item?.ToString() == "#" || string.IsNullOrEmpty(item?.ToString())) ug.Children.Add(GetSimpleLegendRow(layerBrushes["#"], "Нет значения", cs, string.IsNullOrEmpty(zformat)));
                    else ug.Children.Add(GetSimpleLegendRow(layerBrushes[item], item?.ToString(), cs, true));
                }
            }
            else
            {
                foreach (var item in orderedlabels.OfType<Tuple<string, object>>())
                {
                    if (item.Item1=="#" || string.IsNullOrEmpty(item.Item1?.ToString())) ug.Children.Add(GetSimpleLegendRow(layerBrushes["#"], "Нет значения", cs, string.IsNullOrEmpty(zformat)));
                    else ug.Children.Add(GetSimpleLegendRow(layerBrushes[item.Item2 ?? "#"], item.Item1, cs, zformat == null));
                }
            }
            sp.Children.Add(ug);
        }
    }
}
