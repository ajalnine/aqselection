﻿using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawLegend()
        {
            Panel legendPanel;
            if (!SelectLegendPanel(out legendPanel)) return;

            legendPanel.Children.Clear();
            CreateMathFunctionLegend(legendPanel);
            CreateChartSeriesLegend(legendPanel);
            CreateBoxWhiskerLegend(legendPanel);
        }

        private void CreateChartSeriesLegend(Panel legendPanel)
        {
            foreach (ChartSeriesDescription cs in ChartData.ChartSeries)
            {
                Panel sp = GetLegendRow(cs);
                if (sp != null) legendPanel.Children.Add(sp);
            }
        }

        private bool SelectLegendPanel(out Panel legendPanel)
        {
            legendPanel = null;
            if (ChartData.ChartLegendType == ChartLegend.None) return false;

            switch (ChartData.ChartLegendType)
            {
                case ChartLegend.Right:
                    legendPanel = RightLegendPlace;
                    break;
                case ChartLegend.Bottom:
                    legendPanel = BottomLegendPlace; //BottomLegendPanel;
                    break;
                case ChartLegend.Top:
                    legendPanel = TopLegendPlace; //FloatingLegendPanel;
                    break;
            }
            return true;
        }

        private Panel GetLegendRow(ChartSeriesDescription cs)
        {
            var xBound = BottomAxisRange.BoundSeries.Union(TopAxisRange.BoundSeries).FirstOrDefault(a=>a.SeriesName == cs.SeriesTitle);
            var yBound = RightAxisRange.BoundSeries.Union(LeftAxisRange.BoundSeries).FirstOrDefault(a => a.SeriesName == cs.SeriesTitle);
            var zBound = ZAxisRange.BoundSeries.FirstOrDefault(a => a.SeriesName == cs.SeriesTitle); ;
            var wBound = WAxisRange.BoundSeries.FirstOrDefault(a => a.SeriesName == cs.SeriesTitle); ;
            var vBound = VAxisRange.BoundSeries.FirstOrDefault(a => a.SeriesName == cs.SeriesTitle); ;
            _sourceZ = zBound?.SeriesData;
            _sriZ = zBound;
            _sriW = wBound;
            _sriV = vBound;
            _sriX = xBound;
            _sriY = yBound;

            return cs.SeriesType == ChartSeriesType.LineXY || cs.SeriesType == ChartSeriesType.CircularGraph || cs.SeriesType == ChartSeriesType.StairsOnX || cs.SeriesType == ChartSeriesType.StairsOnY || cs.SeriesType == ChartSeriesType.Dummy ||cs.SeriesType == ChartSeriesType.Circles || cs.SeriesType== ChartSeriesType.HexBin ? GetLineXYLegendRow(cs) : GetGenericLegendRow(cs);
        }
    }
}