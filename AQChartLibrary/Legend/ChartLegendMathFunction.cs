﻿using ApplicationCore.CalculationServiceReference;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void CreateMathFunctionLegend(Panel legendPanel)
        {
            if (MathFunctionDescriptions == null) return;
            foreach (var sp in MathFunctionDescriptions.Where(m => m!=null && !String.IsNullOrEmpty(m.Description) && !m.Description.StartsWith("#"))
                .Select(m => GetSimpleLegendRow(new Brushes {strokeBrush = ConvertStringToStroke(m.LineColor), fillBrush = ConvertStringToStroke(m.LineColor) },   m.Description, null, true)).Where(sp => sp != null))
            {
                legendPanel.Children.Add(sp);
            }
        }

        private StackPanel GetSimpleLegendRow(Brushes brushes, string description, ChartSeriesDescription cs, bool isEditable)
        {
            bool wide = false;
            if (cs != null)
            {
                var serie = _sriX ?? _sriY ?? _sriZ;
                wide = (serie.SourceSeries.ColorScale & ColorScales.BW) == ColorScales.BW;
            }
            var sp = new StackPanel {Orientation = Orientation.Horizontal, Margin = new Thickness(5, 0, 0, 0), VerticalAlignment = VerticalAlignment.Center};
            var margin = ChartData.ChartLegendType == ChartLegend.Right
                ? new Thickness(3, 0, 5, 0)
                : new Thickness(3, 0, 15, 0);

            sp.Children.Add(new Rectangle
                {
                    Stroke = brushes.strokeBrush,
                    Fill = brushes.fillBrush,
                    Width =  wide ? 12 : 6,
                    Height = wide ? 12 : 6,
                    VerticalAlignment = VerticalAlignment.Center
                });

            if (isEditable)
            {
                var tb = new TextBox
                {
                    AcceptsReturn = true,
                    BorderThickness = new Thickness(0),
                    Text = " " + description,
                    MaxWidth = 120,
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Margin = margin,
                    Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black),
                    Tag = cs,
                    Background = new SolidColorBrush(Colors.Transparent),
                    VerticalAlignment = VerticalAlignment.Center
                };
                tb.LostFocus += ChartLegendSimple_LostFocus;
                tb.GotFocus += ChartLegend_GotFocus;
                sp.Children.Add(tb);
            }
            else
            {
                var tb = new TextBlock
                {
                    Text = " " + description,
                    MaxWidth = 120,
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Margin = margin,
                    Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black),
                    Tag = cs,
                    VerticalAlignment = VerticalAlignment.Center
                };
                sp.Children.Add(tb);
            }
            return sp;
       }
    }
}