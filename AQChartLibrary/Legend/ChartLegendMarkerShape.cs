﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

namespace AQChartLibrary
{
    public partial class Chart
    {
        private void CreateMarkerShapeLegend(ChartSeriesDescription cs, Panel sp)
        {
            var source = WAxisRange.BoundSeries.FirstOrDefault(a => a.SourceColumnName == cs.WColumnName);
            bool wide = false;
            if (cs != null)
            {
                var serie = _sriX ?? _sriY ?? _sriZ;
                wide = (serie.SourceSeries.ColorScale & ColorScales.BW) == ColorScales.BW;
            }
            var values = source.SeriesData.Distinct();
            var valuesCount = values.Count();
            if (valuesCount > 0)
            {
                var isText = WAxisRange.DataType.ToLower().Contains("string");
                if (((valuesCount <= 16 && isText) || (valuesCount <= 10 && !isText)) && cs.MarkerShapeMode != MarkerShapeModes.Range && cs.MarkerShapeMode != MarkerShapeModes.Bubble)
                {
                    var orderedlabels = source.DataType == "System.Double" || source.DataType == "Double"
                        ? source.SeriesData.Distinct().Where(a => a is double).OrderBy(a => a).ToList()
                        : source.SeriesData.Distinct().Where(a => a != null && a != DBNull.Value).OrderBy(a => a).ToList();
                    if (ReverseOrderedLegendDiscreteItems) orderedlabels = orderedlabels.OrderByDescending(a => a).ToList();
                    var count = orderedlabels.Count();
                    Panel ug;
                    if (ChartData.ChartLegendType == ChartLegend.Right)
                    {
                        ug = new UniformGrid { HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center };
                        if (count > 8 && count <= 16)
                        {
                            ((UniformGrid)ug).Columns = 2;
                            if (count > 8) ((UniformGrid)ug).Rows = (int)Math.Ceiling((double)count / 2.0d);
                        }
                        else
                        {
                            ((UniformGrid)ug).Rows = count > 8 ? 8 : count;
                            if (count > 8) ((UniformGrid)ug).Columns = (int)Math.Ceiling((double)count / 8.0d);
                        }
                    }
                    else
                    {
                        ug = new StackPanel { Orientation = Orientation.Horizontal, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Left };
                    }
                    var wlist = source.SeriesData.Where(a => a != null && a != DBNull.Value && !string.IsNullOrEmpty(a.ToString())).Distinct().OrderBy(a => a).ToList();
                    if (cs.WColumnName != cs.ZColumnName)
                    {
                        foreach (var item in orderedlabels)
                        {
                            double? w = cs.MarkerShapeMode == MarkerShapeModes.Multishape ? wlist.IndexOf(item) : WAxisRange.TransformValueMinMax(item, source);
                            if (w.HasValue && !Double.IsNaN(w.Value))
                            {
                                DrawSingleMarkerForLegend(wide ? new Size(16, 16) : new Size(8, 8), GetDefaultBrushes(), w.Value, ug, cs.MarkerShapeMode, cs.MarkerMode, ProcessTime(item));
                            }
                        }
                    }
                    else
                    {
                        var b = GetLayerBrushes();
                        foreach (var item in orderedlabels)
                        {
                            double? w = cs.MarkerShapeMode == MarkerShapeModes.Multishape ? wlist.IndexOf(item) : WAxisRange.TransformValueMinMax(item, source);
                            if (w.HasValue && !Double.IsNaN(w.Value))
                            {
                                DrawSingleMarkerForLegend(wide ? new Size(16, 16) : new Size(8, 8), b[item ?? "#"], w.Value, ug, cs.MarkerShapeMode, cs.MarkerMode, ProcessTime(item));
                            }
                        }
                    }

                    sp.Children.Add(ug);
                }
                else
                {
                    var spmarkers = new StackPanel { Orientation = Orientation.Horizontal, VerticalAlignment = VerticalAlignment.Center };

                    var sizes = cs.MarkerShapeMode == MarkerShapeModes.Multishape ? new List<double>() { 1, 2, 3 } : new List<double>() { 0.2, 0.5, 0.9 };

                    if (cs.ZColumnName != null)
                    {
                        var b = new Brushes
                        {
                            strokeBrush = ConvertStringToStroke(ColorConvertor.GetColorStringForScale(cs.ColorScale, 0.05)),
                            fillBrush = ConvertStringToStroke(ColorConvertor.GetColorStringForScale(cs.ColorScale, 0.05))
                        };
                        DrawSingleMarkerForLegend(new Size(10, 10), b, sizes[0], spmarkers,
                            cs.MarkerShapeMode, cs.MarkerMode, null);
                        DrawSingleMarkerForLegend(new Size(10, 10), b, sizes[1], spmarkers,
                            cs.MarkerShapeMode, cs.MarkerMode, null);
                        DrawSingleMarkerForLegend(new Size(10, 10), b, sizes[2], spmarkers,
                            cs.MarkerShapeMode, cs.MarkerMode, null);
                    }
                    else
                    {
                        var color = GetDefaultBrushes();
                        DrawSingleMarkerForLegend(new Size(10, 10), color, sizes[0], spmarkers, cs.MarkerShapeMode, cs.MarkerMode,
                            null);
                        DrawSingleMarkerForLegend(new Size(10, 10), color, sizes[1], spmarkers, cs.MarkerShapeMode, cs.MarkerMode,
                            null);
                        DrawSingleMarkerForLegend(new Size(10, 10), color, sizes[2], spmarkers, cs.MarkerShapeMode, cs.MarkerMode,
                            null);
                    }
                    sp.Children.Add(spmarkers);
                }
            }
        }
    }
}
