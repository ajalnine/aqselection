﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class BoxWhiskerLegend
    {
        public static readonly DependencyProperty ModeProperty =
            DependencyProperty.Register("Mode", typeof (string), typeof (BoxWhiskerLegend),
                                        new PropertyMetadata(ModePropertyChanged));

        public static readonly DependencyProperty FillProperty =
            DependencyProperty.Register("Fill", typeof (Brush), typeof (BoxWhiskerLegend),
                                        new PropertyMetadata(new SolidColorBrush(Colors.LightGray), FillPropertyChangedCallback));

        private static void FillPropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o == null) return;
            var th = (BoxWhiskerLegend) o;
            var fillBrush = (Brush) e.NewValue;
            th.Violin.Fill = fillBrush;
            th.ViolinBox.Fill = fillBrush;
            th.Box.Fill = fillBrush;
            th.Pie.Fill = fillBrush;
            th.Outlier.Fill = fillBrush;
            foreach (var p in th.Points.Children)
            {
                ((Ellipse) p).Fill = fillBrush;
            }
        }

        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof (Brush), typeof (BoxWhiskerLegend),
                                        new PropertyMetadata(new SolidColorBrush(Colors.Gray), StrokePropertyChangedCallback));
        //Для рендеринга слайдов DP устанавливаются принудительно
        private static void StrokePropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o == null) return;
            var th = (BoxWhiskerLegend)o;
            var strokeBrush = (Brush)e.NewValue;
            th.Violin.Stroke = strokeBrush;
            th.ViolinBox.Stroke = strokeBrush;
            th.Box.Stroke = strokeBrush;
            th.Pie.Stroke = strokeBrush;
            th.Whisker1.Stroke = strokeBrush;
            th.Whisker2.Stroke = strokeBrush;
            th.Outlier.Stroke = strokeBrush;
            th.OutlierLine1.Stroke = strokeBrush;
            th.OutlierLine2.Stroke = strokeBrush;
            foreach (var p in th.Points.Children)
            {
                ((Ellipse)p).Stroke = strokeBrush;
            }
        }
        
        public new double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FontSize.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(BoxWhiskerLegend), new PropertyMetadata(9.0d, FontSizePropertyChangedCallback));

        private static void FontSizePropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (o == null) return;
            var th = (BoxWhiskerLegend)o;
            th.MaxBox.FontSize = (double) e.NewValue;
            th.MinBox.FontSize = (double)e.NewValue;
            th.MediumPoint.FontSize = (double)e.NewValue;
            th.MaxWhisker.FontSize = (double)e.NewValue;
            th.MinWhisker.FontSize = (double)e.NewValue;
            th.Outliers.FontSize = (double)e.NewValue;
            th.Extremes.FontSize = (double)e.NewValue;
        }


        public BoxWhiskerLegend()
        {
            InitializeComponent();
        }

        public string Mode
        {
            get { return (string) GetValue(ModeProperty); }
            set { SetValue(ModeProperty, value); }
        }

        public Brush Fill
        {
            get { return (Brush) GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        public Brush Stroke
        {
            get { return (Brush) GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static void ModePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var bwl = o as BoxWhiskerLegend;
            if (bwl != null) bwl.UpdateDescriptions();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateDescriptions();
        }

        private void UpdateDescriptions()
        {
            var hasBox = Mode.Contains("Box");
            var hasCenter = Mode.Contains("Center");
            var hasViolin = Mode.Contains("Violin");
            var hasPoints = Mode.Contains("Point");
            var hasPie = Mode.Contains("Pie");
            var centerName = Mode.Contains("Mean") ? "Mean" : "Median";
            var nonoutliers = Mode.Contains("NonOutliers");

            LayoutRoot.Visibility = Visibility.Visible;
            
            if (centerName == "Mean")
            {
                MaxWhisker.Text = "+СКО";
                MaxBox.Text = "+Ошибка среднего";
                MediumPoint.Text = "Среднее";
                MinBox.Text = "-Ошибка среднего";
                MinWhisker.Text = "-СКО";
            }
            if (centerName == "Median")
            {
                MaxWhisker.Text = "Max без выбросов";
                MaxBox.Text = "75%";
                MediumPoint.Text = "Медиана";
                MinBox.Text = "25%";
                MinWhisker.Text = "Min без выбросов";
            }
            var textVisibility = hasBox | hasViolin ? Visibility.Visible : Visibility.Collapsed;

            MaxWhisker.Visibility = textVisibility;
            MinWhisker.Visibility = textVisibility;
            MaxBox.Visibility = textVisibility;
            MinBox.Visibility = textVisibility;
            MediumPoint.Visibility = hasCenter || hasBox || hasViolin || hasPie ? Visibility.Visible : Visibility.Collapsed;
            Center.Visibility = hasCenter ? Visibility.Visible : Visibility.Collapsed;
            Pie.Visibility = hasPie ? Visibility.Visible : Visibility.Collapsed;
            Box.Visibility = hasBox && !hasViolin ? Visibility.Visible : Visibility.Collapsed;
            Whisker1.Visibility = hasBox || hasViolin ? Visibility.Visible : Visibility.Collapsed;
            Whisker2.Visibility = hasBox || hasViolin ? Visibility.Visible : Visibility.Collapsed;
            Violin.Visibility = hasViolin ? Visibility.Visible : Visibility.Collapsed;
            ViolinBox.Visibility = hasBox && hasViolin ? Visibility.Visible : Visibility.Collapsed;
            Points.Visibility = hasPoints ? Visibility.Visible : Visibility.Collapsed;
            if (nonoutliers || !(hasBox | hasViolin))
            {
                OutliersPanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                OutliersPanel.Visibility = Visibility.Visible;
                Outliers.Text = "Выбросы";
                Extremes.Text = "Экстремальные";
            }
        }
    }
}
