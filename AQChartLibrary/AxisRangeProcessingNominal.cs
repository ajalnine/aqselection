﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class AxisRange
    {
        private void ProcessNominalAxis()
        {
            SourceMinimum = null;
            SourceMaximum = null;
            DisplayMinimum = null;
            DisplayMaximum = null;
            IsNominal = true;
            _axisLabels =
                (from s in BoundSeries.First().SeriesData
                    select s != null ? s.ToString(): string.Empty).ToList();

            switch (FixedAxisDescription?.DataType?.Split('.').LastOrDefault()?.ToLower())
            {
                case "datetime":
                    _displayedLabels = new List<string>();
                    foreach (var s in _axisLabels)
                    {
                        DateTime dt;
                        _displayedLabels.Add(DateTime.TryParse(s, CultureInfo.CurrentCulture, DateTimeStyles.None, out dt)
                            ? dt.ToString(FixedAxisDescription.Format)
                            : s?.ToString());
                    }
                    break;

                default:
                    _displayedLabels = _axisLabels;
                    break;
            }

            _orderedAxisLabels = _displayedLabels.OrderBy(a => a).ToList();
            RealSteps = _displayedLabels.Count;
            DisplayStep = 1;
            PartStep = 1.0d/RealSteps;
        }

        private void ProcessStringAxis()
        {
            SourceMinimum = null;
            SourceMaximum = null;
            DisplayMinimum = null;
            DisplayMaximum = null;
            IsNominal = true;
            _index.Clear();

            _axisLabels =
                (from s in BoundSeries.First().SeriesData
                    where s != null && !String.IsNullOrEmpty(s.ToString()) && s.ToString() != "#"
                    select s.ToString()).Distinct().ToList();
            switch (FixedAxisDescription?.DataType?.Split('.').LastOrDefault()?.ToLower())
            {
                case "datetime":
                    _displayedLabels = new List<string>();
                    foreach (var s in _axisLabels)
                    {
                        DateTime dt;
                        _displayedLabels.Add(DateTime.TryParse(s, CultureInfo.CurrentCulture, DateTimeStyles.None, out dt)
                            ? dt.ToString(FixedAxisDescription.Format)
                            : s?.ToString());
                    }
                    break;

                default:
                    _displayedLabels = _axisLabels;
                    break;
            }

            _orderedAxisLabels = _displayedLabels.OrderBy(a => a).ToList();
            if (Axis == AxisType.Z)
            {
                _displayedLabels = _orderedAxisLabels;
                _axisLabels = _orderedAxisLabels;
            }
            for (int i = 0; i < _axisLabels.Count; i++)_index.Add(_axisLabels[i],i);
            RealSteps = _displayedLabels.Count;
            DisplayStep = 1;
            PartStep = 1.0d / RealSteps;
        }
    }
}