﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public delegate double MathFunctionDelegate(ChartRangesInfo cri, double x);

    public class MathFunctionDescription
    {
        public string Description;
        public string Comment;
        public string LineColor;
        public bool Filled;
        public object ColorByZScale;
        public string ZValueSeriesTitle;
        public double LineSize;
        public MathFunctionDelegate MathFunctionCalculator;
        public int PixelStep;
        public object Optional;
        public List<double?> MarkPointsOnXAxis;
    }

    public partial class Chart
    {
        public List<MathFunctionDescription> MathFunctionDescriptions;

        private void DrawFunctions()
        {
            if (MathFunctionDescriptions == null) return;

            var chartRangesInfo = GetChartRanges();
            //if (chartRangesInfo.XDownMin == null || chartRangesInfo.XDownMax == null) return;

            var actualWidth = SeriesSurface.ActualWidth;
            var actualHeight = SeriesSurface.ActualHeight;

            foreach (var d in MathFunctionDescriptions)
            {
                if (d==null ||(d.LineColor == null && d.ColorByZScale == null)  || d.MathFunctionCalculator == null || d.PixelStep <= 0)continue;
                Brush stroke, fill;
                
                if (d.ColorByZScale != null)
                {
                    double? z = null;
                    if (d.ZValueSeriesTitle == null && _sriZ != null) z = _sriZ.BoundAxis.TransformValueZ(d.ColorByZScale, _sriZ, _sriX.SourceSeries.ColorScaleMode);
                    else
                    {
                        var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == d.ZValueSeriesTitle);
                        if (serieForZ != null) z = serieForZ.BoundAxis.TransformValueZ(d.ColorByZScale, serieForZ, _sriX.SourceSeries.ColorScaleMode);
                    }
                    var colorScale = _sriX?.SourceSeries.ColorScale ?? _sriY.SourceSeries.ColorScale;
                    var color = z.HasValue && !Double.IsNaN(z.Value)
                            ? ColorConvertor.GetColorStringForScale(colorScale, z.Value)
                            : "#ffe0e0e0";
                    stroke = ConvertStringToStroke(color);
                    fill = ConvertStringToStroke(color);
                    fill.Opacity = 0.15;
                }
                else 
                {
                    stroke = ConvertStringToStroke(d.LineColor);
                    fill = ConvertStringToStroke(d.LineColor);
                    fill.Opacity = 0.15;
                }


                var dc = new DoubleCollection();
                if (d.LineSize < 1)
                {
                    dc.Add(1);
                    dc.Add(1/d.LineSize);
                }

                var pathData = string.Empty;
                var isStarted = false;
                if (d.PixelStep != 0)
                {
                    for (double x = 0; Math.Round(x, 0) <= Math.Round(actualWidth, 0); x += d.PixelStep)
                    {
                        var argument = x/actualWidth;
                        var func = d.MathFunctionCalculator.Invoke(chartRangesInfo, argument);
                        if (double.IsNaN(func) || double.IsInfinity(func) || func > 10.0d || func < -10.0d)
                        {
                            isStarted = false;
                            continue;
                        }
                        if (!isStarted)
                        {
                            pathData += "M ";
                            isStarted = true;
                        }
                        else pathData += "L ";
                        var y = actualHeight - actualHeight*func;

                        pathData += x.ToString(CultureInfo.InvariantCulture) + "," + y.ToString(CultureInfo.InvariantCulture);
                    }
                }

                if (pathData.Length <= 3) continue;
                var zeroY = _sriY.BoundAxis.TransformValue(0.0d, _sriY) ?? 0.0d;
                if (d.Filled)
                {
                    pathData += $"L {actualWidth}, {actualHeight * (1 - zeroY)} L 0, {actualHeight * (1 - zeroY)}  Z";
                }
                var xaml = $"<Path xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' Data='{pathData}'/>";
                var p = (Path) XamlReader.Load(xaml);
                p.Stroke = stroke;
                p.StrokeThickness = d.LineSize;
                p.StrokeDashArray = dc;
                p.StrokeLineJoin = PenLineJoin.Round;
                if (d.Filled)p.Fill = fill;
                Geometry g = new RectangleGeometry {Rect = new Rect {X = 0, Y = 0, Height = actualHeight, Width = actualWidth}};
                p.Clip = g;
                SeriesSurface.Children.Add(p);
                if (d.MarkPointsOnXAxis?.Count>0)
                {
                    if (_sriY.BoundAxis.DataType == "Double")
                    {
                        
                        if (zeroY > 1) zeroY = 1.0d;
                        if (zeroY < 0) zeroY = 0.0d;
                        foreach (var mark in d.MarkPointsOnXAxis)
                        {
                            if (mark.HasValue)
                            {
                                var x = (mark.Value - (double)chartRangesInfo.XDownMin) / ((double)chartRangesInfo.XDownMax - (double)chartRangesInfo.XDownMin);
                                DrawVerticalLine(actualWidth * x, actualHeight - actualHeight * d.MathFunctionCalculator.Invoke(chartRangesInfo, x), actualHeight - actualHeight * zeroY, stroke, d.LineSize);
                            }
                        }
                    }
                    else
                    {
                        foreach (var mark in d.MarkPointsOnXAxis)
                        {
                            if (mark.HasValue)
                            {
                                var x = (mark.Value - (double)chartRangesInfo.XDownMin) / ((double)chartRangesInfo.XDownMax - (double)chartRangesInfo.XDownMin);
                                DrawVerticalLine(actualWidth * x, actualHeight - actualHeight * d.MathFunctionCalculator.Invoke(chartRangesInfo, x), actualHeight, stroke, d.LineSize);
                            }
                        }
                    }
                }
            }
        }

        private void DrawVerticalLine(double value, double y1, double y2, Brush stroke, double thickness)
        {
            if (double.IsNaN(value)) return;
            var line = new Line { X1 = value, X2 = value, Y1 =  (y1 <= ActualHeight && y1 > 0)  ? y1 : (y1 < 0) ? 0 :ActualHeight, Y2 = (y2 <= ActualHeight && y2 > 0) ? y2 : 0, Stroke = stroke, StrokeThickness = thickness, StrokeDashArray = new DoubleCollection {1,2 } };
            SeriesSurface.Children.Add(line);
        }
    }
}