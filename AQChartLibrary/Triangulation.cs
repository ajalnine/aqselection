﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using System.Linq.Expressions;
using AQMathClasses;

namespace AQChartLibrary
{
    public partial class Chart
    {
        private class Triangulation
        {
            private List<Vertice> _vertices;
            private List<Triangle> _triangles;
            int _counter = 0, _maxcounter = 0;

            private IntersectionResult[] goodIntersections = new[] { IntersectionResult.TriangleIntersection, IntersectionResult.SideAB, IntersectionResult.SideBC, IntersectionResult.SideCA };

            public Triangulation(List<Vertice> vertices, Point origin, Size areaSize)
            {
                var superStructure = GetSuperStructure(origin, areaSize);
                _vertices = superStructure.Union(vertices).Distinct().ToList();
                _triangles = GetInitialTriangles(superStructure);
                Triangulate();
            }

            private List<Vertice> GetSuperStructure(Point origin, Size areaSize)
            {
                return new List<Vertice>
                {
                    new Vertice { X = origin.X, Y=origin.Y, Color = Colors.Black, SuperStructure = true },
                    new Vertice { X = origin.X, Y= origin.Y + areaSize.Height, Color = Colors.Black, SuperStructure = true },
                    new Vertice { X = origin.X + areaSize.Width, Y = origin.Y + areaSize.Height, Color = Colors.Black, SuperStructure = true },
                    new Vertice { X = origin.X + areaSize.Width, Y = origin.Y, Color = Colors.Black, SuperStructure = true },
                };
            }

            private List<Triangle> GetInitialTriangles(List<Vertice> superStructure)
            {
                var result = new List<Triangle>();
                var t1 = new Triangle(superStructure[0], superStructure[1], superStructure[2]);
                var t2 = new Triangle(superStructure[2], superStructure[3], superStructure[0]);
                result.Add(t1);
                result.Add(t2);
                t1.NeighborhoodCA = t2;
                t2.NeighborhoodCA = t1;
                return result;
            }
            
            private IntersectionInfo FindIntersection(Vertice v)
            {
                foreach (var tr in _triangles)
                {
                    var intersection = tr.CheckIntersection(v);
                    if (goodIntersections.Contains(intersection)) return new IntersectionInfo { IntersectionResult = intersection, Triangle = tr };
                }
                return new IntersectionInfo { IntersectionResult = IntersectionResult.NoIntersection };
            }

            private void Triangulate()
            {
                if (_vertices.Count < 4) return;
                for (var i = 4; i < _vertices.Count; i++)
                {
                    var intersection = FindIntersection(_vertices[i]);
                    if (intersection == null) continue;

                    var newTrianglesToCheck = new List<Triangle>();
                    var t = intersection.Triangle;

                    switch (intersection.IntersectionResult)
                    {
                        case IntersectionResult.TriangleIntersection:
                            var t1 = new Triangle(t.A, t.B, _vertices[i]);
                            if (t1.GetSquare() > 1e-05) newTrianglesToCheck.Add(t1);
                            var t2 = new Triangle(t.B, t.C, _vertices[i]);
                            if (t2.GetSquare() > 1e-05) newTrianglesToCheck.Add(t2);
                            var t3 = new Triangle(t.C, t.A, _vertices[i]);
                            if (t3.GetSquare() > 1e-05) newTrianglesToCheck.Add(t3);
                            t.NeighborhoodAB?.ReplaceNeighborhood(t, t1);
                            t.NeighborhoodBC?.ReplaceNeighborhood(t, t2);
                            t.NeighborhoodCA?.ReplaceNeighborhood(t, t3);
                            t1.NeighborhoodAB = t.NeighborhoodAB;
                            t1.NeighborhoodBC = t2;
                            t1.NeighborhoodCA = t3;
                            t2.NeighborhoodAB = t.NeighborhoodBC;
                            t2.NeighborhoodBC = t3;
                            t2.NeighborhoodCA = t1;
                            t3.NeighborhoodAB = t.NeighborhoodCA;
                            t3.NeighborhoodBC = t1;
                            t3.NeighborhoodCA = t2;

                            t.Deleted = true;
                            _triangles.Remove(t);
                            break;
                            
                    case IntersectionResult.SideAB:
                            
                            var ab1 = new Triangle(t.A, _vertices[i], t.C);
                            if (ab1.GetSquare() > 1e-05) newTrianglesToCheck.Add(ab1);
                            var ab2 = new Triangle(t.C, _vertices[i], t.B);
                            if (ab2.GetSquare() > 1e-05) newTrianglesToCheck.Add(ab2);

                            Triangle ab3 = null, ab4 = null;
                            var an1 = t.NeighborhoodAB;

                            if (an1 != null)
                            {
                                var vn1 = t.NeighborhoodAB?.VerticeExcept(t);

                                ab3 = new Triangle(t.A, vn1, _vertices[i]);
                                if (ab3.GetSquare() > 1e-05) newTrianglesToCheck.Add(ab3);
                                ab4 = new Triangle(_vertices[i], vn1, t.B);
                                if (ab4.GetSquare() > 1e-05) newTrianglesToCheck.Add(ab4);

                                var bv1 = t.NeighborhoodAB?.GetNeighborhoodByVertices(vn1, t.B);
                                var bv2 = t.NeighborhoodAB?.GetNeighborhoodByVertices(vn1, t.A);
                                bv1?.ReplaceNeighborhood(an1, ab4);
                                bv2?.ReplaceNeighborhood(an1, ab3);
                                ab3.NeighborhoodAB = bv2;
                                ab3.NeighborhoodBC = ab4;
                                ab3.NeighborhoodCA = ab1;

                                ab4.NeighborhoodAB = ab3;
                                ab4.NeighborhoodBC = bv1;
                                ab4.NeighborhoodCA = ab2;
                                an1.Deleted = true;
                                _triangles.Remove(an1);
                            }

                            t.NeighborhoodCA?.ReplaceNeighborhood(t, ab1);
                            t.NeighborhoodBC?.ReplaceNeighborhood(t, ab2);
                                
                            ab1.NeighborhoodAB = ab3;
                            ab1.NeighborhoodBC = ab2;
                            ab1.NeighborhoodCA = t.NeighborhoodCA;

                            ab2.NeighborhoodAB = ab1;
                            ab2.NeighborhoodBC = ab4;
                            ab2.NeighborhoodCA = t.NeighborhoodBC;

                            t.Deleted = true;
                            _triangles.Remove(t);
                            break;
                                
                    case IntersectionResult.SideBC:
                       
                        var bc1 = new Triangle(t.A, t.B, _vertices[i]);
                        if (bc1.GetSquare() > 1e-05) newTrianglesToCheck.Add(bc1);
                        var bc2 = new Triangle(_vertices[i], t.C, t.A);
                        if (bc2.GetSquare() > 1e-05) newTrianglesToCheck.Add(bc2);

                        Triangle bc3 = null, bc4 = null;
                        var bcn = t.NeighborhoodBC;
                        if (bcn != null)
                        {
                            var vn2 = t.NeighborhoodBC?.VerticeExcept(t);

                            bc3 = new Triangle(_vertices[i], t.B, vn2);
                            if (bc3.GetSquare() > 1e-05) newTrianglesToCheck.Add(bc3);
                            bc4 = new Triangle(_vertices[i], vn2, t.C);
                            if (bc4.GetSquare() > 1e-05) newTrianglesToCheck.Add(bc4);

                            var cv1 = t.NeighborhoodBC?.GetNeighborhoodByVertices(vn2, t.B);
                            var cv2 = t.NeighborhoodBC?.GetNeighborhoodByVertices(vn2, t.C);
                            cv1?.ReplaceNeighborhood(bcn, bc3);
                            cv2?.ReplaceNeighborhood(bcn, bc4);

                            bc3.NeighborhoodAB = bc1;
                            bc3.NeighborhoodBC = cv1;
                            bc3.NeighborhoodCA = bc4;

                            bc4.NeighborhoodAB = bc3;
                            bc4.NeighborhoodBC = cv2;
                            bc4.NeighborhoodCA = bc2;
                            bcn.Deleted = true;
                            _triangles.Remove(bcn);
                        }
                        t.NeighborhoodAB?.ReplaceNeighborhood(t, bc1);
                        t.NeighborhoodCA?.ReplaceNeighborhood(t, bc2);
                        bc1.NeighborhoodAB = t.NeighborhoodAB;
                        bc1.NeighborhoodBC = bc3;
                        bc1.NeighborhoodCA = bc2;

                        bc2.NeighborhoodAB = bc4;
                        bc2.NeighborhoodBC = t.NeighborhoodCA;
                        bc2.NeighborhoodCA = bc1;

                        t.Deleted = true;
                        _triangles.Remove(t);
                        break;
                            
                    case IntersectionResult.SideCA:
                        
                        var ca1 = new Triangle(t.A, t.B, _vertices[i]);
                        if (ca1.GetSquare() > 1e-05) newTrianglesToCheck.Add(ca1);
                        var ca2 = new Triangle(_vertices[i], t.B, t.C);
                        if (ca2.GetSquare() > 1e-05) newTrianglesToCheck.Add(ca2);

                        Triangle ca3 = null, ca4 = null;
                        var can = t.NeighborhoodCA;

                        if (can != null)
                        {
                            var vn3 = t.NeighborhoodCA?.VerticeExcept(t);

                            ca3 = new Triangle(_vertices[i], t.C, vn3);
                            if (ca3.GetSquare() > 1e-05) newTrianglesToCheck.Add(ca3);
                            ca4 = new Triangle(_vertices[i], vn3, t.A);
                            if (ca4.GetSquare() > 1e-05) newTrianglesToCheck.Add(ca4);
                                
                            var av1 = t.NeighborhoodCA?.GetNeighborhoodByVertices(vn3, t.C);
                            var av2 = t.NeighborhoodCA?.GetNeighborhoodByVertices(vn3, t.A);
                            av1?.ReplaceNeighborhood(can, ca3);
                            av2?.ReplaceNeighborhood(can, ca4);

                            ca3.NeighborhoodAB = ca2;
                            ca3.NeighborhoodBC = av1;
                            ca3.NeighborhoodCA = ca4;

                            ca4.NeighborhoodAB = ca3;
                            ca4.NeighborhoodBC = av2;
                            ca4.NeighborhoodCA = ca1;

                            can.Deleted = true;
                            _triangles.Remove(can);
                        }
                        t.NeighborhoodAB?.ReplaceNeighborhood(t, ca1);
                        t.NeighborhoodBC?.ReplaceNeighborhood(t, ca2);
                        ca1.NeighborhoodAB = t.NeighborhoodAB;
                        ca1.NeighborhoodBC = ca2;
                        ca1.NeighborhoodCA = ca4;

                        ca2.NeighborhoodAB = ca1;
                        ca2.NeighborhoodBC = t.NeighborhoodBC;
                        ca2.NeighborhoodCA = ca3;

                        t.Deleted = true;
                        _triangles.Remove(t);
                        break;
  
                        default:
                            break;
                    }
                    _triangles.AddRange(newTrianglesToCheck);

                    foreach (var tr in newTrianglesToCheck)
                    {
                        if (!tr.Deleted)
                        {
                            DoCheck(tr, true);
                        }
                    }
                }
            }

            public void DoCheck(Triangle t, bool resetCounter)
            {
                if (resetCounter) _counter = 0;
                else
                {
                    _counter++;
                    if (_maxcounter < _counter) _maxcounter = _counter;
                    if (_counter > 20) return;
                }
                
                var vn1 = t.NeighborhoodAB?.VerticeExcept(t);
                var d1 = IsDelaunay(vn1, t.A, t.C, t.B);
                if (!d1 && vn1!=null)
                {
                    var t1 = new Triangle(t.C, t.A, vn1);
                    var t2 = new Triangle(vn1, t.B, t.C);
                    _triangles.Remove(t);
                    t.Deleted = true;
                    _triangles.Remove(t.NeighborhoodAB);
                    t.NeighborhoodAB.Deleted = true;
                    _triangles.Add(t1);
                    _triangles.Add(t2);

                    var an1 = t.NeighborhoodAB.GetNeighborhoodByVertices(t.A, vn1);
                    var bn1 = t.NeighborhoodAB.GetNeighborhoodByVertices(t.B, vn1);

                    t.NeighborhoodCA?.ReplaceNeighborhood(t, t1);
                    t.NeighborhoodBC?.ReplaceNeighborhood(t, t2);
                    
                    t1.NeighborhoodAB = t.NeighborhoodCA;
                    t1.NeighborhoodBC = an1;
                    t1.NeighborhoodCA = t2;
                    t2.NeighborhoodAB = bn1;
                    t2.NeighborhoodBC = t.NeighborhoodBC;
                    t2.NeighborhoodCA = t1;

                    an1?.ReplaceNeighborhood(t.NeighborhoodAB, t1);
                    bn1?.ReplaceNeighborhood(t.NeighborhoodAB, t2);

                    if (!t1.Deleted) DoCheck(t1, false);
                    if (!t2.Deleted) DoCheck(t2, false);
                    return;
                }
                
                var vn2 = t.NeighborhoodBC?.VerticeExcept(t);
                var d2 = IsDelaunay(vn2, t.B, t.A, t.C);
                if (!d2 && vn2 != null)
                {
                    var t1 = new Triangle(t.A, t.B, vn2);
                    var t2 = new Triangle(vn2, t.C, t.A);
                    _triangles.Remove(t);
                    t.Deleted = true;
                    _triangles.Remove(t.NeighborhoodBC);
                    t.NeighborhoodBC.Deleted = true;
                    _triangles.Add(t1);
                    _triangles.Add(t2);

                    var bn1 = t.NeighborhoodBC.GetNeighborhoodByVertices(t.B, vn2);
                    var cn1 = t.NeighborhoodBC.GetNeighborhoodByVertices(t.C, vn2);

                    t.NeighborhoodAB?.ReplaceNeighborhood(t, t1);
                    t.NeighborhoodCA?.ReplaceNeighborhood(t, t2);

                    t1.NeighborhoodAB = t.NeighborhoodAB;
                    t1.NeighborhoodBC = bn1;
                    t1.NeighborhoodCA = t2;
                    t2.NeighborhoodAB = cn1;
                    t2.NeighborhoodBC = t.NeighborhoodCA;
                    t2.NeighborhoodCA = t1;

                    bn1?.ReplaceNeighborhood(t.NeighborhoodBC, t1);
                    cn1?.ReplaceNeighborhood(t.NeighborhoodBC, t2);

                    if (!t1.Deleted) DoCheck(t1, false);
                    if (!t2.Deleted) DoCheck(t2, false);
                    return;
                }

                var vn3 = t.NeighborhoodCA?.VerticeExcept(t);
                var d3 = IsDelaunay(vn3, t.C, t.B, t.A);
                if (!d3 && vn3 != null)
                {
                    var t1 = new Triangle(vn3, t.A, t.B);
                    var t2 = new Triangle(t.B, t.C, vn3);
                    _triangles.Remove(t);
                    t.Deleted = true;
                    _triangles.Remove(t.NeighborhoodCA);
                    t.NeighborhoodCA.Deleted = true;

                    _triangles.Add(t1);
                    _triangles.Add(t2);
                    var cn1 = t.NeighborhoodCA.GetNeighborhoodByVertices(t.C, vn3);
                    var an1 = t.NeighborhoodCA.GetNeighborhoodByVertices(t.A, vn3);

                    t.NeighborhoodBC?.ReplaceNeighborhood(t, t2);
                    t.NeighborhoodAB?.ReplaceNeighborhood(t, t1);

                    t1.NeighborhoodAB = an1;
                    t1.NeighborhoodBC = t.NeighborhoodAB;
                    t1.NeighborhoodCA = t2;
                    t2.NeighborhoodAB = t.NeighborhoodBC;
                    t2.NeighborhoodBC = cn1;
                    t2.NeighborhoodCA = t1;

                    an1?.ReplaceNeighborhood(t.NeighborhoodCA, t1);
                    cn1?.ReplaceNeighborhood(t.NeighborhoodCA, t2);

                    if (!t1.Deleted) DoCheck(t1, false);
                    if (!t2.Deleted) DoCheck(t2, false);
                }
            }
           
            public bool IsDelaunay(Vertice v0, Vertice v1, Vertice v2, Vertice v3)
            {
                if (v0 == null || v1 == null || v2 == null || v3 == null) return true;
                return ((v0.X - v1.X) * (v0.Y - v3.Y) - (v0.X - v3.X) * (v0.Y - v1.Y)) *
                       ((v2.X - v3.X) * (v2.X - v1.X) + (v2.Y - v3.Y) * (v2.Y - v1.Y)) +
                       ((v0.X - v1.X) * (v0.X - v3.X) + (v0.Y - v1.Y) * (v0.Y - v3.Y)) *
                       ((v2.X - v3.X) * (v2.Y - v1.Y) - (v2.X - v1.X) * (v2.Y - v3.Y)) > 0;
            }

            public List<Triangle> GetTriangles(bool limited)
            {
                if (limited)
                {
                    var perimeter = AQMath.AggregateMedian(_triangles.Select(a => (object)a.GetPerimeter()).ToList());
                    return _triangles.Where(a => !a.SuperStructure && !a.Deleted && a.GetPerimeter() / perimeter < 2.66).ToList();
                }
                else return _triangles.Where(a => !a.SuperStructure && !a.Deleted).ToList();
            }
        }

        private class IntersectionInfo
        {
            public Triangle Triangle;
            public IntersectionResult IntersectionResult;
        }

        private class Vertice : IEqualityComparer<Vertice>
        {
            public double X;
            public double Y;
            public Color Color;
            public bool SuperStructure;
            public Triangle LastTriangle;

            public bool Equals(Vertice x, Vertice y)
            {
                return Math.Abs(x.X - y.X) < 1e-1 && Math.Abs(x.Y - y.Y) < 1e-5;
            }

            public int GetHashCode(Vertice obj)
            {
                return GetHashCode();
            }

            public List<Point> GetVoronoy()
            {
                var usedTriangles = new List<Triangle> {LastTriangle};
                var r = LastTriangle;
                Triangle t = null;
                while (t!=LastTriangle)
                {
                    t = r?.GetNextNeighborhood(this);
                    r = t;
                    if (t == null) break;
                    usedTriangles.Add(t);
                }
                return usedTriangles.Where(a=>a!=null).Select(a => a.GetOutCircleCenter()).ToList();
            }
        }

        private class Triangle : IEqualityComparer<Triangle>
        {
            public Vertice A;
            public Vertice B;
            public Vertice C;
            public Triangle NeighborhoodAB;
            public Triangle NeighborhoodBC;
            public Triangle NeighborhoodCA;
            public bool Deleted;

            public bool SuperStructure { get { return A.SuperStructure || B.SuperStructure || C.SuperStructure; }}
            public double xMin, yMin, xMax, yMax;

            public Triangle(Vertice v1, Vertice v2, Vertice v3)
            {
                A = v1; B = v2; C = v3;
                xMin = Math.Min(v1.X, v2.X);
                xMin = Math.Min(xMin, v3.X);
                xMax = Math.Max(v1.X, v2.X);
                xMax = Math.Max(xMax, v3.X);
                yMin = Math.Min(v1.Y, v2.Y);
                yMin = Math.Min(yMin, v3.Y);
                yMax = Math.Max(v1.Y, v2.Y);
                yMax = Math.Max(yMax, v3.Y);
                A.LastTriangle = this;
                B.LastTriangle = this;
                C.LastTriangle = this;
            }
            
            double Side(Vertice p1, Vertice p2, Vertice p3)
            {
                return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y);
            }

            public Vertice VerticeExcept(Triangle t)
            {
                if (t.A != A && t.B != A && t.C != A) return A;
                if (t.A != B && t.B != B && t.C != B) return B;
                if (t.A != C && t.B != C && t.C != C) return C;
                return null;
            }

            public IntersectionResult CheckIntersection(Vertice pt)
            {
                if (pt.X < xMin || pt.X > xMax || pt.Y < yMin || pt.Y > yMax) return IntersectionResult.NoIntersection;

                if (Math.Abs(pt.X - A.X) < 1e-5 && Math.Abs(pt.Y - A.Y) < 1e-5) return IntersectionResult.PointA;
                if (Math.Abs(pt.X - B.X) < 1e-5 && Math.Abs(pt.Y - B.Y) < 1e-5) return IntersectionResult.PointB;
                if (Math.Abs(pt.X - C.X) < 1e-5 && Math.Abs(pt.Y - C.Y) < 1e-5) return IntersectionResult.PointC;
                
                var b1 = Side(pt, A, B);
                if (Math.Abs(b1) < 1e-5)
                {
                    var a = Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
                    var x  = Math.Sqrt(Math.Pow(A.X - pt.X, 2) + Math.Pow(A.Y - pt.Y, 2));
                    var x2 = Math.Sqrt(Math.Pow(B.X - pt.X, 2) + Math.Pow(B.Y - pt.Y, 2));
                    if (Math.Abs(a - x - x2) <1e-1) return IntersectionResult.SideAB;
                }
                var b2 = Side(pt, B, C);
                if (Math.Abs(b2) < 1e-5)
                {
                    var b = Math.Sqrt(Math.Pow(B.X - C.X, 2) + Math.Pow(B.Y - C.Y, 2));
                    var x  = Math.Sqrt(Math.Pow(B.X - pt.X, 2) + Math.Pow(B.Y - pt.Y, 2));
                    var x2 = Math.Sqrt(Math.Pow(C.X - pt.X, 2) + Math.Pow(C.Y - pt.Y, 2));
                    if (Math.Abs(b - x - x2) < 1e-1) return IntersectionResult.SideBC;
                }

                var b3 = Side(pt, C, A);
                if (Math.Abs(b3) < 1e-5)
                {
                    var c = Math.Sqrt(Math.Pow(C.X - A.X, 2) + Math.Pow(C.Y - A.Y, 2));
                    var x  = Math.Sqrt(Math.Pow(C.X - pt.X, 2) + Math.Pow(C.Y - pt.Y, 2));
                    var x2 = Math.Sqrt(Math.Pow(A.X - pt.X, 2) + Math.Pow(A.Y - pt.Y, 2));
                    if (Math.Abs(c - x - x2)< 1e-1) return IntersectionResult.SideCA;
                }
                var a1 = b1 < 0;
                var a2 = b2 < 0;
                var a3 = b3 < 0;
                return ((a1 == a2) && (a2 == a3)) ? IntersectionResult.TriangleIntersection : IntersectionResult.NoIntersection;
            }

            public void ReplaceNeighborhood(Triangle from, Triangle to)
            {
                if (from == null ) return;
                if (NeighborhoodAB == from || NeighborhoodAB == to) NeighborhoodAB = to;
                else if (NeighborhoodBC == from || NeighborhoodBC == to) NeighborhoodBC = to;
                else if (NeighborhoodCA == from || NeighborhoodCA == to) NeighborhoodCA = to;
            }

            public Triangle GetNeighborhoodByVertices(Vertice v1, Vertice v2)
            {
                if ((A == v1 && B == v2) || (A == v2 && B == v1)) return NeighborhoodAB;
                if ((B == v1 && C == v2) || (B == v2 && C == v1)) return NeighborhoodBC;
                if ((C == v1 && A == v2) || (C == v2 && A == v1)) return NeighborhoodCA;
                return null;
            }

            public Triangle GetNextNeighborhood(Vertice v1)
            {
                if (A == v1) return NeighborhoodCA;
                if (B == v1) return NeighborhoodAB;
                if (C == v1) return NeighborhoodBC;
                return null;
            }


            public double GetSquare()
            {
                if (xMax - xMin < 1e-5 || yMax - yMin < 1e-5) return 0;

                var a = Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
                var b = Math.Sqrt(Math.Pow(B.X - C.X, 2) + Math.Pow(B.Y - C.Y, 2));
                var c = Math.Sqrt(Math.Pow(C.X - A.X, 2) + Math.Pow(C.Y - A.Y, 2));

                var p = (a + b + c)/2;

                return Math.Sqrt(p*(p - a)*(p - b)*(p - c));
            }

            public double GetPerimeter()
            {
                var a = Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
                var b = Math.Sqrt(Math.Pow(B.X - C.X, 2) + Math.Pow(B.Y - C.Y, 2));
                var c = Math.Sqrt(Math.Pow(C.X - A.X, 2) + Math.Pow(C.Y - A.Y, 2));
                return a+b+c;
            }

            public bool Equals(Triangle x, Triangle y)
            {
                var aExists = y.A == x.A || y.B == x.A || y.C == x.A;
                var bExists = y.A == x.B || y.B == x.B || y.C == x.B;
                var cExists = y.A == x.C || y.B == x.C || y.C == x.C;
                return aExists && bExists && cExists;
            }

            public Point GetOutCircleCenter()
            {
                var d = 2.0d * (A.X * (B.Y - C.Y) + B.X * (C.Y - A.Y) + C.X * (A.Y - B.Y));
                var x = ((A.X * A.X + A.Y * A.Y) * (B.Y - C.Y) + (B.X * B.X + B.Y * B.Y) * (C.Y - A.Y) + (C.X * C.X + C.Y * C.Y) * (A.Y - B.Y)) / d;
                var y = ((A.X * A.X + A.Y * A.Y) * (C.X - B.X) + (B.X * B.X + B.Y * B.Y) * (A.X - C.X) + (C.X * C.X + C.Y * C.Y) * (B.X - A.X)) / d;
                return new Point(x,y);
            }

            public int GetHashCode(Triangle obj)
            {
                return GetHashCode();
            }
        }

        public enum IntersectionResult
        {
            NoIntersection,
            TriangleIntersection,
            SideAB,
            SideBC,
            SideCA,
            PointA,
            PointB,
            PointC,
        }
    }
}
