﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public static class PseudoHTMLAxisTitleParser
    {
        public static void FillRichTextWithHTML(string html, RichTextBox rtb)
        {
            rtb.Blocks.Clear();
            if (string.IsNullOrEmpty(html)) return;
            var wp = new Paragraph();
            rtb.Blocks.Add(wp);
            html = html.Replace("<li>", "<br />● ");
            html = html.Replace("<=", "≤");
            html = html.Replace(">=", "≥");
            var isBold = false;
            var isItalic = false;
            var isRed = false;
            var m = Regex.Matches(html, @"(<[^<>]{1,6}>)");
            if (m.Count != 0)
            {
                var pos = 0;
                var pos2 = 0;

                foreach (Match match in m)
                {
                    foreach (Capture c in match.Captures)
                    {
                        pos2 = c.Index;
                        AddText(wp, html.Substring(pos, pos2 - pos), isBold, isItalic, isRed);
                        var token = c.Value.Replace(" ", "").ToLower();
                        switch (token)
                        {
                            case "<b>":
                                isBold = true;
                                break;
                            case @"</b>":
                                isBold = false;
                                break;
                            case "<red>":
                                isRed = true;
                                break;
                            case @"</red>":
                                isRed = false;
                                break;
                            case "<i>":
                                isItalic = true;
                                break;
                            case @"</i>":
                                isItalic = false;
                                break;
                        }
                        pos = pos2 + c.Length;
                        pos2 = pos;
                    }
                }
                if (pos2 < html.Length) AddText(wp, html.Substring(pos2, html.Length - pos2), false, false, false);
            }
            else AddText(wp, html, false, false, false);
        }

        private static void AddText(Paragraph wp, string text, bool isBold, bool isItalic, bool isRed)
        {
            var r = new Run
            {
                Text = text,
                FontWeight = (isBold) ? FontWeights.Bold : FontWeights.Light,
                FontStyle = (isItalic) ? FontStyles.Italic : FontStyles.Normal
            };
            if (isRed) r.Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xdf, 0x40, 0x00));
            wp.Inlines.Add(r);
        }
    }
}
