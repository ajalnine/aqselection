﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using static System.String;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawTable()
        {
            SubChartTableData.Children.Clear();
            SubChartTableTitles.Children.Clear();

            var seriesToFill = BottomAxisRange.BoundSeries;
            if (seriesToFill == null || seriesToFill.Count == 0) return;
            var columns = seriesToFill.First().SeriesData.Count;
            var rows = seriesToFill.Count;
            SubChartTableData.Columns = columns;
            SubChartTableData.Rows = rows;
            SubChartTableTitles.Columns = 1;
            SubChartTableTitles.Rows = rows;

            foreach (SeriesRangeInfo s in seriesToFill)
            {
                Panel sp = GetGenericLegendRow(s.SourceSeries);
                sp.Margin = new Thickness(5, 3, 3, 5);
                var b = new Border
                {
                    BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                    BorderBrush = new SolidColorBrush {Color = Color.FromArgb(255, 150, 192, 192)},
                    Child = sp,
                };
                SubChartTableTitles.Children.Add(b);
                var ysri = (from y in RightAxisRange.BoundSeries.Union(LeftAxisRange.BoundSeries)
                                        where y.SourceSeries.SeriesTitle == s.SourceSeries.SeriesTitle
                                        select y).SingleOrDefault();
                if (ysri == null) return;
                var isEditable = ysri.SourceSeries.SeriesType == ChartSeriesType.EditableTableOnly;

                foreach (var v in ysri.SeriesData)
                {
                    var b2 = new Border
                    {
                        BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                        BorderBrush = new SolidColorBrush {Color = Color.FromArgb(255, 150, 192, 192)},
                    };
                    if (!isEditable)
                    {
                        var tb2 = new TextBlock
                        {
                            Text = (v != null ? v.ToString() : Empty),
                            HorizontalAlignment = HorizontalAlignment.Left,
                            Margin = new Thickness(5, 3, 3, 5)
                        };
                        b2.Child = tb2;
                    }
                    else
                    {
                        if (v != null)
                        {
                            var tb2 = new TextBox
                            {
                                BorderThickness = new Thickness(0),
                                HorizontalAlignment = HorizontalAlignment.Left,
                                Margin = new Thickness(0)
                            };
                            b2.Child = tb2;
                            var binding = new Binding(s.SourceSeries.Tag)
                            {
                                Source = v,
                                Mode = BindingMode.TwoWay,
                                NotifyOnValidationError = true,
                                ValidatesOnExceptions = true
                            };
                            tb2.SetBinding(TextBox.TextProperty, binding);
                        }
                    }
                    SubChartTableData.Children.Add(b2);
                }
            }
        }

        private void DrawCustomLegendTables(List<SLDataTable> customTables)
        {
            LegendChartTables.Children.Clear();
            if (customTables == null) return;
            foreach (var customTable in customTables)
            {
                if (customTable == null) continue;
                if (customTables.Count > 1)
                {
                    LegendChartTables.Children.Add(new TextBlock
                    {
                        Foreground = defaultForegroundBrush, Text = customTable.TableName, VerticalAlignment = VerticalAlignment.Center, FontSize = 10 * GlobalFontCoefficient
                    });
                }
               
                var LegendChartTable = new Grid() { Margin = new Thickness(0, 0, 0, 5) };

                var columns = customTable.ColumnNames.Count(a => !a.StartsWith("#"));
                var rows = customTable.Table.Count;
                LegendChartTable.MaxWidth = 300;
                for (var x = 0; x < columns; x++) LegendChartTable.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                for (var y = 0; y < rows + 1; y++) LegendChartTable.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                int i = 0, j = 0;
                foreach (var c in customTable.ColumnNames.Where(a => !a.StartsWith("#")))
                {
                    var b2 = new Border
                    {
                        BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                        BorderBrush = new SolidColorBrush { Color = Color.FromArgb(255, 150, 192, 192) }
                    };
                    var tb2 = new TextBlock
                    {
                        Text = (c ?? Empty),
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        Margin = new Thickness(2, 1, 1, 2),
                        FontSize = 9 * GlobalFontCoefficient,
                        VerticalAlignment = VerticalAlignment.Center,
                        Foreground = Inverted ? new SolidColorBrush(Colors.White) : defaultForegroundBrush,
                        TextWrapping = TextWrapping.Wrap
                    };
                    b2.Child = tb2;
                    Grid.SetColumn(b2, i);
                    Grid.SetRow(b2, j);
                    LegendChartTable.Children.Add(b2);
                    i++;
                }
                j++;
                foreach (var row in customTable.Table)
                {
                    i = 0;
                    foreach (var c in customTable.ColumnNames.Where(a => !a.StartsWith("#")))
                    {
                        var x = customTable.ColumnNames.IndexOf(c);
                        var colorColumn = customTable.ColumnNames.IndexOf("#" + c + "_Цвет");
                        var color = (colorColumn >= 0)
                            ? new SolidColorBrush(ColorConvertor.ConvertStringToColor((row.Row[colorColumn]?.ToString() ?? "#ff000000")))
                            : Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                        var b2 = new Border
                        {
                            BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                            BorderBrush = new SolidColorBrush { Color = Color.FromArgb(255, 150, 192, 192) },
                        };
                        var tb2 = new TextBlock
                        {
                            Text = (row.Row[x]?.ToString() ?? Empty),
                            HorizontalAlignment = HorizontalAlignment.Left,
                            Margin = new Thickness(2, 1, 1, 2),
                            FontSize = 9 * GlobalFontCoefficient,
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = color,
                            TextWrapping = TextWrapping.Wrap
                        };
                        b2.Child = tb2;
                        LegendChartTable.Children.Add(b2);
                        Grid.SetColumn(b2, i);
                        Grid.SetRow(b2, j);
                        i++;
                    }
                    j++;
                }
                LegendChartTables.Children.Add(LegendChartTable);
            }
        }
    }
}