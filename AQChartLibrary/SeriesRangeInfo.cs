﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;

namespace AQChartLibrary
{
    public class SeriesRangeInfo
    {
        public string AxisTitle;
        public AxisRange BoundAxis;
        public string DataType;
        public bool IsNominal;
        public bool IsSecondary;
        public string SeriesName;
        public string SourceColumnName;
        public ChartSeriesDescription SourceSeries;
        public SLDataTable SourceTable;
        public string ColorColumnName;
        public string MarkerSizeColumnName;
        public string MarkerLabelColumnName;

        public double Range
        {
            get
            {
                if (SourceColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(SourceColumnName)) return Double.NaN;
                return SourceTable.GetApproximateRange(SourceColumnName);
            }
        }

        public double Min
        {
            get
            {
                if (SourceColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(SourceColumnName)) return Double.NaN;
                return SourceTable.GetApproximateMinimum(SourceColumnName);
            }
        }

        public double Max
        {
            get
            {
                if (SourceColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(SourceColumnName)) return Double.NaN;
                return SourceTable.GetApproximateMaximum(SourceColumnName);
            }
        }

        public List<object> SeriesData
        {
            get
            {
                if (SourceColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(SourceColumnName)) return null;
                return SourceTable.GetColumnData(SourceColumnName);
            }
        }

        public int Count
        {
            get
            {
                if (SourceColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(SourceColumnName))
                    return 0;
                return SourceTable.Table.Count;
            }
        }

        public List<object> SeriesMarkerColors
        {
            get
            {
                if (ColorColumnName==null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(ColorColumnName)) return null;
                return SourceTable.GetColumnData(ColorColumnName);
            }
        }

        public List<object> SeriesMarkerSizes
        {
            get
            {
                if (MarkerSizeColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(MarkerSizeColumnName)) return null;
                return SourceTable.GetColumnData(MarkerSizeColumnName);
            }
        }

        public List<object> SeriesMarkerLabels
        {
            get
            {
                if (MarkerLabelColumnName == null || SourceTable == null || SourceTable.ColumnNames.Count == 0 ||
                    !SourceTable.ColumnNames.Contains(MarkerLabelColumnName)) return null;
                return SourceTable.GetColumnData(MarkerLabelColumnName);
            }
        }
    }
}