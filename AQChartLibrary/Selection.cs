﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private readonly List<Selectable> _chartSelectables = new List<Selectable>();
        private List<Selectable> _selectedSelectables = new List<Selectable>();
        private double _offsetX, _offsetY;

        public delegate void SelectionChangedDelegate(object o, ChartSelectionEventArgs e);

        public event SelectionChangedDelegate SelectionChanged;
        private Color _selectionColor;

        public void AppendSelectable(Point center, Size range, Shape source, InteractivityIndex ii)
        {
            _chartSelectables.Add(new Selectable(center, range, source, ii));
        }

        #region Интерфейс выбора

        private bool _dragStarted, _clickInShape;
        private Point _dragStartPosition;
        private Point _dragEndPosition;
        private Rect _currentSelection;
        private Rectangle _currentMark;
        private TextBlock _currentTextMark;

        private void ChartLayout_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _selectionColor =  Colors.Purple;

            var gt = SeriesSurface.TransformToVisual(ChartLayout);
            var offset = gt.Transform(new Point(0, 0));
            _offsetX = offset.X;
            _offsetY = offset.Y;
            _dragStarted = true;
            _clickInShape = false;
            _dragStartPosition = e.GetPosition(this);
            _dragEndPosition = _dragStartPosition;
            if (!((Keyboard.Modifiers & ModifierKeys.Control) > 0) && !((Keyboard.Modifiers & ModifierKeys.Shift) > 0)) ResetSelection();
            _currentSelection = new Rect(
                Math.Min(_dragStartPosition.X, _dragEndPosition.X)
                , Math.Min(_dragStartPosition.Y, _dragEndPosition.Y)
                , Math.Abs(_dragStartPosition.X - _dragEndPosition.X)
                , Math.Abs(_dragStartPosition.Y - _dragEndPosition.Y));
            var r = new Rectangle { IsHitTestVisible = false, Fill = new SolidColorBrush(_selectionColor), Stroke = new SolidColorBrush(_selectionColor), Width = _currentSelection.Width, Height = _currentSelection.Height };
            r.Fill.Opacity = 0.1;
            Canvas.SetTop(r, _currentSelection.Y);
            Canvas.SetLeft(r, _currentSelection.X);
            SelectionView.Children.Add(r);
            _currentMark = r;
            var t = new TextBlock {Foreground = new SolidColorBrush(_selectionColor), FontSize = 10};
            _currentTextMark = t;
            SelectionView.Children.Add(t);
            UpdateSelectionTextmark();
            EnableAxisInteractivity(false);
        }

        private void UpdateSelectionTextmark()
        {
            Canvas.SetTop(_currentTextMark, _currentSelection.Y + _currentSelection.Height - _currentTextMark.ActualHeight);
            Canvas.SetLeft(_currentTextMark, _currentSelection.X + _currentSelection.Width);
            if ((Keyboard.Modifiers & ModifierKeys.Control) > 0) _currentTextMark.Text = "+";
            if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0) _currentTextMark.Text = "-";
        }

        private void ChartLayout_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            EnableAxisInteractivity(true);
            if (_currentSelection.Width*_currentSelection.Height < 2)
            {
                if (!_clickInShape) ResetSelection();
            }
            else
            {
                if (!((Keyboard.Modifiers & ModifierKeys.Shift) > 0))
                {
                    var selected = FindSelectedItems();
                    HighlightSelectedItems(selected);
                    _selectedSelectables.AddRange(selected);
                    SelectionView.Children.Clear();
                    _dragStarted = false;
                    InvokeSelectionChangedEvent();
                }
                else
                {
                    if (!_dragStarted) return;
                    UnHighlightSelectedItems();
                    var toUnselect = FindSelectedItems();
                    foreach (var t in toUnselect)
                    {
                        _selectedSelectables.Remove(t);
                    }
                    HighlightSelectedItems(_selectedSelectables);
                    SelectionView.Children.Clear();
                    _dragStarted = false;
                    InvokeSelectionChangedEvent();
                }
            }
        }

        private void InvokeSelectionChangedEvent()
        {
            var scea = new ChartSelectionEventArgs
            {
                LastMousePosition = _dragEndPosition,
                InteractivityIndexes = GetUniqueInteractiveIndexes(_selectedSelectables),
                Table = GetInteractiveTable(_selectedSelectables.Select(a=>a.Info).FirstOrDefault())
            };
            SelectionChanged?.Invoke(this, scea);
        }

        private void EnableAxisInteractivity(bool enable)
        {
            LeftAxis.EnableClick(enable);
            RightAxis.EnableClick(enable);
            TopAxis.EnableClick(enable);
            BottomAxis.EnableClick(enable);
        }

        public static List<InteractivityIndex> GetUniqueInteractiveIndexes(List<Selectable> selectables)
        {
            var result = new List<InteractivityIndex>();

            foreach (var s in selectables)
            {
                if (!result.Any(a=>a.Index == s.Info.Index && a.Layer == s.Info.Layer && a.LayerName == s.Info.LayerName && a.IndexInGroup == s.Info.IndexInGroup && a.SeriesName == s.Info.SeriesName))result.Add(s.Info);
            }
            return result;
        }

        private List<Selectable> FindSelectedItems()
        {
            var result = new List<Selectable>();
            {
                foreach (var s in _chartSelectables)
                {
                    if (s.Center.X - s.Range.Width / 2 < _currentSelection.X - _offsetX) continue;
                    if (s.Center.Y - s.Range.Height / 2 < _currentSelection.Y - _offsetY) continue;
                    if (s.Center.X + s.Range.Width / 2 > _currentSelection.X + _currentSelection.Width - _offsetX) continue;
                    if (s.Center.Y + s.Range.Height / 2 > _currentSelection.Y + _currentSelection.Height - _offsetY) continue;
                    result.Add(s);
                }
            }
            return result;
        }

        private void HighlightSelectedItems(List<Selectable> selected)
        {
            foreach (var s in selected)
            {
                s.Select(_selectionColor);
            }
        }

        private void UnHighlightSelectedItems()
        {
            foreach (var s in _selectedSelectables)
            {
                s.UnSelect();
            }
        }

        private void ChartLayout_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!_dragStarted) return;
            _dragEndPosition = e.GetPosition(this);
            _currentSelection.X = Math.Min(_dragStartPosition.X, _dragEndPosition.X);
            _currentSelection.Y = Math.Min(_dragStartPosition.Y, _dragEndPosition.Y);
            _currentSelection.Width = Math.Abs(_dragStartPosition.X - _dragEndPosition.X);
            _currentSelection.Height = Math.Abs(_dragStartPosition.Y - _dragEndPosition.Y);

            SelectionView.Dispatcher.BeginInvoke(() => 
            {
                _currentMark.Width = _currentSelection.Width;
                _currentMark.Height = _currentSelection.Height;
                Canvas.SetTop(_currentMark, _currentSelection.Y);
                Canvas.SetLeft(_currentMark, _currentSelection.X);
                UpdateSelectionTextmark();
            });
        }

        private void ResetSelection()
        {
            SelectionView.Children.Clear();
            UnHighlightSelectedItems();
            _selectedSelectables = new List<Selectable>();
            InvokeSelectionChangedEvent();
        }

        private void HandleSingleClickSelection(MouseButtonEventArgs e, Shape el, InteractivityIndex info)
        {
            _selectionColor = Colors.Purple;
            e.Handled = true;
            _dragStarted = false;
            _clickInShape = true;
            _currentSelection.Width = 0;
            _currentSelection.Height = 0;
            GetInteractiveTable(info);
            if (!((Keyboard.Modifiers & ModifierKeys.Control) > 0) && !((Keyboard.Modifiers & ModifierKeys.Shift) > 0))ResetSelection();
            var s = _chartSelectables.Where(a => a.Info == info).ToList() ;
            if (!((Keyboard.Modifiers & ModifierKeys.Shift) > 0))
            {
                if (s.Any())
                {
                    _selectedSelectables.AddRange(s);
                    HighlightSelectedItems(s);
                }
            }
            else
            {
                foreach (var sel in s)
                {
                    sel.UnSelect();
                    _selectedSelectables.Remove(sel);
                }
            }
            SelectionChanged?.Invoke(el,
                new ChartSelectionEventArgs
                {
                    InteractivityIndexes = GetUniqueInteractiveIndexes(_selectedSelectables),
                    LastMousePosition = e.GetPosition(this), Table = GetInteractiveTable(info)
        });
        }
        #endregion
    }

    public class Selectable
    {
        public Point Center;
        public Size Range;
        public Shape Source;
        public InteractivityIndex Info;
        private readonly Brush _originalStroke;
        private readonly Brush _originalFill;

        public Selectable(Point center, Size range, Shape source, InteractivityIndex ii)
        {
            Source = source;
            Range = range;
            Center = center;
            Info = ii;
            _originalStroke = source?.Stroke;
            _originalFill = source?.Fill;
        }

        public void Select(Color c )
        {
            if (Source == null) return;
            var selectionStrokeColor = c;
            Source.Stroke = new SolidColorBrush(selectionStrokeColor) {Opacity = _originalStroke?.Opacity ?? 1};
            if (_originalFill == null) return;
            var selectionFillColor = c;
            Source.Fill = new SolidColorBrush(selectionFillColor) {Opacity = _originalFill.Opacity};
        }

        public void UnSelect()
        {
            if (Source == null) return;
            Source.Stroke = _originalStroke;
            Source.Fill = _originalFill;
        }
    }

    public class ChartSelectionEventArgs
    {
        public List<InteractivityIndex> InteractivityIndexes;
        public Point LastMousePosition;
        public SLDataTable Table;
    }

    public enum SelectionType
    {
        Multiple,
        None
    }
}
