﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;

namespace AQChartLibrary
{
    #region Классы графиков

    public class ChartDescription
    {
        public List<ChartSeriesDescription> ChartSeries;
        public string ChartTitle;
        public string ChartSubtitle;
        public string ChartCathegoryTitle;
        public ChartLegend ChartLegendType;

        public ChartDescription()
        {
            ChartSeries = new List<ChartSeriesDescription>();
        }
    }

    public class GroupDescription
    {
        public LabelsType ShowText;
        public GroupPointMode PointMode;
        public double? TotalLayersCount;
        public double? TotalGroupsCount;
        public double? LayerIndex;
        public bool UseLayerOffset;
        public double? LabelSize;
        public double? Center;
        public double? StDev;
        public double? Lower;
        public double? Higher;
        public double? LowerPercent;
        public double? HigherPercent;
        public double? LowLimit;
        public double? HighLimit;
        public double? IQR;
        public double? BoxMin;
        public double? BoxMax;
        public double? Min;
        public double? Max;
        public double? WhiskerMin;
        public double? WhiskerMax;
        public int? N;
        public string CustomComments;
        public object Tag;
        public object Layer;
        public List<PieDescription> PieData;
        public List<PointDescription> Outliers;
        public List<PointDescription> Extremes;
        public List<PointDescription> IndexedData;

        public GroupDescription()
        {
            Outliers = new List<PointDescription>();
            Extremes = new List<PointDescription>();
            IndexedData = new List<PointDescription>();
        }
    }

    public enum GroupPointMode
    {
        PointsInLine,
        StripChart,
        BeeSwarm,
        Hexagon,
    }

    public class PointDescription
    {
        public double Value;
        public object Tag;
        public object Layer;
        public int OriginalIndex;
        public double RenderX;
        public double RenderY;
    }

    [Flags]
    public enum LabelsType
    {
        None = 0,
        Centers = 1,
        Outage = 2,
        Whiskers = 4,
        Pie = 8,
        Scaled = 16,
        LayerName = 32
    }

    public class PieDescription
    {
        public double Part;
        public object Z;
        public object Group;
        public string Text;
        public double? LabelSize;
    }

    public class ChartSeriesDescription
    {
        public string XColumnName;
        public string YColumnName;
        public string ZColumnName;
        public string WColumnName;
        public string VColumnName;
        public string XAxisTitle;
        public string YAxisTitle;
        public string ZAxisTitle;
        public string WAxisTitle;
        public string VAxisTitle;
        public string ColorColumnName;
        public string MarkerSizeColumnName;
        public string LabelColumnName;
        public string OptionColumnName;
        public string SeriesTitle;
        public string SourceTableName;
        public bool XIsNominal;
        public bool YIsNominal;
        public bool YIsSecondary;
        public ChartSeriesType SeriesType;
        public ColorScales ColorScale;
        public ColorScaleMode ColorScaleMode;
        public MarkerSizeModes MarkerSizeMode;
        public MarkerShapeModes MarkerShapeMode;
        public MarkerModes MarkerMode;
        public ArrangementMode ArrangementMode;
        public string LineColor;
        public string FillColor;
        public string MarkerColor;
        public double MarkerSize;
        public double LineSize;
        public bool IsSmoothed;
        public string Tag;
        public double CustomParameter;
        public string Options;
        public object ZValueForColor { get; set; }
        public string ZValueSeriesTitle;
        public object WValueForShape { get; set; }
        public string WValueSeriesTitle;
    }

    public enum ChartSeriesType
    {
        LineXY,
        Column,
        ColumnStacked,
        ConstantX,
        ConstantY,
        RectangleX,
        RectangleY,
        StairsOnX,
        StairsOnY,
        Pie,
        CircularGraph,
        Circles,
        Radar,
        Segments,
        Sun,
        EditableTableOnly,
        BoxWhiskers,
        Dummy,
        BeeSwarmOnFloor,
        RugOnX,
        RugOnY,
        KDEOnX,
        KDEOnY,
        RangeOnX,
        RangeOnY,
        Surface,
        Voronoy,
        Bag,
        HexBin,
    }

    public enum ChartLegend
    {
        Right = 0,
        Top = 1,
        Bottom = 2,
        None = 10
    }

    public enum MarkerSizeModes
    {
        Medium,
        Small,
        SmallMedium,
        Large,
        MediumLarge,
        ExtraLarge
    }
    
    public enum MarkerShapeModes
    {
        Bubble,
        Range,
        Clock,
        C,
        Multishape,
    }

    public enum MarkerModes
    {
        Solid,
        SolidBordered,
        Ring,
        SemiTransparent,
        Transparent,
        Invisible
    }

    #endregion
}
