﻿using AQBasicControlsLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class AxisRange
    {
        public AxisType Axis;
        public List<SeriesRangeInfo> BoundSeries = new List<SeriesRangeInfo>();
        private delegate double? TransformDelegate(object value, SeriesRangeInfo sri);

        private string _dataType;
        public string DataType
        {
            get { return _dataType; }
            set { _dataType = value == null ? null : value.StartsWith("System.") ? value.Substring("System.".Length) : value; }
        }
        public object DisplayMaximum;
        public object DisplayMinimum;
        public double DisplayStep;
        public double PartStep;
        public bool IsNominal;
        public bool IsValid;
        public bool IsSecondary = false;    
        public double RealSteps;
        public bool HideNegative;

        public bool IsSymmetrical;
        public bool IsDiscrete; 
        public object SourceMaximum;
        public object SourceMinimum;
        public FixedAxis FixedAxisDescription;

        private List<string> _displayedLabels = new List<string>();
        private List<string> _orderedAxisLabels = new List<string>();
        private List<string> _axisLabels = new List<string>();
        private readonly Dictionary<string, int> _index = new  Dictionary<string, int>();

        private Dictionary<string, TransformDelegate> _transformValueDictionary, _transformMinMaxDictionary, _getIndexDictionary, _transformDiscreteDictionary;

        public AxisRange(bool isSymmetrical)
        {
            IsValid = false;
            IsSymmetrical = isSymmetrical;
            CreateTransformDictionaries();
        }

        private void CreateTransformDictionaries()
        {
            _transformValueDictionary = new Dictionary<string, TransformDelegate>
            {
                {
                    "Int32", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return ((double) value - (double) DisplayMinimum) / ((double) DisplayMaximum - (double) DisplayMinimum);
                    }
                },
                {
                    "Double", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return ((double) value - (double) DisplayMinimum)/((double) DisplayMaximum - (double) DisplayMinimum); 
                    }
                },
                {
                    "GroupDescription", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null || string.IsNullOrEmpty(value.ToString())) return null;
                        if (value is double) return ((double)value - (double)DisplayMinimum) /
                                ((double)DisplayMaximum - (double)DisplayMinimum);
                        var chartGroupDescription = value as GroupDescription;
                        return (chartGroupDescription?.Center - (double) DisplayMinimum) /
                               ((double) DisplayMaximum - (double) DisplayMinimum);
                    }
                },
                {
                    "DateTime", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((DateTime) value).Ticks - ((DateTime) DisplayMinimum).Ticks) / (double) (((DateTime) DisplayMaximum).Ticks - ((DateTime) DisplayMinimum).Ticks);
                    }
                },
                {
                    "TimeSpan", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((TimeSpan) value).Ticks - ((TimeSpan) DisplayMinimum).Ticks) / (double) (((TimeSpan) DisplayMaximum).Ticks - ((TimeSpan) DisplayMinimum).Ticks);
                    }
                },
                {
                    "String", (value, sri) =>
                    {
                        var v = sri.BoundAxis._axisLabels;
                        var i = v.IndexOf(value.ToString());
                        return ((i >= 0) ? i/(double?) v.Count + 0.5/v.Count : null);
                    }
                }
            };

            _transformMinMaxDictionary = new Dictionary<string, TransformDelegate>
            {
                {
                    "Int32", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return ((double) value - (double) SourceMinimum)/
                               ((double) SourceMaximum - (double) SourceMinimum);
                    }
                },
                {
                   "Double", (value, sri) =>
                   {
                       if (DisplayMaximum == null || DisplayMinimum == null) return null;
                       return ((double) value - (double) SourceMinimum)/
                              ((double) SourceMaximum - (double) SourceMinimum);
                   }
                },
                {
                    "GroupDescription", (value, sri) =>
                    {
                        if (SourceMaximum == null || SourceMinimum == null || string.IsNullOrEmpty(value.ToString())) return null;
                        if (value is double)
                            return ((double)value - (double)SourceMinimum) / ((double)SourceMaximum - (double)SourceMinimum);
                        var chartGroupDescription = value as GroupDescription;
                        return (chartGroupDescription?.Center - (double) SourceMinimum) / ((double) SourceMaximum - (double) SourceMinimum);
                    }
                },
                {
                    "DateTime", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((DateTime) value).Ticks - ((DateTime) SourceMinimum).Ticks) / (double) (((DateTime) SourceMaximum).Ticks - ((DateTime) SourceMinimum).Ticks);
                    }
                },
                {
                    "TimeSpan", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((TimeSpan) value).Ticks - ((TimeSpan) SourceMinimum).Ticks) / (double) (((TimeSpan) SourceMaximum).Ticks - ((TimeSpan) SourceMinimum).Ticks);
                    }
                },
                {
                    "String", (value, sri) =>
                    {
                        var v = sri.BoundAxis._orderedAxisLabels;
                        var i = v.IndexOf(value.ToString());
                        return ((i >= 0) ? i/(double?) v.Count + 0.5/v.Count : null);
                    }
                }
            };

            _transformDiscreteDictionary = new Dictionary<string, TransformDelegate>
            {
                {
                    "Int32", (value, sri) =>
                    {
                       if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        var part = ((double) value - (double) DisplayMinimum)/
                                   ((double) DisplayMaximum - (double) DisplayMinimum);
                        return Math.Floor(part / PartStep) * PartStep;
                    }
                },
                {
                    "Double", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        var part = ((double) value - (double) DisplayMinimum) / ((double) DisplayMaximum - (double) DisplayMinimum);
                        return Math.Floor(part / PartStep) * PartStep;
                    }
                },
                {
                    "GroupDescription", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null || string.IsNullOrEmpty(value.ToString())) return null;
                        if (value is double) return ((double)value - (double)DisplayMinimum) / ((double)DisplayMaximum - (double)DisplayMinimum);
                        var chartGroupDescription = value as GroupDescription;
                        var part = (chartGroupDescription?.Center - (double) DisplayMinimum) / ((double) DisplayMaximum - (double) DisplayMinimum);
                        if (!part.HasValue) return null;
                        return Math.Floor(part.Value / PartStep) * PartStep;
                    }
                },
                {
                    "DateTime", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        var part = (((DateTime) value).Ticks - ((DateTime) DisplayMinimum).Ticks) / (double) (((DateTime) DisplayMaximum).Ticks - ((DateTime) DisplayMinimum).Ticks);
                        return Math.Floor(part / PartStep) * PartStep;                    }
                    },
                {
                    "TimeSpan", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        var part = (((TimeSpan) value).Ticks - ((TimeSpan) DisplayMinimum).Ticks) / (double) (((TimeSpan) DisplayMaximum).Ticks - ((TimeSpan) DisplayMinimum).Ticks);
                        return Math.Floor(part / PartStep) * PartStep;
                    }
                },
                {
                    "String", (value, sri) =>
                    {
                        var v = sri.BoundAxis._orderedAxisLabels;
                        var i = v.IndexOf(value.ToString());
                        return ((i >= 0) ? i/(double?) v.Count + 0.5/v.Count : null);
                    }
                }
            };

            _getIndexDictionary = new Dictionary<string, TransformDelegate>
            {
                {
                     "Int32", (value, sri) =>
                     {
                         if (DisplayMaximum == null || DisplayMinimum == null) return null;
                         return ((double) value - (double) SourceMinimum)/((double) SourceMaximum - (double) SourceMinimum);
                     }
                },
                {
                    "Double", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return ((double) value - (double) SourceMinimum)/
                               ((double) SourceMaximum - (double) SourceMinimum);
                    }
                },
                {
                    "GroupDescription", (value, sri) =>
                    {
                        if (SourceMaximum == null || SourceMinimum == null || string.IsNullOrEmpty(value.ToString())) return null;
                        if (value is double) return ((double)value - (double)SourceMinimum) / ((double)SourceMaximum - (double)SourceMinimum);
                        var chartGroupDescription = value as GroupDescription;
                        return (chartGroupDescription?.Center - (double) SourceMinimum) / ((double) SourceMaximum - (double) SourceMinimum);
                    }
                },
                {
                    "DateTime", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((DateTime) value).Ticks - ((DateTime) SourceMinimum).Ticks)/
                               (double) (((DateTime) SourceMaximum).Ticks - ((DateTime) SourceMinimum).Ticks);
                    }
                },
                {
                    "TimeSpan", (value, sri) =>
                    {
                        if (DisplayMaximum == null || DisplayMinimum == null) return null;
                        return (((TimeSpan) value).Ticks - ((TimeSpan) SourceMinimum).Ticks)/
                               (double) (((TimeSpan) SourceMaximum).Ticks - ((TimeSpan) SourceMinimum).Ticks);
                    }
                },
                {
                    "String", (value, sri) =>
                    {
                        var v = sri.BoundAxis._orderedAxisLabels;
                        var i = _index[value.ToString()];
                        return ((i >= 0) ? i / (double?)v.Count + 0.5 / v.Count : null);
                    }
                }
            };
        }

        public List<string> GetLabels()
        {
            return _displayedLabels;
        }

        public double? TransformValue(object value, SeriesRangeInfo sri)
        {
            if (value == null || value.ToString() == "#") return null;
            if (!sri.IsNominal) return _transformValueDictionary[DataType](value, sri);
            return (Convert.ToDouble(value) + 0.5d)/sri.Count;
        }

        public double? TransformValueZ(object value, SeriesRangeInfo sri, ColorScaleMode csm)
        {
            if (value == null || string.IsNullOrEmpty(value.ToString()) ||  value.ToString() == "#") return null;
            if (sri.IsNominal) return (Convert.ToDouble(value) + 0.5d)/sri.Count*ColorConvertor.ColorPart[csm] + ColorConvertor.ColorOffset[csm];
            if (!IsDiscrete) return _transformValueDictionary[DataType](value, sri) * ColorConvertor.ColorPart[csm] + ColorConvertor.ColorOffset[csm];
            return _transformDiscreteDictionary[DataType](value, sri) * ColorConvertor.ColorPart[csm] + ColorConvertor.ColorOffset[csm];
        }

        public double? TransformValueMinMax(object value, SeriesRangeInfo sri)
        {
            if (value == null || value.ToString() == "#") return null;
            if (!sri.IsNominal) return _transformMinMaxDictionary[DataType](value, sri);
            return (Convert.ToDouble(value) + 0.5d) / sri.Count;
        }

        public double? GetIndex(object value, SeriesRangeInfo sri)
        {
            if (value == null || value.ToString() == "#") return null;
            if (!sri.IsNominal) return _getIndexDictionary[DataType](value, sri);
            return (Convert.ToDouble(value) + 0.5d) / sri.Count;
        }

        public void Setup(double? maxDesriredSteps)
        {
            var maxDesiredStep = maxDesriredSteps ?? 14;
            if (BoundSeries == null || BoundSeries.Count == 0) return;
            DataType = BoundSeries.First().DataType;
            if (FixedAxisDescription != null)
            {
                IsDiscrete = FixedAxisDescription.AxisDiscrete;
            }

            if (BoundSeries.Any(a => a.IsNominal)) ProcessNominalAxis();
            else
            {
                switch (DataType)
                {
                    case "Int32":
                    case "Double":
                    case "GroupDescription":

                        var sourceMinimum1 = (from x in BoundSeries
                                              where
                                                  x.DataType != "InputParameter" &&
                                                  x.DataType != "GroupDescription" && x.DataType != null
                                              select x.SeriesData.Where(a => a is Double || a is Int32 || a is GroupDescription).Min()).Min();
                        var sourceMaximum1 = (from x in BoundSeries
                                              where
                                                  x.DataType != "InputParameter" &&
                                                  x.DataType != "GroupDescription" && x.DataType != null
                                              select x.SeriesData.Where(a => a is Double || a is Int32 || a is GroupDescription).Max()).Max();
                        SourceMinimum = sourceMinimum1;
                        SourceMaximum = sourceMaximum1;
                        var data =
                                (from x in BoundSeries where x.DataType == "GroupDescription" select x.SeriesData)
                                    .ToList();

                        var s = new List<object>();
                        foreach (var x in data)s.InsertRange(new List<object>().Count, x);
                        if (s.Count > 0)
                        {
                            object sourceMinimum2 =
                                s.Cast<GroupDescription>().Select(a => a.Min()).Min();
                            object sourceMaximum2 =
                                s.Cast<GroupDescription>().Select(a => a.Max()).Max();
                            var sourceMinimum = (sourceMinimum1 != null)
                                ? Math.Min((double) sourceMinimum1, (double) sourceMinimum2)
                                : sourceMinimum2;
                            var sourceMaximum = (sourceMaximum1 != null)
                                ? Math.Max((double) sourceMaximum1, (double) sourceMaximum2)
                                : sourceMaximum2;

                            ProcessNumericAxis(sourceMinimum, sourceMaximum, maxDesiredStep);
                        }
                        else
                        {
                            ProcessNumericAxis(sourceMinimum1, sourceMaximum1, maxDesiredStep);
                        }
                        break;

                    case "DateTime":
                        if (Axis == AxisType.Z && BoundSeries.Select(a => a.SeriesData.Distinct().Count()).Max() <= 10)
                        {
                            ProcessStringAxis();
                            DataType = "String";
                            break;
                        }
                        ProcessDateAxis(Math.Floor(maxDesiredStep * 1.5));
                        break;

                    case "TimeSpan":
                        ProcessTimeAxis();
                        break;

                    case "String":
                        ProcessStringAxis();
                        break;

                    default:
                        ProcessNominalAxis();
                        break;
                }
                IsNominal = false;
            }
            IsValid = true;
        }
    }

    public enum AxisType
    {
        X,
        Y,
        Z,
        W,
        V
    };
}