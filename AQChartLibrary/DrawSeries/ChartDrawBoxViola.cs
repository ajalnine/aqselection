﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawViolaAndWiskers()
        {
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.ZValueSeriesTitle);
            var options = _sriX.SourceSeries.Options;
            var withViola = options.Contains("Viol");
            var withCenters = options.Contains("Center");
            var withPoint = options.Contains("Point");
            var withBox = options.Contains("Box");
            var withPies = options.Contains("Pie");
            var layerBrushes = GetLayerBrushes(serieForZ);

            if (withViola) DrawViola(withPoint, withBox, serieForZ, layerBrushes);
            if (withBox) DrawBoxWhiskers(withViola, withPoint, serieForZ, layerBrushes);
            DrawLine(true);
            DrawOutliers();
            if (withPoint) DrawPoints(layerBrushes);
            DrawMinMaxLabels(layerBrushes);
            if (withPies) DrawPies(withViola || withBox, layerBrushes);
            DrawCenters(withPies, withCenters);
            DrawLimitLabels(layerBrushes);
            DrawComments(layerBrushes);
        }

        private void DrawViola(bool withPoints, bool withBox, SeriesRangeInfo serieForZ, Dictionary<object, Brushes> layerBrushes)
        {
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || gd.IndexedData == null) continue;
                var brushes = layerBrushes[gd.Layer ?? "#"];
                var stroke = brushes.strokeBrush;
                var fill = brushes.fillBrush;
                if (withBox && fill is SolidColorBrush)fill = new SolidColorBrush { Color = ((SolidColorBrush)fill).Color, Opacity = fill.Opacity / 1.1};
                if (withBox && fill is SolidColorBrush) fill = new SolidColorBrush { Color = ((SolidColorBrush)fill).Color, Opacity = fill.Opacity / 1.2 };
                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                double dx = gd.UseLayerOffset
                    ? _surfaceWidth / (gd.TotalGroupsCount.Value * gd.TotalLayersCount.Value) / 2
                    : areaWidth / 2;
                       boxX += gd.UseLayerOffset
                    ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0
                    : 0;
                var data = gd.IndexedData.Select(a => a.Value).Cast<object>().ToList();
                var n = data.Count;
                var aggregateStDev = AQMath.AggregateStDev(data);
                if (aggregateStDev == null) continue;

                var s = aggregateStDev.Value;
                var h = s * Math.Pow(4.0d / (3.0d * n), 0.2);
                var k = (1 / (Math.Sqrt(2 * Math.PI)));
                var from = (double)_sriY.BoundAxis.DisplayMinimum;
                var to = (double)_sriY.BoundAxis.DisplayMaximum;
                var step = (to - @from) / 150.0d;
                if (!(Math.Abs(h) > 1e-8)) continue;

                var ps = new List<Point>();

                for (var iy = @from; Math.Abs(to + step - iy) > 1e-8; iy += step)
                {
                    var v = GetDensity(data, iy, n, h, k);
                    var p = v * dx;
                    ps.Add(new Point(p, iy));
                }

                var violaMax = ps.Max(a => Math.Abs(a.X));
                var psnormed = new List<Point>();
                psnormed.AddRange(ps.Select(p => new Point(p.X / violaMax * dx * 0.75, p.Y)));

                var isFirstV = true;
                var pf = new PathFigure { IsClosed = true, IsFilled = true };
                foreach (var p in psnormed)
                {
                    if (p.X < 1) continue;
                    var violaX1 = boxX - p.X;
                    var violaY1 = _surfaceHeight - _surfaceHeight * (p.Y - @from) / (to - @from);
                    if (isFirstV) pf.StartPoint = new Point(violaX1, violaY1);
                    else pf.Segments.Add(new LineSegment { Point = new Point(violaX1, violaY1) });
                    isFirstV = false;
                }
                psnormed.Reverse();
                double violaX = double.PositiveInfinity, violaY = double.PositiveInfinity, violaX2 = 0, violaY2 = 0;
                foreach (var p in psnormed)
                {
                    if (p.X < 1) continue;
                    var violaX1 = boxX + p.X;
                    var violaY1 = _surfaceHeight - _surfaceHeight * (p.Y - @from) / (to - @from);
                    if (violaX > violaX1) violaX = violaX1;
                    if (violaY > violaY1) violaY = violaY1;
                    if (violaX2 < violaX1) violaX2 = violaX1;
                    if (violaY2 < violaY1) violaY2 = violaY1;
                    pf.Segments.Add(new LineSegment { Point = new Point(violaX1, violaY1) });
                }

                var path = new Path
                {
                    StrokeThickness = 1,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf } },
                    Fill = fill,
                    Stroke = stroke
                };

                if (gd.Tag != null)
                {
                    path.Fill = ConvertStringToStroke(gd.Tag.ToString());
                    path.Fill.Opacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
                    path.Stroke = (_sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered) ? new SolidColorBrush(Colors.White) : ConvertStringToStroke(gd.Tag.ToString());
                    if (withBox) path.Fill.Opacity /= 1.5;
                    if (withPoints) path.Fill.Opacity /= 3;
                    path.Stroke.Opacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
                }
                SeriesSurface.Children.Add(path);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetBoxCenterInteractivity(i, path);
                AppendSelectable(new Point((violaX2 - violaX) / 2 + violaX, (violaY2 - violaY) / 2 + violaY), new Size(violaX2 - violaX, violaY2 - violaY), path, ii);
            }
        }

        private void DrawBoxWhiskers(bool withViola, bool withPoints, SeriesRangeInfo serieForZ, Dictionary<object, Brushes> layerBrushes)
        {
            var boxSize = (withViola ? 1d : 2d) * GetMarkerSizeCoefficient(_sriX);
            
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null) continue;

                var brushes = layerBrushes[gd.Layer ?? "#"];
                var stroke = brushes.strokeBrush;
                var fill = brushes.fillBrush;
                if (withPoints && fill is SolidColorBrush) fill = new SolidColorBrush { Color = ((SolidColorBrush)fill).Color, Opacity = fill.Opacity / 2 };

                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var yboxmin = _sriY.BoundAxis.TransformValue(gd.BoxMin, _sriY);
                var yboxmax = _sriY.BoundAxis.TransformValue(gd.BoxMax, _sriY);

                var ywhiskermin = _sriY.BoundAxis.TransformValue(gd.WhiskerMin, _sriY);
                var ywhiskermax = _sriY.BoundAxis.TransformValue(gd.WhiskerMax, _sriY);

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                double dx = gd.UseLayerOffset
                   ? _surfaceWidth / (gd.TotalGroupsCount.Value * gd.TotalLayersCount.Value) / 2
                   : areaWidth / 2;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;

                var yboxoffset = Math.Round(_surfaceHeight * (1 - yboxmax.Value), 1);
                var xboxoffset = Math.Round(boxX - dx * 0.5 * boxSize, 1);

                var markerBrush = stroke;

                var boxwidth = dx * boxSize;
                var boxheight = Math.Round(_surfaceHeight * (yboxmax.Value - yboxmin.Value), 1);
                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                var wiskerStroke = (_sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered) ? fill : markerBrush;

                if (yboxmin.HasValue && yboxmax.HasValue)
                {
                    var r = new Rectangle
                    {
                        Height = boxheight,
                        Width = boxwidth,
                        Stroke = stroke,
                        Fill = fill,
                        StrokeThickness = 1
                    };
                    if (gd.Tag != null)
                    {
                        r.Fill = ConvertStringToStroke(gd.Tag.ToString());
                        r.Fill.Opacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
                        if (withPoints) r.Fill.Opacity /= 2;
                        r.Stroke = (_sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered) ? new SolidColorBrush(Colors.White) : ConvertStringToStroke(gd.Tag.ToString());
                        r.Stroke.Opacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
                        wiskerStroke = ConvertStringToStroke(gd.Tag.ToString());
                        wiskerStroke.Opacity = r.Stroke.Opacity;
                    }

                    SeriesSurface.Children.Add(r);
                    Canvas.SetLeft(r, xboxoffset);
                    Canvas.SetTop(r, yboxoffset);
                    AppendSelectable(new Point(xboxoffset + boxwidth / 2, yboxoffset + boxheight / 2), new Size(boxwidth, boxheight), r, ii);
                    SetBoxCenterInteractivity(i, r);
                }

                if (ywhiskermin.HasValue && ywhiskermax.HasValue)
                {
                    var upwiskerOffset = Math.Round(_surfaceHeight * (1 - ywhiskermax.Value), 1);
                    var l1 = new Line
                    {
                        X1 = boxX,
                        X2 = boxX,
                        Y1 = yboxoffset,
                        Y2 = upwiskerOffset,
                        Stroke = wiskerStroke
                    };

                    var l3 = new Line
                    {
                        X1 = boxX - 0.5 * dx * boxSize,
                        X2 = boxX + 0.5 * dx * boxSize,
                        Y1 = upwiskerOffset,
                        Y2 = upwiskerOffset,
                        Stroke = wiskerStroke
                    };
                    var downwiskerOffset = Math.Round(_surfaceHeight * (1 - ywhiskermin.Value), 1);
                    var l2 = new Line
                    {
                        X1 = boxX,
                        X2 = boxX,
                        Y1 = yboxoffset + boxheight,
                        Y2 = downwiskerOffset,
                        Stroke = wiskerStroke
                    };

                    var l4 = new Line
                    {
                        X1 = boxX - 0.5 * dx * boxSize,
                        X2 = boxX + 0.5 * dx * boxSize,
                        Y1 = downwiskerOffset,
                        Y2 = downwiskerOffset,
                        Stroke = wiskerStroke
                    };
                    SeriesSurface.Children.Add(l1);
                    SeriesSurface.Children.Add(l2);
                    SeriesSurface.Children.Add(l3);
                    SeriesSurface.Children.Add(l4);
                    AppendSelectable(new Point(xboxoffset + boxwidth / 2, yboxoffset + boxheight / 2), new Size(boxwidth, boxheight), l1, ii);
                    AppendSelectable(new Point(xboxoffset + boxwidth / 2, yboxoffset + boxheight / 2), new Size(boxwidth, boxheight), l2, ii);
                    AppendSelectable(new Point(xboxoffset + boxwidth / 2, yboxoffset + boxheight / 2), new Size(boxwidth, boxheight), l3, ii);
                    AppendSelectable(new Point(xboxoffset + boxwidth / 2, yboxoffset + boxheight / 2), new Size(boxwidth, boxheight), l4, ii);
                }
            }
        }

        private void DrawOutliers()
        {
            for (var i = 0; i < _nX; i++)
            {
                var stroke = ConvertStringToStroke(ColorConvertor.GetColorStringForScale(_sriX.SourceSeries.ColorScale, 0.27));
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || !x.HasValue) continue;
                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;

                if (gd.Outliers != null)
                    foreach (var o in gd.Outliers)
                    {
                        var pointStroke = o.Tag != null ? ConvertStringToStroke(o.Tag.ToString()) : stroke;
                        pointStroke.Opacity = GetStrokeOpacity();

                        CreateOutlierPoint(_sriX, o.OriginalIndex,
                            _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(o.Value, _sriY), boxX,
                            stroke);
                    }
                if (gd.Extremes != null)
                    foreach (var o in gd.Extremes)
                    {
                        var pointStroke = o.Tag != null ? ConvertStringToStroke(o.Tag.ToString()) : stroke;
                        pointStroke.Opacity = GetStrokeOpacity();

                        CreateExtremePoint(_sriX, o.OriginalIndex,
                            _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(o.Value, _sriY), boxX,
                            stroke);
                    }
            }
        }

        private void DrawPoints(Dictionary<object, Brushes> layerBrushes )
        {
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || !x.HasValue) continue;
                var strip = gd.PointMode == GroupPointMode.StripChart;
                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                double dx = _surfaceWidth / _nX / 2;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                if (gd.IndexedData == null) continue;

                switch (gd.PointMode)
                {
                    case GroupPointMode.BeeSwarm:
                        DrawBeeSwarmForGroups(layerBrushes, dx, gd, boxX);
                        break;
                    case GroupPointMode.Hexagon:
                        DrawHexagons(layerBrushes, dx, gd, boxX);
                        break;
                    case GroupPointMode.PointsInLine:
                    case GroupPointMode.StripChart:
                        DrawStrip(layerBrushes, dx, gd, strip, boxX);
                        break;
                }
            }
        }

        private void DrawStrip(Dictionary<object, Brushes> layerBrushes, double dx, GroupDescription gd, bool strip, double boxX)
        {
            var isInteractive = gd.N.Value < 25000;
            foreach (var o in gd.IndexedData)
            {
                var offset = strip ? (AQMath.RndNormal(0, 1)) * dx / 4 : 0;
                o.RenderY = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(o.Value, _sriY).Value;
                o.RenderX = boxX + offset.Value;
            }

            foreach (var o in gd.IndexedData)
            {
                CreateDataPoint(o, layerBrushes, isInteractive);
            }
        }

        private void DrawHexagons(Dictionary<object, Brushes> layerBrushes, double dx, GroupDescription gd, double boxX)
        {
            var isInteractive = gd.N.Value < 25000;
            var markerSize = _sriX.SourceSeries.MarkerSize;
            var markerDiameter = markerSize * GetMarkerSizeCoefficient(_sriX) * 1.62;
            var k = Math.Sin(Math.PI / 3);
            var rowHeight = markerDiameter * k;
            var _sY = _sriY;
            var yBound = _sriY.BoundAxis;

            double xoffset;
            double? y;
            var rows = gd.IndexedData.OrderBy(a => a.Value).GroupBy(c => Math.Floor((yBound.TransformValue(c.Value, _sY) * _surfaceHeight / rowHeight).Value) * rowHeight);
            byte rowIndex = 0;
            foreach (var row in rows)
            {
                var n = row.Count();
                xoffset = (rowIndex % 2) * markerDiameter / 2 - Math.Floor(n / 2.0d) * markerDiameter;
                y = _surfaceHeight - row.Key;
                foreach (var o in row)
                {
                    if (xoffset > -dx && xoffset < dx)
                    {
                        o.RenderY = y.Value;
                        o.RenderX = boxX + xoffset;
                    }
                    xoffset += markerDiameter;
                }
                rowIndex++;
            }

            foreach (var o in gd.IndexedData)
            {
                CreateDataPoint(o, layerBrushes, isInteractive);
            }
        }

        private void DrawCenters(bool withPies, bool withCenters)
        {
            var centerBrushes = new Brushes
            {
                strokeBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40)),
                fillBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40)),
            };

            var minLabelFontSizeValue = _sriY.Min;
            var maxLabelFontSizeValue = _sriY.Max;
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null) continue;
                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                var boxY = Math.Round(_surfaceHeight - _surfaceHeight * y.Value, 1);
                var markerSize = _sriX.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(_sriX) * 1.5 * (withPies ? 0.7 : 1);
                if (withCenters)
                {
                    Shape boxCenter = withPies ? (Shape)new Ellipse() : (Shape)new Rectangle();
                    boxCenter.Height = markerSize;
                    boxCenter.Width = markerSize;
                    LabelsSurface.Children.Add(boxCenter);
                    Canvas.SetLeft(boxCenter, boxX - markerSize / 2);
                    Canvas.SetTop(boxCenter, boxY - markerSize / 2);
                    SetBrushesToShape(new Size(markerSize, markerSize), centerBrushes, boxCenter, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                    boxCenter.Fill = centerBrushes.fillBrush;
                    boxCenter.Fill.Opacity = 1;
                    boxCenter.StrokeThickness *= 1.5;
                    SetBoxCenterInteractivity(i, boxCenter);
                }
                var text = string.Empty;
                var yLabel = (gd.ShowText & LabelsType.Centers) == LabelsType.Centers
                    && gd.Center.HasValue;

                var layerLabel = ((gd.ShowText & LabelsType.LayerName) == LabelsType.LayerName
                    && gd.UseLayerOffset);
                if (yLabel) text += Math.Round(gd.Center.Value, 4).ToString(CultureInfo.InvariantCulture);
                if (text.Length > 6) text = text.Substring(0, 6) + "..";
                if (layerLabel && yLabel) text += "\r\n";
                if (layerLabel) text += gd.Layer?.ToString();
                if (!string.IsNullOrEmpty(text))
                {
                    var labelBrush = GetLabelBrush(centerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, true);
                    var labelFontSize = gd.LabelSize is double
                        ? LabelFontSize * (1 + ((double?)gd.LabelSize - (double)minLabelFontSizeValue) / ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;
                    if (withCenters && !(layerLabel && yLabel)) DrawUpSingleLabel(text, labelBrush, boxX, boxY, true, labelFontSize);
                    else DrawCenterSingleLabel(text, labelBrush, boxX, boxY, true, labelFontSize);
                }

            }
        }

        private void DrawPies(bool withViola, Dictionary<object, Brushes> layerBrushes)
        {
            double dx = _surfaceWidth / _nX;
            var pieDiameter = (withViola ? 0.4 : 0.8) * GetMarkerSizeCoefficient(_sriX) * dx;
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.ZValueSeriesTitle);
            if (serieForZ == null) return;
            var labelArrange = new PieLabelArrange();

            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || gd.PieData == null || gd.PieData.Count == 0) continue;
                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                var boxY = Math.Round(_surfaceHeight - _surfaceHeight * y.Value, 1);

                labelArrange.AddConstraint(new LabelConstraint { x1 = boxX - pieDiameter / 2, x2 = boxX + pieDiameter / 2, y1 = boxY - pieDiameter / 2, y2 = boxY + pieDiameter / 2, Key = i });

                double startAng = 0;

                for (var j = 0; j < gd.PieData.Count; j++)
                {
                    var pie = gd.PieData[j];
                    if (pie.Part == 0) continue;

                    double? z = null;
                    if (_sriX.SourceSeries.ZValueSeriesTitle == null) continue;

                    var pieBrushes = layerBrushes[pie.Z ?? "#"];
                    Shape a;
                    double left, right, top, bottom;
                    double ang = pie.Part * 359.9999;

                    var angleRad = 2 * Math.PI * ang / 360;
                    var startAngleRad = 2 * Math.PI * startAng / 360;
                    var arcStartX = pieDiameter / 2 + (pieDiameter / 2) * Math.Sin(startAngleRad);
                    var arcStartY = pieDiameter / 2 - (pieDiameter / 2) * Math.Cos(startAngleRad);
                    var arcEndX = pieDiameter / 2 + (pieDiameter / 2) * Math.Sin(startAngleRad + angleRad);
                    var arcEndY = pieDiameter / 2 - (pieDiameter / 2) * Math.Cos(startAngleRad + angleRad);
                    var l = new ArcSegment
                    {
                        Size = new Size(pieDiameter / 2, pieDiameter / 2),
                        IsLargeArc = angleRad > Math.PI,
                        RotationAngle = ang,
                        SweepDirection = SweepDirection.Clockwise,
                        Point = new Point(arcEndX, arcEndY)
                    };
                    if (!string.IsNullOrEmpty(pie.Text))
                    {
                        var labelFontSize = pie.LabelSize is double
                        ? LabelFontSize * (1 + (pie.LabelSize / 2) ?? 1)
                        : LabelFontSize;

                        labelArrange.AddPieGraph(new PieLabelArrange.PieLabelArrangeDescription
                        {
                            Key = j * i,
                            ConstraintKey = j,
                            CircleCenterX = boxX,
                            CircleCenterY = boxY,
                            circleMaxRadius = pieDiameter / 2,
                            circleMinRadius = 0,
                            angle = startAngleRad + angleRad / 2,
                            angleRange = angleRad / 2,
                            LabelText = pie.Text,
                            LabelBrush = pieBrushes.strokeBrush,
                            WhiteBG = !Inverted,
                            FontSize = labelFontSize * GlobalFontCoefficient,
                            MaxWidth = null,
                            AllowInternal = true
                        });
                    }

                    a = new Path();
                    PathFigure pathFigure = new PathFigure
                    {
                        IsFilled = true,
                        IsClosed = true,
                        StartPoint = new Point(arcStartX, arcStartY)
                    };
                    pathFigure.Segments.Add(l);
                    if (Math.Abs(angleRad - 2 * Math.PI) > 1e-4)
                    {
                        pathFigure.Segments.Add(new LineSegment
                        {
                            Point = new Point(pieDiameter / 2, pieDiameter / 2)
                        });
                    }
                    PathGeometry pathGeometry = new PathGeometry();
                    pathGeometry.Figures = new PathFigureCollection { pathFigure };

                    ((Path)a).Data = pathGeometry;
                    a.Height = pieDiameter;
                    a.Width = pieDiameter;
                    a.HorizontalAlignment = HorizontalAlignment.Center;
                    a.VerticalAlignment = VerticalAlignment.Center;
                    a.StrokeThickness = 0.5;
                    a.StrokeEndLineCap = PenLineCap.Round;
                    a.StrokeStartLineCap = PenLineCap.Round;
                    startAng += ang;

                    left = boxX + Math.Min(Math.Min(arcStartX, arcEndX), pieDiameter / 2) - pieDiameter / 2;
                    right = angleRad < Math.PI ? boxX + Math.Max(Math.Max(arcStartX, arcEndX), pieDiameter / 2) - pieDiameter / 2 : boxX + pieDiameter / 2;
                    top = boxY + Math.Min(Math.Min(arcStartY, arcEndY), pieDiameter / 2) - pieDiameter / 2;
                    bottom = boxY + Math.Max(Math.Max(arcStartY, arcEndY), pieDiameter / 2) - pieDiameter / 2;
                    SetBrushesToShape(new Size(pieDiameter, pieDiameter), pieBrushes, a,
                            _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                    SeriesSurface.Children.Add(a);
                    Canvas.SetLeft(a, boxX - pieDiameter / 2);
                    Canvas.SetTop(a, boxY - pieDiameter / 2);

                    var ii = new InteractivityIndex { Index = i, IndexInGroup = j, SeriesName = _sriX.SeriesName + "_Pie" };

                    AppendSelectable(new Point(left + (right - left) / 2, top + (bottom - top) / 2), new Size(right - left, bottom - top), a, ii);
                    SetPieCenterInteractivity(ii, a);
                }
            }
            labelArrange.RenderPlaces(SeriesSurface, LabelsSurface);
        }

        private void DrawLimitLabels(Dictionary<object, Brushes> layerBrushes)
        {
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null) continue;
                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                var labelBrush = GetLabelBrush(layerBrushes[gd.Layer ?? "#"].strokeBrush, _sriX.SourceSeries.MarkerMode, true);
                if ((gd.ShowText & LabelsType.Outage) == LabelsType.Outage
                    && gd.LowLimit.HasValue && gd.Lower.HasValue && gd.Lower > 0 && gd.LowerPercent.HasValue)
                {
                    var text = string.Format("{0}%\r\nn={1}", gd.LowerPercent.Value, gd.Lower.Value);
                    var ylimit = _sriY.BoundAxis.TransformValue(gd.LowLimit, _sriY);
                    if (ylimit.HasValue)
                    {
                        var boxY = Math.Round(_surfaceHeight - _surfaceHeight * ylimit.Value, 1);
                        DrawDownSingleLabel(text, labelBrush, boxX, boxY, true, LabelFontSize);
                    }
                }
                if ((gd.ShowText & LabelsType.Outage) == LabelsType.Outage
                    && gd.Higher.HasValue && gd.Higher > 0 && gd.HigherPercent.HasValue)
                {
                    var text = string.Format("{0}%\r\nn={1}", gd.HigherPercent.Value, gd.Higher.Value);
                    var ylimit = _sriY.BoundAxis.TransformValue(gd.HighLimit, _sriY);
                    if (ylimit.HasValue)
                    {
                        var boxY = Math.Round(_surfaceHeight - _surfaceHeight * ylimit.Value, 1);
                        DrawUpSingleLabel(text, labelBrush, boxX, boxY, true, LabelFontSize);
                    }
                }
            }
        }

        private void DrawMinMaxLabels(Dictionary<object, Brushes> layerBrushes)
        {
            var stroke = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_sriX.SourceSeries.ColorScale));

            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || ((gd.ShowText & LabelsType.Whiskers) != LabelsType.Whiskers)) continue;
                var y = _sriY.BoundAxis.TransformValue(gd.Center, _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                var labelBrush = GetLabelBrush(layerBrushes[gd.Layer ?? "#"].strokeBrush, _sriX.SourceSeries.MarkerMode, true);
                if (gd.WhiskerMin != null)
                {
                    var text = Math.Round(gd.WhiskerMin.Value, 4).ToString(CultureInfo.InvariantCulture);
                    if (text.Length > 7) text = text.Substring(0, 7) + "..";


                    var ymin = _sriY.BoundAxis.TransformValue(gd.WhiskerMin, _sriY);
                    if (ymin.HasValue)
                    {
                        var boxY = Math.Round(_surfaceHeight - _surfaceHeight * ymin.Value, 1);
                        DrawDownSingleLabel(text, labelBrush, boxX, boxY, true, LabelFontSize);
                    }
                }
                if (gd.WhiskerMax != null)
                {
                    var text2 = Math.Round(gd.WhiskerMax.Value, 4).ToString(CultureInfo.InvariantCulture);
                    if (text2.Length > 7) text2 = text2.Substring(0, 7) + "..";

                    var ymax = _sriY.BoundAxis.TransformValue(gd.WhiskerMax, _sriY);
                    if (ymax.HasValue)
                    {
                        var boxY = Math.Round(_surfaceHeight - _surfaceHeight * ymax.Value, 1);
                        DrawUpSingleLabel(text2, labelBrush, boxX, boxY, true, LabelFontSize);
                    }
                }
            }
        }

        private void DrawComments(Dictionary<object, Brushes> layerBrushes)
        {
            for (var i = 0; i < _nX; i++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                var gd = _sourceY[i] as GroupDescription;
                if (gd == null || (string.IsNullOrEmpty(gd.CustomComments))) continue;
                if (!x.HasValue || double.IsNaN(x.Value)) continue;

                var boxX = Math.Round(_surfaceWidth * x.Value, 1);
                double areaWidth = _surfaceWidth / gd.TotalGroupsCount.Value;
                boxX += gd.UseLayerOffset ? -areaWidth / 2 + areaWidth / gd.TotalLayersCount * (gd.LayerIndex.Value + 0.5) ?? 0 : 0;
                var labelBrush = GetLabelBrush(layerBrushes[gd.Layer ?? "#"].strokeBrush, _sriX.SourceSeries.MarkerMode, true);

                var tb = new TextBlock
                {
                    Text = gd.CustomComments,
                    FontSize = LabelFontSize * GlobalFontCoefficient,
                    Foreground = labelBrush,
                    FontFamily = new FontFamily("Verdana"),
                    TextWrapping = TextWrapping.Wrap,
                    TextAlignment = TextAlignment.Center
                };

                Canvas.SetLeft(tb, boxX - tb.ActualWidth / 2);
                Canvas.SetTop(tb, BottomHelperSurface.ActualHeight - tb.ActualHeight - 2);
                BottomHelperSurface.Children.Add(tb);
            }
            BottomHelperSurface.Effect = null;
        }

        private void SetBoxCenterInteractivity(int i, Shape e)
        {
            var tt = new ToolTip();
            tt.Opened += ToolTip_Opened;
            var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
            tt.Tag = ii;
            e.Tag = tt.Tag;
            ToolTipService.SetToolTip(e, tt);
            AppendSelectable(new Point(Canvas.GetLeft(e) + e.ActualWidth / 2, Canvas.GetTop(e) + e.ActualHeight / 2), new Size(e.ActualWidth, e.ActualHeight), e, ii);

            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }

        private void SetPieCenterInteractivity(InteractivityIndex ii, Shape e)
        {
            var tt = new ToolTip();
            tt.Opened += ToolTip_Opened;
            tt.Tag = ii;
            e.Tag = tt.Tag;
            ToolTipService.SetToolTip(e, tt);
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }

        private void CreateOutlierPoint(SeriesRangeInfo sri1, int i, double? oy, double? ox, Brush fill)
        {
            if (!oy.HasValue || !ox.HasValue) return;
            var hs = sri1.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(sri1) * 0.6;
            var e = new Ellipse { Height = hs * 2, Width = hs * 2, Stroke = fill, Fill = fill };
            SeriesSurface.Children.Add(e);
            Canvas.SetLeft(e, ox.Value - hs);
            Canvas.SetTop(e, oy.Value - hs);
            var tt3 = new ToolTip();
            tt3.Opened += ToolTip_Opened;
            var ii = new InteractivityIndex { Index = i, SeriesName = sri1.SeriesName + "_Outliers" };
            tt3.Tag = ii;
            e.Tag = tt3.Tag;
            ToolTipService.SetToolTip(e, tt3);
            AppendSelectable(new Point(ox.Value, oy.Value), new Size(hs * 2, hs * 2), e, ii);
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }

        private void CreateDataPoint(PointDescription o, Dictionary<object, Brushes> layerBrushes, bool isInteractive)
        {
            var b = layerBrushes[o.Layer ?? "#"];
            if (o.Tag != null)
            {
                b = new Brushes {strokeBrush = ConvertStringToStroke(o.Tag.ToString()), fillBrush = ConvertStringToStroke(o.Tag.ToString()) };
                b.fillBrush.Opacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            }
            
            var hs = _sriX.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(_sriX) * 0.8;
            var e = new Ellipse { Height = hs * 2, Width = hs * 2 };

            SetBrushesToShape(new Size(hs * 2, hs * 2), b, e, _sriX.SourceSeries.MarkerMode,
                _sriX.SourceSeries.MarkerShapeMode);
            SeriesSurface.Children.Add(e);
            Canvas.SetLeft(e, o.RenderX - hs);
            Canvas.SetTop(e, o.RenderY - hs);
            var ii = new InteractivityIndex { Index = o.OriginalIndex, SeriesName = _sriX.SeriesName + "_Indexed" };
            AppendSelectable(new Point(o.RenderX, o.RenderY), new Size(hs * 2, hs * 2), e, ii);
            if (isInteractive)
            {
                var tt3 = new ToolTip();
                tt3.Opened += ToolTip_Opened;
                tt3.Tag = ii;
                e.Tag = tt3.Tag;
                ToolTipService.SetToolTip(e, tt3);
                e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
            }
        }

        private void CreateExtremePoint(SeriesRangeInfo sri1, int i, double? oy, double? ox, Brush fill)
        {
            if (!oy.HasValue || !ox.HasValue) return;
            var hs = sri1.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(_sriX) * 0.6;
            var l1 = new Line { X1 = ox.Value - hs, X2 = ox.Value + hs, Y1 = oy.Value, Y2 = oy.Value, Stroke = fill };
            var l2 = new Line { X1 = ox.Value, X2 = ox.Value, Y1 = oy.Value - hs, Y2 = oy.Value + hs, Stroke = fill };
            SeriesSurface.Children.Add(l1);
            SeriesSurface.Children.Add(l2);

            var e = new Ellipse { Height = hs * 2, Width = hs * 2, Stroke = fill, Fill = fill, Opacity = 0.01 };
            SeriesSurface.Children.Add(e);
            Canvas.SetLeft(e, ox.Value - hs);
            Canvas.SetTop(e, oy.Value - hs);
            var tt3 = new ToolTip();
            tt3.Opened += ToolTip_Opened;
            var ii = new InteractivityIndex { Index = i, SeriesName = sri1.SeriesName + "_Extremes" };
            tt3.Tag = ii;
            e.Tag = tt3.Tag;
            ToolTipService.SetToolTip(e, tt3);
            AppendSelectable(new Point(ox.Value, oy.Value), new Size(hs * 2, hs * 2), l1, ii);
            AppendSelectable(new Point(ox.Value, oy.Value), new Size(hs * 2, hs * 2), l2, ii);
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }

    }
}