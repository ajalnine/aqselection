﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private Dictionary<object, Brushes> GetLayerBrushes()
        {
            Brushes defaultBrushes = GetDefaultBrushes();
            
            var layers = _sourceZ?.Distinct();
            if (layers == null) return new Dictionary<object, Brushes> { { "#", defaultBrushes } };

            var brushes = new Dictionary<object, Brushes>();
            double? z;
            var serie = _sriX ?? _sriY ?? _sriZ;
            foreach (var layer in layers)
            {
                z = _sriZ.BoundAxis.TransformValueZ(layer, _sriZ, serie.SourceSeries.ColorScaleMode);

                var color = layer != null && z.HasValue && !Double.IsNaN(z.Value) && layer?.ToString() != "#"
                    ? ColorConvertor.GetColorStringForScale(serie.SourceSeries.ColorScale, z.Value)
                    : "#ffe0e0e0";

                var b = new Brushes();
                b.strokeBrush = ConvertStringToStroke(color);
                var markerMode = serie.SourceSeries.MarkerMode;
                if (markerMode == MarkerModes.Invisible)
                    markerMode =
                        ChartData.ChartSeries.FirstOrDefault(a => a.MarkerMode != MarkerModes.Invisible)?.MarkerMode ?? MarkerModes.Solid;
                if ((serie.SourceSeries.ColorScale & ColorScales.BW) != ColorScales.BW)
                {
                    b.fillBrush =  ConvertStringToStroke(color);
                    b.fillBrush.Opacity = GetFillOpacity(markerMode);
                }
                else
                {
                    if (z.HasValue) b.fillBrush = ColorConvertor.GetPatternBrush((serie.SourceSeries.ColorScale & ColorScales.Reversed) == ColorScales.Reversed ?1 - z.Value : z.Value);
                    else b.fillBrush = new SolidColorBrush(Colors.Transparent);
                    b.fillBrush.Opacity = 1;
                }
                b.strokeBrush.Opacity = GetStrokeOpacity(markerMode);

                brushes.Add(layer ?? "#", b);
            }

            if (!brushes.ContainsKey("#"))
            {
                var b = new Brushes();
                b.strokeBrush = ConvertStringToStroke("#ffe0e0e0");
                b.fillBrush = ConvertStringToStroke("#ffe0e0e0");
                b.fillBrush.Opacity = GetFillOpacity(serie.SourceSeries.MarkerMode);
                b.strokeBrush.Opacity = GetStrokeOpacity(serie.SourceSeries.MarkerMode);
                brushes.Add("#", b);
            }
            return brushes;
        }

        private Dictionary<object, Brushes> GetLayerBrushes(SeriesRangeInfo serieForZ)
        {
            var brushes = new Dictionary<object, Brushes>();
            var layers = serieForZ?.SeriesData?.Distinct();
            Brushes defaultBrushes = GetDefaultBrushes();
            Brush grayBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor("#ffe0e0e0"));
            if (serieForZ == null) return new Dictionary<object, Brushes> { { "#", defaultBrushes } };
            var serie = _sriX ?? _sriY ?? _sriZ;
            foreach (var layer in layers)
            {
                var z = serieForZ.BoundAxis.TransformValueZ(layer, serieForZ, _sriX.SourceSeries.ColorScaleMode);
                var color = z.HasValue && !Double.IsNaN(z.Value) ? ColorConvertor.GetColorStringForScale(_sriX.SourceSeries.ColorScale, z.Value) : "#ffe0e0e0";
                var b = new Brushes();
                b.strokeBrush = ConvertStringToStroke(color);
                if ((serie.SourceSeries.ColorScale & ColorScales.BW) != ColorScales.BW)
                {
                    b.fillBrush = ConvertStringToStroke(color);
                    b.fillBrush.Opacity = GetFillOpacity(serie.SourceSeries.MarkerMode);
                }
                else
                {
                    if (z.HasValue) b.fillBrush = ColorConvertor.GetPatternBrush((serie.SourceSeries.ColorScale & ColorScales.Reversed) == ColorScales.Reversed ? 1 - z.Value : z.Value);
                    else b.fillBrush = new SolidColorBrush(Colors.Transparent);
                    b.fillBrush.Opacity = 1;
                }

                b.strokeBrush.Opacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);

                if (layer == null) brushes.Add("#", defaultBrushes);
                else brushes.Add(layer, b);
            }
            if (!brushes.ContainsKey("#"))brushes.Add("#", defaultBrushes);
            return brushes;
        }

        private Brushes GetDefaultBrushes()
        {
            string color;
            var serie = _sriX ?? _sriY;
            if (serie == null) return null;
            if (serie.SourceSeries.ZValueForColor == null)
            {
                color = serie.SourceSeries.LineColor ?? ColorConvertor.GetDefaultColorForScale(serie.SourceSeries.ColorScale);
            }
            else
            {
                double? z = null;
                if (serie.SourceSeries.ZValueSeriesTitle == null) z = _sriZ.BoundAxis.TransformValueZ(serie.SourceSeries.ZValueForColor, _sriZ, serie.SourceSeries.ColorScaleMode);
                else
                {
                    var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == serie.SourceSeries.ZValueSeriesTitle);
                    if (serieForZ != null) z = serieForZ.BoundAxis.TransformValueZ(serie.SourceSeries.ZValueForColor, serieForZ, serie.SourceSeries.ColorScaleMode);
                }
                color = z.HasValue && !Double.IsNaN(z.Value) ? ColorConvertor.GetColorStringForScale(_sriX.SourceSeries.ColorScale, z.Value) : "#ffe0e0e0";
            }
            var b = new Brushes();
            b.strokeBrush = ConvertStringToStroke(color);
            b.fillBrush = ConvertStringToStroke(color);
            b.fillBrush.Opacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            b.strokeBrush.Opacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
            return b;
        }

        private Brush GetLabelBrush(Brush strokeBrush, MarkerModes markerMode, bool OverBackGround)
        {
            switch (markerMode)
            {
                case MarkerModes.Ring:
                    return strokeBrush;
                case MarkerModes.Solid:
                case MarkerModes.SolidBordered:
                    return OverBackGround ? strokeBrush : new SolidColorBrush { Color = Colors.White, Opacity = 1 };
                case MarkerModes.SemiTransparent:
                case MarkerModes.Transparent:
                case MarkerModes.Invisible:
                    return new SolidColorBrush { Color = ((SolidColorBrush)strokeBrush).Color, Opacity = 1 };
                default:
                    return strokeBrush;
            }
        }

        private Brush GetInvertedLabelBrush(Brush strokeBrush, MarkerModes markerMode)
        {
            switch (markerMode)
            {
                case MarkerModes.Ring:
                    return strokeBrush;
                case MarkerModes.Solid:
                case MarkerModes.SolidBordered:
                    return strokeBrush;
                case MarkerModes.SemiTransparent:
                case MarkerModes.Transparent:
                case MarkerModes.Invisible:
                    return new SolidColorBrush { Color = ((SolidColorBrush)strokeBrush).Color, Opacity = 1 };
                default:
                    return strokeBrush;
            }
        }

        private double GetStrokeOpacity()
        {
            return strokeOpacity[_sriX.SourceSeries.MarkerMode];
        }

        private static Dictionary<MarkerModes, double> fillOpacity = new Dictionary<MarkerModes, double>
        {
            { MarkerModes.Invisible, 0},
            { MarkerModes.Transparent, 0.1},
            { MarkerModes.Ring, 0},
            { MarkerModes.Solid, 1},
            { MarkerModes.SolidBordered, 1},
            { MarkerModes.SemiTransparent, 0.15},
        };

        private double GetFillOpacity(MarkerModes markerMode)
        {
            return fillOpacity[markerMode];
        }

        private static Dictionary<MarkerModes, double> strokeOpacity = new Dictionary<MarkerModes, double>
        {
            { MarkerModes.Invisible, 0},
            { MarkerModes.Transparent, 0.1},
            { MarkerModes.Ring, 1},
            { MarkerModes.Solid, 1},
            { MarkerModes.SolidBordered, 1},
            { MarkerModes.SemiTransparent, 1},
        };

        private double GetStrokeOpacity(MarkerModes markerMode)
        {
            return strokeOpacity[markerMode];
        }

        private void SetInteractivity(Rectangle e, InteractivityIndex ii)
        {
            var tt3 = new ToolTip();
            tt3.Opened += ToolTip_Opened;
            tt3.Tag = ii;
            e.Tag = tt3.Tag;
            ToolTipService.SetToolTip(e, tt3);
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }
    }

    public class Brushes
    {
        public Brush strokeBrush;
        public Brush fillBrush;
    }
}