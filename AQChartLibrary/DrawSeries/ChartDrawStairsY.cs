﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using System.Linq;
using System.Windows;
using System.Windows.Input;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawStairsOnY()
        {
            if (_sriX.BoundAxis == null || _sriY.BoundAxis == null) return;
            var stringProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("StringProcessing") || (_sriX.SourceSeries.Options.Contains("GroupProcessing") && _sriZ == null));
            var normedProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("Normed"));
            var groupProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("GroupProcessing") && _sriZ != null);
            var numericGroupProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("NumericProcessing") && _sriZ != null);
            var blockWhite = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("BlockWhite"));

            if (groupProcessing) GroupProcessingCaseY( normedProcessing, blockWhite);
            else if (numericGroupProcessing) GroupNumericProcessingCaseY(normedProcessing, blockWhite);
            else if (stringProcessing)
            {
                StringProcessingCaseY(normedProcessing, blockWhite);
            }
            else if (!_sriX.IsNominal)
            {
                NumericProcessingCaseY(normedProcessing, blockWhite);
            }
            else
            {
                NominalProcessingCaseY( normedProcessing, blockWhite);
            }
        }

        private void NominalProcessingCaseY(bool normed, bool blockWhite)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var st = ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(i - 0.5, _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var x2 = _sriX.BoundAxis.TransformValue(i + 0.5, _sriX);
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value)) continue;

                var from = Math.Round(_surfaceWidth*x1.Value, 1) - 0.5*lineThickness;
                var height = Math.Round(_surfaceHeight*y1.Value, 1);
                var to = Math.Round(_surfaceWidth*x2.Value, 1) + 0.5*lineThickness;
                var offset = z.HasValue ? Math.Round(_surfaceHeight*z.Value, 1) : 0;
                var cx = @from + (to - @from)*(1 - st)*0.5;
                var cy = _surfaceHeight - height - offset;
                var cw = (to - @from) * st;
                var ch = height;
                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);
                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize*
                          (1 +
                           ((double?) _sriX?.SeriesMarkerSizes[i] - (double) minLabelFontSizeValue)/
                           ((double) maxLabelFontSizeValue - (double) minLabelFontSizeValue)/2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    if (_sriZ != null && _sriX.SourceSeries.MarkerMode != MarkerModes.Invisible)
                        DrawCenterSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from)/2,
                            _surfaceHeight - 0.5*height - offset, false, labelFontSize);
                    else
                        DrawUpSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from)/2,
                            _surfaceHeight - height - offset, false, labelFontSize);
                }
            }
        }

        private void NumericProcessingCaseY(bool normed, bool blockWhite)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var st = ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);
            for (int i = 0; i < n1 - 1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = _sourceV != null ? _sriX.BoundAxis.TransformValue(_sourceV[i], _sriX) : null;
                var x2 = w ?? _sriX.BoundAxis.TransformValue(_sourceX[i + 1], _sriX);

                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value)) continue;

                var from = Math.Round(_surfaceWidth*x1.Value, 1) - 0.5*lineThickness;
                var offset = z.HasValue ? _surfaceHeight*z.Value : 0;
                var height = Math.Round(_surfaceHeight*y1.Value - offset, 1);
                var to = Math.Round(_surfaceWidth*x2.Value, 1) + 0.5*lineThickness;
                var cx = @from + (to - @from)*(1 - st)*0.5;
                var cy = _surfaceHeight - height - offset;
                var cw = (to - @from)*st;
                var ch = height;

                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);
                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize*
                          (1 +
                           ((double?) _sriX?.SeriesMarkerSizes[i] - (double) minLabelFontSizeValue)/
                           ((double) maxLabelFontSizeValue - (double) minLabelFontSizeValue)/2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    if (_sriZ != null && !fromBase && _sriX.SourceSeries.MarkerMode != MarkerModes.Invisible)
                        DrawCenterSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from)/2,
                            _surfaceHeight - 0.5*height - offset, false, labelFontSize);
                    else
                        DrawUpSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from)/2,
                            _surfaceHeight - height - offset, false, labelFontSize);
                }
            }
        }

        private void GroupNumericProcessingCaseY(bool normed, bool blockWhite)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);

            var columnnumber = _sourceV.Distinct().Count();
            var st = 1 / _sriX.BoundAxis.RealSteps;
            var singleColumnSize = st / columnnumber; // Ширина одного столбца
            var k = (1- ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode])/2;

            for (int i = 0; i < n1 - 1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = (double?)_sourceV?[i];
                if (w < 0) continue;
                x1 += st * w + k * singleColumnSize;
                var x2 = x1 + singleColumnSize - 2 * k * singleColumnSize;
                
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value))
                    continue;

                var from = Math.Round(_surfaceWidth * x1.Value, 1) - 0.5 * lineThickness;
                var offset = z.HasValue ? _surfaceHeight * z.Value : 0;
                var height = Math.Round(_surfaceHeight * y1.Value - offset, 1);
                var to = Math.Round(_surfaceWidth * x2.Value, 1) + 0.5 * lineThickness;
                var cx = @from;
                var cy = _surfaceHeight - height - offset;
                var cw = to - @from;
                var ch = height;

                var r = new Rectangle
                {
                    Height = height,
                    Width = cw,
                   StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);
                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize *
                          (1 +
                           ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                           ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    if (_sriZ != null && !fromBase && _sriX.SourceSeries.MarkerMode!= MarkerModes.Invisible)
                        DrawCenterSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from) / 2,
                            _surfaceHeight - 0.5 * height - offset, false, labelFontSize);
                    else
                        DrawUpSingleLabel(_markerLabels[i].ToString(), labelBrush, @from + (to - @from) / 2,
                            _surfaceHeight - height - offset, false, labelFontSize);
                }
            }
        }

        private void GroupProcessingCaseY(bool normed, bool blockWhite)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var bandnumber = _sourceV.Distinct().Count();
            var st = _sriY.BoundAxis.TransformValue(_sourceY[0], _sriY)*2; 
            var singleBandSize = st/ bandnumber;
            var k = (1 - ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode]) / 2;

            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY)- st/2;
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = (double?)_sourceV?[i];
                if (w < 0) continue;

                y1 += st * w + k * singleBandSize;
                var y2 = y1 + singleBandSize - 2 * k * singleBandSize;
                
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !y2.HasValue ||
                    double.IsNaN(y2.Value))
                    continue;

                var from = Math.Round(_surfaceHeight * y1.Value, 1) - 0.5 * lineThickness;
                var offset = z.HasValue ? Math.Round(_surfaceWidth * z.Value, 1) : 0;
                var width = Math.Round(_surfaceWidth * x1.Value, 1);
                var to = Math.Round(_surfaceHeight * y2.Value, 1) + 0.5 * lineThickness;
                 var cx = offset;
                var cy = _surfaceHeight - to;
                var cw = width;
                var ch = to - @from;
                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);

                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize *
                          (1 +
                           ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                           ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;

                    if (_sriZ != null && _sriX.SourceSeries.MarkerMode != MarkerModes.Invisible)
                    DrawRightSingleLabel(_markerLabels[i].ToString(), labelBrush, offset + width - offset, _surfaceHeight - to + (to - @from) * 0.5, false, labelFontSize);
                    DrawRightSingleLabel(_markerLabels[i].ToString(), labelBrush, offset + width - offset, _surfaceHeight - to + (to - @from) * 0.5, false, labelFontSize);
                }
            }
        }

        private void StringProcessingCaseY(bool normed, bool blockWhite)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var firstNonZero = _sourceY.FirstOrDefault(a => a != null && !string.IsNullOrEmpty(a.ToString()));
            var st = _sriY.BoundAxis.TransformValue(firstNonZero, _sriY);
            if (st == null && _sourceX.Count >= 1) st = _sriY.BoundAxis.TransformValue(_sourceY[1], _sriY)/2;
            st *= ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriX.BoundAxis.TransformValue(_sourceW[i], _sriX) : null;
                var w = _sourceV != null ? _sriX.BoundAxis.TransformValue(_sourceV[i], _sriX) : null;
                var y2 = y1 + 2*st;
                y1 -= st;
                y2 -= st;
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !y2.HasValue ||
                    double.IsNaN(y2.Value)) continue;

                var from = Math.Round(_surfaceHeight * y1.Value, 1) - 0.5*lineThickness;
                var offset = z.HasValue ? Math.Round(_surfaceWidth * z.Value, 1) : 0;
                var width = Math.Round(_surfaceWidth * x1.Value, 1);
                var to = Math.Round(_surfaceHeight * y2.Value, 1) + 0.5*lineThickness;
                var size = width - offset;

                var cx = (size < 0 ? offset - Math.Abs(size) : offset);
                var cy = _surfaceHeight - to;
                var ch = to - @from;
                var cw = Math.Abs(size);

                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);

                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                var labelFontSize = maxLabelFontSizeValue is double
                    ? LabelFontSize *
                      (1 +
                       ((double?) _sriX?.SeriesMarkerSizes[i] - (double) minLabelFontSizeValue)/
                       ((double) maxLabelFontSizeValue - (double) minLabelFontSizeValue)/2.0d ?? 1)
                    : LabelFontSize;
                if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    if (_sriZ != null && _sriX.SourceSeries.MarkerMode != MarkerModes.Invisible && !fromBase || normed)
                        DrawCenterSingleLabel(_markerLabels[i].ToString(), labelBrush, offset + (width - offset)/2,
                            _surfaceHeight - to + (to - @from)*0.5, false, labelFontSize);
                    else
                    {
                        if (size >= 0) DrawRightSingleLabel(_markerLabels[i].ToString(), labelBrush, offset + width - offset, _surfaceHeight - to + (to - @from) * 0.5, false, labelFontSize);
                        else DrawLeftSingleLabel(_markerLabels[i].ToString(), labelBrush, offset + width - offset, _surfaceHeight - to + (to - @from) * 0.5, false, labelFontSize);
                    }
                }
            }
        }
    }
}