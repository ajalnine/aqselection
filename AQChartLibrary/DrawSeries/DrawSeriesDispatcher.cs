﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private List<object> _sourceX, _sourceY, _sourceZ, _sourceW, _sourceV, _markerColors, _markerSizes, _markerLabels, _markerW;
        private SeriesRangeInfo _sriX, _sriY, _sriZ, _sriW, _sriV, _serieForW;
        private double _surfaceWidth, _surfaceHeight;
        private int _nX;
        private Dictionary<ChartSeriesType, DrawSeriesDelegate> _seriesDictionary;
        public delegate void ToolTipRequiredDelegate(object o, ToolTipRequiredEventArgs e);

        public event ToolTipRequiredDelegate ToolTipRequired;

        private void DrawSingleSeries(SeriesRangeInfo sriX, SeriesRangeInfo sriY, SeriesRangeInfo sriZ, SeriesRangeInfo sriW, SeriesRangeInfo sriV, ChartSeriesType cst)
        {
            _surfaceWidth = SeriesSurface.ActualWidth;
            _surfaceHeight = SeriesSurface.ActualHeight;
            if (sriX!=null) _sriX = sriX;
            if (sriY != null) _sriY = sriY;
            _sriZ = sriZ;
            _sriW = sriW;
            _sriV = sriV;
            _sourceX = sriX?.SeriesData;
            _sourceY = sriY?.SeriesData;
            _sourceZ = sriZ?.SeriesData;
            _sourceW = sriW?.SeriesData;
            _sourceV = sriV?.SeriesData;
            _markerColors = (sriX == null || _sriX.SeriesMarkerColors == null ? null : _sriX.SeriesMarkerColors) ?? (_markerColors = sriY == null || _sriY.SeriesMarkerColors == null ? null : _sriY.SeriesMarkerColors);
            _markerSizes = (sriX == null || _sriX.SeriesMarkerSizes == null ? null : _sriX.SeriesMarkerSizes) ?? (sriY == null || _sriY.SeriesMarkerSizes == null ? null : _sriY.SeriesMarkerSizes);
            _markerLabels = (sriX == null || _sriX.SeriesMarkerLabels == null ? null : _sriX.SeriesMarkerLabels) ?? (sriY == null || _sriY.SeriesMarkerLabels == null ? null : _sriY.SeriesMarkerLabels);
            _nX = _sriX == null || _sriX.SeriesData==null ? 0 : _sriX.SeriesData.Count;
            _serieForW = _sriX?.SourceSeries?.WColumnName != null 
                ?_sriX.SourceSeries.WValueSeriesTitle !=null  
                        ?  ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.WValueSeriesTitle)
                        : _sriW
                : null;

            _markerW = _serieForW?.SeriesData?.Where(a => a != null && a != DBNull.Value && !string.IsNullOrEmpty(a.ToString())).Distinct().OrderBy(a => a).ToList();
            _seriesDictionary[cst].Invoke();
        }

        delegate void DrawSeriesDelegate();

        private void Marker_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var el = sender as Shape;
            var Info = (InteractivityIndex)el.Tag;
            if (Info == null) return;
            HandleSingleClickSelection(e, el, Info);
        }

        private SLDataTable GetInteractiveTable(InteractivityIndex Info)
        {
            if (Info == null) return null;
            var sourceTableName =
                ChartData.ChartSeries.SingleOrDefault(a => Info.SeriesName.StartsWith(a.SeriesTitle))?.SourceTableName;
            if (sourceTableName == null) return null;
            var sourceTable = DataTables.SingleOrDefault(a => a?.TableName == sourceTableName);
            return sourceTable;
        }

        private void ToolTip_Opened(object sender, RoutedEventArgs e)
        {
            var tt = sender as ToolTip;
            if (ToolTipRequired != null)
                if (tt != null) ToolTipRequired(tt, new ToolTipRequiredEventArgs {Info = (InteractivityIndex) tt.Tag, TT = tt, SourceTable = _sriX?.SourceTable == null ? _sriY?.SourceTable : null});
        }

        public Brush ConvertStringToStroke(string c)
        {
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);
            var cu =  Convert.ToInt32("0x" + c.Substring(1, c.Length - 1), 16);
            Brush stroke = new SolidColorBrush(Color.FromArgb((byte) ((cu >> 0x18) & 0xff), (byte) ((cu >> 0x10) & 0xff), (byte) ((cu >> 8) & 0xff), (byte) (cu & 0xff)));
            return stroke;
        }
        
        public class InteractivityEventArgs
        {
            public List<InteractivityIndex> Infos { get; set; }
            public InteractivityIndex Info { get { return Infos.FirstOrDefault(); } }
            public Point MouseClickPoint { get; set; }
            public SLDataTable SourceTable { get; set; }
        }

        public class ToolTipRequiredEventArgs
        {
            public ToolTip TT { get; set; }
            public InteractivityIndex Info { get; set; }
            public SLDataTable SourceTable { get; set; }
        }
    }
}