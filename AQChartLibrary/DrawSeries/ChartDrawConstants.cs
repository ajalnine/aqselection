﻿using AQBasicControlsLibrary;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private double _verticalRangeLabelOffset = 20;
        private double _horizontalRangeLabelOffset = 10;
        private void DrawConstantXLine()
        {
            var sri1 = _sriX;
            if (sri1.BoundAxis == null || sri1.SeriesData == null || Math.Abs(sri1.SourceSeries.LineSize) < double.Epsilon) return;
            var actualWidth = SeriesSurface.ActualWidth;
            var actualHeight = SeriesSurface.ActualHeight;
  
            var sourceX = sri1.SeriesData.ToList();
            var th = (sri1.SourceSeries.LineSize > 1) ? sri1.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < 1e-8) return;
            var dc = new DoubleCollection();
            if (sri1.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1/sri1.SourceSeries.LineSize);
            }
            double X = 0;
            var str = ConvertStringToStroke(sri1.SourceSeries.LineColor);
            var requiredLabelOffset = 0d;
            for (int i=0; i< sourceX.Count; i++)
            {
                var x = sourceX[i];
                if (x == null) continue;
                var transformValue = sri1.BoundAxis.TransformValue(x, sri1);
                if (transformValue != null)
                    X = actualWidth*transformValue.Value;
                if (!double.IsNaN(X))
                {
                    var l = new Line
                    {
                        Stroke = str,
                        X1 = X,
                        X2 = X,
                        Y1 = 0,
                        Y2 = actualHeight - 1,
                        StrokeThickness = th,
                        UseLayoutRounding = true
                    };
                    if (dc.Count > 0) l.StrokeDashArray = dc;
                    SeriesSurface.Children.Add(l);

                    if ( _markerLabels!=null && !string.IsNullOrEmpty( _markerLabels[i]?.ToString()))
                    {
                        var al = HorizontalAlignment.Right;
                        var text = _markerLabels[i].ToString();
                        if ("<>".Contains(text[0]))
                        {
                            if (text[0] == '<') al = HorizontalAlignment.Left;
                            text = text.Substring(1, text.Length - 1);
                        }

                        var tb = new TextBlock
                        {
                            Foreground = str,
                            Text = text,
                            MaxWidth = 200,
                            FontFamily = new FontFamily("Verdana"),
                            TextWrapping = TextWrapping.Wrap,
                            FontSize = 8 * GlobalFontCoefficient,
                            Margin = new Thickness(0, 0, 0, 0),
                            TextAlignment = TextAlignment.Center,
                            Tag = sri1.SourceSeries,
                        };
                        LabelsSurface.Children.Add(tb);
                        Canvas.SetLeft(tb, (al == HorizontalAlignment.Left) ? X - tb.ActualWidth - 10 : X+10);
                        Canvas.SetTop(tb, _verticalRangeLabelOffset - tb.ActualHeight + 12);
                        if (al==HorizontalAlignment.Right)
                        {
                            if (requiredLabelOffset < tb.ActualWidth + 10) requiredLabelOffset = tb.ActualWidth + 10;
                        }
                    }

                    if (_bottomHelperExists && ConstantsOnHelpers)
                    {
                        var lb = new Line
                        {
                            Stroke = str,
                            X1 = X,
                            X2 = X,
                            Y1 = 0,
                            Y2 = BottomHelperSurface.Height - 1,
                            StrokeThickness = th,
                            UseLayoutRounding = true
                        };
                        if (sri1.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / sri1.SourceSeries.LineSize);
                            lb.StrokeDashArray = dc2;
                        }

                        BottomHelperSurface.Children.Add(lb);
                    }

                    if (_topHelperExists && ConstantsOnHelpers)
                    {
                        var lt = new Line
                        {
                            Stroke = str,
                            X1 = X,
                            X2 = X,
                            Y1 = 0,
                            Y2 = TopHelperSurface.Height - 1,
                            StrokeThickness = th,
                            UseLayoutRounding = true
                        };
                        if (sri1.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / sri1.SourceSeries.LineSize);
                            lt.StrokeDashArray = dc2;
                        }
                        TopHelperSurface.Children.Add(lt);
                    }
                }
            }
            if (sri1.SourceSeries.Options!= null && sri1.SourceSeries.Options.Contains("Labeled") && sri1.SourceSeries.Options.Contains("Editable"))
            {
                var tb = new TextBox
                {
                    AcceptsReturn = true,
                    BorderThickness = new Thickness(0),
                    Foreground = str,
                    Text = sri1.SourceSeries.SeriesTitle.Replace("#", string.Empty),
                    MaxWidth = 200,
                    FontFamily = new FontFamily("Verdana"),
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Margin = new Thickness(0, 0, 0, 0),
                    TextAlignment = TextAlignment.Center,
                    Tag = sri1.SourceSeries,
                    Background = new SolidColorBrush(Colors.Transparent)
                };
                tb.LostFocus += ChartLegend_LostFocus;
                tb.GotFocus += ChartLegend_GotFocus;
                LabelsSurface.Children.Add(tb);
                tb.UpdateLayout();

                var startLabel = sri1.BoundAxis.TransformValue(sourceX.Min(), sri1) * actualWidth;
                var endLabel = sri1.BoundAxis.TransformValue(sourceX.Max(), sri1) * actualWidth;
                var lineEnd = startLabel != endLabel 
                    ? (endLabel - startLabel)> tb.ActualWidth+10 
                        ? endLabel.Value
                        : endLabel + 5 + tb.ActualWidth + requiredLabelOffset
                    : startLabel ;
                var position = startLabel != endLabel
                    ? (endLabel - startLabel) > tb.ActualWidth + 10
                        ? (startLabel + endLabel) / 2 - tb.ActualWidth / 2
                        : endLabel + 5 + requiredLabelOffset
                    : startLabel + 5 + requiredLabelOffset;

                if (!startLabel.HasValue || !endLabel.HasValue) return;

                Canvas.SetLeft(tb, position.Value);
                Canvas.SetTop(tb, _verticalRangeLabelOffset);
                var l = new Line
                {
                    Stroke = str,
                    X1 = startLabel.Value,
                    X2 = lineEnd.Value,
                    Y1 = _verticalRangeLabelOffset + tb.ActualHeight,
                    Y2 = _verticalRangeLabelOffset + tb.ActualHeight,
                    StrokeThickness = th /2,
                    UseLayoutRounding = true
                };
                SeriesSurface.Children.Add(l);
                _verticalRangeLabelOffset += tb.ActualHeight + 10;
                if (_verticalRangeLabelOffset > actualHeight) _verticalRangeLabelOffset = 10;
            }

            else

            if (sri1.SourceSeries.Options != null && sri1.SourceSeries.Options.Contains("Labeled") && !sri1.SourceSeries.Options.Contains("Editable"))
            {
                var tb = new TextBlock
                {
                    Foreground = str,
                    Text = sri1.SourceSeries.SeriesTitle.Replace("#", string.Empty),
                    MaxWidth = 120,
                    FontFamily = new FontFamily("Verdana"),
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Margin = new Thickness(0, 0, 0, 0),
                    TextAlignment = TextAlignment.Center,
                    Tag = sri1.SourceSeries
                };
                LabelsSurface.Children.Add(tb);

                var startLabel = sri1.BoundAxis.TransformValue(sourceX.Min(), sri1) * actualWidth;
                var endLabel = sri1.BoundAxis.TransformValue(sourceX.Max(), sri1) * actualWidth;
                var lineEnd = startLabel != endLabel
                    ? (endLabel - startLabel) > tb.ActualWidth + 10
                        ? endLabel.Value
                        : endLabel + 5 + tb.ActualWidth
                    : startLabel;
                var position = startLabel != endLabel
                    ? (endLabel - startLabel) > tb.ActualWidth + 10
                        ? (startLabel + endLabel) / 2 - tb.ActualWidth / 2
                        : endLabel + 5
                    : startLabel + 5;

                if (!startLabel.HasValue || !endLabel.HasValue) return;

                Canvas.SetLeft(tb, position.Value);
                Canvas.SetTop(tb, _verticalRangeLabelOffset);
                var l = new Line
                {
                    Stroke = str,
                    X1 = startLabel.Value,
                    X2 = lineEnd.Value,
                    Y1 = _verticalRangeLabelOffset + tb.ActualHeight,
                    Y2 = _verticalRangeLabelOffset + tb.ActualHeight,
                    StrokeThickness = th / 2,
                    UseLayoutRounding = true
                };
                SeriesSurface.Children.Add(l);
                _verticalRangeLabelOffset += tb.ActualHeight + 10;
                if (_verticalRangeLabelOffset > actualHeight) _verticalRangeLabelOffset = 10;
            }
        }

        private void DrawConstantYLine()
        {
            var sri2 = _sriY;
            if (sri2.BoundAxis == null || sri2.SeriesData == null || Math.Abs(sri2.SourceSeries.LineSize) < double.Epsilon) return;
            var actualWidth = SeriesSurface.ActualWidth;
            var actualHeight = SeriesSurface.ActualHeight;
            var sourceY = sri2.SeriesData.ToList();
            var str = ConvertStringToStroke(sri2.SourceSeries.LineColor);
            var th = (sri2.SourceSeries.LineSize > 1) ? sri2.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < 1e-8) return;
            var dc = new DoubleCollection();
            if (sri2.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1/sri2.SourceSeries.LineSize);
            }
            var requiredLabelOffset = 0d;
            for (int i = 0; i < sourceY.Count; i++)
            {
                var y = sourceY[i];
                if (y == null) continue;
                var transformValue = sri2.BoundAxis.TransformValue(y, sri2);
                if (transformValue == null) continue;
                var Y = actualHeight - actualHeight*transformValue.Value;
                if (double.IsNaN(Y)) continue;
                var l = new Line
                {
                    Stroke = ConvertStringToStroke(sri2.SourceSeries.LineColor),
                    X1 = 0,
                    X2 = actualWidth - 1,
                    Y1 = Y,
                    Y2 = Y,
                    StrokeThickness = th,
                    UseLayoutRounding = true
                };
                if (dc.Count > 0) l.StrokeDashArray = dc;
                SeriesSurface.Children.Add(l);

                if (_markerLabels != null && !string.IsNullOrEmpty(_markerLabels[i]?.ToString()))
                {
                    var al = VerticalAlignment.Top;
                    var text = _markerLabels[i].ToString();
                    if ("<>".Contains(text[0]))
                    {
                        if (text[0] == '<') al = VerticalAlignment.Bottom;
                        text = text.Substring(1, text.Length - 1);
                    }

                    var tb = new TextBlock
                    {
                        Foreground = str,
                        Text = text,
                        MaxWidth = 120,
                        FontFamily = new FontFamily("Verdana"),
                        TextWrapping = TextWrapping.Wrap,
                        FontSize = 8 * GlobalFontCoefficient,
                        Margin = new Thickness(0, 0, 0, 0),
                        TextAlignment = TextAlignment.Center,
                        Tag = sri2.SourceSeries
                    };
                    LabelsSurface.Children.Add(tb);

                    Canvas.SetLeft(tb, _horizontalRangeLabelOffset + 2);
                    Canvas.SetTop(tb, (al == VerticalAlignment.Top) ? Y - tb.ActualHeight - 2 : Y + 2);
                    if (al==VerticalAlignment.Top)
                    {
                        if (requiredLabelOffset < tb.ActualHeight + 2) requiredLabelOffset = tb.ActualHeight + 2;
                    }
                }

                if (_leftHelperExists && ConstantsOnHelpers)
                {
                    var ll = new Line
                    {
                        Stroke = ConvertStringToStroke(sri2.SourceSeries.LineColor),
                        X1 = 0,
                        X2 = LeftHelperSurface.Width - 1,
                        Y1 = Y,
                        Y2 = Y,
                        StrokeThickness = th,
                        UseLayoutRounding = true
                    };
                    if (sri2.SourceSeries.LineSize < 1)
                    {
                        var dc2 = new DoubleCollection();
                        dc2.Add(1);
                        dc2.Add(1 / sri2.SourceSeries.LineSize);
                        ll.StrokeDashArray = dc2;
                    }
                    LeftHelperSurface.Children.Add(ll);
                }

                if (_rightHelperExists && ConstantsOnHelpers)
                {
                    var lr = new Line
                    {
                        Stroke = ConvertStringToStroke(sri2.SourceSeries.LineColor),
                        X1 = 0,
                        X2 = RightHelperSurface.Width - 1,
                        Y1 = Y,
                        Y2 = Y,
                        StrokeThickness = th,
                        UseLayoutRounding = true
                    };
                    if (sri2.SourceSeries.LineSize < 1)
                    {
                        var dc2 = new DoubleCollection();
                        dc2.Add(1);
                        dc2.Add(1 / sri2.SourceSeries.LineSize);
                        lr.StrokeDashArray = dc2;
                    }
                    RightHelperSurface.Children.Add(lr);
                }
            }

            if (sri2.SourceSeries.Options != null && sri2.SourceSeries.Options.Contains("Labeled"))
            {
                var tb = new TextBox
                {
                    AcceptsReturn = true,
                    BorderThickness = new Thickness(0),
                    Foreground = str,
                    Text = sri2.SourceSeries.SeriesTitle.Replace("#", string.Empty),
                    MaxWidth = 120,
                    FontFamily = new FontFamily("Verdana"),
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Margin = new Thickness(0, 0, 0, 0),
                    Tag = sri2.SourceSeries,
                    TextAlignment = TextAlignment.Center,
                    Background = new SolidColorBrush(Colors.Transparent)
                };
                tb.LostFocus += ChartLegend_LostFocus;
                tb.GotFocus += ChartLegend_GotFocus;
                LabelsSurface.Children.Add(tb);
                tb.UpdateLayout();
                var startLabel = actualHeight - sri2.BoundAxis.TransformValue(sourceY.Min(), sri2) * actualHeight;
                var endLabel = actualHeight - sri2.BoundAxis.TransformValue(sourceY.Max(), sri2) * actualHeight;

                var lineEnd = startLabel != endLabel
                    ? (startLabel - endLabel) > tb.ActualHeight + 5
                        ? endLabel
                        : endLabel - 2 - tb.ActualHeight - requiredLabelOffset
                    : startLabel;

                var position = startLabel != endLabel
                    ? (startLabel - endLabel) > tb.ActualHeight + 5
                        ? (startLabel + endLabel) / 2 - tb.ActualHeight / 2
                        : endLabel - 2 - tb.ActualHeight - requiredLabelOffset
                    : startLabel - 2 - tb.ActualHeight - requiredLabelOffset;

                if (!startLabel.HasValue || !endLabel.HasValue) return;

                Canvas.SetLeft(tb, _horizontalRangeLabelOffset + 2);
                Canvas.SetTop(tb, position.Value);
                var l = new Line
                {
                    Stroke = str,
                    Y1 = startLabel.Value,
                    Y2 = lineEnd.Value,
                    X1 = _horizontalRangeLabelOffset,
                    X2 = _horizontalRangeLabelOffset,
                    StrokeThickness = th / 2,
                    UseLayoutRounding = true
                };
                SeriesSurface.Children.Add(l);
                _horizontalRangeLabelOffset += tb.ActualWidth + 10;
                if (_horizontalRangeLabelOffset > actualWidth) _horizontalRangeLabelOffset = 10;
            }
        }

        private void DrawConstantXRectangle()
        {
            if (_sriX.BoundAxis == null || _sourceX.Count < 2) return;
            var stroke = ConvertStringToStroke(_sriX.SourceSeries.LineColor ?? ColorConvertor.GetDefaultColorForScale(_sriX.SourceSeries.ColorScale));
            var fillcolor = ConvertStringToStroke(_sriX.SourceSeries.LineColor ?? ColorConvertor.GetDefaultColorForScale(_sriX.SourceSeries.ColorScale));
            fillcolor.Opacity = 0.05;
            var isSmooth = _sriX.SourceSeries.Options?.Contains("Smooth");
            var fill = fillcolor;

            var actualWidth = SeriesSurface.ActualWidth;
            var actualHeight = SeriesSurface.ActualHeight;

            var th = (_sriX.SourceSeries.LineSize > 1) ? _sriX.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < 1e-8) return;
            var dc = new DoubleCollection();
            if (_sriX.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1 / _sriX.SourceSeries.LineSize);
            }
            double X1 = 0, X2 = 0;
            for (var i = 0; i < _sourceX.Count; i += 2)
            {
                var x1 = _sourceX[i];
                var x2 = _sourceX[i + 1];
                X1 = x1 == null ? 0 : actualWidth * _sriX.BoundAxis.TransformValue(x1, _sriX).Value;
                if (!double.IsNaN(X1) && !double.IsNaN(X2))
                {
                    X2 = x2 == null ? actualWidth : actualWidth * _sriX.BoundAxis.TransformValue(x2, _sriX).Value;
                    if (X2 < X1)
                    {
                        var t = X2;
                        X2 = X1;
                        X1 = t;
                    }
                    var l = new Rectangle
                    {
                        Stroke = stroke,
                        Fill = fill,
                        Width = X2 - X1,
                        Height = actualHeight,
                        StrokeThickness = 0,
                        UseLayoutRounding = true,
                        IsHitTestVisible = false
                    };
                    Canvas.SetLeft(l, X1);
                    Canvas.SetTop(l, 0);
                    if (dc.Count > 0) l.StrokeDashArray = dc;
                    SeriesSurface.Children.Add(l);

                    if (_topHelperExists && ConstantsOnHelpers)
                    {
                        var lt = new Rectangle
                        {
                            Stroke = stroke,
                            Fill = fill,
                            Width = X2 - X1,
                            Height = TopHelperSurface.Height,
                            StrokeThickness = 0,
                            UseLayoutRounding = true,
                            IsHitTestVisible = false
                        };
                        Canvas.SetLeft(lt, X1);
                        Canvas.SetTop(lt, 0);
                        if (_sriX.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / _sriY.SourceSeries.LineSize);
                            lt.StrokeDashArray = dc2;
                        }
                        TopHelperSurface.Children.Add(lt);
                    }

                    if (_bottomHelperExists && ConstantsOnHelpers)
                    {
                        var lb = new Rectangle
                        {
                            Stroke = stroke,
                            Fill = fill,
                            Width = X2 - X1,
                            Height = BottomHelperSurface.Height,
                            StrokeThickness = 0,
                            UseLayoutRounding = true,
                            IsHitTestVisible = false
                        };
                        Canvas.SetLeft(lb, X1);
                        Canvas.SetTop(lb, 0);
                        if (_sriX.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / _sriY.SourceSeries.LineSize);
                            lb.StrokeDashArray = dc2;
                        }
                        BottomHelperSurface.Children.Add(lb);
                    }
                }
            }
        }

        private void DrawConstantYRectangle()
        {
            if (_sriY.BoundAxis == null || _sourceY.Count<2) return;
            var stroke = ConvertStringToStroke(_sriY.SourceSeries.LineColor ?? ColorConvertor.GetDefaultColorForScale(_sriY.SourceSeries.ColorScale));
            var fillcolor = ConvertStringToStroke(_sriY.SourceSeries.LineColor ?? ColorConvertor.GetDefaultColorForScale(_sriY.SourceSeries.ColorScale));
            fillcolor.Opacity = 0.05;
            var isSmooth = _sriY.SourceSeries.Options?.Contains("Smooth");
            var fill = fillcolor;
            var actualWidth = SeriesSurface.ActualWidth;
            var actualHeight = SeriesSurface.ActualHeight;

            var th = (_sriY.SourceSeries.LineSize > 1) ? _sriY.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < 1e-8) return;
            var dc = new DoubleCollection();
            if (_sriY.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1 / _sriY.SourceSeries.LineSize);
            }
            double Y1 = 0, Y2 = 0;
            for (var i = 0; i < _sourceY.Count; i+=2)
            {
                var y1 = _sourceY[i];
                var y2 = _sourceY[i+1];
                Y1 = y1 == null ? actualHeight : actualHeight - actualHeight * _sriY.BoundAxis.TransformValue(y1, _sriY).Value;
                Y2 = y2 == null ? 0 : actualHeight - actualHeight * _sriY.BoundAxis.TransformValue(y2, _sriY).Value;
                if (!double.IsNaN(Y1) && !double.IsNaN(Y2))
                {
                    if (Y1 < Y2)
                    {
                        var t = Y2;
                        Y2 = Y1;
                        Y1 = t;
                    }
                    var l = new Rectangle
                    {
                        Stroke = stroke,
                        Fill = fill,
                        Width = actualWidth,
                        Height = Y1 - Y2,
                        StrokeThickness = 0,
                        UseLayoutRounding = true,
                        IsHitTestVisible = false
                    };
                    Canvas.SetLeft(l, 0);
                    Canvas.SetTop(l, Y2);

                    if (dc.Count > 0) l.StrokeDashArray = dc;
                    SeriesSurface.Children.Add(l);

                    if (_leftHelperExists && ConstantsOnHelpers)
                    {
                        var ll = new Rectangle
                        {
                            Stroke = stroke,
                            Fill = fill,
                            Width = LeftHelperSurface.Width,
                            Height = Y1 - Y2,
                            StrokeThickness = 0,
                            UseLayoutRounding = true,
                            IsHitTestVisible = false
                        };
                        Canvas.SetLeft(ll, 0);
                        Canvas.SetTop(ll, Y2);

                        if (_sriY.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / _sriY.SourceSeries.LineSize);
                            ll.StrokeDashArray = dc2;
                        }
                        LeftHelperSurface.Children.Add(ll);
                    }

                    if (_rightHelperExists && ConstantsOnHelpers)
                    {
                        var lr = new Rectangle
                        {
                            Stroke = stroke,
                            Fill = fill,
                            Width = LeftHelperSurface.Width,
                            Height = Y1 - Y2,
                            StrokeThickness = 0,
                            UseLayoutRounding = true,
                            IsHitTestVisible = false
                        };
                        Canvas.SetLeft(lr, 0);
                        Canvas.SetTop(lr, Y2);

                        if (_sriY.SourceSeries.LineSize < 1)
                        {
                            var dc2 = new DoubleCollection();
                            dc2.Add(1);
                            dc2.Add(1 / _sriY.SourceSeries.LineSize);
                            lr.StrokeDashArray = dc2;
                        }
                        RightHelperSurface.Children.Add(lr);
                    }
                }
            }
        }
    }
}