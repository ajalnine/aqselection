﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawSun()
        {
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.ZValueSeriesTitle);
            if (serieForZ == null) return;

            var xs = _sourceX.Distinct().ToList();
            var layerBrushes = GetLayerBrushes();
            var margin = 20;
            var arrangment = CreateArrangement(new Size(_surfaceWidth -margin, _surfaceHeight - margin), xs,_sriX.SourceSeries.ArrangementMode);
            var labelArrange = new PieLabelArrange();
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();
            var ringCoefficient = _sriX.SourceSeries.MarkerSize;
            var pieMode = Math.Abs(ringCoefficient) < 1e-8;
            var msc = GetMarkerSizeCoefficient(_sriX);

            for (var j = 0; j < _sriZ.Count; j++)
            {
                var x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[j] : j, _sriX);
                if (x==null)continue;
                var ari = arrangment[_sourceX[j]?.ToString() ?? "#"];
                var pieMaxDiameter = Math.Round(msc * ari.OuterDiameter / 1.3, 1);
                var pieMinDiameter = Math.Round(msc * (ari.InnerDiameter +  (ari.OuterDiameter - ari.InnerDiameter)*ringCoefficient) /1.3, 1) ;
                var centerX = ari.RegionCenter.X;
                var centerY = ari.RegionCenter.Y;

                var pieBrushes = layerBrushes[_sourceZ[j] ?? "#"];
                Shape a;
                double x1, x2, y1, y2;

                a = new Path();

                double ang = (double)_sourceW[j] * 359.9999;
                double startAng = (double)_sourceV[j] * 359.9999;

                var angleRad = 2 * Math.PI * ang / 360;
                var startAngleRad = 2 * Math.PI * startAng / 360;

                var largeArcStartX = pieMaxDiameter / 2 + (pieMaxDiameter / 2) * Math.Sin(startAngleRad);
                var largeArcStartY = pieMaxDiameter / 2 - (pieMaxDiameter / 2) * Math.Cos(startAngleRad);
                var largeArcEndX = pieMaxDiameter / 2 + (pieMaxDiameter / 2) * Math.Sin(startAngleRad + angleRad);
                var largeArcEndY = pieMaxDiameter / 2 - (pieMaxDiameter / 2) * Math.Cos(startAngleRad + angleRad);
                
                var largeArc = new ArcSegment
                {
                    Size = new Size(pieMaxDiameter / 2, pieMaxDiameter / 2),
                    IsLargeArc = angleRad > Math.PI,
                    RotationAngle = ang,
                    SweepDirection = SweepDirection.Clockwise,
                    Point = new Point(largeArcEndX, largeArcEndY)
                };

                var smallArcEndX = pieMaxDiameter / 2 + (pieMinDiameter / 2) * Math.Sin(startAngleRad);
                var smallArcEndY = pieMaxDiameter / 2 - (pieMinDiameter / 2) * Math.Cos(startAngleRad);
                var smallArcStartX = pieMaxDiameter / 2 + (pieMinDiameter / 2) * Math.Sin(startAngleRad + angleRad);
                var smallArcStartY = pieMaxDiameter / 2 - (pieMinDiameter / 2) * Math.Cos(startAngleRad + angleRad);

                x1 = centerX - pieMaxDiameter / 2 + Math.Min(Math.Min(largeArcStartX, smallArcStartX), Math.Min(largeArcEndX, smallArcEndX)); 
                x2 = centerX - pieMaxDiameter / 2 + Math.Max(Math.Max(largeArcStartX, smallArcStartX), Math.Max(largeArcEndX, smallArcEndX));
                y1 = centerY - pieMaxDiameter / 2 + Math.Min(Math.Min(largeArcStartY, smallArcStartY), Math.Min(largeArcEndY, smallArcEndY));
                y2 = centerY - pieMaxDiameter / 2 + Math.Max(Math.Max(largeArcStartY, smallArcStartY), Math.Max(largeArcEndY, smallArcEndY));

                var smallArc = new ArcSegment
                {
                    Size = new Size(pieMinDiameter / 2, pieMinDiameter / 2),
                    IsLargeArc = angleRad > Math.PI,
                    RotationAngle = ang,
                    SweepDirection = SweepDirection.Counterclockwise,
                    Point = new Point(smallArcEndX, smallArcEndY)
                };

                PathFigure pathFigure = new PathFigure
                {
                    IsFilled = true,
                    IsClosed = true,
                    StartPoint = new Point(largeArcStartX, largeArcStartY)
                };
                pathFigure.Segments.Add(largeArc);

                pathFigure.Segments.Add(new LineSegment
                {
                    Point = new Point(smallArcStartX, smallArcStartY)
                });

                if (Math.Abs(pieMinDiameter)>1e-8) pathFigure.Segments.Add(smallArc);

                PathGeometry pathGeometry = new PathGeometry();
                pathGeometry.Figures = new PathFigureCollection { pathFigure };
                
                ((Path)a).Data = pathGeometry;
                a.Height = pieMaxDiameter;
                a.Width = pieMaxDiameter;
                a.HorizontalAlignment = HorizontalAlignment.Center;
                a.VerticalAlignment = VerticalAlignment.Center;
                a.StrokeThickness = 0.5;
                a.StrokeEndLineCap = PenLineCap.Round;
                a.StrokeStartLineCap = PenLineCap.Round;

                if (_markerLabels != null && _markerLabels[j] != null && !string.IsNullOrWhiteSpace(_markerLabels[j].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize *
                          (1 +
                           ((double?)_sriX?.SeriesMarkerSizes[j] - (double)minLabelFontSizeValue) /
                           ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;

                    var onlyInternal = _sriX.SourceSeries.ArrangementMode == ArrangementMode.Concentric && pieMode;

                    labelArrange.AddPieGraph(new PieLabelArrange.PieLabelArrangeDescription
                         {
                            Key = j, ConstraintKey = j,
                            CircleCenterX = centerX,
                            CircleCenterY = centerY,
                            circleMaxRadius = pieMaxDiameter / 2,
                            circleMinRadius = pieMinDiameter/2,
                            angle = startAngleRad + angleRad / 2,
                            angleRange = angleRad / 2,
                            LabelText = _markerLabels[j]?.ToString(),
                            LabelBrush = onlyInternal ? defaultBackgroundBrush : pieBrushes.strokeBrush,
                            WhiteBG = !Inverted && !onlyInternal,
                            FontSize = labelFontSize * GlobalFontCoefficient,
                            MaxWidth = null,
                            InvertedLabelBrush = GetLabelBrush(pieBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, false),
                            AllowInternal = pieMode,
                            OnlyInternal = onlyInternal
                         });
                }
                SetBrushesToShape(new Size(pieMaxDiameter, pieMaxDiameter), pieBrushes, a, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                SeriesSurface.Children.Add(a);
                Canvas.SetLeft(a, centerX - pieMaxDiameter / 2);
                Canvas.SetTop(a, centerY - pieMaxDiameter / 2);
                var ii = new InteractivityIndex { Index = j, IndexInGroup = j, SeriesName = _sriX.SeriesName + "_Pie" };
                AppendSelectable(new Point((x2 + x1) / 2, (y2 + y1) / 2), new Size(Math.Abs(x2 - x1),Math.Abs(y2 - y1)), a, ii);
                labelArrange.AddConstraint(new LabelConstraint { x1 = x1, x2 = x2, y1 = y1, y2 = y2, Key = j });
                SetPieCenterInteractivity(ii, a);

            }
            
            if (_sriX.SourceSeries.ArrangementMode == ArrangementMode.Concentric)
            {
                CreateLabelsForConcentric(arrangment);
            }
            else
            {
                var key = 0x10000;
                if (pieMode) CreateLabelsAboveCircle(arrangment, labelArrange, key, true);
                else
                {
                    var labels = new Dictionary<ArrangementInfo, TextBlock>();
                    var max = Math.Round(msc * ((arrangment.Values.First().OuterDiameter) * ringCoefficient) / 1.3, 1);
                    foreach (var arr in arrangment.Values)
                    {
                        var name = arr.Name?.ToString() == "#" ? "Нет значения" : arr.Name?.ToString();
                        var label = CreateLabel(name, max, false);
                        label.UpdateLayout();
                        labels.Add(arr, label);
                    }

                    if (labels.All(l => l.Value.ActualWidth < max)) CreateLabelsInRing(arrangment, labels);
                    else CreateLabelsAboveCircle(arrangment, labelArrange, key, true);
                }
            }
            labelArrange.RenderPlaces(SeriesSurface, LabelsSurface);
        }
    }
}