﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawKDEOnX()
        {
            if (_sriX == null) return;
            var markerstroke = GetDefaultBrushes();
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.ZValueSeriesTitle);
            var layers = _sourceZ?.Distinct();
            var markerSize = _sriX.SourceSeries.MarkerSize;
            var layeredData = new Dictionary<object, List<object>>();
            var layerSide = new Dictionary<object, double>();
            var layerBrushes = GetLayerBrushes();
            var isOpposite = _sriX.SourceSeries.Options == "Opposite";
            if (layers != null)
            {
                foreach (var layer in layers)
                {
                    var data = new List<object>();
                    var side = 1.0d;
                    for (int i = 0; i < _sourceX.Count; i++)
                    {
                        if (_sourceZ[i]?.ToString() != layer?.ToString()) continue;
                        data.Add(_sourceX[i]);
                        side = ((double?)_sourceW?[i]) ?? 1.0d;
                    }
                    layeredData.Add(layer, data);
                    layerSide.Add(layer, side);
                }
            }
            else
            {
                var data = new List<object>();
                for (int i = 0; i < _sourceX.Count; i++)
                {
                    data.Add(_sourceX[i]);
                }
                layeredData.Add("#", data);
                layerSide.Add("#", 1);

            }

            foreach (var layer in layeredData.Keys)
            {
                var brush = layerBrushes[layer ?? "#"];
                var data = layeredData[layer];
                var n = data.Count;
                var aggregateStDev = AQMath.AggregateStDev(data);
                if (aggregateStDev == null) continue;

                var s = aggregateStDev.Value;
                var h = s * Math.Pow(4.0d / (3.0d * n), 0.2);
                var k = (1 / (Math.Sqrt(2 * Math.PI)));
                var from = (double)_sriX.BoundAxis.DisplayMinimum;
                var to = (double)_sriX.BoundAxis.DisplayMaximum;
                var step = (to - @from) / 150.0d;
                if (!(Math.Abs(h) > 1e-8)) continue;

                var ps = new List<Point>();

                for (var ix = @from; Math.Abs(to + step - ix) > 1e-8; ix += step)
                {
                    var v = GetDensity(data, ix, n, h, k);
                    ps.Add(new Point(ix, v));
                }
                
                var violaMax = ps.Max(a => Math.Abs(a.Y));
                var psnormed = new List<Point>();
                psnormed.AddRange(ps.Select(p => new Point(p.X, p.Y / violaMax)));

                var isFirstV = true;
                var pf = new PathFigure { IsClosed = true, IsFilled = true };
                var topPlaced = (layerSide[layer] > 0 && isOpposite);
                foreach (var p in psnormed)
                {
                    var violaX1 = _surfaceWidth * _sriX.BoundAxis.TransformValue(p.X, _sriX).Value;
                    var violaY1 = BottomHelperSurface.Height * p.Y;
                    if (violaY1 < 0.2) continue;
                    if (ShowHelpersMirrored ^ topPlaced) violaY1 = BottomHelperSurface.Height - violaY1;
                    if (isFirstV) pf.StartPoint = new Point(violaX1, violaY1);
                    else pf.Segments.Add(new LineSegment { Point = new Point(violaX1, violaY1) });
                    isFirstV = false;
                }
                if (ShowHelpersMirrored ^ topPlaced)
                {
                    pf.Segments.Add(new LineSegment { Point = new Point(_surfaceWidth, BottomHelperSurface.Height) });
                    pf.Segments.Add(new LineSegment { Point = new Point(0, BottomHelperSurface.Height) });
                }
                else
                {
                    pf.Segments.Add(new LineSegment { Point = new Point(_surfaceWidth, 0) });
                    pf.Segments.Add(new LineSegment { Point = new Point(0, 0) });
                }

                var path = new Path
                {
                    StrokeThickness = 1,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf } },
                    IsHitTestVisible = false
                };
                SetBrushesToShape(new Size(markerSize, markerSize), brush, path, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                if (layer?.ToString() != "#") path.Fill.Opacity /= 1.2;
                if (topPlaced) TopHelperSurface.Children.Add(path);
                else BottomHelperSurface.Children.Add(path);
            }
        }

        private void DrawKDEOnY()
        {
            if (_sriY == null) return;
            var markerstroke = GetDefaultBrushes();
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriY.SourceSeries.ZValueSeriesTitle);
            var layerBrushes = GetLayerBrushes();
            var layers = _sourceZ?.Distinct();
            var yBound = _sriY.BoundAxis;
            var markerSize = _sriY.SourceSeries.MarkerSize;
            var layeredData = new Dictionary<object, List<object>>();
            if (layers != null)
            {
                foreach (var layer in layers)
                {
                    var data = new List<object>();
                    for (int i = 0; i < _sourceY.Count; i++)
                    {
                        if (_sourceZ[i]?.ToString() != layer?.ToString()) continue;
                        data.Add(_sourceY[i]);
                    }
                    layeredData.Add(layer, data);
                }
            }
            else
            {
                var data = new List<object>();
                for (int i = 0; i < _sourceY.Count; i++)
                {
                    data.Add(_sourceY[i]);
                }
                layeredData.Add("#", data);
            }

            foreach (var layer in layeredData.Keys)
            {
                var brush = layerBrushes[layer ?? "#"];
                var data = layeredData[layer];
                var n = data.Count;
                var aggregateStDev = AQMath.AggregateStDev(data);
                if (aggregateStDev == null) continue;

                var s = aggregateStDev.Value;
                var h = s * Math.Pow(4.0d / (3.0d * n), 0.2);
                var k = (1 / (Math.Sqrt(2 * Math.PI)));
                var from = (double)_sriY.BoundAxis.DisplayMinimum;
                var to = (double)_sriY.BoundAxis.DisplayMaximum;
                var step = (to - @from) / 150.0d;
                if (!(Math.Abs(h) > 1e-8)) continue;

                var ps = new List<Point>();

                for (var iy = @from; Math.Abs(to + step - iy) > 1e-8; iy += step)
                {
                    var v = GetDensity(data, iy, n, h, k);
                    ps.Add(new Point(v, iy));
                }

                var violaMax = ps.Max(a => Math.Abs(a.X));
                var psnormed = new List<Point>();
                psnormed.AddRange(ps.Select(p => new Point(p.X / violaMax, p.Y)));

                var isFirstV = true;
                var pf = new PathFigure { IsClosed = true, IsFilled = true };
                
                if (!_sriY.BoundAxis.IsSecondary)
                {
                    foreach (var p in psnormed)
                    {
                        var violaX1 = LeftHelperSurface.Width - LeftHelperSurface.Width * p.X;
                        var violaY1 = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(p.Y, _sriY).Value;
                        if (violaX1 < 0.2) continue;
                        if (ShowHelpersMirrored) violaX1 = LeftHelperSurface.Width - violaX1;
                        if (isFirstV) pf.StartPoint = new Point(violaX1, violaY1);
                        else pf.Segments.Add(new LineSegment { Point = new Point(violaX1, violaY1) });
                        isFirstV = false;
                    }

                    if (ShowHelpersMirrored)
                    {
                        pf.Segments.Add(new LineSegment { Point = new Point(0, 0) });
                        pf.Segments.Add(new LineSegment { Point = new Point(0, _surfaceHeight) });
                    }
                    else
                    {
                        pf.Segments.Add(new LineSegment { Point = new Point(LeftHelperSurface.Width, 0) });
                        pf.Segments.Add(new LineSegment { Point = new Point(LeftHelperSurface.Width, _surfaceHeight) });
                    }

                    var path = new Path
                    {
                        StrokeThickness = 1,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf } },
                        IsHitTestVisible = false
                    };
                    SetBrushesToShape(new Size(markerSize, markerSize), brush, path, _sriY.SourceSeries.MarkerMode, _sriY.SourceSeries.MarkerShapeMode);
                    if (layer?.ToString() != "#") path.Fill.Opacity /= 1.2;
                    LeftHelperSurface.Children.Add(path);
                }
                else
                {
                    foreach (var p in psnormed)
                    {
                        var violaX1 = RightHelperSurface.Width * p.X;
                        var violaY1 = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(p.Y, _sriY).Value;
                        if (violaX1 < 0.2) continue;
                        if (ShowHelpersMirrored) violaX1 = RightHelperSurface.Width - violaX1;
                        if (isFirstV) pf.StartPoint = new Point(violaX1, violaY1);
                        else pf.Segments.Add(new LineSegment { Point = new Point(violaX1, violaY1) });
                        isFirstV = false;
                    }
                    if (ShowHelpersMirrored)
                    {
                        pf.Segments.Add(new LineSegment { Point = new Point(RightHelperSurface.Width, 0) });
                        pf.Segments.Add(new LineSegment { Point = new Point(RightHelperSurface.Width, _surfaceHeight) });
                    }
                    else
                    {
                        pf.Segments.Add(new LineSegment { Point = new Point(0, 0) });
                        pf.Segments.Add(new LineSegment { Point = new Point(0, _surfaceHeight) });
                    }

                    var path = new Path
                    {
                        StrokeThickness = 1,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf } },
                        IsHitTestVisible = false
                    };
                    SetBrushesToShape(new Size(markerSize, markerSize), brush, path, _sriY.SourceSeries.MarkerMode, _sriY.SourceSeries.MarkerShapeMode);
                    if (layer?.ToString() != "#NoLayers") path.Fill.Opacity /= 1.2;
                    RightHelperSurface.Children.Add(path);
                }
            }
        }
                
        public double GetDensity(List<object> data, double x, double n, double h, double k)
        {
            var a = 1 / n / h;
            var result = data.Where(v=>v!=null).Sum(v => k * Math.Exp(-(Math.Pow((x - (double)v) / h, 2) / 2)));
            result *= a;
            return result;
        }
    }
}