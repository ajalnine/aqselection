﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawSingleLabel(string labelText, Brush textBrush, Size markersize, double surfaceX, double surfaceY, double fontSize)
        {
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                IsHitTestVisible = false
            };

            if (tb.ActualWidth*1.2 > markersize.Height)
            {
                Canvas.SetLeft(tb, surfaceX - tb.ActualWidth/2);
                Canvas.SetTop(tb, surfaceY - tb.ActualHeight - markersize.Height/2 - 2);
                LabelsSurface.Children.Add(tb);
            }
            else
            {
                Canvas.SetLeft(tb, surfaceX - tb.ActualWidth/2);
                Canvas.SetTop(tb, surfaceY - tb.ActualHeight/2);
                if (_sriX.SourceSeries.MarkerMode != MarkerModes.Ring) tb.Foreground = new SolidColorBrush(Colors.White);
                LabelsSurface.Children.Add(tb);
            }
        }

        public static Brush GetWhiteBackGround(bool negative)
        {
            if (negative)
            {
                return new RadialGradientBrush
                {
                    GradientOrigin = new Point(0.5, 0.5),
                    Center = new Point(0.5, 0.5),
                    GradientStops =
                {
                    new GradientStop{Offset = 0, Color = Color.FromArgb(192, 0, 0, 0)},
                    new GradientStop{Offset = 0.8, Color = Color.FromArgb(192, 0, 0, 0)},
                    new GradientStop{Offset = 1, Color =  Color.FromArgb(0, 0, 0, 0)}
                }
                };
            }
            else
            {
                return new RadialGradientBrush
                {
                    GradientOrigin = new Point(0.5, 0.5),
                    Center = new Point(0.5, 0.5),
                    GradientStops =
                {
                    new GradientStop{Offset = 0, Color = Color.FromArgb(192, 255, 255, 255)},
                    new GradientStop{Offset = 0.8, Color = Color.FromArgb(192, 255, 255, 255)},
                    new GradientStop{Offset = 1, Color =  Color.FromArgb(0, 255, 255, 255)}
                }
                };
            }
            
        }

        public static Brush GetColorBackGround(Color c)
        {
            return new RadialGradientBrush
            {
                GradientOrigin = new Point(0.5, 0.5),
                Center = new Point(0.5, 0.5),
                GradientStops =
                {
                    new GradientStop{Offset = 0, Color = Color.FromArgb(64, c.R, c.G, c.B)},
                    new GradientStop{Offset = 0.8, Color = Color.FromArgb(64, c.R, c.G, c.B)},
                    new GradientStop{Offset = 1, Color =  Color.FromArgb(0, c.R, c.G, c.B)}
                }
            };
        }

        private void DrawUpSingleLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, bool whiteBG, double fontSize)
        {
            if (double.IsNaN(fontSize)) return;

            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap, 
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };
            
            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight  - 2);
            
            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX - tb.ActualWidth / 2);
                Canvas.SetTop(rect, surfaceY - tb.ActualHeight - 2);
                LabelsSurface.Children.Add(rect);
            }
            LabelsSurface.Children.Add(tb);
        }


        private void DrawDownSingleLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, bool whiteBG, double fontSize)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY + 2);
            
            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX - tb.ActualWidth / 2);
                Canvas.SetTop(rect, surfaceY + 2);
                LabelsSurface.Children.Add(rect);
            }
            LabelsSurface.Children.Add(tb);
        }

        private void DrawRightSingleLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, bool whiteBG, double fontSize)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX + 2);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight /2);

            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX + 2);
                Canvas.SetTop(rect, surfaceY - tb.ActualHeight / 2);
                LabelsSurface.Children.Add(rect);
            }
            LabelsSurface.Children.Add(tb);
        }

        private void DrawLeftSingleLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, bool whiteBG, double fontSize)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - 2 - tb.ActualWidth);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight / 2);

            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(tb, surfaceX - 2 - tb.ActualWidth);
                Canvas.SetTop(rect, surfaceY - tb.ActualHeight / 2);
                LabelsSurface.Children.Add(rect);
            }
            LabelsSurface.Children.Add(tb);
        }

        private void DrawCenterSingleLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, bool whiteBG, double fontSize)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY  - tb.ActualHeight / 2);
            
            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX - tb.ActualWidth / 2);
                Canvas.SetTop(rect, surfaceY - tb.ActualHeight / 2);
                LabelsSurface.Children.Add(rect);
            }
            LabelsSurface.Children.Add(tb);
        }

        private void DrawCenterHelperLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, double fontSize, bool whiteBG, Canvas surface)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight / 2);
            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualWidth + 8,
                    Height = tb.ActualHeight + 2,
                    Margin = new Thickness(-4, -1, -4, -1),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX - tb.ActualWidth / 2);
                Canvas.SetTop(rect, surfaceY - tb.ActualHeight / 2);
                surface.Children.Add(rect);
            }
            surface.Children.Add(tb);
        }

        private void DrawRotatedCenterHelperLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, double fontSize, bool whiteBG, Canvas surface)
        {
            if (double.IsNaN(fontSize)) return;
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                TextWrapping = TextWrapping.Wrap,
                TextAlignment = TextAlignment.Center,
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight / 2);
            if (whiteBG)
            {
                var rect = new Rectangle
                {
                    Fill = GetWhiteBackGround(Inverted),
                    Width = tb.ActualHeight + 2,
                    Height = tb.ActualWidth + 8,
                    Margin = new Thickness(-1, -4, -1, -4),
                    IsHitTestVisible = false
                };
                Canvas.SetLeft(rect, surfaceX - tb.ActualHeight / 2);
                Canvas.SetTop(rect, surfaceY - tb.ActualWidth / 2);
                surface.Children.Add(rect);
            }
            var rt = new RotateTransform { CenterX = tb.ActualWidth / 2, CenterY = tb.ActualHeight / 2, Angle = -90};
            tb.RenderTransform = rt;
            surface.Children.Add(tb);
        }
    }
}
