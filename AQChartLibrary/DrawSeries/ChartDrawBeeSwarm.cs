﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private class Intersection
        {
            public bool Out;
            public double Offset;
            public bool Valid;
            public int index;
        }

        private void DrawBeeSwarmForGroups(Dictionary<object, Brushes> layerBrushes, double dx, GroupDescription gd, double boxX)
        {
            var markerSize = _sriX.SourceSeries.MarkerSize;
            var markerRadius = markerSize * GetMarkerSizeCoefficient(_sriX) * 0.81;
            var _sY = _sriY;
            var yBound = _sriY.BoundAxis;
            var isInteractive = gd.N.Value < 25000;
            var lastSwarm = new List<Point>();
            double x;
            double? y;
            List<Intersection> intersections;
            foreach (var o in gd.IndexedData.OrderBy(a => a.Value))
            {
                y = _surfaceHeight - yBound.TransformValue(o.Value, _sY) * _surfaceHeight;
                if (!y.HasValue) continue;
                intersections = GetIntersections(lastSwarm, y.Value, markerRadius, null);
                RemoveInvalidIntersections(intersections, false);
                x = GetBestIntersection(intersections);
                if (Math.Abs(x) > dx) x = Math.IEEERemainder(x, dx) + Math.Sign(x) * Math.Log(Math.Abs(x));
                lastSwarm.Add(new Point { X = x, Y = y.Value });
                o.RenderX = boxX + x;
                o.RenderY = y.Value;
                CreateDataPoint(o, layerBrushes, isInteractive);
                }
        }

        private void DrawBeeSwarmOnFloor()
        {
            var layerBrushes = GetLayerBrushes();

            if (_sriX == null || _sriY == null) return;
           
            var _sX = _sriX;
            var isInteractive = _sriX.Count < 25000;
            var lastSwarm = new List<Point>();
            double? x;
            double? y;
            List<Intersection> intersections;
            double markerSize = _sriX.SourceSeries.MarkerSize;
            var markerRadius = markerSize * GetMarkerSizeCoefficient(_sriX) * 0.81;
            var op = new List<IndexedValue>();
            for (var i = 0 ; i <_sourceX.Count; i++) op.Add(new IndexedValue {Value  = _sourceX[i], Index = i, Option = (double?)_sourceW[i]});
            
            var ordered = op.OrderBy(a => a.Value).ToList();
            var xBound = _sriX.BoundAxis;
            var isOpposite = _sriX.SourceSeries.Options == "Opposite";

            if (!isOpposite)
            {
                foreach (var o in ordered)
                {
                    x = xBound.TransformValue(o.Value, _sX)*_surfaceWidth;
                    if (!x.HasValue) continue;
                    intersections = GetIntersections(lastSwarm, x.Value, markerRadius, null);
                    RemoveInvalidIntersections(intersections, true);
                    y = GetBestIntersection(intersections);
                    if (double.IsNaN(y.Value)) continue;
                    if (Math.Abs(y.Value) < 0) y = 0;
                    lastSwarm.Add(new Point {X = y.Value, Y = x.Value});
                    CreateDataPoint(new PointDescription { OriginalIndex = o.Index, RenderY = _surfaceHeight - y.Value - markerRadius, RenderX = x.Value, Tag = _markerColors?[o.Index], Layer = _sourceZ?[o.Index] }, layerBrushes, isInteractive);
                }
            }
            else
            {
                foreach (var o in ordered.Where(a=>a.Option>0))
                {
                    x = xBound.TransformValue(o.Value, _sX) * _surfaceWidth;
                    if (!x.HasValue) continue;
                    intersections = GetIntersections(lastSwarm, x.Value, markerRadius, null);
                    RemoveInvalidIntersections(intersections, true);
                    y = GetBestIntersection(intersections);
                    if (double.IsNaN(y.Value)) continue;
                    if (Math.Abs(y.Value) < 0) y = 0;
                    lastSwarm.Add(new Point { X = y.Value, Y = x.Value });
                    CreateDataPoint(new PointDescription { OriginalIndex = o.Index, RenderY = _surfaceHeight / 2 - y.Value - markerRadius, RenderX = x.Value, Tag = _markerColors?[o.Index], Layer = _sourceZ?[o.Index] }, layerBrushes, isInteractive);
                }
                lastSwarm = new List<Point>();
                foreach (var o in ordered.Where(a => a.Option <0))
                {
                    x = xBound.TransformValue(o.Value, _sX) * _surfaceWidth;
                    if (!x.HasValue) continue;
                    intersections = GetIntersections(lastSwarm, x.Value, markerRadius, null);
                    RemoveInvalidIntersections(intersections, true);
                    y = GetBestIntersection(intersections);
                    if (double.IsNaN(y.Value)) continue;
                    if (Math.Abs(y.Value) < 0) y = 0;
                    lastSwarm.Add(new Point { X = y.Value, Y = x.Value });
                    CreateDataPoint(new PointDescription { OriginalIndex = o.Index, RenderY = _surfaceHeight / 2 + y.Value + markerRadius, RenderX = x.Value, Tag = _markerColors?[o.Index], Layer = _sourceZ?[o.Index] }, layerBrushes, isInteractive);
                }
            }
        }

        public class IndexedValue
        {
            public object Value;
            public int Index;
            public double? Option;
        }

        private void RemoveInvalidIntersections(List<Intersection> intersections, bool removeNegative)
        {
            var candidates = intersections.OrderBy(a => a.Offset).ToList();
            for (var i = 0; i < candidates.Count; ++i)
            {
                if (candidates[i].Out) continue;
                for (var j = i + 1; j < candidates.Count; ++j)
                {
                    if (candidates[i].index == candidates[j].index)
                    {
                        candidates[j].Out = true;
                        break;
                    }
                    candidates[j].Valid = false;
                }
            }

            for (var i = 0; i < candidates.Count; ++i)
            {
                if (removeNegative && candidates[i].Offset < 0) candidates[i].Valid = false;
            }
            foreach (var c in candidates.Where(a => !a.Valid))
            {
                intersections.Remove(c);
            }
        }

        private List<Intersection> GetIntersections(List<Point> swarm, double y, double r, DoWorkEventArgs e)
        {
            if (swarm.Count == 0) return new List<Intersection> { new Intersection { Out = false, Valid = true, Offset = 0 }, new Intersection { Out = true, Valid = true, Offset = 0 } };
            var result = new List<Intersection>();
            var toDelete = new List<Point>();
            var i = 0;
            double o;
            Intersection i1, i2;

            var canBeAt0 = swarm.All(a => Math.Sqrt(a.X*a.X + (a.Y - y)*(a.Y - y)) > 2*r);
            if (canBeAt0) return new List<Intersection> { new Intersection { Out = false, Valid = true, Offset = 0 }, new Intersection { Out = true, Valid = true, Offset = 0 } };
            foreach (var bee in swarm)
            {
                if (Math.Abs(bee.Y - y) >= 2 * r) toDelete.Add(bee);
                else
                {
                    o = Math.Sqrt(4 * r * r - (bee.Y - y) * (bee.Y - y));
                    i1 = new Intersection { Valid = true, Offset = bee.X - o, index = i };
                    result.Add(i1);
                    i2 = new Intersection { Out = true, Valid = true, Offset = bee.X + o, index = i };
                    result.Add(i2);
                    i++;
                    if (e!=null && e.Cancel) return result;
                }
            }

            foreach (var farBee in toDelete) swarm.Remove(farBee);
            return result;
        }
        
        private double GetBestIntersection(List<Intersection> intersections)
        {
            var firstOrDefault = intersections.OrderBy(a => Math.Abs(a.Offset)).FirstOrDefault();
            return firstOrDefault?.Offset ?? 0;
        }
    }
}