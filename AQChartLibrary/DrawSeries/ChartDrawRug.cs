﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawRugOnX()
        {
            if (_sriX == null) return;
            var layerBrushes = GetLayerBrushes();
            double? x = null;
            double? y = null;
            var rugWidth = _sriX.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(_sriX) * 0.1;
            var xBound = _sriX.BoundAxis;
            var zBound = _sriZ?.BoundAxis;
            var isOpposite = _sriX.SourceSeries.Options == "Opposite";
            var isMultiple = _sriX.SourceSeries.Options == "IsMultiple";
            var rugSeries = xBound.BoundSeries.Where(a => a.SourceSeries.SeriesType == ChartSeriesType.RugOnX).ToList();
            var rugNumber = _sourceZ?.Distinct().Count() ?? 1;
            if (isMultiple) rugNumber = rugSeries.Count;

            if (isOpposite) rugNumber /= 2;

            var seriesIndex = rugSeries.IndexOf(_sriX);

            var layers = _sourceZ?.Distinct();
            var layerUp = new List<object>();
            var layerDown = new List<object>();
            if (layers != null)
            {
                if (isOpposite)
                {
                    foreach (var layer in layers)
                    {
                        var side = 1.0d;
                        for (int i = 0; i < _sourceX.Count; i++)
                        {
                            if (_sourceZ[i]?.ToString() != layer?.ToString()) continue;
                            side = ((double?)_sourceW?[i]) ?? 1.0d;
                        }
                        if (side < 1) layerUp.Add(layer);
                        else layerDown.Add(layer);
                    }
                }
                else
                {
                    layerDown.AddRange(_sourceZ.Distinct());
                }
            }

            var brushes = GetLayerBrushes();

            for (var i = 0; i < _sourceX.Count; i++)
            {

                x = xBound.TransformValue(_sourceX[i], _sriX) * _surfaceWidth;
                if (!x.HasValue) continue;
                var option = (double?)_sourceW?[i];

                if (isMultiple) y = BottomHelperSurface.Height * ((seriesIndex + 0.5) / rugNumber);
                else if (zBound == null) y = BottomHelperSurface.Height / 2;
                else
                {
                    var upIndex = layerUp.IndexOf(_sourceZ[i]);
                    if (upIndex >= 0)
                    {
                        y = TopHelperSurface.Height * ((upIndex + 0.5) / layerUp.Count);
                        rugNumber = layerUp.Count;
                    }
                    else
                    {
                        var downIndex = layerDown.IndexOf(_sourceZ[i]);
                        if (downIndex >= 0) y = BottomHelperSurface.Height * ((downIndex + 0.5) / layerDown.Count);
                        rugNumber = layerDown.Count;
                    }
                }
                var rugHeight = BottomHelperSurface.Height / rugNumber * GetMarkerSizeCoefficient(_sriX) * (isOpposite ? 2 : 1);
                if (rugHeight > BottomHelperSurface.Height / 2) rugHeight = BottomHelperSurface.Height / 2;
                if (rugHeight < 3) rugHeight = 3;

                if (!y.HasValue || double.IsNaN(y.Value)) continue;
                if (ShowHelpersMirrored) y = BottomHelperSurface.Height - y;
                var surface = isOpposite
                    ? option < 0 ? BottomHelperSurface : TopHelperSurface
                    : BottomHelperSurface;
                var b = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                CreateRugPointX(_sriX, i, y, x, rugWidth, rugHeight, b, surface, surface == BottomHelperSurface);
            }
        }

        private void DrawRugOnY()
        {
            if (_sriY == null) return;
            var layerBrushes = GetLayerBrushes();
            double? x;
            double? y;
            var rugHeight = _sriY.SourceSeries.MarkerSize * GetMarkerSizeCoefficient(_sriY) * 0.1;
            var yBound = _sriY.BoundAxis;
            var zBound = _sriZ?.BoundAxis;
            var isRight = _sriY.SourceSeries.Options == "Right" || _sriY.BoundAxis.IsSecondary;
            var isMultiple = _sriX.SourceSeries.Options == "IsMultiple";
            var rugSeries = yBound.BoundSeries.Where(a => a.SourceSeries.SeriesType == ChartSeriesType.RugOnY).ToList();
            var rugNumber = _sourceZ?.Distinct().Count() ?? 1;
            if (isMultiple) rugNumber = rugSeries.Count;

            var rugWidth = LeftHelperSurface.Width / rugNumber * GetMarkerSizeCoefficient(_sriY) * (isRight ? 2 : 1);
            if (rugWidth > LeftHelperSurface.Width / 2) rugWidth = LeftHelperSurface.Width / 2;
            if (rugWidth < 3) rugWidth = 3;

            var seriesIndex = rugSeries.IndexOf(_sriY);

            var brushes = GetLayerBrushes();

            for (int i = 0; i < _sourceY.Count; i++)
            {
                y = yBound.TransformValue(_sourceY[i], _sriY) * _surfaceHeight;
                if (!y.HasValue) continue;
                if (isMultiple) x = LeftHelperSurface.Width * ((seriesIndex + 0.5) / rugNumber);
                else x = zBound != null ? zBound.TransformValue(_sourceZ[i], _sriZ) * LeftHelperSurface.Width : LeftHelperSurface.Width / 2;
                if (!x.HasValue || double.IsNaN(x.Value)) continue;
                if (ShowHelpersMirrored) x = LeftHelperSurface.Width - x;
                var surface = isRight
                    ? RightHelperSurface
                    : LeftHelperSurface;
                var b = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                CreateRugPointY(_sriX, i, _surfaceHeight - y, x, rugWidth, rugHeight, b, surface, surface == RightHelperSurface);
            }
        }

        private void CreateRugPointX(SeriesRangeInfo sri1, int i, double? oy, double? ox, double? width, double? height, Brushes stroke, Canvas rugSurface, bool afterChartSurface)
        {
            var e = new Rectangle { Height = height.Value, Width = width.Value };
            SetBrushesToShape(new Size(width.Value, height.Value), stroke, e, sri1.SourceSeries.MarkerMode, sri1.SourceSeries.MarkerShapeMode);
            if (sri1.SourceSeries.MarkerMode == MarkerModes.SolidBordered) e.Stroke = e.Fill;
            var offset = afterChartSurface
                ? ShowHelpersBeyondAxis
                    ? 5 + _surfaceHeight + BottomAxis.ActualHeight
                    : _surfaceHeight
                : ShowHelpersBeyondAxis
                    ? -TopAxis.ActualHeight - TopHelperSurface.Height - 5
                    : -TopHelperSurface.Height;
            var ii = new InteractivityIndex { Index = i, SeriesName = sri1.SeriesName + "_Indexed" };
            AppendSelectable(new Point(ox.Value, oy.Value + offset), new Size(width.Value, height.Value), e, ii);
            rugSurface.Children.Add(e);
            Canvas.SetLeft(e, ox.Value - width.Value / 2);
            Canvas.SetTop(e, oy.Value - height.Value / 2);
            SetInteractivity(e, ii);
        }

        private void CreateRugPointY(SeriesRangeInfo sri1, int i, double? oy, double? ox, double? width, double? height, Brushes stroke, Canvas rugSurface, bool afterChartSurface)
        {
            var e = new Rectangle { Height = height.Value, Width = width.Value };
            SetBrushesToShape(new Size(width.Value, height.Value), stroke, e, sri1.SourceSeries.MarkerMode, sri1.SourceSeries.MarkerShapeMode);
            if (sri1.SourceSeries.MarkerMode == MarkerModes.SolidBordered) e.Stroke = e.Fill;
            var offset = afterChartSurface
                ? ShowHelpersBeyondAxis
                    ? 10 + _surfaceWidth + RightAxis.ActualWidth
                    : _surfaceWidth
                : ShowHelpersBeyondAxis
                    ? -LeftHelperSurface.Width - 10 - LeftAxis.ActualWidth
                    : -LeftHelperSurface.Width;
            var ii = new InteractivityIndex { Index = i, SeriesName = sri1.SeriesName + "_Indexed" };
            rugSurface.Children.Add(e);
            if (afterChartSurface)
            {
                Canvas.SetLeft(e, ox.Value - width.Value / 2);
                AppendSelectable(new Point(ox.Value + offset, oy.Value), new Size(width.Value, height.Value), e, ii);
            }
            else
            {
                Canvas.SetLeft(e, rugSurface.Width - ox.Value - width.Value / 2);
                AppendSelectable(new Point(rugSurface.Width - ox.Value + offset, oy.Value), new Size(width.Value, height.Value), e, ii);
            }
            Canvas.SetTop(e, oy.Value - height.Value / 2);
            SetInteractivity(e, ii);
        }
    }
}