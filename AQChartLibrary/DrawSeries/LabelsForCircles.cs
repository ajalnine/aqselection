﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQChartLibrary
{
    public partial class Chart
    {
        private void CreateLabelsForConcentric(Dictionary<object, ArrangementInfo> arrangment)
        {
            var max = Math.Round(GetMarkerSizeCoefficient(_sriX) * arrangment.Values.Max(a => a.OuterDiameter) / 1.3, 1);
            var center = arrangment.Values.FirstOrDefault()?.RegionCenter;
            var labels = arrangment.Keys.Reverse().ToList();
            if (labels.Count==1 && (labels[0]?.ToString().StartsWith("#") ?? false))return;
            var ca = new ChartAxis
            {
                AxisOrientation = Orientation.Vertical,
                IsNominal = true,
                IsPrimary = true,
                Height = max / 2,
                DataLabels = labels.Select(a=>a?.ToString()).ToList(),
                EnableCellCondense = false,
                Margin = new Thickness(0, 0, 0, 0),
                FontSize = LabelFontSize * GlobalFontCoefficient * 0.9,
                Tag = "X",
                LabelColor = defaultForegroundBrush
            };
            LabelsSurface.Children.Add(ca);
            ca.UpdateLayout();
            ca.AxisClicked += OnAxisClicked;
            Canvas.SetLeft(ca, _surfaceWidth - ca.ActualWidth - 20);
            Canvas.SetTop(ca, center.Value.Y - ca.ActualHeight);
            var y = center.Value.Y;
            var ystep = ca.ActualHeight / labels.Count;
            for (var i = 0; i <= labels.Count; i++)
            {
                var l = new Line
                {
                    X1 = center.Value.X,
                    X2 = _surfaceWidth - 20,
                    Y1 = y,
                    Y2 = y,
                    Stroke = defaultGridBrush,
                    StrokeThickness = 0.5
                };
                LabelsSurface.Children.Add(l);
                y -= ystep;
            }
        }

        private void CreateLabelsInRing(Dictionary<object, ArrangementInfo> arrangment, Dictionary<ArrangementInfo, TextBlock> labels)
        {
            foreach (var arr in arrangment.Values)
            {
                var label = labels[arr];
                if (label == null) continue;
                Canvas.SetLeft(label, arr.RegionCenter.X - label.ActualWidth / 2);
                Canvas.SetTop(label, arr.RegionCenter.Y - label.ActualHeight / 2);
                LabelsSurface.Children.Add(label);
            }
        }

        private void CreateLabelsAboveCircle(Dictionary<object, ArrangementInfo> arrangment, PieLabelArrange labelArrange, int key, bool allowWrapping)
        {
            foreach (var arr in arrangment.Values)
            {
                var name = arr.Name?.ToString() == "#" ? "Нет значения" : arr.Name?.ToString();
                var max = arr.OuterDiameter;
                var label = CreateLabel(name, max, allowWrapping);
                if (label == null) continue;
                label.UpdateLayout();
                Canvas.SetLeft(label, arr.RegionCenter.X - label.ActualWidth / 2);
                Canvas.SetTop(label, arr.RegionCenter.Y - label.ActualHeight - arr.OuterDiameter / 2);
                LabelsSurface.Children.Add(label);
                labelArrange.AddConstraint(new LabelConstraint
                {
                    x1 = arr.RegionCenter.X - label.ActualWidth / 2,
                    x2 = arr.RegionCenter.X + label.ActualWidth / 2,
                    y1 = arr.RegionCenter.Y - label.ActualHeight - arr.OuterDiameter / 2,
                    y2 = arr.RegionCenter.Y - arr.OuterDiameter / 2,
                    Key = key
                });
                key++;
            }
        }

        private TextBlock CreateLabel(string name, double max, bool allowWrapping)
        {
            if (name.StartsWith("#")) return null;
            var label = new TextBlock()
            {
                FontSize = LabelFontSize * GlobalFontCoefficient * 1.2,
                Text = name,
                Foreground = defaultForegroundBrush,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                TextWrapping = allowWrapping ? TextWrapping.Wrap : TextWrapping.NoWrap,
                Width = max,
                MaxWidth = max
            };
            label.MouseLeftButtonDown += LabelOnMouseLeftButtonDown;
            return label;
        }

        private void LabelOnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AxisClicked?.Invoke(this, new AxisEventArgs {Tag = "X"});
        }
    }
}
