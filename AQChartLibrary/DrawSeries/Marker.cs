﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using System.Linq;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private Shape DrawSingleMarker(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, object w, Panel surface)
        {
            switch (_sriX.SourceSeries.MarkerShapeMode)
            {
                case MarkerShapeModes.Bubble:
                    return DrawMarkerShapeBubble(markerSize, markerBrush, surfaceX, surfaceY, surface, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                case MarkerShapeModes.Range:
                    return DrawMarkerShapeRange(markerSize, markerBrush, surfaceX, surfaceY, surface, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                case MarkerShapeModes.C:
                    return DrawMarkerShapeC(markerSize, markerBrush, surfaceX, surfaceY, w, surface, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                case MarkerShapeModes.Clock:
                    return DrawMarkerShapeClock(markerSize, markerBrush, surfaceX, surfaceY, w, surface, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                case MarkerShapeModes.Multishape:
                    return DrawMarkerMultishape(markerSize, markerBrush, surfaceX, surfaceY, w, surface, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
            }
            return null;
        }

        private Panel DrawSingleMarkerForLegend(Size markerSize, Brushes markerBrush, double w, Panel surface, MarkerShapeModes msm, MarkerModes mm, string name)
        {
            var g = new Grid {Margin = new Thickness(3), HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Center};
            Shape s;
            switch (msm)
            {
                case MarkerShapeModes.Range:
                    s = DrawMarkerShapeRange(new Size(markerSize.Width / 4, markerSize.Width * w), markerBrush, 0, 0, g, mm, msm);
                    break;

                case MarkerShapeModes.C:
                    s = DrawMarkerShapeCTransformed(markerSize, markerBrush, 0, 0, g, w, mm, msm);
                    break;

                case MarkerShapeModes.Clock:
                    s = DrawMarkerShapeClockTransformed(markerSize, markerBrush, 0, 0, g, w, mm, msm);
                    break;

                case MarkerShapeModes.Multishape:
                    s = DrawMarkerMultishapeTransformed(markerSize, markerBrush, 0, 0, g, w, mm, msm);
                    break;

                default:
                    s = DrawMarkerShapeBubble(new Size(markerSize.Width * w, markerSize.Width * w), markerBrush, 0, 0, g, mm, msm);
                    break;
            }

            s.VerticalAlignment = VerticalAlignment.Center;

            if (name != null)
            {
                var tb = new TextBlock
                {
                    Text =  name,
                    MaxWidth = 120,
                    TextWrapping = TextWrapping.Wrap,
                    FontSize = 8 * GlobalFontCoefficient,
                    Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black),
                    Margin = new Thickness(3),
                    VerticalAlignment = VerticalAlignment.Center
                };
                surface.Children.Add(new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Left, Children = { g, tb }, Margin = new Thickness(0), VerticalAlignment = VerticalAlignment.Center});
            }
            else surface.Children.Add(g);

            return g;
        }

        private Shape DrawMarkerShapeC(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, object w, Panel surface, MarkerModes mm, MarkerShapeModes msm)
        {
            if (w == null) return DrawMarkerShapeBubble(markerSize, markerBrush, surfaceX, surfaceY, surface, mm, msm);
            var w1 = _serieForW?.BoundAxis.TransformValueMinMax(w, _serieForW);
            var e = DrawMarkerShapeCTransformed(markerSize, markerBrush, surfaceX, surfaceY, surface, w1, mm, msm);

            return e;
        }

        private Shape DrawMarkerShapeCTransformed(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, double? w1, MarkerModes mm, MarkerShapeModes msm)
        {
            Shape e;
            markerSize.Height = markerSize.Width;
            var internalShapeWidth = markerSize.Width;
            e = new Ellipse();
            e.Height = internalShapeWidth;
            e.Width = internalShapeWidth;
            SetBrushesToShape(markerSize, markerBrush, e, mm, msm);
            surface.Children.Add(e);

            double ang = 0;
            if (w1.HasValue && !Double.IsNaN(w1.Value)) ang = w1.Value*359.99;
            else ang = 0;

            var strokeThickness = e.StrokeThickness*3;

            var angleRad = 2*Math.PI*ang/360;
            var l = new ArcSegment
            {
                Size = new Size(markerSize.Width/2, markerSize.Height/2),
                IsLargeArc = angleRad > Math.PI,
                RotationAngle = ang,
                SweepDirection = SweepDirection.Clockwise,
                Point =
                    new Point(markerSize.Width/2 + markerSize.Width/2*Math.Sin(angleRad) + strokeThickness/2,
                        markerSize.Height/2 - markerSize.Height/2*Math.Cos(angleRad) + strokeThickness/2)
            };
            var a = new Path() {IsHitTestVisible = false};

            if (ang > 0)
            {
                PathFigure pathFigure = new PathFigure() {IsFilled = false, IsClosed = false};
                pathFigure.StartPoint = new Point(markerSize.Width/2 + strokeThickness/2, strokeThickness/2);
                pathFigure.Segments.Add(l);
                PathGeometry pathGeometry = new PathGeometry();
                pathGeometry.Figures = new PathFigureCollection();

                pathGeometry.Figures.Add(pathFigure);

                a.Data = pathGeometry;
                a.Height = markerSize.Width + strokeThickness;
                a.Width = markerSize.Height + strokeThickness;
                a.HorizontalAlignment = HorizontalAlignment.Center;
                a.VerticalAlignment = VerticalAlignment.Center;
                a.StrokeThickness = strokeThickness;
                a.StrokeEndLineCap = PenLineCap.Round;
                a.StrokeStartLineCap = PenLineCap.Round;
                surface.Children.Add(a);
                Canvas.SetLeft(a, surfaceX - a.Width/2);
                Canvas.SetTop(a, surfaceY - a.Height/2);
            }
            switch (mm)
            {
                case MarkerModes.Solid:
                    a.Stroke = e.Stroke;
                    if (e.Width > strokeThickness*2.2)
                    {
                        e.Width -= strokeThickness*2.2;
                        e.Height -= strokeThickness*2.2;
                    }
                    break;

                case MarkerModes.SolidBordered:
                    a.Stroke = e.Fill;
                    if (e.Width > strokeThickness*2.2)
                    {
                        e.Width -= strokeThickness*2.2;
                        e.Height -= strokeThickness*2.2;
                    }
                    break;

                case MarkerModes.Ring:
                    a.Stroke = e.Stroke;
                    break;

                case MarkerModes.SemiTransparent:
                    a.Stroke = new SolidColorBrush(((SolidColorBrush) e.Stroke).Color);
                    a.Stroke.Opacity = 0.7;
                    break;

                case MarkerModes.Transparent:
                    a.Stroke = new SolidColorBrush(((SolidColorBrush) e.Fill).Color);
                    a.Stroke.Opacity = 0.2;
                    break;

                case MarkerModes.Invisible:
                    a.Stroke = e.Fill;
                    break;
            }
            Canvas.SetLeft(e, surfaceX - e.Width/2);
            Canvas.SetTop(e, surfaceY - e.Height/2);
            return e;
        }
        
        private Shape DrawMarkerMultishape(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, object w, Panel surface, MarkerModes mm, MarkerShapeModes msm)
        {
            if (w == null) return DrawMarkerShapeBubble(markerSize, markerBrush, surfaceX, surfaceY, surface, mm, msm);
            var w1 = _markerW.IndexOf(w);
            var e = DrawMarkerMultishapeTransformed(markerSize, markerBrush, surfaceX, surfaceY, surface, w1, mm, msm);

            return e;
        }

        private Shape DrawMarkerMultishapeTransformed(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, double? w1, MarkerModes mm, MarkerShapeModes msm)
        {
            if (!w1.HasValue) return null;
            var sizeCoefficient = w1.Value <= 10 ? 1: 1 + Math.Ceiling(w1.Value / 10) * 0.2;
            var shapeNumber = (w1.Value % 10).ToString();
            var ms = new Size(markerSize.Width * sizeCoefficient, markerSize.Height * sizeCoefficient);
            switch (shapeNumber)
            {
                case "0":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 4, false);
                case "1":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 3, false);
                case "2":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 45, 4, false);
                default:
                case "3":
                    return DrawMarkerShapeBubble(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm);
                case "4":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 180, 3, false);
                case "5":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 5, false);
                case "6":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 8, true);
                case "7":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 12, true);
                case "8":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 45, 8, true);
                case "9":
                    return DrawMarkerShapePolygon(ms, markerBrush, surfaceX, surfaceY, surface, mm, msm, 0, 10, true);
                    
            }
        }

        private Shape DrawMarkerShapePolygon(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, MarkerModes mm, MarkerShapeModes msm, double startAngle, double edges, bool starred)
        {
            Path a;
            markerSize.Width = markerSize.Height;
            a = new Path();
            a.Height = markerSize.Height;
            a.Width = markerSize.Width;
            Canvas.SetLeft(a, surfaceX - markerSize.Width / 2);
            Canvas.SetTop(a, surfaceY - markerSize.Height / 2);
            SetBrushesToShape(markerSize, markerBrush, a, mm, msm);

            var startAngleRad = 2 * Math.PI * startAngle / 360;
            var stepAngle = 2 * Math.PI / edges;
            var R = markerSize.Width / 2;
            var r = R * 0.3;

            PathFigure pathFigure = new PathFigure() { IsFilled = true, IsClosed = true };
            pathFigure.StartPoint = new Point(markerSize.Width / 2 + markerSize.Width / 2 * Math.Sin(startAngleRad),
                            markerSize.Height / 2 - markerSize.Height / 2 * Math.Cos(startAngleRad));

            for (var i = 1; i<edges; i++)
            {
                var radius = (starred && i % 2 == 1) ? r : R;
                var l = new LineSegment
                {
                    Point = new Point(markerSize.Width/2 + radius*Math.Sin(startAngleRad + stepAngle*i),
                        markerSize.Height/2 - radius*Math.Cos(startAngleRad + stepAngle*i))
                };
                pathFigure.Segments.Add(l);
            }
            a.Data = new PathGeometry { Figures = new PathFigureCollection { pathFigure } };
            surface.Children.Add(a);
            return a;
        }

        private Shape DrawMarkerShapeBubble(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, MarkerModes mm, MarkerShapeModes msm)
        {
            if (double.IsNaN(surfaceX) || double.IsNaN(surfaceY)) return null;
            markerSize.Width = markerSize.Height;
            var e = new Ellipse
            {
                Height = markerSize.Height,
                Width = markerSize.Width
            };
            Canvas.SetLeft(e, surfaceX - markerSize.Width / 2);
            Canvas.SetTop(e, surfaceY - markerSize.Height / 2);
            SetBrushesToShape(markerSize, markerBrush, e, mm, msm);
            surface.Children.Add(e);
            return e;
        }

        private Shape DrawMarkerShapeClock(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, object w, Panel surface, MarkerModes mm, MarkerShapeModes msm)
        {
            if (w == null) return DrawMarkerShapeBubble(markerSize, markerBrush, surfaceX, surfaceY, surface, mm, msm);
            var w1 = _serieForW?.BoundAxis.TransformValueMinMax(w, _serieForW);
            return DrawMarkerShapeClockTransformed(markerSize, markerBrush, surfaceX, surfaceY, surface, w1, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
        }

        private Shape DrawMarkerShapeClockTransformed(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, double? w1, MarkerModes mm, MarkerShapeModes msm)
        {
            Shape e;
            markerSize.Height = markerSize.Width;
            e = new Ellipse
            {
                Height = markerSize.Height,
                Width = markerSize.Width
            };
            Canvas.SetLeft(e, surfaceX - markerSize.Width/2);
            Canvas.SetTop(e, surfaceY - markerSize.Height/2);
            SetBrushesToShape(markerSize, markerBrush, e, mm, msm);
            surface.Children.Add(e);
            double ang;
            if (w1.HasValue && !Double.IsNaN(w1.Value)) ang = w1.Value*359.99;
            else ang = 0;
            if (ang > 0)
            {
                var angleRad = 2*Math.PI*ang/360;
                var l = new ArcSegment
                {
                    Size = new Size(markerSize.Width/2, markerSize.Height/2),
                    IsLargeArc = angleRad > Math.PI,
                    RotationAngle = ang,
                    SweepDirection = SweepDirection.Clockwise,
                    Point =
                        new Point(markerSize.Width/2 + markerSize.Width/2*Math.Sin(angleRad),
                            markerSize.Height/2 - markerSize.Height/2*Math.Cos(angleRad))
                };

                var a = new Path() {IsHitTestVisible = false};
                PathFigure pathFigure = new PathFigure
                {
                    IsFilled = true,
                    IsClosed = true,
                    StartPoint = new Point(markerSize.Width/2, 0)
                };
                pathFigure.Segments.Add(l);
                pathFigure.Segments.Add(new LineSegment() {Point = new Point(markerSize.Width/2, markerSize.Height/2)});
                PathGeometry pathGeometry = new PathGeometry {Figures = new PathFigureCollection {pathFigure}};


                a.Data = pathGeometry;
                a.Height = markerSize.Width;
                a.Width = markerSize.Height;
                a.HorizontalAlignment = HorizontalAlignment.Center;
                a.VerticalAlignment = VerticalAlignment.Center;
                a.StrokeThickness = 0.5;
                surface.Children.Add(a);
                Canvas.SetLeft(a, surfaceX - markerSize.Width/2);
                Canvas.SetTop(a, surfaceY - markerSize.Height/2);

                switch (mm)
                {
                    case MarkerModes.Solid:
                        a.Stroke = markerBrush.strokeBrush;
                        a.Fill = new SolidColorBrush(Colors.White);
                        break;

                    case MarkerModes.SolidBordered:
                        a.Stroke = new SolidColorBrush(Colors.White);
                        a.Fill = new SolidColorBrush(Colors.White);
                        a.StrokeThickness = 1;
                        break;

                    case MarkerModes.Ring:
                        a.Stroke = markerBrush.strokeBrush;
                        a.Fill = markerBrush.strokeBrush;
                        break;

                    case MarkerModes.SemiTransparent:
                        a.Stroke = a.Stroke;
                        a.Fill = markerBrush.strokeBrush;
                        break;

                    case MarkerModes.Transparent:
                        a.Fill = markerBrush.strokeBrush;
                        a.Stroke = new SolidColorBrush(Colors.White);
                        break;

                    case MarkerModes.Invisible:
                        var brush2 = markerBrush.strokeBrush;
                        brush2.Opacity = 0;
                        a.Fill = brush2;
                        a.Stroke = markerBrush.strokeBrush;
                        break;
                }
            }

            return e;
        }

        private Shape DrawMarkerShapeRange(Size markerSize, Brushes markerBrush, double surfaceX, double surfaceY, Panel surface, MarkerModes mm, MarkerShapeModes msm)
        {
            Shape e = new Rectangle();
            e.Height = markerSize.Height;
            e.Width = markerSize.Width;
            Canvas.SetLeft(e, surfaceX - markerSize.Width / 2);
            Canvas.SetTop(e, surfaceY - markerSize.Height / 2);
            SetBrushesToShape(markerSize, markerBrush, e, mm, msm);
            surface.Children.Add(e);

            var l = new Ellipse
            {
                Height = markerSize.Width * 0.7,
                Width = markerSize.Width * 0.7,
                Stroke = markerBrush.strokeBrush,
                Fill = markerBrush.fillBrush
            };
            surface.Children.Add(l);
            Canvas.SetLeft(l, surfaceX - markerSize.Width * 0.7 / 2);
            Canvas.SetTop(l, surfaceY - markerSize.Width * 0.7 / 2);
            return e;
        }
        private delegate void SetBrushesDelegate(Size markerSize, Brushes markerBrush, Shape e, MarkerShapeModes msm);

        private readonly Dictionary<MarkerModes, SetBrushesDelegate> _setBrushesActions = new Dictionary<MarkerModes, SetBrushesDelegate>
        {
            {
                MarkerModes.Solid, (markerSize, markerBrush, e, msm)=>
                {
                    e.Stroke = markerBrush.strokeBrush;
                    e.Fill = markerBrush.fillBrush;
                }
            },
            {
                MarkerModes.SolidBordered, (markerSize, markerBrush, e, msm)=>
                {
                    e.Stroke = new SolidColorBrush(Colors.White);
                    e.Fill = markerBrush.fillBrush;
                }
            },
            {
                MarkerModes.Ring, (markerSize, markerBrush, e, msm)=>
                {
                    if (markerSize.Width < 0.01) return;
                    markerBrush.strokeBrush.Opacity = 0.8;
                    e.Stroke = markerBrush.strokeBrush;
                    e.StrokeThickness = msm != MarkerShapeModes.Range
                        ? Math.Log(markerSize.Width, 20.0d)
                        : 1;
                }
            },
            {
                MarkerModes.SemiTransparent, (markerSize, markerBrush, e, msm)=>
                {
                    e.Stroke = markerBrush.strokeBrush;
                    e.Fill = markerBrush.fillBrush;
                    e.Fill.Opacity = 0.4;
                    e.Stroke.Opacity = 0.7;
                }
            },
            {
                MarkerModes.Transparent, (markerSize, markerBrush, e, msm)=>
                {
                    e.Stroke = markerBrush.strokeBrush;
                    e.Stroke.Opacity = 0.2;
                    e.Fill = markerBrush.fillBrush;
                }
            },
            {
                MarkerModes.Invisible, (markerSize, markerBrush, e, msm)=>
                {
                    e.StrokeThickness = 0;
                    e.Fill = markerBrush.fillBrush;
                    e.Fill.Opacity = 0;
                }
            },
        };

        private void SetBrushesToShape(Size markerSize, Brushes markerBrush, Shape e, MarkerModes mm, MarkerShapeModes msm)
        {
            _setBrushesActions[MarkerModes.Solid].Invoke(markerSize, markerBrush, e, msm);
            _setBrushesActions[mm].Invoke(markerSize, markerBrush, e, msm);
        }

        private readonly Dictionary<MarkerSizeModes, double> _markerK = new Dictionary<MarkerSizeModes, double> { { MarkerSizeModes.Small, 0.5 }, { MarkerSizeModes.Large, 1.44 }, { MarkerSizeModes.SmallMedium, 0.7 }, { MarkerSizeModes.ExtraLarge, 2.0 }, { MarkerSizeModes.MediumLarge, 1.25 }, { MarkerSizeModes.Medium, 1.0 } };

        private Size GetMarkerSize(int i)
        {
            var markersize = _sriX.SourceSeries.MarkerSize * _markerK[_sriX.SourceSeries.MarkerSizeMode];
            var markerwidth = markersize;

            var userMarkerSize = (_markerSizes != null && _markerSizes.Count == _nX) ? _markerSizes[i] : null;
            if (userMarkerSize is double && (double)userMarkerSize >= 0.0d) markersize = (double)userMarkerSize;
            if (_sriW != null)
            {
                switch(_sriX.SourceSeries.MarkerShapeMode) 
                {
                    case MarkerShapeModes.Bubble:
                    default:
                        var w = _sriW.BoundAxis.TransformValue(_sourceW[i], _sriW);
                        if (w.HasValue && !Double.IsNaN(w.Value)) markersize += w.Value * markersize * markersize;
                        else markersize = 1;
                        break;

                    case MarkerShapeModes.Multishape:
                        markerwidth *= 2;
                        markersize = markerwidth;
                        break;

                    case MarkerShapeModes.Clock:
                        markerwidth *= 2;
                        markersize = markerwidth;
                        break;

                    case MarkerShapeModes.C:
                        markerwidth *= 3;
                        markersize = markerwidth;
                        break;

                    case MarkerShapeModes.Range:
                        if (!(_sriY.BoundAxis.DisplayMinimum is double))
                        {
                            markersize = 1;
                            break;
                        }
                        var min = (double)_sriY.BoundAxis.DisplayMinimum;
                        var max = (double)_sriY.BoundAxis.DisplayMaximum;
                        if (_sourceW[i] is double) markersize = _surfaceHeight * (double)_sourceW[i] / (max - min);
                        else markersize = 1;
                        break;
                }
            }
            var markerheight = markersize;
            return new Size { Height = markerheight, Width = markerwidth };
        }
        
        private static readonly Dictionary<MarkerSizeModes, double> MarkerSizes = new Dictionary<MarkerSizeModes, double>
        {
            { MarkerSizeModes.Small, 0.25},
            { MarkerSizeModes.SmallMedium, 0.37},
            { MarkerSizeModes.Medium, 0.5},
            { MarkerSizeModes.MediumLarge, 0.75},
            { MarkerSizeModes.Large, 1.0},
            { MarkerSizeModes.ExtraLarge, 1.3},
        };

        private double GetMarkerSizeCoefficient(SeriesRangeInfo sri1)
        {
            return sri1 == null ? 0.5 : MarkerSizes[sri1.SourceSeries.MarkerSizeMode];
        }

        private Size GetMarkerSizeForGroups()
        {
            var markersize = _sriX.SourceSeries.MarkerSize;
            var markerwidth = markersize;
            markersize *= _groupMarkerSizeK[_sriX.SourceSeries.MarkerSizeMode];
            return new Size { Height = markersize, Width = markerwidth };
        }

        private readonly Dictionary<MarkerSizeModes, double> _groupMarkerSizeK = new Dictionary<MarkerSizeModes, double>
        {
            {MarkerSizeModes.Small, 0.5 },
            {MarkerSizeModes.SmallMedium, 0.7 },
            {MarkerSizeModes.Large, 1.44 },
            {MarkerSizeModes.ExtraLarge, 2 },
            { MarkerSizeModes.MediumLarge, 1.25},
            {MarkerSizeModes.Medium,1 }
        };


        private InteractivityIndex SetMarkerInteractivity(int i, Shape e)
        {
            var result = new InteractivityIndex {Index = i, SeriesName = _sriX.SeriesName};
            var tt = new ToolTip { Tag = result};
            tt.Opened += ToolTip_Opened;
            e.Tag = tt.Tag;
            ToolTipService.SetToolTip(e, tt);
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
            return result;
        }
    }
}