﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {

        private void DrawVoronoy()
        {
            var vertices = GetVertices();
            var triangles = new Triangulation(vertices, new Point( - _surfaceWidth, - _surfaceHeight), new Size(_surfaceWidth * 3, _surfaceHeight * 3)).GetTriangles(false);
            var m = triangles.Count;
            var strokeOpacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
            var fillOpacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            var isWhiteStroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered;
            var strokeSize = GetMarkerSizeCoefficient(_sriX);

            foreach (var v in vertices)
            {
                var points = v.GetVoronoy();
                if (points == null || points.Count == 0) continue;
                var pf0 = new PathFigure { IsClosed = true, IsFilled = true };
                pf0.StartPoint = points[0];
                for (int i = 1; i < points.Count; i++)
                {
                    pf0.Segments.Add(new LineSegment { Point = points[i] });
                }
                var path0 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry {Figures = new PathFigureCollection {pf0}},
                    IsHitTestVisible = false
                };
                var g0 = new SolidColorBrush(v.Color);
                var gs0 = new SolidColorBrush(v.Color);
                path0.Fill = g0;
                path0.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : gs0;
                path0.Fill.Opacity = fillOpacity;
                path0.Stroke.Opacity = strokeOpacity;
                path0.Tag = m;
                SeriesSurface.Children.Add(path0);
            }
        }
    }
}