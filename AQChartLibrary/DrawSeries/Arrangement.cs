﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQChartLibrary
{
    public partial class Chart
    {
        public Dictionary<object, ArrangementInfo> CreateArrangement(Size windowSize, List<object> xObjects, ArrangementMode mode)
        {
            Dictionary<object, ArrangementInfo> _result = new Dictionary<object, ArrangementInfo>();

            switch (mode)
            {
                case ArrangementMode.Uniform:
                    var regions = GetOptimalCompositionForRegularGrid(xObjects.Count, windowSize);
                    var row = 0;
                    var col = 0;
                    for (int i = 0; i < xObjects.Count; i++)
                    {
                        var halign = (windowSize.Width - regions.xsize * regions.cols) / 2;
                        var valign = (windowSize.Height - regions.ysize * regions.rows) / 2;

                        _result.Add(xObjects[i] ?? "#", new ArrangementInfo
                        {
                            RegionCenter = new Point(halign + regions.xsize / 2 + col * regions.xsize, valign + regions.ysize / 2 + row * regions.ysize),
                            Name = xObjects[i],
                            OuterDiameter = regions.xsize / 1.5,
                            InnerDiameter = 0,
                        });

                        col++;

                        if (col >= regions.cols)
                        {
                            col = 0;
                            row++;
                        }
                    }
                    break;

                case ArrangementMode.Chess:
                    var regions2 = GetOptimalCompositionForChess(xObjects.Count, windowSize);
                    var row2 = 0;
                    var col2 = 0;

                    for (int i = 0; i < xObjects.Count; i++)
                    {
                        var halign = (windowSize.Width - regions2.xsize * regions2.cols) / 2 ;
                        var valign = (windowSize.Height - regions2.ysize * (regions2.rows -1 ) * 0.866 - regions2.ysize) / 2;
                        if (row2 % 2 == 1)
                        {
                            halign += regions2.xsize / 2;
                        }

                        _result.Add(xObjects[i] ?? "#", new ArrangementInfo
                        {
                            RegionCenter = new Point(halign + regions2.xsize / 2 + col2 * regions2.xsize, valign + regions2.ysize / 2 + row2 * regions2.ysize*0.866 ),
                            Name = xObjects[i],
                            OuterDiameter = regions2.xsize / 1.5,
                            InnerDiameter = 0,
                        });

                        col2++;

                        if (col2 >= regions2.cols - row2 % 2)
                        {
                            col2 = 0;
                            row2++;
                        }
                    }
                    break;

                case ArrangementMode.Line:
                    var line = new ArrangementRegions {cols = xObjects.Count, rows = 1, xsize = Math.Min(windowSize.Width / xObjects.Count, windowSize.Height)};
                    line.ysize = line.xsize;
                    var col1 = 0;
                    for (int i = 0; i < xObjects.Count; i++)
                    {
                        var halign = (windowSize.Width - line.xsize * line.cols) / 2;
                        var valign = (windowSize.Height - line.ysize * line.rows) / 2;

                        _result.Add(xObjects[i] ?? "#", new ArrangementInfo
                        {
                            RegionCenter = new Point(halign + line.xsize / 2 + col1 * line.xsize, valign +  line.ysize / 2),
                            Name = xObjects[i],
                            OuterDiameter = line.xsize / 1.5,
                            InnerDiameter = 0,
                        });
                        col1++;
                    }
                    break;

                case ArrangementMode.Concentric:
                    var ring = new ArrangementRegions { cols = 1, rows = 1, xsize = Math.Min(windowSize.Width, windowSize.Height) };
                    ring.ysize = ring.xsize;
                    var maxradius = ring.xsize / 1.1;
                    var radiusstep = maxradius / xObjects.Count;
                    var innerradius = maxradius - radiusstep;
                    var outerradius = maxradius;
                    for (int i = xObjects.Count - 1; i >=0; i--)
                    {
                        var halign = (windowSize.Width - ring.xsize * ring.cols) / 2;
                        var valign = (windowSize.Height - ring.ysize * ring.rows) / 2;

                        _result.Add(xObjects[i] ?? "#", new ArrangementInfo
                        {
                            RegionCenter = new Point(windowSize.Width / 2, windowSize.Height / 2),
                            Name = xObjects[i],
                            OuterDiameter = outerradius,
                            InnerDiameter = innerradius,
                        });
                        innerradius -= radiusstep;
                        outerradius -= radiusstep;
                    }
                    break;
            }
            return _result;
        }

        private ArrangementRegions GetOptimalCompositionForRegularGrid(double n, Size windowSize)
        {
            var A = windowSize.Width;
            var B = windowSize.Height;
            Dictionary<int, ArrangementRegions> S = new Dictionary<int, ArrangementRegions>();

            for (int r = 1; r <= n; r++)
            {
                var c = Math.Ceiling(n / r);
                var x = Math.Min(A / c, B / r);
                var free = A * B - x * x * n;
                S.Add(r,new ArrangementRegions {cols = (int)c, rows = r, free = free, xsize = x, ysize = x});
            }
            var mins = S.Values.OfType<ArrangementRegions>().Min(a=>a.free);
            return S.Where(a => a.Value.free == mins).First().Value;
        }

        private ArrangementRegions GetOptimalCompositionForChess(double n, Size windowSize)
        {
            if (n <= 2) return GetOptimalCompositionForRegularGrid(n, windowSize);
            var A = windowSize.Width;
            var B = windowSize.Height;
            Dictionary<int, ArrangementRegions> S = new Dictionary<int, ArrangementRegions>();

            for (int r = 1; r <= n; r++)
            {
                var c = Math.Ceiling(n / r + 0.5);
                var x = Math.Min(A / c, B / (r * 0.866));
                var free = A * B - x * x * 0.866 * n;
                S.Add(r, new ArrangementRegions { cols = (int)c, rows = r, free = free, xsize = x, ysize = x });
            }
            var mins = S.Values.OfType<ArrangementRegions>().Min(a => a.free);
            return S.Where(a => a.Value.free == mins).First().Value;
        }

        private class ArrangementRegions
        {
            public int rows;
            public int cols;
            public double xsize;
            public double ysize;
            public double free;
        }
    }


    public class ArrangementInfo
    {
        public object Name;
        public Point RegionCenter;
        public double InnerDiameter;
        public double OuterDiameter;
    }

    public enum ArrangementMode
    {
        Line,
        Chess,
        Uniform,
        Concentric
    }
}
