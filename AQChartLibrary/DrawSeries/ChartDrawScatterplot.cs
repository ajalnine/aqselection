﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawScatterplot()
        {
            if (_sriX == null || _sriY == null) return;
            var layerBrushes = GetLayerBrushes();
            if (_sriX.SourceSeries.Options != null && _sriX.SourceSeries.Options.Contains("GroupProcessing"))
            {
                DrawLineInGroups(false, !_sriX.SourceSeries.Options.Contains("GroupProcessingY"), layerBrushes);
                DrawMarkersInGroups(!_sriX.SourceSeries.Options.Contains("GroupProcessingY"), layerBrushes);
            }
            else
            {
                if (_nX < 25000)
                {
                    DrawLine(false);
                    DrawMarkers(layerBrushes);
                    DrawLabels(layerBrushes);
                }
                else DrawMarkersGeometry(layerBrushes);
            }
        }

        private void DrawLine(bool noFill)
        {
            if (_sriX == null || _sriY == null || Math.Abs(_sriX.SourceSeries.LineSize) < double.Epsilon) return;
            Brushes brush = GetDefaultBrushes();
            var firstRegionPoint = new Point();
            var previousPoint = new Point();
            var first = true;
            var th = (_sriX.SourceSeries.LineSize > 1) ? _sriX.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < double.Epsilon) return;

            var dc = new DoubleCollection();
            if (_sriX.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1 / _sriX.SourceSeries.LineSize);
            }

            var pathData = new PathGeometry();
            var filled = _sriX.SourceSeries.FillColor != null && !noFill;
            double surfaceX, surfaceY;
            double? x, y;
            PathFigure pf = new PathFigure { IsClosed = filled, IsFilled = filled };
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(
                    _sourceY[i] is GroupDescription 
                        ? ((GroupDescription)_sourceY[i])?.Center
                        : _sourceY[i]
                    , _sriY);

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value))
                {
                    first = true;
                    if (!filled) continue;
                    pf.Segments.Add(new LineSegment { Point = new Point(previousPoint.X,    _surfaceHeight) });
                    pf.Segments.Add(new LineSegment { Point = new Point(firstRegionPoint.X, _surfaceHeight) });
                    pf = new PathFigure { IsClosed = true, IsFilled = true };
                    pathData.Figures.Add(pf);
                }
                else
                {
                    surfaceX = _surfaceWidth * x.Value;
                    surfaceY =_surfaceHeight - _surfaceHeight * y.Value;

                    if (first)
                    {
                        pf = new PathFigure { IsClosed = filled, IsFilled = filled };
                        pathData.Figures.Add(pf);
                        firstRegionPoint = new Point(surfaceX, surfaceY);
                    }
                    if (brush != null)
                    {
                        if (!_sriX.SourceSeries.IsSmoothed)
                        {
                            if (first) pf.StartPoint = new Point(surfaceX, surfaceY);
                            else pf.Segments.Add(new LineSegment{Point = new Point(surfaceX, surfaceY)});
                        }
                        else
                        {
                            if (first)pf.StartPoint = new Point(surfaceX, surfaceY);
                            else pf.Segments.Add(new PolyQuadraticBezierSegment{Points = new PointCollection
                            {
                                new Point((surfaceX + previousPoint.X) / 2 ,(surfaceY + previousPoint.Y) / 2),
                                new Point(surfaceX, surfaceY),
                            }}); 
                        }
                    }
                    first = false;
                    previousPoint.X = surfaceX;
                    previousPoint.Y = surfaceY;
                }
            }
            if (filled)
            {
                pf = pathData.Figures.Last(); 
                pf.Segments.Add(new LineSegment { Point = new Point(previousPoint.X, _surfaceHeight) });
                pf.Segments.Add(new LineSegment { Point = new Point(firstRegionPoint.X, _surfaceHeight) });
            }
            
            var p = new Path
            {
                Stroke = brush.strokeBrush,
                Fill = brush.fillBrush,
                StrokeThickness = th,
                StrokeDashArray = dc,
                StrokeLineJoin = PenLineJoin.Round,
                Data = pathData
            };

            SeriesSurface.Children.Add(p);
        }

        private void DrawMarkers(Dictionary<object, Brushes> layerBrushes)
        {
            if (_sriX == null || _sriY == null) return;
            double? x, y;
            Size markerSize;
            object sw;
            Shape marker;
            Brushes markerBrushes;
            double surfaceX, surfaceY;
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                surfaceX = _surfaceWidth * x.Value;
                surfaceY = _surfaceHeight - _surfaceHeight * y.Value;
                markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }  
                    : layerBrushes[_sourceZ?[i] ?? "#"];

                markerSize =  GetMarkerSize(i);

                sw = _sriX.SourceSeries.WValueForShape ?? _sourceW?[i];
                marker = DrawSingleMarker(markerSize, markerBrushes, surfaceX, surfaceY, sw, SeriesSurface);
                var ii = SetMarkerInteractivity(i, marker);
                AppendSelectable(new Point(surfaceX, surfaceY), new Size(0,0), marker, ii);
            }
        }

        private void DrawMarkersGeometry(Dictionary<object, Brushes> layerBrushes)
        {
            if (_sriX == null || _sriY == null) return;
            double? x, y;
            double surfaceX, surfaceY;

            var side = _sriX.SourceSeries.MarkerSize / 2 * GetMarkerSizeCoefficient(_sriX);

            var size = new Size(side, side);
            var size2 = new Size(_sriX.SourceSeries.MarkerSize, _sriX.SourceSeries.MarkerSize);
            Brushes markerBrushes;
            var pathData = new PathGeometry();
            if (_sourceZ == null)
            {
                markerBrushes = GetDefaultBrushes();
                for (var i = 0; i < _nX; i++)
                {
                    x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                    if (!x.HasValue || double.IsNaN(x.Value)) continue;
                    y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                    if (!y.HasValue || double.IsNaN(y.Value)) continue;
                    surfaceX = _surfaceWidth * x.Value;
                    surfaceY = _surfaceHeight - _surfaceHeight * y.Value;
                    pathData.Figures.Add(new PathFigure
                    {
                        IsClosed = true,
                        IsFilled = true,
                        StartPoint = new Point(surfaceX - side, surfaceY),
                        Segments =  {new LineSegment { Point = new Point(surfaceX, surfaceY - side) },
                                new LineSegment { Point = new Point(surfaceX + side, surfaceY) },
                                new LineSegment { Point = new Point(surfaceX, surfaceY + side) }}
                    });
                    AppendSelectable(new Point(surfaceX, surfaceY), new Size(0, 0), null, new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName });
                    if (i % 20 == 0 || i == _nX - 1)
                    {
                        var p = new Path
                        {
                            Stroke = markerBrushes.strokeBrush,
                            Fill = markerBrushes.fillBrush,
                            StrokeThickness = 1,
                            Data = pathData
                        };
                        SetBrushesToShape(size, markerBrushes, p, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                        SeriesSurface.Children.Add(p);
                        pathData = new PathGeometry();
                    }
                }
            }
            else
            {
                var layers = layerBrushes.Keys.ToList();
                
                foreach (var layer in layers)
                {
                    var data = new List<Point>();
                    for (int i = 0; i < _sourceX.Count; i++)
                    {
                        if (_sourceX[i] ==null || _sourceY[i] ==null || _sourceZ[i]?.ToString() != layer?.ToString()) continue;
                        data.Add(new Point(
                            _surfaceWidth * (double)_sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX)
                            , _surfaceHeight - _surfaceHeight * (double)_sriY.BoundAxis.TransformValue(_sourceY[i], _sriY)));
                    }

                    markerBrushes =  layerBrushes[layer ?? "#"];

                    for (var i = 0; i < data.Count; i++)
                    {
                        surfaceX = data[i].X;
                        surfaceY = data[i].Y;
                        pathData.Figures.Add(new PathFigure
                        {
                            IsClosed = true,
                            IsFilled = true,
                            StartPoint = new Point(surfaceX - side, surfaceY),
                            Segments =  {new LineSegment { Point = new Point(surfaceX, surfaceY - side) },
                                new LineSegment { Point = new Point(surfaceX + side, surfaceY) },
                                new LineSegment { Point = new Point(surfaceX, surfaceY + side) }}
                        });
                        AppendSelectable(new Point(surfaceX, surfaceY), new Size(0, 0), null, new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName });
                        if (i % 20 == 0 || i == data.Count - 1)
                        {
                            var p = new Path
                            {
                                Stroke = markerBrushes.strokeBrush,
                                Fill = markerBrushes.fillBrush,
                                StrokeThickness = 1,
                                Data = pathData
                            };
                            SetBrushesToShape(size, markerBrushes, p, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                            SeriesSurface.Children.Add(p);
                            pathData = new PathGeometry();
                        }
                    }
                }
            }
        }
        
        private void DrawLineInGroups(bool noFill, bool onX, Dictionary<object, Brushes> layerBrushes)
        {
            if (_sriX == null || _sriY == null) return;
            var brush = GetDefaultBrushes();
            var categoryNumber = (double)_sriX.SeriesMarkerSizes[0];
            var st = onX ?  1/_sriX.BoundAxis.RealSteps : 1 / _sriY.BoundAxis.RealSteps;
            var singleColumnSize = st / categoryNumber; // Ширина одного столбца
            var k = _sriX.SourceSeries.MarkerSizeMode == MarkerSizeModes.Small ? 0.15 : 0;


            var firstRegionPoint = new Point();
            var previousPoint = new Point();
            var first = true;
            var th = (_sriX.SourceSeries.LineSize > 1) ? _sriX.SourceSeries.LineSize : 1;
            if (Math.Abs(th) < Double.Epsilon) return;

            var dc = new DoubleCollection();
            if (_sriX.SourceSeries.LineSize < 1)
            {
                dc.Add(1);
                dc.Add(1 / _sriX.SourceSeries.LineSize);
            }

            var pathData = new PathGeometry();
            var filled = _sriX.SourceSeries.FillColor != null && !noFill;

            double? x, y, w;
            double surfaceX, surfaceY;
            PathFigure pf;
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                w = (double?)_sourceV?[i];
                if (w < 0) continue;
                if (onX) x += st * (w - 0.5) + (k + 0.5) * singleColumnSize;
                else y += st * (w - 0.5) + (k + 0.5) * singleColumnSize;

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value))
                {
                    first = true;
                    if (!filled) continue;
                    pf = pathData.Figures.Last();
                    pf.Segments.Add(new LineSegment { Point = new Point(previousPoint.X, _surfaceHeight) });
                    pf.Segments.Add(new LineSegment { Point = new Point(firstRegionPoint.X, _surfaceHeight) });
                    pathData.Figures.Add(new PathFigure { IsClosed = filled, IsFilled = filled });
                }
                else
                {
                    surfaceX = Math.Round(_surfaceWidth * x.Value, 1);
                    surfaceY = Math.Round(_surfaceHeight - _surfaceHeight * y.Value, 1);

                    if (first)
                    {
                        pathData.Figures.Add(new PathFigure { IsClosed = filled, IsFilled = filled });
                        firstRegionPoint = new Point(surfaceX, surfaceY);
                    }
                    if (brush != null)
                    {
                        pf = pathData.Figures.Last();
                        if (!_sriX.SourceSeries.IsSmoothed)
                        {
                            if (first) pf.StartPoint = new Point(surfaceX, surfaceY);
                            else pf.Segments.Add(new LineSegment { Point = new Point(surfaceX, surfaceY) });
                        }
                        else
                        {
                            if (first) pf.StartPoint = new Point(surfaceX, surfaceY);
                            else
                                pf.Segments.Add(new PolyQuadraticBezierSegment
                                {
                                    Points = new PointCollection
                            {
                                new Point((surfaceX + previousPoint.X) / 2 ,(surfaceY + previousPoint.Y) / 2),
                                new Point(surfaceX, surfaceY),
                            }
                                });
                        }
                    }
                    first = false;
                    previousPoint.X = surfaceX;
                    previousPoint.Y = surfaceY;
                }
            }
            if (filled)
            {
                pf = pathData.Figures.Last();
                pf.Segments.Add(new LineSegment { Point = new Point(previousPoint.X, _surfaceHeight) });
                pf.Segments.Add(new LineSegment { Point = new Point(firstRegionPoint.X, _surfaceHeight) });
            }

            var p = new Path
            {
                Stroke = brush.strokeBrush,
                Fill = brush.fillBrush,
                StrokeThickness = th,
                StrokeDashArray = dc,
                StrokeLineJoin = PenLineJoin.Round,
                Data = pathData
            };

            SeriesSurface.Children.Add(p);
        }

        private void DrawMarkersInGroups(bool onX, Dictionary<object, Brushes> layerBrushes)
        {
            if (_sriX == null || _sriY == null) return;
            var categoryNumber = (double)_sriX.SeriesMarkerSizes[0];
            var st = onX ? 1 / _sriX.BoundAxis.RealSteps : 1 / _sriY.BoundAxis.RealSteps;
            var singleColumnSize = st / categoryNumber; // Ширина одного столбца
            var k = _sriX.SourceSeries.MarkerSizeMode == MarkerSizeModes.Small ? 0.15 : 0;

            double? x, y, w;
            double surfaceX, surfaceY;
            Size markerSize;
            Brushes markerBrushes;
            object sw;
            Shape marker;
            
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                w = (double?)_sourceV?[i];
                if (w < 0) continue;
                if (onX)  x += st * (w - 0.5) + (k + 0.5) * singleColumnSize;
                else y += st * (w - 0.5) + (k + 0.5) * singleColumnSize;

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                surfaceX = Math.Round(_surfaceWidth * x.Value, 1);
                surfaceY = Math.Round(_surfaceHeight - _surfaceHeight * y.Value, 1);
                markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                markerSize = GetMarkerSizeForGroups();
                sw = _sriX.SourceSeries.WValueForShape ?? _sourceW?[i];
                marker = DrawSingleMarker(markerSize, markerBrushes, surfaceX, surfaceY, sw, SeriesSurface);
                SetMarkerInteractivity(i, marker);
            }
        }

        private void DrawLabels(Dictionary<object, Brushes> layerBrushes)
        {
            if (_sriX == null || _sriY == null || (_sriV == null && _markerLabels  == null)) return; 
            string userLabel, sourceLabel, labelText;
            double? x, y;
            double surfaceX, surfaceY;
            Size markersize;
            Brushes markerBrushes;
            for (var i = 0; i < _nX; i++)
            {
                userLabel = (_markerLabels != null && _markerLabels.Count == _nX && _markerLabels[i] != null) ? _markerLabels[i].ToString() : null;
                sourceLabel = (_sourceV?[i] != null) ? _sourceV[i].ToString() : null;
                labelText = (!string.IsNullOrEmpty(userLabel)) ? userLabel : sourceLabel;
                
                if (string.IsNullOrEmpty(labelText)) continue;

                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);

                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;

                surfaceX = Math.Round(_surfaceWidth * x.Value, 1);
                surfaceY = Math.Round(_surfaceHeight - _surfaceHeight * y.Value, 1);
                markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                markersize = GetMarkerSize(i);
                DrawSingleLabel(labelText, markerBrushes.strokeBrush, markersize, surfaceX, surfaceY, LabelFontSize);
            }
        }
   }
}