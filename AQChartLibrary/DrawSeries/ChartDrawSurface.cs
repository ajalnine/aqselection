﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {

        private void DrawSurface()
        {
            var triangles = new Triangulation(GetVertices(), new Point(0, 0), new Size(_surfaceWidth, _surfaceHeight)).GetTriangles(_sriX.SourceSeries.Options?.Contains("Cutted") ?? false);
            if (!ZAxisRange.IsDiscrete && !ZAxisRange.IsNominal) RenderTriangles(triangles);
            else RenderDiscreteTriangles(triangles);
        }
                
        private List<Vertice> GetVertices()
        {
            if (_sriX == null || _sriY == null) return null;

            var layerBrushes = GetLayerBrushes();
            double? x, y;
            double surfaceX, surfaceY;
            var result = new List<Vertice>();
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;
                surfaceX = _surfaceWidth * x.Value;
                surfaceY = _surfaceHeight - _surfaceHeight * y.Value;
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                result.Add(new Vertice {X = surfaceX, Y = surfaceY, Color = ((SolidColorBrush)markerBrushes.strokeBrush).Color, SuperStructure = false });
            }
            return result;
        }

        private void RenderTriangles(List<Triangle> triangles)
        {
            var strokeOpacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
            var fillOpacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            var isWhiteStroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered;
            var strokeSize = GetMarkerSizeCoefficient(_sriX);

            foreach (var t in triangles)
            {
                var endColor = Color.FromArgb(0,
                    Convert.ToByte(((int)t.A.Color.R + (int)t.B.Color.R + (int)t.C.Color.R) / 3),
                    Convert.ToByte(((int)t.A.Color.G + (int)t.B.Color.G + (int)t.C.Color.G) / 3),
                    Convert.ToByte(((int)t.A.Color.B + (int)t.B.Color.B + (int)t.C.Color.B) / 3));
                var substrateColor = Color.FromArgb(0xff,
                    Convert.ToByte(((int)t.A.Color.R + (int)t.B.Color.R + (int)t.C.Color.R) / 3),
                    Convert.ToByte(((int)t.A.Color.G + (int)t.B.Color.G + (int)t.C.Color.G) / 3),
                    Convert.ToByte(((int)t.A.Color.B + (int)t.B.Color.B + (int)t.C.Color.B) / 3));

                var endPoint = new Point
                (
                    (t.A.X + t.B.X + t.C.X) / 3,
                    (t.A.Y + t.B.Y + t.C.Y) / 3
                );

                if (fillOpacity > 0)
                {
                    var pf0 = new PathFigure { IsClosed = true, IsFilled = true };
                    pf0.StartPoint = new Point(t.A.X, t.A.Y);
                    pf0.Segments.Add(new LineSegment { Point = new Point(t.B.X, t.B.Y) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(t.C.X, t.C.Y) });
                    var path0 = new Path
                    {
                        StrokeThickness = strokeSize,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf0 } },
                        IsHitTestVisible = false
                    };
                    var g0 = new SolidColorBrush(substrateColor);
                    var gs0 = new SolidColorBrush(substrateColor);
                    path0.Fill = g0;
                    path0.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : gs0;
                    path0.Fill.Opacity = fillOpacity;
                    path0.Stroke.Opacity = strokeOpacity;
                    SeriesSurface.Children.Add(path0);
                }

                var pf1 = new PathFigure { IsClosed = true, IsFilled = true };
                pf1.StartPoint = new Point(t.A.X, t.A.Y);
                pf1.Segments.Add(new LineSegment { Point = new Point(t.B.X, t.B.Y) });
                pf1.Segments.Add(new LineSegment { Point = new Point(t.C.X, t.C.Y) });
                var path1 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf1 } },
                    IsHitTestVisible = false
                };
                var g1 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.A.X, t.A.Y), EndPoint = endPoint };
                g1.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.A.Color }, new GradientStop { Offset = 1, Color = endColor } };
                var gs1 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.A.X, t.A.Y), EndPoint = endPoint };
                gs1.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.A.Color }, new GradientStop { Offset = 1, Color = endColor } };
                path1.Fill = g1;
                path1.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : (Brush)gs1;
                path1.Fill.Opacity = fillOpacity;
                path1.Stroke.Opacity = strokeOpacity;

                SeriesSurface.Children.Add(path1);

                var pf2 = new PathFigure { IsClosed = true, IsFilled = true };
                pf2.StartPoint = new Point(t.A.X, t.A.Y);
                pf2.Segments.Add(new LineSegment { Point = new Point(t.B.X, t.B.Y) });
                pf2.Segments.Add(new LineSegment { Point = new Point(t.C.X, t.C.Y) });
                var path2 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf2 } },
                    IsHitTestVisible = false
                };
                var g2 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.B.X, t.B.Y), EndPoint = endPoint };
                g2.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.B.Color }, new GradientStop { Offset = 1, Color = endColor } };
                var gs2 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.B.X, t.B.Y), EndPoint = endPoint };
                gs2.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.B.Color }, new GradientStop { Offset = 1, Color = endColor } };
                path2.Fill = g2;
                path2.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : (Brush)gs2;
                path2.Fill.Opacity = fillOpacity;
                path2.Stroke.Opacity = strokeOpacity;
                SeriesSurface.Children.Add(path2);

                var pf3 = new PathFigure { IsClosed = true, IsFilled = true };
                pf3.StartPoint = new Point(t.A.X, t.A.Y);
                pf3.Segments.Add(new LineSegment { Point = new Point(t.B.X, t.B.Y) });
                pf3.Segments.Add(new LineSegment { Point = new Point(t.C.X, t.C.Y) });
                var path3 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf3 } },
                    IsHitTestVisible = false
                };
                var g3 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.C.X, t.C.Y), EndPoint = endPoint };
                g3.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.C.Color }, new GradientStop { Offset = 1, Color = endColor } };
                var gs3 = new LinearGradientBrush { MappingMode = BrushMappingMode.Absolute, StartPoint = new Point(t.C.X, t.C.Y), EndPoint = endPoint };
                gs3.GradientStops = new GradientStopCollection { new GradientStop { Offset = 0, Color = t.C.Color }, new GradientStop { Offset = 1, Color = endColor } };
                path3.Fill = g3;
                path3.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : (Brush)gs3;
                path3.Fill.Opacity = fillOpacity;
                path3.Stroke.Opacity = strokeOpacity;
                SeriesSurface.Children.Add(path3);
            }
        }

        private void RenderDiscreteTriangles(List<Triangle> triangles)
        {
            var strokeOpacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
            var fillOpacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            var isWhiteStroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered;
            var strokeSize = GetMarkerSizeCoefficient(_sriX);

            foreach (var t in triangles)
            {
                var endPoint = new Point
                (
                    (t.A.X + t.B.X + t.C.X) / 3,
                    (t.A.Y + t.B.Y + t.C.Y) / 3
                );
                var ABPoint = new Point
                (
                    (t.A.X + t.B.X) / 2,
                    (t.A.Y + t.B.Y) / 2
                );

                var BCPoint = new Point
                (
                    (t.B.X + t.C.X) / 2,
                    (t.B.Y + t.C.Y) / 2
                );

                var CAPoint = new Point
                (
                    (t.A.X + t.C.X) / 2,
                    (t.A.Y + t.C.Y) / 2
                );

                var pf1 = new PathFigure
                {
                    IsClosed = true,
                    IsFilled = true,
                    StartPoint = new Point(t.A.X, t.A.Y)
                };
                pf1.Segments.Add(new LineSegment { Point = ABPoint });
                pf1.Segments.Add(new LineSegment { Point = endPoint });
                pf1.Segments.Add(new LineSegment { Point = CAPoint });
                var path1 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf1 } },
                    IsHitTestVisible = false
                };
                path1.Fill = new SolidColorBrush(t.A.Color);
                path1.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : new SolidColorBrush(t.A.Color);
                path1.Fill.Opacity = fillOpacity;
                path1.Stroke.Opacity = strokeOpacity;
                SeriesSurface.Children.Add(path1);

                var pf2 = new PathFigure
                {
                    IsClosed = true,
                    IsFilled = true,
                    StartPoint = new Point(t.B.X, t.B.Y)
                };
                pf2.Segments.Add(new LineSegment { Point = BCPoint });
                pf2.Segments.Add(new LineSegment { Point = endPoint });
                pf2.Segments.Add(new LineSegment { Point = ABPoint });
                var path2 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf2 } },
                    IsHitTestVisible = false
                };
                path2.Fill = new SolidColorBrush(t.B.Color);
                path2.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : new SolidColorBrush(t.B.Color);
                path2.Fill.Opacity = fillOpacity;
                path2.Stroke.Opacity = strokeOpacity;
                SeriesSurface.Children.Add(path2);

                var pf3 = new PathFigure
                {
                    IsClosed = true,
                    IsFilled = true,
                    StartPoint = new Point(t.C.X, t.C.Y)
                };
                pf3.Segments.Add(new LineSegment { Point = CAPoint });
                pf3.Segments.Add(new LineSegment { Point = endPoint });
                pf3.Segments.Add(new LineSegment { Point = BCPoint });
                var path3 = new Path
                {
                    StrokeThickness = strokeSize,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round,
                    StrokeLineJoin = PenLineJoin.Round,
                    Data = new PathGeometry { Figures = new PathFigureCollection { pf3 } },
                    IsHitTestVisible = false
                };
                path3.Fill = new SolidColorBrush(t.C.Color);
                path3.Stroke = isWhiteStroke ? new SolidColorBrush(Colors.White) : new SolidColorBrush(t.C.Color);
                path3.Fill.Opacity = fillOpacity;
                path3.Stroke.Opacity = strokeOpacity;
                SeriesSurface.Children.Add(path3);

                if (isWhiteStroke)
                {
                    var pf0 = new PathFigure { IsClosed = true, IsFilled = true };
                    pf0.StartPoint = new Point(t.A.X, t.A.Y);
                    pf0.Segments.Add(new LineSegment { Point = new Point(t.B.X, t.B.Y) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(t.C.X, t.C.Y) });
                    var path0 = new Path
                    {
                        StrokeThickness = strokeSize,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf0 } },
                        IsHitTestVisible = false
                    };
                    path0.Fill = new SolidColorBrush(Colors.Transparent);
                    path0.Stroke = new SolidColorBrush(Colors.White);
                    path0.Fill.Opacity = fillOpacity;
                    path0.Stroke.Opacity = strokeOpacity;
                    SeriesSurface.Children.Add(path0);
                }
            }
        }
    }
}