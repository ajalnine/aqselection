﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawBag()
        {
            var vertices = GetVertices();
            //            var arrangedVertices = DualMapping(vertices);
            // Инициализация - все точки пересечения соседних прямых (сортировка по наклону)

            var strokeOpacity = GetStrokeOpacity(_sriX.SourceSeries.MarkerMode);
            var fillOpacity = GetFillOpacity(_sriX.SourceSeries.MarkerMode);
            var isWhiteStroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered;

            foreach (var v in vertices)
            {
                var b = new Brushes {strokeBrush = new SolidColorBrush(v.Color) , fillBrush = new SolidColorBrush(v.Color) };
                DrawSingleMarker(new Size(20, 20), b, v.X, v.Y, null, SeriesSurface);
            }
        }

        private class BagPoint
        {
            public double X;
            public double Y;
            public Color Color;
            public int Depth;
        }

        private enum PointType
        {
            Starting,
            Ending,
            Intersection
        }
    }
}