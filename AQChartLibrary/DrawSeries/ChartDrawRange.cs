﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawRangeOnX()
        {
            var markerstroke = GetDefaultBrushes();

            if (_sriX == null) return;
            double? y = null;
            var xBound = _sriX.BoundAxis;
            var zBound = _sriZ?.BoundAxis;
            var markerSize = GetMarkerSizeForGroups().Height;
            var isMultiple = _sriX.SourceSeries.Options.Contains("IsMultiple");
            var withText = _sriX.SourceSeries.Options.Contains("Text");
            var isOpposite = _sriX.SourceSeries.Options.Contains("Opposite");
            var isMean = _sriX.SourceSeries.Options.Contains("Mean");
            var rangeXSeries = xBound.BoundSeries.Where(a => a.SourceSeries.SeriesType == ChartSeriesType.RangeOnX).ToList();
            var rangeNumber = _sourceZ?.Distinct().Count() ?? 1;
            if (isMultiple) rangeNumber = rangeXSeries.Count;
            if (isOpposite) rangeNumber /= 2;

            var seriesIndex = rangeXSeries.IndexOf(_sriX);

            var layers = _sourceZ?.Distinct().ToList();
            var layeredData = new Dictionary<object, List<object>>();
            var layerSide = new Dictionary<object, double>();
            var topLayers = new List<object>();
            var bottomLayers = new List<object>();
            var layerBrushes = GetLayerBrushes();

            if (layers != null)
            {
                foreach (var layer in layers)
                {
                    var data = new List<object>();
                    var side = 1.0d;
                    for (int i = 0; i < _sourceX.Count; i++)
                    {
                        if (_sourceZ[i]?.ToString() != layer?.ToString()) continue;
                        data.Add(_sourceX[i]);
                        side = ((double?)_sourceW?[i]) ?? 1.0d;
                    }
                    
                    if (Math.Abs(side - 1) < 1e-8 && isOpposite)topLayers.Add(layer);
                    else bottomLayers.Add(layer);
                    layeredData.Add(layer ?? "#", data);
                    layerSide.Add(layer ?? "#", side);
                }
            }
            else
            {
                var data = new List<object>();
                for (int i = 0; i < _sourceX.Count; i++)
                {
                    data.Add(_sourceX[i]);
                }
                layeredData.Add("#", data);
                layerSide.Add("#", 1);
            }

            double boxCenterX, boxMin, boxMax, whiskerMin, whiskerMax;
            string label;
            var markerMode = _sriX.SourceSeries.MarkerMode;
            if (markerMode == MarkerModes.SolidBordered) markerMode = MarkerModes.Solid;

            markerSize /= 1.5;
            foreach (var layer in layeredData.Keys)
            {
                var ii = isMultiple
                    ? new InteractivityIndex { Layer = layer, LayerName = _sriX?.SourceColumnName, SeriesName = _sriX.SeriesName + "_Parameter" }
                    : new InteractivityIndex { Layer = layer, LayerName = _sriZ?.SourceColumnName, SeriesName = _sriX.SeriesName + "_Layer" };
                var stroke = layerBrushes[layer ?? "#"];
                var boxstroke = stroke;
                var whiskerstroke = stroke;
                var centerstroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered  || _sriX.SourceSeries.MarkerMode == MarkerModes.Solid
                    ? defaultBackgroundBrush
                    : new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40));
                var centerBrushes = new Brushes { strokeBrush = centerstroke, fillBrush = centerstroke};
                var centertextstroke = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40));

                var data = layeredData[layer ?? "#"];
                var n = (double)data.Count;
                if (isMean)
                {
                    var aggregateStDev = AQMath.AggregateStDev(data);
                    var aggregateMean = AQMath.AggregateMean(data);
                    boxCenterX = _surfaceWidth*_sriX.BoundAxis.TransformValue(aggregateMean, _sriX).Value;
                    boxMin = _surfaceWidth*
                             _sriX.BoundAxis.TransformValue(aggregateMean - aggregateStDev / Math.Sqrt(n), _sriX).Value;
                    boxMax = _surfaceWidth*
                             _sriX.BoundAxis.TransformValue(aggregateMean + aggregateStDev / Math.Sqrt(n), _sriX).Value;
                    whiskerMin = _surfaceWidth*
                                 _sriX.BoundAxis.TransformValue(aggregateMean - aggregateStDev, _sriX).Value;
                    whiskerMax = _surfaceWidth*
                                 _sriX.BoundAxis.TransformValue(aggregateMean + aggregateStDev, _sriX).Value;
                    label = AQMath.SmartRound(aggregateMean.Value, 6).ToString();
                }
                else
                {
                    var q1 = AQMath.AggregatePercentile(data, 25);
                    var q3 = AQMath.AggregatePercentile(data, 75);
                    var aggregateMedian = AQMath.AggregateMedian(data);
                    var min = AQMath.AggregateMin(data);
                    var max = AQMath.AggregateMax(data);
                    boxCenterX = _surfaceWidth * _sriX.BoundAxis.TransformValue(aggregateMedian, _sriX).Value;
                    boxMin = _surfaceWidth *
                             _sriX.BoundAxis.TransformValue(q1, _sriX).Value;
                    boxMax = _surfaceWidth *
                             _sriX.BoundAxis.TransformValue(q3, _sriX).Value;
                    whiskerMin = _surfaceWidth *
                                 _sriX.BoundAxis.TransformValue(min, _sriX).Value;
                    whiskerMax = _surfaceWidth *
                                 _sriX.BoundAxis.TransformValue(max, _sriX).Value;
                    label = AQMath.SmartRound(aggregateMedian.Value, 4).ToString();
                }
                var topPlaced = (layerSide[layer ?? "#"] > 0 && isOpposite);

                if (isMultiple) y = BottomHelperSurface.ActualHeight * ((seriesIndex + 0.5) / rangeNumber);
                else if (zBound == null) y = BottomHelperSurface.ActualHeight / 2;
                else
                {
                    var upIndex = topLayers.IndexOf(layer);
                    if (upIndex >= 0)
                    {
                        y = TopHelperSurface.ActualHeight * ((upIndex + 0.5) / topLayers.Count);
                        rangeNumber = topLayers.Count;
                    }
                    else
                    {
                        var downIndex = bottomLayers.IndexOf(layer);
                        if (downIndex >= 0) y = BottomHelperSurface.ActualHeight * ((downIndex + 0.5) / bottomLayers.Count);
                        rangeNumber = bottomLayers.Count;
                    }
                }
                var rangeHeight = BottomHelperSurface.Height / rangeNumber * GetMarkerSizeCoefficient(_sriX);
                if (rangeHeight > BottomHelperSurface.Height / 2) rangeHeight = BottomHelperSurface.Height / 2;
                if (rangeHeight < 3) rangeHeight = 3;
                if (!y.HasValue || double.IsNaN(y.Value)) continue;
                
                var surface = isOpposite
                    ? !topPlaced ? BottomHelperSurface : TopHelperSurface
                    : BottomHelperSurface;
                var offset = surface == BottomHelperSurface
               ? ShowHelpersBeyondAxis
                   ? 5 + _surfaceHeight + BottomAxis.ActualHeight
                   : _surfaceHeight
               : ShowHelpersBeyondAxis
                   ? -TopAxis.ActualHeight - TopHelperSurface.Height - 5
                   : -TopHelperSurface.Height;

                var box = new Rectangle{Width = boxMax - boxMin, Height = rangeHeight};
                Canvas.SetTop(box, y.Value - rangeHeight /2);
                Canvas.SetLeft(box, boxMin);
                SetBrushesToShape(new Size(markerSize, markerSize), boxstroke, box, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                surface.Children.Add(box);
                AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), box, ii);
                SetRangeInteractivity(box, ii);

                if (whiskerMax <= BottomHelperSurface.ActualWidth)
                {
                    var whiskerLeft = new Rectangle {Width = 0.5, Height = rangeHeight};
                    Canvas.SetTop(whiskerLeft, y.Value - rangeHeight/2);
                    Canvas.SetLeft(whiskerLeft, whiskerMax );
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerstroke, whiskerLeft, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerLeft);
                    AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), whiskerLeft, ii );
                    SetRangeInteractivity(whiskerLeft, ii);
                }
                else whiskerMax = BottomHelperSurface.ActualWidth;

                if (whiskerMin >= 0)
                {
                    var whiskerRight = new Rectangle {Width = 0.5, Height = rangeHeight};
                    Canvas.SetTop(whiskerRight, y.Value - rangeHeight/2);
                    Canvas.SetLeft(whiskerRight, whiskerMin);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerstroke, whiskerRight, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerRight);
                    AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), whiskerRight, ii);
                    SetRangeInteractivity(whiskerRight, ii);
                }
                else whiskerMin = 0;

                if (whiskerMin < boxMin)
                {
                    var whiskerRight2 = new Rectangle {Width = boxMin - whiskerMin, Height = 0.5};
                    Canvas.SetTop(whiskerRight2, y.Value);
                    Canvas.SetLeft(whiskerRight2, whiskerMin);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerstroke, whiskerRight2, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerRight2);
                    AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), whiskerRight2, ii);
                    SetRangeInteractivity(whiskerRight2, ii);
                }

                if (whiskerMax > boxMax)
                {
                    var whiskerLeft2 = new Rectangle {Width = whiskerMax - boxMax, Height = 0.5};
                    Canvas.SetTop(whiskerLeft2, y.Value);
                    Canvas.SetLeft(whiskerLeft2, boxMax);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerstroke, whiskerLeft2, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerLeft2);
                    AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), whiskerLeft2, ii);
                    SetRangeInteractivity(whiskerLeft2, ii);
                }

                if (!withText)
                {
                    Rectangle center;
                    if (isMean)
                    {
                        center = new Rectangle { Width = markerSize, Height = markerSize };
                        Canvas.SetTop(center, y.Value - markerSize / 2);
                        Canvas.SetLeft(center, boxCenterX - markerSize / 2);
                    }
                    else
                    {
                        center = new Rectangle { Width = 0.5, Height = rangeHeight };
                        Canvas.SetTop(center, y.Value - rangeHeight / 2);
                        Canvas.SetLeft(center, boxCenterX);
                    }
                    SetBrushesToShape(new Size(markerSize, markerSize), centerBrushes, center, MarkerModes.Solid, MarkerShapeModes.Bubble);
                    surface.Children.Add(center);
                    AppendSelectable(new Point(whiskerMin + (whiskerMax - whiskerMin)/2, offset + y.Value), new Size(whiskerMax - whiskerMin, rangeHeight), center, ii);
                    SetRangeInteractivity(center, ii);
                }
                else
                {
                    DrawCenterHelperLabel(label, centertextstroke, boxCenterX, y.Value, 9, true, surface);
                }
            }
        }

        private void DrawRangeOnY()
        {
            var markerstroke = GetDefaultBrushes();

            if (_sriY == null) return;
            double? x = null;
            var yBound = _sriY.BoundAxis;
            var zBound = _sriZ?.BoundAxis;
            var markerSize = GetMarkerSizeForGroups().Height;
            var serieForZ = ZAxisRange.BoundSeries.SingleOrDefault(a => a.SeriesName == _sriX.SourceSeries.ZValueSeriesTitle);
            var isRight = _sriY.SourceSeries.Options.Contains("Right") || _sriY.BoundAxis.IsSecondary;
            var isMultiple = _sriX.SourceSeries.Options.Contains("IsMultiple");
            var withText = _sriX.SourceSeries.Options.Contains("Text");
            var isOpposite = _sriX.SourceSeries.Options.Contains("Opposite");
            var isMean = _sriX.SourceSeries.Options.Contains("Mean");
            var rangeYSeries = yBound.BoundSeries.Where(a => a.SourceSeries.SeriesType == ChartSeriesType.RangeOnY).ToList();
            var rangeNumber = _sourceZ?.Distinct().Count() ?? 1;
            if (isMultiple) rangeNumber = rangeYSeries.Count;
            if (isOpposite) rangeNumber /= 2;

            var seriesIndex = rangeYSeries.IndexOf(_sriY);

            var layers = _sourceZ?.Distinct().ToList();
            var layeredData = new Dictionary<object, List<object>>();
            var layerBrushes = GetLayerBrushes();
            if (layers != null)
            {
                foreach (var layer in layers)
                {
                    var data = new List<object>();
                    var side = 1.0d;
                    for (int i = 0; i < _sourceY.Count; i++)
                    {
                        if (_sourceZ[i]?.ToString() != layer?.ToString()) continue;
                        data.Add(_sourceY[i]);
                    }
                    layeredData.Add(layer, data);
                }
            }
            else
            {
                var data = new List<object>();
                for (int i = 0; i < _sourceY.Count; i++)
                {
                    data.Add(_sourceY[i]);
                }
                layeredData.Add("#", data);
            }
            double boxCenterY, boxMin, boxMax, whiskerMin, whiskerMax;
            string label;

            var markerMode = _sriY.SourceSeries.MarkerMode;
            if (markerMode == MarkerModes.SolidBordered) markerMode = MarkerModes.Solid;
            var surface = isRight
                    ? RightHelperSurface
                    : LeftHelperSurface;
            var offset = surface == RightHelperSurface
                ? ShowHelpersBeyondAxis
                    ? 10 + _surfaceWidth + RightAxis.ActualWidth
                    : _surfaceWidth
                : ShowHelpersBeyondAxis
                    ? -LeftHelperSurface.Width - 10 - LeftAxis.ActualWidth
                    : -LeftHelperSurface.Width;

            markerSize /= 1.5;
            foreach (var layer in layeredData.Keys)
            {
                var ii = isMultiple
                    ? new InteractivityIndex { Layer = layer, LayerName = _sriY?.SourceColumnName, SeriesName = _sriX.SeriesName + "_Parameter" }
                    : new InteractivityIndex { Layer = layer, LayerName = _sriZ?.SourceColumnName, SeriesName = _sriX.SeriesName + "_Layer" };
                var brushes = layerBrushes[layer ?? "#"];
                var boxbrush = brushes;
                var whiskerbrush = brushes;
                var centerstroke = _sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered || _sriX.SourceSeries.MarkerMode == MarkerModes.Solid
                    ? defaultBackgroundBrush
                    : new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40));
                var centerBrushes = new Brushes { strokeBrush = centerstroke, fillBrush = centerstroke };
                var centertextstroke = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0x40, 0x40));

                var data = layeredData[layer];
                var n = (double)data.Count;
                if (isMean)
                {
                    var aggregateStDev = AQMath.AggregateStDev(data);
                    var aggregateMean = AQMath.AggregateMean(data);
                    if (!aggregateMean.HasValue || !aggregateStDev.HasValue) continue;
                    boxCenterY = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMean, _sriY).Value;
                    boxMin = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMean - aggregateStDev / Math.Sqrt(n), _sriY).Value;
                    boxMax = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMean + aggregateStDev / Math.Sqrt(n), _sriY).Value;
                    whiskerMin = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMean - aggregateStDev, _sriY).Value;
                    whiskerMax = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMean + aggregateStDev, _sriY).Value;
                    label = AQMath.SmartRound(aggregateMean.Value, 4).ToString();
                }
                else
                {
                    var q1 = AQMath.AggregatePercentile(data, 25);
                    var q3 = AQMath.AggregatePercentile(data, 75);
                    if (!q1.HasValue || !q3.HasValue) continue;
                    var aggregateMedian = AQMath.AggregateMedian(data);
                    var min = AQMath.AggregateMin(data);
                    var max = AQMath.AggregateMax(data);
                    boxCenterY = _surfaceHeight - _surfaceHeight * _sriY.BoundAxis.TransformValue(aggregateMedian, _sriY).Value;
                    boxMin = _surfaceHeight - _surfaceHeight *
                             _sriY.BoundAxis.TransformValue(q1, _sriY).Value;
                    boxMax = _surfaceHeight - _surfaceHeight *
                             _sriY.BoundAxis.TransformValue(q3, _sriY).Value;
                    whiskerMin = _surfaceHeight - _surfaceHeight *
                                 _sriY.BoundAxis.TransformValue(min, _sriY).Value;
                    whiskerMax = _surfaceHeight - _surfaceHeight *
                                 _sriY.BoundAxis.TransformValue(max, _sriY).Value;
                    label = AQMath.SmartRound(aggregateMedian.Value, 6).ToString();
                }
                if (isMultiple) x = LeftHelperSurface.ActualWidth * ((seriesIndex + 0.5) / rangeNumber);
                else if (zBound == null) x = LeftHelperSurface.ActualWidth / 2;
                else
                {
                    var downIndex = layers.IndexOf(layer);
                    if (downIndex >= 0) x = LeftHelperSurface.ActualWidth * ((downIndex + 0.5) / layers.Count);
                    rangeNumber = layers.Count;
                }

                var rangeWidth = LeftHelperSurface.ActualWidth / rangeNumber * GetMarkerSizeCoefficient(_sriY) ;
                if (rangeWidth > LeftHelperSurface.ActualWidth / 2) rangeWidth = LeftHelperSurface.ActualWidth / 2;
                if (rangeWidth < 3) rangeWidth = 3;
                if (!x.HasValue || double.IsNaN(x.Value)) continue;

                x = LeftHelperSurface.ActualWidth - x;
                var box = new Rectangle { Width = rangeWidth, Height = boxMin - boxMax };
                Canvas.SetTop(box, boxMax);
                Canvas.SetLeft(box, x.Value - rangeWidth / 2);
                SetBrushesToShape(new Size(markerSize, markerSize), boxbrush, box, markerMode, _sriY.SourceSeries.MarkerShapeMode);
                surface.Children.Add(box);
                AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax)/2), new Size(rangeWidth, whiskerMin - whiskerMax), box, ii);
                SetRangeInteractivity(box, ii);


                if (whiskerMin <= LeftHelperSurface.ActualHeight)
                {
                    var whiskerBottom = new Rectangle { Width = rangeWidth, Height = 0.5 };
                    Canvas.SetTop(whiskerBottom, whiskerMin);
                    Canvas.SetLeft(whiskerBottom, x.Value - rangeWidth / 2);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerbrush, whiskerBottom, markerMode, _sriY.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerBottom);
                    AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax) / 2), new Size(rangeWidth, whiskerMin - whiskerMax), whiskerBottom, ii);
                    SetRangeInteractivity(whiskerBottom, ii);
                }
                else whiskerMin = LeftHelperSurface.ActualHeight;

                if (whiskerMax >= 0)
                {
                    var whiskerTop = new Rectangle { Width = rangeWidth , Height = 0.5};
                    Canvas.SetTop(whiskerTop, whiskerMax);
                    Canvas.SetLeft(whiskerTop, x.Value - rangeWidth / 2);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerbrush, whiskerTop, markerMode, _sriX.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerTop);
                    AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax) / 2), new Size(rangeWidth, whiskerMin - whiskerMax), whiskerTop, ii);
                    SetRangeInteractivity(whiskerTop, ii);
                }
                else whiskerMax = 0;

                if (whiskerMax < boxMax)
                {
                    var whiskerBottom2 = new Rectangle { Width = 0.5, Height = boxMax - whiskerMax };
                    Canvas.SetTop(whiskerBottom2, whiskerMax);
                    Canvas.SetLeft(whiskerBottom2, x.Value);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerbrush, whiskerBottom2, markerMode, _sriY.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerBottom2);
                    AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax) / 2), new Size(rangeWidth, whiskerMin - whiskerMax), whiskerBottom2, ii);
                    SetRangeInteractivity(whiskerBottom2, ii);
                }

                if (whiskerMin > boxMin)
                {
                    var whiskerTop2 = new Rectangle { Width = 0.5, Height = whiskerMin - boxMin};
                    Canvas.SetTop(whiskerTop2, boxMin);
                    Canvas.SetLeft(whiskerTop2, x.Value);
                    SetBrushesToShape(new Size(markerSize, markerSize), whiskerbrush, whiskerTop2, markerMode, _sriY.SourceSeries.MarkerShapeMode);
                    surface.Children.Add(whiskerTop2);
                    AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax) / 2), new Size(rangeWidth, whiskerMin - whiskerMax), whiskerTop2, ii);
                    SetRangeInteractivity(whiskerTop2, ii);
                }

                if (!withText)
                {
                    Rectangle center;
                    if (isMean)
                    {
                        center = new Rectangle { Width = markerSize, Height = markerSize };
                        Canvas.SetTop(center, boxCenterY - markerSize / 2);
                        Canvas.SetLeft(center, x.Value - markerSize / 2);
                    }
                    else
                    {
                        center = new Rectangle { Width = rangeWidth, Height = 0.5 };
                        Canvas.SetTop(center, boxCenterY);
                        Canvas.SetLeft(center, x.Value - rangeWidth / 2);
                    }
                    SetBrushesToShape(new Size(markerSize, markerSize), centerBrushes, center, MarkerModes.Solid, MarkerShapeModes.Bubble);
                    surface.Children.Add(center);
                    AppendSelectable(new Point(x.Value + offset, whiskerMax + (whiskerMin - whiskerMax) / 2), new Size(rangeWidth, whiskerMin - whiskerMax), center, ii);
                    SetRangeInteractivity(center, ii);
                }
                else
                {
                    DrawRotatedCenterHelperLabel(label, centertextstroke, x.Value, boxCenterY, 9, true, surface);
                }
            }
        }
        private void SetRangeInteractivity(Rectangle e, InteractivityIndex ii)
        {
            e.Tag = ii;
            e.MouseLeftButtonDown += Marker_MouseLeftButtonDown;
        }
    }
}