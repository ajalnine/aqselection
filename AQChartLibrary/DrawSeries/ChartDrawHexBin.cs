﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        public double HexTan = Math.Tan(Math.PI / 6);
        public double HexSin = Math.Sin(Math.PI / 6);

        private void DrawHexBin()
        {
            var withText = _sriX.SourceSeries.Options.Contains("Text");
            var log = _sriX.SourceSeries.Options.Contains("Log");
            var isMultiple = _sriX.SourceSeries.Options.Contains("IsMultiple");

            var binK = 10d;
            var binwidth = _markerK[_sriX.SourceSeries.MarkerSizeMode] * binK;
            var A = binwidth / Math.Sqrt(3.0d);
            var h = A / 2.0d;
            var binheight = A + h;
            var cellXNumber = (int) Math.Floor(_surfaceWidth / binwidth);
            var cellYNumber = (int) Math.Floor(_surfaceHeight / binheight);
            binwidth = _surfaceWidth / cellXNumber;
            binheight = _surfaceHeight / cellYNumber;

            int currentBin;
            Tuple<int, int> cellIndexes;

            if (_sourceZ != null)
            {
                int[,] cellCount = new int[cellXNumber + 1, cellYNumber + 1];
                double[,] R = new double[cellXNumber + 1, cellYNumber + 1];
                double[,] G = new double[cellXNumber + 1, cellYNumber + 1];
                double[,] B = new double[cellXNumber + 1, cellYNumber + 1];

                var maxCount = 0;
                var brush = GetLayerBrushes();
                var points = GetLayerPoints();
                Color layercolor;
                foreach (var p in points)
                {
                    layercolor = ((SolidColorBrush)brush[p.Layer ?? "#"].strokeBrush).Color;
                    cellIndexes = FindBin(p.X, p.Y, binwidth, binheight);
                    if (cellIndexes.Item1 < 0 || cellIndexes.Item2 < 0 || cellIndexes.Item1 > cellXNumber || cellIndexes.Item2 > cellYNumber) continue;
                    currentBin = ++cellCount[cellIndexes.Item1, cellIndexes.Item2];
                    if (currentBin > maxCount) maxCount = currentBin;
                    R[cellIndexes.Item1, cellIndexes.Item2] += layercolor.R;
                    G[cellIndexes.Item1, cellIndexes.Item2] += layercolor.G;
                    B[cellIndexes.Item1, cellIndexes.Item2] += layercolor.B;
                }
                RenderCells(cellCount, binwidth, binheight, maxCount, R, G, B, false, log, cellXNumber, cellYNumber);
            }
            else
            {
                var color = ((SolidColorBrush)GetDefaultBrushes().strokeBrush).Color;
                var points = GetPoints();
                var maxCount = 0;
                int[,] cellCount = new int[cellXNumber+1, cellYNumber+1];
                foreach (var p in points)
                {
                    cellIndexes = FindBin(p.X, p.Y, binwidth, binheight);
                    if (cellIndexes.Item1 < 0 || cellIndexes.Item2 < 0 || cellIndexes.Item1> cellXNumber || cellIndexes.Item2 > cellYNumber) continue;
                    currentBin = ++cellCount[cellIndexes.Item1, cellIndexes.Item2];
                    if (currentBin > maxCount) maxCount = currentBin;
                }
                RenderCells(cellCount, binwidth, binheight, maxCount, color, isMultiple, withText, log, cellXNumber, cellYNumber);
            }
        }

        private Tuple<int, int> FindBin(double x, double y, double s, double h)
        {
            var iy = (int)Math.Floor( (_surfaceHeight - y) / h + 1.0d);
            var isodd = (iy % 2 == 1);
            int ix = isodd ? (int)Math.Floor(x / s) : (int)Math.Floor(x / s + 0.5d);
            var localy = (_surfaceHeight - y) - (iy - 1.0d) * h;
            var localx = x - ix * s;
            if (!isodd) localx += s / 2;
            double a, b, k, v;
            if ((localx >= s / 4 && localx <= s - s / 4) || (localy >= h / 4 && localy <= h - h / 4)) return new Tuple<int, int>(ix, iy);

            var leftdelta = !isodd ? -1: 0;
            var rightdelta = !isodd ? 0 : 1;
            var updelta = 1;
            var downdelta = -1;
            var dh = 0.25 * s * HexTan;

            if (localx <= s / 4 && localy >= h - dh) //1
            {
                a = 0;
                b = h - dh;
                k = HexTan;
                v = b + k * (localx - a);
                if (v <= localy)
                {
                    ix += leftdelta;
                    iy += updelta;
                }
                return new Tuple<int, int>(ix, iy);
            }

            if (localx >= s - s / 4 && localy >= h - dh) //2
            {
                a = s - s / 4;
                b = h;
                k = -HexTan;
                v = b + k * (localx - a);
                if (v <= localy)
                {
                    ix += rightdelta;
                    iy += updelta;
                }
                return new Tuple<int, int>(ix, iy);
            }


            if (localx <= s / 4 && localy <= dh) //3
            {
                a = 0;
                b = dh;
                k = -HexTan;
                v = b + k * (localx - a);
                if (v >= localy)
                {
                    ix += leftdelta;
                    iy += downdelta;
                }
                return new Tuple<int, int>(ix, iy);
            }

            if (localx >= s - s / 4 && localy <= dh) //4
            {
                a = s - s / 4;
                b = 0;
                k = HexTan;
                v = b + k * (localx - a);
                if (v >= localy)
                {
                    ix += rightdelta;
                    iy += downdelta;
                }
                return new Tuple<int, int>(ix, iy);
            }
            return new Tuple<int, int>(ix, iy);
        }

        private List<Point> GetPoints()
        {
            if (_sriX == null || _sriY == null) return null;
            var result = new List<Point>();
            double? x, y;
            double surfaceX, surfaceY;
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;
                surfaceX = _surfaceWidth * x.Value;
                surfaceY = _surfaceHeight - _surfaceHeight * y.Value;
                result.Add(new Point (surfaceX, surfaceY));
            }
            return result;
        }

        private List<LayerPoint> GetLayerPoints()
        {
            if (_sriX == null || _sriY == null) return null;
            var result = new List<LayerPoint>();
            double? x, y;
            double surfaceX, surfaceY;
            for (var i = 0; i < _nX; i++)
            {
                x = _sriX.BoundAxis.TransformValue(!_sriX.IsNominal ? _sourceX[i] : i, _sriX);
                y = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                if (!x.HasValue || !y.HasValue || double.IsNaN(x.Value) || double.IsNaN(y.Value)) continue;
                surfaceX = _surfaceWidth * x.Value;
                surfaceY = _surfaceHeight - _surfaceHeight * y.Value;
                result.Add(new LayerPoint { X = surfaceX, Y = surfaceY, Layer = _sourceZ[i] });
            }
            return result;
        }

        public class LayerPoint
        {
            public double X;
            public double Y;
            public object Layer;
        }

        private void RenderCells(int[,] cells, double binWidth, double binHeight, int maxCount, Color color, bool isMultiple, bool withText, bool log, int cellXnumber, int cellYnumber)
        {
            if (maxCount == 0) return;
            var isSolid = _sriX.SourceSeries.MarkerMode == MarkerModes.Solid;
            var A = binWidth / Math.Sqrt(3.0d);
            var h = A / 2.0d;
            var s = binWidth / 2;
            var maxCountLog = Math.Log(maxCount);
            double cell, xtop, ytop;
            var textBrush = new SolidColorBrush(color);
            var invertTextLabel = defaultBackgroundBrush;
            for (var y = 0; y< cellYnumber; y++)
            {
                var isodd = (y % 2 == 1);
                for (var x = 0; x < cellXnumber; x++)
                {
                    if (cells[x, y] == 0) continue;
                    cell = log 
                        ?  Math.Log(cells[x, y]) / (maxCountLog==0 ? 1:maxCountLog)
                        :  cells[x, y] / (double)maxCount;
                    xtop = x * binWidth + (isodd ? binWidth / 2.0d : 0);
                    ytop = _surfaceHeight - y * binHeight - h /2;

                    var pf0 = new PathFigure
                    {
                        IsClosed = true,
                        IsFilled = true,
                        StartPoint = new Point(xtop, ytop)
                    };

                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop + s, ytop + h) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop + s, ytop + h + A) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop, ytop + h + A + h) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop - s, ytop + h + A) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop - s, ytop + h) });

                    var path0 = new Path
                    {
                        StrokeThickness = isSolid ? 0 : 1,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf0 } },
                        IsHitTestVisible = false
                    };
                    SetHexBrushes(isMultiple, path0, color, cell);
                    SeriesSurface.Children.Add(path0);

                    if (withText) DrawHexLabel(cells[x, y].ToString(), cell < 0.5 ? textBrush : invertTextLabel, xtop, ytop + h + A / 2, LabelFontSize);
                }
            }
        }

        private void RenderCells(int[,] cells, double binWidth, double binHeight, int maxCount, double[,] R, double[,] G, double[,] B, bool isMultiple, bool log, int cellXnumber, int cellYnumber)
        {
            if (maxCount == 0) return;
            var isSolid = _sriX.SourceSeries.MarkerMode == MarkerModes.Solid;
            var A = binWidth / Math.Sqrt(3.0d);
            var h = A / 2.0d;
            var s = binWidth / 2;
            var maxCountLog = Math.Log(maxCount);
            double cell, xtop, ytop, r, g, b;
            Color color;
            for (var y = 0; y < cellYnumber; y++)
            {
                var isodd = (y % 2 == 1);
                for (var x = 0; x < cellXnumber; x++)
                {
                    if (cells[x, y] == 0) continue;

                    cell = log
                        ? Math.Log(cells[x, y]) / (maxCountLog == 0 ? 1 : maxCountLog)
                        : cells[x, y] / (double)maxCount;
                    xtop = x * binWidth + (isodd ? binWidth / 2.0d : 0);
                    ytop = _surfaceHeight - y * binHeight - h / 2;

                    r = R[x, y] / cells[x, y];
                    g = G[x, y] / cells[x, y];
                    b = B[x, y] / cells[x, y];
                    
                    color = Color.FromArgb(0xff, (byte)r, (byte)g, (byte)b);

                    var pf0 = new PathFigure
                    {
                        IsClosed = true,
                        IsFilled = true,
                        StartPoint = new Point(xtop, ytop)
                    };

                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop + s, ytop + h) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop + s, ytop + h + A) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop, ytop + h + A + h) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop - s, ytop + h + A) });
                    pf0.Segments.Add(new LineSegment { Point = new Point(xtop - s, ytop + h) });

                    var path0 = new Path
                    {
                        StrokeThickness = isSolid ? 0 : 1,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeLineJoin = PenLineJoin.Round,
                        Data = new PathGeometry { Figures = new PathFigureCollection { pf0 } },
                        IsHitTestVisible = false
                    };
                    SetHexBrushes(isMultiple, path0, color, cell);
                    SeriesSurface.Children.Add(path0);
                }
            }
        }


        private void DrawHexLabel(string labelText, Brush textBrush, double surfaceX, double surfaceY, double fontSize)
        {
            var tb = new TextBlock
            {
                Text = labelText,
                FontSize = fontSize * GlobalFontCoefficient,
                Foreground = textBrush,
                FontFamily = new FontFamily("Verdana"),
                IsHitTestVisible = false
            };

            Canvas.SetLeft(tb, surfaceX - tb.ActualWidth / 2);
            Canvas.SetTop(tb, surfaceY - tb.ActualHeight / 2);
            tb.Foreground = textBrush;
            LabelsSurface.Children.Add(tb);
        }

        private void SetHexBrushes(bool isLayered, Path hex, Color color, double opacity)
        {
            switch (_sriX.SourceSeries.MarkerMode)
            {
                case MarkerModes.Transparent:
                    hex.Fill = new SolidColorBrush(color);
                    hex.Fill.Opacity = isLayered ? opacity / 4 : opacity / 2;
                    hex.Stroke = new SolidColorBrush(color);
                    hex.Stroke.Opacity = isLayered ? opacity / 4 : opacity / 2;
                break;
                case MarkerModes.Ring:
                    hex.Fill = null;
                    hex.Stroke = new SolidColorBrush(color);
                    hex.Stroke.Opacity = isLayered ? opacity / 2 : opacity;
                break;
                case MarkerModes.SemiTransparent:
                    hex.Fill = new SolidColorBrush(color);
                    hex.Fill.Opacity = isLayered ? opacity / 4 : opacity / 2;
                    hex.Stroke = new SolidColorBrush(color);
                break;
                case MarkerModes.Solid:
                    hex.Fill = new SolidColorBrush(color);
                    hex.Fill.Opacity = isLayered ? opacity / 2 : opacity;
                    hex.Stroke = new SolidColorBrush(color);
                break;
                case MarkerModes.SolidBordered:
                    hex.Fill = new SolidColorBrush(color);
                    hex.Fill.Opacity = isLayered ? opacity / 2 : opacity;
                    hex.Stroke = new SolidColorBrush(Colors.White);
                break;
            }
        }
    }
}