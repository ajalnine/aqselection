﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private static Dictionary<MarkerSizeModes, double> ColumnCoefficient = new Dictionary<MarkerSizeModes, double>
        {
            {MarkerSizeModes.Small, 0.75},
            {MarkerSizeModes.SmallMedium, 0.9},
            {MarkerSizeModes.Medium, 1},
            {MarkerSizeModes.MediumLarge, 1},
            {MarkerSizeModes.Large, 1},
            {MarkerSizeModes.ExtraLarge, 1},
        };

        private static Dictionary<MarkerSizeModes, double> StrokeCoefficient = new Dictionary<MarkerSizeModes, double>
        {
            {MarkerSizeModes.Small, 1},
            {MarkerSizeModes.SmallMedium, 1},
            {MarkerSizeModes.Medium, 1},
            {MarkerSizeModes.MediumLarge, 1.5},
            {MarkerSizeModes.Large, 2},
            {MarkerSizeModes.ExtraLarge, 3},
        };

        private void DrawStairsOnX()
        {
            if (_sriX.BoundAxis == null || _sriY.BoundAxis == null) return;
            var stringProcessing = _sriX.SourceSeries.Options!=null && (_sriX.SourceSeries.Options.Contains("StringProcessing") || (_sriX.SourceSeries.Options.Contains("GroupProcessing") && _sriZ == null));
            var groupProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("GroupProcessing") && _sriZ != null);
            var normedProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("Normed"));
            var blockWhite = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("BlockWhite"));
            var labelWideLimited = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("WideLimited"));
            var numericGroupProcessing = _sriX.SourceSeries.Options != null && (_sriX.SourceSeries.Options.Contains("NumericProcessing") && _sriZ != null);
            var labelArrange = new ColumnLabelArrange();

            var smallOverwork =
                _sriX.SourceSeries.Options != null && _sriX.SourceSeries.Options.Contains("SmallDefault")
                    ? SmallLabelOverwork.SmallDefault
                    : _sriX.SourceSeries.Options != null && _sriX.SourceSeries.Options.Contains("SmallOver")
                        ? SmallLabelOverwork.SmallOver
                        : _sriX.SourceSeries.Options != null && _sriX.SourceSeries.Options.Contains("SmallSkip")
                            ? SmallLabelOverwork.SmallSkip
                            : SmallLabelOverwork.SmallOver;
                ;
            if (groupProcessing) GroupProcessingCaseX(labelArrange, normedProcessing, blockWhite, smallOverwork, labelWideLimited);
            else if (numericGroupProcessing) GroupNumericProcessingCaseX(labelArrange, normedProcessing, smallOverwork, labelWideLimited);
            else if (stringProcessing) //Columns, Pareto
            {
                StringProcessingCaseX(labelArrange, normedProcessing, blockWhite, smallOverwork, labelWideLimited);
            }
            else if (!_sriX.IsNominal)
            {
                NumericProcessingCaseX(labelArrange, normedProcessing, blockWhite, smallOverwork, labelWideLimited);
            }
            else
            {
                NominalProcessingCaseX(labelArrange, normedProcessing, blockWhite, smallOverwork, labelWideLimited);
            }
            labelArrange.RenderPlaces(SeriesSurface, LabelsSurface);
        }

        private void NominalProcessingCaseX(ColumnLabelArrange la, bool normed, bool blockWhite, SmallLabelOverwork smallOverwork, bool labelWideLimited)
        {
            var layerBrushes = GetLayerBrushes();

            var n1 = _sriX.SeriesData.Count;
            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var st = ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            var fromBase = _sourceW == null || !_sourceW.OfType<double>().Any(a => a != 0);
            double cx, cy, cw, ch;
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(i - 0.5, _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var x2 = _sriX.BoundAxis.TransformValue(i + 0.5, _sriX);
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue || double.IsNaN(x2.Value)) continue;
                
                var from = Math.Round(_surfaceWidth * x1.Value, 1) - 0.5 * lineThickness;
                var height = Math.Round(_surfaceHeight * y1.Value, 1);
                var to = Math.Round(_surfaceWidth * x2.Value, 1) + 0.5 * lineThickness;
                var offset = z.HasValue ? Math.Round(_surfaceHeight * z.Value, 1) : 0;
                ch = Math.Abs(height);
                cw = (to - @from) * st;
                cx = @from + (to - @from) * (1 - st) * 0.5;
                cy = _surfaceHeight - ((height >= 0) ? height : 0) - offset;

                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };

                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count<=2 )&& !normed) || blockWhite);
                var invertedLabelBrush = GetInvertedLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);
                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);
                SeriesSurface.Children.Add(r);
                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);
                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                     ? LabelFontSize *
                       (1 +
                        ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                        ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                     : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;

                    la.AddConstraint(new LabelConstraint { Key = i, x1 = cx, y1 = cy, x2 = cx + cw, y2 = cy + ch });

                    var clad = new ColumnLabelArrangeDescription
                    {
                        Key = i,
                        ConstraintKey = i,
                        x = cx,
                        y = cy,
                        Width = cw,
                        Height = ch,
                        LabelText = _markerLabels[i].ToString(),
                        LabelBrush = labelBrush,
                        InvertedLabelBrush = invertedLabelBrush,
                        FontSize = labelFontSize * GlobalFontCoefficient,
                        MaxWidth = null,
                        AllowInternal = true,
                        NegativeColumns = false,
                        WhiteBG = !Inverted,
                        SmallOverwork = smallOverwork,
                        AllowColorChange = !normed
                    };

                    if (_sriZ == null || fromBase || _sriX.SourceSeries.MarkerMode == MarkerModes.Invisible)
                    {
                        if (height >= 0)
                        {
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;
                            clad.MaxWidth = labelWideLimited ? (double?)(to - from) : null;
                        }
                        else
                        {
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;
                            clad.MaxWidth = labelWideLimited ? (double?)(to - from) : null;
                            clad.y = cy + ch;
                        }
                    }
                    la.AddColumnGraph(clad);
                }
            }
        }

        private void NumericProcessingCaseX(ColumnLabelArrange la, bool normed, bool blockWhite, SmallLabelOverwork smallOverwork, bool labelWideLimited)
        {
            var layerBrushes = GetLayerBrushes();
            var n1 = _sriX.SeriesData.Count;
            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var st = ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);
            double? x1, y1, z, w, x2;
            double from, offset, height, to, cx, cy, cw, ch;
            for (int i = 0; i < n1; i++)
            {
                x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                w = _sourceV != null ? _sriX.BoundAxis.TransformValue(_sourceV[i], _sriX) : null;
                x2 = w ?? _sriX.BoundAxis.TransformValue(_sourceX[i + 1], _sriX);

                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value)) continue;

                from = Math.Round(_surfaceWidth*x1.Value, 1) - 0.5*lineThickness;
                offset = z.HasValue ? _surfaceHeight*z.Value : 0;
                
                height = Math.Round(_surfaceHeight*y1.Value - offset, 1);
                to = Math.Round(_surfaceWidth*x2.Value, 1) + 0.5*lineThickness;
                ch = Math.Abs(height);
                cw = (to - @from) * st;
                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                var invertedLabelBrush = GetInvertedLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);
                cx = @from + (to - @from)*(1 - st)*0.5;
                cy = _surfaceHeight - ((height >= 0) ? height : 0) - offset;

                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);
                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize*
                          (1 +
                           ((double?) _sriX?.SeriesMarkerSizes[i] - (double) minLabelFontSizeValue)/
                           ((double) maxLabelFontSizeValue - (double) minLabelFontSizeValue)/2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    la.AddConstraint(new LabelConstraint { Key = i, x1 = cx, y1 = cy, x2 = cx + cw, y2 = cy + ch });

                    var clad = new ColumnLabelArrangeDescription
                    {
                        Key = i,
                        ConstraintKey = i,
                        x = cx,
                        y = cy,
                        Width = cw,
                        Height = ch,
                        LabelText = _markerLabels[i].ToString(),
                        LabelBrush = labelBrush,
                        InvertedLabelBrush = invertedLabelBrush,
                        FontSize = labelFontSize * GlobalFontCoefficient,
                        MaxWidth = labelWideLimited ? (double?)(to - from) : null,
                        AllowInternal = true,
                        NegativeColumns = false,
                        WhiteBG = !Inverted,
                        SmallOverwork = smallOverwork,
                        AllowColorChange = !normed
                    };

                    if (_sriZ == null || fromBase || _sriX.SourceSeries.MarkerMode == MarkerModes.Invisible)
                    {
                        if (height >= 0)
                        {
                            clad.AllowInternal = false;
                            clad.NegativeColumns = false;   
                        }
                        else
                        {
                            clad.y = cy + ch;
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;

                        }
                    }
                    la.AddColumnGraph(clad);
                }
            }
        }

        private void GroupNumericProcessingCaseX(ColumnLabelArrange la, bool normed, SmallLabelOverwork smallOverwork, bool labelWideLimited)
        {
            var n1 = _sriX.SeriesData.Count;

            var layerBrushes = GetLayerBrushes();

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);

            var columnnumber = _sourceV.Distinct().Count();
            var st = 1 / _sriX.BoundAxis.RealSteps;
            var singleColumnSize = st / columnnumber; // Ширина одного столбца
            var k = (1 - ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode])/2;
            double cx, cy, ch, cw;
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = (double?)_sourceV?[i];
                if (w < 0) continue;
                x1 += st * w + k * singleColumnSize;
                var x2 = x1 + singleColumnSize - 2 * k * singleColumnSize;
                
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value))
                    continue;

                var from = Math.Round(_surfaceWidth * x1.Value, 1) - 0.5 * lineThickness;
                var offset = z.HasValue ? _surfaceHeight * z.Value : 0;
                var height = Math.Round(_surfaceHeight * y1.Value - offset, 1);
                var to = Math.Round(_surfaceWidth * x2.Value, 1) + 0.5 * lineThickness;
                ch = Math.Abs(height);
                cw = to - @from;
                cx = @from;
                cy = _surfaceHeight - ((height >= 0) ? height : 0) - offset;
                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, (_sriZ == null || layerBrushes.Count <= 2) || !normed);
                var invertedLabelBrush = GetInvertedLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);
                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);
                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels?[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize *
                          (1 +
                           ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                           ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    la.AddConstraint(new LabelConstraint { Key = i, x1 = cx, y1 = cy, x2 = cx + cw, y2 = cy + ch });

                    var clad = new ColumnLabelArrangeDescription
                    {
                        Key = i,
                        ConstraintKey = i,
                        x = cx,
                        y = cy,
                        Width = cw,
                        Height = ch,
                        LabelText = _markerLabels[i].ToString(),
                        LabelBrush = labelBrush,
                        InvertedLabelBrush = invertedLabelBrush,
                        FontSize = labelFontSize * GlobalFontCoefficient,
                        MaxWidth = null,
                        AllowInternal = true,
                        NegativeColumns = false,
                        WhiteBG = !Inverted,
                        SmallOverwork = smallOverwork,
                        AllowColorChange = !normed
                    };

                    if (_sriZ == null || fromBase || _sriX.SourceSeries.MarkerMode == MarkerModes.Invisible)
                    {
                        if (height >= 0)
                        {
                            clad.AllowInternal = true;
                        }
                        else
                        {
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;
                            clad.y = cy + ch;
                        }
                    }
                    la.AddColumnGraph(clad);
                }
            }
        }

        private void GroupProcessingCaseX(ColumnLabelArrange la, bool normed, bool blockWhite, SmallLabelOverwork smallOverwork, bool labelWideLimited)
        {
            var n1 = _sriX.SeriesData.Count;

            var layerBrushes = GetLayerBrushes();

            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();


            var bandnumber = _sourceV.Distinct().Count();
            var st = _sriX.BoundAxis.TransformValue(_sourceX[0], _sriX) * 2;
            var singleBandSize = st / bandnumber;
            var k = (1 - ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode]) / 2;
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);
            double cx, cy, cw, ch;
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX) - st/2;
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = (double?)_sourceV?[i];
                if (w < 0) continue;

                x1 += st * w + k * singleBandSize;
                var x2 = x1 + singleBandSize - 2 * k * singleBandSize;

                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value)) continue;

                var from = Math.Round(_surfaceWidth * x1.Value, 1) - 0.5 * lineThickness;
                var height = Math.Round(_surfaceHeight * y1.Value, 1);
                var to = Math.Round(_surfaceWidth * x2.Value, 1) + 0.5 * lineThickness;
                cx = @from;
                cy = _surfaceHeight - ((height >= 0) ? height : 0);
                cw = to - @from;
                ch = Math.Abs(height);

                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((_sriZ == null || layerBrushes.Count <= 2) && !normed) || blockWhite);
                var invertedLabelBrush = GetInvertedLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode);
                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                r.MouseLeftButtonDown += Rectangle_MouseLeftButtonDownX;
                SeriesSurface.Children.Add(r);

                Canvas.SetLeft(r,cx);
                Canvas.SetTop(r, cy);
                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);
                
                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                        ? LabelFontSize *
                          (1 +
                           ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                           ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                        : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    la.AddConstraint(new LabelConstraint { Key = i, x1 = cx, y1 = cy, x2 = cx + cw, y2 = cy + ch });

                    var clad = new ColumnLabelArrangeDescription
                    {
                        Key = i,
                        ConstraintKey = i,
                        x = cx,
                        y = cy,
                        Width = cw,
                        Height = ch,
                        LabelText = _markerLabels[i].ToString(),
                        LabelBrush = labelBrush,
                        InvertedLabelBrush = invertedLabelBrush,
                        FontSize = labelFontSize * GlobalFontCoefficient,
                        MaxWidth = null,
                        AllowInternal = true,
                        NegativeColumns = false,
                        WhiteBG = !Inverted,
                        SmallOverwork = smallOverwork,
                        AllowColorChange = !normed
                    };

                    if (_sriZ == null || fromBase || _sriX.SourceSeries.MarkerMode == MarkerModes.Invisible)
                    {
                        if (height >= 0)
                        {
                            clad.AllowInternal = false;
                        }
                        else
                        {
                            clad.y = cy + ch;
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;
                        }
                    }
                    la.AddColumnGraph(clad);
                }
            }
        }

        private void StringProcessingCaseX(ColumnLabelArrange la, bool normed, bool blockWhite, SmallLabelOverwork smallOverwork, bool labelWideLimited)
        {
            var n1 = _sriX.SeriesData.Count;

            var layerBrushes = GetLayerBrushes();
            var lineThickness = _sriX.SourceSeries.LineSize;
            var maxLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Max();
            var minLabelFontSizeValue = _sriX.SeriesMarkerSizes?.Min();

            var firstNonZero = _sourceX.FirstOrDefault(a => a != null && !string.IsNullOrEmpty(a.ToString()));
            var st = _sriX.BoundAxis.TransformValue(firstNonZero, _sriX);
            if (st == null && _sourceX.Count >= 1) st = _sriX.BoundAxis.TransformValue(_sourceX[1], _sriX)/2;
            st *= ColumnCoefficient[_sriX.SourceSeries.MarkerSizeMode];
            var fromBase = !_sourceW.OfType<double>().Any(a => a != 0);
            double cx, cy, cw, ch;
            for (int i = 0; i < n1; i++)
            {
                var x1 = _sriX.BoundAxis.TransformValue(_sourceX[i], _sriX);
                var y1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY);
                var z = _sourceW != null ? _sriY.BoundAxis.TransformValue(_sourceW[i], _sriY) : null;
                var w = _sourceV != null ? _sriX.BoundAxis.TransformValue(_sourceV[i], _sriX) : null;
                var x2 = x1 + 2 * st;
                x1 -= st;
                x2 -= st;
                if (!x1.HasValue || !y1.HasValue || double.IsNaN(x1.Value) || double.IsNaN(y1.Value) || !x2.HasValue ||
                    double.IsNaN(x2.Value)) continue;

                var from = Math.Round(_surfaceWidth*x1.Value, 1) - 0.5*lineThickness;
                var offset = z.HasValue ? Math.Round(_surfaceHeight*z.Value, 1) : 0;
                var height = Math.Round(_surfaceHeight*y1.Value - offset, 1);
                var to = Math.Round(_surfaceWidth*x2.Value, 1) + 0.5*lineThickness;
                ch = Math.Abs(height);
                cw = to - @from;
                cx = @from;
                cy = _surfaceHeight - ((height >= 0) ? height : 0) - offset;
                var r = new Rectangle
                {
                    Height = ch,
                    Width = cw,
                    StrokeThickness = _sriX.SourceSeries.LineSize * StrokeCoefficient[_sriX.SourceSeries.MarkerSizeMode]
                };
                var markerBrushes = _markerColors?[i] != null
                    ? new Brushes
                    {
                        strokeBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                        fillBrush = new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString())),
                    }
                    : layerBrushes[_sourceZ?[i] ?? "#"];
                var labelBrush = GetLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode, ((layerBrushes.Count <= 2) && !normed) || blockWhite);
                var invertedLabelBrush = GetInvertedLabelBrush(markerBrushes.strokeBrush, _sriX.SourceSeries.MarkerMode);

                SetBrushesToShape(new Size(cw, ch), markerBrushes, r, _sriX.SourceSeries.MarkerMode, _sriX.SourceSeries.MarkerShapeMode);

                var ii = new InteractivityIndex { Index = i, SeriesName = _sriX.SeriesName };
                SetInteractivity(r, ii);

                SeriesSurface.Children.Add(r);

                Canvas.SetLeft(r, cx);
                Canvas.SetTop(r, cy);

                AppendSelectable(new Point(cx + cw / 2, cy + ch / 2), new Size(cw, ch), r, ii);

                if (_markerLabels != null && _markerLabels[i] != null && !string.IsNullOrWhiteSpace(_markerLabels[i].ToString()))
                {
                    var labelFontSize = maxLabelFontSizeValue is double
                     ? LabelFontSize *
                       (1 +
                        ((double?)_sriX?.SeriesMarkerSizes[i] - (double)minLabelFontSizeValue) /
                        ((double)maxLabelFontSizeValue - (double)minLabelFontSizeValue) / 2.0d ?? 1)
                     : LabelFontSize;
                    if (double.IsNaN(labelFontSize)) labelFontSize = LabelFontSize;
                    la.AddConstraint(new LabelConstraint { Key = i, x1 = cx, y1 = cy, x2 = cx + cw, y2 = cy + ch });

                    var clad = new ColumnLabelArrangeDescription
                    {
                        Key = i,
                        ConstraintKey = i,
                        x = cx,
                        y = cy,
                        Width = cw,
                        Height = ch,
                        LabelText = _markerLabels[i].ToString(),
                        LabelBrush = labelBrush,
                        InvertedLabelBrush = invertedLabelBrush,
                        FontSize = labelFontSize * GlobalFontCoefficient,
                        MaxWidth = labelWideLimited ? (double?)(to - from) : null,
                        AllowInternal = true,
                        NegativeColumns = false,
                        WhiteBG = !Inverted,
                        SmallOverwork = smallOverwork,
                        AllowColorChange = !normed
                    };

                    if (_sriZ == null || fromBase || _sriX.SourceSeries.MarkerMode == MarkerModes.Invisible)
                    {
                        if (height >= 0)
                        {
                            clad.AllowInternal = ((SolidColorBrush) labelBrush).Color == Colors.White;
                        }
                        else
                        {
                            clad.AllowInternal = false;
                            clad.NegativeColumns = true;
                            clad.y = cy + ch;
                        }
                    }
                    la.AddColumnGraph(clad);
                }
            }
        }
        
        void Rectangle_MouseLeftButtonDownX(object sender, MouseButtonEventArgs e)
        {
            var el = sender as Shape;
            var pi = (InteractivityIndex)el?.Tag;
            HandleSingleClickSelection(e, el, pi);
        }
    }
}