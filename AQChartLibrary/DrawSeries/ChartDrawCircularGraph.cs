﻿using AQBasicControlsLibrary;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart
    {
        private void DrawCircularGraph()
        {
            if (_sriX == null || _sriY == null) return;
            var th = (_sriX.SourceSeries.LineSize > 1) ? _sriX.SourceSeries.LineSize : 1;
            var toRed = _sriX.SourceSeries.Options == "ToRed" && _sriZ == null;
            var layerBrushes = GetLayerBrushes();
            if (Math.Abs(th) < 1e-8) return;
                        
            var labelArrange = new PieLabelArrange();
            labelArrange.AddConstraint(new LabelConstraint {Key = 0,  });
            if (_sriX == null || _sriY == null) return;
            double x1, y1, x2,y2, x3,y3, x4,y4;
            Brush markerBrush;
            double graphRadius = Math.Min(_surfaceWidth, _surfaceHeight) * 0.75 * _radialCoefficients[_sriX.SourceSeries.MarkerSizeMode] / 2;
            double angle1, angle2, angle;
            var maxTextWidth = (_surfaceWidth / 2 - graphRadius) * 0.8;
            var labels = _sourceX.Distinct().ToList();
            var labelColor = GetLabelBrush(GetDefaultBrushes().strokeBrush, MarkerModes.Solid, true);
            var markerColor = GetDefaultBrushes();
            var markerSize = GetMarkerSize(0);
            double centerX = _surfaceWidth / 2;
            double centerY = _surfaceHeight / 2;

            for (var i = 0; i < labels.Count; i++)
            {
                angle = _sriY.BoundAxis.TransformValue(labels[i], _sriY).Value * 2 * Math.PI;
                x1 = centerX + graphRadius * Math.Sin(angle);
                y1 = centerY - graphRadius * Math.Cos(angle);

                labelArrange.AddPieGraph(new PieLabelArrange.PieLabelArrangeDescription
                {
                    Key = i,
                    ConstraintKey = 0,
                    CircleCenterX = centerX,
                    CircleCenterY = centerY,
                    circleMaxRadius = graphRadius,
                    circleMinRadius = 0,
                    angle = angle,
                    angleRange = 0,
                    LabelText = labels[i].ToString(),
                    LabelBrush = labelColor,
                    WhiteBG = !Inverted,
                    FontSize = LabelFontSize * GlobalFontCoefficient,
                    MaxWidth = maxTextWidth,
                    AllowInternal = false
                });
                DrawSingleMarker(markerSize, markerColor, x1, y1, null, SeriesSurface);
            }
            double? layerOffset;
            double v, thickness, r, r2, angleDiff, angleMove;
            for (var i = 0; i < _nX; i++)
            {
                if (_sourceY[i] == _sourceX[i]) continue;
                v = (double)_sourceV[i];
                thickness = Math.Abs(v) * markerSize.Width / 2;
                if (double.IsNaN(thickness) || double.IsInfinity(thickness)) continue;

                markerBrush = Math.Abs(v) > 0.5 && toRed 
                    ? new SolidColorBrush(Colors.Red) 
                    : _markerColors?[i] != null
                        ? new SolidColorBrush(ColorConvertor.ConvertStringToColor(_markerColors[i].ToString()))
                        : layerBrushes[_sourceZ?[i] ?? "#"].strokeBrush; 
                angle1 = _sriY.BoundAxis.TransformValue(_sourceY[i], _sriY).Value * 2 * Math.PI;
                angle2 = _sriY.BoundAxis.TransformValue(_sourceX[i], _sriY).Value * 2 * Math.PI;
                layerOffset = _sourceZ == null || _sriZ == null ? null : _sriZ.BoundAxis.TransformValue(_sourceZ[i], _sriZ);

                angleDiff = Math.Abs(angle2 - angle1);
                angleMove = angleDiff < 2*Math.PI ? angleDiff / 2.5 / Math.PI : (angleDiff - 2 / Math.PI) / 2.5 / Math.PI;
                r = (0.9 - layerOffset / 7 - angleMove) ?? (0.9 - angleMove);
                r2 = (0.9 - layerOffset / 8 - angleMove) ?? (0.9 - angleMove);

                x1 = centerX +  graphRadius * Math.Sin(angle2);
                y1 = centerY - graphRadius * Math.Cos(angle2);
                x2 = centerX + graphRadius * Math.Sin(angle1);
                y2 = centerY - graphRadius * Math.Cos(angle1);
                x3 = centerX + graphRadius * Math.Sin(angle2) * r;
                y3 = centerY - graphRadius * Math.Cos(angle2) * r;
                x4 = centerX + graphRadius * Math.Sin(angle1) * r2;
                y4 = centerY - graphRadius * Math.Cos(angle1) * r2;
                var opacity = Math.Pow(Math.Abs(v), _opacityCoefficients[_sriX.SourceSeries.MarkerMode]);
                if (_sriX.SourceSeries.MarkerMode == MarkerModes.SolidBordered)
                {
                    var p2 = new Path
                    {
                        Stroke = defaultBackgroundBrush,
                        StrokeThickness = thickness * 1.3,
                        StrokeStartLineCap = PenLineCap.Round,
                        StrokeEndLineCap = PenLineCap.Round
                    };
                    PathFigure pf2 = new PathFigure { IsClosed = false, IsFilled = false, StartPoint = new Point(x1, y1) };
                    pf2.Segments.Add(new BezierSegment { Point3 = new Point(x2, y2), Point1 = new Point(x3, y3), Point2 = new Point(x4, y4) });
                    PathGeometry pathGeometry2 = new PathGeometry {Figures = new PathFigureCollection {pf2}};
                    p2.Data = pathGeometry2;
                    p2.Opacity = opacity;
                    SeriesSurface.Children.Add(p2);
                }

                var p = new Path
                {
                    Stroke =  markerBrush, StrokeThickness = thickness, StrokeStartLineCap = PenLineCap.Round, StrokeEndLineCap = PenLineCap.Round
                };
                var pf = new PathFigure {IsClosed = false, IsFilled = false, StartPoint = new Point(x1, y1)};
                pf.Segments.Add(new BezierSegment {Point3 = new Point(x2,y2), Point1 = new Point(x3,y3), Point2 = new Point(x4,y4)});
                var pathGeometry = new PathGeometry {Figures = new PathFigureCollection {pf}};
                p.Data = pathGeometry;
                p.Opacity = opacity;
                SeriesSurface.Children.Add(p);
                SetMarkerInteractivity(i, p);

            }
            labelArrange.RenderPlaces(SeriesSurface, LabelsSurface);
        }

        private readonly Dictionary<MarkerModes, double> _opacityCoefficients = new Dictionary<MarkerModes, double>
        {
            {MarkerModes.Invisible, 6},
            {MarkerModes.SemiTransparent, 5},
            {MarkerModes.Ring, 3},
            {MarkerModes.Solid, 1},
            {MarkerModes.SolidBordered, 2},
            {MarkerModes.Transparent, 10},
        };

        private readonly Dictionary<MarkerSizeModes, double> _radialCoefficients = new Dictionary<MarkerSizeModes, double>
        {
            {MarkerSizeModes.SmallMedium, 1},
            {MarkerSizeModes.Small, 0.9},
            {MarkerSizeModes.Medium, 1},
            {MarkerSizeModes.MediumLarge, 1.025},
            {MarkerSizeModes.Large, 1.05},
            {MarkerSizeModes.ExtraLarge, 1.1},
        };
    }
}