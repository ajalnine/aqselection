﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public delegate void AxisValueSelectionChangedDelegate(object o, AxisValueSelectionEventArgs e);
    public delegate void AxisClickedDelegate(object o, AxisEventArgs e);

    public partial class ChartAxis
    {
        public static readonly DependencyProperty AxisOrientationProperty =
            DependencyProperty.Register("AxisOrientation", typeof (Orientation), typeof (ChartAxis),
                                        new PropertyMetadata(Orientation.Horizontal, PropertyChanged));

        public static readonly DependencyProperty IsPrimaryProperty =
            DependencyProperty.Register("IsPrimary", typeof (bool), typeof (ChartAxis),
                                        new PropertyMetadata(true, PropertyChanged));

        public static readonly DependencyProperty IsNominalProperty =
            DependencyProperty.Register("IsNominal", typeof (bool), typeof (ChartAxis),
                                        new PropertyMetadata(false, PropertyChanged));

        public static readonly DependencyProperty AllowPseudoHTMLProperty =
            DependencyProperty.Register("AllowPseudoHTML", typeof(bool), typeof(ChartAxis),
                                        new PropertyMetadata(false, PropertyChanged));

        public static readonly DependencyProperty LabelColorProperty =
            DependencyProperty.Register("LabelColor", typeof(Brush), typeof(ChartAxis),
                                        new PropertyMetadata(new SolidColorBrush(Colors.Black), PropertyChanged));

        public static readonly DependencyProperty DataLabelsProperty =
            DependencyProperty.Register("DataLabels", typeof (List<string>), typeof (ChartAxis),
                                        new PropertyMetadata(null, PropertyChanged));

        public static readonly DependencyProperty SelectableProperty =
            DependencyProperty.Register("Selectable", typeof (bool), typeof (ChartAxis),
                                        new PropertyMetadata(false, PropertyChanged));

        public static readonly DependencyProperty EnableCellCondenseProperty =
            DependencyProperty.Register("EnableCellCondense", typeof (bool), typeof (ChartAxis),
                                        new PropertyMetadata(false, PropertyChanged));

        private readonly List<string> _selectedLabels = new List<string>();
        public event AxisClickedDelegate AxisClicked;

        public ChartAxis()
        {
            InitializeComponent();
        }

        public Orientation AxisOrientation
        {
            get { return (Orientation) GetValue(AxisOrientationProperty); }
            set { SetValue(AxisOrientationProperty, value); }
        }

        public bool IsPrimary
        {
            get { return (bool) GetValue(IsPrimaryProperty); }
            set { SetValue(IsPrimaryProperty, value); }
        }

        public bool IsNominal
        {
            get { return (bool) GetValue(IsNominalProperty); }
            set { SetValue(IsNominalProperty, value); }
        }

        public List<string> DataLabels
        {
            get { return (List<string>) GetValue(DataLabelsProperty); }
            set { SetValue(DataLabelsProperty, value); }
        }

        public bool Selectable
        {
            get { return (bool) GetValue(SelectableProperty); }
            set { SetValue(SelectableProperty, value); }
        }

        public Brush LabelColor
        {
            get { return (Brush)GetValue(LabelColorProperty); }
            set { SetValue(LabelColorProperty, value); }
        }

        public bool EnableCellCondense
        {
            get { return (bool) GetValue(EnableCellCondenseProperty); }
            set { SetValue(EnableCellCondenseProperty, value); }
        }

        public bool AllowPseudoHTML
        {
            get { return (bool)GetValue(AllowPseudoHTMLProperty); }
            set { SetValue(AllowPseudoHTMLProperty, value); }
        }

        public event AxisValueSelectionChangedDelegate AxisValueSelectionChanged;

        private static void PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var ca = (ChartAxis) o;
            if (ca.DataLabels != null) ca.RefreshAxis();
        }

        public void RefreshAxis()
        {
            List<string> DisplayLabels;
            if (DataLabels.Count > 2000)
            {
                var each = DataLabels.Count / 2000;
                var r = new List<string>();
                for (var i = 0; i < DataLabels.Count; i += each)
                {
                    r.Add(DataLabels[i]);
                }
                DisplayLabels = r;
            }
            else DisplayLabels = DataLabels;

            var ha = HorizontalAlignment.Center;
            var va = VerticalAlignment.Center;
            switch (AxisOrientation)
            {
                default:
                    LabelPanel.Columns = DisplayLabels.Count - (IsNominal ? 0 : 1);
                    if (LabelPanel.Columns < 0) LabelPanel.Columns = 0;
                    LabelPanel.Rows = 1;
                    va = IsPrimary ? VerticalAlignment.Top : VerticalAlignment.Bottom;
                    LabelPanel.HorizontalNodeCentered = !IsNominal;
                    LabelPanel.VerticalNodeCentered = false;
                    LabelPanel.Reversed = false;
                    LabelPanel.EnableCellCondense = EnableCellCondense;
                    LabelPanel.RotatedLabelsAlignment = IsPrimary ? VerticalAlignment.Top : VerticalAlignment.Bottom;
                    break;

                case Orientation.Vertical:
                    LabelPanel.Rows = DisplayLabels.Count - (IsNominal ? 0 : 1);
                    if (LabelPanel.Rows < 0) LabelPanel.Rows = 0;
                    LabelPanel.Columns = 1;
                    ha = IsPrimary ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                    LabelPanel.HorizontalNodeCentered = false;
                    LabelPanel.VerticalNodeCentered = !IsNominal;
                    LabelPanel.EnableCellCondense = EnableCellCondense;
                    LabelPanel.Reversed = true;
                    break;
            }

            LabelPanel.Children.Clear();
           
            foreach (string a in DisplayLabels)
            {
                if (Selectable)
                {
                    var tb = new ToggleButton
                    {
                        Content =
                            new TextBlock
                            {
                                Text = a,
                                HorizontalAlignment = ha,
                                VerticalAlignment = va,
                                TextAlignment = TextAlignment.Center,
                                TextTrimming = TextTrimming.WordEllipsis,
                                MaxHeight = 200,
                                MaxWidth = 200,
                                Foreground = LabelColor
                            },
                        Tag = a,
                        Style = Resources["ToggleButtonBorderStyle"] as Style,
                        IsChecked = _selectedLabels.Contains(a)
                    };
                    tb.Checked += tb_Checked;
                    tb.Unchecked += tb_Checked;
                    tb.Effect = new DropShadowEffect
                        {
                            Color = Colors.Black,
                            Opacity = 0.4,
                            Direction = 300,
                            BlurRadius = 3,
                            ShadowDepth = 3
                        };
                    LabelPanel.Children.Add(tb);
                }
                else
                {
                    if (DataLabels.Count < 40 && AllowPseudoHTML)
                    {
                        LabelPanel.Children.Add(new PseudoHTMLBox
                        {
                            Source = a,
                            HorizontalAlignment = ha,
                            VerticalAlignment = va,
                            MaxWidth = 200,
                            MaxHeight = 200,
                            IsHitTestVisible = false,
                            Foreground = LabelColor,
                            TextAlignment = TextAlignment.Center
                        });
                    }
                    else
                    {
                        LabelPanel.Children.Add(new TextBlock
                        {
                            Text = a,
                            HorizontalAlignment = ha,
                            VerticalAlignment = va,
                            TextAlignment = TextAlignment.Center,
                            MaxWidth = 200,
                            MaxHeight = 200,
                            IsHitTestVisible = false,
                            Foreground = LabelColor,
                        });
                    }
                }
            }
        }

        public void ClearSelection()
        {
            _selectedLabels.Clear();
            RefreshAxis();
        }

        public void EnableClick(bool enable)
        {
            AxisButton.Visibility = enable ? Visibility.Visible : Visibility.Collapsed;
        }

        private void tb_Checked(object sender, RoutedEventArgs e)
        {
            if (AxisValueSelectionChanged == null) return;
            var tb = sender as ToggleButton;
            var ea = new AxisValueSelectionEventArgs();
            if (tb != null)
            {
                ea.SelectedLabel = tb.Tag.ToString();
                ea.SelectedIndex = DataLabels.IndexOf(ea.SelectedLabel);
                ea.AxisWidth = LabelPanel.ActualWidth;
                double step = ea.AxisWidth/DataLabels.Count;
                ea.SelectedLabelOffset = step*ea.SelectedIndex + step/2;
                if (tb.IsChecked != null) ea.IsChecked = tb.IsChecked.Value;
            }
            if (ea.IsChecked) _selectedLabels.Add(ea.SelectedLabel);
            else if (_selectedLabels.Contains(ea.SelectedLabel)) _selectedLabels.Remove(ea.SelectedLabel);
            AxisValueSelectionChanged(this, ea);
        }
        
        private void AxisButton_OnClick(object sender, RoutedEventArgs e)
        {
            var a = e as MouseButtonEventArgs;
            if (a != null) a.Handled = true;
            if (AxisClicked != null) AxisClicked.Invoke(sender, new AxisEventArgs {Tag = Tag.ToString()});
        }
    }

    public class AxisValueSelectionEventArgs
    {
        public string SelectedLabel { get; set; }
        public double AxisWidth { get; set; }
        public double SelectedLabelOffset { get; set; }
        public int SelectedIndex { get; set; }
        public bool IsChecked { get; set; }
    }

    public class AxisEventArgs : EventArgs
    {
        public string Tag;
    }
}