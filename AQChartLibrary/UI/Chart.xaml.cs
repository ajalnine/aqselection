﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using System.ComponentModel;
using System.Windows.Input;
using System;

// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class Chart : IReportVisual
    {
        public static readonly DependencyProperty ChartDataProperty =
            DependencyProperty.Register("ChartData", typeof (ChartDescription), typeof (Chart),
                                        new PropertyMetadata(null));

        public static readonly DependencyProperty DataTablesProperty =
            DependencyProperty.Register("DataTables", typeof (List<SLDataTable>), typeof (Chart),
                                        new PropertyMetadata(null));

        public static readonly DependencyProperty SelectableProperty =
            DependencyProperty.Register("Selectable", typeof (bool), typeof (Chart),
                                        new PropertyMetadata(false, SelectablePropertyChanged));

        public static readonly DependencyProperty EnableSecondaryAxisesProperty =
            DependencyProperty.Register("EnableSecondaryAxises", typeof (bool), typeof (Chart),
                                        new PropertyMetadata(false));

        private List<BackgroundWorker> _workers = new List<BackgroundWorker>();
        public bool EnableCellCondense = true;
        public bool ConstantsOnHelpers = true;
        public bool UseDefaultColor = false;
        public bool IsBusy = false;
        public bool ShowHorizontalGridlines = true;
        public bool IsTransparent = false;
        public bool ShowLegend = true; // Temp
        public bool ShowTable = true; // Temp
        public bool ShowTitle = true;
        public bool ShowSubtitle = true;
        public bool HasShadow = false;
        public bool HideNegativeX = false;
        public bool HideMinorGridLines = false;
        public bool ReverseOrderedLegendDiscreteItems = true;
        public bool ShowVerticalAxises = true;
        public bool ShowVerticalZero = false;
        public bool ShowHorizontalZero = false;
        public bool ContrastGridLines = false;
        public bool ShowTickMarks = false;
        public double LabelFontSize = 8;
        public double GlobalFontCoefficient = 1;
        public int NumberOfMinorVerticalGridLines = 5;
        public int NumberOfMinorHorizontalGridLines = 5;
        public double HelpersCoefficient = 0.05;
        public bool ShowHorizontalAxises = true;
        public bool VerticalAxisSymmetry = false;
        public bool ShowXHelpers = false;
        public bool ShowYHelpers = false;
        public bool ShowHelpersBeyondAxis = true;
        public bool ShowHelpersMirrored = false;
        public bool OppositeHelpers = false;
        public bool HorizontalAxisSymmetry = false;
        public bool ShowVerticalGridlines = true;
        public bool ShowHelperGridlines = true;
        public bool Inverted = false;
        public bool DoubleVerticalAxis = false;
        public FixedAxises FixedAxises { get; set; }
        public List<SLDataTable> CustomLegendTables { get; set; }
        public string SourceName { get; set; }
        private Brush defaultForegroundBrush { get; set; }
        private Brush defaultGridBrush { get; set; }
        private Brush defaultBackgroundBrush { get; set; }
        public event VisualReadyDelegate VisualReady;

        private bool _leftHelperExists, _rightHelperExists, _topHelperExists, _bottomHelperExists;

        public Chart()
        {
            InitializeComponent();
            defaultForegroundBrush = Inverted 
                ? new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0xff, 0xff)) 
                : new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0x80, 0xc0));
            defaultGridBrush = Inverted
                ? new SolidColorBrush(Color.FromArgb(0xff, 0x80, 0x80, 0x80))
                : new SolidColorBrush(Color.FromArgb(0xff, 0x96, 0xc0, 0xc0));
            defaultBackgroundBrush = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0xff, 0xff));
        }

        public void StopWorking()
        {
            foreach (var w in _workers) w.CancelAsync();
        }

        public ChartDescription ChartData
        {
            get { return (ChartDescription) GetValue(ChartDataProperty); }
            set { SetValue(ChartDataProperty, value); }
        }

        public List<SLDataTable> DataTables
        {
            get { return (List<SLDataTable>) GetValue(DataTablesProperty); }
            set { SetValue(DataTablesProperty, value); }
        }

        public bool Selectable
        {
            get { return (bool) GetValue(SelectableProperty); }
            set { SetValue(SelectableProperty, value); }
        }

        public bool EnableSecondaryAxises
        {
            get { return (bool) GetValue(EnableSecondaryAxisesProperty); }
            set { SetValue(EnableSecondaryAxisesProperty, value); }
        }

        public event AxisValueSelectionChangedDelegate AxisValueSelectionChanged;
        public event AxisClickedDelegate AxisClicked;

        public static void SelectablePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var c = ((Chart) o);
            c.BottomAxis.Selectable = c.BottomAxisRange.IsNominal && (bool) e.NewValue;
        }

        public ChartRangesInfo GetChartRangesInfo()
        {
            if (ChartData == null) return null;
            SetupSeriesRanges();
            SetupAxisRanges();
            ChartRangesInfo cri = GetChartRanges();
            return cri;
        }

        private ChartRangesInfo GetChartRanges()
        {
            var cri = new ChartRangesInfo
                {
                    XDownMin = BottomAxisRange.DisplayMinimum,
                    XDownMax = BottomAxisRange.DisplayMaximum,
                    XDownSeries = BottomAxisRange.BoundSeries.Select(a => a.SeriesName).Distinct().ToList(),
                    XUpMin = TopAxisRange.DisplayMinimum,
                    XUpMax = TopAxisRange.DisplayMaximum,
                    XUpSeries = TopAxisRange.BoundSeries.Select(a => a.SeriesName).Distinct().ToList(),
                    YLeftMin = LeftAxisRange.DisplayMinimum,
                    YLeftMax = LeftAxisRange.DisplayMaximum,
                    YLeftSeries = LeftAxisRange.BoundSeries.Select(a => a.SeriesName).Distinct().ToList(),
                    YRightMin = RightAxisRange.DisplayMinimum,
                    YRightMax = RightAxisRange.DisplayMaximum,
                    YRightSeries = RightAxisRange.BoundSeries.Select(a => a.SeriesName).Distinct().ToList()
                };
            return cri;
        }

        public void MakeChart()
        {
            if (ChartData == null) return;
            IsBusy = true;
            InitSeriesDictionary();
            SetupColors();
            SetupTitles();
            SetupLayout();
            SetupSeriesRanges();
            SetupAxisRanges();
            if (ShowTable) DrawTable();
            else if (CustomLegendTables != null && ShowLegend) DrawCustomLegendTables(CustomLegendTables);
            SetupAxis();
            SetupHelpers();
            if (ShowLegend) DrawLegend();
            DrawSeries();
            DrawFunctions();
            DrawGridLines();
            if (ShowHorizontalGridlines || ShowVerticalAxises || ShowHorizontalGridlines || ShowVerticalGridlines) DrawZero(GridLinesSurface);
            IsBusy = false;
            VisualReady?.Invoke(this, new VisualReadyEventArgs()); 
        }
        
        private void SetupColors()
        {
            if (!ChartData.ChartSeries.Any()) return;
            var scales = ChartData.ChartSeries.Distinct();
            var defaultScale = (scales.Count() == 1)
                    ? UseDefaultColor 
                        ? ChartData.ChartSeries.Single().ColorScale 
                        : ColorScales.HSV
                    : UseDefaultColor
                        ? ((ColorScales)ChartData.ChartSeries.Max(a=>(int)a.ColorScale))
                        : ColorScales.HSV;

            var defaultColor = Inverted 
                ? "#ffffffff"
                : ColorConvertor.GetDefaultColorForScale(defaultScale);
            defaultForegroundBrush = ConvertStringToStroke(defaultColor);
            defaultGridBrush = ConvertStringToStroke(defaultColor);
            defaultGridBrush.Opacity = 0.3;
            var titleForeground = Inverted ? new SolidColorBrush(Colors.White) : defaultForegroundBrush;
            LeftAxisTitle.Foreground = titleForeground;
            RightAxisTitle.Foreground = titleForeground;
            UpAxisTitle.Foreground = titleForeground;
            BottomAxisTitle.Foreground = titleForeground;
            LeftAxis.LabelColor = titleForeground;
            RightAxis.LabelColor = titleForeground;
            TopAxis.LabelColor = titleForeground;
            BottomAxis.LabelColor = titleForeground;
        }

        private void SetupTitles()
        {
            if (!string.IsNullOrEmpty(ChartData.ChartTitle) && ShowTitle)
            {
                ChartTitle.Visibility = Visibility.Visible;
                ChartTitle.FontSize = 14 * GlobalFontCoefficient;
                ChartTitle.Foreground = defaultForegroundBrush;
                PseudoHTMLAxisTitleParser.FillRichTextWithHTML(ChartData.ChartTitle, ChartTitle);
            }
            else
            {
                ChartTitle.Visibility = Visibility.Collapsed;
            }
            if (!string.IsNullOrEmpty(ChartData.ChartSubtitle) && ShowSubtitle)
            {
                ChartSubtitle.Visibility = Visibility.Visible;
                ChartSubtitle.FontSize = 10.5 * GlobalFontCoefficient;
                ChartSubtitle.Foreground = defaultForegroundBrush;
                PseudoHTMLAxisTitleParser.FillRichTextWithHTML(ChartData.ChartSubtitle, ChartSubtitle);
            }
            else
            {
                ChartSubtitle.Visibility = Visibility.Collapsed;
            }
            if (!string.IsNullOrEmpty(ChartData.ChartCathegoryTitle))
            {
                ChartCathegoryTitle.Visibility = Visibility.Visible;
                ChartSubtitle.FontSize = 12 * GlobalFontCoefficient;
                ChartCathegoryTitle.Foreground = defaultForegroundBrush;
                ChartCathegoryTitle.Text = ChartData.ChartSubtitle;
            }
            else
            {
                ChartCathegoryTitle.Visibility = Visibility.Collapsed;
            }

            RightAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            LeftAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            UpAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            BottomAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            LeftAxis.FontSize = 10 * GlobalFontCoefficient;
            RightAxis.FontSize = 10 * GlobalFontCoefficient;
            TopAxis.FontSize = 10 * GlobalFontCoefficient;
            BottomAxis.FontSize = 10 * GlobalFontCoefficient;
        }

        private void SetupHelpers()
        {
            var bottomSeries = BottomAxisRange.BoundSeries.Any();
            var topSeries = TopAxisRange.BoundSeries.Any();
            var leftSeries = LeftAxisRange.BoundSeries.Any();
            var rightSeries = RightAxisRange.BoundSeries.Any();

            _bottomHelperExists =    ShowXHelpers && (bottomSeries || (topSeries && OppositeHelpers));
            _topHelperExists =       ShowXHelpers && (topSeries || (bottomSeries && OppositeHelpers));
            _leftHelperExists =      ShowYHelpers && (leftSeries || (rightSeries && OppositeHelpers));
            _rightHelperExists =     ShowYHelpers && (rightSeries || (leftSeries && OppositeHelpers));
            var size = Math.Min(Height, Width);

            if (_bottomHelperExists)
            {
                BottomHelperSurface.Height = size * HelpersCoefficient;
            }
            if (_topHelperExists)
            {
                TopHelperSurface.Height = size * HelpersCoefficient;
            }
            if (_leftHelperExists)
            {
                LeftHelperSurface.Width = size * HelpersCoefficient;
            }
            if (_rightHelperExists)
            {
                RightHelperSurface.Width = size * HelpersCoefficient;
            }
            if (ShowHelpersBeyondAxis)
            {
                Grid.SetColumn(LeftHelperGridLinesSurface, 1);
                Grid.SetColumn(LeftHelperSurface, 1);
                Grid.SetColumn(LeftAxis, 2);
                Grid.SetColumn(RightAxis, 4);
                Grid.SetColumn(RightHelperSurface, 5);
                Grid.SetColumn(RightHelperGridLinesSurface, 5);
                Grid.SetRow(TopHelperGridLinesSurface, 4);
                Grid.SetRow(TopHelperSurface, 4);
                Grid.SetRow(TopAxis, 5);
                Grid.SetRow(BottomAxis, 7);
                Grid.SetRow(BottomHelperSurface, 8);
                Grid.SetRow(BottomHelperGridLinesSurface, 8);
                LeftHelperSurface.Margin = new Thickness(0, 0, 0, 0);
                RightHelperSurface.Margin = new Thickness(0, 0, 5, 0);
                TopHelperSurface.Margin = new Thickness(0, 0, 0, 0);
                BottomHelperSurface.Margin = new Thickness(0, 0, 0, 0);
                LeftHelperGridLinesSurface.Margin = new Thickness(0, 0, 0, 0);
                RightHelperGridLinesSurface.Margin = new Thickness(0, 0, 5, 0);
                TopHelperGridLinesSurface.Margin = new Thickness(0, 0, 0, 0);
                BottomHelperGridLinesSurface.Margin = new Thickness(0, 0, 0, 0);
                LeftAxis.Margin = new Thickness(5, 0, 5, 0);
                RightAxis.Margin = new Thickness(5, 0, 5, 0);
                TopAxis.Margin = new Thickness(0, 5, 0, 0);
                BottomAxis.Margin = new Thickness(0, 0, 0, 0);
            }
            else
            {
                Grid.SetColumn(LeftAxis, 1);
                Grid.SetColumn(LeftHelperGridLinesSurface, 2);
                Grid.SetColumn(RightHelperGridLinesSurface, 4);
                Grid.SetColumn(LeftHelperSurface, 2);
                Grid.SetColumn(RightHelperSurface, 4);
                Grid.SetColumn(RightAxis, 5);
                Grid.SetRow(TopAxis, 4);
                Grid.SetRow(TopHelperGridLinesSurface, 5);
                Grid.SetRow(TopHelperSurface, 5);
                Grid.SetRow(BottomAxis, 8);
                Grid.SetRow(BottomHelperGridLinesSurface, 7);
                Grid.SetRow(BottomHelperSurface, 7);
                LeftHelperSurface.Margin = new Thickness(5, 0, 0, 0);
                RightHelperSurface.Margin = new Thickness(0, 0, 5, 0);
                TopHelperSurface.Margin = new Thickness(0, 5, 0, 0);
                BottomHelperSurface.Margin = new Thickness(0, 0, 0, 5);
                LeftHelperGridLinesSurface.Margin = new Thickness(5, 0, 0, 0);
                RightHelperGridLinesSurface.Margin = new Thickness(0, 0, 5, 0);
                TopHelperGridLinesSurface.Margin = new Thickness(0, 5, 0, 0);
                BottomHelperGridLinesSurface.Margin = new Thickness(0, 0, 0, 5);
                LeftAxis.Margin = new Thickness(0, 0, 0, 0);
                RightAxis.Margin = DoubleVerticalAxis ? new Thickness(0, 0, 5, 0) : new Thickness(0, 0, 0, 0);
                TopAxis.Margin = new Thickness(0, 0, 0, 0);
                BottomAxis.Margin = new Thickness(0, 0, 0, 0);
            }
        }

        public void SetupLayout()
        {
            ChartLayout.Background = new SolidColorBrush(IsTransparent ? Colors.Transparent : 
                Inverted ? Colors.Black : Colors.White);
            SeriesSurface.Effect = !HasShadow
                ? null
                : new DropShadowEffect
                {
                    BlurRadius = 8,
                    Color = Inverted ? Colors.White : Colors.Black,
                    Direction = 300,
                    Opacity = 0.5,
                    ShadowDepth = 4
                };

            BottomHelperSurface.Effect = !HasShadow
                ? null
                : new DropShadowEffect
                {
                    BlurRadius = 8,
                    Color = Inverted ? Colors.White : Colors.Black,
                    Direction = 300,
                    Opacity = 0.5,
                    ShadowDepth = 4
                };

            TopHelperSurface.Effect = !HasShadow
                ? null
                : new DropShadowEffect
                {
                    BlurRadius = 8,
                    Color = Inverted ? Colors.White : Colors.Black,
                    Direction = 300,
                    Opacity = 0.5,
                    ShadowDepth = 4
                };

            LeftHelperSurface.Effect = !HasShadow
                ? null
                : new DropShadowEffect
                {
                    BlurRadius = 8,
                    Color = Inverted ? Colors.White : Colors.Black,
                    Direction = 300,
                    Opacity = 0.5,
                    ShadowDepth = 4
                };

            RightHelperSurface.Effect = !HasShadow
                ? null
                : new DropShadowEffect
                {
                    BlurRadius = 8,
                    Color = Inverted ? Colors.White : Colors.Black,
                    Direction = 300,
                    Opacity = 0.5,
                    ShadowDepth = 4
                };
        }

        public void ClearSelection()
        {
            BottomAxis.ClearSelection();
        }

        public Grid GetCustomSurface()
        {
            return CustomSurface;
        }

        public void ClearChart()
        {
            SeriesSurface.Children.Clear();
            GridLinesSurface.Children.Clear();
            RightLegendPlace.Children.Clear();
            LabelsSurface.Children.Clear();
            BottomLegendPlace.Children.Clear();
            TopLegendPlace.Children.Clear();
            RightAxis.Visibility = Visibility.Collapsed;
            LeftAxis.Visibility = Visibility.Collapsed;
            TopAxis.Visibility = Visibility.Collapsed;
            BottomAxis.Visibility = Visibility.Collapsed;
            ChartTitle.Blocks.Clear();
            ChartSubtitle.Blocks.Clear();
            ChartCathegoryTitle.Text = string.Empty;
            RightAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            LeftAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            UpAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            BottomAxisTitle.FontSize = 12 * GlobalFontCoefficient;
            RightAxisTitle.Text = string.Empty;
            LeftAxisTitle.Text = string.Empty;
            UpAxisTitle.Text = string.Empty;
            BottomAxisTitle.Text = string.Empty;
            var titleForeground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
            LeftAxisTitle.Foreground = titleForeground;
            RightAxisTitle.Foreground = titleForeground;
            UpAxisTitle.Foreground = titleForeground;
            BottomAxisTitle.Foreground = titleForeground;
            LeftAxis.LabelColor = titleForeground;
            RightAxis.LabelColor = titleForeground;
            TopAxis.LabelColor = titleForeground;
            BottomAxis.LabelColor = titleForeground;
        }

        private void DrawSeries()
        {
            SeriesSurface.Children.Clear();
            var xBound = BottomAxisRange.BoundSeries.Union(TopAxisRange.BoundSeries).ToList();
            var yBound = RightAxisRange.BoundSeries.Union(LeftAxisRange.BoundSeries).ToList();
            var zBound = ZAxisRange.BoundSeries;
            var wBound = WAxisRange.BoundSeries;
            var vBound = VAxisRange.BoundSeries;
            SeriesSurface.UpdateLayout();

            foreach (var sri in xBound)
            {
                if (sri.SourceSeries.SeriesType == ChartSeriesType.EditableTableOnly) continue;
                var xsri = sri;
                var ysri = (from y in yBound where y.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select y).SingleOrDefault();
                var zsri = (from z in zBound where z.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select z).SingleOrDefault();
                var wsri = (from w in wBound where w.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select w).SingleOrDefault();
                var vsri = (from v in vBound where v.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select v).SingleOrDefault();
                DrawSingleSeries(xsri, ysri, zsri, wsri, vsri, sri.SourceSeries.SeriesType);
            }
            foreach (var sri in yBound.Where(a => !xBound.Select(b => b.SeriesName).Contains(a.SeriesName)))
            {
                if (sri.SourceSeries.SeriesType == ChartSeriesType.EditableTableOnly) continue;
                var xsri =(from x in xBound where x.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select x).SingleOrDefault();
                var ysri = sri;
                var zsri =(from z in zBound where z.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select z).SingleOrDefault();
                var wsri =(from w in wBound where w.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select w).SingleOrDefault();
                var vsri =(from v in vBound where v.SourceSeries.SeriesTitle == sri.SourceSeries.SeriesTitle select v).SingleOrDefault();
                DrawSingleSeries(xsri, ysri, zsri, wsri, vsri, sri.SourceSeries.SeriesType);
            }
        }

        private void BottomAxis_AxisValueSelectionChanged(object o, AxisValueSelectionEventArgs e)
        {
            AxisValueSelectionChanged?.Invoke(this, e);
        }

        private void PartialRefresh()
        {
            if (ChartData == null) return;
            IsBusy = true;
            DrawSeries();
            DrawFunctions();
            DrawGridLines();
            IsBusy = false;
        }

        private void Axis_OnAxisClicked(object o, AxisEventArgs e)
        {
            if (AxisClicked != null) AxisClicked.Invoke(o, e);
        }
        private void SeriesSurface_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var clip = new Rect() { Height = e.NewSize.Height, Width = e.NewSize.Width, X = 0, Y = 0 };
            ClippingRectangle.Rect = clip;
            ClippingRectangle2.Rect = clip;
            RightAxisTitle.MaxHeight = e.NewSize.Height;
            LeftAxisTitle.MaxHeight = e.NewSize.Height;
        }

        private void InitSeriesDictionary()
        {
            _seriesDictionary = new Dictionary<ChartSeriesType, DrawSeriesDelegate>
            {
                {ChartSeriesType.LineXY, DrawScatterplot},
                {ChartSeriesType.ConstantX, DrawConstantXLine},
                {ChartSeriesType.ConstantY, DrawConstantYLine},
                {ChartSeriesType.RectangleX, DrawConstantXRectangle},
                {ChartSeriesType.RectangleY, DrawConstantYRectangle},
                {ChartSeriesType.StairsOnX, DrawStairsOnX},
                {ChartSeriesType.StairsOnY, DrawStairsOnY},
                {ChartSeriesType.CircularGraph, DrawCircularGraph},
                {ChartSeriesType.BoxWhiskers, DrawViolaAndWiskers},
                {ChartSeriesType.BeeSwarmOnFloor, DrawBeeSwarmOnFloor},
                {ChartSeriesType.RugOnX, DrawRugOnX},
                {ChartSeriesType.RugOnY, DrawRugOnY},
                {ChartSeriesType.KDEOnX, DrawKDEOnX},
                {ChartSeriesType.KDEOnY, DrawKDEOnY},
                {ChartSeriesType.RangeOnX, DrawRangeOnX},
                {ChartSeriesType.RangeOnY, DrawRangeOnY},
                {ChartSeriesType.Surface, DrawSurface},
                {ChartSeriesType.Voronoy, DrawVoronoy},
                {ChartSeriesType.Bag, DrawBag},
                {ChartSeriesType.HexBin, DrawHexBin},
                {ChartSeriesType.Circles, DrawCircles },
                {ChartSeriesType.Radar, DrawRadar },
                {ChartSeriesType.Segments, DrawSegments },
                {ChartSeriesType.Sun, DrawSun}
            };
        }
    }

    public class ChartRangesInfo
    {
        public object XDownMax;
        public object XDownMin;
        public List<string> XDownSeries; 
        public object XUpMax;
        public object XUpMin;
        public List<string> XUpSeries;
        public object YLeftMax;
        public object YLeftMin;
        public List<string> YLeftSeries;
        public object YRightMax;
        public object YRightMin;
        public List<string> YRightSeries;
        public object ZMax;
        public object ZMin;
        public List<string> ZSeries;
        public object WMax;
        public object WMin;
        public List<string> WSeries;
        public object VMax;
        public object VMin;
        public List<string> VSeries;
    }

    public class InteractivityIndex
    {
        public int Index { get; set; }
        public int IndexInGroup { get; set; }
        public string SeriesName { get; set; }
        public object Layer { get; set; }
        public string LayerName { get; set; }
    }
}