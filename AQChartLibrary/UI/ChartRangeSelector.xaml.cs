﻿using System;
using System.Windows;


// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    public partial class ChartRangeSelector 
    {
        public delegate void RangeChangedDelegate(object o, RangeChangedEventArgs e);

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof (double), typeof (ChartRangeSelector),
                                        new PropertyMetadata(0.0d, MinValueChangedCallBack));

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof (double), typeof (ChartRangeSelector),
                                        new PropertyMetadata(1d, MaxValueChangedCallBack));

        public static readonly DependencyProperty UpValueProperty =
            DependencyProperty.Register("UpValue", typeof (double), typeof (ChartRangeSelector),
                                        new PropertyMetadata(0.0d, UpValueChangedCallBack));

        public static readonly DependencyProperty DownValueProperty =
            DependencyProperty.Register("DownValue", typeof (double), typeof (ChartRangeSelector),
                                        new PropertyMetadata(1d, DownValueChangedCallBack));

        public static readonly DependencyProperty StepProperty =
            DependencyProperty.Register("Step", typeof (double), typeof (ChartRangeSelector),
                                        new PropertyMetadata(0.1d, StepChangedCallBack));

        private bool _isInitialized;

        public ChartRangeSelector()
        {
            InitializeComponent();
        }

        public double MinValue
        {
            get { return (double) GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public double MaxValue
        {
            get { return (double) GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public double UpValue
        {
            get { return (double) GetValue(UpValueProperty); }
            set { SetValue(UpValueProperty, value); }
        }

        public double DownValue
        {
            get { return (double) GetValue(DownValueProperty); }
            set { SetValue(DownValueProperty, value); }
        }

        public double Step
        {
            get { return (double) GetValue(StepProperty); }
            set { SetValue(StepProperty, value); }
        }

        public event RangeChangedDelegate RangeChanged;

        private static void MinValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (ChartRangeSelector) o;
            r.UpValueSlider.Minimum = (double) e.NewValue;
            r.DownValueSlider.Minimum = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void MaxValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (ChartRangeSelector) o;
            r.UpValueSlider.Maximum = (double) e.NewValue;
            r.DownValueSlider.Maximum = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void UpValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (ChartRangeSelector) o;
            r.UpValueSlider.Value = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void DownValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (ChartRangeSelector) o;
            r.DownValueSlider.Value = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void StepChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (ChartRangeSelector) o;
            r.RefreshControl();
            r.UpValueSlider.SmallChange = (double) e.NewValue;
            r.DownValueSlider.SmallChange = (double) e.NewValue;
            r.UpValueSlider.LargeChange = 5*(double) e.NewValue;
            r.DownValueSlider.LargeChange = 5*(double) e.NewValue;
        }

        private void UpValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (UpValueSlider.Value < DownValueSlider.Value) DownValueSlider.Value = UpValueSlider.Value;
            if (_isInitialized) UpValue = UpValueSlider.Value;
            RefreshControl();
            if (RangeChanged != null)
                RangeChanged(this,
                             new RangeChangedEventArgs
                                 {
                                     UpValue = UpValueSlider.Value,
                                     DownValue = DownValueSlider.Value
                                 });
        }

        private void DownValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (DownValueSlider.Value > UpValueSlider.Value) UpValueSlider.Value = DownValueSlider.Value;
            if (_isInitialized) DownValue = DownValueSlider.Value;
            RefreshControl();
            if (RangeChanged != null)
                RangeChanged(this,
                             new RangeChangedEventArgs
                                 {
                                     UpValue = UpValueSlider.Value,
                                     DownValue = DownValueSlider.Value
                                 });
        }

        private void RefreshSelectedZone()
        {
            RangeView.UpdateLayout();
            Divider.UpdateLayout();
            var actualHeight = Divider.ActualHeight;
            if (MaxValue - MinValue > 0)
            {
                var zoneNormedSize = actualHeight/(MaxValue - MinValue);
                var offset = Math.Abs(DownValue - MinValue)*zoneNormedSize;
                var height = Math.Abs(UpValue - DownValue)*zoneNormedSize;
                SelectedZoneOffset.Height = offset;
                SelectedZoneRectangle.Height = height;
            }
            else
            {
                SelectedZoneOffset.Height = 0;
                SelectedZoneRectangle.Height = 0;
            }
        }

        private void ChartRangeSelector_Loaded(object sender, RoutedEventArgs e)
        {
            _isInitialized = true;
            RefreshControl();
        }

        private void RefreshControl()
        {
            if (!_isInitialized) return;
            UpValueSlider.Value = UpValue;
            DownValueSlider.Value = DownValue;
            UpValueSlider.Minimum = MinValue;
            UpValueSlider.Maximum = MaxValue;
            DownValueSlider.Minimum = MinValue;
            DownValueSlider.Maximum = MaxValue;
            UpValueSlider.SmallChange = Step;
            DownValueSlider.SmallChange = Step;
            UpValueSlider.LargeChange = Step;
            DownValueSlider.LargeChange = Step;

            RefreshSelectedZone();
        }

        private void Divider_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RefreshControl();
        }
    }
    public class RangeChangedEventArgs : EventArgs
    {
        public double UpValue { get; set; }
        public double DownValue { get; set; }
    }
}