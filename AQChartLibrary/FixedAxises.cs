﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;


// ReSharper disable once CheckNamespace
namespace AQChartLibrary
{
    
    public class FixedAxises
    {
        public ObservableCollection<FixedAxis> FixedAxisCollection { get; set; }
        public bool Editable = false;
    };

    public class FixedAxis : INotifyPropertyChanged, IDataErrorInfo
    {
        public FixedAxis()
        {
            AxisContinuous = true;
        }

        private bool _valid;
        public bool IsValid
        {
            get
            {
                return _valid;
            }
            set
            {
                _valid = value;
                NotifyPropertyChanged("IsValid");
            }
        }

        private double? _axismin;
        public double? AxisMin
        {
            get
            {
                return _axismin;
            }
            set
            {
                IsValid = true;
                _axismin = value;
                NotifyPropertyChanged("AxisMin");
            }
        }

        private DateTime? _axismindate;
        public DateTime? AxisMinDate
        {
            get
            {
                return _axismindate;
            }
            set
            {
                IsValid = _axismindate <= _axismaxdate;
                _axismindate = value;
                NotifyPropertyChanged("AxisMinDate");
            }
        }

        private bool _axisminfixed;
        public bool AxisMinFixed
        {
            get
            {
                return _axisminfixed;
            }
            set
            {
                _axisminfixed = value;
                NotifyPropertyChanged("AxisMinFixed");
            }
        }

        private bool _axisminlocked;
        public bool AxisMinLocked
        {
            get
            {
                return _axisminlocked;
            }
            set
            {
                _axisminlocked = value;
                NotifyPropertyChanged("AxisMinLocked");
            }
        }
        
        private double? _axismax;
        public double? AxisMax
        {
            get
            {
                return _axismax;
            }
            set
            {
                IsValid = true;
                _axismax = value;
                NotifyPropertyChanged("AxisMax");
            }
        }


        private DateTime? _axismaxdate;
        public DateTime? AxisMaxDate
        {
            get
            {
                return _axismaxdate;
            }
            set
            {
                IsValid = _axismindate <= _axismaxdate;
                _axismaxdate = value;
                NotifyPropertyChanged("AxisMaxDate");
            }
        }

        private bool _axismaxfixed;
        public bool AxisMaxFixed
        {
            get
            {
                return _axismaxfixed;
            }
            set
            {
                _axismaxfixed = value;
                NotifyPropertyChanged("AxisMaxFixed");
            }
        }

        private bool _axiscontinuous;
        public bool AxisContinuous
        {
            get
            {
                return _axiscontinuous;
            }
            set
            {
                _axiscontinuous = value;
                NotifyPropertyChanged("AxisContinuous");
            }
        }

        public bool AxisDiscrete
        {
            get
            {
                return !_axiscontinuous;
            }
            set
            {
                _axiscontinuous = !value;
                NotifyPropertyChanged("AxisDiscrete");
            }
        }

        private bool _axismaxlocked;
        public bool AxisMaxLocked
        {
            get
            {
                return _axismaxlocked;
            }
            set
            {
                _axismaxlocked = value;
                NotifyPropertyChanged("AxisMaxLocked");
            }
        }
        
        private double _step;
        public double Step
        {
            get
            {
                return _step;
            }
            set
            {
                try
                {
                    IsValid = true;
                    _step = value;
                    NotifyPropertyChanged("Step"); 
                }
                catch (Exception)
                {

                    IsValid = false;

                } 
                
            }
        }

        private bool _stepfixed;
        public bool StepFixed
        {
            get
            {
                return _stepfixed;
            }
            set
            {
                _stepfixed = value;
                NotifyPropertyChanged("StepFixed");
            }
        }

        private bool _stringIsDateTime;
        public bool StringIsDateTime
        {
            get
            {
                return _stringIsDateTime;
            }
            set
            {
                _stringIsDateTime = value;
                NotifyPropertyChanged("StringIsDateTime");
            }
        }

        private string _format;
        public string Format
        {
            get
            {
                return _format;
            }
            set
            {
                _format = value;
                NotifyPropertyChanged("Format");
            }
        }

        private string _dataType;
        public string DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value;
                NotifyPropertyChanged("DataType");
            }
        }

        private bool _steplocked;
        public bool StepLocked
        {
            get
            {
                return _steplocked;
            }
            set
            {
                _steplocked = value;
                NotifyPropertyChanged("StepLocked");
            }
        }

        private string _axis;
        public string Axis
        {
            get
            {
                return _axis;
            }
            set
            {
                _axis = value;
                NotifyPropertyChanged("Axis");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Error
        {
            get { return ""; }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Step":
                        if (Math.Abs(Step) < double.Epsilon) return "Шаг = 0";
                        if (AxisMax.HasValue && AxisMin.HasValue)
                        {
                            var a = (AxisMax.Value - AxisMin.Value)/_step;
                            if (StepFixed && AxisMaxFixed && AxisMinFixed && Math.Abs(a - Math.Round(a)) > 1e-7)
                            {
                                IsValid = false;
                                return "Интервал не кратен шагу";
                            }
                        }
                        break;
                    case "AxisMin":
                    case "AxisMax":
                        if (AxisMin > AxisMax)
                        {
                            IsValid = false;
                            return "Min > Max";
                        }
                        break;

                    case "AxisMinDate":
                    case "AxisMaxDate":
                        if (AxisMinDate > AxisMaxDate)
                        {
                            IsValid = false;
                            return "Min > Max";
                        }
                        break;
                }
                IsValid = true;
                return null;
            }
        }
    };
}
