﻿using System;
using System.Windows;
using System.Windows.Browser;
using AQBasicControlsLibrary;

namespace SLTitle
{
    public partial class Technologies
    {
        public Technologies()
        {
            InitializeComponent();
        }

        private void Link_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri(((TextImageButtonBase)(sender)).Tag.ToString()), "_blank");
        }
    }
}
