﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using ApplicationCore;
using AQSelection;
using OpenRiaServices.Controls;

namespace SLTitle
{
    public partial class NewsPresenter 
    {
        public delegate void NewsLoadedDelegate(object o, EventArgs e);
        public event NewsLoadedDelegate NewsLoaded;
        public event NewsLoadedDelegate NewsLoading;
        private int _loadedItemsCounter;
        
        public NewsPresenter()
        {
            InitializeComponent();
            ((RightsForNewsEdit)Resources["RightsForNewsEdit"]).ShowEditButtons = Security.IsInRole("Администратор") ? Visibility.Visible : Visibility.Collapsed;
        }

        public void Rewind()
        {
            NewsScrollViewer.ScrollToVerticalOffset(0);
            NewsScrollViewer.UpdateLayout();
        }
        
        public void RefreshNews()
        {
            if (Security.Roles == null) return;
            AQ_NewsDomainDataSource.Load();
        }

        private void CancelNewsEdit_Click(object sender, RoutedEventArgs e)
        {
            AQ_NewsDomainDataSource.RejectChanges();
            EditNewsWindow.Visibility = Visibility.Collapsed;
            NewsListBox.IsEnabled = true;
        }

        private void SaveNewsEdit_Click(object sender, RoutedEventArgs e)
        {
            EditNewsWindow.Visibility = Visibility.Collapsed;
            NewsListBox.IsEnabled = true;
            AQ_NewsDomainDataSource.SubmitChanges();
        }

        private void EditNews_Click(object sender, RoutedEventArgs e)
        {
            NewsListBox.IsEnabled = false;
            AQ_NewsDomainDataSource.DataView.Cast<AQ_News>().SingleOrDefault(i => i.id == ((int)((TextImageButtonBase)sender).Tag));
            EditNewsWindow.Visibility = Visibility.Visible;
        }

        private void DeleteNews_Click(object sender, RoutedEventArgs e)
        {
            var ci = (int)((TextImageButtonBase)sender).Tag;
            var toDelete = AQ_NewsDomainDataSource.DataView.Cast<AQ_News>().SingleOrDefault(i => i.id == ci);
            var mbr = MessageBox.Show("Удалить новость ?", "Подтверждение", MessageBoxButton.OKCancel);
            if (mbr != MessageBoxResult.OK) return;
            AQ_NewsDomainDataSource.DataView.Remove(toDelete);
            AQ_NewsDomainDataSource.SubmitChanges();
        }

        private void AddNews_Click(object sender, RoutedEventArgs e)
        {
            EditNewsWindow.Visibility = Visibility.Visible;
            NewsListBox.IsEnabled = false;
            var news = new AQ_News {Date = DateTime.Now, InnerHTML = String.Empty};
            AQ_NewsDomainDataSource.DataView.Add(news);
            AQ_NewsDomainDataSource.DataView.MoveCurrentTo(news);
        }

        private void AQ_NewsDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            AQ_NewsDomainDataSource.Load();
        }

        private void AQ_NewsDomainDataSource_LoadedData(object sender, LoadedDataEventArgs e)
        {
            _loadedItemsCounter = 0;
        }

        private void PseudoHTMLBox_OnPicturesLoaded(object o, EventArgs e)
        {
            _loadedItemsCounter++;
            if (NewsLoaded != null && _loadedItemsCounter == NewsListBox.Items.Count) NewsLoaded.Invoke(this, new EventArgs());
        }

        private void DataPager_OnPageIndexChanging(object sender, CancelEventArgs e)
        {
            _loadedItemsCounter = 0;
            if (NewsLoading!=null)NewsLoading.Invoke(this, new EventArgs());
        }
    }
}
