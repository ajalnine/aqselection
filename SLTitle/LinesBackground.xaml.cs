﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLTitle
{
    public partial class LinesBackground
    {
        private readonly Storyboard _s = new Storyboard{BeginTime = TimeSpan.FromMilliseconds(0), Duration = TimeSpan.FromMilliseconds(50)};

        private double _animationPosition = 530;
        private const double AnimationStep = 5e-2;
        private const double LinesNumber = 20;
        private bool _isFirstRun = true;
        private readonly Brush _brush = new LinearGradientBrush
        {
            StartPoint = new Point(0, 0),
            EndPoint = new Point(1, 0),
            GradientStops = new GradientStopCollection
            {
                new GradientStop{Color = Color.FromArgb(0x00,0xff,0xff,0xff), Offset = 0},
                new GradientStop{Color = Color.FromArgb(0x4F,0x26,0x8f,0x97), Offset = 0.2},
                new GradientStop{Color = Color.FromArgb(0x5F,0x26,0x8f,0x97), Offset = 0.6},
                new GradientStop{Color = Color.FromArgb(0x00,0xff,0xff,0xff), Offset = 1}
            }
        };

        private const double FirstLineY1 = 0;
        private const double FirstLineY2 = 40;
        private const double FirstLineY3 = 50;
        private const double FirstLineY4 = 70;
        private const double Period1LinesYDifference = 3;
        private const double Period2LinesYDifference = 2;
        private const double Period3LinesYDifference = 2;
        private const double Period4LinesYDifference = 2;

        public LinesBackground()
        {
            _s.Completed += AdvanceAnimation;
            InitializeComponent();
            _s.Begin();
        }

        void AdvanceAnimation(object sender, EventArgs e)
        {
            _animationPosition += AnimationStep;
            RefreshLines();
            _s.Begin();
        }

        private void RefreshLines()
        {
            var y1 = FirstLineY1 + Period1LinesYDifference * Math.Sin(_animationPosition);
            var y2 = FirstLineY2 + Period2LinesYDifference * Math.Sin(_animationPosition*0.3);
            var y3 = FirstLineY3 + Period3LinesYDifference * Math.Sin(_animationPosition*0.1);
            var y4 = FirstLineY4 + Period4LinesYDifference * Math.Sin(_animationPosition*0.7);
            for (double i = 0; i < LinesNumber; i++)
            {
                if (_isFirstRun)
                {
                    DrawLine(y1, y2, y3, y4);
                    DrawLine(y2, y3, y4, y1);
                }
                else
                {
                    MoveLine(y1, y2, y3, y4, (int)i * 2);
                    MoveLine(y2, y3, y4, y1, (int)i * 2 + 1);
                }
                y1 += Period1LinesYDifference + 3 * Math.Sin(_animationPosition) + Period1LinesYDifference * Math.Sin(_animationPosition * .11 + 3);
                y2 += Period2LinesYDifference + 3 * Math.Sin(_animationPosition * 1.1) + Period2LinesYDifference * Math.Sin(_animationPosition * .11+1);
                y3 += Period3LinesYDifference + 3 * Math.Sin(_animationPosition * 1.2) + Period3LinesYDifference * Math.Sin(_animationPosition * .31+2);
                y4 += Period4LinesYDifference + 3 * Math.Sin(_animationPosition * 0.3) + Period4LinesYDifference * Math.Sin(_animationPosition * .21); 
            }
            _isFirstRun = false;
        }

        private void DrawLine(double y1, double y2, double y3, double y4)
        {
            var p = new Path {Stroke = _brush, StrokeThickness = 1, Fill = _brush, Opacity = 0.5};
            var pathData = new PathGeometry();
            var pf = new PathFigure {IsClosed = false, IsFilled = false, StartPoint = new Point(0, y1)};
            pf.Segments.Add(new BezierSegment {Point1 =  new Point(100, y2), Point2 = new Point(400, y3), Point3 = new Point(460, y4)});
            pathData.Figures.Add(pf);
            p.Data = pathData;
            LayoutRoot.Children.Add(p);
        }
        private void MoveLine(double y1, double y2, double y3, double y4, int index)
        {
            if (LayoutRoot.Children.Count() - 1 < index) return;
            var line =  LayoutRoot.Children[index] as Path;
            if (line == null) return;
            var pathData = new PathGeometry();
            var pf = new PathFigure {IsClosed = false, IsFilled = false, StartPoint = new Point(0, y1)};
            pf.Segments.Add(new BezierSegment { Point1 = new Point(100, y2), Point2 = new Point(400, y3), Point3 = new Point(460, y4) });
            pathData.Figures.Add(pf);
            line.Data = pathData;
        }
    }
}
