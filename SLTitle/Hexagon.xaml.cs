﻿using System.Windows;
using System.Windows.Media;

namespace SLTitle
{
    public partial class Hexagon
    {



        public Color Highlight  
        {
            get { return (Color)GetValue(HighlightProperty); }
            set { SetValue(HighlightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HighlightProperty =
            DependencyProperty.Register("Highlight", typeof(Color), typeof(Hexagon), new PropertyMetadata(Color.FromArgb(0x1f, 0x2F, 0x8f, 0x97)));
        public Color Stroke
        {
            get { return (Color)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeProperty =
            DependencyProperty.Register("Stroke", typeof(Color), typeof(Hexagon), new PropertyMetadata(Color.FromArgb(0xff, 0x2F, 0x8f, 0x97)));
        
        public Hexagon()
        {
            InitializeComponent();
        }
    }
}
