﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQReportingLibrary;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        void irc_ReportInteractiveRename(object reportchart, ReportInteractiveRenameEventArgs e)
        {
            if (e.RenameOperations == null || e.RenameOperations.Count == 0) return;
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Range)) MainRangeModeSelector.RenameRange(e.RenameOperations);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.DateRange)) MainDateRangeSelector.RenamePeriod(e.RenameOperations);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Field)) RenameFieldsInSelectedTable(e);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Layer)) RenameLayerInSelectedTable(e);
            if (e.RenameOperations.Any(a => a.RenameTarget == ReportRenameTarget.Table)) RenameTable(e);
        }

        private void RenameFieldsInSelectedTable(ReportInteractiveRenameEventArgs reportInteractiveRenameEventArgs)
        {
            var renames = new Dictionary<string, string>();
            foreach (var ro in reportInteractiveRenameEventArgs.RenameOperations
                .Where(a => a.RenameTarget == ReportRenameTarget.Field && a.Table == _selectedTable.TableName))
            {
                while (_selectedTable.ColumnNames.Select(a => a.Trim().ToLower()).Contains(ro.NewName.Trim().ToLower()))ro.NewName += "_";
                var renameFieldIndex =
                    _selectedTable.ColumnNames.Select(a => a.Trim().ToLower())
                        .ToList()
                        .IndexOf(ro.OldName.Trim().ToLower());
                if (renameFieldIndex > -1)
                {
                    var oldname = _selectedTable.ColumnNames[renameFieldIndex];
                    renames.Add(ro.NewName, oldname);
                    _selectedTable.ColumnNames[renameFieldIndex] = ro.NewName;
                    RenamePostfixField(oldname, "_Цвет", ro, renames);
                    RenamePostfixField(oldname, "_Метка", ro, renames);
                    RenamePostfixField(oldname, "_НГ", ro, renames);
                    RenamePostfixField(oldname, "_ВГ", ro, renames);
                }
            }
            
            foreach (var ro in reportInteractiveRenameEventArgs.RenameOperations
                .Where(a => a.RenameTarget == ReportRenameTarget.Field && a.Table == _selectedTable.TableName))
            {
                _selectedTable.RenameFilter(ro.OldName, ro.NewName);
            }
            
            _innerSteps.AddRange(StepFactory.CreateRenameFields(_selectedTable, renames));
            ReportInteractiveRename?.Invoke(this, reportInteractiveRenameEventArgs);
            MainFieldsSelector.RenameFields(reportInteractiveRenameEventArgs.RenameOperations);
        }

        private void RenamePostfixField(string oldname, string postfix, ReportRenameOperation ro, Dictionary<string, string> renames)
        {
            var oldprefixname = "#" + oldname + postfix;
            var newprefixname = "#" + ro.NewName + postfix;
            if (!_selectedTable.ColumnNames.Contains(oldprefixname))
            {
                oldprefixname = oldname + postfix;
                if (!_selectedTable.ColumnNames.Contains(oldprefixname)) return;
                else newprefixname = ro.NewName + postfix;
            }
            renames.Add(newprefixname, oldprefixname);
            _selectedTable.ColumnNames[_selectedTable.ColumnNames.IndexOf(oldprefixname)] = newprefixname;
        }

        private void RenameTable(ReportInteractiveRenameEventArgs e)
        {
            var tableRename = e.RenameOperations.SingleOrDefault(a => a.RenameTarget == ReportRenameTarget.Table);
            if (tableRename == null) return;
            if (_selectedTable.TableName == tableRename.OldName)
            {
                while (_dataTables.Select(a => a.TableName.Trim().ToLower()).Contains(tableRename.NewName.Trim().ToLower())) tableRename.NewName += "_";
                _innerSteps.AddRange(StepFactory.CreateRenameTable(_dataTables, _selectedTable.TableName, tableRename.NewName));
                _selectedTable.TableName = tableRename.NewName;
            }
            ReportInteractiveRename?.Invoke(this, e);

            MakeReportChart(false);
        }

        private void RenameLayerInSelectedTable(ReportInteractiveRenameEventArgs e)
        {
            var layerRename = e.RenameOperations.SingleOrDefault(a => a.RenameTarget == ReportRenameTarget.Layer);
            if (layerRename == null || string.IsNullOrEmpty(layerRename.LayerField)) return;
            var zfieldIndex  = _selectedTable.ColumnNames.IndexOf(layerRename.LayerField);
            var dataType = _selectedTable.DataTypes[zfieldIndex];

            object _oldValue = null, _newValue = null;

            if (dataType.EndsWith("DateTime"))
            {
                DateTime newValue;
                if (!DateTime.TryParse(layerRename.NewName, out newValue)) return;
                DateTime oldValue;
                if (!DateTime.TryParse(layerRename.OldName, out oldValue)) return;
                foreach (var row in _selectedTable.Table)
                {
                    if (!(row.Row[zfieldIndex] is DateTime)) continue;
                    if (row.Row != null && (DateTime)row.Row[zfieldIndex] == oldValue) row.Row[zfieldIndex] = newValue;
                }
                _oldValue = oldValue;
                _newValue = newValue;
                _selectedTable.ChangeFilterValues(layerRename.LayerField, oldValue, newValue);
            }

            if (dataType.EndsWith("TimeSpan"))
            {
                TimeSpan newValue;
                if (!TimeSpan.TryParse(layerRename.NewName, out newValue)) return;
                TimeSpan oldValue;
                if (!TimeSpan.TryParse(layerRename.OldName, out oldValue)) return;
                foreach (var row in _selectedTable.Table)
                {
                    if (!(row.Row[zfieldIndex] is TimeSpan)) continue;
                    if (row.Row != null && (TimeSpan)row.Row[zfieldIndex] == oldValue) row.Row[zfieldIndex] = newValue;
                }
                _oldValue = oldValue;
                _newValue = newValue;
                _selectedTable.ChangeFilterValues(layerRename.LayerField, oldValue, newValue);
            }

            if (dataType.EndsWith("Int32"))
            {
                int newValue;
                if (!int.TryParse(layerRename.NewName, out newValue)) return;
                int oldValue;
                if (!int.TryParse(layerRename.OldName, out oldValue)) return;
                foreach (var row in _selectedTable.Table)
                {
                    if (!(row.Row[zfieldIndex] is int)) continue;
                    if (row.Row != null && (int)row.Row[zfieldIndex] == oldValue) row.Row[zfieldIndex] = newValue;
                }
                _oldValue = oldValue;
                _newValue = newValue;
                _selectedTable.ChangeFilterValues(layerRename.LayerField, oldValue, newValue);
            }

            if (dataType.EndsWith("Double"))
            {
                double newValue;
                if (!double.TryParse(layerRename.NewName, out newValue)) return;
                double oldValue;
                if (!double.TryParse(layerRename.OldName, out oldValue)) return;
                foreach (var row in _selectedTable.Table)
                {
                    if (!(row.Row[zfieldIndex] is double)) continue;
                    if (row.Row != null && Math.Abs((double)row.Row[zfieldIndex] - oldValue) < double.Epsilon) row.Row[zfieldIndex] = newValue;
                }
                _oldValue = oldValue;
                _newValue = newValue;
                _selectedTable.ChangeFilterValues(layerRename.LayerField, oldValue, newValue);
            }

            if (dataType.EndsWith("String"))
            {
                foreach (var row in _selectedTable.Table)
                {
                    var oldvalue = row?.Row[zfieldIndex]?.ToString();
                    if (oldvalue == layerRename.OldName) row.Row[zfieldIndex] = layerRename.NewName;
                }
                _oldValue = layerRename.OldName;
                _newValue = layerRename.NewName;
                _selectedTable.ChangeFilterValues(layerRename.LayerField, layerRename.OldName, layerRename.NewName);
            }

            Data.LDRCache.Clear();
            _innerSteps.Add(StepFactory.CreateLayerOperationCombineLayers(_selectedTable.TableName, layerRename.LayerField, layerRename.LayerField, new List<object>{_oldValue}, _newValue ));
            ReportInteractiveRename?.Invoke(this, e);
            MakeReportChart(false);
        }
    }
}
       