﻿
using System;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ApplicationCore.CalculationServiceReference;
using AQChartViewLibrary.UI;
using AQChartLibrary;
using AQReportingLibrary;
using System.Windows.Media;


// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private const int LargeData = 20000;
        #region События интерфейса

        public FixedAxises AnalysisFixedAxises;

        private void MainFieldsSelector_FieldsChanged(object sender, FieldsChangedEventArgs e)
        {
            switch (e.ChangedField)
            {
                case ChangedFieldType.Renew:
                    return;
                case ChangedFieldType.SelectionMode:
                    SetupFieldSelector();
                    break;
            }
            SetupToolsAvailability();
            SetupZVisibilityForSIngleVariable();
            SetupRangeVisibility();
            if (e.ChangedField == ChangedFieldType.Flip) MainRangeModeSelector.FlipUserRanges();

            if ((e.YFields != null || e.XField != null))
            {
                RefreshFieldMarks();
                MakeReportChart(!MainFieldsSelector.IsMultiSelection());
            }
            SetRegressionAttention();
            HideInteractivityCommandSelector();
            ClosePopups();
        }

        private void MainChartTypeSelector_ChartTypeChanged(object sender, ChartTypeChangedEventArgs e)
        {
            SetupBestOptions();
            AnalysisFixedAxises = _fixedAxisesCache.ContainsKey(e.ChartType) ? _fixedAxisesCache[e.ChartType] : null;
            SetupMultipleSelection();
            SetupToolsAvailability();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            HideInteractivityCommandSelector();
            MakeReportChart(true);
            ClosePopups();
        }

        private void MainHistoryNavigation_HistoryPositionChanged(object o, UI.HistoryPositionChangedEventArgs e)
        {
            SetUIbyStep(e.SelectedStep);
            RefreshChartPanelVisibility();
            SetupFieldSelector();
            MainFieldsSelector.SelectAll(StepXFieldToSelect, StepYFieldsToSelect, StepZFieldToSelect, StepWFieldToSelect, StepVFieldToSelect);
            HideInteractivityCommandSelector();
            MakeReportChart(false);
        }

        private void MainMarkerSelector_OnMarkerModeChanged(object sender, MarkerChangedEventArgs e)
        {
            RefreshReportChart();
            ClosePopups();
            HideInteractivityCommandSelector();
        }

        private void MainHistogramLabelsSelector_HistogramLabelsChanged(object sender, UI.HistogramLabelsChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void RangeModeSelector_RangeModeChanged(object sender, RangeModeChangedEventArgs e)
        {
            if (e.OnlyStyleChanged)
            {
                RefreshReportChart();
                return;
            }
            HideInteractivityCommandSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void RangeDateRangeSelector_RangeChanged(object sender, DateRangeChangedEventArgs e)
        {
            if (e.OnlyStyleChanged)
            {
                RefreshReportChart();
                return;
            }
            HideInteractivityCommandSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void SeriesCheckSelector_SeriesCheckChanged(object sender, SeriesCheckChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void StatisticsSelector_StatisticsChanged(object sender, StatisticsChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void MainFitSelector_OnFitChanged(object sender, FitChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            if (e.OnlyStyleChanged)
            {
                RefreshReportChart();
                return;
            }
            MakeReportChart(false);
            ClosePopups();
        }

        private void CardTypeSelector_CardTypeChanged(object sender, CardTypeChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            SetupBestOptions();
            SetupToolsAvailability();
            SetupMultipleSelection();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void MainHistogramTypeSelector_OnHistogramTypeChanged(object sender, HistogramTypeChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            AnalysisFixedAxises = null;
            if (e.HistogramType != ReportChartHistogramType.Circles)
            {
                MainFrequencyParameterSelector.FrequencyInPercentOfGroup.IsEnabled = MainColumnsParameterSelector.GetCurrentColumnDiagramType() != ReportChartColumnDiagramType.Generic;
                MainFrequencyParameterSelector.SetYModeForLayers(MainColumnsParameterSelector.GetCurrentColumnDiagramType() == ReportChartColumnDiagramType.Layers);
                MainColumnsParameterSelector.SetOrientation(e.HistogramType == ReportChartHistogramType.Bands ? Orientation.Horizontal : Orientation.Vertical);
            }
            SetupBestOptions();
            SetupMultipleSelection();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void MainAggregateSelector_OnAggregateChanged(object sender, AggregateChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            RefreshChartPanelVisibility();
            SetupFieldSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void SmoothnessSelector_SmoothnessChanged(object sender, SmoothChangedEventArgs e)
        {
            if (e.LayersMode == ReportChartLayerMode.TableOnly || e.LayersMode == ReportChartLayerMode.Extended) MainFieldsSelector.SelectAllY();
            HideInteractivityCommandSelector();
            SetupFieldSelector();
            SetupRangeVisibility();
            if (e.OnlyStyleChanged)
            {
                RefreshReportChart();
                return;
            }
            MakeReportChart(false);
            ClosePopups();
        }

        private void ToolsSelector_ToolsChanged(object sender, ToolsChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            SetupToolsAvailability();
            MakeReportChart(false);
            ClosePopups();
        }

        private void RegressionSelector_RegressionChanged(object sender, RegressionChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            if (e.OnlyStyleChanged)
            {
                RefreshReportChart();
                return;
            }
            MakeReportChart(false);
            ClosePopups();
        }

        private void GroupParametersSelector_GroupParametersChanged(object sender, GroupParametersChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void ParetoParametersSelector_ParetoParametersChanged(object sender, ParetoParametersChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            ClosePopups();
        }

        private void MainLayoutModeSelector_OnLayoutModeChanged(object sender, LayoutModeChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            SetBackground();
            if (e.RequiredRecalc) MakeReportChart(false); 
            else RefreshReportChart();
            ClosePopups();
        }

        private void SetBackground()
        {
            var bg = (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted
                ? new SolidColorBrush(Colors.Black)
                : new SolidColorBrush(Colors.White);
            MainViewPanel.Background = bg;
            MainInteractivityCommandSelector.Background = bg;
        }

        private void FrequencyParameterSelector_FrequencyParametersChanged(object sender, FrequencyParametersChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            MakeReportChart(false);
            CheckStep(e);
            ClosePopups();
        }

        private void ColumnsParameterSelector_ColumnsParametersChanged(object sender, ColumnsParametersChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            if (e.DiagramType != ReportChartColumnDiagramType.Normed)
            {
                if (MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Bands)
                {
                    var toRemove = AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X");
                    if (toRemove != null) AnalysisFixedAxises.FixedAxisCollection.Remove(toRemove);
                }
                else
                {
                    var toRemove = AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y");
                    if (toRemove != null) AnalysisFixedAxises.FixedAxisCollection.Remove(toRemove);
                }
            }
            MainFrequencyParameterSelector.FrequencyInPercentOfGroup.IsEnabled = e.DiagramType != ReportChartColumnDiagramType.Generic;
            if (e.DiagramType == ReportChartColumnDiagramType.Generic && MainFrequencyParameterSelector.GetCurrentYMode() == ReportChartFrequencyYMode.FrequencyInPercentOfGroup) MainFrequencyParameterSelector.SetCurrentYMode(ReportChartFrequencyYMode.FrequencyInNumber);
            SetupMultipleSelection();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void CircleParametersSelector_Changed(object sender, CircleParametersChangedEventArgs e)
        {
            HideInteractivityCommandSelector();
            SetupMultipleSelection();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            MakeReportChart(false);
            ClosePopups();
        }

        private void CheckStep(FrequencyParametersChangedEventArgs e)
        {
            var xFixed = AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X");
            if (xFixed == null || !xFixed.AxisMaxFixed || !xFixed.AxisMinFixed || !e.Step.HasValue) return;
            var steps = ((xFixed.AxisMax - xFixed.AxisMin) / (e.Step.Value / Math.Pow(2, e.DetailLevel) * 4)).Value;
            MainFrequencyParameterSelector.ShowExclamation(Math.Abs(steps - Math.Round(steps)) > 1e-7);
        }

        private void ReportChartChanged(object o, EventArgs e)
        {
            ClearMarks();
            MakeReportChart(false);
            RefreshFieldMarks();
        }

        private void SetRegressionAttention()
        {
            if (_selectedTable != null && _selectedTable.Table != null && MainFieldsSelector.YFields != null)
            {
                var f = MainFieldsSelector.YFields.Count;
                MainRegressionSelector.SetAttention(_selectedTable.Table.Count, f);
            }
        }

        private void RefreshFieldMarks()
        {
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.ScatterPlot:
                    MarkCorrelatedFields();
                    break;
                case ReportChartType.ProbabilityPlot:
                    MarkNormalFields();
                    break;
                default:
                    _marker?.CancelAsync();
                    break;
            }
        }

        private void MainViewPanel_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (((Keyboard.Modifiers & ModifierKeys.Shift) > 0))
            {
                if (Math.Sign(e.Delta) > 0) MainLayoutModeSelector.IncrementFontSize();
                else MainLayoutModeSelector.DecrementFontSize();
                e.Handled = true;
                return;
            }

            if (((Keyboard.Modifiers & ModifierKeys.Alt) > 0))
            {
                if (Math.Sign(e.Delta) > 0) MainMarkerSelector.IncrementMarkerSize();
                else MainMarkerSelector.DecrementMarkerSize();
                e.Handled = true;
                return;
            }
            var k = (Math.Sign(e.Delta) < 0 ? 0.9 : 1 / 0.9);
            var layout = MainLayoutModeSelector.GetCurrentLayoutMode();
            MainLayoutModeSelector.SetCurrentLayoutMode(layout | ReportChartLayoutMode.CustomSize);
            var size = MainLayoutModeSelector.GetCurrentSize();
            MainLayoutModeSelector.SetCurrentSize(new Size
            {
                Width = Math.Floor(size.Width * k),
                Height = Math.Floor(size.Height * k)
            });
            e.Handled = true;
            RefreshReportChart();
        }
        #endregion

        private void RefreshParametersUI()
        {
            MainRangeModeSelector.RefreshUI();
            MainSeriesCheckSelector.RefreshUI();
            MainSmoothnessSelector.RefreshUI();
            MainGroupParametersSelector.RefreshUI();
            MainCardTypeSelector.RefreshUI();
            MainChartTypeSelector.RefreshUI();
            MainMarkerSelector.RefreshUI();
            MainFrequencyParameterSelector.RefreshUI();
            MainFitSelector.RefreshUI();
            MainToolsSelector.RefreshUI();
            MainLayoutModeSelector.RefreshUI();
            MainHistogramTypeSelector.RefreshUI();
            MainHistogramLabelsSelector.RefreshUI();
            MainAggregateSelector.RefreshUI();
            MainColumnsParameterSelector.RefreshUI();
            MainRegressionSelector.RefreshUI();
            MainStatisticsSelector.RefreshUI();
            MainCircleParametersSelector.RefreshUI();
        }

        private void SetupBestOptions()
        {
            if (_isSignalProcessingFirstTime) return;
            ClearMarks();
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.Dynamic:
                    MainSmoothnessSelector.SetCurrentSmoothness(ReportChartRegressionFitMode.DynamicSmoothLowess);
                    SwitchToHexBinIfDataLarge();
                    break;

                case ReportChartType.Card:
                    var rcct = MainCardTypeSelector.GetCurrentCardType();
                    if (rcct == ReportChartCardType.CardTypeIsGraphics || rcct == ReportChartCardType.CardTypeIsTrends) SwitchToHexBinIfDataLarge();
                    break;

                case ReportChartType.Histogram:
                    switch (MainHistogramTypeSelector.GetCurrentHistogramType())
                    {
                        case ReportChartHistogramType.Bands:
                        case ReportChartHistogramType.Columns:
                        case ReportChartHistogramType.Circles:
                            MainAggregateSelector.SetCurrentAggregate(Aggregate.N);
                            break;
                        case ReportChartHistogramType.Pareto:
                            MainAggregateSelector.SetCurrentAggregate(Aggregate.Percent);
                            break;
                    }
                    break;

                case ReportChartType.Groups:
                    break;

                case ReportChartType.ScatterPlot:
                    var smoothnessMode = MainSmoothnessSelector.GetCurrentSmoothness();
                    if (smoothnessMode != ReportChartRegressionFitMode.DynamicSmoothLine && smoothnessMode != ReportChartRegressionFitMode.DynamicSmoothIntervals)
                        MainSmoothnessSelector.SetCurrentSmoothness(ReportChartRegressionFitMode.DynamicSmoothLine);
                    SwitchToHexBinIfDataLarge();
                    break;

                case ReportChartType.MultipleRegression:
                    SwitchToHexBinIfDataLarge();
                    break;
            }
        }

        private void SwitchToHexBinIfDataLarge()
        {
            var dataAboveLarge = _selectedTable.Table.Count > LargeData;
            if (dataAboveLarge && !_hexRecommended)
            {
                MainToolsSelector.SetAvailability(MainToolsSelector.GetAvailability() | ReportChartTools.ToolHexBin);
                MainToolsSelector.SetCurrentTools(MainToolsSelector.GetCurrentTools() | ReportChartTools.ToolHexBin);
                _hexRecommended = true;
            }
        }

        private void RefreshChartPanelVisibility()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();
            if (MainSeriesCheckSelector == null) return;
            MainSeriesCheckSelector.Visibility = (rct == ReportChartType.Card &&
                                                  MainCardTypeSelector.GetCurrentCardType() ==
                                                  ReportChartCardType.CardTypeIsX &&
                                                  new[] { ReportChartRangeType.RangeIsSlide, ReportChartRangeType.RangeIsR, ReportChartRangeType.RangeIsSigmas }.Contains(MainRangeModeSelector.GetCurrentRangeType()))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainCardTypeSelector.Visibility = (rct == ReportChartType.Card)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainFrequencyParameterSelector.Visibility = (rct == ReportChartType.Histogram && (MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Frequencies))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainAggregateSelector.Visibility = (rct == ReportChartType.Histogram && (new[] { ReportChartHistogramType.Pareto, ReportChartHistogramType.Bands, ReportChartHistogramType.Columns, ReportChartHistogramType.Circles }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType()))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainHistogramTypeSelector.Visibility = (rct == ReportChartType.Histogram)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainHistogramLabelsSelector.Visibility = (rct == ReportChartType.Histogram && MainHistogramTypeSelector.GetCurrentHistogramType()!= ReportChartHistogramType.Circles)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainGroupParametersSelector.Visibility = (rct == ReportChartType.Groups)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainColumnsParameterSelector.Visibility = (rct == ReportChartType.Histogram && (new[] { ReportChartHistogramType.Bands, ReportChartHistogramType.Columns, ReportChartHistogramType.Frequencies }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType()))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainParetoParameterSelector.Visibility = (rct == ReportChartType.Histogram && (new[] { ReportChartHistogramType.Pareto }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType()))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainRegressionSelector.Visibility = (rct == ReportChartType.MultipleRegression)
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainStatisticsSelector.Visibility = (rct == ReportChartType.Statistics)
                ? Visibility.Visible
                : Visibility.Collapsed;

            MainColumnsParameterSelector.ShowOrderPanel((new[] { ReportChartHistogramType.Bands, ReportChartHistogramType.Columns }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType()));

            MainCircleParametersSelector.Visibility = (rct == ReportChartType.Histogram && (new[] {ReportChartHistogramType.Circles}).Contains(MainHistogramTypeSelector.GetCurrentHistogramType())) ? Visibility.Visible : Visibility.Collapsed;

            MainSmoothnessSelector.Visibility = (rct == ReportChartType.Dynamic ||
                                                 rct == ReportChartType.ScatterPlot || (rct == ReportChartType.Card && MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainSmoothnessSelector.SetLayerStyleSelectorVisibility(
                !(rct == ReportChartType.Card &&
                  MainCardTypeSelector.GetCurrentCardType() == ReportChartCardType.CardTypeIsTrends));
            MainToolsSelector.Visibility = (rct == ReportChartType.Dynamic || rct == ReportChartType.ScatterPlot || rct == ReportChartType.Card || rct == ReportChartType.MultipleRegression || (rct == ReportChartType.Histogram && MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Frequencies))
                ? Visibility.Visible
                : Visibility.Collapsed;
            MainSmoothnessSelector.SetExtendedModesVisibility(rct == ReportChartType.ScatterPlot);
            MainMarkerSelector.Visibility = Visibility.Visible;
            MainFitSelector.Visibility = (rct == ReportChartType.Histogram
                && (new[] { ReportChartHistogramType.Frequencies }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType())
                && MainColumnsParameterSelector.GetCurrentColumnDiagramType() != ReportChartColumnDiagramType.Normed)
                ? Visibility.Visible
                : Visibility.Collapsed;

            MainHistogramLabelsSelector.SetModeAvailability(MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Pareto, MainColumnsParameterSelector.GetCurrentColumnDiagramType() == ReportChartColumnDiagramType.Generic);

            SetupRangeVisibility();
            SetBackground();
            _isSignalProcessingFirstTime = false;
        }

        private void SetupRangeVisibility()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();
            MainRangeModeSelector.Visibility = (rct == ReportChartType.ProbabilityPlot ||
                    (
                        rct == ReportChartType.Histogram && (new[] { ReportChartHistogramType.Pareto, ReportChartHistogramType.Circles }).Contains(MainHistogramTypeSelector.GetCurrentHistogramType())
                                                           || rct == ReportChartType.Dynamic && MainFieldsSelector.IsMultiSelection()
                                                           || rct == ReportChartType.ScatterPlot && ((MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.YFields.Count > 1) || MainSmoothnessSelector.GetCurrentLayerMode() == ReportChartLayerMode.TableOnly || MainSmoothnessSelector.GetCurrentLayerMode() == ReportChartLayerMode.Extended)
                                                           || rct == ReportChartType.Statistics
                    )
                )
                ? Visibility.Collapsed
                : Visibility.Visible;
            MainRangeModeSelector.SetOutagePanelVisibility(rct == ReportChartType.Dynamic || rct == ReportChartType.ScatterPlot || rct == ReportChartType.Groups);
            MainRangeModeSelector.XY = rct == ReportChartType.ScatterPlot;// || (rct == ReportChartType.Histogram && MainHistogramTypeSelector.GetCurrentHistogramType()== ReportChartHistogramType.Frequencies);
            MainDateRangeSelector.Visibility = rct == ReportChartType.Dynamic ? Visibility.Visible : Visibility.Collapsed;
            MainRangeModeSelector.ShowRegressionOptions(rct == ReportChartType.MultipleRegression);
        }

        private void SetupMultipleSelection()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();
            switch (rct)
            {
                case ReportChartType.Card:
                    switch (MainCardTypeSelector.GetCurrentCardType())
                    {
                        case ReportChartCardType.CardTypeIsTrends:
                            MainFieldsSelector.EnableMultipleSelection(true, false);
                            break;
                        case ReportChartCardType.CardTypeIsGraphics:
                            MainFieldsSelector.EnableMultipleSelection(true, true);
                            break;
                        default:
                            MainFieldsSelector.EnableMultipleSelection(false, false);
                            break;
                    }
                    break;

                case ReportChartType.Dynamic:
                    MainFieldsSelector.EnableMultipleSelection(true, false);
                    break;

                case ReportChartType.Histogram:
                    MainFieldsSelector.EnableMultipleSelection(false, false);
                    break;

                case ReportChartType.Groups:
                    MainFieldsSelector.EnableMultipleSelection(false, false);
                    break;

                case ReportChartType.ScatterPlot:
                    MainFieldsSelector.EnableMultipleSelection(true, false);
                    break;

                case ReportChartType.ProbabilityPlot:
                    MainFieldsSelector.EnableMultipleSelection(true, false);
                    break;

                case ReportChartType.MultipleRegression:
                case ReportChartType.Statistics:
                    MainFieldsSelector.EnableMultipleSelection(true, true);
                    break;
            }
        }

        private void SetupToolsAvailability()
        {
            ReportChartTools t = ReportChartTools.ToolBeyondAxis | ReportChartTools.ToolMirrored | ReportChartTools.ToolWithConstants | ReportChartTools.ToolWithGridLines;
            t |= ReportChartTools.ToolKDE | ReportChartTools.ToolRangeMean | ReportChartTools.ToolRangeMedian | ReportChartTools.ToolRug;
            var rct = MainChartTypeSelector.GetCurrentChartType();
            var rcct = MainCardTypeSelector.GetCurrentCardType();
            if (rct == ReportChartType.ScatterPlot)
            {
                t |= ReportChartTools.ToolBag | ReportChartTools.ToolSurface | ReportChartTools.ToolVoronoy;
            }

            if (rct == ReportChartType.ScatterPlot || rct == ReportChartType.Dynamic || rct == ReportChartType.MultipleRegression
                || (rct == ReportChartType.Card && rcct != ReportChartCardType.CardTypeIsX && rcct != ReportChartCardType.CardTypeIsR))
            {
                t |= ReportChartTools.ToolHexBin | ReportChartTools.ToolLog;
                if (!MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.ZField == "Нет") t |= ReportChartTools.ToolText;
            }

            var tools = MainToolsSelector.GetCurrentTools();
            if ((tools & ReportChartTools.ToolSurface) > 0 || (tools & ReportChartTools.ToolVoronoy) > 0) t |= ReportChartTools.ToolCutSurfaces;
            if ((tools & ReportChartTools.ToolRangeMean) > 0 || (tools & ReportChartTools.ToolRangeMedian) > 0) t |= ReportChartTools.ToolText;

            MainToolsSelector.SetAvailability(t);
        }

        private void SetupFieldSelector()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();

            switch (rct)
            {
                case ReportChartType.Card:
                    switch (MainCardTypeSelector.GetCurrentCardType())
                    {
                        case ReportChartCardType.CardTypeIsTrends:
                            if (MainFieldsSelector.IsMultiSelection())
                            {
                                MainFieldsSelector.SetFieldsHeaders(null, "Y", "Имя", null, "↑↓");
                                MainSmoothnessSelector.Visibility = Visibility.Visible;
                                MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double,
                                    SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                                    SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                            }
                            else
                            {
                                MainFieldsSelector.SetFieldsHeaders("Y2", "Y1", "Имя", null, "↑↓");
                                MainSmoothnessSelector.Visibility = Visibility.Visible;
                                MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double,
                                    SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                                    SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                            }
                            break;
                        case ReportChartCardType.CardTypeIsGraphics:
                            if (MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.YFields.Count > 1)
                            {
                                MainFieldsSelector.SetFieldsHeaders("Имя", "Y", null, null, "↑↓");
                                MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                    SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                    SelectorDataTypeFiltration.AllData,
                                    SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                            }
                            else
                            {
                                MainFieldsSelector.SetFieldsHeaders("Имя", "Y", "Слой", null, "↑↓");
                                MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.AllData,
                                    SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                    SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                    SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                            }

                            MainSmoothnessSelector.Visibility = MainCardTypeSelector.GetCurrentCardType() ==
                                                                ReportChartCardType.CardTypeIsGraphics
                                ? Visibility.Collapsed
                                : Visibility.Visible;
                            break;
                        default:
                            MainFieldsSelector.SetFieldsHeaders(null, "Y", null, null, "↑↓");
                            MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                                SelectorDataTypeFiltration.DateTime | SelectorDataTypeFiltration.AddNone, rct);
                            break;
                    }
                    break;

                case ReportChartType.Dynamic:

                    if (MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.YFields.Count > 1)
                    {
                        MainFieldsSelector.SetFieldsHeaders("↑↓", "Y", null, null, "Метка");
                        MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.DateTime,
                            SelectorDataTypeFiltration.Double,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                    }
                    else
                    {
                        MainFieldsSelector.SetFieldsHeaders("↑↓", "Y", "Слой", "Форма", "Метка");
                        MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.DateTime,
                            SelectorDataTypeFiltration.Double,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                    }
                    if (!_isSignalProcessingFirstTime)
                        MainFieldsSelector.SetZWVToStart();
                    //Для того, чтобы при передаче расчета с панели слайдов не сбрасывались поля маркера.
                    break;

                case ReportChartType.Histogram:

                    switch (MainHistogramTypeSelector.GetCurrentHistogramType())
                    {
                        case ReportChartHistogramType.Pareto:
                            switch (MainAggregateSelector.GetCurrentAggregate())
                            {
                                case Aggregate.N:
                                case Aggregate.Percent:
                                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", null, null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                                default:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Параметр", null, null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                            }
                            break;
                        case ReportChartHistogramType.Columns:
                            switch (MainAggregateSelector.GetCurrentAggregate())
                            {
                                case Aggregate.N:
                                case Aggregate.Percent:
                                case Aggregate.PercentInCategory:
                                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;

                                case Aggregate.Single:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Параметр", "Слой", null, "Метка");
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                                    break;

                                default:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Параметр", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                            }
                            MainColumnsParameterSelector.ShowOrderPanel(true);
                            break;

                        case ReportChartHistogramType.Circles:
                            switch (MainAggregateSelector.GetCurrentAggregate())
                            {
                                case Aggregate.N:
                                case Aggregate.Percent:
                                case Aggregate.PercentInCategory:
                                    MainFieldsSelector.SetFieldsHeaders(null, "Группа", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;

                                case Aggregate.Single:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Группа", "Слой", null, "Метка");
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                                    break;

                                default:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Группа", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                            }
                            break;

                        case ReportChartHistogramType.Bands:
                            switch (MainAggregateSelector.GetCurrentAggregate())
                            {
                                case Aggregate.N:
                                case Aggregate.Percent:
                                case Aggregate.PercentInCategory:
                                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;

                                case Aggregate.Single:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Группа", "Слой", null, "Метка");
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                                    break;

                                default:
                                    MainFieldsSelector.SetFieldsHeaders("Значения", "Параметр", "Слой", null, null);
                                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                        SelectorDataTypeFiltration.AllData,
                                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                        SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                                    break;
                            }
                            MainColumnsParameterSelector.ShowOrderPanel(true);
                            break;

                        default:
                            MainFieldsSelector.SetFieldsHeaders(null, "Параметр", "Слой", null, null);
                            MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.AllData, rct);
                            break;
                    }
                    break;

                case ReportChartType.Groups:
                    MainFieldsSelector.SetFieldsHeaders("Группа", "Y", "Слой", null, null);
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.Double,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                        SelectorDataTypeFiltration.AllData, rct);
                    break;

                case ReportChartType.Statistics:
                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", "Слой", null, null);
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.AllData, SelectorDataTypeFiltration.Double,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                        SelectorDataTypeFiltration.AllData, rct);
                    break;

                case ReportChartType.ScatterPlot:
                    if (MainSmoothnessSelector.GetCurrentLayerMode() == ReportChartLayerMode.TableOnly || MainSmoothnessSelector.GetCurrentLayerMode() == ReportChartLayerMode.Extended)
                    {
                        MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                            SelectorDataTypeFiltration.Double,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                            SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                        MainFieldsSelector.SetFieldsHeaders("X", "Y", "Слой", null, null);
                    }
                    else
                    {
                        if (MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.YFields.Count > 1)
                        {
                            MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                            MainFieldsSelector.SetFieldsHeaders("X", "Y", null, null, "Метка");
                        }
                        else
                        {
                            MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.Double,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                                SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                            MainFieldsSelector.SetFieldsHeaders("X", "Y", "Слой", "Форма", "Метка");
                        }
                    }
                    if (!_isSignalProcessingFirstTime)
                        MainFieldsSelector.SetZWVToStart();
                    //Для того, чтобы при передаче расчета с панели слайдов не сбрасывались поля маркера.
                    break;

                case ReportChartType.ProbabilityPlot:
                    MainFieldsSelector.SetFieldsHeaders(null, "Параметр", null, null, null);
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.Double,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                        SelectorDataTypeFiltration.AllData, rct);
                    break;

                case ReportChartType.MultipleRegression:

                    MainFieldsSelector.SetFieldsHeaders("Переменная", "Факторы", "Слой", "Форма", "Метка");
                    MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double,
                        SelectorDataTypeFiltration.Double,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone,
                        SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, rct);
                    if (!_isSignalProcessingFirstTime)
                        MainFieldsSelector.SetZWVToStart();
                    //Для того, чтобы при передаче расчета с панели слайдов не сбрасывались поля маркера.
                    break;
            }
        }

        private void CheckChartPossibility()
        {
            CheckDateForDynamic();
            var dataHasNumbers = _selectedTable.DataTypes.Any(a => a.ToLower().Contains("double"));
            var dataNotTooBig = _selectedTable.Table.Count < 2000000;
            MainAggregateSelector.EnableNumericAggregateSelection(dataHasNumbers);
            MainHistogramTypeSelector.Frequencies.IsEnabled = dataHasNumbers;
            MainHistogramTypeSelector.Pareto.IsEnabled = dataHasNumbers;

            MainChartTypeSelector.Enable(ReportChartType.Dynamic, dataHasNumbers && dataNotTooBig);
            MainChartTypeSelector.Enable(ReportChartType.Card, dataHasNumbers && dataNotTooBig);
            MainChartTypeSelector.Enable(ReportChartType.ScatterPlot, dataHasNumbers && dataNotTooBig);
            MainChartTypeSelector.Enable(ReportChartType.Groups, dataHasNumbers && dataNotTooBig);
            MainChartTypeSelector.Enable(ReportChartType.ProbabilityPlot, dataHasNumbers && dataNotTooBig);
            MainChartTypeSelector.Enable(ReportChartType.Statistics, dataHasNumbers);
            MainChartTypeSelector.Enable(ReportChartType.MultipleRegression, dataHasNumbers);
            if (!dataHasNumbers)
            {

                MainHistogramTypeSelector.SetCurrentHistogramType(ReportChartHistogramType.Columns);
                MainFieldsSelector.SetFilters(SelectorDataTypeFiltration.Double, SelectorDataTypeFiltration.AllData,
                    SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone, SelectorDataTypeFiltration.AllData,
                    SelectorDataTypeFiltration.AllData, ReportChartType.Histogram);
                MainChartTypeSelector.SetCurrentChartType(ReportChartType.Histogram);
            }
        }

        private void SetupZVisibilityForSIngleVariable()
        {
            var rct = MainChartTypeSelector.GetCurrentChartType();
            if (rct != ReportChartType.Dynamic && rct != ReportChartType.ScatterPlot || !MainFieldsSelector.IsMultiSelection()) return;
            var isGraphs = (rct == ReportChartType.ScatterPlot && MainSmoothnessSelector.GetCurrentLayerMode() == ReportChartLayerMode.Extended);
            MainFieldsSelector.SetFieldsHeaders("X", "Y", MainFieldsSelector.YFields.Count == 1 || isGraphs ? "Слой" : null, "Форма", "Метка");
        }

        private void IrcUiReflectChanges(object o, UIReflectChangesEventArgs e)
        {
            //if (e.UserLimits.Table.Count > 0) MainRangeModeSelector.SetupRangesForAB(e.UserLimits);
            if (e.Step.HasValue && e.StepIsAuto.HasValue) MainFrequencyParameterSelector.DisplayStep(e.StepIsAuto.Value, e.Step.Value);
        }

        private void CheckDateForDynamic()
        {
            if (DataHelper.GetFirstDateIndex(_selectedTable) >= 0) return;
            if (MainChartTypeSelector.GetCurrentChartType() == ReportChartType.Dynamic)
                MainChartTypeSelector.SetCurrentChartType(ReportChartType.Histogram);
            MainChartTypeSelector.Enable(ReportChartType.Dynamic, false);
        }
    }
}