﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public delegate void SlideViewDelegate(object o, SlideViewEventArgs e);
    public delegate void SlideDragDelegate(object o, SlideDragEventArgs e);
    public delegate void SlideToAnalysisDelegate(object o, SlideToAnalysisEventArgs e);
    public delegate void SlideRenderedDelegate(object o, SlideReadyEventArgs e);

    public partial class SlideView
    {
        public event SlideViewDelegate SlideAboutDelete;
        public event SlideDragDelegate DragStarted;
        public event SlideDragDelegate DragStopped;
        public event SlideToAnalysisDelegate SlideToAnalysis;
        
        public ClientSlideElementDescription PresentedCsed
        {
            get { return (ClientSlideElementDescription)GetValue(PresentedCsedProperty); }
            set { SetValue(PresentedCsedProperty, value); }
        }

        public static readonly DependencyProperty PresentedCsedProperty =
            DependencyProperty.Register("PresentedCsed", typeof(ClientSlideElementDescription), typeof(SlideView), new PropertyMetadata(null, PresentedCsedChangedCallback));

        public bool CanExport
        {
            get { return (bool)GetValue(CanExportProperty); }
            set { SetValue(CanExportProperty, value); }
        }

        public static readonly DependencyProperty CanExportProperty =
            DependencyProperty.Register("CanExport", typeof(bool), typeof(SlideView), new PropertyMetadata(true, CanExportPropertyChangedCallBack));

        public bool CanSendSlide
        {
            get { return (bool)GetValue(CanSendSlideProperty); }
            set { SetValue(CanSendSlideProperty, value); }
        }

        public static readonly DependencyProperty CanSendSlideProperty =
            DependencyProperty.Register("CanSendSlide", typeof(bool), typeof(SlideView), new PropertyMetadata(false, CanExportPropertyChangedCallBack));

        private static void CanExportPropertyChangedCallBack(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((SlideView)dependencyObject).SetupButtonsState();
        }

        private List<byte> _png;
        public bool PNGReady;
                
        private static void PresentedCsedChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sv = o as SlideView;
            if (sv == null || sv.PresentedCsed?.SED == null) return;
            sv.PresentedCsed.ContentViewer = sv;
            sv.Dispatcher.BeginInvoke(() => 
            { 
                sv.Description.Text = sv.PresentedCsed.SED.Description;
                sv.SetupButtonsState();

                if (sv.PresentedCsed.Icon != null  && sv.PresentedCsed.SED.SlideElementType == ApplicationCore.ReportingServiceReference.SlideElementTypes.Image)
                {
                    sv.Thumbnail.Source = sv.PresentedCsed.Icon;
                    if (sv.PresentedCsed.Thumbnail==null) return;

                    if (sv.PresentedCsed.SED.SlideData == null)
                    {
                        sv.GeneratePNG(sv);
                    }
                }
                else
                {
                    sv.Thumbnail.Source = new BitmapImage {UriSource = new Uri($"/AQResources;component/Images/SlideTypes/{sv.PresentedCsed.SED.SlideElementType}.png", UriKind.Relative)};
                }
                sv.PresentedCsed.SlideContentChanged += PresentedCsed_SlideContentChanged;
                sv.PresentedCsed.SlideIconChanged += PresentedCsed_SlideIconChanged;
            });
        }

        private static void PresentedCsed_SlideIconChanged(object o, SlideIconChangedEventArgs e)
        {
            var csed = (ClientSlideElementDescription)o;
            csed.ContentViewer.Thumbnail.Source = csed.Icon;
        }

        private static void PresentedCsed_SlideContentChanged(object o, SlideContentChangedEventArgs e)
        {
            var csed = (ClientSlideElementDescription) o;
            if (csed.Thumbnail == null) return;
            if (csed.SED.SlideData == null)
            {
                csed.ContentViewer.GeneratePNG(csed.ContentViewer);
            }
        }

        private void SetupButtonsState()
        {
            ExportPNGButton.Visibility = (_png == null || !CanExport) ? Visibility.Collapsed : Visibility.Visible;
            ReturnToPreview.Visibility = (PresentedCsed.Operation == null) ? Visibility.Collapsed : Visibility.Visible;
            ExportCalcButton.Visibility = (PresentedCsed.SourceCalculation == null || !CanExport) ? Visibility.Collapsed : Visibility.Visible;
            ExportToMainPanelButton.Visibility = !CanSendSlide ? Visibility.Collapsed : Visibility.Visible;
            ExportXLSXButton.Visibility = (PresentedCsed.SourceData == null || !CanExport) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void GeneratePNG(SlideView sv)
        {
            var bw = new BackgroundWorker();
            sv.ProgressIndicator.Visibility = Visibility.Visible;
            sv.PresentedCsed.IsBusy = true;
            var wb = sv.PresentedCsed.Thumbnail;
            if (wb == null) return;

            bw.DoWork += (a, b) =>
            {
                sv._png = PNG.WriteableBitmapToPNG(wb).ToList();
            };

            bw.RunWorkerCompleted += (a, b) => sv.Dispatcher.BeginInvoke(() =>
            {
                sv.PresentedCsed.SED.SlideData = sv._png;
                sv.PresentedCsed.IsReady = true;
                sv.ProgressIndicator.Visibility = Visibility.Collapsed;
                sv.PresentedCsed.IsBusy = false;
                sv.SetupButtonsState();
                sv.PresentedCsed.Thumbnail = null;
            });

            bw.RunWorkerAsync();
        }

        public SlideView()
        {
            InitializeComponent();
        }

        private void DeleteSlideButton_Click(object sender, RoutedEventArgs e)
        {
            SlideAboutDelete?.Invoke(this, new SlideViewEventArgs { CSED = PresentedCsed });
        }

        private void ExportPNGButton_Click(object sender, RoutedEventArgs e)
        {
            CommonTasks.SendReadyPNGFile(this, _png, "Слайд ", null);
        }

        private void ExportXLSXButton_Click(object sender, RoutedEventArgs e)
        {
            var cs = Services.GetCalculationService();
            ExportXLSXButton.IsEnabled = false;
            xlsxProgressIndicator.Visibility = Visibility.Visible;
            cs.BeginFillDataWithSLTables(PresentedCsed.SourceData.Select(a=>a.ToSafeSerializable()).ToList(), iar =>
            {
                var sr = cs.EndFillDataWithSLTables(iar);
                cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, iar2 =>
                {
                    sr = cs.EndGetAllResultsInXlsx(iar2);
                    if (sr.Success)
                    {
                        Dispatcher.BeginInvoke(() =>
                        {
                            HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                            ExportXLSXButton.IsEnabled = true;
                            xlsxProgressIndicator.Visibility = Visibility.Collapsed;
                        });
                    }
                    else
                    {
                        Dispatcher.BeginInvoke(() =>
                        {
                            ExportXLSXButton.IsEnabled = true;
                            xlsxProgressIndicator.Visibility = Visibility.Collapsed;
                        });
                    }
                }, null);
            }, null);
        }

        private void ExportCalcButton_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object>();
            if (PresentedCsed.From.HasValue) signalArguments.Add("From", PresentedCsed.From.Value);
            if (PresentedCsed.To.HasValue) signalArguments.Add("To", PresentedCsed.To.Value);
            signalArguments.Add("Operation", PresentedCsed.Operation);
            signalArguments.Add("Calculation", PresentedCsed.SourceCalculation);
            signalArguments.Add("Description", PresentedCsed.SED.Description);
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        private void ExportToMainPanelButton_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object>
            {
                {"Operation", PresentedCsed.Operation},
                {"Calculation", PresentedCsed.SourceCalculation},
                {"Thumbnail", PresentedCsed.Thumbnail},
                {"Icon", PresentedCsed.Icon},
                {"SlideElement", PresentedCsed.SED},
                {"SourceData", PresentedCsed.SourceData}
            };
            if (PresentedCsed.From.HasValue) signalArguments.Add("From", PresentedCsed.From.Value);
            if (PresentedCsed.From.HasValue) signalArguments.Add("To", PresentedCsed.From.Value);
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertSlideElement, signalArguments, null);
        }
        
        private void ReturnToPreview_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object>();
            if (PresentedCsed.From.HasValue) signalArguments.Add("From", PresentedCsed.From.Value);
            if (PresentedCsed.To.HasValue) signalArguments.Add("To", PresentedCsed.To.Value);
            signalArguments.Add("Operation", PresentedCsed.Operation);
            signalArguments.Add("SourceData", PresentedCsed.SourceData);
            signalArguments.Add("Calculation", PresentedCsed.SourceCalculation);
            signalArguments.Add("Description", PresentedCsed.SED.Description);
            SlideToAnalysis?.Invoke(this, new SlideToAnalysisEventArgs {Operation = PresentedCsed.Operation, Description = PresentedCsed.SED.Description, SourceCalculation = PresentedCsed.SourceCalculation, SourceData = PresentedCsed.SourceData});
            if (CanExport)Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.ReopenInAnalisys, signalArguments, null);
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            SetupButtonsState();
            ButtonPanel.Visibility = Visibility.Visible;
            _mouseIsIn = true;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            ButtonPanel.Visibility = Visibility.Collapsed;
            _mouseIsIn = false;
            _mouseIsDown = false;
        }
        
        #region Drag&Drop

        private bool _mouseIsIn;
        private bool _mouseIsDown;
        private bool _dragStartIsInvoked;
        private Point _dragStartPosition;
        
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_mouseIsIn) return;
            _dragStartPosition = e.GetPosition(Parent as UIElement);
            _dragStartIsInvoked = false;
            _mouseIsDown = true;
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!_mouseIsIn) return;
            _mouseIsDown = false;
            _dragStartIsInvoked = false; 
            if (DragStopped != null) DragStopped.Invoke(this, new SlideDragEventArgs { CSED = PresentedCsed, Position = e.GetPosition(Parent as UIElement), Offset = e.GetPosition(this) });
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mouseIsDown || _dragStartIsInvoked) return;
            var currentPosition = e.GetPosition(Parent as UIElement);
            if (!(Math.Sqrt(Math.Pow(currentPosition.X - _dragStartPosition.X, 2) +
                            Math.Pow(currentPosition.Y - _dragStartPosition.Y, 2)) > 20)) return;
            if (DragStarted == null) return;
            DragStarted.Invoke(this, new SlideDragEventArgs { CSED = PresentedCsed, Position = currentPosition, Offset = e.GetPosition(this) });
            _dragStartIsInvoked = true;
        }
        
        #endregion
    }

    public class SlideViewEventArgs
    {
        public ClientSlideElementDescription CSED;
    }

    public class SlideDragEventArgs
    {
        public ClientSlideElementDescription CSED;
        public Point Position;
        public Point Offset;
    }

    public class SlideToAnalysisEventArgs
    {
        public Step Operation;
        public List<SLDataTable> SourceData;
        public string Description;
        public List<Step> SourceCalculation;
    }

    public class SlideReadyEventArgs
    {
        public SerializableAnalysisResult SAR;
        public ReportElementType ElementType;
    }
}
