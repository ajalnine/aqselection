﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using ApplicationCore;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class SlidePanel
    {
        public ObservableCollection<ClientSlideElementDescription> InternalSlideCollection;
        private int _pngpc;
        private int ProcessCount
        {
            get
            {
                return _pngpc;
            }
            set
            {
                _pngpc = value;

                if (value == 0)
                {
                    Dispatcher.BeginInvoke(() => EnableButtons(InternalSlideCollection.Count!=0));
                }
                else
                {
                    Dispatcher.BeginInvoke(() => EnableButtons(false));
                }
            }
        }
        private bool _breakCalcInProcess;
        private bool _limitIsFour = false;
        private bool _continuousInsertion = false;
        private int _insertionIndex;
        private ColorScales lastColorScale = ColorScales.HSV;
        private bool inverted;
        public event SlideToAnalysisDelegate SlideToAnalysis;
        public event SlideCollectionChangedDelegate SlideCollectionChanged;
        public event SlideRenderedDelegate SlideReady;
        public event SlideRenderedDelegate AllSlideReady;
        public event InternalTablesReadyDelegate InternalTablesReady;

        public bool ShowHint
        {
            get { return (bool)GetValue(ShowHintProperty); }
            set { SetValue(ShowHintProperty, value); }
        }

        public static readonly DependencyProperty ShowHintProperty =
            DependencyProperty.Register("ShowHint", typeof(bool), typeof(SlidePanel), new PropertyMetadata(true, ShowHintPropertyChangedCallback));

        public bool ButtonsAutoEnable
        {
            get { return (bool)GetValue(ButtonsAutoEnableProperty); }
            set { SetValue(ButtonsAutoEnableProperty, value); }
        }

        public static readonly DependencyProperty ButtonsAutoEnableProperty =
            DependencyProperty.Register("ButtonsAutoEnable", typeof(bool), typeof(SlidePanel), new PropertyMetadata(true));

        private static void ShowHintPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((SlidePanel)dependencyObject).FirstDivider.DividerMode = (bool)dependencyPropertyChangedEventArgs.NewValue ? SlideDividerMode.Empty : SlideDividerMode.EmptyNoHint;
        }

        public bool CanExport
        {
            get { return (bool)GetValue(CanExportProperty); }
            set { SetValue(CanExportProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanExport.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanExportProperty =
            DependencyProperty.Register("CanExport", typeof(bool), typeof(SlidePanel), new PropertyMetadata(true, CanExportPropertyChangedCallBack));

        public bool CanSendSlide
        {
            get { return (bool)GetValue(CanSendSlideProperty); }
            set { SetValue(CanSendSlideProperty, value); }
        }

        public static readonly DependencyProperty CanSendSlideProperty =
            DependencyProperty.Register("CanSendSlide", typeof(bool), typeof(SlidePanel), new PropertyMetadata(false, CanExportPropertyChangedCallBack));

        private static void CanExportPropertyChangedCallBack(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((SlidePanel)dependencyObject).ExportButtonsPanel.Visibility = (bool)dependencyPropertyChangedEventArgs.NewValue ? Visibility.Visible : Visibility.Collapsed;
        }
        
        public SlidePanel()
        {
            InitializeComponent();
            FirstDivider.DividerMode = ShowHint ? SlideDividerMode.Empty : SlideDividerMode.EmptyNoHint;
        }

        public List<SerializableAnalysisResult> GetSlides()
        {
            var res = new List<SerializableAnalysisResult>();
            foreach (var slide in InternalSlideCollection)
            {
                var sar = new SerializableAnalysisResult {
                    AnalysisGuid = slide.SourceKey,
                    AnalysisOperation = slide.Operation,
                    From = slide.From,
                    To = slide.To,
                    InnerTables = null,
                    IsInverted = inverted,
                    UsedColorScale = lastColorScale,
                    SED = slide.SED,
                    OnlyInnerSteps = true,
                    IconSerializable = Convert.ToBase64String((slide.IconLR ?? slide.Icon).Pixels.SelectMany(BitConverter.GetBytes).ToArray()), 
                    UsedFields = slide?.UsedFields,
                    UsedTable = slide.SelectedTable
                };
                sar.SED.BreakSlide = !slide.IsContinuous;
                res.Add(sar);
            }
            return res;
        } 

        void InternalSlideCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            FirstDivider.DividerMode = (InternalSlideCollection.Count > 0) ? SlideDividerMode.First 
                : ShowHint ?  SlideDividerMode.Empty : SlideDividerMode.EmptyNoHint;
            if (ButtonsAutoEnable) ResetButtonStates();

            for(var i=0; i<InternalSlideCollection.Count; i++)
            {
                InternalSlideCollection[i].DividerMode = (i!=InternalSlideCollection.Count-1) ? SlideDividerMode.Generic : SlideDividerMode.Last;
            }
            ShowCounters();
            SlideCollectionChanged?.Invoke(this, new SlideCollectionChangedEventArgs {SlideCollection = InternalSlideCollection.ToList()});
        }

        private void ShowCounters()
        {
            var slides = InternalSlideCollection.Count(t => !t.IsContinuous);
            CountersPanel.Visibility = slides > 0 ? Visibility.Visible : Visibility.Collapsed;
            var s = slides.ToString(CultureInfo.InvariantCulture).EndsWith("1") ? "слайде" : "слайдах";
            var e = InternalSlideCollection.Count.ToString(CultureInfo.InvariantCulture);
            var last = int.Parse(e.Substring(e.Length - 1));
            var elements = last == 1 ? "элемент" : last < 5 ? "элемента" : "элементов";
            SedNumber.Text = String.Format("На {0} {2} {1} {3}.", slides, e, s, elements);
        }

        public void InsertAnalysisResultElement(AnalysisResult ar, bool createThumb = true)
        {
            lastColorScale = ar.UsedColorScale;
            inverted = ar.IsInverted;
            if (InternalSlideCollection == null) ResetInternalSlidesCollection(true);
            var csed = new ClientSlideElementDescription
            {
                SED = ar.SED,
                Operation = ar.AnalysisOperation,
                SourceCalculation = ar.InnerSteps,
                SourceKey = ar.AnalysisGuid,
                Thumbnail = createThumb? ar.ThumbNail : null,
                Icon = ar.Icon,// createThumb? ar.Icon : ar.IconLR,
                IconLR = ar.IconLR,
                DividerMode = SlideDividerMode.Generic,
                AllowNonBreak = true,
                SourceData = ar.AvailableData,
                From = ar.From,
                To = ar.To,
                IsInsertion = true,
                UsedFields = ar.UsedFields,
                SelectedTable = ar.UsedTable?.TableName,
                StepInSourceCalculation = ar.InnerSteps?.Count ?? 0
            };

            InsertCSED(csed);
        }

        public void InsertSerializableAnalysisResultElement(SerializableAnalysisResult ar)
        {
            lastColorScale = ar.UsedColorScale;
            inverted = ar.IsInverted;
            if (InternalSlideCollection == null) ResetInternalSlidesCollection(true);
            var icon = new WriteableBitmap ((int)ar.SED.Width/8, (int)ar.SED.Height/8);
            var pixels = Convert.FromBase64String(ar.IconSerializable);

            for (var i=0; i<pixels.Length;i+=4) icon.Pixels[i/4] = BitConverter.ToInt32(pixels, i);

            var csed = new ClientSlideElementDescription
            {
                SED = ar.SED,
                Operation = ar.AnalysisOperation,
                SourceKey = ar.AnalysisGuid,
                Icon = icon,
                DividerMode = SlideDividerMode.Generic,
                IsContinuous = !ar.SED.BreakSlide,
                AllowNonBreak = true,
                From = ar.From,
                To = ar.To,
                IsInsertion = true,
                UsedFields = ar.UsedFields,
                SelectedTable = ar.UsedTable,
            };

            csed.PropertyChanged += csed_PropertyChanged;
            InternalSlideCollection?.Insert(_insertionIndex, csed);
            _insertionIndex++;
        }
        
        public void RenderSAR(List<SLDataTable> availableData, SerializableAnalysisResult ar, bool forExport, int position = -1)
        {
            lastColorScale = ar.UsedColorScale;
            inverted = ar.IsInverted;

            var csed = new ClientSlideElementDescription
            {
                SED = ar.SED,
                Operation = ar.AnalysisOperation,
                SourceKey = ar.AnalysisGuid,
                DividerMode = SlideDividerMode.Generic,
                AllowNonBreak = true,
                IsContinuous = !ar.SED.BreakSlide,
                From = ar.From,
                To = ar.To,
                IsInsertion = true,
                UsedFields = ar.UsedFields,
                SelectedTable = ar.UsedTable
            };

            csed.PropertyChanged += csed_PropertyChanged;
            if (position == -1)
            {
                InternalSlideCollection?.Insert(_insertionIndex, csed);
                _insertionIndex++;
            }
            else
            {
                if (InternalSlideCollection.Count > position)
                {
                    InternalSlideCollection.RemoveAt(position);
                    InternalSlideCollection.Insert(position, csed);
                    _insertionIndex = position;
                }
            }

            ChartRenderer.RenderStep(availableData, ar.AnalysisOperation, Loader.InvisibleCanvas,
                cr =>
                {
                    csed.Icon = cr.ThumbNail;
                    csed.IconLR = cr.Icon;
                    csed.Thumbnail = forExport ? cr.Chart : null;
                    var periodDescription = csed.From.HasValue && csed.To.HasValue ? $"Период: {csed.From.Value.ToShortDateString()} - {csed.To.Value.ToShortDateString()}" : string.Empty;
                    csed.SED.Description = cr.ChartDescription + (string.IsNullOrEmpty(cr.ChartDescription) ? string.Empty : "\r\n") + periodDescription;
                    csed.SED.ShortDescription = Description?.Text + (string.IsNullOrEmpty(Description?.Text ?? string.Empty) ? string.Empty : "\r\n") + periodDescription; 

                    ar.InnerTables = cr.InnerTables;
                    SlideReady?.Invoke(this, new SlideReadyEventArgs
                    {
                        SAR = ar,
                        ElementType = ReportElementType.Image | (cr.InnerTables == null ? ReportElementType.None : ReportElementType.DataTable)
                    });
                },
                (o, e) =>
                {
                    InternalTablesReady?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables, AnalysisGuid = ar.AnalysisGuid});   
                });
        }

        public void GetInternalTablesForSAR(List<SLDataTable> availableData, Step s, Guid analysisGuid)
        {
            ChartRenderer.RenderStep(availableData, s, Loader.InvisibleCanvas, null,
                (o, e) =>
                {
                    SlideReady?.Invoke(this, new SlideReadyEventArgs { SAR = new SerializableAnalysisResult {AnalysisGuid = analysisGuid, InnerTables = e.InternalTables},
                        ElementType = (e.InternalTables == null ? ReportElementType.None : ReportElementType.DataTable)
                    });
                    InternalTablesReady?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables, AnalysisGuid = analysisGuid});
                });
        }

        public void InsertSlide(ClientSlideElementDescription csed, bool isInverted, ColorScales cs, bool createThumb = true)
        {
            lastColorScale = cs;
            inverted = isInverted;
            if (InternalSlideCollection == null) ResetInternalSlidesCollection(true);
            InsertCSED(csed);
        }

        private void InsertCSED(ClientSlideElementDescription csed)
        {
            csed.PropertyChanged += csed_PropertyChanged;
            if (InternalSlideCollection == null) return;

            InternalSlideCollection.Insert(_insertionIndex, csed);
            if (_continuousInsertion)
            {
                if (_insertionIndex > 0)
                {
                    if (_insertionIndex < InternalSlideCollection.Count - 1)
                    {
                        InternalSlideCollection[_insertionIndex].IsContinuous =
                            InternalSlideCollection[_insertionIndex - 1].IsContinuous;
                    }
                    InternalSlideCollection[_insertionIndex - 1].AllowNonBreak = true;
                    InternalSlideCollection[_insertionIndex - 1].IsContinuous = true;
                }
            }
            else
            {
                if (_insertionIndex > 0)
                {
                    InternalSlideCollection[_insertionIndex - 1].IsContinuous = false;
                }
                if (_insertionIndex < InternalSlideCollection.Count - 1)
                {
                    InternalSlideCollection[_insertionIndex].IsContinuous = false;
                }
            }

            UpdateLayout();
            _insertionIndex++;
            Dispatcher.BeginInvoke(() =>
            {
                if (InternalSlideCollection.Count > 1)
                    SlideScroller.ScrollToVerticalOffset(SlideScroller.ScrollableHeight*(_insertionIndex - 1)/
                                                         (InternalSlideCollection.Count - 1) + FirstDivider.ActualHeight);
                RecalcBreaks();
            });
        }

        private void ResetInsertionPoint()
        {
            if (_insertionIndex == 0 )FirstDivider.InsertHere = true;
            else
            {
                InternalSlideCollection[_insertionIndex - 1].IsInsertion = true;
            }
        }

        private void LimitedCountSlidesPanelButton_Click(object sender, RoutedEventArgs e)
        {
            EnableLimits(LimitedCountSlidesPanelButton.IsChecked.Value);
        }

        private void SlideInsertionMode_Click(object sender, RoutedEventArgs e)
        {
            SetContinuousInsertionMode(SlideInsertionMode.IsChecked.Value);
        }

        private void RecalcBreaks()
        {
            if (InternalSlideCollection == null) return;
            if (!_limitIsFour)
            {
                foreach (var c in InternalSlideCollection)
                {
                    c.AllowNonBreak = true;
                }
                return;
            };
            var slideElementCounter = 0;
            foreach (var c in InternalSlideCollection)
            {
                if (c.IsContinuous)
                {
                    slideElementCounter++;
                    if (slideElementCounter == 4)
                    {
                        _breakCalcInProcess = true;

                        c.IsContinuous = false;
                        c.AllowNonBreak = false;
                        slideElementCounter = 0;

                        _breakCalcInProcess = false;
                    }
                    else c.AllowNonBreak = true;
                }
                else
                {
                    c.AllowNonBreak = slideElementCounter != 3;
                    slideElementCounter = 0;
                }
            }
        }

        void csed_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsContinuous":
                    if (!_breakCalcInProcess) RecalcBreaks();
                    break;

                case "IsBusy":
                    ProcessCount += ((ClientSlideElementDescription)sender).IsBusy ? 1 : -1;
                    break;
            }
            ShowCounters();
        }

        public PresentationDescription GetPresentation()
        {
            return null;
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Удалить все слайды ?", "Предупреждение", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel) return;
            ResetInternalSlidesCollection(true);
        }

        public void Clear()
        {
            ResetInternalSlidesCollection(true);
        }

        public int ResetInternalSlidesCollection(bool clear, int emptyPositions = 0)
        {
            if (clear)
            {
                InternalSlideCollection = new ObservableCollection<ClientSlideElementDescription>();
                InternalSlideCollection.CollectionChanged += InternalSlideCollection_CollectionChanged;
            }
            _insertionIndex = InternalSlideCollection.Count;
            SlidePresenter.ItemsSource = InternalSlideCollection;
            if (emptyPositions == 0)
            {
                FirstDivider.DividerMode = ShowHint ? SlideDividerMode.Empty : SlideDividerMode.EmptyNoHint;
            }
            else
            {
                for (var i = 0; i < emptyPositions; i++)
                {
                    InternalSlideCollection.Add(new ClientSlideElementDescription());
                }
            }
            
            ResetInsertionPoint();
            if (ButtonsAutoEnable) ResetButtonStates();
            UpdateLayout();
            ShowCounters();
            return _insertionIndex;
        }

        private void ResetButtonStates()
        {
            EnableButtons(InternalSlideCollection.Count != 0);
        }

        public void SetDescription(string description)
        {
            Description.Text = description;
        }
        public void EnableLimits(bool limitsEnabled)
        {
            _limitIsFour = limitsEnabled;
            RecalcBreaks();
        }

        public void SetContinuousInsertionMode(bool continuousInsertion)
        {
            _continuousInsertion = continuousInsertion;
        }

        public void EnableButtons(bool enable)
        {
            DeleteAll.IsEnabled = enable;
            ExportPPTX.IsEnabled = enable;
            ExportDOCX.IsEnabled = enable;
            ExportXLSX.IsEnabled = enable & (InternalSlideCollection.Select(a => a.SourceData).Distinct().Count() == 1);
        }

        private void ExportPPTX_Click(object sender, RoutedEventArgs e)
        {
            DoExportPPTX();
        }

        public void DoExportPPTX()
        {
            var pd = CreatePresentationDescription();

            PPTXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();

            CommonTasks.CreatePPTX(this, pd, GetReportName() + ".pptx", () => Dispatcher.BeginInvoke(() =>
            {
                PPTXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        public void SendPPTX()
        {
            var pd = CreatePresentationDescription();

            PPTXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();

            CommonTasks.SendPPTX(this, pd, GetReportName() + ".pptx", GetMailSubject(), GetMailRecepients(), () => Dispatcher.BeginInvoke(() =>
            {
                PPTXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
                HtmlPage.Window.Invoke("CloseWindow");
            }));
        }

        private void ExportXLSX_Click(object sender, RoutedEventArgs e)
        {
            DoExportXLSX();
        }

        public void DoExportXLSX()
        {
            ExportXLSX.IsEnabled = false;
            XLSXProgressIndicator.Visibility = Visibility.Visible;
            var tables = InternalSlideCollection?.Where(a => a.SourceData != null).LastOrDefault()?.SourceData.ToList();
            CommonTasks.CreateXLSX(this, tables, GetReportName() + ".xlsx", () => Dispatcher.BeginInvoke(() =>
            {
                XLSXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        public void SendXLSX()
        {
            ExportXLSX.IsEnabled = false;
            XLSXProgressIndicator.Visibility = Visibility.Visible;
            var tables = InternalSlideCollection?.Where(a => a.SourceData != null).LastOrDefault()?.SourceData.ToList();
            CommonTasks.SendXLSX(this, tables, GetReportName() + ".xlsx", GetMailSubject(), GetMailRecepients(), () => Dispatcher.BeginInvoke(() =>
            {
                XLSXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        public void SendPPTXXLSX()
        {
            var pd = CreatePresentationDescription();
            var tables = InternalSlideCollection?.Where(a => a.SourceData != null).LastOrDefault()?.SourceData.ToList();
            XLSXProgressIndicator.Visibility = Visibility.Visible;
            PPTXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();

            CommonTasks.SendPPTXXLSX(this, pd, tables, GetReportName() + ".pptx", GetReportName() + ".xlsx", GetMailSubject(), GetMailRecepients(), () => Dispatcher.BeginInvoke(() =>
            {
                XLSXProgressIndicator.Visibility = Visibility.Collapsed;
                PPTXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
                HtmlPage.Window.Invoke("CloseWindow");
            }));
        }

        private void ExportDOCX_Click(object sender, RoutedEventArgs e)
        {
            DoExportDOCX();
        }

        public void DoExportDOCX()
        {
            var pd = CreatePresentationDescription();
            DOCXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            CommonTasks.CreateDOCX(this, pd, GetReportName() + ".docx", () => Dispatcher.BeginInvoke(() =>
            {
                DOCXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        public void SendDOCX()
        {
            var pd = CreatePresentationDescription();
            DOCXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            CommonTasks.SendDOCX(this, pd, GetReportName() + ".docx", GetMailSubject(), GetMailRecepients(), () => Dispatcher.BeginInvoke(() =>
            {
                DOCXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));
        }

        private string GetReportName()
        {
            var fname = "Презентация " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null);
            var lastVariables = InternalSlideCollection
                .Select(a => a.SourceData.SingleOrDefault(b => b.TableName == "Переменные")).LastOrDefault();
            if (lastVariables != null && lastVariables.ColumnNames.Contains("ReportName"))
            {
                fname = lastVariables.GetColumnData("ReportName").FirstOrDefault()?.ToString() ?? fname;
            }
            return fname;
        }

        private string GetMailSubject()
        {
            var lastVariables = InternalSlideCollection
                .Select(a => a.SourceData.SingleOrDefault(b => b.TableName == "Переменные")).LastOrDefault();
            var mailheader = "Авторассылка АС \"Анализ Качества\"";
            if (lastVariables != null && lastVariables.ColumnNames.Contains("Subject"))
            {
                mailheader = lastVariables.GetColumnData("Subject").FirstOrDefault()?.ToString() ?? string.Empty;
            }

            string subject;
            if (HtmlPage.Document.QueryString.TryGetValue("Subject", out subject))mailheader = subject;
            return mailheader;
        }

        private string GetMailRecepients()
        {
            var lastVariables = InternalSlideCollection
                .Select(a => a.SourceData.SingleOrDefault(b => b.TableName == "Переменные")).LastOrDefault();
            var maillist = string.Empty;
            if (lastVariables != null && lastVariables.ColumnNames.Contains("Recepients"))
            {
                maillist = lastVariables.GetColumnData("Recepients").FirstOrDefault()?.ToString() ?? string.Empty;
            }
            string rcpt;
            if (HtmlPage.Document.QueryString.TryGetValue("Recipients", out rcpt)) maillist = rcpt;
            return maillist;
        }

        private PresentationDescription CreatePresentationDescription()
        {
            var color = ColorConvertor.GetDefaultColorForScale(lastColorScale).Substring(3,6);
            var pd = new PresentationDescription {Slides = new List<SlideDescription>(), ForegroundColor = color, BlackBackground = inverted};
            var sd = new SlideDescription {SlideElements = new List<SlideElementDescription>()};
            foreach (var csed in InternalSlideCollection)
            {
                sd.SlideElements.Add(csed.SED);
                if (!csed.IsContinuous)
                {
                    pd.Slides.Add(sd);
                    sd = new SlideDescription { SlideElements = new List<SlideElementDescription>() };
                }
            }
            if (sd.SlideElements.Count>0)pd.Slides.Add(sd);
            return pd;
        }

        private void FirstDivider_MarkedAsInsertion(object o, EventArgs e)
        {
            _insertionIndex = 0;
        }

        private void SlideView_SlideAboutDelete(object o, SlideViewEventArgs e)
        {
            var index = InternalSlideCollection.IndexOf(e.CSED);
            if (index >= 0)
            {
                InternalSlideCollection.Remove(e.CSED);
                if (index < _insertionIndex) _insertionIndex--;
                if (!InternalSlideCollection.Any())_insertionIndex = 0;
            }
            ResetInsertionPoint();
        }

        #region Drag&Drop

        private int _dragStartPosition = -1;
        private int _dragEndPosition = -1;
        private bool _dragStarted;
        private Point _offset;

        private void SlideView_DragStarted(object o, SlideDragEventArgs e)
        {
            var sv = o as SlideView;
            _dragStarted = true;
            _offset = e.Offset;
            _dragStartPosition = InternalSlideCollection.IndexOf(e.CSED);
            if (sv != null)
            {
                var i = new Image { Source = e.CSED.Icon, Width = sv.ActualWidth, Height = sv.ActualHeight };
                SlideDragView.SetDragItem(i);
            }
            SlideDragView.SetDropPossibilityIcon(null);
        }

        private void SlideView_DragStopped(object o, SlideDragEventArgs e)
        {
            if (_dragStarted)
            {
                _dragEndPosition = InternalSlideCollection.IndexOf(e.CSED);
                if (_dragEndPosition != _dragStartPosition)
                {
                    ClientSlideElementDescription movingElement = InternalSlideCollection[_dragStartPosition];
                    InternalSlideCollection.Remove(movingElement);
                    InternalSlideCollection.Insert(_dragEndPosition, movingElement);
                    RecalcBreaks();
                    _insertionIndex = _dragEndPosition+1;
                }
            }
            StopDrag();
        }

        private void StopDrag()
        {
            _dragStartPosition = -1;
            _dragEndPosition = -1;
            _dragStarted = false;
            ResetInsertionPoint();
            SlideDragView.RemoveDragItem();
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            var currentPosition = e.GetPosition(SlidePresenter);
            SlideDragView.SetDragItemPosition(new Point(currentPosition.X - _offset.X, currentPosition.Y - _offset.Y));
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            StopDrag();
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            StopDrag();
        }

        #endregion

        private void SlideDivider_MarkedAsInsertion(object o, EventArgs e)
        {
            var slideDivider = o as SlideDivider;
            if (slideDivider != null) _insertionIndex = 1 + InternalSlideCollection.IndexOf(slideDivider.Tag as ClientSlideElementDescription);
        }

        private void SlideView_OnSlideToAnalysis(object o, SlideToAnalysisEventArgs e)
        {
            SlideToAnalysis?.Invoke(o, e);
        }
    }

    public class ClientSlideElementDescription : INotifyPropertyChanged
    {

        public event SlideContentChangedDelegate SlideContentChanged;
        public event SlideIconChangedDelegate SlideIconChanged;

        public SlideElementDescription SED { get; set; }
        public SlideView ContentViewer { get; set; }
        public List<Step> SourceCalculation { get; set; }

        private List<SLDataTable> _sourceData;

        public List<SLDataTable> SourceData
        {
            get { return _sourceData;}
            set
            {
                _sourceData = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SourceData"));
            }
        }

        public DateTime? From, To;

        public int StepInSourceCalculation { get; set; }

        public string SelectedTable { get; set; }

        public List<string> UsedFields { get; set; }

        public Guid SourceKey { get; set; }
        public Step Operation { get; set; }

        private WriteableBitmap _thumbnail;
        public WriteableBitmap Thumbnail {
            get
            {
                return _thumbnail;
            }
            set
            {
                _thumbnail = value;
                SlideContentChanged?.Invoke(this, new SlideContentChangedEventArgs {Chart= value});
            } 
        }

        private WriteableBitmap _icon;
        public WriteableBitmap Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                SlideIconChanged?.Invoke(this, new SlideIconChangedEventArgs { Icon = value });
            }
        }

        private SlideDividerMode _dividermode;
        public SlideDividerMode DividerMode 
        {
            get { return _dividermode; }
            set { _dividermode = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DividerMode")); }
        }

        public WriteableBitmap IconLR;

        private bool _iscontinuous;
        public bool IsContinuous
        {
            get { return _iscontinuous; }
            set { _iscontinuous = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsContinuous")); }
        }

        private bool _isbusy;
        public bool IsBusy
        {
            get { return _isbusy; }
            set { _isbusy = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsBusy")); }
        }

        private bool _isReady;
        public bool IsReady
        {
            get { return _isReady; }
            set { _isReady = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsReady")); }
        }

        private bool _isinsertion;
        public bool IsInsertion
        {
            get { return _isinsertion; }
            set { _isinsertion = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsInsertion")); }
        }
        
        private bool _allownonbreak;
        public bool AllowNonBreak
        {
            get { return _allownonbreak; }
            set { _allownonbreak = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AllowNonBreak")); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
    }

    public delegate void SlideCollectionChangedDelegate(object o, SlideCollectionChangedEventArgs e);

    public delegate void InternalTablesReadyDelegate(object o, InternalTablesReadyForExportEventArgs e);

    public delegate void SlideContentChangedDelegate(object o, SlideContentChangedEventArgs e);

    public delegate void SlideIconChangedDelegate(object o, SlideIconChangedEventArgs e);

    public class SlideContentChangedEventArgs
    {
        public WriteableBitmap Chart;
    }
    
    public class SlideIconChangedEventArgs
    {
        public WriteableBitmap Icon;
    }

    public class SlideCollectionChangedEventArgs
    {
        public List<ClientSlideElementDescription> SlideCollection;
    }

    public class AnalysisResult
    {
        public List<Step> InnerSteps;
        public bool OnlyInnerSteps;
        public Step AnalysisOperation;
        public SLDataTable UsedTable;
        public List<string> UsedFields;
        public List<SLDataTable> AvailableData;
        public List<SLDataTable> InnerTables;
        public SlideElementDescription SED;
        public WriteableBitmap ThumbNail;
        public WriteableBitmap Icon;
        public WriteableBitmap IconLR;
        public ColorScales UsedColorScale;
        public bool IsInverted;
        public DateTime? From;
        public DateTime? To;
        public Guid AnalysisGuid;

        public SerializableAnalysisResult GetSerializableAnalysisResult()
        {
            return new SerializableAnalysisResult
            {
                UsedFields = UsedFields,
                UsedTable = UsedTable.TableName,
                InnerTables = InnerTables,
                AnalysisGuid = AnalysisGuid,
                AnalysisOperation = AnalysisOperation,
                OnlyInnerSteps = OnlyInnerSteps,
                SED = SED,
                From = From,
                To = To,
                IsInverted = IsInverted,
                UsedColorScale = UsedColorScale,
                IconSerializable = GetArrayForBitmap(),
            };
        }

        private string GetArrayForBitmap()
        {
            return Convert.ToBase64String(IconLR.Pixels.SelectMany(BitConverter.GetBytes).ToArray());
        }
    }

    public class SerializableAnalysisResult
    {
        public bool OnlyInnerSteps;
        public Step AnalysisOperation;
        public string UsedTable;
        public List<string> UsedFields;
        public List<SLDataTable> InnerTables;
        public SlideElementDescription SED;
        public string IconSerializable;
        public ColorScales UsedColorScale;
        public bool IsInverted;
        public DateTime? From;
        public DateTime? To;
        public Guid AnalysisGuid;
    }

    [Flags]
    public enum ReportElementType
    {
        None = 0x00,
        Image = 0x01,
        DataTable = 0x02,
        Table = 0x04,
        Text = 0x08,
        All = 0xff
    }
}
