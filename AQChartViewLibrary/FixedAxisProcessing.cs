﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using AQChartViewLibrary.UI;
using AQChartLibrary;
using AQControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private void ShowPopup(string axisTag, FixedAxis axis)
        {
            MainHistoryNavigation.CloseHistory();
            var fare = new FixedAxisRangeEditor();
            fare.FixedAxisChanged += FareOnFixedAxisChanged;
            CustomWindow.Child = fare;
            CustomWindow.IsOpen = true;
            CustomWindow.VerticalOffset = _mousePosition.Y - 60;
            CustomWindow.HorizontalOffset = _mousePosition.X;
            fare.SetFixedAxis(axisTag, axis, AnalysisFixedAxises);
        }

        private void FareOnFixedAxisChanged(object o, FixedAxisChangedEventArgs fixedAxisChangedEventArgs)
        {
            if (fixedAxisChangedEventArgs.Finished) MakeReportChart(false);
            CustomWindow.IsOpen = !fixedAxisChangedEventArgs.Finished;
        }

        #region Объединение фиксации из UI и автоматически рассчитанных значений из графика

        void chart_AxisRangeReady(object o, Chart.AxisRangeReadyEventArgs e)
        {
            RenewFixedAxises(e);
        }

        private void RenewFixedAxises(Chart.AxisRangeReadyEventArgs e)
        {
            if (AnalysisFixedAxises == null) AnalysisFixedAxises = new FixedAxises();
            if (AnalysisFixedAxises.FixedAxisCollection == null) AnalysisFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X"), e.BottomAxisRange, "X");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X2"), e.TopAxisRange, "X2");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y"), e.LeftAxisRange, "Y");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y2"), e.RightAxisRange, "Y2");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Z"), e.ZAxisRange, "Z");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "W"), e.WAxisRange, "W");
        }

        private void MoveAxisRangeDataToFixedAxis(FixedAxis axis, AxisRange ar, string name)
        {
            if (axis == null)
            {
                axis = new FixedAxis { Axis = name, DataType = ar.DataType};
                AnalysisFixedAxises.FixedAxisCollection.Add(axis);
            }
            if (!axis.AxisMinFixed && !axis.AxisMinLocked && (ar.DisplayMinimum is double?)) axis.AxisMin = (double?)ar.DisplayMinimum;
            if (!axis.AxisMaxFixed && !axis.AxisMaxLocked && (ar.DisplayMaximum is double?)) axis.AxisMax = (double?)ar.DisplayMaximum;
            if (!axis.AxisMinFixed && !axis.AxisMinLocked && (ar.DisplayMinimum is DateTime?)) axis.AxisMinDate = (DateTime?)ar.DisplayMinimum;
            if (!axis.AxisMaxFixed && !axis.AxisMaxLocked && (ar.DisplayMaximum is DateTime?)) axis.AxisMaxDate = (DateTime?)ar.DisplayMaximum;
            if (!axis.StepFixed && !axis.StepLocked) axis.Step = ar.DisplayStep;

        }
        #endregion

        #region Объединение фиксации из параметров операции и UI
        private void SetFixedAxis(Chart chart)
        {
            chart.AxisRangeReady += chart_AxisRangeReady;
            MergeFixedAxis(chart.FixedAxises); //Первоначальные ограничения графика объединяются с ограничениями из кэша
            chart.FixedAxises = AnalysisFixedAxises;
        }

        private void MergeFixedAxis(FixedAxises chartSpecificAxises)
        {
            if (AnalysisFixedAxises == null) AnalysisFixedAxises = new FixedAxises();
            if (AnalysisFixedAxises.FixedAxisCollection == null) AnalysisFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MergeFixedAxisData(chartSpecificAxises, "X");
            MergeFixedAxisData(chartSpecificAxises, "X2");
            MergeFixedAxisData(chartSpecificAxises, "Y");
            MergeFixedAxisData(chartSpecificAxises, "Y2");
            MergeFixedAxisData(chartSpecificAxises, "Z");
            MergeFixedAxisData(chartSpecificAxises, "W");
        }
        private void MergeFixedAxisData(FixedAxises chartSpecificAxises, string axisname)
        {
            if (chartSpecificAxises == null) return;
            var source = chartSpecificAxises.FixedAxisCollection.LastOrDefault(a => a.Axis == axisname);
            var destination = AnalysisFixedAxises.FixedAxisCollection.LastOrDefault(a => a.Axis == axisname);

            if (source != null)
            {
                if (destination != null)
                {
                    if (source.AxisMinLocked)
                    {
                        if (source.AxisMin!=null) destination.AxisMin = source.AxisMin;
                        destination.AxisMinDate = source.AxisMinDate;
                        destination.AxisMinFixed = source.AxisMinFixed;
                    }
                    if (source.AxisMaxLocked)
                    {
                        if (source.AxisMax != null) destination.AxisMax = source.AxisMax;
                        destination.AxisMaxDate = source.AxisMaxDate;
                        destination.AxisMaxFixed = source.AxisMaxFixed;
                    }
                    if (source.StepLocked)
                    {
                        destination.Step = source.Step;
                        destination.StepFixed = source.StepFixed;
                    }
                    destination.AxisMinLocked = source.AxisMinLocked;
                    destination.AxisMaxLocked = source.AxisMaxLocked;
                    destination.StepLocked = source.StepLocked;
                    destination.DataType = source.DataType;
                    destination.StringIsDateTime = source.StringIsDateTime;
                    if (destination.Format==null) destination.Format = source.Format;
                }
                else
                {
                    AnalysisFixedAxises.FixedAxisCollection.Add(source);
                }
            }
        }
        #endregion

        #region События интерфейса
        void irc_AxisClicked(object o, AxisEventArgs e)
        {
            var axisTag = e.Tag.ToString(CultureInfo.InvariantCulture);

            if (AnalysisFixedAxises?.FixedAxisCollection == null ||
                AnalysisFixedAxises.FixedAxisCollection.All(a => a.Axis != axisTag))
            {
                if (AnalysisFixedAxises?.FixedAxisCollection == null)AnalysisFixedAxises = new FixedAxises { Editable = true, FixedAxisCollection = new ObservableCollection<FixedAxis>()};
                AnalysisFixedAxises.FixedAxisCollection.Add(new FixedAxis {Axis = axisTag});
            }
            var fixedAxis = AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == axisTag);
            if (fixedAxis == null) return;

            ShowPopup(axisTag, fixedAxis);
        }

        private void ChartPanel_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ClosePopups();
        }
        #endregion
    }
}