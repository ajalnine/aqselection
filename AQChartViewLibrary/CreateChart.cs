﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using AQReportingLibrary;
using AQControlsLibrary;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private void MakeReportChart(bool recordToHistory)
        {
            AnalysisStarted?.Invoke(this, new AnalysisStartedEventArgs());
            // _internalReportData = null;
            ChartStretcher.Stretch = (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.Stretched) == ReportChartLayoutMode.Stretched ? Stretch.Uniform : Stretch.None;
            if (_dataTables == null) { SetNoDataMessage(); return; }
            if (_selectedTable == null)
            {
                SetNoDataMessage();
                _selectedTable = _dataTables.LastOrDefault();
                MessageBox.Show("Таблица данных не найдена. Будет использована таблица " + _selectedTable?.TableName,
                    "Ошибка", MessageBoxButton.OK);
                SelectedTableChanged?.Invoke(this, new SelectedTableChangedEventArgs { SelectedTable = _selectedTable, AllTables = _dataTables });
            }
            _reportChart?.Cancel();
            _reportChart = GetReportChartByUI();
            if (_reportChart == null) { SetNoDataMessage(); return; }
            SetDescriptions(_reportChart);
            _reportChart.FieldMarkChanging += ReportChartOnFieldMarkChanging;
            _reportChart.ReportChartChanged += ReportChartChanged;
            _reportChart.UiReflectChanges += IrcUiReflectChanges;
            _reportChart.AxisClicked += irc_AxisClicked;
            _reportChart.ReportInteractiveRename += irc_ReportInteractiveRename;
            _reportChart.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
            _reportChart.InteractiveParametersChange += ReportChartInteractiveParametersChange;
            _reportChart.Interactivity += ReportChartInteractivity;
            _reportChart.Finished += ReportChartOnFinished;
            _reportChart.RangeAppended += ReportChartOnRangeAppended;
            _currentChartStep = _reportChart.GetStep();
            if (recordToHistory) MainHistoryNavigation.AppendItem(_currentChartStep);
            else MainHistoryNavigation.UpdateLastItem(_currentChartStep);
            _reportChart.InternalTablesReadyForExport += ReportChartInternalTablesReadyForExport;
            if(!_reportChart.MakeVisuals(_dataTables, ChartPanel)) SetNoDataMessage();
        }

        private void ReportChartOnRangeAppended(object o, RangeAppendedEventArgs e)
        {
            _innerSteps.Add(StepFactory.CreateSetRanges(e.SelectedTable, e.LayerField, e.NewRange.Group, new List<Range> {e.NewRange}));
            _operationGuid = Guid.NewGuid();
        }

        private void ReportChartOnFinished(object o, ReportChartFinishedEventArgs reportChartFinishedEventArgs)
        {
            _operationGuid = Guid.NewGuid();
            AnalysisFinished?.Invoke(this, new AnalysisReadyEventArgs {Result = GetAnalisysResult()});
        }

        private void RefreshReportChart()
        {
            AnalysisStarted?.Invoke(this, new AnalysisStartedEventArgs());
            ChartStretcher.Stretch = (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.Stretched) == ReportChartLayoutMode.Stretched ? Stretch.Uniform : Stretch.None;
            if (_dataTables == null || _reportChart == null) { SetNoDataMessage(); return; }
            RefreshReportChartsParametersFromUI(_reportChart);
            _currentChartStep = _reportChart.GetStep();
            MainHistoryNavigation.UpdateLastItem(_currentChartStep);
            _reportChart.RefreshVisuals();
        }

        private void ReportChartInteractiveParametersChange(object o, InteractiveParametersEventArgs e)
        {
            SetUIbyStep(e.Step);
            SetupFieldSelector();
            MainFieldsSelector.SelectAll(StepXFieldToSelect, StepYFieldsToSelect, StepZFieldToSelect, StepWFieldToSelect, StepVFieldToSelect);
            RefreshParametersUI();
            RefreshChartPanelVisibility();
            MakeReportChart(true);
        }

        private void ReportChartInternalTablesReadyForExport(object o, InternalTablesReadyForExportEventArgs e)
        {
            _innerTables = e.InternalTables;
            InternalTablesReadyForExport?.Invoke(this, e);
        }

        private void ReportChartChartSetsFixedAxis(object o, ChartSetsFixedAxisEventArgs e)
        {
            SetFixedAxis(e.SettingChart);
        }

        private void ReportChartOnFieldMarkChanging(object o, FieldMarkChanging fieldMarkChanging)
        {
            if (fieldMarkChanging.ResetRequired) ClearMarks();
            foreach (MarkedItem x in MainFieldsSelector.GetXItems())
            {
                if (fieldMarkChanging.XFieldMarks.ContainsKey(x.Field)) x.Mark = fieldMarkChanging.XFieldMarks[x.Field];
            }
            foreach (MarkedItem y in MainFieldsSelector.GetYItems())
            {
                if (fieldMarkChanging.YFieldMarks.ContainsKey(y.Field)) y.Mark = fieldMarkChanging.YFieldMarks[y.Field];
            }
        }
    }
}