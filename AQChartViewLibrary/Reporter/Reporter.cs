﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;
using AQClientStepLibrary;
using AQControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public class Reporter
    {
        private int _s, _insertionPoint, _queuePosition;
        private readonly Task _task;
        private readonly CalculationDescription _calc;
        private SlidePanel _sp;
        private readonly CalculationView _cv;
        private readonly CalculationService _cs;
        private readonly bool _forExport;
        private List<QueueElement> _queue;
        private List<Guid> _pureTablesAtStep, _pureChartsAtStep, _combinedOperationAtStep;
        private DateTime? _from, _to;

        public Reporter(CalculationService cs, Task task, CalculationDescription calc, bool forExport, DateTime? from, DateTime? to, CalculationView cv = null)
        {
            _cv = cv;
            _cs = cs;
            _task = task;
            _calc = calc;
            _forExport = forExport;
            _from = from;
            _to = to;
        }

        public void DetachFromSlidePanel()
        {
            _sp.SlideReady -= SlideReady;
        }

        public void AttachToSlidePanel(SlidePanel sp)
        {
            _sp = sp;
            _insertionPoint = _sp.ResetInternalSlidesCollection(true);
            _sp.SlideReady += SlideReady;
        }

        public void DoServerSteps(Action finalizejob)
        {
            _task.SetState(_task.CurrentStep, TaskState.Processing);
            _cv?.ShowProgressInScheme(_task.CurrentStep, TaskState.Processing);

            if (_calc.StepData[_task.CurrentStep].Group == "Отчет")
            {
                ExecuteAnalysisStep(finalizejob);
            }
            else
            {
                DoSingleStep(finalizejob);
            }
        }

        private void DoSingleStep(Action finalizejob)
        {
            _cs.BeginExecuteStep(_task.CustomData.ToString(), _calc.StepData[_task.CurrentStep],
                iar =>
                {
                    try
                    {
                        var sr = _cs.EndExecuteStep(iar);
                        if (sr.Success)
                        {
                            _task.SetState(_task.CurrentStep, TaskState.Ready);
                            _cv?.ShowProgressInScheme(_task.CurrentStep, TaskState.Ready);
                            _task.CurrentStep++;
                            if (_task.CurrentStep < _task.NumberOfSteps - 1) DoServerSteps(finalizejob);
                            else finalizejob();
                        }
                        else
                        {
                            _task.SetState(_task.CurrentStep, TaskState.Error);
                            _task.SetMessage(sr.Message);
                            _cv?.ShowProgressInScheme(_task.CurrentStep, TaskState.Error);
                            _cs.BeginDropData(sr.TaskDataGuid, null, null);
                        }
                    }
                    catch
                    {
                        _task.SetState(_task.CurrentStep, TaskState.Error);
                        _cv?.ShowProgressInScheme(_task.CurrentStep, TaskState.Error);
                    }
                }, _task);
        }

        #region Исполнение операции Анализ

        private Parameters _currentParameters;
        private List<SLDataTable> _data;
        private Action _finalizeAnalysisInternalStepProxessing;
        
        private void ExecuteAnalysisStep(Action finalizejob)
        {
            _finalizeAnalysisInternalStepProxessing = finalizejob;
            _cs.BeginGetTablesSLCompatible(_task.CustomData.ToString(), iar =>
            {
                _data = _cs.EndGetTablesSLCompatible(iar);
                var xs = new XmlSerializer(typeof(Parameters));
                var sr = new StringReader(_calc.StepData[_task.CurrentStep].ParametersXML);
                _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
                _currentParameters.NewTables = new List<NewTableIndex>();
                _s = 0;

                foreach (var sar in _currentParameters.SARs)
                {
                    sar.From = _from;
                    sar.To = _to;
                }

                _sp.Dispatcher.BeginInvoke(() =>
                {
                    _insertionPoint = _sp.ResetInternalSlidesCollection(false, _currentParameters.SARs.Count);
                    CreateQueueForStep();
                });
            }, _task);
        }

        private void CreateQueueForStep()
        {
            _queue = new List<QueueElement>();

            _pureTablesAtStep = _currentParameters.TableGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Except(_currentParameters.SARs.Select(a => a.AnalysisGuid)).ToList();

            _pureChartsAtStep = _currentParameters.ChartGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Where(a => !_currentParameters.TableGeneratingOperations.Select(b=>b.Guid).Contains(a)).ToList();

            _combinedOperationAtStep = _currentParameters.ChartGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Where(a => _currentParameters.TableGeneratingOperations.Select(b => b.Guid).Contains(a)).ToList();

            _queue.AddRange(_pureTablesAtStep.Select(a => new QueueElement { Guid = a }));
            foreach (var co in _combinedOperationAtStep)
            {
                var sars = _currentParameters.SARs.Where(a => co == a.AnalysisGuid);
                foreach (var sar in sars)
                {
                    var position = _currentParameters.SARs.IndexOf(sar);
                    _queue.Add(new QueueElement { Guid = co, SAR = sar, SARPosition = position });
                }
            }
            foreach (var co in _pureChartsAtStep)
            {
                var sars = _currentParameters.SARs.Where(a => co == a.AnalysisGuid);
                foreach (var sar in sars)
                {
                    var position = _currentParameters.SARs.IndexOf(sar);
                    _queue.Add(new QueueElement { Guid = co, SAR = sar, SARPosition = position });
                }
            }
            _queuePosition = 0;
            if (_queue.Count > 0) ProcessQueuePosition();
            else DoStepOrFinalize();
        }

        private void ProcessQueuePosition()
        {
            var processingItem = _queue[_queuePosition];
            _sp.Dispatcher.BeginInvoke(() =>
            {
                if (_pureTablesAtStep.Contains(processingItem.Guid))
                {
                    var o = _currentParameters.TotalOperations.SingleOrDefault(a => processingItem.Guid == a.Guid);
                    if (o != null) _sp.GetInternalTablesForSAR(_data, o.Operation, o.Guid);
                }
                else
                {
                    _sp.RenderSAR(_data, processingItem.SAR, _forExport, _insertionPoint + processingItem.SARPosition);
                }
            });
        }

        public void SlideReady(object o, SlideReadyEventArgs e)
        {
            if ((e.ElementType & ReportElementType.DataTable) == ReportElementType.DataTable)
            {
                if (_currentParameters.TableGeneratingOperations.Select(a => a.Guid).Contains(e.SAR.AnalysisGuid))
                {
                    foreach (var i in e.SAR.InnerTables)
                    {
                        var tableToReplace = _data.SingleOrDefault(a => a.TableName == i.TableName);
                        if (tableToReplace != null)
                        {
                            _data.Remove(tableToReplace);
                        }
                        _data.Add(i);
                        _currentParameters.NewTables.Add(new NewTableIndex { NewTable = i.Clone(), PositionOfOperation = _s });
                    }
                }
            }
            _queuePosition++;
            if (_queuePosition < _queue.Count) ProcessQueuePosition();
            else DoStepOrFinalize();
        }

        public class Parameters
        {
            public List<Step> InternalSteps;
            public List<GuidStepPair> TotalOperations;
            public List<OperationIndex> TableGeneratingOperations;
            public List<OperationIndex> ChartGeneratingOperations;
            public List<SerializableAnalysisResult> SARs;
            public List<NewTableIndex> NewTables;
            public string SelectedTable;
            public bool IsInvertedPanel;
            public bool LastOperationTableAdded;
            public ColorScales DefaultColorScale;
        }

        private void DoStepOrFinalize()
        {
            if (_s < _currentParameters.InternalSteps.Count)
            {
                ClientStepProcessor.ProcessStep(_data, _currentParameters.InternalSteps[_s]);
                _s++;
                CreateQueueForStep();
            }
            else
            {
                var toSend = new List<SLDataTable>();
                foreach (var n in _currentParameters.NewTables)
                {
                    var t = n.NewTable.Clone();
                    t.TableName = $"${n.PositionOfOperation.ToString("000000")}" + t.TableName;
                    toSend.Add(t);
                }
                
                _cs.BeginAppendSLTables(_task.CustomData.ToString(), toSend, iar =>
                {
                    DoSingleStep(_finalizeAnalysisInternalStepProxessing);
                }, _task);
            }
        }
        #endregion
    }
    public class QueueElement
    {
        public Guid Guid;
        public SerializableAnalysisResult SAR;
        public int SARPosition;
    }
}