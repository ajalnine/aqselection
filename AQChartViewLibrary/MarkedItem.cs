﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace AQChartViewLibrary
{
    public class MarkedItem : INotifyPropertyChanged
    {
        private string _field;
        private Visibility _showCheckBox = Visibility.Collapsed;
        private Visibility _showFilter = Visibility.Collapsed;
        private Color _mark;
        private bool _isSelected;
        public object Source;

        public MarkedItem()
        {
            Mark = Colors.Black;
        }

        public MarkedItem(string field)
        {
            Mark = Colors.Black;
            Field = field;
        }

        public string Field
        {
            get { return _field; }
            set
            {
                _field = value;
                NotifyPropertyChanged("Field");
            }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyPropertyChanged("IsSelected");
            }
        }

        public Visibility ShowCheckBox
        {
            get { return _showCheckBox; }
            set
            {
                _showCheckBox = value;
                NotifyPropertyChanged("ShowCheckBox");
            }
        }

        public Visibility ShowFilter
        {
            get { return _showFilter; }
            set
            {
                _showFilter = value;
                NotifyPropertyChanged("ShowFilter");
            }
        }

        public Color Mark
        {
            get { return _mark; }
            set
            {
                _mark = value;
                NotifyPropertyChanged("Mark");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
