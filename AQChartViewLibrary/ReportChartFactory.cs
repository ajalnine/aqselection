﻿using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private IReportElement GetReportChartByUI()
        {
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.Statistics:
                    return new ReportStatistics(GetReportStatisticsParameters());

                case ReportChartType.Dynamic:
                    return new ReportChartDynamic(GetReportChartDynamicParameters());

                case ReportChartType.Card:
                    var cardType = MainCardTypeSelector.GetCurrentCardType();
                    switch (cardType)
                    {
                        case ReportChartCardType.CardTypeIsTrends:
                            return new ReportChartCard(GetReportChartCardIsTrendParameters(cardType));

                        case ReportChartCardType.CardTypeIsGraphics:
                            return new ReportChartCard(GetReportChartCardIsGraphicsParameters(cardType));

                        default:
                            return new ReportChartCard(GetReportChartCardIsGenericParameters(cardType));
                    }

                case ReportChartType.Histogram:
                    var type = MainHistogramTypeSelector.GetCurrentHistogramType();
                    if (type == ReportChartHistogramType.Circles)
                    {
                        return new ReportChartCircles(GetChartIsCirclesParameters());
                    }
                    else
                    {
                        var aggregate = MainAggregateSelector.GetCurrentAggregate();
                        var p = GerChartIsFreqParameters(aggregate);
                        switch (p.HistogramType)
                        {
                            case ReportChartHistogramType.Pareto:
                                return new ReportChartPareto(p);
                            case ReportChartHistogramType.Frequencies:
                                return new ReportChartStratification(p);
                            case ReportChartHistogramType.Columns:
                                return new ReportChartColumns(p);
                            case ReportChartHistogramType.Bands:
                                return new ReportChartBands(p);
                            case ReportChartHistogramType.Circles:
                                return new ReportChartCircles(GetChartIsCirclesParameters());
                            default:
                                return new ReportChartStratification(p);
                        }
                    }

                case ReportChartType.Groups:
                    return new ReportChartGroups(GetChartIsGroupCardParameters());

                case ReportChartType.ScatterPlot:
                    return new ReportChartScatterPlot(GetReportChartScatterPlotParameters());

                case ReportChartType.MultipleRegression:
                    return new ReportChartMultipleRegression(GetReportChartIsMultipleRegressionParameters());

                case ReportChartType.ProbabilityPlot:
                    return new ReportChartProbabilityPlot(GetReportChartProbabilityPlotParameters());
                default:
                    return null;
            }
        }
    }
}