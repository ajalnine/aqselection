﻿using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;
using System.Collections.Generic;

namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        public string StepTableToSelect = null;
        public string StepXFieldToSelect = null;
        public List<string> StepYFieldsToSelect = new List<string>();
        public string StepZFieldToSelect = null;
        public string StepWFieldToSelect = null;
        public string StepVFieldToSelect = null;

        public SLDataTable SetUIbyStep(Step s)
        {
            if (s == null) return null;
            switch (s.Calculator.Substring(5, s.Calculator.Length - 5))
            {
                case "ReportChartDynamic":
                    var p1 = DeserializeParameters<ReportChartDynamicParameters>(s);
                    StepTableToSelect = p1.Table;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Dynamic);
                    MainRangeModeSelector.SetCurrentRangeType(p1.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p1.UserLimits);
                    MainRangeModeSelector.SetCurrentRangeStyle(p1.RangeStyle);
                    MainSmoothnessSelector.SetCurrentSmoothness(p1.SmoothMode);
                    MainSmoothnessSelector.SetCurrentLayerMode(p1.LayersMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p1.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p1.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p1.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p1.ChartLegend);
                    MainRangeModeSelector.SetMarkOutage(p1.MarkOutage);
                    MainDateRangeSelector.SetCurrentPeriodStyle(p1.PeriodStyle);
                    MainDateRangeSelector.SetupPeriods(p1.UserPeriods);
                    MainSmoothnessSelector.SetCurrentSmoothStyle(p1.SmoothStyle);
                    MainSmoothnessSelector.SetCurrentLWSArea(p1.LWSArea);
                    
                    StepYFieldsToSelect = p1.Fields;
                    StepXFieldToSelect = p1.DateField;
                    StepZFieldToSelect = p1.ColorField;
                    StepWFieldToSelect = p1.MarkerSizeField;
                    StepVFieldToSelect = p1.LabelField;
                    MainMarkerSelector.SetCurrentColorScale(p1.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p1.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p1.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p1.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p1.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentFontSize(p1.FontSize);
                    SetupToolsAvailability();
                    MainToolsSelector.SetCurrentTools(p1.Tools);
                    AnalysisFixedAxises = p1.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.Dynamic] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p1.Table);
                    break;

                case "ReportChartCard":
                    var p2 = DeserializeParameters<ReportChartCardParameters>(s);
                    StepTableToSelect = p2.Table;
                    StepVFieldToSelect = p2.DateField;
                    if ((p2.CardType == ReportChartCardType.CardTypeIsTrends) && p2.Fields.Count > 1)
                    {
                        StepXFieldToSelect = p2.Fields.Skip(1).FirstOrDefault();
                    }
                    if (p2.CardType == ReportChartCardType.CardTypeIsGraphics)
                    {
                        StepZFieldToSelect = p2.LayerField;
                        StepXFieldToSelect = p2.GroupField;
                    }
                    else
                    {
                        StepZFieldToSelect = p2.GroupField;
                    }
                    StepYFieldsToSelect = p2.Fields;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Card);
                    MainSeriesCheckSelector.SetCurrentChartSeriesType(p2.Series);
                    MainCardTypeSelector.SetCurrentCardType(p2.CardType);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p2.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p2.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p2.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p2.ChartLegend);
                    MainRangeModeSelector.SetCurrentRangeType(p2.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p2.UserLimits);
                    MainRangeModeSelector.SetCurrentRangeStyle(p2.RangeStyle);
                    MainSmoothnessSelector.SetCurrentLWSArea(p2.LWSArea);
                    MainMarkerSelector.SetCurrentColorScale(p2.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p2.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p2.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p2.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p2.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentFontSize(p2.FontSize);
                    SetupToolsAvailability();
                    MainToolsSelector.SetCurrentTools(p2.Tools);
                    AnalysisFixedAxises = p2.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.Card] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p2.Table);
                    break;

                case "ReportChartGroups":
                    var p3 = DeserializeParameters<ReportChartGroupsParameters>(s);
                    StepTableToSelect = p3.Table;
                    StepYFieldsToSelect = p3.Fields.Take(1).ToList();
                    StepXFieldToSelect = p3.GroupField;
                    StepZFieldToSelect = p3.ColorField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Groups);
                    MainGroupParametersSelector.SetCurrentGroupParameters(p3.Center, p3.ShowOutliers, p3.Sorted, p3.ShowText, p3.PointMode, p3.Mode, p3.AnalisysMode);
                    MainRangeModeSelector.SetCurrentRangeType(p3.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p3.UserLimits);
                    MainRangeModeSelector.SetCurrentRangeStyle(p3.RangeStyle);
                    MainRangeModeSelector.SetMarkOutage(p3.MarkOutage);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p3.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p3.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p3.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p3.ChartLegend);
                    MainMarkerSelector.SetCurrentColorScaleMode(p3.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p3.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p3.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p3.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentFontSize(p3.FontSize);
                    AnalysisFixedAxises = p3.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.Groups] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p3.Table);
                    break;

                case "ReportChartHistogram":
                    var p4 = DeserializeParameters<ReportChartHistogramParameters>(s);
                    StepTableToSelect = p4.Table;
                    StepYFieldsToSelect = p4.Fields.Take(1).ToList();
                    StepXFieldToSelect = p4.Fields.Count > 1 ? p4.Fields[1] : p4.Fields[0];
                    StepZFieldToSelect = p4.ColorField;
                    StepVFieldToSelect = p4.Fields.Count > 2 ? p4.Fields[2] : null; 
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Histogram);
                    MainFrequencyParameterSelector.SetCurrentAlignMode(p4.AlignMode);
                    MainFrequencyParameterSelector.SetCurrentYMode(p4.YMode);
                    MainFrequencyParameterSelector.SetDetailLevel(p4.DetailLevel);
                    MainParetoParameterSelector.SetCurrentGroupNumber(p4.GroupNumber);
                    MainFrequencyParameterSelector.SetCurrentStep(p4.Step);
                    MainFrequencyParameterSelector.SetCurrentUserAlignValue(p4.UserAlignValue);
                    MainFrequencyParameterSelector.SetCurrentBinMode(p4.BinMode);
                    MainRangeModeSelector.SetCurrentRangeType(p4.RangeType);
                    MainRangeModeSelector.SetupRangesForAB(p4.UserLimits);
                    MainRangeModeSelector.SetCurrentRangeStyle(p4.RangeStyle);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p4.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p4.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p4.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p4.ChartLegend);
                    MainMarkerSelector.SetCurrentColorScale(p4.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p4.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p4.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p4.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p4.MarkerShapeMode);
                    MainHistogramTypeSelector.SetCurrentHistogramType(p4.HistogramType);
                    MainHistogramLabelsSelector.SetCurrentColumnLabelsParameters(p4.LabelsType);
                    AnalysisFixedAxises = p4.FixedAxisesDescription;
                    if (p4.ColumnsType!= ReportChartColumnDiagramType.Normed) _fixedAxisesCache[ReportChartType.Histogram] = AnalysisFixedAxises;
                    MainFitSelector.SetCurrentFit(p4.FitMode);
                    MainFitSelector.SetCurrentFitStyle(p4.FitStyle);
                    MainAggregateSelector.SetCurrentAggregate(p4.Aggregate);
                    MainColumnsParameterSelector.SetCurrentColumnsParameters(p4.ColumnsType, p4.ColumnsOrder, p4.DrawParts);
                    MainLayoutModeSelector.SetCurrentFontSize(p4.FontSize);
                    SetupToolsAvailability();
                    MainToolsSelector.SetCurrentTools(p4.Tools);
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p4.Table);
                    break;

                case "ReportChartCircles":
                    var p9 = DeserializeParameters<ReportChartCirclesParameters>(s);
                    StepTableToSelect = p9.Table;
                    StepYFieldsToSelect = p9.Fields.Take(1).ToList();
                    StepXFieldToSelect = p9.Fields.Count > 1 ? p9.Fields[1] : p9.Fields[0];
                    StepZFieldToSelect = p9.ColorField;
                    StepVFieldToSelect = p9.Fields.Count > 2 ? p9.Fields[2] : null;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Histogram);
                    MainCircleParametersSelector.SetCurrentCircleParameters(p9.CirclesType, p9.ArrangementMode, p9.LabelsType);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p9.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p9.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p9.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p9.ChartLegend);
                    MainMarkerSelector.SetCurrentColorScale(p9.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p9.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p9.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p9.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p9.MarkerShapeMode);
                    MainHistogramTypeSelector.SetCurrentHistogramType(p9.HistogramType);
                    AnalysisFixedAxises = p9.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.Histogram] = AnalysisFixedAxises;
                    MainAggregateSelector.SetCurrentAggregate(p9.Aggregate);
                    MainLayoutModeSelector.SetCurrentFontSize(p9.FontSize);
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p9.Table);
                    break;

                case "ReportChartScatterPlot":
                    var p5 = DeserializeParameters<ReportChartScatterPlotParameters>(s);
                    StepTableToSelect = p5.Table;
                    StepXFieldToSelect = p5.FieldX;
                    StepYFieldsToSelect = p5.Fields;
                    StepZFieldToSelect = p5.ColorField;
                    StepWFieldToSelect = p5.MarkerSizeField;
                    StepVFieldToSelect = p5.LabelField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ScatterPlot);
                    MainRangeModeSelector.SetCurrentRangeType(p5.RangeType);
                    MainRangeModeSelector.SetCurrentRangeStyle(p5.RangeStyle);
                    MainRangeModeSelector.SetMarkOutage(p5.MarkOutage);
                    MainRangeModeSelector.SetupRangesForAB(p5.UserLimits);
                    MainSmoothnessSelector.SetCurrentSmoothness(p5.SmoothMode);
                    MainSmoothnessSelector.SetCurrentSmoothStyle(p5.SmoothStyle);
                    MainSmoothnessSelector.SetCurrentLayerMode(p5.LayersMode);
                    MainSmoothnessSelector.SetCurrentLWSArea(p5.LWSArea);
                    MainMarkerSelector.SetCurrentColorScale(p5.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p5.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p5.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p5.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p5.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p5.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p5.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p5.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p5.ChartLegend);
                    MainSmoothnessSelector.SetCurrentSmoothStyle(p5.SmoothStyle);
                    MainLayoutModeSelector.SetCurrentFontSize(p5.FontSize);
                    SetupToolsAvailability();
                    MainToolsSelector.SetCurrentTools(p5.Tools);
                    AnalysisFixedAxises = p5.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ScatterPlot] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p5.Table);
                    break;

                case "ReportChartMultipleRegression":
                    var p7 = DeserializeParameters<ReportChartMultipleRegressionParameters>(s);
                    StepTableToSelect = p7.Table;
                    StepXFieldToSelect = p7.FieldX;
                    StepYFieldsToSelect = p7.Fields;
                    StepZFieldToSelect = p7.ColorField;
                    StepWFieldToSelect = p7.MarkerSizeField;
                    StepVFieldToSelect = p7.LabelField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.MultipleRegression);
                    MainRangeModeSelector.SetCurrentRangeType(p7.RangeType);
                    MainRangeModeSelector.SetCurrentRangeStyle(p7.RangeStyle);
                    MainRangeModeSelector.SetMarkOutage(p7.MarkOutage);
                    MainRangeModeSelector.SetupRangesForAB(p7.UserLimits);
                    MainSmoothnessSelector.SetCurrentSmoothness(p7.SmoothMode);
                    MainRegressionSelector.SetCurrentSmoothStyle(p7.SmoothStyle);
                    MainRegressionSelector.SetCurrentLayerMode(p7.LayersMode);
                    MainRegressionSelector.SetCurrentRegression(p7.ViewType);
                    MainRegressionSelector.SetCurrentModelQuality(p7.ModelQuality);
                    MainRegressionSelector.SetCurrentStepwiseType(p7.StepwiseType);
                    MainRegressionSelector.SetCurrentRegressionOptions(p7.RegressionOptions);
                    MainMarkerSelector.SetCurrentColorScale(p7.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p7.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p7.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p7.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p7.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p7.LayoutMode);
                    MainLayoutModeSelector.SetCurrentSize(p7.LayoutSize);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p7.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentChartLegend(p7.ChartLegend);
                    MainSmoothnessSelector.SetCurrentSmoothStyle(p7.SmoothStyle);
                    MainLayoutModeSelector.SetCurrentFontSize(p7.FontSize);
                    AnalysisFixedAxises = p7.FixedAxisesDescription;
                    SetupToolsAvailability();
                    MainToolsSelector.SetCurrentTools(p7.Tools);
                    _fixedAxisesCache[ReportChartType.ScatterPlot] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p7.Table);
                    break;

                case "ReportChartProbabilityPlot":
                    var p6 = DeserializeParameters<ReportChartProbabilityPlotParameters>(s);
                    StepTableToSelect = p6.Table;
                    StepYFieldsToSelect = p6.Fields;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.ProbabilityPlot);
                    MainMarkerSelector.SetCurrentColorScale(p6.ColorScale);
                    MainMarkerSelector.SetCurrentColorScaleMode(p6.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerMode(p6.MarkerMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p6.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p6.MarkerShapeMode);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p6.LayoutMode);
                    MainLayoutModeSelector.SetCurrentChartLegend(p6.ChartLegend);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p6.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentFontSize(p6.FontSize);
                    MainLayoutModeSelector.SetCurrentSize(p6.LayoutSize);
                    AnalysisFixedAxises = p6.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.ProbabilityPlot] = AnalysisFixedAxises;
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p6.Table);
                    break;

                case "ReportStatistics":
                    var p8 = DeserializeParameters<ReportStatisticsParameters>(s);
                    StepTableToSelect = p8.Table;
                    StepYFieldsToSelect = p8.Fields;
                    StepZFieldToSelect = p8.ColorField;
                    MainChartTypeSelector.SetCurrentChartType(ReportChartType.Statistics);
                    MainLayoutModeSelector.SetCurrentLayoutMode(p8.LayoutMode);
                    MainLayoutModeSelector.SetCurrentLimitedLayers(p8.LimitedLayers);
                    MainLayoutModeSelector.SetCurrentFontSize(p8.FontSize);
                    MainLayoutModeSelector.SetCurrentSize(p8.LayoutSize);
                    MainMarkerSelector.SetCurrentMarkerMode(p8.MarkerMode);
                    MainMarkerSelector.SetCurrentColorScaleMode(p8.ColorScaleMode);
                    MainMarkerSelector.SetCurrentMarkerSizeMode(p8.MarkerSizeMode);
                    MainMarkerSelector.SetCurrentMarkerShapeMode(p8.MarkerShapeMode);
                    MainStatisticsSelector.SetStatistics(p8.Statistics);
                    MainStatisticsSelector.SetCurrentLayerMode(p8.LayerMode);
                    MainMarkerSelector.SetCurrentColorScale(p8.ColorScale);
                    _selectedTable = _dataTables.SingleOrDefault(a => a.TableName == p8.Table);
                    AnalysisFixedAxises = p8.FixedAxisesDescription;
                    _fixedAxisesCache[ReportChartType.Statistics] = AnalysisFixedAxises;
                    break;
            }
            return _selectedTable;
        }

        private static T DeserializeParameters<T>(Step s)
        {
            var xs = new XmlSerializer(typeof (T));
            var sr = new StringReader(s.ParametersXML.Replace("Parameters", typeof (T).Name));
            return (T) xs.Deserialize(XmlReader.Create(sr));
        }
    }
}