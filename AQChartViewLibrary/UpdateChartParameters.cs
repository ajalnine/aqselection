﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartViewLibrary.UI;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private void RefreshReportChartsParametersFromUI(IReportElement irc)
        {
            switch (MainChartTypeSelector.GetCurrentChartType())
            {
                case ReportChartType.Statistics:
                    if (irc is ReportStatistics) ((ReportStatistics)irc).SetNewParameters(GetReportStatisticsParameters());
                    return;

                case ReportChartType.Dynamic:
                    if (irc is ReportChartDynamic) ((ReportChartDynamic)irc).SetNewParameters(GetReportChartDynamicParameters());
                    return;

                case ReportChartType.Card:
                    if (!(irc is ReportChartCard)) return;
                    var cardType = MainCardTypeSelector.GetCurrentCardType();
                    switch (cardType)
                    {
                        case ReportChartCardType.CardTypeIsTrends:
                            ((ReportChartCard)irc).SetNewParameters(GetReportChartCardIsTrendParameters(cardType));
                            return;

                        case ReportChartCardType.CardTypeIsGraphics:
                            ((ReportChartCard)irc).SetNewParameters(GetReportChartCardIsGraphicsParameters(cardType));
                            return;

                        default:
                            ((ReportChartCard)irc).SetNewParameters(GetReportChartCardIsGenericParameters(cardType));
                            return;
                    }

                case ReportChartType.Histogram:

                    if (MainHistogramTypeSelector.GetCurrentHistogramType() == ReportChartHistogramType.Circles)
                    {
                        var p = GetChartIsCirclesParameters();
                        ((ReportChartCircles)irc).SetNewParameters(p);
                        return;
                    }
                    else
                    {
                        var aggregate = MainAggregateSelector.GetCurrentAggregate();

                        var p = GerChartIsFreqParameters(aggregate);
                        switch (p.HistogramType)
                        {
                            case ReportChartHistogramType.Pareto:
                                ((ReportChartPareto)irc).SetNewParameters(p);
                                return;
                            default:
                            case ReportChartHistogramType.Frequencies:
                                ((ReportChartStratification)irc).SetNewParameters(p);
                                return;
                            case ReportChartHistogramType.Columns:
                                ((ReportChartColumns)irc).SetNewParameters(p);
                                return;
                            case ReportChartHistogramType.Bands:
                                ((ReportChartBands)irc).SetNewParameters(p);
                                return;
                        }
                    }

                case ReportChartType.Groups:
                    ((ReportChartGroups)irc).SetNewParameters(GetChartIsGroupCardParameters());
                    return;

                default:
                case ReportChartType.ScatterPlot:
                    ((ReportChartScatterPlot)irc).SetNewParameters(GetReportChartScatterPlotParameters());
                    return;
                case ReportChartType.MultipleRegression:
                    ((ReportChartMultipleRegression)irc).SetNewParameters(GetReportChartIsMultipleRegressionParameters());
                    return;
                case ReportChartType.ProbabilityPlot:
                    ((ReportChartProbabilityPlot)irc).SetNewParameters(GetReportChartProbabilityPlotParameters());
                    return;
            }
        }
    }
}