﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQReportingLibrary;
using AQChartLibrary;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQStepFactoryLibrary;

namespace AQChartViewLibrary
{
    public partial class CommandProcessor
    {
        private SLDataTable _table;
        private string _commandField, _destinationField, _message;
        public List<int> Cases;
        public List<object> _selected;

        public Dictionary<InteractiveOperations, CommandProcessorDelegate> processors;

        public CommandProcessor(SLDataTable table, string commandField, string destinationField, List<int> cases, string message, List<object> selected)
        {
            processors = new Dictionary<InteractiveOperations, CommandProcessorDelegate>
            {
                {InteractiveOperations.DeleteValue,     (arg)=>DeleteValueCommand(arg)      },
                {InteractiveOperations.DeleteColumn,    (arg)=>DeleteColumnCommand(arg)     },
                {InteractiveOperations.DeleteCase,      (arg)=>DeleteCaseCommand(arg)       },
                {InteractiveOperations.CopyToNewTable,  (arg)=>CopyToNewTableCommand(arg)   },
                {InteractiveOperations.CutInNewTable,   (arg)=>CutInNewTableCommand(arg)    },
                {InteractiveOperations.SetColor,        (arg)=>SetColorCommand(arg)         },
                {InteractiveOperations.SetColorForRow,  (arg)=>SetColorForRowCommand(arg)   },
                {InteractiveOperations.SetLabel,        (arg)=>SetLabelCommand(arg)         },
                {InteractiveOperations.SplitLayers,     (arg)=>SplitLayersCommand(arg)      },
                {InteractiveOperations.SetFilter,       (arg)=>SetFilterCommand(arg)        },
                {InteractiveOperations.DeleteFilter,    (arg)=>DeleteFilterCommand(arg)     },
                {InteractiveOperations.RemoveFilters,   (arg)=>RemoveFiltersCommand(arg)    },
                {InteractiveOperations.CombineLayers,   (arg)=>CombineLayersCommand(arg)    },
                {InteractiveOperations.OutOfRange,      (arg)=>OutOfRangeCommand(arg)       },
                {InteractiveOperations.Outliers,        (arg)=>OutliersCommand(arg)    },
                {InteractiveOperations.None,            (arg)=>NoneCommand(arg)             },
            };
            _table = table;
            _commandField = commandField;
            _destinationField = destinationField;
            Cases = cases;
            _message = message;
            _selected = selected;
        }

        public SLDataTable GetTable() { return _table; }

        public string GetField() { return _commandField; }

        public string GetMessage() { return _message;  }

        public List<int> GetCases() { return Cases;  }

        public List<object> GetValues() { return _selected; }

        public CommandResult ExecuteComand(InteractiveOperations command, object argument)
        {
            return processors[command](argument);
        }

        public CommandResult DeleteValueCommand(object argument)
        {
            _table.DeleteValues(_destinationField, Cases);
            var steps = StepFactory.CreateLayerOperationDeleteValues(_table.TableName, _commandField, _destinationField, _selected);
            return new CommandResult {TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult DeleteCaseCommand(object argument)
        {
            _table.DeleteCases(_commandField, Cases);
            var steps = StepFactory.CreateLayerOperationDeleteCases(_table.TableName, _commandField, _selected);
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult DeleteColumnCommand(object argument)
        {
            _table.DeleteColumn(_commandField);
            var steps = StepFactory.CreateDeleteFields(_table, new List<string> {_commandField});
            return new CommandResult { TablesCreated = false, NewSteps = steps.ToList()};
        }

        public CommandResult CopyToNewTableCommand(object argument)
        {
            var t = _table.CopyToNewTable(_commandField, Cases);
            t.TableName += ", " + ((_message.Length < 50) ? _message : _message.Substring(0, 47) + "...");
            var steps = StepFactory.CreateLayerOperationCopyIntoTable(_table.TableName, _commandField, t.TableName, _selected);
            return new CommandResult { NewTables = new List<SLDataTable> { t }, TablesCreated = true, NewSteps = new List<Step> { steps } };
        }
        
        public CommandResult CutInNewTableCommand(object argument)
        {
            var t = _table.CutToNewTable(_commandField, Cases);
            t.TableName += ", " + ((_message.Length < 50) ? _message : _message.Substring(0, 47) + "...");
            var steps = StepFactory.CreateLayerOperationCutIntoTable(_table.TableName, _commandField, t.TableName, _selected);
            return new CommandResult { NewTables = new List<SLDataTable> { t }, TablesCreated = true, NewSteps = new List<Step> { steps } };
        }

        public CommandResult SetFilterCommand(object argument)
        {
            if (_table.GetDistinctWithNulls(_commandField).Count()==_selected.Count) return new CommandResult { TablesCreated = false };
            _table.NewFilter(_commandField, _selected);
            _table.ApplyFilters();
            return new CommandResult {TablesCreated = false };
        }

        public CommandResult DeleteFilterCommand(object argument)
        {
            _table.StopFilter(_commandField);
            _table.ApplyFilters();
            return new CommandResult {TablesCreated = false };
        }

        public CommandResult RemoveFiltersCommand(object argument)
        {
            _table.StopFiltering();
            return new CommandResult { TablesCreated = false };
        }

        public CommandResult SetColorCommand(object argument)
        {
            _table.SetColor(_commandField, _destinationField, Cases, argument?.ToString());
            var steps = StepFactory.CreateLayerOperationSetColor(_table.TableName, _commandField, _destinationField, _selected, ColorConvertor.ConvertStringToColor(argument?.ToString()), !string.IsNullOrEmpty(argument?.ToString()) ? ColorMode.SetForValue : ColorMode.ResetFromValues);
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult CombineLayersCommand(object argument)
        {
            _table.SetFieldValue(_destinationField, Cases, argument);
            foreach (var s in _selected)_table.ChangeFilterValues(_destinationField, s, argument);
            var steps = StepFactory.CreateLayerOperationCombineLayers(_table.TableName, _commandField, _destinationField, _selected, argument);
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult SetColorForRowCommand(object argument)
        {
            _table.SetColorForRow(Cases, argument?.ToString());
            var steps = StepFactory.CreateLayerOperationSetColor(_table.TableName, _commandField, _commandField, _selected, ColorConvertor.ConvertStringToColor(argument?.ToString()), !string.IsNullOrEmpty(argument?.ToString()) ? ColorMode.SetForCase : ColorMode.ResetFromCases);
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult SetLabelCommand(object argument)
        {
            _table.SetLabel(_commandField, _destinationField, Cases, argument?.ToString());
            var steps = StepFactory.CreateLayerOperationSetLabel(_table.TableName, _commandField, _destinationField, _selected, argument?.ToString());
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> {steps}};
        }

        public CommandResult OutOfRangeCommand(object argument)
        {
            _table.RemoveOutOfRanges(_commandField);
            var steps = StepFactory.CreateNormFilter(_table.TableName, false, false, false, false, _commandField);
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }

        public CommandResult OutliersCommand(object argument)
        {
            _table.RemoveOutliers(_commandField);
            var steps = StepFactory.CreateOutliers(_table.TableName, new List<string> {_commandField}, "3sigma", true, true, 0, 3, false, "Выбросы", new List<string>());
            return new CommandResult { TablesCreated = false, NewSteps = new List<Step> { steps } };
        }
        
        public CommandResult SplitLayersCommand(object argument)
        {
            var t = _table.SplitByLayers(_commandField, (List<object>)argument);
            var prefix = _commandField+": ";
            var steps = StepFactory.CreateSplitTable(_table.TableName, _commandField, prefix, ((List<object>) argument).Select(a=> a.ToString() ?? "##Нет значения").ToList(), false,false);
            return new CommandResult { NewTables = t, TablesCreated = true, NewSteps = new List<Step> {steps} };
        }

        public CommandResult NoneCommand(object argument)
        {
            return new CommandResult { TablesCreated = false };
        }
    }

    public delegate CommandResult CommandProcessorDelegate(object argument);

    public class CommandResult
    {
        public List<SLDataTable> NewTables;
        public List<Step> NewSteps;
        public bool TablesCreated;
    }
}
