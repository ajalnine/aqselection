﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using AQChartViewLibrary.UI;
using AQChartLibrary;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private BackgroundWorker _marker;
        private List<MarkedItem> _fieldsToCheckX = new List<MarkedItem>();
        private List<MarkedItem> _fieldsToCheckY = new List<MarkedItem>();
        private string _xField = string.Empty;
        private List<string> _yFields = new List<string>();

        private void ClearMarks()
        {
            _marker?.CancelAsync();
            foreach (MarkedItem x in MainFieldsSelector.GetXItems())x.Mark = Colors.Black;
            foreach (MarkedItem y in MainFieldsSelector.GetYItems())y.Mark = Colors.Black;
        }

        private void MarkCorrelatedFields()
        {
            _marker = new BackgroundWorker {WorkerSupportsCancellation = true};
            _marker.DoWork += MarkerCorrelationDoWork;
            RunMarker();
        }

        private void MarkNormalFields()
        {
            _marker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _marker.DoWork += MarkerNormalDoWork;
            RunMarker();
        }

        private void RunMarker()
        {
            if (_selectedTable.Table.Count < 5000)
            {
                _fieldsToCheckX = MainFieldsSelector.GetXItems();
                _fieldsToCheckY = MainFieldsSelector.GetYItems();
                _xField = MainFieldsSelector.XField;
                _yFields = MainFieldsSelector.YFields;
                _marker.RunWorkerAsync();
            }
            else
            {
                ClearMarks();
            }
        }

        private void MarkerCorrelationDoWork(object sender, DoWorkEventArgs e)
        {
            foreach (MarkedItem y in _fieldsToCheckY)
            {
                if (y.Field != "Толщина")
                {
                    if (!e.Cancel)
                    {
                        MarkedItem y1 = y;
                        MainFieldsSelector.Dispatcher.BeginInvoke(
                            () =>
                            {
                                y1.Mark = Color.FromArgb(255, (byte)(255 * CheckCorrelation(_selectedTable, _xField, y1.Field) * ((y1.Field != _xField) ? 1 : 0)), 0, 0);
                            });
                    }
                    else return;
                }
            }

            foreach (MarkedItem x in _fieldsToCheckX)
            {
                if (x.Field != "Толщина")
                {
                    if (!e.Cancel)
                    {
                        MarkedItem x1 = x;
                        var yField = _yFields.FirstOrDefault();
                        MainFieldsSelector.Dispatcher.BeginInvoke(
                            () =>
                            {
                                x1.Mark = Color.FromArgb(255, (byte)(255 * CheckCorrelation(_selectedTable, yField, x1.Field) * ((x1.Field != yField) ? 1 : 0)), 0, 0);
                            });
                    }
                    else return;
                }
            }
        }

        private void MarkerNormalDoWork(object sender, DoWorkEventArgs e)
        {
            foreach (MarkedItem y in _fieldsToCheckY)
            {
                if (y.Field != "Толщина")
                {
                    if (!e.Cancel)
                    {
                        MarkedItem y1 = y;
                        MainFieldsSelector.Dispatcher.BeginInvoke(
                            () =>
                            {
                                y1.Mark = GetNormalityMark(_selectedTable, y1.Field);
                            });
                    }
                    else return;
                }
            }
        }
        
        private double CheckCorrelation(SLDataTable sldt, string xfield, string yfield)
        {
            if (yfield == null || xfield == null) return 0.0d;
            int xindex = sldt.ColumnNames.IndexOf(xfield);
            int yindex = sldt.ColumnNames.IndexOf(yfield);
            double n = 0;
            double sumX = 0, sumY = 0, sumX2 = 0, sumXy = 0, sumY2 = 0;
            foreach (SLDataRow sldr in sldt.Table)
            {
                if (sldr.Row[xindex] == null || string.IsNullOrEmpty(sldr.Row[xindex].ToString()) ||
                    sldr.Row[yindex] == null || string.IsNullOrEmpty(sldr.Row[yindex].ToString())) continue;
                var x = (double) sldr.Row[xindex];
                var y = (double) sldr.Row[yindex];
                sumX += x;
                sumY += y;
                sumX2 += x*x;
                sumXy += x*y;
                sumY2 += y*y;
                n++;
            }
            double r = (n*sumXy - sumX*sumY)/(Math.Sqrt((n*sumX2 - sumX*sumX)*(n*sumY2 - sumY*sumY)));
            double? t = r*Math.Sqrt(n - 2)/Math.Sqrt(1 - r*r);
            double? p = (t <= 0) ? 2 * AQMath.StudentCD(t, n - 2) : 2 * (1 - AQMath.StudentCD(t, n - 2));
            return Math.Abs(r) >= 0.4 && p.HasValue && p <= 0.05 ? 1 : Math.Abs(r) * 1.5d;
        }

        private Color GetNormalityMark(SLDataTable sldt, string yfield)
        {
            var data = sldt.GetColumnData(yfield);
            var mean = AQMath.AggregateMean(data);
            var stdev = AQMath.AggregateStDev(data);
            bool isNormal = false, isHomogenic = false;
            if (mean.HasValue && stdev.HasValue && mean > 0)
            {
                var kv = Math.Round( stdev.Value / mean.Value, 4);
                isHomogenic = kv <= 0.33;
            }
            if (data.Count < 1000)
            {
                var pSw = AQMath.ShapiroWilkP(data);
                if (pSw.HasValue && !double.IsNaN(pSw.Value))isNormal = pSw >= 0.05;
            }
            else
            {
                var pks = AQMath.KolmogorovP(data);
                if (!double.IsNaN(pks)) isNormal = pks >= 0.05 ;
            }
            if (isNormal && isHomogenic) return Color.FromArgb(0xff, 0x00, 0xa0, 0x00);
            if (!isNormal && !isHomogenic) return Color.FromArgb(0xff, 0xc0, 0x00, 0x00);
            return Colors.Black;
        }
    }
}