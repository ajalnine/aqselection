﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartViewLibrary.UI;
using AQBasicControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private ReportChartProbabilityPlotParameters GetReportChartProbabilityPlotParameters()
        {
            return new ReportChartProbabilityPlotParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
            };
        }

        private ReportStatisticsParameters GetReportStatisticsParameters()
        {
            return new ReportStatisticsParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                ColorField = MainFieldsSelector.ZField,
                Statistics = MainStatisticsSelector.GetStatistics(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayerMode = MainStatisticsSelector.GetCurrentLayerMode(),
                FixedAxisesDescription = AnalysisFixedAxises,
            };
        }

        private ReportChartMultipleRegressionParameters GetReportChartIsMultipleRegressionParameters()
        {
            return new ReportChartMultipleRegressionParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                FieldX = MainFieldsSelector.XField,
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                SmoothStyle = MainRegressionSelector.GetCurrentSmoothStyle(),
                LayersMode = MainRegressionSelector.GetCurrentLayerMode(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                ModelQuality = MainRegressionSelector.GetCurrentModelQuality(),
                StepwiseType = MainRegressionSelector.GetCurrentStepwiseType(),
                RegressionOptions = MainRegressionSelector.GetCurrentRegressionOptions(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorField = MainFieldsSelector.ZField,
                MarkOutage = MainRangeModeSelector.GetMarkOutage(),
                MarkerSizeField = MainFieldsSelector.WField,
                LabelField = MainFieldsSelector.VField,
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FixedAxisesDescription =  AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                ViewType = MainRegressionSelector.GetCurrentRegression(),
                Tools = MainToolsSelector.GetCurrentTools()
            };
        }

        private ReportChartScatterPlotParameters GetReportChartScatterPlotParameters()
        {
            return new ReportChartScatterPlotParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                FieldX = MainFieldsSelector.XField,
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                LWSArea = MainSmoothnessSelector.GetCurrentLWSArea(),
                SmoothStyle = MainSmoothnessSelector.GetCurrentSmoothStyle(),
                LayersMode = MainSmoothnessSelector.GetCurrentLayerMode(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorField = MainFieldsSelector.ZField,
                MarkOutage = MainRangeModeSelector.GetMarkOutage(),
                MarkerSizeField = MainFieldsSelector.WField,
                LabelField = MainFieldsSelector.VField,
                Tools = MainToolsSelector.GetCurrentTools(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
            };
        }

        private ReportChartGroupsParameters GetChartIsGroupCardParameters()
        {
            return new ReportChartGroupsParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                GroupField = MainFieldsSelector.XField,
                ColorField = MainFieldsSelector.ZField,
                Mode = MainGroupParametersSelector.GetCurrentGroupMode(),
                ShowOutliers = MainGroupParametersSelector.GetCurrentOutliers(),
                PointMode = MainGroupParametersSelector.GetCurrentPointMode(),
                AnalisysMode = MainGroupParametersSelector.GetCurrentGroupAnalisysMode(),
                OnlySignificant = false,
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                Sorted = MainGroupParametersSelector.GetCurrentSorted(),
                ShowText = MainGroupParametersSelector.GetCurrentShowText(),
                MarkOutage = MainRangeModeSelector.GetMarkOutage(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                Center = MainGroupParametersSelector.GetCurrentGroupCenter(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
            };
        }

        private ReportChartHistogramParameters GerChartIsFreqParameters(Aggregate aggregate)
        {
            return new ReportChartHistogramParameters
            {
                Table = _selectedTable.TableName,
                Fields = new List<string> { MainFieldsSelector.YFields.FirstOrDefault(), MainFieldsSelector.XField, MainFieldsSelector.VField },
                YMode = MainFrequencyParameterSelector.GetCurrentYMode(),
                AlignMode = MainFrequencyParameterSelector.GetCurrentAlignMode(),
                DetailLevel = MainFrequencyParameterSelector.GetCurrentDetailLevel(),
                Step = MainFrequencyParameterSelector.GetCurrentStep(),
                ColorField = MainFieldsSelector.ZField,
                ColumnType = MainColumnsParameterSelector.GetCurrentColumnDiagramType(),
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                UserAlignValue = MainFrequencyParameterSelector.GetCurrentUserAlignValue(),
                FixedAxisesDescription = AnalysisFixedAxises,
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                FitMode = MainFitSelector.GetCurrentFit(),
                FitStyle = MainFitSelector.GetCurrentFitStyle(),
                Tools = MainToolsSelector.GetCurrentTools(),
                LabelsType = MainHistogramLabelsSelector.GetCurrentShowText(),
                HistogramType = MainHistogramTypeSelector.GetCurrentHistogramType(),
                Aggregate = aggregate,
                AggregateDescription = AggregateSelector.GetDescription(aggregate),
                DrawParts = MainColumnsParameterSelector.GetCurrentDrawParts(),
                ColumnsOrder = MainColumnsParameterSelector.GetCurrentColumnOrder(),
                ColumnsType = MainColumnsParameterSelector.GetCurrentColumnDiagramType(),
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                BinMode = MainFrequencyParameterSelector.GetCurrentBinMode(),
                GroupNumber = MainParetoParameterSelector.GetCurrentGroupNumber()
            };
        }

        private ReportChartCirclesParameters GetChartIsCirclesParameters()
        {
            return new ReportChartCirclesParameters
            {
                Table = _selectedTable.TableName,
                Fields = new List<string> { MainFieldsSelector.YFields.FirstOrDefault(), MainFieldsSelector.XField, MainFieldsSelector.VField },
                ColorField = MainFieldsSelector.ZField,
                CirclesType = MainCircleParametersSelector.GetCurrentCircleDiagramType(),
                LabelsType = MainCircleParametersSelector.GetCurrentLabelsType(),
                ArrangementMode = MainCircleParametersSelector.GetCurrentArrangement(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                FixedAxisesDescription = AnalysisFixedAxises,
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                HistogramType = MainHistogramTypeSelector.GetCurrentHistogramType(),
                Aggregate = MainAggregateSelector.GetCurrentAggregate(),
                AggregateDescription = AggregateSelector.GetDescription(MainAggregateSelector.GetCurrentAggregate()),
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
            };
        }

        private ReportChartCardParameters GetReportChartCardIsGenericParameters(ReportChartCardType cardType)
        {
            return new ReportChartCardParameters
            {
                CardType = cardType,
                GroupField = MainFieldsSelector.ZField,
                DateField = MainFieldsSelector.VField,
                Series = MainSeriesCheckSelector.GetCurrentChartSeriesType(),
                Table = _selectedTable.TableName,
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                LWSArea = MainSmoothnessSelector.GetCurrentLWSArea(),
                Fields = MainFieldsSelector.YFields,
                Tools = MainToolsSelector.GetCurrentTools(),
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
            };
        }

        private ReportChartCardParameters GetReportChartCardIsGraphicsParameters(ReportChartCardType cardType)
        {
            return new ReportChartCardParameters
            {
                CardType = cardType,
                GroupField = MainFieldsSelector.XField,
                LayerField = MainFieldsSelector.IsMultiSelection() && MainFieldsSelector.YFields.Count>1  ? null : MainFieldsSelector.ZField,
                DateField = MainFieldsSelector.VField,
                Series = ReportChartSeriesType.None,
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                Tools = MainToolsSelector.GetCurrentTools(),
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                LWSArea = MainSmoothnessSelector.GetCurrentLWSArea(),
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                SmoothStyle = MainSmoothnessSelector.GetCurrentSmoothStyle(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
            };
        }

        private ReportChartCardParameters GetReportChartCardIsTrendParameters(ReportChartCardType cardType)
        {
            return new ReportChartCardParameters
            {
                CardType = cardType,
                GroupField = MainFieldsSelector.ZField,
                DateField = MainFieldsSelector.VField,
                Series = ReportChartSeriesType.None,
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.IsMultiSelection()
                                                    ? MainFieldsSelector.YFields
                                                    : new List<string> { MainFieldsSelector.YFields.FirstOrDefault(), MainFieldsSelector.XField },
                Tools = MainToolsSelector.GetCurrentTools(),
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                SmoothStyle = MainSmoothnessSelector.GetCurrentSmoothStyle(),
                LWSArea = MainSmoothnessSelector.GetCurrentLWSArea(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
            };
        }

        private ReportChartDynamicParameters GetReportChartDynamicParameters()
        {
            return new ReportChartDynamicParameters
            {
                Table = _selectedTable.TableName,
                Fields = MainFieldsSelector.YFields,
                DateField = MainFieldsSelector.XField,
                RangeType = MainRangeModeSelector.GetCurrentRangeType(),
                RangeStyle = MainRangeModeSelector.GetCurrentRangeStyle(),
                PeriodStyle = MainDateRangeSelector.GetCurrentPeriodStyle(),
                SmoothMode = MainSmoothnessSelector.GetCurrentSmoothness(),
                LWSArea = MainSmoothnessSelector.GetCurrentLWSArea(),
                SmoothStyle = MainSmoothnessSelector.GetCurrentSmoothStyle(),
                LayersMode = MainSmoothnessSelector.GetCurrentLayerMode(),
                UserLimits = MainRangeModeSelector.GetUserLimits(),
                UserPeriods = MainDateRangeSelector.GetPeriods(),
                ColorField = MainFieldsSelector.ZField,
                Tools = MainToolsSelector.GetCurrentTools(),
                MarkerSizeField = MainFieldsSelector.WField,
                LabelField = MainFieldsSelector.VField,
                MarkOutage = MainRangeModeSelector.GetMarkOutage(),
                ColorScale = MainMarkerSelector.GetCurrentColorScale(),
                ColorScaleMode = MainMarkerSelector.GetCurrentColorScaleMode(),
                MarkerMode = MainMarkerSelector.GetCurrentMarkerMode(),
                MarkerSizeMode = MainMarkerSelector.GetCurrentMarkerSizeMode(),
                MarkerShapeMode = MainMarkerSelector.GetCurrentMarkerShapeMode(),
                LayoutMode = MainLayoutModeSelector.GetCurrentLayoutMode(),
                LimitedLayers = MainLayoutModeSelector.GetCurrentLimitedLayers(),
                ChartLegend = MainLayoutModeSelector.GetCurrentChartLegend(),
                FixedAxisesDescription = AnalysisFixedAxises,
                FontSize = MainLayoutModeSelector.GetCurrentFontSize(),
                LayoutSize = MainLayoutModeSelector.GetCurrentSize(),
            };
        }
    }
}