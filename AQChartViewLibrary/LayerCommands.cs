﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using AQChartViewLibrary.UI;
using AQReportingLibrary;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;
using AQStepFactoryLibrary;

namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        private LayersOperationSelector _los;
        private string _field;
        private void MainFieldsSelector_OnLayerCommand(object sender, LayerCommandEventArgs e)
        {
            if (e.Field == "Нет") return;
            _field = e.Field;
            _los = new LayersOperationSelector();
            SetCommandAvailability();
            _los.InteractiveCommand += Los_InteractiveCommand;
            _los.Background = (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.Inverted) > 0
                ? new SolidColorBrush(Colors.Black)
                : new SolidColorBrush(Colors.White);
            CustomWindow.Child = _los;
            CustomWindow.Visibility = Visibility.Visible;
            CustomWindow.IsOpen = true;
            CustomWindow.VerticalOffset = e.ClickPoint.Y;
            CustomWindow.HorizontalOffset = -40;
            var layerList = _selectedTable.GetLayerList(e.Field);
            var z = layerList.OrderBy(a=>a?.ToString()??string.Empty).ToList();
            _los.SetLayerSelector(z, _selectedTable.GetDataType(e.Field), _selectedTable.GetDataType(MainFieldsSelector.YFields.FirstOrDefault()));
        }

        private void SetCommandAvailability()
        {
            if (!EnableInteractivity)
            {
                var availableCommands = InteractiveOperations.DeleteColumn | InteractiveOperations.SplitLayers |
                    InteractiveOperations.CopyToNewTable | InteractiveOperations.CutInNewTable |
                    InteractiveOperations.DeleteCase | InteractiveOperations.DeleteValue |
                    InteractiveOperations.SetColor | InteractiveOperations.SetColorForRow |
                    InteractiveOperations.SetLabel | InteractiveOperations.CombineLayers;
                if (_selectedTable.GetDataType(_field).Split('.').LastOrDefault()?.ToLower() == "double")
                {
                    availableCommands |= InteractiveOperations.Outliers;
                    if (_selectedTable.ColumnNames.Contains("#"+_field+"_НГ") || _selectedTable.ColumnNames.Contains("#" + _field + "_ВГ")) availableCommands |= InteractiveOperations.OutOfRange;
                }
                _los.SetCommandAvailability(availableCommands) ;
                return;
            }

            if (!_selectedTable.IsFiltered)
            {
                var availableCommands = InteractiveOperations.DeleteColumn | InteractiveOperations.SplitLayers |
                                        InteractiveOperations.CopyToNewTable | InteractiveOperations.CutInNewTable |
                                        InteractiveOperations.DeleteCase | InteractiveOperations.DeleteValue |
                                        InteractiveOperations.SetColor | InteractiveOperations.SetColorForRow |
                                        InteractiveOperations.SetLabel | InteractiveOperations.CombineLayers | InteractiveOperations.SetFilter;
                if (_selectedTable.GetDataType(_field).Split('.').LastOrDefault()?.ToLower() == "double")
                {
                    availableCommands |= InteractiveOperations.Outliers;
                    if (_selectedTable.ColumnNames.Contains("#" + _field + "_НГ") || _selectedTable.ColumnNames.Contains("#" + _field + "_ВГ")) availableCommands |= InteractiveOperations.OutOfRange;
                }
                _los.SetCommandAvailability(availableCommands);
                return;
            }

            if (_selectedTable.IsFiltered && _selectedTable.Filters != null &&
                _selectedTable.Filters.Any(a => a.ColumnName == _field))
            {
                var availableCommands = InteractiveOperations.DeleteColumn | InteractiveOperations.SplitLayers |
                                        InteractiveOperations.CopyToNewTable | InteractiveOperations.CutInNewTable |
                                        InteractiveOperations.DeleteCase | InteractiveOperations.DeleteValue |
                                        InteractiveOperations.SetColor | InteractiveOperations.SetColorForRow |
                                        InteractiveOperations.SetLabel | InteractiveOperations.CombineLayers | InteractiveOperations.DeleteFilter | InteractiveOperations.RemoveFilters | InteractiveOperations.SetFilter;
                if (_selectedTable.GetDataType(_field).Split('.').LastOrDefault()?.ToLower() == "double")
                {
                    availableCommands |= InteractiveOperations.Outliers;
                    if (_selectedTable.ColumnNames.Contains("#" + _field + "_НГ") || _selectedTable.ColumnNames.Contains("#" + _field + "_ВГ")) availableCommands |= InteractiveOperations.OutOfRange;
                }
                _los.SetCommandAvailability(availableCommands);

            }
            else
            {
                var availableCommands = InteractiveOperations.DeleteColumn | InteractiveOperations.SplitLayers |
                                        InteractiveOperations.CopyToNewTable | InteractiveOperations.CutInNewTable |
                                        InteractiveOperations.DeleteCase | InteractiveOperations.DeleteValue |
                                        InteractiveOperations.SetColor | InteractiveOperations.SetColorForRow |
                                        InteractiveOperations.SetLabel | InteractiveOperations.CombineLayers | InteractiveOperations.RemoveFilters | InteractiveOperations.SetFilter;
                if (_selectedTable.GetDataType(_field).Split('.').LastOrDefault()?.ToLower() == "double")
                {
                    availableCommands |= InteractiveOperations.Outliers;
                    if (_selectedTable.ColumnNames.Contains("#" + _field + "_НГ") || _selectedTable.ColumnNames.Contains("#" + _field + "_ВГ")) availableCommands |= InteractiveOperations.OutOfRange;
                }
                _los.SetCommandAvailability(availableCommands);
            }
        }

        private void Los_InteractiveCommand(object sender, InteractiveCommandEventArgs e)
        {
            CustomWindow.Child = null;
            CustomWindow.IsOpen = false;
            if (e.Command == InteractiveOperations.None) return;

            var selected = new List<int>();
            var z = _los.GetSelectedLayers();
            foreach (var layer in z)selected.AddRange(_selectedTable.GetRowsForAllValuesFromLayer(_field, layer));    
            
            var name = _field + ": " + AQMath.Concatenate(z, ";");
            var argument = e.Command == InteractiveOperations.SplitLayers ? (object)z : e.Argument;

            _processors = new List<CommandProcessor> { new CommandProcessor(_selectedTable, _field, e.yField? MainFieldsSelector.YFields.FirstOrDefault() : _field, selected, name, z) };

            Data.LDRCache.Clear();
            foreach (var p in _processors)
            {
                var cr = p.ExecuteComand(e.Command, argument);
                if (cr.TablesCreated)
                {
                    MessageBoxResult mbr = MessageBoxResult.OK;

                    if (cr.NewTables.Count > 10)
                    {
                        mbr = MessageBox.Show($"Будет создано {cr.NewTables.Count} новых таблиц. Продолжить?", "Внимание",
                            MessageBoxButton.OKCancel);
                    }

                    if (mbr == MessageBoxResult.OK)
                    {
                        SafeAppendNewData(cr.NewTables, cr.NewSteps);
                        InteractiveCommandProcessed?.Invoke(this, new InteractiveCommandProcessedEventArgs { Argument = e.Argument, Command = e.Command, Result = cr });
                    }
                }
                else
                {
                    if (cr.NewSteps!=null && cr.NewSteps.Count>0) SafeAppendNewData(null, cr.NewSteps);
                    InteractiveCommandProcessed?.Invoke(this, new InteractiveCommandProcessedEventArgs { Argument = e.Argument, Command = e.Command, Result = cr });
                }
            }
            if ( new[]{ InteractiveOperations.DeleteColumn, InteractiveOperations.DeleteFilter, InteractiveOperations.RemoveFilters, InteractiveOperations.SetFilter,  }.Contains(e.Command)) SetupFieldSelector();
            MakeReportChart(false);
        }

        private void SafeAppendNewData(List<SLDataTable> newTables, IEnumerable<Step> newCalculation)
        {
            if (newCalculation!=null)_innerSteps.InsertRange(_innerSteps.Count, newCalculation);
            if (newTables == null) return;
            var intersection = _dataTables.Select(a => a.TableName).Intersect(newTables.Select(a => a.TableName)).ToList();
            if (!intersection.Any())
            {
                _dataTables.InsertRange(_dataTables.Count, newTables);
            }
            else
            {
                foreach (var doubleTable in intersection)
                {
                    var index = _dataTables.IndexOf(_dataTables.Single(a => a.TableName == doubleTable));
                    if (index >= 0)
                    {
                        _dataTables.RemoveAt(index);
                        _dataTables.Insert(index, newTables.Single(a=>a.TableName==doubleTable));
                    }
                }
            }
        }
    }
}
