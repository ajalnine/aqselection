﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ApplicationCore.ReportingServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        public void DoAnalysis(List<SLDataTable> dataTables, SLDataTable selectedTable, List<Step> innerSteps, Step operation, DateTime? from, DateTime? to, string desc, bool preselect = true)
        {
            _dataTables = dataTables;
            _currentChartStep = operation;
            _from = from;
            _to = to;
            _innerSteps = innerSteps;
            _noExternalSteps = innerSteps == null;
            _innerTables = null;
            if (_innerSteps == null) _innerSteps = new List<Step>();
            Description = desc;

            if (_dataTables == null || _dataTables.Count==0)
            {
                ChartPanel.Children.Clear();
                SetNoDataMessage();
                return;
            }
            if (_currentChartStep != null && preselect)
            {
                ResetPreselection();
                SetupToolsAvailability();
                var table = SetUIbyStep(operation);
                _selectedTable = table ?? selectedTable ?? (dataTables.Count > 1 ? dataTables[1] : dataTables[0]);
                _isSignalProcessingFirstTime = true;
                _currentChartStep = null;
            }
            else
            {
                _selectedTable = selectedTable ?? (dataTables.Count > 1 ? dataTables[1] : dataTables[0]);
            }
            SelectedTableChanged?.Invoke(this, new SelectedTableChangedEventArgs { SelectedTable = _selectedTable, AllTables = dataTables });
            if (!_to.HasValue) _to = _selectedTable.GetMaximumDate();
            if(!_from.HasValue) _from = _selectedTable.GetMinimumDate();
            HideInteractivityCommandSelector();
            ClosePopups();
            MainFieldsSelector.SetupInputs(_selectedTable);
            CheckChartPossibility();
            _hexRecommended = false;
            if (StepTableToSelect != null)
            {
                MainFieldsSelector.SelectAll(StepXFieldToSelect, StepYFieldsToSelect, StepZFieldToSelect, StepWFieldToSelect, StepVFieldToSelect);
            }

            RefreshParametersUI();
            SetupMultipleSelection();
            SetupFieldSelector();
            RefreshChartPanelVisibility();
            SetupToolsAvailability();
            MakeReportChart(true);
        }

        private void ResetPreselection()
        {
            StepTableToSelect = null;
            StepYFieldsToSelect = new List<string>();
            StepXFieldToSelect = null;
            StepZFieldToSelect = null;
            StepWFieldToSelect = null;
            StepVFieldToSelect = null;
        }

        public void Refresh()
        {
            MakeReportChart(false);
        }

        public void Reset()
        {
            ChartPanel.Children.Clear();
            if (AnalysisFixedAxises != null) AnalysisFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            ResetPreselection();
            _selectedTable = null;
            _dataTables = null;
            _innerTables = null;
            _innerSteps = null;
            _currentChartStep = null;
            _currentChartDescription = string.Empty;
            _currentShortChartDescription = string.Empty;
        }

        public SLDataTable GetSelectedTable()
        {
            return _selectedTable;
        }

        public AnalysisResult GetAnalisysResult()
        {
            var wb = new WriteableBitmap(ChartPanel,
                (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.HD) == ReportChartLayoutMode.HD
                ? new ScaleTransform { ScaleX = 4, ScaleY = 4 }
                : new ScaleTransform { ScaleX = 2, ScaleY = 2 });
            var iconLR = new WriteableBitmap(ChartPanel,
                (MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.HD) == ReportChartLayoutMode.HD
                ? new ScaleTransform { ScaleX = 0.5, ScaleY = 0.5 }
                : new ScaleTransform { ScaleX = 0.25, ScaleY = 0.25 }); 
            var icon = new WriteableBitmap(ChartPanel, new ScaleTransform { ScaleX = 1, ScaleY = 1 });

            var res = new AnalysisResult
            {
                InnerSteps = _innerSteps,
                AvailableData = _dataTables,
                UsedTable = _selectedTable,
                AnalysisOperation = _currentChartStep,
                OnlyInnerSteps = _noExternalSteps,
                UsedColorScale =
                    ((MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.UseColorScale) ==
                     ReportChartLayoutMode.UseColorScale)
                        ? MainMarkerSelector.GetCurrentColorScale()
                        : ColorScales.HSV,
                IsInverted = ((MainLayoutModeSelector.GetCurrentLayoutMode() & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted),
                SED = new SlideElementDescription
                {
                    BreakSlide = true,
                    Description = _currentChartDescription,
                    ShortDescription = _currentShortChartDescription,
                    SlideElementType = SlideElementTypes.Image,
                    Width = wb.PixelWidth,
                    Height = wb.PixelHeight
                },
                ThumbNail = wb,
                Icon = icon,
                IconLR = iconLR,
                From = _from,
                AnalysisGuid = _operationGuid,
                To = _to, 
                InnerTables = _innerTables,
                UsedFields = MainFieldsSelector.GetUsedFields()
            };
            return res;
        }
    }

    
    public class AnalysisReadyEventArgs : EventArgs
    {
        public AnalysisResult Result;
    }

    public class AnalysisStartedEventArgs : EventArgs
    {
    }

    public delegate void AnalysisFinishedDelegate(object o, AnalysisReadyEventArgs e);
    public delegate void AnalysisStartedDelegate(object o, AnalysisStartedEventArgs e);
}