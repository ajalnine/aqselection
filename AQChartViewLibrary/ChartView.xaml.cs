﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQChartLibrary;
using AQControlsLibrary;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        #region Поля и свойства компонента

        public delegate void DataProcessDelegate(object sender, TaskStateEventArgs e);

        public static readonly DependencyProperty DisposeDataAfterClosingProperty =
            DependencyProperty.Register("DisposeDataAfterClosing", typeof(bool), typeof(ChartView),
                                        new PropertyMetadata(true));
        public bool DisposeDataAfterClosing
        {
            get { return (bool)GetValue(DisposeDataAfterClosingProperty); }
            set { SetValue(DisposeDataAfterClosingProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(ChartView),
                                new PropertyMetadata(String.Empty));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            private set { SetValue(DescriptionProperty, value); }
        }



        public bool EnableInteractivity
        {
            get { return (bool)GetValue(EnableInteractivityProperty); }
            set { SetValue(EnableInteractivityProperty, value); }
        }

        public static readonly DependencyProperty EnableInteractivityProperty =
            DependencyProperty.Register("EnableInteractivity", typeof(bool), typeof(ChartView), new PropertyMetadata(true));


        private Step _currentChartStep;
        private List<SLDataTable> _dataTables;
        private List<SLDataTable> _innerTables;
        private SLDataTable _selectedTable;
        private bool _hexRecommended;
        private DateTime? _from;
        private DateTime? _to;
        private bool _isSignalProcessingFirstTime;
        private List<Step> _innerSteps;
        private bool _noExternalSteps;

        private readonly AQService _aqs;
        public AQModuleDescription ParentAqmd { get; set; }

        private readonly CalculationService _cas;
        private Dictionary<ReportChartType, FixedAxises> _fixedAxisesCache;
        private Point _mousePosition;
        private string _currentChartDescription = string.Empty;
        private string _currentShortChartDescription = string.Empty;
        private IReportElement _reportChart;
        private Guid _operationGuid;

        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event InteractiveCommandProcessedDelegate InteractiveCommandProcessed;
        public event SelectedTableChangedDelegate SelectedTableChanged;
        public event AnalysisFinishedDelegate AnalysisFinished;
        public event AnalysisStartedDelegate AnalysisStarted;

        #endregion

        public ChartView()
        {
            InitializeComponent();
            InitFixedAxisesCache();
            MouseMove += Window_MouseMove;
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (_cas == null) _cas = Services.GetCalculationService();
            if (_aqs == null) _aqs = Services.GetAQService();
        }
        
        void Window_MouseMove(object sender, MouseEventArgs e)
        {
            _mousePosition = e.GetPosition(this);
        }

        private void InitFixedAxisesCache()
        {
            _fixedAxisesCache = new Dictionary<ReportChartType, FixedAxises>
            {
                {ReportChartType.Card, new FixedAxises()},
                {ReportChartType.Statistics, new FixedAxises()},
                {ReportChartType.Dynamic, new FixedAxises()},
                {ReportChartType.Histogram, new FixedAxises()},
                {ReportChartType.Groups, new FixedAxises()},
                {ReportChartType.ProbabilityPlot, new FixedAxises()},
                {ReportChartType.ScatterPlot, new FixedAxises()}
            };
        }

        public void ClosePopups()
        {
            CustomWindow.IsOpen = false;
            MainHistoryNavigation.CloseHistory();
        }

        private void SetDescriptions(IReportElement irc)
        {
            _currentShortChartDescription = Description;
            _currentChartDescription = irc.GetChartDescription();
            AppendPeriodToDescription();
        }

        private void SetNoDataMessage()
        {
            ChartPanel.Children.Clear();

            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 50,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = "Нет данных",
                TextAlignment = TextAlignment.Center,
                Margin = new Thickness(0, 150, 0, 100)
            };
            ChartPanel.Children.Add(tb);
        }

        private void AppendPeriodToDescription()
        {
            if (!_from.HasValue || !_to.HasValue) return;
            var periodDescription = $"Период: {_from.Value.ToShortDateString()} - {_to.Value.ToShortDateString()}";
            _currentShortChartDescription += (string.IsNullOrEmpty(_currentShortChartDescription) ? string.Empty : "\r\n") + periodDescription;
            _currentChartDescription += (string.IsNullOrEmpty(_currentChartDescription) ? string.Empty : "\r\n") + periodDescription;
        }
        
        private void Panel_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // ClosePopups();
        }
    }
}