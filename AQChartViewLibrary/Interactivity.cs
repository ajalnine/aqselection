﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using AQChartViewLibrary.UI;
using AQReportingLibrary;
using AQMathClasses;

namespace AQChartViewLibrary
{
    public partial class ChartView
    {
        List<CommandProcessor> _processors = new List<CommandProcessor>();
        private bool keySubscripted;
        private void ReportChartInteractivity(object o, InteractivityEventArgs e)
        {
            if (e.Clear) _processors.Clear();
            if (e.SelectedCases == null || e.SelectedCases.Count == 0 || e.AvailableOperations == InteractiveOperations.None || !EnableInteractivity)
            {
                HideInteractivityCommandSelector();
            }
            else
            {
                if (!keySubscripted)SubscribeHotkeys();
                keySubscripted = true;
                Root.Focus();
                _processors.Add(new CommandProcessor(_dataTables.Single(a => a.TableName == e.Table), e.Field, e.TargetField ?? MainFieldsSelector.YFields.FirstOrDefault(), e.SelectedCases, e.Message, e.SelectedValues));
                ShowInteractivityCommandSelector(e.AvailableOperations, e.Message, _processors.SelectMany(a=>a.Cases).Distinct().Count());
            }
        }

        private void HideInteractivityCommandSelector()
        {
            MainInteractivityCommandSelector.Visibility = Visibility.Collapsed;
        }

        private void ShowInteractivityCommandSelector(InteractiveOperations io, string message, int n)
        {
            MainInteractivityCommandSelector.Visibility = Visibility.Visible;
            if (message != null)
            {
                var m = message.Count() > 1000 ? message.Substring(0, 999) + "..." : message;
                MainInteractivityCommandSelector.SetMessage($"n={n}\r\n{m}", n);
            }
            MainInteractivityCommandSelector.EnableOperations(io);
        }

        public void InteractiveCommand_Click(object sender, InteractiveCommandEventArgs e)
        {
            Data.LDRCache.Clear();
            HideInteractivityCommandSelector();
            if ((new List<InteractiveOperations> { InteractiveOperations.CopyToNewTable, InteractiveOperations.CutInNewTable, InteractiveOperations.SplitLayers}).Contains(e.Command))
            {
                var commonProcessor = MergeProcessors();
                if (commonProcessor == null) return;
                var cr = commonProcessor.ExecuteComand(e.Command, e.Argument);
                if (cr.TablesCreated)
                {
                    SafeAppendNewData(cr.NewTables, cr.NewSteps);
                }
                InteractiveCommandProcessed?.Invoke(this, new InteractiveCommandProcessedEventArgs { Argument = e.Argument, Command = e.Command, Result = cr });
            }
            else
            {
                foreach (var p in _processors)
                {
                    var cr = p.ExecuteComand(e.Command, e.Argument);
                    
                    if (cr.TablesCreated)
                    {
                        SafeAppendNewData(cr.NewTables, cr.NewSteps);
                    }
                    
                    InteractiveCommandProcessed?.Invoke(this, new InteractiveCommandProcessedEventArgs { Argument = e.Argument, Command = e.Command, Result = cr });
                }
            }
            MakeReportChart(false);
        }

        private CommandProcessor MergeProcessors()
        {
            if (!_processors.Any()) return null;
            var common = _processors.First();
            var commonValues = new List<object>();
            var commonCases = new List<int>();
            foreach (var p in _processors)
            {
                if (p.GetCases () != null) commonCases.AddRange(p.GetCases());
                if (p.GetValues() !=null) commonValues.AddRange(p.GetValues());
            }
            var commonMessage = AQMath.ConcatenateDistinct(_processors.Select(a=>(object)a.GetMessage()?? string.Empty).ToList(), ";");
            var result = new CommandProcessor(common.GetTable(), common.GetField(), MainFieldsSelector.YFields.FirstOrDefault(), commonCases.Distinct().ToList(), commonMessage, commonValues);
            return result;
        }
        public bool _shiftPressed;

        private void SubscribeHotkeys()
        {
            Root.AddHandler(UIElement.KeyDownEvent, new KeyEventHandler((o, e) =>
            {
                switch (e.Key)
                {
                    case Key.Delete:
                        if (MainInteractivityCommandSelector.Visibility != Visibility.Collapsed)
                        {
                            if (_shiftPressed)MainInteractivityCommandSelector.EmitDeleteCaseCommand();
                            else MainInteractivityCommandSelector.EmitDeleteValueCommand();
                            e.Handled = true;
                        }
                        break;
                        case Key.Shift:
                            _shiftPressed = true;
                        break;
                }
            }), true);

            Root.AddHandler(UIElement.KeyUpEvent, new KeyEventHandler((o, e) =>
            {
                switch (e.Key)
                {
                   
                    case Key.Shift:
                        _shiftPressed = false;
                        break;
                }
            }), true);
        }
    }
    public delegate void InteractiveCommandProcessedDelegate(object o, InteractiveCommandProcessedEventArgs e);

    public class InteractiveCommandProcessedEventArgs
    {
        public InteractiveOperations Command;
        public object Argument;
        public CommandResult Result;
    }
}
