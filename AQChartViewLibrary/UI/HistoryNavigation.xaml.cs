﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using System.Collections.ObjectModel;

namespace AQChartViewLibrary.UI
{
    public partial class HistoryNavigation : UserControl
    {
        private ObservableCollection<Step> _history;
        private int _selectedIndex = -1;
        public event HistoryPositionChangedDelegate HistoryPositionChanged;
        private bool _processingEnabled;

        public HistoryNavigation()
        {
            InitializeComponent();
            _history = new ObservableCollection<Step>();
            HistoryView.DataContext = _history;
            UpdateButtonEnabled();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            _selectedIndex--;
            if (_selectedIndex < 0) _selectedIndex = 0;
            else
            {
                ShowHistoryPosition();
                if (_processingEnabled) HistoryPositionChanged?.Invoke(this, new HistoryPositionChangedEventArgs { SelectedStep = _history[HistoryView.SelectedIndex] });
            }
            UpdateButtonEnabled();
        }

        private void ForwardButton_Click(object sender, RoutedEventArgs e)
        {
            _selectedIndex++;
            if (_selectedIndex >= _history.Count) _selectedIndex = _history.Count - 1;
            else
            {
                ShowHistoryPosition();
                if (_processingEnabled) HistoryPositionChanged?.Invoke(this, new HistoryPositionChangedEventArgs { SelectedStep = _history[HistoryView.SelectedIndex] });
            }
            UpdateButtonEnabled();
        }

        public void CloseHistory()
        {
            OpenHistoryButton.IsChecked = false;
        }

        public void AppendItem(Step step)
        {
            while (_selectedIndex>=0 && _selectedIndex < _history.Count - 1)
            {
                _history.RemoveAt(_history.Count - 1);
            }
            _history.Add(step);
            _selectedIndex = _history.Count - 1;
            ShowHistoryPosition();
            UpdateButtonEnabled();
        }

        private void ShowHistoryPosition()
        {
            _processingEnabled = false;
            HistoryView.SelectedIndex = _selectedIndex;
            _processingEnabled = true;
        }

        private void UpdateButtonEnabled()
        {
            BackButton.IsEnabled = _selectedIndex != 0;
            ForwardButton.IsEnabled = _selectedIndex != _history.Count - 1;
            ClearHistory.IsEnabled = _history.Count > 1;
        }

        public void UpdateLastItem(Step step)
        {
            if (_selectedIndex < 0 || _selectedIndex>_history.Count-1) return;
            _processingEnabled = false;
            _history[_selectedIndex] = step;
            HistoryView.SelectedItem = step;
            _processingEnabled = true;
        }

        private void HistoryView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_processingEnabled) return;
            _selectedIndex = HistoryView.SelectedIndex;
            if (HistoryView.SelectedIndex < 0) return;
            UpdateButtonEnabled();
            HistoryPositionChanged?.Invoke(this, new HistoryPositionChangedEventArgs { SelectedStep = _history[HistoryView.SelectedIndex] });
        }

        private void ClearHistory_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedStep = (Step)HistoryView.SelectedItem;
            _processingEnabled = false;
            _history.Clear();
            _history.Add(selectedStep);
            HistoryView.SelectedItem = selectedStep;
            _processingEnabled = true;
            UpdateButtonEnabled();
        }

        private void Ok_OnClick(object sender, RoutedEventArgs e)
        {
            CloseHistory();
        }
    }

    public delegate void HistoryPositionChangedDelegate(object o, HistoryPositionChangedEventArgs e);
    public class HistoryPositionChangedEventArgs
    {
        public Step SelectedStep;
    }
}
