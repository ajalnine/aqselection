﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class CardTypeSelector
    {
        public delegate void CardTypeChangedDelegate(object sender, CardTypeChangedEventArgs e);

        private ReportChartCardType _currentCardType;

        private string _currentCardTypeDescription;

        public CardTypeSelector()
        {
            InitializeComponent();
            SetCurrentCardType(ReportChartCardType.CardTypeIsX);
        }

        public event CardTypeChangedDelegate CardTypeChanged;

        public ReportChartCardType GetCurrentCardType()
        {
            return _currentCardType;
        }

        public string GetCurrentCardTypeDescription()
        {
            return _currentCardTypeDescription;
        }

        public void SetCurrentCardType(ReportChartCardType rcct)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcct.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentCardType = rcct;
            if (rb == null) return;
            var textBlock = rb.Content as TextBlock;
            if (textBlock != null) _currentCardTypeDescription = textBlock.Text;
        }

        public void RefreshUI()
        {
            SetCurrentCardType(_currentCardType);
        }

        private void CardType_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentCardType = (ReportChartCardType) Enum.Parse(typeof (ReportChartCardType), rb.Name, true);
            var textBlock = rb.Content as TextBlock;
            if (textBlock != null) _currentCardTypeDescription = textBlock.Text;
            if (CardTypeChanged != null)
                CardTypeChanged.Invoke(this,
                                       new CardTypeChangedEventArgs
                                           {
                                               CardType = _currentCardType,
                                               CardDescription = _currentCardTypeDescription
                                           });
        }
    }

    public class CardTypeChangedEventArgs : EventArgs
    {
        public ReportChartCardType CardType { get; set; }
        public string CardDescription { get; set; }
    }
}