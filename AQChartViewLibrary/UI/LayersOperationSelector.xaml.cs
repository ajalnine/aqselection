﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class LayersOperationSelector
    {
        public delegate void InteractiveCommandDelegate(object sender, InteractiveCommandEventArgs e);
        private ObservableCollection<MarkedItem> _list = new ObservableCollection<MarkedItem>();
        private string _zType, _yType;

        public LayersOperationSelector()
        {
            InitializeComponent();
            DeleteCase.Text = "Удалить наблюдения";
            DeleteValue.Text = "Удалить значения";
            DeleteColumn.Text = "Удалить переменную";
            CutInNewTable.Text = "Вырезать в новую таблицу";
            CopyToNewTable.Text = "Копировать в новую таблицу";
            SplitLayers.Text = "Разделить на слои в новые таблицы";
            SetColor.Text = "Установить цвета маркеров";
            SetLabel.Text = "Установить метки маркеров";
            SetFilter.Text = "Установить фильтр";
            DeleteFilter.Text = "Удалить фильтр";
            Outliers.Text = "Удалить выбросы";
            CombineLayers.Text = "Замена нескольких значений одним";
            OutOfRange.Text = "Удалить значения вне норм";
        }

        public event InteractiveCommandDelegate InteractiveCommand;
        
        private void Command_Click(object sender, RoutedEventArgs e)
        {
            var b = sender as TextImageButtonBase;
            if (b == null) return;
            var command = (InteractiveOperations)Enum.Parse(typeof(InteractiveOperations), b.Name, true);

            CombineValue.Background = null;
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = command, yField = yField.IsChecked.Value});
        }

        public void SetLayerSelector(List<object> z, string zType, string yType)
        {
            _list = new ObservableCollection<MarkedItem>(z.Select(a=>new MarkedItem {Field = string.IsNullOrEmpty(a?.ToString()) ? " Нет значения" : a.ToString() , IsSelected = !string.IsNullOrEmpty(a?.ToString()), Source = a}));
            LayerSelector.ItemsSource = _list;
            _zType = zType;
            _yType = yType;
        }

        public void SetCommandAvailability(InteractiveOperations io)
        {
            DeleteFilter.IsEnabled = ((io & InteractiveOperations.DeleteFilter) == InteractiveOperations.DeleteFilter);
            SetFilter.IsEnabled = ((io & InteractiveOperations.SetFilter) == InteractiveOperations.SetFilter);
            SetColor.IsEnabled = ((io & InteractiveOperations.SetColor) == InteractiveOperations.SetColor);
            SetLabel.IsEnabled = ((io & InteractiveOperations.SetLabel) == InteractiveOperations.SetLabel);
            CopyToNewTable.IsEnabled = ((io & InteractiveOperations.CopyToNewTable) == InteractiveOperations.CopyToNewTable);
            CutInNewTable.IsEnabled = ((io & InteractiveOperations.CutInNewTable) == InteractiveOperations.CutInNewTable);
            DeleteColumn.IsEnabled = ((io & InteractiveOperations.DeleteColumn) == InteractiveOperations.DeleteColumn);
            DeleteCase.IsEnabled = ((io & InteractiveOperations.DeleteCase) == InteractiveOperations.DeleteCase);
            DeleteValue.IsEnabled = ((io & InteractiveOperations.DeleteValue) == InteractiveOperations.DeleteValue);
            SplitLayers.IsEnabled = ((io & InteractiveOperations.SplitLayers) == InteractiveOperations.SplitLayers);
            CombineLayers.IsEnabled = ((io & InteractiveOperations.CombineLayers) == InteractiveOperations.CombineLayers);
            Outliers.IsEnabled = ((io & InteractiveOperations.Outliers) == InteractiveOperations.Outliers);
            RemoveFilters.IsEnabled = ((io & InteractiveOperations.RemoveFilters) == InteractiveOperations.RemoveFilters);
            OutOfRange.IsEnabled = ((io & InteractiveOperations.OutOfRange) == InteractiveOperations.OutOfRange);
        }

        public List<object> GetSelectedLayers()
        {
            return _list.Where(a => a.IsSelected).Select(a => a.Source).ToList();
        }

        private void ClearSelection_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var z in _list)
            {
                z.IsSelected = false;
            }
        }

        private void SelectAll_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var z in _list)
            {
                z.IsSelected = true;
            }
        }

        private void NoCommand_OnClick(object sender, RoutedEventArgs e)
        {
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.None });
        }

        private void SetColor_Click(object sender, RoutedEventArgs e)
        {
            LabelOptionsPanel.Visibility = Visibility.Collapsed;
            ColorOptionsPanel.Visibility = Visibility.Visible;
            CombineLayersOptionsPanel.Visibility = Visibility.Collapsed;
        }

        private void SetColorOk_Click(object sender, RoutedEventArgs e)
        {
            if (SetNoColor.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColor, Argument = null, yField = yField.IsChecked.Value });
            else if (SetNoColorForRow.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColorForRow, Argument = null });
            else if (SetColorForValue.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColor, Argument = ColorConvertor.ConvertColorToString(ColorSelector.Color), yField = yField.IsChecked.Value });
            else if (SetColorForRow.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColorForRow, Argument = ColorConvertor.ConvertColorToString(ColorSelector.Color) });
        }

        private void SetLabel_Click(object sender, RoutedEventArgs e)
        {
            LabelOptionsPanel.Visibility = Visibility.Visible;
            ColorOptionsPanel.Visibility = Visibility.Collapsed;
            CombineLayersOptionsPanel.Visibility = Visibility.Collapsed;
        }

        private void SetMessageOk_Click(object sender, RoutedEventArgs e)
        {
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetLabel, Argument = string.IsNullOrWhiteSpace(LabelText.Text) ? null : LabelText.Text, yField = yField.IsChecked.Value });
        }

        private void CombineLayers_OnClick(object sender, RoutedEventArgs e)
        {
            LabelOptionsPanel.Visibility = Visibility.Collapsed;
            ColorOptionsPanel.Visibility = Visibility.Collapsed;
            CombineLayersOptionsPanel.Visibility = Visibility.Visible;
            CombineValue.Background = null;
        }

        private void SetLayersValueOk_OnClick(object sender, RoutedEventArgs e)
        {
            var type = yField.IsChecked.Value ? _yType : _zType;

            if (string.IsNullOrEmpty(CombineValue.Text))
            {
                InteractiveCommand?.Invoke(this,
                    new InteractiveCommandEventArgs {Command = InteractiveOperations.CombineLayers, Argument = null, yField = yField.IsChecked.Value });
                return;
            }

            switch (type.Split('.').LastOrDefault()?.ToLower())
            {
                case "double":
                    double v;
                    if (double.TryParse(CombineValue.Text, out v)) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.CombineLayers, Argument = (double?)(v), yField = yField.IsChecked.Value});
                    else
                    {
                        CombineValue.Background = new SolidColorBrush(Colors.Red);
                        CombineValue.Focus();
                    }
                    break;

                case "bool":
                    bool v4;
                    if (bool.TryParse(CombineValue.Text, out v4)) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.CombineLayers, Argument = (bool?)v4, yField = yField.IsChecked.Value });
                    else
                    {
                        CombineValue.Background = new SolidColorBrush(Colors.Red);
                        CombineValue.Focus();
                    }
                    break;

                case "datetime":
                    DateTime v2;
                    if (DateTime.TryParse(CombineValue.Text, out v2)) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.CombineLayers, Argument = (DateTime?)v2, yField = yField.IsChecked.Value });
                    else
                    {
                        CombineValue.Background = new SolidColorBrush(Colors.Red);
                        CombineValue.Focus();
                    }
                    break;

                case "timespan":
                    TimeSpan v3;
                    if (TimeSpan.TryParse(CombineValue.Text, out v3)) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.CombineLayers, Argument = (TimeSpan?)v3, yField = yField.IsChecked.Value });
                    else
                    {
                        CombineValue.Background = new SolidColorBrush(Colors.Red);
                        CombineValue.Focus();
                    }
                    break;

                default:
                    InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.CombineLayers, Argument = CombineValue.Text, yField = yField.IsChecked.Value });
                    break;
            }
        }
    }
}