﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQReportingLibrary;
using System.IO.IsolatedStorage;

namespace AQChartViewLibrary.UI
{
    public partial class ToolsSelector
    {
        public delegate void ToolsChangedDelegate(object sender, ToolsChangedEventArgs e);

        private ReportChartTools _currentTools, _availableTools;

        public ToolsSelector()
        {
            InitializeComponent();

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisToolsSettings"))
            {
                SetCurrentTools(ReportChartTools.ToolNone | ReportChartTools.ToolCutSurfaces & _availableTools);
            }
            else SetCurrentTools((ReportChartTools)IsolatedStorageSettings.ApplicationSettings["AnalysisToolsSettings"] & _availableTools);
        }

        public event ToolsChangedDelegate ToolsChanged;

        public ReportChartTools GetCurrentTools()
        {
            return _currentTools;
        }

        public ReportChartTools GetAvailability()
        {
            return _availableTools;
        }

        public void SetCurrentTools(ReportChartTools allrct)
        {
            var rct = allrct & _availableTools;
            ToolRug.IsChecked = (rct & ReportChartTools.ToolRug) > 0;
            ToolKDE.IsChecked = (rct & ReportChartTools.ToolKDE) > 0;
            ToolSurface.IsChecked = (rct & ReportChartTools.ToolSurface) > 0;
            ToolBag.IsChecked = (rct & ReportChartTools.ToolBag) > 0;
            ToolVoronoy.IsChecked = (rct & ReportChartTools.ToolVoronoy) > 0;
            ToolBeyondAxis.IsChecked = (rct & ReportChartTools.ToolBeyondAxis) > 0;
            ToolMirrored.IsChecked = (rct & ReportChartTools.ToolMirrored) > 0;
            ToolWithConstants.IsChecked = (rct & ReportChartTools.ToolWithConstants) > 0;
            ToolWithGridLines.IsChecked = (rct & ReportChartTools.ToolWithGridLines) > 0;
            ToolCutSurfaces.IsChecked = (rct & ReportChartTools.ToolCutSurfaces) > 0;
            ToolRangeMean.IsChecked = (rct & ReportChartTools.ToolRangeMean) > 0;
            ToolRangeMedian.IsChecked = (rct & ReportChartTools.ToolRangeMedian) > 0;
            ToolText.IsChecked = (rct & ReportChartTools.ToolText) > 0;
            ToolHexBin.IsChecked = (rct & ReportChartTools.ToolHexBin) > 0;
            ToolLog.IsChecked = (rct & ReportChartTools.ToolLog) > 0;
            _currentTools = rct;
            StoreSettings(_currentTools);
        }

        public void SetAvailability(ReportChartTools rct)
        {
            _availableTools = rct;
            ToolRug.IsEnabled = (rct & ReportChartTools.ToolRug) > 0;
            ToolKDE.IsEnabled = (rct & ReportChartTools.ToolKDE) > 0;
            ToolSurface.IsEnabled = (rct & ReportChartTools.ToolSurface) > 0;
            ToolBag.IsEnabled = (rct & ReportChartTools.ToolBag) > 0;
            ToolVoronoy.IsEnabled = (rct & ReportChartTools.ToolVoronoy) > 0;
            ToolBeyondAxis.IsEnabled = (rct & ReportChartTools.ToolBeyondAxis) > 0;
            ToolMirrored.IsEnabled = (rct & ReportChartTools.ToolMirrored) > 0;
            ToolWithConstants.IsEnabled = (rct & ReportChartTools.ToolWithConstants) > 0;
            ToolWithGridLines.IsEnabled = (rct & ReportChartTools.ToolWithGridLines) > 0;
            ToolCutSurfaces.IsEnabled = (rct & ReportChartTools.ToolCutSurfaces) > 0;
            ToolRangeMean.IsEnabled = (rct & ReportChartTools.ToolRangeMean) > 0;
            ToolRangeMedian.IsEnabled = (rct & ReportChartTools.ToolRangeMedian) > 0;
            ToolText.IsEnabled = (rct & ReportChartTools.ToolText) > 0;
            ToolHexBin.IsEnabled = (rct & ReportChartTools.ToolHexBin) > 0;
            ToolLog.IsEnabled = (rct & ReportChartTools.ToolLog) > 0;
            foreach (var c in ButtonPanel.Children)
            {
                if (c is TextImageToggleButtonBase && !((TextImageToggleButtonBase)c).IsEnabled) ((TextImageToggleButtonBase)c).IsChecked = false;
            }
            _currentTools &= _availableTools;
        }

        public void RefreshUI()
        {
            SetCurrentTools(_currentTools);
        }

        private static void StoreSettings(ReportChartTools rct)
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisToolsSettings"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisToolsSettings", rct);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisToolsSettings"] = rct;
        }

        private void Tool_Click(object sender, RoutedEventArgs e)
        {
            if ((sender as TextImageToggleButtonBase)?.Name == "ToolRangeMean" && ToolRangeMean.IsChecked.Value) ToolRangeMedian.IsChecked = false;
            if ((sender as TextImageToggleButtonBase)?.Name == "ToolRangeMedian" && ToolRangeMedian.IsChecked.Value) ToolRangeMean.IsChecked = false;
            _currentTools = ReportChartTools.ToolNone;
            if (ToolRug.IsChecked != null && ToolRug.IsChecked.Value) _currentTools |= ReportChartTools.ToolRug;
            if (ToolKDE.IsChecked != null && ToolKDE.IsChecked.Value) _currentTools |= ReportChartTools.ToolKDE;
            if (ToolSurface.IsChecked != null && ToolSurface.IsChecked.Value) _currentTools |= ReportChartTools.ToolSurface;
            if (ToolVoronoy.IsChecked != null && ToolVoronoy.IsChecked.Value) _currentTools |= ReportChartTools.ToolVoronoy;
            if (ToolBag.IsChecked != null && ToolBag.IsChecked.Value) _currentTools |= ReportChartTools.ToolBag;
            if (ToolMirrored.IsChecked != null && ToolMirrored.IsChecked.Value) _currentTools |= ReportChartTools.ToolMirrored;
            if (ToolBeyondAxis.IsChecked != null && ToolBeyondAxis.IsChecked.Value) _currentTools |= ReportChartTools.ToolBeyondAxis;
            if (ToolWithConstants.IsChecked!=null && ToolWithConstants.IsChecked.Value)_currentTools |= ReportChartTools.ToolWithConstants;
            if (ToolWithGridLines.IsChecked != null && ToolWithGridLines.IsChecked.Value) _currentTools |= ReportChartTools.ToolWithGridLines;
            if (ToolCutSurfaces.IsChecked != null && ToolCutSurfaces.IsChecked.Value) _currentTools |= ReportChartTools.ToolCutSurfaces;
            if (ToolText.IsChecked != null && ToolText.IsChecked.Value) _currentTools |= ReportChartTools.ToolText;
            if (ToolRangeMean.IsChecked != null && ToolRangeMean.IsChecked.Value) _currentTools |= ReportChartTools.ToolRangeMean;
            if (ToolRangeMedian.IsChecked != null && ToolRangeMedian.IsChecked.Value) _currentTools |= ReportChartTools.ToolRangeMedian;
            if (ToolHexBin.IsChecked != null && ToolHexBin.IsChecked.Value) _currentTools |= ReportChartTools.ToolHexBin;
            if (ToolLog.IsChecked != null && ToolLog.IsChecked.Value) _currentTools |= ReportChartTools.ToolLog;

            StoreSettings(_currentTools);
            ToolsChanged?.Invoke(this, new ToolsChangedEventArgs { Tools = _currentTools });
        }
    }

    public class ToolsChangedEventArgs : EventArgs
    {
        public ReportChartTools Tools { get; set; }
    }
}