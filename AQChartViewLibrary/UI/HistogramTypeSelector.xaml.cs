﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class HistogramTypeSelector
    {
        public delegate void HistogramTypeChangedDelegate(object sender, HistogramTypeChangedEventArgs e);

        private ReportChartHistogramType _currentHistogramType;

        private string _currentHistogramTypeDescription;

        public HistogramTypeSelector()
        {
            InitializeComponent();
            SetCurrentHistogramType(ReportChartHistogramType.Frequencies);
        }

        public event HistogramTypeChangedDelegate HistogramTypeChanged;

        public ReportChartHistogramType GetCurrentHistogramType()
        {
            return _currentHistogramType;
        }

        public string GetCurrentHistogramTypeDescription()
        {
            return _currentHistogramTypeDescription;
        }

        public void SetCurrentHistogramType(ReportChartHistogramType rcct)
        {
            RadioButton rb = (from r in ButtonPanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == rcct.ToString()
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentHistogramType = rcct;
            _currentHistogramTypeDescription = (rb.Content as TextBlock).Text;
        }

        public void RefreshUI()
        {
            SetCurrentHistogramType(_currentHistogramType);
        }

       
        private void HistogramType_Click(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton) sender;
            _currentHistogramType = (ReportChartHistogramType) Enum.Parse(typeof (ReportChartHistogramType), rb.Name, true);
            _currentHistogramTypeDescription = (rb.Content as TextBlock).Text;
            if (HistogramTypeChanged != null)
                HistogramTypeChanged.Invoke(this,
                                       new HistogramTypeChangedEventArgs
                                           {
                                               HistogramType = _currentHistogramType,
                                               HistogramDescription = _currentHistogramTypeDescription
                                           });
        }
    }

    public class HistogramTypeChangedEventArgs : EventArgs
    {
        public ReportChartHistogramType HistogramType { get; set; }
        public string HistogramDescription { get; set; }
    }
}