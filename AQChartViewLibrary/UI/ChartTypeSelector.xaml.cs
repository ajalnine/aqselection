﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class ChartTypeSelector : UserControl
    {
        public delegate void ChartTypeChangedDelegate(object sender, ChartTypeChangedEventArgs e);

        private ReportChartType _currentChartType = ReportChartType.Histogram;

        public ChartTypeSelector()
        {
            InitializeComponent();
        }

        public event ChartTypeChangedDelegate ChartTypeChanged;

        public ReportChartType GetCurrentChartType()
        {
            return _currentChartType;
        }

        public void SetCurrentChartType(ReportChartType rct)
        {
            RadioButton rb = (from r in ButtonPanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == rct.ToString()
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentChartType = rct;
        }

        public void Enable(ReportChartType rct, bool enabled)
        {
            RadioButton rb = (from r in ButtonPanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == rct.ToString()
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsEnabled = enabled;
        }

        public void RefreshUI()
        {
            SetCurrentChartType(_currentChartType);
        }

        private void ChartType_Click(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton) sender;
            _currentChartType = (ReportChartType) Enum.Parse(typeof (ReportChartType), rb.Name, true);
            if (ChartTypeChanged != null)
                ChartTypeChanged.Invoke(this, new ChartTypeChangedEventArgs {ChartType = _currentChartType});
        }

        public string GetChartName()
        {
            switch (_currentChartType)
            {
                case ReportChartType.Statistics:
                    return "Статистика";

                case ReportChartType.Dynamic:
                    return "Динамика";

                case ReportChartType.Card:
                    return "Карта";

                case ReportChartType.Histogram:
                    return "Гистограмма";

                case ReportChartType.Groups:
                    return "График по группам";

                case ReportChartType.ScatterPlot:
                    return "График рассеяния";

                case ReportChartType.ProbabilityPlot:
                    return "Карта вероятностей P-P";
                default:
                    return null;
            }
        }
    }

    public class ChartTypeChangedEventArgs : EventArgs
    {
        public ReportChartType ChartType { get; set; }
    }
}