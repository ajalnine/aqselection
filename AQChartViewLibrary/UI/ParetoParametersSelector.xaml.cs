﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class ParetoParametersSelector
    {
        public delegate void ParetoParametersChangedDelegate(object sender, ParetoParametersChangedEventArgs e);

        private int? _currentGroupNumber;
        
        public ParetoParametersSelector()
        {
            InitializeComponent();
            SetCurrentGroupNumber(8);
        }

        public event ParetoParametersChangedDelegate ParetoParametersChanged;

        public int? GetCurrentGroupNumber()
        {
            return _currentGroupNumber;
        }
        
        public void SetCurrentGroupNumber(int? groupNumber)
        {
            _currentGroupNumber = groupNumber;
            GroupNumberBox.Text = !groupNumber.HasValue ? string.Empty : groupNumber.ToString();
        }
        
        public void RefreshUI()
        { 
            SetCurrentGroupNumber(_currentGroupNumber);
        }

        private void GroupNumberBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            int currentGroupNumber;
            var gnExists = int.TryParse(GroupNumberBox.Text, out currentGroupNumber);
            if (!gnExists && GroupNumberBox.Text.Count() > 0)
            {
                GroupNumberBox.Focus();
                ShowExclamation(true);
                return;
            }
            else ShowExclamation(false);
            _currentGroupNumber = gnExists ? currentGroupNumber : new int?();
            InvokeChangeEvent();
        }

        private void DecrementButtonClick(object sender, RoutedEventArgs e)
        {
            if (_currentGroupNumber == 0) return;
            _currentGroupNumber--;
            SetCurrentGroupNumber(_currentGroupNumber);
            InvokeChangeEvent();
       }

        private void IncrementButtonClick(object sender, RoutedEventArgs e)
        {
            _currentGroupNumber++;
            SetCurrentGroupNumber(_currentGroupNumber);
            InvokeChangeEvent();
        }

        public void ShowExclamation(bool iserror)
        {
            GroupNumberBox.Background = iserror ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);
        }

        private void InvokeChangeEvent()
        {
            if (ParetoParametersChanged != null)
            {
                ParetoParametersChanged.Invoke(this, new ParetoParametersChangedEventArgs
                {
                    GroupNumber = _currentGroupNumber
                });
            }
        }

    }

    public class ParetoParametersChangedEventArgs : EventArgs
    {
        public int? GroupNumber { get; set; }
    }
}