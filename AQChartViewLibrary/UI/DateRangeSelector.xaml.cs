﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQControlsLibrary.Annotations;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class DateRangeSelector
    {
        public delegate void DateRangeChangedDelegate(object sender, DateRangeChangedEventArgs e);
        private DateTime? _minLimit = null;
        private DateTime? _maxLimit = null;
        private string _currentPeriodDescription;

        private ReportChartRangeStyle _currentPeriodStyle;
        private SLDataTable _Periods;
        private ObservableCollection<PeriodLine> _periodLines;
        private bool _colorProcessingDisabled;

        public DateRangeSelector()
        {
            InitializeComponent();
            SetCurrentPeriodStyle(ReportChartRangeStyle.Filled | ReportChartRangeStyle.Thin | ReportChartRangeStyle.Labeled);
            PeriodsInputPanel.Visibility = Visibility.Visible;
            RefreshDatePickerStates(null);
        }

        public event DateRangeChangedDelegate PeriodChanged;
        
        public ReportChartRangeStyle GetCurrentPeriodStyle()
        {
            return _currentPeriodStyle;
        }

        public string GetCurrentPeriodDescription()
        {
            return _currentPeriodDescription;
        }

        public SLDataTable GetPeriods()
        {
            return _Periods;
        }

        public void SetCurrentPeriodStyle(ReportChartRangeStyle style)
        {
            _currentPeriodStyle = style;
            Thick.IsChecked = (_currentPeriodStyle & ReportChartRangeStyle.Thick) == ReportChartRangeStyle.Thick;
            Thin.IsChecked = (_currentPeriodStyle & ReportChartRangeStyle.Thin) == ReportChartRangeStyle.Thin;
            Invisible.IsChecked = (_currentPeriodStyle & ReportChartRangeStyle.Invisible) == ReportChartRangeStyle.Invisible;
            Filled.IsChecked = (_currentPeriodStyle & ReportChartRangeStyle.Filled) == ReportChartRangeStyle.Filled;
            Labeled.IsChecked = (_currentPeriodStyle & ReportChartRangeStyle.Labeled) == ReportChartRangeStyle.Labeled;
        }
        
        public void RefreshUI()
        {
        }

        public void SetupPeriods(SLDataTable userPeriods)
        {
            //PeriodsInputPanel.Visibility = Visibility.Visible;
            //RefreshDatePickerStates(userPeriods);
        }

        private void RefreshDatePickerStates(SLDataTable userLimits)
        {
            if (userLimits == null || userLimits.Table == null || userLimits.Table.Count == 0)
            {
                _minLimit = null;
                _maxLimit = null;
                userLimits = new SLDataTable
                {
                    Table = new List<SLDataRow>(),
                    DataTypes = new List<string> {"DateTime", "DateTime" }
                };
                userLimits.Table.Add(new SLDataRow {Row = new List<object> {null, null}});
                userLimits.ColumnNames = new List<string> {"От", "До"};
            }
            else
            {
                _minLimit = userLimits.Table[0].Row[0] as DateTime?;
                _maxLimit = userLimits.Table[0].Row[1] as DateTime?;
            }

            _periodLines = new ObservableCollection<PeriodLine>();

            for (var i=0; i<userLimits.Table[0].Row.Count; i+=2)
            {
                var r = new PeriodLine
                {
                    Min = userLimits.Table[0].Row[i] as DateTime?,
                    Max = userLimits.Table[0].Row[i + 1] as DateTime?,
                    Tag = i>0? (i-1).ToString(CultureInfo.InvariantCulture) : string.Empty
                };
                r.PropertyChanged += PeriodsIsChanged;
                _periodLines.Add(r);    
            }
            CreateUserLimitsTable();
            LimitsViewListBox.ItemsSource = _periodLines;
        }
        
        private void SetPeriods()
        {
            CreateUserLimitsTable();
            PeriodChanged?.Invoke(this, new DateRangeChangedEventArgs
                                        {
                                            RangeDescription = _currentPeriodDescription,
                                            UserRanges = _Periods,
                                            RangeStyle = _currentPeriodStyle,
                                            OnlyStyleChanged = false
                                    });
        }

        private void CreateUserLimitsTable()
        {
            _Periods = new SLDataTable
            {
                TableName = "Периоды",
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                DataTypes = new List<string>()
            };

            _Periods.Table.Add(new SLDataRow {Row = new List<object>()});
            _Periods.Table.Add(new SLDataRow { Row = new List<object>() });

            foreach (var r in _periodLines)
            {
                _Periods.ColumnNames.Add(r.Name);
                _Periods.ColumnNames.Add("#" + r.Name + "_Цвет");
                _Periods.DataTypes.Add("DateTime");
                _Periods.DataTypes.Add("String");
                _Periods.Table[0].Row.Add(r.Min);
                _Periods.Table[1].Row.Add(r.Max);
                _Periods.Table[0].Row.Add(r.Color);
                _Periods.Table[1].Row.Add(r.Color);
            }
        }

        public void RenamePeriod(List<ReportRenameOperation> ro)
        {
            foreach (var r in ro.Where(a=>a.RenameTarget== ReportRenameTarget.DateRange))
            {
                var renaming = _periodLines.SingleOrDefault(a => String.Equals(a.Name.Trim(), r.OldName.Trim(), StringComparison.CurrentCultureIgnoreCase));
                if (renaming != null) renaming.Name = r.NewName;
            }
            SetPeriods();
        }

        private void DeletePeriodLineButtonClick(object sender, RoutedEventArgs e)
        {
            var a = sender as TextImageButtonBase;
            if (a == null) return;
            if (_periodLines.Count > 1) _periodLines.Remove(_periodLines.SingleOrDefault(x => x.Tag == (string) a.Tag));
            else
            {
                _periodLines[0].Min = null;
                _periodLines[0].Max = null;
                _periodLines[0].Name = null;
            }
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            SetPeriods();
        }

        private void AddPeriodLineButtonClick(object sender, RoutedEventArgs e)
        {
            if (_periodLines.Count == 9) return;
            var r = new PeriodLine
            {
                Max = null,
                Min = null,
                Tag = (_periodLines.Any() ? (_periodLines.Count()+1).ToString(CultureInfo.InvariantCulture):string.Empty)
                ,Color = _periodLines[_periodLines.Count - 1].Color
            };
            r.PropertyChanged += PeriodsIsChanged;
            _periodLines.Add(r);
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            SetPeriods();
        }

        void PeriodsIsChanged(object sender, PropertyChangedEventArgs e)
        {
            SetPeriods();
        }

        private void ResetRangeLinesButtonClick(object sender, RoutedEventArgs e)
        {
            if (_periodLines.Count > 1)
            {
                for(var i=_periodLines.Count-1; i>0; i--)_periodLines.RemoveAt(i);
            }
            else
            {
                _periodLines[0].Min = DateTime.Now;
                _periodLines[0].Max = DateTime.Now;
                _periodLines[0].Name = string.Empty;
                _periodLines[0].Color = ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.32);
            }
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            SetPeriods();
        }
        
        private void RangeStyle_Click(object sender, RoutedEventArgs e)
        {
            _currentPeriodStyle = ReportChartRangeStyle.None;
            _currentPeriodStyle |= Thick.IsChecked != null && Thick.IsChecked.Value ? ReportChartRangeStyle.Thick : 0;
            _currentPeriodStyle |= Thin.IsChecked != null && Thin.IsChecked.Value ? ReportChartRangeStyle.Thin : 0;
            _currentPeriodStyle |= Invisible.IsChecked != null && Invisible.IsChecked.Value ? ReportChartRangeStyle.Invisible : 0;
            _currentPeriodStyle |= Filled.IsChecked != null && Filled.IsChecked.Value ? ReportChartRangeStyle.Filled : 0;
            _currentPeriodStyle |= Labeled.IsChecked != null && Labeled.IsChecked.Value ? ReportChartRangeStyle.Labeled : 0;
            
            PeriodChanged?.Invoke(this,
                                        new DateRangeChangedEventArgs
                                        {
                                            RangeDescription = _currentPeriodDescription,
                                            RangeStyle = _currentPeriodStyle,
                                            OnlyStyleChanged = true
                                        });
        }

        private void AutoColor_OnClick(object sender, RoutedEventArgs e)
        {
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
        }

        private void RecalcAutoColors()
        {
            _colorProcessingDisabled = true;
            if (_periodLines != null && _periodLines.Count > 1)
            {
                for (var i = 0; i < _periodLines.Count; i++)
                {
                    if (i == _periodLines.Count - 1) _colorProcessingDisabled = false;
                    _periodLines[i].Color = ColorConvertor.GetColorStringForScale(ColorScales.HSV,
                        0.32 + 0.69 * Math.Floor(i) / _periodLines.Count);
                }
            }
        }
    }

    public class DateRangeChangedEventArgs : EventArgs
    {
        public ReportChartRangeStyle RangeStyle { get; set; }
        public string RangeDescription { get; set; }
        public SLDataTable UserRanges { get; set; }
        public bool OnlyStyleChanged { get; set; }
    }
    
    public class PeriodLine : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) 
                    ?  GetPeriodName(_min, _max)
                    : _name; }
            set { _name = value; }
        }

        private string GetPeriodName(DateTime? _min, DateTime? _max)
        {
            if (_min==null || _max == null)  return String.Format("{0}..{1}", _min?.ToString("dd.MM.yyyy"), _max?.ToString("dd.MM.yyyy"));
            if (_min == _max)   return String.Format("{0}", _min?.ToString("dd.MM.yyyy"));
            var min = _min.Value;
            var max = _max.Value;
            if (min.Year == max.Year && min.Month == max.Month) return String.Format("{0}..{1}", _min?.ToString("dd"), _max?.ToString("dd.MM.yyyy"));
            if (min.Year == max.Year) return String.Format("{0}..{1}", _min?.ToString("dd.MM"), _max?.ToString("dd.MM.yyyy"));
            return String.Format("{0}..{1}", _min?.ToString("dd.MM.yyyy"), _max?.ToString("dd.MM.yyyy"));
        }

        private DateTime? _max, _min;

        public DateTime? Min
        {
            get { return _min; }
            set
            {
                _min = value;
                OnPropertyChanged("Min");
            }
        }

        public DateTime? Max
        {
            get { return _max; }
            set
            {
                _max = value;
                OnPropertyChanged("Max");
            }
        }
        private string _tag;
        public string Tag
        {
            get { return _tag; }
            set
            {
                _tag = value;
                OnPropertyChanged("Tag");
            }
        }

        private string _color;
        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
                OnPropertyChanged("Color");
            }
        }
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}