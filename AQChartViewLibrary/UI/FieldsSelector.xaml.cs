﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class FieldsSelector
    {
        private bool _processingDisabled;
        private bool _multipleYMode = false, _zChanged = false;
        public delegate void FieldsChangedDelegate(object sender, FieldsChangedEventArgs e);

        public delegate void LayerCommandDelegate(object sender, LayerCommandEventArgs e);

        public static readonly DependencyProperty XFieldProperty =
            DependencyProperty.Register("XField", typeof (string), typeof (FieldsSelector), new PropertyMetadata(null));
        public static readonly DependencyProperty YFieldsProperty =
            DependencyProperty.Register("YFields", typeof(List<string>), typeof(FieldsSelector), new PropertyMetadata(new List<string>()));
        public static readonly DependencyProperty ZFieldProperty =
            DependencyProperty.Register("Field", typeof (string), typeof (FieldsSelector), new PropertyMetadata(null));
        public static readonly DependencyProperty WFieldProperty =
            DependencyProperty.Register("WField", typeof(string), typeof(FieldsSelector), new PropertyMetadata(null));
        public static readonly DependencyProperty VFieldProperty =
            DependencyProperty.Register("VField", typeof(string), typeof(FieldsSelector), new PropertyMetadata(null));
        List<string> xtypes = new List<string>();
        List<string> ytypes = new List<string>();
        List<string> ztypes = new List<string>();
        List<string> wtypes = new List<string>();
        List<string> vtypes = new List<string>();

        public FieldsSelector()
        {
            _processingDisabled = false;
            InitializeComponent();
            FlipVariablesXYButton.ImageSource = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/flip.png", UriKind.Relative)};
            FlipVariablesYZButton.ImageSource = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/flip.png", UriKind.Relative) };
            FlipVariablesXZButton.ImageSource = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/flip.png", UriKind.Relative) };
        }

        public string XField
        {
            get { return (string) GetValue(XFieldProperty); }
            set { SetValue(XFieldProperty, value); }
        }

        public List<string> YFields
        {
            get { return (List<string>)GetValue(YFieldsProperty); }
            set { SetValue(YFieldsProperty, value); }
        }

        public string ZField
        {
            get { return (string)GetValue(ZFieldProperty); }
            set { SetValue(ZFieldProperty, value); }
        }

        public string WField
        {
            get { return (string)GetValue(WFieldProperty); }
            set { SetValue(WFieldProperty, value); }
        }
        
        public string VField
        {
            get { return (string)GetValue(VFieldProperty); }
            set { SetValue(VFieldProperty, value); }
        }
        
        public event FieldsChangedDelegate FieldsChanged;
        public event LayerCommandDelegate LayerCommand;
        private SLDataTable sldt;
        private ReportChartType rct = ReportChartType.Histogram;

        private SelectorDataTypeFiltration XFilterMode = SelectorDataTypeFiltration.Double;
        private SelectorDataTypeFiltration YFilterMode = SelectorDataTypeFiltration.Double;
        private SelectorDataTypeFiltration ZFilterMode = SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone;
        private SelectorDataTypeFiltration WFilterMode = SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone;
        private SelectorDataTypeFiltration VFilterMode = SelectorDataTypeFiltration.AllData | SelectorDataTypeFiltration.AddNone;

        private ObservableCollection<MarkedItem> xdata;
        private ObservableCollection<MarkedItem> ydata;
        private ObservableCollection<MarkedItem> zdata;
        private ObservableCollection<MarkedItem> wdata;
        private ObservableCollection<MarkedItem> vdata;
        public bool SetupInputs(SLDataTable SLDT)
        {
            sldt = SLDT;
            return UpdateSelectorSources();
        }

        public bool IsMultiSelection()
        {
            return _multipleYMode;
        }

        private bool UpdateSelectorSources()
        {
            if (sldt == null || sldt.ColumnNames==null) return false;
            _processingDisabled = true;
            
            FillFilterHelper(ref xtypes, XFilterMode);
            FillFilterHelper(ref ytypes, YFilterMode);
            FillFilterHelper(ref ztypes, ZFilterMode);
            FillFilterHelper(ref wtypes, WFilterMode);
            FillFilterHelper(ref vtypes, VFilterMode);
            var possibleColumns = (from x in sldt.ColumnNames where !x.ToLower().EndsWith("_нг") && !x.ToLower().EndsWith("_вг") && !x.ToLower().EndsWith("_цвет") && !x.ToLower().EndsWith("_размер") && !x.ToLower().EndsWith("_метка") select x).ToList();

            xdata = new ObservableCollection<MarkedItem>(from x in possibleColumns where (xtypes.IsEmpty() || xtypes.Contains(sldt.DataTypes[sldt.ColumnNames.IndexOf(x)].ToLower())) select new MarkedItem(x));
            ydata = new ObservableCollection<MarkedItem>(from y in possibleColumns where (ytypes.IsEmpty() || ytypes.Contains(sldt.DataTypes[sldt.ColumnNames.IndexOf(y)].ToLower())) select new MarkedItem(y) { ShowCheckBox = _multipleYMode ? Visibility.Visible : Visibility.Collapsed });
            zdata = new ObservableCollection<MarkedItem>(from z in possibleColumns where (ztypes.IsEmpty() || ztypes.Contains(sldt.DataTypes[sldt.ColumnNames.IndexOf(z)].ToLower())) select new MarkedItem(z));
            wdata = new ObservableCollection<MarkedItem>(from w in possibleColumns where (wtypes.IsEmpty() || wtypes.Contains(sldt.DataTypes[sldt.ColumnNames.IndexOf(w)].ToLower())) select new MarkedItem(w));
            vdata = new ObservableCollection<MarkedItem>(from v in possibleColumns where (vtypes.IsEmpty() || vtypes.Contains(sldt.DataTypes[sldt.ColumnNames.IndexOf(v)].ToLower())) select new MarkedItem(v));

            if ((XFilterMode & SelectorDataTypeFiltration.AddNone) == SelectorDataTypeFiltration.AddNone) xdata.Insert(0, new MarkedItem("Нет"));
            if ((YFilterMode & SelectorDataTypeFiltration.AddNone) == SelectorDataTypeFiltration.AddNone) ydata.Insert(0, new MarkedItem("Нет"));
            if ((ZFilterMode & SelectorDataTypeFiltration.AddNone) == SelectorDataTypeFiltration.AddNone) zdata.Insert(0, new MarkedItem("Нет"));
            if ((WFilterMode & SelectorDataTypeFiltration.AddNone) == SelectorDataTypeFiltration.AddNone) wdata.Insert(0, new MarkedItem("Нет"));
            if ((VFilterMode & SelectorDataTypeFiltration.AddNone) == SelectorDataTypeFiltration.AddNone) vdata.Insert(0, new MarkedItem("Нет"));

            if (sldt.IsFiltered)
            {
                var filteredColumns = sldt.Filters.Select(a => a.ColumnName).ToList();
                foreach (var x in xdata) x.ShowFilter = filteredColumns.Contains(x.Field) ? Visibility.Visible : Visibility.Collapsed;
                foreach (var y in ydata) y.ShowFilter = filteredColumns.Contains(y.Field) ? Visibility.Visible : Visibility.Collapsed;
                foreach (var z in zdata) z.ShowFilter = filteredColumns.Contains(z.Field) ? Visibility.Visible : Visibility.Collapsed;
                foreach (var w in wdata) w.ShowFilter = filteredColumns.Contains(w.Field) ? Visibility.Visible : Visibility.Collapsed;
                foreach (var v in vdata) v.ShowFilter = filteredColumns.Contains(v.Field) ? Visibility.Visible : Visibility.Collapsed;
            }

            XSelector.ItemsSource = xdata;
            YSelector.ItemsSource = ydata;
            var oldzSource = ((ObservableCollection<MarkedItem>)ZSelector.ItemsSource);
            _zChanged = oldzSource != null && oldzSource.Count > 0 && oldzSource[0].ToString() != zdata[0].ToString();
            ZSelector.ItemsSource = zdata;
            WSelector.ItemsSource = wdata;
            VSelector.ItemsSource = vdata;
            _processingDisabled = false; 
            return SelectDefaultValues();
        }

        

        private static void FillFilterHelper(ref List<string> types, SelectorDataTypeFiltration FilterMode)
        {
            types = new List<string>();
            if ((FilterMode & SelectorDataTypeFiltration.Double) == SelectorDataTypeFiltration.Double)
            {
                types.Add("system.double");
                types.Add("double");
            }

            if ((FilterMode & SelectorDataTypeFiltration.DateTime) == SelectorDataTypeFiltration.DateTime)
            {
                types.Add("system.datetime");
                types.Add("datetime");
            }

            if ((FilterMode & SelectorDataTypeFiltration.String) == SelectorDataTypeFiltration.String)
            {
                types.Add("system.string");
                types.Add("string");
            }
        }

        private bool SelectDefaultValues()
        {
            _processingDisabled = true; 
            var result = false;
            var xfields = ((IEnumerable<MarkedItem>) XSelector.ItemsSource).Select(a => a.Field).ToList();
            var yfields = ((IEnumerable<MarkedItem>) YSelector.ItemsSource).Select(a => a.Field).ToList();
            var xcount = xfields.Count();
            var ycount = yfields.Count();
            var count = Math.Max((double)xcount, (double)ycount);
            if (count >= 0)
            {
                MarkedItem mi = ((IEnumerable<MarkedItem>) XSelector.ItemsSource).FirstOrDefault();
                //if (mi == null) return false;
                if (count >= 1)
                {
                    var indexOfLastXField = xfields.IndexOf(XField);
                    var sourceindexOfLastXField = sldt.ColumnNames.IndexOf(XField);
                    if (indexOfLastXField < 0) XField = xfields.Count>0 ? xfields[0] : null;
                    if (indexOfLastXField >= 0 && xtypes.Count > 0 && !xtypes.Contains(sldt.DataTypes[sourceindexOfLastXField].ToLower())) indexOfLastXField = 0;
                    if (xfields.Count > 0)XSelector.SelectedIndex = indexOfLastXField > 0 ? indexOfLastXField : 0;

                    if (YSelector.SelectionMode == SelectionMode.Multiple)
                    {
                        foreach (var y in YFields)
                        {
                            var sourceindexOfY = sldt.ColumnNames.IndexOf(y);
                            if (sourceindexOfY >= 0 && ytypes.Count > 0 && ytypes.Contains(sldt.DataTypes[sourceindexOfY].ToLower()))
                            {
                                var toSelect = YSelector.ItemsSource.OfType<MarkedItem>().Where(a =>a.Field==y).ToList();
                                foreach (var m in toSelect)YSelector.SelectedItems.Add(m);
                            }
                        }
                    }
                    else
                    {
                        var YField = YFields.FirstOrDefault();
                        var indexOfLastYField = yfields.IndexOf(YField);
                        var sourceindexOfLastYField = sldt.ColumnNames.IndexOf(YField);
                        if (indexOfLastYField < 0) YField = yfields[0];
                        if (indexOfLastYField >= 0 && ytypes.Count > 0 && !ytypes.Contains(sldt.DataTypes[sourceindexOfLastYField].ToLower())) indexOfLastYField = 0;
                        YSelector.SelectedIndex = indexOfLastYField > 0 ? indexOfLastYField : 0;
                    }
                }
                else
                {
                    if (xfields.Count > 0) XSelector.SelectedIndex = 0;
                    if (yfields.Count > 0) YSelector.SelectedIndex = 0;
                }

                var zfields = ((IEnumerable<MarkedItem>)ZSelector.ItemsSource).Select(a => a.Field).ToList();
                var indexOfLastZField = zfields.IndexOf(ZField);

                if (indexOfLastZField > 0 && !_zChanged) ZSelector.SelectedIndex = indexOfLastZField;
                else
                {
                    if (rct != ReportChartType.Groups)
                    {
                        ZSelector.SelectedIndex = 0;
                    }
                    else 
                    {
                        SelectZforGroups(zfields);
                    }
                }

                var wfields = ((IEnumerable<MarkedItem>)WSelector.ItemsSource).Select(a => a.Field).ToList();
                var indexOfLastWField = wfields.IndexOf(WField);
                WSelector.SelectedIndex = indexOfLastWField > 0 ? indexOfLastWField : 0;

                var vfields = ((IEnumerable<MarkedItem>)VSelector.ItemsSource).Select(a => a.Field).ToList();
                var indexOfLastVField = vfields.IndexOf(VField);
                VSelector.SelectedIndex = indexOfLastVField > 0 ? indexOfLastVField : 0;
                result = true;
            }
            _processingDisabled = false;
            RenewValues();
            return result;
        }

        private void RenewValues()
        {

            if (XSelector.SelectedItem != null) XField = ((MarkedItem)XSelector.SelectedItem).Field;
            if (ZSelector.SelectedItem != null) ZField = ((MarkedItem)ZSelector.SelectedItem).Field;
            if (WSelector.SelectedItem != null) WField = ((MarkedItem)WSelector.SelectedItem).Field;
            if (VSelector.SelectedItem != null) VField = ((MarkedItem)VSelector.SelectedItem).Field;
            if (YSelector.SelectedItems != null) YFields = YSelector.SelectedItems.OfType<MarkedItem>().Select(a=>a.Field).ToList();

            InvokeFieldsChangeEvent(null, ChangedFieldType.Renew);
        }

        public void SetZWVToStart()
        {
            _processingDisabled = true;
            //if (ZSelector.ItemsSource != null && ((IEnumerable<MarkedItem>)ZSelector.ItemsSource).Any()) ZSelector.SelectedIndex = 0;
            if (WSelector.ItemsSource != null && ((IEnumerable<MarkedItem>)WSelector.ItemsSource).Any()) WSelector.SelectedIndex = 0;
            if (VSelector.ItemsSource != null && ((IEnumerable<MarkedItem>)VSelector.ItemsSource).Any()) VSelector.SelectedIndex = 0;
            _processingDisabled = false;
            RenewValues();
        }

        private void SelectZforGroups(List<string> zfields)
        {
            if (zfields.Count > 0 && zfields[0] == "Нет" || _zChanged)
            {
                ZField = zfields[0];
                ZSelector.SelectedIndex = 0;
                return;
            }
            var isSelected = false;
            for (var i = 0; i < zfields.Count; i++)
            {
                var distinctValues = sldt.GetColumnData(zfields[i]).Distinct();
                if (distinctValues.Count() >= 1000) continue;
                ZField = zfields[i];
                ZSelector.SelectedIndex = i;
                isSelected = true;
                break;
            }
            if (!isSelected)
            {
                ZField = zfields[0];
                ZSelector.SelectedIndex = 0;
            }
        }

        public void ResetXMarks()
        {
            foreach (object x in XSelector.ItemsSource) ((MarkedItem)x).Mark = Colors.Black;
        }

        public void ResetYMarks()
        {
            foreach (object x in YSelector.ItemsSource) ((MarkedItem)x).Mark = Colors.Black;
        }

        public List<MarkedItem> GetXItems()
        {
            return ((IEnumerable<MarkedItem>)XSelector.ItemsSource).ToList();
        }

        public List<MarkedItem> GetYItems()
        {
            return ((IEnumerable<MarkedItem>)YSelector.ItemsSource).ToList();
        }
        
        public void MarkX(string xName, Color c)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)XSelector.ItemsSource where (x).Field == xName select x)
                    .FirstOrDefault();
            if (mi != null) mi.Mark = c;
        }

        public void MarkY(string yName, Color c)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)YSelector.ItemsSource where (x).Field == yName select x)
                    .FirstOrDefault();
            if (mi != null) mi.Mark = c;
        }

        private void SelectX(string xName)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)XSelector.ItemsSource where (x).Field == xName select x)
                    .FirstOrDefault();
            if (mi != null) XSelector.SelectedItem = mi;
        }

        public void SelectAllY()
        {
            SetMultipleSelection();
            EnableMultipleSelection(true, true);
            var ydata = ((IEnumerable<MarkedItem>) YSelector.ItemsSource).Where(a => a.Field != XField);
            foreach (var y in ydata) y.IsSelected = true;
            YFields = ydata.Select(a => a.Field).ToList();
        }


        private void SelectY(List<string> yNames)
        {
            if (yNames.Count > 1)
            {
                SetMultipleSelection();
                EnableMultipleSelection(true, true);
                YSelector.SelectedItems.Clear();
                foreach (var y in yNames)
                {
                    MarkedItem mi = (from x in (IEnumerable<MarkedItem>)YSelector.ItemsSource where (x).Field == y select x)
                            .FirstOrDefault();
                    if (mi != null) YSelector.SelectedItems.Add(mi);
                }
            }
            else
            {
                SetSingleSelection();
                var ydata = (IEnumerable<MarkedItem>) YSelector.ItemsSource ;
                foreach (var y in ydata) y.ShowCheckBox = Visibility.Collapsed;
                MarkedItem mi = (from x in (IEnumerable<MarkedItem>)YSelector.ItemsSource where (x).Field == yNames.FirstOrDefault() select x)
                                    .FirstOrDefault();
                if (mi != null) YSelector.SelectedItem = mi;
            }
        }

        private void SelectZ(string zName)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)ZSelector.ItemsSource where (x).Field == zName select x)
                    .FirstOrDefault();
            if (mi != null) ZSelector.SelectedItem = mi;
        }

        private void SelectW(string wName)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)WSelector.ItemsSource where (x).Field == wName select x)
                    .FirstOrDefault();
            if (mi != null) WSelector.SelectedItem = mi;
        }

        private void SelectV(string vName)
        {
            MarkedItem mi =
                (from x in (IEnumerable<MarkedItem>)VSelector.ItemsSource where (x).Field == vName select x)
                    .FirstOrDefault();
            if (mi != null) VSelector.SelectedItem = mi;
        }

        public void SelectAll(string x, List<string> y, string z, string w, string v)
        {
            _processingDisabled = true;
            XField = x;
            YFields = y;
            ZField = z;
            WField = w;
            VField = v;

            if (x != null) SelectX(x);
            if (y != null && y.Count>0) SelectY( y );
            if (z != null) SelectZ(z);
            if (w != null) SelectW(w);
            if (v != null) SelectV(v);
            _processingDisabled = false; 
            FieldsChanged.Invoke(this,
            new FieldsChangedEventArgs
            {
                XField = x,
                YFields = y,
                ZField = z,
                WField = w,
                VField = v,
                ChangedField = ChangedFieldType.Renew,
                MultipleSelection = _multipleYMode
            });
        }

        public void SetFieldsHeaders(string xFieldHeader, string yFieldHeader, string zFieldHeader, string wFieldHeader, string vFieldHeader)
        {
            XParameterSelectorPanel.Visibility = xFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            FlipVariablesXYButton.Visibility = xFieldHeader != null && yFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            FlipVariablesXZButton.Visibility = xFieldHeader != null && zFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            FlipVariablesYZButton.Visibility = yFieldHeader != null && zFieldHeader != null &&  xFieldHeader == null ? Visibility.Visible : Visibility.Collapsed;
            YParameterSelectorPanel.Visibility = yFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            ZSelectorPanel.Visibility = zFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            WSelectorPanel.Visibility = wFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            VSelectorPanel.Visibility = vFieldHeader != null ? Visibility.Visible : Visibility.Collapsed;
            CallLayerCommandX.Visibility = XParameterSelectorPanel.Visibility;
            CallLayerCommandY.Visibility = YParameterSelectorPanel.Visibility;
            CallLayerCommandY.IsEnabled = !_multipleYMode;
            CallLayerCommandZ.Visibility = ZSelectorPanel.Visibility;
            XParameterSelectorPanel.Header = xFieldHeader;
            YParameterSelectorPanel.Header = yFieldHeader;
            ZSelectorPanel.Header = zFieldHeader;
            WSelectorPanel.Header = wFieldHeader;
            VSelectorPanel.Header = vFieldHeader;

            XRowDefinition.Height = new GridLength(1, xFieldHeader != null ? GridUnitType.Star : GridUnitType.Auto);
            YRowDefinition.Height = new GridLength(1, yFieldHeader != null ? GridUnitType.Star : GridUnitType.Auto);
        }

        public void SetFilters(SelectorDataTypeFiltration xFiltration, SelectorDataTypeFiltration yFiltration, SelectorDataTypeFiltration zFiltration, SelectorDataTypeFiltration wFiltration, SelectorDataTypeFiltration vFiltration, ReportChartType RCT)
        {
            rct = RCT;
            XFilterMode = xFiltration;
            YFilterMode = yFiltration;
            ZFilterMode = zFiltration;
            WFilterMode = wFiltration;
            VFilterMode = vFiltration;
            UpdateSelectorSources();
        }

        private void Selector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_processingDisabled) return;

            var selector = sender as Selector;
            var cft = ChangedFieldType.X;
            if ( _processingDisabled) return;
            switch (selector.Name)
            {
                case "XSelector":
                    XField = ((MarkedItem)selector.SelectedItem).Field;
                    cft = ChangedFieldType.X;
                    break;
                case "YSelector":
                    YFields = YSelector.SelectedItems.OfType<MarkedItem>().Select(a => a.Field).ToList();
                    cft = ChangedFieldType.Y;
                    break;
                case "ZSelector":
                    ZField = ((MarkedItem)selector.SelectedItem).Field;
                    cft = ChangedFieldType.Z;
                    break;
                case "WSelector":
                    WField = ((MarkedItem)selector.SelectedItem).Field;
                    cft = ChangedFieldType.W;
                    break;
                case "VSelector":
                    VField = ((MarkedItem)selector.SelectedItem).Field;
                    cft = ChangedFieldType.V;
                    break;
            }

            InvokeFieldsChangeEvent(selector, cft);
        }

        private void InvokeFieldsChangeEvent(Selector selector, ChangedFieldType cft)
        {
            
            if (selector==null)
                FieldsChanged.Invoke(this,
                        new FieldsChangedEventArgs
                        {
                            XField = XField,
                            YFields = YFields,
                            ZField = ZField,
                            WField = WField,
                            VField = VField,
                            ChangedField = cft,
                            MultipleSelection = _multipleYMode
                        });
            else
            if (FieldsChanged != null && selector.SelectedIndex >= 0 && selector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                    new FieldsChangedEventArgs
                    {
                        XField = XField,
                        YFields = YFields,
                        ZField = ZField,
                        WField = WField,
                        VField = VField,
                        ChangedField = cft,
                        MultipleSelection = _multipleYMode
                    });
        }

        

        private void FlipVariablesXYButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selected1 = ((MarkedItem)YSelector.SelectedItem).Field;
            var selected2 = ((MarkedItem)XSelector.SelectedItem).Field;
            if (selected1 == selected2) return;
            _processingDisabled = true;
            var toSelect1 = ((IEnumerable<MarkedItem>)XSelector.ItemsSource).Where(a => a.Field == selected1).SingleOrDefault();
            var toSelect2 = ((IEnumerable<MarkedItem>)YSelector.ItemsSource).Where(a => a.Field == selected2).SingleOrDefault();
            if (toSelect1 != null && toSelect2 != null)
            { 
                XSelector.SelectedItem = toSelect1;
                YSelector.SelectedItem = toSelect2;
                YFields = new List<string> { ((MarkedItem)YSelector.SelectedItem).Field };
                XField = ((MarkedItem)XSelector.SelectedItem).Field;
                UpdateLayout();
            }
            _processingDisabled = false;

            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.Flip,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        private void FlipVariablesYZButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selected1 = ((MarkedItem)YSelector.SelectedItem).Field;
            var selected2 = ((MarkedItem)ZSelector.SelectedItem).Field;
            if (selected1 == selected2) return;
            _processingDisabled = true;
            var toSelect1 = ((IEnumerable<MarkedItem>)ZSelector.ItemsSource).Where(a => a.Field == selected1).SingleOrDefault();
            var toSelect2 = ((IEnumerable<MarkedItem>)YSelector.ItemsSource).Where(a => a.Field == selected2).SingleOrDefault();
            if (toSelect1 != null && toSelect2 != null)
            {
                ZSelector.SelectedItem = toSelect1;
                YSelector.SelectedItem = toSelect2;
                YFields = new List<string> { ((MarkedItem)YSelector.SelectedItem).Field };
                ZField = ((MarkedItem)ZSelector.SelectedItem).Field;
                UpdateLayout();
            }
            _processingDisabled = false;

            if (FieldsChanged != null && ZSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.Flip,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        private void FlipVariablesXZButton_OnClick(object sender, RoutedEventArgs e)
        {
            var selected1 = ((MarkedItem)ZSelector.SelectedItem).Field;
            var selected2 = ((MarkedItem)XSelector.SelectedItem).Field;
            if (selected1 == selected2) return;
            _processingDisabled = true;
            var toSelect1 = ((IEnumerable<MarkedItem>)XSelector.ItemsSource).Where(a => a.Field == selected1).SingleOrDefault();
            var toSelect2 = ((IEnumerable<MarkedItem>)ZSelector.ItemsSource).Where(a => a.Field == selected2).SingleOrDefault();
            if (toSelect1 != null && toSelect2 != null)
            {
                XSelector.SelectedItem = toSelect1;
                ZSelector.SelectedItem = toSelect2;
                ZField = ((MarkedItem)ZSelector.SelectedItem).Field;
                XField = ((MarkedItem)XSelector.SelectedItem).Field;
                UpdateLayout();
            }
            _processingDisabled = false;

            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && ZSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.Flip,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        public void RenameFields(List<ReportRenameOperation> renameOperations)
        {
            _processingDisabled = true;
            foreach (var ro in renameOperations)
            {
                var x = XSelector.ItemsSource.OfType<MarkedItem>().Where(a => a.Field == ro.OldName);
                var y = YSelector.ItemsSource.OfType<MarkedItem>().Where(a => a.Field == ro.OldName);
                var z = ZSelector.ItemsSource.OfType<MarkedItem>().Where(a => a.Field == ro.OldName);
                var v = VSelector.ItemsSource.OfType<MarkedItem>().Where(a => a.Field == ro.OldName);
                var w = WSelector.ItemsSource.OfType<MarkedItem>().Where(a => a.Field == ro.OldName);
                foreach (var Xitem in x) Xitem.Field = ro.NewName;
                foreach (var Yitem in y) Yitem.Field = ro.NewName;
                foreach (var Zitem in z) Zitem.Field = ro.NewName;
                foreach (var Vitem in v) Vitem.Field = ro.NewName;
                foreach (var Witem in w) Witem.Field = ro.NewName;
                
                if (XSelector.SelectedItem != null && ((MarkedItem)XSelector.SelectedItem).Field == ro.NewName)
                {
                    XField = ro.NewName;
                }
                if (YSelector.SelectedItem != null && (YSelector.SelectedItems.OfType<MarkedItem>().Select(a=>a.Field)).Contains(ro.NewName))
                {
                    if (YFields.Contains(ro.OldName))
                    {
                        var i = YFields.IndexOf(ro.OldName);
                        YFields.Remove(ro.OldName);
                        YFields.Insert(i, ro.NewName);
                    }
                }
                if (ZSelector.SelectedItem != null && ((MarkedItem)ZSelector.SelectedItem).Field == ro.NewName)
                {
                    ZField = ro.NewName;
                }
                if (WSelector.SelectedItem != null && ((MarkedItem)WSelector.SelectedItem).Field == ro.NewName)
                {
                    WField = ro.NewName;
                }
                if (VSelector.SelectedItem != null && ((MarkedItem)VSelector.SelectedItem).Field == ro.NewName)
                {
                    VField = ro.NewName;
                }
            }
            _processingDisabled = false;
            InvokeFieldsChangeEvent(null, ChangedFieldType.Rename);
        }

        public void EnableMultipleSelection(bool enable, bool defaultIsMultiple)
        {
            if (enable)
            {
                SelectionModePanel.Visibility = Visibility.Visible;
                if (defaultIsMultiple) SetMultipleSelection();
            }
            else
            {
                SelectionModePanel.Visibility = Visibility.Collapsed;
                SetSingleSelection();
            }
        }

        private void SingleSelection_Click(object sender, RoutedEventArgs e)
        {
            SetSingleSelection();
            UpdateSelectorSources();
            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.SelectionMode,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        private void SetSingleSelection()
        {
            _multipleYMode = false;
            ResetSelection.Visibility = Visibility.Collapsed;
            SelectAllButton.Visibility = Visibility.Collapsed;
            YSelector.SelectionMode = SelectionMode.Single;
            CallLayerCommandY.IsEnabled = true;
            SingleSelection.IsChecked = true;
        }

        private void MultipleSelection_Click(object sender, RoutedEventArgs e)
        {
            SetMultipleSelection();
            UpdateSelectorSources();
            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.SelectionMode,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        private void CallLayerCommand_OnClick(object sender, RoutedEventArgs e)
        {
            var s = sender as TextImageButtonBase;
            var gt = s.TransformToVisual(this);
            var offset = gt.Transform(new Point(0, 0));

            switch (s.Tag.ToString())
            {
                case "X":
                    LayerCommand?.Invoke(this, new LayerCommandEventArgs { Field = XField, ClickPoint = offset });
                    break;
                case "Y":
                    LayerCommand?.Invoke(this, new LayerCommandEventArgs { Field = YFields.FirstOrDefault(), ClickPoint = offset });
                    break;
                case "Z":
                    LayerCommand?.Invoke(this, new LayerCommandEventArgs { Field = ZField, ClickPoint = offset });
                    break;
            }

        }

        private void SetMultipleSelection()
        {
            _multipleYMode = true;
            ResetSelection.Visibility = Visibility.Visible;
            SelectAllButton.Visibility = Visibility.Visible;
            YSelector.SelectionMode = SelectionMode.Multiple;
            CallLayerCommandY.IsEnabled = false;
            MultipleSelection.IsChecked = true;
        }

        private void ResetSelection_Click(object sender, RoutedEventArgs e)
        {
            if (YFields.Count == 1) return;
            YFields = YFields.Take(1).ToList();
            UpdateSelectorSources();
            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.Y,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            var fields = ((IEnumerable<MarkedItem>)XSelector.ItemsSource).Select(a => a.Field).ToList();
            var toSelect = (XParameterSelectorPanel.Visibility == Visibility.Collapsed) ? fields : fields.Except(new[] {XField}).ToList();
            YFields = toSelect;
            UpdateSelectorSources();
            if (FieldsChanged != null && XSelector.SelectedIndex >= 0 && YSelector.SelectedItem != null)
                FieldsChanged.Invoke(this,
                                     new FieldsChangedEventArgs
                                     {
                                         XField = XField,
                                         YFields = YFields,
                                         ZField = ZField,
                                         WField = WField,
                                         VField = VField,
                                         ChangedField = ChangedFieldType.Y,
                                         MultipleSelection = _multipleYMode
                                     });
        }

        public List<string> GetUsedFields()
        {
            var res = new List<string>();
            if (XField != null && XParameterSelectorPanel.Visibility == Visibility.Visible) res.Add(XField);
            if (YFields != null && YParameterSelectorPanel.Visibility == Visibility.Visible) res.AddRange(YFields);
            if (ZField != null && ZSelectorPanel.Visibility == Visibility.Visible) res.Add(ZField);
            if (WField != null && WSelectorPanel.Visibility == Visibility.Visible) res.Add(WField);
            if (VField != null && VSelectorPanel.Visibility == Visibility.Visible) res.Add(VField);

            return res;
        }
    }

    public class FieldsChangedEventArgs : EventArgs
    {
        public string XField { get; set; }
        public List<string> YFields { get; set; }
        public string ZField { get; set; }
        public string WField { get; set; }
        public string VField { get; set; }
        public bool MultipleSelection { get; set; }
        public ChangedFieldType ChangedField { get; set; }
    }

    public class LayerCommandEventArgs : EventArgs
    {
        public string Field { get; set; }
        public Point ClickPoint;
    }

    public enum ChangedFieldType
    {
        X,
        Y,
        Z,
        W,
        V,
        Flip,
        Renew,
        Rename, 
        SelectionMode
    }

    [Flags]
    public enum SelectorDataTypeFiltration
    {
        AllData = 0x10,
        Double = 0x1,
        DateTime = 0x2,
        String = 0x4,
        AddNone = 0x8
    }
}