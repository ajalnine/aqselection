﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class SmoothnessSelector
    {
        public delegate void SmoothnessChangedDelegate(object sender, SmoothChangedEventArgs e);

        private ReportChartRegressionFitMode _currentSmoothness;
        private ReportChartRangeStyle _currentStyle;
        private ReportChartLayerMode _currentLayerMode;
        private double _LWSArea;
        
        public SmoothnessSelector()
        {
            InitializeComponent();
            SetCurrentSmoothness(ReportChartRegressionFitMode.DynamicSmoothLowess);
            SetCurrentSmoothStyle(ReportChartRangeStyle.Thick);
            SetCurrentLayerMode(ReportChartLayerMode.Single);
            SetCurrentLWSArea(0.8);
        }

        public event SmoothnessChangedDelegate SmoothnessChanged;

        public ReportChartRegressionFitMode GetCurrentSmoothness()
        {
            return _currentSmoothness;
        }

        public double GetCurrentLWSArea()
        {
            return _LWSArea;
        }

        public ReportChartRangeStyle GetCurrentSmoothStyle()
        {
            return _currentStyle;
        }

        public ReportChartLayerMode GetCurrentLayerMode()
        {
            return _currentLayerMode;
        }

        public void SetCurrentSmoothness(ReportChartRegressionFitMode rcdsm)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcdsm.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentSmoothness = rcdsm;
            SetLWSAreaVisibility();
        }

        public void RefreshUI()
        {
            SetCurrentSmoothness(_currentSmoothness);
            SetCurrentSmoothStyle(_currentStyle);
            SetCurrentLayerMode(_currentLayerMode);
            SetLWSAreaVisibility();
        }

        public void SetLayerStyleSelectorVisibility(bool visible)
        {
            Single.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
            Multiple.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
        }

        private void DynamicParametersButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentSmoothness =
                (ReportChartRegressionFitMode) Enum.Parse(typeof (ReportChartRegressionFitMode), rb.Name, true);
            if (SmoothnessChanged != null)
                SmoothnessChanged.Invoke(this, new SmoothChangedEventArgs { SmoothMode = _currentSmoothness, SmoothStyle = _currentStyle, OnlyStyleChanged = false, LWSArea = _LWSArea});
            SetLWSAreaVisibility();
        }

        public void SetCurrentSmoothStyle(ReportChartRangeStyle style)
        {
            _currentStyle = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            SetLWSAreaVisibility();
        }

        public void SetExtendedModesVisibility(bool visible)
        {
            var vis = visible ? Visibility.Visible : Visibility.Collapsed;
            TableOnly.Visibility = vis;
            Extended.Visibility = vis;
        }

        public void SetCurrentLWSArea(double area)
        {
            _LWSArea = area;
            LWSAreaSlider.Value = area;
        }

        public void SetLWSAreaVisibility()
        {
            LWSAreaSliderContainer.Visibility = (_currentSmoothness == ReportChartRegressionFitMode.DynamicSmoothLowess)
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        public void SetCurrentLayerMode(ReportChartLayerMode style)
        {
            _currentLayerMode = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void Style_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentStyle = (ReportChartRangeStyle)Enum.Parse(typeof(ReportChartRangeStyle), rb.Name, true);
            if (SmoothnessChanged != null)
                SmoothnessChanged.Invoke(this, new SmoothChangedEventArgs { SmoothMode = _currentSmoothness, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, OnlyStyleChanged = true, LWSArea = _LWSArea });

        }

        private void Layer_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentLayerMode = (ReportChartLayerMode)Enum.Parse(typeof(ReportChartLayerMode), rb.Name, true);
            if (SmoothnessChanged != null)
                SmoothnessChanged.Invoke(this, new SmoothChangedEventArgs { SmoothMode = _currentSmoothness, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, OnlyStyleChanged = false, LWSArea = _LWSArea });
        }

        private void LWSAreaSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (LWSAreaSlider == null) return;
            _LWSArea = LWSAreaSlider.Value;
            if (SmoothnessChanged != null)
                SmoothnessChanged.Invoke(this, new SmoothChangedEventArgs { SmoothMode = _currentSmoothness, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, OnlyStyleChanged = false, LWSArea = _LWSArea });
        }
    }

    public class SmoothChangedEventArgs : EventArgs
    {
        public ReportChartRegressionFitMode SmoothMode { get; set; }
        public ReportChartRangeStyle SmoothStyle { get; set; }
        public ReportChartLayerMode LayersMode { get; set; }
        public bool OnlyStyleChanged { get; set; }
        public double LWSArea { get; set; }
    }
}