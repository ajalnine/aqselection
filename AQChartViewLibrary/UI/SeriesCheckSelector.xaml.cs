﻿using System;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class SeriesCheckSelector
    {
        public delegate void SeriesCheckModeChangedDelegate(object sender, SeriesCheckChangedEventArgs e);

        private ReportChartSeriesType _currentChartSeriesType;

        public SeriesCheckSelector()
        {
            InitializeComponent();
            LoadImages();
            SetCurrentChartSeriesType(ReportChartSeriesType.None);
        }

        public event SeriesCheckModeChangedDelegate SeriesCheckChanged;

        public ReportChartSeriesType GetCurrentChartSeriesType()
        {
            return _currentChartSeriesType;
        }

        private void LoadImages()
        {
            ImageSeries1.Source =
                new BitmapImage {UriSource = new Uri("/AQResources;component/Images/Series/1.png", UriKind.Relative) };
            ImageSeries2.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/2.png", UriKind.Relative) };
            ImageSeries3.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/3.png", UriKind.Relative) };
            ImageSeries4.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/4.png", UriKind.Relative) };
            ImageSeries5.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/5.png", UriKind.Relative) };
            ImageSeries6.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/6.png", UriKind.Relative) };
            ImageSeries7.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/7.png", UriKind.Relative) };
            ImageSeries8.Source =
                new BitmapImage { UriSource = new Uri("/AQResources;component/Images/Series/8.png", UriKind.Relative) };
        }

        public void SetCurrentChartSeriesType(ReportChartSeriesType rcst)
        {
            SeriesButton1.IsChecked = (rcst & ReportChartSeriesType.Series1) > 0;
            SeriesButton2.IsChecked = (rcst & ReportChartSeriesType.Series2) > 0;
            SeriesButton3.IsChecked = (rcst & ReportChartSeriesType.Series3) > 0;
            SeriesButton4.IsChecked = (rcst & ReportChartSeriesType.Series4) > 0;
            SeriesButton5.IsChecked = (rcst & ReportChartSeriesType.Series5) > 0;
            SeriesButton6.IsChecked = (rcst & ReportChartSeriesType.Series6) > 0;
            SeriesButton7.IsChecked = (rcst & ReportChartSeriesType.Series7) > 0;
            SeriesButton8.IsChecked = (rcst & ReportChartSeriesType.Series8) > 0;
            _currentChartSeriesType = rcst;
        }

        public void RefreshUI()
        {
            SetCurrentChartSeriesType(_currentChartSeriesType);
        }

        private void SeriesChanged()
        {
            var CurrentSeriesType = ReportChartSeriesType.None;
            if (SeriesButton1.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series1;
            if (SeriesButton2.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series2;
            if (SeriesButton3.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series3;
            if (SeriesButton4.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series4;
            if (SeriesButton5.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series5;
            if (SeriesButton6.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series6;
            if (SeriesButton7.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series7;
            if (SeriesButton8.IsChecked.Value) CurrentSeriesType |= ReportChartSeriesType.Series8;
            _currentChartSeriesType = CurrentSeriesType;
            if (SeriesCheckChanged != null)
                SeriesCheckChanged.Invoke(this, new SeriesCheckChangedEventArgs {SeriesType = _currentChartSeriesType});
        }

        private void SeriesButton_Click(object sender, RoutedEventArgs e)
        {
            SeriesChanged();
        }

        private void SeriesNone_Click(object sender, RoutedEventArgs e)
        {
            ResetSeriesSelection(false);
            SeriesChanged();
        }

        private void SeriesAll_Click(object sender, RoutedEventArgs e)
        {
            ResetSeriesSelection(true);
            SeriesChanged();
        }

        private void ResetSeriesSelection(bool State)
        {
            foreach (UIElement c in ButtonPanel.Children)
            {
                if (c is ToggleButton) ((ToggleButton) c).IsChecked = State;
            }
        }
    }

    public class SeriesCheckChangedEventArgs : EventArgs
    {
        public ReportChartSeriesType SeriesType { get; set; }
    }
}