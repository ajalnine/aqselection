﻿using System;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using System.Windows.Controls;
using System.Linq;
using AQBasicControlsLibrary;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class FitSelector
    {
        public delegate void FitChangedDelegate(object sender, FitChangedEventArgs e);

        private ReportChartFitMode _currentFit;
        private ReportChartRangeStyle _currentFitStyle;

        public FitSelector()
        {
            InitializeComponent();
            SetCurrentFit(ReportChartFitMode.FitGauss);
            SetCurrentFitStyle(ReportChartRangeStyle.Thin);
        }

        public event FitChangedDelegate FitChanged;

        public ReportChartFitMode GetCurrentFit()
        {
            return _currentFit;
        }

        public ReportChartRangeStyle GetCurrentFitStyle()
        {
            return _currentFitStyle;
        }

        public void SetCurrentFit(ReportChartFitMode rcfm)
        {
            FitGauss.IsChecked = (rcfm & ReportChartFitMode.FitGauss) > 0;
            FitGaussEx.IsChecked = (rcfm & ReportChartFitMode.FitGaussEx) > 0;
            FitKDE.IsChecked = (rcfm & ReportChartFitMode.FitKDE) > 0;
            FitKDESilverman.IsChecked = (rcfm & ReportChartFitMode.FitKDESilverman) > 0;
            FitWeibull.IsChecked = (rcfm & ReportChartFitMode.FitWeibull) > 0;
            FitBeeSwarm.IsChecked = (rcfm & ReportChartFitMode.FitBeeSwarm) > 0;
            _currentFit = rcfm;
        }

        public void SetCurrentFitStyle(ReportChartRangeStyle style)
        {
            _currentFitStyle = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            Filled.IsChecked = (style & ReportChartRangeStyle.Filled) > 0;
        }

        public void RefreshUI()
        {
            SetCurrentFit(_currentFit);
            SetCurrentFitStyle(_currentFitStyle);
        }

        private void FitButton_Click(object sender, RoutedEventArgs e)
        {
            _currentFit = ReportChartFitMode.FitNone;
            if (FitGauss.IsChecked != null && FitGauss.IsChecked.Value) _currentFit |= ReportChartFitMode.FitGauss;
            if (FitGaussEx.IsChecked != null && FitGaussEx.IsChecked.Value) _currentFit |= ReportChartFitMode.FitGaussEx;
            if (FitKDE.IsChecked != null && FitKDE.IsChecked.Value) _currentFit |= ReportChartFitMode.FitKDE;
            if (FitKDESilverman.IsChecked != null && FitKDESilverman.IsChecked.Value) _currentFit |= ReportChartFitMode.FitKDESilverman;
            if (FitWeibull.IsChecked != null && FitWeibull.IsChecked.Value) _currentFit |= ReportChartFitMode.FitWeibull;
            if (FitBeeSwarm.IsChecked != null && FitBeeSwarm.IsChecked.Value) _currentFit |= ReportChartFitMode.FitBeeSwarm;
            if (FitChanged != null) FitChanged.Invoke(this, new FitChangedEventArgs { FitMode = _currentFit, FitStyle = _currentFitStyle, OnlyStyleChanged = false });
        }

        private void FitStyle_Click(object sender, RoutedEventArgs e)
        {
            var rb = sender as TextImageRadioButtonBase;
            _currentFitStyle = ReportChartRangeStyle.None;
            if (Invisible.IsChecked.Value) _currentFitStyle = ReportChartRangeStyle.Invisible;
            if (Thick.IsChecked.Value) _currentFitStyle = ReportChartRangeStyle.Thick;
            if (Thin.IsChecked.Value) _currentFitStyle = ReportChartRangeStyle.Thin;
            if (Filled.IsChecked.Value) _currentFitStyle |= ReportChartRangeStyle.Filled;
            if (FitChanged != null) FitChanged.Invoke(this, new FitChangedEventArgs { FitMode = _currentFit, FitStyle = _currentFitStyle, OnlyStyleChanged = true });
        }
    }

    public class FitChangedEventArgs : EventArgs
    {
        public ReportChartFitMode FitMode { get; set; }
        public ReportChartRangeStyle FitStyle { get; set; }
        public bool OnlyStyleChanged { get; set; }
    }
}