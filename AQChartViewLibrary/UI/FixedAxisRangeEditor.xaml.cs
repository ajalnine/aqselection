﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using AQChartLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class FixedAxisRangeEditor : UserControl
    {
        private FixedAxis _processingAxis;
        private FixedAxises _fixedAxises;
        public event FixedAxisChangedDelegate FixedAxisChanged;
        private bool dateMode, numberMode;
        public FixedAxisRangeEditor()
        {
            InitializeComponent();
        }

        public void SetFixedAxis(string axisTag, FixedAxis axis, FixedAxises fixedAxises)
        {
            dateMode = (axis.AxisMinDate.HasValue && !axis.AxisMin.HasValue) ||
                       (axis.AxisMaxDate.HasValue && !axis.AxisMax.HasValue);
            numberMode = (axis.AxisMin.HasValue && !axis.AxisMinDate.HasValue) ||
                             (!axis.AxisMaxDate.HasValue && axis.AxisMax.HasValue);
            if (dateMode)
            {
                AxisMinTextBox.Visibility = Visibility.Collapsed;
                AxisMaxTextBox.Visibility = Visibility.Collapsed;
                StepRow.Height = new GridLength(0);
                AxisMinDatePicker.Visibility = Visibility.Visible;
                AxisMaxDatePicker.Visibility = Visibility.Visible;
            }
            else if (numberMode)
            {
                AxisMinTextBox.Visibility = Visibility.Visible;
                AxisMaxTextBox.Visibility = Visibility.Visible;
                StepRow.Height = new GridLength(1, GridUnitType.Star);
                AxisMinDatePicker.Visibility = Visibility.Collapsed;
                AxisMaxDatePicker.Visibility = Visibility.Collapsed;
                FormatRow.Height = new GridLength(0);
            }
            else
            {
                AxisMinTextBox.Visibility = Visibility.Collapsed;
                AxisMaxTextBox.Visibility = Visibility.Collapsed;
                StepRow.Height = new GridLength(0);
                MinRow.Height = new GridLength(0);
                MaxRow.Height = new GridLength(0);
                var dt = axis.DataType?.Split('.').Last().ToLower();
                if (dt!="datetime" && dt!="timespan")FormatRow.Height = new GridLength(0);
                AxisMinDatePicker.Visibility = Visibility.Collapsed;
                AxisMaxDatePicker.Visibility = Visibility.Collapsed;
            }

            if (!axis.StringIsDateTime && !dateMode)
            {
                FormatRow.Height = new GridLength(0);
            }

            _fixedAxises = fixedAxises;
            Layout.Tag = axisTag;
            AxisName.Text = $"Параметры оси {axisTag}:";
            Layout.DataContext = axis;
            DiscreteSelector.Height = axis.Axis == "Z" ? new GridLength(1, GridUnitType.Star) : new GridLength(0);
            _processingAxis = axis;
        }
        private void Fixed_OnBindingValidationError(object sender, ValidationErrorEventArgs e)
        {
            var fixedAxis = _fixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == Layout.Tag.ToString());
            if (fixedAxis != null && (dateMode || numberMode)) fixedAxis.IsValid = false;
            FixedAxisChanged?.Invoke(this, new FixedAxisChangedEventArgs { Finished = false, NewFixedAxises = _fixedAxises });
        }
        private void ResetFixationButton_OnClick(object sender, RoutedEventArgs e)
        {
            _fixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            FixedAxisChanged?.Invoke(this, new FixedAxisChangedEventArgs { Finished = true, NewFixedAxises = _fixedAxises});
        }

        private void FixedAxisOK_OnClick(object sender, RoutedEventArgs e)
        {
            AxisMinTextBox?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            AxisMaxTextBox?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            AxisMinDatePicker?.GetBindingExpression(DatePicker.SelectedDateProperty)?.UpdateSource();
            AxisMaxDatePicker?.GetBindingExpression(DatePicker.SelectedDateProperty)?.UpdateSource();
            ContinuousAxisButton?.GetBindingExpression(ToggleButton.IsCheckedProperty)?.UpdateSource();
            StepTextBox?.GetBindingExpression(TextBox.TextProperty)?.UpdateSource();
            var fixedAxis = _fixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == Layout.Tag.ToString());
            if (fixedAxis != null && fixedAxis.IsValid)
            {
                FixedAxisChanged?.Invoke(this, new FixedAxisChangedEventArgs { Finished = true, NewFixedAxises = _fixedAxises });
            }
        }

        #region DateTimePicker error overwork
        private void FixedTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var t = sender as TextBox;
            if (t != null && (!t.Text.EndsWith(".") && t.Text != "0" && !t.Text.EndsWith(",")))
            {
                var be = t.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
            }
        }

        private void FromDatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                AxisMinDatePicker.UpdateLayout();
                DependencyObject a = VisualTreeHelper.GetChild(AxisMinDatePicker, 0);
                DependencyObject b = VisualTreeHelper.GetChild(a, 0);
                var ft = b as DatePickerTextBox;
                if (ft == null) return;
                ft.KeyUp += ft_KeyUp;
                ft.GotFocus += ft_GotFocus;
                ft.LostFocus += ft_LostFocus;
            }
            catch
            {
            }
        }

        private bool _fromTyping;
        private bool _toTyping;

        private void ft_GotFocus(object sender, RoutedEventArgs e)
        {
            _fromTyping = true;
        }

        private void ft_LostFocus(object sender, RoutedEventArgs e)
        {
            _fromTyping = false;
        }

        private void ft_KeyUp(object sender, KeyEventArgs e)
        {
            var d = sender as DatePickerTextBox;
            if (d == null) return;
            d.UpdateLayout();
            var s = d.Text;
            DateTime f;
            if (!DateTime.TryParse(s, out f)) return;
            _processingAxis.AxisMinDate = f;
        }

        private void ToDatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {


                AxisMaxDatePicker.UpdateLayout();
                var a = VisualTreeHelper.GetChild(AxisMaxDatePicker, 0);
                var b = VisualTreeHelper.GetChild(a, 0);
                var tt = b as DatePickerTextBox;
                if (tt == null) return;
                tt.KeyUp += tt_KeyUp;
                tt.GotFocus += tt_GotFocus;
                tt.LostFocus += tt_LostFocus;
            }
            catch
            {
            }
        }

        private void tt_GotFocus(object sender, RoutedEventArgs e)
        {
            _toTyping = true;
        }

        private void tt_LostFocus(object sender, RoutedEventArgs e)
        {
            _toTyping = false;
        }

        private void tt_KeyUp(object sender, KeyEventArgs e)
        {
            var d = sender as DatePickerTextBox;
            if (d == null) return;
            d.UpdateLayout();
            var s = d.Text;
            DateTime T;
            if (!DateTime.TryParse(s, out T)) return;
            _processingAxis.AxisMaxDate = T;
        }
        #endregion

        private void DateFormatToolTip_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!(sender is TextBlock)) return;
            ((TextBlock)sender).Text =
                @"
d - день месяца
dd - день месяца с дополнением 0
ddd - сокращенное название дня недели
dddd - полное название дня недели
MM - номер месяца с дополнением 0
MMM - краткое название месяца (янв)
MMMM - полное название месяца (январь)
yy - двузначный год
yyyy - четырехзначный год

HH - часы в 24 часовом формате
hh - часы в 12 часовом формате
mm - минуты
ss - секунды
ffff - миллисекунды

Примеры форматирования даты 01.01.2020 10:20:30

dd.MM.yyyy - 01.01.2020
dd MMMM yyyyг - 01 января 2020г
d MM (dddd) - 1 января (среда)
hh:mm:ss - 10:20:30
";
        }
    }

    public delegate void FixedAxisChangedDelegate(object o, FixedAxisChangedEventArgs e);

    public class FixedAxisChangedEventArgs
    {
        public FixedAxises NewFixedAxises;
        public bool Finished;
    }
}
