﻿using System;
using System.Globalization;
using System.Windows;
using AQReportingLibrary;
using System.Windows.Media;
using System.IO.IsolatedStorage;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class LayoutModeSelector
    {
        public delegate void LayoutModeChangedDelegate(object sender, LayoutModeChangedEventArgs e);

        private ReportChartLayoutMode _currentLayoutMode;
        private ChartLegend _currentChartLegend = ChartLegend.Right;
        private double _fontSize = 1;
        private int _limitedLayersNumber = 10;
        public Size LayoutSize = new Size(700, 360);

        public LayoutModeSelector()
        {
            InitializeComponent();
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettings"))
            {
                _currentLayoutMode = ReportChartLayoutMode.ShowGrid
                                     | ReportChartLayoutMode.ShowLegend
                                     | ReportChartLayoutMode.ShowX
                                     | ReportChartLayoutMode.ShowY
                                     | ReportChartLayoutMode.Normal
                                     | ReportChartLayoutMode.ShowTitle
                                     | ReportChartLayoutMode.ShowSubtitle
                                     | ReportChartLayoutMode.Stretched
                                     | ReportChartLayoutMode.OrderLegend
                                     | ReportChartLayoutMode.FilterDescription
                                     | ReportChartLayoutMode.GroupByFormatted;
            }
            else
                _currentLayoutMode =
                    (ReportChartLayoutMode) IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettings"];

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsLegend"))SetCurrentChartLegend(ChartLegend.Right);
            else SetCurrentChartLegend((ChartLegend) IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsLegend"]);

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsLL")) SetCurrentLimitedLayers(10);
            else SetCurrentLimitedLayers((int)IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsLL"]);

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsFontSize")) SetCurrentFontSize(1.0d);
            else SetCurrentFontSize((double)IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsFontSize"]);

            RefreshUI();
            SetSize(_currentLayoutMode);
        }

        public event LayoutModeChangedDelegate LayoutModeChanged;

        public ReportChartLayoutMode GetCurrentLayoutMode()
        {
            if ((_currentLayoutMode & ReportChartLayoutMode.Mini)== ReportChartLayoutMode.Mini)
            {
                var toOff = ReportChartLayoutMode.ShowLegend | ReportChartLayoutMode.ShowSubtitle | ReportChartLayoutMode.ShowTitle;
                return _currentLayoutMode & ((ReportChartLayoutMode)(0xffffffff ^ (int)toOff));
            }
            else return _currentLayoutMode;
        }

        public double GetCurrentFontSize()
        {
            return _fontSize;
        }

        public ChartLegend GetCurrentChartLegend()
        {
            return _currentChartLegend;
        }

        public void SetCurrentChartLegend(ChartLegend cl)
        {
            _currentChartLegend = cl;
            LegendRight.IsChecked = ChartLegend.Right == cl;
            LegendTop.IsChecked = ChartLegend.Top == cl;
            LegendBottom.IsChecked = ChartLegend.Bottom == cl;
        }

        public int GetCurrentLimitedLayers()
        {
            return _limitedLayersNumber;
        }

        public Size GetCurrentSize()
        {
            return LayoutSize;
        }

        public void SetCurrentSize(Size s)
        {
            LayoutSize = s;
            WidthTextBox.Text = s.Width.ToString();
            HeightTextBox.Text = s.Height.ToString();
        }

        public void SetCurrentLimitedLayers(int i)
        {
            _limitedLayersNumber = i;
            LimitedLayersTextBox.Text = i.ToString();
        }

        public void SetCurrentFontSize(double fontSize)
        {
            _fontSize = fontSize;
        }

        public void SetCurrentLayoutMode(ReportChartLayoutMode rclm)
        {
            Mini.IsChecked = ((rclm & ReportChartLayoutMode.Mini) == ReportChartLayoutMode.Mini);
            Normal.IsChecked = ((rclm & ReportChartLayoutMode.Normal) == ReportChartLayoutMode.Normal);
            Wide.IsChecked = ((rclm & ReportChartLayoutMode.Wide) == ReportChartLayoutMode.Wide);
            Wider.IsChecked = ((rclm & ReportChartLayoutMode.Wider) == ReportChartLayoutMode.Wider);
            Widest.IsChecked = ((rclm & ReportChartLayoutMode.Widest) == ReportChartLayoutMode.Widest);
            Tall.IsChecked = ((rclm & ReportChartLayoutMode.Tall) == ReportChartLayoutMode.Tall);
            Full.IsChecked = ((rclm & ReportChartLayoutMode.Full) == ReportChartLayoutMode.Full);
            CustomSize.IsChecked = ((rclm & ReportChartLayoutMode.CustomSize) == ReportChartLayoutMode.CustomSize);
            DoubleYAxis.IsChecked = ((rclm & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis);
            Transparent.IsChecked = ((rclm & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent) ;
            UseColorScale.IsChecked = ((rclm & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale);
            Shadowed.IsChecked = ((rclm & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed);
            HD.IsChecked = ((rclm & ReportChartLayoutMode.HD) == ReportChartLayoutMode.HD);
            ShowGrid.IsChecked = ((rclm & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid);
            ShowLegend.IsChecked = ((rclm & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend);
            ShowX.IsChecked = ((rclm & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX);
            ShowY.IsChecked = ((rclm & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY);
            ShowTitle.IsChecked = ((rclm & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle);
            ShowSubTitle.IsChecked = ((rclm & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle);
            Inverted.IsChecked = ((rclm & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted);
            Stretched.IsChecked = ((rclm & ReportChartLayoutMode.Stretched) == ReportChartLayoutMode.Stretched);
            OrderLegend.IsChecked = ((rclm & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend);
            ShowTickMarks.IsChecked = ((rclm & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks);
            ContrastGrid.IsChecked = ((rclm & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid);
            FilterDescription.IsChecked = ((rclm & ReportChartLayoutMode.FilterDescription) == ReportChartLayoutMode.FilterDescription);
            LimitedLayers.IsChecked = ((rclm & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers);
            LimitedLayersPanel.Visibility = ((rclm & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? Visibility.Visible : Visibility.Collapsed;
            GroupByFormatted.IsChecked = ((rclm & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted);

            _currentLayoutMode = rclm;
            
            WidthTextBox.IsReadOnly = !CustomSize.IsChecked.Value;
            HeightTextBox.IsReadOnly = !CustomSize.IsChecked.Value;
            
            StoreSettings();
        }

        private void StoreSettings()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettings"))IsolatedStorageSettings.ApplicationSettings.Add("AnalysisLayoutSettings", _currentLayoutMode);
            else IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettings"] = _currentLayoutMode;
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsLegend"))IsolatedStorageSettings.ApplicationSettings.Add("AnalysisLayoutSettingsLegend", _currentChartLegend);
            else IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsLegend"] = _currentChartLegend;
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsLL")) IsolatedStorageSettings.ApplicationSettings.Add("AnalysisLayoutSettingsLL", _limitedLayersNumber);
            else IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsLL"] = _limitedLayersNumber;
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisLayoutSettingsFontSize")) IsolatedStorageSettings.ApplicationSettings.Add("AnalysisLayoutSettingsFontSize", _fontSize);
            else IsolatedStorageSettings.ApplicationSettings["AnalysisLayoutSettingsFontSize"] = _fontSize;
        }

        public void RefreshUI()
        {
            SetCurrentLayoutMode(_currentLayoutMode);
            _fontSize = 1;
        }

        private void LayoutMode_Click(object sender, RoutedEventArgs e)
        {
            var name =  sender is TextImageToggleButtonBase ? ((TextImageToggleButtonBase) sender).Name : null;
            var shadowed = (Shadowed.IsChecked != null && Shadowed.IsChecked.Value ? ReportChartLayoutMode.Shadowed : 0);
            var inverted = (Inverted.IsChecked != null && Inverted.IsChecked.Value ? ReportChartLayoutMode.Inverted : 0);
            var transparent = (Transparent.IsChecked != null && Transparent.IsChecked.Value ? ReportChartLayoutMode.Transparent : 0);
            var showGrid = (ShowGrid.IsChecked != null && ShowGrid.IsChecked.Value ? ReportChartLayoutMode.ShowGrid : 0);
            var contrastGrid = (ContrastGrid.IsChecked != null && ContrastGrid.IsChecked.Value ? ReportChartLayoutMode.ContrastGrid : 0);
            var showTickMarks = (ShowTickMarks.IsChecked != null && ShowTickMarks.IsChecked.Value ? ReportChartLayoutMode.ShowTickMarks : 0);
            var showLegend = (ShowLegend.IsChecked != null && ShowLegend.IsChecked.Value ? ReportChartLayoutMode.ShowLegend : 0);
            var showX = (ShowX.IsChecked != null && ShowX.IsChecked.Value ? ReportChartLayoutMode.ShowX : 0);
            var showY = (ShowY.IsChecked != null && ShowY.IsChecked.Value ? ReportChartLayoutMode.ShowY : 0);
            var hd = (HD.IsChecked != null && HD.IsChecked.Value ? ReportChartLayoutMode.HD : 0);
            var showTitle = (ShowTitle.IsChecked != null && ShowTitle.IsChecked.Value ? ReportChartLayoutMode.ShowTitle : 0);
            var showSubtitle = (ShowSubTitle.IsChecked != null && ShowSubTitle.IsChecked.Value ? ReportChartLayoutMode.ShowSubtitle : 0);
            var useColorScale = (UseColorScale.IsChecked != null && UseColorScale.IsChecked.Value ? ReportChartLayoutMode.UseColorScale : 0);
            var stretched = (Stretched.IsChecked != null && Stretched.IsChecked.Value ? ReportChartLayoutMode.Stretched : 0);
            var orderLegend = (OrderLegend.IsChecked != null && OrderLegend.IsChecked.Value ? ReportChartLayoutMode.OrderLegend : 0);
            var doubleYAxis = (DoubleYAxis.IsChecked != null && DoubleYAxis.IsChecked.Value ? ReportChartLayoutMode.DoubleYAxis : 0);
            var filterDesc = (FilterDescription.IsChecked != null && FilterDescription.IsChecked.Value ? ReportChartLayoutMode.FilterDescription : 0);
            var limitedLayers = (LimitedLayers.IsChecked != null && LimitedLayers.IsChecked.Value ? ReportChartLayoutMode.LimitedLayers : 0);
            var groupByFormatted = (GroupByFormatted.IsChecked != null && GroupByFormatted.IsChecked.Value ? ReportChartLayoutMode.GroupByFormatted : 0);

            var flags = shadowed | showGrid | showY | showX | showLegend | showTitle | showSubtitle | transparent | useColorScale | inverted | stretched | hd | orderLegend | contrastGrid | showTickMarks | doubleYAxis | filterDesc | limitedLayers | groupByFormatted;
            SetSize(flags);
            LimitedLayersPanel.Visibility = (LimitedLayers.IsChecked != null && LimitedLayers.IsChecked.Value) ? Visibility.Visible : Visibility.Collapsed;

            if (LegendRight.IsChecked != null && LegendRight.IsChecked.Value) _currentChartLegend = ChartLegend.Right;
            else if (LegendTop.IsChecked != null && LegendTop.IsChecked.Value) _currentChartLegend = ChartLegend.Top;
            else if (LegendBottom.IsChecked != null && LegendBottom.IsChecked.Value) _currentChartLegend = ChartLegend.Bottom;

            if (LayoutModeChanged != null)
            {
                StoreSettings();
                LayoutModeChanged.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize,
                    LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1, RequiredRecalc = (name == "LimitedLayers" || name == "GroupByFormatted"),
                    ChartLegend = _currentChartLegend
                });
            }
        }

        private void SetSize(ReportChartLayoutMode flags)
        {
            
            if (Normal.IsChecked != null && Normal.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Normal;
                LayoutSize = new Size(700, 360);
            }

            else if (Wide.IsChecked != null && Wide.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Wide;
                LayoutSize = new Size(1000, 360);
            }

            else if (Tall.IsChecked != null && Tall.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Tall;
                LayoutSize = new Size(700, 520);
            }
            else if (Full.IsChecked != null && Full.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Full;
                LayoutSize = new Size(1000, 520);
            }
            else if (Wider.IsChecked != null && Wider.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Wider;
                LayoutSize = new Size(1000, 270);
            }
            else if (Widest.IsChecked != null && Widest.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Widest;
                LayoutSize = new Size(1000, 220);
            }
            else if (Mini.IsChecked != null && Mini.IsChecked.Value)
            {
                _currentLayoutMode = flags | ReportChartLayoutMode.Mini;
                LayoutSize = new Size(400, 220);
            }
            if (CustomSize.IsChecked != null && CustomSize.IsChecked.Value)
            {
                WidthTextBox.IsReadOnly = false;
                HeightTextBox.IsReadOnly = false;
                if (string.IsNullOrWhiteSpace(WidthTextBox.Text)) WidthTextBox.Text = LayoutSize.Width.ToString(CultureInfo.InvariantCulture);
                if (string.IsNullOrWhiteSpace(HeightTextBox.Text)) HeightTextBox.Text = LayoutSize.Height.ToString(CultureInfo.InvariantCulture);
                _currentLayoutMode = ReportChartLayoutMode.Wide | ReportChartLayoutMode.Tall | flags;
                double userWidth, userHeight;
                var w = double.TryParse(WidthTextBox.Text, out userWidth);
                var h = double.TryParse(HeightTextBox.Text, out userHeight);
                LayoutSize = new Size(w? userWidth : 1000, h? userHeight : 520);
                WidthTextBox.Background = new SolidColorBrush(!w ? Colors.Red : Colors.White);
                HeightTextBox.Background = new SolidColorBrush(!h ? Colors.Red : Colors.White);
            }
            else
            {
                WidthTextBox.IsReadOnly = true;
                HeightTextBox.IsReadOnly = true;
                WidthTextBox.Text = LayoutSize.Width.ToString();
                HeightTextBox.Text = LayoutSize.Height.ToString();
            }
        }

        private void FontSmaller_Click(object sender, RoutedEventArgs e)
        {
            DecrementFontSize();
        }

        public void DecrementFontSize()
        {
            _fontSize = _fontSize >= 0.6 ? _fontSize -= 0.1 : _fontSize;
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1,
                ChartLegend = _currentChartLegend
            });
        }

        private void FontLarger_Click(object sender, RoutedEventArgs e)
        {
            IncrementFontSize();
        }

        public void IncrementFontSize()
        {
            _fontSize = _fontSize <= 2 ? _fontSize += 0.1 : _fontSize;
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1,
                ChartLegend = _currentChartLegend
            });
        }

        private void FontBasic_Click(object sender, RoutedEventArgs e)
        {
            _fontSize = 1;
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1,
                ChartLegend = _currentChartLegend
            });
        }

        private void WidthTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SetSize(_currentLayoutMode);
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1,
                ChartLegend = _currentChartLegend
            });
        }

        private void HeightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            SetSize(_currentLayoutMode);
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1 });
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            _currentLayoutMode = ReportChartLayoutMode.ShowGrid
                | ReportChartLayoutMode.ShowLegend
                | ReportChartLayoutMode.ShowX
                | ReportChartLayoutMode.ShowY
                | ReportChartLayoutMode.Normal
                | ReportChartLayoutMode.ShowTitle
                | ReportChartLayoutMode.ShowSubtitle
                | ReportChartLayoutMode.Stretched
                | ReportChartLayoutMode.OrderLegend
                | ReportChartLayoutMode.FilterDescription
                | ReportChartLayoutMode.GroupByFormatted;
            RefreshUI();
            SetSize(_currentLayoutMode);
            SetCurrentLimitedLayers(10);
            SetCurrentFontSize(1);
            SetCurrentChartLegend(ChartLegend.Right);
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = ((_currentLayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers) ? _limitedLayersNumber : -1,
                ChartLegend = _currentChartLegend, RequiredRecalc = true
            });
        }

        private void LimitedLayersTextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            int i;
            if (int.TryParse(LimitedLayersTextBox.Text, out i))
            {
                LimitedLayersTextBox.Background = new SolidColorBrush(Colors.White);
                SetCurrentLimitedLayers(i);
                StoreSettings();
                LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = _limitedLayersNumber, RequiredRecalc = true,
                    ChartLegend = _currentChartLegend
                });
            }
            else
            {
                LimitedLayersTextBox.Background = new SolidColorBrush(Colors.Red);
            }
        }

        public void IncrementLimitedLayers_Click(object sender, RoutedEventArgs e)
        {
            _limitedLayersNumber = _limitedLayersNumber <100 ? _limitedLayersNumber + 1 : _limitedLayersNumber;
            SetCurrentLimitedLayers(_limitedLayersNumber);
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = _limitedLayersNumber, RequiredRecalc = true,
                ChartLegend = _currentChartLegend
            });
        }

        private void DecrementLimitedLayers_Click(object sender, RoutedEventArgs e)
        {
            _limitedLayersNumber = _limitedLayersNumber > 1 ? _limitedLayersNumber - 1 : _limitedLayersNumber;
            SetCurrentLimitedLayers(_limitedLayersNumber);
            StoreSettings();
            LayoutModeChanged?.Invoke(this, new LayoutModeChangedEventArgs { LayoutMode = _currentLayoutMode, FontSize = _fontSize, LayoutSize = LayoutSize, LimitedLayersNumber = _limitedLayersNumber, RequiredRecalc = true,
                ChartLegend = _currentChartLegend
            });
        }
    }

    public class LayoutModeChangedEventArgs : EventArgs
    {
        public ReportChartLayoutMode LayoutMode { get; set; }
        public double FontSize { get; set; }

        public ChartLegend ChartLegend { get; set; }
        public int LimitedLayersNumber { get; set; }
        public Size LayoutSize { get; set; }

        public bool RequiredRecalc { get; set; }
    }
}