﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQChartLibrary;
using AQBasicControlsLibrary;
using System.IO.IsolatedStorage;
using System.Windows.Shapes;

namespace AQChartViewLibrary.UI
{
    public partial class MarkerSelector
    {
        public delegate void MarkerModeChangedDelegate(object sender, MarkerChangedEventArgs e);

        private ColorScales _currentColorScale;
        private ColorScaleMode _currentColorScaleMode;
        private MarkerModes _currentMarkerMode;
        private MarkerSizeModes _currentMarkerSizeMode;
        private MarkerShapeModes _currentMarkerShapeMode;

        public MarkerSelector()
        {
            InitializeComponent();

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisColorScales")
                || !IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisColorScaleMode")
                || !IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerModes")
                || !IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerSizeModes")
                || !IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerShapeModes")
                )
            {
                ResetMarkerSettings();
            }
            else
            {
                if ((ColorScales)IsolatedStorageSettings.ApplicationSettings["AnalysisColorScales"] == 0) ResetMarkerSettings();
                SetCurrentColorScale((ColorScales)IsolatedStorageSettings.ApplicationSettings["AnalysisColorScales"]);
                SetCurrentColorScaleMode((ColorScaleMode)IsolatedStorageSettings.ApplicationSettings["AnalysisColorScaleMode"]);
                SetCurrentMarkerMode((MarkerModes)IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerModes"]);
                SetCurrentMarkerSizeMode((MarkerSizeModes)IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerSizeModes"]);
                SetCurrentMarkerShapeMode((MarkerShapeModes)IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerShapeModes"]);
            }
        }

        private void StoreSettings()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisColorScales"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisColorScales", _currentColorScale);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisColorScales"] = _currentColorScale;

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisColorScaleMode"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisColorScaleMode", _currentColorScaleMode);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisColorScaleMode"] = _currentColorScaleMode;

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerModes"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisMarkerModes", _currentMarkerMode);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerModes"] = _currentMarkerMode;

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerSizeModes"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisMarkerSizeModes", _currentMarkerSizeMode);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerSizeModes"] = _currentMarkerSizeMode;

            if (!IsolatedStorageSettings.ApplicationSettings.Contains("AnalysisMarkerShapeModes"))
            {
                IsolatedStorageSettings.ApplicationSettings.Add("AnalysisMarkerShapeModes", _currentMarkerShapeMode);
            }
            else IsolatedStorageSettings.ApplicationSettings["AnalysisMarkerShapeModes"] = _currentMarkerShapeMode;
        }

        public void ResetMarkerSettings()
        {
            SetCurrentColorScale(ColorScales.HSV2);
            SetCurrentColorScaleMode(ColorScaleMode.Full);
            SetCurrentMarkerMode(MarkerModes.SemiTransparent);
            SetCurrentMarkerSizeMode(MarkerSizeModes.Medium);
            SetCurrentMarkerShapeMode(MarkerShapeModes.Multishape);
            RefreshUI();
        }

        public event MarkerModeChangedDelegate MarkerModeChanged;

        public ColorScales GetCurrentColorScale()
        {
            return _currentColorScale;
        }

        public ColorScaleMode GetCurrentColorScaleMode()
        {
            return _currentColorScaleMode;
        }

        public MarkerModes GetCurrentMarkerMode()
        {
            return _currentMarkerMode;
        }
        public MarkerSizeModes GetCurrentMarkerSizeMode()
        {
            return _currentMarkerSizeMode;
        }
        public MarkerShapeModes GetCurrentMarkerShapeMode()
        {
            return _currentMarkerShapeMode;
        }

        public void SetCurrentColorScale(ColorScales cs)
        {
            ReversePaletteButton.IsChecked = ((cs & ColorScales.Reversed) == ColorScales.Reversed);
            RadioButton rb = (from r in ScalePanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == ((ColorScales)((int)cs & 0xff)).ToString() && (r as RadioButton).GroupName == "ScaleGroup"
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentColorScale = cs;
        }

        public void SetCurrentColorScaleMode(ColorScaleMode csm)
        {
            SetupColorScaleButton();
            _currentColorScaleMode = csm;
        }

        public void SetCurrentMarkerMode(MarkerModes mm)
        {
            RadioButton rb = (from r in ModePanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == mm.ToString() && (r as RadioButton).GroupName == "MarkerModeGroup"
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentMarkerMode = mm;
        }

        public void SetCurrentMarkerSizeMode(MarkerSizeModes msm)
        {
            RadioButton rb = (from r in SizePanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == msm.ToString() && (r as RadioButton).GroupName == "MarkerSizeGroup"
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentMarkerSizeMode = msm;
        }

        public void SetCurrentMarkerShapeMode(MarkerShapeModes msm)
        {
            RadioButton rb = (from r in ShapePanel.Children
                              where (r is RadioButton) && (r as RadioButton).Name == msm.ToString() && (r as RadioButton).GroupName == "MarkerShapeGroup"
                              select r as RadioButton).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentMarkerShapeMode = msm;
        }

        public void RefreshUI()
        {
            SetCurrentColorScale(_currentColorScale);
            SetCurrentColorScaleMode(_currentColorScaleMode);
            SetCurrentMarkerMode(_currentMarkerMode);
            SetCurrentMarkerSizeMode(_currentMarkerSizeMode);
            SetCurrentMarkerShapeMode(_currentMarkerShapeMode);
            StoreSettings();
        }

        private void MarkerButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton) sender;

            if (rb.GroupName == "ScaleGroup" || rb.GroupName == "PaletteModeGroup")
            {
                if (rb.GroupName == "ScaleGroup")  _currentColorScale = (ColorScales)Enum.Parse(typeof(ColorScales), rb.Name, true);
                if (ReversePaletteButton.IsChecked.Value) _currentColorScale |= ColorScales.Reversed;
                else _currentColorScale &= (ColorScales)((int)ColorScales.Reversed ^ 0xffff);
            }

            if (rb.GroupName == "MarkerModeGroup") _currentMarkerMode = (MarkerModes)Enum.Parse(typeof(MarkerModes), rb.Name, true);
            if (rb.GroupName == "MarkerSizeGroup") _currentMarkerSizeMode = (MarkerSizeModes)Enum.Parse(typeof(MarkerSizeModes), rb.Name, true);
            if (rb.GroupName == "MarkerShapeGroup") _currentMarkerShapeMode = (MarkerShapeModes)Enum.Parse(typeof(MarkerShapeModes), rb.Name, true);
           
            InvokeMarkerModeChanged();
        }

        private void ColorCircleButton_Click(object sender, RoutedEventArgs e)
        {
            var mode = (int) _currentColorScaleMode;
            mode = mode == 3 ? 0 : mode + 1;
            _currentColorScaleMode = (ColorScaleMode) mode;
            SetupColorScaleButton();

            InvokeMarkerModeChanged();
        }

        private void SetupColorScaleButton()
        {
            CircleView.Children.Clear();
            for (var i = 0.0d; i < 1.0d; i += 0.025d)
            {
                var x = 10d + 12.0d * Math.Sin(i * 2.0d * Math.PI);
                var y = 14d + 12.0d * Math.Cos(i * 2.0d * Math.PI);
                var c = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(_currentColorScale, i));
                var o = (i >= ColorConvertor.ColorOffset[_currentColorScaleMode] && i <= ColorConvertor.ColorEnd[_currentColorScaleMode]) ? 0.7d : 0.05d;
                var b = new SolidColorBrush {Color = c, Opacity = o};
                var e = new Ellipse { Fill =  b, Stroke = b, Height = 5, Width = 5};
                Canvas.SetTop(e, 24 -y);
                Canvas.SetLeft(e, x);
                CircleView.Children.Add(e);
            }
        }

        private void InvokeMarkerModeChanged()
        {
            StoreSettings();
            SetupColorScaleButton();
            if (MarkerModeChanged != null)
            MarkerModeChanged.Invoke(this, new MarkerChangedEventArgs
            {
                MarkerSizeMode = _currentMarkerSizeMode,
                ColorScale = _currentColorScale,
                MarkerMode = _currentMarkerMode,
                MarkerShapeMode = _currentMarkerShapeMode,
                ColorScaleMode =  _currentColorScaleMode
            });
        }

        private void ButtonPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            SetupButtonGradients();
        }

        private void SetupButtonGradients()
        {
            HSVGradient.GradientStops.Clear();
            HSV2Gradient.GradientStops.Clear();
            BlueGradient.GradientStops.Clear();
            RedGradient.GradientStops.Clear();
            RedGreenGradient.GradientStops.Clear();
            MetalGradient.GradientStops.Clear();
            GrayGradient.GradientStops.Clear();
            WarmGradient.GradientStops.Clear();
            ColdGradient.GradientStops.Clear();
            ProtanopiaGradient.GradientStops.Clear();
            BWGradient.GradientStops.Clear();
            BrandGradient.GradientStops.Clear();
            var isReversed = ReversePaletteButton.IsChecked.Value;
            for (double a = 1; a >= 0; a -= 0.1)
            {
                HSVGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.HSV : ColorScales.HSV  |ColorScales.Reversed, a))
                });
                HSV2Gradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.HSV2 : ColorScales.HSV2 | ColorScales.Reversed, a))
                });
                BlueGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Blue : ColorScales.Blue | ColorScales.Reversed, a))
                });
                RedGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Red : ColorScales.Red | ColorScales.Reversed, a))
                });
                RedGreenGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.RedGreen : ColorScales.RedGreen | ColorScales.Reversed, a))
                });
                MetalGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Metal : ColorScales.Metal | ColorScales.Reversed, a))
                });
                GrayGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Gray : ColorScales.Gray | ColorScales.Reversed, a))
                });
                WarmGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Warm : ColorScales.Warm | ColorScales.Reversed, a))
                });
                ColdGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Cold : ColorScales.Cold | ColorScales.Reversed, a))
                });
                ProtanopiaGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Protanopia : ColorScales.Protanopia | ColorScales.Reversed, a))
                });
                BWGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.BW : ColorScales.BW | ColorScales.Reversed, a))
                });
                BrandGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(!isReversed ? ColorScales.Brand : ColorScales.Brand | ColorScales.Reversed, a))
                });
            }
        }

        private void ReversePaletteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ReversePaletteButton.IsChecked.Value) _currentColorScale |= ColorScales.Reversed;
            else _currentColorScale &= (ColorScales)((int)ColorScales.Reversed ^ 0xffff);
            SetupButtonGradients();
            SetupColorScaleButton();
            InvokeMarkerModeChanged();
        }

        public void IncrementMarkerSize()
        {
            switch (_currentMarkerSizeMode)
            {
                case MarkerSizeModes.ExtraLarge:
                return;
                case MarkerSizeModes.Large:
                    _currentMarkerSizeMode = MarkerSizeModes.ExtraLarge;
                    break;
                case MarkerSizeModes.Medium:
                    _currentMarkerSizeMode = MarkerSizeModes.Large;
                    break;
                case MarkerSizeModes.SmallMedium:
                    _currentMarkerSizeMode = MarkerSizeModes.Medium;
                    break;
                case MarkerSizeModes.Small:
                    _currentMarkerSizeMode = MarkerSizeModes.SmallMedium;
                    break;
            }
            SetCurrentMarkerSizeMode(_currentMarkerSizeMode);
            StoreSettings();
            InvokeMarkerModeChanged();
        }

        public void DecrementMarkerSize()
        {
            switch (_currentMarkerSizeMode)
            {
                case MarkerSizeModes.ExtraLarge:
                    _currentMarkerSizeMode = MarkerSizeModes.Large;
                    break;
                case MarkerSizeModes.Large:
                    _currentMarkerSizeMode = MarkerSizeModes.Medium;
                    break;
                case MarkerSizeModes.Medium:
                    _currentMarkerSizeMode = MarkerSizeModes.SmallMedium;
                    break;
                case MarkerSizeModes.SmallMedium:
                    _currentMarkerSizeMode = MarkerSizeModes.Small;
                    break;
                case MarkerSizeModes.Small:
                    return;
            }
            SetCurrentMarkerSizeMode(_currentMarkerSizeMode);
            StoreSettings();
            InvokeMarkerModeChanged();
        }
    }

    public class MarkerChangedEventArgs : EventArgs
    {
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
    }
}