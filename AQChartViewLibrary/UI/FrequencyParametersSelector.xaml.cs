﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class FrequencyParametersSelector
    {
        public delegate void FrequencyParametersChangedDelegate(object sender, FrequencyParametersChangedEventArgs e);

        private ReportChartAlignMode _currentAlignMode;

        private ReportChartFrequencyYMode _currentYMode;
        private ReportChartBinMode _currentBinMode;
        private bool _noProcessDetailChanges;

        private double? _currentStep;

        private double? _currentUserAlignValue;

        public FrequencyParametersSelector()
        {
            InitializeComponent();
            SetCurrentYMode(ReportChartFrequencyYMode.FrequencyInNumber);
            SetCurrentBinMode(ReportChartBinMode.IncludeRight);
            SetCurrentAlignMode(ReportChartAlignMode.AlignByLL);
            SetDetailLevel(4);
            SetCurrentStep(null);
            SetCurrentUserAlignValue(null);
        }

        public event FrequencyParametersChangedDelegate FrequencyParametersChanged;

        public void SetYModeForLayers(bool isLayered)
        {
            if (!isLayered)
            {
                if (_currentYMode == ReportChartFrequencyYMode.FrequencyInPercentOfGroup)
                {
                    _currentYMode = ReportChartFrequencyYMode.FrequencyInPercent;
                    RefreshUI();
                    InvokeChangeEvent();
                }
            }
        }

        public ReportChartFrequencyYMode GetCurrentYMode()
        {
            return _currentYMode;
        }

        public ReportChartBinMode GetCurrentBinMode()
        {
            return _currentBinMode;
        }

        public ReportChartAlignMode GetCurrentAlignMode()
        {
            return _currentAlignMode;
        }

        public double? GetCurrentStep()
        {
            return _currentStep;
        }
        
        public double? GetCurrentUserAlignValue()
        {
            return _currentUserAlignValue;
        }

        public double GetCurrentDetailLevel()
        {
            return  Math.Pow(2, Math.Floor(DetailLevelSlider.Value));
        }

        public void SetCurrentYMode(ReportChartFrequencyYMode rcfm)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where
                                  (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcfm.ToString() &&
                                  (r as TextImageRadioButtonBase).GroupName == "FrequencyYGroup"
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentYMode = rcfm;
        }

        public void SetCurrentBinMode(ReportChartBinMode rcbm)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                                           where
                                               (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcbm.ToString() &&
                                               (r as TextImageRadioButtonBase).GroupName == "BinAlignment"
                                           select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentBinMode = rcbm;
        }

        public void SetCurrentStep(double? step)
        {
            _currentStep = step;
            StepTextBox.Text = !step.HasValue ? string.Empty : step.ToString();
        }
        public void SetCurrentUserAlignValue(double? value)
        {
            _currentUserAlignValue = value;
            UserAlignTextBox.Text = !value.HasValue ? string.Empty : value.ToString();
        }

        public void SetCurrentAlignMode(ReportChartAlignMode rcam)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where
                                  (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcam.ToString() &&
                                  (r as TextImageRadioButtonBase).GroupName == "FrequencyAlignGroup"
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentAlignMode = rcam;
            UserAlignTextBox.IsEnabled = _currentAlignMode == ReportChartAlignMode.AlignByUser;
        }
        public void RefreshUI()
        { 
            SetCurrentAlignMode(_currentAlignMode);
            SetCurrentBinMode(_currentBinMode);
            SetCurrentStep(_currentStep);
            SetCurrentUserAlignValue(_currentUserAlignValue);
            SetCurrentYMode(_currentYMode);
        }

        public void SetDetailLevel(double d)
        {
            _noProcessDetailChanges = true;
            DetailLevelSlider.Value = Math.Floor(Math.Sqrt(d));
            if (DetailLevelSlider != null)
            {
                DetailLevelSlider.IsEnabled = false;
                UpdateLayout();
                DetailLevelSlider.IsEnabled = true;
            }
            _noProcessDetailChanges = false;
        }

        public void DisplayStep(bool stepIsAuto, double autoStep)
        {
            Dispatcher.BeginInvoke(() =>
            {
                AutoStepTextBlock.Visibility = stepIsAuto ? Visibility.Visible : Visibility.Collapsed;
                AutoStepTextBlock.Text = autoStep.ToString(CultureInfo.InvariantCulture);
            });
        }
        
        private void FreqYModeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentYMode = (ReportChartFrequencyYMode) Enum.Parse(typeof (ReportChartFrequencyYMode), rb.Name, true);
            InvokeChangeEvent();
        }

        private void FreqAlignModeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentAlignMode = (ReportChartAlignMode) Enum.Parse(typeof (ReportChartAlignMode), rb.Name, true);
            UserAlignTextBox.IsEnabled = _currentAlignMode == ReportChartAlignMode.AlignByUser;
            InvokeChangeEvent();
        }

        private void BinModeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentBinMode = (ReportChartBinMode)Enum.Parse(typeof(ReportChartBinMode), rb.Name, true);
            InvokeChangeEvent();
        }

        

        private void DetailLevel_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (_noProcessDetailChanges) return;
            InvokeChangeEvent();
        }

        private void InvokeChangeEvent()
        {
            if (FrequencyParametersChanged != null)
            {
                FrequencyParametersChanged.Invoke(this, new FrequencyParametersChangedEventArgs
                    {
                        YMode = _currentYMode,
                        AlignMode = _currentAlignMode,
                        DetailLevel = DetailLevelSlider.Value,
                        Step = _currentStep,
                        UserAlignValue = _currentUserAlignValue,
                        BinMode = _currentBinMode
                });
            }
        }

        private void StepTextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            double currentstep;
            var stepExists = Double.TryParse(StepTextBox.Text, out currentstep);
            if (!stepExists && StepTextBox.Text.Count() > 0)
            {
                StepTextBox.Focus();
                ShowExclamation(true);
            }
            else ShowExclamation(false);
            _currentStep = stepExists ? currentstep : new double?();
            AutoStepTextBlock.Visibility = stepExists ? Visibility.Collapsed : Visibility.Visible;
            InvokeChangeEvent();
        }

        public void ShowExclamation(bool iserror)
        {
            StepTextBox.Background = iserror ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);
        }
        private void StepTextBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            AutoStepTextBlock.Visibility = Visibility.Collapsed;
        }

        private void UserAlignTextBox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            double currentUserAlignValue;
            var valueExists = Double.TryParse(UserAlignTextBox.Text, out currentUserAlignValue);
            if (!valueExists && UserAlignTextBox.Text.Count() > 0)
            {
                UserAlignTextBox.Focus();
                UserAlignTextBox.Background = new SolidColorBrush(Colors.Red);
            }
            else UserAlignTextBox.Background = new SolidColorBrush(Colors.White);
            _currentUserAlignValue = valueExists ? currentUserAlignValue : new double?();
            InvokeChangeEvent();
        }
    }

    public class FrequencyParametersChangedEventArgs : EventArgs
    {
        public ReportChartFrequencyYMode YMode { get; set; }
        public ReportChartAlignMode AlignMode { get; set; }
        public ReportChartBinMode BinMode { get; set; }
        public double DetailLevel { get; set; }
        public double? Step { get; set; }
        public double? UserAlignValue { get; set; }
    }
}