﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQControlsLibrary.Annotations;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class RangeModeSelector
    {
        public delegate void RangeModeChangedDelegate(object sender, RangeModeChangedEventArgs e);
        private double? _minLimit = null;
        private double? _maxLimit = null;
        private string _currentRangeDescription;
        private bool _colorProcessingDisabled;
        private bool _processingDisabled;
        private ReportChartRangeType _currentRangeType;
        private ReportChartRangeStyle _currentRangeStyle;
        private SLDataTable _userLimits;
        private ObservableCollection<RangeLine> _rangeLines;
        public static readonly DependencyProperty XYProperty =
            DependencyProperty.Register("XY", typeof(bool), typeof(RangeModeSelector), new PropertyMetadata(null));
        public bool XY
        {
            get { return (bool) GetValue(XYProperty); }
            set { SetValue(XYProperty, value); }
        }

        public RangeModeSelector()
        {
            InitializeComponent();
            SetCurrentRangeType(ReportChartRangeType.RangeIsAB);
            SetCurrentRangeStyle(ReportChartRangeStyle.Filled | ReportChartRangeStyle.Thin | ReportChartRangeStyle.ErrorBar | ReportChartRangeStyle.Labeled | ReportChartRangeStyle.OutagesCount | ReportChartRangeStyle.OutagesPercent | ReportChartRangeStyle.OutagesProbability);
        }

        public event RangeModeChangedDelegate RangeModeChanged;
        public event RangeModeChangedDelegate MarkOutageClicked;
        public event RangeModeChangedDelegate UnmarkOutageClicked;

        public ReportChartRangeType GetCurrentRangeType()
        {
            return _currentRangeType;
        }

        public ReportChartRangeStyle GetCurrentRangeStyle()
        {
            return _currentRangeStyle;
        }
        public string GetCurrentRangeDescription()
        {
            return _currentRangeDescription;
        }

        public SLDataTable GetUserLimits()
        {
            return _userLimits;
        }

        public void SetCurrentRangeType(ReportChartRangeType rcrt)
        {
            _currentRangeType = rcrt;
            RefreshTextBoxStates(_userLimits);
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcrt.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            if (rb != null)
            {
                var textBlock = rb.Content as TextBlock;
                if (textBlock != null) _currentRangeDescription = textBlock.Text;
            }
        }

        public void SetCurrentRangeStyle(ReportChartRangeStyle style)
        {
            _currentRangeStyle = style;
            Thick.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Thick) == ReportChartRangeStyle.Thick;
            Thin.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Thin) == ReportChartRangeStyle.Thin;
            Invisible.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Invisible) == ReportChartRangeStyle.Invisible;
            Filled.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Filled) == ReportChartRangeStyle.Filled;
            Labeled.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Labeled) == ReportChartRangeStyle.Labeled;
            OutagesPercent.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.OutagesPercent) == ReportChartRangeStyle.OutagesPercent;
            OutagesCount.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.OutagesCount) == ReportChartRangeStyle.OutagesCount;
            OutagesProbability.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.OutagesProbability) == ReportChartRangeStyle.OutagesProbability;
            CustomerRisk.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.CustomerRisk) == ReportChartRangeStyle.CustomerRisk;
            LayeredErrorBar.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.LayeredErrorBar) == ReportChartRangeStyle.LayeredErrorBar;
            ErrorBar.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.ErrorBar) == ReportChartRangeStyle.ErrorBar;
            Single.IsChecked = (_currentRangeStyle & ReportChartRangeStyle.Single) == ReportChartRangeStyle.Single;
        }
        
        public void SetMarkOutage(bool markOutage)
        {
            MarkOutage.IsChecked = markOutage;
        }

        public void SetOutagePanelVisibility(bool visible)
        {
            OutagePanel.Visibility = visible ? Visibility.Visible : Visibility.Collapsed;
        }

        public bool GetMarkOutage()
        {
            return MarkOutage.IsChecked != null && MarkOutage.IsChecked.Value;
        }

        public void RefreshUI()
        {
            SetCurrentRangeType(_currentRangeType);
        }

        private void Range_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentRangeType = (ReportChartRangeType) Enum.Parse(typeof (ReportChartRangeType), rb.Name, true);
            var textBlock = rb.Content as TextBlock;
            if (textBlock != null) _currentRangeDescription = textBlock.Text;
            RefreshTextBoxStates(_userLimits);
            RangeModeChanged?.Invoke(this,
                new RangeModeChangedEventArgs
                {
                    RangeType = _currentRangeType,
                    RangeDescription = _currentRangeDescription,
                    ShowOutages = MarkOutage.IsChecked != null && MarkOutage.IsChecked.Value,
                    RangeStyle = _currentRangeStyle,
                    OnlyStyleChanged = false
                });
        }

        public void SetMultipleRanges()
        {
            LimitInputPanel.Visibility = Visibility.Collapsed;
        }

        public void SetupRangesForAB(SLDataTable userLimits)
        {
            LimitInputPanel.Visibility = Visibility.Visible;
            RefreshTextBoxStates(userLimits);
        }

        private void RefreshTextBoxStates(SLDataTable userLimits)
        {
            if (userLimits == null || userLimits.Table == null || userLimits.Table.Count == 0)
            {
                _minLimit = null;
                _maxLimit = null;
                userLimits = new SLDataTable
                {
                    Table = new List<SLDataRow>(),
                    DataTypes = new List<string> {"Double", "String"}
                };
                userLimits.Table.Add(new SLDataRow {Row = new List<object> {null, null}});
                userLimits.Table.Add(new SLDataRow { Row = new List<object> { null, null} });
                userLimits.ColumnNames = new List<string> {"Границы", "#Границы_Цвет" };
                userLimits.ColumnTags = new List<object> {"Y", "Y" };
            }
            else
            {
                _minLimit = userLimits.Table[0].Row[0] as Double?;
                _maxLimit = userLimits.Table[1].Row[0] as Double?;
            }

            if (_currentRangeType != ReportChartRangeType.RangeIsAB)
            {
                LimitsViewListBox.IsEnabled = false;
                ResetLinesButton.IsEnabled = false;
                AddRangeLineButton.IsEnabled = false;
            }
            else
            {
                ResetLinesButton.IsEnabled = true;
                AddRangeLineButton.IsEnabled = true;
                LimitsViewListBox.IsEnabled = true;
            }
            _rangeLines = new ObservableCollection<RangeLine>();

            for(var i=0; i<userLimits.Table[0].Row.Count; i+=2)
            {
                var r = new RangeLine
                {
                    Min = userLimits.Table[0].Row[i] as double?,
                    Max = userLimits.Table[1].Row[i] as double?,
                    Tag = i > 0 ? (i - 1).ToString(CultureInfo.InvariantCulture) : string.Empty,
                    Axis = userLimits.ColumnTags[i]?.ToString(),
                    Color = userLimits.Table[0].Row.Count>= i + 1 ? userLimits.Table[0].Row[i + 1]?.ToString() : null
                };
                r.PropertyChanged += LimitsChanged;
                _rangeLines.Add(r);    
            }
            CreateUserLimitsTable();
            LimitsViewListBox.ItemsSource = _rangeLines;
        }

        void LimitsChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Color" && _colorProcessingDisabled || _processingDisabled) return;

            SetRangesForAB();
        }

        
        private void SetRangesForAB()
        {
            CreateUserLimitsTable();

            if (RangeModeChanged != null)
                RangeModeChanged.Invoke(this,
                                        new RangeModeChangedEventArgs
                                            {
                                                RangeType = _currentRangeType,
                                                RangeDescription = _currentRangeDescription,
                                                UserRanges = _userLimits,
                                                ShowOutages = MarkOutage.IsChecked!=null && MarkOutage.IsChecked.Value,
                                                RangeStyle = _currentRangeStyle,
                                                OnlyStyleChanged = false
                                        });
        }

        private void CreateUserLimitsTable()
        {
            _userLimits = new SLDataTable
            {
                TableName = "Пределы",
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                DataTypes = new List<string>()
            };

            _userLimits.Table.Add(new SLDataRow {Row = new List<object>()});
            _userLimits.Table.Add(new SLDataRow { Row = new List<object>() });

            foreach (var r in _rangeLines)
            {
                _userLimits.ColumnNames.Add(r.Name);
                _userLimits.ColumnNames.Add("#"+r.Name+"_Цвет");
                _userLimits.ColumnTags.Add(r.Axis);
                _userLimits.ColumnTags.Add(r.Axis);
                _userLimits.DataTypes.Add("Double");
                _userLimits.DataTypes.Add("String");
                _userLimits.Table[0].Row.Add(r.Min);
                _userLimits.Table[0].Row.Add(r.Color);
                _userLimits.Table[1].Row.Add(r.Max);
                _userLimits.Table[1].Row.Add(r.Color);
            }
        }

        public void RenameRange(List<ReportRenameOperation> ro)
        {
            if (_currentRangeType != ReportChartRangeType.RangeIsAB) return;
            foreach (var r in ro.Where(a=>a.RenameTarget== ReportRenameTarget.Range))
            {
                var renaming = _rangeLines.SingleOrDefault(a => String.Equals(a.Name.Trim(), r.OldName.Trim(), StringComparison.CurrentCultureIgnoreCase));
                if (renaming != null) renaming.Name = r.NewName;
            }
            SetRangesForAB();
        }

        private void DeleteRangeLineButtonClick(object sender, RoutedEventArgs e)
        {
            var a = sender as TextImageButtonBase;
            if (a == null) return;
            if (_rangeLines.Count > 1) _rangeLines.Remove(_rangeLines.SingleOrDefault(x => x.Tag == (string) a.Tag));
            else
            {
                _rangeLines[0].MinText = string.Empty;
                _rangeLines[0].MaxText = string.Empty;
                _rangeLines[0].Name = null;
            }
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            for (var i = 0; i < _rangeLines.Count; i += 2)
            {
                _rangeLines[i].Tag = i > 0 ? (i - 1).ToString(CultureInfo.InvariantCulture) : string.Empty;
            }
            SetRangesForAB();
        }

        private void AddRangeLineButtonClick(object sender, RoutedEventArgs e)
        {
            var r = new RangeLine {Max = null, Min = null, Tag = (_rangeLines.Any() ? (_rangeLines.Count()+1).ToString(CultureInfo.InvariantCulture):string.Empty)
                , Color = _rangeLines[_rangeLines.Count - 1].Color, Axis = _rangeLines[_rangeLines.Count - 1].Axis
            };
            r.PropertyChanged += LimitsChanged;
            _rangeLines.Add(r);
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            SetRangesForAB();
        }

        private void ResetRangeLinesButtonClick(object sender, RoutedEventArgs e)
        {
            if (_rangeLines.Count > 1)
            {
                for(var i=_rangeLines.Count-1; i>0; i--)_rangeLines.RemoveAt(i);
            }
            else
            {
                _rangeLines[0].MinText = string.Empty;
                _rangeLines[0].MaxText = string.Empty;
                _rangeLines[0].Name = string.Empty;
                _rangeLines[0].Color = ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.32);
            }
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
            SetRangesForAB();
        }

        private void XY_Click(object sender, RoutedEventArgs e)
        {
            var a = sender as TextImageButtonBase;
            if (a == null) return;
            var item = _rangeLines.SingleOrDefault(x => x.Tag == (string) a.Tag);
            if (item != null) item.Axis = item.Axis == "Y" ? "X" : "Y";
        }

        public void FlipUserRanges()
        {
            if (_userLimits == null || _rangeLines == null || !_rangeLines.Any()) return;
            _processingDisabled = true;
            foreach (var line in _rangeLines)
            {
                line.Axis = line.Axis == "Y" ? "X" : "Y";
            }
            CreateUserLimitsTable();
            _processingDisabled = false;
        }

        private void MarkOutage_OnClick(object sender, RoutedEventArgs e)
        {
            SetRangesForAB();
        }

        public void ShowRegressionOptions(bool show)
        {
            CustomerRisk.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
            LayeredErrorBar.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
            ErrorBar.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
            Single.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
        }

        private void RangeStyle_Click(object sender, RoutedEventArgs e)
        {
            string name;
            var a = sender as TextImageToggleButtonBase;
            if (a != null) name = a.Name;
            else
            {
                var b = sender as TextImageRadioButtonBase;
                if (b != null) name = b.Name;
                else return;
            }
            _currentRangeStyle = ReportChartRangeStyle.None;
            _currentRangeStyle |= Thick.IsChecked != null && Thick.IsChecked.Value ? ReportChartRangeStyle.Thick : 0;
            _currentRangeStyle |= Thin.IsChecked != null && Thin.IsChecked.Value ? ReportChartRangeStyle.Thin : 0;
            _currentRangeStyle |= Invisible.IsChecked != null && Invisible.IsChecked.Value ? ReportChartRangeStyle.Invisible : 0;
            _currentRangeStyle |= Filled.IsChecked != null && Filled.IsChecked.Value ? ReportChartRangeStyle.Filled : 0;
            _currentRangeStyle |= Labeled.IsChecked != null && Labeled.IsChecked.Value ? ReportChartRangeStyle.Labeled : 0;
            _currentRangeStyle |= OutagesPercent.IsChecked != null && OutagesPercent.IsChecked.Value ? ReportChartRangeStyle.OutagesPercent : 0;
            _currentRangeStyle |= OutagesProbability.IsChecked != null && OutagesProbability.IsChecked.Value ? ReportChartRangeStyle.OutagesProbability : 0;
            _currentRangeStyle |= OutagesCount.IsChecked != null && OutagesCount.IsChecked.Value ? ReportChartRangeStyle.OutagesCount : 0;
            _currentRangeStyle |= CustomerRisk.IsChecked != null && CustomerRisk.IsChecked.Value ? ReportChartRangeStyle.CustomerRisk : 0;
            _currentRangeStyle |= LayeredErrorBar.IsChecked != null && LayeredErrorBar.IsChecked.Value ? ReportChartRangeStyle.LayeredErrorBar : 0;
            _currentRangeStyle |= ErrorBar.IsChecked != null && ErrorBar.IsChecked.Value ? ReportChartRangeStyle.ErrorBar : 0;
            _currentRangeStyle |= Single.IsChecked != null && Single.IsChecked.Value ? ReportChartRangeStyle.Single : 0;

            RangeModeChanged?.Invoke(this,
                new RangeModeChangedEventArgs
                {
                    RangeType = _currentRangeType,
                    RangeDescription = _currentRangeDescription,
                    ShowOutages = MarkOutage.IsChecked != null && MarkOutage.IsChecked.Value,
                    RangeStyle = _currentRangeStyle,
                    OnlyStyleChanged = !new[] { "OutagesPercent", "OutagesCount", "OutagesProbability", "CustomerRisk", "LayeredErrorBar", "ErrorBar", "Single" }.Contains(name) 
                });
        }

        private void AutoColor_OnClick(object sender, RoutedEventArgs e)
        {
            if (!AutoColor.IsChecked.Value) RecalcAutoColors();
        }

        private void RecalcAutoColors()
        {
            _colorProcessingDisabled = true;
            if(_rangeLines!=null && _rangeLines.Count >= 1)
            {
                for (var i = 0; i < _rangeLines.Count; i++)
                {
                    if (i == _rangeLines.Count - 1) _colorProcessingDisabled = false;
                    _rangeLines[i].Color = ColorConvertor.GetColorStringForScale(ColorScales.HSV,
                        0.32 + 0.69*Math.Floor(i)/_rangeLines.Count);
                }
            }
        }
    }

    public class RangeModeChangedEventArgs : EventArgs
    {
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public string RangeDescription { get; set; }
        public SLDataTable UserRanges { get; set; }
        public bool ShowOutages { get; set; }
        public bool OnlyStyleChanged { get; set; }
    }

    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (bool)value;
            return visibility ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (Visibility)value;
            return (visibility == Visibility.Visible);
        }
    }

    public class RangeLine : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public RangeLine()
        {
            Axis = "Y";
        }

        private double? _max, _min;
        public double? Min
        {
            get { return _min; }
            set { _min = value;
            }
        }
        public double? Max
        {
            get { return _max; }
            set { _max = value;
            }
        }

        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) 
                    ? _min==_max 
                        ? String.Format("{0}", _min) 
                        : String.Format("{0}..{1}",_min, _max) 
                    :_name; }
            set { _name = value; }
        }
        
        public string MinText
        {
            get { return _min.HasValue ? _min.Value.ToString(CultureInfo.InvariantCulture) : null; }
            set
            {
                if (String.IsNullOrWhiteSpace(value)) _min = null;
                else
                {
                    double mintemp;
                    if (!double.TryParse(
                            value.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                                .Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator),
                            out mintemp))
                    {
                        _min = null;
                        throw new Exception("Неверный формат строки");
                    }
                _min = mintemp;
                }
                OnPropertyChanged("MinText");
            }
        }

        public string MaxText
        {
            get { return _max.HasValue ? _max.Value.ToString(CultureInfo.InvariantCulture) : null; }
            set
            {
                if (String.IsNullOrWhiteSpace(value)) _max = null;
                else
                {
                    double maxtemp;
                    if (!double.TryParse(
                            value.Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                                .Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator),
                            out maxtemp))
                    {
                        _max = null;
                        throw new Exception("Неверный формат строки");
                    }
                    _max = maxtemp;
                }
                OnPropertyChanged("MaxText");
            }
        }
        private string _tag;
        public string Tag
        {
            get { return _tag; }
            set
            {
                _tag = value;
                OnPropertyChanged("Tag");
            }
        }

        private string _color;
        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
                OnPropertyChanged("Color");
            }
        }

        private string _axis;
        public string Axis
        {
            get { return _axis; }
            set
            {
                _axis = value;
                OnPropertyChanged("Axis");
            }
        }
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}