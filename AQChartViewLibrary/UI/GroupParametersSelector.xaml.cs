﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQReportingLibrary;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class GroupParametersSelector 
    {
        public delegate void GroupParametersChangedDelegate(object sender, GroupParametersChangedEventArgs e);
        private ReportChartGroupCenter _currentGroupCenter;
        private GroupPointMode _currentPointMode;
        private ReportChartGroupMode _currentGroupMode;
        private ReportChartGroupAnalisysMode _currentAnalisysMode;
        private bool _outliers;
        private bool _sorted;
        private LabelsType _currentLabelType;

        public GroupParametersSelector()
        {
            InitializeComponent();
            SetCurrentGroupParameters(ReportChartGroupCenter.Median, false, true, LabelsType.None, GroupPointMode.Hexagon, ReportChartGroupMode.Box | ReportChartGroupMode.Center, ReportChartGroupAnalisysMode.Single);
        }

        public event GroupParametersChangedDelegate GroupParametersChanged;

        public bool GetCurrentOutliers()
        {
            return _outliers;
        }

        public GroupPointMode GetCurrentPointMode()
        {
            return _currentPointMode;
        }

        public bool GetCurrentSorted()
        {
            return _sorted;
        }
        public LabelsType GetCurrentShowText()
        {
            return _currentLabelType;
        }
        
        public ReportChartGroupCenter GetCurrentGroupCenter()
        {
            return _currentGroupCenter;
        }

        public ReportChartGroupMode GetCurrentGroupMode()
        {
            return _currentGroupMode;
        }
        public ReportChartGroupAnalisysMode GetCurrentGroupAnalisysMode()
        {
            return _currentAnalisysMode;
        }

        public void SetCurrentGroupAnalisysMode(ReportChartGroupAnalisysMode mode)
        {
            _currentAnalisysMode = mode;
            var rb = (from r in ButtonPanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == mode.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        public void SetCurrentGroupParameters(ReportChartGroupCenter center, bool outliers, bool sorted, LabelsType showText, GroupPointMode pointMode, ReportChartGroupMode groupMode, ReportChartGroupAnalisysMode analisysMode)
        {
            SetCurrentSorted(sorted);
            SetCurrentOutliers(outliers);
            SetCurrentGroupCenter(center);
            SetCurrentShowText(showText);
            SetCurrentPointMode(pointMode);
            SetCurrentGroupMode(groupMode);
            SetCurrentGroupAnalisysMode(analisysMode);
            SetPointModeVisibility();
        }

        public void SetCurrentGroupMode(ReportChartGroupMode rcdsm)
        {
            PointToggle.IsChecked = (rcdsm & ReportChartGroupMode.Point) > 0;
            BoxToggle.IsChecked = (rcdsm & ReportChartGroupMode.Box) > 0;
            ViolinToggle.IsChecked = (rcdsm & ReportChartGroupMode.Violin) > 0;
            CenterToggle.IsChecked = (rcdsm & ReportChartGroupMode.Center) > 0;
            _currentGroupMode = rcdsm;
        }


        private void SetCurrentOutliers(bool outliers)
        {
            ShowOutliers.IsChecked = outliers;
            _outliers = outliers;
        }

        private void SetCurrentShowText(LabelsType showText)
        {
            if ((showText & LabelsType.Centers) == LabelsType.Centers) LabelCenters.IsChecked = true;
            if ((showText & LabelsType.Outage) == LabelsType.Outage) LabelOutage.IsChecked = true;
            if ((showText & LabelsType.Whiskers) == LabelsType.Whiskers) LabelWhiskers.IsChecked = true;
            if ((showText & LabelsType.Pie) == LabelsType.Pie) LabelPie.IsChecked = true;
            if ((showText & LabelsType.Scaled) == LabelsType.Scaled) LabelScaled.IsChecked = true;
            if ((showText & LabelsType.LayerName) == LabelsType.LayerName) LabelLayerName.IsChecked = true;
            _currentLabelType = showText;
        }

        private void SetCurrentSorted(bool sorted)
        {
            SortData.IsChecked = sorted;
            _sorted = sorted;
        }

        public void RefreshUI()
        {
            SetCurrentGroupParameters(_currentGroupCenter, _outliers, _sorted, _currentLabelType, _currentPointMode, _currentGroupMode, _currentAnalisysMode);
        }

        public void SetCurrentGroupCenter(ReportChartGroupCenter rcgc)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase) r).Name == rcgc.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentGroupCenter = rcgc;
        }

        public void SetCurrentPointMode(GroupPointMode rcpm)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                                           where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase)r).Name == rcpm.ToString()
                                           select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentPointMode = rcpm;
        }

        private void ShowOutliersButton_Click(object sender, RoutedEventArgs e)
        {
            var isChecked = ((TextImageToggleButtonBase) sender).IsChecked;
            if (isChecked != null) _outliers = isChecked.Value;
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Outliers = _outliers, Sorted = _sorted, ShowText = _currentLabelType, GroupMode = _currentGroupMode, AnalysisMode = _currentAnalisysMode });
        }

        private void GroupModeButton_Click(object sender, RoutedEventArgs e)
        {
            _currentGroupMode = 0;
            _currentGroupMode |= CenterToggle.IsChecked != null && CenterToggle.IsChecked.Value ? ReportChartGroupMode.Center : 0;
            _currentGroupMode |= BoxToggle.IsChecked != null && BoxToggle.IsChecked.Value ? ReportChartGroupMode.Box : 0;
            _currentGroupMode |= PointToggle.IsChecked != null && PointToggle.IsChecked.Value ? ReportChartGroupMode.Point : 0;
            _currentGroupMode |= ViolinToggle.IsChecked != null && ViolinToggle.IsChecked.Value ? ReportChartGroupMode.Violin : 0;
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Outliers = _outliers, Sorted = _sorted, ShowText = _currentLabelType, GroupMode = _currentGroupMode, AnalysisMode = _currentAnalisysMode });
        }

        private void GroupCenterButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentGroupCenter = (ReportChartGroupCenter)Enum.Parse(typeof(ReportChartGroupCenter), rb.Name, true);
            GroupParametersChanged?.Invoke(this,
                new GroupParametersChangedEventArgs
                {
                    GroupCenter = _currentGroupCenter,
                    Outliers = _outliers,
                    Sorted = _sorted,
                    AnalysisMode = _currentAnalisysMode
                });
        }

        private void Layer_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentAnalisysMode = (ReportChartGroupAnalisysMode)Enum.Parse(typeof(ReportChartGroupAnalisysMode), rb.Name, true);
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Outliers = _outliers, Sorted = _sorted, ShowText = _currentLabelType, GroupMode = _currentGroupMode, AnalysisMode = _currentAnalisysMode });
        }

        private void SortDataButton_Click(object sender, RoutedEventArgs e)
        {
            var isChecked = ((TextImageToggleButtonBase)sender).IsChecked;
            if (isChecked != null) _sorted = isChecked.Value;
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Sorted = _sorted, Outliers = _outliers, ShowText = _currentLabelType, AnalysisMode = _currentAnalisysMode });
        }

        private void ShowTextButton_Click(object sender, RoutedEventArgs e)
        {
            var isChecked = ((TextImageToggleButtonBase)sender).IsChecked;
            if (isChecked == null) return;
            var currentLabelType = LabelsType.None;
            if (LabelCenters.IsChecked != null && LabelCenters.IsChecked.Value) currentLabelType |= LabelsType.Centers;
            if (LabelOutage.IsChecked != null && LabelOutage.IsChecked.Value) currentLabelType |= LabelsType.Outage;
            if (LabelWhiskers.IsChecked != null && LabelWhiskers.IsChecked.Value) currentLabelType |= LabelsType.Whiskers;
            if (LabelPie.IsChecked != null && LabelPie.IsChecked.Value) currentLabelType |= LabelsType.Pie;
            if (LabelScaled.IsChecked != null && LabelScaled.IsChecked.Value) currentLabelType |= LabelsType.Scaled;
            if (LabelLayerName.IsChecked != null && LabelLayerName.IsChecked.Value) currentLabelType |= LabelsType.LayerName;
            _currentLabelType = currentLabelType;
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Outliers = _outliers, Sorted = _sorted, ShowText = _currentLabelType, GroupMode = _currentGroupMode, AnalysisMode = _currentAnalisysMode });
        }

        private void PointModeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentPointMode = (GroupPointMode)Enum.Parse(typeof(GroupPointMode), rb.Name, true);
            GroupParametersChanged?.Invoke(this, new GroupParametersChangedEventArgs { GroupCenter = _currentGroupCenter, PointMode = _currentPointMode, Outliers = _outliers, Sorted = _sorted, ShowText = _currentLabelType, GroupMode = _currentGroupMode, AnalysisMode = _currentAnalisysMode });
        }

        private void PointToggle_Checked(object sender, RoutedEventArgs e)
        {
            SetPointModeVisibility();
        }

        private void SetPointModeVisibility()
        {
            var v = PointToggle.IsChecked.Value;
            BeeSwarm.IsEnabled = v;
            PointsInLine.IsEnabled = v;
            StripChart.IsEnabled = v;
            Hexagon.IsEnabled = v;
        }
    }

    public class GroupParametersChangedEventArgs : EventArgs
    {
        public ReportChartGroupCenter GroupCenter { get; set; }
        public GroupPointMode PointMode { get; set; }
        public bool Outliers { get; set; }
        public bool Sorted { get; set; }
        public LabelsType ShowText { get; set; }
        public ReportChartGroupMode GroupMode { get; set; }
        public ReportChartGroupAnalisysMode AnalysisMode { get; set; }
    }
}