﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQReportingLibrary;
using AQBasicControlsLibrary;
using System.Windows.Media;
using AQChartLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class CircleParametersSelector 
    {
        public delegate void CircleParametersChangedDelegate(object sender, CircleParametersChangedEventArgs e);
        private ReportChartCircleDiagramType _currentDiagramType;
        private ArrangementMode _currentArrangement;
        private ReportChartCircleLabelsType _currentLabelsType;

        public CircleParametersSelector()
        {
            InitializeComponent();
            SetCurrentCircleParameters(ReportChartCircleDiagramType.Circles, ArrangementMode.Uniform, ReportChartCircleLabelsType.None);
        }

        public event CircleParametersChangedDelegate CircleParametersChanged;

        public ReportChartCircleDiagramType GetCurrentCircleDiagramType()
        {
            return _currentDiagramType;
        }
        
        public ArrangementMode GetCurrentArrangement()
        {
            return _currentArrangement;
        }

        public ReportChartCircleLabelsType GetCurrentLabelsType()
        {
            return _currentLabelsType;
        }

        public void SetCurrentCircleParameters(ReportChartCircleDiagramType diagramType, ArrangementMode arrangement, ReportChartCircleLabelsType labelsType)
        {
            SetCurrentDiagramType(diagramType);
            SetCurrentArrangement(arrangement);
            SetCurrentLabelsType(labelsType);
        }

        private void SetCurrentDiagramType(ReportChartCircleDiagramType diagramType)
        {
            _currentDiagramType = diagramType;
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase)r).Name == diagramType.ToString() && ((TextImageRadioButtonBase)r).GroupName=="DiagramType" select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void SetCurrentArrangement(ArrangementMode arrangement)
        {
            _currentArrangement = arrangement;
            TextImageRadioButtonBase rb = (from r in DrawPanel.Children where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase)r).Name == arrangement.ToString() && ((TextImageRadioButtonBase)r).GroupName == "ArrangementMode" select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void SetCurrentLabelsType(ReportChartCircleLabelsType labelsType)
        {
            _currentLabelsType = labelsType;
            N.IsChecked = (labelsType & ReportChartCircleLabelsType.N) > 0;
            Percent.IsChecked = (labelsType & ReportChartCircleLabelsType.Percent) > 0;
            PercentInGroup.IsChecked = (labelsType & ReportChartCircleLabelsType.PercentInGroup) > 0;
            Layer.IsChecked = (labelsType & ReportChartCircleLabelsType.Layer) > 0;
            NumberOfCases.IsChecked = (labelsType & ReportChartCircleLabelsType.NumberOfCases) > 0;
            Scaled.IsChecked = (labelsType & ReportChartCircleLabelsType.Scaled) > 0;
        }

        public void RefreshUI()
        {
            SetCurrentCircleParameters(_currentDiagramType, _currentArrangement, _currentLabelsType);
        }

        private void ArrangementButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentArrangement = (ArrangementMode)Enum.Parse(typeof(ArrangementMode), rb.Name, true);
            CircleParametersChanged?.Invoke(this, new CircleParametersChangedEventArgs { DiagramType = _currentDiagramType, Arrangement = _currentArrangement, LabelsType = _currentLabelsType});
        }
        private void LabelsType_Click(object sender, RoutedEventArgs e)
        {
            _currentLabelsType = ReportChartCircleLabelsType.None;
            if (N.IsChecked != null && N.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.N;
            if (Percent.IsChecked != null && Percent.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.Percent;
            if (PercentInGroup.IsChecked != null && PercentInGroup.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.PercentInGroup;
            if (Layer.IsChecked != null && Layer.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.Layer;
            if (NumberOfCases.IsChecked != null && NumberOfCases.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.NumberOfCases;
            if (Scaled.IsChecked != null && Scaled.IsChecked.Value) _currentLabelsType |= ReportChartCircleLabelsType.Scaled;

            CircleParametersChanged?.Invoke(this, new CircleParametersChangedEventArgs { DiagramType = _currentDiagramType, Arrangement = _currentArrangement, LabelsType = _currentLabelsType });
        }
        private void DiagramTypeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentDiagramType = (ReportChartCircleDiagramType)Enum.Parse(typeof(ReportChartCircleDiagramType), rb.Name, true);
            CircleParametersChanged?.Invoke(this, new CircleParametersChangedEventArgs { DiagramType = _currentDiagramType, Arrangement = _currentArrangement, LabelsType = _currentLabelsType });
        }
    }

    public class CircleParametersChangedEventArgs : EventArgs
    {
        public ReportChartCircleDiagramType DiagramType { get; set; }
        public ArrangementMode Arrangement { get; set; }
        public ReportChartCircleLabelsType LabelsType { get; set; }
    }
}