﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class InteractivityCommandSelector
    {
        public delegate void InteractiveCommandDelegate(object sender, InteractiveCommandEventArgs e);

        public InteractivityCommandSelector()
        {
            InitializeComponent();
        }

        public event InteractiveCommandDelegate InteractiveCommand;
        
        public void EnableOperations(InteractiveOperations io)
        {
            HideOptions();
            DeleteValue.IsEnabled = (io & InteractiveOperations.DeleteValue) > 0;
            DeleteCase.IsEnabled = (io & InteractiveOperations.DeleteCase) > 0;
            CutInNewTable.IsEnabled = (io & InteractiveOperations.CutInNewTable) > 0;
            CopyToNewTable.IsEnabled = (io & InteractiveOperations.CopyToNewTable) > 0;
            SetColor.IsEnabled = (io & InteractiveOperations.SetColor) > 0;
            SetNoColor.IsEnabled = (io & InteractiveOperations.SetColor) > 0;
            SetColorForValue.IsEnabled = (io & InteractiveOperations.SetColor) > 0;
            SetColorForRow.IsEnabled = (io & InteractiveOperations.SetColorForRow) > 0;
            SetLabel.IsEnabled = (io & InteractiveOperations.SetLabel) > 0;

        }

        public void SetMessage(string message, int n)
        {
            DeleteValue.Text = "Удалить значения, " + message;
            DeleteCase.Text = "Удалить наблюдения, " + message;
            CutInNewTable.Text = "Вырезать в новую таблицу, " + message;
            CopyToNewTable.Text = "Копировать в новую таблицу, " + message;
            SetColor.Text = "Установить цвета маркеров, " + message;
            SetLabel.Text = "Установить метки маркеров, " + message;
            numberOfCases.Text = "n=" + n;
        }

        private void Command_Click(object sender, RoutedEventArgs e)
        {
            var b = sender as TextImageButtonBase;
            if (b == null) return;
            var command = (InteractiveOperations)Enum.Parse(typeof(InteractiveOperations), b.Name, true);
            HideOptions();

            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = command });
        }

        public void EmitDeleteValueCommand()
        {
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.DeleteValue });
        }

        public void EmitDeleteCaseCommand()
        {
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.DeleteCase });
        }

        private void HideOptions()
        {
            ColorSelector.Visibility = Visibility.Collapsed;
            SetColorOk.Visibility = Visibility.Collapsed;
            LabelText.Visibility = Visibility.Collapsed;
            SetMessageOk.Visibility = Visibility.Collapsed;
            ColorOptionsPanel.Visibility = Visibility.Collapsed;
        }

        private void SetColor_Click(object sender, RoutedEventArgs e)
        {
            ColorSelector.Visibility = Visibility.Visible;
            SetColorOk.Visibility = Visibility.Visible;
            LabelText.Visibility = Visibility.Collapsed;
            SetMessageOk.Visibility = Visibility.Collapsed;
            ColorOptionsPanel.Visibility = Visibility.Visible;

        }

        private void SetColorOk_Click(object sender, RoutedEventArgs e)
        {
            if (SetNoColor.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColor, Argument = null });
            else if (SetNoColorForRow.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColorForRow, Argument = null });
            else if (SetColorForValue.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColor, Argument = ColorConvertor.ConvertColorToString(ColorSelector.Color) });
            else if (SetColorForRow.IsChecked.Value) InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetColorForRow, Argument = ColorConvertor.ConvertColorToString(ColorSelector.Color) });
        }
        
        private void SetLabel_Click(object sender, RoutedEventArgs e)
        {
            ColorSelector.Visibility = Visibility.Collapsed;
            SetColorOk.Visibility = Visibility.Collapsed;
            LabelText.Visibility = Visibility.Visible;
            SetMessageOk.Visibility = Visibility.Visible;
            ColorOptionsPanel.Visibility = Visibility.Collapsed;
        }

        private void SetMessageOk_Click(object sender, RoutedEventArgs e)
        {
            InteractiveCommand?.Invoke(this, new InteractiveCommandEventArgs { Command = InteractiveOperations.SetLabel, Argument = string.IsNullOrWhiteSpace(LabelText.Text) ? null : LabelText.Text });
        }
    }

    public class InteractiveCommandEventArgs
    {
        public InteractiveOperations Command;
        public object Argument;
        public bool yField;
    }
}