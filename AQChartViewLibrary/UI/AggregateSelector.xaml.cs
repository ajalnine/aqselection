﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQReportingLibrary;
using AQBasicControlsLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class AggregateSelector
    {
        public delegate void AggregateChangedDelegate(object sender, AggregateChangedEventArgs e);

        private Aggregate _currentAggregate;

        private string _currentAggregateDescription;

        public AggregateSelector()
        {
            InitializeComponent();
            SetCurrentAggregate(Aggregate.Percent);
        }

        public event AggregateChangedDelegate AggregateChanged;

        public Aggregate GetCurrentAggregate()
        {
            return _currentAggregate;
        }

        public string GetCurrentAggregateDescription()
        {
            return _currentAggregateDescription;
        }

        public void SetCurrentAggregate(Aggregate rcct)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcct.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentAggregate = rcct;
            _currentAggregateDescription = (rb.Content as TextBlock).Text;
        }

        public void EnableNumericAggregateSelection(bool isEnabled)
        {
            var allowed = new string[] {"Percent", "N", "PercentInCategory"};
            foreach (var button in ButtonPanel.Children)
                if (!allowed.Contains((button as TextImageRadioButtonBase).Name)) (button as TextImageRadioButtonBase).IsEnabled = isEnabled;
        }


        public static string GetDescription(Aggregate rca)
        {
            switch (rca)
            {
                case Aggregate.Percent:
                    return "%";
                case Aggregate.PercentInCategory:
                    return "% в слое";
                case Aggregate.N:
                    return "Число";
                case Aggregate.Sum:
                    return "Сумма";
                case Aggregate.Min:
                    return "Min";
                case Aggregate.Max:
                    return "Max";
                case Aggregate.Mean:
                    return "Среднее";
                case Aggregate.Median:
                    return "Медиана";
                case Aggregate.First:
                    return "Первое значение";
                case Aggregate.Last:
                    return "Последнее значение";
                case Aggregate.StDev:
                    return "СКО";
                case Aggregate.Single:
                    return String.Empty;
            }
            return string.Empty;
        }
        public void RefreshUI()
        {
            SetCurrentAggregate(_currentAggregate);
        }

        private void Aggregate_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentAggregate = (Aggregate) Enum.Parse(typeof (Aggregate), rb.Name, true);
            _currentAggregateDescription = rb.Tag?.ToString();
            AggregateChanged?.Invoke(this,
                new AggregateChangedEventArgs
                {
                    Aggregate = _currentAggregate,
                    HistogramDescription = _currentAggregateDescription
                });
        }
    }

    public class AggregateChangedEventArgs : EventArgs
    {
        public Aggregate Aggregate { get; set; }
        public string HistogramDescription { get; set; }
    }
}