﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class RegressionSelector
    {
        public delegate void RegressionChangedDelegate(object sender, RegressionChangedEventArgs e);

        private ReportChartRegressionViewType _currentRegression;
        private ReportChartRegressionStepwiseType _currentStepwiseType;
        private ReportChartRegressionModelQuality _currentModelQuality;
        private ReportChartRangeStyle _currentStyle;
        private ReportChartLayerMode _currentLayerMode;
        private ReportChartRegressionOptions _currentRegressionOptions;

        public RegressionSelector()
        {
            InitializeComponent();
            SetCurrentRegression(ReportChartRegressionViewType.PredictedObserved);
            SetCurrentSmoothStyle(ReportChartRangeStyle.Thick);
            SetCurrentLayerMode(ReportChartLayerMode.Single);
            SetCurrentStepwiseType(ReportChartRegressionStepwiseType.All);
            SetCurrentModelQuality(ReportChartRegressionModelQuality.BIC);
            SetCurrentRegressionOptions(ReportChartRegressionOptions.OnlyFullCases | ReportChartRegressionOptions.OnlyRed);
            RefreshButtonsAvailability();
            AttentionMarkFull.Foreground = new SolidColorBrush(Colors.Transparent);
            AttentionMarkGenetic.Foreground = new SolidColorBrush(Colors.Transparent);
        }

        public event RegressionChangedDelegate RegressionChanged;

        public ReportChartRegressionStepwiseType GetCurrentStepwiseType()
        {
            return _currentStepwiseType;
        }

        public void SetAttention(int rows, int fields)
        {
            var warningFull = rows * fields > 1e4 || fields >= 10 || rows > 2000;
            var warningGenetic = rows * fields > 1e5 || fields >= 15 || rows > 5000;
            AttentionMarkFull.Foreground = new SolidColorBrush(warningFull? Color.FromArgb(0xff, 0xff, 0x00, 0x00) : Colors.Transparent);
            AttentionMarkGenetic.Foreground = new SolidColorBrush(warningGenetic ? Color.FromArgb(0xff, 0xff, 0x00, 0x00) : Colors.Transparent);
        }

        public ReportChartRegressionModelQuality GetCurrentModelQuality()
        {
            return _currentModelQuality;
        }

        public ReportChartRegressionOptions GetCurrentRegressionOptions()
        {
            return _currentRegressionOptions;
        }

        public ReportChartRegressionViewType GetCurrentRegression()
        {
            return _currentRegression   ;
        }

        public ReportChartRangeStyle GetCurrentSmoothStyle()
        {
            return _currentStyle;
        }

        public ReportChartLayerMode GetCurrentLayerMode()
        {
            return _currentLayerMode;
        }

        public void SetCurrentRegression(ReportChartRegressionViewType rcrvt)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcrvt.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentRegression = rcrvt;
        }

        public void SetCurrentModelQuality(ReportChartRegressionModelQuality rcrmq)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcrmq.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentModelQuality = rcrmq;
        }

        public void SetCurrentStepwiseType(ReportChartRegressionStepwiseType rcrst)
        {
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children
                              where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == rcrst.ToString()
                              select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
            _currentStepwiseType = rcrst;
            RefreshButtonsAvailability();
        }

        public void SetCurrentRegressionOptions(ReportChartRegressionOptions options)
        {
            if ((options & ReportChartRegressionOptions.OnlyRed) == ReportChartRegressionOptions.OnlyRed) OnlyRed.IsChecked = true;
            if ((options & ReportChartRegressionOptions.ExternalControl) == ReportChartRegressionOptions.ExternalControl) ExternalControl.IsChecked = true;
            if ((options & ReportChartRegressionOptions.ExternalControlInterlaced) == ReportChartRegressionOptions.ExternalControlInterlaced) ExternalControlInterlaced.IsChecked = true;
            if ((options & ReportChartRegressionOptions.OnlyFullCases) == ReportChartRegressionOptions.OnlyFullCases) OnlyFullCases.IsChecked = true;
            if ((options & ReportChartRegressionOptions.FillEmpty) == ReportChartRegressionOptions.FillEmpty) FillEmpty.IsChecked = true;
            if ((options & ReportChartRegressionOptions.Ridge) == ReportChartRegressionOptions.Ridge) Ridge.IsChecked = true;
            if ((options & ReportChartRegressionOptions.NonLinearLn) == ReportChartRegressionOptions.NonLinearLn) NonLinearLn.IsChecked = true;
            if ((options & ReportChartRegressionOptions.NonLinear2) == ReportChartRegressionOptions.NonLinear2) NonLinear2.IsChecked = true;
            if ((options & ReportChartRegressionOptions.NonLinear3) == ReportChartRegressionOptions.NonLinear3) NonLinear3.IsChecked = true;
            if ((options & ReportChartRegressionOptions.NonLinearSqrt) == ReportChartRegressionOptions.NonLinearSqrt) NonLinearSqrt.IsChecked = true;
            _currentRegressionOptions = options;
        }

        public void RefreshUI()
        {
            SetCurrentRegression(_currentRegression);
            SetCurrentSmoothStyle(_currentStyle);
            SetCurrentLayerMode(_currentLayerMode);
            SetCurrentModelQuality(_currentModelQuality);
            SetCurrentStepwiseType(_currentStepwiseType);
            SetCurrentRegressionOptions(_currentRegressionOptions);
            RefreshButtonsAvailability();
        }

        private void RegressionChartButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase) sender;
            _currentRegression =
                (ReportChartRegressionViewType) Enum.Parse(typeof (ReportChartRegressionViewType), rb.Name, true);
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = new[] {ReportChartRegressionViewType.ResultsInTable, ReportChartRegressionViewType.RMatrix }.Contains(_currentRegression), Options = _currentRegressionOptions });
        }

        private void RegressionModelButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentModelQuality =
                (ReportChartRegressionModelQuality)Enum.Parse(typeof(ReportChartRegressionModelQuality), rb.Name, true);
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = false, Options = _currentRegressionOptions });
        }

        private void RegressionStepWiseButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentStepwiseType =
                (ReportChartRegressionStepwiseType)Enum.Parse(typeof(ReportChartRegressionStepwiseType), rb.Name, true);
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = false, Options = _currentRegressionOptions });
            RefreshButtonsAvailability();
        }

        private void RefreshButtonsAvailability()
        {
            var enabled = _currentStepwiseType != ReportChartRegressionStepwiseType.All;
            R.IsEnabled = enabled;
            SE.IsEnabled = enabled;
            AIC.IsEnabled = enabled;
            BIC.IsEnabled = enabled;
        }

        public void SetCurrentSmoothStyle(ReportChartRangeStyle style)
        {
            _currentStyle = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        public void SetCurrentLayerMode(ReportChartLayerMode style)
        {
            _currentLayerMode = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void Style_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentStyle = (ReportChartRangeStyle)Enum.Parse(typeof(ReportChartRangeStyle), rb.Name, true);
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = true, Options = _currentRegressionOptions });
        }

        private void Layer_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentLayerMode = (ReportChartLayerMode)Enum.Parse(typeof(ReportChartLayerMode), rb.Name, true);
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = false, Options = _currentRegressionOptions });
        }

        private void RegressionOptionButton_Click(object sender, RoutedEventArgs e)
        {
            var button = ((TextImageToggleButtonBase) sender);
            if (button.Name == "ExternalControlInterlaced") ExternalControl.IsChecked = false;
            if (button.Name == "ExternalControl") ExternalControlInterlaced.IsChecked = false;
            if (button.Name == "FillEmpty") OnlyFullCases.IsChecked = false;
            if (button.Name == "OnlyFullCases") FillEmpty.IsChecked = false;
            UpdateLayout();
            var isChecked = button.IsChecked;
            if (isChecked == null) return;
            var options = ReportChartRegressionOptions.None;
            if (OnlyRed.IsChecked != null && OnlyRed.IsChecked.Value) options |= ReportChartRegressionOptions.OnlyRed;
            if (ExternalControl.IsChecked != null && ExternalControl.IsChecked.Value) options |= ReportChartRegressionOptions.ExternalControl;
            if (ExternalControlInterlaced.IsChecked != null && ExternalControlInterlaced.IsChecked.Value) options |= ReportChartRegressionOptions.ExternalControlInterlaced;
            if (OnlyFullCases.IsChecked != null && OnlyFullCases.IsChecked.Value) options |= ReportChartRegressionOptions.OnlyFullCases;
            if (FillEmpty.IsChecked != null && FillEmpty.IsChecked.Value) options |= ReportChartRegressionOptions.FillEmpty;
            if (Ridge.IsChecked != null && Ridge.IsChecked.Value) options |= ReportChartRegressionOptions.Ridge;
            if (NonLinearLn.IsChecked != null && NonLinearLn.IsChecked.Value) options |= ReportChartRegressionOptions.NonLinearLn;
            if (NonLinear2.IsChecked != null && NonLinear2.IsChecked.Value) options |= ReportChartRegressionOptions.NonLinear2;
            if (NonLinear3.IsChecked != null && NonLinear3.IsChecked.Value) options |= ReportChartRegressionOptions.NonLinear3;
            if (NonLinearSqrt.IsChecked != null && NonLinearSqrt.IsChecked.Value) options |= ReportChartRegressionOptions.NonLinearSqrt;
            _currentRegressionOptions = options;
            if (RegressionChanged != null)
                RegressionChanged.Invoke(this, new RegressionChangedEventArgs { RegressionMode = _currentRegression, SmoothStyle = _currentStyle, LayersMode = _currentLayerMode, ModelQuality = _currentModelQuality, StepwiseType = _currentStepwiseType, OnlyStyleChanged = false, Options = _currentRegressionOptions });
        }
    }

    public class RegressionChangedEventArgs : EventArgs
    {
        public ReportChartRegressionViewType RegressionMode { get; set; }
        public ReportChartRangeStyle SmoothStyle { get; set; }
        public ReportChartLayerMode LayersMode { get; set; }
        public ReportChartRegressionStepwiseType StepwiseType { get; set; }
        public ReportChartRegressionModelQuality ModelQuality { get; set; }
        public ReportChartRegressionOptions Options { get; set; }
        public bool OnlyStyleChanged { get; set; }
    }
}