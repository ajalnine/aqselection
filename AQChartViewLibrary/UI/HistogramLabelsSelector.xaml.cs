﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQBasicControlsLibrary;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class HistogramLabelsSelector
    {
        public delegate void HistogramLabelsChangedDelegate(object sender, HistogramLabelsChangedEventArgs e);
        private ReportChartColumnsLabelsType _currentLabelType;

        public HistogramLabelsSelector()
        {
            InitializeComponent();
            SetCurrentColumnLabelsParameters(ReportChartColumnsLabelsType.None | ReportChartColumnsLabelsType.SmallOver | ReportChartColumnsLabelsType.LabelWideLimited);
        }

        public event HistogramLabelsChangedDelegate HistogramLabelsChanged;

        public ReportChartColumnsLabelsType GetCurrentShowText()
        {
            return _currentLabelType;
        }
        
        public void SetCurrentColumnLabelsParameters(ReportChartColumnsLabelsType showText)
        {
            SetCurrentShowText(showText);
        }

        private void SetCurrentShowText(ReportChartColumnsLabelsType showText)
        {
            LabelN.IsChecked = ((showText & ReportChartColumnsLabelsType.N) == ReportChartColumnsLabelsType.N);
            LabelPercent.IsChecked = ((showText & ReportChartColumnsLabelsType.Percent) == ReportChartColumnsLabelsType.Percent);
            LabelPercentInGroup.IsChecked = ((showText & ReportChartColumnsLabelsType.PercentInGroup) == ReportChartColumnsLabelsType.PercentInGroup);
            LabelScaled.IsChecked = ((showText & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled);
            LabelNumberOfCases.IsChecked = ((showText & ReportChartColumnsLabelsType.NumberOfCases) == ReportChartColumnsLabelsType.NumberOfCases);
            LabelLayer.IsChecked = ((showText & ReportChartColumnsLabelsType.Layer) == ReportChartColumnsLabelsType.Layer);
            LabelTotal.IsChecked = ((showText & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total);
            LabelTotalPercent.IsChecked = ((showText & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent);
            LabelConnected.IsChecked = ((showText & ReportChartColumnsLabelsType.Connected) == ReportChartColumnsLabelsType.Connected);
            LabelSmallDefault.IsChecked = ((showText & ReportChartColumnsLabelsType.SmallDefault) == ReportChartColumnsLabelsType.SmallDefault);
            LabelSmallSkip.IsChecked = ((showText & ReportChartColumnsLabelsType.SmallSkip) == ReportChartColumnsLabelsType.SmallSkip);
            LabelSmallOver.IsChecked = ((showText & ReportChartColumnsLabelsType.SmallOver) == ReportChartColumnsLabelsType.SmallOver);
            LabelWideLimited.IsChecked = ((showText & ReportChartColumnsLabelsType.LabelWideLimited) == ReportChartColumnsLabelsType.LabelWideLimited);
            _currentLabelType = showText;
        }

        public void RefreshUI()
        {
            SetCurrentColumnLabelsParameters(_currentLabelType);
        }

        private void ShowTextButton_Click(object sender, RoutedEventArgs e)
        {
            var isChecked = sender is TextImageToggleButtonBase 
                ? ((TextImageToggleButtonBase)sender).IsChecked
                : ((TextImageRadioButtonBase)sender).IsChecked;
            if (isChecked == null) return;
            var currentLabelType = ReportChartColumnsLabelsType.None;
            if (LabelN.IsChecked != null && LabelN.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.N;
            if (LabelPercent.IsChecked != null && LabelPercent.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.Percent;
            if (LabelPercentInGroup.IsChecked != null && LabelPercentInGroup.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.PercentInGroup;
            if (LabelScaled.IsChecked != null && LabelScaled.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.Scaled;
            if (LabelNumberOfCases.IsChecked != null && LabelNumberOfCases.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.NumberOfCases;
            if (LabelLayer.IsChecked != null && LabelLayer.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.Layer;
            if (LabelTotal.IsChecked != null && LabelTotal.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.Total;
            if (LabelTotalPercent.IsChecked != null && LabelTotalPercent.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.TotalPercent;
            if (LabelConnected.IsChecked != null && LabelConnected.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.Connected;
            if (LabelWideLimited.IsChecked != null && LabelWideLimited.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.LabelWideLimited;

            if (LabelSmallDefault.IsChecked != null && LabelSmallDefault.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.SmallDefault;
            if (LabelSmallOver.IsChecked != null && LabelSmallOver.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.SmallOver;
            if (LabelSmallSkip.IsChecked != null && LabelSmallSkip.IsChecked.Value) currentLabelType |= ReportChartColumnsLabelsType.SmallSkip;

            _currentLabelType = currentLabelType;
            HistogramLabelsChanged?.Invoke(this, new HistogramLabelsChangedEventArgs { ShowText = _currentLabelType });
        }

        public void SetModeAvailability(bool pareto, bool stacked)
        {
            LabelN.IsEnabled = true;
            LabelPercent.IsEnabled = true;
            LabelPercentInGroup.IsEnabled = !pareto;
            LabelScaled.IsEnabled = true;
            LabelNumberOfCases.IsEnabled = !pareto;
            LabelLayer.IsEnabled = !pareto;
            LabelTotal.IsEnabled = !pareto && stacked;
            LabelConnected.IsEnabled = !pareto && stacked;
            LabelTotalPercent.IsEnabled = !pareto && stacked;
            LabelSmallDefault.IsEnabled = true;
            LabelSmallOver.IsEnabled = true;
            LabelSmallSkip.IsEnabled = true;
            LabelWideLimited.IsEnabled = true;
        }
    }

    public class HistogramLabelsChangedEventArgs : EventArgs
    {
        public ReportChartColumnsLabelsType ShowText { get; set; }
    }
}