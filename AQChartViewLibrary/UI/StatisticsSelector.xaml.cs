﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQBasicControlsLibrary;
using AQReportingLibrary;

namespace AQChartViewLibrary.UI
{
    public partial class StatisticsSelector
    {
        public delegate void StatisticsChangedDelegate(object sender, StatisticsChangedEventArgs e);
        private Statistics _currentStatistics;
        private ReportChartLayerMode _currentLayerMode;

        public StatisticsSelector()
        {
            InitializeComponent();
            SetCurrentLayerMode(ReportChartLayerMode.Extended);
            SetStatistics(Statistics.Basic | Statistics.N | Statistics.SkipZeroCasesOption);
        }

        public event StatisticsChangedDelegate StatisticsChanged;

        public Statistics GetStatistics()
        {
            return _currentStatistics;
        }

        public ReportChartLayerMode GetCurrentLayerMode()
        {
            return _currentLayerMode;
        }

        public void SetStatistics(Statistics statistics)
        {
            N.IsChecked = (statistics & Statistics.N) > 0;
            Sum.IsChecked = (statistics & Statistics.Sum) > 0;
            Basic.IsChecked = (statistics & Statistics.Basic) > 0;
            Median.IsChecked = (statistics & Statistics.Median) > 0;
            Dispersion.IsChecked = (statistics & Statistics.Dispersion) > 0;
            Probability.IsChecked = (statistics & Statistics.Probability) > 0;
            MinMax.IsChecked = (statistics & Statistics.MinMax) > 0;
            Percents.IsChecked = (statistics & Statistics.Percents) > 0;
            Moment.IsChecked = (statistics & Statistics.Moment) > 0;
            ZTest.IsChecked = (statistics & Statistics.ZTest) > 0;
            Ranges.IsChecked = (statistics & Statistics.Ranges) > 0;
            Interval.IsChecked = (statistics & Statistics.Interval) > 0;
            SixSigma.IsChecked = (statistics & Statistics.SixSigma) > 0;
            SkipZeroCasesOption.IsChecked = (statistics & Statistics.SkipZeroCasesOption) > 0;
            ProcessIndexes.IsChecked = (statistics & Statistics.ProcessIndexes) > 0;
            _currentStatistics = statistics;
        }

        public void RefreshUI()
        {
            SetStatistics(_currentStatistics);
            SetCurrentLayerMode(_currentLayerMode);
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            var isChecked = ((TextImageToggleButtonBase)sender).IsChecked;
            if (isChecked == null) return;
            var s = Statistics.None;
            if (Basic.IsChecked != null && Basic.IsChecked.Value) s |= Statistics.Basic;
            if (N.IsChecked != null && N.IsChecked.Value) s |= Statistics.N;
            if (Sum.IsChecked != null && Sum.IsChecked.Value) s |= Statistics.Sum;
            if (Moment.IsChecked != null && Moment.IsChecked.Value) s |= Statistics.Moment;
            if (ZTest.IsChecked != null && ZTest.IsChecked.Value) s |= Statistics.ZTest;
            if (Ranges.IsChecked != null && Ranges.IsChecked.Value) s |= Statistics.Ranges;
            if (ProcessIndexes.IsChecked != null && ProcessIndexes.IsChecked.Value) s |= Statistics.ProcessIndexes;
            if (Percents.IsChecked != null && Percents.IsChecked.Value) s |= Statistics.Percents;
            if (Median.IsChecked != null && Median.IsChecked.Value) s |= Statistics.Median;
            if (MinMax.IsChecked != null && MinMax.IsChecked.Value) s |= Statistics.MinMax;
            if (Dispersion.IsChecked != null && Dispersion.IsChecked.Value) s |= Statistics.Dispersion;
            if (Probability.IsChecked != null && Probability.IsChecked.Value) s |= Statistics.Probability;
            if (Interval.IsChecked != null && Interval.IsChecked.Value) s |= Statistics.Interval;
            if (SixSigma.IsChecked != null && SixSigma.IsChecked.Value) s |= Statistics.SixSigma;
            if (SkipZeroCasesOption.IsChecked != null && SkipZeroCasesOption.IsChecked.Value) s |= Statistics.SkipZeroCasesOption;

            _currentStatistics = s;
            StatisticsChanged?.Invoke(this, new StatisticsChangedEventArgs { Statistics = _currentStatistics, LayerMode = _currentLayerMode });
        }

        private void Layer_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentLayerMode = (ReportChartLayerMode)Enum.Parse(typeof(ReportChartLayerMode), rb.Name, true);
            StatisticsChanged?.Invoke(this, new StatisticsChangedEventArgs { Statistics = _currentStatistics, LayerMode = _currentLayerMode });
        }
        public void SetCurrentLayerMode(ReportChartLayerMode style)
        {
            _currentLayerMode = style;
            var rb = (from r in StylePanel.Children
                      where (r is TextImageRadioButtonBase) && (r as TextImageRadioButtonBase).Name == style.ToString()
                      select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void ResetSelection_OnClick(object sender, RoutedEventArgs e)
        {
            SetStatistics(Statistics.Basic | Statistics.N);
            StatisticsChanged?.Invoke(this, new StatisticsChangedEventArgs { Statistics = _currentStatistics, LayerMode = _currentLayerMode });
        }
    }

    public class StatisticsChangedEventArgs : EventArgs
    {
        public Statistics Statistics { get; set; }
        public ReportChartLayerMode LayerMode { get; set; }
    }
}