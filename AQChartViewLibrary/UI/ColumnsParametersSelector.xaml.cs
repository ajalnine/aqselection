﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using AQReportingLibrary;
using AQBasicControlsLibrary;
using System.Windows.Media;

namespace AQChartViewLibrary.UI
{
    public partial class ColumnsParametersSelector 
    {
        public delegate void ColumnsParametersChangedDelegate(object sender, ColumnsParametersChangedEventArgs e);
        private ReportChartColumnDiagramType _currentDiagramType;
        private ReportChartColumnOrder _currentOrder;
        private ReportChartDrawParts _currentDrawParts;

        public ColumnsParametersSelector()
        {
            InitializeComponent();
            SetCurrentColumnsParameters(ReportChartColumnDiagramType.Generic, ReportChartColumnOrder.ByGroupsAsc, ReportChartDrawParts.Solid);
            SetOrientation(Orientation.Vertical);
        }

        public event ColumnsParametersChangedDelegate ColumnsParametersChanged;

        public ReportChartColumnDiagramType GetCurrentColumnDiagramType()
        {
            return _currentDiagramType;
        }

        public void SetOrientation(Orientation orientation)
        {
            var angle = (orientation == Orientation.Horizontal) ? 90 : 0;
            var rt = new RotateTransform { Angle = angle, CenterX = 20, CenterY = 20 };
            Generic.RenderTransform = rt;
            Normed.RenderTransform = rt;
            ByCategory.RenderTransform = rt;
            Layers.RenderTransform = rt;
            Opposite.RenderTransform = rt;
        }


        public ReportChartColumnOrder GetCurrentColumnOrder()
        {
            return _currentOrder;
        }

        public ReportChartDrawParts GetCurrentDrawParts()
        {
            return _currentDrawParts;
        }

        public void SetCurrentColumnsParameters(ReportChartColumnDiagramType diagramType, ReportChartColumnOrder order, ReportChartDrawParts drawParts)
        {
            SetCurrentDiagramType(diagramType);
            SetCurrentOrder(order);
            SetCurrentDrawParts(drawParts);
        }

        private void SetCurrentDiagramType(ReportChartColumnDiagramType diagramType)
        {
            _currentDiagramType = diagramType;
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase)r).Name == diagramType.ToString() select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void SetCurrentOrder(ReportChartColumnOrder order)
        {
            _currentOrder = order;
            TextImageRadioButtonBase rb = (from r in ButtonPanel.Children where (r is TextImageRadioButtonBase) && ((TextImageRadioButtonBase)r).Name == order.ToString() select r as TextImageRadioButtonBase).FirstOrDefault();
            if (rb != null) rb.IsChecked = true;
        }

        private void SetCurrentDrawParts(ReportChartDrawParts drawParts)
        {
            _currentDrawParts = drawParts;
            Column.IsChecked = (drawParts & ReportChartDrawParts.Solid) > 0;
            Line.IsChecked = (drawParts & ReportChartDrawParts.Line) > 0;
            Marker.IsChecked = (drawParts & ReportChartDrawParts.Marker) > 0;
        }

        public void RefreshUI()
        {
            SetCurrentColumnsParameters(_currentDiagramType, _currentOrder, _currentDrawParts);
        }

        private void OrderButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentOrder = (ReportChartColumnOrder)Enum.Parse(typeof(ReportChartColumnOrder), rb.Name, true);
            ColumnsParametersChanged?.Invoke(this, new ColumnsParametersChangedEventArgs { DiagramType = _currentDiagramType, Order = _currentOrder, DrawParts = _currentDrawParts});
        }
        private void DrawPartButton_Click(object sender, RoutedEventArgs e)
        {
            _currentDrawParts = ReportChartDrawParts.None;
            if (Column.IsChecked != null && Column.IsChecked.Value) _currentDrawParts |= ReportChartDrawParts.Solid;
            if (Line.IsChecked != null && Line.IsChecked.Value) _currentDrawParts |= ReportChartDrawParts.Line;
            if (Marker.IsChecked != null && Marker.IsChecked.Value) _currentDrawParts |= ReportChartDrawParts.Marker;
            
            ColumnsParametersChanged?.Invoke(this, new ColumnsParametersChangedEventArgs { DiagramType = _currentDiagramType, Order = _currentOrder, DrawParts = _currentDrawParts });
        }
        public void ShowOrderPanel(bool show)
        {
            OrderingPanel.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
        }

        private void DiagramTypeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (TextImageRadioButtonBase)sender;
            _currentDiagramType = (ReportChartColumnDiagramType)Enum.Parse(typeof(ReportChartColumnDiagramType), rb.Name, true);
            ColumnsParametersChanged?.Invoke(this, new ColumnsParametersChangedEventArgs { DiagramType = _currentDiagramType, Order = _currentOrder, DrawParts = _currentDrawParts });
        }
    }

    public class ColumnsParametersChangedEventArgs : EventArgs
    {
        public ReportChartColumnDiagramType DiagramType { get; set; }
        public ReportChartColumnOrder Order { get; set; }
        public ReportChartDrawParts DrawParts { get; set; }
    }
}