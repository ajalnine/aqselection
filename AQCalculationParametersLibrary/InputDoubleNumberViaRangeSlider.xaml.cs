﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using AQCalculationsLibrary;

namespace AQCalculationParametersLibrary
{
    public partial class InputDoubleNumberViaRangeSlider : UserControl, IAQParameterView
    {
        private Parameters p;
        private InputParameter i;

        public InputDoubleNumberViaRangeSlider()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            double CurrentValue = 0, CurrentValue2 = 100;
            CurrentValue = (i != null && (i.CurrentValue >= i.MinValue && i.CurrentValue <= i.MaxValue)) ? i.CurrentValue : p.DefaultStart;
            CurrentValue2 = (i != null && (i.CurrentValue2 >= i.MinValue && i.CurrentValue2 <= i.MaxValue && i.CurrentValue2> i.CurrentValue)) ? i.CurrentValue2 : p.DefaultEnd;
            i = new InputParameter() { Caption = p.Caption, MinAllowedValue = p.GreenStart, MaxAllowedValue = p.GreenEnd,  CheckIsEnabled = false, MinValue = p.Min, MaxValue = p.Max, Step = p.Step, Unlocked = true, CurrentValue = CurrentValue, CurrentValue2 = CurrentValue2 };
            LayoutRoot.DataContext = i;
            this.UpdateLayout();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Result.Add(new InputDescription() { Name = p.VariableStart, Value = i.CurrentValue, Type = "Double" });
            Result.Add(new InputDescription() { Name = p.VariableEnd, Value = i.CurrentValue2, Type = "Double" });
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public const string InputName = "InputDoubleNumberViaRangeSlider";
            public string Caption;
            public string VariableStart;
            public string VariableEnd;
            public double Min;
            public double Max;
            public double GreenStart;
            public double GreenEnd;
            public double Step;
            public double DefaultStart;
            public double DefaultEnd;
        }
    }
}
