﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using AQCalculationsLibrary;

namespace AQCalculationParametersLibrary
{
    public partial class InputSingleNumberViaSlider : UserControl, IAQParameterView
    {
        private Parameters p;
        private InputParameter i;

        public InputSingleNumberViaSlider()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            double CurrentValue = 0;
            CurrentValue = (i != null && (i.CurrentValue >= i.MinValue && i.CurrentValue <= i.MaxValue)) ? i.CurrentValue : p.Default;
            i = new InputParameter() { Caption = p.Caption, CheckIsEnabled = false, MinValue = p.Min, MaxValue = p.Max, Step = p.Step, Unlocked = true, CurrentValue = CurrentValue };
            LayoutRoot.DataContext = i;
            this.UpdateLayout();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Result.Add(new InputDescription() { Name = p.Variable, Value = i.CurrentValue, Type = "Double" });
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public const string InputName = "InputSingleNumberViaSlider";
            public string Caption;
            public string Variable;
            public double Min;
            public double Max;
            public double Step;
            public double Default;
        }
    }
}
