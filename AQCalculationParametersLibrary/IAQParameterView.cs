﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;

namespace AQCalculationParametersLibrary
{
    public interface IAQParameterView
    {
        void Setup(string XMLParameters, DateRangePicker MainDateRangePicker);
        List<InputDescription> GetValues();
        bool Check();
    }
}
