﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQControlsLibrary;

namespace AQCalculationParametersLibrary
{
    public partial class InputListSQLSingle : IAQParameterView
    {
        private Parameters p;
        private List<ListData> KeyValues;
        private List<List<object>> SourceData;
        string Selected = string.Empty;
        bool IsFirstRun = true;
        private ConstructorService cs;
        private DateRangePicker ParentDateRange;

        public InputListSQLSingle()
        {
            cs = Services.GetConstructorService();
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            ParentDateRange = MainDateRangePicker;
            if (KeyValues != null) Selected = SetResultValue().Item1;
            p = ParseParameters(XMLParameters);
            Caption.Text = p.Caption;
            if (IsFirstRun) RefreshData();
        }

        private void RefreshData()
        {
            Content.UpdateLayout();
            ProgressIndicator.Visibility = Visibility.Visible;
            KeyValues = new List<ListData>();
            string SQL = @"set dateformat ymd 
                           declare @date1 datetime 
                           declare @date2 datetime
                           set @date1='" + ParentDateRange.From.Value.ToString("yyyy-MM-dd") + "'  set @date2='" + ParentDateRange.To.Value.ToString("yyyy-MM-dd") + "' \r\n" + p.SQL;

            cs.BeginGetRows(SQL, (iar) =>
            {
                SourceData = cs.EndGetRows(iar);
                KeyValues = (from i in SourceData select new ListData(i.First().ToString(), (string)i.Last().ToString())).ToList();
                this.Dispatcher.BeginInvoke(delegate ()
                {
                    ResultContent.ItemsSource = KeyValues;
                    ResultContent.UpdateLayout();
                    if (KeyValues.Any()) ResultContent.SelectedIndex = 0;
                    ResultContent.Focus();
                    ResultContent.UpdateLayout();
                    ProgressIndicator.Visibility = System.Windows.Visibility.Collapsed;
                    IsFirstRun = false;
                });
            }
            , null);
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Selected = SetResultValue().Item1;
            var SelectedCaption = SetResultValue().Item2;
            Result.Add(new InputDescription() { Name = p.Variable, Value = Selected, Type = "String" });
            Result.Add(new InputDescription() { Name = p.VariableCaption, Value = SelectedCaption, Type = "String" });
            return Result;
        }

        private Tuple<string, string> SetResultValue()
        {
            var a = ResultContent.SelectedItem as ListData;
            return new Tuple<string, string>(a?.Key, a?.Value);
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string InputName = "InputListSQL";
            public string SQL;
            public string Caption;
            public string Variable;
            public string VariableCaption;
        }

        public class ListData : INotifyPropertyChanged
        {
            private string key;
            public string Key
            {
                get { return key; }
                set
                {
                    key = value;
                    if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Key"));
                }
            }

            private string val;
            public string Value
            {
                get { return val; }
                set
                {
                    val = value;
                    if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Value"));
                }
            }

            
            public ListData(string key, string value)
            {
                Key = key;
                Value = value;
            }

            public ListData()
            {
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Selected = SetResultValue().Item1;
            RefreshData();
        }
    }
}
