﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Calculations
{
    public partial class Page
    {
        CommandCallBackDelegate _readySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { _readySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool _readyForAcceptSignalsProcessed;
        private int _pasteCounter = 1;

        #region Обработка сигналов

        public AQModuleDescription GetAQModuleDescription()
        {
            _aqmd.ShortDescription = MainCatalogueDataEditor.GetQueryName();
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            var ca = commandArgument as Dictionary<string, object>;
            switch (commandType)
            {
                case Command.InsertQuery:
                    if (ca != null )
                    {
                        InsertSteps(StepFactory.CreateSQLQuery(ca["SQL"].ToString(), ca["Name"].ToString()));
                        SetupDatePicker(ca);
                    }
                    break;

                case Command.InsertCalculation:
                    if (ca != null)
                    {
                        InsertSteps(ca["Calculation"] as List<Step>);
                        SetupDatePicker(ca);
                    }
                    break;

                case Command.InsertAndSaveCalculation:
                    if (ca != null)
                    {
                        InsertSteps(ca["Calculation"] as List<Step>);
                        SetupDatePicker(ca);
                        MainTabControl.SelectedIndex = 1;
                        MainCatalogueDataEditor.Focus();
                    }
                    break;

                case Command.InsertTable:
                    if (ca != null)
                    {
                        var steps = StepFactory.CreateTable(ca["Table"] as SLDataTable);
                        InsertSteps(steps);
                        SetupDatePicker(ca);
                    }
                    break;

                case Command.PasteToCalculation:
                    if (ca != null)
                    {
                        var fromBuffer = ClipboardDataHelper.GetSLDataTableFromClipboard(ca["Text"].ToString(), "Таблица" + (_pasteCounter > 1 ? _pasteCounter.ToString() : string.Empty));
                        if (fromBuffer == null) return;
                        InsertSteps(StepFactory.CreateTable(fromBuffer));
                        _pasteCounter++;
                    }
                    break;

                case Command.InsertTables:
                    if (ca != null)
                    {
                        var steps = new List<Step>();
                        var sources = ((List<SLDataTable>) ca["Tables"]).Where(a=>!a.TableName.StartsWith("Переменные"));
                        foreach (var t in sources) steps.AddRange( StepFactory.CreateTable(t));
                        InsertSteps(steps);
                        SetupDatePicker(ca);
                    }
                    break;

                case Command.InsertStoredCalculation:
                    _currentCatalogueID = (int)commandArgument;
                    MainCatalogueDataEditor.LoadCalculation(_currentCatalogueID);
                    break;

            }
            commandCallBack?.Invoke(commandType, commandArgument, null);
        }
        
        private void InsertSteps(List<Step> steps)
        {
            var task = new Task(new[] { "Анализ схемы" }, "Открытие расчета", TaskPanel);
            task.StartTask();
            SchemaPanel.Visibility = Visibility.Collapsed;
            MainCalculationView.InsertSteps(steps, CreateVariablesDataItem(), () =>
            {
                task.AdvanceProgress();
                SchemaPanel.Visibility = Visibility.Visible;
                CheckCalculationConsistency();
                RefreshInternalParameters();
                RefreshExternalParameters();
                UpdateSourcePanel();
            });
        }

        private void SetupDatePicker(Dictionary<string, object> ca)
        {
            if (ca == null) return;
            if (ca.ContainsKey("From")) MainDateRangePicker.From = (DateTime)ca["From"];
            if (ca.ContainsKey("To")) MainDateRangePicker.To = (DateTime)ca["To"];
        }

        #endregion
    }
}
