﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;
using AQCatalogueLibrary;
using AQControlsLibrary;

namespace SLConstructor_Calculations
{
    public partial class Page 
    {
        private int _currentCatalogueID = -1;
        private bool _newItem;
        
        #region Сохранение расчета

        private void SaveCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainCatalogueDataEditor.CheckName())
            {
                MessageBox.Show("Для сохранения выберите папку и введите наименование расчета");
                return;
            }

            var tibb = sender as TextImageButtonBase;
            if (tibb != null) _newItem = tibb.Tag.ToString() == "NewItem";

            var saveModuleTask = new Task(new [] { "Сохранение в каталог" }, "Сохранение расчета", TaskPanel);
            saveModuleTask.StartTask();
            var cd = MainCalculationView.GetCalculation();
            MainCatalogueDataEditor.SaveCalculation(cd.StepData, cd.Results, saveModuleTask, true, _newItem);
        }

        private void MainCatalogueDataEditor_CatalogueItemSaved(object o, CatalogueItemOperationEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var task = (Task) e.State;
                task.AdvanceProgress();
                _currentCatalogueID = ((ItemDescription) e.Cd).ID;
            });
        }
        #endregion

        #region Загрузка расчета

        private void MainCatalogueDataEditor_CatalogueItemLoaded(object o, CatalogueItemOperationEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                EnableSave(true);
                DeleteCalculation.IsEnabled = (((CalculationDescription)e.Cd).CanBeDeleted);
                InsertSteps(((CalculationDescription)e.Cd).StepData);
            });
        }
        #endregion

        #region Удаление расчета

        private void DeleteCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            var task = new Task(new[] { "Удаление\r\nиз каталога" }, "Удаление расчета", TaskPanel);
            task.StartTask();
            DeleteCalculation.IsEnabled = false;
            MainCatalogueDataEditor.DeleteQuery(task, true);
        }

        private void MainCatalogueDataEditor_CatalogueItemDeleted(object o, CatalogueItemOperationEventArgs e)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var task = (Task) e.State;
                task.AdvanceProgress();
                _currentCatalogueID = -1;
                DeleteCalculation.IsEnabled = false;
            });
        }
        #endregion
    }
}
