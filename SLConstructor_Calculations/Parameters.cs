﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCatalogueLibrary;
using AQCalculationParametersLibrary;

namespace SLConstructor_Calculations
{
    public partial class Page 
    {
        public List<InputDescription> GetParametersData()
        {
            return GetInputs(InternalParametersPanel).Union(GetInputs(ExternalParametersPanel)).ToList();
        }

        private IEnumerable<InputDescription> GetInputs(StackPanel parametersPanel)
        {
            if (parametersPanel == null) return null;
            var result = new List<InputDescription>();

            foreach (var c in parametersPanel.Children.Where(a => a is IAQParameterView).OfType<IAQParameterView>())
            {
                foreach (var v in c.GetValues().Where(v => !result.Select(a => a.Name).Distinct().Contains(v.Name)))
                {
                    result.Add(v);
                }
            }
            return result;
        }

        private void RefreshInternalParameters()
        {
            if (MainCalculationView.GetCalculation() == null) return;

            var ps = new ParametersScan();
            
            var ls = ps.GetFirstLevelParameters(MainCalculationView.GetCalculation().StepData);
            var count = 0;
            var controlCacheIndex = ls.Select(s => AddParameterView(s, InternalParametersPanel, count++)).ToList();
            var unusedControls = (from i in InternalParametersPanel.Children.Cast<UserControl>() where !controlCacheIndex.Contains(i.Tag.ToString()) select i).ToList();
            foreach (var u in unusedControls)InternalParametersPanel.Children.Remove(u);
            InternalParametersHeader.Visibility = (ExternalParametersPanel.Children.Count > 0 && InternalParametersPanel.Children.Count == 0) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void RefreshExternalParameters()
        {
            if (MainCalculationView.GetCalculation() == null) return;
            
            var ps = new ParametersScan();
            
            ps.GetAllChildParametersAsync(MainCalculationView.GetCalculation().StepData, (o, e) => Dispatcher.BeginInvoke(delegate
            {
                var count = 0;
                var controlCacheIndex = (from c in e.Found from s in c.CalcStep select AddParameterView(s, InternalParametersPanel, count++)).ToList();
                var unusedControls = (from i in InternalParametersPanel.Children.Cast<UserControl>() where !controlCacheIndex.Contains(i.Tag.ToString()) select i).ToList();
                foreach (var u in unusedControls) ExternalParametersPanel.Children.Remove(u);
                ExternalParametersHeader.Visibility = (ExternalParametersPanel.Children.Count > 0) ? Visibility.Visible : Visibility.Collapsed;
                InternalParametersHeader.Visibility = (ExternalParametersPanel.Children.Count > 0 && InternalParametersPanel.Children.Count == 0) ? Visibility.Collapsed : Visibility.Visible;
            }));
        }

        private string AddParameterView(Step s, StackPanel panel, int index)
        {
            var xml = s.ParametersXML;
            var cp = Regex.Match(xml, @"<Caption>([^<>]*)<", RegexOptions.IgnoreCase);
            if (!cp.Success) return null;
            var caption = cp.Groups[1].Captures[0].Value + index;

            var control = (from c in panel.Children.Cast<UserControl>() where c.Tag.ToString() == caption select c).FirstOrDefault();
            if (control == null)
            {
                var m = Regex.Match(xml, @"<InputName>([a-z]*)<", RegexOptions.IgnoreCase);
                if (!m.Success) return caption;
                var inputName = m.Groups[1].Captures[0].Value;
                var t = Type.GetType(
                    $"AQCalculationParametersLibrary.{inputName}, AQCalculationParametersLibrary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                if (t == null) return caption;
                var uc = Activator.CreateInstance(t) as UserControl;
                if (uc == null) return string.Empty;
                uc.Name = Guid.NewGuid().ToString();
                uc.Tag = caption;
                ((IAQParameterView)uc).Setup(xml, MainDateRangePicker);
                panel.UpdateLayout();
                panel.Children.Add(uc);
            }
            else
            {
                ((IAQParameterView)control).Setup(xml, MainDateRangePicker);
            }
            return caption;
        }
    }
}
