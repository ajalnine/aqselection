﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLConstructor_Calculations
{
    public partial class Page :IAQModule
    {
        private CalculationService _cs;
        private readonly AQModuleDescription _aqmd;
        
        public Page()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name, ShortDescription = MainCatalogueDataEditor.GetQueryName(), URL = "SLConstructor_Calculations.Page" };
            Loader.TabControlSizeChanged += Loader_TabControlSizeChanged;
        }

        private void Loader_TabControlSizeChanged(object o, TabSizeChangedEventArgs e)
        {
            LayoutRoot.Width = (e.NewSize.Width > 25) ? e.NewSize.Width - 25 : 0;
            MainSlidePanel.Height = Loader.MainTabSize.Height - 150;
            SlidePanelBorder.MaxWidth = LayoutRoot.Width - 80;
        }

        private void CheckCalculationConsistency()
        {
            var message = MainCalculationView.CheckOperations();
            ErrorMessage.Text = message;
            var noErrors = string.IsNullOrEmpty(message);
            EnableSave(noErrors);
        }

        private void ShowCalculationResults()
        {
            var results = MainCalculationView.GetResults() ?? new List<DataItem>();
            AvailableSourcesView.ItemsSource = from r in results where r != null select r;
            ResultTablesView.ItemsSource = null;
            ResultTablesView.UpdateLayout();
            ResultTablesView.ItemsSource = results;
            ResultTablesView.UpdateLayout();
        }
        
        private void EnableSave(bool isEnabled)
        {
            TabSaveCalculation.IsEnabled = isEnabled;
            OperationPanel.IsEnabled = isEnabled;
            SaveAsCalculationButton.IsEnabled = isEnabled;
            SaveCalculationButton.IsEnabled = isEnabled;
            DeleteCalculation.IsEnabled = isEnabled;
        }

        private void EnableReport(bool isEnabled)
        {
            Report.IsEnabled = isEnabled;
        }

        private void UpdateSourcePanel()
        {
            if (MainCatalogueDataEditor.CurrentCatalogueItem == null) return;

            CalculationInfo.Blocks.Clear();
            var p1 = new Paragraph();
            p1.Inlines.Add(new Run { Text = MainCatalogueDataEditor.GetQueryName(), FontWeight = FontWeights.SemiBold });
            p1.Inlines.Add(new LineBreak());
            p1.Inlines.Add(new Run { Text = MainCatalogueDataEditor.GetQueryDescription(), Foreground = new SolidColorBrush(Colors.Gray), FontSize = 10 });
            p1.Inlines.Add(new LineBreak());
            CalculationInfo.Blocks.Add(p1);
        }
        
        private void SLConstructor_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Width = Loader.MainTabSize.Width - 25;
            MainSlidePanel.Height = Loader.MainTabSize.Height - 150;
            LayoutRoot.Visibility = Visibility.Visible;
            if (_readyForAcceptSignalsProcessed) return;
            _cs = Services.GetCalculationService();

            MainCalculationView.InsertSteps(null, CreateVariablesDataItem(), () =>
            {
                CheckCalculationConsistency();
                ShowCalculationResults();
            });

            MainCatalogueDataEditor.SetToEmptySelection();
            ReadyForAcceptSignals?.Invoke(this, new ReadyForAcceptSignalsEventArg { CommandCallBack = _readySignalCallBack });
            _readyForAcceptSignalsProcessed = true;
        }

        private List<DataItem> CreateVariablesDataItem()
        {
            var results = new List<DataItem>();
            var vars = new DataItem { Name = "Переменные", DataItemType = DataType.Variables, Fields = new List<DataField>(), TableName = "Переменные" };
            vars.Fields.Add(new DataField { Name = "От", DataType = "DateTime" });
            vars.Fields.Add(new DataField { Name = "До", DataType = "DateTime" });
            results.Add(vars);
            return results;
        }
        
        private void RefreshParametersButton_Click(object sender, RoutedEventArgs e)
        {
            InternalParametersPanel.Children.Clear();
            ExternalParametersPanel.Children.Clear();
            RefreshInternalParameters();
            RefreshExternalParameters();
        }

        private void MainCalculationView_OnDataFlowChanged(object o, GenericEventArgs e)
        {
            Refresh();
        }

        private void Refresh()
        {
            CheckCalculationConsistency();
            EnableReport(MainCalculationView.GetCalculation().StepData.Any(a=>a.Group=="Отчет"));
            RefreshInternalParameters();
            RefreshExternalParameters();
            ShowCalculationResults();
        }

        private void MainCalculationView_OnLoaded(object sender, RoutedEventArgs e)
        {
            MainCalculationView.InsertSteps(null, CreateVariablesDataItem(), null);
        }

        private void MainCalculationView_OnBlockSave(object o, GenericEventArgs e)
        {
            EnableSave((bool)e.CustomData);
        }

        private void ScaleSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (EditorWindowTransform == null) return;
            var s = sender as Slider;
            if (s != null)
            {
                EditorWindowTransform.ScaleX = s.Value;
                EditorWindowTransform.ScaleY = s.Value;
            }
            SchemeTab.UpdateLayout();
        }
        private void SchemaPanel_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var gt = LayoutRoot.TransformToVisual(MainCalculationView);
            MainCalculationView.ScrollOffset = gt.Transform(new Point(0, -50)).Y;
        }
    }
}