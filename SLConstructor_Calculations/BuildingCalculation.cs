﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;


namespace SLConstructor_Calculations
{
    public partial class Page
    {
        private Task _loadModuleTask;
        private bool _calculationInLoadingProcess;
        private List<OperationView> _loadedOperationViews;

        private void SetEditorToCurrentCalculation()
        {
            if (_currentCalculation.StepData.Count == 0)
            {
                MessageBox.Show("Расчет поврежден", "Ошибка", MessageBoxButton.OK);
                return;
            }
            SchemaPanel.Visibility = Visibility.Collapsed;
            _loadModuleTask = new Task(new[] { "Загрузка\r\nмодулей", "Инициализация\r\nмодулей", "Построение расчета" }, "Загрузка расчета", TaskPanel);
            _loadModuleTask.SetState(0, TaskState.Processing);
            _calculationInLoadingProcess = true;
            _loadedOperationViews = CreateOperationViews(_currentCalculation.StepData);
            _loadModuleTask.SetState(0, TaskState.Ready);
            _loadModuleTask.SetState(1, TaskState.Processing);

            TraceDataFlow();
        }

        private List<OperationView> CreateOperationViews(List<Step> stepdata)
        {
            var result = new List<OperationView>();
            foreach (var s in stepdata)
            {
                var ov = new OperationView
                {
                    AutoEdit = false,
                    OperationBorderBrush = OperationsToolBox.GetBackgroundByGroupName(s.Group),
                    PreviousResultDataItems = CurrentResults,
                    PresentedStep = s,
                    BoundModule = new CalculationStep
                    {
                        Calculator = s.Calculator,
                        EditorAssembly = s.EditorAssemblyPath,
                        ImageUrl =
                            s.ImagePath.Replace("/AQSelection/Images/", "/AQResources;component/Images/"),
                        Name = s.Name,
                        Group = StepGroupConvert.NameToGroup(s.Group)
                    },
                    StepReadyToSave = true
                };
                ov.StateChanged += OperationView_StateChanged;
                ov.StepAboutToDelete += OperationView_StepAboutToDelete;
                ov.RequestForPreview += OperationView_RequestForPreview;
                ov.RequestForData += OperationView_RequestForData;
                ov.AskForContextMenu += AskForContextMenu_Invoked;
                ov.GeometryChanged += OperationView_GeometryChanged;
                ov.DataUpdated += OperationView_DataUpdated;
                ov.ControlPreprocess();
                result.Add(ov);
            }
            return result;
        }

        private void FinalizeLoading()
        {
            RenewScheme(true);
            CheckCalculationConsistency();
            _loadModuleTask.SetState(1, TaskState.Ready);
            SchemaPanel.Visibility = Visibility.Visible;

            _calculationInLoadingProcess = false;
            _currentInsertionPointView = _loadedOperationViews.Last();
            ShowAdditionalPanelAfter(_loadedOperationViews.LastOrDefault());
        }
    }
}
