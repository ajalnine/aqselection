﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using ApplicationCore;
using AQControlsLibrary;
using AQCalculationsLibrary;
using AQChartViewLibrary;

namespace SLConstructor_Calculations
{
    public partial class Page
    {
        #region Экспорт в Xlsx

        private void ExportToXlsx_Click(object sender, RoutedEventArgs e)
        {
            if (!MainDateRangePicker.From.HasValue || !MainDateRangePicker.To.HasValue) return;
            ReportTab.IsEnabled = false;
            var stepNames = MainCalculationView.GetStepNames().ToArray().Concat(new [] { "Формирование файла Xlsx" }).ToArray();
            var task = new Task(stepNames, "Экспорт в Xlsx", TaskPanel);
            var reporter = new Reporter(_cs, task, MainCalculationView.GetCalculation(), false, MainDateRangePicker.From, MainDateRangePicker.To, MainCalculationView);
            reporter.AttachToSlidePanel(MainSlidePanel);
            MainCalculationView.ResetProgressInScheme();
            if (MainCalculationView.GetCalculation().StepData.Any()) MainCalculationView.ShowProgressInScheme(task.CurrentStep, TaskState.Processing);
            _cs.BeginInitCalculation(MainDateRangePicker.From.Value, MainDateRangePicker.To.Value, GetParametersData(),iar => 
                {
                    var sr = _cs.EndInitCalculation(iar);
                    task.CustomData = sr.TaskDataGuid;
                    try
                    {
                        reporter.DoServerSteps(() =>
                        {
                            var filename = task.CustomData.ToString();
                            task.SetState(task.CurrentStep, TaskState.Processing);
                            _cs.BeginGetAllResultsInXlsx(filename, iar2 =>
                            {
                                var sr2 = _cs.EndGetAllResultsInXlsx(iar2);
                                task.SetState(task.CurrentStep, TaskState.Ready);
                                Dispatcher.BeginInvoke(() =>
                                {
                                    MainCalculationView.FinalizeProgressInScheme();
                                    HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                        $@"GetFile.aspx?FileName={HttpUtility.UrlEncode(sr2.TaskDataGuid)}&FName={
                                            HttpUtility.UrlEncode($"Расчет {DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)}.xlsx")
                                            }&ContentType={HttpUtility.UrlEncode("application/msexcel")}"));
                                    ExportToXlsx.IsEnabled = true;
                                });
                            }, task);
                        });
                    }
                    catch (Exception)
                    {
                        Report.IsEnabled = true;
                        task.SetState(task.CurrentStep, TaskState.Error);
                        task.SetMessage("Ошибка");
                        reporter.DetachFromSlidePanel();
                    }
                }, task);
        }
        
        #endregion

        #region Предварительный просмотр

        void CalculationView_RequestForPreview(object o, GenericEventArgs e)
        {
            if (!MainDateRangePicker.From.HasValue || !MainDateRangePicker.To.HasValue) return;
            ReportTab.IsEnabled = false;
            var stepNames = MainCalculationView.GetStepNames().Take((int)e.CustomData + 1).ToArray().Concat(new [] { "Прием данных"}).ToArray();
            var task = new Task(stepNames, "Результат операции", TaskPanel);
            var reporter = new Reporter(_cs, task, MainCalculationView.GetCalculation(), false, MainDateRangePicker.From, MainDateRangePicker.To, MainCalculationView);
            reporter.AttachToSlidePanel(MainSlidePanel);
            MainCalculationView.ResetProgressInScheme();
            if (MainCalculationView.GetCalculation().StepData.Any()) MainCalculationView.ShowProgressInScheme(task.CurrentStep, TaskState.Processing);
            _cs.BeginInitCalculation(MainDateRangePicker.From.Value, MainDateRangePicker.To.Value, GetParametersData(),
               iar =>
               {
                   var sr = _cs.EndInitCalculation(iar);
                   task.CustomData = sr.TaskDataGuid;
                   try
                   {
                       reporter.DoServerSteps(() =>
                       {
                           task.SetState(task.CurrentStep, TaskState.Processing);
                           reporter.DetachFromSlidePanel();
                           Dispatcher.BeginInvoke(
                               () => _cs.BeginGetTablesSLCompatible(task.CustomData.ToString(), i =>
                               {
                                   var tables = _cs.EndGetTablesSLCompatible(i);
                                   if (tables == null) return;
                                   task.AdvanceProgress();
                                   task.SetMessage("Данные готовы");
                                   Dispatcher.BeginInvoke(() =>
                                   {
                                       var ca = new Dictionary<string, object>
                                       {
                                        {"From", MainDateRangePicker.From},
                                        {"To", MainDateRangePicker.To},
                                        {"SourceData", tables},
                                        {"Calculation", MainCalculationView.GetCalculation().StepData},
                                        {"Description", MainCatalogueDataEditor.GetQueryDescription()},
                                        {"New", true},
                                       };
                                       Preview.IsEnabled = true;
                                       MainCalculationView.FinalizeProgressInScheme();
                                       Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                                   });
                               }, task));
                       });
                   }
                   catch (Exception)
                   {
                       Report.IsEnabled = true;
                       task.SetState(task.CurrentStep, TaskState.Error);
                       task.SetMessage("Ошибка");
                       reporter.DetachFromSlidePanel();
                   }
               }, task);
        }

        #endregion
        
        #region Анализ

        private void Preview_Click(object sender, RoutedEventArgs e)
        {
            if (!MainDateRangePicker.From.HasValue || !MainDateRangePicker.To.HasValue) return;
            ReportTab.IsEnabled = false;
            var stepNames = MainCalculationView.GetStepNames().ToArray().Concat(new[] { "Прием данных"}).ToArray();
            var task = new Task(stepNames, "Анализ результата", TaskPanel);
            var reporter = new Reporter(_cs, task, MainCalculationView.GetCalculation(), false, MainDateRangePicker.From, MainDateRangePicker.To, MainCalculationView);
            reporter.AttachToSlidePanel(MainSlidePanel);
            MainCalculationView.ResetProgressInScheme();
            if (MainCalculationView.GetCalculation().StepData.Any()) MainCalculationView.ShowProgressInScheme(task.CurrentStep, TaskState.Processing);
            _cs.BeginInitCalculation(MainDateRangePicker.From.Value, MainDateRangePicker.To.Value, GetParametersData(),
                iar =>
                {
                    var sr = _cs.EndInitCalculation(iar);
                    task.CustomData = sr.TaskDataGuid;
                    try
                    {
                        reporter.DoServerSteps(() =>
                        {
                            task.SetState(task.CurrentStep, TaskState.Processing);
                            reporter.DetachFromSlidePanel();
                            Dispatcher.BeginInvoke(
                                () => _cs.BeginGetTablesSLCompatible(task.CustomData.ToString(), i =>
                                {
                                    var tables = _cs.EndGetTablesSLCompatible(i);
                                    if (tables == null) return;
                                    task.AdvanceProgress();
                                    task.SetMessage("Данные готовы");
                                    Dispatcher.BeginInvoke(() =>
                                    {
                                        var ca = new Dictionary<string, object>
                                        {
                                            {"From", MainDateRangePicker.From},
                                            {"To", MainDateRangePicker.To},
                                            {"SourceData", tables},
                                            {"Calculation", MainCalculationView.GetCalculation().StepData},
                                            {"Description", MainCatalogueDataEditor.GetQueryDescription()},
                                            {"New", true},
                                        };
                                        Preview.IsEnabled = true;
                                        MainCalculationView.FinalizeProgressInScheme();
                                        Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(),
                                            Command.InsertTablesToAnalysis, ca, null);
                                    });
                                }, task));
                        });
                    }
                    catch (Exception)
                    {
                        Report.IsEnabled = true;
                        task.SetState(task.CurrentStep, TaskState.Error);
                        task.SetMessage("Ошибка");
                        reporter.DetachFromSlidePanel();
                    }
                }, task);
        }

        #endregion
        
        #region Запрос данных

        void CalculationView_RequestForData(object o, GenericEventArgs e)
        {
            if (MainDateRangePicker.From == null || MainDateRangePicker.To == null) return;
            
            var stepNames = MainCalculationView.GetStepNames().Take((int)e.CustomData).ToArray().Concat(new[] { "Прием данных" }).ToArray();
            var calc = MainCalculationView.GetCalculation();
            ReportTab.IsEnabled = false;
            MainCalculationView.ResetProgressInScheme();

            var task = new Task(stepNames, "Исполнение расчета", TaskPanel) { ProcessingItem = o as OperationView };
            if (calc.StepData.Any()) MainCalculationView.ShowProgressInScheme(task.CurrentStep, TaskState.Processing);
            var reporter = new Reporter(_cs, task, calc, false, MainDateRangePicker.From, MainDateRangePicker.To, MainCalculationView);
            reporter.AttachToSlidePanel(MainSlidePanel);

            _cs.BeginInitCalculation(MainDateRangePicker.From.Value, MainDateRangePicker.To.Value, GetParametersData(),
                iar =>
                {
                    var sr = _cs.EndInitCalculation(iar);
                    task.CustomData = sr.TaskDataGuid;
                    try
                    {
                        if (MainCalculationView.GetCalculation().StepData.Any())
                            reporter.DoServerSteps(() =>
                            {
                                task.AdvanceProgress();
                                reporter.DetachFromSlidePanel();
                                Dispatcher.BeginInvoke(() => (task.ProcessingItem as OperationView)?.DataReceived(task.CustomData.ToString()));
                            });
                        else
                        {
                            task.AdvanceProgress();
                            reporter.DetachFromSlidePanel();
                            Dispatcher.BeginInvoke(() => (task.ProcessingItem as OperationView)?.DataReceived(task.CustomData.ToString()));
                        }
                    }
                    catch (Exception)
                    {
                        Report.IsEnabled = true;
                        task.SetState(task.CurrentStep, TaskState.Error);
                        task.SetMessage("Ошибка");
                        reporter.DetachFromSlidePanel();
                    }
                }, task);
        }
        #endregion

        #region Отчет

        private void Report_OnClick(object sender, RoutedEventArgs e)
        {
            ReportTab.IsEnabled = true;
            ReportTab.IsSelected = true;
            MainSlidePanel.Clear();
            MainSlidePanel.UpdateLayout();
            if (!MainDateRangePicker.From.HasValue || !MainDateRangePicker.To.HasValue) return;

            var stepNames = MainCalculationView.GetStepNames().ToArray().Concat(new[] { "Прием данных" }).ToArray();

            var task = new Task(stepNames, "Формирование отчета", TaskPanel);
            
            MainCalculationView.ResetProgressInScheme();
            var calc = MainCalculationView.GetCalculation();
            if (calc.StepData.Any()) MainCalculationView.ShowProgressInScheme(task.CurrentStep, TaskState.Processing);

            var reporter = new Reporter(_cs, task, calc, true, MainDateRangePicker.From, MainDateRangePicker.To, MainCalculationView);
            reporter.AttachToSlidePanel(MainSlidePanel);

            _cs.BeginInitCalculation(MainDateRangePicker.From.Value, MainDateRangePicker.To.Value, GetParametersData(),
                iar =>
                {
                    var sr = _cs.EndInitCalculation(iar);
                    task = (Task)iar.AsyncState;
                    task.CustomData = sr.TaskDataGuid;
                    
                    try
                    {
                        reporter.DoServerSteps(() =>
                        {
                            var taskGuid = task.CustomData.ToString();
                            task.SetState(task.CurrentStep, TaskState.Processing);
                            Dispatcher.BeginInvoke(() => _cs.BeginGetTablesSLCompatible(taskGuid, i =>
                            {
                                var data = _cs.EndGetTablesSLCompatible(i);
                                if (data == null) return;
                                task.AdvanceProgress();
                                task.SetMessage("Данные готовы");
                                Dispatcher.BeginInvoke(() =>
                                {
                                    foreach (var msp in MainSlidePanel.InternalSlideCollection) msp.SourceData = data;
                                    Report.IsEnabled = true;
                                    MainCalculationView.FinalizeProgressInScheme();
                                    reporter.DetachFromSlidePanel();
                                    Services.GetAQService().BeginWriteLog("Report created in SLConstructor_Calculation: " + MainCalculationView.GetCalculation().Name, null, null);

                                });
                            }, task));
                        });
                    }
                    catch (Exception)
                    {
                        Report.IsEnabled = true;
                        task.SetState(task.CurrentStep, TaskState.Error);
                        task.SetMessage("Ошибка");
                        reporter.DetachFromSlidePanel();
                    }

                }, task);
        }
        
        #endregion

        #region Сохранение схемы

        private void ToPNG_Click(object sender, RoutedEventArgs e)
        {
            ScaleSlider.Opacity = 0;
            OperationPanel.Opacity = 0;
            MainCalculationView.ResetProgressInScheme();
            UpdateLayout();
            CommonTasks.SendPNGFile(this, new WriteableBitmap(SchemaPanel, null), TaskPanel, "Схема ",
                () => Dispatcher.BeginInvoke(() =>
                {
                    ScaleSlider.Opacity = 1;
                    OperationPanel.Opacity = 1;
                }));
        }
        #endregion
    }
}
