﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQSelection;

namespace AQCatalogueLibrary
{
    public class ParametersScan
    {
        public delegate void ScanFinishedDelegate(object o, ScanFinishedEventArgs e);
        public delegate void ScanStepDelegate(object o, EventArgs e);

        public List<Step> GetFirstLevelParameters(List<Step> allSteps) // 3.1
        {
            return allSteps.Where(a=>a.Group=="Параметры").ToList();
        }
        
        public void GetAllParametersAsync(int itemID, ScanFinishedDelegate ret) //3.4
        {
            var cdc = CatalogueOperations.CreateDomainContext();
            var result = new List<Steps>();
            Counter.Value = 1;
            RecursiveScanAsync(itemID, cdc, result, ret);
        }

        public void GetAllChildParametersAsync(List<Step> steps, ScanFinishedDelegate ret)
        {
            var cdc = CatalogueOperations.CreateDomainContext();
            var result = new List<Steps>(); 
            var includes = steps.Where(a => a.Name == "Расчет из каталога").ToList();
            var groups = steps.Where(a => a.Name == "Группа операций").ToList();
            Counter.Value = includes.Count + groups.Count;
            if (Counter.Value == 0) ret?.Invoke(this, new ScanFinishedEventArgs { Found = result });
            
            foreach (var i in includes)
            {
                int calcID = GetCalcIDFromParameters(i.ParametersXML);
                RecursiveScanAsync(calcID, cdc, result, ret);
            }

            foreach (var i in groups)
            {
                var st = GetStepsFromParameters(i.ParametersXML);
                RecursiveScanGroupsAsync(st, cdc, result, ret);
            }
        }

        private void RecursiveScanAsync(int itemID, CatalogueDomainContext cdc, List<Steps> result,  ScanFinishedDelegate ret)
        {
            if (Counter.Value > 100) ret(this, new ScanFinishedEventArgs { Found = null });
            CatalogueOperations.GetItem(cdc, itemID, (ev, state) =>
            {
                var cd = CatalogueOperations.ItemToCalculationDescription(ev.Item);
                var s = new Steps { CalcStep = cd.StepData.Where(a => a.Group == "Параметры").ToList(), SourceCalculationName = cd.Name };
                result.Add(s);
                var includes = cd.StepData.Where(a => a.Name == "Расчет из каталога").ToList();
                var groups = cd.StepData.Where(a => a.Name == "Группа операций").ToList();
                Counter.Value += includes.Count + groups.Count;
                Counter.Value--;
                if (Counter.Value == 0) ret?.Invoke(this, new ScanFinishedEventArgs { Found = result });

                foreach (var i in includes)
                {
                    var calcID = GetCalcIDFromParameters(i.ParametersXML);
                    RecursiveScanAsync(calcID, cdc, result, ret);
                }

                foreach (var i in groups)
                {
                    var st = GetStepsFromParameters(i.ParametersXML);
                    RecursiveScanGroupsAsync(st, cdc, result, ret);
                }
            }, null, true, null);
        }

        private void RecursiveScanGroupsAsync(List<Step> steps, CatalogueDomainContext cdc, List<Steps> result, ScanFinishedDelegate ret)
        {
            if (Counter.Value > 100) ret(this, new ScanFinishedEventArgs { Found = null });

            var s = new Steps { CalcStep = steps.Where(a => a.Group == "Параметры").ToList(), SourceCalculationName = null};
            result.Add(s);
            var includes = s.CalcStep.Where(a => a.Name == "Расчет из каталога").ToList();
            var groups = steps.Where(a => a.Name == "Группа операций").ToList();
            Counter.Value += includes.Count + groups.Count;
            Counter.Value--;
            if (Counter.Value == 0) ret?.Invoke(this, new ScanFinishedEventArgs { Found = result });

            foreach (var i in includes)
            {
                var calcID = GetCalcIDFromParameters(i.ParametersXML);
                RecursiveScanAsync(calcID, cdc, result, ret);
            }
            foreach (var i in groups)
            {
                var st = GetStepsFromParameters(i.ParametersXML);
                RecursiveScanGroupsAsync(st, cdc, result, ret);
            }
        }

        private int GetCalcIDFromParameters(string xmlParameters)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            var cp = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            return cp.CurrentCalculationID;
        }

        private List<Step> GetStepsFromParameters(string xmlParameters)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            var cp = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            return cp.CalculationDescription.StepData;
        }

        public class Parameters
        {
            public int CurrentCalculationID;
            public ObservableCollection<Alias> TableAliases;
            public CalculationDescription CalculationDescription;
        }

        public class Alias
        {
            public string Name{get;set;}
            public string AliasName{get;set;}
        }
    }

    public static class Counter
    {
        public static int Value;
    }

    public class Steps
    {
        public List<Step> CalcStep {get; set;}
        public string SourceCalculationName {get; set;}
    }

    public class ScanFinishedEventArgs : EventArgs
    {
        public List<Steps> Found;
    }
}
