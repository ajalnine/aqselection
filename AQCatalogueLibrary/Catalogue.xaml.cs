﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using OpenRiaServices.DomainServices.Client;
using AQSelection;
using AQBasicControlsLibrary;

namespace AQCatalogueLibrary
{
    public partial class Catalogue
    {
        #region Свойства и события

        public delegate void SelectedItemDelegate(object o, ItemOperationEventArgs e);

        public event SelectedItemDelegate FolderSelected;
        public event SelectedItemDelegate ItemSelected;
        public event SelectedItemDelegate ItemDoubleClick;
        public event SelectedItemDelegate SelectedItemGeometryPossibleChanged;
        
        public bool TreeIsPopulated;
        public TreeViewExtended MainTreeView;

        private Point _position;
        private List<int> _selectionChain;
        private CatalogueDomainContext _cdc;
        private IQueryable<AQ_CatalogueItems> _data;
        private IQueryable<AQ_CatalogueItems> _source;
        
        private double _xContext, _yContext;

        public IEnumerable<string> URL
        {
            get { return (IEnumerable<string>)GetValue(URLProperty); }
            set { SetValue(URLProperty, value); }
        }

        public static readonly DependencyProperty URLProperty =
            DependencyProperty.Register("URL", typeof(IEnumerable<string>), typeof(Catalogue), new PropertyMetadata(null));

        public int? SelectedFolderID
        {
            get { return (int?)GetValue(SelectedFolderIDProperty); }
            set { SetValue(SelectedFolderIDProperty, value); }
        }

        public static readonly DependencyProperty SelectedFolderIDProperty =
            DependencyProperty.Register("SelectedFolderID", typeof(int?), typeof(Catalogue), new PropertyMetadata(null));

        public int? SelectedItemID
        {
            get { return (int?)GetValue(SelectedItemIDProperty); }
            set { SetValue(SelectedItemIDProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemIDProperty =
            DependencyProperty.Register("SelectedItemID", typeof(int?), typeof(Catalogue), new PropertyMetadata(null));

        public bool SelectedItemIsFolder
        {
            get { return (bool)GetValue(SelectedItemIsFolderProperty); }
            set { SetValue(SelectedItemIsFolderProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemIsFolderProperty =
            DependencyProperty.Register("SelectedItemIsFolder", typeof(bool), typeof(Catalogue), new PropertyMetadata(false));

        public string ItemFilter
        {
            get { return (string)GetValue(ItemFilterProperty); }
            set { SetValue(ItemFilterProperty, value); }
        }

        public static readonly DependencyProperty ItemFilterProperty =
            DependencyProperty.Register("ItemFilter", typeof(string), typeof(Catalogue), new PropertyMetadata(string.Empty, ItemFilterChangedCallback));

        public string ItemOrder
        {
            get { return (string)GetValue(ItemOrderProperty); }
            set { SetValue(ItemOrderProperty, value); }
        }

        public static readonly DependencyProperty ItemOrderProperty =
            DependencyProperty.Register("ItemOrder", typeof(string), typeof(Catalogue), new PropertyMetadata("Name", ItemOrderChangedCallback));

        public bool AutoPopulate
        {
            get { return (bool)GetValue(AutoPopulateProperty); }
            set { SetValue(AutoPopulateProperty, value); }
        }

        public static readonly DependencyProperty AutoPopulateProperty =
            DependencyProperty.Register("AutoPopulate", typeof(bool), typeof(Catalogue), new PropertyMetadata(false));

        public bool FullViewMode
        {
            get { return (bool)GetValue(FullViewModeProperty); }
            set { SetValue(FullViewModeProperty, value); }
        }

        public static readonly DependencyProperty FullViewModeProperty =
            DependencyProperty.Register("FullViewMode", typeof(bool), typeof(Catalogue), new PropertyMetadata(true));

        public bool PreselectRequired
        {
            get { return (bool)GetValue(PreselectRequiredProperty); }
            set { SetValue(PreselectRequiredProperty, value); }
        }

        public static readonly DependencyProperty PreselectRequiredProperty =
            DependencyProperty.Register("PreselectRequired", typeof(bool), typeof(Catalogue), new PropertyMetadata(true));

        private AQ_CatalogueItems _defaultItem;
        
        #endregion

        public Catalogue()
        {
            InitializeComponent();
            var vmc = Resources["ViewModeConverter"] as ViewModeConverter;
            if (vmc == null) return;
            vmc.FullModeStyle = Resources["FullItemView"] as Style;
            vmc.CompactModeStyle = Resources["CompactItemView"] as Style;
        }

        #region Обновление дерева

        public void RenewData()
        {
            _cdc = CatalogueOperations.CreateDomainContext();
            var itemsQuery = ItemFilter == string.Empty 
                ? _cdc.GetAQ_CatalogueItemsQuery(ItemOrder).Where(a => a.IsDeleted == false) 
                : _cdc.GetAQ_CatalogueItemsFilteredQuery(ItemFilter, ItemOrder).Where(a => a.IsDeleted == false );

            var loadOp = _cdc.Load(itemsQuery);
            loadOp.Completed += (sender, e) =>
            {
                _source = loadOp.Entities.AsQueryable();
                _data = _source.Where(a => a.ParentId == null).AsQueryable();
                CreateTreeView();

                if (_defaultItem == null)
                {
                    _defaultItem = _data.FirstOrDefault();
                }

                var toSelect = _defaultItem;
                var source = _data.ToList();
                _selectionChain = CatalogueOperations.GetItemChain(_defaultItem).OfType<AQ_CatalogueItems>().Select(a => a.id).ToList();
                if (_defaultItem != null) _selectionChain.Add(_defaultItem.id);
                foreach (var i in _selectionChain)
                {
                    toSelect = source?.SingleOrDefault(a => a.id == i);
                    source = toSelect?.AQ_CatalogueItems1?.ToList();
                }

                var itemChain = CatalogueOperations.GetItemChain(toSelect);

                if (MainTreeView.Items.Count > 0 && PreselectRequired)
                {
                    MainTreeView.SetSelectedItem(toSelect, itemChain);
                    SetSelectedItems(toSelect);
                    URL = CatalogueOperations.GetURL(toSelect);
                    TreeIsPopulated = true;
                }
            };
        }

        public int Search(string search)
        {
            MainTreeView.UnMark();
            int id;
            var isNumber = int.TryParse(search, out id);
            DateTime dt;
            var isDate = DateTime.TryParse(search, out dt);
            var numberOfFounds = 0;
            foreach (var d in _source)
            {
                var rev = d?.AQ_CatalogueRevisions.Where(a => a.RevisionDescription != null).OrderByDescending(b => b.RevisionDate).FirstOrDefault();
                if (d == null) continue;
                var found = 
                    d.Name.ToLower().Contains(search.ToLower()) 
                    || rev?.RevisionDescription != null && rev.RevisionDescription.ToLower().Contains(search.ToLower()) 
                    || rev?.RevisionAuthor != null && rev.RevisionAuthor.ToLower().Contains(search.ToLower()) 
                    || isNumber && d.id == id 
                    || isDate && rev.RevisionDate!=null && rev.RevisionDate.Value.ToString("dd.MM.yyyy") == search;

                if (!found) continue;
                MainTreeView.ExpandDelayItemsAndMark(CatalogueOperations.GetItemChain(d), d);
                numberOfFounds++;
            }
            return numberOfFounds;
        }

        public void ResetSearch()
        {
            MainTreeView.UnMark();
        }
        
        private void CreateTreeView()
        {
            if (LayoutRoot.Children[0] is TreeViewExtended) LayoutRoot.Children.RemoveAt(0);
            MainTreeView = new TreeViewExtended
            {
                Style = (Style)Resources["ExtendedTreeViewStyle"],
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                BorderThickness = new Thickness(0),
                Name = "MainTreeView",
                ItemContainerStyle = (Style)Resources["ExtendedTreeViewItemStyle"],
                Background = new SolidColorBrush(Colors.Transparent)
            };
            MainTreeView.Loaded += MainTreeView_Loaded;
            MainTreeView.OnMouseLeftButtonDoubleClick += MainTreeView_OnMouseLeftButtonDoubleClick;
            MainTreeView.ContextMenuCall += MainTreeView_ContextMenuCall;
            MainTreeView.SelectedItemChanged += MainTreeView_SelectedItemChanged;
            MainTreeView.DragStarted += MainTreeView_DragStarted;
            MainTreeView.DragCandidateChanged += MainTreeView_DragCandidateChanged;
            MainTreeView.DragFinished += MainTreeView_DragFinished;
            MainTreeView.SelectedItemGeometryPossiblyChanged += MainTreeView_SelectedItemGeometryPossiblyChanged;

            MainTreeView.ItemTemplate = (DataTemplate)Resources["HDT"];
            MainTreeView.ItemsSource = _data;
            LayoutRoot.Children.Insert(0, MainTreeView);
            MainTreeView.UpdateLayout();
        }

        private void SetSelectedItems(AQ_CatalogueItems defaultItem)
        {
            if (defaultItem == null) return;
            if (defaultItem.Type.ToLower() == "folder")
            {
                SelectedFolderID = defaultItem.id;
                SelectedItemIsFolder = true;
            }
            else
            {
                SelectedItemIsFolder = false;
                SelectedItemID = defaultItem.id;
            }
        }

        public static void ItemFilterChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            //((Catalogue)o).RenewData();
        }
        
        public static void ItemOrderChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((Catalogue)o).RenewData();
        }
        #endregion

        #region Drag & Drop

        private void MainTreeView_DragFinished(object sender, DragFinishedEventArgs e)
        {
            if (e.Source == null) return;
            var source = e.Source.DataContext as AQ_CatalogueItems;
            var destination = e.Destination.DataContext as AQ_CatalogueItems;
            if (CheckIfDragAndDropPossible(source, destination) && source!=null && destination !=null)
            {
                source.ParentId = destination.id;
                _cdc.SubmitChanges();
            }

            Dispatcher.BeginInvoke(() =>
            {
                MainTreeView.SetSelectedItem(source, CatalogueOperations.GetItemChain(source));
                MainDragView.RemoveDragItem();
            });
        }

        private bool CheckIfDragAndDropPossible(AQ_CatalogueItems source, AQ_CatalogueItems destination)
        {
            if (!(source != null && destination != null && destination.Type.ToLower() == "folder" && source != destination))return false;
            var ic = CatalogueOperations.GetItemChain(destination);
            return !ic.Contains(source);
        }

        private void MainTreeView_DragStarted(object sender, MouseEventArgs e)
        {
            var source = (sender as TreeViewItemExtended)?.DataContext  as AQ_CatalogueItems;
            if (source != null && source.ParentId == null) MainTreeView.CancelDrag();
            else
            {
                    MainDragView.SetDragItem(new CatalogueItemPresenter() { ItemName = source?.Name, Description = string.Empty, 
                    Style = (Resources["ViewModeConverter"] as ViewModeConverter)?.Convert(FullViewMode, typeof(Style), null, null) as Style, 
                    ItemSign = CatalogueHelper.GetIconByType(source?.Type) });
            }
            e.GetHashCode();
        }

        private void MainTreeView_DragCandidateChanged(object sender, DragCandidateEventArgs e)
        {
            var destination = (sender as TreeViewItemExtended)?.DataContext as AQ_CatalogueItems;

            if (e.Destination != null)
            {
                var source = e.Source.DataContext as AQ_CatalogueItems;
                MainTreeView.SetSelectedItem(destination, CatalogueOperations.GetItemChain(destination));
                MainDragView.SetDropPossibilityIcon(CheckIfDragAndDropPossible(source, destination));
            }
            else MainDragView.SetDropPossibilityIcon(null);
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            _position = e.GetPosition(LayoutRoot);
            MainDragView.SetDragItemPosition(_position);
            CloseContextMenuOnMouseOut(_position);
        }

        private void CloseContextMenuOnMouseOut(Point position)
        {
            if (MainToolBar.Visibility == Visibility.Collapsed) return;
            if (_xContext > position.X + 10 ||
                _yContext > position.Y + 10 ||
                _xContext < position.X - MainToolBar.ActualWidth - 10 ||
                _yContext < position.Y - MainToolBar.ActualHeight - 10)
            {
                MainToolBar.Visibility = Visibility.Collapsed;
            }
        }

        private void CatalogueItemPresenter_EditFinished(object o, CatalogueItemPresenterEditEventArgs e)
        {
            var source = e.DataItem  as AQ_CatalogueItems;
            if (source == null) return;
            source.Name = e.NewName;
            _cdc.SubmitChanges(s => 
            {
                URL = CatalogueOperations.GetURL(source);
            }, null);
        }

        private void LayoutRoot_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainTreeView.CancelDrag();
            MainDragView.RemoveDragItem();
        }

        private void LayoutRoot_MouseLeave(object sender, MouseEventArgs e)
        {
            MainTreeView.CancelDrag();
            MainDragView.RemoveDragItem();
        }
        #endregion

        #region Операции, вызываемые с тулбара

        private void CopyToolButton_Click(object sender, RoutedEventArgs e)
        {
            var aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci != null)
            {
                CatalogueOperations.CloneItem(_cdc, aqci.id, (ev, state) => 
                {
                }, null);
            }
        }
        
        private void AddToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            var aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci == null) return;
            var newItem = new AQ_CatalogueItems { IsDeleted = false, Name = "Новая папка", ParentId = aqci.id, Type = "Folder" };
            _cdc.AQ_CatalogueItems.Add(newItem);
            aqci.AQ_CatalogueItems1.Add(newItem);
            _cdc.SubmitChanges(a =>
            {
                Dispatcher.BeginInvoke(() =>
                {
                    MainTreeView.SetSelectedItem(newItem, CatalogueOperations.GetItemChain(newItem));
                });
            }, new object());
        }

        private void DeleteToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed; 
            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            CatalogueOperations.DeleteItem(_cdc, aqci, null, null);
        }

        private void CloseRecycle_Click(object sender, RoutedEventArgs e)
        {
            RecycleToolBar.Visibility = Visibility.Collapsed;
        }

        private void CloseHistory_Click(object sender, RoutedEventArgs e)
        {
            HistoryToolBar.Visibility = Visibility.Collapsed;
        }

        private void RefreshToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            PreselectRequired = true;
            RenewData();
        }
        
        private void RenameToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            MainTreeView.StartRename();
        }

        private void HistoryToolButton_Click(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(HistoryToolBar, _xContext);
            Canvas.SetTop(HistoryToolBar, _yContext);
            ClearHistoryToolButton.IsEnabled = false;
            CopyFromHistoryToolButton.IsEnabled = false;
            MainToolBar.Visibility = Visibility.Collapsed;
            HistoryToolBar.Visibility = Visibility.Visible;
            RecycleToolBar.Visibility = Visibility.Collapsed;
            var aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            HistoryHeader.Text = "История изменений" + (string.IsNullOrEmpty(aqci?.Name) ? string.Empty : ": " + aqci.Name);
            FillHistoryList(aqci);
        }

        private void RecycleToolButton_Click(object sender, RoutedEventArgs e)
        {
            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            FillRecycleList(aqci);
        }


        private void ClearHistoryToolButton_Click(object sender, RoutedEventArgs e)
        {
            HistoryToolBar.Visibility = Visibility.Collapsed;

            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci != null)
            {
                var res = MessageBox.Show("Действие необратимо. Будут удалены все редакции, кроме последней. Продолжить?", "Очистка истории изменений", MessageBoxButton.OKCancel);
                if (res == MessageBoxResult.Cancel) return;
                CatalogueOperations.ClearHistory(_cdc, aqci.id, (ev, state) =>
                {
                    PreselectRequired = true;
                    RenewData();
                }, null);
            }
        }

        private void CopyFromHistoryToolButton_Click(object sender, RoutedEventArgs e)
        {
            HistoryToolBar.Visibility = Visibility.Collapsed;
            var revision = (EditionSelector.SelectedItem as HistoryListItem)?.Revision ?? -1;
            if (revision == -1) return;
            var aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci != null)
            {
                CatalogueOperations.CloneFromHistoryItem(_cdc, aqci.id, revision, (ev, state) =>
                {
                }, null);
            }
        }

        private void RestoreToolButton_Click(object sender, RoutedEventArgs e)
        {
            RecycleToolBar.Visibility = Visibility.Collapsed;
            var id = (RecycleSelector.SelectedItem as RecycleListItem)?.Id ?? -1;
            if (id == -1) return;

            CatalogueOperations.RestoreItem(_cdc, id, (ev, state) =>
            {
                RenewData();
            }, null);
            
        }

        private void FillHistoryList(AQ_CatalogueItems aqci)
        {
            var s = aqci.AQ_CatalogueRevisions.Select(a=>new HistoryListItem {Revision = a.Revision, Description = $"#{a.Revision} {a.RevisionDescription}, {a.RevisionAuthor} {a.RevisionDate}" }).OrderBy(b=>b.Revision).ToList();
            EditionSelector.ItemsSource = s;
            EditionSelector.UpdateLayout();
            if (s.Count > 1) ClearHistoryToolButton.IsEnabled = true;
        }

        private void FillRecycleList(AQ_CatalogueItems aqci)
        {
            var cdc2 = CatalogueOperations.CreateDomainContext();
            var itemsQuery = cdc2.GetAQ_CatalogueItemsQuery(ItemOrder).Where(a => a.IsDeleted == true && a.ParentId == aqci.id);
            var loadOp = cdc2.Load(itemsQuery);
            loadOp.Completed += (sender, e) =>
            {
                var source = loadOp.Entities.AsQueryable();
                if (!source.Any())
                {
                    MessageBox.Show("Папка не содержит удаленных элементов", "Корзина", MessageBoxButton.OK);
                    return;
                }
                RecycleHeader.Text = "Удаленные расчеты в папке " + aqci.Name;
                Canvas.SetLeft(RecycleToolBar, _xContext);
                Canvas.SetTop(RecycleToolBar, _yContext);
                RestoreToolButton.IsEnabled = false;
                MainToolBar.Visibility = Visibility.Collapsed;
                HistoryToolBar.Visibility = Visibility.Collapsed;
                RecycleToolBar.Visibility = Visibility.Visible;

                RecycleSelector.ItemsSource = source.Select(a=>new RecycleListItem {Id = a.id, Name=$"#{a.id}: {a.Name} {a.AQ_CatalogueRevisions.OrderByDescending(b=>b.RevisionDate).Take(1).Select(c=>c.RevisionAuthor + " " + c.RevisionDate).FirstOrDefault()}" });
                RecycleSelector.UpdateLayout();
            };
        }

        private void EditionSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0) CopyFromHistoryToolButton.IsEnabled = true;
        }

        private void RecycleSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0) RestoreToolButton.IsEnabled = true;
        }

        public void SelectItem(int itemID, bool selectParentFolder)
        {
            if (itemID < 0)
            {
                RenewData();
                return;
            }
            _cdc = CatalogueOperations.CreateDomainContext();
            _cdc.Load(_cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.RefreshCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();
                if (selectParentFolder) currentItem = currentItem?.AQ_CatalogueItems2;
                _defaultItem = currentItem;
                RenewData();
            }, null);
        }

        #endregion

        #region Обработка событий

        private void MainTreeView_ContextMenuCall(object sender, ContextMenuCallEventArgs e)
        {
            var x = e.CallPoint.X - 2;
            var y = e.CallPoint.Y - 2;
            var sizeX = MainToolBar.ActualWidth;
            var sizeY = MainToolBar.ActualHeight;
            var contX = ActualWidth;
            var contY = ActualHeight;
            if (x + sizeX > contX) x = contX - sizeX;
            if (y + sizeY > contY) y = contY - sizeY;
            _xContext = x;
            _yContext = y;
            Canvas.SetLeft(MainToolBar, x);
            Canvas.SetTop(MainToolBar, y);
            AQ_CatalogueItems aqci = e.SelectedItem.DataContext as AQ_CatalogueItems;
            DeleteToolButton.IsEnabled = (aqci?.ParentId != null && aqci.AQ_CatalogueItems1.Count==0);
            AddToolButton.IsEnabled = (aqci?.Type.ToLower() == "folder");
            CopyToolButton.IsEnabled = !AddToolButton.IsEnabled;
            RecycleToolButton.IsEnabled = (aqci?.Type.ToLower() == "folder");
            HistoryToolButton.IsEnabled = aqci?.AQ_CatalogueRevisions.Count > 1 && (aqci.Type.ToLower() != "folder");
            MainToolBar.Visibility = Visibility.Visible;
        }
        
        private void MainTreeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (AutoPopulate)RenewData();
            var vmc = Resources["ViewModeConverter"] as ViewModeConverter;
            if (vmc != null)
            {
                vmc.FullModeStyle = Resources["FullItemView"] as Style;
                vmc.CompactModeStyle = Resources["CompactItemView"] as Style;
            }
            AutoPopulate = false;
        }
        #endregion

        #region Навигация по дереву

        private void MainTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var si = ((AQ_CatalogueItems)MainTreeView.SelectedItem);
            if (si == null) return;
            var tvi = MainTreeView.ContainerFromItem(si);

            var r = new Rect();

            try
            {
                var tv = MainTreeView;
                var gt = tvi.TransformToVisual(tv);
                var topLeftOfItem = gt.Transform(new Point(0, 0));

                var f = VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(tvi, 0), 1) as FrameworkElement;
                if (f != null) r = new Rect(topLeftOfItem, new Size(f.ActualWidth, f.ActualHeight));
            }
            catch (Exception)
            {
            }

            URL = CatalogueOperations.GetURL(si);
            _defaultItem = si;
            if (si.Type.ToLower() == "folder")
            {
                SelectedFolderID = si.id;
                SelectedItemIsFolder = true;
                SelectedItemID = si.id;
                FolderSelected?.Invoke(this, new ItemOperationEventArgs(si.id, r, si.Type));
            }
            else
            {
                SelectedItemIsFolder = false;
                SelectedItemID = si.id;
                if (si.AQ_CatalogueItems2 != null)
                {
                    SelectedFolderID = si.AQ_CatalogueItems2.id;
                }
                else SelectedFolderID = null;
                ItemSelected?.Invoke(this, new ItemOperationEventArgs(si.id, r, si.Type));
            }
        }
        
        private void MainTreeView_OnMouseLeftButtonDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ItemDoubleClick?.Invoke(this, new ItemOperationEventArgs(((AQ_CatalogueItems) ((TreeViewItemExtended) sender).DataContext).id));
        }

        private void MainTreeView_SelectedItemGeometryPossiblyChanged(object sender, TreeViewEventArgs e)
        {
            if (SelectedItemGeometryPossibleChanged == null) return;
            var tvi = MainTreeView.ContainerFromItem(e.Item);
            if (tvi == null) return;
            var tv = MainTreeView;
            var gt = tvi.TransformToVisual(tv);
            var topLeftOfItem = gt.Transform(new Point(0, 0));
            var r = new Rect(topLeftOfItem, new Size(tvi.ActualWidth, tvi.ActualHeight));
            SelectedItemGeometryPossibleChanged(this, new ItemOperationEventArgs(((AQ_CatalogueItems) e.Item).id, r, ((AQ_CatalogueItems)e.Item).Type ));
        }
        #endregion
    }
    #region Вспомогательные классы и конвертеры

    public class TypeToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CatalogueHelper.GetIconByType(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class ViewModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value)? FullModeStyle: CompactModeStyle;
        }

        public Style FullModeStyle { get; set; }
        public Style CompactModeStyle { get; set; }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class RevisionsToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var a = (value as IEnumerable<AQ_CatalogueRevisions>).ToList();
            if (a != null && a.Any()) 
            {
                var lastEdition=a.OrderByDescending(x => x.RevisionDate).FirstOrDefault();
                if (lastEdition != null) return
                    $"Ред. {a.Count}, автор: {lastEdition.RevisionAuthor} от {((lastEdition.RevisionDate.HasValue) ? lastEdition.RevisionDate.Value.ToString(CultureInfo.InvariantCulture) : String.Empty)} #{lastEdition.AQ_CatalogueItems.id}\r\n{lastEdition.RevisionDescription}";
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class HistoryListItem
    {
        public int? Revision { get; set; }
        public string Description { get; set; }
    }

    public class RecycleListItem
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }

    #endregion
}