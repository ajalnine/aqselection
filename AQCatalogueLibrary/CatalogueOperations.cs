﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using ApplicationCore.CalculationServiceReference;
using AQSelection;
using AQCalculationsLibrary;
using ApplicationCore;
using OpenRiaServices.DomainServices.Client;

namespace AQCatalogueLibrary
{
    public static class CatalogueOperations
    {
        public delegate void ItemOperationFinishedDelegate(ItemOperationEventArgs e, object state);
        public delegate void ItemListingFinishedDelegate<T>(ItemListingEventArgs<T> e, object state);
        public delegate void ItemDataFinishedDelegate(ItemDataEventArgs e, object state);
        public delegate void ItemCheckedDelegate(ItemCheckEventArgs e, object state);
        public delegate void ItemURLDelegate(ItemUrlEventArgs e, object state);
        public delegate void ItemReadedDelegate(ItemReadedEventArgs e, object state);
        public delegate void ItemOperationErrorDelegate(ItemOperationErrorEventArgs e, object state);

        public static event ItemOperationErrorDelegate ItemOperationError;

        #region Операции с данными

        public static CatalogueDomainContext CreateDomainContext()
        {
            return new CatalogueDomainContext();
        }

        public static void CreateItem(CatalogueDomainContext cdc, int folderID, string name, string type, string revisionDescription, ItemOperationFinishedDelegate operationCallBack, object state, List<AttachmentDescription> attachmentList)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemNoDeletedQuery(folderID), LoadBehavior.KeepCurrent, o =>
            {
                var parentItem = o.Entities.SingleOrDefault();
                var newItem = new AQ_CatalogueItems
                {
                    IsDeleted = false,
                    Name = name,
                    Type = type
                };
                if (parentItem != null) newItem.ParentId = folderID;
                cdc.AQ_CatalogueItems.Add(newItem);
                parentItem?.AQ_CatalogueItems1.Add(newItem);

                var newRevision = AddRevision(cdc, revisionDescription, newItem);

                foreach (var attachment in attachmentList)
                {
                    AddAttachment(cdc, attachment, newRevision);
                }

                cdc.SubmitChanges(a =>
                {
                    var e = new ItemOperationEventArgs(newItem.id);
                    operationCallBack?.Invoke(e, state);
                }, new object());
            }, null);
        }

        public static void CreateItem(CatalogueDomainContext cdc, ItemDescription item, ItemOperationFinishedDelegate operationCallBack, object state)
        {
            CreateItem(cdc, item.ParentID ?? 0, item.Name, item.ItemType, item.RevisionDescription, operationCallBack, state, item.AttachmentDescriptions);
        }

        public static IEnumerable<string> GetURL(AQ_CatalogueItems si)
        {
            if (si == null) return null;
            var ti = new List<object>();
            if (si.ParentId != null) ti.Add(si);
            return (from u in GetItemChain(si).Concat(ti) select ((AQ_CatalogueItems)u).Name);
        }

        public static List<object> GetItemChain(AQ_CatalogueItems item)
        {
            var result = new List<object>();
            if (item == null) return result;
            var currentItem = item;
            if (currentItem.AQ_CatalogueItems2 != null)
            {
                do
                {
                    if (result.Count == 0) result.Add(currentItem.AQ_CatalogueItems2);
                    else result.Insert(0, currentItem.AQ_CatalogueItems2);
                    currentItem = currentItem.AQ_CatalogueItems2;
                }
                while (currentItem?.ParentId != null);
            }
            else result.Add(currentItem);
            return result;
        }
        
        public static void MoveItem(CatalogueDomainContext cdc, int newFolderID, int itemID, ItemURLDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    currentItem.ParentId = newFolderID;

                    cdc.SubmitChanges(a =>
                    {
                        callBack?.Invoke(new ItemUrlEventArgs { Url = GetURL(currentItem) }, state);
                    }, new object());
                }
                else
                {
                    callBack?.Invoke(new ItemUrlEventArgs { Url = null }, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void ClearHistory(CatalogueDomainContext cdc, int itemID, ItemURLDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    var toDelete = new List<AQ_CatalogueRevisions>();
                    var maxId = currentItem.AQ_CatalogueRevisions.Select(a => a.Revision).Max();

                    foreach(var r in currentItem.AQ_CatalogueRevisions)
                    {
                        if (r.Revision != maxId) toDelete.Add(r);
                        else r.Revision = 1;
                    }

                    foreach(var d in toDelete)
                    {
                        currentItem.AQ_CatalogueRevisions.Remove(d);
                    }
                    
                    cdc.SubmitChanges(a =>
                    {
                        callBack?.Invoke(new ItemUrlEventArgs { Url = GetURL(currentItem) }, state);
                    }, new object());
                }
                else
                {
                    callBack?.Invoke(new ItemUrlEventArgs { Url = null }, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void UpdateItemWithNewRevisionCreation(CatalogueDomainContext cdc, int itemID, int newFolderID, string name, string type, string revisionDescription, ItemOperationFinishedDelegate operationCallBack, object state, List<AttachmentDescription> attachmentList)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    currentItem.ParentId = newFolderID;
                    currentItem.IsDeleted = false;
                    currentItem.Name = name;
                    currentItem.Type = type;

                    var newRevision = AddRevision(cdc, revisionDescription, currentItem);
                    newRevision.RevisionAuthor = Security.CurrentUserName;
                    newRevision.RevisionDate = DateTime.Now;
                    foreach (var attachment in attachmentList)
                    {
                        AddAttachment(cdc, attachment, newRevision);
                    }

                    cdc.SubmitChanges(a =>
                    {
                        var e = new ItemOperationEventArgs(currentItem.id);
                        operationCallBack?.Invoke(e, state);
                    }, new object());
                }
                else
                {
                    ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                    operationCallBack?.Invoke(e, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void UpdateItemLastRevision(CatalogueDomainContext cdc, int itemID, int newFolderID, string name, string type, string revisionDescription, ItemOperationFinishedDelegate operationCallBack, object state, List<AttachmentDescription> attachmentList)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    currentItem.ParentId = newFolderID;
                    currentItem.IsDeleted = false;
                    currentItem.Name = name;
                    currentItem.Type = type;

                    var lastRevision = currentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();

                    if (lastRevision != null)
                    {
                        lastRevision.RevisionDescription = revisionDescription;
                        lastRevision.RevisionAuthor = Security.CurrentUserName;
                        lastRevision.RevisionDate = DateTime.Now;

                        var temp = lastRevision.AQ_CatalogueData.ToList();

                        foreach (var toDelete in temp) lastRevision.AQ_CatalogueData.Remove(toDelete);
                        foreach (var attachment in attachmentList) AddAttachment(cdc, attachment, lastRevision);

                        cdc.SubmitChanges(a =>
                        {
                            var e = new ItemOperationEventArgs(currentItem.id);
                            operationCallBack?.Invoke(e, state);
                        }, new object());
                    }
                    else
                    {
                        var e = new ItemOperationEventArgs(-1);
                        operationCallBack?.Invoke(e, state);
                        ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "Для указанного элемента в каталоге нет сохраненных данных" }, state);
                    }
                }
                else
                {
                    var e = new ItemOperationEventArgs(-1);
                    operationCallBack?.Invoke(e, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void GetItemURL(CatalogueDomainContext cdc, int itemID, ItemURLDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    callBack?.Invoke(new ItemUrlEventArgs { Url = GetURL(currentItem) }, state);
                }
                else
                {
                    callBack?.Invoke(new ItemUrlEventArgs { Url = null }, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void CreateRevision(CatalogueDomainContext cdc, int itemID, string newName,
            string revisionDescription, string itemType, ItemOperationFinishedDelegate operationCallBack, object state,
            List<AttachmentDescription> attachmentList)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();
                if (currentItem != null)
                {
                    currentItem.Name = newName;
                    currentItem.Type = itemType;
                    var newRevision = AddRevision(cdc, revisionDescription, currentItem);
                    newRevision.RevisionAuthor = Security.CurrentUserName;
                    newRevision.RevisionDate = DateTime.Now;

                    foreach (var attachment in attachmentList)
                    {
                        AddAttachment(cdc, attachment, newRevision);
                    }

                    cdc.SubmitChanges(a =>
                    {
                        var e = new ItemOperationEventArgs(currentItem.id);
                        operationCallBack?.Invoke(e, state);
                    }, new object());
                }
                else
                {
                    var e = new ItemOperationEventArgs(-1);
                    operationCallBack?.Invoke(e, state);
                    ItemOperationError?.Invoke(
                        new ItemOperationErrorEventArgs
                        {
                            Message = "В каталоге нет элемента с указанным идентификатором"
                        }, state);
                }
            }, null);
        }

        public static void DeleteItem(CatalogueDomainContext cdc, int itemID, ItemOperationFinishedDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                if (o.Entities != null)
                {
                    DeleteItem(cdc, o.Entities.SingleOrDefault(), callBack, state);
                }
                else
                {
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void RestoreItem(CatalogueDomainContext cdc, int itemID, ItemOperationFinishedDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                if (o.Entities != null)
                {
                    RestoreItem(cdc, o.Entities.SingleOrDefault(), callBack, state);
                }
                else
                {
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        private static AQ_CatalogueRevisions AddRevision(CatalogueDomainContext cdc, string revisionDescription, AQ_CatalogueItems newItem)
        {
            var newRevision = new AQ_CatalogueRevisions
            {
                CalculationID = newItem.id,
                Revision = newItem.AQ_CatalogueRevisions?.Count + 1 ?? 1,
                RevisionDescription = revisionDescription,
                RevisionDate = DateTime.Now,
                RevisionAuthor = Security.CurrentUserName
            };
            cdc.AQ_CatalogueRevisions.Add(newRevision);
            newItem.AQ_CatalogueRevisions?.Add(newRevision);
            return newRevision;
        }

        private static void AddAttachment(CatalogueDomainContext cdc, AttachmentDescription attachment, AQ_CatalogueRevisions revision)
        {
            var newAttachment = new AQ_CatalogueData
            {
                BinaryData = attachment.BinaryData,
                XmlData = attachment.XmlData,
                BinaryDataMIME = attachment.BinaryDataMIME,
                DataDescription = attachment.DataDescription,
                DataItemName = attachment.DataItemName,
                IsDeleted = false,
                AttachedAt = DateTime.Now,
                AttachedBy = Security.CurrentUserName
            };
            cdc.AQ_CatalogueDatas.Add(newAttachment);
            revision.AQ_CatalogueData.Add(newAttachment);
        }

        private static AttachmentDescription GetAttachmentDescription(AQ_CatalogueData attachment)
        {
            var atd = new AttachmentDescription
            {
                BinaryData = attachment.BinaryData,
                XmlData = attachment.XmlData,
                BinaryDataMIME = attachment.BinaryDataMIME,
                DataDescription = attachment.DataDescription,
                DataItemName = attachment.DataItemName
            };
            return atd;
        }

        public static void AddAttachmentToItem(CatalogueDomainContext cdc, AttachmentDescription attachment, int itemID, ItemOperationFinishedDelegate callBack, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueItemQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();

                if (currentItem != null)
                {
                    var currentRevision = currentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();
                    AddAttachment(cdc, attachment, currentRevision);

                    cdc.SubmitChanges(a =>
                    {
                        callBack?.Invoke(new ItemOperationEventArgs(currentItem.id), state);
                    }, new object());
                }
                else
                {
                    var e = new ItemOperationEventArgs(-1);
                    callBack?.Invoke(e, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void GetItem(int itemID, ItemReadedDelegate callBack, object state)
        {
            GetItem(CreateDomainContext(), itemID, callBack, null, true, state);
        }
        
        public static void GetItem(CatalogueDomainContext cdc, int itemID, ItemReadedDelegate callBack, int? revision, bool includeDocuments, object state)
        {
            var result = new ItemDescription();

            cdc.Load(cdc.GetAQ_CatalogueItemNoDeletedQuery(itemID), LoadBehavior.KeepCurrent, o =>
            {
                var currentItem = o.Entities.SingleOrDefault();
                if (currentItem != null)
                {
                    var currentRevision = !revision.HasValue 
                        ? currentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault() 
                        : currentItem.AQ_CatalogueRevisions.SingleOrDefault(a => a.Revision == revision.Value);

                    if (currentRevision != null)
                    {
                        result.ID = currentItem.id;
                        result.ItemType = currentItem.Type;
                        result.Name = currentItem.Name;
                        result.RevisionAuthor = currentRevision.RevisionAuthor;
                        result.RevisionDate = currentRevision.RevisionDate;
                        result.RevisionDescription = currentRevision.RevisionDescription;
                        result.ParentID = currentItem.ParentId;

                        if (includeDocuments)
                        {
                            cdc.Load(cdc.GetAQ_CatalogueDataListQuery(itemID, true, revision, true), LoadBehavior.KeepCurrent, o2 =>
                            {
                                if (callBack != null)
                                {
                                    foreach (var a in o2.Entities)
                                    {
                                        result.AttachmentDescriptions.Add(GetAttachmentDescription(a));
                                    }
                                    var e = new ItemReadedEventArgs{ Item = result };
                                    callBack.Invoke(e, state);
                                }
                            }, null);
                        }
                        else
                        {
                            var e = new ItemReadedEventArgs{ Item = result };
                            callBack?.Invoke(e, state);
                        }
                    }
                    else
                    {
                        var e = new ItemReadedEventArgs() { Item = null };
                        callBack?.Invoke(e, state);
                        ItemOperationError?.Invoke(new ItemOperationErrorEventArgs() { Message = "Для указанного элемента в каталоге нет сохраненных данных" }, state);
                    }
                }
                else
                {
                    var e = new ItemReadedEventArgs() { Item = null };
                    callBack?.Invoke(e, state);
                    ItemOperationError?.Invoke(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, state);
                }
            }, null);
        }

        public static void CloneItem(CatalogueDomainContext cdc, int itemID, ItemReadedDelegate operationCallBack, object state)
        {
            GetItem(cdc, itemID, (e, s) => 
            {
                
                var clonedItem = e.Item;
                clonedItem.Name += " (Копия)";
                CreateItem(cdc, clonedItem, (ev, state2)=>
                {
                    var irea = new ItemReadedEventArgs {Item = clonedItem};
                    operationCallBack?.Invoke(irea, state2);
                }, s);
            }, null, true, state);
        }

        public static void CloneFromHistoryItem(CatalogueDomainContext cdc, int itemID, int revisionID, ItemReadedDelegate operationCallBack, object state)
        {
            GetItem(cdc, itemID, (e, s) =>
            {
                var clonedItem = e.Item;
                clonedItem.Name += $" (Копия редакции {revisionID})";
                CreateItem(cdc, clonedItem, (ev, state2) =>
                {
                    var irea = new ItemReadedEventArgs {Item = clonedItem};
                    operationCallBack?.Invoke(irea, state2);
                }, s);
            }, revisionID, true, state);
        }

        public static void GetDocuments(CatalogueDomainContext cdc, int itemID, ItemListingFinishedDelegate<AttachmentDescription> callBack, bool includeContent, int? revisionNumber, bool includeDeleted, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueDataListQuery(itemID, includeContent, revisionNumber, includeDeleted), LoadBehavior.KeepCurrent, o =>
            {
                if (callBack == null) return;
                var e = new ItemListingEventArgs<AttachmentDescription>();

                if (o.Entities == null)callBack.Invoke(e, state);
                else
                {
                    foreach (var a in o.Entities)
                    {
                        e.Result.Add(GetAttachmentDescription(a));
                    }
                    callBack.Invoke(e, state);
                }
            }, null);
        }

        public static void GetDocument(CatalogueDomainContext cdc, int itemID, ItemDataFinishedDelegate callBack, int? revisionNumber, string documentName, object state)
        {
            cdc.Load(cdc.GetAQ_CatalogueDataSingleQuery(itemID, revisionNumber, documentName), LoadBehavior.KeepCurrent, o =>
            {
                var e = new ItemDataEventArgs();
                if (o.Entities == null)
                {
                    callBack?.Invoke(e, state);
                }
                else
                {
                    if (callBack == null) return;
                    e.Result = GetAttachmentDescription(o.Entities.SingleOrDefault());
                    callBack(e, state);
                }
            }, null);
        }

        public static void DeleteDocumentInLastRevision(CatalogueDomainContext cdc, int itemID, ItemOperationFinishedDelegate callBack, string documentName, object state)
        {
            cdc.DeleteAQ_CatalogueDataSingle(itemID, documentName, o =>
            {
                if (callBack == null) return;
                var e = new ItemOperationEventArgs(itemID);
                callBack(e, state);
            }, null);
        }

        public static void CheckIsItemExistingOrDeleted(CatalogueDomainContext cdc, int itemID, ItemCheckedDelegate callBack, object state)
        {
            cdc.CheckAQ_CatalogueItems(itemID, o =>
            {
                if (callBack == null) return;
                var e = new ItemCheckEventArgs();
                if (o.Value == null)
                {
                    e.IsDeleted = null;
                    e.IsExisting = false;
                }
                else
                {
                    e.IsDeleted = o.Value;
                    e.IsExisting = true;
                }
                callBack(e, state);
            }, null);
        }

        public static void CheckIsItemDataExistingOrDeleted(CatalogueDomainContext cdc, int itemID, string documentName, ItemCheckedDelegate callBack, object state)
        {
            cdc.CheckAQ_CatalogueData(itemID, documentName, o =>
            {
                if (callBack == null) return;
                var e = new ItemCheckEventArgs();
                if (o.Value == null)
                {
                    e.IsDeleted = null;
                    e.IsExisting = false;
                }
                else
                {
                    e.IsDeleted = o.Value;
                    e.IsExisting = true;
                }
                callBack(e, state);
            }, null);
        }

        public static void DeleteItem(CatalogueDomainContext cdc, AQ_CatalogueItems aqci, ItemOperationFinishedDelegate callBack, object state)
        {
            if (aqci == null) return;
            var id = aqci.id;
            aqci.IsDeleted = true;
            cdc.SubmitChanges(a =>
            {
                cdc.AQ_CatalogueItems.Detach(aqci);
                callBack?.Invoke(new ItemOperationEventArgs(id), state);
            }, new object());
        }

        public static void RestoreItem(CatalogueDomainContext cdc, AQ_CatalogueItems aqci, ItemOperationFinishedDelegate callBack, object state)
        {
            if (aqci == null) return;
            var id = aqci.id;
            aqci.IsDeleted = false;
            cdc.SubmitChanges(a =>
            {
                cdc.AQ_CatalogueItems.Detach(aqci);
                callBack?.Invoke(new ItemOperationEventArgs(id), state);
            }, new object());
        }
        #endregion

        #region Преобразование данных

        public static CalculationDescription ItemToCalculationDescription(ItemDescription item)
        {
            var result = new CalculationDescription
            {
                ID = item.ID,
                CanBeDeleted = true,
                Name = item.Name,
                TargetID = item.ParentID ?? 0,
                Parameters = item.RevisionDescription
            };

            var stepDataText = item.AttachmentDescriptions.SingleOrDefault(a => a.DataItemName == "StepData")?.XmlData;
            if (stepDataText != null)
            {
                var xs = new XmlSerializer(typeof(List<Step>));
                result.StepData = (List<Step>)xs.Deserialize(XmlReader.Create(new StringReader(stepDataText)));
            }

            var resultDataText = item.AttachmentDescriptions.SingleOrDefault(a => a.DataItemName == "Results")?.XmlData;
            if (resultDataText == null) return result;

            var xs2 = new XmlSerializer(typeof(List<DataItem>));
            result.Results = (List<DataItem>)xs2.Deserialize(XmlReader.Create(new StringReader(resultDataText)));
            return result;
        }

        public static void ItemToIngot(ItemDescription item, out InteractiveParameters p, out InteractiveResults r)
        {
            var parametersDataText = item.AttachmentDescriptions.SingleOrDefault(a => a.DataItemName == "Parameters")?.XmlData;
            if (parametersDataText != null)
            {
                var xs = new XmlSerializer(typeof(InteractiveParameters));
                p = (InteractiveParameters)xs.Deserialize(XmlReader.Create(new StringReader(parametersDataText)));
            }
            else p = null;

            var resultDataText = item.AttachmentDescriptions.SingleOrDefault(a => a.DataItemName == "Results")?.XmlData;
            if (resultDataText != null)
            {
                var xs2 = new XmlSerializer(typeof(InteractiveResults));
                r = (InteractiveResults)xs2.Deserialize(XmlReader.Create(new StringReader(resultDataText)));
            }
            else r = null;
        }

        public static void ItemToSFM(ItemDescription item, out InteractiveParameters p, out InteractiveResults r)
        {
            ItemToIngot(item, out p, out r);
        }


        public static ItemDescription CalculationDescriptionToItem(CalculationDescription cd)
        {
            var result = new ItemDescription
            {
                ItemType = cd.StepData.Any(a=>a.Group=="Отчет")? "Report" : "Calculation",
                Name = cd.Name,
                ParentID = cd.TargetID,
                RevisionDescription = cd.Parameters
            };
            var xs = new XmlSerializer(typeof(List<Step>));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), cd.StepData);
            var stepData = new AttachmentDescription
            {
                XmlData = sb.ToString(),
                DataItemName = "StepData"
            };
            result.AttachmentDescriptions.Add(stepData);

            var xs2 = new XmlSerializer(typeof(List<DataItem>));
            var sb2 = new StringBuilder();
            xs2.Serialize(XmlWriter.Create(sb2), cd.Results);
            var results = new AttachmentDescription
            {
                XmlData = sb2.ToString(),
                DataItemName = "Results"
            };
            result.AttachmentDescriptions.Add(results);

            return result;
        }

        public static ItemDescription IngotCalculationToItem(InteractiveParameters p, InteractiveResults r)
        {
            var result = new ItemDescription {ItemType = "Ingot"};
            var xs = new XmlSerializer(typeof(InteractiveParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), p);
            var stepData = new AttachmentDescription
            {
                XmlData = sb.ToString(),
                DataItemName = "Parameters"
            };
            result.AttachmentDescriptions.Add(stepData);

            var xs2 = new XmlSerializer(typeof(InteractiveResults));
            var sb2 = new StringBuilder();
            xs2.Serialize(XmlWriter.Create(sb2), r);
            var results = new AttachmentDescription
            {
                XmlData = sb2.ToString(),
                DataItemName = "Results"
            };
            result.AttachmentDescriptions.Add(results);
            return result;
        }

        public static ItemDescription SFMCalculationToItem(InteractiveParameters p, InteractiveResults r)
        {
            var result = new ItemDescription { ItemType = "SFM" };
            var xs = new XmlSerializer(typeof(InteractiveParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), p);
            var stepData = new AttachmentDescription
            {
                XmlData = sb.ToString(),
                DataItemName = "Parameters"
            };
            result.AttachmentDescriptions.Add(stepData);

            var xs2 = new XmlSerializer(typeof(InteractiveResults));
            var sb2 = new StringBuilder();
            xs2.Serialize(XmlWriter.Create(sb2), r);
            var results = new AttachmentDescription
            {
                XmlData = sb2.ToString(),
                DataItemName = "Results"
            };
            result.AttachmentDescriptions.Add(results);
            return result;
        }
        #endregion
    }

    public class ItemOperationEventArgs : EventArgs
    {
        public int ItemID { get; set; }
        public Rect ItemRectangle { get; set; }
        public string ItemType { get; set; }

        public ItemOperationEventArgs(int id)
        {
            ItemID = id;
        }

        public ItemOperationEventArgs(int id, Rect itemrectangle, string itemtype)
        {
            ItemID = id;
            ItemRectangle = itemrectangle;
            ItemType = itemtype;
        }
    }

    public class ItemListingEventArgs<T> : EventArgs
    {
        public List<T> Result { get; set; }

        public ItemListingEventArgs()
        {
            Result = new List<T>();
        }
    }

    public class ItemDataEventArgs : EventArgs
    {
        public AttachmentDescription Result;

        public ItemDataEventArgs()
        {
            Result = new AttachmentDescription();
        }
    }

    public class ItemReadedEventArgs : EventArgs
    {
        public ItemDescription Item;

        public ItemReadedEventArgs()
        {
            Item = new ItemDescription();
        }
    }

    public class ItemUrlEventArgs : EventArgs
    {
        public IEnumerable<string> Url;
    }

    public class ItemCheckEventArgs : EventArgs
    {
        public bool IsExisting;
        public bool? IsDeleted;
    }

    public class ItemOperationErrorEventArgs : EventArgs
    {
        public string Message;
    }

    public class AttachmentDescription
    {
        public byte[] BinaryData { get; set; }
        public string XmlData { get; set; }
        public string DataItemName { get; set; }
        public string DataDescription { get; set; }
        public string BinaryDataMIME { get; set; }
    }

    public class ItemDescription
    {
        public string Name { get; set; }
        public string RevisionDescription { get; set; }
        public DateTime? RevisionDate { get; set; }
        public string RevisionAuthor { get; set; }
        public string ItemType { get; set; }
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public List<AttachmentDescription> AttachmentDescriptions { get; set; }

        public ItemDescription()
        {
            AttachmentDescriptions = new List<AttachmentDescription>();
        }
    }
}
