﻿using System;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace AQCatalogueLibrary
{
    public static class CatalogueHelper
    {
        public static UserControl GetIconByType(string t)
        {
            var vb = new UserControl();
            var i = new Image
            {
                Source =
                    new BitmapImage
                    {
                        UriSource = new Uri($@"/AQResources;component/Images/catalogue/{t}.png", UriKind.Relative)
                    }
            };
            vb.Content = i;
            return vb;
        }

        public static ICatalogueItemProcessor GetItemProcessorByType(string t)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            return currentAssembly.CreateInstance($"AQCatalogueLibrary.{t}ItemProcessor") as ICatalogueItemProcessor;
        }

        public static ICommandAcceptor GetCommandAcceptorByType(string command)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            return currentAssembly.CreateInstance($"AQCatalogueLibrary.{command}Acceptor") as ICommandAcceptor;
        }
    }
}
