﻿using System;
using System.Collections.Generic;
using System.Windows;
using AQControlsLibrary;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQSelection;
using AQStepFactoryLibrary;

namespace AQCatalogueLibrary
{
    public partial class CatalogueItemEditor
    {
        public CatalogueItemEditor()
        {
            InitializeComponent();
            _cdc = CatalogueOperations.CreateDomainContext();
            Initialized?.Invoke(this, new EventArgs());
            TargetsLoaded?.Invoke(this, new CatalogueItemOperationEventArgs(null, null));
        }
        
        protected CalculationService Cs;
        private readonly CatalogueDomainContext _cdc;
        public delegate void CatalogueItemOperationDelegate(object o, CatalogueItemOperationEventArgs e);
        public delegate void InitializedDelegate(object o, EventArgs e);
        public event CatalogueItemOperationDelegate CatalogueItemLoaded;
        public event CatalogueItemOperationDelegate CatalogueItemSaved;
        public event CatalogueItemOperationDelegate CatalogueItemDeleted;
        public event CatalogueItemOperationDelegate TargetsLoaded;
        public event InitializedDelegate Initialized;
        public ItemDescription CurrentCatalogueItem;
        private bool _newItem;
        
        private string _querySQL;

        public string Description 
        {
            get { return QueryDescription.Text; }
            set { QueryDescription.Text = value; }
        }

        
        public void LoadCalculation(int id)
        {
            CatalogueOperations.GetItem(_cdc, id, (e, state) =>
            {
                LoadCalculationEnd(e, new CatalogueItemOperationEventArgs(CatalogueOperations.ItemToCalculationDescription(e.Item), state));
            }, null, true, null);
        }

        public void LoadAbstractItem(int id)
        {
            CatalogueOperations.GetItem(_cdc, id, (e, state) =>
            {
                LoadCalculationEnd(e, new CatalogueItemOperationEventArgs(e.Item, state));
            }, null, true, null);
        }
        
        public void SetToEmptySelection()
        {
            MainCatalogue.SelectItem(-1, false);
        }

        private void LoadCalculationEnd(ItemReadedEventArgs e, CatalogueItemOperationEventArgs callbackEventArgs)
        {
            Dispatcher.BeginInvoke(delegate
            {
                CurrentCatalogueItem = e.Item;
                QueryName.Text = e.Item.Name;
                QueryDescription.Text = e.Item.RevisionDescription;
                if (e.Item.ParentID.HasValue) MainCatalogue.SelectItem(e.Item.ParentID.Value, false);
                CatalogueItemLoaded?.Invoke(this, callbackEventArgs);
            });
        }
                
        public void SaveCalculation(List<Step> stepData, List<DataItem> results, Task task, bool canBeDeleted, bool newItem)
        {
            if (CurrentCatalogueItem != null && !newItem)
            {
                UpdateCurrentCatalogueCalculation(stepData, results);
                CatalogueOperations.CreateRevision(_cdc, CurrentCatalogueItem.ID, QueryName.Text, QueryDescription.Text, CurrentCatalogueItem.ItemType, (e, t) =>
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        CatalogueItemSaved?.Invoke(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, t));
                    });
                }, task, CurrentCatalogueItem.AttachmentDescriptions);
                
            }
            else
            {
                CurrentCatalogueItem = new ItemDescription();
                UpdateCurrentCatalogueCalculation(stepData, results);
                CatalogueOperations.CreateItem(_cdc, CurrentCatalogueItem, (e, t) =>
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        CurrentCatalogueItem.ID = e.ItemID;
                        CatalogueItemSaved?.Invoke(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, t));
                    });
                }, task);
            }
        }

        public void SaveAbstractItem(ItemDescription item, Task task, bool canBeDeleted, bool newItem)
        {
            _newItem = newItem;
            if (CurrentCatalogueItem != null && !_newItem)
            {
                UpdateCurrentCatalogueItem(item);
                CatalogueOperations.CreateRevision(_cdc, CurrentCatalogueItem.ID, QueryName.Text, QueryDescription.Text, CurrentCatalogueItem.ItemType, (e, t) =>
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        CatalogueItemSaved?.Invoke(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, t));
                    });
                }, task, CurrentCatalogueItem.AttachmentDescriptions);

            }
            else
            {
                CurrentCatalogueItem = new ItemDescription();
                UpdateCurrentCatalogueItem(item);
                CatalogueOperations.CreateItem(_cdc, CurrentCatalogueItem, (e, t) =>
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        CurrentCatalogueItem.ID = e.ItemID;
                        CatalogueItemSaved?.Invoke(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, t));
                    });
                }, task);
            }
        }

        public void SaveSQLQuery(string sql, Task task, bool canBeDeleted, bool newitem)
        {
            _newItem = newitem;
            _querySQL = sql;
            if (Cs == null) Cs = Services.GetCalculationService();
            Cs.BeginGetQueryResults(sql, QueryName.Text, null, null, GetQueryResultsDone, task);
        }

        public void GetQueryResultsDone(IAsyncResult iar)
        {
            var results = Cs.EndGetQueryResults(iar);
            Dispatcher.BeginInvoke(delegate { SaveCalculation(StepFactory.CreateSQLQuery(_querySQL, QueryName.Text), results.Fields, iar.AsyncState as Task, true, _newItem); });
        }

        public void DeleteQuery(Task task, bool clearEditor)
        {
            if (CurrentCatalogueItem != null)
            {
                CatalogueOperations.DeleteItem(_cdc, CurrentCatalogueItem.ID, (e, s) =>
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                            if (clearEditor)
                            {
                                QueryName.Text = string.Empty;
                                QueryDescription.Text = string.Empty;
                            }
                            CurrentCatalogueItem = null;
                        CatalogueItemDeleted?.Invoke(this, new CatalogueItemOperationEventArgs(null, s));
                    });
                 }, task);
             }
        }

        public bool CheckName()
        {
            if (MainCatalogue.URL == null) return false;
            return !string.IsNullOrWhiteSpace(QueryName.Text);
        }

        public void MainCatalogue_ItemDoubleClick(object o, ItemOperationEventArgs e)
        {
            FolderSelector.Visibility = Visibility.Collapsed;
        }

        private void UpdateCurrentCatalogueCalculation(List<Step> stepData, List<DataItem> results)
        {
            CalculationDescription cd = new CalculationDescription
            {
                StepData = stepData,
                Results = results,
                Name = QueryName.Text,
                Parameters = QueryDescription.Text
            };

            if (MainCatalogue.SelectedFolderID.HasValue) cd.TargetID = MainCatalogue.SelectedFolderID.Value;
            var id = CurrentCatalogueItem.ID;
            CurrentCatalogueItem = CatalogueOperations.CalculationDescriptionToItem(cd);
            CurrentCatalogueItem.ID = id;
        }

        private void UpdateCurrentCatalogueItem(ItemDescription item)
        {
            CurrentCatalogueItem.AttachmentDescriptions = item.AttachmentDescriptions;
            CurrentCatalogueItem.Name = QueryName.Text;
            CurrentCatalogueItem.RevisionDescription = QueryDescription.Text;
            CurrentCatalogueItem.ItemType = item.ItemType;
            if (MainCatalogue.SelectedFolderID.HasValue) CurrentCatalogueItem.ParentID = MainCatalogue.SelectedFolderID.Value;
        }

        public string GetQueryName()
        {
            return QueryName.Text;
        }

        public IEnumerable<string> GetQueryUrl()
        {
            return MainCatalogue.URL;
        }

        public string GetQueryDescription()
        {
            return QueryDescription.Text;
        }

        public class Parameters
        {
            public string SQL;
            public string TableName;
        }

        private void ClosePopupButton_Click(object sender, RoutedEventArgs e)
        {
            FolderSelector.Visibility = Visibility.Collapsed;
        }

        private void OpenPopupButton_Click(object sender, RoutedEventArgs e)
        {
            if (!FolderPopup.IsOpen) FolderSelector.Visibility = Visibility.Visible;
            else FolderSelector.Visibility = (FolderSelector.Visibility==Visibility.Collapsed)
                    ? Visibility.Visible 
                    : Visibility.Collapsed;
            FolderPopup.IsOpen = true;
        }
    }

    public class CatalogueItemOperationEventArgs : EventArgs
    {
        public object Cd;
        public object State;
        public CatalogueItemOperationEventArgs(object cd, object state)
        {
            Cd = cd;
            State = state;
        }
    }
}
