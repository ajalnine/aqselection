﻿using System.Windows;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public partial class IngotItemProcessor : ICatalogueItemProcessor
    {
        private int _currentItemID = -1;
        
        public IngotItemProcessor()
        {
            DateRangeRequired = false;
            InitializeComponent();
        }

        public event CommandDelegate CommandEmitted;

        public void InitializeProcessor(int itemID)
        {
            _currentItemID = itemID;
        }
        
        private void TextImageButtonBase_Click(object sender, RoutedEventArgs e)
        {
            CommandEmitted?.Invoke(this, new CommandEventArgs((sender as TextImageButtonBase)?.Tag.ToString(), _currentItemID, "Calculation"));
        }

        public bool DateRangeRequired {get;set;}
    }
}
