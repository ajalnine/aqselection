﻿using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public delegate void CommandDelegate(object o, CommandEventArgs e);

    public interface ICatalogueItemProcessor
    {
        event CommandDelegate CommandEmitted; 
        void InitializeProcessor(int itemID);
    }

    public class CommandEventArgs : EventArgs
    {
        public string Command { get; set; }
        public string ItemType {get; set; }
        public int ItemID { get; set; }
        public Dictionary<string, object> CustomArguments { get; set; }

        public CommandEventArgs()
        {
            CustomArguments = new Dictionary<string, object>();
        }

        public CommandEventArgs(string command, int itemid, string itemtype)
        {
            Command = command;
            ItemID = itemid;
            ItemType = itemtype;
        }
    }
}
