﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQBasicControlsLibrary;
using AQControlsLibrary;
using AQCalculationsLibrary;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using AQSelection;
using AQCatalogueLibrary;
using AQCalculationParametersLibrary;

namespace AQCatalogueLibrary
{
    public partial class ReportItemProcessor : UserControl, ICatalogueItemProcessor
    {
        private int CurrentItemID = -1;
        public event CommandDelegate CommandEmitted;
        public List<Step> Parameters;
        public List<Step> Steps;
        public string Description = string.Empty;
        public CalculationDescription CD;
        public ReportItemProcessor()
        {
            InitializeComponent();
            EnableButtons(false);
        }

        public void InitializeProcessor(int itemID)
        {
            CatalogueDomainContext cdc = CatalogueOperations.CreateDomainContext();
            CurrentItemID = itemID;
            CatalogueOperations.GetItem(cdc, CurrentItemID, (ev, state) =>
            {
                CD = CatalogueOperations.ItemToCalculationDescription(ev.Item);
                Parameters = CD.StepData.Where(a => a.Group == "Параметры").ToList();
                Steps = CD.StepData;
                Description = "Расчет: " + CD.Name;
                this.Dispatcher.BeginInvoke(delegate() 
                {
                    ParametersScan ps = new ParametersScan();
                    ps.GetAllParametersAsync(CurrentItemID, (o, e) => 
                    {
                        int Visible = 0;
                        ParametersPlaceHolder.Children.Clear();
                        foreach (var c in e.Found)
                        {
                            foreach (var s in c.CalcStep)
                            {
                                string xml = s.ParametersXML;
                                Match m = Regex.Match(xml, @"<InputName>([a-z]*)<", RegexOptions.IgnoreCase);
                                if (m.Success)
                                {
                                    string InputName = m.Groups[1].Captures[0].Value;
                                    Type t = Type.GetType("AQCalculationParametersLibrary." + InputName + ", AQCalculationParametersLibrary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
                                    UserControl uc = Activator.CreateInstance(t) as UserControl;
                                    ((IAQParameterView)uc).Setup(xml, MainDateRangePicker);
                                    uc.UpdateLayout();
                                    uc.Name = Guid.NewGuid().ToString();
                                    ParametersPlaceHolder.Children.Add(uc);
                                    if (InputName!="InputDateRange")Visible++;
                                }
                            }
                        }
                        ParametersHeader.Visibility = (Visible > 0) ? Visibility.Visible : Visibility.Collapsed;
                        EnableButtons(true);
                        ProgressIndicator.Visibility = System.Windows.Visibility.Collapsed;
                    });
                });
            }, null, true, null);
        }

        private void TextImageButtonBase_Click(object sender, RoutedEventArgs e)
        {
            CommandEventArgs cea = new CommandEventArgs((sender as TextImageButtonBase).Tag.ToString(), CurrentItemID, "Calculation");
            cea.CustomArguments = GetArguments();
            if (CommandEmitted != null) CommandEmitted(this, cea);
        }

        private void EnableButtons(bool Enable)
        {
            foreach (TextImageButtonBase a in ButtonsPlaceHolder.Children)
            {
                a.IsEnabled = Enable;
            }
        }

        public Dictionary<string, object> GetArguments()
        {
            Dictionary<string, object> Result = new Dictionary<string, object>();
            Result.Add("От", MainDateRangePicker.From);
            Result.Add("До", MainDateRangePicker.To);
            Result.Add("Расчет", Steps);
            Result.Add("Описание", Description);
            Result.Add("CD", CD);
            if (ParametersHeader != null && ParametersPlaceHolder != null && ParametersPlaceHolder.Children.Count > 0)
            {
                Result.Add("Параметры", GetInputs());
            }
            return Result;
        }

        private List<InputDescription> GetInputs()
        {
            List<InputDescription> Result = new List<InputDescription>();

            foreach (var c in ParametersPlaceHolder.Children)
            {
                IAQParameterView pv = c as IAQParameterView;
                if (pv != null)
                {
                    foreach (var v in pv.GetValues())
                    {
                        if (!Result.Select(a => a.Name).Distinct().Contains(v.Name)) Result.Add(v);
                    }
                }
            }
            return Result;
        }
    }
}
