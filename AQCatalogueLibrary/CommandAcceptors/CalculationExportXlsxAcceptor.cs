﻿using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Windows.Controls;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class CalculationExportXlsxAcceptor: ICommandAcceptor
    {
        private CalculationService _cs;
        private UserControl _parent;
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public CalculationExportXlsxAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            _parent = parent;
            _cs = Services.GetCalculationService();
            
            int calculationID = e.ItemID;
            
            if (e.CustomArguments == null || !e.CustomArguments.ContainsKey("От") || !e.CustomArguments.ContainsKey("До")) return;
            var from = e.CustomArguments["От"] as DateTime?;
            var to = e.CustomArguments["До"] as DateTime?;
            var parameters = e.CustomArguments.ContainsKey("Параметры") ? e.CustomArguments["Параметры"] as List<InputDescription> : null;

            var task = new Task(new [] { "Выполнение расчета", "Формирование файла Xlsx" }, "Экспорт в Xlsx", taskPanel);
            task.SetState(0, TaskState.Processing);
            _cs.BeginExecuteCalculation(calculationID, @from ?? DateTime.Now, @to ?? DateTime.Now, parameters, iar=>
            {
                var sr = _cs.EndExecuteCalculation(iar);
                if (sr.Success)
                {
                    task.SetState(0, TaskState.Ready);
                    task.SetState(1, TaskState.Processing);
                    _cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, iar2 => 
                    {
                        sr = _cs.EndGetAllResultsInXlsx(iar2);
                        if (sr.Success)
                        {
                            task.SetState(1, TaskState.Ready);
                            _parent.Dispatcher.BeginInvoke(() =>
                            {
                                commandFinished?.Invoke();
                                Services.GetAQService().BeginWriteLog("Catalogue: Calculation to xlsx " + e.CustomArguments["Описание"], null, null);
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                            });
                        }
                        else
                        {
                            task.SetState(1, TaskState.Error);
                            task.SetMessage(sr.Message);
                            commandFinished?.Invoke();
                            _cs.BeginDropData(sr.TaskDataGuid, null, null);
                        }
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    commandFinished?.Invoke();
                    _cs.BeginDropData(sr.TaskDataGuid, null, null);
                }
            }, task);
        }
    }
}
