﻿using System;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public interface ICommandAcceptor
    {
        bool PopupMustBeClosed { get; set; }
        bool PopupCanBeReopened { get; set; }

        void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished);
    }
}
