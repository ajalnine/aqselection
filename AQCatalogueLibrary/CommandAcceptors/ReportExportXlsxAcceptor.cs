﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using System.Windows.Controls;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQChartViewLibrary;
using AQSelection;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class ReportExportXlsxAcceptor: ICommandAcceptor
    {
        private CalculationService _cs;
        private UserControl _parent;
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public ReportExportXlsxAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent,
            Action commandFinished)
        {
            _parent = parent;
            _cs = Services.GetCalculationService();
            if (e.CustomArguments == null || !e.CustomArguments.ContainsKey("От") ||
                !e.CustomArguments.ContainsKey("До")) return;
            var from = e.CustomArguments["От"] as DateTime?;
            var to = e.CustomArguments["До"] as DateTime?;
            var parameters = e.CustomArguments.ContainsKey("Параметры")
                ? e.CustomArguments["Параметры"] as List<InputDescription>
                : null;
            var steps = e.CustomArguments["Расчет"] as List<Step>;
            var CD = e.CustomArguments["CD"] as CalculationDescription;

            var stepNames = steps.Select(a=>a.Name).ToArray().Concat(new[] {"Формирование файла Xlsx"}).ToArray();
            var task = new Task(stepNames, "Экспорт в Xlsx", taskPanel);
            var reporter = new Reporter(_cs, task, CD, false, from, to, null);
            reporter.AttachToSlidePanel(new SlidePanel());
            
            task.SetState(0, TaskState.Processing);
            _cs.BeginInitCalculation(from.Value, to.Value, parameters, iar =>
            {
                var sr = _cs.EndInitCalculation(iar);
                task.CustomData = sr.TaskDataGuid;
                try
                {
                    reporter.DoServerSteps(() =>
                    {
                        var filename = task.CustomData.ToString();
                        task.SetState(task.CurrentStep, TaskState.Processing);
                        _cs.BeginGetAllResultsInXlsx(filename, iar2 =>
                        {
                            var sr2 = _cs.EndGetAllResultsInXlsx(iar2);
                            task.SetState(task.CurrentStep, TaskState.Ready);
                            reporter.DetachFromSlidePanel();
                            _parent.Dispatcher.BeginInvoke(()=>
                            {
                                Services.GetAQService().BeginWriteLog("Catalogue: Report to xlsx" + ((CalculationDescription)e.CustomArguments["CD"]).Name, null, null);
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                    $@"GetFile.aspx?FileName={HttpUtility.UrlEncode(sr2.TaskDataGuid)}&FName={
                                            HttpUtility.UrlEncode($"Расчет {DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)}.xlsx")
                                        }&ContentType={HttpUtility.UrlEncode("application/msexcel")}"));
                            });
                        }, task);
                    });
                }
                catch (Exception)
                {
                    task.SetState(task.CurrentStep, TaskState.Error);
                    task.SetMessage("Ошибка");
                    reporter.DetachFromSlidePanel();
                }
            }, task);
        }
    }
}
