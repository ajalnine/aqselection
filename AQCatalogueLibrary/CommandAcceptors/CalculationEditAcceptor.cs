﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class CalculationEditAcceptor : ICommandAcceptor
    {
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public CalculationEditAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }
        
        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            var p = parent as IAQModule;
            if (p != null)
            {
                Services.GetAQService().BeginWriteLog("Catalogue: Calculation Edit " + (e.CustomArguments["Описание"].ToString()), null, null);
                Signal.Send(p.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertStoredCalculation, e.ItemID, (ct, ca, r) => 
                {
                    commandFinished?.Invoke();
                });
            }
        }

    }
}
