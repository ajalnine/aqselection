﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using System.Windows.Controls;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQChartViewLibrary;
using AQSelection;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class ReportPreviewAcceptor: ICommandAcceptor
    {
        private CalculationService _cs;
        private UserControl _parent;
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public ReportPreviewAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent,
            Action commandFinished)
        {
            _parent = parent;
            _cs = Services.GetCalculationService();
            if (e.CustomArguments == null || !e.CustomArguments.ContainsKey("От") ||
                !e.CustomArguments.ContainsKey("До")) return;
            var from = e.CustomArguments["От"] as DateTime?;
            var to = e.CustomArguments["До"] as DateTime?;
            var description = e.CustomArguments["Описание"] as string;
            var parameters = e.CustomArguments.ContainsKey("Параметры")
                ? e.CustomArguments["Параметры"] as List<InputDescription>
                : null;
            var CD = e.CustomArguments["CD"] as CalculationDescription;
            
            var stepNames = CD.StepData.Select(a=>a.Name).ToArray().Concat(new[] {"Анализ данных"}).ToArray();
            var task = new Task(stepNames, "Анализ данных", taskPanel);
            var reporter = new Reporter(_cs, task, CD, false, from, to, null);
            reporter.AttachToSlidePanel(new SlidePanel());
            
            task.SetState(0, TaskState.Processing);
            _cs.BeginInitCalculation(from.Value, to.Value, parameters,
            iar =>
            {
                var sr = _cs.EndInitCalculation(iar);
                task.CustomData = sr.TaskDataGuid;
                try
                {
                    reporter.DoServerSteps(() =>
                    {
                        task.SetState(task.CurrentStep, TaskState.Processing);
                        reporter.DetachFromSlidePanel();
                        _parent.Dispatcher.BeginInvoke(
                            () => _cs.BeginGetTablesSLCompatible(task.CustomData.ToString(), i =>
                            {
                                var tables = _cs.EndGetTablesSLCompatible(i);
                                if (tables == null) return;
                                task.AdvanceProgress();
                                task.SetMessage("Данные готовы");
                                _parent.Dispatcher.BeginInvoke(() =>
                                {
                                    var ca = new Dictionary<string, object>
                                    {
                                        {"From", from},
                                        {"To", to},
                                        {"SourceData", tables},
                                        {"Calculation", CD.StepData},
                                        {"Description", description},
                                        {"New", true},
                                    };
                                    Services.GetAQService().BeginWriteLog("Catalogue: Report to analysis " + ((CalculationDescription)e.CustomArguments["CD"]).Name, null, null);
                                    Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                                });
                            }, task));
                    });
                }
                catch (Exception)
                {
                    task.SetState(task.CurrentStep, TaskState.Error);
                    task.SetMessage("Ошибка");
                    reporter.DetachFromSlidePanel();
                }
            }, task);
        }
    }
}
