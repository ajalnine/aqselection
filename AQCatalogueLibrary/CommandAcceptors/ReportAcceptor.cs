﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class ReportAcceptor : ICommandAcceptor
    {
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public ReportAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }
        
        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            var p = parent as IAQModule;
            if (p != null)
            {
                var from = e.CustomArguments["От"] as DateTime?;
                var to = e.CustomArguments["До"] as DateTime?;
                var steps = e.CustomArguments["Расчет"] as List<Step>;
                var description = e.CustomArguments["Описание"].ToString();
                var cd = e.CustomArguments["CD"] as CalculationDescription;
                var parameters = e.CustomArguments.ContainsKey("Параметры") ? e.CustomArguments["Параметры"] as List<InputDescription> : null;
                var ca = new Dictionary<string, object>
                {
                    {"From",from},
                    {"To", to},
                    {"Calculation", steps},
                    {"CD", cd},
                    {"Description", description},
                    {"Parameters", parameters},
                    {"New", true},
                };

                parent.Dispatcher.BeginInvoke(() =>
                {
                    Services.GetAQService().BeginWriteLog("Catalogue: Report " + ((CalculationDescription)e.CustomArguments["CD"]).Name, null, null);
                    Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.InsertReport, ca, null);
                    commandFinished?.Invoke();
                });
            }
        }

    }
}
