﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using ApplicationCore;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class IngotEditAcceptor : ICommandAcceptor
    {
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public IngotEditAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }
        
        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            var p = parent as IAQModule;
            if (p == null) return;
            var commandArgument = new Dictionary<string, object> { { "URI", "IngotCalculator.MainPage" } };
            Signal.Send(p.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.Load, commandArgument, (ct, ca, r) => 
            {
                var loadedEditor = r as IAQModule;
                if (loadedEditor != null) Signal.Send(p.GetAQModuleDescription(), loadedEditor.GetAQModuleDescription(), Command.InsertStoredCalculation, e.ItemID, null);
                commandFinished?.Invoke();
            });
        }
    }
}
