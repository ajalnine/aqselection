﻿using System;
using System.Collections.Generic;
using System.Windows.Browser;
using System.Windows.Controls;
using AQCalculationsLibrary;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQCatalogueLibrary
{
    public class IngotExportXlsxAcceptor: ICommandAcceptor
    {
        private CalculationService _cs;
        private UserControl _parent;
        public bool PopupMustBeClosed { get; set; }
        public bool PopupCanBeReopened { get; set; }

        public IngotExportXlsxAcceptor()
        {
            PopupMustBeClosed = false;
            PopupCanBeReopened = true;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            int calculationID = e.ItemID;
            
            _parent = parent;
            _cs = Services.GetCalculationService();
            var cdc = CatalogueOperations.CreateDomainContext();

            CatalogueOperations.GetItem(cdc, calculationID, (ev, state) =>
            {
                var item = ev.Item;
                InteractiveParameters p;
                InteractiveResults r;
                CatalogueOperations.ItemToIngot(item, out p, out r);

                var task = new Task(new [] { "Чтение данных", "Формирование файла Xlsx" }, "Экспорт в Xlsx", taskPanel);
                task.SetState(0, TaskState.Processing);
                var sl = new SLDataTable
                {
                    ColumnNames = new List<string> {"Параметр", "Значение"},
                    DataTypes = new List<string> {"String", "Double"},
                    TableName = "Расчет",
                    Table = new List<SLDataRow>()
                };

                foreach (var a in p.Children)
                {
                    sl.Table.Add(new SLDataRow { Row = new List<object> { a.Caption, a.CurrentValue } });
                }
                foreach (var a in r.Children)
                {
                    sl.Table.Add(new SLDataRow { Row = new List<object> { a.Caption, a.CurrentValue } });
                }

                var sldt = new List<SLDataTable> {sl};
                _cs.BeginFillDataWithSLTables(sldt, iar =>
                {
                    StepResult sr = _cs.EndFillDataWithSLTables(iar);
                    if (sr.Success)
                    {
                        task.SetState(0, TaskState.Ready);
                        task.SetState(1, TaskState.Processing);
                        _cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, iar2 =>
                        {
                            sr = _cs.EndGetAllResultsInXlsx(iar2);
                            task.SetState(1, TaskState.Ready);
                            _parent.Dispatcher.BeginInvoke(() =>
                            {
                                commandFinished?.Invoke();
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                            });
                        }, task);
                    }
                    else
                    {
                        task.SetState(0, TaskState.Error);
                        commandFinished?.Invoke();
                        _cs.BeginDropData(sr.TaskDataGuid, null, null);
                    }
                }, task);

            }, null, true, null);
        }
    }
}
