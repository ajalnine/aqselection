﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathClasses
{
    public static partial class AQMath
    {
        public static List<object> Filter(List<object> Input, string Substring)
        {
            return Input.Where(a => a.ToString().Contains(Substring)).ToList<object>();
        }

        public static List<object> MovingAverageFilter(List<object> Input, int WindowSize)
        {
            if (Input == null) return null;
            var m = Input.OfType<Double>().ToList();
            if (m.Count() == 0) return null; 
            if (WindowSize < 2) return Input;

            List<object> Res = new List<object>();

            for (int x = 0; x < m.Count(); x++)
            {
                double sum = 0; 
                double count = 0;
                for (int i = -WindowSize / 2; i < WindowSize / 2; i++)
                {
                    int index = x + i;
                    if (index >= 0 && index < Input.Count)
                    {
                        sum += (double)m[index];
                        count++;
                    }
                }
                Res.Add(sum / count);
            }
            return Res;
        }

        public static List<object> MedianFilter(List<object> Input, int WindowSize)
        {
            if (Input == null) return null;
            var m = Input.OfType<Double>().ToList();
            if (m.Count() == 0) return null;
            if (WindowSize < 2) return Input;

            List<object> Res = new List<object>();

            for (int x = 0; x < m.Count(); x++)
            {
                List<double> Window = new List<double>();
                for (int i = -WindowSize / 2; i < WindowSize / 2; i++)
                {
                    int index = x + i;
                    if (index >= 0 && index < Input.Count)
                    {
                        Window.Add((double)m[index]);
                    }
                }
                if (Window.Count()>0)Res.Add(Window.OrderBy(a=>a).ToList()[Window.Count()/2]);
            }
            return Res;
        }

        public static List<object> LowessFilter(List<object> X, List<object> Y,  double Bandwidth, double Iterations)
        {
            #region Проверки и инициализации
            if (X == null || Y == null) return null;
            List<double> xval = X.OfType<double>().ToList();
            List<double> yval = Y.OfType<double>().ToList();
            List<double> Residuals = null;
            List<object> Result = null;
            
            if (xval.Count < 3 || xval.Count != yval.Count) return Y;
            int WindowSize = (int)(Bandwidth * xval.Count);
            if (WindowSize < 3) return Y;
                
            List<double> Weights = new List<double>(); 
            for (int i = 0; i < xval.Count; i++) Weights.Add(1);
            #endregion

            for (int PassLeft = (int)Iterations - 1; PassLeft >= 0; PassLeft--)
            {
                Residuals = new List<double>();
                Result = new List<object>();
                
                for (int i = 0; i < xval.Count; ++i) //Один проход
                {
                    int WindowStart = i - WindowSize / 2;
                    if (WindowStart < 0) WindowStart = 0;
                    int WindowEnd = i + WindowSize / 2;
                    if (WindowEnd >= xval.Count) WindowEnd  = xval.Count - 1;

                    double sw = 0, sx = 0, sx2 = 0, sy = 0, sxy = 0;
                    double h = Math.Max(xval[i] - xval[WindowStart], xval[WindowEnd] - xval[i]);

                    for (int k = WindowStart; k <= WindowEnd; k++)
                    {
                        double w = h > 0 ? Math.Pow(1 - Math.Pow(Math.Abs((xval[i] - xval[k]) / h), 3), 3) * Weights[k] : Weights[k];
                        sw += w;
                        sx += xval[k] * w;
                        sy += yval[k] * w;
                        sx2 += xval[k] * xval[k] * w;
                        sxy += yval[k] * xval[k] * w;
                    }

                    double mx = sx / sw;
                    double my = sy / sw;
                    double div = (sx2 / sw - mx * mx);
                    double K =  (div==0) ? 0 : (sxy / sw - mx * my) / div;
                    double B = my - K * mx;
                    double yt = K * xval[i] + B;
                    Result.Add(yt);
                    Residuals.Add(Math.Abs(yval[i] - yt));
                }

                #region пересчет весов по остаткам

                if (PassLeft == 0) break;
                double ResidualsMedian = AQMath.AggregateMedian(Residuals.OfType<object>().ToList()).Value;
                if (ResidualsMedian == 0) break;
                for (int i = 0; i < xval.Count; ++i)
                {
                    double arg = Residuals[i] / (6 * ResidualsMedian);
                    Weights[i] = (arg >= 1) ? 1 : Math.Pow(1 - arg * arg, 2);
                }

                #endregion
            }
            if (Result != null) return Result.OfType<object>().ToList();
            else return null;
        }
    }
}
