﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MathClasses
{
    public static partial class AQMath
    {
        public static Double? AggregateMin(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return m.Min();
        }
        public static Double? AggregateMax(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null; 
            return m.Max();
        }
        public static Double? AggregateRange(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return m.Max() - m.Min();
        }
        public static Double? AggregateMean(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return m.Sum() / m.Count();
        }
        public static Double? AggregateMedian(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return AggregatePercentile(data,50);
        }

        public static Double? AggregateIQR(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return AggregatePercentile(data, 75) - AggregatePercentile(data, 25);
        }

        public static Double? AggregatePercentile(List<object> data, Double? percent)
        {
            if (!percent.HasValue) return null;
            var m = data.OfType<Double>();
            var d = m.OrderBy(a => a);
            var n = m.Count();
            if (n == 0) return null;
            if (n == 1) return (Double?)m.Single();
            int minIndex = (int) Math.Floor((n - 1) * percent.Value / 100d);
            if (percent!=50 || n % 2 == 1) return (Double?) d.ElementAt(minIndex);
            return ((Double?)d.ElementAt(minIndex)+(Double?)d.ElementAt(minIndex+1))/2;
        }

        public static Double? PercentInRange(List<object> data, Double? RangeStart, Double? RangeEnd)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            int c= m.Count();
            if (RangeStart.HasValue) c -= m.Where(a=> a < RangeStart.Value).Count();
            if (RangeEnd.HasValue) c -= m.Where(a => a > RangeEnd.Value).Count();
            return (Double?) ((double)c)/(double)m.Count()*100;
        }

        public static Double? AggregateSum(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            return m.Sum();
        }

        public static Double? AggregateStDev(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            
            Double? Avg = AggregateMean(data);
            int Cnt = (int)AggregateCount(data);
            if (Avg == null) return null;
            if (Cnt <= 1) return 0;
            Double? m2 = Moment2(data);
            if (m2.HasValue) return Math.Sqrt(m2.Value / (Cnt - 1));
            else return null;
        }

        public static Double? AggregateVariance(List<object> data)
        {
            var m = data.OfType<Double>();
            if (m.Count() == 0) return null;
            Double? X = AggregateStDev(data).Value;
            if (!X.HasValue) return null;
            return Pow(X.Value, 2);
        }

        public static int AggregateCount(List<object> data)
        {
            if (data == null) return 0;
            else return data.Where(s => s != null && s.ToString() != "").Count();
        }

       

        public static List<object> List(params object[] Data)
        {
            return Data.ToList();
        }

        public static List<object> ListFromStrings(string data)
        {
            if (data == null) return null;
            if (data == String.Empty) return new List<object>();
            var div = new[] { "\r\n" };
            return (from d in data.Split(div, StringSplitOptions.RemoveEmptyEntries) select d.Trim()).Cast<object>().ToList();
        }

        public static List<object> ListFromQuotedStrings(string data)
        {
            if (data == null) return null;
            if (data == String.Empty) return new List<object>();
            var div = new[] { "," };
            return (from d in data.Split(div, StringSplitOptions.RemoveEmptyEntries) select d.Trim()).Select(a=>a.Substring(1, a.Length-2)).Cast<object>().ToList();
        }

        public static Boolean? Exists(List<object> Data, object Value)
        {
            if (Data == null) return null;
            return Data.Contains(Value);
        }
    }
}