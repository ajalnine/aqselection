﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathClasses
{
    public static partial class AQMath
    {
        #region Множители хим. состава

        private static InterpolatedPolynoms BoronFactor;
        private static InterpolatedPolynoms NonBoronMetric;
        private static InterpolatedPolynoms NonBoronInches;
        private static InterpolatedPolynoms BoronMetric;
        private static InterpolatedPolynoms BoronInches;
        private static Random Rand;
        
        static AQMath()
        {
            Rand = new Random(DateTime.Now.Millisecond); 
            
            BoronFactor = new InterpolatedPolynoms();
            BoronFactor.AppendPolynom(5,  x => (x > 0.85) ? 1 : (13.120634080 -101.158364721 * x + 383.762693652 * x * x - 729.903004837 * Math.Pow(x, 3) + 675.126745197 * Math.Pow(x, 4) - 242.442917071 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(7,  x => (x > 0.81) ? 1 : (10.318169981 - 70.135192037 * x + 248.922508714 * x * x - 454.751465497 * Math.Pow(x, 3) + 411.017805829 * Math.Pow(x, 4) - 146.472642898 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(9,  x => (x > 0.77) ? 1 : (10.541959508 - 80.630882020 * x + 320.361915293 * x * x - 653.009491996 * Math.Pow(x, 3) + 655.515989202 * Math.Pow(x, 4) - 257.506883618 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(11, x => (x > 0.73) ? 1 : (9.034040719 -  64.879444286 * x + 252.917064101 * x * x - 515.530148648 * Math.Pow(x, 3) + 522.328107147 * Math.Pow(x, 4) - 208.457066929 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(13, x => (x > 0.67) ? 1 : (8.094092379 -  55.905812858 * x + 219.375664776 * x * x - 466.230658125 * Math.Pow(x, 3) + 504.973407521 * Math.Pow(x, 4) - 219.452542419 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(15, x => (x > 0.63) ? 1 : (9.048417426 -  77.438198392 * x + 362.806239739 * x * x - 895.727295662 * Math.Pow(x, 3) +1101.919941657 * Math.Pow(x, 4) - 532.491292076 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(18, x => (x > 0.59) ? 1 : (6.921174330 -  48.237716991 * x + 207.289528110 * x * x - 507.174294094 * Math.Pow(x, 3) + 644.037381758 * Math.Pow(x, 4) - 328.386549825 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(22, x => (x > 0.55) ? 1 : (7.240006106 -  55.333910465 * x + 254.541657880 * x * x - 655.329737339 * Math.Pow(x, 3) + 867.431399514 * Math.Pow(x, 4) - 459.590788563 * Math.Pow(x, 5)));
            BoronFactor.AppendPolynom(26, x => (x > 0.53) ? 1 : (7.111577537 -  56.579494845 * x + 273.615083025 * x * x - 740.003765643 * Math.Pow(x, 3) +1021.468340761 * Math.Pow(x, 4) - 559.446159069 * Math.Pow(x, 5)));

            NonBoronMetric = new InterpolatedPolynoms();
            NonBoronMetric.AppendPolynom(1, x => 1);
            NonBoronMetric.AppendPolynom(3,   x => (x > 52.5)   ? 1 : (1.551781429 - 0.026051050 * x + 0.000429703 * x * x - 0.000002486 * Math.Pow(x, 3)));
            NonBoronMetric.AppendPolynom(4.5, x => (x > 77.5)   ? 1 : (2.723679262 - 0.076948390 * x + 0.001187894 * x * x - 0.000006198 * Math.Pow(x, 3)));
            NonBoronMetric.AppendPolynom(6, x => (x > 105)      ? 1 : (0.542116270 - 0.007708604 * x + 0.000162038 * x * x - 0.000000768 * Math.Pow(x, 3)  + (39.605073911 / x)));
            NonBoronMetric.AppendPolynom(7.5, x => (x > 112.5)  ? 1 : (4.062070795 - 0.102477540 * x + 0.001374024 * x * x - 0.000008606 * Math.Pow(x, 3) + (3.014508581 / x) + 0.000000021 * Math.Pow(x, 4)));
            NonBoronMetric.AppendPolynom(9, x => (x > 127.5)    ? 1 : (3.198208630 - 0.174329641 * x + 0.001669368 * x * x - 0.000009107 * Math.Pow(x, 3) + 0.000000020 * Math.Pow(x, 4) + 0.573016942 * Math.Pow(x, 0.5)));
            NonBoronMetric.AppendPolynom(10.5, x => (x > 130.5) ? 1 : (4.294792092 - 0.073539841 * x + 0.000559233 * x * x - 0.000001216 * Math.Pow(x, 3) - 0.000000002 * Math.Pow(x, 4)));
            NonBoronMetric.AppendPolynom(12, x => (x > 140)     ? 1 : (4.434721479 - 0.077028713 * x + 0.000636004 * x * x - 0.000002032 * Math.Pow(x, 3) + 0.000000001 * Math.Pow(x, 4)));
            NonBoronMetric.AppendPolynom(13.5, x => (x > 147.5) ? 1 : (5.654874371 - 0.114996681 * x + 0.001209356 * x * x - 0.000006118 * Math.Pow(x, 3) + 0.000000012 * Math.Pow(x, 4) - 11.167383113 / x));
            NonBoronMetric.AppendPolynom(15, x =>   (x > 152.5) ? 1 : (5.001837876 - 0.094745228 * x + 0.000932078 * x * x - 0.000004387 * Math.Pow(x, 3) + 0.000000008 * Math.Pow(x, 4)));
            NonBoronMetric.AppendPolynom(18, x => (x > 167.5)   ? 1 : (4.889314528 - 0.072406127 * x + 0.000480108 * x * x - 0.00000111085379857 * Math.Pow(x, 3)));
            NonBoronMetric.AppendPolynom(21, x => 4.932090545 - 0.068675314 * x + 0.000432629 * x * x - 0.0000009626699 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(24, x => 5.031553128 - 0.068490359 * x + 0.000427652 * x * x - 0.000000957 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(27, x => 5.066295588 - 0.066266469 * x + 0.000401541 * x * x - 0.000000886 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(33, x => 5.442344130 - 0.070543586 * x + 0.000416967 * x * x - 0.000000900 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(39, x => 5.569607794 - 0.068637575 * x + 0.000385663 * x * x - 0.000000798 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(45, x => 6.002223607 - 0.076461671 * x + 0.000439709 * x * x - 0.000000926 * Math.Pow(x, 3));
            NonBoronMetric.AppendPolynom(51, x => 6.374147180 - 0.082227139 * x + 0.000470574 * x * x - 0.000000975 * Math.Pow(x, 3));

            NonBoronInches = new InterpolatedPolynoms();
            NonBoronInches.AppendPolynom(1, x => 1);
            NonBoronInches.AppendPolynom(2, x => (x > 2.1) ? 1 : (4.689065518 - 11.006439436 * x + 13.830587606 * x * x - 8.801127358 * Math.Pow(x, 3) + 2.786415673 * Math.Pow(x, 4) - 0.348728383 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(3, x => (x > 3.1) ? 1 : (2.349110894 - 0.282731358 * x -  1.429746804 * x * x  + 1.166862922 * Math.Pow(x, 3) - 0.338103824 * Math.Pow(x, 4) + 0.034023320 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(4, x => (x > 4.1) ? 1 : (5.667947619 - 6.146481845 * x +  3.528740075 * x * x  - 1.060264510 * Math.Pow(x, 3) + 0.163013454 * Math.Pow(x, 4) - 0.010153794 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(5, x => (x > 4.4) ? 1 : (4.529015600 - 2.907390719 * x  + 0.986608533 * x * x  - 0.163588451 * Math.Pow(x, 3) + 0.012095078 * Math.Pow(x, 4) - 0.000257206 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(6, x => (x > 5.0) ? 1 : (4.394354034 - 2.160718068 * x  + 0.560271566 * x * x  - 0.081446705 * Math.Pow(x, 3) + 0.008400886 * Math.Pow(x, 4) - 0.000530821 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(7, x => (x > 5.3) ? 1 : (4.152467238 - 1.427505181 * x  - 0.004775717 * x * x +  0.116337754 * Math.Pow(x, 3) - 0.024407433 * Math.Pow(x, 4) + 0.001553782 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(8, x => (x > 5.6) ? 1 : (4.444726991 - 1.790852881 * x  + 0.246169598 * x * x +  0.033777818 * Math.Pow(x, 3) - 0.011887306 * Math.Pow(x, 4) + 0.000841836 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(9, x => (x > 5.8) ? 1 : (4.954211687 - 2.435211476 * x  + 0.629833126 * x * x -  0.079141805 * Math.Pow(x, 3) + 0.003991588 * Math.Pow(x, 4) - 0.000012039 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(10, x => (x > 6.1) ? 1 : (5.316104501 - 2.809767303 * x  + 0.841833892 * x * x - 0.141780556 * Math.Pow(x, 3) + 0.013013797 * Math.Pow(x, 4) - 0.000512387 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(12, x => (x > 6.6) ? 1 : (5.636486198 - 2.892644278 * x  + 0.903086193 * x * x - 0.172970815 * Math.Pow(x, 3) + 0.018810400 * Math.Pow(x, 4) - 0.000865930 * Math.Pow(x, 5)));
            NonBoronInches.AppendPolynom(14, x => 5.831757615 - 2.996461111 * x  + 0.940881024* x * x -  0.177339920 * Math.Pow(x, 3) + 0.018388504 * Math.Pow(x, 4) - 0.000790017 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(16, x => 6.069523515 - 3.151983156 * x  + 0.992968153* x * x -  0.180096340 * Math.Pow(x, 3) + 0.017202929 * Math.Pow(x, 4) - 0.000664079 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(18, x => 7.320179109 - 4.606049268 * x  + 1.684421472* x * x -  0.338443322 * Math.Pow(x, 3) + 0.034511426 * Math.Pow(x, 4) - 0.001389264 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(20, x => 7.813824147 - 5.100224293 * x  + 1.921411433* x * x -  0.394590937 * Math.Pow(x, 3) + 0.040784034 * Math.Pow(x, 4) - 0.001653271 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(24, x => 9.181377284 - 6.690482383 * x  + 2.758906061* x * x -  0.611613325 * Math.Pow(x, 3) + 0.067716481 * Math.Pow(x, 4) - 0.002930736 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(28, x => 9.279039199 - 6.214608304 * x  + 2.331584258* x * x  - 0.469722744 * Math.Pow(x, 3) + 0.047266357 * Math.Pow(x, 4) - 0.001860349 * Math.Pow(x, 5));
            NonBoronInches.AppendPolynom(32, x => 8.628566346 - 5.161247619 * x  + 1.812136833 * x * x - 0.354890331 * Math.Pow(x, 3) + 0.035687041 * Math.Pow(x, 4) - 0.001433998 * Math.Pow(x, 5));

            BoronMetric = new InterpolatedPolynoms();
            BoronMetric.AppendPolynom(1, x => 1);
            BoronMetric.AppendPolynom(3, x => (x > 62.5) ? 1 : (1.361811760 - 0.011186096 * x + 0.000111881 * x * x - 0.000000373 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(4.5, x => (x > 72.5) ? 1 :  (1.337279431   - 0.005857980 * x  - 0.000011754 * x * x + 0.000000428 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(6, x => (x > 90) ? 1 :      (124.496286515 + 6.625717467 * x  - 0.044875944 * x * x + 0.000244308 * Math.Pow(x, 3) - 0.000000595 * Math.Pow(x, 4) - 52.217835255*Math.Pow(x, 0.5)));
            BoronMetric.AppendPolynom(7.5, x => (x > 112.5) ? 1 : (69.050911915  + 2.560098768 * x  - 0.011380644 * x * x + 0.000036399 * Math.Pow(x, 3) - 0.000000042 * Math.Pow(x, 4) - 24.248260598*Math.Pow(x, 0.5)));
            BoronMetric.AppendPolynom(9, x => (x > 120) ? 1 :     (12.072069203  - 0.436747793 * x  + 0.006557060 * x * x - 0.000043976 * Math.Pow(x, 3) + 0.000000111*Math.Pow(x, 4)));
            BoronMetric.AppendPolynom(10.5, x => (x > 125) ? 1 :  (9.217458159   - 0.236230518 * x  + 0.002277705 * x * x - 0.000007319 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(12, x => (x > 137.5) ? 1 :  (9.066443005   - 0.213909782 * x  + 0.001898094 * x * x - 0.000005600 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(13.5, x => (x > 147.5) ? 1 :(8.857035651   - 0.193716034 * x  + 0.001604396 * x * x - 0.000004431 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(15, x => (x > 150) ? 1 :    (8.877558672   - 0.185134345 * x  + 0.001471107 * x * x - 0.000003920 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(18, x => (x > 167.5) ? 1 :  (8.552228740   - 0.157576401 * x  + 0.001119014 * x * x - 0.000002678 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(21, x => (x > 170) ? 1 :    (9.438197104   - 0.164616756 * x  + 0.001104645 * x * x - 0.000002523 * Math.Pow(x, 3)));
            BoronMetric.AppendPolynom(24, x => 9.971044285  - 0.167855023 * x  + 0.001085909 * x * x - 0.000002397 * Math.Pow(x, 3));
            BoronMetric.AppendPolynom(27, x => 33.047844009 + 0.425428643 * x  - 0.000597490 * x * x + 0.000000395 * Math.Pow(x, 3) - 6.823858789*Math.Pow(x, 0.5));
            BoronMetric.AppendPolynom(33, x => 8.871319820  - 0.153764506 * x  + 0.000995106 * x * x - 0.000002199 * Math.Pow(x, 3)   + 73.603999341 / x);
            BoronMetric.AppendPolynom(39, x => 12.801920223 - 0.211511577 * x  + 0.001353588 * x * x - 0.000003010 * Math.Pow(x, 3));
            BoronMetric.AppendPolynom(45, x => -20.527746153 + 0.209672440 * x - 0.000942820 * x * x + 0.000001550 * Math.Pow(x, 3) + 977.143790688 / x);
            BoronMetric.AppendPolynom(51, x => 16.008680726  - 0.275554311 * x + 0.001824563 * x * x - 0.000004192 * Math.Pow(x, 3));

            BoronInches = new InterpolatedPolynoms();
            BoronInches.AppendPolynom(1, x=>1);
            BoronInches.AppendPolynom(2, x => (x > 2.5) ? 1 : (22.953054753 - 54.543400153 * x + 54.240159664 * x * x - 26.827199318 * Math.Pow(x, 3) + 6.583692980 * Math.Pow(x, 4) - 0.640887800 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(3, x => (x > 2.9) ? 1 : (13.256439773 - 28.289558860 * x + 26.356618494 * x * x - 12.232067106 * Math.Pow(x, 3) + 2.813872527 * Math.Pow(x, 4) - 0.256241240 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(4, x => (x > 3.5) ? 1 : (28.506235326 - 46.704574851 * x + 31.904540603 * x * x - 10.912728497 * Math.Pow(x, 3) + 1.865724649 * Math.Pow(x, 4) - 0.127475207 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(5, x => (x > 4.4) ? 1 : (24.563688473 - 33.706059566 * x + 19.346243185 * x * x - 5.521324694 * Math.Pow(x, 3)  + 0.780888437 * Math.Pow(x, 4) - 0.043747269 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(6, x => (x > 4.9) ? 1 : (5.328708339  +  1.003365509 * x -  3.675725878 * x * x + 1.707522298 * Math.Pow(x, 3)  - 0.310244728 * Math.Pow(x, 4) + 0.020175573 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(7, x => (x > 5.2) ? 1 : (5.345988125  +  0.988080938 * x -  3.150658090 * x * x + 1.337267744 * Math.Pow(x, 3)  - 0.222852215 * Math.Pow(x, 4) + 0.013318161 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(8, x => (x > 5.6) ? 1 : (2.613984900  +  4.690707631 * x  - 4.715515020 * x * x + 1.580309599 * Math.Pow(x, 3)  - 0.228445343 * Math.Pow(x, 4) + 0.012192001 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(9, x => (x > 5.8) ? 1 : (3.809471033  +  2.964359379 * x  -3.588399414 * x * x  + 1.229040205 * Math.Pow(x, 3)  - 0.177303381 * Math.Pow(x, 4) + 0.009381084 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(10, x => (x > 6.1) ? 1 : (11.751365575 - 8.159021081 * x + 2.573036522 * x * x  - 0.423837024 * Math.Pow(x, 3) +  0.036790246 * Math.Pow(x, 4) - 0.001356116 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(12, x => (x > 6.6) ? 1 : (10.945797690 - 6.429044482 * x  + 1.729003899 * x * x  - 0.241866799 * Math.Pow(x, 3) + 0.017691734 * Math.Pow(x, 4)  - 0.000547833 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(14, x => (x > 6.9) ? 1 : (14.868319764 - 10.163742046 * x + 3.326997064 * x * x  - 0.594795122 * Math.Pow(x, 3) + 0.056392669 * Math.Pow(x, 4)  - 0.002210152 * Math.Pow(x, 5)));
            BoronInches.AppendPolynom(16, x => 14.093488968  -7.936990698 * x  + 1.932282134 * x * x  - 0.222072526 * Math.Pow(x, 3) + 0.010660569 * Math.Pow(x, 4)  - 0.000095257 * Math.Pow(x, 5));
            BoronInches.AppendPolynom(18, x => 11.295304140 - 4.462473855 * x  + 0.412861592 * x * x  + 0.090966723 * Math.Pow(x, 3) - 0.020345010 * Math.Pow(x, 4)  + 0.001095289 * Math.Pow(x, 5));
            BoronInches.AppendPolynom(20, x => 7.147555546  + 0.354965732 * x  - 1.613572037 * x * x  + 0.494027330 * Math.Pow(x, 3) - 0.058785370 * Math.Pow(x, 4)  + 0.002509446 * Math.Pow(x, 5));
            BoronInches.AppendPolynom(24, x => 12.447885779 - 4.735731242 * x  + 0.442101927 * x * x  + 0.081533654 * Math.Pow(x, 3) - 0.018158782 * Math.Pow(x, 4)  + 0.000938370 * Math.Pow(x, 5));
            BoronInches.AppendPolynom(28, x => 27.509862578 - 20.459403301 * x + 6.975775727 * x * x  - 1.251837879 * Math.Pow(x, 3) + 0.115426907 * Math.Pow(x, 4)  - 0.004327504 * Math.Pow(x, 5));
            BoronInches.AppendPolynom(32, x => 43.356194998 - 35.342560572 * x + 12.582362939 * x * x - 2.298206296 * Math.Pow(x, 3) + 0.211959228 * Math.Pow(x, 4) - 0.007851238 * Math.Pow(x, 5));
        }

        private static Double? HGetMultiplierForC(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x<=0.39)      return  - 0.000334 + 0.542612 * x - 0.004557 * x * x;
            else if (x<=0.55) return 0.131102 + 0.169737 * x + 0.089286 * x * x;
            else if (x<=0.65) return 0.102533 + 0.311668 * x - 0.075759 * x * x;
            else if (x<=0.75) return 0.143000 + 0.2 * x;
            else if (x<=0.9)  return 0.059626 + 0.41472 * x -  0.138171 * x * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForMn(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 1.2)       return 0.999994 + 3.33334 * x;
            else if (x <= 1.95) return -1.12648 + 5.10435 * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForSi(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 2) return 1 + 0.7 * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForNi(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 1.5)      return 1 + 0.366177 * x - 0.00404  * x * x + 0.001544 * Math.Pow(x,3);
            else if (x <= 1.8) return 1 + 0.374937 * x - 0.05011 * x * x + 0.030063 * Math.Pow(x, 3);
            else if (x <= 1.9) return 1 - 0.23822 * x + 0.505821 * x * x - 0.09089 * Math.Pow(x, 3);
            else if (x <= 2.0) return 1 + 0.826337 * x - 0.39083 * x * x + 0.086196 * Math.Pow(x, 3);
            return Double.NaN;
        }

        private static Double? HGetMultiplierForCr(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 1.75) return 1 + 2.15997 * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForMo(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 0.55) return 1 + 3 * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForCu(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 0.55) return 1 + 0.366795 * x;
            return Double.NaN;
        }

        private static Double? HGetMultiplierForV(Double? X)
        {
            if (!X.HasValue) return null;
            double x = X.Value;
            if (x <= 0.2) return 1 + 1.73206 * x;
            return Double.NaN;
        }

        #endregion

        #region Вычисление твердости

        private static Double? HGetInitialHardness(Double? C)
        {
            if (!C.HasValue) return null;
            double x = C.Value;
            if (x <= 0.69) return 35.395140702 + 6.990315384 * x + 312.329032089 * x * x - 821.740117089 * Math.Pow(x, 3) + 1015.473735800 * Math.Pow(x, 4) - 538.343421708 * Math.Pow(x, 5);
            return Double.NaN;
        }

        private static Double? HGetHardnessAtHalfMartensit(Double? C)
        {
            if (!C.HasValue) return null;
            double x = C.Value;
            if (x <= 0.69) return 22.974420345 + 6.212848945 * x + 356.369854666 * x * x - 1091.504301761 * Math.Pow(x, 3) + 1464.902145102 * Math.Pow(x, 4) - 750.451935199 * Math.Pow(x, 5);
            return Double.NaN;
        }

        private static Double? HGetJAtHalfMartensitInches(Double? DI)
        {
            if (!DI.HasValue) return null;
            double x = DI.Value;
            if (x <= 6.42) return 0.026029210 + 0.508893054 * x - 0.021971397 * x * x + 0.000599045 * Math.Pow(x, 3) - 0.000006717 * Math.Pow(x, 4);
            return Double.NaN;
        }

        private static Double? HGetJAtHalfMartensitMetric(Double? DI)
        {
            if (!DI.HasValue) return null;
            double x = DI.Value;
            if (x <= 161.8) return 0.756891553 + 8.115902485 * x - 0.220664581 * x * x + 0.003813432 * Math.Pow(x, 3) - 0.000027228 * Math.Pow(x, 4);
            return Double.NaN;
        }
        
        #endregion

        #region Вычисление прокаливаемости

        public static Double? GetHardenabilityNonBoronMetric(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V)
        {
            return GetHardenabilityNonBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, true, 7);
        }

        public static Double? GetHardenabilityNonBoronInches(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V)
        {
            return GetHardenabilityNonBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, false, 7);
        }

        public static Double? GetHardenabilityBoronMetric(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V)
        {
            return GetHardenabilityBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, true, 7);
        }

        public static Double? GetHardenabilityBoronInches(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V)
        {
            return GetHardenabilityBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, false, 7);
        }

        public static Double? GetHardenabilityNonBoronMetricWithGrain(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, Double? Grain)
        {
            return GetHardenabilityNonBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, true, Grain);
        }

        public static Double? GetHardenabilityNonBoronInchesWithGrain(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, Double? Grain)
        {
            return GetHardenabilityNonBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, false, Grain);
        }

        public static Double? GetHardenabilityBoronMetricWithGrain(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, Double? Grain)
        {
            return GetHardenabilityBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, true, Grain);
        }

        public static Double? GetHardenabilityBoronInchesWithGrain(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, Double? Grain)
        {
            return GetHardenabilityBoron(Point, C, Mn, Si, Ni, Cr, Mo, Cu, V, false, Grain);
        }


        public static Double? GetHardenabilityNonBoronMetricWithGrain(Double? Point, Double? C, Double? DI)
        {
            return GetHardenabilityNonBoronDI(Point, C, true, DI);
        }

        public static Double? GetHardenabilityNonBoronInchesWithGrain(Double? Point, Double? C, Double? DI)
        {
            return GetHardenabilityNonBoronDI(Point, C, false, DI);
        }

        public static Double? GetHardenabilityBoronMetricWithGrain(Double? Point, Double? C, Double? DI)
        {
            return GetHardenabilityBoronDI(Point, C, true, DI);
        }

        public static Double? GetHardenabilityBoronInchesWithGrain(Double? Point, Double? C, Double? DI)
        {
            return GetHardenabilityBoronDI(Point, C, false, DI);
        }


        private static double? GetHardenabilityNonBoron(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, bool IsMetric, Double? Grain)
        {
            if (!Point.HasValue) return null;
            double grain = (Grain.HasValue) ? Grain.Value : 7;
            double DI = HGetDINonBoron(C, Mn, Si, Ni, Cr, Mo, Cu, V, IsMetric, grain);
            return GetHardenabilityNonBoronDI(Point, C, IsMetric, DI);
        }

        private static double? GetHardenabilityNonBoronDI(Double? Point, Double? C, bool IsMetric, Double? DI)
        {
            if (!DI.HasValue || Double.IsNaN(DI.Value)) return null;
            
            double? ih = AQMath.HGetInitialHardness(C);

            if (!ih.HasValue) return null;

            if (Point.Value == 1)
            {
                return ih.Value;
            }
            else
            {
                if (IsMetric) return ih.Value / NonBoronMetric.GetInterpolatedValue(DI.Value, Point);
                else return ih.Value / NonBoronInches.GetInterpolatedValue(DI.Value, Point);
            }
        }

        private static double? GetHardenabilityBoron(Double? Point, Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, bool IsMetric, Double? Grain)
        {
            if (!Point.HasValue || !C.HasValue) return null;
            double grain = (Grain.HasValue) ? Grain.Value : 7;
            double DI = HGetDIBoron(C, Mn, Si, Ni, Cr, Mo, Cu, V, IsMetric, grain);
            return GetHardenabilityBoronDI(Point, C, IsMetric, DI);
        }

        private static double? GetHardenabilityBoronDI(Double? Point, Double? C, bool IsMetric, Double? DI)
        {
            if (!DI.HasValue || Double.IsNaN(DI.Value)) return null;
            double? ih = AQMath.HGetInitialHardness(C);
            if (!ih.HasValue) return null;

            if (Point.Value == 1)
            {
                return ih.Value;
            }
            else
            {
                if (IsMetric) return ih.Value / BoronMetric.GetInterpolatedValue(DI.Value, Point);
                else return ih.Value / BoronInches.GetInterpolatedValue(DI.Value, Point);
            }
        }

        public static double HGetDIBoron(Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, bool IsMetric, double grain)
        {
            double DI = HGetDI(C, Mn, Si, Ni, Cr, Mo, Cu, V, IsMetric);
            double FL = DI / AQMath.HGetMultiplierForC(C).Value;
            DI *= BoronFactor.GetInterpolatedValue(C, FL / (IsMetric ? 25.4 : 1)).Value;
            DI *= Math.Pow(1.083, 7 - grain);
            return DI;
        }

        public static double HGetDINonBoron(Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, bool IsMetric, double grain)
        {
            double DI = HGetDI(C, Mn, Si, Ni, Cr, Mo, Cu, V, IsMetric) * Math.Pow(1.083, 7 - grain);
            return DI;
        }
        
        private static double HGetDI(Double? C, Double? Mn, Double? Si, Double? Ni, Double? Cr, Double? Mo, Double? Cu, Double? V, bool IsMetric)
        {
            double? cf = AQMath.HGetMultiplierForC(C);
            double? mnf = AQMath.HGetMultiplierForMn(Mn);
            double? sif = AQMath.HGetMultiplierForSi(Si);
            double? nif = AQMath.HGetMultiplierForNi(Ni);
            double? crf = AQMath.HGetMultiplierForCr(Cr);
            double? mof = AQMath.HGetMultiplierForMo(Mo);
            double? cuf = AQMath.HGetMultiplierForCu(Cu);
            double? vf = AQMath.HGetMultiplierForV(V);

            double DI = IsMetric ? 25.4 : 1;

            if (cf.HasValue) DI *= cf.Value;
            if (mnf.HasValue) DI *= mnf.Value;
            if (sif.HasValue) DI *= sif.Value;
            if (nif.HasValue) DI *= nif.Value;
            if (crf.HasValue) DI *= crf.Value;
            if (mof.HasValue) DI *= mof.Value;
            if (cuf.HasValue) DI *= cuf.Value;
            if (vf.HasValue) DI *= vf.Value;
            return DI;
        }

        #endregion

    }
}