﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathClasses
{
    static partial class AQMath
    {
        public static List<object> AggregateCT(DataRow dr, RowExecDelegate red)
        {
            List<object> result = new List<object>();
            foreach (DataRow r in dr.Table.Rows)
            {
                result.Add(red.Invoke(r));
            }
            return result;
        }
    }

    public interface IMathExpression
    {
        object GetResult(DataRow row);
    }

    public delegate object RowExecDelegate(DataRow dr);
}
