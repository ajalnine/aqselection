﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathClasses
{
    #region Полиномы с интерполяцией
    
    
    public class InterpolatedPolynoms
    {
        public delegate double PolynomFunc(double x);
        
        private Dictionary<double, PolynomFunc> SourcePolynoms;
        
        public InterpolatedPolynoms()
        {
            SourcePolynoms = new Dictionary<double, PolynomFunc>();
        }

        public InterpolatedPolynoms(Dictionary<double, PolynomFunc> sourcepolynoms)
        {
            SourcePolynoms = sourcepolynoms;
        }

        public void AppendPolynom(Double? Key, PolynomFunc Polynom)
        {
            if (!Key.HasValue) return;
            if (SourcePolynoms == null) SourcePolynoms = new Dictionary<double, PolynomFunc>();
            SourcePolynoms.Add(Key.Value, Polynom);
        }

        public Double? GetInterpolatedValue(double? X,  double? C)
        {
            if (!X.HasValue || !C.HasValue) return null;
            if (Double.IsNaN(X.Value) || Double.IsNaN(C.Value)) return null; 
            double x = X.Value;
            double c = C.Value;
            if (SourcePolynoms.Keys.Max() < c || SourcePolynoms.Keys.Min() > c) return Double.NaN;
            if (SourcePolynoms.Keys.Contains<double>(c))
            {
                return SourcePolynoms[c].Invoke(x);    
            }
            double Key1 = (from k in SourcePolynoms.Keys orderby c - k where c - k > 0 select k).First();
            double Key2 = (from k in SourcePolynoms.Keys orderby k  where k > Key1 select k).First();
            double y1 = SourcePolynoms[Key1].Invoke(x);
            double y2 = SourcePolynoms[Key2].Invoke(x);
            return y1 + (y2 - y1) * ((c - Key1) / (Key2 - Key1)) ;
        }
    }
    #endregion
}