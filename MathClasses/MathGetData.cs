﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace MathClasses
{
    public static partial class AQMath
    {
        public static DataTable GetTable(string TableName, DataRow row)
        {
            if (row == null) return null;
            if (!row.Table.DataSet.Tables.Contains(TableName)) return null;
            return row.Table.DataSet.Tables[TableName];
        }

        public static object Cell(this DataTable table, string cellName)
        {
            if (table == null || string.IsNullOrEmpty(cellName)) return null;
            
            var x = GetCellRowNumber(cellName); // 0..count-1
            var y = GetCellColumnNumber(cellName); // 0..count-1

            if (x >= table.Columns.Count || y >= table.Rows.Count || x < 0 || y < 0) return null;
            return table.Rows[y][x];
        }

        private static int GetCellColumnNumber(string cellName)
        {
            var index = Regex.Replace(cellName.ToUpper(), @"[0-9]*", string.Empty);
            var bytes = System.Text.Encoding.ASCII.GetBytes(index);
            var column = 0;
            var n = bytes.Count();
            for (var i = 0; i <= n-1; i++)
            {
                column += (bytes[i] - 65) * (int)Math.Pow(26, n - 1 - i);
            }
            return column;
        }

        private static int GetCellRowNumber(string cellName)
        {
            var index = Regex.Replace(cellName, @"[A-z]*", string.Empty);
            int result;
            if( int.TryParse(index, out result)) return result-1;
            throw new IndexOutOfRangeException();
        }

        public static List<object> CellRange(this DataTable table, string rangeName)
        {
            if (table == null || string.IsNullOrEmpty(rangeName)) return null;
            var result = new List<object>();
            var ranges = rangeName.ToUpper().Split(new[] {";"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var r in ranges)
            {
                AppendCellRange(table, result, r);
            }
            return result;
        }

        private static void AppendCellRange(DataTable table, List<object> result, string s)
        {
            var rangeDescription = s.Split(new[] {":"}, StringSplitOptions.None);
            if (rangeDescription.Count() == 1)
            {
                var x = GetCellColumnNumber(rangeDescription[0]);
                var y = GetCellRowNumber(rangeDescription[0]);
                if (x >= table.Columns.Count || y >= table.Rows.Count || x < 0 || y < 0) return;
                result.Add(table.Rows[y][x]);
            }
            if (rangeDescription.Count() != 2) return;
            var x1 = GetCellColumnNumber(rangeDescription[0]);
            var x2 = GetCellColumnNumber(rangeDescription[1]);
            var y1 = GetCellRowNumber(rangeDescription[0]);
            var y2 = GetCellRowNumber(rangeDescription[1]);

            for (var x = x1; x <= x2; x++)
            {
                for (var y = y1; y <= y2; y++)
                {
                    if (x >= table.Columns.Count || y >= table.Rows.Count || x < 0 || y < 0) continue;
                    result.Add(table.Rows[y][x]);
                }
            }
        }

        public static List<object> GetFieldValues<T>(object o, string FieldName)
        {
            if (o is DataTable)
            {
                var Table = o as DataTable;
                if (Table == null) return null;
                if (Table.Columns.IndexOf(FieldName) == -1) return null;
                List<object> Result = new List<object>();
                foreach (DataRow r in Table.Rows)
                {
                    object v = r[FieldName];
                    if (v is decimal) v = double.Parse(v.ToString());
                    Result.Add((object)v);
                }
                return Result;
            }
            return null;
        }

        public static T GetFieldValue<T>(DataTable Table, string FieldName, int RowIndex)
        {
            if (Table == null) return default(T);
            if (Table.Columns.IndexOf(FieldName) == -1) return default(T);
            if (Table.Rows.Count > RowIndex)
            {
                var v = Table.Rows[RowIndex][FieldName];
                if (v == null) return default(T);
                if (v is decimal) v = double.Parse(v.ToString());
                return (T)v;
            }
            else return default(T);
        }

        public static T GetFieldValue<T>(string FieldName, DataRow row)
        {
            object v = row[FieldName];
            if (v != DBNull.Value)
            {
                if (v is decimal) v = double.Parse(v.ToString().Replace('.', System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToCharArray()[0]));
                return (T)v;
            }
            else return default(T);
        }

        public static T GetPreviousRowFieldValue<T>(string FieldName, DataRow row)
        {
            int index = row.Table.Rows.IndexOf(row)-1;
            if (index<0) return default(T);
            DataRow previousRow = row.Table.Rows[index];

            object v = previousRow[FieldName];
            if (v != DBNull.Value)
            {
                if (v is decimal) v = double.Parse(v.ToString());
                return (T)v;
            }
            else return default(T);
        }
    }
 }