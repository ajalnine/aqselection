﻿using System;

namespace AQReportingLibrary
{
    #region Классы высокоуровневых графиков

    public enum ReportChartRangeType
    {
        RangeIsNone,
        RangeIsNTD,
        RangeIsSlide,
        RangeIsSigmas,
        RangeIsR,
        RangeIsAB
    }

    [Flags]
    public enum ReportChartRangeStyle
    {
        None = 0,
        Invisible = 1,
        Thin = 2,
        Thick = 4,
        Filled = 8,
        Labeled = 16,
        OutagesCount = 32,
        OutagesPercent = 64,
        OutagesProbability = 128,
        CustomerRisk = 256,
        LayeredErrorBar = 512,
        ErrorBar = 1024,
        Single = 2048,
    }

    [Flags]
    public enum ReportChartSeriesType
    {
        None = 0,
        Series1 = 1,
        Series2 = 2,
        Series3 = 4,
        Series4 = 8,
        Series5 = 16,
        Series6 = 32,
        Series7 = 64,
        Series8 = 128
    }

    public enum ReportChartFrequencyYMode
    {
        FrequencyInNumber,
        FrequencyInPercent,
        FrequencyInPercentOfGroup
    }

    public enum ReportChartAlignMode
    {
        AlignByLL,
        AlignByUL,
        NoAlign,
        AlignByUser
    }

    public enum ReportChartBinMode
    {
        IncludeRight,
        IncludeLeft
    }

    [Flags]
    public enum ReportChartGroupMode
    {
        None = 0,
        Box = 1,
        Point = 2,
        Violin = 4,
        Center = 8
    }

    public enum ReportChartGroupCenter
    {
        Median,
        Mean
    }

    public enum ReportChartRegressionFitMode
    {
        DynamicSmoothLowess,
        DynamicSmoothNone,
        DynamicSmoothLine,
        DynamicSmoothQuadratic,
        DynamicSmoothCubic,
        DynamicSmoothQuartic,
        DynamicSmoothIntervals,
        DynamicSmoothE,
        DynamicSmoothLn
    }

    public enum ReportChartLayerMode
    {
        Single,
        Multiple,
        Extended,
        TableOnly
    }

    public enum ReportChartGroupAnalisysMode
    {
        Single,
        Multiple,
        Extended,
        ANOVA,
        TableTests,
        GraphTests
    }

    [Flags]
    public enum ReportChartLayoutMode
    {
        Mini = 0x01,
        Normal = 0x04,
        Wide = 0x08,
        Wider = 0x10,
        Widest = 0x20,
        Tall = 0x40,
        Full = 0x80,
        HD = 0x100,
        Shadowed = 0x200,
        ShowGrid = 0x400,
        ContrastGrid = 0x800,
        ShowX = 0x1000,
        ShowY = 0x2000,
        ShowTitle = 0x4000,
        ShowSubtitle = 0x8000,
        Transparent = 0x10000,
        UseColorScale = 0x20000,
        Inverted = 0x40000,
        Stretched = 0x80000,
        CustomSize = 0x100000,
        OrderLegend = 0x200000,
        ShowLegend = 0x400000,
        ShowTickMarks = 0x800000,
        DoubleYAxis = 0x1000000,
        FilterDescription = 0x2000000,
        LimitedLayers = 0x4000000,
        GroupByFormatted = 0x8000000
    }

    [Flags]
    public enum ReportChartFitMode
    {
        FitNone = 0,
        FitGauss = 1,
        FitKDE = 2,
        FitKDESilverman = 4,
        FitWeibull = 8,
        FitGaussEx = 16,
        FitBeeSwarm  =32,
    }

    [Flags]
    public enum ReportChartTools
    {
        ToolNone = 0,
        ToolRug = 1,
        ToolKDE = 2,
        ToolSurface = 4,
        ToolVoronoy = 8,
        ToolBag = 0x10,
        ToolRangeMean = 0x20,
        ToolRangeMedian = 0x40,
        ToolHexBin = 0x80,
        ToolBeyondAxis = 0x100,
        ToolMirrored = 0x200,
        ToolWithConstants = 0x400,
        ToolWithGridLines = 0x800,
        ToolCutSurfaces = 0x1000,
        ToolText = 0x2000,
        ToolLog = 0x4000,
    }

    [Flags]
    public enum Statistics
    {
        None = 0,
        N = 1024,
        Sum = 2048,
        Basic = 1,
        Moment = 2,
        ZTest = 4,
        Ranges = 8,
        ProcessIndexes = 16,
        MinMax = 32,
        Dispersion = 64,
        Probability= 128,
        Percents = 256,
        Median = 512,
        SixSigma = 4096,
        Interval = 8192,
        SkipZeroCasesOption = 65536
    }

    public enum ReportChartCardType
    {
        CardTypeIsX,
        CardTypeIsR,
        CardTypeIsTrends,
        CardTypeIsGraphics
    }

    public enum ReportChartHistogramType
    {
        Frequencies,
        Pareto,
        Columns,
        Bands,
        Circles
    }
        
    public enum ReportChartColumnOrder
    {
        Unordered,
        ByValuesAsc,
        ByGroupsAsc,
        ByValuesDesc,
        ByGroupsDesc
    }

    public enum ReportChartColumnDiagramType
    {
        Generic,
        Normed,
        Layers,
        ByCategory,
        Opposite
    }

    public enum ReportChartCircleDiagramType
    {
        Circles,
        Rings,
        Angles,
        Radar,
        Sun
    }

    [Flags]
    public enum ReportChartCircleLabelsType
    {
        None = 0,
        N = 1,
        Percent = 2,
        PercentInGroup = 4,
        NumberOfCases = 8,
        Layer = 0x10,
        Scaled = 0x100
    }

    public enum ReportChartRegressionViewType
    {
        PredictedObserved,
        PredictedResiduals,
        ObservedResiduals,
        ResultsInTable,
        RMatrix,
        RGraph,
    }

    public enum ReportChartRegressionStepwiseType
    {
        All,
        Forward,
        Backward,
        Group,
        Full,
        Genetic,
    }

    public enum ReportChartRegressionModelQuality
    {
        R,
        SE,
        AIC,
        BIC,
    }

    [Flags]
    public enum ReportChartRegressionOptions
    {
        None = 0,
        OnlyRed = 1,
        ExternalControl = 2,
        ExternalControlInterlaced = 4,
        OnlyFullCases = 8,
        Ridge = 16,
        FillEmpty = 32,
        NonLinear2 = 64,
        NonLinear3 = 128,
        NonLinearSqrt = 256,
        NonLinearLn = 512,
    }

    [Flags]
    public enum ReportChartColumnsLabelsType
    {
        None = 0,
        N = 1,
        Percent = 2,
        PercentInGroup = 4,
        NumberOfCases = 8,
        Layer = 0x10,
        Total = 0x20,
        TotalPercent = 0x40,
        Connected = 0x80,
        Scaled = 0x100,
        SmallDefault = 0x200,
        SmallSkip = 0x400,
        SmallOver = 0x800,
        LabelWideLimited = 0x1000
    }

    [Flags]
    public enum ReportChartDrawParts
    {
        None = 0,
        Solid = 1,
        Line = 2,
        Marker = 256,
    }

    public enum ReportChartType
    {
        Statistics,
        Dynamic,
        Card,
        Groups,
        ScatterPlot,
        Histogram,
        ProbabilityPlot,
        MultipleRegression
    }

    public enum Aggregate
    {
        Single,
        Percent,
        PercentInCategory,
        N,
        Sum,
        Min,
        Max,
        Mean,
        Median,
        StDev,
        First,
        Last
    }
        
    #endregion
}
