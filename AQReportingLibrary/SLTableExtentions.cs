﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public static class SLDataTableExtentions
    {
        public static SLDataTable Clone(this SLDataTable sourceTable)
        {
            var clone = new SLDataTable
            {
                Tag = sourceTable.Tag,
                TableName = sourceTable.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                CustomProperties = sourceTable.CustomProperties,
                SelectionString = sourceTable.SelectionString,
                IsFiltered = sourceTable.IsFiltered,
                Table = new List<SLDataRow>()
            };
            if (sourceTable.DataTypes != null) clone.DataTypes.AddRange(sourceTable.DataTypes);
            if (sourceTable.ColumnNames != null) clone.ColumnNames.AddRange(sourceTable.ColumnNames);
            if (sourceTable.ColumnTags != null) clone.ColumnTags.AddRange(sourceTable.ColumnTags);
            foreach (var row in sourceTable.Table)
            {
                var r = new SLDataRow { Row = new List<object>() };
                if (row.Row != null) r.Row.AddRange(row.Row);
                clone.Table.Add(r);
            }
            if (!sourceTable.IsFiltered) return clone;
            clone.UnfilteredTable = new List<SLDataRow>();
            foreach (var row in sourceTable.UnfilteredTable)
            {
                var r = new SLDataRow { Row = new List<object>() };
                if (row.Row != null) r.Row.AddRange(row.Row);
                clone.UnfilteredTable.Add(r);
            }
            if (sourceTable.Filters == null || !sourceTable.Filters.Any()) return clone;
            clone.Filters = new List<SLDataFilter>();
            foreach (var f in sourceTable.Filters)
            {
                var filter = new SLDataFilter() { ColumnName = f.ColumnName, FilterItems = new List<SLDataFilterItem>() };
                if (f.FilterItems != null) filter.FilterItems.AddRange(f.FilterItems);
                clone.Filters.Add(filter);
            }
            return clone;
        }
        
        public static SLDataTable PartialClone(this SLDataTable sourceTable, List<string> columns)
        {
            var clone = new SLDataTable
            {
                Tag = sourceTable.Tag,
                TableName = sourceTable.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                CustomProperties = sourceTable.CustomProperties,
                SelectionString = sourceTable.SelectionString,
                IsFiltered = sourceTable.IsFiltered,
                Table = new List<SLDataRow>()
            };
            ////....////
            /// 
            return clone;
        }

        public static void FillCell(this SLDataTable table, string columnName, object value, string dataType, int rowIndex)
        {
            var index = GetColumn(table, columnName, dataType);
            table.Table[rowIndex].Row[index] = value;
        }
        public static void FillCell(this SLDataTable table, string columnName, object value, string dataType, SLDataRow rowToFill)
        {
            var index = GetColumn(table, columnName, dataType);
            rowToFill.Row[index] = value;
        }

        public static void FillCells(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, int startRow, int endRow)
        {
            foreach (var index in columnList.Select(column => GetColumn(table, column, commonDataType)))
            {
                for (var i = startRow; i <= endRow; i++)
                {
                    table.Table[i].Row[index] = value;
                }
            }
        }

        public static void FillCells(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, List<SLDataRow> rowsToFill)
        {
            foreach (var index in columnList.Select(column => GetColumn(table, column, commonDataType)))
            {
                foreach (var r in rowsToFill)
                {
                    r.Row[index] = value;
                }
            }
        }

        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType)
        {
            var index = GetColumn(table, columnName, dataType);
            foreach (var row in table.Table)
            {
                row.Row[index] = value;
            }
        }

        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType, int startRow, int endRow)
        {
            var index = GetColumn(table, columnName, dataType);
            for (var i = startRow; i <= endRow; i++)
            {
                table.Table[i].Row[index] = value;
            }
        }

        public static void FillColumn(this SLDataTable table, string columnName, object value, string dataType, List<SLDataRow> rowsToFill)
        {
            var index = GetColumn(table, columnName, dataType);
            foreach (var r in rowsToFill)
            {
                r.Row[index] = value;
            }
        }

        public static void FillRow(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, int rowIndex)
        {
            foreach (var column in columnList)
            {
                var index = GetColumn(table, column, commonDataType);
                table.Table[rowIndex].Row[index] = value;
            }
        }

        public static void FillRow(this SLDataTable table, IEnumerable<string> columnList, object value, string commonDataType, SLDataRow rowToFill)
        {
            foreach (var column in columnList)
            {
                var index = GetColumn(table, column, commonDataType);
                rowToFill.Row[index] = value;
            }
        }

        private static int GetColumn(this SLDataTable table, string columnName, string dateType)
        {
            if (table.ColumnNames.Contains(columnName)) return table.ColumnNames.IndexOf(columnName);
            table.ColumnNames.Add(columnName);
            table.DataTypes.Add(dateType);
            if (table.ColumnTags != null) table.ColumnTags.Add(null);
            var t = table.IsFiltered ? table.UnfilteredTable : table.Table;
            foreach (var r in t)
            {
                switch (dateType)
                {
                    case "System.String":
                    case "String":
                        r.Row.Add(string.Empty);
                        break;
                    default:
                        r.Row.Add(null);
                        break;
                }
            }
            return table.ColumnNames.Count - 1;
        }

        public static SLDataTable ReorderColumns(this SLDataTable sourceTable, List<string> newOrder)
        {
            var result = new SLDataTable
            {
                Tag = sourceTable.Tag,
                TableName = sourceTable.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                CustomProperties = sourceTable.CustomProperties,
                SelectionString = sourceTable.SelectionString,
                IsFiltered = sourceTable.IsFiltered,
                Table = new List<SLDataRow>()
            };
            for (var i = 0; i < sourceTable.Table.Count; i++)
            {
                result.Table.Add(new SLDataRow {Row = new List<object>()});
            }
            foreach (var col in newOrder)
            {
                var index = sourceTable.ColumnNames.IndexOf(col);
                result.ColumnNames.Add(col);
                result.DataTypes.Add(sourceTable.DataTypes[index]);
                if (sourceTable.ColumnTags!=null && sourceTable.ColumnTags.Count == sourceTable.ColumnNames.Count) result.ColumnTags.Add(sourceTable.ColumnTags[index]);
                for (var i = 0; i< sourceTable.Table.Count; i++)
                {
                    result.Table[i].Row.Add(sourceTable.Table[i].Row[index]);
                }
            }
            return result;
        }

        public static SLDataTable GetTableWithSameName(this SLDataTable table)
        {
            return new SLDataTable
            {
                TableName = table.TableName,
                DataTypes = new List<string>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                Table = new List<SLDataRow>()
            };
        }

        public static SLDataTable ToSafeSerializable(this SLDataTable table)
        {
            foreach (var row in table.Table)
            {
                for (var i=0; i<row.Row.Count; i++)
                {
                    if (row.Row[i] == DBNull.Value) row.Row[i] = null;
                }
            }

            return table;
        }

        public static void DeleteColumn(this SLDataTable table, string columnName)
        {
            var index = table.ColumnNames.IndexOf(columnName);
            if (index < 0) return;
            table.ColumnNames.RemoveAt(index);
            table.DataTypes.RemoveAt(index);
            if (table.ColumnTags != null && table.ColumnTags.Count > index)
            {
                table.ColumnTags.RemoveAt(index);
            }
            var t = table.IsFiltered ? table.UnfilteredTable : table.Table;
            foreach (var r in t) r.Row.RemoveAt(index);
        }

        public static void RenameColumn(this SLDataTable table, string oldColumnName, string newColumnName)
        {
            table.ColumnNames[table.ColumnNames.IndexOf(oldColumnName)] = newColumnName;
        }

        public static string GetFilterDescription(this SLDataTable table)
        {
            if (!table.IsFiltered || table.Filters == null || table.Filters.Count == 0) return null;
            string result = table.Filters.Count==1 ? "Условие отбора: " : "Условия отбора: ";
            bool isFirst = true;
            foreach (var f in table.Filters.Where(a=>!a.ColumnName.StartsWith("#")))
            {
                if (f.FilterItems != null && f.FilterItems.Count == 0) continue;
                if (!isFirst) result += "; ";
                result += $"<b>{f.ColumnName}</b>: ";
                var captions = f.FilterItems.Where(a=>a.Included).Select(a => a.Caption).ToList();
                var desc = AQMath.Concatenate(captions.OfType<object>().ToList(), ", ");
                result += (desc.Length > 300) ? $"{captions.First()}..{captions.Last()}" : desc;
                isFirst = false;
            }
            return result;
        }

        public static DataItem GetDataItem(this SLDataTable table)
        {
            var res = new DataItem {DataItemType = DataType.Selection, Name = table.TableName, TableName = table.TableName, Fields = new List<DataField>()};

            for (var i = 0; i < table.ColumnNames.Count; i++)
            {
                res.Fields.Add(new DataField {Name = table.ColumnNames[i], DataType = table.DataTypes[i]});
            }
            return res;
        }

        public static SLDataTable GetSLDataTable(this DataItem dataItem)
        {
            var res = new SLDataTable {ColumnNames = dataItem.Fields.Select(a=>a.Name).ToList(), DataTypes = dataItem.Fields.Select(a=>a.DataType).ToList(), TableName = dataItem.TableName, Table = new List<SLDataRow>()};
            return res;
        }
    }
}
