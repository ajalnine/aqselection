﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public class Columns
    {
        private readonly List<GroupValues> _data;
        private readonly Data _sourceData;
        private SLDataTable _chartColumnsTable;
        private SLDataTable _sourceTable;
        private readonly ReportChartHistogramParameters _p;

        private bool _groupIsDate;
        private bool _groupHasTime, _groupIsMonth, _groupIsYear, _groupIsMonthAndYear;
        
        public Columns(ReportChartHistogramParameters p, Data source, bool ignoreLayers)
        {
            _data = source.GetLDR().Where(a => a != null && a.Group!=null).Select(a => new GroupValues { Name = a.Group, Value = a.Y, Category = ignoreLayers? null: a.Z, Color = a.Color, Label = a.Label ?? a.V?.ToString(), OriginalIndex = a.Index}).ToList();
            _p = p;
            _sourceData = source;
            _sourceTable = source.GetDataTable();
            _groupIsDate = _data.Where(a => a?.Name != null).All(a => a.Name is DateTime);
            BuildColumnsData();
        }

        #region Заполнение таблицы с bin

        private void BuildColumnsData()
        {
            CreateChartColumnsTableHeaders();
            CreateBinData();
        }

        private void CreateChartColumnsTableHeaders()
        {
            var z = _p.Fields[0] == _p.ColorField ? _p.ColorField + " " : _p.ColorField; 
            _chartColumnsTable = new SLDataTable
            {
                TableName = "Диаграмма",
                Table = new List<SLDataRow>(),
                DataTypes = (new[] { "String", "Double", "String", "String", "Double", _sourceTable.DataTypes[8], "Double", "Double", "Double", "Double", "Double", "String", "String", "Double", "Double", "Double", "String" }).ToList(),
                ColumnNames = (new[] { _p.Fields[0], "Значение", "Статистика", "Подписи", "Отступ", z, "%", "Отступ, %", "Отступ категории", "Размер", "Число категорий", "Оригинал", "Цвет", "Индекс", "Итог", "Итог, %", "Подписи итогов" }).ToList()
            };
        }

        private string GetTextLabel(double binvalue, double binpercent, double binpercentingroup, double n, string label, string layer)
        {
            string textLabel = string.Empty;
            if (Math.Abs(binvalue) > double.Epsilon)
            {
                if ((_p.LabelsType & ReportChartColumnsLabelsType.Layer) == ReportChartColumnsLabelsType.Layer && !string.IsNullOrEmpty(label))
                {
                    textLabel += label;
                }
                else if ((_p.LabelsType & ReportChartColumnsLabelsType.Layer) == ReportChartColumnsLabelsType.Layer && !string.IsNullOrEmpty(layer))
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += layer;
                }
                if ((_p.LabelsType & ReportChartColumnsLabelsType.N) == ReportChartColumnsLabelsType.N)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += AQMath.SmartRound(binvalue, 3);
                }
                if ((_p.LabelsType & ReportChartColumnsLabelsType.Percent) == ReportChartColumnsLabelsType.Percent)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += Math.Round(binpercent, 2) + " %";
                }
                if ((_p.LabelsType & ReportChartColumnsLabelsType.PercentInGroup) == ReportChartColumnsLabelsType.PercentInGroup)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += Math.Round(binpercentingroup, 2) + " %";
                }
                if ((_p.LabelsType & ReportChartColumnsLabelsType.NumberOfCases) == ReportChartColumnsLabelsType.NumberOfCases)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel +="n="+Math.Round(n, 0);
                }
            }
            return textLabel;
        }

        private string GetTotalLabel(double total, double totalInPercent)
        {
            string textLabel = string.Empty;
            if (Math.Abs(total) > double.Epsilon)
            {
                if ((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total)
                {
                    textLabel += AQMath.SmartRound(total, 3);
                }
                if ((_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += Math.Round(totalInPercent, 2) + " %";
                }
            }
            return textLabel;
        }

        private void CreateBinData()
        {
            var groups = AggregateGrouping.Calculators[_p.Aggregate](_data).ToList();
            var groupParameterName = AggregateGrouping.ParameterNames[_p.Aggregate];
            var names = groups.Select(a => a.Name).Distinct().ToList();
            foreach(var n in names)
            {
                var d = groups.Where(a => a.Name?.ToString() == n?.ToString());
                var maxY = d.Sum(a => a.Value);
                foreach (var g in d) g.Total = maxY;
            }
            List<GroupValues> orderedData;

            switch (_p.ColumnsOrder)
            {
                case ReportChartColumnOrder.ByValuesAsc:
                    orderedData = groups.OrderBy(a => a.Total).ToList();
                    break;
                case ReportChartColumnOrder.ByValuesDesc:
                    orderedData = groups.OrderByDescending(a => a.Total).ToList();
                    break;
                case ReportChartColumnOrder.ByGroupsAsc:
                    orderedData = groups.Where(a => a.Name != null && !string.IsNullOrEmpty(a.Name.ToString()) && a.Name != DBNull.Value).ToList()
                    .OrderBy(a => a.Name)
                    .Union(groups.Where(a => a.Name == null || string.IsNullOrEmpty(a.Name.ToString()) || a.Name == DBNull.Value)).ToList();
                    break;
                case ReportChartColumnOrder.ByGroupsDesc:
                    orderedData = groups.Where(a => a.Name != null && !string.IsNullOrEmpty(a.Name.ToString()) && a.Name != DBNull.Value).ToList()
                    .OrderByDescending(a => a.Name)
                    .Union(groups.Where(a => a.Name == null || string.IsNullOrEmpty(a.Name.ToString()) || a.Name == DBNull.Value)).ToList();
                    break;
                default:
                    orderedData = groups.ToList();
                    break;
            }

            var total = orderedData.Where(a => a.Value.HasValue).Sum(a => a.Value.Value);

            var x = orderedData.Select(a => a.Name).Distinct().ToList();
            var clist = orderedData.Select(a => a.Category).Distinct().ToList();
            var totalPerCategory = _p.ColorField == "Нет" 
                ? new Dictionary<object, int>{{"#", (int)total}} 
                :  _data.GroupBy(a => a.Category).ToDictionary(a => a.Key ?? "#", a => a.Count());
            var categories = clist.Where(a => a != null && !string.IsNullOrEmpty(a.ToString()) && a != DBNull.Value)
                    .OrderBy(a => a).ToList();
            var clean = clist.Where(a => a == null || string.IsNullOrEmpty(a.ToString()) || a == DBNull.Value).ToList();
            categories.AddRange(clean);
            
            var categoriesNumber = (double)categories.Count;
            double offset, offsetInPercent, percentInGroup, percent, categoryOffset, percentInCategory;
            int counter = 0;
            foreach (var name in x)
            {
                offset = 0.0d;
                offsetInPercent = 0.0d;
                var currentGroup = orderedData.Where(a => a.Name?.ToString() == name?.ToString()).ToList();
                var totalInGroup = currentGroup.Where(a => a.Value.HasValue).Sum(a => a.Value.Value);
                int categoryCounter = 0;
                foreach (var g in currentGroup
                    .Where(a=>a.Category!=null && !string.IsNullOrEmpty(a.Category.ToString())&& a.Category!=DBNull.Value).ToList()
                    .OrderBy(a=>a.Category)
                    .Union(currentGroup.Where(a => a.Category == null || string.IsNullOrEmpty(a.Category.ToString()) || a.Category == DBNull.Value).ToList()).ToList())
                {
                    var side = _p.ColumnType != ReportChartColumnDiagramType.Opposite || categories.IndexOf(g.Category) % 2 == 0 ? 1 : -1;
                    if (g.Value.HasValue || g.Value != 0)
                    {

                        percentInGroup = Math.Abs(totalInGroup) < double.Epsilon
                            ? 100.0d
                            : g.Value.Value/totalInGroup*100.0d;
                        percent = Math.Abs(total) < double.Epsilon ? 100.0d : g.Value.Value/total*100.0d;
                        var binName = g.Name;
                        categoryOffset = (double) categories.IndexOf(g.Category)/categories.Count;
                        percentInCategory = Math.Abs(totalPerCategory[g.Category ?? "#"]) < double.Epsilon 
                            ?100.0d 
                            : g.Value.Value/(double)totalPerCategory[g.Category ?? "#"] * 100.0d;

                        //_p.Fields[0]
                        //"Значение"
                        //"Статистика
                        // "Подписи"
                        //"Отступ"
                        //_p.ColorField
                        //"%"
                        //"Отступ, %"
                        //"Отступ категории"
                        //"Размер"
                        //"Число категорий"
                        //"Оригинал"
                        //"Цвет"
                        //"Индекс для Single"
                        //"Итоги"
                        //"Итоги в процентах"
                        var totalPercent = Math.Round(totalInGroup / total * 100, 2);

                        if (_p.Aggregate == Aggregate.Percent || _p.Aggregate == Aggregate.PercentInCategory)
                        {
                            var stat = String.Format("{0} {2}: {1}", g.Category, Math.Round(percent, 3),
                                groupParameterName);
                            var p = _p.Aggregate == Aggregate.Percent ? percent : percentInCategory;
                            _chartColumnsTable.Table.Add(new SLDataRow
                            {
                                Row = new List<object>
                                {
                                    binName,
                                    side*(p + offset),
                                    stat,
                                    GetTextLabel(Math.Round(p, 2), 
                                        Math.Round(p, 2),
                                        Math.Round(_p.Aggregate == Aggregate.PercentInCategory ? percentInCategory : percentInGroup, 2), 
                                        g.n, g.Label, g.Category?.ToString()),
                                    offset,
                                    g.Category,
                                    side*(percentInGroup + offsetInPercent),
                                    offsetInPercent,
                                    categoryOffset,
                                    p,
                                    categoriesNumber,
                                    g.Name,
                                    g.Color,
                                    g.OriginalIndex,
                                    categoryCounter == 0 ? (double?)totalInGroup : null,
                                    categoryCounter == 0 ? (double?)totalPercent : null,
                                    categoryCounter == 0 ? GetTotalLabel(totalInGroup, totalPercent ) :null
                                }
                            });
                            if (_p.ColumnsType != ReportChartColumnDiagramType.ByCategory &&
                                _p.ColumnsType != ReportChartColumnDiagramType.Layers &&
                                _p.ColumnsType != ReportChartColumnDiagramType.Opposite)
                            {
                                offset += p;
                                offsetInPercent += percentInGroup;
                            }
                        }
                        else
                        {
                            var stat = String.Format("{0} {2}: {1}", g.Category, Math.Round(g.Value.Value, 3),
                                groupParameterName);
                            _chartColumnsTable.Table.Add(new SLDataRow
                            {
                                Row = new List<object>
                                {
                                    binName,
                                    side*(g.Value + offset),
                                    stat,
                                    GetTextLabel(g.Value.Value, Math.Round(percent, 2), Math.Round(percentInGroup, 2),
                                        g.n, g.Label, g.Category?.ToString()),
                                    offset,
                                    g.Category,
                                    side*(percentInGroup + offsetInPercent),
                                    offsetInPercent,
                                    categoryOffset,
                                    percent,
                                    categoriesNumber,
                                    g.Name,
                                    g.Color,
                                    g.OriginalIndex,
                                    categoryCounter == 0 ? (double?)totalInGroup : null,
                                    categoryCounter == 0 ? (double?)Math.Round(totalInGroup/total*100,2) : null,
                                    categoryCounter == 0 ? GetTotalLabel(totalInGroup, totalPercent ) :null
                                }
                            });
                            if (_p.ColumnsType != ReportChartColumnDiagramType.ByCategory &&
                                _p.ColumnsType != ReportChartColumnDiagramType.Layers &&
                                _p.ColumnsType != ReportChartColumnDiagramType.Opposite)
                            {
                                offset += g.Value.Value;
                                offsetInPercent += percentInGroup;
                            }
                        }
                    }
                    counter++;
                    categoryCounter++;
                }
            }
        }

       
        #endregion

        public SLDataTable GetColumnsTable()
        {
            return _chartColumnsTable;
        }

        public SLDataTable GetPartialColumnsTable(object group)
        {
            var partialTable = new SLDataTable
            {
                ColumnNames = _chartColumnsTable.ColumnNames,
                ColumnTags = _chartColumnsTable.ColumnTags,
                CustomProperties = _chartColumnsTable.CustomProperties,
                DataTypes = _chartColumnsTable.DataTypes,
                SelectionString = _chartColumnsTable.SelectionString,
                TableName = _chartColumnsTable.TableName,
                Tag = _chartColumnsTable.Tag,
                Table = (group == null)
                ? _chartColumnsTable.Table.Where(a => a.Row[5] == null).ToList()
                : _chartColumnsTable.Table.Where(a => a.Row[5] != null && a.Row[5].ToString() == group.ToString()).ToList()
            };

            return partialTable;
        }

        public List<object> GetGroups()
        {
            List<object> groups;
            switch (_sourceTable.DataTypes[8])
            {
                case "Double":
                case "System.Double":
                    groups = (from g in _sourceData.GetLDR() where g.Z is double select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => !(a.Z is double))) groups.Add(DBNull.Value);
                    break;
                case "DateTime":
                case "System.DateTime":
                    groups =
                        (from g in _sourceData.GetLDR() where g.Z is DateTime select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => !(a.Z is DateTime))) groups.Add(DBNull.Value);
                    break;
                case "String":
                case "System.String":
                    groups =
                        (from g in _sourceData.GetLDR()
                         where g.Z != DBNull.Value && g.Z != null && (string)g.Z != string.Empty
                         select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => !(a.Z is string))) groups.Add(DBNull.Value);
                    break;
                default:
                    groups = (from g in _sourceData.GetLDR() select g.Z).Distinct().OrderBy(a => a).ToList();
                    break;
            }
            return groups;
        }
    }
};