﻿using System;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public static class CheckSeries
    {
        #region Проверка серий

        public static void SetTagsForSpecialPoints(ReportChartSeriesType st, SLDataTable sldt, double currentAverage,
                                                   double minLimit, double maxLimit, ReportChartRangeType rcrt)
        {

            var tagPosition = sldt.ColumnNames.IndexOf("Tag");
            if (st == ReportChartSeriesType.None)
            {
                foreach (var t in sldt.Table) t.Row[tagPosition] = null;
                return;
            }
            int series1Lenght = 0,
                series22Lenght = 0,
                series23Lenght = 0,
                series31Lenght = 0,
                series32Lenght = 0,
                series4Lenght = 0,
                series7Lenght = 0,
                series8Lenght = 0;
            bool series8SignChanged = false;
            double yprevious = double.NaN;
            
            if (rcrt == ReportChartRangeType.RangeIsSlide || rcrt == ReportChartRangeType.RangeIsSigmas || rcrt == ReportChartRangeType.RangeIsR)
            {
                for (int i = 0; i < sldt.Table.Count; i++)
                {
                    sldt.Table[i].Row[tagPosition] = null;
                    var y = (double) sldt.Table[i].Row[2];

                    if ((st & ReportChartSeriesType.Series1) == ReportChartSeriesType.Series1)
                    {
                        if (IsOutZoneA(y, minLimit, maxLimit)) series1Lenght++;
                        else series1Lenght = 0; //Одна точка вне зоны А. Критерий 1
                        if (series1Lenght >= 1)
                            Mark(sldt, series1Lenght - 1, i, "#ffff0000", tagPosition); //Раскраска по критерию 1
                    }

                    if ((st & ReportChartSeriesType.Series2) == ReportChartSeriesType.Series2)
                    {
                        if (IsOverCenter(y, currentAverage)) series23Lenght++;
                        else series23Lenght = 0; //Девять точек выше центра. Критерий 2
                        if (IsBelowCenter(y, currentAverage)) series22Lenght++;
                        else series22Lenght = 0; //Девять точек ниже центра. Критерий 2
                        if (series22Lenght >= 9)
                            Mark(sldt, series22Lenght - 1, i, "#ffe17802", tagPosition); //Раскраска по критерию 2 
                        else if (series23Lenght >= 9)
                            Mark(sldt, series23Lenght - 1, i, "#ffe17802", tagPosition); //Раскраска по критерию 2
                    }

                    if ((st & ReportChartSeriesType.Series3) == ReportChartSeriesType.Series3)
                    {
                        if (i > 0)
                        {
                            if (y > yprevious) series31Lenght++;
                            else series31Lenght = 0;

                            if (y < yprevious) series32Lenght++;
                            else series32Lenght = 0;
                        } // 6 возрастающих или убывающих точек подряд. Критерий 3
                        if (series31Lenght >= 5)
                            Mark(sldt, series31Lenght, i, "#ff5d9c00", tagPosition); //Раскраска по критерию 3
                        else if (series32Lenght >= 5)
                            Mark(sldt, series32Lenght, i, "#ff5d9c00", tagPosition); //Раскраска по критерию 3
                    }

                    if ((st & ReportChartSeriesType.Series4) == ReportChartSeriesType.Series4)
                    {
                        if (i > 0 && Math.Sign(yprevious - currentAverage) != Math.Sign(y - currentAverage) &&
                            Math.Abs(y - yprevious) > double.Epsilon) series4Lenght++;
                        else series4Lenght = 0; //14 точек в шахматном порядке от центра. Критерий 4
                        if (series4Lenght >= 14)
                            Mark(sldt, series4Lenght, i - 1, "#ff00b96b", tagPosition); //Раскраска по критерию 4
                    }

                    double y1 = 0, y2 = 0, y3 = 0;

                    if (i > 1 && (st & ReportChartSeriesType.Series5) == ReportChartSeriesType.Series5)
                    {
                        y1 = (double) sldt.Table[i].Row[2];
                        y2 = (double) sldt.Table[i - 1].Row[2];
                        y3 = (double) sldt.Table[i - 2].Row[2];
                        var y1Out = IsInZoneA(y1, minLimit, maxLimit, currentAverage) ||
                                    IsOutZoneA(y1, minLimit, maxLimit)
                                        ? 1
                                        : 0;
                        var y2Out = IsInZoneA(y2, minLimit, maxLimit, currentAverage) ||
                                    IsOutZoneA(y2, minLimit, maxLimit)
                                        ? 1
                                        : 0;
                        var y3Out = IsInZoneA(y3, minLimit, maxLimit, currentAverage) ||
                                    IsOutZoneA(y3, minLimit, maxLimit)
                                        ? 1
                                        : 0;
                        if (y1Out + y2Out + y3Out >= 2)
                            Mark(sldt, 2, i, "#ff28d0d1", tagPosition);
                                //2 точки из 3 в зоне А или за ней. Критерий 5. Раскраска по критерию 5
                    }

                    if (i > 3 && (st & ReportChartSeriesType.Series6) == ReportChartSeriesType.Series6)
                    {
                        var y4 = (double) sldt.Table[i - 3].Row[2];
                        var y5 = (double) sldt.Table[i - 4].Row[2];
                        var y1Out = !IsInZoneC(y1, minLimit, maxLimit, currentAverage) ? 1 : 0;
                        var y2Out = !IsInZoneC(y2, minLimit, maxLimit, currentAverage) ? 1 : 0;
                        var y3Out = !IsInZoneC(y3, minLimit, maxLimit, currentAverage) ? 1 : 0;
                        var y4Out = !IsInZoneC(y4, minLimit, maxLimit, currentAverage) ? 1 : 0;
                        var y5Out = !IsInZoneC(y5, minLimit, maxLimit, currentAverage) ? 1 : 0;
                        if (y1Out + y2Out + y3Out + y4Out + y5Out >= 4)
                            Mark(sldt, 4, i, "#ff01c2ff", tagPosition);
                                //4 точки из 5 в зоне B или за ней. Критерий 6. Раскраска по критерию 6
                    }


                    if ((st & ReportChartSeriesType.Series7) == ReportChartSeriesType.Series7)
                    {
                        if (IsInZoneC(y, minLimit, maxLimit, currentAverage)) series7Lenght++;
                        else series7Lenght = 0; //15 точек в зоне C. Критерий 7
                        if (series7Lenght >= 15)
                            Mark(sldt, series7Lenght, i, "#ff4032b8", tagPosition); //Раскраска по критерию 7
                    }

                    if ((st & ReportChartSeriesType.Series8) == ReportChartSeriesType.Series8)
                    {
                        if (i > 0)
                        {
                            if (!IsInZoneC(y, minLimit, maxLimit, currentAverage))
                            {
                                series8Lenght++;
                                if (Math.Sign(yprevious - currentAverage) != Math.Sign(y - currentAverage) &&
                                    Math.Abs(y - yprevious) > double.Epsilon) series8SignChanged = true;
                            }
                            else
                            {
                                series8Lenght = 0;
                                series8SignChanged = false;
                            }
                        } //8 не в зоне С но по разные стороны центра. Критерий 8
                        if (series8Lenght >= 8 && series8SignChanged)
                            Mark(sldt, series8Lenght, i, "#ffa90ab9", tagPosition); //Раскраска по критерию 8
                    }
                    yprevious = y;
                }
            }
            else foreach (var t in sldt.Table)t.Row[tagPosition] = null;
        }

        private static void Mark(SLDataTable sldt, int lenght, int position, string color, int tagPosition)
        {
            for (int i = position - lenght; i <= position; i++) sldt.Table[i].Row[tagPosition] = color;
        }

        #region Базовые проверки

        private static bool IsOutZoneA(double y, double minLimit, double maxLimit)
        {
            return (y > maxLimit || y < minLimit);
        }

        private static bool IsInZoneA(double y, double minLimit, double maxLimit, double currentAverage)
        {
            return ((y <= maxLimit && y >= currentAverage + (maxLimit - currentAverage)*2/3) ||
                    (y >= minLimit && y <= currentAverage - (currentAverage - minLimit)*2/3));
        }

/*
        private static bool IsInZoneB(double y, double minLimit, double maxLimit, double currentAverage)
        {
            return (y >= currentAverage + (maxLimit - currentAverage)*1/3 &&
                    y <= currentAverage + (maxLimit - currentAverage)*2/3 ||
                    y <= currentAverage - (currentAverage - minLimit)*1/3 &&
                    y >= currentAverage - (currentAverage - minLimit)*2/3);
        }
*/

        private static bool IsInZoneC(double y, double minLimit, double maxLimit, double currentAverage)
        {
            return ((y >= currentAverage && y <= currentAverage + (maxLimit - currentAverage)*1/3) ||
                    (y <= currentAverage && y >= currentAverage - (currentAverage - minLimit)*1/3));
        }

        private static bool IsOverCenter(double y, double currentAverage)
        {
            return y > currentAverage;
        }

        private static bool IsBelowCenter(double y, double currentAverage)
        {
            return y < currentAverage;
        }

        public static void ResetSeries(SLDataTable sldt)
        {
            var tagPosition = sldt.ColumnNames.IndexOf("Tag");
            foreach (var r in sldt.Table) r.Row[tagPosition] = null;
        }

        #endregion

        #endregion
    }
}