﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression : IReportElement
    {
        

        private string _regressionComment = string.Empty;
        private SLDataTable _selectedTable, _resultedTable;
        private List<MathFunctionDescription> _fit;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType;

        private Step _step;
        private  ReportChartMultipleRegressionParameters _p;

        public ReportChartMultipleRegression(ReportChartMultipleRegressionParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartMultipleRegression(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartMultipleRegressionParameters>(s);
            _step = s;
        }
        public void SetNewParameters(ReportChartMultipleRegressionParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            switch(_p.ViewType)
            {
                case ReportChartRegressionViewType.ResultsInTable:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет") return $@"<b>{_p.Table}</b>: результат регрессии <b>{_p.FieldX}</b>";
                    else return $@"<b>{_p.Table}</b>: результаты регрессии <b>{_p.FieldX}</b> со стратификацией ";
                case ReportChartRegressionViewType.RMatrix:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет") return $@"<b>{_p.Table}</b>: матрица парных корреляций";
                    else return $@"<b>{_p.Table}</b>: матрицы парных корреляций";
                default:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет")  return $@"<b>{_p.Table}</b>: регрессионная модель <b>{_p.FieldX}</b>";
                    else return $@"<b>{_p.Table}</b>: регрессионная модель <b>{_p.FieldX}</b> со стратификацией";
            }
            
        }

        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            switch (_p.ViewType)
            {
                case ReportChartRegressionViewType.ResultsInTable:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет") return $@"{_p.Table}: результат регрессии {_p.FieldX}";
                    else return $@"{_p.Table}: результаты регрессии {_p.FieldX} со стратификацией ";
                case ReportChartRegressionViewType.RMatrix:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет") return $@"{_p.Table}: матрица парных корреляций";
                    else return $@"{_p.Table}: матрицы парных корреляций";
                default:
                    if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет") return $@"{_p.Table}: регрессионная модель {_p.FieldX}";
                    else return $@"{_p.Table}: регрессионная модель {_p.FieldX} со стратификацией";
            }
        }
        public string GetChartName()
        {
            return "Регрессионная модель";
        }
    }
}