﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        private Dictionary<object, SLDataTable> _interactiveTables, _regressionTables, _resultedTables, _correlationTables, _fullRegressionTables;
        private Dictionary<object, Data> _datas;
        private List<SLDataTable> _chartRangesTables, _chartRangesTablesWithErrors;
        private List<object> _layers = new List<object>();
        private List<SLDataTable> _dataTables;
        private List<ErrorBarInfo> _ebi = new List<ErrorBarInfo>();

        private bool SetupChartDataMultipleLayers(IEnumerable<SLDataTable> dataTables)
        {

            _dataTables = dataTables.ToList();
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _p.FieldX,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            _layers = GetGroups();
            _interactiveTables = new Dictionary<object, SLDataTable>();
            _resultedTables = new Dictionary<object, SLDataTable>();
            _regressionTables = new Dictionary<object, SLDataTable>();
            _fullRegressionTables = new Dictionary<object, SLDataTable>();
            _correlationTables = new Dictionary<object, SLDataTable>();
            _graphTables = new List<SLDataTable>();
            _datas = new Dictionary<object, Data>();
            var modelExists = true;
            _worker.RunWorkerCompleted += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(() =>
                {
                    if (_datas == null || _datas.Count==0 || !modelExists) return;

                    switch (_p.ViewType)
                    {
                        case ReportChartRegressionViewType.ResultsInTable:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _fullRegressionTables.Values.ToList() });
                            ShowRegressionTables();
                            break;
                        case ReportChartRegressionViewType.RMatrix:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _correlationTables.Values.ToList() });
                            ShowCorrelationTables();
                            break;
                        case ReportChartRegressionViewType.RGraph:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables.ToList() });
                            ShowCircularGraph();
                            break;
                        default:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable> { _resultedTable } });
                            SetupFitMultipleLayers();
                            CreateChartsMultipleLayers();
                            break;
                    }
                });
            };

            _worker.DoWork += (o, e) =>
            {
                modelExists = CreateRegressionDataForMultipleLayers();
            };
            _worker.RunWorkerAsync();
            return true;
        }

        private bool CreateRegressionDataForMultipleLayers()
        {
            _data = null;
            _workerThread = Thread.CurrentThread;
            var globalr = new Regression(_selectedTable, _p.FieldX, _p.Fields, null, null,
                ReportChartRegressionStepwiseType.All, _p.ModelQuality, _p.RegressionOptions, _worker);
            _surface?.Dispatcher.BeginInvoke(() => { MarkFieldsSingleLayer(globalr); });
            bool modelFound = false;
            _ebi = new List<ErrorBarInfo>();
            foreach (var z in _layers)
            {
                var regression =
                    (new Regression(_selectedTable, _p.FieldX, _p.Fields, _p.ColorField, z, _p.StepwiseType,
                        _p.ModelQuality, _p.RegressionOptions, _worker));
                if (regression.RegressionInput == null) continue;
                var r = regression.RegressionResult;
                if (r.BMatrix == null || double.IsNaN(r.MultipleR)) continue;
                var model = new RegressionLine(regression);
                modelFound = true;
                
                var rt = model.GetCalculatedTable(_p.FieldX + " расчет", _p.FieldX + " остатки", _p.ColorField, z);
                rt.TableName += z?.ToString();
                _resultedTables.Add(z, rt);
                var fi = new FieldsInfo
                {
                    ColumnZ = _p.ColorField,
                    ColumnW = _p.MarkerSizeField,
                    ColumnV = _p.LabelField
                };
                switch (_p.ViewType)
                {
                    case ReportChartRegressionViewType.PredictedObserved:
                    case ReportChartRegressionViewType.RMatrix:
                    case ReportChartRegressionViewType.RGraph:
                    case ReportChartRegressionViewType.ResultsInTable:
                        fi.ColumnY = _p.FieldX;
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;

                    case ReportChartRegressionViewType.ObservedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX;
                        break;

                    case ReportChartRegressionViewType.PredictedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;
                }
                var d = new Data(rt, fi, _p.Fields);

                _datas.Add(z, d);
                _ebi.Add(new ErrorBarInfo {SE = regression.RegressionResult.StdError,
                    n = regression.RegressionInput.n, Layer = z, Ldr = d.GetLDR(),
                    MeanOfResult = regression.RegressionResult.MeanOfResult,
                    StDevOfResult = regression.RegressionResult.StdevOfResult,
                    StDevOdResiduals = regression.RegressionResult.StDevOfResiduals,
                    Mean = regression.RegressionResult.Mean,
                    StDev = regression.RegressionResult.StDev});

                var table = d.GetDataTable();
                if (table == null) continue;
                foreach (var t in table.Table)
                {
                    var rowIndex = (int)t.Row[4];
                    var sourceRowIndex = (int)rt.RowTags[rowIndex];
                    t.Row[4] = sourceRowIndex;
                }
                table.TableName = "Данные" + z;

                _interactiveTables.Add(z, table);
                if (_layers.Count <= 8)
                {
                    if (!string.IsNullOrEmpty(_regressionComment)) _regressionComment += "\r\n";
                    var regressionInfo = model.GetRegressionComment(true, true);
                    _regressionComment += $"<b>{z}</b>: {regressionInfo}";

                    var t = GetShortRegressionTable(regression);
                    var ft = GetFullRegressionTable(regression);
                    if (t != null)
                    {
                        t.TableName = z?.ToString();
                        t.Header = regressionInfo;
                        _regressionTables.Add(z, t);

                        ft.TableName = z?.ToString();
                        ft.Header = regressionInfo;
                        _fullRegressionTables.Add(z, ft);
                    }
                    var ct = GetCorrelationTable(regression);
                    if (ct != null)
                    {
                        ct.TableName = z?.ToString();
                        ct.Header = regressionInfo;
                        _correlationTables.Add(z, ct);
                    }
                }
                _interactiveTable = _interactiveTables.Values.FirstOrDefault();

                CreateCorrelationGraphData();
                SetupChartRanges();

                for (var i = 1; i < _interactiveTables.Count; i++)
                {
                    _interactiveTable?.Table.AddRange(_interactiveTables.Values.ToList()[i].Table);
                }

                if (_interactiveTable != null) _interactiveTable.TableName = "Данные";
            }
            if (!modelFound) _surface?.Dispatcher.BeginInvoke(() => { SetMessage("Не удалось построить ни одной модели"); });
            return modelFound;
        }

        public void SetupChartRanges()
        {
            if (_p.RangeType == ReportChartRangeType.RangeIsNone ||
                (_p.RangeType == ReportChartRangeType.RangeIsAB && _p.UserLimits?.Table.Count == 0)) return;
            var fi = new FieldsInfo
            {
                ColumnX = _p.FieldX,
                ColumnY = _p.FieldX
            };

            if (_p.ColorField=="Нет" || _p.LayersMode == ReportChartLayerMode.TableOnly || (_p.RangeStyle & ReportChartRangeStyle.Single) == ReportChartRangeStyle.Single)
            {
                var limits = new Limits(_data.GetLDR(),
                    _p.RangeType,
                    ReportChartCardType.CardTypeIsX,
                    ReportChartType.ScatterPlot,
                    _p.UserLimits,
                    Limits.GetExternalLimitsTable(_dataTables, _selectedTable),
                    _p.FieldX, false, _p.RangeStyle);

                _chartRangesTables = new List<SLDataTable>{ limits.GetRangesYTable().Clone()};
            
                _chartRangesTablesWithErrors = new List<SLDataTable> { _chartRangesTables[0].Clone()};
                _chartRangesTablesWithErrors[0].TableName += "±Q·SE";
                RangesHelper.AppendErrorBar(_chartRangesTablesWithErrors[0], _ebi[0], _p.RangeStyle);
            }
            else
            {
                _chartRangesTables = new List<SLDataTable>();
                _chartRangesTablesWithErrors = new List<SLDataTable>();
                var isFirst = true;
                var singleRanges = ! (_p.RangeType == ReportChartRangeType.RangeIsAB ||
                                     _p.RangeType == ReportChartRangeType.RangeIsNTD );
                    
                foreach (var e in _ebi)
                {
                    var limits = new Limits(_datas[e.Layer].GetLDR(),
                        _p.RangeType,
                        ReportChartCardType.CardTypeIsX,
                        ReportChartType.ScatterPlot,
                        _p.UserLimits,
                        Limits.GetExternalLimitsTable(_dataTables, _selectedTable),
                        _p.FieldX, false, _p.RangeStyle);
                    if (!singleRanges || isFirst)
                    {
                        var t = limits.GetRangesYTable().Clone();
                        t.TableName += "; " + e.Layer?.ToString();
                        _chartRangesTables.Add(t);
                        var seTable = limits.GetRangesYTable().Clone();
                        _chartRangesTablesWithErrors.Add(seTable);
                        seTable.TableName += "; " + e.Layer?.ToString() + ": ±Q·SE";
                    } 
                    RangesHelper.AppendErrorBar(_chartRangesTablesWithErrors.Last(), e, _p.RangeStyle);
                    
                    isFirst = false;
                }
            }
        }

        private void CreateCorrelationGraphData()
        {
            if (_correlationTables == null) return;
            var dt = _p.ColorField == "Нет" ? "String" : _selectedTable.DataTypes[_selectedTable.ColumnNames.IndexOf(_p.ColorField)];
            _graphTables = new List<SLDataTable>();
            var graphTable = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string> { "Фактор1", "Фактор2", "Корреляция", _p.ColorField },
                ColumnTags = new List<object>(),
                DataTypes = new List<string>() { "String", "String", "Double", dt },
                TableName = "Граф"
            };
            _graphTables.Add(graphTable);
            foreach (var t in _correlationTables)
            {
                var layer = (_p.ColorField!="Нет" && _p.LayersMode != ReportChartLayerMode.Single) ?  t.Key : null;

                foreach (var xname in t.Value.ColumnNames.Skip(1).Where(a => !a.StartsWith("#")))
                {
                    var x = t.Value.ColumnNames.IndexOf(xname);
                    foreach (var row in t.Value.Table)
                    {
                        var yname = row.Row[0].ToString();
                        if (graphTable.Table.Any(a => a.Row[0] != a.Row[1] && a.Row[0].ToString() == yname && a.Row[1].ToString() == xname)) continue;
                        var cor = row.Row[x];
                        graphTable.Table.Add(new SLDataRow { Row = new List<object> { xname, yname, (double)cor, layer } });
                    }
                }
            }
        }

        private void SetupFitMultipleLayers()
        {
            _fit = new List<MathFunctionDescription>();

            foreach (var z in _layers)
            {
                if (_datas.ContainsKey(z))
                {
                    var x = string.Empty;
                    switch (_p.ViewType)
                    {
                        case ReportChartRegressionViewType.PredictedObserved:
                            x = _p.FieldX + " расчет";
                            break;

                        case ReportChartRegressionViewType.ObservedResiduals:
                            x = _p.FieldX;
                            break;

                        case ReportChartRegressionViewType.PredictedResiduals:
                            x = _p.FieldX + " расчет";
                            break;
                    }

                    var d = _datas[z];
                    var lri2 = LineSmoothHelper.GetLinearRegression(d.GetLDR());
                    _fit.Add(lri2.SetupLine(x, null, GetLineThickness(_p.SmoothStyle), z, x));
                    _fit.Add(lri2.SetupPredictionLine(x, null, GetLineThickness(_p.SmoothStyle) / 2, 1, true, z, x));
                    _fit.Add(lri2.SetupPredictionLine(x, null, GetLineThickness(_p.SmoothStyle) / 2, -1, true, z, x));
                    _fit.Add(lri2.SetupPredictionLine(x, null, GetLineThickness(_p.SmoothStyle) / 2, 1, false, z, x));
                    _fit.Add(lri2.SetupPredictionLine(x, null, GetLineThickness(_p.SmoothStyle) / 2, -1, false, z, x));
                }
            }
            foreach (var f in _fit)
            {
                f.Description = "#" + f.Description;
            }

        }

        public List<object> GetGroups()
        {
            List<object> groups;
            var l = _selectedTable.ColumnNames.IndexOf(_p.ColorField);
            if (l < 0) return null;
            switch (_selectedTable.DataTypes[l])
            {
                case "Double":
                case "System.Double":
                    groups = (from g in _selectedTable.Table where g.Row[l] is double select g.Row[l]).Distinct().OrderBy(a => a).ToList();
                    break;
                case "DateTime":
                case "System.DateTime":
                    groups =
                        (from g in _selectedTable.Table where g.Row[l] is DateTime select g.Row[l]).Distinct().OrderBy(a => a).ToList();
                    break;
                case "String":
                case "System.String":
                    groups =
                        (from g in _selectedTable.Table
                         where g.Row[l] != DBNull.Value && g.Row[l] != null && (string)g.Row[l] != string.Empty
                         select g.Row[l]).Distinct().OrderBy(a => a).ToList();
                    break;
                default:
                    groups = (from g in _selectedTable.Table select g.Row[l]).Distinct().OrderBy(a => a).ToList();
                    break;
            }
            return groups;
        }
        
        private void CreateChartsMultipleLayers()
        {
            _surface.Children.Clear();
            foreach (ChartDescription cd in GetDescriptionForMultipleRegressionMultipleLayers())
            {
                var chartMultipleRegression = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowXHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    EnableCellCondense = true,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable> { _interactiveTable},
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } }, Editable = true },
                    CustomLegendTables = new List<SLDataTable>()
                };
                chartMultipleRegression.DataTables.AddRange(_chartRangesTables);
                chartMultipleRegression.DataTables.AddRange(_chartRangesTablesWithErrors);
                chartMultipleRegression.CustomLegendTables.AddRange(_regressionTables?.Values);
                chartMultipleRegression.ToolTipRequired += ChartDynamics_ToolTipRequired;
                chartMultipleRegression.SelectionChanged += ChartMultipleRegression_SelectionChanged;
                chartMultipleRegression.AxisClicked += chartScatterPlot_AxisClicked;
                chartMultipleRegression.InteractiveRename += chartScatterPlot_InteractiveRename;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartMultipleRegression });
                _surface.Children.Add(chartMultipleRegression);
                chartMultipleRegression.VisualReady += ChartMultipleRegression_VisualReady;
                chartMultipleRegression.MakeChart();
            }
        }

        private void ChartMultipleRegression_VisualReady(object o, AQBasicControlsLibrary.VisualReadyEventArgs e)
        {
            switch (_p.ViewType)
            {
                case ReportChartRegressionViewType.ResultsInTable:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _fullRegressionTables.Values.ToList() });
                    break;
                case ReportChartRegressionViewType.RMatrix:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _correlationTables.Values.ToList() });
                    break;
                case ReportChartRegressionViewType.RGraph:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _graphTables.ToList() });
                    break;
                default:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = new List<SLDataTable> { _resultedTable } });
                    break;
            }
        }
    }

    public class ErrorBarInfo
    {
        public double Mean;
        public double StDev;
        public double SE;
        public double StDevOdResiduals;
        public double MeanOfResult;
        public double StDevOfResult;
        public int n;
        public object Layer;
        public List<DynamicRow> Ldr;
    }
}