﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        private SLDataTable GetShortRegressionTable(Regression regression)
        {
            if (regression.RegressionInput.m == 0) return null;
            var r = regression.RegressionResult;
            if (r.BMatrix == null) return null;
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = "Регрессия",
                ColumnNames = new List<string> {"Фактор", "β", "p", "#β_Цвет", "#p_Цвет"},
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string> {"String","Double","Double", "String", "String"}
            };
            var table = new List<SLDataRow>();
            for (var i = 0; i < regression.RegressionInput.m; i++)
            {
                var color = r.p[i] < 0.05 ? "#ffff0000" : null;
                var row = new SLDataRow {Row = new List<object>
                {
                    regression.IndependentVariables[i],
                    AQMath.SmartRound(r.BetaMatrix[i],2),
                    AQMath.SmartRound(r.p[i],2),
                    color,
                    color
                } };
                table.Add(row);
            }
            result.Table.AddRange(table.OrderByDescending(a=>Math.Abs((double?) a.Row[1] ?? 0)));
            return result;
        }

        private SLDataTable GetCorrelationTable(Regression regression)
        {
            if (regression.RegressionInput.m == 0) return null;
            var r = regression.RegressionResult;
            if (r.BMatrix == null) return null;
            var fields = new List<string> {regression.DependentVariable };
            var colorfields = new List<string> {"#" + regression.DependentVariable + "_Цвет"};
            foreach (var ri in regression.IndependentVariables)
            {
                fields.Add(ri);
                colorfields.Add("#" + ri + "_Цвет");
            }

            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = "Регрессия",
                ColumnNames = new List<string> { "Фактор" },
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty> { new SLExtendedProperty { Key = "FirstColumnIsCaseName", Value = true } },
                DataTypes = new List<string>() { "String" }
            };

            for (int i = 0; i < regression.RegressionInput.m + 1; i++)
            {
                result.ColumnNames.Add(fields[i]);
                result.DataTypes.Add("Double");
                result.ColumnNames.Add(colorfields[i]);
                result.DataTypes.Add("String");
            }
            
            var table = new List<SLDataRow>();
            for (var i = 0; i < regression.RegressionInput.m + 1; i++)
            {
                var row = new SLDataRow { Row = new List<object>() };
                row.Row.Add(fields[i]);
                for (var j = 0; j < regression.RegressionInput.m + 1; j++)
                {
                    var v = Math.Round(regression.RegressionInput.PairCorellation[i, j], 2);
                    row.Row.Add(v);
                    var alpha = (byte)(Math.Abs(regression.RegressionInput.PairCorellation[i, j]) * 127);

                    var color =  Color.FromArgb((byte)255, (byte)255, (byte)(255 - alpha), (byte)(255 - alpha)).ToString();
                    row.Row.Add(color);
                }
                table.Add(row);
            }
            result.Table = table;
            return result;
        }

        private SLDataTable GetFullRegressionTable(Regression regression)
        {
            if (regression.RegressionInput.m == 0) return null;
            var r = regression.RegressionResult;
            var tn= r.d;
            if (r.BMatrix == null) return null;
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = "Регрессия",
                ColumnNames = new List<string> { "Фактор", "β", "β _se", "b", "b _se", "t(" + tn + ")", "p", "#β_Цвет", "#β SE_Цвет", "#b_Цвет", "#b SE_Цвет", "#t(" + tn + ")_Цвет", "#p_Цвет" },
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string> { "String", "Double", "Double", "Double", "Double", "Double", "Double", "String", "String", "String", "String", "String", "String" }
            };
            var table = new List<SLDataRow>();

            var bcolor = r.pIntercept < 0.05 ? "#ffff0000" : null;
            var brow = new SLDataRow
            {
                Row = new List<object>
            {
                string.Empty,
                null,
                null,
                AQMath.SmartRound(r.Intercept,5),
                AQMath.SmartRound(r.BIntercept,3),
                AQMath.SmartRound(r.tIntercept,2),
                AQMath.SmartRound(r.pIntercept,2),
                null,
                null,
                bcolor,
                bcolor,
                bcolor,
                bcolor
            }
            };
            
            for (var i = 0; i < regression.RegressionInput.m; i++)
            {
                var color = r.p[i] < 0.05 ? "#ffff0000" : null;
                var row = new SLDataRow
                {
                    Row = new List<object>
                {
                    regression.IndependentVariables[i],
                    AQMath.SmartRound(r.BetaMatrix[i],2),
                    AQMath.SmartRound(r.BetaSE[i],3),
                    AQMath.SmartRound(r.BMatrix[i],5),
                    AQMath.SmartRound(r.BSE[i],3),
                    AQMath.SmartRound(r.t[i],2),
                    AQMath.SmartRound(r.p[i],2),
                    color,
                    color,
                    color,
                    color,
                    color,
                    color
                }
                };
                table.Add(row);
            }
            result.Table.Add(brow);
            result.Table.AddRange(table.OrderByDescending(a => Math.Abs((double?)a.Row[1] ?? 0)));
            return result;
        }
        
        private void ShowRegressionTables()
        {
            _surface.Children.Clear();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush =  ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)), 
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix = _p.ColorField!="Нет" || _p.LayersMode == ReportChartLayerMode.Multiple ? _p.ColorField + ": " : string.Empty,
                FirstColumnIsCaseName = true,
                Title = GetChartTitle(),
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
            };
            rt.VisualReady += RtOnVisualReady;
            rt.DrawReportTables(_fullRegressionTables?.Values.ToList(), _surface);
            rt.InteractiveRename += chartScatterPlot_InteractiveRename;
        }

        private void RtOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs());
        }

        private void ShowCorrelationTables()
        {
            _surface.Children.Clear();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)),
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix = _p.ColorField != "Нет" || _p.LayersMode == ReportChartLayerMode.Multiple ? _p.ColorField + ": " : string.Empty,
                FirstColumnIsCaseName = true,
                ColorIsBackground = true,
                Title = GetChartTitle(),
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
            };
            rt.VisualReady += RtOnVisualReady;
            rt.DrawReportTables(_correlationTables.Values.ToList(), _surface);
            rt.InteractiveRename += chartScatterPlot_InteractiveRename;
        }

        public Brush ConvertStringToStroke(string c)
        {
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            Brush stroke = new SolidColorBrush(Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff)));
            return stroke;
        }
    }
}