﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        private IEnumerable<ChartDescription> GetDescriptionForMultipleRegressionSingleLayer()
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            if ((_p.ColorField != null && _p.ColorField != "Нет")
                || (_p.LabelField != null && _p.LabelField != "Нет")
                || (_p.MarkerSizeField != null && _p.MarkerSizeField != "Нет"))
                cd.ChartLegendType = ChartLegend.Right;

            cd.ChartTitle = GetChartTitle();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            cd.ChartSubtitle += _regressionComment;
            cd.ChartLegendType = _p.ChartLegend;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            var color = ColorConvertor.GetDefaultColorForScale(_p.ColorScale);
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;

            string ColumnX = _p.FieldX, ColumnY = _p.FieldX;
            switch (_p.ViewType)
            {
                case ReportChartRegressionViewType.PredictedObserved:
                    ColumnX = _p.FieldX + " расчет";
                    break;

                case ReportChartRegressionViewType.ObservedResiduals:
                    ColumnY = _p.FieldX + " остатки";
                    break;

                case ReportChartRegressionViewType.PredictedResiduals:
                    ColumnX = _p.FieldX + " расчет";
                    ColumnY = _p.FieldX + " остатки";
                    break;
            }

            if (!withHexBin)
            {
                cd.ChartSeries.Add(new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    XColumnName = ColumnX,
                    XIsNominal = false,
                    XAxisTitle = ColumnX,
                    SeriesTitle = _p.FieldX,
                    SeriesType = ChartSeriesType.LineXY,
                    YAxisTitle = ColumnY,
                    YColumnName = ColumnY,
                    VColumnName = (_p.LabelField != "Нет") ? _p.LabelField : null,
                    ZColumnName = (_p.ColorField != "Нет") ? _p.ColorField : null,
                    WColumnName = (_p.MarkerSizeField != "Нет") ? _p.MarkerSizeField : null,
                    ColorColumnName = "Tag",
                    MarkerSizeColumnName = "Маркер",
                    LabelColumnName = "Метка",
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = _p.MarkerMode,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    VAxisTitle = _p.LabelField,
                    MarkerColor = color,
                    LineColor = color,
                    MarkerSize = 5,
                });
            }

            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                    ? "Mean"
                    : "Median";
                AppendHelper(cd, color, ColumnX, ColumnY, "#RangeOnX", ChartSeriesType.RangeOnX, options + textenabled);
                AppendHelper(cd, color, ColumnX, ColumnY, "#RangeOnY", ChartSeriesType.RangeOnY, options + textenabled);
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                AppendHelper(cd, color, ColumnX, ColumnY, "#KDEOnY", ChartSeriesType.KDEOnY);
                AppendHelper(cd, color, ColumnX, ColumnY, "#KDEOnX", ChartSeriesType.KDEOnX);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                AppendHelper(cd, color, ColumnX, ColumnY, "#RugOnY", ChartSeriesType.RugOnY);
                AppendHelper(cd, color, ColumnX, ColumnY, "#RugOnX", ChartSeriesType.RugOnX);
            }

            if (withHexBin)
            {
                AppendHelper(cd, color, ColumnX, ColumnY, string.Empty, ChartSeriesType.HexBin, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty));
            }

            if (_p.ColorField != null && _p.ColorField != "Нет" && (_p.RangeStyle& ReportChartRangeStyle.Single)!=ReportChartRangeStyle.Single)
            {
                foreach (var crt in _chartRangesTables)
                {
                    RangesHelper.AppendRanges(cd, false, crt, _p.RangeType == ReportChartRangeType.RangeIsSigmas || _p.RangeType == ReportChartRangeType.RangeIsSlide || _p.RangeType == ReportChartRangeType.RangeIsR, true, _p.RangeStyle, _p.RangeType == ReportChartRangeType.RangeIsAB);
                }

                foreach (var crte in _chartRangesTablesWithErrors)
                {
                    RangesHelper.AppendRanges(cd, true, crte, _p.RangeType == ReportChartRangeType.RangeIsSigmas || _p.RangeType == ReportChartRangeType.RangeIsSlide || _p.RangeType == ReportChartRangeType.RangeIsR, true, _p.RangeStyle, _p.RangeType == ReportChartRangeType.RangeIsAB);
                }
                
            }
            else
            {

                if (_p.ViewType == ReportChartRegressionViewType.PredictedObserved && _chartRangesTables != null)
                {
                    RangesHelper.AppendRanges(cd, false, _chartRangesTables[0],
                        _p.RangeType == ReportChartRangeType.RangeIsSigmas ||
                        _p.RangeType == ReportChartRangeType.RangeIsSlide ||
                        _p.RangeType == ReportChartRangeType.RangeIsR, true, _p.RangeStyle,
                        _p.RangeType == ReportChartRangeType.RangeIsAB);
                }

                if (_chartRangesTablesWithErrors != null)
                {
                    RangesHelper.AppendRanges(cd, true, _chartRangesTablesWithErrors[0],
                        _p.RangeType == ReportChartRangeType.RangeIsSigmas ||
                        _p.RangeType == ReportChartRangeType.RangeIsSlide ||
                        _p.RangeType == ReportChartRangeType.RangeIsR, true, _p.RangeStyle,
                        _p.RangeType == ReportChartRangeType.RangeIsAB);
                }
            }
            return result;
        }

        private void AppendHelper(ChartDescription cd, string color, string ColumnX, string ColumnY, string seriesPrefix, ChartSeriesType helperType, string options=null)
        {
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Данные",
                XColumnName = ColumnX,
                XIsNominal = false,
                XAxisTitle = ColumnX,
                SeriesTitle = seriesPrefix + _p.FieldX,
                SeriesType = helperType,
                YAxisTitle = ColumnY,
                YColumnName = ColumnY,
                ZColumnName = (_p.ColorField != "Нет") ? _p.ColorField : null,
                ColorColumnName = "Tag",
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                ZAxisTitle = _p.ColorField,
                MarkerColor = color,
                MarkerSize = 5,
                LineColor = color,
                Options = options,
                ZValueSeriesTitle = (_p.ColorField != "Нет" && !string.IsNullOrEmpty(seriesPrefix)) ? _p.FieldX : null,
            };
            cd.ChartSeries.Add(csd2);
        }
    }
}