﻿using System;
using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using AQChartLibrary;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        void chartScatterPlot_AxisClicked(object o, AxisEventArgs e)
        {
            AxisClicked?.Invoke(this, e);
        }
        
        private void ChartMultipleRegression_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (_selectedTable == null || e.Table == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs { AvailableOperations = InteractiveOperations.None, });
                return;
            }
            var rowList = new List<int>();
            var message = _p.FieldX + " = ";
            var isFirst = true;

            foreach (var ii in e.InteractivityIndexes.Where(a=>!a.SeriesName.EndsWith("_Layer")))
            {
                if (!isFirst) message += "; ";
                var f = _selectedTable.ColumnNames.IndexOf(_p.FieldX);
                var worktable = _interactiveTable;
                var sourceIndex = (int) worktable.Table[ii.Index].Row[4];

                rowList.Add(sourceIndex);

                var value = _selectedTable.Table[sourceIndex].Row[f].ToString();
                message += $"{value}";
                isFirst = false;
            }
            foreach (var ii in e.InteractivityIndexes.Where(a => a.SeriesName.EndsWith("_Layer")))
            {
                if (ii.Layer?.ToString() == "#NoLayers") rowList.AddRange(_selectedTable.GetRowsForAllValues());
                else rowList.AddRange(_selectedTable.GetRowsForAllValuesFromLayer(ii.LayerName, ii.Layer));
            }

            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable> { _interactiveTable } });
            Interactivity?.Invoke(this,
                new InteractivityEventArgs
                {
                    Table = _selectedTable.TableName,
                    Field = _p.FieldX,
                    Message = message,
                    AvailableOperations = InteractiveOperations.All,
                    SelectedCases = rowList.Distinct().ToList(),
                    Clear = true
                });
        }


        private void ChartDynamics_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null) return;
            var worktable = _interactiveTable;
            var sourceIndex = (int)worktable.Table[e.Info.Index].Row[4];
            var wp2 = new WrapPanel {Orientation = Orientation.Vertical, MaxHeight = 200};
            e.TT.Content = wp2;
            var sldr2 = _selectedTable.Table[sourceIndex];
            for (int i = 0; i <_selectedTable.ColumnNames.Count; i++)
            {
                if (sldr2.Row[i]==null || String.IsNullOrEmpty(sldr2.Row[i].ToString()) || _selectedTable.ColumnNames[i].StartsWith("#")) continue;
                var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = _selectedTable.ColumnNames[i] + ": " + sldr2.Row[i],
                    Margin = new Thickness(0, 0, 20, 0)
                };
                wp2.Children.Add(tb);
            }
        }

        void chartScatterPlot_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}