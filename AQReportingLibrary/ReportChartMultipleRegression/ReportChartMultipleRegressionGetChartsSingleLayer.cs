﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        private SLDataTable _interactiveTable;
        private Data _data;
        private Panel _surface;
        private BackgroundWorker _worker;
        private Thread _workerThread;

        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _dataTables = dataTables.ToList();
            _surface = surface;
            _surface.Children.Clear();
            if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField=="Нет")
            {
                SetMessage("Построение модели...");
                SetupChartDataSingleLayer(dataTables);
                return true;
            }
            SetMessage("Построение моделей...");
            SetupChartDataMultipleLayers(dataTables);
            return true;
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 30,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }

        public void RefreshVisuals()
        {
            
            if (_p.LayersMode == ReportChartLayerMode.Single || _p.ColorField == "Нет")
            {
                switch(_p.ViewType)
                {
                    case ReportChartRegressionViewType.ResultsInTable:
                        if (_regressionTables == null || _regressionTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _fullRegressionTables.Values.ToList() });
                        ShowRegressionTables();
                        break;
                    case ReportChartRegressionViewType.RMatrix:
                        if (_correlationTables == null || _correlationTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _correlationTables.Values.ToList() });
                        ShowCorrelationTables();
                        break;
                    case ReportChartRegressionViewType.RGraph:
                        if (_correlationTables == null || _correlationTables.Count == 0 || _graphTables == null || _graphTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables });
                        ShowCircularGraph();
                        break;
                    default:
                        if (_interactiveTable == null || _interactiveTable.Table.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable> { _resultedTable } });
                        SetupChartRanges();
                        SetupFitSingleLayer();
                        CreateChartsSingleLayer();
                        break;
                }
            }
            else
            {
                switch (_p.ViewType)
                {
                    case ReportChartRegressionViewType.ResultsInTable:
                        if (_regressionTables == null || _regressionTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _fullRegressionTables.Values.ToList() });
                        ShowRegressionTables();
                        break;
                    case ReportChartRegressionViewType.RMatrix:
                        if (_correlationTables == null || _correlationTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _correlationTables.Values.ToList() });
                        ShowCorrelationTables();
                        break;
                    case ReportChartRegressionViewType.RGraph:
                        if (_correlationTables == null || _correlationTables.Count == 0 || _graphTables == null || _graphTables.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables });
                        ShowCircularGraph();
                        break;
                    default:
                        if (_interactiveTable == null || _interactiveTable.Table.Count == 0 || (_worker != null && _worker.IsBusy)) return;
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable> { _resultedTable } });
                        SetupFitMultipleLayers();
                        CreateChartsMultipleLayers();
                        break;
                }
            }
        }

        private void SetupFitSingleLayer()
        {
            if (_data == null) return;
            var lri2 = LineSmoothHelper.GetLinearRegression(_data.GetLDR());
            var c1 = "#ffff4000";
            _fit = new List<MathFunctionDescription>
            {
                lri2.SetupLine(_p.FieldX, c1, GetLineThickness(_p.SmoothStyle), null, null),
                lri2.SetupPredictionLine(_p.FieldX, c1, GetLineThickness(_p.SmoothStyle), 1, true, null, null),
                lri2.SetupPredictionLine(_p.FieldX, c1, GetLineThickness(_p.SmoothStyle), -1, true, null, null),
                lri2.SetupPredictionLine(_p.FieldX, c1, GetLineThickness(_p.SmoothStyle), 1, false, null, null),
                lri2.SetupPredictionLine(_p.FieldX, c1, GetLineThickness(_p.SmoothStyle), -1, false, null, null),
            };
            
            foreach (var f in _fit)
            {
                f.Description = "#" + f.Description;
            }
        }

        private bool SetupChartDataSingleLayer(IEnumerable<SLDataTable> dataTables)
        {
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _p.FieldX,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }
            
            var modelExists = true;
            _worker.RunWorkerCompleted += (o, e) =>
            {
                if (!modelExists) return;
                _surface.Dispatcher.BeginInvoke(() =>
                {
                    switch (_p.ViewType)
                    {
                        case ReportChartRegressionViewType.ResultsInTable:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _fullRegressionTables?.Values.ToList() });
                            ShowRegressionTables();
                            break;
                        case ReportChartRegressionViewType.RMatrix:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _correlationTables?.Values.ToList() });
                            ShowCorrelationTables();
                            break;
                        case ReportChartRegressionViewType.RGraph:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables });
                            ShowCircularGraph();
                            break;
                        default:
                            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable> { _resultedTable } });
                            SetupFitSingleLayer();
                            CreateChartsSingleLayer();
                            break;
                    }
                });
            };

            _worker.DoWork += (o, e) =>
            {
                _data = null;
                _workerThread = Thread.CurrentThread; 
                var r = new Regression(_selectedTable, _p.FieldX, _p.Fields, null, null, _p.StepwiseType, _p.ModelQuality, _p.RegressionOptions, (BackgroundWorker)o);
                if (r == null || r.RegressionInput == null || _p.Fields.Count == 0)
                {
                    modelExists = false;
                    _surface?.Dispatcher.BeginInvoke(() => { SetMessage("Не удалось построить модель"); });
                    return;
                }
                var model = new RegressionLine(r);
                _resultedTable = model.GetCalculatedTable(_p.FieldX + " расчет", _p.FieldX + " остатки", null, null);

                var fi = new FieldsInfo
                {
                    ColumnZ = _p.ColorField,
                    ColumnW = _p.MarkerSizeField,
                    ColumnV = _p.LabelField
                };
                switch (_p.ViewType)
                {
                    case ReportChartRegressionViewType.PredictedObserved:
                        fi.ColumnY = _p.FieldX;
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;

                    case ReportChartRegressionViewType.ObservedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX;
                        break;

                    case ReportChartRegressionViewType.PredictedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;
                }


                _data = new Data(_resultedTable, fi, _p.Fields);
                _ebi = new List<ErrorBarInfo> { new ErrorBarInfo {Mean = r.RegressionResult.Mean, StDev = r.RegressionResult.StDev,
                    SE = r.RegressionResult.StdError, n = r.RegressionInput.n, Ldr =_data.GetLDR(),
                    MeanOfResult = r.RegressionResult.MeanOfResult,
                    StDevOfResult = r.RegressionResult.StdevOfResult,
                    StDevOdResiduals = r.RegressionResult.StDevOfResiduals } };
                _interactiveTable = _data.GetDataTable();
                _surface?.Dispatcher.BeginInvoke(() => { MarkFieldsSingleLayer(r); });
                _regressionComment = model.GetRegressionComment(true, true);
                var ft = GetFullRegressionTable(r);
                if (ft!=null)ft.Header = _regressionComment;
                var t = GetShortRegressionTable(r);
                if (t!=null)t.Header = _regressionComment;
                
                
                var ct = GetCorrelationTable(r);
                if (ct != null)
                {
                    ct.Header = _regressionComment;
                    _correlationTables = new Dictionary<object, SLDataTable> { { string.Empty, ct } };
                }
                _regressionTables = new Dictionary<object, SLDataTable> { { string.Empty, t } };
                _fullRegressionTables = new Dictionary<object, SLDataTable> { { string.Empty, ft } };
                CreateCorrelationGraphData();
                if (_p.ColorField!="Нет" && (_p.RangeStyle & ReportChartRangeStyle.Single) != ReportChartRangeStyle.Single) SetLayeredDataForRanges(r);
                else SetupChartRanges();
            };
            _worker.RunWorkerAsync();
            return true;
        }

        private void SetLayeredDataForRanges(Regression regression)
        {
            _ebi = new List<ErrorBarInfo>();
            _layers = GetGroups();
            _datas = new Dictionary<object, Data>();
            foreach (var z in _layers)
            {
                if (regression.RegressionInput == null) continue;
                var r = regression.RegressionResult;
                if (r.BMatrix == null || double.IsNaN(r.MultipleR)) continue;
                var model = new RegressionLine(regression);
                var rt = model.GetCalculatedTable(_p.FieldX + " расчет", _p.FieldX + " остатки", _p.ColorField, z);
                rt.TableName += z?.ToString();
                var fi = new FieldsInfo
                {
                    ColumnZ = _p.ColorField,
                    ColumnW = _p.MarkerSizeField,
                    ColumnV = _p.LabelField
                };
                switch (_p.ViewType)
                {
                    case ReportChartRegressionViewType.PredictedObserved:
                    case ReportChartRegressionViewType.RMatrix:
                    case ReportChartRegressionViewType.RGraph:
                    case ReportChartRegressionViewType.ResultsInTable:
                        fi.ColumnY = _p.FieldX;
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;

                    case ReportChartRegressionViewType.ObservedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX;
                        break;

                    case ReportChartRegressionViewType.PredictedResiduals:
                        fi.ColumnY = _p.FieldX + " остатки";
                        fi.ColumnX = _p.FieldX + " расчет";
                        break;
                }
                var d = new Data(rt, fi, _p.Fields);

                _datas.Add(z, d);
                var columndata = rt.GetColumnData(_p.FieldX).Select(a => (object) a).ToList();
                var columnCalcdata = rt.GetColumnData(_p.FieldX + " расчет").Select(a => (object)a).ToList();
                var columnResdata = rt.GetColumnData(_p.FieldX + " остатки").Select(a => (object)a).ToList();
                int n;
                double layerSe;
                if ((_p.RangeStyle & ReportChartRangeStyle.LayeredErrorBar) == ReportChartRangeStyle.LayeredErrorBar)
                {
                    n = AQMath.AggregateCount(columnResdata);
                    var m = r.Input.m;
                    layerSe = Math.Sqrt(columnResdata.OfType<double>().Sum(a => a*a)/(n - m - 1));
                }
                else
                {
                    n = r.Input.n;
                    layerSe = r.StdError;
                }
                if (columnCalcdata.Any(a => a is double) && columndata.Any(a=>a is double))
                _ebi.Add(new ErrorBarInfo
                {
                    Mean = AQMath.AggregateMean(columndata).Value,
                    StDev = AQMath.AggregateStDev(columndata).Value,
                    SE = layerSe,
                    n = n,
                    Layer = z,
                    Ldr = d.GetLDR(),
                    MeanOfResult = AQMath.AggregateMean(columnCalcdata).Value,
                    StDevOfResult = AQMath.AggregateStDev(columnCalcdata).Value,
                    StDevOdResiduals = AQMath.AggregateStDev(columnResdata).Value});
                }
            SetupChartRanges();
        }

        private void MarkFieldsSingleLayer(Regression regression)
        {
            if (regression.RegressionInput == null) return;
            var xmark = new Dictionary<string, Color>();
            var ymark = new Dictionary<string, Color>();
            var r = regression.RegressionResult;
            var ri = regression.RegressionInput;
            for (var i = 0; i < ri.m; i++)
            {
                if (regression.IndependentVariables[i]!= regression.DependentVariable) ymark.Add(regression.IndependentVariables[i], (r.p[i] <= 0.05) ? Colors.Red : Colors.Gray);
            }
            ymark.Add(regression.DependentVariable, Colors.Purple);
            if (Math.Abs(r.MultipleR) >= 0.4) xmark.Add(regression.DependentVariable, Colors.Red);
            FieldMarkChanging?.Invoke(this, new FieldMarkChanging {ResetRequired = true, YFieldMarks = ymark, XFieldMarks =xmark});
        }

        private void CreateChartsSingleLayer()
        {
            _surface.Children.Clear();
            foreach (ChartDescription cd in GetDescriptionForMultipleRegressionSingleLayer())
            {
                var chartMultipleRegression = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    GlobalFontCoefficient = _p.FontSize,
                    EnableCellCondense = true,
                    ShowXHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable> {_interactiveTable},
                    FixedAxises = new FixedAxises {FixedAxisCollection = new ObservableCollection<FixedAxis> {new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } }, Editable = true},
                    CustomLegendTables = _regressionTables?.Values?.ToList()
                };
                if (_chartRangesTables!=null) chartMultipleRegression.DataTables.AddRange(_chartRangesTables);
                if (_chartRangesTablesWithErrors != null) chartMultipleRegression.DataTables.AddRange(_chartRangesTablesWithErrors);
                chartMultipleRegression.ToolTipRequired += ChartDynamics_ToolTipRequired;
                chartMultipleRegression.SelectionChanged += ChartMultipleRegression_SelectionChanged;
                chartMultipleRegression.AxisClicked += chartScatterPlot_AxisClicked;
                chartMultipleRegression.InteractiveRename += chartScatterPlot_InteractiveRename;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartMultipleRegression });
                _surface.Children.Add(chartMultipleRegression);
                chartMultipleRegression.VisualReady += ChartMultipleRegressionOnVisualReady;
                chartMultipleRegression.MakeChart();
            }
        }

        private void ChartMultipleRegressionOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            switch (_p.ViewType)
            {
                case ReportChartRegressionViewType.ResultsInTable:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _fullRegressionTables.Values.ToList() });
                    break;
                case ReportChartRegressionViewType.RMatrix:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _correlationTables.Values.ToList() });
                    break;
                case ReportChartRegressionViewType.RGraph:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = _graphTables.ToList() });
                    break;
                default:
                    Finished?.Invoke(this, new ReportChartFinishedEventArgs { InnerTables = new List<SLDataTable> { _resultedTable } });
                    break;
            }
        }

        double GetLineThickness(ReportChartRangeStyle lineStyle)
        {
            return lineStyle == ReportChartRangeStyle.Invisible
                          ? 0.5
                          : (lineStyle == ReportChartRangeStyle.Thin ? 1 : 2);
        }
    }
}