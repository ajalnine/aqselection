﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartMultipleRegression
    {
        private List<SLDataTable> _graphTables;
        
        private void ShowCircularGraph()
        {
            if (_graphTables == null || _graphTables.Count==0) return;
            _surface.Children.Clear();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs {InternalTables = _graphTables});
            var cd = new ChartDescription {ChartSeries = new List<ChartSeriesDescription>()};
            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = _p.ColorScale == ColorScales.HSV && (_p.ColorField == "Нет" || _p.LayersMode == ReportChartLayerMode.Single)
                ? "<red>●</ red> R > 0.5    ● R ≤ 0.5"
                : String.Empty;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = _p.ChartLegend;
            var csd = new ChartSeriesDescription
            {
                SourceTableName = _graphTables[0].TableName,
                XColumnName = "Фактор1",
                XIsNominal = false,
                YIsNominal = false,
                XAxisTitle = "Фактор1",
                SeriesTitle = "Граф корреляций",
                SeriesType = ChartSeriesType.CircularGraph,
                YAxisTitle = "Фактор2",
                YColumnName = "Фактор2",
                Options = _p.ColorScale == ColorScales.HSV ? "ToRed" : null,
                VColumnName = "Корреляция",
                ZColumnName = _p.ColorField == "Нет" || _p.LayersMode == ReportChartLayerMode.Single ? null : _p.ColorField,
                ColorScale = _p.ColorScale,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(_p.ColorScale),
                MarkerSize = 5
            };
            cd.ChartSeries.Add(csd);

            var circularGraph = new Chart
            {
                EnableSecondaryAxises = false,
                Height = _p.LayoutSize.Height,
                Width = _p.LayoutSize.Width,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = false,
                ShowHorizontalGridlines = false,
                ShowHorizontalAxises = false,
                ShowVerticalAxises = false,
                ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                GlobalFontCoefficient = _p.FontSize,
                EnableCellCondense = true,
                ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                ShowTable = false,
                Margin = new Thickness(0, 0, 0, 20),
                ChartData = cd,
                SourceName = _p.Table,
                DataTables = new List<SLDataTable>
                {
                    _graphTables[0]
                },
                FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } }, Editable = true },
            };
            circularGraph.InteractiveRename += chartScatterPlot_InteractiveRename;
            ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs {SettingChart = circularGraph});
            _surface.Children.Add(circularGraph);
            circularGraph.VisualReady += CircularGraph_VisualReady;
            circularGraph.MakeChart();
        }

        private void CircularGraph_VisualReady(object o, VisualReadyEventArgs e)
        {
            Finished.Invoke(this, new ReportChartFinishedEventArgs());
        }
    }
}