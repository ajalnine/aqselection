﻿using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;
using System.Linq;
using AQChartLibrary;
using AQMathClasses;
using System.Globalization;
using System;

namespace AQReportingLibrary
{
    public class LimitedLayers
    {
        private List<object> _othersLayer;
        private int _allowedLayersNumber;
        private string _layerField;
        private string _valueField;
        private string _Xfield;
        private string _Yfield;
        private bool _useFormat, _useLayersLimitation;
        private Aggregate _method;
        private SLDataTable _source, _result;
        private FixedAxises _fixedAxises;

        private delegate IEnumerable<object> GroupCalcDelegate(IEnumerable<GroupValuePair> data, int groupNumber);
        private readonly Dictionary<Aggregate, GroupCalcDelegate> _limitedLayerCalculators = new Dictionary<Aggregate, GroupCalcDelegate>
        {
            {Aggregate.Single,   (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.N,        (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.Percent,  (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.PercentInCategory,  (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.Sum,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Sum(a => a.Value)).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.Min,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Min(a => a.Value)).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.Max,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Max(a => a.Value)).Skip(groupNumber).Select(g => g.Key).ToList()},
            {Aggregate.Mean,     (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateMean(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Skip(groupNumber).Select(g =>g.Key).ToList()},
            {Aggregate.Median,   (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateMedian(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Skip(groupNumber).Select(g =>g.Key).ToList()},
            {Aggregate.First,    (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.First().Value).Skip(groupNumber).Select(g =>g.Key).ToList()},
            {Aggregate.Last,     (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Last().Value).Skip(groupNumber).Select(g =>g.Key).ToList()},
            {Aggregate.StDev,    (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateStDev(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Skip(groupNumber).Select(g =>g.Key).ToList()}
        };

        public LimitedLayers(LimitedLayersDescription lld)
        {
            _source = lld.SourceTable;
            _Xfield = lld.X;
            _Yfield = lld.Y;
            _layerField = lld.Z;
            _valueField = lld.ValuesForAggregation;
            _allowedLayersNumber = lld.AllowedLayersNumber;
            _method = lld.Method;
            _useFormat = lld.UseFormat;
            _useLayersLimitation = lld.UseLayersLimitation;
            _fixedAxises = lld.FixedAxises;

            if (_useFormat || (_useLayersLimitation && _allowedLayersNumber > 0))
            {
                _result = _source.Clone();

                if (_useFormat)
                {
                    FormatFields();
                }

                if (_useLayersLimitation && _allowedLayersNumber > 0 )
                {
                    FillOthersLayer();
                    ReduceLayers();
                }
            }
            else
            {
                _result = _source;
            }
        }

        private void FillOthersLayer()
        {
            var z = _result.ColumnNames.IndexOf(_layerField);
            var y = _result.ColumnNames.IndexOf(_valueField);
            if (z < 0 || y < 0) return;
            var allData = (!(new[] { Aggregate.N, Aggregate.Percent, Aggregate.PercentInCategory }).Contains(_method))
                ? _result.Table.Select(a => new GroupValuePair { Name = string.IsNullOrEmpty(a.Row[z]?.ToString()) ? "#" : a.Row[z], Value = (double?)(string.IsNullOrEmpty(a.Row[y]?.ToString()) ? null : a.Row[y]) }).ToList()
                : _result.Table.Select(a => new GroupValuePair { Name = string.IsNullOrEmpty(a.Row[z]?.ToString()) ? "#" : a.Row[z], Value = (double?)(string.IsNullOrEmpty(a.Row[y]?.ToString()) ? (double?) null : 1.0d) }).ToList();

                _othersLayer =  _limitedLayerCalculators[_method].Invoke(allData, _allowedLayersNumber).ToList();
        }

        private void ReduceLayers()
        {
            var i = _result.ColumnNames.IndexOf(_layerField);
            if (_layerField == "Нет" || _layerField == null)return;
            var othersRows = _result.GetRowsForAllValuesFromLayers(_layerField, _othersLayer);
            _result.DataTypes[i] = "String";
            foreach (var row in _result.Table) row.Row[i] = row.Row[i]?.ToString();
            if (_result.IsFiltered)
            {
                if (_result.UnfilteredTable != null)
                    foreach (var row in _result.UnfilteredTable)
                        row.Row[i] = row.Row[i]?.ToString();
                var filter = _result?.Filters?.SingleOrDefault(a => a.ColumnName == _layerField);
                if (filter != null)
                    foreach (var f in filter.FilterItems)
                        f.Value = f.Value?.ToString();
            }

            for (var r = 0; r < othersRows.Count; r++) _result.Table[othersRows[r]].Row[i] = " Прочие";
        }

        private void FormatFields()
        {
            var Xformat = _fixedAxises?.FixedAxisCollection?.SingleOrDefault(a => a.Axis == "X")?.Format;
            var x = _result.ColumnNames.IndexOf(_Xfield);
            if (!string.IsNullOrEmpty(Xformat) && _Xfield != null && x >= 0 && _result.DataTypes[x].Split('.').Last().ToLower()=="datetime") FormatField(x, Xformat);

            var Yformat = _fixedAxises?.FixedAxisCollection?.SingleOrDefault(a => a.Axis == "Y")?.Format;
            var y = _result.ColumnNames.IndexOf(_Yfield);
            if (!string.IsNullOrEmpty(Yformat) && _Yfield != null && y >= 0 && y!= x && _result.DataTypes[y].Split('.').Last().ToLower() == "datetime") FormatField(y, Yformat);
            
            var Zformat = _fixedAxises?.FixedAxisCollection?.SingleOrDefault(a => a.Axis == "Z")?.Format;
            var z = _result.ColumnNames.IndexOf(_layerField);
            if (!string.IsNullOrEmpty(Zformat) && _layerField != null && z >= 0 && z!= x && z!= y && _result.DataTypes[z].Split('.').Last().ToLower() == "datetime") FormatField(z, Zformat);
        }

        private void FormatField(int i, string format)
        {
            foreach (var row in _result.Table.Where(a=>a.Row[i] is DateTime))
            {
                row.Row[i] = ((DateTime) row.Row[i]).ToString(format, CultureInfo.CurrentCulture);
            }
            _result.DataTypes[i] = "String";
        }


        public SLDataTable GetLimitedLayersTable()
        {
            return _result;
        }

        public List<object> GetOthersLayer()
        {
            return _othersLayer;
        }
        private class GroupValuePair
        {
            public object Name;
            public double? Value;
        }
    }
    public class LimitedLayersDescription
    {
        public SLDataTable SourceTable;
        public string X;
        public string Y;
        public string Z;
        public string ValuesForAggregation;
        public int AllowedLayersNumber;
        public Aggregate Method = Aggregate.N;
        public FixedAxises FixedAxises;
        public bool UseFormat;
        public bool UseLayersLimitation;
    }

}
