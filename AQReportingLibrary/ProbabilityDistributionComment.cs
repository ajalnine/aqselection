﻿using System;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class Probability
    {
        public string GetDistributionFitComment()
        {
            return  GetVariationComment() + (_source.Count < 1000 ? GetShapiroWilkComment() : GetKolmogorovSmirnovComment());
        }

        private string GetKolmogorovSmirnovComment()
        {
            var result = string.Empty;
            var ks = AQMath.KolmogorovD(_source);
            var pks = AQMath.KolmogorovP(_source);
            if (!double.IsNaN(ks) && !double.IsNaN(pks))
            {
                if (pks > 0.05) result +=
                    $"\r\nD = {Math.Round(ks, 3)}, p(D) = {Math.Round(pks, 3)}, распределение близко к нормальному";
                else result +=
                    $"\r\nD = {Math.Round(ks, 3)}, <red>p(D) = {Math.Round(pks, 3)}</red>, распределение отличается от нормального";
            }
            return result;
        }

        private string GetShapiroWilkComment()
        {
            var result = string.Empty;
            var sw = AQMath.ShapiroWilkW(_source);
            var pSw = AQMath.ShapiroWilkP(_source);
            if (sw.HasValue && !double.IsNaN(sw.Value) && pSw.HasValue && !double.IsNaN(pSw.Value))
            {
                if (pSw > 0.05) result +=
                    $"\r\nW = {Math.Round(sw.Value, 3)}, p(W) = {Math.Round(pSw.Value, 3)}, распределение близко к нормальному";
                else result +=
                    $"\r\nW = {Math.Round(sw.Value, 3)}, <red>p(W) = {Math.Round(pSw.Value, 3)}</red>, распределение отличается от нормального";
            }
            return result;
        }

        private string GetVariationComment()
        {
            if (!_mean.HasValue || !_stdev.HasValue|| _mean==0) return string.Empty;
            var result = string.Empty;
            var kv = Math.Round(_stdev.Value / _mean.Value, 4);
            result += String.Format(", Квар = {0}", kv);
            
            /*if (kv < 0.17) result += ", данные однородны";
            else if (kv > 0.40) result += ", данные неоднородны";
            else if (kv > 0.33) result += ", данные недостаточно однородны";
            else result += ", данные достаточно однородны";*/
            return result;
        }

        public SLDataTable GetTable()
        {
            return _probabilityTable;
        }
    }
    public class ProbabilityItem
    {
        public double Frequency { get; set; }
        public double Value { get; set; }
    }
}
