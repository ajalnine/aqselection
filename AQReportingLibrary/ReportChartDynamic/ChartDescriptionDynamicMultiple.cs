﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartDynamic
    {
       private IEnumerable<ChartDescription> GetDescriptionForDynamicMultiple()
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            if ( (_p.LabelField != null && _p.LabelField != "Нет") || _p.Fields.Count >1) cd.ChartLegendType = _p.ChartLegend;
            cd.ChartTitle = GetChartTitle();
            cd.ChartSeries = new List<ChartSeriesDescription>();

            var colorPosition = 0.4d;

            var spNumber = _selectedParameters.Count();
            var colorStep = 1.0d / (spNumber+1);
            cd.ChartSubtitle += _regressionComment;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;
            var count = 0;
            foreach(var sp in _selectedParameters)
            {
                if (spNumber <= 4)
                {
                    if (!string.IsNullOrEmpty(cd.ChartSubtitle)) cd.ChartSubtitle += "\r\n";
                    cd.ChartSubtitle += string.Format("n(<b>{0}</b>)={1}", sp, _datas[sp].GetDataTable().Table.Count(a => a.Row[2] != null));
                }
                var color = ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition);
                if (!withHexBin)
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Данные" + sp,
                        XColumnName = _p.DateField,
                        YAxisTitle = sp,
                        XIsNominal = false,
                        XAxisTitle = _p.DateField,
                        YColumnName = sp,
                        SeriesTitle = sp,
                        SeriesType = ChartSeriesType.LineXY,
                        YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                        VColumnName = (_p.LabelField != "Нет") ? _p.LabelField : null,
                        ColorColumnName = "Tag",
                        MarkerSizeColumnName = "Маркер",
                        LabelColumnName = "Метка",
                        ColorScale = _p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        MarkerMode = _p.MarkerMode,
                        MarkerSizeMode = _p.MarkerSizeMode,
                        VAxisTitle = _p.LabelField,
                        MarkerColor = color,
                        LineColor = color,
                        MarkerSize = 5,
                        MarkerShapeMode = _p.MarkerShapeMode,
                    });
                }

                if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
                {
                    var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                        ? "Text"
                        : string.Empty;
                    var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                        ? "Mean"
                        : "Median";
                    if (spNumber > 1) options += "IsMultiple";
                    AppendHelperMultiple(cd, count, sp, color, "#RangeOnY", ChartSeriesType.RangeOnY, options + textenabled);
                }


                if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#KDEOnX", ChartSeriesType.KDEOnX);
                    AppendHelperMultiple(cd, count, sp, color, "#KDEOnY", ChartSeriesType.KDEOnY);
                }

                if ((_p.Tools & ReportChartTools.ToolRug) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#RugOnX", ChartSeriesType.RugOnX);
                    AppendHelperMultiple(cd, count, sp, color, "#RugOnY", ChartSeriesType.RugOnY);
                }
                if (withHexBin)
                {
                    AppendHelperMultiple(cd, count, sp, color, string.Empty, ChartSeriesType.HexBin, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty));
                }

                if (_p.SmoothMode== ReportChartRegressionFitMode.DynamicSmoothLowess && _chartLowessTables!=null) cd.ChartSeries.Add(new ChartSeriesDescription
                {
                    SourceTableName = "Lowess" + sp,
                    XColumnName = _p.DateField,
                    XIsNominal = false,
                    XAxisTitle = _p.DateField,
                    YColumnName = sp,
                    YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                    SeriesTitle = "#Lowess" + sp,
                    SeriesType = ChartSeriesType.LineXY,
                    LineColor = _selectedParameters.Count == 1
                    ? _p.LayersMode == ReportChartLayerMode.Single ? "#ffFF4040" : color
                    : color,
                    LineSize = GetLineThickness(_p.SmoothStyle) * 2,
                    IsSmoothed = true,
                    YAxisTitle = sp
                });
                colorPosition += colorStep;
                if (colorPosition > 1) colorPosition -= 1;
                count++;
            }
            RangesHelper.AppendRanges(cd, true, _p.UserPeriods, false,false, _p.PeriodStyle, _p.RangeType == ReportChartRangeType.RangeIsAB);
            return result;
        }

        private void AppendHelperMultiple(ChartDescription cd, int count, string sp, string color, string seriesPrefix, ChartSeriesType helperType, string options = null)
        {
            var csd3 = new ChartSeriesDescription
            {
                SourceTableName = "Данные" + sp,
                XColumnName = _p.DateField,
                XIsNominal = false,
                XAxisTitle = _p.DateField,
                SeriesTitle = seriesPrefix + sp,
                SeriesType = helperType,
                YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                YAxisTitle = sp,
                YColumnName = sp,
                ColorColumnName = "Tag",
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                ZAxisTitle = _p.ColorField,
                MarkerColor = color,
                LineColor = color,
                MarkerSize = 5,
                Options = "IsMultiple"+options
            };
            cd.ChartSeries.Add(csd3);
        }
    }
}