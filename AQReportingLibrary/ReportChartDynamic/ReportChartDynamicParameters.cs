﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public class ReportChartDynamicParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string DateField { get; set; }
        public ReportChartRegressionFitMode SmoothMode { get; set; }
        public ReportChartRangeStyle SmoothStyle { get; set; }
        public ReportChartLayerMode LayersMode { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartRangeStyle PeriodStyle { get; set; }
        public ReportChartTools Tools { get; set; }
        public bool MarkOutage { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }
        public SLDataTable UserLimits { get; set; }
        public SLDataTable UserPeriods { get; set; }
        public string ColorField { get; set; }
        public string MarkerSizeField { get; set; }
        public string LabelField { get; set; }
        public double LWSArea { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }

        public ChartLegend ChartLegend { get; set; }
    }
}