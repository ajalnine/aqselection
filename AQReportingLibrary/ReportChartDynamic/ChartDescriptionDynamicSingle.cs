﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;
using System.Globalization;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartDynamic
    {
        private IEnumerable<ChartDescription> GetDescriptionForDynamic(Data d, ReportChartDynamicParameters p)
        {
            var result = new List<ChartDescription>();
            int n = d.GetDataTable().Table.Count;
            var cd = new ChartDescription();
            result.Add(cd);
            if ((p.ColorField != null && p.ColorField != "Нет")
                || (p.LabelField != null && p.LabelField != "Нет")
                || (p.MarkerSizeField != null && p.MarkerSizeField != "Нет")
                || p.RangeType != ReportChartRangeType.RangeIsNone) cd.ChartLegendType = _p.ChartLegend;

            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n.ToString(CultureInfo.InvariantCulture);
            cd.ChartSubtitle += (_chartRangesYTable.Table.Count > 0)
                ? RangesHelper.GetDescription(p.RangeType)
                : String.Empty;
            cd.ChartSubtitle += _regressionComment;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;

            cd.ChartSeries = new List<ChartSeriesDescription>();

            if (!withHexBin)
            {
                var csd = new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    XColumnName = p.DateField,
                    XIsNominal = false,
                    XAxisTitle = p.DateField,
                    YColumnName = _selectedParameter,
                    SeriesTitle = _selectedParameter,
                    SeriesType = ChartSeriesType.LineXY,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    WColumnName = (p.MarkerSizeField != "Нет") ? p.MarkerSizeField : null,
                    VColumnName = (p.LabelField != "Нет") ? p.LabelField : null,
                    ColorColumnName = "Tag",
                    MarkerSizeColumnName = "Маркер",
                    LabelColumnName = "Метка",
                    ColorScale = p.ColorScale,
                    MarkerMode = p.MarkerMode,
                    MarkerSizeMode = p.MarkerSizeMode,
                    ZAxisTitle = p.ColorField,
                    VAxisTitle = p.LabelField,
                    MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                    MarkerSize = (p.ColorField == "Нет" || p.MarkerSizeField != "Нет") ? 5 : 7,
                    MarkerShapeMode = p.MarkerShapeMode,
                };
                csd.MarkerSize = 6;
                csd.YAxisTitle = _selectedParameter;
                cd.ChartSeries.Add(csd);
            }

            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                    ? "Mean"
                    : "Median";
                AppendHelper(p, cd, "#RangeOnY", ChartSeriesType.RangeOnY, options + textenabled);
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                AppendHelper(p, cd, "#KDEOnY", ChartSeriesType.KDEOnY);
                AppendHelper(p, cd, "#KDEOnX", ChartSeriesType.KDEOnX);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                AppendHelper(p, cd, "#RugOnY", ChartSeriesType.RugOnY);
                AppendHelper(p, cd, "#RugOnX", ChartSeriesType.RugOnX);
            }

            if (withHexBin)
            {
                AppendHelper(p, cd, String.Empty, ChartSeriesType.HexBin, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty));
            }

            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, p.RangeType == ReportChartRangeType.RangeIsSigmas || p.RangeType == ReportChartRangeType.RangeIsSlide || p.RangeType == ReportChartRangeType.RangeIsR, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);
            RangesHelper.AppendRanges(cd, true, p.UserPeriods, false, false, p.PeriodStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            if (n > 15 && _chartLowessTables != null && p.SmoothMode == ReportChartRegressionFitMode.DynamicSmoothLowess)
            {
                var singleLowess = _chartLowessTables.Count == 1;
                foreach (var l in _chartLowessTables)
                {
                    var csd2 = new ChartSeriesDescription
                    {
                        SourceTableName = l.TableName,
                        XColumnName = p.DateField,
                        XIsNominal = false,
                        XAxisTitle = p.DateField,
                        YColumnName = "Lowess",
                        SeriesTitle = l.TableName,
                        SeriesType = ChartSeriesType.LineXY,
                        ColorScale = p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        LineColor = singleLowess
                        ? p.LayersMode == ReportChartLayerMode.Single ? "#ffFF4040" : ColorConvertor.GetDefaultColorForScale(_p.ColorScale)
                        : null,
                        LineSize = GetLineThickness(p.SmoothStyle) * 2,
                        ZValueForColor = l.CustomProperties?.SingleOrDefault(a => a.Key == "ZColor")?.Value,
                        ZValueSeriesTitle = _selectedParameter,
                        IsSmoothed = true,
                        YAxisTitle = _selectedParameter
                    };
                    cd.ChartSeries.Add(csd2);
                }
            }
            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;
            return result;
        }

        private void AppendHelper(ReportChartDynamicParameters p, ChartDescription cd, string seriesPrefix, ChartSeriesType helperType, string options=null)
        {
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Данные",
                XColumnName = p.DateField,
                XIsNominal = false,
                XAxisTitle = p.DateField,
                SeriesTitle = seriesPrefix + _selectedParameter,
                SeriesType = helperType,
                YAxisTitle = _selectedParameter,
                YColumnName = _selectedParameter,
                ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                ColorColumnName = "Tag",
                ColorScale = p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = p.MarkerMode,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                ZAxisTitle = p.ColorField,
                Options = options,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                MarkerSize = (p.ColorField == "Нет" || p.MarkerSizeField != "Нет") ? 5 : 7,
                ZValueSeriesTitle = (p.ColorField != "Нет" && !string.IsNullOrEmpty(seriesPrefix)) ? _selectedParameter : null,
            };
            cd.ChartSeries.Add(csd2);
        }
    }
}