﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartDynamic
    {
        private void ChartDynamics_AxisClicked(object o, AxisEventArgs e)
        {
            AxisClicked?.Invoke(this, e);
        }

        private void ChartDynamics_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (e.Table == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs { AvailableOperations = InteractiveOperations.None, });
                return;
            }
            var rowList = new List<int>();
            var message = _selectedParameter + " = ";
            var isFirst = true;
            var start = "#RugOnX".Length;
            var end = "_indexed".Length;
            var series = e.InteractivityIndexes.Select(a => a.SeriesName).Distinct();
            var operations = new List<InteractivityEventArgs>();
            foreach (var serie in series)
            {
                if (serie == "Lowess" || serie.StartsWith("#Lowess")) continue;
                var tableName = _selectedParameter ?? serie;
                if (serie.StartsWith("#RugOn")) tableName = serie.Substring(start, serie.Length - end - start);
                var sp = _selectedParameter ?? tableName;
                foreach (var ii in e.InteractivityIndexes.Where(a => a.SeriesName == serie && !a.SeriesName.EndsWith("_Parameter") && !a.SeriesName.EndsWith("_Layer")))
                {
                    if (!isFirst) message += "; ";
                    var f = _selectedTable.ColumnNames.IndexOf(sp);
                    var f2 = _selectedTable.ColumnNames.IndexOf(_p.DateField);
                    var isMultiple = _p.Fields.Count > 1;
                    var worktable = (!isMultiple) ? _interactiveTable : _interactiveTables[tableName];
                    var sourceIndex = (int)worktable.Table[ii.Index].Row[4];

                    var value = _selectedTable.Table[sourceIndex].Row[f].ToString();
                    var value2 = _selectedTable.Table[sourceIndex].Row[f2].ToString();

                    rowList.AddRange(_selectedTable.GetRowsForValueFromFieldAndLayer(sp, value, _p.DateField, value2));

                    message += $"{value} при {_p.DateField}={value2}";
                    isFirst = false;
                }
                foreach (var ii in e.InteractivityIndexes.Where(a => a.SeriesName.EndsWith("_Layer")))
                {
                    if (ii.Layer?.ToString() == "#NoLayers") rowList.AddRange(_selectedTable.GetRowsForAllValues());
                    else rowList.AddRange(_selectedTable.GetRowsForAllValuesFromLayer(ii.LayerName, ii.Layer));
                }
                foreach (var ii in e.InteractivityIndexes.Where(a => a.SeriesName.EndsWith("_Parameter")))
                {
                    var rowListAll = _selectedTable.GetRowsForAllValues();
                    operations.Add(
                        new InteractivityEventArgs
                        {
                            Table = _selectedTable.TableName,
                            Field = ii.LayerName,
                            Message = message,
                            AvailableOperations = InteractiveOperations.All,
                            SelectedCases = rowListAll
                        });
                }
                if (rowList.Count > 0) operations.Add
                        (
                           new InteractivityEventArgs
                           {
                               Table = _selectedTable.TableName,
                               Field = sp,
                               Message = message,
                               AvailableOperations = InteractiveOperations.All,
                               SelectedCases = rowList.Distinct().ToList()
                           }
                        );
            }
            if (operations.Any()) operations.First().Clear = true;
            foreach (var op in operations) Interactivity?.Invoke(this, op);

        }

        private void ChartDynamics_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null) return;
            var start = "#RugOnX".Length;
            var end = "_indexed".Length;
            var tableName = _selectedParameter ?? e.Info.SeriesName;
            var isMultiple = _p.Fields.Count > 1;
            if (e.Info.SeriesName.StartsWith("#RugOn")) tableName = e.Info.SeriesName.Substring(start, e.Info.SeriesName.Length - end - start);
            var worktable = (!isMultiple) ? _interactiveTable : _interactiveTables[tableName];
            var sourceIndex = (int)worktable.Table[e.Info.Index].Row[4];
            var wp2 = new WrapPanel { Orientation = Orientation.Vertical, MaxHeight = 200 };
            e.TT.Content = wp2;
            var sldr2 = _selectedTable.Table[sourceIndex];
            for (int i = 0; i < _selectedTable.ColumnNames.Count; i++)
            {
                if (sldr2.Row[i] == null || String.IsNullOrEmpty(sldr2.Row[i].ToString()) || _selectedTable.ColumnNames[i].StartsWith("#")) continue;
                var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = _selectedTable.ColumnNames[i] + ": " + sldr2.Row[i],
                    Margin = new Thickness(0, 0, 20, 0)
                };
                wp2.Children.Add(tb);
            }
        }


        void chartDynamics_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}