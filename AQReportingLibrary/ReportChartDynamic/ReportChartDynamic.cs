﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartDynamic : IReportElement
    {
        private List<SLDataTable> _chartLowessTables;
        private SLDataTable _chartRangesYTable;
        private SLDataTable _interactiveTable;

        private Dictionary<string, SLDataTable> _interactiveTables;
        private List<string> _selectedParameters;
        private Dictionary<string, Data> _datas;


        private string _probabilityComment = string.Empty;
        private string _regressionComment = string.Empty;
        private string _selectedParameter;
        private List<MathFunctionDescription> _fit;
        private Limits _limits;
        private Data _data;
        private SLDataTable _selectedTable;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType, _originalXType;
        private  Step _step;
        private ReportChartDynamicParameters _p;

        public ReportChartDynamic(ReportChartDynamicParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartDynamic(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartDynamicParameters>(s);
            _step = s;
        }

        public void SetNewParameters(ReportChartDynamicParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return $@"<b>{_p.Table}</b>: {GetChartName().ToLower()} <b>{_p.Fields[0]}</b>";
            else return $@"<b>{_p.Table}</b>: {GetChartName().ToLower()}";
        }

        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return $@"{_p.Table}: {GetChartName().ToLower()} {_p.Fields[0]}";
            return $@"{_p.Table}: {GetChartName().ToLower()}";
        }
        
        public string GetChartName()
        {
            if ((_p.Tools & ReportChartTools.ToolHexBin) > 0) return "Карта плотности";
            return "Динамика";
        }
    }
}