﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartDynamic
    {
        public bool BuildChartsMultiple(List<SLDataTable> dataTables)
        {
            SetupChartDataMultiple(dataTables);
            if (_interactiveTables == null || _interactiveTables.Count == 0 || _interactiveTables.Where(a => a.Value.Table.Count == 0).Any()) return false;
            SetupFitMultiple();
            CreateChartsMultiple();
            return true;
        }

        public void RefreshChartsMultiple()
        {
            if (_interactiveTables == null || _interactiveTables.Count == 0 || _interactiveTables.Where(a => a.Value.Table.Count == 0).Any()) return;
            SetupFitMultiple();
            CreateChartsMultiple();
        }

        private void SetupChartDataMultiple(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameters = _p.Fields;
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);
            _originalXType = _selectedTable.GetDataType(_p.DateField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            _datas = new Dictionary<string, Data>();
            _interactiveTables = new Dictionary<string, SLDataTable>();
            foreach (var sp in _selectedParameters)
            {
                var fi = new FieldsInfo
                {
                    ColumnY = sp,
                    ColumnV = _p.LabelField,
                    ColumnDate = _p.DateField
                };

                var data = new Data(_selectedTable, fi);
                _datas.Add(sp, data);
                var table = _datas[sp].GetDataTable();
                table.TableName = "Данные"+sp;
                _interactiveTables.Add(sp, table);
            }
        }
        
        private void CreateChartsMultiple()
        {
            _surface.Children.Clear();
            foreach (var cd in GetDescriptionForDynamicMultiple())
            {
                var chartDynamics = new Chart
                {
                    EnableSecondaryAxises = true,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    EnableCellCondense = true,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowTable = false,
                    ShowXHelpers = ((_p.Tools & ReportChartTools.ToolRug)) > 0,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable>(),
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalXType, Axis = "X", Format = "dd.MM.yyyy", StringIsDateTime = true }, new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = true } }, Editable = true },
                };
                if (_interactiveTables!=null) chartDynamics.DataTables.AddRange(_interactiveTables.Values);
                if (_chartLowessTables!=null) chartDynamics.DataTables.AddRange(_chartLowessTables);
                chartDynamics.DataTables.Add(_p.UserPeriods);
                chartDynamics.ToolTipRequired += ChartDynamics_ToolTipRequired;
                chartDynamics.SelectionChanged += ChartDynamics_SelectionChanged;
                chartDynamics.AxisClicked += ChartDynamics_AxisClicked;
                chartDynamics.InteractiveRename += chartDynamics_InteractiveRename;
                chartDynamics.VisualReady += ChartDynamicsOnVisualReady;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartDynamics });
                _surface.Children.Add(chartDynamics);
                chartDynamics.MakeChart();
            }
        }
        
        private void SetupFitMultiple()
        {
            var colorPosition = 0.4d;
            var spNumber = _selectedParameters.Count();
            var colorStep = 1.0d / (double)(spNumber + 1);

            switch (_p.SmoothMode)
            {
                case ReportChartRegressionFitMode.DynamicSmoothLowess:
                    _chartLowessTables = new List<SLDataTable>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lowessTable = Smooth.GetChartLowessTableForDateCase(_datas[sp].GetLDR().OrderBy(a => a.Xdate).ToList(),  _p.DateField, null, _p.LWSArea, sp);
                        lowessTable.TableName = "Lowess" + sp;
                        _chartLowessTables.Add(lowessTable);
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothLine:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lri = LineSmoothHelper.GetLinearRegression(_datas[sp].GetLDR());
                        _fit.Add(lri.SetupLine(sp, ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition),
                            GetLineThickness(_p.SmoothStyle) * 2, null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                case ReportChartRegressionFitMode.DynamicSmoothE:
                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {

                        Regression r = new Regression(_datas[sp].GetLDR(), LDRUsedFields.XY, _p.SmoothMode);
                        RegressionLine rl = new RegressionLine(r);
                        _fit.Add(
                            rl.SetupLine(sp, ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition),
                                GetLineThickness(_p.SmoothStyle) * 2,
                                null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lri2 = LineSmoothHelper.GetLinearRegression(_datas[sp].GetLDR());
                        var c1 = ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition);
                        _fit.Add(lri2.SetupLine(sp, c1, GetLineThickness(_p.SmoothStyle) * 2, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), 1, true, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), -1, true, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), 1, false, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), -1, false, null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;
            }
            if (_fit != null)
            foreach (var f in _fit)
            {
                f.Description = "#" + f.Description;
            }
        }
    }
}