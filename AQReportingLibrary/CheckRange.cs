﻿using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

// ReSharper disable once CheckNamespace
public static class CheckRange
{
    private const string MarkColor = "#FFEF6060";
    public static void MarkOutages(bool markOutage, SLDataTable userLimits, ReportChartRangeType rangeType, Limits limits, SLDataTable sldt)
    {
        var tagPosition = sldt.ColumnNames.IndexOf("Tag");
        if (!markOutage) return;

        switch (rangeType)
        {
            case ReportChartRangeType.RangeIsNone:
                foreach (var t in sldt.Table) t.Row[tagPosition] = null;
                break;
            
            case ReportChartRangeType.RangeIsAB:
                foreach (var t in sldt.Table)
                {
                    var x = (double?) t.Row[1];
                    var y = (double?) t.Row[2];
                    for (var r = 0; r < userLimits.ColumnNames.Count; r++)
                    {
                        if (userLimits.ColumnNames[r].EndsWith("Цвет")) continue;
                        var minlimit = (double?)userLimits.Table[0].Row[r];
                        var maxlimit = (double?)userLimits.Table[1].Row[r];
                        var axis = userLimits.ColumnTags[r];
                        if (axis.ToString().ToUpper() == "X")
                        {
                            if (x.HasValue && minlimit.HasValue && x.Value < minlimit.Value) t.Row[tagPosition] = MarkColor;
                            if (x.HasValue && maxlimit.HasValue && x.Value > maxlimit.Value) t.Row[tagPosition] = MarkColor;
                        }
                        else
                        {
                            if (y.HasValue && minlimit.HasValue && y.Value < minlimit.Value) t.Row[tagPosition] = MarkColor;
                            if (y.HasValue && maxlimit.HasValue && y.Value > maxlimit.Value) t.Row[tagPosition] = MarkColor;
                        }
                    }
                }
                break;
            
            default:
                foreach (var t in sldt.Table)
                {
                    var y = (double?)t.Row[2];
                    if (y.HasValue && limits.MinLimit.HasValue && limits.MinLimit.Value > y.Value) t.Row[tagPosition] = MarkColor;
                    if (y.HasValue && limits.MaxLimit.HasValue && limits.MaxLimit.Value < y.Value) t.Row[tagPosition] = MarkColor;
                }
                break;
        }
    }
}