﻿using System;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

namespace AQReportingLibrary
{
    public static class RangesHelper
    {
        public static void AppendRanges(ChartDescription cd, bool isVertical, SLDataTable chartRangesTable, bool isCc, bool ignoreAxis, ReportChartRangeStyle rcrs, bool editable)
        {
            var c = 0.1;
            if (chartRangesTable == null || chartRangesTable.ColumnNames == null ||
                chartRangesTable.ColumnNames.Count == 0) return;
            var n = (double) chartRangesTable.ColumnNames.Where(a=>!a.StartsWith("#")).Count();
            var filled = (rcrs & ReportChartRangeStyle.Filled) == ReportChartRangeStyle.Filled;
            var labeled = (rcrs & ReportChartRangeStyle.Labeled) == ReportChartRangeStyle.Labeled;
            var postfix = (isVertical ? " " : string.Empty);
            for (int index = 0; index < chartRangesTable.ColumnNames.Count; index++)
            {
                var l = chartRangesTable.ColumnNames[index];
                if (l.StartsWith("#"))continue;
                var colorIndex = chartRangesTable.ColumnNames.IndexOf("#"+l+"_Цвет");
                var labelIndex = chartRangesTable.ColumnNames.IndexOf("#" + l + "_Метка");
                if (chartRangesTable.GetColumnData(l).Any(a => a != null))
                {
                    var csd3 = new ChartSeriesDescription { SourceTableName = chartRangesTable.TableName };
                    var invert = !ignoreAxis && (chartRangesTable.ColumnTags != null
                        && chartRangesTable.ColumnTags.Count > 0
                        && chartRangesTable.ColumnTags[index] != null
                        && chartRangesTable.ColumnTags[index].ToString() == "X");

                    if (isVertical ^ invert)
                    {
                        csd3.SeriesType = ChartSeriesType.ConstantX;
                        csd3.XColumnName = l;
                        csd3.SeriesTitle = filled || labeled ? "#" + l + postfix : l + postfix;
                    }
                    else
                    {
                        csd3.SeriesType = ChartSeriesType.ConstantY;
                        csd3.YColumnName = l;
                        csd3.SeriesTitle = filled || labeled ? "# " + l + postfix : l + postfix;
                    }
                    csd3.Options = labeled ? "Labeled" : null;
                    if (editable) csd3.Options += "Editable";
                    csd3.LineColor = colorIndex>=0 && chartRangesTable.Table[0].Row[colorIndex]!= null
                        ? chartRangesTable.Table[0].Row[colorIndex].ToString()
                        : ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.32 + 0.69 * Math.Floor(c) / n);
                        
                    csd3.LineSize = ((rcrs & ReportChartRangeStyle.Invisible) == ReportChartRangeStyle.Invisible)
                        ? ((rcrs & ReportChartRangeStyle.Filled) == ReportChartRangeStyle.Filled) ? 0 : 0.3
                        : (!isCc || c < 2)
                            ? ((rcrs & ReportChartRangeStyle.Thin) == ReportChartRangeStyle.Thin)
                                ? 1
                                : 2
                            : 0.5;
                    if (labelIndex >= 0) csd3.LabelColumnName = chartRangesTable.ColumnNames[labelIndex];
                    cd.ChartSeries.Insert(0, csd3);
                }
                c += 1;
            }
            c = 0.1;
            if (filled)
            {
                for (var index = 0; index < chartRangesTable.ColumnNames.Count; index += 1)
                {
                    var from = chartRangesTable.ColumnNames[index];
                    if (from.StartsWith("#")) continue;
                    var colorIndex = chartRangesTable.ColumnNames.IndexOf("#" + from + "_Цвет");
                    if (chartRangesTable.GetColumnData(from).Any(a => a != null))
                    {
                        var csd3 = new ChartSeriesDescription { SourceTableName = chartRangesTable.TableName };
                        var invert = !ignoreAxis && (chartRangesTable.ColumnTags != null
                            && chartRangesTable.ColumnTags.Count > 0
                            && chartRangesTable.ColumnTags[index] != null
                            && chartRangesTable.ColumnTags[index].ToString() == "X");

                        if (isVertical ^ invert)
                        {
                            csd3.SeriesType = ChartSeriesType.RectangleX;
                            csd3.XColumnName = from;
                            csd3.SeriesTitle = labeled ? "# " + from + postfix : from + postfix;
                        }
                        else
                        {
                            csd3.SeriesType = ChartSeriesType.RectangleY;
                            csd3.YColumnName = from;
                            csd3.SeriesTitle = labeled ? "#" + from + postfix : from + postfix;
                        }
                        csd3.Options = labeled ? "Labeled" : null;
                        if (editable) csd3.Options += "Editable";
                        csd3.LineColor = colorIndex >= 0 && chartRangesTable.Table[0].Row[colorIndex]!=null
                        ? chartRangesTable.Table[0].Row[colorIndex].ToString()
                        : ColorConvertor.GetColorStringForScale(ColorScales.HSV, 0.32 + 0.69 * Math.Floor(c) / n);
                        csd3.LineSize = ((rcrs & ReportChartRangeStyle.Invisible) == ReportChartRangeStyle.Invisible)
                        ? ((rcrs & ReportChartRangeStyle.Filled) == ReportChartRangeStyle.Filled) ? 0 : 0.3
                        : (!isCc || c < 2)
                            ? ((rcrs & ReportChartRangeStyle.Thin) == ReportChartRangeStyle.Thin)
                                ? 1
                                : 2
                            : 0.5;
                        cd.ChartSeries.Insert(0, csd3);
                    }
                    c += 1;
                }
            }
        }

        public static void AppendErrorBar(SLDataTable chartRangesTable, ErrorBarInfo e, ReportChartRangeStyle style)
        {
            if (chartRangesTable.Table.Count < 2 || ((style& ReportChartRangeStyle.ErrorBar) ==0 && (style & ReportChartRangeStyle.LayeredErrorBar) == 0 && (style & ReportChartRangeStyle.CustomerRisk) == 0)) return;
            var quantile = AQMath.StudentTbyP(0.05, e.n);
            if (chartRangesTable?.Table == null || chartRangesTable.Table.Count == 0) return;

            var min = (double?)chartRangesTable.Table[0].Row[0];
            var max = (double?)chartRangesTable.Table[1].Row[0];
            var minWithError = (min + e.SE * quantile); //Приемочное число по нижней границе
            var maxWithError = (max - e.SE * quantile); //Приемочное число по верхней границе
            var name = e.Layer == null ? string.Empty : e.Layer + ": ";
            var setext = AQMath.SmartRound(e.SE, 5);
            var qtext = AQMath.SmartRound(quantile, 3);
            var comment = $"\r\n±{qtext}·SE, SE={setext}";
            name += AQMath.SmartRound(minWithError,5) + ".." + AQMath.SmartRound(maxWithError,5) + comment;
            if ((style& ReportChartRangeStyle.CustomerRisk) == ReportChartRangeStyle.CustomerRisk)
            {
                var rpleft = AQMath.CustomerRiskForRegressionLeft(e.StDevOdResiduals, e.MeanOfResult, e.StDevOfResult, e.Mean, e.StDev, minWithError);
                var rpright = AQMath.CustomerRiskForRegressionRight(e.StDevOdResiduals, e.MeanOfResult, e.StDevOfResult, e.Mean, e.StDev, maxWithError);
                var rp = rpleft?? 0 + rpright ?? 0;
                name += $", Rc={AQMath.Round(rp, 4.0f)}";
            }
            if (maxWithError < minWithError) name += "\r\nперекрытие";
            chartRangesTable.ColumnNames.Add(name);
            chartRangesTable.ColumnTags.Add(chartRangesTable.ColumnTags[0]);
            chartRangesTable.DataTypes.Add("Double");
            chartRangesTable.Table[0].Row.Add(minWithError);
            chartRangesTable.Table[1].Row.Add(maxWithError);
            chartRangesTable.ColumnNames.Add("#" + name + "_Метка");
            chartRangesTable.DataTypes.Add("String");
            chartRangesTable.ColumnTags.Add("X");
            Limits.ProcessLabel(chartRangesTable, minWithError, maxWithError, true, style, e.Ldr);
        }

        public static string GetDescription(ReportChartRangeType r)
        {
            switch (r)
            {
                case ReportChartRangeType.RangeIsNone:
                    return String.Empty;
                case ReportChartRangeType.RangeIsAB:
                    return ", Границы указаны вручную";
                case ReportChartRangeType.RangeIsNTD:
                    return ", Границы - нормы";
                case ReportChartRangeType.RangeIsSlide:
                    return ", Границы - контрольные уровни";
                case ReportChartRangeType.RangeIsSigmas:
                    return ", Границы: Среднее ± 3∙СКО";
                case ReportChartRangeType.RangeIsR:
                    return ", Границы: Среднее ± 2.66∙R";
                default:
                    return String.Empty;
            }
        }
    }
}
