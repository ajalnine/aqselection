﻿namespace AQReportingLibrary
{
    public interface IFilterInterface
    {
         event FilterChangedDelegate FilterChanged;
    }

    public enum FilterResult { Cancelled, Changed, Unfilter }

    public delegate void FilterChangedDelegate(object o, FilterChangedEventArgs e);

    public class FilterChangedEventArgs
    {
        public FilterResult Result;
    }

}
