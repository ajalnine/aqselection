﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class Limits
    {
        public double CurrentAverage = double.NaN;
        public int LimitCount;
        public double? MaxLimit = null;
        public double? MinLimit = null;
        public bool LimitForX = false;
        private readonly ReportChartCardType _cardType;
        private readonly ReportChartType _chartType;
        private bool _rotatedView;
        private SLDataTable _chartRangesTable;
        private readonly List<DynamicRow> _ldr;
        private readonly SLDataTable _externalLimitsTable;
        public SLDataTable UserLimitsTable;
        private readonly string _parameterName;
        private readonly List<object> _data;
        private readonly double _n;
        private readonly ReportChartRangeStyle _rangeStyle;
        
    
        public Limits(List<DynamicRow> ldr, ReportChartRangeType chartRangeType, ReportChartCardType cardType,
                      ReportChartType chartType, SLDataTable userLimits, SLDataTable externalLimitsTable, string parameterName, bool rotatedView, ReportChartRangeStyle rangeStyle)
        {
            var limitsCalculators = new Dictionary<ReportChartRangeType, Action>
            {
                { ReportChartRangeType.RangeIsNone,     ConstructRangeIsNone    },
                { ReportChartRangeType.RangeIsAB,       ConstructRangeIsAB      },
                { ReportChartRangeType.RangeIsNTD,      ConstructRangeIsNTD     },
                { ReportChartRangeType.RangeIsSigmas,   ConstructRangeIsSigmas  },
                { ReportChartRangeType.RangeIsR,        ConstructRangeIsR       },
                { ReportChartRangeType.RangeIsSlide,    ConstructRangeIsCard    }
            };

            _rangeStyle = rangeStyle;
            _ldr = ldr;
            _n = _ldr?.Count ?? 0;
            _data = _ldr?.Select(a => a.Y).Cast<object>().ToList();
            _cardType = cardType;
            _chartType = chartType;
            _rotatedView = rotatedView;
            UserLimitsTable = userLimits;
            _externalLimitsTable = externalLimitsTable;
            _parameterName = parameterName;

            _chartRangesTable = new SLDataTable
            {
                TableName = "Пределы",
                Table = new List<SLDataRow>()
            };
            
            CurrentAverage = double.NaN;
            LimitCount = 0;
            limitsCalculators[chartRangeType].Invoke();
        }

        private void ConstructRangeIsNTD()
        {
            if (_externalLimitsTable == null) ConstructRangeIsNorm();
            else ConstructRangeIsNormFromExternalTable();
        }

        public SLDataTable GetRangesYTable()
        {
            return _chartRangesTable;
        }
        
        private void ConstructRangeIsCard()
        {
            if (_cardType == ReportChartCardType.CardTypeIsX || _chartType != ReportChartType.Card) ConstructRangeIsXCard(true);
            else ConstructRangeIsRCard(true);
        }

        private void ConstructRangeIsR()
        {
            if (_cardType == ReportChartCardType.CardTypeIsX || _chartType != ReportChartType.Card) ConstructRangeIsXCard(false);
            else ConstructRangeIsRCard(false);
        }

        private void ConstructRangeIsAB()
        {
            if (UserLimitsTable != null && UserLimitsTable.Table != null)
            {
                var allLimitsNull = true;
                foreach (var row in UserLimitsTable.Table)
                {
                    if (row.Row.Any(value => value != null))
                    {
                        allLimitsNull = false;
                    }
                    if (!allLimitsNull) break;
                }
                if (allLimitsNull) _chartRangesTable.Table = new List<SLDataRow>();
                else
                {
                    LimitCount = UserLimitsTable.Table[0].Row.Count;
                    _chartRangesTable = UserLimitsTable.Clone();
                    MinLimit = _chartRangesTable.Table[0].Row[0] as Double?;
                    MaxLimit = _chartRangesTable.Table[1].Row[0] as Double?;
                    if ((_rangeStyle & (ReportChartRangeStyle.OutagesCount | ReportChartRangeStyle.OutagesPercent | ReportChartRangeStyle.OutagesProbability)) > 0)
                    {
                        var n = _chartRangesTable.ColumnNames.Count;
                        for (int i=0; i< n; i++)
                        {
                            var columnName = _chartRangesTable.ColumnNames[i];
                            if (columnName.StartsWith("#")) continue;
                            var min = _chartRangesTable.Table[0].Row[i] as Double?;
                            var max = _chartRangesTable.Table[1].Row[i] as Double?;
                            var lowerLabelText = string.Empty;
                            var upperLabelText = string.Empty;
                            _chartRangesTable.ColumnNames.Add("#" + columnName + "_Метка");
                            _chartRangesTable.DataTypes.Add("String");
                            _chartRangesTable.ColumnTags.Add(_chartRangesTable.ColumnTags[i]);
                            ProcessLabel(_chartRangesTable, min, max, _chartRangesTable.ColumnTags[i].ToString() == "X", _rangeStyle, _ldr);
                        }
                    }
                }
            }
            else
            {
                LimitCount = 0;
                _chartRangesTable = new SLDataTable
                {
                    Table = new List<SLDataRow>(),
                    ColumnNames = new List<string>(),
                    DataTypes = new List<string>(),
                    TableName = "Пределы"
                };
                MinLimit = null;
                MaxLimit = null;
            }

        }

        private void ConstructRangeIsSigmas()
        {
            LimitCount = 1;
            _chartRangesTable.DataTypes = (new[] { "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double" }).ToList();

            var s = _rotatedView ?  AQMath.AggregateStDev(_ldr.Select(a => a.X).Cast<object>().ToList())
                                 : AQMath.AggregateStDev(_ldr.Select(a => a.Y).Cast<object>().ToList());
            if (!s.HasValue) return;

            double x2 = _rotatedView ? _ldr.Where(a => a.X != null).Select(a => a.Y.Value).Average()
                                     :_ldr.Where(a=>a.Y != null).Select(a => a.Y.Value).Average();
            double lcl2 =  x2 - s.Value * 3;
            double ucl2 =  x2 + s.Value * 3;
            double lcl2B = x2 - s.Value * 2;
            double ucl2B = x2 + s.Value * 2;
            double lcl2C = x2 - s.Value * 1;
            double ucl2C = x2 + s.Value * 1;
            
            _chartRangesTable.Table.Add(new SLDataRow { Row = new List<object> { lcl2, ucl2, x2, null, lcl2C, ucl2C, lcl2B, ucl2B } });

            _chartRangesTable.ColumnNames =
                (new[]
                    {
                        "LCL=" + Math.Round(lcl2, 4), "UCL=" + Math.Round(ucl2, 4),
                        "Среднее=" + Math.Round(x2, 4), "#", "#LCLC", "#UCLC", "#LCLB", "#UCLB"
                    }).ToList();

            MinLimit = lcl2;
            MaxLimit = ucl2;
            CurrentAverage = x2;
        }

        private void ConstructRangeIsXCard(bool doCycle)
        {
            LimitCount = 1;

            _chartRangesTable.DataTypes =
                (new[] { "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double" }).ToList();

            var sortedYValues = _rotatedView ? _ldr.Where(a => a.X.HasValue).Select(a => a.X.Value).ToList()
                                             : _ldr.Where(a => a.Y.HasValue).Select(a => a.Y.Value).ToList(); ;
            int outliersCount;
            double x, rav;

            do
            {
                rav = GetRangeSigmaEstimation(sortedYValues);
                x = sortedYValues.Average();
                var lcl = x - rav * 2.66;
                var ucl = x + rav * 2.66;
                MinLimit = lcl;
                MaxLimit = ucl;

                var topOutliers = sortedYValues.Where((a => a > ucl)).ToList();
                var bottomOutliers = sortedYValues.Where((a => a < lcl)).ToList();
                outliersCount = topOutliers.Count() + bottomOutliers.Count();
                if (outliersCount <= 0) continue;

                if (topOutliers.Any() && !bottomOutliers.Any()) sortedYValues.Remove(topOutliers.Max());
                else if (bottomOutliers.Any() && !topOutliers.Any()) sortedYValues.Remove(bottomOutliers.Min());
                else
                {
                    var maxOutlier = topOutliers.Any() ? topOutliers.Max() : x;
                    var minOutlier = bottomOutliers.Any() ? bottomOutliers.Min() : x;
                    sortedYValues.Remove(maxOutlier - x > x - minOutlier ? maxOutlier : minOutlier);
                }

            } while (outliersCount > 0 && doCycle);

            var lclc = x - rav * 2.66 /3;
            var uclc = x + rav * 2.66 /3;
            var lclb = x - rav * 2.66 /3 * 2;
            var uclb = x + rav * 2.66 /3 * 2;
            _chartRangesTable.Table.Add(new SLDataRow {Row = new List<object> {MinLimit.Value, MaxLimit.Value, x, null, lclc, uclc, lclb, uclb}});
            _chartRangesTable.ColumnNames =
                (new[]
                {
                    "LCL=" + Math.Round(MinLimit.Value, 4), "UCL=" + Math.Round(MaxLimit.Value, 4),
                    "X=" + Math.Round(x, 4), "#", "#LCLC", "#UCLC", "#LCLB", "#UCLB"
                }).ToList();
            CurrentAverage = x;
        }

        private static double GetRangeSigmaEstimation(List<double> sortedYValues)
        {
            double r = 0;

            for (var i = 0; i < sortedYValues.Count - 1; i++)
                r += Math.Abs(sortedYValues[i + 1] - sortedYValues[i]);

            return r/sortedYValues.Count;
        }

        private void ConstructRangeIsRCard(bool doCycle)
        {
            LimitCount = 1;

            _chartRangesTable.DataTypes = (new[] {"Double"}).ToList();

            var r = GetRangeList();

            int outliersCount;
            
            do
            {
                var ravr = r.Any()? r.Average() : 0;
                var uclr = ravr * 3.27;
                MinLimit = 0;
                MaxLimit = uclr;
            
                var topOutliers = r.Where((a => a > uclr)).ToList();
                outliersCount = topOutliers.Count();
                if (outliersCount > 0) r.Remove(topOutliers.Max());

            } while (outliersCount > 0 && doCycle);

            _chartRangesTable.Table.Add(new SLDataRow { Row = new List<object> { MaxLimit.Value } });
            _chartRangesTable.ColumnNames = (new[] { "UCLR=" + Math.Round(MaxLimit.Value, 4) }).ToList();
        }

        private List<double> GetRangeList()
        {
            var sortedYValues = _rotatedView ? _ldr.Where(a => a.X.HasValue).Select(a => a.X.Value).ToList()
                                             : _ldr.Where(a => a.Y.HasValue).Select(a => a.Y.Value).ToList(); ;

            var r = new List<double>();
            for (var i = 0; i < sortedYValues.Count - 1; i++) r.Add(Math.Abs(sortedYValues[i + 1] - sortedYValues[i]));
            return r;
        }

        private void ConstructRangeIsNormFromExternalTable()
        {
            _chartRangesTable.DataTypes = (new[] {"Double", "Double"}).ToList();

            var minLimits = _externalLimitsTable.GetColumnData(_parameterName + "_НГ");
            var maxLimits = _externalLimitsTable.GetColumnData(_parameterName + "_ВГ");
            
            MinLimit = (minLimits != null && minLimits.Any()) ? (double?)minLimits.Min() : null;
            MaxLimit = (maxLimits != null && maxLimits.Any()) ? (double?)maxLimits.Max() : null;

            if (minLimits != null)
                foreach (var sldr in minLimits.Select(l => new SLDataRow {Row = new List<object> {(double) l, null}}))_chartRangesTable.Table.Add(sldr);

            if (maxLimits != null)
                foreach (var sldr in maxLimits.Select(l => new SLDataRow {Row = new List<object> {null, (double) l}}))_chartRangesTable.Table.Add(sldr);
            

            var minLimitCount = (minLimits != null && minLimits.Any()) ? minLimits.Count() : 0;
            var maxLimitCount = (maxLimits != null && maxLimits.Any()) ? maxLimits.Count() : 0;
            
            LimitCount = Math.Max(minLimitCount, maxLimitCount);

            if (LimitCount==1)
            {
                _chartRangesTable.ColumnNames =
                    (new[]
                        {
                            "НГ" + ((MinLimit.HasValue) ? ("=" + Math.Round(MinLimit.Value, 4)) : String.Empty),
                            "ВГ" + ((MaxLimit.HasValue) ? ("=" + Math.Round(MaxLimit.Value, 4)) : String.Empty)
                        }).ToList();
            }
            else
            {
                _chartRangesTable.ColumnNames = (new[] {"НГ", "ВГ"}).ToList();
            }
        }

        private void ConstructRangeIsNorm()
        {
            var limitsY = (from r in _ldr select new { MinAllowed = r.MinallowedY, MaxAllowed = r.MaxallowedY }).Distinct()
                .Where(b => !(!b.MaxAllowed.HasValue && !b.MinAllowed.HasValue)).ToList();
            var limitsX = (from r in _ldr select new  { MinAllowed = r.MinallowedX, MaxAllowed = r.MaxallowedX }).Distinct()
                .Where(b => !(!b.MaxAllowed.HasValue && !b.MinAllowed.HasValue)).ToList();
            
            var limitsPairsY =limitsY.Select(a => new LimitPair { MinAllowed = a.MinAllowed, MaxAllowed = a.MaxAllowed, Axis = "Y"}).ToList();
            var limitsPairsX = limitsX.Select(a => new LimitPair { MinAllowed = a.MinAllowed, MaxAllowed = a.MaxAllowed, Axis = "X"}).ToList();
            MinLimit = (limitsPairsY.Any()) ? limitsPairsY.Select(a => a.MinAllowed).Min() : null;
            MaxLimit = (limitsPairsY.Any()) ? limitsPairsY.Select(a => a.MaxAllowed).Max() : null;

            var limitsPairs = limitsPairsY.Union(limitsPairsX).ToList();

            _chartRangesTable = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                DataTypes = new List<string>(),
                TableName = "Границы"
            };
            if (limitsPairs.Any())
            {
                _chartRangesTable.Table.Add(new SLDataRow { Row = new List<object>()});
                _chartRangesTable.Table.Add(new SLDataRow { Row = new List<object>() });
            }
            LimitCount = 0;
            foreach (var l in limitsPairs)
            {
                _chartRangesTable.Table[0].Row.Add((l.MinAllowed.HasValue && Math.Abs(l.MinAllowed.Value) > double.Epsilon) ? l.MinAllowed : null);
                _chartRangesTable.Table[1].Row.Add(l.MaxAllowed);
                var columnName = l.MinAllowed.HasValue ? Math.Round(l.MinAllowed.Value, 4).ToString(CultureInfo.InvariantCulture) : String.Empty;
                 
                columnName += l.MaxAllowed.HasValue 
                    ? (l.MinAllowed.HasValue ? ".." : string.Empty)+ Math.Round(l.MaxAllowed.Value, 4).ToString(CultureInfo.InvariantCulture) 
                    : String.Empty;
                _chartRangesTable.ColumnNames.Add(columnName);
                _chartRangesTable.DataTypes.Add("Double");
                _chartRangesTable.ColumnTags.Add(l.Axis);

                if ((_rangeStyle & (ReportChartRangeStyle.OutagesCount | ReportChartRangeStyle.OutagesPercent | ReportChartRangeStyle.OutagesProbability)) > 0)
                {
                    _chartRangesTable.ColumnNames.Add("#" + columnName + "_Метка");
                    _chartRangesTable.DataTypes.Add("String");
                    _chartRangesTable.ColumnTags.Add(l.Axis);

                    var min = l.MinAllowed;
                    var max = l.MaxAllowed;
                    ProcessLabel(_chartRangesTable, min, max, l.Axis == "X", _rangeStyle, _ldr);
                }

                LimitCount++;
            }
        }

        public static void ProcessLabel(SLDataTable chartRangesTable,  double? min, double? max, bool limitForX, ReportChartRangeStyle rangeStyle, List<DynamicRow> ldr)
        {
            var lowerLabelText = string.Empty;
            var upperLabelText = string.Empty;

            if ((rangeStyle & (ReportChartRangeStyle.OutagesCount | ReportChartRangeStyle.OutagesPercent)) > 0)
            {
                double total = limitForX
                ? AQMath.AggregateCount(ldr.Select(a => (object)a.X).ToList())
                : AQMath.AggregateCount(ldr.Select(a => (object)a.Y).ToList());
                double lower = limitForX
                    ? AQMath.AggregateCount(ldr.Where(a => a.X < min).Select(a => (object)a.X).ToList())
                    : AQMath.AggregateCount(ldr.Where(a => a.Y < min).Select(a => (object)a.Y).ToList());
                double upper = limitForX
                    ? AQMath.AggregateCount(ldr.Where(a => a.X > max).Select(a => (object)a.X).ToList())
                    : AQMath.AggregateCount(ldr.Where(a => a.Y > max).Select(a => (object)a.Y).ToList());
                if ((rangeStyle & (ReportChartRangeStyle.OutagesPercent)) > 0)
                {
                    lowerLabelText += $"{AQMath.Round(lower / total * 100, 2d)}%";
                    upperLabelText += $"{AQMath.Round(upper / total * 100, 2d)}%";
                }
                if ((rangeStyle & (ReportChartRangeStyle.OutagesCount)) > 0)
                {
                    if (!string.IsNullOrEmpty(lowerLabelText)) lowerLabelText += "\r\n";
                    if (!string.IsNullOrEmpty(upperLabelText)) upperLabelText += "\r\n";
                    lowerLabelText += $"n={lower}";
                    upperLabelText += $"n={upper}";
                }
            }
            if ((rangeStyle & (ReportChartRangeStyle.OutagesProbability)) > 0)
            {
                if (!string.IsNullOrEmpty(lowerLabelText)) lowerLabelText += "\r\n";
                if (!string.IsNullOrEmpty(upperLabelText)) upperLabelText += "\r\n";
                var data = limitForX
                ? ldr.Select(a => (object)a.X).ToList()
                : ldr.Select(a => (object)a.Y).ToList();
                double? avg = AQMath.AggregateMean(data);
                double? stdev = AQMath.AggregateStDev(data);
                double? pLower = AQMath.NormalCD(min, avg, stdev);
                double? pUpper = 1 - AQMath.NormalCD(max, avg, stdev);

                if (pLower.HasValue && !double.IsNaN(pLower.Value)) lowerLabelText += $"p={AQMath.Round(pLower, 3d)}";
                if (pUpper.HasValue && !double.IsNaN(pUpper.Value)) upperLabelText += $"p={AQMath.Round(pUpper, 3d)}";
            }
            chartRangesTable.Table[0].Row.Add(string.IsNullOrEmpty(lowerLabelText) ? null : "<" + lowerLabelText);
            chartRangesTable.Table[1].Row.Add(string.IsNullOrEmpty(upperLabelText) ? null : ">" + upperLabelText);
        }

        private void ConstructRangeIsNone()
        {
            _chartRangesTable.ColumnNames = new List<string>();
            _chartRangesTable.DataTypes   = new List<string>();
            MinLimit = null;
            MaxLimit = null;
            LimitCount = 0;
        }

        public static SLDataTable GetExternalLimitsTable(IEnumerable<SLDataTable> dataTables, SLDataTable selectedTable)
        {
           return  dataTables.SingleOrDefault(a => a.TableName == "#" + selectedTable.TableName + "_Границы");
        }

        public class LimitPair
        {
            public double? MinAllowed;
            public double? MaxAllowed;
            public string Axis;
        }
    }
}