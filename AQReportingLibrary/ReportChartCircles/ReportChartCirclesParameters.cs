﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportChartCirclesParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string ColorField { get; set; }

        public string AggregateDescription { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }
        public ReportChartCircleDiagramType CirclesType { get; set; }
        public ReportChartCircleLabelsType LabelsType { get; set; }
        public ArrangementMode ArrangementMode { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public ReportChartHistogramType HistogramType { get; set; }
        public Aggregate Aggregate { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }

        public ChartLegend ChartLegend { get; set; }
    }
}