﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;
using System.Windows.Media;

namespace AQReportingLibrary
{
    public partial class ReportChartCircles
    {
        private BackgroundWorker _worker;
        private Thread _workerThread;
        Panel _surface;
        private List<object> _othersLayer = new List<object>();
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            var table = _data.GetDataTable();
            if (table == null || table.Table.Count == 0) return false;

            if (table.Table.Count > 5000)
            {
                _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
                _worker.RunWorkerCompleted += (o, e) =>
                {
                    _surface.Dispatcher.BeginInvoke(() =>
                    {
                        CreateCharts();
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable } });
                    });
                };

                _worker.DoWork += (o, e) =>
                {
                    PrepareChartData();
                };
                SetMessage("Построение графика...");
                _worker.RunWorkerAsync();
            }
            else
            {
                PrepareChartData();
                CreateCharts();
                InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable } });
            }
            return true;
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 30,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }

        public void RefreshVisuals()
        {
            if (_data.GetDataTable() == null || _data.GetDataTable().Table.Count == 0 || _columnsTable == null || _columnsTable.Table.Count == 0) return;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable } });
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields[0];
            var fi = ((new[] { Aggregate.N, Aggregate.Percent, Aggregate.PercentInCategory }).Contains(_p.Aggregate))
                ? new FieldsInfo { ColumnGroup = _p.Fields[0], ColumnY = _p.Fields[0] == "Нет" ? _p.ColorField : _p.Fields[0], ColumnZ = _p.ColorField, ColumnV = _p.Fields.Count > 2 ? _p.Fields[2] : null }
                : new FieldsInfo
                {
                    ColumnGroup = _p.Fields[0],
                    ColumnY = _p.Fields.Count > 1
                        ? _p.Fields[1]
                        : _p.Fields[0] == "Нет" ? _p.ColorField : _p.Fields[0],
                    ColumnZ = _p.ColorField,
                    ColumnV = _p.Fields.Count > 2 ? _p.Fields[2] : null
                };
            _originalLayersType = _selectedTable.GetDataType(fi.ColumnZ);
            _originalXType = _selectedTable.GetDataType(fi.ColumnGroup);
            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = fi.ColumnZ,
                    X = _p.Fields[0],
                    ValuesForAggregation = fi.ColumnY,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation,
                    Method = _p.Aggregate,
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
        }

        private void PrepareChartData()
        {
            _columns = new Circles(_p, _data, false);
            _columnsTable = _columns.GetColumnsTable();
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (var cd in GetDescriptionForCircles(_data, _p))
            {
                var chartColumns = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = false,
                    ShowHorizontalGridlines = false,
                    ShowHorizontalAxises = false,
                    ShowVerticalAxises = false,
                    ShowHorizontalZero = false,
                    VerticalAxisSymmetry = false,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    EnableCellCondense = true,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    HideMinorGridLines = true,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable> { _interactiveTable, _columnsTable},
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                           new FixedAxis{},
                           new FixedAxis { DataType = _originalXType, Axis = "X", Format = "dd.MM.yyyy", StringIsDateTime = _originalXType?.ToLower().Contains("datetime") ?? false},
                           new FixedAxis { DataType = _originalLayersType, Axis = "Z", AxisContinuous = true, AxisDiscrete = false, Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false}
                        },
                        Editable = false
                    }
                };
                chartColumns.ToolTipRequired += ChartColumns_ToolTipRequired;
                chartColumns.SelectionChanged += ChartColumns_SelectionChanged;
                chartColumns.InteractiveRename += chartColumns_InteractiveRename;
                chartColumns.AxisClicked += ChartColumns_AxisClicked;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartColumns });
                _surface.Children.Add(chartColumns);
                chartColumns.VisualReady += ChartColumns_VisualReady;
                chartColumns.MakeChart();
            }
        }

        private void ChartColumns_VisualReady(object o, AQBasicControlsLibrary.VisualReadyEventArgs e)
        {
            Finished?.Invoke(this,new ReportChartFinishedEventArgs {InnerTables = new List<SLDataTable>() { _columnsTable } });
        }
    }
}