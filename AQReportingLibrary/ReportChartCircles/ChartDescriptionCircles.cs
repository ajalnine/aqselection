﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCircles
    {
        private IEnumerable<ChartDescription> GetDescriptionForCircles(Data d, ReportChartCirclesParameters p)
        {
            var seriesType = new Dictionary<ReportChartCircleDiagramType, ChartSeriesType>
            {
                {ReportChartCircleDiagramType.Circles, ChartSeriesType.Circles},
                {ReportChartCircleDiagramType.Rings, ChartSeriesType.Circles},
                {ReportChartCircleDiagramType.Radar, ChartSeriesType.Radar},
                {ReportChartCircleDiagramType.Sun, ChartSeriesType.Sun},
                {ReportChartCircleDiagramType.Angles, ChartSeriesType.Segments},
            };

            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();

            string xName = p.Fields[0];
            string yName = p.Fields.Count > 1 && p.Aggregate != Aggregate.N && p.Aggregate != Aggregate.Percent
                ? p.Fields[1]
                : string.Empty;
            var yaxisTitle = p.AggregateDescription + (!String.IsNullOrWhiteSpace(yName) ? " " + yName : string.Empty);
            result.Add(cd);
           cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = (p.ColorField != "Нет") ? _p.ChartLegend : ChartLegend.None;
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Диаграмма",
                XColumnName = xName,
                XIsNominal = false,
                XAxisTitle = xName,
                WColumnName = "Ширина_",
                VColumnName = "Отступ_",
                ZColumnName = (p.ColorField != "Нет")
                    ? p.ColorField == _p.Fields[0]
                        ? p.ColorField + " "
                        : p.ColorField
                    : null,
                MarkerSizeColumnName =
                    ((_p.LabelsType & ReportChartCircleLabelsType.Scaled) == ReportChartCircleLabelsType.Scaled)
                        ? "Ширина_"
                        : null,
                LabelColumnName = _p.LabelsType != ReportChartCircleLabelsType.None ? "Подписи" : null,
                SeriesTitle = p.AggregateDescription,
                SeriesType = seriesType[p.CirclesType],
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                ColorColumnName = "Цвет",
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                LineSize = 0.5,
                YAxisTitle = yaxisTitle,
                ZValueSeriesTitle = p.AggregateDescription,
                ArrangementMode = p.ArrangementMode,
                MarkerSize = p.CirclesType == ReportChartCircleDiagramType.Rings ? 0.66 : 0,
            };
            cd.ChartSeries.Add(csd);
            return result;
        }
    }
}