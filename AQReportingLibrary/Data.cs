﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public class Data
    {
        private SLDataTable _chartDataTable;
        private readonly SLDataTable _selectedTable;
        private readonly FieldsInfo _fi;
        private bool _groupHasTime = true;
        private bool _groupIsDate;
        private List<DynamicRow> _ldr;

        public static Dictionary<string, List<DynamicRow>> LDRCache = new Dictionary<string, List<DynamicRow>>();
        #region Заполнение данных для графика
        
        // Сделать Extention для фильтрации SLDataTable.Table - Filter.GetFilteredTable(SLDataTable.Table)
        // Добавить к Data альтернативный конструктор с фильтрами датасета
        // В LDR пропускать строки через фильтр вызовом метода фильтра с индексом Filter.CheckRow(i)

        public Data(SLDataTable selectedTable, FieldsInfo fi)
        {
            _selectedTable = selectedTable;
            _fi = fi;
            FillLDR(false, null);
            ConvertLDRtoSLDataTable();
        }

        public Data(SLDataTable selectedTable, FieldsInfo fi, bool includeEmptyY)
        {
            _selectedTable = selectedTable;
            _fi = fi;
            FillLDR(includeEmptyY, null);
            ConvertLDRtoSLDataTable();
        }

        public Data(SLDataTable selectedTable, FieldsInfo fi, List<string> nonEmptyColumns)
        {
            _selectedTable = selectedTable;
            _fi = fi;
            FillLDR(false, nonEmptyColumns);
            ConvertLDRtoSLDataTable();
        }

        private void ConvertLDRtoSLDataTable()
        {
            var zDataType = _selectedTable.GetDataType(_fi.ColumnZ);
            var wDataType = _selectedTable.GetDataType(_fi.ColumnW);
            var vDataType = _selectedTable.GetDataType(_fi.ColumnV);
            var dateIndex = _fi.ColumnDate == null
                ? DataHelper.GetFirstDateIndex(_selectedTable)
                : _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnDate));
            var currentDateField = (dateIndex >= 0) ? _selectedTable.ColumnNames[dateIndex] : null;

            var sldt = new SLDataTable
            {
                ColumnNames =
                    (new[]
                    {
                        currentDateField, _fi.ColumnX, _fi.ColumnY, "R", "SourceIndex", "Tag", "Наблюдение", "#Группа#", _fi.ColumnZ, _fi.ColumnW, _fi.ColumnV, "Маркер", "Метка", "Опция"
                    }).ToList(),
                DataTypes =
                    (new[]
                    {
                        "DateTime", "Double", "Double", "Double", "Int32", "String", "Double", "String", zDataType, wDataType, vDataType, "Double", "String", "Double"
                    }).ToList(),
                TableName = "Данные",
                Table = new List<SLDataRow>()
            };

            if (_ldr == null) return;
            CheckGroupIsDate(_selectedTable);

            for (var i = 0; i < _ldr.Count; i++)
            {
                sldt.Table.Add(new SLDataRow
                {
                    Row = new List<object>
                    {
                        _ldr[i].Xdate,
                        _ldr[i].X,
                        _ldr[i].Y,
                        GetRange(i),
                        _ldr[i].Index,
                        GetItemColor(i),
                        i + 1.0d,
                        GetGroup(i),
                        _ldr[i].Z,
                        _ldr[i].W,
                        _ldr[i].V,
                        _ldr[i].Markersize,
                        _ldr[i].Label,
                        0.0d
                    }
                });
            }
            _chartDataTable = sldt;
        }

        private void CheckGroupIsDate(SLDataTable selectedTable)
        {
            if (string.IsNullOrWhiteSpace(_fi.ColumnGroup) || _fi.ColumnGroup == "Нет") return;

            var dt = selectedTable.DataTypes[selectedTable.ColumnNames.IndexOf(_fi.ColumnGroup)];
            _groupIsDate = dt == "System.DateTime" || dt == "DateTime";
            _groupHasTime = false;
            if (!_groupIsDate) return;
            _groupHasTime = _ldr.Where(l => l.Group is DateTime).Any(a => ((DateTime)a.Group).TimeOfDay.Ticks != 0);
        }

        private string GetItemColor(int i)
        {
            return _ldr[i].Color != null && _ldr[i].Color.StartsWith("#") ? _ldr[i].Color : null;
        }

        private double? GetRange(int i)
        {
            if (i >= _ldr.Count || i==0) return null;
            var d1 = _ldr[i - 1].Y;
            var d2 = _ldr[i].Y;
            if (!d1.HasValue || !d2.HasValue)return null;
            return Math.Abs(d1.Value - d2.Value);
        }

        private string GetGroup(int i)
        {
            string group;
            if (!_groupIsDate || _groupHasTime) group = (_ldr[i].Group != null) ? _ldr[i].Group.ToString() : null;
            else group = (_ldr[i].Group is DateTime) ? ((DateTime) _ldr[i].Group).ToString("dd.MM.yyyy") : null;
            return group;
        }

        public SLDataTable GetDataTable()
        {
            return _chartDataTable;
        }

        public List<DynamicRow> GetLDR()
        {
            return _ldr;
        }
        
        public List<DynamicRow> GetLDRForLayer(object layer)
        {
            return _ldr.Where(a => layer != null && a.Z != null ? a.Z.ToString() == layer.ToString() : a.Z == null).ToList();
        }

        public List<object> GetLayers()
        {
            return _ldr.Select(a => a.Z).Distinct().ToList();
        }

        public FieldsInfo GetFieldsInfo()
        {
            return _fi;
        }

        private void FillLDR(bool includeEmptyY, List<string> nonEmptyColumns)
        {
            if (_selectedTable == null) return;
            var dateIndex = _fi.ColumnDate == null || _fi.ColumnDate == "Нет" ? -1 : _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnDate));
            var groupIndex = (_fi.ColumnGroup == null) ? -1 : _selectedTable.ColumnNames.IndexOf(_fi.ColumnGroup);
            var zIndex = (_fi.ColumnZ == null || _fi.ColumnZ == "Нет") ? -1 : _selectedTable.ColumnNames.IndexOf(_fi.ColumnZ);
            var wIndex = (_fi.ColumnW == null || _fi.ColumnW == "Нет") ? -1 : _selectedTable.ColumnNames.IndexOf(_fi.ColumnW);
            var vIndex = (_fi.ColumnV == null || _fi.ColumnV == "Нет") ? -1 : _selectedTable.ColumnNames.IndexOf(_fi.ColumnV);
            var columnYIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY));
            var columnXIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnX));
            var colorColumnYIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY + "_Цвет" || a == "#" + _fi.ColumnY + "_Цвет"));
            var colorColumnXIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnX + "_Цвет" || a == "#" + _fi.ColumnX + "_Цвет"));
            var markerSizeColumnIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY + "_Размер" || a == "#" + _fi.ColumnY + "_Размер"));
            var labelColumnIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY + "_Метка" || a == "#" + _fi.ColumnY + "_Метка"));
            var minColumnYIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY + "_НГ" || a == "#" + _fi.ColumnY + "_НГ"));
            var maxColumnYIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnY + "_ВГ" || a == "#" + _fi.ColumnY + "_ВГ"));
            var minColumnXIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnX + "_НГ" || a == "#" + _fi.ColumnX + "_НГ"));
            var maxColumnXIndex = _selectedTable.ColumnNames.IndexOf(_selectedTable.ColumnNames.FirstOrDefault(a => a == _fi.ColumnX + "_ВГ" || a == "#" + _fi.ColumnX + "_ВГ"));
            
            var key = _selectedTable.GetHashCode().ToString() + _selectedTable.TableName + ";" + dateIndex + ";" + columnXIndex + ";" +
                         columnYIndex + ";" + minColumnYIndex + ";" + maxColumnYIndex + ";" + groupIndex + "; " + zIndex + "; " + wIndex + "; " + vIndex + "; " + colorColumnYIndex + "; " + colorColumnXIndex + "; " + markerSizeColumnIndex + "; " + _selectedTable.Table.Count;

            if (LDRCache.ContainsKey(key) && nonEmptyColumns == null)
            {
                _ldr = LDRCache[key];
                return;
            }

            if (columnYIndex < 0) return;
            var ldr = new List<DynamicRow>();
            int c;
            for (var i = 0; i < _selectedTable.Table.Count; i++)
            {
                if (!includeEmptyY && (_selectedTable.Table[i].Row[columnYIndex] == null || string.IsNullOrEmpty(_selectedTable.Table[i].Row[columnYIndex].ToString()))) continue;

                var rowIsFull = true;
                if (nonEmptyColumns != null)
                {
                    foreach (var sp in nonEmptyColumns)
                    {
                        c = _selectedTable.ColumnNames.IndexOf(sp);
                        if (_selectedTable.Table[i].Row[c] != null && _selectedTable.Table[i].Row[c] != DBNull.Value)continue;
                        rowIsFull = false;
                        break;
                    }
                    if (!rowIsFull) continue;
                }

                var dr = new DynamicRow();

                if (dateIndex >= 0)
                {
                    var o = _selectedTable.Table[i].Row[dateIndex];
                    var datevalue = o is DateTime? ? (DateTime?)o : null;
                    dr.Xdate = datevalue;
                    if (datevalue.HasValue) dr.Xf = datevalue.Value.Ticks/1e18;
                }
                else
                {
                    dr.Xdate = new DateTime(i);
                    dr.Xf = i;
                }

                var y = _selectedTable.Table[i].Row[columnYIndex];
                dr.Y =  y is double ? (double?)_selectedTable.Table[i].Row[columnYIndex] : null;
                dr.Index = i;


                if (columnXIndex >= 0)
                {
                    if (_selectedTable.Table[i].Row[columnXIndex] != null && !string.IsNullOrEmpty(_selectedTable.Table[i].Row[columnXIndex].ToString()))
                    {
                        dr.X = (double?)_selectedTable.Table[i].Row[columnXIndex];
                    }
                    else continue;
                }
                else
                {
                    dr.X = dr.Xf;
                }
                
                if (minColumnYIndex >= 0)
                {
                    double min;
                    var hasmin = double.TryParse(_selectedTable.Table[i].Row[minColumnYIndex]?.ToString(), out min);
                    if (hasmin) dr.MinallowedY = min;
                }


                if (maxColumnYIndex >= 0)
                {
                    double max;
                    var hasmax = double.TryParse(_selectedTable.Table[i].Row[maxColumnYIndex]?.ToString(), out max);
                    if (hasmax) dr.MaxallowedY = max;
                }

                if (minColumnXIndex >= 0)
                {
                    double min;
                    var hasmin = double.TryParse(_selectedTable.Table[i].Row[minColumnXIndex]?.ToString(), out min);
                    if (hasmin) dr.MinallowedX = min;
                }


                if (maxColumnXIndex >= 0)
                {
                    double max;
                    var hasmax = double.TryParse(_selectedTable.Table[i].Row[maxColumnXIndex]?.ToString(), out max);
                    if (hasmax) dr.MaxallowedX = max;
                }

                string color = null;

                if (colorColumnYIndex >= 0) color = (string)_selectedTable.Table[i].Row[colorColumnYIndex];
                if (colorColumnXIndex >= 0 && String.IsNullOrEmpty(color)) color = (string)_selectedTable.Table[i].Row[colorColumnXIndex];
                if (!String.IsNullOrEmpty(color)) dr.Color = color;
                

                if (markerSizeColumnIndex >= 0)
                {
                    var markersize = _selectedTable.Table[i].Row[markerSizeColumnIndex] as double?;
                    if (markersize.HasValue) dr.Markersize = markersize;
                }

                if (labelColumnIndex >= 0)
                {
                    var label = _selectedTable.Table[i].Row[labelColumnIndex]?.ToString();
                    if (!String.IsNullOrEmpty(label)) dr.Label = label;
                }

                dr.Group = (groupIndex >= 0) ? _selectedTable.Table[i].Row[groupIndex] : null;
                dr.Z = (zIndex >= 0) ? _selectedTable.Table[i].Row[zIndex] : null;
                dr.W = (wIndex >= 0) ? _selectedTable.Table[i].Row[wIndex] : null;
                dr.V = (vIndex >= 0) ? _selectedTable.Table[i].Row[vIndex] : null;
                ldr.Add(dr);
            }
            var t = _fi.ColumnDate != null && _fi.ColumnDate != "Нет" ? (from d in ldr orderby d.Xdate select d).ToList() : ldr;

            if (nonEmptyColumns== null && t.Count < 50000) LDRCache.Add(key, t);
            _ldr = t;
        }

        #endregion
    }
}