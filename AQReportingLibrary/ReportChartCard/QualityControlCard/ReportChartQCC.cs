﻿ using System.Collections.Generic;
 using System.Collections.ObjectModel;
 using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        public bool BuildChartForGenericCards(List<SLDataTable> dataTables)
        {
            SetupChartData(dataTables);
            if (_data.GetDataTable() == null || _data.GetDataTable().Table.Count == 0) return false;
            SetupLimits(dataTables);
            AnalizeSpecialPoints();
            CreateCharts();
            return true;
        }

        public void RefreshChartsForGenericCards()
        {
            if ((_interactiveTables!=null && _interactiveTables.Count == 0) && (_interactiveTable == null || _interactiveTable.Table.Count == 0)) return;
            CreateCharts();
        }

        private void AnalizeSpecialPoints()
        {
            if (_p.CardType == ReportChartCardType.CardTypeIsX && _limits.MinLimit.HasValue && _limits.MaxLimit.HasValue &&
                !double.IsNaN(_limits.CurrentAverage))
            {
                CheckSeries.SetTagsForSpecialPoints(_p.Series, _data.GetDataTable(), _limits.CurrentAverage,
                    _limits.MinLimit.Value, _limits.MaxLimit.Value, _p.RangeType);
            }
            else
            {
                CheckSeries.ResetSeries(_data.GetDataTable());
            }
        }

        private void SetupLimits(IEnumerable<SLDataTable> dataTables)
        {
            _limits = new Limits(_data.GetLDR(),
                _p.RangeType,
                _p.CardType,
                ReportChartType.Card,
                _p.UserLimits,
                Limits.GetExternalLimitsTable(dataTables, _selectedTable),
                _selectedParameter, false, _p.RangeStyle);

            _chartRangesYTable = _limits.GetRangesYTable();

            _probabilityComment = string.Empty;
            if (_p.RangeType == ReportChartRangeType.RangeIsAB || _p.RangeType == ReportChartRangeType.RangeIsNTD)
            {
                _probabilityComment = _limits.GetProbabilityComment(true);
                if (UiReflectChanges != null)
                    UiReflectChanges.Invoke(this,
                        new UIReflectChangesEventArgs {UserLimits = _limits.UserLimitsTable});
            }
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields.FirstOrDefault();
            _originalLayersType = _selectedTable.GetDataType(_p.LayerField);
            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.LayerField, 
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            var fi = new FieldsInfo
            {
                ColumnY = _selectedParameter,
                ColumnDate = _p.DateField
            };
            _data = new Data(_selectedTable, fi);

            _interactiveTable = _data.GetDataTable();
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (var cd in GetDescriptionForCard(_data, _p))
            {
                var chartCard = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    EnableCellCondense = true,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalGroupsType, Axis = "X", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalGroupsType?.ToLower().Contains("datetime") ?? false }, new FixedAxis { DataType = _originalLayersType, Axis = "Z", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } } },
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    GlobalFontCoefficient = _p.FontSize,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable> {_interactiveTable, _chartRangesYTable}
                };
                chartCard.ToolTipRequired += ChartCard_ToolTipRequired;
                chartCard.SelectionChanged += ChartCard_SelectionChanged;
                chartCard.AxisClicked += chartCard_AxisClicked;
                chartCard.InteractiveRename +=chartCard_InteractiveRename;
                chartCard.VisualReady += ChartCard_VisualReady;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartCard });
                _surface.Children.Add(chartCard);
                chartCard.MakeChart();
            }
        }
    }
}