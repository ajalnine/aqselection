﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        private IEnumerable<ChartDescription> GetDescriptionForCard(Data d, ReportChartCardParameters p)
        {
            switch (p.CardType)
            {
                case ReportChartCardType.CardTypeIsX:
                    return GetDescriptionForXCard(d, p);
                case ReportChartCardType.CardTypeIsR:
                    return GetDescriptionForRCard(d, p);
                default:
                    return null;
            }
        }

        private IEnumerable<ChartDescription> GetDescriptionForXCard(Data d, ReportChartCardParameters p)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();
            result.Add(cd);

            var currentChartTitle = GetChartTitle();
            
            cd.ChartLegendType = _p.ChartLegend;
            cd.ChartTitle = currentChartTitle;
            cd.ChartSubtitle = "n=" + n.ToString(CultureInfo.InvariantCulture);
            cd.ChartSubtitle += (_chartRangesYTable.Table.Count > 0) ? RangesHelper.GetDescription(p.RangeType) : String.Empty;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();

            cd.ChartSeries = new List<ChartSeriesDescription>
            {
                new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    XColumnName = "Наблюдение",
                    XIsNominal = true,
                    XAxisTitle = "Наблюдение",
                    YColumnName = _selectedParameter,
                    MarkerSizeColumnName = "Маркер",
                    SeriesTitle = _selectedParameter,
                    ColorColumnName = "Tag",
                    LabelColumnName = "Метка",
                    SeriesType = ChartSeriesType.LineXY,
                    YAxisTitle = _selectedParameter,
                    ColorScale = p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                    MarkerMode = p.MarkerMode,
                    MarkerSizeMode = p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    MarkerSize = 6,
                    LineSize = n < 500 ? 0.25 : 0
                }
            };
            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                    ? "Mean"
                    : "Median";
                AppendHelperX(p, cd, "#RangeOnY", ChartSeriesType.RangeOnY, _selectedParameter, options + textenabled);
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                AppendHelperX(p, cd, "#KDEOnY", ChartSeriesType.KDEOnY, _selectedParameter);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                AppendHelperX(p, cd, "#RugOnY", ChartSeriesType.RugOnY, _selectedParameter);
            }

            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;

            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, true, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            return result;
        }

        private void AppendHelperX(ReportChartCardParameters p, ChartDescription cd, string seriesPrefix, ChartSeriesType helperType, string yColumn, string options=null)
        {
            var csd3 = new ChartSeriesDescription
            {
                SourceTableName = "Данные",
                XColumnName = "Наблюдение",
                XIsNominal = false,
                XAxisTitle = "Наблюдение",
                SeriesTitle = seriesPrefix + _selectedParameter,
                SeriesType = helperType,
                YAxisTitle = _selectedParameter,
                YColumnName = yColumn,
                ColorColumnName = "Tag",
                ColorScale = p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = p.MarkerMode,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                Options = options,
                MarkerSize = 6,
            };
            cd.ChartSeries.Add(csd3);
        }

        private IEnumerable<ChartDescription> GetDescriptionForRCard(Data d, ReportChartCardParameters p)
        {
            var result = new List<ChartDescription>();
            int n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();
            result.Add(cd);
            cd.ChartLegendType = _p.ChartLegend;

            var currentChartTitle = GetChartTitle();
            
            cd.ChartTitle = currentChartTitle;
            cd.ChartSubtitle = "n=" + n.ToString(CultureInfo.InvariantCulture);
            cd.ChartSubtitle += (_chartRangesYTable.Table.Count > 0) ? RangesHelper.GetDescription(p.RangeType) : String.Empty;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();

            cd.ChartSeries = new List<ChartSeriesDescription>
            {
                new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    XColumnName = "Наблюдение",
                    XIsNominal = true,
                    XAxisTitle = "Наблюдение",
                    YColumnName = "R",
                    YAxisTitle = _selectedParameter,
                    ColorColumnName = "Tag",
                    SeriesTitle = _selectedParameter,
                    SeriesType = ChartSeriesType.LineXY,
                    ColorScale = p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                    MarkerMode = p.MarkerMode,
                    MarkerSizeMode = p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    MarkerSize = 6,
                    LineSize = n < 500 ? 0.5 : 0
                }
            };

            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                    ? "Mean"
                    : "Median";
                AppendHelperX(p, cd, "#RangeOnY", ChartSeriesType.RangeOnY, "R", options + textenabled);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                AppendHelperX(p, cd, "#RugOnY", ChartSeriesType.RugOnY, "R");
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                AppendHelperX(p, cd, "#KDEOnY", ChartSeriesType.KDEOnY, "R");
            }

            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, true, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);
            return result;
        }
    }
}