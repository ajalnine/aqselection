﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        private IEnumerable<ChartDescription> GetDescriptionForTrendsCardMultiple(ReportChartCardParameters p)
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            cd.ChartLegendType = _p.ChartLegend;
                
            cd.ChartTitle = GetChartTitle();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var colorPosition = 0.4d;
            var spNumber = _selectedParameters.Count;
            var colorStep = 1.0d / (spNumber + 1);
            cd.ChartSubtitle += "n=" + _datas.Values.FirstOrDefault()?.GetDataTable()?.Table.Count;
            if (!String.IsNullOrEmpty(_comparisonComment)) cd.ChartSubtitle += _comparisonComment;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();

            var count = 0;
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;

            foreach (var sp in _selectedParameters)
            {
                var color = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition);
                if (!withHexBin)
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Данные" + sp,
                        XColumnName = "#Группа#",
                        XIsNominal = true,
                        YIsSecondary = _selectedParameters.Count == 2 && count == 1 && _limits.LimitCount == 0,
                        XAxisTitle = p.GroupField,
                        YColumnName = sp,
                        SeriesTitle = sp,
                        SeriesType = ChartSeriesType.LineXY,
                        ColorColumnName = "Tag",
                        ColorScale = p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        MarkerColor = color,
                        LineColor = color,
                        MarkerMode = p.MarkerMode,
                        LineSize = 0.25,
                        MarkerSize = 6,
                        MarkerSizeMode = p.MarkerSizeMode,
                        MarkerShapeMode = p.MarkerShapeMode,
                        YAxisTitle = sp
                    });
                }
                if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
                {
                    var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                        ? "Text"
                        : string.Empty;
                    var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                        ? "Mean"
                        : "Median";
                    if (spNumber > 1) options += "IsMultiple";
                    AppendHelper(p, cd, colorPosition, count, sp, "#RangeOnY", ChartSeriesType.RangeOnY, spNumber, options + textenabled);
                }

                if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
                {
                    AppendHelper(p, cd, colorPosition, count, sp, "#KDEOnY", ChartSeriesType.KDEOnY, spNumber);
                }

                if ((_p.Tools & ReportChartTools.ToolRug) > 0)
                {
                    AppendHelper(p, cd, colorPosition, count, sp, "#RugOnY", ChartSeriesType.RugOnY, spNumber);
                }

                if (withHexBin)
                {
                    AppendHelper(p, cd, colorPosition, count, sp, string.Empty, ChartSeriesType.HexBin, spNumber, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty)); ;
                }
                colorPosition += colorStep;
                if (colorPosition > 1) colorPosition -= 1;
                if (p.SmoothMode == ReportChartRegressionFitMode.DynamicSmoothLowess)
                {
                        cd.ChartSeries.Add(new ChartSeriesDescription
                        {
                            SourceTableName = "Lowess" +sp,
                            XColumnName = "#Группа#",
                            XIsNominal = true,
                            XAxisTitle = p.GroupField,
                            YIsSecondary = _selectedParameters.Count == 2 && count == 1 && _limits.LimitCount == 0,
                            YColumnName = sp,
                            SeriesTitle = "#Lowess" + sp,
                            SeriesType = ChartSeriesType.LineXY,
                            LineColor = _singleParameter ? "#FFFF4040" : color,
                            LineSize = GetLineThickness(_p.SmoothStyle) * 2,
                            YAxisTitle = sp
                        });
                }
                count++;
            }
            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, true, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            return result;
        }

        private void AppendHelper(ReportChartCardParameters p, ChartDescription cd, double colorPosition, int count, string sp, string seriesPrefix, ChartSeriesType helperType, int spNumber, string options=null)
        {
            var csd3 = new ChartSeriesDescription
            {
                SourceTableName = "Данные" + sp,
                XColumnName = "#Группа#",
                XIsNominal = true,
                XAxisTitle = p.GroupField,
                SeriesTitle = seriesPrefix + sp,
                SeriesType = helperType,
                YIsSecondary = _selectedParameters.Count == 2 && count == 1 && _limits.LimitCount == 0,
                YAxisTitle = sp,
                YColumnName = sp,
                ColorColumnName = "Tag",
                ColorScale = p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = p.MarkerMode,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                MarkerColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                LineColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                Options = spNumber > 1 ? "IsMultiple" + options : options,
                MarkerSize = 6,
            };
            cd.ChartSeries.Add(csd3);
        }
    }
}