﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        private bool _singleParameter;
        public bool BuildChartForTrendsCardMultiple(List<SLDataTable> dataTables)
        {
            SetupChartData(dataTables);
            SetupChartDataForTrendsCardMultiple(dataTables);
            if (_interactiveTables == null || _interactiveTables.Count == 0 || _interactiveTables.Where(a => a.Value.Table.Count == 0).Any()) return false;
            SetupLimits(dataTables);
            SetupFitMultiple();
            CreateChartsForTrendsMultiple();
            return true;
        }

        public void RefreshChartsForTrendsCardMultiple()
        {
            if (_interactiveTables == null || _interactiveTables.Count == 0 || _interactiveTables.Where(a => a.Value.Table.Count == 0).Any()) return;
            SetupFitMultiple();
            CreateChartsForTrendsMultiple();
        }

        private void SetupChartDataForTrendsCardMultiple(IEnumerable<SLDataTable> dataTables)
        {
            _chartLowessTables = null;
            _comparisonComment = string.Empty;
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameters = _p.Fields.Distinct().ToList();
            _originalGroupsType = _selectedTable.GetDataType(_p.GroupField);
            _originalLayersType = _selectedTable.GetDataType(_p.LayerField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.LayerField,
                    X = _p.GroupField,
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            var listOfVariables = _selectedParameters.ToList();
            if (_p.DateField != "Нет") listOfVariables.Add(_p.DateField);
            if (_p.GroupField != "Нет") listOfVariables.Add(_p.GroupField);
            _datas = new Dictionary<string, Data>();
            _interactiveTables = new Dictionary<string, SLDataTable>();
            foreach (var sp in _selectedParameters)
            {
                var fi = new FieldsInfo
                {
                    ColumnX = sp,
                    ColumnY = sp,
                    ColumnGroup = _p.GroupField,
                    ColumnDate = _p.DateField
                };

                _datas[sp] = new Data(_selectedTable, fi, listOfVariables);
                _interactiveTables[sp] = _datas[sp].GetDataTable();
                _interactiveTables[sp].TableName = "Данные" + sp;
            }
        }
        
        private void SetupFitMultiple()
        {
            var colorPosition = 0.4d;
            var spNumber = _selectedParameters.Count();
            var colorStep = 1.0d / (double)(spNumber + 1);

            switch (_p.SmoothMode)
            {
                case ReportChartRegressionFitMode.DynamicSmoothLowess:
                    _chartLowessTables = new List<SLDataTable>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lowessTable = Smooth.GetChartLowessTableForGroupAndYCase(_datas[sp].GetLDR(), sp, _p.LWSArea);
                        lowessTable.TableName = "Lowess" + sp;
                        _chartLowessTables.Add(lowessTable);
                    }
                    break;
            
                case ReportChartRegressionFitMode.DynamicSmoothLine:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lri = LineSmoothHelper.GetLinearRegressionYAndIndex(_datas[sp].GetLDR());
                        _fit.Add(lri.SetupLine(sp, _singleParameter ? "#FFFF4040" : ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition),
                            GetLineThickness(_p.SmoothStyle)*2, null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                case ReportChartRegressionFitMode.DynamicSmoothE:
                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {

                        Regression r = new Regression(_datas[sp].GetLDR(), LDRUsedFields.YI, _p.SmoothMode);
                        RegressionLine rl = new RegressionLine(r);
                        _fit.Add(
                            rl.SetupLine(sp, _singleParameter ? "#FFFF4040" : ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition),
                                GetLineThickness(_p.SmoothStyle)*2,
                                null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;
                    
                case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                    _fit = new List<MathFunctionDescription>();
                    foreach (var sp in _selectedParameters)
                    {
                        var lri2 = LineSmoothHelper.GetLinearRegressionYAndIndex(_datas[sp].GetLDR());
                        var c1 = _singleParameter ? "#FFFF4040" : ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition);
                        _fit.Add(lri2.SetupLine(sp, c1, GetLineThickness(_p.SmoothStyle) * 2, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), 1, true, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), -1, true, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), 1, false, null, null));
                        _fit.Add(lri2.SetupPredictionLine(sp, c1, GetLineThickness(_p.SmoothStyle), -1, false, null, null));
                        colorPosition += colorStep;
                        if (colorPosition > 1) colorPosition -= 1;
                    }
                    break;
            }

            if (_selectedParameters.Count==2)
            {
                var fi = new FieldsInfo
                {
                    ColumnX = _selectedParameters[0],
                    ColumnY = _selectedParameters[1],
                    ColumnGroup = _p.GroupField,
                    ColumnDate = _p.DateField
                };
                _commondata = new Data(_selectedTable, fi, false);
                var lri = LineSmoothHelper.GetLinearRegression(_commondata.GetLDR());
                _comparisonComment = lri.GetRegressionComment(false);
            }
            if (_fit!=null) foreach (var f in _fit)
            {
                f.Description = "#" + f.Description;
            }
        }

        private void CreateChartsForTrendsMultiple()
        {
            _surface.Children.Clear();
            var canShowSecondaryAxises = _limits.LimitCount == 0;
            foreach (var cd in GetDescriptionForTrendsCardMultiple(_p))
            {
                var chartCard = new Chart
                {
                    EnableSecondaryAxises = canShowSecondaryAxises,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalGroupsType, Axis = "X", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalGroupsType?.ToLower().Contains("datetime") ?? false }, new FixedAxis { DataType = _originalLayersType, Axis = "Z", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } } },
                    EnableCellCondense = true,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable> {_chartRangesYTable}
                };
                chartCard.DataTables.AddRange(_interactiveTables.Values);
                if (_chartLowessTables!=null) chartCard.DataTables.AddRange(_chartLowessTables);
                chartCard.ToolTipRequired += ChartCard_ToolTipRequired;
                chartCard.SelectionChanged += ChartCard_SelectionChanged;
                chartCard.AxisClicked += chartCard_AxisClicked;
                chartCard.InteractiveRename += chartCard_InteractiveRename;
                chartCard.VisualReady += ChartCard_VisualReady;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartCard });
                _surface.Children.Add(chartCard);
                chartCard.MakeChart();
            }
        }

        private void ChartCard_VisualReady(object o, VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = null});
        }

        double GetLineThickness(ReportChartRangeStyle lineStyle)
        {
            return lineStyle == ReportChartRangeStyle.Invisible
                          ? 0.5
                          : (lineStyle == ReportChartRangeStyle.Thin ? 1 : 2);
        }
    }
}