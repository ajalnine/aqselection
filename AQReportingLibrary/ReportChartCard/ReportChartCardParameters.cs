﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportChartCardParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string GroupField { get; set; }
        public string DateField { get; set; }
        public string LayerField { get; set; }
        public ReportChartCardType CardType { get; set; }
        public ReportChartSeriesType Series { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartRangeStyle SmoothStyle { get; set; }
        public ReportChartRegressionFitMode SmoothMode { get; set; }
        public double LWSArea { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }
        public ReportChartTools Tools { get; set; }
        public SLDataTable UserLimits { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }
        public ChartLegend ChartLegend { get; set; }
    }
}