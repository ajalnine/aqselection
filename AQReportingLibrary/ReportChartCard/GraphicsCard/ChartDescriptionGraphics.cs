﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        private IEnumerable<ChartDescription> GetDescriptionForGraphicsCard(ReportChartCardParameters p)
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            cd.ChartLegendType = _p.ChartLegend;
                
            cd.ChartTitle = GetChartTitle();

            cd.ChartSeries = new List<ChartSeriesDescription>();
            var colorPosition = 0.4d;
            var spNumber = _selectedParameters.Count();
            var colorStep = 1.0d / (double)(spNumber + 1);
            if (spNumber>1)cd.ChartSubtitle += "n="+_datas.Values.FirstOrDefault()?.GetDataTable()?.Table.Count;
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();

            foreach (var sp in _selectedParameters)
            {
                if (spNumber <= 4)
                {
                    if (!string.IsNullOrEmpty(cd.ChartSubtitle)) cd.ChartSubtitle += "\r\n";
                    cd.ChartSubtitle += string.Format("n({0})={1}", sp, _datas[sp].GetDataTable().Table.Count(a => a.Row[2] != null));
                }
                if (!withHexBin)
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Данные_" + sp,
                        XColumnName = "#Группа#",
                        XIsNominal = true,
                        XAxisTitle = p.GroupField,
                        ZColumnName = p.LayerField == "Нет" ? null : p.LayerField,
                        YColumnName = sp,
                        SeriesTitle = sp,
                        SeriesType = ChartSeriesType.LineXY,
                        ColorScale = p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        ColorColumnName = "Tag",
                        LabelColumnName = "Метка",
                        MarkerColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                        LineColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                        MarkerMode = p.MarkerMode,
                        LineSize = 0.25,
                        MarkerSize = 6,
                        MarkerSizeMode = p.MarkerSizeMode,
                        MarkerShapeMode = p.MarkerShapeMode,
                        YAxisTitle = sp
                    });
                }
                if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
                {
                    var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                        ? "Text"
                        : string.Empty;
                    var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                        ? "Mean"
                        : "Median";
                    if (spNumber > 1) options += "IsMultiple";
                    AppendHelper(p, cd, colorPosition, sp, "#RangeOnY", ChartSeriesType.RangeOnY, spNumber, options + textenabled);
                }

                if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
                {
                    AppendHelper(p, cd, colorPosition, sp, "#KDEOnY", ChartSeriesType.KDEOnY, spNumber);
                }

                if ((_p.Tools & ReportChartTools.ToolRug) > 0)
                {
                    AppendHelper(p, cd, colorPosition, sp, "#RugOnY", ChartSeriesType.RugOnY, spNumber);
                }
                if (withHexBin)
                {
                    AppendHelper(p, cd, colorPosition, sp, string.Empty, ChartSeriesType.HexBin, spNumber, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty)); ;
                }

                colorPosition += colorStep;
                if (colorPosition > 1) colorPosition -= 1;
            }
            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, true, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            return result;
        }

        private static void AppendHelper(ReportChartCardParameters p, ChartDescription cd, double colorPosition, string sp, string seriesPrefix, ChartSeriesType helperType, int spNumber, string options = null)
        {
            var csd3 = new ChartSeriesDescription
            {
                SourceTableName = "Данные_" + sp,
                XColumnName = "#Группа#",
                XIsNominal = true,
                XAxisTitle = p.GroupField,
                SeriesTitle = seriesPrefix + sp,
                SeriesType = helperType,
                YAxisTitle = sp,
                ZColumnName = p.LayerField == "Нет" ? null : p.LayerField,
                YColumnName = sp,
                ColorColumnName = "Tag",
                ColorScale = p.ColorScale,
                ColorScaleMode = p.ColorScaleMode,
                MarkerMode = p.MarkerMode,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                Options = spNumber > 1 ? "IsMultiple"+ options : options,
                MarkerColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                LineColor = ColorConvertor.GetColorStringForScale(p.ColorScale, colorPosition),
                MarkerSize = 6,
                ZValueSeriesTitle = (p.LayerField != "Нет" && !string.IsNullOrEmpty(seriesPrefix) && spNumber==1) ? sp : null
            };
            cd.ChartSeries.Add(csd3);
        }
    }
}