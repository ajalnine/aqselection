﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        public bool BuildChartForGraphicsCard(List<SLDataTable> dataTables)
        {
            SetupChartData(dataTables);
            SetupChartDataForGraphicsCard(dataTables);
            if (_interactiveTables.Count==0 &&(_interactiveTable == null || _interactiveTable.Table.Count == 0)) return false;
            SetupLimits(dataTables);
            CreateChartsForGraphics();
            return true;
        }

        public bool RefreshChartsForGraphicsCard()
        {
            if (_interactiveTables.Count == 0 && (_interactiveTable == null || _interactiveTable.Table.Count == 0)) return false;
            CreateChartsForGraphics();
            return true;
        }

        private void SetupChartDataForGraphicsCard(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameters = _p.Fields;
            _originalLayersType = _selectedTable.GetDataType(_p.LayerField);
            
            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.LayerField,
                    X = _p.GroupField,
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }
            
            _datas = new Dictionary<string, Data>();
            _interactiveTables = new Dictionary<string, SLDataTable>();

            foreach(var p in _selectedParameters)
            {
                var fi = new FieldsInfo
                {
                    ColumnY = p,
                    ColumnGroup = _p.GroupField,
                    ColumnDate = _p.DateField,
                    ColumnZ = _p.LayerField
                };
                var d = new Data(_selectedTable, fi, true);
                _datas.Add(p, d);
                var table = d.GetDataTable();
                table.TableName += "_" + p;
                _interactiveTables.Add(p, table);
            }
        }

        private void CreateChartsForGraphics()
        {
            _surface.Children.Clear();
            var canShowSecondaryAxises = _limits.LimitCount == 0;
            foreach (var cd in GetDescriptionForGraphicsCard(_p))
            {
                var chartCard = new Chart
                {
                    EnableSecondaryAxises = canShowSecondaryAxises,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    EnableCellCondense = true,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalGroupsType, Axis = "X", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalGroupsType?.ToLower().Contains("datetime") ?? false }, new FixedAxis { DataType = _originalLayersType, Axis = "Z", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } } },
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    GlobalFontCoefficient = _p.FontSize,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable> {}
                };
                chartCard.DataTables.AddRange(_interactiveTables.Values);
                chartCard.DataTables.Add(_chartRangesYTable);
                chartCard.ToolTipRequired += ChartCard_ToolTipRequired;
                chartCard.SelectionChanged += ChartCard_SelectionChanged;
                chartCard.AxisClicked += chartCard_AxisClicked;
                chartCard.VisualReady += ChartCard_VisualReady;
                chartCard.InteractiveRename += chartCard_InteractiveRename;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartCard });
                _surface.Children.Add(chartCard);
                chartCard.MakeChart();
            }
        }
    }
}