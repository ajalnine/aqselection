﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartCard : IReportElement
    {
        private SLDataTable _chartRangesYTable, _nonEmptyDataTable;
        private SLDataTable _chartLowessTable, _chartLowessTable2;
        private List<SLDataTable> _chartLowessTables;
        private SLDataTable _interactiveTable;
        private Dictionary<string, SLDataTable> _interactiveTables;
        private List<MathFunctionDescription> _fit;
        private string _probabilityComment = string.Empty;
        private string _comparisonComment = string.Empty;
        private string _selectedParameter;
        private string _selectedParameter2;
        private List<string> _selectedParameters;
        private Data _data, _data2, _commondata;
        private Dictionary<string, Data> _datas;
        private Limits _limits;
        private SLDataTable _selectedTable;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType, _originalGroupsType;

        ReportChartCardParameters _p;
        private Step _step;
        private readonly Dictionary<ReportChartCardType, string> _chartNames = new Dictionary<ReportChartCardType, string>
        {
            {ReportChartCardType.CardTypeIsR, "R-карта"},
            {ReportChartCardType.CardTypeIsX, "X-карта"},
            {ReportChartCardType.CardTypeIsTrends, "Тренды"},
            {ReportChartCardType.CardTypeIsGraphics, "Графики"},
        };

        public ReportChartCard(ReportChartCardParameters p)
        {
            _p = p;
            CheckSingleParameterMode();
            _step = StepConverter.GetStep(p, GetChartName());
        }

        private void CheckSingleParameterMode()
        {
            _singleParameter = _p.Fields.Count <= 1 || (_p.Fields.Count == 2 && _p.Fields[0] == _p.Fields[1]);
        }

        public ReportChartCard(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartCardParameters>(s);
            CheckSingleParameterMode();
            _step = s;
        }

        public void SetNewParameters(ReportChartCardParameters p)
        {
            _p = p;
            CheckSingleParameterMode();
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if ((_p.Tools & ReportChartTools.ToolHexBin) > 0)return _p.Fields.Count == 1
                        ? String.Format(@"<b>{0}</b>: карта плотности <b>{1}</b>", _p.Table, _p.Fields[0])
                        : _p.Fields.Count == 2
                                             ? _p.Fields[0] != _p.Fields[1]
                                                    ? String.Format(@"<b>{0}</b>: карта плотности <b>{1}</b> и <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1])
                                                    : String.Format(@"<b>{0}</b>: карта плотности <b>{1}</b>", _p.Table, _p.Fields[0])
                                             : String.Format(@"<b>{0}</b>: карта плотности", _p.Table);

            switch (_p.CardType)
            {
             case ReportChartCardType.CardTypeIsX:
                    return String.Format(@"<b>{0}</b>: X-Карта <b>{1}</b>", _p.Table, _p.Fields[0]);
             case ReportChartCardType.CardTypeIsR:
                    return String.Format(@"<b>{0}</b>: R-Карта <b>{1}</b>", _p.Table, _p.Fields[0]);
             case ReportChartCardType.CardTypeIsTrends:
                    return _p.Fields.Count == 1
                        ? String.Format(@"<b>{0}</b>: тренд <b>{1}</b>", _p.Table, _p.Fields[0])
                        : _p.Fields.Count == 2 
                                             ?  _p.Fields[0] != _p.Fields[1] 
                                                    ? String.Format(@"<b>{0}</b>: тренды <b>{1}</b> и <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1])
                                                    : String.Format(@"<b>{0}</b>: тренд <b>{1}</b>", _p.Table, _p.Fields[0])
                                             : String.Format(@"<b>{0}</b>: тренды", _p.Table);
                case ReportChartCardType.CardTypeIsGraphics:
                    return _p.Fields.Count == 1 
                        ? String.Format(@"<b>{0}</b>: график <b>{1}</b>", _p.Table, _p.Fields[0])
                        : _p.Fields.Count==2 ? String.Format(@"<b>{0}</b>: графики <b>{1}</b> и <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1])
                                             : String.Format(@"<b>{0}</b>: графики", _p.Table);
            default:
                    return string.Empty;
            }                
        }

        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if ((_p.Tools & ReportChartTools.ToolHexBin) > 0) return _p.Fields.Count == 1
                         ? String.Format(@"{0}: карта плотности {1}", _p.Table, _p.Fields[0])
                         : _p.Fields.Count == 2
                                              ? _p.Fields[0] != _p.Fields[1]
                                                     ? String.Format(@"{0}: карта плотности {1} и {2}", _p.Table, _p.Fields[0], _p.Fields[1])
                                                     : String.Format(@"{0}: карта плотности {1}", _p.Table, _p.Fields[0])
                                              : String.Format(@"{0}: карта плотности", _p.Table);
            switch (_p.CardType)
            {
                case ReportChartCardType.CardTypeIsX:
                    return String.Format(@"{0}: X-Карта {1}", _p.Table, _p.Fields[0]);
                case ReportChartCardType.CardTypeIsR:
                    return String.Format(@"{0}: R-Карта {1}", _p.Table, _p.Fields[0]);
                case ReportChartCardType.CardTypeIsTrends:
                    return _p.Fields.Count == 1
                        ? String.Format(@"{0}: тренд {1}", _p.Table, _p.Fields[0])
                        : _p.Fields.Count == 2
                                             ? _p.Fields[0] != _p.Fields[1]
                                                    ? String.Format(@"{0}: тренды {1} и {2}", _p.Table, _p.Fields[0], _p.Fields[1])
                                                    : String.Format(@"{0}: тренд {1}", _p.Table, _p.Fields[0])
                                             : String.Format(@"{0}: тренды", _p.Table);
                case ReportChartCardType.CardTypeIsGraphics:
                    return _p.Fields.Count == 1
                        ? String.Format(@"{0}: график {1}", _p.Table, _p.Fields[0])
                        : _p.Fields.Count == 2 ? String.Format(@"{0}: графики {1} и {2}", _p.Table, _p.Fields[0], _p.Fields[1])
                                             : String.Format(@"{0}: графики", _p.Table);
                default:
                    return string.Empty;
            }
        }

        public string GetChartName()
        {
            if ((_p.Tools & ReportChartTools.ToolHexBin) > 0) return "Карта плотности";
            return _chartNames[_p.CardType];
        }
    }
}