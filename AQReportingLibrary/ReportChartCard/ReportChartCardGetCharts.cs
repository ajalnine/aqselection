﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;

namespace AQReportingLibrary
{
    public partial class ReportChartCard
    {
        private Panel _surface;
        
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            switch (_p.CardType)
            {
                case ReportChartCardType.CardTypeIsTrends:
                    return BuildChartForTrendsCardMultiple(dataTables);
                case ReportChartCardType.CardTypeIsGraphics:
                    return BuildChartForGraphicsCard(dataTables);
                default:
                    return BuildChartForGenericCards(dataTables);
            }
        }

        public void Cancel() { }

        public void RefreshVisuals()
        {
            switch (_p.CardType)
            {
                case ReportChartCardType.CardTypeIsTrends:
                    RefreshChartsForTrendsCardMultiple();
                    return;
                case ReportChartCardType.CardTypeIsGraphics:
                    RefreshChartsForGraphicsCard();
                    return;
                default:
                    RefreshChartsForGenericCards();
                    return;
            }
        }
    }
}