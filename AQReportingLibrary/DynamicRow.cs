﻿using System;
using System.Collections.Generic;

namespace AQReportingLibrary
{
    public class DynamicRow
    {
        public object Group;
        public double? X;
        public double? Xf;
        public DateTime? Xdate;
        public double? Y;
        public object Z;
        public object W;
        public object V;
        public int Index;
        public double? MaxallowedY;
        public double? MinallowedY;
        public double? MaxallowedX;
        public double? MinallowedX;
        public string Color;
        public string Label;
        public double? Markersize;
    }

    public class StatInfo
    {
        public object Group;
        public double? BinMax;
        public double? BinMaxInPercent;
        public double? DisplayMax;
        public double? DisplayMin;
        public double? Px;
        public double? S;
        public double? Step;
        public double? WK;
        public double? WLambda;
        public double? X;
        public double? N;
        public double? PercentOfTotal;
        public string ZAxis;
        public bool ShowInversed;
        public List<object> SourceData;
    }

    public class FieldsInfo
    {
        public string ColumnX = string.Empty;
        public string ColumnY = string.Empty;
        public string ColumnZ = string.Empty;
        public string ColumnW = string.Empty;
        public string ColumnV = string.Empty;
        public string ColumnGroup = string.Empty;
        public string ColumnDate = string.Empty;
    }
}