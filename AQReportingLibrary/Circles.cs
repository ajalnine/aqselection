﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public class Circles
    {
        private readonly List<GroupValues> _data;
        private readonly Data _sourceData;
        private SLDataTable _chartColumnsTable;
        private SLDataTable _sourceTable;
        private readonly ReportChartCirclesParameters _p;

        public Circles(ReportChartCirclesParameters p, Data source, bool ignoreLayers)
        {
            _data = source.GetLDR().Where(a => a != null ).Select(a => new GroupValues
            {
                Name = a.Group, Value = a.Y, Category = ignoreLayers ? null : a.Z, Color = a.Color,
                Label = a.Label ?? a.V?.ToString(), OriginalIndex = a.Index
            }).ToList();
            _p = p;
            _sourceData = source;
            _sourceTable = source.GetDataTable();
            BuildCirclesData();
        }

        #region Заполнение таблицы с bin

        private void BuildCirclesData()
        {
            CreateChartCirclesTableHeaders();
            CreateCircleData();
        }

        private void CreateChartCirclesTableHeaders()
        {
            var z = _p.Fields[0] == _p.ColorField ? _p.ColorField + " " : _p.ColorField;
            _chartColumnsTable = new SLDataTable
            {
                TableName = "Диаграмма",
                Table = new List<SLDataRow>(),
                DataTypes = new[]
                {
                    "String",     "Double",    "String",     "String", "Double", "Double", _sourceTable.DataTypes[8], "Double", "String", "Double", "String", "Double"}.ToList(),
                ColumnNames = new[]
                {
                    _p.Fields[0], "Значение", "Статистика", "Подписи", "Отступ_",  "Ширина_", z,                           "Позиция", "Цвет", "Индекс", "Оригинал", "Размер"}.ToList()
            };
        }

        private string GetTextLabel(double binvalue, double binpercent, double binpercentingroup, double n,
            string label, string layer)
        {
            string textLabel = string.Empty;
            if (Math.Abs(binvalue) > double.Epsilon)
            {
                if ((_p.LabelsType & ReportChartCircleLabelsType.Layer) == ReportChartCircleLabelsType.Layer &&
                    !string.IsNullOrEmpty(label))
                {
                    textLabel += label;
                }
                else if ((_p.LabelsType & ReportChartCircleLabelsType.Layer) == ReportChartCircleLabelsType.Layer &&
                         !string.IsNullOrEmpty(layer))
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += layer;
                }

                if ((_p.LabelsType & ReportChartCircleLabelsType.N) == ReportChartCircleLabelsType.N)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += AQMath.SmartRound(binvalue, 3);
                }

                if ((_p.LabelsType & ReportChartCircleLabelsType.Percent) == ReportChartCircleLabelsType.Percent)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += Math.Round(binpercent, 2) + " %";
                }

                if ((_p.LabelsType & ReportChartCircleLabelsType.PercentInGroup) ==
                    ReportChartCircleLabelsType.PercentInGroup)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += Math.Round(binpercentingroup, 2) + " %";
                }

                if ((_p.LabelsType & ReportChartCircleLabelsType.NumberOfCases) ==
                    ReportChartCircleLabelsType.NumberOfCases)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                    textLabel += "n=" + Math.Round(n, 0);
                }
            }

            return textLabel;
        }
        
        private void CreateCircleData()
        {
            var unordered = AggregateGrouping.Calculators[_p.Aggregate](_data).ToList();
            var groupParameterName = AggregateGrouping.ParameterNames[_p.Aggregate];
            var names = unordered.Select(a => a.Name).Distinct().ToList();
            foreach (var n in names)
            {
                var d = unordered.Where(a => a.Name?.ToString() == n?.ToString());
                var maxY = d.Sum(a => a.Value);
                foreach (var g in d) g.Total = maxY;
            }

            var total = unordered.Where(a => a.Value.HasValue).Sum(a => a.Value.Value);

            var x = unordered.Select(a => a.Name).Distinct().ToList();
            var clist = unordered.Select(a => a.Category).Distinct().ToList();
            var totalPerCategory = _p.ColorField == "Нет"
                ? new Dictionary<object, int> {{"#", (int) total}}
                : _data.GroupBy(a => a.Category).ToDictionary(a => a.Key ?? "#", a => a.Count());
            var categories = clist.Where(a => a != null && !string.IsNullOrEmpty(a.ToString()) && a != DBNull.Value)
                .OrderBy(a => a).ToList();
            var clean = clist.Where(a => a == null || string.IsNullOrEmpty(a.ToString()) || a == DBNull.Value).ToList();
            categories.AddRange(clean);

            double offsetInPercent, percentInGroup, percent, xPosition, percentInCategory;

            foreach (var name in x)
            {
                offsetInPercent = 0.0d;
                var currentGroup = unordered.Where(a => a.Name?.ToString() == name?.ToString()).ToList();
                var totalInGroup = currentGroup.Where(a => a.Value.HasValue).Sum(a => a.Value.Value);
                var partOffset = 0.0d;
                foreach (var g in currentGroup
                    .Where(a => a.Category != null && !string.IsNullOrEmpty(a.Category.ToString()) &&
                                a.Category != DBNull.Value).ToList()
                    .OrderByDescending(a => a.Value)
                    .Union(currentGroup.Where(a =>
                            a.Category == null || string.IsNullOrEmpty(a.Category.ToString()) ||
                            a.Category == DBNull.Value)
                        .ToList()).ToList())
                {
                   if (g.Value.HasValue || g.Value != 0)
                    {

                        percentInGroup = Math.Abs(totalInGroup) < double.Epsilon ? 100.0d : g.Value.Value / totalInGroup * 100.0d;
                        percent = Math.Abs(total) < double.Epsilon ? 100.0d : g.Value.Value / total * 100.0d;
                        var binName = g.Name ?? "#Нет";
                        xPosition = (double) x.IndexOf(name);
                        percentInCategory = Math.Abs(totalPerCategory[g.Category ?? "#"]) < double.Epsilon ? 100.0d : g.Value.Value / (double) totalPerCategory[g.Category ?? "#"] * 100.0d;

                        if (_p.Aggregate == Aggregate.Percent || _p.Aggregate == Aggregate.PercentInCategory)
                        {
                            var stat = String.Format("{0} {2}: {1}", g.Category, Math.Round(percent, 3),
                                groupParameterName);
                            var p = _p.Aggregate == Aggregate.Percent ? percent : percentInCategory;
                            _chartColumnsTable.Table.Add(new SLDataRow
                            {
                                /*_p.Fields[0], "Значение", "Статистика", "Подписи", "Отступ",  "Угол", z, "Позиция", "Цвет", "Индекс", "Оригинал"    */

                                Row = new List<object>
                                {
                                    binName,
                                    p,
                                    stat,
                                    GetTextLabel(Math.Round(p, 2),Math.Round(p, 2),Math.Round(_p.Aggregate == Aggregate.PercentInCategory? percentInCategory: percentInGroup, 2),
                                        g.n, g.Label, g.Category?.ToString()),
                                    partOffset,
                                    percentInGroup / 100.0d,
                                    g.Category,
                                    xPosition,
                                    g.Color,
                                    g.OriginalIndex,
                                    g.Name,
                                    p
                                }
                            });
                            offsetInPercent += percentInGroup;
                            partOffset += percentInGroup / 100.0d;
                        }
                        else
                        {
                            var stat = String.Format("{0} {2}: {1}", g.Category, Math.Round(g.Value.Value, 3),
                                groupParameterName);
                            _chartColumnsTable.Table.Add(new SLDataRow
                            {
                                Row = new List<object>
                                {
                                    binName,
                                    g.Value,
                                    stat,
                                    GetTextLabel(g.Value.Value, Math.Round(percent, 2), Math.Round(percentInGroup, 2),
                                        g.n, g.Label, g.Category?.ToString()),
                                    partOffset,
                                    g.Value / totalInGroup,
                                    g.Category,
                                    xPosition,
                                    g.Color,
                                    g.OriginalIndex,
                                    g.Name,
                                    percent
                                }
                            });
                            offsetInPercent += percentInGroup;
                            partOffset += (g.Value.Value / totalInGroup);
                        }
                    }
                }
            }
        }


        #endregion

        public SLDataTable GetColumnsTable()
        {
            return _chartColumnsTable;
        }
    }
}