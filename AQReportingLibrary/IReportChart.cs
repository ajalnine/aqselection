﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;
using AQStepFactoryLibrary;

namespace AQReportingLibrary
{
    public interface IReportElement
    {
        event ReportElementChangedDelegate ReportChartChanged;
        event LimitsKnownDelegate UiReflectChanges;
        event AxisClickedDelegate AxisClicked;
        event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        event ReportInteractiveRenameDelegate ReportInteractiveRename;
        event FieldMarkChangingDelegate FieldMarkChanging;
        event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        event InteractiveParametersChangeDelegate InteractiveParametersChange;
        event InteractivityDelegate Interactivity;
        event RangeAppendedDelegate RangeAppended;
        event ReportChartFihishedDelegate Finished;

        bool MakeVisuals(List<SLDataTable> dataTables, Panel chartSurface);
        void RefreshVisuals();
        void Cancel();
        string GetChartTitle();
        string GetChartDescription(); 
        string GetChartName();
        Step GetStep();
    }
    
    public delegate void ReportElementChangedDelegate(object o, EventArgs e);

    public delegate void InteractiveParametersChangeDelegate(object o, InteractiveParametersEventArgs e);

    public delegate void InteractivityDelegate(object o, InteractivityEventArgs e);

    public delegate void ReportInteractiveRenameDelegate(object o, ReportInteractiveRenameEventArgs e);

    public delegate void InternalTablesReadyForExportDelegate(object o, InternalTablesReadyForExportEventArgs e);

    public delegate void LimitsKnownDelegate(object o, UIReflectChangesEventArgs e);

    public delegate void AxisClickedDelegate(object o, AxisEventArgs e);

    public delegate void ChartSetsFixedAxisDelegate(object o, ChartSetsFixedAxisEventArgs e);

    public delegate void FieldMarkChangingDelegate(object o, FieldMarkChanging e);

    public delegate void RangeAppendedDelegate(object o, RangeAppendedEventArgs e);

    public delegate void ReportChartFihishedDelegate(object o, ReportChartFinishedEventArgs e);

    public class UIReflectChangesEventArgs : EventArgs
    {
        public SLDataTable UserLimits{ get; set; }
        public double? Step { get; set; }
        public bool? StepIsAuto { get; set; }
    }

    public class InteractiveParametersEventArgs : EventArgs
    {
        public Step Step { get; set; }
    }

    public class ReportChartFinishedEventArgs :EventArgs
    {
        public List<SLDataTable> InnerTables { get; set; }
    }

    public class ChartSetsFixedAxisEventArgs : EventArgs
    {
        public Chart SettingChart { get; set; }
    }

    public class ReportRenameOperation : EventArgs
    {
        public ReportRenameTarget RenameTarget { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public string Table { get; set; }
        public string LayerField { get; set; }
    }

    public class InteractivityEventArgs : EventArgs
    {
        public string Table;
        public string Field;
        public string TargetField;
        public List<int> SelectedCases;
        public List<object> SelectedValues;
        public InteractiveOperations AvailableOperations;
        public string Message;
        public bool Clear = false;
    }

    [Flags]
    public enum InteractiveOperations
    {
        None = 0,
        DeleteValue = 1,
        DeleteCase = 2,
        CutInNewTable = 4,
        CopyToNewTable = 8,
        SetColor = 0x10,
        SetColorForRow = 0x20,
        SetLabel = 0x40,
        SplitLayers = 0x80,
        DeleteColumn = 0x100,
        SetFilter = 0x200,
        DeleteFilter = 0x400,
        RemoveFilters = 0x800,
        CombineLayers = 0x1000,
        Outliers = 0x2000,
        OutOfRange = 0x4000,
        All = 0xffff
    }

    public class FieldMarkChanging : EventArgs
    {
        public Dictionary<string, Color> XFieldMarks { get; set; }
        public Dictionary<string, Color> YFieldMarks { get; set; }
        public bool ResetRequired { get; set; }
    }

    public class ReportInteractiveRenameEventArgs : EventArgs
    {
        public List<ReportRenameOperation> RenameOperations { get; set; }
    }

    public class RangeAppendedEventArgs : EventArgs
    {
        public string SelectedTable;
        public string LayerField;
        public Range NewRange;
    }

    public class InternalTablesReadyForExportEventArgs : EventArgs
    {
        public List<SLDataTable> InternalTables { get; set; }
        public Guid AnalysisGuid { get; set; }
    }

    public enum ReportRenameTarget
    {
        Table,
        Field,
        Cathegory,
        Range,
        DateRange, 
        Layer
    }
}