﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartScatterPlot
    {
        private IEnumerable<ChartDescription> GetDescriptionForScatterPlotMultiple()
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            if ((_p.LabelField != null && _p.LabelField != "Нет") || _p.Fields.Count > 1) cd.ChartLegendType = ChartLegend.Right;

            cd.ChartTitle = GetChartTitle();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            cd.ChartLegendType = _p.ChartLegend;
            var colorPosition = 0.4d;

            var spNumber = _selectedParameters.Count();
            var colorStep = 1.0d / (spNumber + 1);
            var count = 0;
            var withSurface = (_p.Tools & ReportChartTools.ToolSurface) > 0;
            var withVoronoy = (_p.Tools & ReportChartTools.ToolVoronoy) > 0;
            var withBag = (_p.Tools & ReportChartTools.ToolBag) > 0;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;
            var cutted = (_p.Tools & ReportChartTools.ToolCutSurfaces) > 0;
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();

            foreach (var sp in _selectedParameters)
            {
                if (spNumber <= 4)
                {
                    if (!string.IsNullOrEmpty(cd.ChartSubtitle)) cd.ChartSubtitle += "\r\n";
                    cd.ChartSubtitle += string.Format("n(<b>{0}</b>)={1}", sp, _datas[sp].GetDataTable().Table.Count(a => a.Row[2] != null));
                    if (_regressionComments!=null && _regressionComments.Count > 0 && _regressionComments.ContainsKey(sp)) cd.ChartSubtitle += string.IsNullOrEmpty(_regressionComments[sp]) ? string.Empty : _regressionComments[sp];
                }
                var color = ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition);

                if ((_p.Tools & ReportChartTools.ToolVoronoy) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#Voronoy", ChartSeriesType.Voronoy, null);
                }

                if ((_p.Tools & ReportChartTools.ToolBag) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#Bag", ChartSeriesType.Bag, null);
                }

                if (!withHexBin)
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Данные" + sp,
                        XColumnName = _p.FieldX,
                        XIsNominal = false,
                        XAxisTitle = _p.FieldX,
                        SeriesTitle = sp,
                        SeriesType = ChartSeriesType.LineXY,
                        YAxisTitle = sp,
                        YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                        YColumnName = sp,
                        VColumnName = (_p.LabelField != "Нет") ? _p.LabelField : null,
                        ColorColumnName = "Tag",
                        MarkerSizeColumnName = "Маркер",
                        LabelColumnName = "Метка",
                        ColorScale = _p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        MarkerMode = _p.MarkerMode,
                        MarkerSizeMode = _p.MarkerSizeMode,
                        MarkerShapeMode = _p.MarkerShapeMode,
                        VAxisTitle = _p.LabelField,
                        MarkerColor = color,
                        LineColor = color,
                        MarkerSize = withSurface ? 1 : 5,
                    });
                }

                if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
                {
                    var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                        ? "Text"
                        : string.Empty;
                    var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                        ? "Mean"
                        : "Median";
                    AppendHelperMultiple(cd, count, sp, color, "#RangeOnX", ChartSeriesType.RangeOnX, options + textenabled);
                    AppendHelperMultiple(cd, count, sp, color, "#RangeOnY", ChartSeriesType.RangeOnY, options + textenabled);
                }

                if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#KDEOnY", ChartSeriesType.KDEOnY);
                    AppendHelperMultiple(cd, count, sp, color, "#KDEOnX", ChartSeriesType.KDEOnX);
                }

                if ((_p.Tools & ReportChartTools.ToolRug) > 0)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#RugOnY", ChartSeriesType.RugOnY);
                    AppendHelperMultiple(cd, count, sp, color, "#RugOnX", ChartSeriesType.RugOnX);
                }

                if (withSurface)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#Surface", ChartSeriesType.Surface, cutted && withSurface ? "Cutted" : null);
                }

                if (withVoronoy)
                {
                    AppendHelperMultiple(cd, count, sp, color, "#Voronoy", ChartSeriesType.Voronoy, null);
                }

                if (withHexBin)
                {
                    AppendHelperMultiple(cd, count, sp, color, string.Empty, ChartSeriesType.HexBin, (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty));
                }

                if (_p.SmoothMode == ReportChartRegressionFitMode.DynamicSmoothLowess && _chartLowessTables != null)
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Lowess" + sp,
                        XColumnName = _p.FieldX,
                        XIsNominal = false,
                        XAxisTitle = _p.FieldX,
                        YColumnName = sp,
                        YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                        SeriesTitle = "#Lowess" + sp,
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = _selectedParameters.Count == 1
                            ? _p.LayersMode == ReportChartLayerMode.Single ? "#ffFF4040" : color
                            : color,
                        ColorScale = _p.ColorScale,
                        ColorScaleMode = _p.ColorScaleMode,
                        LineSize = GetLineThickness(_p.SmoothStyle) * 2,
                        IsSmoothed = true,
                        YAxisTitle = sp
                    });

                colorPosition += colorStep;
                if (colorPosition > 1) colorPosition -= 1;
                count++;
            }
            return result;
        }

        private void AppendHelperMultiple(ChartDescription cd, int count, string sp, string color, string seriesPrefix, ChartSeriesType helperType, string options = null)
        {
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Данные" + sp,
                XColumnName = _p.FieldX,
                XIsNominal = false,
                XAxisTitle = _p.FieldX,
                SeriesTitle = seriesPrefix + sp,
                SeriesType = helperType,
                YAxisTitle = sp,
                YIsSecondary = _selectedParameters.Count == 2 && count == 1,
                YColumnName = sp,
                ColorColumnName = "Tag",
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerColor = color,
                LineColor = color,
                MarkerSize = 5,
                Options = "IsMultiple" + options ?? string.Empty
            };
            cd.ChartSeries.Add(csd2);
        }
    }
}