﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartScatterPlot
    {
        Panel _surface;

        double GetLineThickness(ReportChartRangeStyle lineStyle)
        {
            return lineStyle == ReportChartRangeStyle.Invisible
                          ? 0.5
                          : (lineStyle == ReportChartRangeStyle.Thin ? 1 : 2);
        }

        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            _selectedTable = dataTables.Single(a => a.TableName == _p.Table);
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _p.FieldX,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            if (_p.LayersMode == ReportChartLayerMode.TableOnly)
            {
                ShowCorrelationTables();
                return true;
            }
            if (_p.LayersMode == ReportChartLayerMode.Extended)
            {
                ShowCircularGraph();
                return true;
            }
            if (_p.Fields.Count == 1) return BuildChartsSingle(dataTables);
            return BuildChartsMultiple(dataTables);
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }

        public void RefreshVisuals()
        {
            if (_p.LayersMode == ReportChartLayerMode.TableOnly)
            {
                RefreshCorrelationTables();
                return;
            }
            if (_p.LayersMode == ReportChartLayerMode.Extended)
            {
                RefreshCircularGraph();
                return;
            }
            if (_p.Fields.Count == 1)RefreshChartsSingle();
            else RefreshChartsMultiple();
        }

        public bool BuildChartsSingle(List<SLDataTable> dataTables)
        {
            SetupChartData(dataTables);
            if (_interactiveTable.Table.Count == 0) return false;
            SetupLimits(dataTables);
            CheckRange.MarkOutages(_p.MarkOutage, _p.UserLimits, _p.RangeType, _limits, _data.GetDataTable());
            SetupFit();
            CreateCharts();
            return true;
        }

        public void RefreshChartsSingle()
        {
            if (_interactiveTable.Table.Count == 0) return;
            SetupFit();
            CreateCharts();
        }

        private void SetupLimits(IEnumerable<SLDataTable> dataTables)
        {
            _limits = new Limits(_data.GetLDR(),
                _p.RangeType,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.ScatterPlot,
                _p.UserLimits,
                Limits.GetExternalLimitsTable(dataTables, _selectedTable),
                _selectedParameter, false, _p.RangeStyle);

            _chartRangesTable = _limits.GetRangesYTable();

            _probabilityComment = _limits.GetProbabilityComment(false);
            UiReflectChanges?.Invoke(this, new UIReflectChangesEventArgs { UserLimits = _limits.UserLimitsTable });
        }

        private void SetupFit()
        {
            _chartLowessTables = new List<SLDataTable>();
            _regressionComment = string.Empty;
            _fit = new List<MathFunctionDescription>();
            if (_p.LayersMode == ReportChartLayerMode.Single)
            {
                switch (_p.SmoothMode)
                {
                    case ReportChartRegressionFitMode.DynamicSmoothLowess:
                        _chartLowessTables.Add(Smooth.GetChartLowessTableForNumberCase(_data.GetLDR(), _p.FieldX, null, _p.LWSArea));
                        break;

                    case ReportChartRegressionFitMode.DynamicSmoothLine:
                        LinearRegressionInfo lri = LineSmoothHelper.GetLinearRegression(_data.GetLDR());
                        _fit.AddRange(new List<MathFunctionDescription> { lri.SetupLine(GetLineThickness(_p.SmoothStyle), null, null) });
                        _regressionComment = lri.GetRegressionComment(false);
                        break;

                    case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                    case ReportChartRegressionFitMode.DynamicSmoothCubic:
                    case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                    case ReportChartRegressionFitMode.DynamicSmoothE:
                    case ReportChartRegressionFitMode.DynamicSmoothLn:
                        Regression r = new Regression(_data.GetLDR(), LDRUsedFields.XY, _p.SmoothMode);
                        RegressionLine rl = new RegressionLine(r);
                        _fit.AddRange(new List<MathFunctionDescription> { rl.SetupLine(GetLineThickness(_p.SmoothStyle), null, null) });
                        _regressionComment = rl.GetRegressionComment(false);
                        break;
                        
                    case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                        LinearRegressionInfo lri2 = LineSmoothHelper.GetLinearRegression(_data.GetLDR());
                        _fit.AddRange(new List<MathFunctionDescription>
                        {
                            lri2.SetupLine(GetLineThickness(_p.SmoothStyle), null, null),
                            lri2.SetupPredictionLine(1, true, GetLineThickness(_p.SmoothStyle), null, null),
                            lri2.SetupPredictionLine(-1, true, GetLineThickness(_p.SmoothStyle), null, null),
                            lri2.SetupPredictionLine(1, false, GetLineThickness(_p.SmoothStyle), null, null),
                            lri2.SetupPredictionLine(-1, false, GetLineThickness(_p.SmoothStyle), null, null)
                        });
                        _regressionComment = lri2.GetRegressionComment(false);
                        break;
                }
            }
            else
            {
                var layers = _data.GetLayers();
                bool showRegression = layers.Count <= 5;
                var defaultColor = ColorConvertor.GetDefaultColorForScale(_p.ColorScale);
                foreach (var l in layers)
                {
                    var layerData = _data.GetLDRForLayer(l);
                    switch (_p.SmoothMode)
                    {
                        case ReportChartRegressionFitMode.DynamicSmoothLowess:
                            var lowessTable = Smooth.GetChartLowessTableForNumberCase(layerData, _p.FieldX, l, _p.LWSArea);
                            lowessTable.CustomProperties = new List<SLExtendedProperty> { new SLExtendedProperty{ Key = "ZColor", Value = l } };
                            _chartLowessTables.Add(lowessTable);
                            break;

                        case ReportChartRegressionFitMode.DynamicSmoothLine:
                            LinearRegressionInfo lri = LineSmoothHelper.GetLinearRegression(layerData);
                            if (l != null)_fit.AddRange(new List<MathFunctionDescription> { lri.SetupLine(GetLineThickness(_p.SmoothStyle), l, _selectedParameter) });
                            else _fit.AddRange(new List<MathFunctionDescription> { lri.SetupLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), l, _selectedParameter) });
                            if (showRegression) _regressionComment += $"\r\n<b>{l}</b>: {lri.GetRegressionComment(true)}";
                            break;

                        case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                        case ReportChartRegressionFitMode.DynamicSmoothCubic:
                        case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                        case ReportChartRegressionFitMode.DynamicSmoothE:
                        case ReportChartRegressionFitMode.DynamicSmoothLn:
                            Regression r = new Regression(layerData, LDRUsedFields.XY, _p.SmoothMode);
                            RegressionLine rl = new RegressionLine(r);

                            if (l!= null)_fit.AddRange(new List<MathFunctionDescription> { rl.SetupLine(GetLineThickness(_p.SmoothStyle), l, _selectedParameter) });
                            else _fit.AddRange(new List<MathFunctionDescription> {rl.SetupLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), l, _selectedParameter) });

                            if (showRegression) _regressionComment += $"\r\n<b>{l}</b>: {rl.GetRegressionComment(true)}";
                            break;
                            
                        case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                            LinearRegressionInfo lri2 = LineSmoothHelper.GetLinearRegression(layerData);
                            if (showRegression) _regressionComment += $"\r\n<b>{l}</b>: {lri2.GetRegressionComment(true)}";
                            if (l!=null)
                            _fit.AddRange(new List<MathFunctionDescription>
                            {
                                lri2.SetupLine(GetLineThickness(_p.SmoothStyle), l,_selectedParameter),
                                lri2.SetupPredictionLine(1, true, GetLineThickness(_p.SmoothStyle), l,_selectedParameter),
                                lri2.SetupPredictionLine(-1, true, GetLineThickness(_p.SmoothStyle), l,_selectedParameter),
                                lri2.SetupPredictionLine(1, false, GetLineThickness(_p.SmoothStyle), l,_selectedParameter),
                                lri2.SetupPredictionLine(-1, false, GetLineThickness(_p.SmoothStyle), l,_selectedParameter)
                            });
                            else
                            {
                                _fit.AddRange(new List<MathFunctionDescription>
                                {
                                    lri2.SetupLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), l,
                                        _selectedParameter),
                                    lri2.SetupPredictionLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), 1,
                                        true, l, _selectedParameter),
                                    lri2.SetupPredictionLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), -1,
                                        true, l, _selectedParameter),
                                    lri2.SetupPredictionLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), 1,
                                        false, l, _selectedParameter),
                                    lri2.SetupPredictionLine(null, defaultColor, GetLineThickness(_p.SmoothStyle), -1,
                                        false, l, _selectedParameter)
                                });
                            }
                            break;
                    }
                }
            }
        }
        
        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedParameter = _p.Fields.FirstOrDefault();

            var fi = new FieldsInfo
            {
                ColumnX = _p.FieldX,
                ColumnY = _selectedParameter,
                ColumnZ = _p.ColorField,
                ColumnW = _p.MarkerSizeField,
                ColumnV = _p.LabelField
            };

            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (ChartDescription cd in GetDescriptionForScatterPlot(_data, _p))
            {
                var chartScatterPlot = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    GlobalFontCoefficient = _p.FontSize,
                    EnableCellCondense = true,
                    ShowXHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowYHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = new List<SLDataTable>
                    {
                        _interactiveTable,
                        _chartRangesTable
                    },
                    FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis> { new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false } }, Editable = true },
                };
                chartScatterPlot.DataTables.AddRange(_chartLowessTables);
                chartScatterPlot.ToolTipRequired += ChartDynamics_ToolTipRequired;
                chartScatterPlot.SelectionChanged += ChartScatterPlot_SelectionChanged;
                chartScatterPlot.AxisClicked += chartScatterPlot_AxisClicked;
                chartScatterPlot.InteractiveRename += chartScatterPlot_InteractiveRename;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartScatterPlot });
                _surface.Children.Add(chartScatterPlot);
                chartScatterPlot.VisualReady += ChartScatterPlotOnVisualReady;
                chartScatterPlot.MakeChart();
            }
        }
        
        void chartScatterPlot_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}