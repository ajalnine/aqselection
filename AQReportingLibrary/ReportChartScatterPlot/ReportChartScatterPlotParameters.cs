﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public class ReportChartScatterPlotParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string FieldX { get; set; }
        public string ColorField { get; set; }
        public string MarkerSizeField { get; set; }
        public string LabelField { get; set; }
        public ReportChartRegressionFitMode SmoothMode { get; set; }
        public double LWSArea { get; set; }
        public ReportChartRangeStyle SmoothStyle { get; set; }
        public ReportChartLayerMode LayersMode { get; set; }
        public ReportChartTools Tools { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public Size LayoutSize {get;set; }
        public int LimitedLayers { get; set; }

        public SLDataTable UserLimits { get; set; }

        public bool MarkOutage { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }

        public ChartLegend ChartLegend { get; set; }
        public double FontSize { get; set; }
    }
}