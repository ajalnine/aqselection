﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;
using System.Windows.Media;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartScatterPlot
    {
        private IEnumerable<ChartDescription> GetDescriptionForScatterPlot(Data d, ReportChartScatterPlotParameters p)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count;
            var cd = new ChartDescription();
            result.Add(cd);
            if ((p.ColorField != null && p.ColorField != "Нет")
                || (p.LabelField != null && p.LabelField != "Нет")
                || (p.MarkerSizeField != null && p.MarkerSizeField != "Нет") || p.RangeType!= ReportChartRangeType.RangeIsNone) cd.ChartLegendType = ChartLegend.Right;

            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n.ToString(CultureInfo.InvariantCulture);
            cd.ChartSubtitle += (_chartRangesTable.Table.Count > 0)
                ? RangesHelper.GetDescription(p.RangeType)
                : String.Empty;
            cd.ChartSubtitle += _regressionComment;
            cd.ChartLegendType = _p.ChartLegend;

            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var withSurface = (_p.Tools & ReportChartTools.ToolSurface) > 0;
            var cutted = (_p.Tools & ReportChartTools.ToolCutSurfaces) > 0;
            var withVoronoy = (_p.Tools & ReportChartTools.ToolVoronoy) > 0;
            var withBag = (_p.Tools & ReportChartTools.ToolBag) > 0;
            var withHexBin = (_p.Tools & ReportChartTools.ToolHexBin) > 0;
            var withSurfaceOrHexBin = withSurface || withHexBin;
            var withText = (_p.Tools & ReportChartTools.ToolText) > 0;
            var withLog = (_p.Tools & ReportChartTools.ToolLog) > 0;

            if (withVoronoy)
            {
                AppendHelper(p, cd, "#Voronoy", ChartSeriesType.Voronoy, null);
            }

            if (withBag)
            {
                AppendHelper(p, cd, "#Bag", ChartSeriesType.Bag, null);
            }
            if ( !withHexBin  )
            {
                var csd = new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    XColumnName = p.FieldX,
                    XIsNominal = false,
                    XAxisTitle = p.FieldX,
                    SeriesTitle = _selectedParameter,
                    SeriesType = ChartSeriesType.LineXY,
                    YAxisTitle = _selectedParameter,
                    YColumnName = _selectedParameter,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    WColumnName = (p.MarkerSizeField != "Нет") ? p.MarkerSizeField : null,
                    VColumnName = (p.LabelField != "Нет") ? p.LabelField : null,
                    ColorColumnName = "Tag",
                    MarkerSizeColumnName = "Маркер",
                    LabelColumnName = "Метка",
                    ColorScale = p.ColorScale,
                    ColorScaleMode = p.ColorScaleMode,
                    MarkerMode = p.MarkerMode,
                    MarkerSizeMode = p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    ZAxisTitle = p.ColorField,
                    VAxisTitle = p.LabelField,
                    MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                    MarkerSize = withSurfaceOrHexBin ? 0 : (p.ColorField == "Нет" || p.MarkerSizeField != "Нет") ? 5 : 7,/////////////////
                };
                cd.ChartSeries.Add(csd);
            }

            #region Tools

            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0 
                    ?"Mean"
                    :"Median";
                AppendHelper(p, cd, "#RangeOnX", ChartSeriesType.RangeOnX, options + textenabled);
                AppendHelper(p, cd, "#RangeOnY", ChartSeriesType.RangeOnY, options + textenabled);
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                AppendHelper(p, cd, "#KDEOnY", ChartSeriesType.KDEOnY);
                AppendHelper(p, cd, "#KDEOnX", ChartSeriesType.KDEOnX);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                AppendHelper(p, cd, "#RugOnY", ChartSeriesType.RugOnY);
                AppendHelper(p, cd, "#RugOnX", ChartSeriesType.RugOnX);
            }

            if (withSurface)
            {
                AppendHelper(p, cd, "#Surface", ChartSeriesType.Surface, cutted && withSurface ? "Cutted" : null);
            }

            if (withHexBin)
            {
                AppendHelper(p, cd, String.Empty, ChartSeriesType.HexBin,  (withText ? "Text" : string.Empty) + (withLog ? "Log" : string.Empty));
            }

            #endregion

            RangesHelper.AppendRanges(cd, false, _chartRangesTable, p.RangeType == ReportChartRangeType.RangeIsSigmas || p.RangeType == ReportChartRangeType.RangeIsSlide || p.RangeType == ReportChartRangeType.RangeIsR, false, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            if (n > 15 && _chartLowessTables != null && p.SmoothMode == ReportChartRegressionFitMode.DynamicSmoothLowess)
            {
                var singleLowess = _chartLowessTables.Count == 1;
                foreach (var l in _chartLowessTables)
                {

                    var csd2 = new ChartSeriesDescription
                    {
                        SourceTableName = l.TableName,
                        XColumnName = p.FieldX,
                        XIsNominal = false,
                        XAxisTitle = p.FieldX,
                        YColumnName = "Lowess",
                        SeriesTitle = l.TableName,
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = singleLowess 
                        ? p.LayersMode == ReportChartLayerMode.Single ? "#ffFF4040" : ColorConvertor.GetDefaultColorForScale(_p.ColorScale)
                        : null,
                        ColorScale = p.ColorScale,
                        ColorScaleMode = p.ColorScaleMode,
                        LineSize = GetLineThickness(p.SmoothStyle),
                        ZValueForColor = l.CustomProperties?.SingleOrDefault(a => a.Key == "ZColor")?.Value,
                        ZValueSeriesTitle = _selectedParameter,
                        IsSmoothed = true,
                        YAxisTitle = _selectedParameter
                    };
                    cd.ChartSeries.Add(csd2);
                }
            }
            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;

            return result;
        }

        private void AppendHelper(ReportChartScatterPlotParameters p, ChartDescription cd, string seriesPrefix, ChartSeriesType helperType, string options = null)
        {
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Данные",
                XColumnName = p.FieldX,
                XIsNominal = false,
                XAxisTitle = p.FieldX,
                SeriesTitle = seriesPrefix + _selectedParameter,
                SeriesType = helperType,
                YAxisTitle = _selectedParameter,
                YColumnName = _selectedParameter,
                ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                ColorColumnName = "Tag",
                Options = options,
                ColorScale = p.ColorScale,
                ColorScaleMode = p.ColorScaleMode,
                MarkerMode = p.MarkerMode,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                ZAxisTitle = p.ColorField,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                MarkerSize = (p.ColorField == "Нет" || p.MarkerSizeField != "Нет") ? 5 : 7,
                ZValueSeriesTitle = (p.ColorField != "Нет" && !string.IsNullOrEmpty(seriesPrefix)) ? _selectedParameter : null,
            };
            cd.ChartSeries.Add(csd2);
        }
    }
}