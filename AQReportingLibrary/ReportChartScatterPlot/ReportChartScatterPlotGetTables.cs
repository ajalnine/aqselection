﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartScatterPlot
    {
        private List<SLDataTable> _mapTables;
        private void ShowCorrelationTables()
        {
            if (_selectedTable.Table.Count*_p.Fields.Count < 5e4 && _p.ColorField == "Нет")
            {
                var c = new Correlations(_selectedTable, _p.FieldX, _p.Fields, _p.ColorField, _worker);
                _mapTables = c.GetCorrelationTables();
                RefreshCorrelationTables();
                return;
            }

            SetMessage("Расчет матрицы парных корреляций...");
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _worker.DoWork += (o, e) =>
            {
                _data = null;
                _workerThread = Thread.CurrentThread;
                var c = new Correlations(_selectedTable, _p.FieldX, _p.Fields, _p.ColorField, _worker);
                _mapTables = c.GetCorrelationTables();
            };
            _worker.RunWorkerCompleted += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(RefreshCorrelationTables);
            };
            _worker.RunWorkerAsync();
        }

        private void RefreshCorrelationTables()
        {
            if (_mapTables == null || _mapTables.Count == 0) return;
            _surface.Children.Clear();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)),
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix =
                    _p.ColorField != "Нет" || _p.LayersMode == ReportChartLayerMode.Multiple
                        ? _p.ColorField + ": "
                        : string.Empty,
                FirstColumnIsCaseName = true,
                ColorIsBackground = true,
                IsInteractive = true,
                Title = GetChartTitle(),
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
            };
            rt.ActiveCellClicked += Rt_ActiveCellClicked;
            rt.ActiveCellToolTipRequired += Rt_ActiveCellToolTipRequired;
            rt.InteractiveRename += chartScatterPlot_InteractiveRename;
            rt.VisualReady += ChartScatterPlotOnVisualReady;
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs {InternalTables = _mapTables});
            rt.DrawReportTables(_mapTables, _surface);
        }

        private void Rt_ActiveCellToolTipRequired(object o, ActiveCellEventArgs e)
        {
            if (e.column == 0 || e.row == 0) return;

            if (e.column != e.row * 2 - 1) CreateTooltipScatterplot(o, e);
            else CreateTooltipHistogram(o, e);
        }

        private void CreateTooltipScatterplot(object o, ActiveCellEventArgs e)
        {
            var xField = e.source.ColumnNames[e.column];
            var yField = e.source.ColumnNames[e.row * 2 - 1];
            var toolTip = o as ToolTip;
            if (toolTip == null) return;
            EmitScatterPlot(toolTip, e.tag, xField, yField);
        }

        private void EmitScatterPlot(ToolTip o, object layer, string xField, string yField)
        {
            
            var data = new Data(_selectedTable, new FieldsInfo { ColumnX = xField, ColumnY = yField });
            var table = _selectedTable;
            var layerData = data.GetLDR();
            var xi = table.ColumnNames.IndexOf(xField);
            var yi = table.ColumnNames.IndexOf(yField);
            table = new SLDataTable
            {
                ColumnNames = _selectedTable.ColumnNames,
                DataTypes = _selectedTable.DataTypes,
                TableName = _selectedTable.TableName,
                Table = new List<SLDataRow>()
            };

            if (_p.ColorField != "Нет")
            {
                layerData = data.GetLDRForLayer(layer);
                var ci = table.ColumnNames.IndexOf(_p.ColorField);
                foreach (var row in _selectedTable.Table)
                {
                    if (row.Row[ci].ToString() == layer.ToString() && row.Row[xi] is double && row.Row[yi] is double) table.Table.Add(row);
                }
            }
            else
            {
                foreach (var row in _selectedTable.Table)
                {
                    if (row.Row[xi] is double && row.Row[yi] is double) table.Table.Add(row);
                }
            }

            LinearRegressionInfo lri = LineSmoothHelper.GetLinearRegression(layerData);

            var cd = new ChartDescription { ChartSeries = new List<ChartSeriesDescription>() };

            var csd = new ChartSeriesDescription
            {
                SourceTableName = table.TableName,
                XColumnName = xField,
                XIsNominal = false,
                XAxisTitle = xField,
                SeriesTitle = xField,
                SeriesType = ChartSeriesType.LineXY,
                YAxisTitle = yField,
                YColumnName = yField,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(_p.ColorScale),
                MarkerSize = 7
            };
            cd.ChartSeries.Add(csd);

            var chartScatterPlot = new Chart
            {
                EnableSecondaryAxises = false,
                Height = 240,
                Width = 300,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = true,
                ShowHorizontalGridlines = true,
                ShowHorizontalAxises = true,
                ShowVerticalAxises = true,
                ShowTitle = false,
                ShowSubtitle = false,
                IsTransparent = false,
                UseDefaultColor = false,
                Inverted = false,
                GlobalFontCoefficient = _p.FontSize,
                EnableCellCondense = true,
                ShowLegend = false,
                ShowTable = false,
                Margin = new Thickness(-5, 0, -5, 0),
                ChartData = cd,
                SourceName = _p.Table,
                MathFunctionDescriptions = new List<MathFunctionDescription> { lri.SetupLine(GetLineThickness(_p.SmoothStyle), null, xField) },
                DataTables = new List<SLDataTable>
                    {
                        table,
                    },
                FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis>(), Editable = false }
            };
            o.Content = chartScatterPlot;
            chartScatterPlot.MakeChart();
        }

        private void CreateTooltipHistogram(object o, ActiveCellEventArgs e)
        {
            var yField = e.source.ColumnNames[e.row * 2 - 1];
            var toolTip = o as ToolTip;
            if (toolTip == null) return;
            var data = new Data(_selectedTable, new FieldsInfo { ColumnX = yField, ColumnY = yField, ColumnZ = _p.ColorField=="Нет"? null: _p.ColorField });
            var table = _selectedTable;
            var layerData = data.GetLDR();
            ReportChartHistogramParameters rchp = CreateHistogramParameters(yField);

            var frequencies = new Frequencies(data, rchp, null, true, false);
            var frequencyTable = (_p.ColorField != "Нет") 
                ? frequencies.GetPartialFreqTable(e.tag) 
                : frequencies.GetFreqTable();
            var statinfo = (_p.ColorField != "Нет") 
                ? frequencies.GetGroupStatInfo().Single(a=>a.Group.ToString() == e.tag.ToString())    
                : frequencies.GetDataStatInfo();
            statinfo.Group = null;

            var mfd = new List<MathFunctionDescription>();
            
            mfd.Add(ReportChartStratification.SetupZ(rchp, statinfo, false));
            var cd = new ChartDescription { ChartSeries = new List<ChartSeriesDescription>() };

            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Частоты",
                XColumnName = yField,
                XIsNominal = false,
                XAxisTitle = yField,
                WColumnName = "_Отступ",
                VColumnName = "Шаг",
                ZColumnName = null,
                YColumnName = "Число наблюдений",
                SeriesTitle = "Частоты",
                SeriesType = ChartSeriesType.StairsOnX,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                LineSize = 1,
                YAxisTitle = "Число наблюдений",
                ColorColumnName = "Интервал"
            };

            cd.ChartSeries.Add(csd);

            var chartFreq = new Chart
            {
                EnableSecondaryAxises = false,
                Height = 240,
                Width = 300,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                ShowHorizontalZero = false,
                VerticalAxisSymmetry = false,
                ShowTitle = false,
                ShowSubtitle = false,
                IsTransparent = false,
                UseDefaultColor = false,
                EnableCellCondense = true,
                GlobalFontCoefficient = _p.FontSize,
                Inverted = false,
                ShowLegend = false,
                ShowTable = false,
                Margin = new Thickness(-5, 0, -5, 0),
                HideNegativeX = false,
                ChartData = cd,
                SourceName = _p.Table,
                MathFunctionDescriptions = mfd,
                DataTables = new List<SLDataTable> { frequencyTable },
                FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis>(), Editable = false },
            };
            chartFreq.FixedAxises.FixedAxisCollection.Add(new FixedAxis
            {
                Axis = "X",
                Step = statinfo.Step.Value,
                StepFixed = true,
                StepLocked = true
            });
            toolTip.Content = chartFreq;
            chartFreq.MakeChart();
        }

        private ReportChartHistogramParameters CreateHistogramParameters(string yField)
        {
            return new ReportChartHistogramParameters
            {
                AlignMode = ReportChartAlignMode.NoAlign,
                BinMode = ReportChartBinMode.IncludeRight,
                ColorField = _p.ColorField,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                ColumnsType = ReportChartColumnDiagramType.Generic,
                DetailLevel = 4,
                DrawParts = ReportChartDrawParts.Solid,
                Fields = new List<string> { yField },
                FitMode = ReportChartFitMode.FitGauss,
                FitStyle = ReportChartRangeStyle.Thick,
                LabelsType = ReportChartColumnsLabelsType.None,
                MarkerMode = _p.MarkerMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                Table = _p.Table,
                YMode = ReportChartFrequencyYMode.FrequencyInNumber,
                RangeType = ReportChartRangeType.RangeIsNone,
                RangeStyle = ReportChartRangeStyle.Invisible,
                FontSize = _p.FontSize,
                LayoutMode = _p.LayoutMode,
                LayoutSize = _p.LayoutSize,
                HistogramType = ReportChartHistogramType.Frequencies,
            };
        }

        private void Rt_ActiveCellClicked(object o, ActiveCellEventArgs e)
        {
            var source = e.source;
            var xField = e.source.ColumnNames[e.column * 2 - 1];
            var yField = e.source.ColumnNames[e.row * 2 - 1];
            Step step;
            step = CreateScatterplotParameters(xField, yField);
            this.InteractiveParametersChange(this, new InteractiveParametersEventArgs { Step = step });
        }

        private Step CreateScatterplotParameters(string xField, string yField)
        {
            Step step;
            if (xField != yField)
            {
                ReportChartScatterPlotParameters newp = new ReportChartScatterPlotParameters
                {
                    ColorField = _p.ColorField,
                    Fields = new List<string> { yField },
                    FieldX = xField,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    FixedAxisesDescription = _p.FixedAxisesDescription,
                    FontSize = _p.FontSize,
                    LabelField = _p.LabelField,
                    LayersMode = ReportChartLayerMode.Single,
                    LayoutMode = _p.LayoutMode,
                    LayoutSize = _p.LayoutSize,
                    MarkerMode = _p.MarkerMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    MarkerSizeField = _p.MarkerSizeField,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkOutage = _p.MarkOutage,
                    RangeStyle = _p.RangeStyle,
                    RangeType = _p.RangeType,
                    SmoothMode = _p.SmoothMode,
                    SmoothStyle = _p.SmoothStyle,
                    Table = _p.Table,
                    UserLimits = _p.UserLimits
                };
                step = StepConverter.GetStep(newp, GetChartName());
            }
            else step = StepConverter.GetStep(CreateHistogramParameters(yField), string.Empty);
            return step;
        }

        public Brush ConvertStringToStroke(string c)
        {
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            Brush stroke = new SolidColorBrush(Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff)));
            return stroke;
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 20,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }
    }
}