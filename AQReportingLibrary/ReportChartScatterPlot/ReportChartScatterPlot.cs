﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartScatterPlot : IReportElement
    {
        private List<SLDataTable> _chartLowessTables;
        private SLDataTable _chartRangesTable;
        private SLDataTable _interactiveTable;

        private Dictionary<string, SLDataTable> _interactiveTables;
        private List<string> _selectedParameters;
        private Dictionary<string, Data> _datas;

        private BackgroundWorker _worker;
        private Thread _workerThread;
        private string _probabilityComment = string.Empty;
        private string _regressionComment = string.Empty;
        private Dictionary<string, string> _regressionComments = new Dictionary<string, string>();
        private string _selectedParameter;
        private SLDataTable _selectedTable;
        private Data _data;
        private Limits _limits;
        private List<MathFunctionDescription> _fit;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType;

        private Step _step;
        private ReportChartScatterPlotParameters _p;

        public ReportChartScatterPlot(ReportChartScatterPlotParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public ReportChartScatterPlot(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartScatterPlotParameters>(s);
            _step = s;
        }
        public void SetNewParameters(ReportChartScatterPlotParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.LayersMode == ReportChartLayerMode.TableOnly)
            {
                if (_p.ColorField!="Нет")return String.Format(@"<b>{0}</b>: стратификация парных корреляций", _p.Table);
                
            }
            if (_p.LayersMode == ReportChartLayerMode.Extended) return String.Format(@"<b>{0}</b>: круговой граф парных корреляций", _p.Table);
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return String.Format(@"<b>{0}</b>: {3} <b>{1}</b> и <b>{2}</b>", _p.Table, _p.Fields[0], _p.FieldX, GetChartName());
            return String.Format(@"<b>{0}</b>: {1}", _p.Table, GetChartName());
        }


        public string GetChartDescription()
        {
            if (_p.LayersMode == ReportChartLayerMode.TableOnly)
            {
                if (_p.ColorField != "Нет") return String.Format(@"{0}: стратификация парных корреляций", _p.Table);

            }
            if (_p.LayersMode == ReportChartLayerMode.Extended) return String.Format(@"{0}: круговой граф парных корреляций", _p.Table);
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return String.Format(@"{0}: {3} {1} и {2}", _p.Table, _p.Fields[0], _p.FieldX, GetChartName());
            return String.Format(@"{0}: {1}", _p.Table, GetChartName());
        }

        public string GetChartName()
        {
            if ((_p.Tools & ReportChartTools.ToolVoronoy) > 0) return "диаграмма Вороного";
            if ((_p.Tools & ReportChartTools.ToolHexBin) > 0) return "карта плотности";
            if ((_p.Tools & ReportChartTools.ToolSurface) > 0) return "триангуляция Делоне";
            return "график рассеяния";
        }
    }
}