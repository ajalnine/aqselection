﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public class Smooth
    {
        public static SLDataTable GetChartLowessTableForDateCase(List<DynamicRow> ldr, string currentDateField, object group, double LWSArea, string YparameterName = "Lowess")
        {
            if (ldr == null || ldr.Count == 0) return null;

            var x = new List<object>();
            var xtoFilter = new List<object>();
            var ytoFilter = new List<object>();
            var firstOrDefault   = ldr.FirstOrDefault(a => a.Xdate.HasValue);
            if (firstOrDefault != null)
            {
                var oldDate = firstOrDefault.Xdate;
                if (oldDate == null) return null;
                double sum = 0, count = 0;
                foreach (DynamicRow r in ldr)
                {
                    if (!r.Xdate.HasValue) continue;
                    var currentDate = r.Xdate.Value;
                    count++;
                    sum += r.Y.HasValue? r.Y.Value : 0;

                    if (currentDate.Year == oldDate.Value.Year && currentDate.Month == oldDate.Value.Month && currentDate.Day == oldDate.Value.Day) continue;
                    xtoFilter.Add(currentDate.Ticks/1e9);
                    x.Add(currentDate);
                    ytoFilter.Add(sum/count);
                    count = 0;
                    oldDate = currentDate;
                    sum = 0;
                }
            }
            var filtered = AQMath.LowessFilter(xtoFilter, ytoFilter, 0.5 * LWSArea, 2);
            var sldtl = new SLDataTable
                {
                    ColumnNames = (new[] {currentDateField, YparameterName }).ToList(),
                    DataTypes = (new[] {"DateTime", "Double"}).ToList(),
                    TableName = ((group != null) ? "#" : string.Empty) + "Lowess" + ((group != null) ? group.ToString() : string.Empty),
                    Table = new List<SLDataRow>()
                };
            for (int i = 0; i < filtered.Count; i++)
            {
                sldtl.Table.Add(new SLDataRow {Row = new List<object> {x[i], filtered[i]}});
            }
            return sldtl;
        }

        public static SLDataTable GetChartLowessTableForNumberCase(List<DynamicRow> ldr, string currentXField, object group, double LWSArea, string YparameterName = "Lowess")
        {
            if (ldr == null || ldr.Count == 0) return null;

            var xtoFilter = new List<object>();
            var ytoFilter = new List<object>();

            var l = ldr.OrderBy(a => a.X).ToList();
            var each = l.Count / 1000.0d;

            var step = each < 1.0d ? 1.0 : 1.0d / (double)l.Count * each;

            for (var i = 0.0d; i < l.Count; i += each)
            {
                xtoFilter.Add(l[(int)i].X);
                ytoFilter.Add(l[(int)i].Y);
            }

            if (each > 2.0d)
            {
                xtoFilter = AQMath.MovingAverageFilter(xtoFilter, (int)Math.Ceiling((double)each));
                ytoFilter = AQMath.MovingAverageFilter(ytoFilter, (int)Math.Ceiling((double)each));
            }

            List<object> filtered = AQMath.LowessFilter(xtoFilter, ytoFilter, 0.5 * LWSArea, 2);
            var sldtl = new SLDataTable
            {
                ColumnNames = (new[] {currentXField, YparameterName }).ToList(),
                DataTypes = (new[] {"Double", "Double"}).ToList(),
                TableName = ((group != null) ? "#" : string.Empty) + "Lowess" + ((group != null) ? group.ToString() : string.Empty),
                Table = new List<SLDataRow>()
            };

            for (int i = 0; i < filtered.Count; i++)
            {
                sldtl.Table.Add(new SLDataRow {Row = new List<object> {xtoFilter[i], filtered[i]}});
            }
            return sldtl;
        }

        public static SLDataTable GetChartLowessTableForGroupAndXCase(List<DynamicRow> ldr, string yField, double LWSArea)
        {
            if (ldr == null || ldr.Count == 0) return null;

            var xtoFilter = new List<object>();
            var ytoFilter = new List<object>();

            var c = 0D;
            var each = ldr.Count / 1000.0d;
            var step = each < 1.0d ? 1.0 : 1.0d / (double)ldr.Count * each;

            for (var i = 0.0d; i < ldr.Count; i += each)
            {
                xtoFilter.Add(c);
                ytoFilter.Add(ldr[(int)i].X);
                c += step;
            }

            if (each > 2.0d)
            {
                xtoFilter = AQMath.MovingAverageFilter(xtoFilter, (int)Math.Ceiling((double)each));
                ytoFilter = AQMath.MovingAverageFilter(ytoFilter, (int)Math.Ceiling((double)each));
            }

            List<object> filtered = AQMath.LowessFilter(xtoFilter, ytoFilter, 0.5 * LWSArea, 2);
            var sldtl = new SLDataTable
            {
                ColumnNames = (new[] { "#Группа#", yField }).ToList(),
                DataTypes = (new[] { "String", "Double" }).ToList(),
                TableName = "Lowess",
                Table = new List<SLDataRow>()
            };
            for (int i = 0; i < filtered.Count; i++)
            {
                sldtl.Table.Add(new SLDataRow { Row = new List<object> { ldr[i].Group.ToString(), filtered[i] } });
            }
            return sldtl;
        }

        public static SLDataTable GetChartLowessTableForGroupAndYCase(List<DynamicRow> ldr,  string yField, double LWSArea)
        {
            if (ldr == null || ldr.Count == 0) return null;

            var xtoFilter = new List<object>();
            var ytoFilter = new List<object>();
            var groups = new List<object>();

            var c = 0D;

            var each = ldr.Count / 1000.0d;
            var step = each < 1.0d ? 1.0 : 1.0d / (double)ldr.Count * each;

            for (var i = 0.0d; i<ldr.Count; i+=each)
            {
                xtoFilter.Add(c);
                ytoFilter.Add(ldr[(int)i].Y);
                groups.Add(ldr[(int)i].Group);
                c += step;
            }

            if (each > 2.0d)
            {
                xtoFilter = AQMath.MovingAverageFilter(xtoFilter, (int)Math.Ceiling((double)each));
                ytoFilter = AQMath.MovingAverageFilter(ytoFilter, (int)Math.Ceiling((double)each));
            }
            
            List<object> filtered = AQMath.LowessFilter(xtoFilter, ytoFilter, 0.5 * LWSArea, 2);
            var sldtl = new SLDataTable
            {
                ColumnNames = (new[] { "#Группа#", yField }).ToList(),
                DataTypes = (new[] { "String", "Double" }).ToList(),
                TableName = "Lowess",
                Table = new List<SLDataRow>()
            };

            for (int i = 0; i < filtered.Count; i++)
            {
                sldtl.Table.Add(new SLDataRow { Row = new List<object> { groups[i] !=null ? groups[i].ToString() : string.Empty , filtered[i] } });
            }
            return sldtl;
        }
    }
}