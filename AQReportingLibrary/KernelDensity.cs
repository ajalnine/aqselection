﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;

namespace AQReportingLibrary
{
    public class KernelDensity
    {
        private readonly List<object> _data;
        private double? _iqr, _s;
        private readonly double _n;
        private readonly double _h;
        private readonly double _h2;
        private readonly double _k;

        public KernelDensity(List<DynamicRow> ldr)
        {
            _n = ldr.Count;
            _data = ldr.Select(a => a.Y).Cast<object>().ToList();
            _iqr = AQMath.AggregateIQR(_data);
            _s = AQMath.AggregateStDev(_data);
            _h = EstimateBinSize();
            _h2 = EstimateBinSize2();
            _k = (1 / (Math.Sqrt(2 * Math.PI)));
        }

        private double EstimateBinSize()
        {
            if (!_iqr.HasValue|| !_s.HasValue) return 0;
            double sigma = Math.Min(_s.Value, _iqr.Value/1.34);
            return 0.9 * sigma / Math.Pow(_n, 1 / 5);
        }

        private double EstimateBinSize2()
        {
            if (!_s.HasValue) return 0;
            return _s.Value * Math.Pow(4 / (3 * _n), 0.2);
        }

        public double GetDensity(double x)
        {
            double a = 1/ _n /_h2;
            double result = _data.Sum(v => FastGaussian((x - (double) v)/_h2));
            result *= a;
            return result;
        }
        
        public double GetDensitySilverman(double x)
        {
            double a = 1 / _n / _h;
            double result = _data.Sum(v => FastGaussian((x - (double)v) / _h));
            result *= a;
            return result;
        }

        public double FastGaussian(double x)
        {
            return  _k * Math.Exp(-(Math.Pow(x, 2) / 2));
        }
    }
}