﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;

namespace AQReportingLibrary
{
    public partial class ReportChartProbabilityPlot
    {
        Panel _surface;
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            if (_probabilityTables == null || _probabilityTables.Count == 0 || _probabilityTables.Any(a => a.Value.Table.Count == 0)) return false;
            SetupFit();
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _probabilityTables.Values.ToList() });
            return true;
        }

        public void Cancel() { }

        public void RefreshVisuals()
        {
            if (_probabilityTables == null || _probabilityTables.Count == 0 || _probabilityTables.Any(a => a.Value.Table.Count == 0)) return;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _probabilityTables.Values.ToList() });
        }

        private void SetupFit()
        {
            _fit = new List<MathFunctionDescription>
            {
                new MathFunctionDescription
                {
                    Description = "X=Y",
                    LineColor = "#ffff0000",
                    LineSize = 3,
                    PixelStep = 5,
                    MathFunctionCalculator = (ari, x) => x
                }
            };
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameters = _p.Fields;
            _datas = new Dictionary<string, Data>();
            _probabilityComments = new Dictionary<string, string>();
            _probabilityTables = new Dictionary<string, SLDataTable>();
            foreach (var sp in _selectedParameters)
            {
                var fi = new FieldsInfo { ColumnY = sp };
                var data = new Data(_selectedTable, fi);
                _datas.Add(sp, data);
                var probability = new Probability(data.GetLDR());
                var pt = probability.GetTable();
                pt.TableName = "Вероятности" + sp;
                _probabilityTables.Add(sp, pt);
                _probabilityComments.Add(sp, probability.GetDistributionFitComment());
            }
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (ChartDescription cd in GetDescriptionForProbabilityPlot())
            {
                var chartProbabilityPlot = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    GlobalFontCoefficient = _p.FontSize,
                    EnableCellCondense = true,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                            new FixedAxis
                            {
                                Axis = "X",
                                AxisMin = 0.0d,
                                AxisMinLocked = true,
                                AxisMinFixed = true,
                                AxisMax = 1.0d,
                                AxisMaxLocked = true,
                                AxisMaxFixed = true,
                                Step = 0.1d,
                                StepLocked = true,
                                StepFixed = true
                            },
                            new FixedAxis
                            {
                                Axis = "Y",
                                AxisMin = 0.0d,
                                AxisMinLocked = true,
                                AxisMinFixed = true,
                                AxisMax = 1.0d,
                                AxisMaxLocked = true,
                                AxisMaxFixed = true,
                                Step = 0.1d,
                                StepLocked = true,
                                StepFixed = true
                            }
                        },
                        Editable = false
                    },
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = _fit,
                    DataTables = _probabilityTables.Values.ToList()
                };
                chartProbabilityPlot.ToolTipRequired += ChartDynamics_ToolTipRequired;
                chartProbabilityPlot.AxisClicked += chartProbabilityPlot_AxisClicked;
                chartProbabilityPlot.InteractiveRename += chartProbabilityPlot_InteractiveRename;
                chartProbabilityPlot.VisualReady += ChartProbabilityPlot_VisualReady;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartProbabilityPlot });
                _surface.Children.Add(chartProbabilityPlot);
                chartProbabilityPlot.MakeChart();
            }
        }

        private void ChartProbabilityPlot_VisualReady(object o, AQBasicControlsLibrary.VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs());
        }
    }
}