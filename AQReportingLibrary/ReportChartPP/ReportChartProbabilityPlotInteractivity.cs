﻿using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartProbabilityPlot
    {
       void chartProbabilityPlot_AxisClicked(object o, AxisEventArgs e)
        {
            if (AxisClicked != null) AxisClicked.Invoke(this, e);
        }
        
        private void ChartDynamics_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null)return;
            var worktable = _probabilityTables[e.Info.SeriesName];

            var comment = worktable.Table[e.Info.Index].Row[2].ToString();
            var wp2 = new WrapPanel {Orientation = Orientation.Vertical, MaxHeight = 200};
            e.TT.Content = wp2;
             var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = comment,
                    Margin = new Thickness(0, 0, 20, 0)
                };
            wp2.Children.Add(tb);
        }

        void chartProbabilityPlot_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            if (ReportInteractiveRename != null) ReportInteractiveRename(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}