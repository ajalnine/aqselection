﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartProbabilityPlot : IReportElement
    {
        private List<string> _selectedParameters;
        private Dictionary<string, string> _probabilityComments;
        private SLDataTable _selectedTable;
        private Dictionary<string, SLDataTable> _probabilityTables;
        private Dictionary<string, Data> _datas;
        private Probability _probability;
        private List<MathFunctionDescription> _fit;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;

        private Step _step;
        private ReportChartProbabilityPlotParameters _p;
        public ReportChartProbabilityPlot(ReportChartProbabilityPlotParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartProbabilityPlot(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartProbabilityPlotParameters>(s);
            _step = s;
        }
        public void SetNewParameters(ReportChartProbabilityPlotParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public Step GetStep()
        {
            return _step;
        }
        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return $@"<b>{_p.Table}</b>: карта накопленных вероятностей <b>{_p.Fields[0]}</b>";
            return $@"<b>{_p.Table}</b>: карта накопленных вероятностей";
        }
        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Fields.Count == 1) return $@"{_p.Table}: карта накопленных вероятностей {_p.Fields[0]}";
            return $@"{_p.Table}: карта накопленных вероятностей";
        }
        
        public string GetChartName()
        {
            return "Карта накопленных вероятностей";
        }
    }
}