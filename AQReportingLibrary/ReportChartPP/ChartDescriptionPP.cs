﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartProbabilityPlot
    {
        private IEnumerable<ChartDescription> GetDescriptionForProbabilityPlot()
        {
            var result = new List<ChartDescription>();
            var cd = new ChartDescription();
            result.Add(cd);
            var colorPosition = 0.4d;

            var spNumber = _selectedParameters.Count;
            var colorStep = 1.0d / (spNumber+1);
            cd.ChartTitle = GetChartTitle();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            cd.ChartLegendType = _p.ChartLegend;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            foreach (var sp in _selectedParameters)
            {
                var color = ColorConvertor.GetColorStringForScale(_p.ColorScale, colorPosition);
                if (spNumber <= 4)
                {
                    if (!string.IsNullOrEmpty(cd.ChartSubtitle)) cd.ChartSubtitle += "\r\n";
                    cd.ChartSubtitle += $"n(<b>{sp}</b>)=" +
                                        _datas[sp].GetDataTable()
                                            .Table.Count(a => a.Row[2] != null)
                                            .ToString(CultureInfo.InvariantCulture) + _probabilityComments[sp];
                }
                var csd = new ChartSeriesDescription
                {
                    SourceTableName = "Вероятности" + sp,
                    XColumnName = "Расчетная вероятность",
                    XIsNominal = false,
                    XAxisTitle = "Расчетная вероятность",
                    YColumnName = "Наблюдаемая вероятность",
                    ColorColumnName = null,
                    SeriesTitle = sp,
                    SeriesType = ChartSeriesType.LineXY,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerColor =color,
                    LineColor = color,
                    MarkerMode = _p.MarkerMode,
                    LineSize = 2,
                    MarkerSize = 6,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    YAxisTitle = "Наблюдаемая вероятность"
                };
                cd.ChartSeries.Add(csd);

                colorPosition += colorStep;
                if (colorPosition > 1) colorPosition -= 1;
            }
            return result;
        }
    }
}