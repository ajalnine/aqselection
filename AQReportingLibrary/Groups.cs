﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class Groups
    {
        private readonly SLDataTable _chartGroupStatisticsTable, _chartGroupDescriptionTable;
        private List<DynamicRow> _groupData;
        public bool ChartGroupIsSorted = false;
        public bool IsMean;
        public bool LayeredMode;
        private bool _groupIsDate;
        private bool _groupHasTime, _groupIsMonth, _groupIsYear, _groupIsMonthAndYear, _groupIsNumber;
        private const string MarkColor = "#FFEF6060";
        private ReportChartGroupsParameters _rcgp;
        public int NumberOfLayers, NumberOfGroups;
        public Groups(IEnumerable<DynamicRow> ldr, SLDataTable selectedTable, ReportChartGroupsParameters rcgp, Limits limits)
        {
            _rcgp = rcgp;
            var center = rcgp.Center;
            var withOutliers = rcgp.ShowOutliers;
            var sorted = rcgp.Sorted;
            var groupField = rcgp.GroupField;
            var showText = rcgp.ShowText;
            var markLimits = rcgp.MarkOutage;
            var pointMode = rcgp.PointMode;

            IsMean = center == ReportChartGroupCenter.Mean;
            var sldtstat = new SLDataTable
                {
                    TableName = "Статистика",
                    Table = new List<SLDataRow>(),
                    DataTypes =   (new[] {"String", "GroupDescription", "Double", "Object", "Object"}).ToList(),
                    ColumnNames = (new[] {"#Группа#", "Статистика", IsMean ? "Средние" : "Медианы", "Значение", "Слой"}).ToList()
                };

            var sldtDescriptionTable = new SLDataTable
            {
                TableName = "Статистика",
                Table = new List<SLDataRow>(),
                DataTypes = (new[] { "String", "String", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double" }).ToList(),
                ColumnNames = (new[] { "Группа", "Слой", "Число наблюдений", "Среднее" , "Медиана", "σ", "Min", "Max", "Ящик min", "Ящик max", "Ус min", "Ус max" }).ToList()
            };

            if (sorted)GetSortedData(ldr, selectedTable, groupField);
            else GetUnsortedData(ldr, selectedTable, groupField);
            CheckIsGroupIsDate(selectedTable, groupField);
            CheckIsGroupIsNumber(selectedTable, groupField);
            CheckIsGroupHasTime();
            LayeredMode = (rcgp.AnalisysMode == ReportChartGroupAnalisysMode.Multiple || rcgp.AnalisysMode == ReportChartGroupAnalisysMode.Extended) && rcgp.ColorField != "Нет";

            var groups =  (from l in _groupData.Where(a => a !=null) group l by new { Group = l.Group, Z= LayeredMode ? l.Z : null}).ToList();
            

            var sCount = rcgp.AnalisysMode == ReportChartGroupAnalisysMode.Extended ? (from l in _groupData.Where(a => a != null) group l by l.Group).Count() : groups.Count;

            var layers = _groupData.Select(a => a.Z).Distinct().ToList();
            var smallestZ = layers.FirstOrDefault(a=>a!=null && a!=DBNull.Value && !string.IsNullOrEmpty(a.ToString()));
            if (rcgp.Sorted)
            {
                groups = groups.Where(a => a.Key.Group != null && !string.IsNullOrEmpty(a.Key.Group.ToString()) && a.Key.Group != DBNull.Value).OrderBy(a => a.Key.Group).ThenBy(a => (a.Key.Z != null && !string.IsNullOrEmpty(a.Key.ToString()) && a.Key.Z != DBNull.Value) ? a.Key.Z : smallestZ ).ToList();
                layers = layers.OrderBy(a => (a != null && !string.IsNullOrEmpty(a.ToString()) && a != DBNull.Value) ? a : smallestZ).ToList();
            }
            NumberOfGroups = groups.Count;
            NumberOfLayers = layers.Count();
            var scaled =  ((rcgp.ShowText & LabelsType.Scaled) == LabelsType.Scaled) ;
            var showLabels = ((rcgp.ShowText & LabelsType.Pie) == LabelsType.Pie);
            var showLayerLabels = ((rcgp.ShowText & LabelsType.LayerName) == LabelsType.LayerName);

            foreach (var group in groups)
            {
                var data = group.Select(g => g.Y).Cast<object>().ToList();
                var indexedData = group.Select(g => new {value = g.Y, index = g.Index, tag = g.Color, Layer = g.Z}).ToList();
                var values = group.Where(a=>a!=null && a.Y != null).Select(g => g.Y).Cast<double>().ToList();
                if (!values.Any()) continue;
                var groupName = GetGroupName(group.Key.Group, rcgp.AnalisysMode == ReportChartGroupAnalisysMode.Multiple? group.Key.Z: null, NumberOfGroups);
                var gd = new GroupDescription();
                if (NumberOfGroups < 40) gd.CustomComments = "n=" + group.Count();
                var average = AQMath.AggregateMean(data);
                var median = AQMath.AggregateMedian(data);
                gd.Layer = group.Key.Z;
                gd.TotalLayersCount = NumberOfLayers;
                gd.LayerIndex = layers.IndexOf(gd.Layer);
                gd.TotalGroupsCount = sCount;
                gd.UseLayerOffset = rcgp.AnalisysMode == ReportChartGroupAnalisysMode.Extended;
                gd.LowLimit = limits.MinLimit;
                gd.HighLimit = limits.MaxLimit;
                gd.PieData = (from g in @group group g by g.Z).Select(a => new PieDescription {Z=a.Key, Part = a.Count() / (double)group.Count()}).OrderByDescending(a=>a.Part).ToList();
                foreach (var p in gd.PieData) { p.Text = 
                        ((showLabels) ? Math.Round(p.Part * 100, 2).ToString() + "%" : string.Empty) 
                        + (showLayerLabels && showLabels ? "\r\n" : string.Empty)
                        + (showLayerLabels ? p.Z?.ToString() :string.Empty); }
                if (scaled) foreach (var p in gd.PieData) { p.LabelSize = p.Part; }
                if (gd.LowLimit.HasValue)
                {
                    gd.Lower = indexedData.Count(a => a.value < gd.LowLimit);
                    gd.LowerPercent = Math.Round((double)gd.Lower * 100.0d / indexedData.Count,2);
                }
                if (gd.HighLimit.HasValue)
                {
                    gd.Higher = indexedData.Count(a => a.value > gd.HighLimit);
                    gd.HigherPercent = Math.Round((double)gd.Higher * 100.0d / indexedData.Count, 2);
                }
                gd.Min = values.Min();
                gd.Max = values.Max();
                gd.PointMode = pointMode;
                var stDev = AQMath.AggregateStDev(data);
                var stdErr = stDev / Math.Sqrt(data.Count);

                if (!IsMean)
                {
                    var q1 = AQMath.AggregatePercentile(data, 25);
                    var q3 = AQMath.AggregatePercentile(data, 75);
                    var iqr = (q3 - q1)*1.5;
                    var iqr2 = (q3 - q1)*3;
                    var nonOutlierMin = q1 - iqr;
                    var nonOutlierMax = q3 + iqr;
                    var extremesMin = q1 - iqr2;
                    var extremesMax = q3 + iqr2;
                    IEnumerable<double> data2;
                    if (withOutliers) data2 = from d in data.Cast<Double>() where d >= nonOutlierMin && d <= nonOutlierMax select d;
                    else data2 = from d in data.Cast<Double>() select d;
                    var enumerable = data2 as IList<double> ?? data2.ToList();
                    
                    gd.Center = median;
                    if (scaled) gd.LabelSize = median;
                    gd.ShowText = showText;
                    gd.IQR = q3 - q1;
                    gd.BoxMin = q1;
                    gd.BoxMax = q3;
                    gd.WhiskerMin = enumerable.Min();
                    gd.WhiskerMax = enumerable.Max();
                    gd.N = data.Count;
                    gd.IndexedData = (from d in indexedData select new PointDescription
                                       {
                                           Value = d.value.Value,
                                           OriginalIndex = d.index, 
                                           Tag = d.tag,
                                           Layer = d.Layer
                                       }).ToList();
                    if (gd.LowLimit.HasValue && markLimits)
                    {
                        foreach (var p in gd.IndexedData.Where(a=>a.Value < gd.LowLimit.Value))
                        {
                            p.Tag = MarkColor;
                        }
                    }
                    if (gd.HighLimit.HasValue && markLimits)
                    {
                        foreach (var p in gd.IndexedData.Where(a => a.Value > gd.HighLimit.Value))
                        {
                            p.Tag = MarkColor;
                        }
                    }
                    var tags = gd.IndexedData.Select(a => a.Tag).Distinct().ToList();
                    if (tags.Count() == 1) gd.Tag = tags.FirstOrDefault();
                    if (withOutliers)
                    {
                        gd.Outliers = (from d in gd.IndexedData
                                                             where
                                                                 d.Value < nonOutlierMin ||
                                                                 d.Value > nonOutlierMax &&
                                                                 (d.Value >= extremesMin && d.Value <= extremesMax)
                                                             select d).ToList();
                        gd.Extremes = (from d in gd.IndexedData
                                                             where d.Value < extremesMin || d.Value > extremesMax
                                                             select d).ToList();
                    }
                }
                else
                {
                    gd.Center = average;
                    if (scaled) gd.LabelSize = average;
                    gd.ShowText = showText;
                    gd.BoxMin = average - stdErr;
                    gd.BoxMax = average + stdErr;
                    gd.WhiskerMin = average - stDev;
                    gd.WhiskerMax = average + stDev;
                    gd.N = data.Count;
                    gd.StDev = stDev;
                    gd.IndexedData = (from d in indexedData select new PointDescription { Value = d.value.Value, OriginalIndex = d.index, Tag = d.tag, Layer = d.Layer }).ToList();
                    if (gd.LowLimit.HasValue && markLimits)
                    {
                        foreach (var p in gd.IndexedData.Where(a => a.Value < gd.LowLimit.Value))
                        {
                            p.Tag = MarkColor;
                        }
                    }
                    if (gd.HighLimit.HasValue && markLimits)
                    {
                        foreach (var p in gd.IndexedData.Where(a => a.Value > gd.HighLimit.Value))
                        {
                            p.Tag = MarkColor;
                        }
                    }
                    var tags = gd.IndexedData.Select(a => a.Tag).Distinct().ToList();
                    if (tags.Count() == 1) gd.Tag = tags.FirstOrDefault();
                    if (withOutliers)
                    {
                        var nonOutlierMin = average - 1.5 * stDev;
                        var nonOutlierMax = average + 1.5 * stDev;
                        var extremesMin = average - 3 * stDev;
                        var extremesMax = average + 3 * stDev;

                        gd.Outliers = (from d in gd.IndexedData
                                                             where
                                                                 d.Value < nonOutlierMin ||
                                                                 d.Value > nonOutlierMax &&
                                                                 (d.Value >= extremesMin && d.Value <= extremesMax) select d).ToList();
                        gd.Extremes = (from d in gd.IndexedData
                                                             where d.Value < extremesMin || d.Value > extremesMax select d).ToList();
                    }
                }
                sldtstat.Table.Add(new SLDataRow { Row = new List<object> { groupName, gd, IsMean ? average : median, @group.Key.Group, group.Key.Z } });
                sldtDescriptionTable.Table.Add(new SLDataRow { Row = new List<object> { @group.Key.Group,@group.Key.Z?.ToString(), gd.N, average, median, stDev, gd.Min, gd.Max, gd.BoxMin, gd.BoxMax, gd.WhiskerMin, gd.WhiskerMax} });
            }
            _chartGroupStatisticsTable = sldtstat;
            _chartGroupDescriptionTable = sldtDescriptionTable;
        }

        private string GetGroupName(object group, object Z, int groupNumber)
        {
            if (_groupIsNumber)
            {
                var groupName1 = ((double?)group)?.ToString(CultureInfo.InvariantCulture);
                if (Z != null) groupName1 += (groupNumber > 40 ? ", <#80808080>" + Z.ToString() + "</#>" : "\r\n<#80808080>" + Z.ToString() + "</#>");
                return groupName1;
            }
            var groupName = (_groupHasTime || !_groupIsDate)
                ? group.ToString() :
                _groupIsMonthAndYear ? ((DateTime)group).ToString("yyyy MMMM") :
                _groupIsYear ? ((DateTime)group).ToString("yyyy") :
                _groupIsMonth ? ((DateTime)group).ToString("MMMM") : ((DateTime)group).ToString("dd.MM.yyyy");
            if (Z != null) groupName += (groupNumber > 40 ? ", <#80808080>" + Z.ToString() + "</#>" : "\r\n<#80808080>" + Z.ToString() + "</#>");
            return groupName;
        }

        private void CheckIsGroupIsDate(SLDataTable selectedTable, string groupField)
        {
            var dt = selectedTable.DataTypes[selectedTable.ColumnNames.IndexOf(groupField)];
            _groupIsDate = dt == "System.DateTime" || dt == "DateTime";
        }
        private void CheckIsGroupIsNumber(SLDataTable selectedTable, string groupField)
        {
            var dt = selectedTable.DataTypes[selectedTable.ColumnNames.IndexOf(groupField)];
            _groupIsNumber = dt == "System.Double" || dt == "Double";
        }

        private void CheckIsGroupHasTime()
        {
            if (!_groupIsDate) return;
            var g = _groupData.Select(d => (DateTime?) d.Group).ToList();
            _groupHasTime = _groupIsDate && g.Any(date => date.HasValue && (date.Value.TimeOfDay.Ticks>0));
            _groupIsMonth = !_groupHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1));
            _groupIsYear = !_groupHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1 && date.Value.Month == 1));
            _groupIsMonthAndYear = !_groupHasTime && g.All(date => !date.HasValue || (date.Value.Day == 1)) && g.Where(date => date.HasValue).Select(a=>a.Value.Year).Distinct().Count()>1 && g.Where(date => date.HasValue).Select(a => a.Value.Month).Distinct().Count() > 1; 
        }

        private void GetSortedData(IEnumerable<DynamicRow> ldr, SLDataTable selectedTable, string groupField)
        {
            var index = selectedTable.ColumnNames.IndexOf(groupField);
            if (index < 0)
            {
                _groupData = ldr.ToList();
                ChartGroupIsSorted = false;
                return;
            }
            var dt = selectedTable.DataTypes[index];
            switch (dt)
            {
                default:
                    _groupData = ldr.Where(a => a.Group != null).ToList();
                    ChartGroupIsSorted = false;
                    break;
                case "Double":
                case "System.Double":
                    _groupData = (from l in ldr
                              where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                              orderby (double?) l.Group
                              select l).ToList();
                    ChartGroupIsSorted = true;
                    break;
                case "DateTime":
                case "System.DateTime":
                    _groupData = (ldr.Where(l => l.Group is DateTime).OrderBy(l => (DateTime) l.Group)).ToList();
                    ChartGroupIsSorted = true;
                    break;
                case "String":
                case "System.String":
                    var groupnames =
                        (from l in ldr where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString()) select l.Group.ToString())
                            .Distinct().ToList();
                    foreach ( var format in new []{"MMMM yyyy","yyyy MMMM","MMMM", "yyyy", "yy MMMM", "MMMM yy", "yy MM", "MM yy"})
                    {
                        DateTime dateTimeResult;
                        if (!groupnames.All(groupname => DateTime.TryParseExact(groupname, format, CultureInfo.CurrentCulture,
                                    DateTimeStyles.AllowWhiteSpaces, out dateTimeResult))) continue;
                        SetSortedByDateFormat(ldr, format);
                        return;
                    }
                    _groupData = (from l in ldr
                    where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                    orderby l.Group.ToString()
                    select l).ToList();
                    ChartGroupIsSorted = false;
                    break;
            }
        }

        private void GetUnsortedData(IEnumerable<DynamicRow> ldr, SLDataTable selectedTable, string groupField)
        {
            var i = selectedTable.ColumnNames.IndexOf(groupField);
            if (i<0)
            {
                _groupData = ldr.ToList();
                ChartGroupIsSorted = false;
                return;
            }
            var dt = selectedTable.DataTypes[i];
            switch (dt)
            {
                default:
                    _groupData = ldr.Where(a => a.Group != null).ToList();
                    break;
                case "Double":
                case "System.Double":
                    _groupData = (from l in ldr
                                 where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                                 select l).ToList();
                    break;
                case "DateTime":
                case "System.DateTime":
                    _groupData = ldr.Where(l => l.Group is DateTime).ToList();
                    break;
                case "String":
                case "System.String":
                    var groupnames =
                        (from l in ldr where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString()) select l.Group.ToString())
                            .Distinct().ToList();
                    foreach (var format in new[] { "MMMM yyyy", "yyyy MMMM", "MMMM", "yyyy", "yy MMMM", "MMMM yy", "yy MM", "MM yy" })
                    {
                        DateTime dateTimeResult;
                        if (!groupnames.All(groupname => DateTime.TryParseExact(groupname, format, CultureInfo.CurrentCulture,
                                    DateTimeStyles.AllowWhiteSpaces, out dateTimeResult))) continue;
                        _groupData = (from l in ldr
                                     where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                                     select l).ToList();
                        return;
                    }
                    _groupData = (from l in ldr
                                 where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                                 select l).ToList();
                    break;
            }
            ChartGroupIsSorted = false;
        }
        private void SetSortedByDateFormat(IEnumerable<DynamicRow> ldr, string format)
        {
            _groupData = (from l in ldr
                where l.Group != null && !String.IsNullOrEmpty(l.Group.ToString())
                orderby DateTime.ParseExact(l.Group.ToString(), format, CultureInfo.CurrentCulture)
                select l).ToList();
            ChartGroupIsSorted = true;
        }

        public SLDataTable GetGroupTable()
        {
            return _chartGroupStatisticsTable;
        }

        public SLDataTable GetGroupDescriptionTable()
        {
            return _chartGroupDescriptionTable;
        }
    }
}