﻿using ApplicationCore.CalculationServiceReference;
using AQMathClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using AQChartLibrary;

namespace AQReportingLibrary
{
    class Correlations
    {
        private Dictionary<object, CorrelationInfo[,]> _maps;
        private string _xfield, _layerField;
        private List<string> _yfields;
        private SLDataTable _sldt;
        public Correlations(SLDataTable sldt, string xfield, List<string> yfields, string layerField, BackgroundWorker worker)
        {
            _maps = new Dictionary<object, CorrelationInfo[,]>();
            _sldt = sldt;
            _xfield = xfield;
            _yfields = yfields;
            _layerField = layerField;

            if (layerField != "Нет")
            {
                var layers = sldt.GetColumnData(layerField)
                    .Where(a => a != null && a.ToString() != string.Empty)
                    .Distinct()
                    .OrderBy(a => a);

                foreach (var l in layers)
                {
                    _maps.Add(l, GetCorrelationMatrix(sldt, xfield, yfields, layerField, l, worker));
                }
            }
            else
            {
                _maps.Add(string.Empty, GetCorrelationMatrix(sldt, xfield, yfields, null, null, worker));
            }
        }
    

        public Dictionary<object, CorrelationInfo[,]> GetMaps()
        {
            return _maps;
        }

        private static Tuple<double,double> CheckCorrelation(SLDataTable sldt, string xfield, string yfield, string layerField, object layer)
        {
            if (yfield == null || xfield == null) return new Tuple<double, double>(0.0d, 1.0d);
            int xindex = sldt.ColumnNames.IndexOf(xfield);
            int yindex = sldt.ColumnNames.IndexOf(yfield);
            int layerIndex = layerField=="Нет" ? -1 : sldt.ColumnNames.IndexOf(layerField);
            double n = 0;
            double sumX = 0, sumY = 0, sumX2 = 0, sumXy = 0, sumY2 = 0, x, y, r;
            foreach (SLDataRow sldr in sldt.Table)
            {
                if (sldr.Row[xindex] == null || string.IsNullOrEmpty(sldr.Row[xindex].ToString()) ||
                    sldr.Row[yindex] == null || string.IsNullOrEmpty(sldr.Row[yindex].ToString()) || (layerIndex!=-1 && (sldr.Row[layerIndex] == null || sldr.Row[layerIndex].ToString()!=layer.ToString())))
                    continue;
                x = (double)sldr.Row[xindex];
                y = (double)sldr.Row[yindex];
                sumX += x;
                sumY += y;
                sumX2 += x * x;
                sumXy += x * y;
                sumY2 += y * y;
                n++;
            }
            if (sumX==0 || sumY==0) return new Tuple<double, double>(double.NaN, double.NaN);
            r = (n * sumXy - sumX * sumY) / (Math.Sqrt((n * sumX2 - sumX * sumX) * (n * sumY2 - sumY * sumY)));
            double? t = r * Math.Sqrt(n - 2) / Math.Sqrt(1 - r * r);
            double? p = (t <= 0) ? 2 * AQMath.StudentCD(t, n - 2) : 2 * (1 - AQMath.StudentCD(t, n - 2));
            if (!p.HasValue || double.IsNaN(r)) return new Tuple<double, double>(double.NaN, double.NaN);
            return new Tuple<double, double>(r, p.Value);
        }

        private static CorrelationInfo[,] GetCorrelationMatrix(SLDataTable sldt, string xfield, List<string> yfields, string layerField, object layer, BackgroundWorker worker)
        {
            var fields = (new[] { xfield }).Union(yfields).ToList();
            var n = fields.Count;
            var rmap = new CorrelationInfo[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = i; j < n; j++)
                {
                    if (i==j)
                    {
                        rmap[i, j] = new CorrelationInfo {r = 1, p= 0, xfield = fields[i], yfield = fields[j] };
                        continue;
                    }
                    var r = CheckCorrelation(sldt, fields[i], fields[j], layerField, layer);
                    if (worker!=null && worker.CancellationPending) return null;
                    var mapElement = new CorrelationInfo { r = r.Item1, p = r.Item2, xfield = fields[i], yfield = fields[j] };
                    rmap[i, j] = mapElement;
                    rmap[j, i] = mapElement;
                }
            }
            return rmap;
        }

        public List<SLDataTable> GetCorrelationTables()
        {
            var res = new List<SLDataTable>();
            foreach (var map in _maps)
            {
                var correlationMap = map.Value;
                if (correlationMap == null) return null;
                var m = correlationMap.GetLength(0);
                if (m == 0) return null;
                var result = new SLDataTable
                {
                    Table = new List<SLDataRow>(),
                    Tag = map.Key.ToString(),
                    TableName = map.Key.ToString(),
                    ColumnNames = new List<string> {"Фактор"},
                    ColumnTags = new List<object>(),
                    CustomProperties =
                        new List<SLExtendedProperty>
                        {
                            new SLExtendedProperty {Key = "FirstColumnIsCaseName", Value = true}
                        },
                    DataTypes = new List<string>() {"String"}
                };
                for (int i = 0; i < m; i++)
                {
                    result.ColumnNames.Add(correlationMap[i, i].xfield);
                    result.DataTypes.Add("Double");
                    result.ColumnNames.Add("#" + correlationMap[i, i].xfield + "_Цвет");
                    result.DataTypes.Add("String");
                }

                var table = new List<SLDataRow>();
                for (var i = 0; i < m; i++)
                {
                    var row = new SLDataRow {Row = new List<object>()};
                    row.Row.Add(correlationMap[i, i].yfield);
                    for (var j = 0; j < m; j++)
                    {
                        var v = correlationMap[i, j].r;
                        if (double.IsNaN(v))
                        {
                            row.Row.Add(null);
                            row.Row.Add(Colors.White.ToString());
                            continue;
                        }
                        row.Row.Add(Math.Round(v, 2));
                        var alpha = i==j ? 0: (byte) (Math.Round(Math.Abs(v*127)/8,0)*8);

                        var color =
                            Color.FromArgb((byte) 255, (byte) 255, (byte) (255 - alpha), (byte) (255 - alpha))
                                .ToString();
                        row.Row.Add(color);
                    }
                    table.Add(row);
                }
                result.Table = table;
                res.Add(result);
            }
            return res;
        }

        public List<SLDataTable> GetCorrelationCircularGraphData()
        {
            var res = new List<SLDataTable>();
            var dt = _layerField == "Нет" ? "String" : _sldt.DataTypes[_sldt.ColumnNames.IndexOf(_layerField)];

            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string> { "Фактор1", "Фактор2", "Корреляция", _layerField },
                ColumnTags = new List<object>(),
                DataTypes = new List<string>() { "String", "String", "Double", dt },
                TableName = "Граф"
            };

            var table = new List<SLDataRow>();
            result.Table = table;

            foreach (var map in _maps)
            {
                var CorrelationMap = map.Value;
                if (CorrelationMap == null) return null;
                var m = CorrelationMap.GetLength(0);
                if (m == 0) return null;
                
                for (var i = 0; i < m; i++)
                {
                    for (var j = i; j < m; j++)
                    {
                        //if (i == j) continue;
                        var row = new SLDataRow { Row = new List<object>() };
                        var cor = CorrelationMap[i, j];
                        row.Row.Add(cor.xfield);
                        row.Row.Add(cor.yfield);
                        row.Row.Add(Math.Round(Math.Abs(cor.r),1));
                        row.Row.Add(map.Key);
                        table.Add(row);
                    }
                }
                res.Add(result);
            }
            return res;
        }
    }
    
    public class CorrelationInfo
    {
        public double r;
        public double p;
        public string xfield;
        public string yfield;
    }
}
