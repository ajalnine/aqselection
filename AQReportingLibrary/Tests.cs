﻿using ApplicationCore.CalculationServiceReference;
using AQMathClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public enum TestType
    {
        T,
        Welch,
        MU
    }

    class Tests
    {
        private Dictionary<object, TestInfo[,]> _maps;
        private string _xfield, _layerField, _groupField;
        private List<string> _yfields;
        private Dictionary<object, List<object>> _groupData = new Dictionary<object, List<object>>();
        private List<object> _groups = new List<object>();
        private SLDataTable _sldt;
        private TestType _testType;
        private delegate Tuple<double?, double?> TestMethod(List<object> g1, List<object> g2);

        private static Dictionary<TestType, TestMethod> testers = new Dictionary<TestType, TestMethod>
        {
            {TestType.T, AQMath.TTest},
            {TestType.MU, AQMath.UTest},
            {TestType.Welch, AQMath.TTestWelch},
        };


        public Tests(SLDataTable sldt, string xfield, string groupfield, string layerField, BackgroundWorker worker, TestType testType)
        {
            _maps = new Dictionary<object, TestInfo[,]>();
            _sldt = sldt;
            _xfield = xfield;
            _groupField = groupfield;
            _layerField = layerField;
            _testType = testType;

            if (layerField != "Нет")
            {
                var layers = sldt.GetColumnData(layerField)
                    .Where(a => a != null && a.ToString() != string.Empty)
                    .Distinct()
                    .OrderBy(a => a);

                foreach (var l in layers)
                {
                    _maps.Add(l, GetTestsMatrix(sldt, xfield, groupfield, layerField, l, testType, worker));
                }
            }
            else
            {
                _maps.Add("", GetTestsMatrix(sldt, xfield, groupfield, "Нет", null, testType, worker));
            }
        }
    
        public Dictionary<object, TestInfo[,]> GetMaps()
        {
            return _maps;
        }

        private TestInfo[,] GetTestsMatrix(SLDataTable sldt, string xfield, string groupField, string layerField, object layer, TestType testType, BackgroundWorker worker)
        {
            _groups = sldt.GetLayerList(groupField).OrderBy(a=>a).ToList();
            var data = layer != null ? sldt.ExtractLayer(layerField, layer) : sldt;
            _groupData = new Dictionary<object, List<object>>();
            
            foreach (var g in _groups)
            {
                _groupData.Add(g, data.GetRowDataForValueFromField(xfield, g, groupField));
            }

            var n = _groups.Count;

            var tmap = new TestInfo[n, n];
            for (int i = 0; i < n; i++)
            {
                var groupI = _groups[i];
                for (int j = i; j < n; j++)
                {
                    var groupJ = _groups[j];
                    if (i==j)
                    {
                        tmap[i, j] = new TestInfo { t = double.NaN, p= double.NaN, group1 = _groups[i]?.ToString(), group2 = _groups[j]?.ToString() };
                        continue;
                    }
                    var t = testers[testType](_groupData[groupI], _groupData[groupJ]);
                    if (worker!=null && worker.CancellationPending) return null;
                    var mapElement = t!=null 
                        ? new TestInfo { t = t.Item1.Value, p = t.Item2.Value, group1 = _groups[i]?.ToString(), group2 = _groups[j]?.ToString() }
                        : new TestInfo { t = double.NaN, p = double.NaN, group1 = _groups[i]?.ToString(), group2 = _groups[j]?.ToString() };
                    ;
                    tmap[i, j] = mapElement;
                    tmap[j, i] = mapElement;
                }
            }
            return tmap;
        }

        public List<SLDataTable> GetTestsTables()
        {
            var res = new List<SLDataTable>();
            foreach (var map in _maps)
            {
                var testsMap = map.Value;
                if (testsMap == null) return null;
                var m = testsMap.GetLength(0);
                if (m == 0) return null;
                var result = new SLDataTable
                {
                    Table = new List<SLDataRow>(),
                    Tag = map.Key.ToString(),
                    TableName = map.Key.ToString(),
                    ColumnNames = new List<string> {"n="},
                    ColumnTags = new List<object>(),
                    CustomProperties =
                        new List<SLExtendedProperty>
                        {
                            new SLExtendedProperty {Key = "FirstColumnIsCaseName", Value = true}
                        },
                    DataTypes = new List<string>() {"String"}
                };
                for (int i = 0; i < m; i++)
                {
                    result.ColumnNames.Add(testsMap[i, i].group1);
                    result.DataTypes.Add("Double");
                    result.ColumnNames.Add("#" + testsMap[i, i].group1 + "_Цвет");
                    result.DataTypes.Add("String");
                }

                var table = new List<SLDataRow>();
                for (var i = 0; i < m; i++)
                {
                    var row = new SLDataRow {Row = new List<object>()};
                    row.Row.Add(testsMap[i, i].group2);
                    for (var j = 0; j < m; j++)
                    {
                        if (i != j)
                        {
                            var v = testsMap[i, j].p;
                            if (double.IsNaN(v))
                            {
                                row.Row.Add(null);
                                row.Row.Add(Colors.White.ToString());
                                continue;
                            }
                            row.Row.Add(Math.Round(testsMap[i, j].p, 2));
                            var alpha = i == j ? 0 : (byte) (Math.Round(Math.Abs(v*127)/8, 0)*8);

                            var color =
                                Color.FromArgb((byte) 255, (byte) 255, (byte) (255 - alpha), (byte) (255 - alpha))
                                    .ToString();
                            row.Row.Add(color);
                        }
                        else
                        {
                            row.Row.Add(AQMath.AggregateCount(_groupData[_groups[i]]));
                            row.Row.Add("#ffe8e8e8");
                        }
                    }

                    table.Add(row);
                }
                result.Table = table;
                res.Add(result);
            }
            return res;
        }

        public List<SLDataTable> GetTestsCircularGraphData()
        {
            var res = new List<SLDataTable>();
            var dt = _layerField == "Нет" ? "String" : _sldt.DataTypes[_sldt.ColumnNames.IndexOf(_layerField)];

            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                ColumnNames = new List<string> { "Группа1", "Группа2", "#pt", _layerField },
                ColumnTags = new List<object>(),
                DataTypes = new List<string>() { "String", "String", "Double", dt },
                TableName = "Граф"
            };

            var table = new List<SLDataRow>();
            result.Table = table;

            foreach (var map in _maps)
            {
                var CorrelationMap = map.Value;
                if (CorrelationMap == null) return null;
                var m = CorrelationMap.GetLength(0);
                if (m == 0) return null;
                
                for (var i = 0; i < m; i++)
                {
                    for (var j = i; j < m; j++)
                    {
                        var cor = CorrelationMap[i, j];
                        var p = double.IsNaN(cor.p) ? 0.0d : (double?)Math.Round(Math.Abs(cor.p), 1);
                        var row = new SLDataRow { Row = new List<object> { cor.group1, cor.group2, p, map.Key } };
                        table.Add(row);
                    }
                }
                res.Add(result);
            }
            return res;
        }
    }
    
    public class TestInfo
    {
        public double t;
        public double p;
        public string group1;
        public string group2;
    }
}
