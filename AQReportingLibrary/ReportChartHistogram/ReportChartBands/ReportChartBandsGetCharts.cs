﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;
using System.Windows.Media;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartBands
    {
        private SLDataTable _chartRangesYTable;
        private BackgroundWorker _worker;
        private Thread _workerThread;
        private Limits _limits;
        private string _probabilityComment = string.Empty;
        private List<object> _othersLayer = new List<object>();
        Panel _surface;
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            var table = _data.GetDataTable();
            if (table == null || table.Table.Count == 0) return false;

            if (table.Table.Count > 5000)
            {
                _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
                _worker.RunWorkerCompleted += (o, e) =>
                {
                    _surface.Dispatcher.BeginInvoke(() =>
                    {
                        CreateCharts();
                        InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable }});
                    });
                };

                _worker.DoWork += (o, e) =>
                {
                    PrepareChartData();
                    SetupLimits(dataTables);
                };
                SetMessage("Построение графика...");
                _worker.RunWorkerAsync();
            }
            else
            {
                PrepareChartData();
                SetupLimits(dataTables);
                CreateCharts();
                InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable } });
            }
            return true;
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 30,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }
        public void RefreshVisuals()
        {
            if (_data.GetDataTable() == null || _data.GetDataTable().Table.Count == 0 || _columnsTable == null || _columnsTable.Table.Count == 0) return;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _columnsTable } });
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields[0];
            var fi = ((new[] { Aggregate.N, Aggregate.Percent, Aggregate.PercentInCategory}).Contains(_p.Aggregate)) 
                ? new FieldsInfo { ColumnGroup = _p.Fields[0], ColumnY = _p.Fields[0], ColumnZ = _p.ColorField }
                : new FieldsInfo { ColumnGroup = _p.Fields[0], ColumnY = _p.Fields.Count > 1 ? _p.Fields[1] : _p.Fields[0], ColumnZ = _p.ColorField, ColumnV = _p.Fields.Count > 2 ? _p.Fields[2] : null };

            _originalLayersType = _selectedTable.GetDataType(fi.ColumnZ);
            _originalYType = _selectedTable.GetDataType(fi.ColumnGroup);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = fi.ColumnZ,
                    Y = _p.Fields[0],
                    ValuesForAggregation = fi.ColumnY,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation,
                    Method = _p.Aggregate,
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
        }

        private void PrepareChartData()
        {
            SetupTotals();
            _columns = new Columns(_p, _data, false);
            _columnsTable = _columns.GetColumnsTable();
        }

        private void SetupTotals()
        {
            if (((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total ||
                 (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent) &&
                _p.ColumnType == ReportChartColumnDiagramType.Generic && _p.ColorField != "Нет")
            {

                var _p2 = new ReportChartHistogramParameters
                {
                    Fields = _p.Fields,
                    FontSize = _p.FontSize * 1.3,
                    LabelsType =
                        (((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total)
                            ? ReportChartColumnsLabelsType.N
                            : ReportChartColumnsLabelsType.None) |
                        (((_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) ==
                          ReportChartColumnsLabelsType.TotalPercent)
                            ? ReportChartColumnsLabelsType.Percent
                            : ReportChartColumnsLabelsType.None),
                    Table = _p.Table,
                    Aggregate = _p.Aggregate,
                    AggregateDescription = _p.AggregateDescription,
                    AlignMode = _p.AlignMode,
                    BinMode = _p.BinMode,
                    ColorField = null,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    ColumnType = _p.ColumnType,
                    ColumnsOrder = _p.ColumnsOrder,
                    ColumnsType = _p.ColumnsType,
                    DetailLevel = _p.DetailLevel,
                    DrawParts = ReportChartDrawParts.None,
                    FitMode = _p.FitMode,
                    FitStyle = _p.FitStyle,
                    FixedAxisesDescription = _p.FixedAxisesDescription,
                    GroupNumber = _p.GroupNumber,
                    HistogramType = _p.HistogramType,
                    LayoutMode = _p.LayoutMode,
                    LayoutSize = _p.LayoutSize,
                    MarkerMode = _p.MarkerMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    RangeStyle = _p.RangeStyle,
                    RangeType = _p.RangeType,
                    Step = _p.Step,
                    Tools = ReportChartTools.ToolNone,
                    UserAlignValue = _p.UserAlignValue,
                    UserLimits = _p.UserLimits,
                    YMode = _p.YMode
                };
            }
        }
        private void SetupLimits(IEnumerable<SLDataTable> dataTables)
        {
            _limits = new Limits(_data.GetLDR(),
                _p.RangeType,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.Dynamic,
                _p.UserLimits,
                Limits.GetExternalLimitsTable(dataTables, _selectedTable),
                _selectedParameter, false, _p.RangeStyle);
            _chartRangesYTable = _limits.GetRangesYTable();
        }
        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (var cd in GetDescriptionForColumns(_data, _p))
            {
                var chartColumns = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowVerticalZero = _p.ColumnsType == ReportChartColumnDiagramType.Opposite,
                    HorizontalAxisSymmetry = _p.ColumnsType == ReportChartColumnDiagramType.Opposite,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    EnableCellCondense = true,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    HideMinorGridLines = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable> { _interactiveTable, _columnsTable, _chartRangesYTable},
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                            new FixedAxis{},
                            new FixedAxis { DataType = _originalYType, Axis = "Y", Format = "dd.MM.yyyy", StringIsDateTime = _originalYType?.ToLower().Contains("datetime") ?? false},
                            new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false }
                        },
                        Editable = false
                    }
                };

                var axis = chartColumns.FixedAxises.FixedAxisCollection[0];
                
                if (_p.ColumnsType != ReportChartColumnDiagramType.Opposite)
                {
                    axis.Axis = "X";
                    axis.AxisMin = 0.0d;
                    axis.AxisMinLocked = false;
                    axis.AxisMinFixed = true;
                }
                if (_p.ColumnsType == ReportChartColumnDiagramType.Normed)
                {
                    axis.Axis = "X";
                    axis.AxisMax = 100.0d;
                    axis.AxisMaxFixed = true;
                    axis.AxisMaxLocked = true;
                    axis.AxisMinLocked = true;
                }
                
                if ((_p.DrawParts & ReportChartDrawParts.Line) == ReportChartDrawParts.Line ||
                    (_p.DrawParts & ReportChartDrawParts.Marker) == ReportChartDrawParts.Marker)
                {
                    if (_p.ColorField != "Нет")
                    {
                        var groups = _columns.GetGroups();
                        foreach (var group in groups)
                        {
                            var partialTable = _columns.GetPartialColumnsTable(group);
                            partialTable.TableName = "Диаграмма:" + (group == null ? "null" : group.ToString());
                            chartColumns.DataTables.Add(partialTable);
                        }
                    }
                }

                chartColumns.ToolTipRequired += ChartColumns_ToolTipRequired;
                chartColumns.SelectionChanged += ChartColumns_SelectionChanged; ;
                chartColumns.InteractiveRename += chartColumns_InteractiveRename;
                chartColumns.AxisClicked += ChartColumns_AxisClicked;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartColumns });
                _surface.Children.Add(chartColumns);
                chartColumns.VisualReady += ChartColumnsOnVisualReady;
                chartColumns.MakeChart();
            }
        }

        private void ChartColumnsOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = new List<SLDataTable>() { _columnsTable } });
        }
    }
}