﻿using System;
using System.Windows;
using System.Windows.Controls;
using AQChartLibrary;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class ReportChartBands
    {
        private void ChartColumns_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null || _columnsTable == null) return;
            var binName = _columnsTable.Table[e.Info.Index].Row[2].ToString();
            var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = binName,
                    Margin = new Thickness(2)
                };
            e.TT.Content = tb;
        }


        private void ChartColumns_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (_selectedTable == null || _columnsTable == null || e.Table == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs { AvailableOperations = InteractiveOperations.None, });
                return;
            }
            var rowList = new List<int>();
            var message = _selectedParameter + " = ";
            var isFirst = true;

            foreach (var ii in e.InteractivityIndexes)
            {
                if (!isFirst) message += "\r\n";
                var color = e.Table.Table[ii.Index].Row[12];
                var bin = e.Table.Table[ii.Index].Row[11];
                var category = e.Table.Table[ii.Index].Row[5];

                if (_p.Aggregate == Aggregate.Single)
                {
                    message += $"\"{bin}\"";
                    rowList.Add((int)e.Table.Table[ii.Index].Row[13]);
                }
                else
                {
                    if (_p.ColorField == "Нет")
                    {
                        message += $"\"{bin}\"";
                        if (!_selectedTable.ColumnNames.Contains("#" + _data.GetFieldsInfo().ColumnY + "_Цвет")) rowList.AddRange(_selectedTable.GetRowsForValueFromField(_selectedParameter, bin));
                        else rowList.AddRange(_selectedTable.GetRowsForValueFromFieldAndLayer(_selectedParameter, bin, "#" + _data.GetFieldsInfo().ColumnY + "_Цвет", color));
                    }
                    else
                    {
                        message += $"\"{bin}\" для категории {category}";
                        if (!_selectedTable.ColumnNames.Contains("#" + _data.GetFieldsInfo().ColumnY + "_Цвет")) rowList.AddRange(_selectedTable.GetRowsForValueFromFieldAndLayer(_selectedParameter, bin, _p.ColorField, category));
                        else rowList.AddRange(_selectedTable.GetRowsForValueFromFieldLayerAndColor(_selectedParameter, bin, _p.ColorField, category, "#" + _data.GetFieldsInfo().ColumnY + "_Цвет", color));
                    }
                }
                isFirst = false;
            }
            Interactivity?.Invoke(this,
                new InteractivityEventArgs
                {
                    Table = _selectedTable.TableName,
                    Field = _selectedParameter,
                    TargetField = _data.GetFieldsInfo().ColumnY,
                    Message = message,
                    AvailableOperations = InteractiveOperations.All,
                    SelectedCases = rowList.Distinct().ToList(),
                    Clear = true
                });
        }
        
        void ChartColumns_AxisClicked(object o, AxisEventArgs e)
        {
            AxisClicked?.Invoke(this, e);
        }

        void chartColumns_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}