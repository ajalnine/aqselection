﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartBands
    {
        private IEnumerable<ChartDescription> GetDescriptionForColumns(Data d, ReportChartHistogramParameters p)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();

            string yName = p.Fields[0];
            string xName = p.Fields.Count > 1 && p.Aggregate != Aggregate.N && p.Aggregate != Aggregate.Percent ? p.Fields[1] : string.Empty;
            var xaxisTitle = p.ColumnsType == ReportChartColumnDiagramType.Normed
                ? "%"
                : p.AggregateDescription + (!String.IsNullOrWhiteSpace(yName) ? " " + xName : string.Empty);
            result.Add(cd);
            var blockWhite = (_p.ColumnsType == ReportChartColumnDiagramType.Layers ||
                              _p.ColumnType == ReportChartColumnDiagramType.Opposite ||
                              _p.ColumnType == ReportChartColumnDiagramType.ByCategory);
            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = (p.ColorField != "Нет") || (_chartRangesYTable.Table.Count > 0) ? _p.ChartLegend : ChartLegend.None;
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var smallDefaultOption = (_p.LabelsType & ReportChartColumnsLabelsType.SmallDefault) ==
                                     ReportChartColumnsLabelsType.SmallDefault
                ? "SmallDefault"
                : (_p.LabelsType & ReportChartColumnsLabelsType.SmallOver) ==
                  ReportChartColumnsLabelsType.SmallOver
                    ? "SmallOver"
                    : (_p.LabelsType & ReportChartColumnsLabelsType.SmallSkip) == ReportChartColumnsLabelsType.SmallSkip
                        ? "SmallSkip"
                        : string.Empty;

            var labelWideLimited = (_p.LabelsType & ReportChartColumnsLabelsType.LabelWideLimited) == ReportChartColumnsLabelsType.LabelWideLimited ? "WideLimited" : string.Empty;



            if ((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total ||
                 (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) ==
                 ReportChartColumnsLabelsType.TotalPercent)
            {
                var csdtotal = new ChartSeriesDescription
                {
                    SourceTableName = "Диаграмма",
                    YColumnName = yName,
                    YAxisTitle = yName,
                    YIsNominal = false,
                    XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Итог, %" : "Итог",
                    WColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Отступ, %" : "Отступ",
                    VColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Отступ категории" : null,
                    ZColumnName = null,
                    MarkerSizeColumnName = ((_p.LabelsType & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled) ? "Размер" : null,
                    LabelColumnName = _p.LabelsType != ReportChartColumnsLabelsType.None ? "Подписи итогов" : null,
                    SeriesTitle = "_" + p.AggregateDescription + "_",
                    SeriesType = ChartSeriesType.StairsOnY,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = MarkerModes.Invisible,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    Options = (_p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "GroupProcessing" : "StringProcessing")
                              + (_p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Normed" : string.Empty)
                              + (blockWhite ? "BlockWhite" : string.Empty),
                    LineSize = 0.5,
                    XAxisTitle = xaxisTitle
                };
                cd.ChartSeries.Add(csdtotal);
            }
            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Диаграмма",
                YColumnName = yName,
                YAxisTitle = yName,
                YIsNominal = false,
                XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                WColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Отступ, %" : "Отступ",
                VColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Отступ категории" : null,
                ZColumnName = (p.ColorField != "Нет")
                    ? p.ColorField == _p.Fields[0]
                        ? p.ColorField + " "
                        : p.ColorField
                    : null,
                MarkerSizeColumnName = ((_p.LabelsType & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled) ? "Размер" : null,
                LabelColumnName = _p.LabelsType != ReportChartColumnsLabelsType.None ? "Подписи" : null,
                SeriesTitle = p.AggregateDescription,
                SeriesType = ChartSeriesType.StairsOnY,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                ColorColumnName = "Цвет",
                MarkerMode = (_p.DrawParts& ReportChartDrawParts.Solid)==ReportChartDrawParts.Solid ? _p.MarkerMode : MarkerModes.Invisible,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                Options = (_p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "GroupProcessing" : "StringProcessing")
                + (_p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Normed" : string.Empty)
                + (blockWhite ? "BlockWhite" : string.Empty) + smallDefaultOption + labelWideLimited,
                LineSize = 0.5,
                XAxisTitle = xaxisTitle
            };
            cd.ChartSeries.Add(csd);


            
            if ((_p.DrawParts & ReportChartDrawParts.Line) == ReportChartDrawParts.Line)
            {
                if (_p.ColorField == "Нет")
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Диаграмма",
                        YColumnName = yName,
                        YAxisTitle = _selectedParameter,
                        XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                        SeriesTitle = "#Полигон",
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        ColorColumnName = "Цвет",
                        LineSize = GetLineThickness(p.FitStyle),
                        MarkerShapeMode = _p.MarkerShapeMode,
                        XAxisTitle = xaxisTitle,
                        MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        MarkerSize = 2,
                    });
                }
                else
                {
                    var groups = _columns.GetGroups();
                    foreach (var group in groups)
                    {
                        cd.ChartSeries.Add(new ChartSeriesDescription
                        {
                            SourceTableName = "Диаграмма:" + (group == null ? "null" : group.ToString()),
                            YColumnName = yName,
                            YAxisTitle = _selectedParameter,
                            ZColumnName = (p.ColorField != "Нет")
                                ? p.ColorField == _p.Fields[0]
                                    ? p.ColorField + " "
                                    : p.ColorField
                                : null,
                            XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                            VColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Отступ категории" : null,
                            MarkerSizeColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Число категорий" : null,
                            Options = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "GroupProcessingY" : null,
                            SeriesTitle = "#Полигон" + (group == null ? "null" : group.ToString()),
                            SeriesType = ChartSeriesType.LineXY,
                            ColorScale = _p.ColorScale,
                            ColorColumnName = "Цвет",
                            ColorScaleMode = _p.ColorScaleMode,
                            ZValueForColor = group,
                            ZValueSeriesTitle = p.AggregateDescription,
                            MarkerShapeMode = _p.MarkerShapeMode,
                            LineSize = 2,
                            XAxisTitle = xaxisTitle,
                            MarkerSize = 2
                        });
                    }
                }
            }

            var total = (_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total;
            var totalPercent = (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent;
            var connected = (_p.LabelsType & ReportChartColumnsLabelsType.Connected) == ReportChartColumnsLabelsType.Connected;
            if ((total || totalPercent) && connected)
            {
                cd.ChartSeries.Add(new ChartSeriesDescription
                {
                    SourceTableName = "Диаграмма",
                    YColumnName = yName,
                    YAxisTitle = _selectedParameter,
                    XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                    SeriesTitle = "#Огибающая",
                    SeriesType = ChartSeriesType.LineXY,
                    LineColor = "#FFFF4040",
                    LineSize = GetLineThickness(p.FitStyle),
                    MarkerShapeMode = _p.MarkerShapeMode,
                    XAxisTitle = xaxisTitle,
                    MarkerColor = "#FFFF4040",
                    MarkerSize = 2,
                });
            }

            if ((_p.DrawParts & ReportChartDrawParts.Marker) == ReportChartDrawParts.Marker)
            {
                if (_p.ColorField == "Нет")
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Диаграмма",
                        YColumnName = yName,
                        YAxisTitle = _selectedParameter,
                        XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                        SeriesTitle = "#Маркер",
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        LineSize = 0,
                        ColorColumnName = "Цвет",
                        XAxisTitle = xaxisTitle,
                        MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        MarkerSize = 5,
                        MarkerSizeMode = _p.MarkerSizeMode,
                        MarkerShapeMode = _p.MarkerShapeMode,
                        MarkerMode = _p.MarkerMode
                    });
                }
                else
                {
                    var groups = _columns.GetGroups();
                    foreach (var group in groups)
                    {
                        cd.ChartSeries.Add(new ChartSeriesDescription
                        {
                            SourceTableName = "Диаграмма:" + (group == null ? "null" : group.ToString()),
                            YColumnName = yName,
                            YAxisTitle = _selectedParameter,
                            ZColumnName = (p.ColorField != "Нет")
                                ? p.ColorField == _p.Fields[0]
                                    ? p.ColorField + " "
                                    : p.ColorField
                                : null,
                            WColumnName = (p.ColorField != "Нет" && p.MarkerShapeMode != MarkerShapeModes.Bubble && p.MarkerShapeMode != MarkerShapeModes.Range) ? p.ColorField : null,
                            XColumnName = p.ColumnsType == ReportChartColumnDiagramType.Normed ? "%" : "Значение",
                            VColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Отступ категории" : null,
                            MarkerSizeColumnName = p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Число категорий" : null,
                            SeriesTitle = "#Маркер" + (group == null ? "null" : group.ToString()),
                            SeriesType = ChartSeriesType.LineXY,
                            Options = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "GroupProcessingY" : null,
                            ColorScale = _p.ColorScale,
                            ColorScaleMode = _p.ColorScaleMode,
                            ColorColumnName = "Цвет",
                            ZValueForColor = group,
                            WValueForShape = group,
                            LineSize = 0,
                            XAxisTitle = xaxisTitle,
                            ZValueSeriesTitle = p.AggregateDescription,
                            WValueSeriesTitle = p.AggregateDescription,
                            MarkerSize = 5,
                            MarkerShapeMode = _p.MarkerShapeMode,
                            MarkerSizeMode = _p.MarkerSizeMode,
                            MarkerMode = _p.MarkerMode
                        });
                    }
                }
            }
            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;

            RangesHelper.AppendRanges(cd, true, _chartRangesYTable, p.RangeType == ReportChartRangeType.RangeIsSigmas || p.RangeType == ReportChartRangeType.RangeIsSlide || p.RangeType == ReportChartRangeType.RangeIsR, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);
            
            return result;
        }
        double GetLineThickness(ReportChartRangeStyle lineStyle)
        {
            return lineStyle == ReportChartRangeStyle.Invisible
                          ? 0.5
                          : (lineStyle == ReportChartRangeStyle.Thin ? 1 : 2);
        }
    }
}