﻿using System;
using System.Windows;
using System.Windows.Controls;
using AQChartLibrary;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class ReportChartPareto
    {
        private void ChartPareto_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null || _paretoTable == null) return;
            var binName = _paretoTable.Table[e.Info.Index].Row[4].ToString();
            var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = binName,
                    Margin = new Thickness(2)
                };
            e.TT.Content = tb;
        }
        
        private void ChartPareto_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (_selectedTable == null || _paretoTable == null || e.Table == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs { AvailableOperations = InteractiveOperations.None, });
                return;
            }

            var rowList = new List<int>();
            var message = _selectedParameter + " = ";
            var isFirst = true;

            foreach (var ii in e.InteractivityIndexes)
            {
                if (!isFirst) message += "; ";
                var bin = _paretoTable.Table[ii.Index].Row[0];
                if (bin.ToString() == " Прочие") continue;
                message += $"\"{bin}\"";

                rowList.AddRange(_selectedTable.GetRowsForValueFromField(_selectedParameter, bin));

                isFirst = false;
            }
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _paretoTable } });

            Interactivity?.Invoke(this,
                new InteractivityEventArgs
                {
                    Table = _selectedTable.TableName,
                    Field = _selectedParameter,
                    Message = message,
                    AvailableOperations = InteractiveOperations.All,
                    SelectedCases = rowList.Distinct().ToList(),
                    Clear = true
                });
        }

        void chartPareto_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            if (ReportInteractiveRename != null) ReportInteractiveRename(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}