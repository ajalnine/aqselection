﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;

namespace AQReportingLibrary
{
    public partial class ReportChartPareto
    {
        private Panel _surface;
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            if (_data.GetDataTable() == null || _data.GetDataTable().Table.Count == 0) return false;
            SetupFrequencies();
            if (_paretoTable == null || _paretoTable.Table.Count == 0) return false;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _paretoTable } });
            return true;
        }

        public void Cancel() { }

        public void RefreshVisuals()
        {
            if (_paretoTable == null || _paretoTable.Table.Count == 0) return;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _paretoTable } });
        }

        private void SetupFrequencies()
        {
            _pareto = new Pareto(_data.GetLDR(), _p);
            _paretoTable = _pareto.GetParetoTable();
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields[0];
            var fi = ((new[] { Aggregate.N, Aggregate.Percent}).Contains(_p.Aggregate)) 
                ? new FieldsInfo { ColumnGroup = _p.Fields[0], ColumnY = _p.Fields[0] }
                : new FieldsInfo { ColumnGroup = _p.Fields[0], ColumnY = _p.Fields.Count > 1 ? _p.Fields[1] : _p.Fields[0] };
            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            foreach (var cd in GetDescriptionForPareto(_data, _p))
            {
                var chartPareto = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    EnableCellCondense = true,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable> {_interactiveTable, _paretoTable},
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                           new FixedAxis
                            {
                                Axis = "Y2",
                                AxisMin = 0.0d,
                                AxisMinLocked = true,
                                AxisMinFixed = true,
                                AxisMax = 100.0d,
                                AxisMaxLocked = true,
                                AxisMaxFixed = true,
                                Step = 10.0d,
                                StepLocked = true,
                                StepFixed = true
                            }
                            ,new FixedAxis { DataType = _selectedTable.GetDataType(_p.Fields[0]), Axis = "X", Format = "dd.MM.yyyy", StringIsDateTime = _selectedTable.GetDataType(_p.Fields[0])?.ToLower().Contains("datetime") ?? false}
                        },
                        Editable = true
                    }};
                chartPareto.ToolTipRequired += ChartPareto_ToolTipRequired;
                chartPareto.SelectionChanged += ChartPareto_SelectionChanged;
                chartPareto.AxisClicked += ChartPareto_AxisClicked; ;
                chartPareto.InteractiveRename += chartPareto_InteractiveRename;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartPareto });
                _surface.Children.Add(chartPareto);
                chartPareto.VisualReady += ChartPareto_VisualReady;
                chartPareto.MakeChart();
            }
        }

        private void ChartPareto_AxisClicked(object o, AxisEventArgs e)
        {
            AxisClicked?.Invoke(this, e);
        }

        private void ChartPareto_VisualReady(object o, AQBasicControlsLibrary.VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = new List<SLDataTable> {_paretoTable}});
        }
    }
}