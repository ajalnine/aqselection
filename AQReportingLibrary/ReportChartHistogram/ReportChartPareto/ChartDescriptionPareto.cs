﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartPareto
    {
        private IEnumerable<ChartDescription> GetDescriptionForPareto(Data d, ReportChartHistogramParameters p)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();
            result.Add(cd);

            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = ChartLegend.None;
            cd.ChartSeries = new List<ChartSeriesDescription>();

            string xName = p.Fields[0];
            string yName = p.Fields.Count>1 && p.Aggregate!=Aggregate.N && p.Aggregate!= Aggregate.Percent ? p.Fields[1] : string.Empty;
            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Парето",
                XColumnName = xName,
                XIsNominal = true,
                XAxisTitle = xName,
                YColumnName = "Значение",
                YIsSecondary = p.Aggregate==Aggregate.Percent,
                LabelColumnName = _p.LabelsType != ReportChartColumnsLabelsType.None ? "Подписи" : null,
                MarkerSizeColumnName = ((_p.LabelsType & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled) ? "Значение" : null,
                SeriesTitle = p.AggregateDescription,
                SeriesType = ChartSeriesType.StairsOnX,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                LineSize = 0.5,
                YAxisTitle = p.AggregateDescription + (!String.IsNullOrWhiteSpace(yName) ? " " + yName : string.Empty)
            };
            cd.ChartSeries.Add(csd);
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Парето",
                XColumnName = xName,
                XIsNominal = true,
                YIsSecondary = true,
                XAxisTitle = xName,
                YColumnName = "% накопления",
                SeriesTitle = "Накопление",
                SeriesType = ChartSeriesType.LineXY,
                MarkerColor = "#ffff0000",
                MarkerSize = 3,
                LineColor = "#ffff0000",
                LineSize = 3,
                YAxisTitle = "%"
            };
            cd.ChartSeries.Add(csd2);

            return result;
        }
    }
}