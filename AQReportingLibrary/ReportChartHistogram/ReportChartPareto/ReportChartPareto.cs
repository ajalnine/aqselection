﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class ReportChartPareto : IReportElement
    {
        private SLDataTable _chartRangesYTable;
        private SLDataTable _paretoTable;
        private SLDataTable _interactiveTable;
        
        private Data _data;
        private Pareto _pareto;
        private string _selectedParameter;
        private SLDataTable _selectedTable;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;

        private Step _step;
        private ReportChartHistogramParameters _p;

        public ReportChartPareto(ReportChartHistogramParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartPareto(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartHistogramParameters>(s);
            _step = s;
        }
        public void SetNewParameters(ReportChartHistogramParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Aggregate == Aggregate.N || _p.Aggregate == Aggregate.Percent) return String.Format(@"<b>{0}</b>: диаграмма Парето <b>{1}</b>", _p.Table, _p.Fields[0]);
            else
            {
                switch (_p.Aggregate)
                {
                    case Aggregate.Single:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Sum:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, сумма значений <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Min:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, минимум <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Max:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, максимум <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Mean:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, среднее <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Median:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, медиана <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.First:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, первое значение <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Last:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, последнее значение <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.StDev:
                        return String.Format(@"<b>{0}</b>: диаграмма Парето по группам <b>{1}</b>, СКО <b>{2}</b>", _p.Table, _p.Fields[0], _p.Fields[1]);
                }
            }
            return string.Empty;
        }
        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.Aggregate == Aggregate.N || _p.Aggregate == Aggregate.Percent) return String.Format(@"{0}: диаграмма Парето {1}", _p.Table, _p.Fields[0]);
            else
            {
                switch (_p.Aggregate)
                {
                    case Aggregate.Single:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Sum:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, сумма значений {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Min:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, минимум {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Max:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, максимум {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Mean:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, среднее {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Median:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, медиана {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.First:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, первое значение {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.Last:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, последнее значение {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                    case Aggregate.StDev:
                        return String.Format(@"{0}: диаграмма Парето по группам {1}, СКО {2}", _p.Table, _p.Fields[0], _p.Fields[1]);
                }
            }
            return string.Empty;
        }

        public string GetChartName()
        {
            return "Диаграмма Парето";
        }
    }
}