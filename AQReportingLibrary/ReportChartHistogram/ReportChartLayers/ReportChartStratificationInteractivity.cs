﻿using System;
using System.Windows;
using System.Windows.Controls;
using AQChartLibrary;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartStratification
    {
        void ChartFreq_AxisClicked(object o, AxisEventArgs e)
        {
            AxisClicked?.Invoke(this, e);
        }

        private void ChartFreq_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null || _frequencyTable == null) return;
            if (!e.Info.SeriesName.EndsWith("_Indexed"))
            {
                var binName = _frequencyTable.Table[e.Info.Index].Row[2].ToString();
                var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = binName,
                    Margin = new Thickness(2)
                };
                e.TT.Content = tb;
            }
            else
            {
                var sourceIndex = (int)_interactiveTable.Table[e.Info.Index].Row[4];

                var wp2 = new WrapPanel { Orientation = Orientation.Vertical, MaxHeight = 200 };
                e.TT.Content = wp2;
                var sldr2 = _selectedTable.Table[sourceIndex];
                for (int i = 0; i < _selectedTable.ColumnNames.Count; i++)
                {
                    if (sldr2.Row[i] == null || string.IsNullOrEmpty(sldr2.Row[i].ToString()) || _selectedTable.ColumnNames[i].StartsWith("#")) continue;
                    var tb = new TextBlock
                    {
                        TextWrapping = TextWrapping.Wrap,
                        MaxWidth = 400,
                        Text = _selectedTable.ColumnNames[i] + ": " + sldr2.Row[i],
                        Margin = new Thickness(0, 0, 20, 0)
                    };
                    wp2.Children.Add(tb);
                }
            }
        }

        private void ChartFreq_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (e.Table == null || _frequencies.GetDataStatInfo().Step == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs{AvailableOperations = InteractiveOperations.None, });
                return;
            }
            var rowList = new List<int>();
            var message = _selectedParameter + " ϵ ";
            var isFirst = true;
            foreach (var ii in e.InteractivityIndexes.Where(a=> !a.SeriesName.EndsWith("_Indexed") && !a.SeriesName.EndsWith("_Layer") && !a.SeriesName.Contains("Огибающая")))
            {
                if (!isFirst) message += "; ";
                var from = (double)e.Table.Table[ii.Index].Row[0];
                var to = (double)e.Table.Table[ii.Index].Row[6];
                var groupfield = _p.ColorField == "Нет" ? null : _p.ColorField;
                var group = e.Table.Table[ii.Index].Row[5];

                if (groupfield == null)
                {
                    rowList.AddRange(_selectedTable.GetRowsInRangeFromField(_selectedParameter, from, to, _p.BinMode == ReportChartBinMode.IncludeRight));
                }
                else
                {
                    rowList.AddRange(_selectedTable.GetRowsInRangeFromFieldAndLayer(_selectedParameter, _p.ColorField, @group, @from, to, _p.BinMode == ReportChartBinMode.IncludeRight));
                }

                message += groupfield == null
                    ? $"({@from}..{to}]"
                    : $"({@from}..{to}] для группы {@group}";
                isFirst = false;
            }
            message += "; отдельные значения ";
            isFirst = true;
            foreach (var ii in e.InteractivityIndexes.Where(a => a.SeriesName == "#Точки_Indexed").Union(e.InteractivityIndexes.Where(a => a.SeriesName == "#Rug_Indexed")))
            {
                var f = _selectedTable.ColumnNames.IndexOf(_selectedParameter);
                var sourceIndex = (int)_interactiveTable.Table[ii.Index].Row[4];
                if (!isFirst) message += "; ";
                message += _selectedTable.Table[sourceIndex].Row[f];
                rowList.Add(sourceIndex);
                isFirst = false;
            }

            foreach (var ii in e.InteractivityIndexes.Where(a =>a.SeriesName.EndsWith("_Layer")))
            {
                if (ii.Layer?.ToString()=="#NoLayers") rowList.AddRange(_selectedTable.GetRowsForAllValues());
                else rowList.AddRange(_selectedTable.GetRowsForAllValuesFromLayer(ii.LayerName, ii.Layer));
            }

            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _frequencyTable } });

            Interactivity?.Invoke(this,
                new InteractivityEventArgs
                {
                    Table = _selectedTable.TableName,
                    Field = _selectedParameter,
                    Message = message,
                    AvailableOperations = InteractiveOperations.All,
                    SelectedCases = rowList.Distinct().ToList(),
                    Clear = true
                });
        }
        
        void chartFreq_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }
    }
}