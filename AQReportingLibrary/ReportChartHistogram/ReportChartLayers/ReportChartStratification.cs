﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class ReportChartStratification : IReportElement
    {
        private SLDataTable _chartRangesYTable;
        private SLDataTable _frequencyTable, _frequencyTableNoLayers;
        private SLDataTable _interactiveTable;
        
        private Data _data;
        private Limits _limits;
        private Frequencies _frequencies, _frequenciesNoLayers;
        private string _probabilityComment = string.Empty, _ANOVAComment = string.Empty;
        private string _selectedParameter;
        private SLDataTable _selectedTable;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType;

        private Step _step;
        private ReportChartHistogramParameters _p;

        public ReportChartStratification(ReportChartHistogramParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartStratification(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartHistogramParameters>(s);
            _step = s;
        }
        public void SetNewParameters(ReportChartHistogramParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.ColorField!="Нет") return String.Format(@"<b>{0}</b>: стратификация <b>{1}</b>", _p.Table, _p.Fields[0]);
            return String.Format(@"<b>{0}</b>: частотное распределение <b>{1}</b>",_p.Table, _p.Fields[0]);
        }
        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.ColorField != "Нет") return String.Format(@"{0}: стратификация {1}", _p.Table, _p.Fields[0]);
            return String.Format(@"{0}: частотное распределение {1}", _p.Table, _p.Fields[0]);
        }

        public string GetChartName()
        {
            if (_p.ColorField != "Нет") return "Стратификация";
            return "Частотное распределение";
        }
    }
}