﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;
using System.Windows.Controls;
using System.Windows.Media;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartStratification
    {
        private BackgroundWorker _worker;
        private Thread _workerThread;
        private Panel _surface;
        private List<object> _othersLayer = new List<object>();
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            var table = _data.GetDataTable();
            if (table == null || table.Table.Count == 0) return false;
            if (table.Table.Count > 25000 && _p.ColorField!="Нет")
            {
                _worker = new BackgroundWorker {WorkerSupportsCancellation = true};
                _worker.RunWorkerCompleted += (o, e) =>
                {
                    _surface.Dispatcher.BeginInvoke(() =>
                    {
                        CreateCharts();
                        InternalTablesReadyForExport?.Invoke(this,
                            new InternalTablesReadyForExportEventArgs
                            {
                                InternalTables = new List<SLDataTable> {_frequencyTable}
                            });
                    });
                };

                _worker.DoWork += (o, e) =>
                {
                    PrepareChart(dataTables);
                };
                SetMessage("Построение графика...");
                _worker.RunWorkerAsync();
            }
            else
            {
                PrepareChart(dataTables);
                CreateCharts();
                InternalTablesReadyForExport?.Invoke(this,
                    new InternalTablesReadyForExportEventArgs
                    {
                        InternalTables = new List<SLDataTable>() {_frequencyTable}
                    });
            }
            return true;
        }

        private bool PrepareChart(List<SLDataTable> dataTables)
        {
            if (new[]
            {
                ReportChartColumnDiagramType.Opposite, ReportChartColumnDiagramType.Layers,
                ReportChartColumnDiagramType.ByCategory
            }.Contains(_p.ColumnType) && _p.ColorField != "Нет") SetupANOVAComment();

            SetupLimits(dataTables);
            SetupFrequencies();
            if (_p.ColumnsType == ReportChartColumnDiagramType.Opposite && (_p.FitMode & ReportChartFitMode.FitBeeSwarm) > 0
                || (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0
                || (_p.Tools & ReportChartTools.ToolRug) > 0
                || (_p.Tools & ReportChartTools.ToolKDE) > 0) FillSideData();
            if (_frequencyTable == null || _frequencyTable.Table.Count == 0) return false;
            return true;
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 30,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }

        private void SetupANOVAComment()
        {
            var a = new ANOVA(_data.GetLDR());
            _ANOVAComment = string.Empty;
            if ((_p.FitMode & ReportChartFitMode.FitGauss) == ReportChartFitMode.FitGauss) _ANOVAComment += ", " + a.GetANOVAResultForMeansByLayer(_p.ColorField)?.Comment ?? string.Empty;
            else _ANOVAComment += ", " + a.GetANOVAResultForKWByLayer(_p.ColorField)?.Comment ?? string.Empty;
        }

        
        public void RefreshVisuals()
        {
            if (_frequencyTable == null || _frequencyTable.Table.Count == 0) return;
            CreateCharts();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _frequencyTable } });
        }

        private void SetupFrequencies()
        {
            SetupTotals();

            _frequencies = new Frequencies(_data, _p, _limits, _p.ColumnsType != ReportChartColumnDiagramType.Generic, false);
            _frequencyTable = _frequencies.GetFreqTable();
            if (UiReflectChanges != null)
                UiReflectChanges.Invoke(this,
                    new UIReflectChangesEventArgs { StepIsAuto = _frequencies.StepIsAuto, Step = _frequencies.AutoStep, UserLimits = _limits.UserLimitsTable});
        }

        private void SetupTotals()
        {
            if (((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total ||
                 (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent) &&
                _p.ColumnType == ReportChartColumnDiagramType.Generic && _p.ColorField!="Нет")
            {
                var _p2 = new ReportChartHistogramParameters
                {
                    Fields = _p.Fields,
                    FontSize = _p.FontSize * 1.3,
                    LabelsType =
                        (((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total)
                            ? ReportChartColumnsLabelsType.N
                            : ReportChartColumnsLabelsType.None) |
                        (((_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) ==
                          ReportChartColumnsLabelsType.TotalPercent)
                            ? ReportChartColumnsLabelsType.Percent
                            : ReportChartColumnsLabelsType.None),
                    Table = _p.Table,
                    Aggregate = Aggregate.Sum,
                    AggregateDescription = _p.AggregateDescription,
                    AlignMode = _p.AlignMode,
                    BinMode = _p.BinMode,
                    ColorField = null,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    ColumnType = _p.ColumnType,
                    ColumnsOrder = _p.ColumnsOrder,
                    ColumnsType = _p.ColumnsType,
                    DetailLevel = _p.DetailLevel,
                    DrawParts = ReportChartDrawParts.None,
                    FitMode = _p.FitMode,
                    FitStyle = _p.FitStyle,
                    FixedAxisesDescription = _p.FixedAxisesDescription,
                    GroupNumber = _p.GroupNumber,
                    HistogramType = _p.HistogramType,
                    LayoutMode = _p.LayoutMode,
                    LayoutSize = _p.LayoutSize,
                    MarkerMode = _p.MarkerMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    RangeStyle = _p.RangeStyle,
                    RangeType = _p.RangeType,
                    Step = _p.Step,
                    Tools = ReportChartTools.ToolNone,
                    UserAlignValue = _p.UserAlignValue,
                    UserLimits = _p.UserLimits,
                    YMode = _p.YMode
                };
                _frequenciesNoLayers = new Frequencies(_data, _p2, _limits, false, true);
                _frequencyTableNoLayers = _frequenciesNoLayers.GetFreqTable();
                _frequencyTableNoLayers.TableName = "Частоты без слоев";
            }
        }

        private void FillSideData()
        {
            var groups = _frequencies.GetGroups();
            foreach (var row in _interactiveTable.Table)
            {
                var group = row.Row[8];
                var groupnumber = groups.IndexOf(group);
                var side = groupnumber % 2 == 0 ? 1.0d : -1.0d;
                row.Row[13] = side;
            }
        }

        private void SetupLimits(IEnumerable<SLDataTable> dataTables)
        {
            _limits = new Limits(_data.GetLDR(),
                _p.RangeType,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.Dynamic,
                _p.UserLimits,
                Limits.GetExternalLimitsTable(dataTables, _selectedTable),
                _selectedParameter, false, _p.RangeStyle);

            _probabilityComment = _limits.GetProbabilityComment(false);

            _chartRangesYTable = _limits.GetRangesYTable();
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields.FirstOrDefault();
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation,
                    Method = _p.Aggregate,
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }

            var fi = new FieldsInfo {ColumnY = _selectedParameter, ColumnZ = _p.ColorField};
            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
            
        }

        private void CreateCharts()
        {
            var statinfo = _frequencies.GetDataStatInfo();
            var mfd = new List<MathFunctionDescription>();

            if (_p.ColorField == "Нет" || _p.ColumnsType == ReportChartColumnDiagramType.Generic)
            {
                if ((_p.FitMode & ReportChartFitMode.FitGauss) == ReportChartFitMode.FitGauss)  mfd.Add(SetupZ(_p, statinfo, false));
                if ((_p.FitMode & ReportChartFitMode.FitGaussEx) == ReportChartFitMode.FitGaussEx) mfd.Add(SetupZ(_p, statinfo, true));
                if ((_p.FitMode & ReportChartFitMode.FitKDE) == ReportChartFitMode.FitKDE)
                {
                    var kde = new KernelDensity(_data.GetLDR());
                    mfd.Add(SetupKDE(_p, statinfo, kde));
                }
                if ((_p.FitMode & ReportChartFitMode.FitKDESilverman) == ReportChartFitMode.FitKDESilverman)
                {
                    var kde = new KernelDensity(_data.GetLDR());
                    mfd.Add(SetupKDESilverman(_p, statinfo, kde));
                }
                if ((_p.FitMode & ReportChartFitMode.FitWeibull) == ReportChartFitMode.FitWeibull)
                {
                    var d = _data.GetLDR().Where(a => a.Y.HasValue).Select(a => (object)a.Y).ToList();
                    statinfo.WK = AQMath.WeibullGetKFromData(d);
                    if (!double.IsNaN(statinfo.WK.Value))
                    {
                        statinfo.WLambda = AQMath.WeibullGetLambdaFromData(d, statinfo.WK);
                        mfd.Add(SetupW(_p, statinfo));
                    }
                }
            }
            else
            {
                foreach (var groupStatinfo in _frequencies.GetGroupStatInfo())
                {
                    groupStatinfo.ZAxis = "Частоты";

                    if ((_p.FitMode & ReportChartFitMode.FitGauss) == ReportChartFitMode.FitGauss) mfd.Add(SetupZ(_p, groupStatinfo, false));
                    if ((_p.FitMode & ReportChartFitMode.FitGaussEx) == ReportChartFitMode.FitGaussEx) mfd.Add(SetupZ(_p, groupStatinfo, true));

                    if ((_p.FitMode & ReportChartFitMode.FitKDE) == ReportChartFitMode.FitKDE)
                    {
                        var ldr = _data.GetLDR();
                        List<DynamicRow> groupldr;
                        if (groupStatinfo.Group == null)
                        {
                            groupldr = ldr.Where(a => a.Z == null).ToList();
                        }
                        else
                        {
                            groupldr = ldr.Where(a => a.Z.ToString() == groupStatinfo.Group.ToString()).ToList();
                        }
                        var kde = new KernelDensity(groupldr);
                        mfd.Add(SetupKDE(_p, groupStatinfo, kde));
                    }

                    if ((_p.FitMode & ReportChartFitMode.FitKDESilverman) == ReportChartFitMode.FitKDESilverman)
                    {
                        var ldr = _data.GetLDR();
                        List<DynamicRow> groupldr;
                        if (groupStatinfo.Group == null)
                        {
                            groupldr = ldr.Where(a => a.Z == null).ToList();
                        }
                        else
                        {
                            groupldr = ldr.Where(a => a.Z.ToString() == groupStatinfo.Group.ToString()).ToList();
                        }
                        var kde = new KernelDensity(groupldr);
                        mfd.Add(SetupKDESilverman(_p, groupStatinfo, kde));
                    }

                    if ((_p.FitMode & ReportChartFitMode.FitWeibull) == ReportChartFitMode.FitWeibull)
                    {
                        var ldr = _data.GetLDR();
                        List<DynamicRow> groupldr;
                        if (groupStatinfo.Group == null)
                        {
                            groupldr = ldr.Where(a => a.Z == null).ToList();
                        }
                        else
                        {
                            groupldr = ldr.Where(a => a.Z.ToString() == groupStatinfo.Group.ToString()).ToList();
                        }
                        var d = groupldr.Where(a => a.Y.HasValue).Select(a => (object)a.Y).ToList();
                        groupStatinfo.WK = AQMath.WeibullGetKFromData(d);
                        if (!double.IsNaN(groupStatinfo.WK.Value))
                        {
                            groupStatinfo.WLambda = AQMath.WeibullGetLambdaFromData(d, groupStatinfo.WK);
                            mfd.Add(SetupW(_p, groupStatinfo));
                        }
                    }
                }
            }

            foreach (var cd in GetDescriptionForFreq(_data, _p, SetupFitComment(mfd, statinfo)))
            {
                _surface.Children.Clear();
                var chartFreq = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalZero = _p.ColumnsType == ReportChartColumnDiagramType.Opposite,
                    OppositeHelpers = _p.ColumnsType == ReportChartColumnDiagramType.Opposite,
                    VerticalAxisSymmetry = _p.ColumnsType == ReportChartColumnDiagramType.Opposite,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    EnableCellCondense = true,
                    ShowXHelpers = ((_p.Tools & ReportChartTools.ToolRangeMean) | (_p.Tools & ReportChartTools.ToolRangeMedian) | (_p.Tools & ReportChartTools.ToolRug) | (_p.Tools & ReportChartTools.ToolKDE)) > 0,
                    ShowYHelpers = false,
                    ShowHelpersBeyondAxis = (_p.Tools & ReportChartTools.ToolBeyondAxis) > 0,
                    ShowHelpersMirrored = (_p.Tools & ReportChartTools.ToolMirrored) > 0,
                    ShowHelperGridlines = (_p.Tools & ReportChartTools.ToolWithGridLines) > 0,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    ConstantsOnHelpers = (_p.Tools & ReportChartTools.ToolWithConstants) > 0,
                    HelpersCoefficient = ((_p.Tools & ReportChartTools.ToolKDE) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0 || (_p.Tools & ReportChartTools.ToolRangeMean) > 0) ? 0.1 : 0.05,
                    GlobalFontCoefficient = _p.FontSize,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    HideNegativeX = !_data.GetLDR().Where(a => a.Y < 0).Any(),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable> {_interactiveTable, _chartRangesYTable, _frequencyTable, _frequencyTableNoLayers },
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                            new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalLayersType?.ToLower().Contains("datetime") ?? false}
                        },
                        Editable = false
                    },
                };

                if ((_p.DrawParts & ReportChartDrawParts.Line) == ReportChartDrawParts.Line ||
                    (_p.DrawParts & ReportChartDrawParts.Marker) == ReportChartDrawParts.Marker)
                {
                    if (_p.ColorField != "Нет")
                    {
                        var groups = _frequencies.GetGroups();
                        foreach (var group in groups)
                        {
                            var partialTable = _frequencies.GetPartialFreqTable(group);
                            partialTable.TableName = "Частоты:" + (group == null ? "null" : group.ToString());
                            chartFreq.DataTables.Add(partialTable);
                        }
                    }
                }

                chartFreq.ToolTipRequired += ChartFreq_ToolTipRequired;
                chartFreq.AxisClicked += ChartFreq_AxisClicked;
                chartFreq.SelectionChanged += ChartFreq_SelectionChanged;
                chartFreq.InteractiveRename += chartFreq_InteractiveRename;
                chartFreq.MathFunctionDescriptions = _p.ColumnsType == ReportChartColumnDiagramType.Normed ? null : mfd;

                statinfo.ZAxis = "Частоты";
                if (statinfo.Step != null)
                    chartFreq.FixedAxises.FixedAxisCollection.Add(new FixedAxis
                    {
                        Axis = "X",
                        Step = statinfo.Step.Value,
                        StepFixed = true,
                        StepLocked = true
                    });

                if (_p.ColumnsType == ReportChartColumnDiagramType.Normed)
                {
                    var axis = new FixedAxis
                    {
                        Axis = "Y",
                        AxisMin = 0.0d,
                        AxisMinLocked = true,
                        AxisMinFixed = true,
                        AxisMax = 100.0d,
                        AxisMaxFixed = true,
                        AxisMaxLocked = true,
                        Step = 10,
                        StepFixed = true,
                        StepLocked = true
                    };
                    chartFreq.FixedAxises.FixedAxisCollection.Add(axis);
                }
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartFreq });
                _surface.Children.Add(chartFreq);
                chartFreq.VisualReady += ChartFreqOnVisualReady;
                chartFreq.MakeChart();
            }
        }

        private void ChartFreqOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = new List<SLDataTable> {_frequencyTable}});
        }

        private string SetupFitComment(List<MathFunctionDescription> mfd, StatInfo statinfo)
        {
            var divider = "\r\n";
            string fitComment = string.Empty;
            bool isFirst = true;
            int count = 0;
            foreach (var m in mfd)
            {
                if (m==null)continue;
                if (!string.IsNullOrEmpty(m.Comment))
                {
                    fitComment += !isFirst ? $"{divider}{m.Comment}" : $"{m.Comment}";
                    count++;
                    isFirst = false;
                }
            }
            if (count > 3 || count == 0) fitComment = "n=" + statinfo.N  + _ANOVAComment;
            else if (count > 1) fitComment = "n=" + statinfo.N + _ANOVAComment + (!string.IsNullOrEmpty( fitComment)? "\r\n": string.Empty) + fitComment;
            return fitComment;
        }
    }
}