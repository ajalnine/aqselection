﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQChartLibrary;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartStratification
    {
        private Dictionary<ReportChartFrequencyYMode, string> axisTitle = new Dictionary<ReportChartFrequencyYMode, string>
        {
            {ReportChartFrequencyYMode.FrequencyInNumber, "Число наблюдений" },
            {ReportChartFrequencyYMode.FrequencyInPercent, "% наблюдений"  },
            {ReportChartFrequencyYMode.FrequencyInPercentOfGroup, "% наблюдений в группе"  }
        };
        private Dictionary<ReportChartFrequencyYMode, string> axisName = new Dictionary<ReportChartFrequencyYMode, string>
        {
            {ReportChartFrequencyYMode.FrequencyInNumber, "Число наблюдений" },
            {ReportChartFrequencyYMode.FrequencyInPercent, "% наблюдений"  },
            {ReportChartFrequencyYMode.FrequencyInPercentOfGroup, "% наблюдений"  }
        };
        private Dictionary<ReportChartFrequencyYMode, string> WaxisName = new Dictionary<ReportChartFrequencyYMode, string>
        {
            {ReportChartFrequencyYMode.FrequencyInNumber, "_Отступ" },
            {ReportChartFrequencyYMode.FrequencyInPercent, "_Отступ, %"  },
            {ReportChartFrequencyYMode.FrequencyInPercentOfGroup, "_Отступ, %"  }
        };
        private IEnumerable<ChartDescription> GetDescriptionForFreq(Data d, ReportChartHistogramParameters p, string zComment)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();
            result.Add(cd);
            var categoryProcessing = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory;
            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = string.IsNullOrEmpty(zComment) ? "n=" + n : zComment;
            cd.ChartSubtitle += (_chartRangesYTable.Table.Count > 0) ? RangesHelper.GetDescription(p.RangeType) : String.Empty;
            cd.ChartSubtitle += _p.BinMode == ReportChartBinMode.IncludeRight ? ", (..]" : ", [..)";
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0  && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = _p.ChartLegend;
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var blockWhite = (_p.ColumnsType == ReportChartColumnDiagramType.Layers ||
                              _p.ColumnType == ReportChartColumnDiagramType.Opposite ||
                              _p.ColumnType == ReportChartColumnDiagramType.ByCategory);

            string yName = _p.ColumnsType != ReportChartColumnDiagramType.Normed 
                ? axisName[p.YMode]
                : "% наблюдений в столбце";
            string wName = _p.ColumnsType != ReportChartColumnDiagramType.Normed
                ? WaxisName[p.YMode]
                : "Отступ, % в столбце";
            string yTitle = _p.ColumnsType != ReportChartColumnDiagramType.Normed
                ? axisTitle[p.YMode]
                : "% наблюдений в столбце";

            var smallDefaultOption = (_p.LabelsType & ReportChartColumnsLabelsType.SmallDefault) ==
                                     ReportChartColumnsLabelsType.SmallDefault
                ? "SmallDefault"
                : (_p.LabelsType & ReportChartColumnsLabelsType.SmallOver) ==
                  ReportChartColumnsLabelsType.SmallOver
                    ? "SmallOver"
                    : (_p.LabelsType & ReportChartColumnsLabelsType.SmallSkip) == ReportChartColumnsLabelsType.SmallSkip
                        ? "SmallSkip"
                        : string.Empty;
            var labelWideLimited = (_p.LabelsType & ReportChartColumnsLabelsType.LabelWideLimited) == ReportChartColumnsLabelsType.LabelWideLimited ? "WideLimited" : string.Empty;

            if ((_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total ||
                (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent)
            {
                var csdtotal = new ChartSeriesDescription
                {
                    SourceTableName = "Частоты без слоев",
                    XColumnName = _selectedParameter,
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    WColumnName = wName,
                    VColumnName = "Шаг",
                    ZColumnName = null,
                    YColumnName = yName,
                    LabelColumnName = _p.LabelsType != ReportChartColumnsLabelsType.None ? "Подписи" : null,
                    SeriesTitle = "_" + "Частоты без слоев",
                    SeriesType = ChartSeriesType.StairsOnX,
                    ColorScale = _p.ColorScale,
                    ColorColumnName = "Цвет",
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = MarkerModes.Invisible,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    MarkerSizeColumnName =
                        ((_p.LabelsType & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled)
                            ? "Размер"
                            : null,
                    LineSize = 0.5,
                    YAxisTitle = yTitle
                };
                cd.ChartSeries.Add(csdtotal);
            }

            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Частоты",
                XColumnName = _selectedParameter,
                XIsNominal = false,
                XAxisTitle = _selectedParameter,
                WColumnName = wName,
                VColumnName = categoryProcessing && _p.ColorField != "Нет" 
                        ? "Отступ категории"
                        : "Шаг",
                ZColumnName = (p.ColorField != "Нет") ? p.ColorField +" ": null,  //если совпадают имена параметра и слоя
                YColumnName = yName,
                LabelColumnName = _p.LabelsType != ReportChartColumnsLabelsType.None ? "Подписи" : null,
                SeriesTitle = "Частоты",
                SeriesType = ChartSeriesType.StairsOnX,
                ColorScale = _p.ColorScale,
                ColorColumnName = "Цвет",
                ColorScaleMode = _p.ColorScaleMode,
                Options = (_p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "NumericProcessing" : string.Empty) 
                + (_p.ColumnsType == ReportChartColumnDiagramType.Normed ? "Normed" : string.Empty)
                + (blockWhite ? "BlockWhite" : string.Empty) + smallDefaultOption + labelWideLimited,
                MarkerMode = (_p.DrawParts & ReportChartDrawParts.Solid) == ReportChartDrawParts.Solid ?  _p.MarkerMode : MarkerModes.Invisible,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                MarkerSizeColumnName =
                    ((_p.LabelsType & ReportChartColumnsLabelsType.Scaled) == ReportChartColumnsLabelsType.Scaled)
                        ? "Размер"
                        : null,
                LineSize = 0.5,
                YAxisTitle = yTitle
            };
            cd.ChartSeries.Add(csd);

            

            if ((_p.FitMode & ReportChartFitMode.FitBeeSwarm ) > 0)
            {
                var csd2 = new ChartSeriesDescription
                {
                    SourceTableName = _interactiveTable.TableName,
                    XColumnName = _selectedParameter,
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    SeriesTitle = "#Точки",
                    SeriesType = ChartSeriesType.BeeSwarmOnFloor,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = _p.MarkerMode,
                    ColorColumnName = "Tag",
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    Options =   _p.ColumnsType != ReportChartColumnDiagramType.Opposite ? null : "Opposite",
                    WColumnName = "Опция",
                    LineSize = 1,
                    MarkerSize = 5,
                    ZValueSeriesTitle = (p.ColorField != "Нет") ? "Частоты" : null, 
                };
                cd.ChartSeries.Add(csd2);
            }

            if ((_p.Tools & ReportChartTools.ToolRug) > 0)
            {
                var csd3 = new ChartSeriesDescription
                {
                    SourceTableName = _interactiveTable.TableName,
                    XColumnName = _selectedParameter,
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    SeriesTitle = "#Rug",
                    SeriesType = ChartSeriesType.RugOnX,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = _p.MarkerMode,
                    ColorColumnName = "Tag",
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    Options = _p.ColumnsType != ReportChartColumnDiagramType.Opposite ? null : "Opposite",
                    WColumnName = "Опция",
                    LineSize = 1,
                    MarkerSize = 5,
                    ZValueSeriesTitle = (p.ColorField != "Нет") ? "Частоты" : null,
                };
                cd.ChartSeries.Add(csd3);
            }

            if ((_p.Tools & ReportChartTools.ToolRangeMean) > 0 || (_p.Tools & ReportChartTools.ToolRangeMedian) > 0)
            {
                var textenabled = (_p.Tools & ReportChartTools.ToolText) > 0
                    ? "Text"
                    : string.Empty;
                var options = (_p.Tools & ReportChartTools.ToolRangeMean) > 0
                    ? "Mean"
                    : "Median";
                var csd3 = new ChartSeriesDescription
                {
                    SourceTableName = _interactiveTable.TableName,
                    XColumnName = _selectedParameter,
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    SeriesTitle = "#RangeOnX",
                    SeriesType = ChartSeriesType.RangeOnX,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = _p.MarkerMode,
                    ColorColumnName = "Tag",
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    Options = (_p.ColumnsType != ReportChartColumnDiagramType.Opposite ? string.Empty : "Opposite")+options+textenabled,
                    WColumnName = "Опция",
                    LineSize = 1,
                    MarkerSize = 5,
                    ZValueSeriesTitle = (p.ColorField != "Нет") ? "Частоты" : null,
                };
                cd.ChartSeries.Add(csd3);
            }

            if ((_p.Tools & ReportChartTools.ToolKDE) > 0)
            {
                var csd3 = new ChartSeriesDescription
                {
                    SourceTableName = _interactiveTable.TableName,
                    XColumnName = _selectedParameter,
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    ZColumnName = (p.ColorField != "Нет") ? p.ColorField : null,
                    SeriesTitle = "#KDE",
                    SeriesType = ChartSeriesType.KDEOnX,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerMode = _p.MarkerMode,
                    ColorColumnName = "Tag",
                    MarkerSizeMode = _p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    Options = _p.ColumnsType != ReportChartColumnDiagramType.Opposite ? null : "Opposite",
                    WColumnName = "Опция",
                    LineSize = 1,
                    MarkerSize = 5,
                    ZValueSeriesTitle = (p.ColorField != "Нет") ? "Частоты" : null,
                };
                cd.ChartSeries.Add(csd3);
            }

            if ((_p.DrawParts & ReportChartDrawParts.Line) == ReportChartDrawParts.Line)
            {
                if (_p.ColorField == "Нет")
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Частоты",
                        XColumnName = "Центр",
                        XIsNominal = false,
                        XAxisTitle = _selectedParameter,
                        YColumnName = yName,
                        SeriesTitle = "#Полигон",
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        ColorColumnName = "Цвет",
                        LineSize = GetLineThickness(p.FitStyle),
                        YAxisTitle = yTitle,
                        MarkerMode = _p.MarkerMode,
                        MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        MarkerSize = 2,
                    });
                }
                else
                {
                    var groups = _frequencies.GetGroups();
                    foreach (var group in groups)
                    {
                        cd.ChartSeries.Add(new ChartSeriesDescription
                        {
                            SourceTableName = "Частоты:" + (group == null ? "null" : group.ToString()),
                            XColumnName = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Центр категории" : "Центр",
                            XIsNominal = false,
                            XAxisTitle = _selectedParameter,
                            ZColumnName = (p.ColorField != "Нет") ? p.ColorField + " " : null,
                            YColumnName = yName,
                            SeriesTitle = "#Полигон" + (group == null ? "null" : group.ToString()),
                            SeriesType = ChartSeriesType.LineXY,
                            ColorScaleMode = _p.ColorScaleMode,
                            ColorColumnName = "Цвет",
                            ColorScale = _p.ColorScale,
                            ZValueForColor = group,
                            ZValueSeriesTitle = "Частоты",
                            LineSize = 2,
                            MarkerMode = _p.MarkerMode,
                            YAxisTitle = yTitle,
                            MarkerSize = 2
                        });
                    }
                }
            }

            var total = (_p.LabelsType & ReportChartColumnsLabelsType.Total) == ReportChartColumnsLabelsType.Total;
            var totalPercent = (_p.LabelsType & ReportChartColumnsLabelsType.TotalPercent) == ReportChartColumnsLabelsType.TotalPercent;
            var connected = (_p.LabelsType & ReportChartColumnsLabelsType.Connected) == ReportChartColumnsLabelsType.Connected;
            if ((total || totalPercent) && connected)
            {
                cd.ChartSeries.Add(new ChartSeriesDescription
                {
                    SourceTableName = "Частоты без слоев",
                    XColumnName = "Центр",
                    XIsNominal = false,
                    XAxisTitle = _selectedParameter,
                    YColumnName = yName,
                    SeriesTitle = "#Огибающая",
                    SeriesType = ChartSeriesType.LineXY,
                    LineColor = "#FFFF4040",
                    LineSize = GetLineThickness(p.FitStyle),
                    YAxisTitle = yTitle,
                    MarkerColor = "#FFFF4040",
                    MarkerSize = 2,
                });
            }

            if ((_p.DrawParts & ReportChartDrawParts.Marker) == ReportChartDrawParts.Marker)
            {
                if (_p.ColorField == "Нет")
                {
                    cd.ChartSeries.Add(new ChartSeriesDescription
                    {
                        SourceTableName = "Частоты",
                        XColumnName = "Центр",
                        XIsNominal = false,
                        XAxisTitle = _selectedParameter,
                        YColumnName = yName,
                        SeriesTitle = "#Маркер",
                        SeriesType = ChartSeriesType.LineXY,
                        LineColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        ColorColumnName = "Цвет",
                        LineSize = 0,
                        YAxisTitle = yTitle,
                        MarkerColor = ColorConvertor.GetDefaultColorForScale(p.ColorScale),
                        MarkerSize = 5,
                        MarkerSizeMode = _p.MarkerSizeMode,
                        MarkerShapeMode = p.MarkerShapeMode,
                        MarkerMode = _p.MarkerMode
                    });
                }
                else
                {
                    var groups = _frequencies.GetGroups();
                    foreach (var group in groups)
                    {
                        cd.ChartSeries.Add(new ChartSeriesDescription
                        {
                            SourceTableName = "Частоты:" + (group == null ? "null" : group.ToString()),
                            XColumnName = _p.ColumnsType == ReportChartColumnDiagramType.ByCategory ? "Центр категории" : "Центр",
                            XIsNominal = false,
                            XAxisTitle = _selectedParameter,
                            ZColumnName = (p.ColorField != "Нет") ? p.ColorField + " " : null,
                            WColumnName = (p.ColorField != "Нет" && p.MarkerShapeMode != MarkerShapeModes.Bubble && p.MarkerShapeMode != MarkerShapeModes.Range) ? p.ColorField : null,
                            YColumnName = yName,
                            SeriesTitle = "#Маркер" + (group == null ? "null" : group.ToString()),
                            SeriesType = ChartSeriesType.LineXY,
                            ColorScale = _p.ColorScale,
                            ColorScaleMode = _p.ColorScaleMode,
                            ColorColumnName = "Цвет",
                            ZValueForColor = group,
                            LineSize = 0,
                            YAxisTitle = yTitle,
                            ZValueSeriesTitle = "Частоты",
                            WValueForShape = group,
                            MarkerSize = 5,
                            MarkerSizeMode = _p.MarkerSizeMode,
                            MarkerShapeMode = p.MarkerShapeMode,
                            MarkerMode = _p.MarkerMode,
                            WValueSeriesTitle = "Частоты"
                        });
                    }
                }
            }

            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;

            RangesHelper.AppendRanges(cd, true, _chartRangesYTable, p.RangeType == ReportChartRangeType.RangeIsSigmas || p.RangeType == ReportChartRangeType.RangeIsSlide || p.RangeType == ReportChartRangeType.RangeIsR, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);

            return result;
        }
    }
}