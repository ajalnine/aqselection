﻿using AQChartLibrary;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;
using System.Windows;
using System;

namespace AQReportingLibrary
{
    public partial class ReportChartStratification
    {
        private static double GetLineThickness(ReportChartRangeStyle lineStyle)
        {
            var l = lineStyle & (ReportChartRangeStyle.Thick | ReportChartRangeStyle.Thin | ReportChartRangeStyle.Invisible);
            return l == ReportChartRangeStyle.Invisible
                          ? 0
                          : (l == ReportChartRangeStyle.Thin ? 1 : 2);
        }

        public static MathFunctionDescription SetupZ(ReportChartHistogramParameters pr, StatInfo statinfo, bool withCheck)
        {
            if(statinfo.N==0)return null;

            string comment = string.Empty;
            if (statinfo.X == null)
            {
                comment = statinfo.Group == null
                    ? $"n={statinfo.N}"
                    : $"<b>{statinfo.Group}</b>: n={statinfo.N}";
                return null;
            }
            else
            {
                comment = statinfo.Group == null
                    ? $"n={statinfo.N}, Среднее={Math.Round(statinfo.X.Value, 4)}, СКО={Math.Round(statinfo.S.Value, 4)}"
                    : $"<b>{statinfo.Group}</b>: n={statinfo.N}, Среднее={Math.Round(statinfo.X.Value, 4)}, СКО={ Math.Round(statinfo.S.Value, 4)}";
            }

            if (withCheck)
            {
                var ks = AQMath.KolmogorovD(statinfo.SourceData);
                var pks = AQMath.KolmogorovP(statinfo.SourceData);
                comment += $", D={ Math.Round(ks, 4)}";
                comment += pks <= 0.05 ? $",<RED> p={ Math.Round(pks, 4)}</RED>" : $", p={ Math.Round(pks, 4)}";
                var w = AQMath.ShapiroWilkW(statinfo.SourceData);
                if (w.HasValue)
                {
                    var pw = AQMath.ShapiroWilkP(statinfo.SourceData);
                    comment += $", W={ Math.Round(w.Value, 4)}";
                    if (pw.HasValue) comment += pw <= 0.05 ? $",<RED> p={ Math.Round(pw.Value, 4)}</RED>" : $", p={ Math.Round(pw.Value, 4)}";
                }
            }

            var result = new MathFunctionDescription
            {
                LineColor = statinfo.Group == null ? "#ffff4000" : null,
                LineSize = GetLineThickness(pr.FitStyle),
                PixelStep = 4,
                Filled = (pr.FitStyle & ReportChartRangeStyle.Filled) > 0,
                ColorByZScale = statinfo.Group,
                ZValueSeriesTitle = statinfo.ZAxis,
                Comment = comment,
                MathFunctionCalculator = (cri, x) =>
                {
                    if (statinfo == null || !statinfo.S.HasValue || !statinfo.X.HasValue || !statinfo.N.HasValue || !statinfo.Step.HasValue || statinfo.S == 0) return 0;
                    var v = AQMath.NormalPD((double) cri.XDownMin + ((double) cri.XDownMax - (double) cri.XDownMin)*x, statinfo.X.Value, statinfo.S.Value);
                    var p = (pr.YMode == ReportChartFrequencyYMode.FrequencyInNumber)
                        ? v * statinfo.N.Value * statinfo.Step.Value
                        : v * statinfo.PercentOfTotal.Value * statinfo.Step.Value;
                    if (statinfo.ShowInversed) p = -p;
                    if (v.HasValue)
                        return (p.Value - (double) cri.YLeftMin)/((double) cri.YLeftMax - (double) cri.YLeftMin);
                    return 0;
                },
                MarkPointsOnXAxis = new List<double?> { statinfo.X },
            };
            
            return result;
        }

        private MathFunctionDescription SetupW(ReportChartHistogramParameters pr, StatInfo statinfo)
        {
            var k = statinfo.WK.Value;
            var l = statinfo.WLambda.Value;
           // var mw = l * AQMath.Gamma(1.0d + 1.0d/k);
            var moda = l * Math.Pow(k - 1, 1 / k) / Math.Pow(k, 1 / k);
            var comment = statinfo.Group == null
                ? $"W: Мода = {Math.Round(moda, 4)}"
                : $"W: <b>{statinfo.Group}<b>: n={statinfo.N}, Мода = {Math.Round(moda, 4)}";


            var result = new MathFunctionDescription
            {
                LineColor = statinfo.Group == null ? "#ff8f2faf" : null,
                LineSize = GetLineThickness(pr.FitStyle),
                PixelStep = 4,
                Filled = (pr.FitStyle & ReportChartRangeStyle.Filled) > 0,
                ZValueSeriesTitle = statinfo.ZAxis,
                ColorByZScale = statinfo.Group,
                Comment = comment,
                MathFunctionCalculator = (cri, x) =>
                {
                    if (statinfo == null || !statinfo.WLambda.HasValue || !statinfo.WK.HasValue || !statinfo.N.HasValue ||
                        !statinfo.Step.HasValue)
                        return 0;
                    var v = AQMath.WeibullPD((double)cri.XDownMin + ((double)cri.XDownMax - (double)cri.XDownMin) * x,
                        statinfo.WLambda.Value, statinfo.WK.Value);

                    var p = (pr.YMode == ReportChartFrequencyYMode.FrequencyInNumber)
                        ? v * statinfo.N.Value * statinfo.Step.Value
                        : v * statinfo.PercentOfTotal * statinfo.Step.Value;
                    if (statinfo.ShowInversed) p = -p;
                    if (v.HasValue)
                        return (p.Value - (double)cri.YLeftMin) / ((double)cri.YLeftMax - (double)cri.YLeftMin);
                    return 0;
                },

                MarkPointsOnXAxis = new List<double?> { moda }
            };

            return result;
        }

        private MathFunctionDescription SetupKDE(ReportChartHistogramParameters pr, StatInfo statinfo, KernelDensity kde)
        {
            var result = new MathFunctionDescription
            {
                LineColor = statinfo.Group == null ? "#af0040ff" : null,
                LineSize = GetLineThickness(pr.FitStyle),
                PixelStep = 4,
                Filled = (pr.FitStyle & ReportChartRangeStyle.Filled) > 0,
                ZValueSeriesTitle = statinfo.ZAxis,
                ColorByZScale = statinfo.Group,
                MathFunctionCalculator = (cri, x) =>
                {
                    if (statinfo == null || !statinfo.S.HasValue || !statinfo.X.HasValue || !statinfo.N.HasValue || !statinfo.Step.HasValue || statinfo.S == 0) return 0;
                    var v = kde.GetDensity((double)cri.XDownMin + ((double)cri.XDownMax - (double)cri.XDownMin) * x);
                    var p = (pr.YMode == ReportChartFrequencyYMode.FrequencyInNumber)
                        ? v*statinfo.N.Value*statinfo.Step.Value
                        : v * statinfo.PercentOfTotal.Value * statinfo.Step.Value;
                    if (statinfo.ShowInversed) p = -p;
                    return (p - (double) cri.YLeftMin)/((double) cri.YLeftMax - (double) cri.YLeftMin);
                }
            };
            return result;
        }

        private MathFunctionDescription SetupKDESilverman(ReportChartHistogramParameters pr, StatInfo statinfo, KernelDensity kde)
        {
            var result = new MathFunctionDescription
            {
                LineColor = statinfo.Group == null ? "#af008080" : null,
                LineSize = GetLineThickness(pr.FitStyle),
                PixelStep = 4,
                Filled = (pr.FitStyle & ReportChartRangeStyle.Filled) > 0,
                ZValueSeriesTitle = statinfo.ZAxis,
                ColorByZScale = statinfo.Group,
                MathFunctionCalculator = (cri, x) =>
                {
                    if (statinfo == null || !statinfo.S.HasValue || !statinfo.X.HasValue || !statinfo.N.HasValue || !statinfo.Step.HasValue || statinfo.S == 0) return 0;
                    var v = kde.GetDensitySilverman((double) cri.XDownMin + ((double) cri.XDownMax - (double) cri.XDownMin)*x);
                    var p = (pr.YMode == ReportChartFrequencyYMode.FrequencyInNumber)
                        ? v*statinfo.N.Value*statinfo.Step.Value
                        : v * statinfo.PercentOfTotal.Value * statinfo.Step.Value;
                    if (statinfo.ShowInversed) p = -p;
                    return (p - (double) cri.YLeftMin)/((double) cri.YLeftMax - (double) cri.YLeftMin);
                }
            };
            return result;
        }
    }
}