﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportChartHistogramParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string ColorField { get; set; }

        public string AggregateDescription { get; set; }
        public ReportChartAlignMode AlignMode { get; set; }
        public ReportChartFrequencyYMode YMode { get; set; }
        public ReportChartBinMode BinMode { get; set; }
        public ReportChartFitMode FitMode { get; set; }
        public double DetailLevel { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartRangeStyle FitStyle { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }

        public ReportChartColumnDiagramType ColumnType { get; set; }
        public ReportChartColumnsLabelsType LabelsType { get; set; }
        public ReportChartDrawParts DrawParts { get; set; }
        public ReportChartTools Tools { get; set; }
        public SLDataTable UserLimits { get; set; }
        public Double? Step { get; set; }
        public int? GroupNumber { get; set; }
        public Double? UserAlignValue { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public ReportChartHistogramType HistogramType { get; set; }
        public Aggregate Aggregate { get; set; }
        public ReportChartColumnDiagramType ColumnsType { get; set; }
        public ReportChartColumnOrder ColumnsOrder { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }

        public ChartLegend ChartLegend { get; set; }

    }
}