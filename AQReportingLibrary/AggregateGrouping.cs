﻿using System.Collections.Generic;
using System.Linq;
using AQMathClasses;

namespace AQReportingLibrary
{
    public static class AggregateGrouping
    {
        public delegate IEnumerable<GroupValues> GroupCalcDelegate(IEnumerable<GroupValues> data);

        public static Dictionary<Aggregate, string> ParameterNames = new Dictionary<Aggregate, string>
        {
            {Aggregate.Single,   string.Empty},
            {Aggregate.N,        "Число наблюдений"},
            {Aggregate.Percent,  "Процент наблюдений"},
            {Aggregate.PercentInCategory,  "Процент наблюдений в слое"},
            { Aggregate.Sum,      "Сумма значений"},
            {Aggregate.Min,      "Минимум"},
            {Aggregate.Max,      "Максимум"},
            {Aggregate.Mean,     "Среднее"},
            {Aggregate.Median,   "Медиана"},
            {Aggregate.First,    "Первое значение"},
            {Aggregate.Last,     "Последнее значение"},
            {Aggregate.StDev,    "СКО"}
        };

        public static Dictionary<Aggregate, GroupCalcDelegate> Calculators = new Dictionary<Aggregate, GroupCalcDelegate>
        {
            {Aggregate.Single,   data=> data.Select(g => new GroupValues { Name = g.Name, Value = g.Value, Category = g.Category, n = 1, Color = g.Color,  Label = g.Label, OriginalIndex = g.OriginalIndex}).ToList()},
            {Aggregate.N,        data=> (from l in data group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Count(), Category = g.First().Category, n = g.Count(), Color = g.Key.Color}).ToList()},
            {Aggregate.Percent,  data=> (from l in data group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Count(), Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.PercentInCategory,  data=> (from l in data group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Count(), Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.Sum,      data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Sum(a => a.Value), Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.Min,      data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Min(a => a.Value), Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.Max,      data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Max(a => a.Value), Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.Mean,     data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = AQMath.AggregateMean(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()), Color = g.Key.Color, Category = g.First().Category, n = g.Count() }).ToList()},
            {Aggregate.Median,   data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = AQMath.AggregateMedian(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()), Color = g.Key.Color, Category = g.First().Category, n = g.Count() }).ToList()},
            {Aggregate.First,    data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.First().Value, Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.Last,     data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = g.Last().Value, Category = g.First().Category, n = g.Count(), Color = g.Key.Color }).ToList()},
            {Aggregate.StDev,    data=> (from l in data where l.Value.HasValue group l by new {Name = l.Name?.ToString(), Category = l.Category?.ToString(), Color = l.Color}).Select(g => new GroupValues { Name = g.First().Name, Value = AQMath.AggregateStDev(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()), Color = g.Key.Color, Category = g.First().Category, n = g.Count() }).ToList()}
        };

    }
    public class GroupValues
    {
        public object Name;
        public object Category;
        public string Label;
        public string Color;
        public double? Value;
        public double? Total;
        public double n;
        public int OriginalIndex;
    }
}
