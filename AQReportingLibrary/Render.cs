﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public static class ChartRenderer
    {
        public class RenderedChart
        {
            public WriteableBitmap Chart;
            public WriteableBitmap ThumbNail;
            public WriteableBitmap Icon;
            public List<SLDataTable> InnerTables;
            public string ChartDescription;
        }

        public delegate void ChartRenderedCallBackDelegate(RenderedChart rc);

        public static void RenderStep(List<SLDataTable> sourceData, Step s, Canvas panel, ChartRenderedCallBackDelegate chartReady, InternalTablesReadyForExportDelegate internalTablesReady)
        {
            if (s == null) return;
            panel.Children.Clear();
            Data.LDRCache.Clear();
            switch (s.Calculator.Substring(5, s.Calculator.Length - 5))
            {
                case "ReportChartDynamic":
                    var p1 = DeserializeParameters<ReportChartDynamicParameters>(s);
                    panel.Width = p1.LayoutSize.Width;
                    panel.Height = p1.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart1 = new ReportChartDynamic(p1);
                    var layout1 = p1.LayoutMode;
                    chart1.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs {InternalTables = e.InternalTables});
                    };
                    chart1.Finished += (o, e) =>
                    {
                        FinalizeRender(chartReady, panel, layout1, e.InnerTables, true, chart1.GetChartDescription());
                    };
                    chart1.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p1.FixedAxisesDescription;
                    if (!chart1.MakeVisuals(sourceData, panel))FinalizeRender(chartReady, panel, layout1, null, false, chart1.GetChartDescription()); 
                    return;

                case "ReportChartCard":
                    var p2 = DeserializeParameters<ReportChartCardParameters>(s);
                    panel.Width = p2.LayoutSize.Width;
                    panel.Height = p2.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart2 = new ReportChartCard(p2);
                    var layout2 = p2.LayoutMode;
                    chart2.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart2.Finished += (o, e) =>
                    {
                        FinalizeRender(chartReady, panel, layout2, e.InnerTables, true, chart2.GetChartDescription());
                    };
                    chart2.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p2.FixedAxisesDescription;
                    if (!chart2.MakeVisuals(sourceData, panel))FinalizeRender(chartReady, panel, layout2, null, false, chart2.GetChartDescription());
                    return;
                    
                case "ReportChartGroups":
                    var p3 = DeserializeParameters<ReportChartGroupsParameters>(s);
                    panel.Width = p3.LayoutSize.Width;
                    panel.Height = p3.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart3 = new ReportChartGroups(p3);
                    var layout3 = p3.LayoutMode;
                    chart3.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart3.Finished += (o, e) =>
                    {
                        if (p3.AnalisysMode == ReportChartGroupAnalisysMode.ANOVA ||
                            p3.AnalisysMode == ReportChartGroupAnalisysMode.TableTests)
                        {
                            panel.Width = panel.Children[0].RenderSize.Width;
                            panel.Height = panel.Children[0].RenderSize.Height;
                            panel.UpdateLayout();
                        }
                        FinalizeRender(chartReady, panel, layout3, e.InnerTables, true, chart3.GetChartDescription());
                    };
                    chart3.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p3.FixedAxisesDescription;
                    if (!chart3.MakeVisuals(sourceData, panel))FinalizeRender(chartReady, panel, layout3, null, false, chart3.GetChartDescription());
                    return;

                case "ReportChartHistogram":
                    var p4 = DeserializeParameters<ReportChartHistogramParameters>(s);
                    panel.Width = p4.LayoutSize.Width;
                    panel.Height = p4.LayoutSize.Height;
                    panel.UpdateLayout();
                    IReportElement chart4 = null;
                    if (p4.HistogramType == ReportChartHistogramType.Frequencies) chart4 = new ReportChartStratification(p4);
                    else if (p4.HistogramType == ReportChartHistogramType.Columns) chart4 = new ReportChartColumns(p4);
                    else if (p4.HistogramType == ReportChartHistogramType.Bands) chart4 = new ReportChartBands(p4);
                    else if (p4.HistogramType == ReportChartHistogramType.Pareto) chart4 = new ReportChartPareto(p4);
                    var layout4 = p4.LayoutMode;
                    chart4.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart4.Finished += (o, e) =>
                    {
                        FinalizeRender(chartReady, panel, layout4, e.InnerTables, true, chart4.GetChartDescription());
                    };
                    chart4.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p4.FixedAxisesDescription;
                    if (!chart4.MakeVisuals(sourceData, panel))FinalizeRender(chartReady, panel, layout4, null, false, chart4.GetChartDescription());
                    return;

                case "ReportChartScatterPlot":
                    var p5 = DeserializeParameters<ReportChartScatterPlotParameters>(s);
                    panel.Width = p5.LayoutSize.Width;
                    panel.Height = p5.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart5 = new ReportChartScatterPlot(p5);
                    var layout5 = p5.LayoutMode;
                    chart5.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart5.Finished += (o, e) =>
                    {
                        if (p5.LayersMode == ReportChartLayerMode.TableOnly)
                        {
                            panel.Width = panel.Children[0].RenderSize.Width;
                            panel.Height = panel.Children[0].RenderSize.Height;
                            panel.UpdateLayout();
                        }
                        FinalizeRender(chartReady, panel, layout5, e.InnerTables, true, chart5.GetChartDescription());
                    };
                    chart5.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p5.FixedAxisesDescription;
                    if (!chart5.MakeVisuals(sourceData, panel)) FinalizeRender(chartReady, panel, layout5, null, false, chart5.GetChartDescription());
                    return;

                case "ReportChartMultipleRegression":
                    var p6 = DeserializeParameters<ReportChartMultipleRegressionParameters>(s);
                    panel.Width = p6.LayoutSize.Width;
                    panel.Height = p6.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart6 = new ReportChartMultipleRegression(p6);
                    var layout6 = p6.LayoutMode;
                    chart6.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart6.Finished += (o, e) =>
                    {
                        if (p6.ViewType == ReportChartRegressionViewType.RMatrix || p6.ViewType == ReportChartRegressionViewType.ResultsInTable)
                        {
                            panel.Width = panel.Children[0].RenderSize.Width;
                            panel.Height = panel.Children[0].RenderSize.Height;
                            panel.UpdateLayout();
                        }
                        FinalizeRender(chartReady, panel, layout6, e.InnerTables, true, chart6.GetChartDescription());
                    };
                    chart6.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p6.FixedAxisesDescription;
                    if (!chart6.MakeVisuals(sourceData, panel)) FinalizeRender(chartReady, panel, layout6, null, false, chart6.GetChartDescription());
                    return;

                case "ReportChartProbabilityPlot":
                    var p7 = DeserializeParameters<ReportChartProbabilityPlotParameters>(s);
                    panel.Width = p7.LayoutSize.Width;
                    panel.Height = p7.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart7 = new ReportChartProbabilityPlot(p7);
                    var layout7 = p7.LayoutMode;
                    chart7.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart7.Finished += (o, e) =>
                    {
                        FinalizeRender(chartReady, panel, layout7, e.InnerTables, true, chart7.GetChartDescription());
                    };
                    chart7.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p7.FixedAxisesDescription;
                    if (!chart7.MakeVisuals(sourceData, panel)) FinalizeRender(chartReady, panel, layout7, null, false, chart7.GetChartDescription());
                    return;

                case "ReportStatistics":
                    var p8 = DeserializeParameters<ReportStatisticsParameters>(s);
                    panel.Width = p8.LayoutSize.Width;
                    panel.Height = p8.LayoutSize.Height;
                    panel.UpdateLayout();
                    var chart8 = new ReportStatistics(p8);
                    var layout8 = p8.LayoutMode;
                    chart8.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart8.Finished += (o, e) =>
                    {
                        panel.Width = panel.Children[0].RenderSize.Width;
                        panel.Height = panel.Children[0].RenderSize.Height;
                        panel.UpdateLayout();
                        FinalizeRender(chartReady, panel, layout8, e.InnerTables, true, chart8.GetChartDescription());
                    };
                    if (!chart8.MakeVisuals(sourceData, panel)) FinalizeRender(chartReady, panel, layout8, null, false, chart8.GetChartDescription());
                    return;

                case "ReportChartCircles":
                    var p9 = DeserializeParameters<ReportChartCirclesParameters>(s);
                    panel.Width = p9.LayoutSize.Width;
                    panel.Height = p9.LayoutSize.Height;
                    panel.UpdateLayout();
                    IReportElement chart9 = new ReportChartCircles(p9);
                    var layout9 = p9.LayoutMode;
                    chart9.InternalTablesReadyForExport += (o, e) =>
                    {
                        internalTablesReady.Invoke(o, new InternalTablesReadyForExportEventArgs { InternalTables = e.InternalTables });
                    };
                    chart9.Finished += (o, e) =>
                    {
                        FinalizeRender(chartReady, panel, layout9, e.InnerTables, true, chart9.GetChartDescription());
                    };
                    chart9.ChartSetsFixedAxis += ReportChartChartSetsFixedAxis;
                    AnalysisFixedAxises = p9.FixedAxisesDescription;
                    if (!chart9.MakeVisuals(sourceData, panel)) FinalizeRender(chartReady, panel, layout9, null, false, chart9.GetChartDescription());
                    return;
            }
        }
        public static FixedAxises AnalysisFixedAxises;

        private static void ReportChartChartSetsFixedAxis(object o, ChartSetsFixedAxisEventArgs e)
        {
            SetFixedAxis(e.SettingChart);
        }

        private static void SetFixedAxis(Chart chart)
        {
            chart.AxisRangeReady += chart_AxisRangeReady;
            MergeFixedAxis(chart.FixedAxises); //Первоначальные ограничения графика объединяются с ограничениями из кэша
            chart.FixedAxises = AnalysisFixedAxises;
        }

        static void chart_AxisRangeReady(object o, Chart.AxisRangeReadyEventArgs e)
        {
            RenewFixedAxises(e);
        }

        private static void RenewFixedAxises(Chart.AxisRangeReadyEventArgs e)
        {
            if (AnalysisFixedAxises == null) AnalysisFixedAxises = new FixedAxises();
            if (AnalysisFixedAxises.FixedAxisCollection == null) AnalysisFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X"), e.BottomAxisRange, "X");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "X2"), e.TopAxisRange, "X2");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y"), e.LeftAxisRange, "Y");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Y2"), e.RightAxisRange, "Y2");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "Z"), e.ZAxisRange, "Z");
            MoveAxisRangeDataToFixedAxis(AnalysisFixedAxises.FixedAxisCollection.SingleOrDefault(a => a.Axis == "W"), e.WAxisRange, "W");
        }

        private static void MoveAxisRangeDataToFixedAxis(FixedAxis axis, AxisRange ar, string name)
        {
            if (axis == null)
            {
                axis = new FixedAxis { Axis = name };
                AnalysisFixedAxises.FixedAxisCollection.Add(axis);
            }
            if (!axis.AxisMinFixed && !axis.AxisMinLocked && (ar.DisplayMinimum is double?)) axis.AxisMin = (double?)ar.DisplayMinimum;
            if (!axis.AxisMaxFixed && !axis.AxisMaxLocked && (ar.DisplayMaximum is double?)) axis.AxisMax = (double?)ar.DisplayMaximum;
            if (!axis.AxisMinFixed && !axis.AxisMinLocked && (ar.DisplayMinimum is DateTime?)) axis.AxisMinDate = (DateTime?)ar.DisplayMinimum;
            if (!axis.AxisMaxFixed && !axis.AxisMaxLocked && (ar.DisplayMaximum is DateTime?)) axis.AxisMaxDate = (DateTime?)ar.DisplayMaximum;
            if (!axis.StepFixed && !axis.StepLocked) axis.Step = ar.DisplayStep;
        }

        private static void MergeFixedAxis(FixedAxises chartSpecificAxises)
        {
            if (AnalysisFixedAxises == null) AnalysisFixedAxises = new FixedAxises();
            if (AnalysisFixedAxises.FixedAxisCollection == null) AnalysisFixedAxises.FixedAxisCollection = new ObservableCollection<FixedAxis>();
            MergeFixedAxisData(chartSpecificAxises, "X");
            MergeFixedAxisData(chartSpecificAxises, "X2");
            MergeFixedAxisData(chartSpecificAxises, "Y");
            MergeFixedAxisData(chartSpecificAxises, "Y2");
            MergeFixedAxisData(chartSpecificAxises, "Z");
            MergeFixedAxisData(chartSpecificAxises, "W");
        }
        private static void MergeFixedAxisData(FixedAxises chartSpecificAxises, string axisname)
        {
            if (chartSpecificAxises == null) return;
            var source = chartSpecificAxises.FixedAxisCollection.LastOrDefault(a => a.Axis == axisname);
            var destination = AnalysisFixedAxises.FixedAxisCollection.LastOrDefault(a => a.Axis == axisname);

            if (source != null)
            {
                if (destination != null)
                {
                    if (source.AxisMinLocked)
                    {
                        destination.AxisMin = source.AxisMin;
                        destination.AxisMinDate = source.AxisMinDate;
                        destination.AxisMinFixed = source.AxisMinFixed;
                    }
                    if (source.AxisMaxLocked)
                    {
                        destination.AxisMax = source.AxisMax;
                        destination.AxisMaxDate = source.AxisMaxDate;
                        destination.AxisMaxFixed = source.AxisMaxFixed;
                    }
                    if (source.StepLocked)
                    {
                        destination.Step = source.Step;
                        destination.StepFixed = source.StepFixed;
                    }
                    destination.AxisMinLocked = source.AxisMinLocked;
                    destination.AxisMaxLocked = source.AxisMaxLocked;
                    destination.StepLocked = source.StepLocked;
                    destination.AxisContinuous = source.AxisContinuous;
                }
                else
                {
                    AnalysisFixedAxises.FixedAxisCollection.Add(source);
                }
            }
        }
        private static void FinalizeRender(ChartRenderedCallBackDelegate chartReady, Panel panel,
            ReportChartLayoutMode layout1, List<SLDataTable> innerTables, bool dataExists, string chartDescription)
        {
            var panel1 = panel;
            if (!dataExists)
            {
                panel1 = new Canvas {Width = panel.ActualWidth, Height = panel.ActualHeight, Background = new SolidColorBrush(Colors.White)};
                panel1.Children.Add(new TextBlock {Text = "Нет данных", Margin = new Thickness(20),Foreground = new SolidColorBrush(Color.FromArgb(0x4f, 0x00, 0x80, 0xff)), FontSize = 48, HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Center});
            }

            var wb = new WriteableBitmap(panel1, (layout1 & ReportChartLayoutMode.HD) == ReportChartLayoutMode.HD
                    ? new ScaleTransform {ScaleX = 4, ScaleY = 4}
                    : new ScaleTransform {ScaleX = 2, ScaleY = 2});
                var iconLR = new WriteableBitmap(panel1, new ScaleTransform {ScaleX = 0.25, ScaleY = 0.25});
                var icon = new WriteableBitmap(panel1, new ScaleTransform {ScaleX = 1, ScaleY = 1});
                chartReady?.Invoke(new RenderedChart { Chart = wb, ThumbNail = icon, Icon = iconLR, InnerTables = innerTables, ChartDescription = chartDescription});
        }

        private static T DeserializeParameters<T>(Step s)
        {
            var xs = new XmlSerializer(typeof (T));
            var sr = new StringReader(s.ParametersXML.Replace("Parameters", typeof (T).Name));
            return (T) xs.Deserialize(XmlReader.Create(sr));
        }
    }
}