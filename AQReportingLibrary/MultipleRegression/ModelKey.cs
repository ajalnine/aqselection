﻿using AQMathClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{

    public static class ModelKey
    {
        public static string GetEmptyKey(List<string> independent)
        {
            return independent.Aggregate(string.Empty, (current, i) => current + "0");
        }

        public static string GetAllVariablesKey(List<string> independent)
        {
            return independent.Aggregate(string.Empty, (current, i) => current + "1");
        }

        public static string GetRandomKey(List<string> independent)
        {
            return independent.Aggregate(string.Empty, (current, i) => current + (AQMath.Rnd() > 0.5d ? "1" : "0"));
        }

        public static string GetInvertedKey(string key)
        {
            var k = string.Empty;
            foreach(var c in key)
            {
                k += (c == '0') ? "1" : "0";
            }
            return k;
        }

        public static string MutateKey(string key)
        {
            if (AQMath.Rnd() < 0.1) return key;
            var sb = new StringBuilder(key);
            var pos = (int)Math.Floor(AQMath.Rnd() * key.Length);
            sb[pos] = sb[pos] == '0' ? '1' : '0';
            return sb.ToString();
        }

        public static string GetChildKey(string key1, string key2)
        {
            var child = string.Empty;
            for (var i =0; i<key1.Length; i++)
            {
                child += AQMath.Rnd() > 0.5 ? key1[i] : key2[i];
            }
            return child;
        }

        public static string GetKeyBinary(int mask)
        {
            var key = string.Empty;
            for (var i = 0; i < 32; i++)
            {
                var m = (int)Math.Pow(2, i);
                key += ((mask & m) == m) ? "1": "0";
            }
            return key;
        }

        public static List<string> GetInitialKeys(List<string> independent)
        {
            var keys = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                var key = GetEmptyKey(independent);
                var sb = new StringBuilder(key) {[i] = '1'};
                keys.Add(sb.ToString());
            }
            return keys;
        }

        public static List<string> GetKeysForUnusedVariables(string key, List<string> independent)
        {
            var keys = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i] == '1') continue;
                var sb = new StringBuilder(key) {[i] = '1'};
                keys.Add(sb.ToString());
            }
            return keys;
        }

        public static List<string> GetKeysForUnusedVariablesExceptBlocked(string key, List<string> independent, List<string>blocked)
        {
            var keys = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i] == '1' || blocked.Contains(independent[i])) continue;
                var sb = new StringBuilder(key) {[i] = '1'};
                keys.Add(sb.ToString());
            }
            return keys;
        }

        public static List<string> GetKeysForUsedVariables(string key, List<string> independent)
        {
            var keys = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i] == '0') continue;
                var sb = new StringBuilder(key) {[i] = '0' };
                keys.Add(sb.ToString());
            }
            return keys;
        }

        public static List<string> GetKeysForUsedVariablesExceptBlocked(string key, List<string> independent, List<string> blocked)
        {
            var keys = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i] == '0' || blocked.Contains(independent[i])) continue;
                var sb = new StringBuilder(key) {[i] = '0'};
                keys.Add(sb.ToString());
            }
            return keys;
        }

        public static string AppendVariableToKey(string key, List<string> independent, string appendingVariable)
        {
            var sb = new StringBuilder(key) {[independent.IndexOf(appendingVariable)] = '1'};
            return sb.ToString();
        }

        public static string AppendVariablesToKey(string key, List<string> independent, List<string> appendingVariables)
        {
            var sb = new StringBuilder(key);
            foreach (var appendingVariable in appendingVariables) sb[independent.IndexOf(appendingVariable)] = '1';
            return sb.ToString();
        }

        public static string RemoveVariableFromKey(string key, List<string> independent, string removingVariable)
        {
            var sb = new StringBuilder(key) {[independent.IndexOf(removingVariable)] = '0'};
            return sb.ToString();
        }

        public static string RemoveVariablesFromKey(string key, List<string> independent, List<string> removingVariables)
        {
            var sb = new StringBuilder(key);
            foreach (var removingVariable in removingVariables) sb[independent.IndexOf(removingVariable)] = '0';
            return sb.ToString();
        }

        public static List<string> GetUnusedVariablesFromKey(string key, List<string> independent)
        {
            var result = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i]!='1')result.Add(independent[i]);
            }
            return result;
        }

        public static List<string> GetUsedVariablesFromKey(string key, List<string> independent)
        {
            var result = new List<string>();
            for (var i = 0; i < independent.Count; i++)
            {
                if (key[i] == '1') result.Add(independent[i]);
            }
            return result;
        }

        public static string GetKey(List<string> all, List<string> used)
        {
            var key = string.Empty;
            for (int i = 0; i < all.Count; i++) key += used.Contains(all[i]) ? "1" : "0";
            return key;
        }
    }
}