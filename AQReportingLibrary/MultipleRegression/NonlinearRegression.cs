﻿using ApplicationCore.CalculationServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using System.ComponentModel;
using System.Windows.Media;

namespace AQReportingLibrary
{

    public partial class Regression
    {
        private void NonlinearRegression(SLDataTable source, string dependent, List<string> independent, string layer, object zvalue,
            ReportChartRegressionStepwiseType stepType, ReportChartRegressionModelQuality modelQuality)
        {
            var nonLinearVariablesTable = new SLDataTable
            {
                ColumnNames = new List<string>(),
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string>(),
                RowTags = new List<object>(),
                SelectionString = source.SelectionString,
                TableName = source.TableName,
                Tag = source.TableName,
                Table = new List<SLDataRow>()
            };
            nonLinearVariablesTable.ColumnNames.AddRange(source.ColumnNames);
            if(source.ColumnTags!=null && source.ColumnTags.Count>0) nonLinearVariablesTable.ColumnTags.AddRange(source?.ColumnTags);
            if (source.CustomProperties != null && source.CustomProperties.Count > 0) nonLinearVariablesTable.CustomProperties.AddRange(source?.CustomProperties);
            nonLinearVariablesTable.DataTypes.AddRange(source.DataTypes);
            if (source.RowTags != null && source.RowTags.Count > 0) nonLinearVariablesTable.RowTags.AddRange(source.RowTags);

            foreach (var t in source.Table)
            {
                var row = new SLDataRow {Row = new List<object>()};
                for (int i = 0; i < source.ColumnNames.Count; i++)
                {
                    row.Row.Add(t.Row[i]);
                }
                nonLinearVariablesTable.Table.Add(row);
            }

            var availableNewFields = source.Table.Count / dependent.Count() / 7;
            
            foreach (string t in independent)
            {
                if (_nonLinear2)
                {
                    var sourceFieldIndex = source.ColumnNames.IndexOf(t);
                    var squareName = source.ColumnNames[sourceFieldIndex] + "²";
                    nonLinearVariablesTable.ColumnNames.Add(squareName);
                    _usedIndependentVariables.Add(squareName);
                    nonLinearVariablesTable.DataTypes.Add(source.DataTypes[sourceFieldIndex]);

                    if (source.ColumnTags != null)
                        nonLinearVariablesTable.ColumnTags.Add(source.Tag[sourceFieldIndex]);
                    if (source.CustomProperties != null)
                        nonLinearVariablesTable.CustomProperties.Add(source.CustomProperties[sourceFieldIndex]);

                    for (var j = 0; j < source.Table.Count; j++)
                    {
                        var value = source.Table[j].Row[sourceFieldIndex];
                        if (value is double) nonLinearVariablesTable.Table[j].Row.Add(Math.Pow((double) value, 2));
                        else nonLinearVariablesTable.Table[j].Row.Add(null);
                    }
                }

                if (_nonLinearLn)
                {
                    var sourceFieldIndex = source.ColumnNames.IndexOf(t);
                    var lnName = "Log[" + source.ColumnNames[sourceFieldIndex] + "]";
                    nonLinearVariablesTable.ColumnNames.Add(lnName);
                    _usedIndependentVariables.Add(lnName);
                    nonLinearVariablesTable.DataTypes.Add(source.DataTypes[sourceFieldIndex]);

                    if (source.ColumnTags != null)
                        nonLinearVariablesTable.ColumnTags.Add(source.Tag[sourceFieldIndex]);
                    if (source.CustomProperties != null)
                        nonLinearVariablesTable.CustomProperties.Add(source.CustomProperties[sourceFieldIndex]);

                    for (var j = 0; j < source.Table.Count; j++)
                    {
                        var value = source.Table[j].Row[sourceFieldIndex];
                        if (value is double && (double)value > 0) nonLinearVariablesTable.Table[j].Row.Add(Math.Log((double)value));
                        else nonLinearVariablesTable.Table[j].Row.Add(null);
                    }
                }

                if (_nonLinear3)
                {
                    var sourceFieldIndex = source.ColumnNames.IndexOf(t);
                    var cubeName = source.ColumnNames[sourceFieldIndex] + "³";
                    nonLinearVariablesTable.ColumnNames.Add(cubeName);
                    _usedIndependentVariables.Add(cubeName);
                    nonLinearVariablesTable.DataTypes.Add(source.DataTypes[sourceFieldIndex]);

                    if (source.ColumnTags != null)
                        nonLinearVariablesTable.ColumnTags.Add(source.Tag[sourceFieldIndex]);
                    if (source.CustomProperties != null)
                        nonLinearVariablesTable.CustomProperties.Add(source.CustomProperties[sourceFieldIndex]);

                    for (var j = 0; j < source.Table.Count; j++)
                    {
                        var value = source.Table[j].Row[sourceFieldIndex];
                        if (value is double) nonLinearVariablesTable.Table[j].Row.Add(Math.Pow((double)value, 3));
                        else nonLinearVariablesTable.Table[j].Row.Add(null);
                    }
                }

                if (_nonLinearSqrt)
                {
                    var sourceFieldIndex = source.ColumnNames.IndexOf(t);
                    var cubeName = "√["+source.ColumnNames[sourceFieldIndex] + "]";
                    nonLinearVariablesTable.ColumnNames.Add(cubeName);
                    _usedIndependentVariables.Add(cubeName);
                    nonLinearVariablesTable.DataTypes.Add(source.DataTypes[sourceFieldIndex]);

                    if (source.ColumnTags != null)
                        nonLinearVariablesTable.ColumnTags.Add(source.Tag[sourceFieldIndex]);
                    if (source.CustomProperties != null)
                        nonLinearVariablesTable.CustomProperties.Add(source.CustomProperties[sourceFieldIndex]);

                    for (var j = 0; j < source.Table.Count; j++)
                    {
                        var value = source.Table[j].Row[sourceFieldIndex];
                        if (value is double && (double)value >=0) nonLinearVariablesTable.Table[j].Row.Add(Math.Pow((double)value, 0.5));
                        else nonLinearVariablesTable.Table[j].Row.Add(null);
                    }
                }
            }
            SourceTable = nonLinearVariablesTable;
            StepwiseRegression(SourceTable, dependent, _usedIndependentVariables, layer, zvalue, modelQuality, stepType);
        }
    }
}