﻿using ApplicationCore.CalculationServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using System.Text;

namespace AQReportingLibrary
{
    public class RegressionModelQuality
    {
        public double Quality;
        public double[] p;
        public double ridgeLambda;
    }

    public class RegressionModel
    {
        public double Quality;
        public List<string> IndependentVariables;
        public string Key;
        public List<List<double>> StepData;
        public List<List<double>> StepCheckData;
        public List<List<double>> AllData;
        public RegressionInput RegressionInput;
        public double[,] PairCorrelation;
        public double[] p;
        public double ridgeLambda;

        public RegressionModel(string key, List<string> allVariables, List<List<double>> allData, ReportChartRegressionModelQuality modelQuality, List<List<double>> checkData, bool onlyRed, bool ridge)
        {
            AllData = allData;
            Key = key;
            IndependentVariables = new List<string>();
            StepData = new List<List<double>>();
            StepCheckData = new List<List<double>>();
            StepData.Add(allData[0]);
            StepCheckData.Add(checkData[0]);

            for (int i=0; i< allVariables.Count; i++)
            {
                if (Key[i]=='1')
                {
                    IndependentVariables.Add(allVariables[i]);
                    StepData.Add(allData[i+1]);
                    StepCheckData.Add(checkData[i + 1]);
                }
            }
            RegressionInput = Regression.CreateRegressionInput(StepData);
            if (RegressionInput == null)
            {
                Quality = double.PositiveInfinity;
                p = new double[StepData.Count];
                return;
            }
            PairCorrelation = Regression.CalcPairCorellation(StepData, RegressionInput);
            var m = ridge 
                ? Regression.GetRidgeModelQuality(StepData, RegressionInput, PairCorrelation, modelQuality, StepCheckData, onlyRed)
                : Regression.GetModelQuality(StepData, RegressionInput, PairCorrelation, modelQuality, StepCheckData, onlyRed);
            Quality = m.Quality;
            p = m.p;
            ridgeLambda = ridge ?  m.ridgeLambda : 0;
        }

        public RegressionModel(string key, List<string> allVariables, List<List<double>> allData, List<List<double>> checkData, ReportChartRegressionModelQuality modelQuality, bool onlyRed, bool ridge, RegressionInput ri)
        {
            AllData = allData;
            Key = key;
            IndependentVariables = new List<string>();
            StepData = new List<List<double>>();
            StepCheckData = new List<List<double>>();
            StepData.Add(allData[0]);
            StepCheckData.Add(checkData[0]);
            if (ri == null)
            {
                Quality = double.PositiveInfinity;
                p = new double[StepData.Count];
                return;
            }

            var variablesInKey = 0;
            for (int i = 0; i < allVariables.Count; i++)
            {
                if (Key[i] == '1')
                {
                    IndependentVariables.Add(allVariables[i]);
                    StepData.Add(allData[i + 1]);
                    StepCheckData.Add(checkData[i + 1]);
                    variablesInKey++;
                }
            }
            RegressionInput partialRI = new RegressionInput();
            partialRI.m = variablesInKey;
            partialRI.n = ri.n;
            partialRI.Average = new double[variablesInKey + 1];
            partialRI.Dispersion = new double[variablesInKey + 1];
            partialRI.Stdev = new double[variablesInKey + 1];
            partialRI.PairCorellation = new double[variablesInKey + 1, variablesInKey + 1];

            var v = 0;
            
            for (int i = 0; i < allVariables.Count + 1; i++)
            {
                if (i==0 || Key[i - 1] == '1')
                {
                    partialRI.Average[v] = ri.Average[i];
                    partialRI.Dispersion[v] = ri.Dispersion[i];
                    partialRI.Stdev[v] = ri.Stdev[i];
                    v++;
                }
            }

            v = 0;

            for (int i = 0; i < allVariables.Count + 1; i++)
            {
                if (i == 0 || Key[i - 1] == '1')
                {
                    var w = 0;
                    for (int j = 0; j < allVariables.Count + 1; j++)
                    {
                        if (j == 0 || Key[j - 1] == '1')
                        {
                            partialRI.PairCorellation[v, w] = ri.PairCorellation[i, j];
                            w++;
                        }
                    }
                    v++;
                }
            }
            PairCorrelation = partialRI.PairCorellation;
            RegressionInput = partialRI;

            var m = ridge 
                ?Regression.GetRidgeModelQuality(StepData, partialRI, partialRI.PairCorellation, modelQuality, StepCheckData, onlyRed)
                :Regression.GetModelQuality(StepData, partialRI, partialRI.PairCorellation, modelQuality, StepCheckData, onlyRed);
            Quality = m.Quality;
            p = m.p;
            ridgeLambda = ridge ? m.ridgeLambda : 0;
        }
    }
}