﻿using ApplicationCore.CalculationServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using System.Text;
using System.Windows.Input;

namespace AQReportingLibrary
{
    public partial class Regression
    {
        public void StepwiseRegression(SLDataTable source, string dependent, List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality, ReportChartRegressionStepwiseType modelMethod)
        {
            DependentVariable = dependent;
            var safeVariables = independent.ToList();
            if (safeVariables.Count>1)safeVariables.Remove(dependent);
            var bestmodel = GetBestModel(source, dependent, safeVariables, layer, zvalue, modelQuality, modelMethod);
            if (bestmodel == null || bestmodel.RegressionInput == null || bestmodel.RegressionInput.n==0) return;
            RegressionInput = bestmodel.RegressionInput;
            RegressionInput.PairCorellation = bestmodel.PairCorrelation;
            RegressionResult = CalcMultipleRegression(bestmodel.StepData, bestmodel.RegressionInput, bestmodel.PairCorrelation, ReportChartRegressionFitMode.DynamicSmoothLine, bestmodel.StepCheckData, bestmodel.ridgeLambda, false);
            RegressionResult.Correct = true;
            IndependentVariables = bestmodel.IndependentVariables;
            Order = new double[IndependentVariables.Count];
            for (int i = 0; i < IndependentVariables.Count; i++) Order[i] = 1;
        }
        
        private static void RemoveBadVariables(ref string key, ModelData modelData)
        {
            var data = modelData.LearningData;

            int position = 0;
            while (position < data.Count - 1)
            {
                var d = data[position + 1];
                var isSame = true;
                for (int i=0; i<d.Count - 1;i++)
                {
                    if (!(Math.Abs(d[i] - d[i+1]) > double.Epsilon)) continue;
                    isSame = false;
                    break;
                }
                if (isSame)
                {
                    var sb = new StringBuilder(key);
                    sb[position] = '0';
                    key = sb.ToString();
                }
                position++;
            }
        }
        
        public static RegressionModelQuality GetModelQuality(List<List<double>> variableData, RegressionInput ri, double[,] pc, ReportChartRegressionModelQuality modelQualityMode, List<List<double>> checkData, bool onlyRed)
        {
            var modelCheckData = (checkData == null || checkData.Count != variableData.Count) ? variableData : checkData;
            double[,] RMatrix = new double[ri.m, ri.m];
            double[] R0Matrix = new double[ri.m];
            var BMatrix = new double[ri.m];
            var BSE = new double[ri.m];
            var p = new double[ri.m];
            var t = new double[ri.m];
            for (int i = 0; i < ri.m; i++)
            {
                for (int j = 0; j < ri.m; j++)
                {
                    RMatrix[i, j] = pc[i + 1, j + 1];
                }
                R0Matrix[i] = pc[i + 1, 0];
            }

            var BetaMatrix = SolveSystem(RMatrix, R0Matrix).Item1;
            for (int i = 0; i < ri.m; i++)
            {
                BMatrix[i] = BetaMatrix[i] * ri.Stdev[0] / ri.Stdev[i + 1];
            }
            var Intercept = ri.Average[0];
            var MultipleR2 = 0.0d;

            for (int i = 0; i < ri.m; i++)
            {
                Intercept -= BMatrix[i] * ri.Average[i + 1];
                MultipleR2 += BetaMatrix[i] * R0Matrix[i];
            }
            double checkN = modelCheckData[0].Count;
            double[] calcDependentVariable = new double[(int)checkN];
            
            for (int j = 0; j < checkN; j++)
            {
                calcDependentVariable[j] = Intercept;
                for (int i = 0; i < ri.m; i++)
                {
                    calcDependentVariable[j] += BMatrix[i] * modelCheckData[i + 1][j];
                }
            }

            var RSS = 0.0d;
            for (int j = 0; j < checkN; j++)
            {
                RSS += (modelCheckData[0][j] - calcDependentVariable[j]) * (modelCheckData[0][j] - calcDependentVariable[j]);
            }
            
            double[,] indCorellation = new double[ri.m, ri.m];

            for (int i = 0; i < ri.m; i++) for (int j = 0; j < ri.m; j++) indCorellation[i, j] = pc[i + 1, j + 1];
            var indDet = AQMath.MatrixDeterminant(indCorellation);
            double d = checkN - ri.m - 1;
            double[,] xCorellation;
            double xdet, xr;
            double? studentCd;

            for (int b = 0; b < ri.m; b++)
            {
                xCorellation = GetXCorrelation(b, indCorellation);
                xdet = AQMath.MatrixDeterminant(xCorellation);
                xr = Math.Sqrt(Math.Abs(1 - indDet / xdet));

                BSE[b] = (ri.Stdev[0] * Math.Sqrt(1 - MultipleR2)) / (ri.Stdev[b + 1] * Math.Sqrt(1 - xr * xr)) / Math.Sqrt(d);
                t[b] = BMatrix[b] / BSE[b];
                studentCd = AQMath.StudentCD(t[b], d);
                if (studentCd != null) p[b] = t[b] < 0 ? studentCd.Value * 2 : (1 - studentCd.Value) * 2;
            }
            var maxLevel = p.Count() > 0 ? p.Max(a => Math.Abs(a)) : 0;
            var penalty = onlyRed && maxLevel>0.05 ? Math.Pow(Math.Exp(maxLevel) - 0.05, 3) : 1.0d;
            switch (modelQualityMode)
            {
                default:
                    return new RegressionModelQuality {Quality = - Math.Abs(MultipleR2)/ penalty, p = p};
                case ReportChartRegressionModelQuality.AIC:
                    return new RegressionModelQuality { Quality = (2 * ri.m + checkN * (Math.Log(2 * Math.PI * RSS / checkN) + 1)) * penalty, p = p };
                case ReportChartRegressionModelQuality.BIC:
                    return new RegressionModelQuality { Quality = (ri.m * Math.Log(checkN) + checkN * Math.Log(RSS / checkN)) * penalty, p = p };
                case ReportChartRegressionModelQuality.SE:
                    return new RegressionModelQuality { Quality = (Math.Abs(RSS / (checkN - ri.m - 1))) * penalty, p = p };
            }
        }

        public static RegressionModelQuality GetRidgeModelQuality(List<List<double>> variableData, RegressionInput ri, double[,] pc, ReportChartRegressionModelQuality modelQualityMode, List<List<double>> checkData, bool onlyRed)
        {
            var modelCheckData = (checkData == null || checkData.Count != variableData.Count) ? variableData : checkData;
            double[,] RMatrix = new double[ri.m, ri.m];
            double[] R0Matrix = new double[ri.m];
            var BMatrix = new double[ri.m];
            var BSE = new double[ri.m];
            var p = new double[ri.m];
            var t = new double[ri.m];
            var ridgeLambda = 0.0d;

            var tries = 0;
            double checkN = modelCheckData[0].Count;
            double d = checkN - ri.m - 1;
            double newRidge, MultipleR2, RSS, bb, CalcDispersion, StdError;
            do
            {
                newRidge = ridgeLambda;
                for (int i = 0; i < ri.m; i++)
                {
                    for (int j = 0; j < ri.m; j++)
                    {
                        RMatrix[i, j] = pc[i + 1, j + 1];
                        if (i == j) RMatrix[i, j] += ridgeLambda;
                    }
                    R0Matrix[i] = pc[i + 1, 0];
                }

                var BetaMatrix = SolveSystem(RMatrix, R0Matrix).Item1;
                for (int i = 0; i < ri.m; i++)
                {
                    BMatrix[i] = BetaMatrix[i]*ri.Stdev[0]/ri.Stdev[i + 1];
                }
                var Intercept = ri.Average[0];
                MultipleR2 = 0.0d;

                for (int i = 0; i < ri.m; i++)
                {
                    Intercept -= BMatrix[i]*ri.Average[i + 1];
                    MultipleR2 += BetaMatrix[i]*R0Matrix[i];
                }
                double[] calcDependentVariable = new double[(int)checkN];

                for (int j = 0; j < checkN; j++)
                {
                    calcDependentVariable[j] = Intercept;
                    for (int i = 0; i < ri.m; i++)
                    {
                        calcDependentVariable[j] += BMatrix[i]*modelCheckData[i + 1][j];
                    }
                }

                RSS = 0.0d;
                for (int j = 0; j < checkN; j++)
                {
                    RSS += (modelCheckData[0][j] - calcDependentVariable[j])*
                           (modelCheckData[0][j] - calcDependentVariable[j]);
                }
                CalcDispersion = RSS / d;
                bb = 0.0d;
                for (int i = 0; i < ri.m; i++)
                {
                    bb += Math.Pow(Math.Sqrt(ri.Dispersion[i + 1 ] * (checkN - 1)) * BMatrix[i], 2);
                }

                if (Math.Abs(bb) < double.Epsilon) break;
                newRidge = ri.m * (CalcDispersion) / bb;
                if (newRidge > 10)
                {
                    return GetModelQuality(variableData, ri, pc, modelQualityMode, checkData, onlyRed);
                }
                if (newRidge > 1) newRidge /= 2;
                if (Math.Abs(newRidge - ridgeLambda) < 0.00001 || tries > 100) break;
                ridgeLambda = newRidge;
                tries++;
            } while (true);

            ridgeLambda = newRidge;

            double[,] indCorellation = new double[ri.m, ri.m];

            for (int i = 0; i < ri.m; i++) for (int j = 0; j < ri.m; j++) indCorellation[i, j] = pc[i + 1, j + 1];
            var indDet = AQMath.MatrixDeterminant(indCorellation);
            double[,] xCorellation;
            double xdet, xr;
            double? studentCd;

            for (int b = 0; b < ri.m; b++)
            {
                xCorellation = GetXCorrelation(b, indCorellation);
                xdet = AQMath.MatrixDeterminant(xCorellation);
                xr = Math.Sqrt(Math.Abs(1 - indDet / xdet));

                BSE[b] = (ri.Stdev[0] * Math.Sqrt(1 - MultipleR2)) / (ri.Stdev[b + 1] * Math.Sqrt(1 - xr * xr)) / Math.Sqrt(d);
                t[b] = BMatrix[b] / BSE[b];
                studentCd = AQMath.StudentCD(t[b], d);
                if (studentCd != null) p[b] = t[b] < 0 ? studentCd.Value * 2 : (1 - studentCd.Value) * 2;
            }
            var maxLevel = p.Count() > 0 ? p.Max(a => Math.Abs(a)) : 0;
            var penalty = onlyRed && maxLevel > 0.05 ? Math.Pow(Math.Exp(maxLevel) - 0.05, 3) : 1.0d;

            var ridgePenalty = ridgeLambda * bb;
            switch (modelQualityMode)
            {
                default:
                    return new RegressionModelQuality { Quality = -Math.Abs(MultipleR2) / penalty, p = p, ridgeLambda = ridgeLambda};
                case ReportChartRegressionModelQuality.AIC:
                    return new RegressionModelQuality { Quality = (2 * ri.m + checkN * (Math.Log(2 * Math.PI * (RSS + ridgePenalty) / checkN) + 1)) * penalty, p = p, ridgeLambda = ridgeLambda };
                case ReportChartRegressionModelQuality.BIC:
                    return new RegressionModelQuality { Quality = (ri.m * Math.Log(checkN) + checkN * Math.Log((RSS + ridgePenalty) / checkN)) * penalty, p = p, ridgeLambda = ridgeLambda };
                case ReportChartRegressionModelQuality.SE:
                    return new RegressionModelQuality { Quality = (Math.Abs((RSS + ridgePenalty) / (checkN - ri.m - 1))) * penalty, p = p, ridgeLambda = ridgeLambda };
            }
        }
    }
}