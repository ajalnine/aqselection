﻿using ApplicationCore.CalculationServiceReference;
using AQMathClasses;
using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class Regression
    {
        public RegressionModel GetBestModel(SLDataTable source, string dependent, List<string> independent, string layer,
            object zvalue, ReportChartRegressionModelQuality modelQuality, ReportChartRegressionStepwiseType modelMethod)
        {
            switch (modelMethod)
            {
                case ReportChartRegressionStepwiseType.Full:
                    return _onlyFullCases 
                        ? GetFullCheckModelFullCases(source, dependent, ref independent, layer, zvalue, modelQuality)
                        : GetFullCheckModel(source, dependent, ref independent, layer, zvalue, modelQuality);

                case ReportChartRegressionStepwiseType.Forward:
                    return _onlyFullCases
                        ? GetForwardIncludeModelFullCases(source, dependent, ref independent, layer, zvalue,
                            modelQuality)
                        : GetForwardIncludeModel(source, dependent, ref independent, layer, zvalue, modelQuality);

                case ReportChartRegressionStepwiseType.Backward:
                    return _onlyFullCases
                        ? GetBackwardRemoveModelFullCases(source, dependent, ref independent, layer, zvalue,
                            modelQuality)
                        : GetBackwardRemoveModel(source, dependent, ref independent, layer, zvalue, modelQuality);

                case ReportChartRegressionStepwiseType.Group:
                    return _onlyFullCases
                        ? GetForwardGroupModelFullCases(source, dependent, ref independent, layer, zvalue, modelQuality)
                        : GetForwardGroupModel(source, dependent, ref independent, layer, zvalue, modelQuality);

                case ReportChartRegressionStepwiseType.Genetic:
                    return _onlyFullCases
                        ? GetGeneticModelFullCases(source, dependent, ref independent, layer, zvalue, modelQuality)
                        : GetGeneticModel(source, dependent, ref independent, layer, zvalue, modelQuality);

                default:
                    return GetAllVariablesModel(source, dependent, ref independent, layer, zvalue, modelQuality);
            }
        }
    }
}