﻿using System;
using System.Linq;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;
using System.Collections.Generic;

namespace AQReportingLibrary
{
    public class RegressionLine
    {
        public Regression RegressionInfo;
        public string AttachedSeries { get; set; }
        public string LineColor { get; set; }
        public double LineSize { get; set; }

        public RegressionLine(Regression regression)
        {
            RegressionInfo = regression;
        }

        public string GetRegressionComment(bool showN, bool showName = false)
        {

            string result = string.Empty;
            if (RegressionInfo.RegressionInput.n == RegressionInfo.RegressionInput.nt) result = showN ? "n=" + RegressionInfo.RegressionInput.n : string.Empty;
            else result = showN ? "n(модель)=" + RegressionInfo.RegressionInput.n + ", n(тест)=" + RegressionInfo.RegressionInput.nt : string.Empty;
            if (double.IsNaN(RegressionInfo.RegressionResult.MultipleR)) return result;
            result += Math.Abs(RegressionInfo.RegressionResult.MultipleR) < 0.4 || double.IsNaN(RegressionInfo.RegressionResult.MultipleR)
                    ? (", R=" + Math.Round(RegressionInfo.RegressionResult.MultipleR, 2))
                    : (@", <red>R=" + Math.Round(RegressionInfo.RegressionResult.MultipleR, 2) + @"</red>");

            result += Math.Abs(RegressionInfo.RegressionResult.MultipleR2) < 0.16 || double.IsNaN(RegressionInfo.RegressionResult.MultipleR2)
                    ? (", R²=" + Math.Round(RegressionInfo.RegressionResult.MultipleR2, 2))
                    : (@", <red>R²=" + Math.Round(RegressionInfo.RegressionResult.MultipleR2, 2) + @"</red>");

            result += Math.Abs(RegressionInfo.RegressionResult.MultipleRCorrected) < 0.16 || double.IsNaN(RegressionInfo.RegressionResult.MultipleRCorrected)
                    ? (", R²c=" + Math.Round(RegressionInfo.RegressionResult.MultipleRCorrected, 2))
                    : (@", <red>R²c=" + Math.Round(RegressionInfo.RegressionResult.MultipleRCorrected, 2) + @"</red>");

            result += ", F(" + RegressionInfo.RegressionInput.m + ", " + (RegressionInfo.RegressionInput.nt - RegressionInfo.RegressionInput.m -1) + ")=" + Math.Round(RegressionInfo.RegressionResult.F, 2);

            result += (RegressionInfo.RegressionResult.pF > 0.05 || double.IsNaN(RegressionInfo.RegressionResult.pF)
                ? (", pF=" + Math.Round(RegressionInfo.RegressionResult.pF, 2))
                    : (@", <red>pF=" + Math.Round(RegressionInfo.RegressionResult.pF, 2) + @"</red>"));

            result += ", SE=" + Math.Round(RegressionInfo.RegressionResult.StdError, 4);
            result += ", MAPE=" + Math.Round(RegressionInfo.RegressionResult.MAPE, 2)+"%";

            var modelDescription = string.Empty;
            if (RegressionInfo.RegressionResult.RidgeLambda>0) modelDescription += "Гребневая регрессия, λ=" + AQMath.SmartRound(RegressionInfo.RegressionResult.RidgeLambda, 3);
            if (RegressionInfo.RegressionInput.FillEmpty && (RegressionInfo.RegressionInput.n != RegressionInfo.RegressionInput.nt)) modelDescription += (!string.IsNullOrEmpty(modelDescription)? ", ": string.Empty) + "замена пропусков средним";
            if (RegressionInfo.RegressionInput.Halfed) modelDescription += (!string.IsNullOrEmpty(modelDescription) ? ", " : string.Empty) + "деление данных 50/50";
            if (RegressionInfo.RegressionInput.Interlaced) modelDescription += (!string.IsNullOrEmpty(modelDescription) ? ", " : string.Empty) + "чересстрочное деление данных";
            modelDescription += (!string.IsNullOrEmpty(modelDescription) ? ", " : string.Empty) + modelMethodDescriptions[RegressionInfo.RegressionInput.ModelMethod];
            modelDescription += (!string.IsNullOrEmpty(modelDescription) ? ", " : string.Empty) + modelQualityDescriptions[RegressionInfo.RegressionInput.ModelQuality];
            if (!string.IsNullOrEmpty(modelDescription)) result += modelDescription.Length >25 ? "\r\n" + modelDescription.CapitalizeFirst() : ", " + modelDescription;
            result += "\r\n";
            result += GetEquation(showName);
            return result;
        }

        Dictionary<ReportChartRegressionStepwiseType, string> modelMethodDescriptions = new Dictionary<ReportChartRegressionStepwiseType, string>
        {
            {ReportChartRegressionStepwiseType.All, "все выбранные факторы"  },
            {ReportChartRegressionStepwiseType.Backward, "пошаговое исключение"  },
            {ReportChartRegressionStepwiseType.Forward, "пошаговое включение"  },
            {ReportChartRegressionStepwiseType.Full, "анализ всех вариантов"  },
            {ReportChartRegressionStepwiseType.Genetic, "генетический алгоритм" },
            {ReportChartRegressionStepwiseType.Group, "G-Bi алгоритм" },
        };

        Dictionary<ReportChartRegressionModelQuality, string> modelQualityDescriptions = new Dictionary<ReportChartRegressionModelQuality, string>
        {
            {ReportChartRegressionModelQuality.AIC, "оценка по AIC" },
            {ReportChartRegressionModelQuality.BIC, "оценка по BIC" },
            {ReportChartRegressionModelQuality.R, "оценка по R²" },
            {ReportChartRegressionModelQuality.SE, "оценка по SE" },
        };

        private string GetEquation(bool showName = false)
        {
            if (RegressionInfo.RegressionResult.BMatrix == null) return null;
            switch (RegressionInfo.SmoothMode)
            {
                case ReportChartRegressionFitMode.DynamicSmoothLine:
                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                    var result = showName ? RegressionInfo.DependentVariable + " = " :"y = ";
                    var isRedIntercept = RegressionInfo.RegressionResult.pIntercept <= 0.05;

                    if (Math.Abs(RegressionInfo.RegressionResult.Intercept) > 1e-15) result += 
                            (isRedIntercept ? "<red>" : string.Empty) 
                            + AQMath.SmartRound(RegressionInfo.RegressionResult.Intercept, 5) 
                            + (isRedIntercept ? "</red>" : string.Empty);
                    bool isFirst = true;
                    var ei = new List<EquationItem>();
                    for (int k = 0; k < RegressionInfo.RegressionInput.m; k++)
                    {
                        ei.Add(new EquationItem {beta = RegressionInfo.RegressionResult.BetaMatrix[k], B = RegressionInfo.RegressionResult.BMatrix[k], Name = RegressionInfo?.IndependentVariables == null ? "x" : RegressionInfo?.IndependentVariables[k], Order = RegressionInfo.Order[k], p = RegressionInfo.RegressionResult.p[k] });
                    }
                    var orderedEI = ei.OrderByDescending(a => Math.Abs(a.beta)).ToList();

                    foreach (var e in orderedEI)
                    {
                        if (Math.Abs(e.B) < 1e-15) continue;
                        var isRed = e.p <= 0.05;
                        result += e.B < 0 ? " - " : ((Math.Abs(RegressionInfo.RegressionResult.Intercept) > 1e-15 || !isFirst) ? " + " : string.Empty);
                        if (Math.Abs(Math.Abs(e.B) - 1) > 1e-15) result += 
                                (isRed ? "<red>" : string.Empty) +
                               AQMath.SmartRound(Math.Abs(e.B), 5) +  (isRed ? "</red>" : string.Empty) + " ∙" ;
                        result += showName? " "+e.Name : " x";
                        var o = e.Order;
                        if (o == 2) result += "²";
                        if (o == 3) result += "³";
                        if (o > 3) result += $"^{o}";
                        isFirst = false;
                    }
                    return result;
                    
                case ReportChartRegressionFitMode.DynamicSmoothE:
                    var result2 = "y = ";
                    if (Math.Abs(RegressionInfo.RegressionResult.Intercept) > 1e-15) result2 += AQMath.SmartRound(RegressionInfo.RegressionResult.Intercept, 5) + " ∙ ";
                    result2 +="e^(" + AQMath.SmartRound(RegressionInfo.RegressionResult.BMatrix[0], 5)  + " ∙ x)";
                    return result2;

                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    var result3 = "y = ";
                    if (Math.Abs(RegressionInfo.RegressionResult.Intercept) > 1e-15) result3 += AQMath.SmartRound(RegressionInfo.RegressionResult.Intercept, 5);
                    result3 += RegressionInfo.RegressionResult.BMatrix[0] < 0 ? " - " : ((Math.Abs(RegressionInfo.RegressionResult.Intercept) > 1e-15) ? " + " : string.Empty);
                    result3 += AQMath.SmartRound(RegressionInfo.RegressionResult.BMatrix[0], 5) + " ∙ Ln(x)";
                    return result3;
                default:
                    return string.Empty;
            }
        }

        private class EquationItem
        {
            public double B;
            public double beta;
            public double p;
            public double Order;
            public string Name;
        }

        public SLDataTable GetCalculatedTable(string calculatedName, string residualsName, string layer, object zvalue)
        {
            if (RegressionInfo.RegressionResult == null) return null;
            var l = layer != null ? RegressionInfo.SourceTable.ColumnNames.IndexOf(layer) : -1;

            var resultingTable = new SLDataTable
            {
                ColumnNames = RegressionInfo.SourceTable.ColumnNames.ToList(),
                ColumnTags = RegressionInfo.SourceTable.ColumnTags?.ToList(),
                RowTags = new List<object>(),
                CustomProperties = RegressionInfo.SourceTable.CustomProperties?.ToList(),
                DataTypes = RegressionInfo.SourceTable.DataTypes.ToList(),
                SelectionString = RegressionInfo.SourceTable.SelectionString,
                TableName = RegressionInfo.SourceTable.TableName,
                Tag = RegressionInfo.SourceTable.Tag,
                Table = new List<SLDataRow>()
            };
            resultingTable.ColumnNames.Add(calculatedName);
            resultingTable.ColumnNames.Add(residualsName);
            if (resultingTable.ColumnTags != null)
            {
                resultingTable.ColumnTags.Add(null); resultingTable.ColumnTags.Add(null);
            }
            if (resultingTable.CustomProperties != null)
            {
                resultingTable.CustomProperties.Add(null); resultingTable.CustomProperties.Add(null);
            }
            resultingTable.DataTypes.Add("Double");
            resultingTable.DataTypes.Add("Double");
            var rowCounter = 0;
            foreach (var row in RegressionInfo.SourceTable.Table)
            {
                if (l < 0 || (row.Row[l] != null ? row.Row[l].ToString() == zvalue.ToString() : row.Row[l] == null))
                {
                    var newRow = new SLDataRow { Row = new List<object>() };
                    newRow.Row.AddRange(row.Row);
                    var value = GetCalculatedValue(row);
                    newRow.Row.Add(value);
                    newRow.Row.Add(GetResidualsValue(row, value));
                    resultingTable.Table.Add(newRow);
                    resultingTable.RowTags.Add(rowCounter);
                }
                rowCounter++;
            }

            return resultingTable;
        }

        public double? GetCalculatedValue(SLDataRow row)
        {
            if (RegressionInfo.RegressionResult.BMatrix == null) return null;
            var y = RegressionInfo.RegressionResult.Intercept;
            var indexes = RegressionInfo.IndependentVariables.Select(a => RegressionInfo.SourceTable.ColumnNames.IndexOf(a)).ToList();
            for (int i = 0; i < RegressionInfo.RegressionInput.m; i++)
            {
                var x = row.Row[indexes[i]];
                if (!(x is double) || x == null|| x == DBNull.Value) return null;
                y += RegressionInfo.RegressionResult.BMatrix[i] * Math.Pow((double)x, RegressionInfo.Order[i]);
            }
            return y;
        }

        public double? GetResidualsValue(SLDataRow row, double? calculatedValue)
        {
            var i= RegressionInfo.SourceTable.ColumnNames.IndexOf(RegressionInfo.DependentVariable);
            var y = row.Row[i];
            if (!(y is double) || y == null || y == DBNull.Value || !calculatedValue.HasValue) return null;
            return (double)y - calculatedValue.Value;
        }

        private double GetTransformedY(ChartRangesInfo cri, double y)
        {
            if (cri.YLeftSeries.Contains(AttachedSeries) || AttachedSeries == null)
                return (y - (double)cri.YLeftMin) / ((double)cri.YLeftMax - (double)cri.YLeftMin);
            return (y - (double)cri.YRightMin) / ((double)cri.YRightMax - (double)cri.YRightMin);
        }

        private double GetTransformedX(ChartRangesInfo cri, double X)
        {
            double x;
            if (cri.XDownSeries.Contains(AttachedSeries) || AttachedSeries == null)
            {
                if (cri.XDownMin is DateTime)
                {
                    x = ((((DateTime)cri.XDownMin).Ticks +
                          (((DateTime)cri.XDownMax).Ticks - ((DateTime)cri.XDownMin).Ticks) * X)) / 1e18;
                }
                else
                {
                    if (cri.XDownMax == null)
                    {
                        x = X;
                    }
                    else
                    {
                        x = ((double)cri.XDownMin + ((double)cri.XDownMax - (double)cri.XDownMin) * X);
                    }
                }
            }
            else
            {
                if (cri.XUpMin is DateTime)
                {
                    x = ((((DateTime)cri.XUpMin).Ticks +
                          (((DateTime)cri.XUpMax).Ticks - ((DateTime)cri.XUpMin).Ticks) * X)) / 1e18;
                }
                else
                {
                    if (cri.XUpMax == null)
                    {
                        x = X;
                    }
                    else
                    {
                        x = ((double)cri.XUpMin + ((double)cri.XUpMax - (double)cri.XUpMin) * X);
                    }
                }
            }
            return x;
        }

        public MathFunctionDescription SetupLine(string attachedSeries, string lineColor, double lineSize, object colorByZ, string seriesName)
        {
            AttachedSeries = attachedSeries;
            LineColor = lineColor;
            LineSize = lineSize;
            return SetupLine(colorByZ, seriesName);
        }

        public MathFunctionDescription SetupLine(double lineSize, object colorByZ, string seriesName)
        {
            LineSize = lineSize;
            return SetupLine(colorByZ, seriesName);
        }

        public MathFunctionDescription SetupLine(object colorByZ, string seriesName)
        {
            if (LineColor == null && colorByZ == null) LineColor = "#ffff4000";

            if (LineSize < 1e-15) LineSize = 2;

            switch (RegressionInfo.SmoothMode)
            {
                case ReportChartRegressionFitMode.DynamicSmoothLine:
                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                case ReportChartRegressionFitMode.DynamicSmoothQuartic:

                    var result = new MathFunctionDescription
                    {
                        LineSize = LineSize,
                        PixelStep = 4,
                        LineColor = LineColor,
                        ColorByZScale = colorByZ,
                        ZValueSeriesTitle = seriesName,
                        Description = (colorByZ != null ? "#" : string.Empty) + "Регрессия",
                        MathFunctionCalculator = (cri, X) =>
                        {
                            double x = GetTransformedX(cri, X);
                            var y = RegressionInfo.RegressionResult.Intercept;
                            for (int i = 0; i < RegressionInfo.RegressionInput.m; i++)
                            {
                                y += RegressionInfo.RegressionResult.BMatrix[i] * Math.Pow(x, RegressionInfo.Order[i]);
                            }

                            return GetTransformedY(cri, y);
                        }
                    };
                    return result;

                    case ReportChartRegressionFitMode.DynamicSmoothE:
                    var result2 = new MathFunctionDescription
                    {
                        LineSize = LineSize,
                        PixelStep = 4,
                        LineColor = LineColor,
                        ColorByZScale = colorByZ,
                        ZValueSeriesTitle = seriesName,
                        Description = (colorByZ != null ? "#" : string.Empty) + "Регрессия",
                        MathFunctionCalculator = (cri, X) =>
                        {
                            double x = GetTransformedX(cri, X);
                            var y = RegressionInfo.RegressionResult.Intercept * Math.Exp(x * RegressionInfo.RegressionResult.BMatrix[0]);
                            return GetTransformedY(cri, y);
                        }
                    };
                    return result2;

                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    var result3 = new MathFunctionDescription
                    {
                        LineSize = LineSize,
                        PixelStep = 4,
                        LineColor = LineColor,
                        ColorByZScale = colorByZ,
                        ZValueSeriesTitle = seriesName,
                        Description = (colorByZ != null ? "#" : string.Empty) + "Регрессия",
                        MathFunctionCalculator = (cri, X) =>
                        {
                            double x = GetTransformedX(cri, X);
                            var y = RegressionInfo.RegressionResult.Intercept  + RegressionInfo.RegressionResult.BMatrix[0] * Math.Log(x, Math.E);
                            return GetTransformedY(cri, y);
                        }
                    };
                    return result3;
                default:
                    return null;
            }
        }
    }
}