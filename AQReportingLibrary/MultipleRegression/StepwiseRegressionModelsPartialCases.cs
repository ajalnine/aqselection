﻿using ApplicationCore.CalculationServiceReference;
using AQMathClasses;
using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class Regression
    {
        private RegressionModel GetAllVariablesModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var key = ModelKey.GetKey(independent, independent);
            var modelData = GetVariableDataFromSLDataTable(key, source, dependent, independent, layer, zvalue);
            RemoveBadVariables(ref key, modelData);
            if (independent.Count == 0 || modelData.LearningData.Any(a => a.Count == 0)) return null;
            return new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
        }

        private RegressionModel GetFullCheckModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var rm = new List<RegressionModel>();
            var modelNumber = Math.Pow(2, independent.Count);
            for (int x = 1; x < modelNumber; x++)
            {
                if (_worker!=null && _worker.CancellationPending) return null;
                var key = ModelKey.GetKeyBinary(x);
                var modelData = GetVariableDataFromSLDataTable(key, source, dependent, independent, layer, zvalue);
                RemoveBadVariables(ref key, modelData);
                var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                if (!model.p.Any(a => a > 0.05) || modelQuality != ReportChartRegressionModelQuality.R) rm.Add(model);
            }
            return rm.OrderBy(a => a.Quality).FirstOrDefault();
        }

        private RegressionModel GetForwardIncludeModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var keys = ModelKey.GetInitialKeys(independent); // Начало с уравнений парной регрессии каждой переменной
            var badVariables = new List<string>();
            var previousBestQuality = double.PositiveInfinity;
            RegressionModel previousBest = null;
            do
            {
                if (keys.Count == 0) return previousBest; // Если все переменные либо уже в уравнении, либо отброшены
                var stepModels = new List<RegressionModel>();
                foreach (var key in keys)
                {
                    if (_worker != null && _worker.CancellationPending) return null;
                    var k = key;
                    var modelData = GetVariableDataFromSLDataTable(k, source, dependent, independent, layer, zvalue);
                    RemoveBadVariables(ref k, modelData);
                    var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                    stepModels.Add(model);
                }
                var current = stepModels.OrderBy(a => a.Quality).FirstOrDefault(); //Выбор лучшей модели
                var currentModelQuality = current?.Quality;  
                if (currentModelQuality < previousBestQuality && badVariables.Count < independent.Count)
                {
                    if (modelQuality == ReportChartRegressionModelQuality.R) // Для модели, оцениваемой по R - удаление всех незначимых переменных
                    {
                        badVariables.AddRange(current.IndependentVariables.Where((t, i) => Math.Abs(current.p[i]) > 0.05));
                        current.Key = ModelKey.RemoveVariablesFromKey(current.Key, independent, badVariables);
                        previousBest = new RegressionModel(current.Key, independent, current.AllData, modelQuality, current.AllData, _onlyRed, _ridge);
                        previousBestQuality = previousBest.Quality;
                    }
                    else
                    {
                        previousBest = current;
                        previousBestQuality = current.Quality;
                    }
                    keys = ModelKey.GetKeysForUnusedVariablesExceptBlocked(previousBest.Key, independent, badVariables); //Для нового набора уравнений - все оставшиеся переменые кроме отброшенных как незначимые
                }
                else return previousBest; //Если не удалось найти лучшей модели
            } while (true);
        }

        private RegressionModel GetBackwardRemoveModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            if (modelQuality == ReportChartRegressionModelQuality.R) return BackwardRemoveModelForR(source, dependent, independent, layer, zvalue, modelQuality);
            var initialKey = ModelKey.GetAllVariablesKey(independent); // Начало с уравнения всех переменных
            var svd = GetVariableDataFromSLDataTable(initialKey, source, dependent, independent,
                    layer, zvalue);
            RemoveBadVariables(ref initialKey, svd);
            var bestModel = new RegressionModel(initialKey, independent, svd.LearningData, modelQuality, svd.TestData, _onlyRed, _ridge);

            var previousBestQuality = bestModel.Quality;

            do
            {
                var previousKey = bestModel.Key;
                var keys = ModelKey.GetKeysForUsedVariables(previousKey, independent); //Для нового набора уравнений - из уравнения удаляются по одной все оставшиеся переменые кроме отброшенных как незначимые
                if (keys.Count == 0) return bestModel;
                var stepModels = new List<RegressionModel>();
                foreach (var key in keys)
                {
                    if (_worker != null && _worker.CancellationPending) return null;
                    var k = key;
                    var modelData = GetVariableDataFromSLDataTable(k, source, dependent, independent, layer, zvalue);
                    RemoveBadVariables(ref k, modelData);
                    var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                    stepModels.Add(model);
                }
                var current = stepModels.OrderBy(a => a.Quality).FirstOrDefault(); //Выбор лучшей модели
                var currentModelQuality = current?.Quality;
                if (currentModelQuality < previousBestQuality)
                {
                        bestModel = current;
                        previousBestQuality = current.Quality;
                }
                else return bestModel; //Если не удалось найти лучшей модели
            } while (true);
        }

        private RegressionModel BackwardRemoveModelForR(SLDataTable source, string dependent, List<string> independent,
            string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var key = ModelKey.GetAllVariablesKey(independent); // Начало с уравнений парной регрессии каждой переменной
            do
            {
                var modelData = GetVariableDataFromSLDataTable(key, source, dependent, independent,
                    layer, zvalue);
                RemoveBadVariables(ref key, modelData);
                var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                var badP = 0.0d;
                var badVariable = string.Empty;
                for (var i = 0; i < model.IndependentVariables.Count; i++)
                {
                    if (_worker != null && _worker.CancellationPending) return null;
                    var p = Math.Abs(model.p[i]);
                    if (!(p > badP)) continue;
                    badP = p;
                    badVariable = model.IndependentVariables[i];
                }
                if (badP <= 0.05d) return model;
                key = ModelKey.RemoveVariableFromKey(key, independent, badVariable);
            } while (true);
        }

        private RegressionModel GetForwardGroupModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var keys = ModelKey.GetInitialKeys(independent); // Начало с уравнений парной регрессии каждой переменной
            var previousBestQuality = double.PositiveInfinity;
            var groupNumber = independent.Count / 2;
            var oldKeys = new List<string>();
            RegressionModel previousBest = null;
            var counter = 0;
            do
            {
                if (keys.Count == 0) return previousBest; // Если все переменные либо уже в уравнении, либо отброшены
                var stepModels = new List<RegressionModel>();
                foreach (var key in keys)
                {
                    if (_worker != null && _worker.CancellationPending) return null;
                    var k = key;
                    var modelData = GetVariableDataFromSLDataTable(k, source, dependent, independent, layer, zvalue);
                    RemoveBadVariables(ref k, modelData);
                    var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                    stepModels.Add(model);
                    if (!oldKeys.Contains(model.Key))oldKeys.Add(model.Key);
                }
                var bestModels = modelQuality != ReportChartRegressionModelQuality.R
                    ? stepModels.OrderBy(a => a.Quality).Take(groupNumber).ToList() //Выбор лучших моделей
                    : stepModels.Where(a=>a.IndependentVariables.Count>0 && a.p.Select(Math.Abs).Max()<=0.05).OrderBy(a => a.Quality).Take(groupNumber).ToList();//Выбор лучших моделей
                var current = bestModels.FirstOrDefault(); //Выбор самой лучшей модели для возможности прекращения поиска
                var currentModelQuality = current?.Quality;
                counter++;
                if (currentModelQuality < previousBestQuality || counter>2000)
                {
                    keys = new List<string>();
                    foreach (var m in bestModels)
                    {
                        previousBest = current;
                        previousBestQuality = current.Quality;

                        keys.AddRange(ModelKey.GetKeysForUnusedVariables(m.Key, independent)
                            .Union(ModelKey.GetKeysForUsedVariables(m.Key, independent)).ToList());
                    }
                    keys = keys.Except(oldKeys).Distinct().ToList();
                }
                else return previousBest; //Если не удалось найти лучшей модели
            } while (true);
        }

        private RegressionModel GetGeneticModel(SLDataTable source, string dependent, ref List<string> independent, string layer, object zvalue, ReportChartRegressionModelQuality modelQuality)
        {
            var keys = new List<string>();
            var generationSize = independent.Count * 2;
            var bestSize = generationSize / 2;
            for (var i = 0; i < generationSize / 2; i++)
            {
                var k = ModelKey.GetRandomKey(independent);
                keys.Add(k);
                keys.Add(ModelKey.GetInvertedKey(k));
            }

            var oldModels = new Dictionary<string, RegressionModel>();
            RegressionModel previousBest = null;
            var counter = 0;
            do
            {
                if (keys.Count == 0) return previousBest; // Если все переменные либо уже в уравнении, либо отброшены
                var stepModels = new List<RegressionModel>();
                foreach (var key in keys)
                {
                    if (_worker != null && _worker.CancellationPending) return null;
                    if (!oldModels.ContainsKey(key))
                    {
                        var k = key;
                        var modelData = GetVariableDataFromSLDataTable(k, source, dependent, independent, layer, zvalue);
                        RemoveBadVariables(ref k, modelData);
                        var model = new RegressionModel(key, independent, modelData.LearningData, modelQuality, modelData.TestData, _onlyRed, _ridge);
                        stepModels.Add(model);
                        oldModels.Add(model.Key, model);
                    }
                    else stepModels.Add(oldModels[key]);
                }
                var bestModels = stepModels.Where(a=>!double.IsNaN(a.Quality)).OrderBy(a => a.Quality).Take(bestSize).ToList();//Выбор лучших моделей
                var current = bestModels.FirstOrDefault(); //Выбор самой лучшей модели
                if (current == null) return oldModels.Values.OrderBy(a => a.Quality).FirstOrDefault();
                counter++;
                if (counter < generationSize * generationSize)
                {
                    keys = new List<string>();
                    keys.AddRange(bestModels.Select(a => a.Key));
                    while (keys.Count < generationSize)
                    {
                        if (_worker != null && _worker.CancellationPending) return null;
                        var partner = (int)AQMath.Rnd() * bestModels.Count;
                        keys.Add(ModelKey.GetChildKey(current.Key, bestModels[partner].Key));
                    }
                    keys = keys.Take(generationSize).Select(a => ModelKey.MutateKey(a)).ToList();
                }
                else return oldModels.Values.OrderBy(a => a.Quality).FirstOrDefault();
            } while (true);
        }
    }
}