﻿using ApplicationCore.CalculationServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using System.ComponentModel;
using System.Windows.Media;

namespace AQReportingLibrary
{
    public enum LDRUsedFields {XY, XI, YI }

    public class RegressionInput
    {
        public int n;
        public int nt;
        public int m;
        public double[,] PairCorellation;
        public double[] Average;
        public double[] Dispersion;
        public double[] Stdev;
        public bool FillEmpty;
        public bool Interlaced;
        public bool FullCases;
        public bool Halfed;
        public ReportChartRegressionStepwiseType ModelMethod;
        public ReportChartRegressionModelQuality ModelQuality;
    }

    public class RegressionResult
    {
        public bool Correct;
        public double StdError;
        public double MAPE;
        public double MS;
        public double MSE;
        public double MeanOfResult;
        public double StdevOfResult;
        public double Mean;
        public double StDev;
        public double MultipleR;
        public double MultipleR2;
        public double MultipleRCorrected;
        public double StDevOfResiduals;
        public double F, d;
        public double pF;
        public double CalcDispersion = 0;
        public double[] BetaMatrix;
        public bool[] RedundantCheckResult;
        public double[] BMatrix;
        public double Intercept;
        public double BIntercept, tIntercept, pIntercept;
        public double[] t;
        public double[] p;
        public double[] BSE;
        public double[] BetaSE;
        public double AIC;
        public double BIC;
        public double RSS;
        public double RidgeLambda;
        public RegressionInput Input;
    }
    
    public partial class Regression
    {
        public RegressionInput RegressionInput;
        public RegressionResult RegressionResult;    
        public SLDataTable SourceTable, _source;

        public double[] Order;
        public List<string> IndependentVariables;
        public string DependentVariable;
        public ReportChartRegressionFitMode SmoothMode = ReportChartRegressionFitMode.DynamicSmoothQuadratic;
        private BackgroundWorker _worker;
        private bool _externalCheck, _onlyRed, _onlyFullCases, _externalCheckInterlaced, _ridge,_nonLinear, _nonLinearLn, _nonLinear2, _nonLinear3, _nonLinearSqrt, _fillEmpty;
        private List<string> _usedIndependentVariables;
        private ReportChartRegressionOptions _options;
        private ReportChartRegressionStepwiseType _modelMethod;
        private ReportChartRegressionModelQuality _modelQuality;

        public Regression(List<List<double>> variableData, bool GLMOnly)
        {
            if (variableData.Count < 1) return;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, GLMOnly);
        }

        public Regression(SLDataTable source, string dependent, List<string> independent, string layer, object zvalue,
            ReportChartRegressionStepwiseType stepType, ReportChartRegressionModelQuality modelQuality, ReportChartRegressionOptions options, BackgroundWorker worker = null)
        {
            _options = options;
            _externalCheck = (_options & ReportChartRegressionOptions.ExternalControl) == ReportChartRegressionOptions.ExternalControl;
            _externalCheckInterlaced = (_options & ReportChartRegressionOptions.ExternalControlInterlaced) == ReportChartRegressionOptions.ExternalControlInterlaced;
            _onlyRed = (_options & ReportChartRegressionOptions.OnlyRed) == ReportChartRegressionOptions.OnlyRed;
            _onlyFullCases = (_options & ReportChartRegressionOptions.OnlyFullCases) == ReportChartRegressionOptions.OnlyFullCases;
            _ridge = (_options & ReportChartRegressionOptions.Ridge) == ReportChartRegressionOptions.Ridge;
            _nonLinearLn = (_options & ReportChartRegressionOptions.NonLinearLn) == ReportChartRegressionOptions.NonLinearLn;
            _nonLinear2 = (_options & ReportChartRegressionOptions.NonLinear2) == ReportChartRegressionOptions.NonLinear2;
            _nonLinear3 = (_options & ReportChartRegressionOptions.NonLinear3) == ReportChartRegressionOptions.NonLinear3;
            _nonLinearSqrt = (_options & ReportChartRegressionOptions.NonLinearSqrt) == ReportChartRegressionOptions.NonLinearSqrt;
            _nonLinear = _nonLinear2 || _nonLinear3 || _nonLinearLn || _nonLinearSqrt;
            _fillEmpty = (_options & ReportChartRegressionOptions.FillEmpty) == ReportChartRegressionOptions.FillEmpty;
            _worker = worker;
            _usedIndependentVariables = new List<string>();
            _usedIndependentVariables.AddRange(independent);
            _modelMethod = stepType;
            _modelQuality = modelQuality;
            if (_nonLinear) NonlinearRegression(source, dependent, independent, layer, zvalue, stepType, modelQuality);
            else
            {
                SourceTable = source;
                StepwiseRegression(source, dependent, independent, layer, zvalue, modelQuality, stepType);
            }
        }
        
        public void FullRegression(SLDataTable source, string dependent, List<string> independent, string layer, object zvalue)
        {
            var modelData = GetVariableDataFromSLDataTable(null, source, dependent, independent, layer, zvalue);
            var variableData = modelData.LearningData;
            if (variableData.Count < 1) return;
            Order = new double[variableData.Count];
            for (var i = 0; i < variableData.Count; i++) Order[i] = 1;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
            IndependentVariables = independent;
            DependentVariable = dependent;
        }

        public Regression(SLDataTable source, string dependent, string independent, ReportChartRegressionFitMode smoothMode)
        {
            SourceTable = source;
            SmoothMode = smoothMode;
            switch (smoothMode)
            {
                    case ReportChartRegressionFitMode.DynamicSmoothNone:
                    case ReportChartRegressionFitMode.DynamicSmoothLowess:
                    case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                    case ReportChartRegressionFitMode.DynamicSmoothE:
                    case ReportChartRegressionFitMode.DynamicSmoothLn: 
                    return;

                    case ReportChartRegressionFitMode.DynamicSmoothLine:
                        SetupPolynome(source, dependent, independent, 1);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                    SetupPolynome(source, dependent, independent, 2);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                    SetupPolynome(source, dependent, independent, 3);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                    SetupPolynome(source, dependent, independent, 4);
                    break;
            }
            IndependentVariables = new List<string> { independent };
            DependentVariable = dependent;
        }

        public Regression(SLDataTable source, string dependent, string independent, int order)
        {
            SourceTable = source;
            IndependentVariables = new List<string> { independent };
            DependentVariable = dependent;
            SetupPolynome(source, dependent, independent, order);
        }

        public Regression(List<DynamicRow> ldr, LDRUsedFields luf)
        {
            var data = ldr.Where((a => ((a.X != null || a.Xf != null) && a.Y != null)));
            var caseNumber = data.Count();
            var dep = new List<double>();
            var ind = new List<double>();
            switch (luf)
            {
               case LDRUsedFields.XI:
                    data = ldr.Where(a => (a.X != null || a.Xf != null && a.Y != null));
                    foreach (var l in data)
                    {
                        ind.Add(l.Index / caseNumber + 0.5 / caseNumber);
                        dep.Add(l.X.HasValue ? l.X.Value : l.Xf.Value);
                    }
                    break;
                case LDRUsedFields.XY:
                    foreach (var l in data)
                    {
                        ind.Add(l.X.HasValue ? l.X.Value : l.Xf.Value);
                        dep.Add(l.Y.Value);
                    }
                    break;
                case  LDRUsedFields.YI:
                    foreach (var l in data)
                    {
                        ind.Add(l.Index / caseNumber + 0.5 / caseNumber);
                        dep.Add(l.Y.Value);
                    }
                    break;
            }
            var variableData = new List<List<double>> {dep, ind};
            Order = new double[variableData.Count];
            for (var i = 0; i < variableData.Count; i++) Order[i] = 1;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
        }

        public Regression(List<DynamicRow> ldr, LDRUsedFields luf, int order)
        {
            SetupPolynome(ldr, luf, order);
        }

        public Regression(List<DynamicRow> ldr, LDRUsedFields luf, ReportChartRegressionFitMode smoothMode)
        {
            SmoothMode = smoothMode;
            switch (smoothMode)
            {
                case ReportChartRegressionFitMode.DynamicSmoothNone:
                case ReportChartRegressionFitMode.DynamicSmoothLowess:
                case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                    return;

                case ReportChartRegressionFitMode.DynamicSmoothLine:
                    SetupPolynome(ldr, luf, 1);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                    SetupPolynome(ldr, luf, 2);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                    SetupPolynome(ldr, luf, 3);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                    SetupPolynome(ldr, luf, 4);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothE:
                    SetupExponent(ldr, luf);
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    SetupLn(ldr, luf);
                    break;
            }
        }
        
        private void SetupPolynome(SLDataTable source, string dependent, string independent, int order)
        {
            IndependentVariables = new List<string> { independent };
            DependentVariable = dependent;
            var modelData = GetVariableDataFromSLDataTable(null, source, dependent, new List<string> { independent }, null, null);
            var variableData = modelData.LearningData;
            var caseNumber = variableData.Count;
            if (caseNumber < 1 ) return;
            for (var i = 2; i <= order; i++)
            {
                var d = new List<double>();
                for (var j = 0; j < caseNumber; j++)
                {
                    d.Add(Math.Pow(variableData[1][j], i));
                }
                variableData.Add(d);
            }
            for (var i = 0; i < caseNumber; i++) Order[i] = order;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
        }

        private void SetupPolynome(List<DynamicRow> ldr, LDRUsedFields luf, int order)
        {
            var data = ldr.Where((a => ((a.X != null || a.Xf != null) && a.Y != null)));
            var dep = new List<double>();
            var ind = new List<List<double>>();
            var caseNumber = data.Count();

            for (var k = 1; k <= order; k++)
            {
                ind.Add(new List<double>());
            }
            switch (luf)
            {
                case LDRUsedFields.XI:
                    data = ldr.Where(a => (a.X != null || a.Xf != null && a.Y != null));
                    foreach (var l in data)
                    {
                        for (var o = 1; o <= order; o++)
                        {
                            ind[o - 1].Add(Math.Pow((double) l.Index / caseNumber + 0.5 / caseNumber, o));
                        }
                        dep.Add(l.X.HasValue ? l.X.Value : l.Xf.Value);
                    }
                    break;
                case LDRUsedFields.XY:
                    foreach (var l in data)
                    {
                        var v = l.X.HasValue ? l.X.Value : l.Xf.Value;
                        for (var o = 1; o <= order; o++)
                        {
                            ind[o - 1].Add(Math.Pow(v, o));
                        }
                        dep.Add(l.Y.Value);
                    }
                    break;
                case LDRUsedFields.YI:
                    foreach (var l in data)
                    {
                        for (var o = 1; o <= order; o++)
                        {
                            ind[o - 1].Add(Math.Pow((double) l.Index/ caseNumber + 0.5/ caseNumber, o));
                        }
                        dep.Add(l.Y.Value);
                    }
                    break;
            }
            var variableData = new List<List<double>>();
            variableData.Add(dep);
            variableData.AddRange(ind);
            Order = new double[variableData.Count];
            for (var i = 0; i < variableData.Count; i++) Order[i] = i + 1;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
        }

        private void SetupExponent(List<DynamicRow> ldr, LDRUsedFields luf)
        {
            var data = ldr.Where((a => ((a.X != null || a.Xf != null) && a.Y != null)));
            var dep = new List<double>();
            var ind = new List<List<double>>();
            var caseNumber = data.Count();

            ind.Add(new List<double>());
            switch (luf)
            {
                case LDRUsedFields.XI:
                    data = ldr.Where(a => (a.X != null || a.Xf != null && a.Y != null));
                    foreach (var l in data)
                    {
                        ind[0].Add((double)l.Index / caseNumber + 0.5 / caseNumber);
                        dep.Add(l.X.HasValue ? Math.Log(l.X.Value, Math.E) : Math.Log(l.Xf.Value, Math.E));
                    }
                    break;
                case LDRUsedFields.XY:
                    foreach (var l in data)
                    {
                        var v = l.X.HasValue ? l.X.Value : l.Xf.Value;
                        ind[0].Add(v);
                        dep.Add(Math.Log(l.Y.Value, Math.E));
                    }
                    break;
                case LDRUsedFields.YI:
                    foreach (var l in data)
                    {
                        ind[0].Add((double)l.Index / caseNumber + 0.5 / caseNumber);
                        dep.Add(Math.Log(l.Y.Value, Math.E));
                    }
                    break;
            }
            var variableData = new List<List<double>>();
            variableData.Add(dep);
            variableData.AddRange(ind);
            Order = new double[variableData.Count];
            for (var i = 0; i < variableData.Count; i++) Order[i] =1;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
            RegressionResult.Intercept = Math.Exp(RegressionResult.Intercept);
        }

        private void SetupLn(List<DynamicRow> ldr, LDRUsedFields luf)
        {
            var data = ldr.Where((a => ((a.X != null || a.Xf != null) && a.Y != null)));
            var dep = new List<double>();
            var ind = new List<List<double>>();
            var caseNumber = data.Count();

            ind.Add(new List<double>());
            switch (luf)
            {
                case LDRUsedFields.XI:
                    data = ldr.Where(a => (a.X != null || a.Xf != null && a.Y != null));
                    foreach (var l in data)
                    {
                        ind[0].Add(Math.Log((double)l.Index / caseNumber + 0.5 / caseNumber, Math.E));
                        dep.Add(l.X ?? l.Xf.Value);
                    }
                    break;
                case LDRUsedFields.XY:
                    foreach (var l in data)
                    {
                        var v = l.X ?? l.Xf.Value;
                        ind[0].Add(Math.Log(v, Math.E));
                        dep.Add(l.Y.Value);
                    }
                    break;
                case LDRUsedFields.YI:
                    foreach (var l in data)
                    {
                        ind[0].Add(Math.Log((double)l.Index / caseNumber + 0.5 / caseNumber, Math.E));
                        dep.Add(l.Y.Value);
                    }
                    break;
            }
            var variableData = new List<List<double>>();
            variableData.Add(dep);
            variableData.AddRange(ind);
            Order = new double[variableData.Count];
            for (var i = 0; i < variableData.Count; i++) Order[i] = 1;
            RegressionResult = CalculateRegressionParameters(variableData, SmoothMode, false);
        }

        private ModelData GetVariableDataFromSLDataTable(string key, SLDataTable source, string dependent, List<string> independent, string layer, object zvalue)
        {
            var indep = GetValidModelData(key, source, dependent, independent, layer, zvalue);

            if (_fillEmpty)
            {
                var indep2 = GetEmptyFilledModelData(key, source, dependent, independent, layer, zvalue);
                if (!_externalCheck && !_externalCheckInterlaced) return new ModelData { LearningData = indep2, TestData = indep };
                var div1 = DivideData(indep);
                var div2 = DivideData(indep2);
                return new ModelData { LearningData = div2.LearningData, TestData = div1.TestData };
            }
            else
            {
                if (!_externalCheck && !_externalCheckInterlaced) return new ModelData { LearningData = indep, TestData = indep };
                return DivideData(indep);
            }
            
        }

        private List<List<double>> GetValidModelData(string key, SLDataTable source, string dependent, List<string> independent, string layer, object zvalue)
        {
            var k = key ?? ModelKey.GetAllVariablesKey(independent);
            var dep = new List<double>();
            var indep = new List<List<double>>();
            var ii = new Dictionary<int, int>();
            for (var i = 0; i < independent.Count(); i++)
            {
                indep.Add(new List<double>());
                ii.Add(i, source.ColumnNames.IndexOf(independent[i]));
            }

            var l = layer != null ? source.ColumnNames.IndexOf(layer) : -1;
            var di = source.ColumnNames.IndexOf(dependent);
            foreach (var row in source.Table)
            {
                var rowGood = true;
                if (row.Row[di] == null || row.Row[di] == DBNull.Value || !(row.Row[di] is double) || (l >= 0 && !(zvalue != null && row.Row[l] != null ? row.Row[l].ToString() == zvalue.ToString() : row.Row[l] == null)))
                {
                    rowGood = false;
                    continue;
                }
                for (var j = 0; j < independent.Count; j++)
                {
                    if (k[j] == '0') continue;
                    if (row.Row[ii[j]] == null || row.Row[ii[j]] == DBNull.Value || !(row.Row[ii[j]] is double))
                    {
                        rowGood = false;
                        continue;
                    }
                }
                if (rowGood)
                {
                    dep.Add((double)row.Row[di]);
                    for (var j = 0; j < independent.Count; j++)
                    {
                        indep[j].Add(k[j] == '1' ? (double)row.Row[ii[j]] : double.NaN);
                    }
                }
            }

            indep.Insert(0, dep);
            return indep;
        }

        private List<List<double>> GetEmptyFilledModelData(string key, SLDataTable source, string dependent, List<string> independent, string layer, object zvalue)
        {
            var k = key ?? ModelKey.GetAllVariablesKey(independent);
            var dep = new List<double>();
            var indep = new List<List<double>>();
            var ii = new Dictionary<int, int>();
            var indepAvg = new List<double>();

            for (var i = 0; i < independent.Count(); i++)
            {
                indep.Add(new List<double>());
                var index = source.ColumnNames.IndexOf(independent[i]);
                ii.Add(i, index);
            }
            var l = layer != null ? source.ColumnNames.IndexOf(layer) : -1;
            var di = source.ColumnNames.IndexOf(dependent);

            foreach (var row in source.Table)
            {
                if ((l >= 0 && !(zvalue != null && row.Row[l] != null ? row.Row[l].ToString() == zvalue.ToString() : row.Row[l] == null)))
                {
                    continue;
                }
                if (row.Row[di] == null || row.Row[di] == DBNull.Value || !(row.Row[di] is double)) continue;

                dep.Add((double)row.Row[di]);

                for (var j = 0; j < independent.Count; j++)
                {
                    if (k[j] == '0') indep[j].Add(double.NaN);
                    if (row.Row[ii[j]] == null || row.Row[ii[j]] == DBNull.Value || !(row.Row[ii[j]] is double))indep[j].Add(double.NaN);
                    else indep[j].Add(k[j] == '1' ? (double)row.Row[ii[j]] : double.NaN);
                }
            }
            for (var j = 0; j < independent.Count; j++)
            {
                if (k[j] == '0') indepAvg.Add(double.NaN);
                else indepAvg.Add(AQMath.AggregateMean(indep[j].Where(a=>!double.IsNaN(a)).OfType<Double>().Cast<object>().ToList()).Value);
            }
            for (var j = 0; j < independent.Count; j++)
            {
                if (k[j] == '0') continue;
                for (var i = 0; i <indep[j].Count; i++)
                {
                    if (double.IsNaN(indep[j][i])) indep[j][i] = indepAvg[j];
                }
            }
            indep.Insert(0, dep);
            return indep;
        }

        private ModelData DivideData(List<List<double>> indep)
        {
            var learning = new List<List<double>>();
            var testing = new List<List<double>>();
            for (var i = 0; i < indep.Count(); i++)
            {
                learning.Add(new List<double>());
                testing.Add(new List<double>());
            }
            var learningN = (int) indep[0].Count/2;
            if (_externalCheckInterlaced && !_externalCheck)
            {
                for (var i = 0; i < indep.Count; i++)
                {
                    for (var j = 0; j < learningN*2; j++)
                    {
                        if (j%2 == 1) learning[i].Add(indep[i][j]);
                        else testing[i].Add(indep[i][j]);
                    }
                }
                return new ModelData {LearningData = learning, TestData = testing};
            }
            for (var i = 0; i < indep.Count; i++)
            {
                for (var j = 0; j < learningN*2; j++)
                {
                    if (j < learningN) learning[i].Add(indep[i][j]);
                    else testing[i].Add(indep[i][j]);
                }
            }
            return new ModelData {LearningData = learning, TestData = testing};
        }

        public static RegressionInput CreateRegressionInput(List<List<double>> variableData)
        {
            var ri = new RegressionInput();
            ri.m = variableData.Count - 1;
            ri.n = variableData[0].Count;
            ri.Average = new double[variableData.Count];
            ri.Stdev = new double[variableData.Count];
            ri.Dispersion = new double[variableData.Count];
            if (ri.n < 1) return null;
            for (var i = 0; i < variableData.Count; i++)
            {
                var a = AQMath.AggregateMean(variableData[i].Cast<object>().ToList());
                if (!a.HasValue) return null;
                ri.Average[i] = a.Value;

                var s = AQMath.AggregateStDev(variableData[i].Cast<object>().ToList());
                if (!s.HasValue) return null;
                ri.Stdev[i] = s.Value;
                ri.Dispersion[i] = s.Value * s.Value;
            }
            return ri;
        }
        
        private RegressionResult CalculateRegressionParameters(List<List<double>> variableData, ReportChartRegressionFitMode smoothMode, bool GLMOnly)
        {
            RegressionInput = CreateRegressionInput(variableData);
            if (RegressionInput == null) return null;
            var pc= CalcPairCorellation(variableData, RegressionInput);
            var rr = CalcMultipleRegression(variableData, RegressionInput, pc, smoothMode, null, 0, GLMOnly);
            rr.Correct = true;
            return rr;
        }

        public static double[,] CalcPairCorellation(List<List<double>> variableData, RegressionInput ri)
		{
            var pairCorellation = new double[ri.m + 1, ri.m + 1];
            double value, value2;
            for (var j = 0; j < ri.n; j++)
			{
				for (var i = 0; i < ri.m + 1; i++)
				{
                    for (var k = 0; k < ri.m + 1; k++)
					{
                        value = variableData[i][j];
                        value2 = variableData[k][j];
                        pairCorellation[i,k] += ((value - ri.Average[i])/ ri.Stdev[i]) * ((value2 - ri.Average[k])/ ri.Stdev[k]);
					}
				}
			}
			for (var i = 0; i < ri.m + 1; i++)
			{
				for (var k = 0; k < ri.m + 1; k++)
				{
					pairCorellation[i,k] /= (ri.n -1);
				}
			}
            ri.PairCorellation = pairCorellation;
            return pairCorellation;
		}
        
        private RegressionResult CalcMultipleRegression(List<List<double>> variableData, RegressionInput ri, double[,] pc, ReportChartRegressionFitMode smoothMode, List<List<double>> checkData, double ridgeLambda, bool GLMOnly)
		{
            var modelCheckData = (checkData == null || checkData.Count!=variableData.Count) ? variableData : checkData;
            var rr = new RegressionResult();
            rr.Input = ri;
            ri.FillEmpty = _fillEmpty;
            ri.FullCases = _onlyFullCases;
            ri.Halfed = _externalCheck;
            ri.Interlaced = _externalCheckInterlaced;
            ri.ModelMethod = _modelMethod;
            ri.ModelQuality = _modelQuality;
            ri.nt = modelCheckData[0].Count;

            var RMatrix = new double[ri.m, ri.m];
			var R0Matrix = new double[ri.m];
            var residuals = new double[ri.n];
			rr.BMatrix = new double[ri.m];
            rr.BSE = new double[ri.m];
            rr.BetaSE = new double[ri.m];
            rr.MAPE = 0.0d;
            rr.Mean = ri.Average[0];
            rr.StDev = ri.Stdev[0];
            var MAPE = 0.0d;
            var nMAPE = 0;

            for (var i = 0; i < ri.m; i++)
			{
				for (var j = 0; j < ri.m; j++)
				{
					RMatrix[i,j]=pc[i + 1, j + 1];
				    if (i == j) RMatrix[i, j] += ridgeLambda;
				}
				R0Matrix[i]=pc[ i + 1,0];
			}

            var solveResult = SolveSystem(RMatrix, R0Matrix);
            rr.BetaMatrix = solveResult.Item1;
            rr.RedundantCheckResult = solveResult.Item2;
            for (var i = 0; i < ri.m; i++)
            {
                if (ri.Stdev[i] < double.Epsilon) rr.RedundantCheckResult[i] = true;
            }
			for (var i=0; i < ri.m; i++)
			{
				rr.BMatrix[i] = rr.BetaMatrix[i] * ri.Stdev[0] / ri.Stdev[i + 1];
			}
            rr.Intercept = ri.Average[0];
			rr.MultipleR = 0;
            
            for (var i = 0; i < ri.m; i++)
			{
                rr.Intercept -= rr.BMatrix[i] * ri.Average[i + 1];
				rr.MultipleR += rr.BetaMatrix[i] * R0Matrix[i];
			}
			rr.MultipleR = Math.Sqrt(rr.MultipleR);
            
            var calcDependentVariable = new double[(int)ri.nt];

            switch (smoothMode)
            {

                case ReportChartRegressionFitMode.DynamicSmoothNone:
                case ReportChartRegressionFitMode.DynamicSmoothLowess:
                case ReportChartRegressionFitMode.DynamicSmoothIntervals:
                    return null;

                case ReportChartRegressionFitMode.DynamicSmoothLine:
                case ReportChartRegressionFitMode.DynamicSmoothQuadratic:
                case ReportChartRegressionFitMode.DynamicSmoothCubic:
                case ReportChartRegressionFitMode.DynamicSmoothQuartic:
                    for (var j = 0; j < ri.nt; j++)
                    {
                        calcDependentVariable[j] = rr.Intercept;
                        for (var i = 0; i < ri.m; i++)
                        {
                            calcDependentVariable[j] += rr.BMatrix[i] * modelCheckData[i + 1][j];
                        }
                    }
                    for (var j = 0; j < ri.nt; j++)
                    {
                        residuals[j] = modelCheckData[0][j] - calcDependentVariable[j];
                        rr.RSS += Math.Pow(residuals[j], 2);
                        if (!(Math.Abs(calcDependentVariable[j]) > double.Epsilon)) continue;
                        MAPE += Math.Abs(modelCheckData[0][j] - calcDependentVariable[j]) / modelCheckData[0][j];
                        nMAPE++;
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothE:
                    for (var j = 0; j < ri.nt; j++)
                    {
                        calcDependentVariable[j] += Math.Exp(rr.Intercept) * Math.Exp(rr.BMatrix[0] * modelCheckData[1][j]);
                    }
                    for (var j = 0; j < ri.nt; j++)
                    {
                        residuals[j] = Math.Exp(modelCheckData[0][j]) - calcDependentVariable[j];
                        rr.RSS += Math.Pow(residuals[j], 2);
                        if (!(Math.Abs(calcDependentVariable[j]) > double.Epsilon)) continue;
                        MAPE += Math.Abs(modelCheckData[0][j] - calcDependentVariable[j]) / modelCheckData[0][j];
                        nMAPE++;
                    }
                    break;

                case ReportChartRegressionFitMode.DynamicSmoothLn:
                    for (var j = 0; j < ri.nt; j++)
                    {
                        calcDependentVariable[j] += rr.Intercept + rr.BMatrix[0] * modelCheckData[1][j];
                    }
                    for (var j = 0; j < ri.nt; j++)
                    {
                        residuals[j] = modelCheckData[0][j] - calcDependentVariable[j];
                        rr.RSS += Math.Pow(residuals[j], 2);
                        if (!(Math.Abs(calcDependentVariable[j]) > double.Epsilon)) continue;
                        MAPE += Math.Abs(modelCheckData[0][j] - calcDependentVariable[j]) / modelCheckData[0][j];
                        nMAPE++;
                    }
                    break;
            }
            var calculated = calcDependentVariable.Select(a => (object) a).ToList();
            rr.MeanOfResult = AQMath.AggregateMean(calculated).Value;
            rr.StdevOfResult = AQMath.AggregateStDev(calculated).Value;
            rr.StDevOfResiduals = AQMath.AggregateStDev(residuals.Select(a => (object) a).ToList()).Value;

            rr.MAPE = nMAPE!=0 ? 100.0f * MAPE / nMAPE : 0; 
            rr.MS = Math.Pow(AQMath.AggregateStDevPopulation(calcDependentVariable.OfType<object>().ToList()).Value, 2) * ri.nt; // for GLM
            rr.CalcDispersion = rr.RSS / (ri.nt - ri.m - 1);
            rr.MSE = rr.CalcDispersion;
            rr.StdError = Math.Sqrt(rr.CalcDispersion);
            rr.MultipleR2 = rr.MultipleR * rr.MultipleR;
            rr.MultipleRCorrected = 1 - (1 - rr.MultipleR2) * (ri.nt - 1) / (ri.nt - ri.m - 1);
            if (GLMOnly) return rr;
            rr.d = ri.nt - ri.m - 1;
            rr.t = new double[ri.m];
            rr.p = new double[ri.m];
            rr.F = rr.MultipleR2 / (1 - rr.MultipleR2) * rr.d / ri.m;

            rr.pF = double.IsNaN(rr.F) || double.IsInfinity(rr.F) ? 0 : 1 - AQMath.FisherCD(rr.F, ri.m, ri.nt).Value;

            var indCorellation = new double[ri.m, ri.m];

            for (var i = 0; i < ri.m; i++) for (var j = 0; j < ri.m; j++) indCorellation[i, j] = pc[i + 1, j + 1];
            var indDet = AQMath.MatrixDeterminant(indCorellation);
            var YR = Math.Sqrt(1 - AQMath.MatrixDeterminant(pc) / indDet);
            for (var b = 0; b < ri.m; b++)
            {
                var xCorellation = GetXCorrelation(b, indCorellation);
                var xdet = AQMath.MatrixDeterminant(xCorellation);
                var xr = Math.Sqrt(Math.Abs(1 - (indDet/xdet)));
                
                rr.BSE[b] = (ri.Stdev[0] * Math.Sqrt(1 - rr.MultipleR2)) /  (ri.Stdev[b + 1] * Math.Sqrt(1 - xr * xr)) / Math.Sqrt(rr.d);
                rr.BetaSE[b] = rr.BSE[b] /( ri.Stdev[0] / ri.Stdev[b + 1]);
                rr.t[b] = rr.BMatrix[b] / rr.BSE[b];
                var studentCd = AQMath.StudentCD(rr.t[b], rr.d);
                if (studentCd != null) rr.p[b] = rr.t[b]<0 ? studentCd.Value * 2 : (1 - studentCd.Value) * 2;
            }
            rr.AIC = 2 * ri.m + ri.nt * (Math.Log(2 * Math.PI * rr.RSS / ri.nt) + 1);
            rr.BIC = ri.m * Math.Log(ri.nt) + ri.nt * Math.Log(rr.RSS / ri.nt);
            rr.BIntercept = GetBIntercept(variableData, rr.CalcDispersion);
            rr.tIntercept = rr.Intercept/rr.BIntercept;
            rr.RidgeLambda = ridgeLambda;
            
            var cd = AQMath.StudentCD(rr.tIntercept, rr.d);
            if (cd != null) rr.pIntercept = rr.tIntercept < 0 ? cd.Value * 2 : (1 - cd.Value) * 2;
            return rr;
		}

        private static double GetBIntercept(List<List<double>> variableData, double calcDispersion)
        {
            var n = variableData[0].Count;
            var m = variableData.Count - 1;

            var X = new double[m + 1, n];
            for (var j = 0; j < n; j++) // X
            {
                X[0, j] = 1;
                for (var i = 1; i < m + 1; i++)
                {
                    X[i, j] = variableData[i][j];
                }
            }
            var TX = new double[n, m + 1];
            for (var j = 0; j < n; j++) //XT
            {
                for (var i = 0; i < m + 1; i++)
                {
                    TX[j, i] = X[i, j];
                }
            }

            var c = new double[m + 1, m + 1];
            for (var i = 0; i < m + 1; i++) //  XT * X
            {
                for (var j = 0; j < m + 1; j++)
                {
                    for (var k = 0; k < n; k++)
                        c[i, j] += TX[k, j]*X[i, k];
                }
            }

            var minor = new double[m, m]; // Обращение, расчет только [0,0] для скорости
            for (var j = 0; j < m; j++)
            {
                for (var i = 0; i < m; i++)
                {
                    minor[j, i] = c[i + 1, j + 1];
                }
            }
            
            var c1 = AQMath.MatrixDeterminant(minor) / AQMath.MatrixDeterminant(c);

            return Math.Sqrt(calcDispersion*c1);
        }

        private static double[,] GetXCorrelation(int b, double[,] indCorellation)
        {
            var m = indCorellation.GetLength(0);
            double[,] XCorellation;
            XCorellation = new double[m - 1, m - 1];
            var x = 0;
            for (var i = 0; i < m; i++)
            {
                var y = 0;
                if (i == b) i++;
                if (i == m) continue;
                for (var j = 0; j < m; j++)
                {
                    if (j == b) j++;
                    if (j == m) continue;
                    XCorellation[x, y] = indCorellation[i, j];
                    y++;
                }
                x++;
            }
            return XCorellation;
        }

        private static Tuple<double[], bool[]> SolveSystem (double[,] K, double[] K0)
		{
			var NumberOfVariables = K0.Length;
			var M = new double[NumberOfVariables, NumberOfVariables];
			var M0 = new double[NumberOfVariables];
			var Result = new double[NumberOfVariables];
            var Redundant = new bool[NumberOfVariables];
			for (var i = 0; i < NumberOfVariables; i++)
			{
				for (var j = 0; j < NumberOfVariables; j++)
				{	
					M[i,j] = K[i,j];
				}
				M0[i] = K0[i];
			}
            double Diff;
			for (var i = 0; i < NumberOfVariables - 1; i++)
			{
				for (var k = i + 1; k < NumberOfVariables; k++)
				{
					Diff = M[k,i] / M[i,i];
					for (var j=i; j < NumberOfVariables; j++)
					{
						M[k,j]-=M[i,j] * Diff;
					}
					M0[k]-=M0[i] * Diff;
				}
			}
            for (var i = 0; i < NumberOfVariables; i++)
            {
                Redundant[i] = Math.Abs(M0[i]) < 1e-7 || double.IsNaN(M0[i]);
            }

            double summ;
			for (var i = NumberOfVariables - 1; i>=0; i--)
			{
				summ = M0[i];
				for (var j = NumberOfVariables-1; j > i; j--)
				{
					    summ -= M[i,j] * Result[j]; 
				}
				Result[i] = summ / M[i,i];
			}
			return new Tuple<double[], bool[]>(Result, Redundant);
		}
    }

    public class ModelData
    {
        public List<List<double>> LearningData { get; set; }
        public List<List<double>> TestData { get; set; }
    }
}