﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;

namespace AQReportingLibrary
{
    public partial class Limits
    {
        public string GetProbabilityComment(bool includeProcessQuality)
        {
            var vals = new List<string>();
            if (includeProcessQuality) GetProcessQualityComments(vals);
            GetRangeQualityComments(vals);
            return EmitProbabilityComment(vals);
        }

        private static string EmitProbabilityComment(IEnumerable<string> vals)
        {
            var isFirst = true;
            var probabilityComment = string.Empty;
            foreach (var s in vals)
            {
                if (s.StartsWith("\r\n"))
                {
                    isFirst = true;
                    probabilityComment += s;
                }
                else
                {
                    probabilityComment += !isFirst ? ", " + s : s;
                    isFirst = false;
                }
            }
            return probabilityComment;
        }

        private void GetRangeQualityComments(List<string> vals)
        {
            if (LimitCount == 0) return;
            var data = LimitForX 
                ? from d in _ldr select d.X
                : from d in _ldr select d.Y;

            int ? nBelow = (MinLimit.HasValue) ? (int?) (from d in data where d < MinLimit.Value select d).Count() : null;
            int? nAbove = (MaxLimit.HasValue) ? (int?) (from d in data where d > MaxLimit.Value select d).Count() : null;

            double? percentBelow = (nBelow.HasValue) ? (double?) 100*nBelow/_n : null;
            double? percentAbove = (nAbove.HasValue) ? (double?) 100*nAbove/_n : null;
            double acceptable = 100;
            if (percentBelow.HasValue) acceptable -= percentBelow.Value;
            if (percentAbove.HasValue) acceptable -= percentAbove.Value;
            var nAcceptable = _n;
            if (nBelow.HasValue) nAcceptable -= nBelow.Value;
            if (nAbove.HasValue) nAcceptable -= nAbove.Value;
            var rangeName = LimitCount < 2 ? String.Empty :
                (MinLimit.HasValue ? " от " + MinLimit : string.Empty) + (MaxLimit.HasValue ? " до " + MaxLimit : string.Empty);
            vals.Add(string.Format("\r\nВ границах{2}: {0}% (n={1}) ", Math.Round(acceptable, 2), nAcceptable, rangeName));
            if (percentBelow.HasValue)
                vals.Add(string.Format("Ниже: {0}% (n={1})", Math.Round(percentBelow.Value, 2), nBelow.Value));
            if (percentAbove.HasValue)
                vals.Add(string.Format("Выше: {0}% (n={1})", Math.Round(percentAbove.Value, 2), nAbove.Value));
        }

        private void GetProcessQualityComments(List<string> vals)
        {
            double sr = GetSigmaRageEstimation();
            double? sv = AQMath.AggregateStDev(_data);
            double? xv = AQMath.AggregateMean(_data);
            if (!sv.HasValue || !xv.HasValue) return;

            double? png = (MinLimit.HasValue) ? AQMath.NormalCD(MinLimit, xv, sv) : null;
            double? pvg = (MaxLimit.HasValue) ? AQMath.NormalCD(MaxLimit, xv, sv) : null;

            double? cp = (MinLimit.HasValue && MaxLimit.HasValue) ? (double?) (MaxLimit.Value - MinLimit.Value)/(6*sr) : null;
            double? cpl = (MinLimit.HasValue) ? (xv - MinLimit.Value)/(3*sr) : null;
            double? cpu = (MaxLimit.HasValue) ? (MaxLimit.Value - xv)/(3*sr) : null;

            double? pp = (MinLimit.HasValue && MaxLimit.HasValue) ? (MaxLimit.Value - MinLimit.Value)/(6*sv) : null;
            double? ppl = (MinLimit.HasValue) ? (xv - MinLimit.Value)/(3*sv) : null;
            double? ppu = (MaxLimit.HasValue) ? (MaxLimit.Value - xv)/(3*sv) : null;
            if (cpl.HasValue || cpu.HasValue || cp.HasValue || pp.HasValue || ppl.HasValue || ppu.HasValue || png.HasValue || pvg.HasValue)
            {
                vals.Add("\r\nИндексы процесса: ");
                if (cp.HasValue) vals.Add("Cp=" + Math.Round(cp.Value, 2));
                if (cpl.HasValue && cpu.HasValue) vals.Add("Cpk=" + Math.Round(Math.Min(cpl.Value, cpu.Value), 2));
                else
                {
                    if (cpl.HasValue) vals.Add("Cpk=" + Math.Round(cpl.Value, 2));
                    if (cpu.HasValue) vals.Add("Cpk=" + Math.Round(cpu.Value, 2));
                }

                if (pp.HasValue) vals.Add("Pp=" + Math.Round(pp.Value, 2));
                if (ppl.HasValue && ppu.HasValue) vals.Add("Ppk=" + Math.Round(Math.Min(ppl.Value, ppu.Value), 2));
                else
                {
                    if (ppl.HasValue) vals.Add("Ppk=" + Math.Round(ppl.Value, 2));
                    if (ppu.HasValue) vals.Add("Ppk=" + Math.Round(ppu.Value, 2));
                }

                if (png.HasValue) vals.Add("Pнг=" + Math.Round(png.Value, 2));
                if (pvg.HasValue) vals.Add("Pвг=" + Math.Round(1 - pvg.Value, 2));
            }
        }

        private double GetSigmaRageEstimation()
        {
            double rr = 0.0d;
            for (var j = 0; j < _n - 1; j++) rr += Math.Abs(_ldr[j + 1].Y.Value - _ldr[j].Y.Value);
            var sr = rr/_n/1.10d;
            return sr;
        }
    }
}