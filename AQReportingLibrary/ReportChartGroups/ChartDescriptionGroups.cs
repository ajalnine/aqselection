﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQBasicControlsLibrary;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
        private IEnumerable<ChartDescription> GetDescriptionForBoxWhiskers(Data d, ReportChartGroupsParameters p)
        {
            var result = new List<ChartDescription>();
            var n = d.GetDataTable().Table.Count();
            var cd = new ChartDescription();
            result.Add(cd);
            cd.ChartLegendType = _p.ChartLegend;
            var withCenters = (p.Mode & ReportChartGroupMode.Center)>0;
            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = "n=" + n + (string.IsNullOrEmpty( _groupANOVA?.Comment) ? string.Empty : ", " + _groupANOVA?.Comment);
            cd.ChartSubtitle += (_chartRangesYTable.Table.Count > 0) ? RangesHelper.GetDescription(p.RangeType) : String.Empty;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0 && !string.IsNullOrWhiteSpace(_selectedTable.GetFilterDescription())) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartSeries = new List<ChartSeriesDescription>();
            var layered = (_p.AnalisysMode == ReportChartGroupAnalisysMode.Multiple ||_p.AnalisysMode == ReportChartGroupAnalisysMode.Extended) && _p.ColorField != "Нет";
            RangesHelper.AppendRanges(cd, false, _chartRangesYTable, p.RangeType == ReportChartRangeType.RangeIsSigmas || p.RangeType == ReportChartRangeType.RangeIsSlide || p.RangeType == ReportChartRangeType.RangeIsR, true, p.RangeStyle, p.RangeType == ReportChartRangeType.RangeIsAB);
            if (_p.ColorField != "Нет")
            { 
                var csd = new ChartSeriesDescription
                {
                    SourceTableName = "Данные",
                    ZColumnName = _p.ColorField,
                    ZAxisTitle = _p.ColorField,
                    SeriesTitle = _p.ColorField,
                    ColorScale = _p.ColorScale,
                    ColorScaleMode = _p.ColorScaleMode,
                    MarkerSizeMode = p.MarkerSizeMode,
                    MarkerShapeMode = p.MarkerShapeMode,
                    MarkerMode = (_p.ColorScale & ColorScales.BW) == ColorScales.BW ? MarkerModes.Solid : p.MarkerMode,
                    SeriesType = ChartSeriesType.Dummy,
                };
                cd.ChartSeries.Add(csd);
            }
            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Статистика",
                XColumnName = "#Группа#",
                XIsNominal = false,
                XAxisTitle = p.GroupField,
                YColumnName = "Статистика",
                ZValueSeriesTitle = _p.ColorField != "Нет" ? _p.ColorField : null,
                SeriesTitle =  p.Center == ReportChartGroupCenter.Mean ? "#Средние" : "#Медианы",
                SeriesType = ChartSeriesType.BoxWhiskers,
                ColorColumnName = "Tag",
                LineSize = _isSorted && withCenters && !layered ? 0.5 : 0,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerSize = 7,
                MarkerSizeMode = p.MarkerSizeMode,
                MarkerShapeMode = p.MarkerShapeMode,
                MarkerMode = p.MarkerMode,
                YAxisTitle = _selectedParameter,
                Options = _boxMode.ToString() + _center.ToString() + ((!_outliers) ? "NonOutliers" : String.Empty) 
                + (!((_boxMode & ReportChartGroupMode.Point) >0 )&& (_p.AnalisysMode == ReportChartGroupAnalisysMode.Single || (!((_boxMode & ReportChartGroupMode.Box) > 0 && !((_boxMode & ReportChartGroupMode.Violin) > 0 )))) && _p.ColorField != "Нет" ? "Pie" : string.Empty)
            };

            cd.ChartSeries.Add(csd2);

            if (!String.IsNullOrEmpty(_probabilityComment)) cd.ChartSubtitle += _probabilityComment;

            return result;
        }
    }
}