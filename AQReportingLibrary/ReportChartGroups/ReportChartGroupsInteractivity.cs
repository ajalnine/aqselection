﻿using System;
using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Collections.Generic;
using System.Linq;

namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
       
        void ChartDynamics_AxisClicked(object o, AxisEventArgs e)
        {
            if (AxisClicked != null) AxisClicked.Invoke(this, e);
        }

        private void ChartDynamics_SelectionChanged(object o, ChartSelectionEventArgs e)
        {
            if (_selectedTable == null || e.Table == null)
            {
                Interactivity?.Invoke(this, new InteractivityEventArgs { AvailableOperations = InteractiveOperations.None, });
                return;
            }
            var rowList = new List<int>();
            var message = _selectedParameter + " = ";
            var isFirst = true;

            foreach (var ii in e.InteractivityIndexes)
            {
                var f = _selectedTable.ColumnNames.IndexOf(_selectedParameter);
                var g = _selectedTable.ColumnNames.IndexOf(_p.GroupField);
                var z = _selectedTable.ColumnNames.IndexOf(_p.ColorField);
                var sourceIndex = ii.Index;
                if (!isFirst) message += "; ";

                switch (ii.SeriesName)
                {
                    case "#Средние":
                    case "#Медианы":
                    case "Статистика":
                        var group = _groupStatisticsTable.Table[ii.Index].Row[3].ToString();
                        var layer = _groupStatisticsTable.Table[ii.Index].Row[4]?.ToString();
                        if (layer == null)
                        {
                            message += $"все значения в группе {_p.GroupField}=\"{@group}\"";
                            rowList.AddRange( _selectedTable.GetRowsForAllValuesFromLayer(_p.GroupField, group));
                        }
                        else
                        {
                            message += $"все значения для слоя { _p.ColorField}={ layer} в группе {_p.GroupField}=\"{group}\"";
                            rowList.AddRange(_selectedTable.GetRowsForAllValuesFromGroupAndLayer(_p.ColorField, layer, _p.GroupField, group));
                        }
                        break;

                    case "#Медианы_Outliers":
                    case "#Средние_Outliers":
                    case "Статистика_Outliers":
                    case "#Медианы_Extremes":
                    case "#Средние_Extremes":
                    case "Статистика_Extremes":
                    case "#Медианы_Indexed":
                    case "#Средние_Indexed":
                    case "Статистика_Indexed":
                        var value = _selectedTable.Table[sourceIndex].Row[f].ToString();
                        var groupname = _selectedTable.Table[sourceIndex].Row[g].ToString();
                        message += $"{value} в группе {_p.GroupField}=\"{groupname}\"";
                        rowList.AddRange(_selectedTable.GetRowsForValueFromFieldAndLayer(_selectedParameter, value, _p.GroupField,
                            groupname));
                        break;

                    case "#Медианы_Pie":
                    case "#Средние_Pie":
                    case "Статистика_Pie":
                        var group2 = _groupStatisticsTable.Table[ii.Index].Row[3].ToString();
                        var gd = (GroupDescription) _groupStatisticsTable.Table[ii.Index].Row[1];
                        var pie = gd.PieData[ii.IndexInGroup];
                        message += $"все значения для слоя {_p.ColorField}={pie.Z} в группе {_p.GroupField}=\"{group2}\"?";
                        rowList.AddRange( _selectedTable.GetRowsForAllValuesFromGroupAndLayer(_p.ColorField, pie.Z, _p.GroupField, group2));
                        break;
                }
                isFirst = false;
            }

            InternalTablesReadyForExport?.Invoke(this,
                            new InternalTablesReadyForExportEventArgs
                            {
                                InternalTables = new List<SLDataTable>() { _groupDescriptionTable }
                            });

            Interactivity?.Invoke(this,
                new InteractivityEventArgs
                {
                    Table = _selectedTable.TableName,
                    Field = _selectedParameter,
                    Message = message,
                    AvailableOperations = InteractiveOperations.All,
                    SelectedCases = rowList.Distinct().ToList(),
                    Clear = true
                });
        }
        
        private void ChartGroupCard_ToolTipRequired(object o, Chart.ToolTipRequiredEventArgs e)
        {
            if (_selectedTable == null) return;

            int sourceIndex;
            switch (e.Info.SeriesName)
            {
                case "#Средние":
                case "#Медианы":
                case "Статистика":
                    var gd = (GroupDescription) _groupStatisticsTable.Table[e.Info.Index].Row[1];
                    if (!gd.N.HasValue || !gd.WhiskerMax.HasValue || !gd.BoxMax.HasValue 
                        || !gd.Center.HasValue || !gd.BoxMin.HasValue || !gd.WhiskerMin.HasValue) return;

                    string boxDescription;
                    if (!_isMean)
                    {
                        boxDescription = _p.GroupField + ": " + _groupStatisticsTable.Table[e.Info.Index].Row[3] + "\r\n";
                        boxDescription += "n: " + Math.Round((double) gd.N.Value, 3) + "\r\n";
                        if (_p.ColorField!="Нет" && gd.Layer!=null) boxDescription += "Слой: " + gd.Layer + "\r\n";
                        if (gd.IQR.HasValue) boxDescription += "IQR: " + Math.Round(gd.IQR.Value, 3) + "\r\n";
                        boxDescription += "Max без выбросов: " + Math.Round(gd.WhiskerMax.Value, 3) +
                                          "\r\n";
                        boxDescription += "75%: " + Math.Round(gd.BoxMax.Value, 3) + "\r\n";
                        boxDescription += "Медиана: " + Math.Round(gd.Center.Value, 3) + "\r\n";
                        boxDescription += "25%: " + Math.Round(gd.BoxMin.Value, 3) + "\r\n";
                        boxDescription += "Min без выбросов: " + Math.Round(gd.WhiskerMin.Value, 3);
                        if (!String.IsNullOrEmpty(gd.CustomComments)) boxDescription += "\r\n" + gd.CustomComments;
                    }
                    else
                    {
                        boxDescription = _p.GroupField + ": " + _groupStatisticsTable.Table[e.Info.Index].Row[3] + "\r\n";
                        boxDescription += "n: " + Math.Round((double)gd.N.Value, 3) + "\r\n";
                        if (_p.ColorField != "Нет" && gd.Layer != null) boxDescription += "Слой: " + gd.Layer + "\r\n";
                        if (gd.StDev.HasValue) boxDescription += "СКО: " + Math.Round(gd.StDev.Value, 3) + "\r\n";
                        boxDescription += "+SD: " + Math.Round(gd.WhiskerMax.Value, 3) + "\r\n";
                        boxDescription += "+SE: " + Math.Round(gd.BoxMax.Value, 3) + "\r\n";
                        boxDescription += "Среднее: " + Math.Round(gd.Center.Value, 3) + "\r\n";
                        boxDescription += "-SE: " + Math.Round(gd.BoxMin.Value, 3)+ "\r\n";
                        boxDescription += "-SD: " + Math.Round(gd.WhiskerMin.Value, 3);
                        if (!String.IsNullOrEmpty(gd.CustomComments)) boxDescription += "\r\n" + gd.CustomComments;
                    }

                    var tb1 = new TextBlock
                        {
                            TextWrapping = TextWrapping.Wrap,
                            MaxWidth = 400,
                            Text = boxDescription,
                            Margin = new Thickness(2)
                        };
                    e.TT.Content = tb1;
                    break;

                default:
                    sourceIndex = (int) _interactiveTable.Table[e.Info.Index].Row[4];
                    CreateRowDescription(e, sourceIndex);
                    break;

                case "#Медианы_Outliers":
                case "#Средние_Outliers":
                case "Статистика_Outliers":
                case "#Медианы_Extremes":
                case "#Средние_Extremes":
                case "Статистика_Extremes":
                case "#Медианы_Indexed":
                case "#Средние_Indexed":
                case "Статистика_Indexed":
                    sourceIndex = e.Info.Index;
                    CreateRowDescription(e, sourceIndex);
                    break;

                case "#Медианы_Pie":
                case "#Средние_Pie":
                case "Статистика_Pie":
                    var gd2 = (GroupDescription)_groupStatisticsTable.Table[e.Info.Index].Row[1];
                    var pie = gd2.PieData[e.Info.IndexInGroup];
                    CreatePieDescription(e, pie);
                    break;
            }
        }

        private void CreatePieDescription(Chart.ToolTipRequiredEventArgs e, PieDescription pie)
        {
            var wp2 = new WrapPanel { Orientation = Orientation.Vertical, MaxHeight = 200 };
            e.TT.Content = wp2;
            var tb = new TextBlock
            {
                TextWrapping = TextWrapping.Wrap,
                MaxWidth = 400,
                Text = pie.Z + ": " + Math.Round(pie.Part * 100,2) +"%",
                Margin = new Thickness(0, 0, 20, 0)
            };
            wp2.Children.Add(tb);
        }

        private void CreateRowDescription(Chart.ToolTipRequiredEventArgs e, int sourceIndex)
        {
            var wp2 = new WrapPanel {Orientation = Orientation.Vertical, MaxHeight = 200};
            e.TT.Content = wp2;
            SLDataRow sldr2 = _selectedTable.Table[sourceIndex];
            for (int i = 0; i < Math.Min(50, _selectedTable.ColumnNames.Count); i++)
            {
                if (String.IsNullOrEmpty(sldr2.Row[i]?.ToString()) || _selectedTable.ColumnNames[i].StartsWith("#")) continue;
                var tb = new TextBlock
                {
                    TextWrapping = TextWrapping.Wrap,
                    MaxWidth = 400,
                    Text = _selectedTable.ColumnNames[i] + ": " + sldr2.Row[i],
                    Margin = new Thickness(0, 0, 20, 0)
                };
                wp2.Children.Add(tb);
            }
        }

        void chartDynamics_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            if (ReportInteractiveRename != null) ReportInteractiveRename(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }

    }
}