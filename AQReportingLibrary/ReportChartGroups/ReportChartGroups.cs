﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public partial class ReportChartGroups : IReportElement
    {
        private ReportChartGroupMode _boxMode;
        private ReportChartGroupCenter _center;
        private SLDataTable _chartRangesYTable;

        private ANOVA.ANOVAResult _groupANOVA;
        private SLDataTable _groupStatisticsTable, _groupDescriptionTable;
        private SLDataTable _interactiveTable;
        private bool _isSorted;
        private bool _outliers;
        private Data _data;
        private Groups _g;
        private Limits _limits;
        private string _probabilityComment = string.Empty;
        private string _selectedParameter;
        private SLDataTable _selectedTable;
        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private string _originalLayersType, _originalGroupType;

        private bool _isMean;
        private Step _step;
        private ReportChartGroupsParameters _p;
        private readonly Dictionary<ReportChartGroupMode, string> _chartNames = new Dictionary<ReportChartGroupMode, string>
        {
            {ReportChartGroupMode.None, "График по группам"},
            {ReportChartGroupMode.Center, "Центры групп"},
            {ReportChartGroupMode.Point, "График по группам"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Center, "График по группам"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Box | ReportChartGroupMode.Center, "Диаграмма размаха"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Box, "Диаграмма размаха"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Center, "Диаграмма - виола"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Violin, "Диаграмма - виола"},
            {ReportChartGroupMode.Box | ReportChartGroupMode.Violin | ReportChartGroupMode.Point, "Диаграмма - виола"},
            {ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center, "Диаграмма - виола"},
            {ReportChartGroupMode.Violin, "Диаграмма - виола"},
            {ReportChartGroupMode.Violin | ReportChartGroupMode.Center, "Диаграмма - виола"},
            {ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center , "Диаграмма - виола"},
            {ReportChartGroupMode.Violin | ReportChartGroupMode.Box , "Диаграмма - виола"},
            {ReportChartGroupMode.Box, "Диаграмма размаха"},
            {ReportChartGroupMode.Box | ReportChartGroupMode.Center, "Диаграмма размаха"},
        };

        public ReportChartGroups(ReportChartGroupsParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }
        public ReportChartGroups(Step s)
        {
            _p = StepConverter.GetParameters<ReportChartGroupsParameters>(s);
            _step = s;
        }

        public void SetNewParameters(ReportChartGroupsParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.ANOVA)
            {
                return _p.ColorField=="Нет" || _p.GroupField == _p.ColorField 
                    ? String.Format(@"<b>{0}</b>: ANOVA <b>{1}</b> по группам <b>{2}</b>", _p.Table, _p.Fields[0], _p.GroupField)
                    : String.Format(@"<b>{0}</b>: ANOVA <b>{1}</b> по группам <b>{2}</b> и слоям <b>{3}</b>", _p.Table, _p.Fields[0], _p.GroupField, _p.ColorField);
            }

            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.TableTests)
            {
                var c = _p.Center == ReportChartGroupCenter.Mean ? "Welch" : "Mann-Witney";
                return _p.ColorField == "Нет" || _p.GroupField == _p.ColorField
                    ? String.Format($"<b>{ _p.Table}</b>: {c}-тест <b>{_p.Fields[0]}</b> по группам <b>{_p.GroupField}</b>")
                    : String.Format($"<b>{ _p.Table}</b>: {c}-тест <b>{_p.Fields[0]}</b> по группам <b>{_p.GroupField}</b> и слоям <b>{_p.ColorField}</b>");
            }

            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.GraphTests)
            {
                var c = _p.Center == ReportChartGroupCenter.Mean ? "Welch" : "M-U";
                return _p.ColorField == "Нет" || _p.GroupField == _p.ColorField
                    ? String.Format($"<b>{ _p.Table}</b>: круговой граф {c}-тестов <b>{_p.Fields[0]}</b> по группам <b>{_p.GroupField}</b>")
                    : String.Format($"<b>{ _p.Table}</b>: круговой граф {c}-тестов <b>{_p.Fields[0]}</b> по группам <b>{_p.GroupField}</b> и слоям <b>{_p.ColorField}</b>");
            }
            switch (_p.Mode)
            {
                case ReportChartGroupMode.None:
                    if (_p.ColorField!="Нет") return String.Format(@"<b>{0}</b>: стратификация групп <b>{1}</b>", _p.Table, _p.Fields[0]);
                    return String.Format(@"<b>{0}</b>: график по группам <b>{1}</b>", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Center:
                    return String.Format(@"<b>{0}</b>: центры групп <b>{1}</b>", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Point | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Point:
                    return String.Format(@"<b>{0}</b>: график по группам <b>{1}</b>", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Box | ReportChartGroupMode.Point:
                case ReportChartGroupMode.Point | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Box:
                    return String.Format(@"<b>{0}</b>: диаграмма размаха <b>{1}</b>", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Violin:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Point:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Point | ReportChartGroupMode.Box:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Box:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                    return String.Format(@"<b>{0}</b>: диаграмма - виола <b>{1}</b>", _p.Table, _p.Fields[0]);
                default:
                    return String.Empty;
            }
        }
        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;

            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.ANOVA)
            {
                return _p.ColorField == "Нет" || _p.GroupField == _p.ColorField
                    ? String.Format(@"{0}: ANOVA {1} по группам {2}", _p.Table, _p.Fields[0], _p.GroupField)
                    : String.Format(@"{0}: ANOVA {1} по группам {2} и слоям {3}", _p.Table, _p.Fields[0], _p.GroupField, _p.ColorField);
            }

            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.TableTests)
            {
                var c = _p.Center == ReportChartGroupCenter.Mean ? "Welch" : "M-U";
                return _p.ColorField == "Нет" || _p.GroupField == _p.ColorField
                    ? String.Format($"{ _p.Table}: {c}-тест {_p.Fields[0]} по группам {_p.GroupField}")
                    : String.Format($"{ _p.Table}: {c}-тест {_p.Fields[0]} по группам {_p.GroupField} и слоям {_p.ColorField}");
            }

            if (_p.AnalisysMode == ReportChartGroupAnalisysMode.GraphTests)
            {
                var c = _p.Center == ReportChartGroupCenter.Mean ? "Welch" : "M-U";
                return _p.ColorField == "Нет" || _p.GroupField == _p.ColorField
                    ? String.Format($"{ _p.Table}: круговой граф {c}-тестов {_p.Fields[0]} по группам {_p.GroupField}")
                    : String.Format($"{ _p.Table}: круговой граф {c}-тестов {_p.Fields[0]} по группам {_p.GroupField} и слоям {_p.ColorField}");
            }

            switch (_p.Mode)
            {
                case ReportChartGroupMode.None:
                    if (_p.ColorField != "Нет") return String.Format(@"{0}: стратификация групп {1}", _p.Table, _p.Fields[0]);
                    return String.Format(@"{0}: график по группам {1}", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Center:
                    return String.Format(@"{0}: центры групп {1}", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Point | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Point:
                    return String.Format(@"{0}: график по группам {1}", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Box | ReportChartGroupMode.Point:
                case ReportChartGroupMode.Point | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Box:
                    return String.Format(@"{0}: диаграмма размаха {1}", _p.Table, _p.Fields[0]);
                case ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Point | ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Violin:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Point:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Point | ReportChartGroupMode.Box:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Center:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Box:
                case ReportChartGroupMode.Violin | ReportChartGroupMode.Box | ReportChartGroupMode.Center:
                    return String.Format(@"{0}: диаграмма - виола {1}", _p.Table, _p.Fields[0]);
                default:
                    return String.Empty;
            }
        }
        public string GetChartName()
        {
            return _chartNames[_p.Mode];
        }
    }
}