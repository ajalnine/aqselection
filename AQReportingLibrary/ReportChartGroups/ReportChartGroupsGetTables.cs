﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
        private List<SLDataTable> _anovaTables;

        private void ShowANOVATables()
        {
            _surface.Children.Clear();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush =  ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)), 
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix = string.Empty,
                FirstColumnIsCaseName = true,
                Title = GetChartTitle(),
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
            };
            rt.InteractiveRename += chartDynamics_InteractiveRename;
            rt.VisualReady += RtOnVisualReady;
            rt.DrawReportTables(_anovaTables, _surface);
        }

        private void RtOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            Finished.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = null});
        }

        public Brush ConvertStringToStroke(string c)
        {
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            Brush stroke = new SolidColorBrush(Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff)));
            return stroke;
        }
    }
}