﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportChartGroupsParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string GroupField { get; set; }
        public string ColorField { get; set; }
        public ReportChartGroupMode Mode { get; set; }
        public ReportChartGroupCenter Center { get; set; }
        public GroupPointMode PointMode { get; set; }
        public bool ShowOutliers { get; set; }
        public LabelsType ShowText { get; set; }
        public bool Sorted { get; set; }
        public bool OnlySignificant { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }

        public ReportChartGroupAnalisysMode AnalisysMode { get; set; }
        public bool MarkOutage { get; set; }
        public SLDataTable UserLimits { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }

        public ChartLegend ChartLegend { get; set; }
    }
}