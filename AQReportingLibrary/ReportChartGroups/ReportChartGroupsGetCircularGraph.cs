﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
        private List<SLDataTable> _graphTables;

        private void ShowTestsGraphs()
        {
            if (_selectedTable.Table.Count * _p.Fields.Count < 5e4 && _p.ColorField == "Нет")
            {
                var t = new Tests(_selectedTable, _p.Fields[0], _p.GroupField, _p.ColorField, _worker,
                    _p.Center == ReportChartGroupCenter.Mean ? TestType.Welch : TestType.MU);
                _graphTables = t.GetTestsCircularGraphData();
                RefreshTestsGraph();
                return;
            }

            SetMessage("Расчет матрицы сравнений групп...");
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _worker.DoWork += (o, e) =>
            {
                _data = null;
                _workerThread = Thread.CurrentThread;
                var t = new Tests(_selectedTable, _p.Fields[0], _p.GroupField, _p.ColorField, _worker,
                    _p.Center == ReportChartGroupCenter.Mean ? TestType.Welch : TestType.MU);
                _graphTables = t.GetTestsCircularGraphData();
            };
            _worker.RunWorkerCompleted += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(RefreshTestsGraph);
            };
            _worker.RunWorkerAsync();
        }

        private void RefreshTestsGraph()
        {
            if (_graphTables == null || _graphTables.Count==0) return;
            _surface.Children.Clear();
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs {InternalTables = _graphTables});
            var cd = new ChartDescription {ChartSeries = new List<ChartSeriesDescription>()};
            cd.ChartTitle = GetChartTitle();
            cd.ChartSubtitle = _p.ColorScale == ColorScales.HSV && _p.ColorField == "Нет"
                ? "<red>●</ red> p > 0.5    ● p ≤ 0.5"
                : null;
            if ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) cd.ChartSubtitle += "\r\n" + _selectedTable.GetFilterDescription();
            cd.ChartLegendType = ChartLegend.Right;
            var csd = new ChartSeriesDescription
            {
                SourceTableName = _graphTables[0].TableName,
                XColumnName = "Группа1",
                XIsNominal = false,
                YIsNominal = false,
                XAxisTitle = "Группа1",
                SeriesTitle = "Граф тестов",
                SeriesType = ChartSeriesType.CircularGraph,
                YAxisTitle = "Группа2",
                YColumnName = "Группа2",
                Options = _p.ColorScale == ColorScales.HSV ? "ToRed" : null,
                VColumnName = "#pt",
                ZColumnName = _p.ColorField == "Нет" ? null : _p.ColorField,
                ColorScale = _p.ColorScale,
                ColorScaleMode = _p.ColorScaleMode,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(_p.ColorScale),
                MarkerSize = 5
            };
            cd.ChartSeries.Add(csd);

            var circularGraph = new Chart
            {
                EnableSecondaryAxises = false,
                Height = _p.LayoutSize.Height,
                Width = _p.LayoutSize.Width,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = false,
                ShowHorizontalGridlines = false,
                ShowHorizontalAxises = false,
                ShowVerticalAxises = false,
                ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                GlobalFontCoefficient = _p.FontSize,
                EnableCellCondense = true,
                ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                ShowTable = false,
                Margin = new Thickness(0, 0, 0, 20),
                ChartData = cd,
                SourceName = _p.Table,
                DataTables = new List<SLDataTable>
                {
                    _graphTables[0]
                },
                FixedAxises = new FixedAxises {FixedAxisCollection = new ObservableCollection<FixedAxis>(), Editable = true}
            };
            circularGraph.InteractiveRename += chartDynamics_InteractiveRename;
            ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs {SettingChart = circularGraph});
            _surface.Children.Add(circularGraph);
            circularGraph.VisualReady += CircularGraph_VisualReady;
            circularGraph.MakeChart();
        }

        private void CircularGraph_VisualReady(object o, VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs());
        }
    }
}