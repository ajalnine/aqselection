﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
        private List<SLDataTable> _mapTables;
        private BackgroundWorker _worker;
        private Thread _workerThread;

        private void ShowTestsTables()
        {
            if (_selectedTable.Table.Count*_p.Fields.Count < 5e4 && _p.ColorField == "Нет")
            {
                var t = new Tests(_selectedTable, _p.Fields[0], _p.GroupField, _p.ColorField, _worker, 
                    _p.Center == ReportChartGroupCenter.Mean ? TestType.Welch : TestType.MU);
                _mapTables = t.GetTestsTables();
                RefreshTestsTables();
                return;
            }

            SetMessage("Расчет матрицы сравнений групп...");
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _worker.DoWork += (o, e) =>
            {
                _data = null;
                _workerThread = Thread.CurrentThread;
                var t = new Tests(_selectedTable, _p.Fields[0], _p.GroupField, _p.ColorField, _worker,
                    _p.Center == ReportChartGroupCenter.Mean ? TestType.Welch : TestType.MU);
                _mapTables = t.GetTestsTables();
            };
            _worker.RunWorkerCompleted += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(RefreshTestsTables);
            };
            _worker.RunWorkerAsync();
        }

        private void RefreshTestsTables()
        {
            if (_mapTables == null || _mapTables.Count == 0) return;
            _surface.Children.Clear();
            _surface.UpdateLayout();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush = ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)),
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix =
                    _p.ColorField != "Нет" ? _p.ColorField + ": " : string.Empty,
                FirstColumnIsCaseName = true,
                ColorIsBackground = true,
                IsInteractive = true,
                Title = GetChartTitle(),
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
            };
            rt.InteractiveRename += chartDynamics_InteractiveRename;
            InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs {InternalTables = _mapTables});
            rt.VisualReady += Rt_VisualReady;
            rt.DrawReportTables(_mapTables, _surface);
        }

        private void Rt_VisualReady(object o, VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs {InnerTables = _mapTables});
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 20,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }
    }
}