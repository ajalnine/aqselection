﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows.Controls;

namespace AQReportingLibrary
{
    public partial class ReportChartGroups
    {
        Panel _surface;
        List<Chart> _charts;
        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            SetupChartData(dataTables);
            if (_interactiveTable == null || _interactiveTable.Table.Count == 0) return false;
            SetupLimits(dataTables);
            SetupGroups();
            switch (_p.AnalisysMode)
            {
                case ReportChartGroupAnalisysMode.ANOVA:
                    ShowANOVATables();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _anovaTables });
                    break;

                case ReportChartGroupAnalisysMode.TableTests:
                    ShowTestsTables();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _mapTables });
                    break;

                case ReportChartGroupAnalisysMode.GraphTests:
                    ShowTestsGraphs();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables });
                    break;

                default:
                    CreateCharts();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _groupDescriptionTable } });
                    break;
            }
            return true;
        }

        public void Cancel() { if (_charts!= null) foreach (var c in _charts) c.StopWorking(); }

        public void RefreshVisuals()
        {
            switch (_p.AnalisysMode)
            {
                case ReportChartGroupAnalisysMode.ANOVA:
                    ShowANOVATables();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _anovaTables });
                    break;

                case ReportChartGroupAnalisysMode.TableTests:
                    ShowTestsTables();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _mapTables });
                    break;

                case ReportChartGroupAnalisysMode.GraphTests:
                    ShowTestsGraphs();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = _graphTables });
                    break;

                default:
                    CreateCharts();
                    InternalTablesReadyForExport?.Invoke(this, new InternalTablesReadyForExportEventArgs { InternalTables = new List<SLDataTable>() { _groupDescriptionTable } });
                    break;
            }
        }

        private void SetupGroups()
        {
            _g = new Groups(_data.GetLDR(), _selectedTable, _p, _limits);
            var a = new ANOVA(_data.GetLDR());
            _groupANOVA = ((_p.AnalisysMode != ReportChartGroupAnalisysMode.ANOVA || _p.ColorField=="Нет" || _p.ColorField == _p.GroupField) 
                ? (_p.Center == ReportChartGroupCenter.Mean ? a.GetANOVAResultForMeansByGroup(_p.GroupField) : a.GetANOVAResultForKWByGroup(_p.GroupField))
                :(_p. Center == ReportChartGroupCenter.Mean ? a.GetANOVAResultForTwoWay(_p.GroupField, _p.ColorField) : a.GetANOVAResultForKWByGroupAndLayer(_p.GroupField, _p.ColorField)));
            _anovaTables = _groupANOVA?.ANOVATables;
            _isSorted = _g.ChartGroupIsSorted;
            _groupStatisticsTable = _g.GetGroupTable();
            _groupDescriptionTable = _g.GetGroupDescriptionTable();
        }

        private void SetupLimits(IEnumerable<SLDataTable> dataTables)
        {
            _limits = new Limits(_data.GetLDR(),
                _p.RangeType,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.Dynamic,
                _p.UserLimits,
                Limits.GetExternalLimitsTable(dataTables, _selectedTable),
                _selectedParameter, false, _p.RangeStyle);

            _chartRangesYTable = _limits.GetRangesYTable();

            _probabilityComment = _limits.GetProbabilityComment(false);

            UiReflectChanges?.Invoke(this, new UIReflectChangesEventArgs {UserLimits = _limits.UserLimitsTable});
        }

        private void SetupChartData(IEnumerable<SLDataTable> dataTables)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _selectedParameter = _p.Fields.FirstOrDefault();
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);
            _originalGroupType = _selectedTable.GetDataType(_p.GroupField);

            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    X = _p.GroupField,
                    ValuesForAggregation = _selectedParameter,
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }
            
            _boxMode = _p.Mode;
            _outliers = _p.ShowOutliers;
            _center = _p.Center;
            _isMean = _center == ReportChartGroupCenter.Mean;
            var fi = new FieldsInfo
            {
                ColumnY = _selectedParameter,
                ColumnGroup = _p.GroupField,
                ColumnZ = _p.ColorField
            };
            _data = new Data(_selectedTable, fi);
            _interactiveTable = _data.GetDataTable();
        }

        private void CreateCharts()
        {
            _surface.Children.Clear();
            _charts = new List<Chart>();
            foreach (var cd in GetDescriptionForBoxWhiskers(_data, _p))
            {
                var chartDynamics = new Chart
                {
                    EnableSecondaryAxises = false,
                    Height = _p.LayoutSize.Height,
                    Width = _p.LayoutSize.Width,
                    HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                    ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                    ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                    IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                    ShowTitle = (_p.LayoutMode & ReportChartLayoutMode.ShowTitle) == ReportChartLayoutMode.ShowTitle,
                    ShowSubtitle = (_p.LayoutMode & ReportChartLayoutMode.ShowSubtitle) == ReportChartLayoutMode.ShowSubtitle,
                    UseDefaultColor = (_p.LayoutMode & ReportChartLayoutMode.UseColorScale) == ReportChartLayoutMode.UseColorScale,
                    Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                    ReverseOrderedLegendDiscreteItems = (_p.LayoutMode & ReportChartLayoutMode.OrderLegend) == ReportChartLayoutMode.OrderLegend,
                    ShowTickMarks = (_p.LayoutMode & ReportChartLayoutMode.ShowTickMarks) == ReportChartLayoutMode.ShowTickMarks,
                    ContrastGridLines = (_p.LayoutMode & ReportChartLayoutMode.ContrastGrid) == ReportChartLayoutMode.ContrastGrid,
                    DoubleVerticalAxis = (_p.LayoutMode & ReportChartLayoutMode.DoubleYAxis) == ReportChartLayoutMode.DoubleYAxis,
                    ShowXHelpers = true,
                    ShowHelperGridlines = false,
                    ShowHelpersMirrored = false,
                    ShowYHelpers = false,
                    ShowHelpersBeyondAxis = false,
                    ConstantsOnHelpers = false,
                    HelpersCoefficient = _g.NumberOfGroups < 40 ? 0.035 : 0,
                    EnableCellCondense = true,
                    GlobalFontCoefficient = _p.FontSize,
                    ShowLegend = (_p.LayoutMode & ReportChartLayoutMode.ShowLegend) == ReportChartLayoutMode.ShowLegend,
                    ShowTable = false,
                    Margin = new Thickness(0, 0, 0, 20),
                    ChartData = cd,
                    SourceName = _p.Table,
                    MathFunctionDescriptions = new List<MathFunctionDescription>(),
                    DataTables = new List<SLDataTable>
                    {
                        _interactiveTable,
                        _chartRangesYTable,
                        _groupStatisticsTable
                    },
                    FixedAxises = new FixedAxises
                    {
                        FixedAxisCollection = new ObservableCollection<FixedAxis> {new FixedAxis {Axis = "Y"}, new FixedAxis { DataType = _originalGroupType, Axis = "X", AxisDiscrete = true, Format = "dd.MM.yyyy", StringIsDateTime = _originalGroupType?.ToLower().Contains("datetime") ?? false }, new FixedAxis { DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", StringIsDateTime = _originalGroupType?.ToLower().Contains("datetime") ?? false } },
                        Editable = false
                    },
                };
                if (_p.AnalisysMode == ReportChartGroupAnalisysMode.Extended)
                    chartDynamics.NumberOfMinorVerticalGridLines = _g.NumberOfGroups < 20? _g.NumberOfLayers : 1;
                    
                chartDynamics.ToolTipRequired += ChartGroupCard_ToolTipRequired;
                chartDynamics.SelectionChanged += ChartDynamics_SelectionChanged;
                chartDynamics.AxisClicked += ChartDynamics_AxisClicked;
                chartDynamics.InteractiveRename += chartDynamics_InteractiveRename;
                chartDynamics.VisualReady += ChartDynamics_VisualReady;
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs { SettingChart = chartDynamics });
                _surface.Children.Add(chartDynamics);
                _charts.Add(chartDynamics);
                chartDynamics.MakeChart();
            }
        }

        private void ChartDynamics_VisualReady(object o, AQBasicControlsLibrary.VisualReadyEventArgs e)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs());
        }
    }
}