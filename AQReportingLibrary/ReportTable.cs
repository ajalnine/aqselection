﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace AQReportingLibrary
{
    public class ReportTables : IReportVisual
    {
        public double GlobalFontCoefficient;
        public double Width;
        public Brush ForegroundBrush;
        public bool Inverted;
        public bool IsTransparent;
        public bool FirstColumnIsCaseName;
        public string HeaderPrefix;
        public bool ColorIsBackground;
        public string Title;
        public string SubTitle;
        public bool IsInteractive;
        public bool FixedHeader;
        private string _textBeforeEditing;
        private List<InlineDescription> _richTextBeforeEditing;
        public event InteractiveRenameDelegate InteractiveRename;
        public event ActiveCellDelegate ActiveCellClicked;
        public event ActiveCellDelegate ActiveCellToolTipRequired;
        public event CellEditDelegate CellEdited;
        public event VisualReadyDelegate VisualReady;

        private SLDataTable _selectedTable;
        RichTextBox _titleRichTextBox, _subTitleRichTextBox;
        public ReportTables(SLDataTable selectedTable)
        {
            _selectedTable = selectedTable;
        }

        public void DrawReportTables(List<SLDataTable> customTables, Panel baseSurface)
        {
            baseSurface.Children.Clear();
            var surface = new StackPanel { Orientation = Orientation.Vertical};

            if (customTables == null) return;
            var bg = IsTransparent 
                ? new SolidColorBrush(Colors.Transparent) 
                : Inverted 
                    ? new SolidColorBrush(Colors.Black)
                    : new SolidColorBrush(Colors.White);
            surface.Background = bg;

            if (!string.IsNullOrEmpty(Title))
            {
                _titleRichTextBox = new RichTextBox
                {
                    Foreground = Inverted ? new SolidColorBrush(Colors.White) : ForegroundBrush,
                    FontSize = 18 * GlobalFontCoefficient,
                    TextWrapping = TextWrapping.Wrap,
                    BorderThickness = new Thickness(0),
                    Margin = new Thickness(0),
                    Background = new SolidColorBrush(Colors.Transparent),
                    MaxWidth = Width
                };
                PseudoHTMLAxisTitleParser.FillRichTextWithHTML(Title, _titleRichTextBox);
                surface.Children.Add(_titleRichTextBox);
                _titleRichTextBox.GotFocus += TitleRichTextBox_GotFocus;
                _titleRichTextBox.LostFocus += TitleRichTextBox_LostFocus;
            }

            if (!string.IsNullOrEmpty(SubTitle))
            {
                _subTitleRichTextBox = new RichTextBox
                {
                    Foreground = Inverted ? new SolidColorBrush(Colors.White) : ForegroundBrush,
                    FontSize = 14 * GlobalFontCoefficient,
                    TextWrapping = TextWrapping.Wrap,
                    BorderThickness = new Thickness(0),
                    Margin = new Thickness(0),
                    Background = new SolidColorBrush(Colors.Transparent),
                    MaxWidth = Width
                };
                PseudoHTMLAxisTitleParser.FillRichTextWithHTML(SubTitle, _subTitleRichTextBox);
                surface.Children.Add(_subTitleRichTextBox);
            }

            foreach (var customTable in customTables)
            {
                if (customTable == null) continue;
                surface.Children.Add(new TextBlock
                {
                    Foreground = ForegroundBrush,
                    Text = HeaderPrefix + customTable.TableName,
                    VerticalAlignment = VerticalAlignment.Center,
                    FontSize = 16 * GlobalFontCoefficient
                });
                if (!string.IsNullOrEmpty(customTable.Header))
                {
                    var headerRichTextBox = new RichTextBox
                    {
                        Foreground = Inverted ? new SolidColorBrush(Colors.White) : ForegroundBrush,
                        FontSize = 14*GlobalFontCoefficient,
                        TextWrapping = TextWrapping.Wrap,
                        BorderThickness = new Thickness(0),
                        Margin = new Thickness(0),
                        Background = new SolidColorBrush(Colors.Transparent),
                        MaxWidth = Width
                    };

                    PseudoHTMLAxisTitleParser.FillRichTextWithHTML(customTable.Header, headerRichTextBox);
                    surface.Children.Add(headerRichTextBox);
                }
                var LegendChartTable = new Grid { Margin = new Thickness(10, 10, 10, 10) };

                var columns = customTable.ColumnNames.Count(a => !a.StartsWith("#"));
                var rows = customTable.Table.Count;
                LegendChartTable.MaxWidth = Width;
                for (var x = 0; x < columns; x++) LegendChartTable.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = (x==0 && FirstColumnIsCaseName)
                    ? new GridLength(1, GridUnitType.Auto)
                    : new GridLength(1, GridUnitType.Star)
                });
                for (var y = 0; y < rows + 1; y++) LegendChartTable.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

                int i = 0, j = 0;
                foreach (var c in customTable.ColumnNames.Where(a => !a.StartsWith("#")))
                {
                    var b2 = new Border
                    {
                        BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                        BorderBrush = ForegroundBrush
                    };
                    if (FixedHeader)
                    {
                        var tb2 = new TextBlock
                        {
                            Text = (c ?? string.Empty),
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Margin = new Thickness(2, 1, 1, 2),
                            FontSize = 14 * GlobalFontCoefficient,
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Inverted ? new SolidColorBrush(Colors.White) : ForegroundBrush,
                            TextWrapping = TextWrapping.Wrap
                        };
                        b2.Child = tb2;
                    }
                    else
                    {
                        var tb2 = new TextBox
                        {
                            Text = (c ?? string.Empty),
                            HorizontalAlignment = HorizontalAlignment.Center,
                            Margin = new Thickness(2, 1, 1, 2),
                            FontSize = 14*GlobalFontCoefficient,
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Inverted ? new SolidColorBrush(Colors.White) : ForegroundBrush,
                            TextWrapping = TextWrapping.Wrap,
                            BorderThickness = new Thickness(0),
                            Background = new SolidColorBrush(Colors.Transparent),
                        };
                        tb2.GotFocus += Header_GotFocus;
                        tb2.LostFocus += Header_LostFocus;
                        b2.Child = tb2;
                    }
                    Grid.SetColumn(b2, i);
                    Grid.SetRow(b2, j);
                    LegendChartTable.Children.Add(b2);
                    i++;
                }
                j++;
                var tabindex = 1000;
                foreach (var row in customTable.Table)
                {
                    i = 0;
                    foreach (var c in customTable.ColumnNames.Where(a => !a.StartsWith("#")))
                    {
                        var x = customTable.ColumnNames.IndexOf(c);
                        var columnIndex = customTable.ColumnNames.IndexOf(c);
                        var colorColumn = customTable.ColumnNames.IndexOf("#" + c + "_Цвет");
                        var color = (colorColumn >= 0)
                            ? new SolidColorBrush(ColorConvertor.ConvertStringToColor((row.Row[colorColumn]?.ToString() ?? "#ff000000")))
                            : Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                        var b2 = new Border
                        {
                            BorderThickness = new Thickness(0.25, 0.25, 0.25, 0.25),
                            BorderBrush = ForegroundBrush
                        };

                        if (i > 0)
                        {
                            if (customTable.ColumnTags!=null && customTable.ColumnTags.Count > 0 && customTable.ColumnTags[x]?.ToString() == "Editable")
                            {
                                var tb2 = new TextBox
                                {
                                    Text = (row.Row[x]?.ToString() ?? string.Empty),
                                    HorizontalAlignment = HorizontalAlignment.Center,
                                    MinWidth = 45,
                                    Margin = new Thickness(2, 1, 1, 2),
                                    FontSize = 14 * GlobalFontCoefficient,
                                    VerticalAlignment = VerticalAlignment.Center,
                                    Foreground = FirstColumnIsCaseName && i == 0 ? ForegroundBrush : color,
                                    TextWrapping = TextWrapping.Wrap,
                                    BorderThickness = new Thickness(0),
                                    TabIndex =  tabindex,
                                    IsTabStop = true,
                                    Background = new SolidColorBrush(Colors.Transparent),
                                };
                                tb2.Tag = new CellEditEventArgs{ column = columnIndex, row = j, source = customTable};
                                b2.MinWidth = 60;
                                if (ColorIsBackground && colorColumn >= 0)
                                {
                                    b2.Background = color;
                                    tb2.Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                                }
                                b2.Child = tb2;
                                tb2.LostFocus += Cell_LostFocus;
                                tb2.KeyDown += Tb2_KeyDown;
                                tabindex++;
                            }

                            else
                            {
                                var tb2 = new TextBlock
                                {
                                    Text = (row.Row[x]?.ToString() ?? string.Empty),
                                    HorizontalAlignment = HorizontalAlignment.Center,
                                    Margin = new Thickness(2, 1, 1, 2),
                                    FontSize = 14 * GlobalFontCoefficient,
                                    VerticalAlignment = VerticalAlignment.Center,
                                    Foreground = FirstColumnIsCaseName && i == 0 ? ForegroundBrush : color,
                                    TextWrapping = TextWrapping.Wrap
                                };

                                if (ColorIsBackground && colorColumn >= 0)
                                {
                                    b2.Background = color;
                                    tb2.Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                                }
                                if (!IsInteractive) b2.Child = tb2;
                                else
                                {
                                    var ea = new ActiveCellEventArgs { column = columnIndex, row = j, source = customTable, tag = customTable.Tag };
                                    b2.MouseLeftButtonDown += TableCell_Click;
                                    var tt = new ToolTip();
                                    tt.Opened += ToolTip_Opening;
                                    tt.Tag = ea;
                                    b2.Tag = ea;
                                    ToolTipService.SetToolTip(b2, tt);
                                    b2.Child = tb2;
                                }
                            }
                        }
                        else
                        {
                            var tb2 = new TextBox
                            {
                                Text = (row.Row[x]?.ToString() ?? string.Empty),
                                HorizontalAlignment = i == 0 && FirstColumnIsCaseName ? HorizontalAlignment.Left : HorizontalAlignment.Center,
                                Margin = new Thickness(2, 1, 1, 2),
                                FontSize = 14 * GlobalFontCoefficient,
                                VerticalAlignment = VerticalAlignment.Center,
                                Foreground = FirstColumnIsCaseName && i == 0 ? ForegroundBrush : color,
                                TextWrapping = TextWrapping.Wrap,
                                BorderThickness = new Thickness(0),
                                Background = new SolidColorBrush(Colors.Transparent),
                            };
                            if (ColorIsBackground && colorColumn >= 0)
                            {
                                b2.Background = color;
                                tb2.Foreground = Inverted ? new SolidColorBrush(Colors.White) : new SolidColorBrush(Colors.Black);
                            }
                            b2.Child = tb2;
                            tb2.GotFocus += Header_GotFocus;
                            tb2.LostFocus += Header_LostFocus;
                            tb2.KeyDown += Tb2_KeyDown;
                        }
                        
                        LegendChartTable.Children.Add(b2);
                        Grid.SetColumn(b2, i);
                        Grid.SetRow(b2, j);
                        i++;
                    }
                    j++;
                }
                surface.Children.Add(LegendChartTable);
            }
            baseSurface.Children.Add(surface);
            baseSurface.UpdateLayout();
            VisualReady?.Invoke(this, new VisualReadyEventArgs());
        }

        private void Tb2_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var source = (Border) ((TextBox) sender).Parent;
            var x = Grid.GetColumn(source);
            var y = Grid.GetRow(source);
            var surface = (Grid) source.Parent;

            switch (e.Key)
            {
                case Key.Down:
                    (((Border) surface.Children.SingleOrDefault(a =>Grid.GetRow((Border)a) == y + 1 && Grid.GetColumn((Border)a) == x))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                case Key.Up:
                    (((Border)surface.Children.SingleOrDefault(a => Grid.GetRow((Border)a) == y - 1 && Grid.GetColumn((Border)a) == x))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                case Key.Left:
                    (((Border)surface.Children.LastOrDefault(a => Grid.GetRow((Border)a) == y && Grid.GetColumn((Border)a) < x && ((Border)a).Child is TextBox))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                case Key.Right:
                    (((Border)surface.Children.FirstOrDefault(a => Grid.GetRow((Border)a) == y && Grid.GetColumn((Border)a) > x && ((Border)a).Child is TextBox))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                case Key.Home:
                    (((Border)surface.Children.FirstOrDefault(a => ((Border)a).Child is TextBox))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                case Key.End:
                    (((Border)surface.Children.LastOrDefault(a => ((Border)a).Child is TextBox))?.Child as TextBox)?.Focus();
                    e.Handled = true;
                    break;
                default:
                    e.Handled = false;
                    break;
            }
        }

        private void ToolTip_Opening(object sender, RoutedEventArgs e)
        {
            var tt = sender as ToolTip;
            var ea = tt?.Tag as ActiveCellEventArgs;

            if (tt != null) ActiveCellToolTipRequired?.Invoke(tt, ea);
        }

        private void TableCell_Click(object sender, MouseButtonEventArgs e)
        {
            var ea = (sender as Border)?.Tag as ActiveCellEventArgs;
            e.Handled = true;
            ActiveCellClicked?.Invoke(this, ea);
        }

        private void TitleRichTextBox_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            _textBeforeEditing = s != null ? Chart.GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;
            _richTextBeforeEditing = s != null ? Chart.GetInlinesFromRichTextBox(s.Blocks) : null;
        }
        
        private void TitleRichTextBox_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as RichTextBox;
            var textAfterEditing = s != null ? Chart.GetPlainTextFromRichTextBox(s.Blocks) : string.Empty;
            if (textAfterEditing == _textBeforeEditing) return;

            var match = Regex.Match(textAfterEditing, Chart.GetRegexPatternForTitleProcessing(_richTextBeforeEditing));
            var renameOperations = new List<ChartRenameOperation>();
            var firstOrDefault = _richTextBeforeEditing.FirstOrDefault(a => a.IsBold);
            var possibleTable = string.Empty;
            if (firstOrDefault != null) possibleTable = firstOrDefault.Text;

            var possibleFields = _richTextBeforeEditing.Where(a => a.IsBold).Skip(1).Select(a => new { Field = a.Text, Table = possibleTable });

            var fieldNames = _selectedTable.ColumnNames;

            var candidates = _richTextBeforeEditing.Where(a => a.IsBold).Select(a => a.Text).ToList();
            for (int index = 0; index < candidates.Count; index++)
            {
                var r = candidates[index];
                var groupname = "A" + (index + 1);
                if (match.Groups[groupname].Captures.Count == 0) continue;
                var newnameGroup = match.Groups[groupname].Captures[match.Groups[groupname].Captures.Count - 1];
                if (!match.Groups[groupname].Success || newnameGroup.Value == r) continue;
                var oldname = r;
                if (_selectedTable.TableName == oldname)
                {
                    renameOperations.Add(new ChartRenameOperation
                    {
                        ChartItemType = ChartRenameItemType.Table,
                        OldName = oldname,
                        NewName = newnameGroup.Value,
                        TableName = oldname
                    });
                }
                else
                {
                    var foundFields = fieldNames.Where(a => a == r).ToList();
                    renameOperations.AddRange(foundFields.Select(foundField => new ChartRenameOperation
                    {
                        ChartItemType = ChartRenameItemType.Field,
                        OldName = oldname,
                        NewName = newnameGroup.Value,
                        TableName = _selectedTable.TableName
                    }));
                }
            }
            renameOperations.Reverse();
            var duplicateCleaned = new List<ChartRenameOperation>();
            foreach (var ro in renameOperations
                .Where(ro => !duplicateCleaned
                    .Any(a => a.OldName == ro.OldName && a.ChartItemType == ro.ChartItemType && a.TableName == ro.TableName)))
            {
                duplicateCleaned.Add(ro);
            }

            if (duplicateCleaned.Count > 0) InvokeRenameEvent(new ChartRenameEventArgs { RenameOperations = duplicateCleaned });
        }

        private void Header_GotFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            _textBeforeEditing = s != null ? s.Text : string.Empty;
        }

        private void Header_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            var textAfterEditing = s != null ? s.Text : string.Empty;
            if (textAfterEditing == _textBeforeEditing) return;
            var description = s?.Tag as ChartSeriesDescription;
            var cs = description != null ? ((ChartSeriesDescription)s.Tag) : null;
            var e = new ChartRenameEventArgs
            {
                RenameOperations = new List<ChartRenameOperation>
                        {
                            new ChartRenameOperation
                            {
                                OldName =  _textBeforeEditing.Trim(),
                                NewName = textAfterEditing.Trim(),
                                ChartItemType = ChartRenameItemType.Field,
                                TableName = _selectedTable.TableName
                            }
                        }
            };

            InvokeRenameEvent(e);
        }

        private void Cell_LostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            var s = sender as TextBox;
            var ceea = s?.Tag as CellEditEventArgs;
            ceea.newvalue = s.Text;
            CellEdited?.Invoke(s, ceea);
        }

        private void InvokeRenameEvent(ChartRenameEventArgs e)
        {
            if (e.RenameOperations[0].TableName != null && InteractiveRename != null)
            {
                InteractiveRename(this, e);
            }
        }
    }

    public delegate void ActiveCellDelegate(object o, ActiveCellEventArgs e);
    public class ActiveCellEventArgs
    {
        public int column;
        public int row;
        public SLDataTable source;
        public object tag;
    }

    public delegate void CellEditDelegate(object o, CellEditEventArgs e);
    public class CellEditEventArgs
    {
        public int column;
        public int row;
        public SLDataTable source;
        public string newvalue;
    }
}
