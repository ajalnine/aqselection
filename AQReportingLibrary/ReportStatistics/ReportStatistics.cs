﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQStepFactoryLibrary;

namespace AQReportingLibrary
{
    public partial class ReportStatistics : IReportElement
    {
        private SLDataTable _selectedTable, _originalTable;

        public event ReportElementChangedDelegate ReportChartChanged;
        public event LimitsKnownDelegate UiReflectChanges;
        public event AxisClickedDelegate AxisClicked;
        public event ReportInteractiveRenameDelegate ReportInteractiveRename;
        public event FieldMarkChangingDelegate FieldMarkChanging;
        public event ChartSetsFixedAxisDelegate ChartSetsFixedAxis;
        public event InternalTablesReadyForExportDelegate InternalTablesReadyForExport;
        public event InteractiveParametersChangeDelegate InteractiveParametersChange;
        public event InteractivityDelegate Interactivity;
        public event ReportChartFihishedDelegate Finished;
        public event RangeAppendedDelegate RangeAppended;
        private Step _step;
        private ReportStatisticsParameters _p;
       

        public ReportStatistics(ReportStatisticsParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public ReportStatistics(Step s)
        {
            _p = StepConverter.GetParameters<ReportStatisticsParameters>(s);
            _step = s;
        }

        public void SetNewParameters(ReportStatisticsParameters p)
        {
            _p = p;
            _step = StepConverter.GetStep(p, GetChartDescription());
        }

        public Step GetStep()
        {
            return _step;
        }

        public string GetChartTitle()
        {
            if (_p.Fields.Count == 0) return string.Empty;
                return _p.ColorField=="Нет" 
                    ? String.Format(@"<b>{0}</b>: статистика", _p.Table)
                    : String.Format(@"<b>{0}</b>: статистика по слоям <b>{1}</b>", _p.Table,  _p.ColorField);
        }

        public string GetChartDescription()
        {
            if (_p.Fields.Count == 0) return string.Empty;
            return _p.ColorField == "Нет"
                ? String.Format(@"{0}: статистика", _p.Table)
                : String.Format(@"{0}: статистика по слоям {1}", _p.Table,  _p.ColorField);
        }

        public string GetChartName()
        {
            return "Статистика";
        }
    }
}