﻿using System.Windows;
using System.Windows.Controls;
using AQBasicControlsLibrary;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportStatistics
    {
        void chartDynamics_InteractiveRename(object source, ChartRenameEventArgs e)
        {
            ReportInteractiveRename?.Invoke(this, RenameProcessor.DefaultRenameProcess(e, _selectedTable));
        }

        private void Rt_CellEdited(object o, CellEditEventArgs e)
        {
            var tb = ((TextBox)o);

            double? result = null;
            
            if (!string.IsNullOrWhiteSpace(e.newvalue) )
            {
                double r;
                if (!double.TryParse(e.newvalue, out r))
                {
                    tb.Background = new SolidColorBrush(Colors.Red);
                    return;
                }
                result = r;
            }
            e.source.Table[e.row - 1].Row[e.column] = result;
            tb.Background = null;
            
            var layer = _p.LayerMode == ReportChartLayerMode.Multiple
                ? e.source.Tag
                : _statisticsTables[0].Table[e.row - 1].Row[1]?.ToString();
            var field = _statisticsTables[0].ColumnNames[e.column];
            var parameter = _statisticsTables[0].Table[e.row - 1].Row[0]?.ToString();
            int index = -1;
            bool needAppend = false;
            switch (field)
            {
                case "Нижняя граница":
                    RangeAppended?.Invoke(this, new RangeAppendedEventArgs {NewRange = new Range {Group = layer, Min = result, Max = double.NaN, Parameter = parameter}, SelectedTable = _originalTable.TableName, LayerField = _p.ColorField});
                    index = _originalTable.ColumnNames.IndexOf("#" + parameter + "_НГ");
                    if (index == -1)index = _originalTable.ColumnNames.IndexOf(parameter + "_НГ");
                    if (index == -1)
                    {
                        _originalTable.ColumnNames.Add("#" + parameter + "_НГ");
                        _originalTable.ColumnTags?.Add(null);
                        _originalTable.DataTypes.Add("Double");
                        index = _originalTable.ColumnNames.Count - 1;
                        needAppend = true;
                    }
                    break;

                case "Верхняя граница":
                    RangeAppended?.Invoke(this, new RangeAppendedEventArgs { NewRange = new Range { Group = layer, Max = result, Min = double.NaN, Parameter = parameter }, SelectedTable = _originalTable.TableName, LayerField = _p.ColorField });
                    index = _originalTable.ColumnNames.IndexOf("#" + parameter + "_ВГ");
                    if (index == -1) index = _originalTable.ColumnNames.IndexOf(parameter + "_ВГ");
                    if (index == -1)
                    {
                        _originalTable.ColumnNames.Add("#" + parameter + "_ВГ");
                        _originalTable.ColumnTags?.Add(null);
                        _originalTable.DataTypes.Add("Double");
                        index = _originalTable.ColumnNames.Count - 1;
                        needAppend = true;
                    }
                    break;
            }
            if (_p.ColorField == "Нет")
            {
                foreach (var row in _originalTable.Table)
                {
                    if (needAppend) row.Row.Add(result);
                    else row.Row[index] = result;
                }
            }
            else
            {
                var layerIndex = _originalTable.ColumnNames.IndexOf(_p.ColorField);
                var toinsert = result.HasValue ? (object) result.Value : null;
                foreach (var row in _originalTable.Table)
                {
                    if (row.Row[layerIndex]?.ToString() != layer)
                    {
                        if (needAppend) row.Row.Add(null);
                        continue;
                    }
                    if (needAppend) row.Row.Add(toinsert);
                    else row.Row[index] = toinsert;
                }
            }
            Data.LDRCache.Clear();
            if ((_p.Statistics & Statistics.Probability) > 0 || (_p.Statistics & Statistics.Percents) > 0 || (_p.Statistics & Statistics.ProcessIndexes) > 0) MakeVisuals(_dataTables, _surface);
        }


        private void Rt_ActiveCellToolTipRequired(object o, ActiveCellEventArgs e)
        {
            if (_statisticsTables == null || _statisticsTables.Count == 0) return;

            var field = _statisticsTables[0].ColumnNames.Where(a=> !a.StartsWith("#")).ToList()[e.column];
            if (field == _p.ColorField) return;
            var parameter = _statisticsTables[0].Table[e.row - 1].Row[0]?.ToString();
            var layer = _p.LayerMode == ReportChartLayerMode.Multiple 
                ? e.source.Tag
                : _statisticsTables[0].Table[e.row - 1].Row[1]?.ToString();
            switch (field)
            {
                case "Pp":
                case "Ppk":
                    CreateTooltipXCard(o, parameter, layer, true);
                    break;

                case "Скользящая оценка отклонения":
                case "Cp":
                case "Cpk":
                    CreateTooltipXCard(o, parameter, layer, false);
                    break;

                case "Статистика D Колмогорова - Смирнова":
                case "p(D)":
                case "Статистика W Шапиро - Уилка":
                case "p(W)":
                    CreateTooltipPP(o, parameter, layer);
                    break;

                case "Нижняя граница":
                case "Верхняя граница":
                    break;

                default:
                    CreateTooltipHistogram(o, parameter, layer);
                    break;
            }
        }

        private void Rt_ActiveCellClicked(object o, ActiveCellEventArgs e)
        {
            if (_statisticsTables == null || _statisticsTables.Count == 0) return;

            var field = _statisticsTables[0].ColumnNames.Where(a => !a.StartsWith("#")).ToList()[e.column];
            var parameter = _statisticsTables[0].Table[e.row - 1].Row[0]?.ToString();

            if (field == _p.ColorField)
            {
                AxisClicked?.Invoke(this, new AxisEventArgs {Tag="Z"});
                return;
            }

            switch (field)
            {
                case "Pp":
                case "Ppk":
                    var step1 = StepConverter.GetStep(CreateXCardParameters(parameter, true), string.Empty);
                    InteractiveParametersChange(this, new InteractiveParametersEventArgs { Step = step1 });
                    break;

                case "Скользящая оценка отклонения":
                case "Cp":
                case "Cpk":
                    var step3 = StepConverter.GetStep(CreateXCardParameters(parameter, false), string.Empty);
                    InteractiveParametersChange(this, new InteractiveParametersEventArgs { Step = step3 });
                    break;

                case "Нижняя граница":
                case "Верхняя граница":
                    break;

                case "Статистика D Колмогорова - Смирнова":
                case "p(D)":
                case "Статистика W Шапиро - Уилка":
                case "p(W)":
                    var step4 = StepConverter.GetStep(CreatePPParameters(parameter), string.Empty);
                    InteractiveParametersChange(this, new InteractiveParametersEventArgs { Step = step4 });
                    break;
                default:
                    var step2 = StepConverter.GetStep(CreateHistogramParameters(parameter), string.Empty);
                    InteractiveParametersChange(this, new InteractiveParametersEventArgs { Step = step2 });
                    break;
            }
        }

        private void CreateTooltipHistogram(object o, string yField, string layer)
        {
            var toolTip = o as ToolTip;
            if (toolTip == null) return;
            
            var data = new Data(_selectedTable, new FieldsInfo { ColumnX = yField, ColumnY = yField, ColumnZ = _p.ColorField == "Нет" ? null : _p.ColorField });
            ReportChartHistogramParameters rchp = CreateHistogramParameters(yField);

            var limits = new Limits((_p.ColorField == "Нет") ? data.GetLDR() : data.GetLDRForLayer(layer),
                ReportChartRangeType.RangeIsNTD,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.Card,
                null,
                null,
                yField, false, ReportChartRangeStyle.None);
            
            var frequencies = new Frequencies(data, rchp, limits, true, false);
            var frequencyTable = (_p.ColorField != "Нет")
                ? frequencies.GetPartialFreqTable(layer)
                : frequencies.GetFreqTable();
            var statinfo = (_p.ColorField != "Нет")
                ? frequencies.GetGroupStatInfo().Single(a => a.Group.ToString() == layer)
                : frequencies.GetDataStatInfo();
            statinfo.Group = null;

            var mfd = new List<MathFunctionDescription> {ReportChartStratification.SetupZ(rchp, statinfo, false)};

            var cd = new ChartDescription { ChartSeries = new List<ChartSeriesDescription>() };
            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Частоты",
                XColumnName = yField,
                XIsNominal = false,
                XAxisTitle = yField,
                WColumnName = "_Отступ",
                VColumnName = "Шаг",
                ZColumnName = _p.ColorField == "Нет" ? null : _p.ColorField,
                YColumnName = "Число наблюдений",
                SeriesTitle = "Частоты",
                SeriesType = ChartSeriesType.StairsOnX,
                ColorScale = _p.ColorScale,
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                LineSize = 1,
                YAxisTitle = "Число наблюдений",
                ColorColumnName = "Цвет"
            };
            cd.ChartSeries.Add(csd);
            var rangeTable = limits.GetRangesYTable();
            RangesHelper.AppendRanges(cd, true, rangeTable, true, true, ReportChartRangeStyle.Thin, false);

            var chartFreq = new Chart
            {
                EnableSecondaryAxises = false,
                Height = 240,
                Width = 300,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                ShowHorizontalGridlines = (_p.LayoutMode & ReportChartLayoutMode.ShowGrid) == ReportChartLayoutMode.ShowGrid && (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                ShowHorizontalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowX) == ReportChartLayoutMode.ShowX,
                ShowVerticalAxises = (_p.LayoutMode & ReportChartLayoutMode.ShowY) == ReportChartLayoutMode.ShowY,
                ShowHorizontalZero = false,
                VerticalAxisSymmetry = false,
                ShowTitle = false,
                ShowSubtitle = false,
                IsTransparent = false,
                UseDefaultColor = false,
                EnableCellCondense = true,
                GlobalFontCoefficient = _p.FontSize,
                Inverted = false,
                ShowLegend = false,
                ShowTable = false,
                Margin = new Thickness(-5, 0, -5, 0),
                HideNegativeX = false,
                ChartData = cd,
                SourceName = _p.Table,
                MathFunctionDescriptions = mfd,
                DataTables = new List<SLDataTable> { frequencyTable, rangeTable },
                FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis>(), Editable = false },
            };
            chartFreq.FixedAxises.FixedAxisCollection.Add(new FixedAxis
            {
                Axis = "X",
                Step = statinfo.Step ?? double.NaN,
                StepFixed = true,
                StepLocked = true
            });
            toolTip.Content = chartFreq;
            chartFreq.MakeChart();
        }

        private void CreateTooltipXCard(object o, string yField, string layer, bool realStdev)
        {
            var toolTip = o as ToolTip;
            if (toolTip == null) return;
            var data = new Data(_selectedTable, new FieldsInfo { ColumnY = yField, ColumnZ = _p.ColorField});
            var table = data.GetDataTable();
            if (_p.ColorField != "Нет")
            {
                var ci = table.ColumnNames.IndexOf(_p.ColorField);
                var layertable = new SLDataTable
                {
                    ColumnNames = table.ColumnNames,
                    DataTypes = table.DataTypes,
                    TableName = table.TableName,
                    Table = new List<SLDataRow>()
                };
                foreach (var row in table.Table)
                {
                    if (row.Row[ci].ToString() == layer) layertable.Table.Add(row);
                }
                table = layertable;
            }
            var limits = new Limits((_p.ColorField == "Нет") ? data.GetLDR() : data.GetLDRForLayer(layer),
                realStdev ? ReportChartRangeType.RangeIsSigmas : ReportChartRangeType.RangeIsSlide,
                ReportChartCardType.CardTypeIsX,
                ReportChartType.Card,
                null,
                null,
                yField, false, ReportChartRangeStyle.None);

            var cd = new ChartDescription { ChartSeries = new List<ChartSeriesDescription>() };

            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Данные",
                XColumnName = "Наблюдение",
                XIsNominal = true,
                XAxisTitle = "Наблюдение",
                YColumnName = yField,
                MarkerSizeColumnName = "Маркер",
                SeriesTitle = yField,
                ColorColumnName = "Tag",
                LabelColumnName = "Метка",
                SeriesType = ChartSeriesType.LineXY,
                YAxisTitle = yField,
                ColorScale = _p.ColorScale,
                MarkerColor = ColorConvertor.GetDefaultColorForScale(_p.ColorScale),
                MarkerMode = _p.MarkerMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerSize = 6,
                LineSize = table.Table.Count < 500 ? 0.5 : 0
            };

            cd.ChartSeries.Add(csd);
            var rangeTable = limits.GetRangesYTable();
            RangesHelper.AppendRanges(cd, false, rangeTable, true, true, ReportChartRangeStyle.Thin, false);

            var chartXCard = new Chart
            {
                EnableSecondaryAxises = false,
                Height = 240,
                Width = 300,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = true,
                ShowHorizontalGridlines = true,
                ShowHorizontalAxises = true,
                ShowVerticalAxises = true,
                ShowTitle = false,
                ShowSubtitle = false,
                IsTransparent = false,
                UseDefaultColor = false,
                Inverted = false,
                GlobalFontCoefficient = _p.FontSize,
                EnableCellCondense = true,
                ShowLegend = false,
                ShowTable = false,
                Margin = new Thickness(-5, 0, -5, 0),
                ChartData = cd,
                SourceName = _p.Table,
                DataTables = new List<SLDataTable>
                    {
                        table,rangeTable
                    },
                FixedAxises = new FixedAxises { FixedAxisCollection = new ObservableCollection<FixedAxis>(), Editable = false }
            };
            toolTip.Content = chartXCard;
            chartXCard.MakeChart();
        }

        private void CreateTooltipPP(object o, string yField, string layer)
        {
            var toolTip = o as ToolTip;
            if (toolTip == null) return;
            var data = new Data(_selectedTable, new FieldsInfo { ColumnY = yField, ColumnZ = _p.ColorField });
           

            var probability = new Probability(_p.ColorField == "Нет" ? data.GetLDR() : data.GetLDRForLayer(layer));
            var pt = probability.GetTable();
            pt.TableName = "Вероятности";

            var cd = new ChartDescription { ChartSeries = new List<ChartSeriesDescription>() };

            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Вероятности",
                XColumnName = "Расчетная вероятность",
                XIsNominal = false,
                XAxisTitle = "Расчетная вероятность",
                YColumnName = "Наблюдаемая вероятность",
                ColorColumnName = null,
                SeriesTitle = yField,
                SeriesType = ChartSeriesType.LineXY,
                ColorScale = _p.ColorScale,
                MarkerMode = _p.MarkerMode,
                LineSize = 2,
                MarkerSize = 6,
                MarkerSizeMode = _p.MarkerSizeMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                YAxisTitle = "Наблюдаемая вероятность"
            };

            cd.ChartSeries.Add(csd);

            var chartPP = new Chart
            {
                EnableSecondaryAxises = false,
                Height = 240,
                Width = 300,
                HasShadow = (_p.LayoutMode & ReportChartLayoutMode.Shadowed) == ReportChartLayoutMode.Shadowed,
                ShowVerticalGridlines = true,
                ShowHorizontalGridlines = true,
                ShowHorizontalAxises = true,
                ShowVerticalAxises = true,
                ShowTitle = false,
                ShowSubtitle = false,
                IsTransparent = false,
                UseDefaultColor = false,
                Inverted = false,
                GlobalFontCoefficient = _p.FontSize,
                EnableCellCondense = true,
                ShowLegend = false,
                ShowTable = false,
                Margin = new Thickness(-5, 0, -5, 0),
                ChartData = cd,
                SourceName = _p.Table,
                DataTables = new List<SLDataTable>
                    {
                        pt,
                    },
                FixedAxises = new FixedAxises
                {
                    FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {
                            new FixedAxis
                            {
                                Axis = "X",
                                AxisMin = 0.0d,
                                AxisMinLocked = true,
                                AxisMinFixed = true,
                                AxisMax = 1.0d,
                                AxisMaxLocked = true,
                                AxisMaxFixed = true,
                                Step = 0.1d,
                                StepLocked = true,
                                StepFixed = true
                            },
                            new FixedAxis
                            {
                                Axis = "Y",
                                AxisMin = 0.0d,
                                AxisMinLocked = true,
                                AxisMinFixed = true,
                                AxisMax = 1.0d,
                                AxisMaxLocked = true,
                                AxisMaxFixed = true,
                                Step = 0.1d,
                                StepLocked = true,
                                StepFixed = true
                            }
                        },
                    Editable = false
                },
            };
            chartPP.MathFunctionDescriptions = new List<MathFunctionDescription>
            {
                new MathFunctionDescription
                {
                    Description = "X=Y",
                    LineColor = "#ffff0000",
                    LineSize = 3,
                    PixelStep = 5,
                    MathFunctionCalculator = (ari, x) => x
                }
            }; 

            toolTip.Content = chartPP;
            chartPP.MakeChart();
        }

        private ReportChartHistogramParameters CreateHistogramParameters(string yField)
        {
            return new ReportChartHistogramParameters
            {
                AlignMode = ReportChartAlignMode.AlignByLL,
                BinMode = ReportChartBinMode.IncludeRight,
                ColorField = _p.ColorField,
                ColorScale = _p.ColorScale,
                ColumnsType = ReportChartColumnDiagramType.Generic,
                DetailLevel = 4,
                DrawParts = ReportChartDrawParts.Solid,
                Fields = new List<string> { yField },
                FitMode = ReportChartFitMode.FitGauss,
                FitStyle = ReportChartRangeStyle.Thick,
                LabelsType = ReportChartColumnsLabelsType.None,
                MarkerMode = _p.MarkerMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                Table = _p.Table,
                YMode = ReportChartFrequencyYMode.FrequencyInNumber,
                RangeType = ReportChartRangeType.RangeIsNTD,
                RangeStyle = ReportChartRangeStyle.Thin,
                FontSize = _p.FontSize,
                LayoutMode = _p.LayoutMode,
                LayoutSize = _p.LayoutSize,
                HistogramType = ReportChartHistogramType.Frequencies,
            };
        }

        private ReportChartCardParameters CreateXCardParameters(string yField, bool realStdev)
        {
            ReportChartCardParameters newp = new ReportChartCardParameters
            {
                    Fields = new List<string> { yField },
                    ColorScale = _p.ColorScale,
                    FixedAxisesDescription = _p.FixedAxisesDescription,
                    FontSize = _p.FontSize,
                    LayoutMode = _p.LayoutMode,
                    LayoutSize = _p.LayoutSize,
                    MarkerMode = _p.MarkerMode,
                    MarkerShapeMode = _p.MarkerShapeMode,
                    MarkerSizeMode = _p.MarkerSizeMode,
                    RangeStyle = ReportChartRangeStyle.Thin,
                    RangeType = realStdev ? ReportChartRangeType.RangeIsSigmas : ReportChartRangeType.RangeIsSlide,
                    SmoothMode = ReportChartRegressionFitMode.DynamicSmoothNone,
                    SmoothStyle = ReportChartRangeStyle.None,
                    Table = _p.Table,
                    CardType = ReportChartCardType.CardTypeIsX 
                };
                
            return newp;
        }

        private ReportChartProbabilityPlotParameters CreatePPParameters(string yField)
        {
            ReportChartProbabilityPlotParameters newp = new ReportChartProbabilityPlotParameters
            {
                Fields = new List<string> { yField },
                ColorScale = _p.ColorScale,
                FixedAxisesDescription = _p.FixedAxisesDescription,
                FontSize = _p.FontSize,
                LayoutMode = _p.LayoutMode,
                LayoutSize = _p.LayoutSize,
                MarkerMode = _p.MarkerMode,
                MarkerShapeMode = _p.MarkerShapeMode,
                MarkerSizeMode = _p.MarkerSizeMode,
                Table = _p.Table 
            };

            return newp;
        }
    }
}