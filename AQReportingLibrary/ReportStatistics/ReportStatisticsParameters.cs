﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportStatisticsParameters
    {
        public string Table { get; set; }
        public List<string> Fields { get; set; }
        public string ColorField { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public Statistics Statistics { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }
        public int LimitedLayers { get; set; }

        public ReportChartLayerMode LayerMode { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
    }
}