﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQChartLibrary;
using AQMathClasses;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class ReportStatistics
    {
        Panel _surface;
        List<Chart> _charts;
        private List<SLDataTable> _statisticsTables, _dataTables;
        private BasicStatistics _basicStatistics;
        private BackgroundWorker _worker;
        private Thread _workerThread;
        private string _originalLayersType;

        public bool MakeVisuals(List<SLDataTable> dataTables, Panel surface)
        {
            _surface = surface;
            _dataTables = dataTables;
            _surface.Children.Clear();
            SetMessage("Расчет статистики...");

            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _worker.RunWorkerCompleted += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(ShowStatisticsTables);
            };

            _worker.DoWork += (o, e) =>
            {
                _surface.Dispatcher.BeginInvoke(() =>
                {
                    _workerThread = Thread.CurrentThread;
                    SetupReportData(dataTables, _worker);
                    InternalTablesReadyForExport?.Invoke(this,
                        new InternalTablesReadyForExportEventArgs {InternalTables = _statisticsTables});
                });
            };
            _worker.RunWorkerAsync();
            return true;
        }

        public void Cancel()
        {
            if (_workerThread != null) _worker.CancelAsync();
        }

        public void RefreshVisuals()
        {
            InternalTablesReadyForExport?.Invoke(this,
                new InternalTablesReadyForExportEventArgs { InternalTables = _statisticsTables });
            ShowStatisticsTables();
        }

        private void SetupReportData(IEnumerable<SLDataTable> dataTables, BackgroundWorker worker)
        {
            _selectedTable = dataTables.FirstOrDefault(a => a.TableName == _p.Table);
            _originalLayersType = _selectedTable.GetDataType(_p.ColorField);
            var useFormat = (_p.LayoutMode & ReportChartLayoutMode.GroupByFormatted) == ReportChartLayoutMode.GroupByFormatted;
            var useLayerLimitation = (_p.LayoutMode & ReportChartLayoutMode.LimitedLayers) == ReportChartLayoutMode.LimitedLayers;
            _originalTable = _selectedTable;

            if (useLayerLimitation || useFormat)
            {
                var lld = new LimitedLayersDescription
                {
                    SourceTable = _selectedTable,
                    Z = _p.ColorField,
                    ValuesForAggregation = _p?.Fields?.FirstOrDefault(),
                    FixedAxises = _p.FixedAxisesDescription,
                    AllowedLayersNumber = _p.LimitedLayers,
                    UseFormat = useFormat,
                    UseLayersLimitation = useLayerLimitation
                };
                var ll = new LimitedLayers(lld);
                _selectedTable = ll.GetLimitedLayersTable();
            }
            if (_p.FixedAxisesDescription == null || _p.FixedAxisesDescription.FixedAxisCollection == null || _p.FixedAxisesDescription.FixedAxisCollection.Count == 0)
            {
                var fixedAxises = new FixedAxises
                {
                    FixedAxisCollection = new ObservableCollection<FixedAxis>
                        {new FixedAxis {DataType = _originalLayersType, Axis = "Z", Format = "dd.MM.yyyy", IsValid = true}},
                    Editable = true
                };
                ChartSetsFixedAxis?.Invoke(this, new ChartSetsFixedAxisEventArgs {SettingChart = new Chart {FixedAxises = fixedAxises}});
            }

            _basicStatistics = new BasicStatistics(_selectedTable, _p.Fields, _p.ColorField == "Нет" ? null : _p.ColorField, _p.Statistics, _p.LayerMode, worker);
            _statisticsTables = _basicStatistics.GetStatisticTables((_p.Statistics&Statistics.SkipZeroCasesOption) > 0);
            
        }

        private void SetMessage(string text)
        {
            _surface.Children.Clear();
            var tb = new TextBlock
            {
                Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0xe8, 0xf4, 0xf4)),
                FontSize = 40,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Text = text,
                TextAlignment = TextAlignment.Center,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(0, 150, 0, 100)
            };
            _surface.Children.Add(tb);
        }

        private void ShowStatisticsTables()
        {
            _surface.Children.Clear();
            _surface.UpdateLayout();
            var rt = new ReportTables(_selectedTable)
            {
                Inverted = (_p.LayoutMode & ReportChartLayoutMode.Inverted) == ReportChartLayoutMode.Inverted,
                IsTransparent = (_p.LayoutMode & ReportChartLayoutMode.Transparent) == ReportChartLayoutMode.Transparent,
                Width = _p.LayoutSize.Width,
                ForegroundBrush =  ConvertStringToStroke(ColorConvertor.GetDefaultColorForScale(_p.ColorScale)), 
                GlobalFontCoefficient = _p.FontSize,
                HeaderPrefix = string.Empty,
                FirstColumnIsCaseName = true,
                Title = GetChartTitle(), 
                IsInteractive = true,
                FixedHeader = true,
                SubTitle = ((_p.LayoutMode & ReportChartLayoutMode.FilterDescription) > 0) ? _selectedTable.GetFilterDescription() : null
        };
            rt.InteractiveRename += chartDynamics_InteractiveRename;
            rt.ActiveCellClicked += Rt_ActiveCellClicked;
            rt.ActiveCellToolTipRequired += Rt_ActiveCellToolTipRequired;
            rt.CellEdited += Rt_CellEdited;
            rt.VisualReady += RtOnVisualReady;
            rt.DrawReportTables(_statisticsTables, _surface);
        }

        private void RtOnVisualReady(object o, VisualReadyEventArgs visualReadyEventArgs)
        {
            Finished?.Invoke(this, new ReportChartFinishedEventArgs());
        }

        public Brush ConvertStringToStroke(string c)
        {
            if (string.IsNullOrEmpty(c)) return new SolidColorBrush(Colors.Transparent);
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            Brush stroke = new SolidColorBrush(Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff)));
            return stroke;
        }
    }
}