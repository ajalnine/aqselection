﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public static class RenameProcessor
    {
        public static ReportInteractiveRenameEventArgs DefaultRenameProcess(ChartRenameEventArgs e, SLDataTable selectedTable)
        {
            var re = new ReportInteractiveRenameEventArgs { RenameOperations = new List<ReportRenameOperation>() };
            foreach (var ro in e.RenameOperations)
            {
                var rename = new ReportRenameOperation { OldName = ro.OldName, NewName = ro.NewName, Table = selectedTable.TableName, LayerField = ro.LayerField};

                switch (ro.ChartItemType)
                {
                    case ChartRenameItemType.Table:
                        if (rename.OldName != selectedTable.TableName) continue;
                            rename.RenameTarget = ReportRenameTarget.Table;
                        break;

                    case ChartRenameItemType.Cathegory:
                        rename.RenameTarget = ReportRenameTarget.Cathegory;
                        break;

                    case ChartRenameItemType.Layer:
                        rename.RenameTarget = ReportRenameTarget.Layer;
                        break;

                    case ChartRenameItemType.Field:
                        if (ro.TableName.Contains("Границы") || ro.TableName.Contains("Пределы"))
                        {
                            rename.RenameTarget = ReportRenameTarget.Range;
                            break;
                        }
                        switch (ro.TableName)
                        {
                            case "Периоды":
                                rename.RenameTarget = ReportRenameTarget.DateRange;
                                break;
                            default:
                                if (!selectedTable.ColumnNames.Contains(rename.OldName)) continue;
                                rename.RenameTarget = ReportRenameTarget.Field;
                                break;
                        }
                        break;
                }
                re.RenameOperations.Add(rename);
            }
            var duplicateCleaned = new List<ReportRenameOperation>();
            foreach (var ro in re.RenameOperations.Where(ro => !duplicateCleaned.Any(a => a.OldName == ro.OldName && a.RenameTarget == ro.RenameTarget && a.Table == ro.Table)))
            {
                duplicateCleaned.Add(ro);
            }
            re.RenameOperations = duplicateCleaned;
            return re;
        }
    }
}
       