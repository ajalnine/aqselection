﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQMathClasses;

namespace AQReportingLibrary
{
    public class ANOVA
    {
        private readonly IEnumerable<DynamicRow> _source;

        #region TwoWay

        public class ANOVAResult
        {
            public string Comment;
            public List<SLDataTable> ANOVATables;
        }

        public class GLM
        {
            public double MSA;
            public double MSB;
            public double MSAB;
            public double MSE;
            public double SSA;
            public double SSB;
            public double SSAB;
            public double SSE;
            public double R2;
            public bool TwoWay;
            public double FA;
            public double FB;
            public double FAB;
            public double pA;
            public double pB;
            public double pAB;
            public string Method;
            public double N;
            public double I;
            public double K;
            public double Intercept;
            public double Fintercept;
            public double Pintercept;

            public double DFA;
            public double DFB;
            public double DFAB;
            public double DFE;
            public double DFI;
            public RegressionResult r;
            public RegressionResult rA;
            public RegressionResult rB;
            public RegressionResult rAB;
            public double Levene;
            public double pLevene;
        }

        private GLM GetTwoWayANOVAResult(IEnumerable<DynamicRow> ldr)
        {
            var filledLDR = ldr.Where(a => a.Group != null && !string.IsNullOrEmpty(a.Group.ToString()) && a.Z != null && !string.IsNullOrEmpty(a.Z.ToString()) && a.Y.HasValue).ToList();
            var aGroups = filledLDR.Select(a => a.Group).Distinct().ToList();
            var bGroups = filledLDR.Select(a => a.Z).Distinct().ToList();
            var abGroups = filledLDR.GroupBy(a=>(object)new {g = a.Group, z = a.Z }).ToList();
            var levene = LeveneTest(abGroups, false);
            var af = aGroups.Count - 1;
            var bf = bGroups.Count - 1;
            var abf = af * bf;
            if (abf > 255) return null;

            var m = 1 + af + bf + abf;
            var n = filledLDR.Count;
            var groupCheck = new int[af + 1, bf + 1];
            var data = new List<List<double>>();
            for (var i = 0; i < m; i++) data.Add(new List<double>());
            for (var i = 0; i < n; i++)
            {
                var dr = filledLDR[i];
                if (dr.Y != null) data[0].Add(dr.Y.Value);

                var a = aGroups.IndexOf(dr.Group);
                if (a < 0) return null;
                if (a < af) for (var j = 0; j < af; j++) data[j + 1].Add(j == a ? 1.0d : 0.0d);
                else for (var j = 0; j < af; j++) data[j + 1].Add(-1.0d);

                var b = bGroups.IndexOf(dr.Z);
                groupCheck[a, b]++;
                if (b < 0) return null;
                if (b < bf) for (var j = 0; j < bf; j++) data[j + af + 1].Add(j == b ? 1.0d : 0.0d);
                else for (var j = 0; j < bf; j++) data[j + af + 1].Add(-1.0d);

                var c = af + 1 + bf;
                for (var k = 0; k < af; k++)
                    for (var l = 0; l < bf; l++)
                    {
                        data[c].Add(data[1 + k][i] * data[1 + l + af][i]);
                        c++;
                    }
            }
            if (groupCheck.Length == 0) return null;
            var prev = groupCheck[0, 0];
            var balanced = true;
            for (var a = 0; a <= af; a++)
                for (var b = 0; b <= bf; b++)
                {
                    if (groupCheck[a, b] < 1)
                    {
                        var result6 = TypeVI(filledLDR, aGroups, bGroups, af, bf, abf, data, groupCheck, false);
                        if (result6 == null) return null;
                        result6.Levene = levene.Item1;
                        result6.pLevene = levene.Item2;
                        return result6;
                    }
                    if (prev != groupCheck[a, b]) balanced = false;
                }
            var result3 = TypeIII(filledLDR, aGroups, bGroups, af, bf, abf, data, balanced);
            if (result3 != null)
            {
                result3.Levene = levene.Item1;
                result3.pLevene = levene.Item2;
            }
            return result3;
        }

        private static GLM TypeIII(IEnumerable<DynamicRow> ldr, List<object> aGroups, List<object> bGroups, int af, int bf, int abf, List<List<double>> data, bool balanced)
        {
            var result = new GLM
            {
                TwoWay = true,
                I = aGroups.Count,
                K = bGroups.Count,
                N = ldr.Count()
            };
            result.DFE = result.N - result.I * result.K;
            result.DFA = result.I - 1;
            result.DFB = result.K - 1;
            result.DFI = 1;
            result.DFAB = result.DFA * result.DFB;
            result.Method = balanced
                ? "GLM, сбалансированный дизайн"
                : "GLM, несбалансированный дизайн";
            var dataA = new List<List<double>> { data[0] };
            for (var i = 0; i < bf; i++) dataA.Add(data[1 + i + af]);
            for (var i = 0; i < abf; i++) dataA.Add(data[1 + i + af + bf]);
            var dataB = new List<List<double>> { data[0] };
            for (var i = 0; i < af; i++) dataB.Add(data[1 + i]);
            for (var i = 0; i < abf; i++) dataB.Add(data[1 + i + af + bf]);
            var dataAB = new List<List<double>> { data[0] };
            for (var i = 0; i < af; i++) dataAB.Add(data[1 + i]);
            for (var i = 0; i < bf; i++) dataAB.Add(data[1 + i + af]);

            var totalRegression = new Regression(data, true);
            var abRegression = new Regression(dataAB, true);
            var aRegression = new Regression(dataA, true);
            var bRegression = new Regression(dataB, true);

            result.r = totalRegression.RegressionResult;
            if (result.r == null || double.IsNaN(result.r.MS)) return null;
            result.rA = aRegression.RegressionResult;
            if (result.rA == null || double.IsNaN(result.rA.MS)) return null;
            result.rB = bRegression.RegressionResult;
            if (result.rB == null || double.IsNaN(result.rB.MS)) return null;
            result.rAB = abRegression.RegressionResult;
            if (result.rAB == null || double.IsNaN(result.rAB.MS)) return null;

            result.MSE = totalRegression.RegressionResult.MSE;
            result.SSAB = totalRegression.RegressionResult.MS - abRegression.RegressionResult.MS;
            result.SSA = totalRegression.RegressionResult.MS - aRegression.RegressionResult.MS;
            result.SSB = totalRegression.RegressionResult.MS - bRegression.RegressionResult.MS;

            result.R2 = totalRegression.RegressionResult.MultipleR2;
            result.SSE = result.MSE * result.DFE;
            result.MSA = result.SSA / result.DFA;
            result.MSB = result.SSB / result.DFB;
            result.MSAB = result.SSAB / result.DFAB;
            result.FA = result.MSA / result.MSE;
            result.FB = result.MSB / result.MSE;
            result.FAB = result.MSAB / result.MSE;
            result.pA = 1 - AQMath.FisherCD(result.FA, result.DFA, result.DFE) ?? double.NaN;
            result.pB = 1 - AQMath.FisherCD(result.FB, result.DFB, result.DFE) ?? double.NaN;
            result.pAB = 1 - AQMath.FisherCD(result.FAB, result.DFAB, result.DFE) ?? double.NaN;
            result.Intercept = Math.Abs(result.DFI) < double.Epsilon
                ? 0
                : Math.Pow(totalRegression.RegressionResult.Intercept, 2) * totalRegression.RegressionInput.n
                 ;

            result.Fintercept = result.Intercept / result.MSE;
            result.Pintercept = 1 - AQMath.FisherCD(result.Fintercept, result.DFI, result.DFE) ?? double.NaN;
            return result;
        }

        private static GLM TypeVI(List<DynamicRow> ldr, List<object> aGroups, List<object> bGroups, int af, int bf, int abf, List<List<double>> data, int[,] groupCheck, bool balanced)
        {
            var result = new GLM();
            var lostCells = 0;
            var lostA = new List<int>();
            var lostB = new List<int>();
            for (var i = 0; i <= af; i++)
            {
                for (var j = 0; j <= bf; j++)
                {
                    if (groupCheck[i, j] == 0)
                    {
                        lostCells++;
                        lostA.Add(i);
                        lostB.Add(j);
                    }
                }
            }
            var lostAFactor = lostA.Distinct().Count();
            var lostBFactor = lostB.Distinct().Count();
            result.TwoWay = true;
            var m = 1 + af + bf + abf;
            if (m > 255) return null;
            result.I = aGroups.Count;
            result.K = bGroups.Count;
            result.N = ldr.Count;
            result.DFE = result.N - result.I * result.K + lostCells;
            result.DFA = result.I - 1 - lostAFactor;
            result.DFB = result.K - 1 - lostBFactor;
            result.DFI = 1 - lostCells;
            result.DFAB = (result.I - 1) * (result.K - 1) - lostCells;

            if (result.DFAB < 0) result.DFAB = 0;
            if (result.DFI < 0) result.DFI = 0;
            if (result.DFA < 0) result.DFA = 0;
            if (result.DFB < 0) result.DFB = 0;
            var prefix = !balanced ? "не" : string.Empty;
            result.Method = $"GLM, {prefix}сбалансированный дизайн с пропусками";
            var checkRegression = new Regression(data, true);
            Regression totalRegression, aRegression, bRegression;

            var dataCheckA = new List<List<double>> { data[0] };
            for (var i = 0; i < bf; i++) dataCheckA.Add(data[1 + i + af]);
            for (var i = 0; i < abf; i++) dataCheckA.Add(data[1 + i + af + bf]);
            var dataCheckB = new List<List<double>> { data[0] };
            for (var i = 0; i < af; i++) dataCheckB.Add(data[1 + i]);
            for (var i = 0; i < abf; i++) dataCheckB.Add(data[1 + i + af + bf]);

            var aCheckRegression = new Regression(dataCheckA, true);
            var bCheckRegression = new Regression(dataCheckB, true);

            var dataNoEmpty = new List<List<double>>();
            var removedABFactors = 0;
            if (checkRegression.RegressionResult.RedundantCheckResult.Any(a => a))
            {
                for (var j = 0; j < 1 + af + bf; j++)
                {
                    dataNoEmpty.Add(data[j]);
                }
                var abfstart = 1 + af + bf;
                for (var j = abfstart; j < abfstart + abf; j++)
                {
                    var isRedundant =
                        (
                            checkRegression.RegressionResult.RedundantCheckResult[j - 1]
                        );
                    if (!isRedundant) dataNoEmpty.Add(data[j]);
                    else removedABFactors++;
                }

                totalRegression = new Regression(dataNoEmpty, true);
            }
            else
            {
                totalRegression = checkRegression;
                dataNoEmpty = data;
            }

            result.r = totalRegression.RegressionResult;
            if (result.r == null || double.IsNaN(result.r.MS)) return null;
            result.MSE = totalRegression.RegressionResult.MSE;
            result.R2 = totalRegression.RegressionResult.MultipleR2;
            result.SSE = result.MSE * (result.N - result.I * result.K + removedABFactors);

            var dataAB = new List<List<double>> { data[0] };
            for (var i = 0; i < af; i++) dataAB.Add(dataNoEmpty[1 + i]);
            for (var i = 0; i < bf; i++) dataAB.Add(dataNoEmpty[1 + i + af]);
            var abRegression = new Regression(dataAB, true);
            result.rAB = abRegression.RegressionResult;
            if (result.rAB == null || double.IsNaN(result.rAB.MS)) return null;
            result.SSAB = totalRegression.RegressionResult.MS - abRegression.RegressionResult.MS;

            if (aCheckRegression.RegressionResult.RedundantCheckResult.Any(a => a))
            {
                var dataA = new List<List<double>> { data[0] };
                for (var i = 0; i < bf; i++) dataA.Add(data[1 + i + af]);
                for (var i = 0; i < abf; i++)
                {
                    var isRedundant = aCheckRegression.RegressionResult.RedundantCheckResult[i + bf];
                    if (!isRedundant) dataA.Add(data[1 + i + af + bf]);
                }
                aRegression = new Regression(dataA, true);
            }
            else aRegression = aCheckRegression;

            result.rA = aRegression.RegressionResult;
            result.SSA = totalRegression.RegressionResult.MS - aRegression.RegressionResult.MS;

            if (bCheckRegression.RegressionResult.RedundantCheckResult.Any(a => a))
            {
                var dataB = new List<List<double>> { data[0] };
                for (var i = 0; i < af; i++) dataB.Add(data[1 + i]);
                for (var i = 0; i < abf; i++)
                {
                    var isRedundant = bCheckRegression.RegressionResult.RedundantCheckResult[i + af];
                    if (!isRedundant) dataB.Add(data[1 + i + af + bf]);
                }
                bRegression = new Regression(dataB, true);
            }
            else bRegression = bCheckRegression;

            result.rB = bRegression.RegressionResult;
            result.SSB = totalRegression.RegressionResult.MS - bRegression.RegressionResult.MS;


            result.MSE = result.DFE > 0 ? result.SSE / result.DFE : 0;
            result.MSA = result.DFA > 0 ? result.SSA / result.DFA : 0;
            result.MSB = result.DFB > 0 ? result.SSB / result.DFB : 0;
            result.MSAB = result.DFAB > 0 ? result.SSAB / result.DFAB : 0;
            result.FA = result.MSA / result.MSE;
            result.FB = result.MSB / result.MSE;
            result.FAB = result.MSAB / result.MSE;
            result.pA = 1 - AQMath.FisherCD(result.FA, result.DFA, result.DFE) ?? double.NaN;
            result.pB = 1 - AQMath.FisherCD(result.FB, result.DFB, result.DFE) ?? double.NaN;
            result.pAB = 1 - AQMath.FisherCD(result.FAB, result.DFAB, result.DFE) ?? double.NaN;
            result.Intercept = Math.Abs(result.DFI) < double.Epsilon ? 0 : Math.Pow(totalRegression.RegressionResult.Intercept, 2) * totalRegression.RegressionInput.n;
            result.Fintercept = result.Intercept / result.MSE;
            result.Pintercept = 1 - AQMath.FisherCD(result.Fintercept, result.DFI, result.DFE) ?? double.NaN;
            return result;
        }

        public ANOVAResult GetANOVAResultForTwoWay(string a, string b)
        {
            var anova = GetTwoWayANOVAResult(_source);
            if (anova == null)
            {
                var groups = (from l in _source group l by l.Group).ToList();
                var layers = (from l in _source group l by l.Z).ToList();
                var anova1WayA = GetANOVAResult(groups);
                var anova1WayB = GetANOVAResult(layers);
                var resultA = ANOVAResultForMeans(anova1WayA, a);
                if (resultA!= null) { resultA.ANOVATables[0].Header += ", " + GetLeveneGroupComment(anova1WayA.Levene, anova1WayA.pLevene, false); }
                var resultB = ANOVAResultForMeans(anova1WayB, b);
                if (resultB != null) { resultB.ANOVATables[0].Header += ", " + GetLeveneGroupComment(anova1WayB.Levene, anova1WayB.pLevene, false); }
                if (resultA == null)
                {
                    if (resultB == null) return new ANOVAResult { Comment = string.Empty, ANOVATables = null };
                    else return new ANOVAResult { Comment = b + ": " + resultB.Comment, ANOVATables = resultB.ANOVATables };
                }
                if (resultB == null) return new ANOVAResult { Comment = a + ": " + resultA.Comment, ANOVATables = resultA.ANOVATables };
                else return new ANOVAResult { Comment = a + ": " + resultA.Comment + ", " + b + ": " + resultB.Comment, ANOVATables = resultA.ANOVATables.Union(resultB.ANOVATables).ToList() };
            }
            if (anova.I + anova.K < 2) return null;
            var c3 = $"A:{a}, B:{b}, {anova.Method} " 
                   + 
                   (double.IsNaN(anova.pA) ? string.Empty : (Math.Abs(anova.pA) >= 0.05
                        ? $" pA(F)={Math.Round(Math.Abs(anova.pA), 3)}"
                        : $@" <red>pA(F)={Math.Round(Math.Abs(anova.pA), 3)}</red>"))
                   +
                   (double.IsNaN(anova.pB) ? string.Empty : (Math.Abs(anova.pB) >= 0.05
                       ? $", pB(F)={Math.Round(Math.Abs(anova.pB), 3)}"
                       : $@", <red>pB(F)={Math.Round(Math.Abs(anova.pB), 3)}</red>"))
                   +
                   (double.IsNaN(anova.pAB) ? string.Empty : (Math.Abs(anova.pAB) >= 0.05
                       ? $", pAB(F)={Math.Round(Math.Abs(anova.pAB), 3)}"
                       : $@", <red>pAB(F)={Math.Round(Math.Abs(anova.pAB), 3)}</red>")
                    + ", R²=" + Math.Round(anova.R2, 2) + GetLeveneGroupComment(anova.Levene, anova.pLevene, false));
            return new ANOVAResult { Comment = c3, ANOVATables = GetANOVATableForTwoWay(anova, a, b) };
        }

        private List<SLDataTable> GetANOVATableForTwoWay(GLM anova, string a, string b)
        {
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = anova.Method,
                Header = "R²= " + Math.Round(anova.R2, 2) + GetLeveneGroupComment(anova.Levene, anova.pLevene, false),
                ColumnNames = new List<string> { "Фактор", "SS", "DF", "MS", "F", "p", "#SS_Цвет", "#DF_Цвет", "#MS_Цвет", "#F_Цвет", "#p_Цвет" },
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string> { "String", "Double", "Double", "Double", "Double", "Double", "String", "String", "String", "String", "String" }
            };

            var icolor = anova.Pintercept < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    string.Empty,
                    anova.DFI > 0 ? (double?)Math.Round(anova.Intercept,5) : null,
                    AQMath.SmartRound(anova.DFI,0),
                    anova.DFI > 0 ? (double?) Math.Round(anova.Intercept,5) : null,
                    anova.DFI > 0 ? (double?)Math.Round(anova.Fintercept,1) : null,
                    anova.DFI > 0 ? (double?)Math.Round(anova.Pintercept,5) : null,
                    icolor,
                    icolor,
                    icolor,
                    icolor,
                    icolor
                }
            });

            var acolor = anova.pA < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    "A:"+a,
                    anova.DFA > 0 ? (double?)Math.Round(anova.SSA,5) : null,
                    AQMath.SmartRound(anova.DFA,0),
                    anova.DFA > 0 ? (double?)Math.Round(anova.MSA,5) : null,
                    anova.DFA > 0 ?(double?)Math.Round(anova.FA,1) : null,
                    anova.DFA > 0 ?(double?)Math.Round(anova.pA,5) : null,
                    acolor,
                    acolor,
                    acolor,
                    acolor,
                    acolor
                }
            });

            var bcolor = anova.pB < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    "B:"+b,
                    anova.DFB > 0 ? (double?)Math.Round(anova.SSB,5) : null,
                    AQMath.SmartRound(anova.DFB,0),
                    anova.DFB > 0 ? (double?)Math.Round(anova.MSB,5) : null,
                    anova.DFB > 0 ? (double?)Math.Round(anova.FB,1) : null,
                    anova.DFB > 0 ? (double?)Math.Round(anova.pB,5) : null,
                    bcolor,
                    bcolor,
                    bcolor,
                    bcolor,
                    bcolor
                }
            });

            var aBcolor = anova.pAB < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    "AxB",
                    anova.DFAB > 0 ? (double?)Math.Round(anova.SSAB,5) : null,
                    AQMath.SmartRound(anova.DFAB,0),
                    anova.DFAB > 0 ? (double?)Math.Round(anova.MSAB,5) : null,
                    anova.DFAB > 0 ? (double?)Math.Round(anova.FAB,1) : null,
                    anova.DFAB > 0 ? (double?)Math.Round(anova.pAB,5) : null,
                    aBcolor,
                    aBcolor,
                    aBcolor,
                    aBcolor,
                    aBcolor
                }
            });

            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    "Ошибка",
                    Math.Round(anova.SSE,5),
                    Math.Round(anova.DFE,0),
                    Math.Round(anova.MSE,5),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                }
            });
            return new List<SLDataTable> { result };
        }

        #endregion

        #region OneWay

        private GLM GetOneWayANOVAResult(IEnumerable<DynamicRow> ldr, bool layers)
        {
            var dynamicRows = ldr as IList<DynamicRow> ?? ldr.ToList();
            var filledLDR = dynamicRows.ToList();
            var aGroups = layers ? filledLDR.Select(a => a.Z).Distinct().ToList() : filledLDR.Select(a => a.Group).Distinct().ToList();
            var agroupsData = layers ? filledLDR.GroupBy(a => a.Z).ToList() : filledLDR.GroupBy(a => a.Group).ToList();
            var levene = LeveneTest(agroupsData, false);

            var af = aGroups.Count - 1;
            if (af > 30) return null;

            var m = 1 + af;
            var n = filledLDR.Count;

            var data = new List<List<double>>();
            for (var i = 0; i < m; i++) data.Add(new List<double>());
            for (var i = 0; i < n; i++)
            {
                var dr = filledLDR[i];
                if (dr.Y != null) data[0].Add(dr.Y.Value);

                var a = aGroups.IndexOf(layers ? dr.Z : dr.Group);
                if (a < 0) return null;
                if (a < af) for (var j = 0; j < af; j++) data[j + 1].Add(j == a ? 1.0d : 0.0d);
                else for (var j = 0; j < af; j++) data[j + 1].Add(-1.0d);
            }
            var result = new GLM
            {
                TwoWay = false,
                I = aGroups.Count,
                N = dynamicRows.Count
            };
            result.DFE = result.N - result.I;
            result.DFA = result.I - 1;
            result.Method = "GLM";
            var totalRegression = new Regression(data, true);
            result.r = totalRegression.RegressionResult;
            if (result.r == null || double.IsNaN(result.r.MS)) return null;

            result.MSE = totalRegression.RegressionResult.MSE;
            result.SSA = totalRegression.RegressionResult.MS;
            result.R2 = totalRegression.RegressionResult.MultipleR2;
            result.SSE = result.MSE * result.DFE;
            result.MSA = result.SSA / result.DFA;
            result.FA = result.MSA / result.MSE;
            result.pA = 1 - AQMath.FisherCD(result.FA, result.DFA, result.DFE) ?? double.NaN;
            result.Intercept = Math.Pow(totalRegression.RegressionResult.Intercept, 2) * totalRegression.RegressionInput.n;
            result.Fintercept = result.Intercept / result.MSE;
            result.Pintercept = 1 - AQMath.FisherCD(result.Fintercept, 1, result.DFE) ?? double.NaN;
            result.Levene = levene.Item1;
            result.pLevene = levene.Item2;
            return result;
        }


        public ANOVA(IEnumerable<DynamicRow> source)
        {
            _source = source;
        }

        public ANOVAResult GetANOVAResultForMeansByGroup(string a)
        {
            var glm = GetOneWayANOVAResult(_source, false);
            if (glm != null) return ANOVAResultForGLM(glm, a);

            var groups = (from l in _source group l by l.Group).ToList();
            var anova = GetANOVAResult(groups);
            var result = ANOVAResultForMeans(anova, a);
            return result;
        }

        public ANOVAResult GetANOVAResultForMeansByLayer(string a)
        {
            var glm = GetOneWayANOVAResult(_source, true);
            if (glm != null) return ANOVAResultForGLM(glm, a);

            var groups = (from l in _source group l by l.Z).ToList();
            var anova = GetANOVAResult(groups);
            var result = ANOVAResultForMeans(anova, a);
            return result;
        }

        private ANOVAResult ANOVAResultForMeans(GLM anova, string a)
        {
            if (anova == null || (anova.I + anova.K)<2) return null;
            return new ANOVAResult()
            {
                Comment = Math.Abs(anova.pA) >= 0.05
                    ? $"p(F)={Math.Round(Math.Abs(anova.pA), 3)}"
                    : $@"<red>p(F)={Math.Round(Math.Abs(anova.pA), 3)}</red>",
                ANOVATables = GetANOVATableForOneWay(anova, a)
            };
        }

        private List<SLDataTable> GetANOVATableForOneWay(GLM anova, string a)
        {
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = a,
                Header = anova.Method + ((!anova.Method.EndsWith("Фишера")) ? ", R²= " + Math.Round(anova.R2, 2) : string.Empty),
                ColumnNames = new List<string> { "Фактор", "SS", "DF", "MS", "F", "p", "#SS_Цвет", "#DF_Цвет", "#MS_Цвет", "#F_Цвет", "#p_Цвет" },
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string> { "String", "Double", "Double", "Double", "Double", "Double", "String", "String", "String", "String", "String" }
            };

            var icolor = anova.Pintercept < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    string.Empty,
                    Math.Round(anova.Intercept,5),
                    1.0,
                    Math.Round(anova.Intercept,5),
                    Math.Round(anova.Fintercept,1),
                    Math.Round(anova.Pintercept,5),
                    icolor,
                    icolor,
                    icolor,
                    icolor,
                    icolor
                }
            });

            var acolor = anova.pA < 0.05 ? "#ffff0000" : null;
            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    a,
                    Math.Round(anova.SSA,5),
                    Math.Round(anova.DFA,0),
                    Math.Round(anova.MSA,5),
                    Math.Round(anova.FA,1),
                    Math.Round(anova.pA,5),
                    acolor,
                    acolor,
                    acolor,
                    acolor,
                    acolor
                }
            });

            result.Table.Add(new SLDataRow
            {
                Row = new List<object>
                {
                    "Ошибка",
                    Math.Round(anova.SSE,5),
                    Math.Round(anova.DFE,0),
                    Math.Round(anova.MSE,5),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                }
            });
            return new List<SLDataTable> { result };
        }

        private ANOVAResult ANOVAResultForGLM(GLM anova, string A)
        {
            if (anova == null || (anova.I + anova.K) < 2) return null;
            var lc = GetLeveneGroupComment(anova.Levene, anova.pLevene, false); 
            var result = Math.Abs(anova.pA) >= 0.05
                ? $"p(F)={Math.Round(Math.Abs(anova.pA), 3)}"
                : $@"<red>p(F)={Math.Round(Math.Abs(anova.pA), 3)}</red>";
            result += ", R²=" + Math.Round(anova.R2, 2);
            result += lc;

            var r = new ANOVAResult { Comment = result, ANOVATables = GetANOVATableForOneWay(anova, A) };
            r.ANOVATables[0].Header += lc;
            return r;
        }

        private GLM GetANOVAResult(List<IGrouping<object, DynamicRow>> groups)
        {
            var anova = new GLM
            {
                I = groups.Count,
                N = _source.Count(),
                Intercept = AQMath.AggregateMean(_source.Select(a => a.Y).Cast<object>().ToList()).Value
            };
            anova.DFE = (double)(anova.N - anova.I);
            anova.DFA = (double)anova.I - 1;
            anova.Method = "Тест Фишера";
            var levene = LeveneTest(groups, false);
            foreach (var g in groups)
            {
                var aggregateMean = AQMath.AggregateMean(g.Select(a => a.Y).Cast<object>().ToList());
                if (!aggregateMean.HasValue) continue;

                anova.SSA += g.Count() * (aggregateMean.Value - anova.Intercept) * (aggregateMean.Value - anova.Intercept);
                var groupData = g.Select(a => a.Y).Cast<object>().ToList();
                anova.SSE += groupData.Sum(gd => ((double)gd - aggregateMean.Value) * ((double)gd - aggregateMean.Value));
            }
            anova.MSA = anova.SSA / anova.DFA;
            anova.MSE = anova.SSE / anova.DFE;
            if (!(Math.Abs(anova.MSE) > double.Epsilon)) return null;
            anova.Fintercept = anova.Intercept / anova.MSE;
            anova.Pintercept = 1 - AQMath.FisherCD(anova.Fintercept, 1, anova.DFE).Value;
            anova.FA = anova.MSA / anova.MSE;
            var p = AQMath.FisherCD(anova.FA, anova.DFA, anova.DFE);
            if (!p.HasValue || double.IsNaN(p.Value)) return null;
            anova.pA = 1 - p.Value;
            anova.Levene = levene.Item1;
            anova.pLevene = levene.Item2;
            return anova;
        }
        #endregion

        #region KW

        public ANOVAResult GetANOVAResultForKWByGroupAndLayer(string A, string B)
        {
            var sortedByY = _source.OrderBy(a => a.Y).ToList();
            var rawgroups = (from l in _source group l by l.Group).ToList();
            var rawlayers = (from l in _source group l by l.Z).ToList();

            var gLevene = LeveneTest(rawgroups, true);
            var zLevene = LeveneTest(rawlayers, true);
            var gLeveneComment = GetLeveneGroupComment(gLevene.Item1, gLevene.Item2, true);
            var zLeveneComment = GetLeveneGroupComment(zLevene.Item1, zLevene.Item2, true);

            var ranked = sortedByY.Select((current, i) => new RankOfValue { Value = current.Y, Group = current.Group, Rank = i + 1, Layer = current.Z }).ToList();
            var groups = (from l in ranked group l by l.Group).ToList();
            var layers = (from l in ranked group l by l.Layer).ToList();

            var rA = ANOVAResultForKw(ranked, groups, A);
            var anovaTables = new List<SLDataTable>();
            if (rA != null)
            {
                rA.ANOVATables[0].Header += gLeveneComment;
                anovaTables.AddRange(rA.ANOVATables);
            }
            var rB = ANOVAResultForKw(ranked, layers, B);
            if (rB != null)
            {
                rB.ANOVATables[0].Header += zLeveneComment;
                anovaTables.AddRange(rB.ANOVATables);
            }
            if (rA == null && rB == null) return null;
            var rAComment = A + ": " + rA?.Comment + gLeveneComment;
            var rBComment = B + ": " + rB?.Comment + zLeveneComment;
            return new ANOVAResult { Comment =  rAComment + (!string.IsNullOrEmpty(rBComment) ? ", " : string.Empty) + rBComment, ANOVATables = anovaTables };
        }

        public ANOVAResult GetANOVAResultForKWByGroup(string A)
        {
            var sortedByY = _source.OrderBy(a => a.Y).ToList();
            var ranked = sortedByY.Select((current, i) => new RankOfValue { Value = current.Y, Group = current.Group, Rank = i + 1, Layer = current.Z }).ToList();
            var groups = (from l in ranked group l by l.Group).ToList();

            var rawgroups = (from l in _source group l by l.Group).ToList();
            var gLevene = LeveneTest(rawgroups, true);
            var gLeveneComment = GetLeveneGroupComment(gLevene.Item1, gLevene.Item2, true);
            var result = ANOVAResultForKw(ranked, groups, A);
            if (result!=null)
            {
                result.Comment +=  gLeveneComment;
                result.ANOVATables[0].Header +=  gLeveneComment;
            }
            return result;
        }
        public ANOVAResult GetANOVAResultForKWByLayer(string B)
        {
            var sortedByY = _source.OrderBy(a => a.Y).ToList();
            var ranked = sortedByY.Select((current, i) => new RankOfValue { Value = current.Y, Group = current.Group, Rank = i + 1, Layer = current.Z }).ToList();
            var groups = (from l in ranked group l by l.Layer).ToList();


            var rawgroups = (from l in _source group l by l.Z).ToList();
            var zLevene = LeveneTest(rawgroups, true);
            var zLeveneComment = GetLeveneGroupComment(zLevene.Item1, zLevene.Item2, true);
            var result = ANOVAResultForKw(ranked, groups, B);
            if (result != null)
            {
                result.Comment += zLeveneComment;
                result.ANOVATables[0].Header += zLeveneComment;
            }
            return result; ;
        }

        private static ANOVAResult ANOVAResultForKw(List<RankOfValue> ranked, List<IGrouping<object, RankOfValue>> groups, string factor)
        {
            if (groups.Count < 2) return null;
            var tieSum = 0d;
            var n = ranked.Count;
            var tied = (from r in ranked group r by r.Value).Select(a=> new { Key = a.Key, Group = a.ToList() } ).ToList();
            var tieCount = new List<double>();

            foreach (var t in tied)
            {
                var toTie = t.Group;
                if (toTie.Count <= 1) continue;
                var tiedValue = AQMath.AggregateMean(toTie.Select(a => a.Rank).Cast<object>().ToList());
                if (!tiedValue.HasValue) continue;
                foreach (var tt in toTie) tt.Rank = tiedValue.Value;
                tieCount.Add(toTie.Count);
            }

            tieSum += tieCount.Sum(t => (t * t * t - t));
            var tieCorrection = 1 - tieSum / (n * n * n - n);


            var ks = (from g in groups let sum = g.Sum(a => a.Rank) select sum * sum / (double)g.Count()).Sum();

            if (groups.Count() <= 1) return null;
            var kw = ((12.0d / (n * (n + 1.0d))) * ks - 3.0d * (n + 1.0d)) / tieCorrection;
            var chiSquareCd = AQMath.ChiSquareCD(kw, groups.Count - 1);
            if (chiSquareCd == null) return null;
            var kWp = 1 - chiSquareCd.Value;
            if (double.IsNaN(kWp)) return null;
            return new ANOVAResult
            {
                Comment = Math.Abs(kWp) > 0.05
                    ? $"p(KW)={Math.Round(kWp, 3)}"
                    : $@"<red>p(KW)={Math.Round(kWp, 3)}</red>",
                ANOVATables = GetANOVATableForKW(kw, kWp, factor)
            };
        }

        private static List<SLDataTable> GetANOVATableForKW(double KW, double pKW, string factor)
        {
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = factor,
                Header = "Тест Краскела-Уоллиса",
                ColumnNames = new List<string> { "Фактор", "KW", "p", "#KW_Цвет", "#p_Цвет" },
                ColumnTags = new List<object>(),
                CustomProperties = new List<SLExtendedProperty>(),
                DataTypes = new List<string> { "String", "Double", "Double", "String", "String" }
            };
            var bcolor = pKW < 0.05 ? "#ffff0000" : null;
            var brow = new SLDataRow
            {
                Row = new List<object>
                {
                    factor,
                    Math.Round(KW,5),
                    Math.Round(pKW,5),
                    bcolor,
                    bcolor
                }
            };
            result.Table.Add(brow);
            return new List<SLDataTable> { result };
        }
        public class RankOfValue
        {
            public double? Value { get; set; }
            public double Rank { get; set; }
            public object Group { get; set; }
            public object Layer { get; set; }
        }
        #endregion

        #region Leven's test

        public static Tuple<double, double> LeveneTest(List<IGrouping<object, DynamicRow>> groups, bool median)
        {
            var ZiList = new List<List<double>>();
            double n = 0;
            double k = groups.Count;
            if (k==0)return new Tuple<double, double>(double.NaN, double.NaN);
            foreach (var g in groups)
            {
                var groupData = g.Select(a => a.Y).ToList();
                var Yi = median ? AQMath.AggregateMedian(groupData.OfType<object>().ToList()).Value :
                                  AQMath.AggregateMean(groupData.OfType<object>().ToList()).Value;

                var Zdata = g.Select(a => Math.Abs(a.Y.Value - Yi)).ToList();
                ZiList.Add(Zdata);
                n += groupData.Count;
            }

            var Z = AQMath.AggregateMean(ZiList.SelectMany(a => a).OfType<object>().ToList()).Value;

            double upperSum = 0, bottomSum = 0;
            foreach (var zdata in ZiList)
            {
                var Zi = AQMath.AggregateMean(zdata.OfType<object>().ToList()).Value;
                var zSum = zdata.Select(a => Math.Pow(a - Zi, 2)).Sum();
                var zN = zdata.Count * Math.Pow(Zi - Z, 2);
                upperSum += zN;
                bottomSum += zSum;
            }
            var levene = ((n - k) / (k - 1)) * upperSum / bottomSum;

            var p = 1 - AQMath.FisherCD(levene, k - 1, n - k).Value;
            return new Tuple<double, double>(levene, p);
        }

        private static string GetLeveneGroupComment(double Levene, double p, bool median)
        {
            if (double.IsNaN(Levene) || double.IsNaN(p)) return string.Empty;
            return median ? Math.Abs(p) > 0.05 ? $", BrF={Math.Round(Levene, 3)}, p={Math.Round(p, 3)}" : $@", <red>BrF={Math.Round(Levene, 3)}, p={Math.Round(p, 3)}</red>":
                            Math.Abs(p) > 0.05 ? $", Levene={Math.Round(Levene, 3)}, p={Math.Round(p, 3)}" : $@", <red>Levene={Math.Round(Levene, 3)}, p={Math.Round(p, 3)}</red>";
        }
        #endregion
    }
}