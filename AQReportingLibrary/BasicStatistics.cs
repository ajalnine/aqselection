﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;

namespace AQReportingLibrary
{
    public class BasicStatistics
    {
        private readonly SLDataTable _selectedTable;
        private readonly List<string> _selectedParameters;
        private readonly string _layerField;
        private readonly ReportChartLayerMode _layerMode;
        private readonly Statistics _statistics;
        private readonly BackgroundWorker _worker;

        public BasicStatistics(SLDataTable selectedTable, List<string> parameters, string layerField, Statistics statistics, ReportChartLayerMode layerMode, BackgroundWorker worker)
        {
            _selectedTable = selectedTable;
            _selectedParameters = parameters;
            _layerField = layerField;
            _layerMode = layerMode;
            _statistics = statistics;
            _worker = worker;
        }

        public List<SLDataTable> GetStatisticTables(bool removeEmpty)
        {
            var result = new List<SLDataTable>();

            if (_layerField ==null)
            {
                result.Add(GetSingleLayerStatistics(_selectedTable, "Статистика таблицы", false, null));
            }
            else
            {
                var layers = GetLayers();
                switch (_layerMode)
                {
                    case ReportChartLayerMode.Multiple:
                        foreach (var layer in layers)
                        {
                            var name = _layerField + (layer == null ? _layerField + ": нет значения" : ": " + layer);
                            result.Add(GetSingleLayerStatistics(_selectedTable.ExtractLayer(_layerField, layer), name, false, layer?.ToString()));
                        }
                        break;

                    case ReportChartLayerMode.Single:
                        CreateSingleTable(result, layers);

                        result[0].CustomProperties.Add(new SLExtendedProperty { Key = _layerField + "#@#CellCombined", Value = true });
                        result[0].TableName = "Статистика таблицы";
                        break;

                    case ReportChartLayerMode.Extended:
                        CreateSingleTable(result, layers);

                        var reordered = new List<SLDataRow>();
                        var rowtags = new List<object>();
                        for (int j = 0; j < _selectedParameters.Count; j++)
                        {
                            for (int i = 0; i < layers.Count; i++)
                            {
                                reordered.Add(result[0].Table[i * _selectedParameters.Count + j]);
                                rowtags.Add(result[0].RowTags[i * _selectedParameters.Count + j]);
                            }
                        }
                        result[0].Table = reordered;
                        result[0].RowTags = rowtags;
                        result[0].CustomProperties.Add(new SLExtendedProperty { Key = "Переменная" + "#@#CellCombined", Value = true });
                        result[0].TableName = "Статистика таблицы";
                        break;
                }
            }
            if (!removeEmpty) return result;

            foreach (var table in result)
            {
                var toDelete = new List<SLDataRow>();
                for (var i = 0; i < table.RowTags.Count; i++)
                {
                    if ((int)table.RowTags[i]==0)toDelete.Add(table.Table[i]);
                }
                foreach (var row in toDelete)
                {
                    table.Table.Remove(row);
                }
            }
            return result;
        }

        private void CreateSingleTable(List<SLDataTable> result, List<object> layers)
        {
            var count = 0;
            foreach (var layer in layers)
            {
                var name = _layerField + (layer == null ? _layerField + ": нет значения" : ": " + layer);
                var toAdd = GetSingleLayerStatistics(_selectedTable.ExtractLayer(_layerField, layer), name, true,
                    layer?.ToString());
                if (count == 0) result.Add(toAdd);
                else
                {
                    result[0].Table.AddRange(toAdd.Table);
                    result[0].RowTags.AddRange(toAdd.RowTags);
                }
                count++;
            }
        }

        private SLDataTable GetSingleLayerStatistics(SLDataTable source, string name, bool includeLayerField, string layer)
        {
            var result = new SLDataTable
            {
                Table = new List<SLDataRow>(),
                TableName = name,
                ColumnNames = new List<string> { "Переменная" },
                ColumnTags = new List<object> { null },
                CustomProperties = new List<SLExtendedProperty> { null },
                DataTypes = new List<string> { "String" },
                Tag = layer
            };

            var parametersData = new Dictionary<string, List<object>>();
            var statisticsData = new Dictionary<string, SLDataRow>();
            var meanData = new Dictionary<string, double?>();
            var stdevData = new Dictionary<string, double?>();
            var countData = new Dictionary<string, double?>();
            var minRangeData = new Dictionary<string, double?>();
            var maxRangeData = new Dictionary<string, double?>();

            foreach (var parameter in _selectedParameters)
            {
                var row = new SLDataRow { Row = new List<object>()};
                row.Row.Add(parameter);
                var data = source.GetColumnData(parameter);
                result.Table.Add(row);
                parametersData.Add(parameter, data);
                statisticsData.Add(parameter, row);
                if (_worker.CancellationPending) return null;
            }
            result.RowTags = new List<object>();
            foreach (var parameter in _selectedParameters)
            {
                var data = parametersData[parameter];
                result.RowTags.Add(AQMath.AggregateCount(data));
                if (_worker.CancellationPending) return null;
            }
            if (includeLayerField)
            {
                result.ColumnNames.Add(_layerField);
                result.DataTypes.Add("String");
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    row.Row.Add(layer);
                }
            }

            foreach (var parameter in _selectedParameters)
            {
                var data = parametersData[parameter];
                countData.Add(parameter, AQMath.AggregateCount(data));
                if (_worker.CancellationPending) return null;
            }

            if ((_statistics & Statistics.N) == Statistics.N)
            {
                result.ColumnNames.Add("Число наблюдений");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    row.Row.Add(countData[parameter]);
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Basic) == Statistics.Basic 
                || (_statistics & Statistics.Ranges) == Statistics.Ranges
                || (_statistics & Statistics.SixSigma) == Statistics.SixSigma
                || (_statistics & Statistics.Interval) == Statistics.Interval
                || (_statistics & Statistics.Probability) == Statistics.Probability 
                || (_statistics & Statistics.ProcessIndexes) == Statistics.ProcessIndexes)
            {
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    meanData.Add(parameter, AQMath.AggregateMean(data));
                    stdevData.Add(parameter, AQMath.AggregateStDev(data));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Basic) == Statistics.Basic)
            {
                result.ColumnNames.Add("Среднее");
                result.ColumnNames.Add("СКО");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(meanData[parameter], 5));
                    row.Row.Add(AQMath.SmartRound(stdevData[parameter], 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Dispersion) == Statistics.Dispersion)
            {
                result.ColumnNames.Add("Дисперсия");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregateVariance(data), 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Median) == Statistics.Median)
            {
                result.ColumnNames.Add("5%");
                result.ColumnNames.Add("10%");
                result.ColumnNames.Add("25%");
                result.ColumnNames.Add("Медиана");
                result.ColumnNames.Add("75%");
                result.ColumnNames.Add("90%");
                result.ColumnNames.Add("95%");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);

                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 5), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 10), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 25), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 50), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 75), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 90), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregatePercentileInc(data, 95), 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.MinMax) == Statistics.MinMax)
            {
                result.ColumnNames.Add("Минимум");
                result.ColumnNames.Add("Максимум");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregateMin(data), 5));
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregateMax(data), 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.ZTest) == Statistics.ZTest)
            {
                result.ColumnNames.Add("Статистика D Колмогорова - Смирнова");
                result.ColumnNames.Add("p(D)");
                result.ColumnNames.Add("#p(D)_Цвет");
                result.ColumnNames.Add("Статистика W Шапиро - Уилка");
                result.ColumnNames.Add("p(W)");
                result.ColumnNames.Add("#p(W)_Цвет");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];

                    row.Row.Add(AQMath.SmartRound(AQMath.KolmogorovD(data),3));
                    var pks = AQMath.KolmogorovP(data);
                    row.Row.Add(AQMath.SmartRound(pks, 3));
                    row.Row.Add(pks>0.05? null:"#ffff0000");
                    row.Row.Add(AQMath.SmartRound(AQMath.ShapiroWilkW(data), 3));
                    var pw = AQMath.ShapiroWilkP(data);
                    row.Row.Add(AQMath.SmartRound(pw, 3));
                    row.Row.Add(pw > 0.05 ? null : "#ffff0000");

                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Moment) == Statistics.Moment)
            {
                result.ColumnNames.Add("Асимметрия");
                result.ColumnNames.Add("Ошибка асимметрии");
                result.ColumnNames.Add("Отношение асимметрии к ошибке");
                result.ColumnNames.Add("#Отношение асимметрии к ошибке_Цвет");
                result.ColumnNames.Add("Эксцесс");
                result.ColumnNames.Add("Ошибка эксцесса");
                result.ColumnNames.Add("Отношение эксцесса к ошибке");
                result.ColumnNames.Add("#Отношение эксцесса к ошибке_Цвет");

                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");

                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);

                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    var y1 = AQMath.Skewness(data);
                    var yerr1 = AQMath.SkewnessStdErr(data);
                    var y2 = AQMath.Kurtosis(data);
                    var yerr2 = AQMath.KurtosisStdErr(data);

                    row.Row.Add(AQMath.SmartRound(y1, 4));
                    row.Row.Add(AQMath.SmartRound(yerr1, 4));
                    row.Row.Add(AQMath.SmartRound(y1 / yerr1, 4));
                    row.Row.Add(AQMath.Abs(y1 / yerr1) <= 3 ? null : "#ffff0000");
                    row.Row.Add(AQMath.SmartRound(y2, 4));
                    row.Row.Add(AQMath.SmartRound(yerr2, 4));
                    row.Row.Add(AQMath.SmartRound(y2 / yerr2, 4));
                    row.Row.Add(AQMath.Abs(y2 / yerr2) <= 3 ? null : "#ffff0000");
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Sum) == Statistics.Sum)
            {
                result.ColumnNames.Add("Сумма");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(AQMath.AggregateSum(data), 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.SixSigma) == Statistics.SixSigma)
            {
                result.ColumnNames.Add("-3σ");
                result.ColumnNames.Add("+3σ");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    row.Row.Add(AQMath.SmartRound(meanData[parameter] - 3 * stdevData[parameter], 5));
                    row.Row.Add(AQMath.SmartRound(meanData[parameter] + 3 * stdevData[parameter], 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Interval) == Statistics.Interval)
            {
                result.ColumnNames.Add("-95%");
                result.ColumnNames.Add("-90%");
                result.ColumnNames.Add("+90%");
                result.ColumnNames.Add("+95%");
                result.ColumnNames.Add("Квантиль 10%");
                result.ColumnNames.Add("Квантиль 5%");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    var n = countData[parameter];
                    var s = stdevData[parameter];
                    var x = meanData[parameter];
                    var k = AQMath.StudentTbyP(0.05, n);
                    var k2 = AQMath.StudentTbyP(0.10, n);
                    row.Row.Add(AQMath.SmartRound(x - k * s, 5));
                    row.Row.Add(AQMath.SmartRound(x - k2 * s, 5));
                    row.Row.Add(AQMath.SmartRound(x + k2 * s, 5));
                    row.Row.Add(AQMath.SmartRound(x + k* s, 5));
                    row.Row.Add(AQMath.SmartRound(k2, 5));
                    row.Row.Add(AQMath.SmartRound(k, 5));
                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Ranges) == Statistics.Ranges || (_statistics & Statistics.Probability) == Statistics.Probability || (_statistics & Statistics.ProcessIndexes) == Statistics.ProcessIndexes || (_statistics & Statistics.Percents) == Statistics.Percents)
            {
                foreach (var parameter in _selectedParameters)
                {
                    double? rangeMin = null, rangeMax = null;

                    if (source.ColumnNames.Contains("#" + parameter + "_НГ"))
                    {
                        var rm = source.GetColumnData("#" + parameter + "_НГ").OfType<double>();
                        var enumerable = rm as IList<double> ?? rm.ToList();
                        if (enumerable.Any())rangeMin = enumerable.Max();
                    }
                    else if (source.ColumnNames.Contains(parameter + "_НГ"))
                    {
                        var rm = source.GetColumnData(parameter + "_НГ").OfType<double>();
                        var enumerable = rm as IList<double> ?? rm.ToList();
                        if (enumerable.Any())rangeMin = enumerable.Max();
                    }

                    if (source.ColumnNames.Contains("#" + parameter + "_ВГ"))
                    {
                        var rm = source.GetColumnData("#" + parameter + "_ВГ").OfType<double>();
                        var enumerable = rm as IList<double> ?? rm.ToList();
                        if (enumerable.Any()) rangeMax = enumerable.Min();
                    }
                    else if (source.ColumnNames.Contains(parameter + "_ВГ"))
                    {
                        var rm = source.GetColumnData(parameter + "_ВГ").OfType<double>();
                        var enumerable = rm as IList<double> ?? rm.ToList();
                        if (enumerable.Any()) rangeMax = enumerable.Min();
                    }
                    minRangeData.Add(parameter, rangeMin);
                    maxRangeData.Add(parameter, rangeMax);
                }
            }
            
            if ((_statistics & Statistics.Ranges) == Statistics.Ranges)
            {
                result.ColumnNames.Add("Нижняя граница");
                result.DataTypes.Add("Double");
                result.ColumnNames.Add("Верхняя граница");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add("Editable");
                result.ColumnTags.Add("Editable");

                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    double? rangeMin = minRangeData[parameter], rangeMax = maxRangeData[parameter];

                    if (source.ColumnNames.Contains("#"+parameter + "_НГ"))
                    {
                        row.Row.Add(rangeMin);
                    }
                    else if(source.ColumnNames.Contains(parameter + "_НГ"))
                    {
                        row.Row.Add(rangeMin);
                    }
                    else
                    {
                        row.Row.Add(null);
                    }

                    if (source.ColumnNames.Contains("#" + parameter + "_ВГ"))
                    {
                        row.Row.Add(rangeMax);
                    }
                    else if (source.ColumnNames.Contains(parameter + "_ВГ"))
                    {
                        row.Row.Add(rangeMax);
                    }
                    else
                    {
                        row.Row.Add(null);
                    }
                }
            }

            if ((_statistics & Statistics.Probability) == Statistics.Probability)
            {
                result.ColumnNames.Add("Pнг");
                result.ColumnNames.Add("#Pнг_Цвет");
                result.ColumnNames.Add("Pвг");
                result.ColumnNames.Add("#Pвг_Цвет");
                result.ColumnNames.Add("P");
                result.ColumnNames.Add("#P_Цвет");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("String");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var row = statisticsData[parameter];
                    var plg = AQMath.NormalCD(minRangeData[parameter], meanData[parameter], stdevData[parameter]);
                    var pug = 1 - AQMath.NormalCD(maxRangeData[parameter], meanData[parameter], stdevData[parameter]);
                    var p = (!plg.HasValue && !pug.HasValue) ? null : (double?)((plg ?? 0) + (pug ?? 0));
                    row.Row.Add(AQMath.Round(plg, 3d));
                    row.Row.Add(plg < 0.05 || !plg.HasValue ? null : "#ffff0000");

                    row.Row.Add(AQMath.Round(pug, 3d));
                    row.Row.Add(pug <0.05 || !pug.HasValue ? null : "#ffff0000");

                    row.Row.Add(AQMath.Round(p, 3d));
                    row.Row.Add(p < 0.05 || !p.HasValue ? null : "#ffff0000");

                    if (_worker.CancellationPending) return null;
                }
            }
            
            if ((_statistics & Statistics.ProcessIndexes) == Statistics.ProcessIndexes)
            {
                result.ColumnNames.Add("Скользящая оценка отклонения");
                result.ColumnNames.Add("Cp");
                result.ColumnNames.Add("Cpk");
                result.ColumnNames.Add("Pp");
                result.ColumnNames.Add("Ppk");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);

                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];

                    var ppl = (meanData[parameter]  - minRangeData[parameter]) / (3 * stdevData[parameter]);
                    var ppu = (maxRangeData[parameter] - meanData[parameter]) / (3 * stdevData[parameter]);
                    var pp = (maxRangeData[parameter] - minRangeData[parameter])/(6 * stdevData[parameter]);
                    var ppk = ppl.HasValue && ppu.HasValue 
                        ? Math.Min(ppl.Value, ppu.Value)
                        : ppl.HasValue ? ppl : ppu;
                    var stdevEst = AQMath.AggregateStDevRangeEstimation(data);
                    var cpl = (meanData[parameter] - minRangeData[parameter]) / (3 * stdevEst);
                    var cpu = (maxRangeData[parameter] - meanData[parameter]) / (3 * stdevEst);
                    var cp = (maxRangeData[parameter] - minRangeData[parameter]) / (6 * stdevEst);
                    var cpk = cpl.HasValue && cpu.HasValue
                        ? Math.Min(cpl.Value, cpu.Value)
                        : cpl.HasValue ? cpl : cpu;

                    row.Row.Add(AQMath.Round(stdevEst, 3d));
                    row.Row.Add(AQMath.Round(cp, 3d));
                    row.Row.Add(AQMath.Round(cpk, 3d));
                    row.Row.Add(AQMath.Round(pp, 3d));
                    row.Row.Add(AQMath.Round(ppk, 3d));

                    if (_worker.CancellationPending) return null;
                }
            }

            if ((_statistics & Statistics.Percents) == Statistics.Percents)
            {
                result.ColumnNames.Add("Ниже НГ, %");
                result.ColumnNames.Add("Выше ВГ, %");
                result.DataTypes.Add("Double");
                result.DataTypes.Add("Double");
                result.ColumnTags.Add(null);
                result.ColumnTags.Add(null);
                foreach (var parameter in _selectedParameters)
                {
                    var data = parametersData[parameter];
                    var row = statisticsData[parameter];

                    var below = minRangeData[parameter].HasValue ? 100 - AQMath.PercentInRange(data, minRangeData[parameter], double.PositiveInfinity) : null;
                    var above = maxRangeData[parameter].HasValue ? 100 - AQMath.PercentInRange(data, double.NegativeInfinity, maxRangeData[parameter]) : null;

                    row.Row.Add(AQMath.Round(below, 3d));
                    row.Row.Add(AQMath.Round(above, 3d));

                    if (_worker.CancellationPending) return null;
                }
            }

            return result;
        }

        public List<object> GetLayers()
        {
            var l = _selectedTable.GetColumnData(_layerField).Distinct();
            var enumerable = l as IList<object> ?? l.ToList();
            var ordered = enumerable.Where(a=>!string.IsNullOrEmpty( a?.ToString())).OrderBy(a => a).ToList();
            var result = enumerable.Contains(null) ? (new List<object> { null }).Union(ordered).ToList() : ordered;
            return  result;
        }
    }
}