﻿using System;
using System.Collections.Generic;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public partial class Probability
    {
        private readonly SLDataTable _probabilityTable;
        private readonly List<object> _source;
        private readonly double? _mean, _stdev;
        public Probability(IEnumerable<DynamicRow> ldr)
        {
            var ldr1 = ldr.ToList();
            _source = ldr1.Select(a => (object)a.Y).ToList();
            var n = (double) ldr1.Count;
            _mean = AQMath.AggregateMean(_source);
            _stdev = AQMath.AggregateStDev(_source);
            var valuesorted = (from l in ldr1 orderby l.Y select l.Y).ToList();
            var f = new List<ProbabilityItem>();
            for (var i = 1; i <= n; i++ )
            {
                var d = valuesorted[i - 1];
                if (d != null) f.Add(new ProbabilityItem{Frequency = i/n, Value = d.Value});
            }

            var sorted = valuesorted.Select(a=> new { Frequency = (double)(valuesorted.IndexOf(a) + 1) / n, Value = a}).ToList();
            
            _probabilityTable = new SLDataTable
                {
                    ColumnNames = new List<string> { "Расчетная вероятность", "Наблюдаемая вероятность", "Комментарий" },
                    DataTypes = new List<string> {"Double", "Double", "String"},
                    TableName = "Вероятности",
                    Table = new List<SLDataRow>()
                };
            foreach (var group in (from s in sorted group s by s.Value))
            {
                var frequencies = group.Select(a => a.Frequency).Cast<object>().ToList();
                var observed = AQMath.AggregateMean(frequencies);
                var normal = AQMath.NormalCD(group.Key, _mean, _stdev);
                var comment = String.Format("{0} наблюдений значения {1}", frequencies.Count, group.Key);
                var sldr = new SLDataRow {Row = new List<object> {normal, observed, comment}};
                _probabilityTable.Table.Add(sldr);
            }
        }
    }
}
