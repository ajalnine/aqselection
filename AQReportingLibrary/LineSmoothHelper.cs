﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using AQChartLibrary;
using AQMathClasses;

namespace AQReportingLibrary
{
    public class LineSmoothHelper
    {
        public static LinearRegressionInfo GetLinearRegression(List<DynamicRow> ldr)
        {
            var lri = new LinearRegressionInfo();
            double sumX = 0, sumY = 0, sumX2 = 0, sumXy = 0, sumY2 = 0;
            double n = ldr.Count;
            var points = new List<Point>();

            foreach (DynamicRow l in ldr.Where((a => ((a.X != null || a.Xf != null) && a.Y != null))))
            {
                double x = l.X.HasValue ? l.X.Value : l.Xf.Value;

                sumX += x;
                sumY += l.Y.Value;
                sumX2 += x*x;
                sumXy += x*l.Y.Value;
                sumY2 += l.Y.Value*l.Y.Value;
                points.Add(new Point(x, l.Y.Value));
            }
            lri.Xmean = sumX/n;
            lri.Xstdev = AQMath.AggregateStDev((from p in points select (object)p.X).ToList());
            
            FillEquationLRI(ref lri, n, sumXy, sumX, sumY, sumX2, sumY2);
            FillErrorLRI(ref lri, points);
            
            return lri;
        }

        public static LinearRegressionInfo GetLinearRegressionXAndIndex(List<DynamicRow> ldr)
        {
            var lri = new LinearRegressionInfo();
            double sumX = 0, sumY = 0, sumX2 = 0, sumXy = 0, sumY2 = 0;
            double n = ldr.Count;
            var points = new List<Point>();

            foreach (DynamicRow l in ldr.Where((a => (a.X != null || a.Xf != null && a.Y != null))))
            {
                double x = l.Index / n + 0.5 / n;

                sumX += x;
                sumY += l.X.Value;
                sumX2 += x * x;
                sumXy += x * l.X.Value;
                sumY2 += l.X.Value * l.X.Value;
                points.Add(new Point(x, l.X.Value));
            }
            lri.Xmean = sumX / n;
            lri.Xstdev = AQMath.AggregateStDev((from p in points select (object) p.X).ToList());

            FillEquationLRI(ref lri, n, sumXy, sumX, sumY, sumX2, sumY2);
            FillErrorLRI(ref lri, points);

            return lri;
        }

        public static LinearRegressionInfo GetLinearRegressionYAndIndex(List<DynamicRow> LDR)
        {
            var lri = new LinearRegressionInfo();
            double sumX = 0, sumY = 0, sumX2 = 0, sumXy = 0, sumY2 = 0;
            double n = LDR.Count;
            var points = new List<Point>();

            foreach (DynamicRow l in LDR.Where((a => ((a.X != null || a.Xf != null) && a.Y != null))))
            {
                double x = l.Index /n + 0.5/n;

                sumX += x;
                sumY += l.Y.Value;
                sumX2 += x * x;
                sumXy += x * l.Y.Value;
                sumY2 += l.Y.Value * l.Y.Value;
                points.Add(new Point(x, l.Y.Value));
            }
            lri.Xmean = sumX / n;
            lri.Xstdev = AQMath.AggregateStDev((from p in points select (object)p.X).ToList());
            
            FillEquationLRI(ref lri, n, sumXy, sumX, sumY, sumX2, sumY2);
            FillErrorLRI(ref lri, points);

            return lri;
        }

        private static void FillEquationLRI(ref LinearRegressionInfo lri, double n, double sumXy, double sumX, double sumY,
                                                   double sumX2, double sumY2)
        {
            lri.n = n;
            lri.k = (n*sumXy - sumX*sumY)/(n*sumX2 - sumX*sumX);
            lri.b = sumY/n - lri.k*sumX/n;
            lri.r = (n*sumXy - sumX*sumY)/(Math.Sqrt((n*sumX2 - sumX*sumX)*(n*sumY2 - sumY*sumY)));
            lri.r2 = lri.r*lri.r;
            lri.rcorrected2 = 1 - (1 - lri.r2) * (n - 1) / (n - 2);
            double? t = lri.r.Value*Math.Sqrt(n - 2)/Math.Sqrt((1 - lri.r*lri.r).Value);

            lri.p = (t <= 0) ? 2*AQMath.StudentCD(t, n - 2) : 2*(1 - AQMath.StudentCD(t, n - 2));
        }

        private static void FillErrorLRI(ref LinearRegressionInfo lri, List<Point> points)
        {
            var sum = 0.0d;
            foreach (var p in points)
            {
                sum += Math.Pow(lri.b.Value + lri.k.Value*p.X - p.Y,2);
            }

            lri.StdError = Math.Sqrt(sum/(lri.n.Value - 2));
            lri.Student95 = AQMath.StudentCD(0.05, lri.n - 2);
        }
    }

    public class LinearRegressionInfo
    {
        public double? n { get; set; }
        public double? k { get; set; }
        public double? b { get; set; }
        public double? Student95 { get; set; }
        public double? Xmean { get; set; }
        public double? Xstdev { get; set; }
        public double? r { get; set; }
        public double? r2 { get; set; }
        public double? rcorrected2 { get; set; }
        public double? p { get; set; }
        public double? StdError { get; set; }
        public string AttachedSeries { get; set; }
        public string LineColor { get; set; }
        public double LineSize { get; set; }

        public string GetRegressionComment(bool showN)
        {
            var result = showN ? "n=" + n : string.Empty;
            if (r.HasValue && double.IsNaN(r.Value)) return result;
            result += ((r.HasValue) 
                ? (Math.Abs(r.Value) < 0.4 || double.IsNaN(r.Value))
                    ? (", R=" + Math.Round(r.Value, 2))
                    : (@", <red>R=" + Math.Round(r.Value, 2) + @"</red>") 
                : String.Empty);
            result += ((r2.HasValue)
                ? (r2 < 0.16 || double.IsNaN(r2.Value)) 
                    ? (", R²=" + Math.Round(r2.Value, 2))
                    : (@", <red>R²=" + Math.Round(r2.Value, 2) + @"</red>") 
                : String.Empty);

            result += ((p.HasValue) 
                ?(p.Value > 0.05 || double.IsNaN(p.Value) 
                    ? (", p=" + Math.Round(p.Value, 2))
                    : (@", <red>p=" + Math.Round(p.Value, 2) + @"</red>")) 
                : String.Empty);

            result +=((StdError.HasValue) ? (", SE=" + Math.Round(StdError.Value, 4)) : String.Empty);

            result += GetLinearEquation();
            return result;
        }

        private string GetLinearEquation()
        {
            var result = string.Empty;
            if (!b.HasValue || !k.HasValue)return result;
            if (Math.Abs(b.Value) > 1e-15) result += ", y = " + Math.Round(b.Value, 5) + (k.Value < 0 ? " - " : " + ");
            else result += ", y = " + (k.Value < 0 ? " - " : String.Empty);
            if (Math.Abs(Math.Abs(k.Value) - 1) > 1e-15) result += Math.Round(Math.Abs(k.Value), 5) + " ∙ x";
            else result += "x";
            return result;
        }

        public MathFunctionDescription SetupLine(string attachedSeries, string lineColor, double lineSize, object colorByZ, string seriesName)
        {
            AttachedSeries = attachedSeries;
            LineColor = lineColor;
            LineSize = lineSize;
            return SetupLine(colorByZ, seriesName);
        }

        public MathFunctionDescription SetupPredictionLine(string attachedSeries, string lineColor, double lineSize, double coefficient, bool prediction, object colorByZ, string seriesName)
        {
            AttachedSeries = attachedSeries;
            LineColor = lineColor;
            LineSize = lineSize;
            return SetupPredictionLine(coefficient, prediction, colorByZ, seriesName);
        }

        public MathFunctionDescription SetupLine(double lineSize, object colorByZ, string seriesName)
        {
            LineSize = lineSize;
            return SetupLine(colorByZ, seriesName);
        }

        public MathFunctionDescription SetupLine(object colorByZ, string seriesName)
        {
            if (!b.HasValue || !k.HasValue) return null;
            if (LineColor == null && colorByZ==null) LineColor = "#ffff4000";

            if (LineSize <1e-15) LineSize = 2;
            var result = new MathFunctionDescription
            {
                LineSize = LineSize,
                PixelStep = 4,
                LineColor = LineColor,
                ColorByZScale = colorByZ,
                ZValueSeriesTitle = seriesName,
                Description = (colorByZ!=null ? "#" : string.Empty) + "Регрессия",
                MathFunctionCalculator = (cri, X) =>
                {
                    double x = GetTransformedX(cri, X);
                    var y = b.Value + k.Value*x;
                    return GetTransformedY(cri, y);
                }
            };

            return result;
        }

        public MathFunctionDescription SetupPredictionLine(double coefficient, bool confidence, double lineSize, object colorByZ, string seriesName)
        {
            LineSize = lineSize;
            return SetupPredictionLine(coefficient, confidence, colorByZ, seriesName);
        }

        public MathFunctionDescription SetupPredictionLine(double coefficient, bool confidence, object colorByZ, string seriesName)
        {

            var color = LineColor ?? "#ffff4000";
            var result = new MathFunctionDescription { LineSize = LineSize * 0.75, PixelStep = 4, LineColor = "#" + (confidence? "A0" : "40") + color.Substring(3,6), ColorByZScale = colorByZ, ZValueSeriesTitle = seriesName};

            result.Description = (colorByZ != null ? "#" : string.Empty) + (coefficient <0 ? "-95%":"+95%");

            result.Description += confidence ? " линия" : " прогноз";

            result.MathFunctionCalculator = (cri, X) =>
            {
                double x;
                x = GetTransformedX(cri, X);

                var y = b.Value + k.Value * x + coefficient * AQMath.StudentTbyP(0.025, n-2) * StdError * Math.Sqrt((confidence ? 0 : 1) + 1 / n.Value + (x - Xmean.Value) * (x - Xmean.Value) / ((n.Value - 1) * Xstdev.Value * Xstdev.Value));
                
                return y==null ? double.NaN: GetTransformedY(cri, y.Value);
            };
            return result;
        }

        public MathFunctionDescription SetupXEqualYLine()
        {
            var color =  "#f0808080";
            var result = new MathFunctionDescription
            {
                LineSize = 0.5,
                PixelStep = 4,
                LineColor = color,
                Description = "Y = X",
                MathFunctionCalculator = (cri, X) => GetTransformedY(cri, GetTransformedX(cri, X))
            };
            return result;
        }

        private double GetTransformedY(ChartRangesInfo cri, double y)
        {
            try
            {
                if (cri.YLeftSeries.Contains(AttachedSeries) || AttachedSeries == null)
                    return (y - (double)cri.YLeftMin) / ((double)cri.YLeftMax - (double)cri.YLeftMin);
                return (y - (double)cri.YRightMin) / ((double)cri.YRightMax - (double)cri.YRightMin);
            }
            catch
            {
                return double.NaN;
            }
        }

        private double GetTransformedX(ChartRangesInfo cri, double X)
        {
            double x;
            if (cri.XDownSeries.Contains(AttachedSeries) || AttachedSeries == null)
            {
                if (cri.XDownMin is DateTime)
                {
                    x = ((((DateTime) cri.XDownMin).Ticks +
                          (((DateTime) cri.XDownMax).Ticks - ((DateTime) cri.XDownMin).Ticks)*X))/1e18;
                }
                else
                {
                    if (cri.XDownMax == null)
                    {
                        x = X;
                    }
                    else
                    {
                        x = ((double) cri.XDownMin + ((double) cri.XDownMax - (double) cri.XDownMin)*X);
                    }
                }
            }
            else
            {
                if (cri.XUpMin is DateTime)
                {
                    x = ((((DateTime) cri.XUpMin).Ticks +
                          (((DateTime) cri.XUpMax).Ticks - ((DateTime) cri.XUpMin).Ticks)*X))/1e18;
                }
                else
                {
                    if (cri.XUpMax == null)
                    {
                        x = X;
                    }
                    else
                    {
                        x = ((double) cri.XUpMin + ((double) cri.XUpMax - (double) cri.XUpMin)*X);
                    }
                }
            }
            return x;
        }
    }
}