﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQReportingLibrary
{
    public static class FilteringExtentions
    {
        public static void StartFiltering(this SLDataTable table)
        {
            table.UnfilteredTable = new List<SLDataRow>();
            foreach (var row in table.Table)
            {
                table.UnfilteredTable.Add(row);
            }

            table.Filters = new List<SLDataFilter>();
            table.IsFiltered = true;
        }

        public static void StopFiltering(this SLDataTable table)
        {
            if (!table.IsFiltered) return;
            table.Table = new List<SLDataRow>();
            table.Filters = null;
            table.IsFiltered = false;
            if (table.UnfilteredTable == null) return;

            foreach (var row in table.UnfilteredTable)
            {
                table.Table.Add(row);
            }
            table.UnfilteredTable = null;
        }

        public static void ApplyFilters(this SLDataTable table)
        {
            if (!table.IsFiltered) return;
            if (table.Filters == null || table.Filters.Count == 0) StopFiltering(table);
            else
            {
                var rows = table.Filters.Aggregate(table.UnfilteredTable, table.GetFilteredRows);
                table.Table = rows;
            }
        }

        public static void NewFilter(this SLDataTable table, string columnName, List<object> filter)
        {
            if (!table.IsFiltered) table.StartFiltering();
            var v = table.GetDistinctWithNulls(columnName);
            var f = table.Filters.SingleOrDefault(a => a.ColumnName == columnName);
            if (f != null) table.Filters.Remove(f);
            table.Filters.Add(new SLDataFilter { ColumnName = columnName, FilterItems = v.Select(a => new SLDataFilterItem { Caption = a?.ToString() ?? " Нет значения", Included = filter.Contains(a), Value = a }).ToList() });
        }

        public static void StopFilter(this SLDataTable table, string columnName)
        {
            if (!table.IsFiltered) return;

            var f = table.Filters.SingleOrDefault(a => a.ColumnName == columnName);
            if (f != null) table.Filters.Remove(f);
            if (table.Filters.Count == 0) table.StopFiltering();
        }

        public static void OrderByAsc(this SLDataTable table, string columnName)
        {
            var dataType = table.GetDataType(columnName);
            List<SLDataRow> temp;
            var source = table.IsFiltered ? table.UnfilteredTable : table.Table;
            var n = table.ColumnNames.IndexOf(columnName);

            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    temp = source.OrderBy(a => a.Row[n] is string ? a.Row[n] : string.Empty).ToList();
                    break;

                case "system.bool":
                case "bool":
                    temp = source.OrderBy(a => a.Row[n] is bool ? a.Row[n] : false).ToList();
                    break;

                case "system.datetime":
                case "datetime":
                    temp = source.OrderBy(a => a.Row[n] is DateTime ? a.Row[n] : DateTime.MinValue).ToList();
                    break;

                case "system.timespan":
                case "timespan":
                    temp = source.OrderBy(a => a.Row[n] is TimeSpan ? a.Row[n] : TimeSpan.MinValue).ToList();
                    break;

                case "system.double":
                case "double":
                    temp = source.OrderBy(a => a.Row[n] is double ? a.Row[n] : double.MinValue).ToList();
                    break;

                default:
                    temp = source.OrderBy(a => a.Row[n] is int ? a.Row[n] : int.MinValue).ToList();
                    break;
            }
            if (table.IsFiltered) table.UnfilteredTable = temp;
            else table.Table = temp;
            ApplyFilters(table);
        }

        public static void OrderByDesc(this SLDataTable table, string columnName)
        {
            var dataType = table.GetDataType(columnName);
            List<SLDataRow> temp;
            var source = table.IsFiltered ? table.UnfilteredTable : table.Table;
            var n = table.ColumnNames.IndexOf(columnName);

            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    temp = source.OrderByDescending(a => a.Row[n] is string ? a.Row[n] : string.Empty).ToList();
                    break;

                case "system.bool":
                case "bool":
                    temp = source.OrderByDescending(a => a.Row[n] is bool ? a.Row[n] : false).ToList();
                    break;

                case "system.datetime":
                case "datetime":
                    temp = source.OrderByDescending(a => a.Row[n] is DateTime ? a.Row[n] : DateTime.MinValue).ToList();
                    break;

                case "system.timespan":
                case "timespan":
                    temp = source.OrderByDescending(a => a.Row[n] is TimeSpan ? a.Row[n] : TimeSpan.MinValue).ToList();
                    break;

                case "system.double":
                case "double":
                    temp = source.OrderByDescending(a => a.Row[n] is double ? a.Row[n] : double.MinValue).ToList();
                    break;

                default:
                    temp = source.OrderByDescending(a => a.Row[n] is int ? a.Row[n] : int.MinValue).ToList();
                    break;
            }
            if (table.IsFiltered) table.UnfilteredTable = temp;
            else table.Table = temp;
            ApplyFilters(table);
        }

        private static List<SLDataRow> GetFilteredRows(this SLDataTable sldt, List<SLDataRow> unfilteredRows, SLDataFilter filter)
        {
            var f = sldt.ColumnNames.IndexOf(filter.ColumnName);
            var filterData = filter.FilterItems.Where(a => a.Included).Select(a => a.Value).ToList();
            return unfilteredRows.Where(r => filterData.Contains(r.Row[f])).ToList();
        }

        public static void RenameFilter(this SLDataTable table, string oldName, string newName)
        {
            var f = table.Filters?.SingleOrDefault(a => a.ColumnName == oldName);
            if (f != null) f.ColumnName = newName;
        }

        public static void ChangeFilterValues(this SLDataTable table, string fieldToRename, object oldValue, object newValue)
        {
            if (!table.IsFiltered) return;
            var filtered = table.Filters.SingleOrDefault(a => a.ColumnName == fieldToRename);
            if (filtered == null) return;

            var dataType = table.GetDataType(fieldToRename);
            IEnumerable<SLDataFilterItem> source;
            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    source = filtered.FilterItems.Where(f => f.Value is string && (string) f.Value == (string) oldValue);
                    break;

                case "system.bool":
                case "bool":
                    source = filtered.FilterItems.Where(f => f.Value is bool && (bool) f.Value == (bool) oldValue);
                    break;

                case "system.datetime":
                case "datetime":
                    source = filtered.FilterItems.Where(f => f.Value is DateTime && (DateTime) f.Value == (DateTime) oldValue);
                    break;

                case "system.timespan":
                case "timespan":
                    source = filtered.FilterItems.Where(f => f.Value is TimeSpan && (TimeSpan) f.Value == (TimeSpan) oldValue);
                    break;

                case "system.double":
                case "double":
                    source = filtered.FilterItems.Where( f => f.Value is double && Math.Abs((double) f.Value - (double) oldValue) < double.Epsilon);
                    break;

                default:
                    source = filtered.FilterItems.Where(f => f.Value is int && (int)f.Value == (int)oldValue);
                    break;
            }
            foreach (var f in source)
            {
                f.Value = newValue;
                f.Caption = f.Value?.ToString() ?? " Нет значения";
            }
            table.DoFilterDistinct(fieldToRename);
        }

        public static void DoFilterDistinct(this SLDataTable sldt, string field)
        {
            if (!sldt.IsFiltered || sldt.Filters == null || !sldt.Filters.Any()) return;
            var filtered = sldt.Filters.SingleOrDefault(a => a.ColumnName == field);
            if (filtered == null) return;
            var dataType = sldt.GetDataType(field);
            var source = filtered.FilterItems;
            var resulted = new List<SLDataFilterItem>();
            foreach (var f in source)
            {
                if (!CheckExisting(resulted, f.Value, dataType))resulted.Add(f);
            }
            filtered.FilterItems = resulted;
        }

        private static bool CheckExisting(IEnumerable<SLDataFilterItem> filter, object value, string dataType)
        {
            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    return filter.Any(f => f.Value is string && (string)f.Value == (string)value);
                    
                case "system.bool":
                case "bool":
                    return filter.Any(f => f.Value is bool && (bool)f.Value == (bool)value);
                    
                case "system.datetime":
                case "datetime":
                    return filter.Any(f => f.Value is DateTime && (DateTime)f.Value == (DateTime)value);
                    
                case "system.timespan":
                case "timespan":
                    return filter.Any(f => f.Value is TimeSpan && (TimeSpan)f.Value == (TimeSpan)value);

                case "system.double":
                case "double":
                    return filter.Any(f => f.Value is double && Math.Abs((double)f.Value - (double)value) < double.Epsilon);

                default:
                    return filter.Any(f => f.Value is int && (int)f.Value == (int)value);
             }
        }

        public static void StoreFilter(this SLDataTable table, List<SLDataFilterItem> data, string columnName, int totalValuesCount)
        {
            if (data.All(a => a.Included) && totalValuesCount == data.Count)
            {
                if (table.Filters.Any(a => a.ColumnName == columnName))
                    table.Filters.Remove(table.Filters.Single(a => a.ColumnName == columnName));
            }
            else
            {
                if (table.Filters.All(a => a.ColumnName != columnName))
                    table.Filters.Add(new SLDataFilter
                    {
                        FilterItems = data,
                        ColumnName = columnName
                    });
                else table.Filters.Single(a => a.ColumnName == columnName).FilterItems = data;
            }
        }

        public static List<SLDataFilterItem> CreateFilter(this SLDataTable table, string columnName)
        {
            var sourcePrefilteredData = table.GetPrefilteredBeforeCurrentFilters(columnName).Distinct().ToList();
            var sourceDistinctData = table.GetDistinctWithNulls(columnName).ToList();
            var data = sourcePrefilteredData.Select(a => new SLDataFilterItem { Value = a, Caption = string.IsNullOrEmpty(a?.ToString()) ? " Нет значения" : a.ToString(), Included = sourceDistinctData.Contains(a) }).ToList();
            return table.OrderFilter(columnName, data);
        }

        public static List<SLDataFilterItem> OrderFilter(this SLDataTable table, string columnName, List<SLDataFilterItem> filter)
        {
            var dataType = table.GetDataType(columnName);
            List<SLDataFilterItem> temp;
            var n = table.ColumnNames.IndexOf(columnName);
            if (dataType == null || n < 0) return filter;

            switch (dataType.ToLower())
            {
                case "system.string":
                case "string":
                    temp = filter.OrderBy(a => a.Value is string ? a.Value : string.Empty).ToList();
                    break;

                case "system.bool":
                case "bool":
                    temp = filter.OrderBy(a => a.Value is bool ? a.Value : false).ToList();
                    break;

                case "system.datetime":
                case "datetime":
                    temp = filter.OrderBy(a => a.Value is DateTime ? a.Value : DateTime.MinValue).ToList();
                    break;

                case "system.timespan":
                case "timespan":
                    temp = filter.OrderBy(a => a.Value is TimeSpan ? a.Value : TimeSpan.MinValue).ToList();
                    break;

                case "system.double":
                case "double":
                    temp = filter.OrderBy(a => a.Value is double ? a.Value : double.MinValue).ToList();
                    break;

                default:
                    temp = filter.OrderBy(a => a.Value is int ? a.Value : int.MinValue).ToList();
                    break;
            }
            return temp;
        }

        public static List<object> GetPrefilteredBeforeCurrentFilters(this SLDataTable table, string columnName)
        {
            if (!table.IsFiltered || table.Filters == null || table.Filters.Count == 0) return table.GetDistinctWithNulls(columnName).ToList();

            var rows = table.UnfilteredTable;
            var i = table.ColumnNames.IndexOf(columnName);
            if (i==-1) return new List<object>();
            var exclusion = (columnName.StartsWith("#") && columnName.EndsWith("_Цвет"))
                ? new[] {columnName, columnName.Substring(1, columnName.Length - 6)}  // "_Цвет".Length - 1
                : new[] {columnName, "#" + columnName + "_Цвет"};

            rows = table.Filters.Where(a => !exclusion.Contains(a.ColumnName)).Aggregate(rows, table.GetDataFiltered);
            return rows.Select(a => a.Row[i]).ToList();
        }

        private static List<SLDataRow> GetDataFiltered(this SLDataTable table, List<SLDataRow> rows, SLDataFilter filter)
        {
            var i = table.ColumnNames.IndexOf(filter.ColumnName);
            var filterData = filter.FilterItems.Where(a => a.Included).Select(a => a.Value).ToList();
            return rows.Where(r => filterData.Contains(r.Row[i])).ToList();
        }

        public static void RemoveFilter(this SLDataTable table, string columnName)
        {
            table.Filters.Remove(table.Filters.SingleOrDefault(a => a.ColumnName == columnName));
        }
    }
}
