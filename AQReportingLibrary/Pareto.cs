﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public class Pareto
    {
        private readonly List<GroupValuePair> _allData;
        private List<GroupValuePair> _data;
        private SLDataTable _chartParetoTable;
        private readonly ReportChartHistogramParameters _p;

        private delegate IEnumerable<GroupValuePair> GroupCalcDelegate(IEnumerable<GroupValuePair> data, int groupNumber);
        private delegate double CalcDelegate(IEnumerable<GroupValuePair> data, List<string> bestGroups );

        private bool _isNominal;
        private readonly Dictionary<Aggregate, string> _parameterNames = new Dictionary<Aggregate, string>
        {
            {Aggregate.Single,   "Значения"},
            {Aggregate.N,        "Процент от числа наблюдений"},
            {Aggregate.Percent,  "Процент наблюдений"},
            {Aggregate.PercentInCategory,  "Процент наблюдений"},
            {Aggregate.Sum,      "Процент от суммы"},
            {Aggregate.Min,      "Процент от суммы минимумов"},
            {Aggregate.Max,      "Процент от суммы максимумов"},
            {Aggregate.Mean,     "Процент от суммы средних"},
            {Aggregate.Median,   "Процент от суммы медиан"},
            {Aggregate.First,    "Процент от суммы первых значений"},
            {Aggregate.Last,     "Процент от суммы последних значений"},
            {Aggregate.StDev,    "Процент от суммы СКО"}
        };

        private readonly Dictionary<Aggregate, string> _groupParameterNames = new Dictionary<Aggregate, string>
        {
            {Aggregate.Single,   "Значения"},
            {Aggregate.N,        "Число наблюдений"},
            {Aggregate.Percent,  "Процент наблюдений"},
            {Aggregate.PercentInCategory,  "Процент наблюдений"},
            {Aggregate.Sum,      "Сумма значений"},
            {Aggregate.Min,      "Минимум"},
            {Aggregate.Max,      "Максимум"},
            {Aggregate.Mean,     "Среднее"},
            {Aggregate.Median,   "Медиана"},
            {Aggregate.First,    "Первое значение"},
            {Aggregate.Last,     "Последнее значение"},
            {Aggregate.StDev,    "СКО"}
        };

        private readonly Dictionary<Aggregate, GroupCalcDelegate> _groupCalculators = new Dictionary<Aggregate, GroupCalcDelegate>
        {
            {Aggregate.Single,   (data, groupNumber)=> data.Distinct().OrderByDescending(g => g.Value).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Name, Value = g.Value }).ToList()},
            {Aggregate.N,        (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Count() }).ToList()},
            {Aggregate.Percent,  (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Count() }).ToList()},
            {Aggregate.PercentInCategory,  (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Count()).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Count() }).ToList()},
            {Aggregate.Sum,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Sum(a => a.Value)).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Sum(a => a.Value) }).ToList()},
            {Aggregate.Min,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Min(a => a.Value)).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Min(a => a.Value) }).ToList()},
            {Aggregate.Max,      (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Max(a => a.Value)).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Max(a => a.Value) }).ToList()},
            {Aggregate.Mean,     (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateMean(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = AQMath.AggregateMean(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()) }).ToList()},
            {Aggregate.Median,   (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateMedian(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = AQMath.AggregateMedian(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()) }).ToList()},
            {Aggregate.First,    (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.First().Value).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.First().Value }).ToList()},
            {Aggregate.Last,     (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => g.Last().Value).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = g.Last().Value }).ToList()},
            {Aggregate.StDev,    (data, groupNumber)=> (from l in data group l by l.Name).OrderByDescending(g => AQMath.AggregateStDev(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList())).Take(groupNumber).Select(g => new GroupValuePair { Name = g.Key.ToString(CultureInfo.InvariantCulture), Value = AQMath.AggregateStDev(g.Where(a => a.Value.HasValue).Select(a => a.Value.Value).Cast<object>().ToList()) }).ToList()}
        };

        private readonly Dictionary<Aggregate, CalcDelegate> _calculators = new Dictionary<Aggregate, CalcDelegate>
        {
            {Aggregate.Single,   (data, bestGroups) => double.NaN},
            {Aggregate.N,        (data, bestGroups) => (from l in data where !bestGroups.Contains(l.Name) select l.Value).Count()},
            {Aggregate.Percent,  (data, bestGroups) => (from l in data where !bestGroups.Contains(l.Name) select l.Value).Count()},
            {Aggregate.PercentInCategory,  (data, bestGroups) => (from l in data where !bestGroups.Contains(l.Name) select l.Value).Count()},
            {Aggregate.Sum,      (data, bestGroups) => (from l in data where !bestGroups.Contains(l.Name) select l.Value.Value).Sum()},
            {Aggregate.Min, (data, bestGroups) =>
            {
                var a = (from l in data where !bestGroups.Contains(l.Name) select l.Value.Value).ToList();
                return a.Any() ? a.Min() : double.NaN;
            }},
            {Aggregate.Max, (data, bestGroups) =>
            {
                var a = (from l in data where !bestGroups.Contains(l.Name) select l.Value.Value).ToList();
                return a.Any() ? a.Max() : double.NaN;
            }},
            {Aggregate.Mean, (data, bestGroups) =>
            {
                var a = AQMath.AggregateMean((from l in data where !bestGroups.Contains(l.Name) select l.Value).Cast<object>().ToList());
                return a == null ? double.NaN : a.Value;
            }},
            {Aggregate.Median,   (data, bestGroups) => 
            {
                var a = AQMath.AggregateMedian((from l in data where !bestGroups.Contains(l.Name) select l.Value).Cast<object>().ToList());
                return a == null ? double.NaN : a.Value;
            }},
            {Aggregate.First,    (data, bestGroups) => 
            {
                var a = (from l in data where !bestGroups.Contains(l.Name) select l.Value.Value).ToList();
                return a.Any() ? a.First() : double.NaN;
            }},
            {Aggregate.Last,     (data, bestGroups) => 
            {
                var a = (from l in data where !bestGroups.Contains(l.Name) select l.Value.Value).ToList();
                return a.Any() ? a.Last() : double.NaN;
            }},
            {Aggregate.StDev,    (data, bestGroups) => 
            {
                var a = AQMath.AggregateStDev((from l in data where !bestGroups.Contains(l.Name) select l.Value).Cast<object>().ToList());
                return a == null ? double.NaN : a.Value;
            }}
        };

        public Pareto(IEnumerable<DynamicRow> ldr, ReportChartHistogramParameters p)
        {
            _allData = ldr.Where(a => a != null && a.Group!=null).Select(a => new GroupValuePair { Name = a.Group.ToString(), Value = a.Y }).ToList();
            _data = ldr.All(b=>!b.Y.HasValue)
                ?  _allData 
                :  _allData.Where(a => a.Value.HasValue).ToList();
            _p = p;
            BuildParetoData();
        }

       #region Заполнение таблицы с bin
        private void BuildParetoData()
        {
            CreateChartParetoTableHeaders();
            CreateBinData();
        }

        private void CreateChartParetoTableHeaders()
        {
            _chartParetoTable = new SLDataTable
            {
                TableName = "Парето",
                Table = new List<SLDataRow>(),
                DataTypes = (new[] { "String", "Double", "Double", "Double", "String", "String" }).ToList(),
                ColumnNames = (new[] {_p.Fields[0], "Значение", "%", "% накопления", "Статистика", "Подписи"}).ToList()
            };
        }

        private string GetTextLabel(double bincount, double binpercent)
        {
            string textLabel = string.Empty;
            if (bincount != 0)
            {
                if ((_p.LabelsType & ReportChartColumnsLabelsType.N) == ReportChartColumnsLabelsType.N) textLabel += AQMath.SmartRound(bincount, 3);
                if ((_p.LabelsType & ReportChartColumnsLabelsType.Percent) == ReportChartColumnsLabelsType.Percent)
                {
                    if (!string.IsNullOrEmpty(textLabel)) textLabel += ", ";
                    textLabel += Math.Round(binpercent, 2) + " %";
                }
            }
            return textLabel;
        }


        private void CreateBinData()
        {
            var groups = _groupCalculators[_p.Aggregate](_data, _p.GroupNumber.Value);
            var parameterName = _parameterNames[_p.Aggregate];
            var groupParameterName = _groupParameterNames[_p.Aggregate];
            var groupValuePairs = groups as IList<GroupValuePair> ?? groups.ToList();
            var bestGroups = groupValuePairs.Select(a => a.Name).Distinct().ToList();
            var other = _calculators[_p.Aggregate](_data, bestGroups);
            
            var total = groupValuePairs.Where(a=>a.Value.HasValue).Sum(a => a.Value.Value) + other;
            var additivepercent = 0.0d;
            var counter = 0.0d;
            foreach (var g in groupValuePairs)
            {
                if (!g.Value.HasValue) continue;

                var percent = g.Value.Value / total * 100.0d;
                additivepercent += percent;
                if (_p.Aggregate == Aggregate.Percent)
                {
                    var stat = String.Format("Группа: {0} \r\n{3}: {1} \r\nНакопительный процент: {2}", g.Name, Math.Round(percent, 3), Math.Round(additivepercent, 3), parameterName);
                    _chartParetoTable.Table.Add(new SLDataRow { Row = new List<object> { g.Name,  percent , percent, additivepercent, stat, GetTextLabel(Math.Round(percent,2), Math.Round(percent, 2)) } });
                }
                else
                {
                    var stat = String.Format("Группа: {0} \r\n{5}: {1} \r\n{4}: {2} \r\nНакопительный процент: {3}", g.Name, Math.Round( g.Value.Value, 3), Math.Round(percent, 3), Math.Round(additivepercent, 3), parameterName, groupParameterName);
                    _chartParetoTable.Table.Add(new SLDataRow { Row = new List<object> { g.Name,  g.Value, percent, additivepercent, stat, GetTextLabel(g.Value.Value, Math.Round(percent, 2)) } });
                }
                counter += g.Value.Value;
            }
            if (!(other > 0)) return;
            if (_p.Aggregate == Aggregate.Percent)
            {
                var finalstat = String.Format("Группа: {0} \r\n{3}: {1} \r\nНакопительный процент: {2}", " Прочие", Math.Round(100 - additivepercent, 3), 100, parameterName);
                _chartParetoTable.Table.Add(new SLDataRow { Row = new List<object> { " Прочие", 100.0d - additivepercent, 100.0d - additivepercent, 100.0d, finalstat, GetTextLabel(Math.Round(100.0d - additivepercent,2), Math.Round(100.0d - additivepercent, 2)) } });
            }
            else
            {
                var finalstat = String.Format("Группа: {0} \r\n{5}: {1} \r\n{4}: {2} \r\nНакопительный процент: {3}", " Прочие", Math.Round(total - counter, 3), 100.0d - additivepercent, 100, parameterName, groupParameterName);
                _chartParetoTable.Table.Add(new SLDataRow { Row = new List<object> { " Прочие", other, 100.0d - additivepercent, 100.0d, finalstat, GetTextLabel(other, Math.Round(100.0d - additivepercent, 2)) } });
            }
        }
        #endregion
       
        public SLDataTable GetParetoTable()
        {
            return _chartParetoTable;
        }

        public class GroupValuePair
        {
            public string Name;
            public double? Value;
        }
    }
};