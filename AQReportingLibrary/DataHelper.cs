﻿using System;
using System.Linq;
using ApplicationCore.CalculationServiceReference;

namespace AQReportingLibrary
{
    public static class DataHelper
    {
        public static int GetFirstDateIndex(SLDataTable selectedTable)
        {
            var firstDateField = selectedTable.DataTypes.FirstOrDefault(a => a == "System.DateTime" || a == "DateTime");
            return (firstDateField == null) ? -1 : selectedTable.DataTypes.IndexOf(firstDateField);
        }

        public static int GetGroupIndex(SLDataTable selectedTable, string groupField)
        {
            if (string.IsNullOrEmpty(groupField)) return -1;
            return
                (from c in selectedTable.ColumnNames select c.Replace("_", String.Empty)).ToList()
                                                                                         .IndexOf(groupField.Replace(
                                                                                             "_", String.Empty));
        }
    }
}