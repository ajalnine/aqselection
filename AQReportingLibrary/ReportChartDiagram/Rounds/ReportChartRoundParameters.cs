﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using System.Windows;
using AQBasicControlsLibrary;

namespace AQReportingLibrary
{
    public class ReportChartRoundParameters
    {
        public string Table { get; set; }
        public string  ValueField { get; set; }
        public string GroupField { get; set; }
        public string ColorField { get; set; }
        public string AggregateDescription { get; set; }
        public ReportChartRangeType RangeType { get; set; }
        public ReportChartRangeStyle RangeStyle { get; set; }
        public ReportChartRangeStyle FitStyle { get; set; }
        public ReportChartLayoutMode LayoutMode { get; set; }
        public int LimitedLayers { get; set; }
        public ReportChartDiagramLabelsType LabelsType { get; set; }
        public SLDataTable UserLimits { get; set; }
        public int? GroupNumber { get; set; }
        public FixedAxises FixedAxisesDescription { get; set; }
        public MarkerModes MarkerMode { get; set; }
        public MarkerSizeModes MarkerSizeMode { get; set; }
        public MarkerShapeModes MarkerShapeMode { get; set; }
        public ColorScales ColorScale { get; set; }
        public ColorScaleMode ColorScaleMode { get; set; }
        public Aggregate Aggregate { get; set; }
        public double FontSize { get; set; }
        public Size LayoutSize { get; set; }
        public ChartLegend ChartLegend { get; set; }

    }
}