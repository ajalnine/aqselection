﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;
using System.Windows;

// ReSharper disable once CheckNamespace
namespace AQReportingLibrary
{
    public class Frequencies
    {
        private readonly ReportChartHistogramParameters _rcfp;
        private readonly List<object> _columndata;
        private readonly bool _fromFloor;
        private readonly bool _ignoreGroups;
        private readonly SLDataTable _sourceTable;
        private readonly Data _sourceData;

        private SLDataTable _chartFrequencyTable;
        private double _histogramStep;
        private double _autoStep;
        private double? _stdev, _mean, _min, _max;
        private double _n;
        private double _binMax;
        private double _displayMin, _displayMax;
        private double? _minLimit, _maxLimit;
        private bool _isInteger;

        public double AutoStep
        {
            get { return _autoStep; }
        }

        private bool _stepIsAuto;

        public bool StepIsAuto
        {
            get { return _stepIsAuto; } 
        }
        public string ProbabilityComment;

        public Frequencies(Data data, ReportChartHistogramParameters rcfp, Limits l, bool fromFloor, bool ignoreGroups)
        {
            _fromFloor = fromFloor;
            _rcfp = rcfp;
            _ignoreGroups = ignoreGroups; 
            _sourceData = data;
            _columndata = data.GetLDR().Select(a => a.Y).Cast<object>().ToList();
            _minLimit = l?.MinLimit;
            _maxLimit = l?.MaxLimit;
            _sourceTable = data.GetDataTable();
            FillStatisticalData();
            if (!_min.HasValue || !_max.HasValue || !_stdev.HasValue || !_mean.HasValue) return;
            BuildFrequencyData();
        }

        private void BuildFrequencyData()
        {
            CheckStepIsInteger();
            CalcStep();
            CreateFrequencyTable();
        }

        private void FillStatisticalData()
        {
            _stdev = AQMath.AggregateStDev(_columndata);
            _mean = AQMath.AggregateMean(_columndata);
            _n = _columndata.Count;
            _min = AQMath.AggregateMin(_columndata);
            _max = AQMath.AggregateMax(_columndata);
        }

        #region Заполнение таблицы с bin
        private void CreateFrequencyTable()
        {
            CreateChartFrequencyTableHeaders();

            if (_histogramStep > 0)
            {
                FindDisplayRange();
                GetSafeStep();
                CreateBinData();
            }
            else
            {
                if (_min.HasValue && Math.Abs(_min.Value) > double.Epsilon) CreateFrequencyTableForEqualValues();
                else CreateFrequencyTableForZeroValues();
                _binMax = _n;
            }
        }

        private void GetSafeStep()
        {
            while ((_displayMax - _displayMin)/_histogramStep > 1e2)
            {
                _histogramStep *= 2;
            }
        }

        private void CreateChartFrequencyTableHeaders()
        {
            _chartFrequencyTable = new SLDataTable
            {
                TableName = "Частоты",
                Table = new List<SLDataRow>(),
                
                DataTypes = (new[] { "Double", "Double", "String", "Double", "Double", _sourceTable.DataTypes[8] ?? "String", "Double" , "Double", "Double", "String", "Double", "Double", "Double" ,"Double", "Double", "Double", "String" }).ToList(),
                ColumnNames = (new[] { _rcfp.Fields[0],
                    "Число наблюдений",
                    "Интервал",
                    "% наблюдений",
                    "_Отступ",
                    _rcfp.ColorField+" ",
                    "Шаг",
                    "Центр",
                    "_Отступ, %" ,
                    "Подписи",
                    "Размер",
                    "% наблюдений в столбце",
                    "Отступ, % в столбце",
                    "Отступ категории",
                    "Центр категории",
                    "Опция", "Цвет" }).ToList()
            };
        }

        private void CreateFrequencyTableForZeroValues()
        {
            if (!_min.HasValue) return;
            _histogramStep = 1;
            var offset = _rcfp.BinMode == ReportChartBinMode.IncludeRight ? 0 : 1;

            if (_rcfp.ColorField != "Нет" && _rcfp.ColorField != null && !_ignoreGroups)
            {
                var groups = GetGroups();
                double binoffset = 0;
                double bincount = _n;
                double binoffsetinpercent = 0;
                _binMax = _binMax < bincount ? _binMax : bincount;
                double categorySize = _histogramStep / groups.Count;
                var groupCounter = 0;
                foreach (var group in groups)
                {
                    var side = _rcfp.ColumnType != ReportChartColumnDiagramType.Opposite || groupCounter % 2 == 0 ? 1 : -1;
                    var groupData = _sourceData.GetLDR().Where(a => group != null && a.Z != null ? Equals(a.Z, group) : a.Z == null).Select(a => a.Y).Cast<object>().ToList();
                    double bincountInGroup = groupData.Count();
                    if (Math.Abs(bincountInGroup) < double.Epsilon) continue;
                    double binpercentInGroup = _rcfp.YMode != ReportChartFrequencyYMode.FrequencyInPercentOfGroup
                                            ? 100.0d * bincountInGroup / _n
                                            : 100.0d * bincountInGroup / groupData.Count;

                    var binName = String.Format("Все значения равны нулю, n={0}, {1}: {2} ", bincountInGroup, _rcfp.ColorField, group);
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>{
                                    -2.0d + offset, 
                                    0.0d,
                                    String.Empty,
                                    0.0d,
                                    0.0d,
                                    @group,
                                    -1.0d + offset,
                                    -1.5d, 0.0d, null, 0.0d, 0.0d, 0.0d, (double)groups.IndexOf(group)/groups.Count,
                                    -2.0d + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null}
                    });
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>
                        {
                            -1.0d + offset,
                            side * bincountInGroup + (_fromFloor ? 0.0d : binoffset),
                            binName,
                            side * binpercentInGroup + (_fromFloor ? 0.0d : binoffsetinpercent),
                            _fromFloor ? 0.0d : binoffset,
                            @group,
                            0.0d + offset,
                            -0.5d,
                            _fromFloor ? 0.0d : binoffsetinpercent,
                            GetTextLabel(bincountInGroup, binpercentInGroup, binpercentInGroup, bincountInGroup, group?.ToString()),
                            binpercentInGroup,
                            side * binpercentInGroup,
                            binpercentInGroup + binoffsetinpercent, (double)groups.IndexOf(group)/groups.Count,
                            -1.0d + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize
                            , 1.0d, null
                        }
                    });
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>{
                                    0.0d + offset,
                                    0.0d,
                                    String.Empty,
                                    0.0d,
                                    0.0d,
                                    @group,
                                    1.0d + offset,
                                    0.5d, 0.0d, null, 0.0d, 0.0d, 0.0d, (double)groups.IndexOf(group)/groups.Count,
                                    categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize
                                    , 1.0d, null }
                    });

                    binoffset += bincountInGroup;
                    binoffsetinpercent += binpercentInGroup;
                    groupCounter++;
                }
            }
            else
            {
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { -2.0d + offset, 0.0d, string.Empty, 0.0d, 0.00d, null, -1.0d + offset, -1.5d, 0.0d, null, 0.0d, 0.0d, 0.0d, -1.5d, 1.0d, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { -1.0d + offset, _n, String.Format("Все значения равны нулю, n={0}", _n), 100.0d, 0.00d, null, 0.0d + offset, -0.5d, 0.0d, GetTextLabel(_n, 100, 100, _n, null), 100.0d, 100.0d, 0.0d, -0.5d, 1.0d, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { 0.0d + offset, 0.0d, string.Empty, 0.0d, 0.00d, null, 1.0d + offset,  0.5d, 0.0d, null, 0.0d, 0.0d, 0.0d, 0.5d, 1.0d, null }
                });
            }
        }

        private void CreateFrequencyTableForEqualValues()
        {
            if (!_min.HasValue) return;
            _histogramStep = _min.Value;
            var offset = _rcfp.BinMode == ReportChartBinMode.IncludeRight ? 0 : _min.Value;
            var groupCounter = 0;
            if (_rcfp.ColorField != "Нет" && _rcfp.ColorField != null && !_ignoreGroups)
            {
                var groups = GetGroups();
                double binoffset = 0;
                double bincount = _n;
                double binoffsetinpercent = 0;
                _binMax = _binMax < bincount ? _binMax : bincount;
                double categorySize = _histogramStep / groups.Count;

                foreach (var group in groups)
                {
                    var side = _rcfp.ColumnType != ReportChartColumnDiagramType.Opposite || groupCounter % 2 == 0 ? 1 : -1;
                    var groupData = _sourceData.GetLDR().Where(a => group != null && a.Z != null ? Equals(a.Z, group) : a.Z == null).Select(a => a.Y).Cast<object>().ToList();
                    double bincountInGroup =groupData.Count(a =>Math.Round((double) a, 8) > 0 &&Math.Round((double) a, 8) <= Math.Round(_histogramStep, 8));
                    if (Math.Abs(bincountInGroup) < double.Epsilon) continue;
                    double binpercentInGroup = _rcfp.YMode != ReportChartFrequencyYMode.FrequencyInPercentOfGroup
                                            ? 100.0d * bincountInGroup / _n
                                            : 100.0d * bincountInGroup / groupData.Count;

                    var binName = String.Format("Все значения равны {0}, n={1}, {2}: {3} ", _min.Value, bincountInGroup,_rcfp.ColorField, group);
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>{
                                    - _min.Value + offset,
                                    0.0d,
                                    String.Empty,
                                    0.0d,
                                    0.0d,
                                    @group,
                                    0.0d + offset,
                                    -0.5 * _min.Value, 0.0d, null, 0.0d, 0.0d, 0.0d, (double)groups.IndexOf(group)/groups.Count
                                    ,- _min.Value + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null}
                    });
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>
                        {
                            0.0d + offset,
                            side * bincountInGroup + (_fromFloor ? 0.0d : binoffset),
                            binName,
                            side * binpercentInGroup + (_fromFloor ? 0.0d : binoffsetinpercent),
                            _fromFloor ? 0.0d : binoffset,
                            @group,
                            _min.Value + offset,
                             _min.Value * 0.5,
                            _fromFloor ? 0.0d : binoffsetinpercent,
                            GetTextLabel(bincountInGroup, binpercentInGroup, binpercentInGroup, bincountInGroup, group?.ToString()),
                            binpercentInGroup,
                            side * binpercentInGroup,
                            binpercentInGroup + binoffsetinpercent, (double)groups.IndexOf(group)/groups.Count,
                            categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null
                        }
                    });
                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object>{
                                    _min.Value + offset,
                                    0.0d,
                                    String.Empty,
                                    0.0d,
                                    0.0d,
                                    @group,
                                    2.0d * _min.Value + offset,
                                    1.5d * _min.Value, 0.0d, null, 0.0d, 0.0d, 0.0d, (double)groups.IndexOf(group)/groups.Count,
                                    _min.Value + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null}
                    });

                    binoffset += bincountInGroup;
                    binoffsetinpercent += binpercentInGroup;
                    groupCounter++;
                }
                
            }
            else
            {
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { -_min.Value + offset, 0.0d, string.Empty, 0.0d, 0.00d, null, 0.0d + offset, -_min.Value * 0.5d, 0.0d, null, 0.0d, 0.0d, 0.0d, 0.0d, -_min.Value * 0.5d, 1.0d, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { 0.0d + offset, _n, String.Format("Все значения одинаковы, v={0}, n={1}", _min.Value, _n), 100.0d, 0.00d, null, _min.Value + offset, _min.Value * 0.5d, 0.0d, GetTextLabel(_n, 100, 100, _n, null), 100.0d, 100.0d, 0.0d, 0.0d, _min.Value * 0.5d, 1.0d, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object> { _min.Value + offset, 0.0d, string.Empty, 0.0d, 0.00d, null, _min.Value * 2.0d, _min.Value * 1.5d, 0.0d, null, 0.0d + offset, 0.0d, 0.0d, 0.0d, _min.Value * 1.5d, 1.0d, null }
                });
            }
        }

        private void CreateBinData()
        {
            if (_rcfp.ColorField != "Нет" && _rcfp.ColorField != null && !_ignoreGroups)
            {
                switch(_rcfp.BinMode)
                {
                    case ReportChartBinMode.IncludeRight:
                        CreateBinDataForMultipleLayersRight();
                        return;
                    case ReportChartBinMode.IncludeLeft:
                        CreateBinDataForMultipleLayersLeft();
                        return;
                }
            }
            else CreateBinDataForSingleLayer();
        }

        private void CreateBinDataForSingleLayer()
        {
            _binMax = 0;
            var start = Math.Round(_displayMin, 8);
            var leftMode = _rcfp.BinMode == ReportChartBinMode.IncludeLeft;
            double bincount, bincountOnMax, binpercent, binpercentOnMax, roundBin, roundBinAndStep;
            var roundColumnData = _columndata.Select(a => Math.Round((double)a, 8)).ToList();
            if (leftMode)
            {
                for (var bin = start; _displayMax - bin > 1e-8; bin = Math.Round(_histogramStep + bin, 8))
                {
                    roundBin = Math.Round(bin, 8);
                    roundBinAndStep = Math.Round(_histogramStep + bin, 8);
                    bincount = roundColumnData.Count(a => a >= roundBin && a < roundBinAndStep);
                    if (Math.Abs(bin - start) < double.Epsilon && bincount > 0)
                    {
                        _chartFrequencyTable.Table.Add(new SLDataRow
                        {
                            Row = new List<object> { start, 0.0d, String.Empty, 0.0d, 0.0d, null, start + _histogramStep, start - _histogramStep * 0.5, 0.0d, null, bincount, 00.0d, 0.0d, 0.0d, start - _histogramStep * 0.5, 1.0d, null }
                        });
                    }

                    bincountOnMax = roundColumnData.Count(a => Math.Abs(a - roundBin) < double.Epsilon);
                    binpercent = 100.0d * bincount / _n;
                    binpercentOnMax = 100.0d * bincountOnMax / _n;
                    _binMax = _binMax < bincount ? _binMax : bincount;
                    var binName = String.Format("Число наблюдений в интервале {0} ≤ x < {1} n={2} ({3} %)", bin, bin + _histogramStep, bincount, Math.Round(binpercent, 2));
                    binName += String.Format("\r\nНа границе x = {0} число наблюдений n={1} ({2} %)", bin , bincountOnMax, Math.Round(binpercentOnMax, 2));

                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object> { bin, bincount, binName, binpercent, 0.0d, null, _histogramStep + bin, _histogramStep * 0.5 + bin, 0.0d, GetTextLabel(bincount, binpercent, 100, bincount, null), bincount, bincount > 0 ? 100.0d : 0.00d, 0.00d, 0.0d, _histogramStep * 0.5 + bin, 1.0d, null }
                    });
                }
            }
            else
            {
                for (var bin = start; _displayMax - bin > 1e-8; bin = Math.Round(_histogramStep + bin, 8))
                {
                    roundBin = Math.Round(bin, 8);
                    roundBinAndStep = Math.Round(_histogramStep + bin, 8);
                    bincount = roundColumnData.Count(a => a > roundBin && a <= roundBinAndStep);
                    if (Math.Abs(bin - start) < double.Epsilon && bincount > 0)
                    {
                        _chartFrequencyTable.Table.Add(new SLDataRow
                        {
                            Row = new List<object> { start - _histogramStep, 0.0d, String.Empty, 0.0d, 0.0d, null, start, start - _histogramStep * 0.5, 0.0d, null, bincount, 00.0d, 0.0d, 0.0d, start - _histogramStep * 0.5, 1.0d, null }
                        });
                    }

                    bincountOnMax = roundColumnData.Count(a => Math.Abs(a - roundBinAndStep) < double.Epsilon);
                    binpercent = 100.0d * bincount / _n;
                    binpercentOnMax = 100.0d * bincountOnMax / _n;
                    _binMax = _binMax < bincount ? _binMax : bincount;
                    var binName = String.Format("Число наблюдений в интервале {0} < x ≤ {1} n={2} ({3} %)", bin, bin + _histogramStep, bincount, Math.Round(binpercent, 2));
                    binName += String.Format("\r\nНа границе x = {0} число наблюдений n={1} ({2} %)", bin + _histogramStep, bincountOnMax, Math.Round(binpercentOnMax, 2));

                    _chartFrequencyTable.Table.Add(new SLDataRow
                    {
                        Row = new List<object> { bin, bincount, binName, binpercent, 0.0d, null, _histogramStep + bin, _histogramStep * 0.5 + bin, 0.0d, GetTextLabel(bincount, binpercent, 100, bincount, null), bincount, bincount > 0 ? 100.0d : 0.00d, 0.00d, 0.0d, _histogramStep * 0.5 + bin, 1.0d, null }
                    });
                }
            }
        }

        private string GetTextLabel(double binvalue, double binpercent, double binpercentingroup, double n, string layer)
        {
            var textLabel = string.Empty;
            if (!(Math.Abs(binvalue) > double.Epsilon)) return textLabel;
            if ((_rcfp.LabelsType & ReportChartColumnsLabelsType.Layer) == ReportChartColumnsLabelsType.Layer && !string.IsNullOrEmpty(layer))
            {
                textLabel += layer;
            }

            if ((_rcfp.LabelsType & ReportChartColumnsLabelsType.N) == ReportChartColumnsLabelsType.N)
            {
                if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                textLabel += AQMath.SmartRound(binvalue, 3);
            }
            if ((_rcfp.LabelsType & ReportChartColumnsLabelsType.Percent) == ReportChartColumnsLabelsType.Percent)
            {
                if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                textLabel += Math.Round(binpercent, 2) + " %";
            }
            if ((_rcfp.LabelsType & ReportChartColumnsLabelsType.PercentInGroup) == ReportChartColumnsLabelsType.PercentInGroup)
            {
                if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                textLabel += Math.Round(binpercentingroup, 2) + " %";
            }
            if ((_rcfp.LabelsType & ReportChartColumnsLabelsType.NumberOfCases) == ReportChartColumnsLabelsType.NumberOfCases)
            {
                if (!string.IsNullOrEmpty(textLabel)) textLabel += "\r\n";
                textLabel += "n="+Math.Round(n, 0);
            }
            return textLabel;
        }

        private void CreateBinDataForMultipleLayersRight()
        {
            _binMax = 0;
            var limited = _sourceTable.Table.Count > 100000;
            var groups = GetGroups();
            var data = SplitData(groups);
            
            double binoffset, binoffsetinpercent, binoffsetInPercentOfBin, bincount, bincountOnMax, binpercent, binpercentOnMax, categorySize, bincountInGroup, bincountOnMaxInGroup, binpercentInGroup, binpercentOnMaxGroup, binpercentInGroupOfBin, roundBin, roundBinAndStep;
            int groupCounter;
            double side;
            var roundColumnData = _columndata.Select(a => Math.Round((double)a, 8)).ToList();


            for (var bin = Math.Round(_displayMin, 8); _displayMax - bin > 1e-8; bin = Math.Round(_histogramStep + bin, 8))
            {
                roundBin = Math.Round(bin, 8);
                roundBinAndStep = Math.Round(_histogramStep + bin, 8);
                binoffset = 0;
                binoffsetinpercent = 0;
                binoffsetInPercentOfBin = 0;
                bincount = roundColumnData.Count(a => a >  roundBin && a <= roundBinAndStep);
                bincountOnMax = roundColumnData.Count(a => Math.Abs(a - roundBinAndStep) < double.Epsilon);
                binpercent = 100.0d * bincount / _n;
                binpercentOnMax = 100.0d * bincountOnMax / _n;
                _binMax = _binMax < bincount ? _binMax : bincount;
                categorySize = _histogramStep/groups.Count;
                groupCounter = 0;
                foreach (var group in groups)
                {
                    var key = GetKey(group);
                    var groupData = data[key];
                    bincountInGroup = groupData.Count(a => Math.Round((double)a, 8) > roundBin && Math.Round((double)a, 8) <= roundBinAndStep);
                    binpercentInGroup = _rcfp.YMode != ReportChartFrequencyYMode.FrequencyInPercentOfGroup
                        ? 100.0d * bincountInGroup / _n
                        : 100.0d * bincountInGroup / groupData.Count;
                    binpercentInGroupOfBin = Math.Abs(bincount) < Double.Epsilon ? 0.0d : bincountInGroup / bincount * 100.0d;
                    if (Math.Abs(bincountInGroup) > double.Epsilon)
                    {
                        side = _rcfp.ColumnType != ReportChartColumnDiagramType.Opposite || groupCounter % 2 == 0 ? 1.0 : -1.0;
                        bincountOnMaxInGroup = groupData.Count(a => Math.Abs(Math.Round((double)a, 8) - Math.Round(_histogramStep + bin, 8)) < double.Epsilon);
                        binpercentOnMaxGroup = 100.0d * bincountOnMaxInGroup / _n;

                        var binName = limited
                            ? $"n = {bincountInGroup}"
                            : $"Число наблюдений в интервале {bin} < x ≤ {bin + _histogramStep} n={bincount} ({Math.Round(binpercent, 2)} %)"
                        +
                            $"\r\nНа границе x = {bin + _histogramStep} число наблюдений n={bincountOnMax} ({Math.Round(binpercentOnMax, 2)} %)"
                        +
                            $"\r\n{_rcfp.ColorField}: {@group}, число наблюдений в группе n={bincountInGroup} ({Math.Round(binpercentInGroup, 2)} %)"
                        +
                            $"\r\nНа границе в группе n={bincountOnMaxInGroup} ({Math.Round(binpercentOnMaxGroup, 2)} %)";
                        _chartFrequencyTable.Table.Add(new SLDataRow
                        {
                            Row =
                                new List<object>
                                {
                                bin, //_rcfp.Fields[0] 
                                (bincountInGroup + (_fromFloor ? 0.0d : binoffset))*side, // "Число наблюдений" 
                                binName, //"Интервал"
                                (binpercentInGroup + (_fromFloor ? 0.0d : binoffsetinpercent)) * side, //"% наблюдений"
                                _fromFloor ? 0.0d : binoffset, //"Отступ"
                                @group, //_rcfp.ColorField
                                _histogramStep + bin, //"Шаг"
                                _histogramStep * 0.5 + bin, //"Центр"
                                _fromFloor ? 0.0d : binoffsetinpercent, //"Отступ, %"
                                limited
                                ? null
                                : GetTextLabel(bincountInGroup, binpercentInGroup, 100.0d * bincountInGroup / bincount, bincountInGroup, group?.ToString()), // "Подписи"
                                binpercentInGroup, //"Размер"
                                (binpercentInGroupOfBin + binoffsetInPercentOfBin)  * side, //"% наблюдений в столбце"
                                binoffsetInPercentOfBin, //"Отступ, % в столбце"
                                (double)groupCounter/groups.Count, //"Отступ категории"
                                categorySize * 0.5 + bin + groupCounter * categorySize, //"Центр категории"
                                side, //Сторона
                                null // Цвет
                                }
                        });
                    }
                    binoffset += bincountInGroup;
                    binoffsetinpercent += binpercentInGroup;
                    binoffsetInPercentOfBin += binpercentInGroupOfBin;
                    groupCounter++;
                }
            }
            double minBin, maxBin;
            foreach (var group in groups)
            {
                var groupData = _chartFrequencyTable.Table.Where(a => a.Row[5] == group).ToList();
                if (!groupData.Any()) continue;
                minBin = (double)groupData.Min(a => a.Row[0]);
                maxBin = (double)groupData.Max(a => a.Row[0]);
                categorySize = _histogramStep / groups.Count;
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                        Row = new List<object>{
                                minBin - _histogramStep,
                                0.0d,
                                String.Empty,
                                0.0d,
                                0.0d,
                                @group,
                                minBin,
                                 minBin - 0.5 * _histogramStep, 0.0d, null, 0.0d, 0.0d, 0.0d,
                                (double)groups.IndexOf(group)/groups.Count,
                                minBin - _histogramStep + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object>{
                                maxBin + _histogramStep,
                                0.0d,
                                String.Empty,
                                0.0d,
                                0.0d,
                                @group,
                                maxBin + 2 * _histogramStep,
                                maxBin + 1.5 * _histogramStep, 0.0d, null, 0.0d, 0.0d, 0.0d,
                                (double)groups.IndexOf(group)/groups.Count,
                                maxBin + _histogramStep + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1.0d, null }
                });
            }
            var orderedTable = _chartFrequencyTable.Table.OrderBy(a => a.Row[0]).ToList();
            _chartFrequencyTable.Table = orderedTable;
        }

        private void CreateBinDataForMultipleLayersLeft()
        {
            _binMax = 0;
            var groups = GetGroups();
            var data = SplitData(groups);

            var limited = _sourceTable.Table.Count > 100000;
            double binoffset, binoffsetinpercent, binoffsetInPercentOfBin, bincount, bincountOnMin, binpercent, binpercentOnMin, categorySize, bincountInGroup, binpercentInGroup, binpercentInGroupOfBin, bincountOnMinInGroup, binpercentOnMinGroup, roundBin, roundBinAndStep;
            int groupCounter;
            double side;
            var roundColumnData = _columndata.Select(a => Math.Round((double)a, 8)).ToList();

            for (var bin = Math.Round(_displayMin, 8); _displayMax - bin > 1e-8; bin = Math.Round(_histogramStep + bin, 8))
            {
                roundBin = Math.Round(bin, 8);
                roundBinAndStep = Math.Round(_histogramStep + bin, 8);
                binoffset = 0;
                binoffsetinpercent = 0;
                binoffsetInPercentOfBin = 0;
                bincount = roundColumnData.Count(a => a >= roundBin && a < roundBinAndStep);
                bincountOnMin = roundColumnData.Count(a => a - roundBin < double.Epsilon);
                binpercent = 100.0d * bincount / _n;
                binpercentOnMin = 100.0d * bincountOnMin / _n;
                _binMax = _binMax < bincount ? _binMax : bincount;
                categorySize = _histogramStep / groups.Count;
                groupCounter = 0;
                foreach (var group in groups)
                {
                    var key = GetKey(group);
                    var groupData = data[key];
                    bincountInGroup = groupData.Count(a => Math.Round((double)a, 8) >= Math.Round(bin, 8) && Math.Round((double)a, 8) < Math.Round(_histogramStep + bin, 8));
                    binpercentInGroup = _rcfp.YMode != ReportChartFrequencyYMode.FrequencyInPercentOfGroup
                        ? 100.0d * bincountInGroup / _n
                        : 100.0d * bincountInGroup / groupData.Count;
                    binpercentInGroupOfBin = Math.Abs(bincount) < Double.Epsilon ? 0.0d : bincountInGroup / bincount * 100.0d;
                    if (Math.Abs(bincountInGroup) > double.Epsilon)
                    {
                        side = _rcfp.ColumnType != ReportChartColumnDiagramType.Opposite || groupCounter % 2 == 0 ? 1.0d : -1.0d;
                        bincountOnMinInGroup = groupData.Count(a => Math.Round((double)a, 8).ToString(CultureInfo.InvariantCulture) == Math.Round(bin, 8).ToString(CultureInfo.InvariantCulture));
                        binpercentOnMinGroup = 100.0d * bincountOnMinInGroup / _n;

                        var binName = limited
                            ? $"n={bincountInGroup}"
                            : $"Число наблюдений в интервале {bin} ≤ x < {bin + _histogramStep} n={bincount} ({Math.Round(binpercent, 2)} %)"
                        +
                            $"\r\nНа границе x = {bin} число наблюдений n={bincountOnMin} ({Math.Round(binpercentOnMin, 2)} %)"
                        +
                            $"\r\n{_rcfp.ColorField}: {@group}, число наблюдений в группе n={bincountInGroup} ({Math.Round(binpercentInGroup, 2)} %)"
                        +
                            $"\r\nНа границе в группе n={bincountOnMinInGroup} ({Math.Round(binpercentOnMinGroup, 2)} %)";
                        _chartFrequencyTable.Table.Add(new SLDataRow
                        {
                            Row =
                                new List<object>
                                {
                                bin, //_rcfp.Fields[0] 
                                (bincountInGroup + (_fromFloor ? 0.0d : binoffset))*side, // "Число наблюдений" 
                                binName, //"Интервал"
                                (binpercentInGroup + (_fromFloor ? 0.0d : binoffsetinpercent)) * side, //"% наблюдений"
                                _fromFloor ? 0.0d : binoffset, //"Отступ"
                                @group, //_rcfp.ColorField
                                _histogramStep + bin, //"Шаг"
                                _histogramStep * 0.5 + bin, //"Центр"
                                _fromFloor ? 0.0d : binoffsetinpercent, //"Отступ, %"
                                limited
                                ? null
                                : GetTextLabel(bincountInGroup, binpercentInGroup, 100.0d * bincountInGroup / bincount, bincountInGroup, group?.ToString()), // "Подписи"
                                binpercentInGroup, //"Размер"
                                (binpercentInGroupOfBin + binoffsetInPercentOfBin)  * side, //"% наблюдений в столбце"
                                binoffsetInPercentOfBin, //"Отступ, % в столбце"
                                (double)groupCounter/groups.Count, //"Отступ категории"
                                categorySize * 0.5 + bin + groupCounter * categorySize, //"Центр категории"
                                side, //Сторона
                                null // Цвет
                                }
                        });
                    }
                    binoffset += bincountInGroup;
                    binoffsetinpercent += binpercentInGroup;
                    binoffsetInPercentOfBin += binpercentInGroupOfBin;
                    groupCounter++;
                }
            }

            foreach (var group in groups)
            {
                var groupData = _chartFrequencyTable.Table.Where(a => a.Row[5] == group).ToList();
                if (!groupData.Any())continue;
                var minBin = (double)groupData.Min(a => a.Row[0]);
                var maxBin = (double)groupData.Max(a => a.Row[0]);
                categorySize = _histogramStep / groups.Count;
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object>{
                                minBin - _histogramStep,
                                0.0d,
                                String.Empty,
                                0.0d,
                                0.0d,
                                @group,
                                minBin,
                                 minBin - 0.5 * _histogramStep, 0.0d, null, 0.0d, 0.0d, 0.0d,
                                (double)groups.IndexOf(group)/groups.Count,
                                minBin - _histogramStep + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1, null }
                });
                _chartFrequencyTable.Table.Add(new SLDataRow
                {
                    Row = new List<object>{
                                maxBin + _histogramStep,
                                0.0d,
                                String.Empty,
                                0.0d,
                                0.0d,
                                @group,
                                maxBin + 2 * _histogramStep,
                                maxBin + 1.5 * _histogramStep, 0.0d, null, 0.0d, 0.0d, 0.0d,
                                (double)groups.IndexOf(group)/groups.Count,
                                maxBin + _histogramStep + categorySize * 0.5 + (double)groups.IndexOf(group) * categorySize, 1, null}
                });
            }
            var orderedTable = _chartFrequencyTable.Table.OrderBy(a => a.Row[0]).ToList();
            _chartFrequencyTable.Table = orderedTable;
        }

        public List<object> GetGroups()
        {
            List<object> groups;
            switch (_sourceTable.DataTypes[8])
            {
                case "Double":
                case "System.Double":
                    groups = (from g in _sourceData.GetLDR() where g.Z is double select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => !(a.Z is double))) groups.Add("#");
                    break;
                case "DateTime":
                case "System.DateTime":
                    groups =
                        (from g in _sourceData.GetLDR() where g.Z is DateTime select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => !(a.Z is DateTime))) groups.Add("#");
                    break;
                case "String":
                case "System.String":
                    groups =
                        (from g in _sourceData.GetLDR()
                         where g.Z != DBNull.Value && g.Z != null && (string)g.Z != string.Empty
                         select g.Z).Distinct().OrderBy(a => a).ToList();
                    if (_sourceData.GetLDR().Any(a => (a.Z == DBNull.Value || a.Z == null || (string)a.Z == string.Empty))) groups.Add("#");
                    break;
                default:
                    groups = (from g in _sourceData.GetLDR() select g.Z).Distinct().OrderBy(a => a).ToList();
                    break;
            }
            return groups;
        }

        private Dictionary<object, List<object>> SplitData(List<object> groups)
        {
            var ldr = _sourceData.GetLDR();
            var result = new Dictionary<object, List<object>>();


            switch (_sourceTable.DataTypes[8])
            {
                case "Double":
                case "System.Double":
                    foreach (var l in ldr)
                    {
                        var o = l.Z is double ? l.Z : "#";
                        if (!result.ContainsKey(o)) result.Add(o, new List<object> { l.Y });
                        else result[o].Add(l.Y);
                    }
                    break;

                case "DateTime":
                case "System.DateTime":
                    foreach (var l in ldr)
                    {
                        var o = l.Z is DateTime ? l.Z : "#";
                        if (!result.ContainsKey(o)) result.Add(o, new List<object> { l.Y });
                        else result[o].Add(l.Y);
                    }
                    break;

                case "String":
                case "System.String":
                    foreach (var l in ldr)
                    {
                        var o = !string.IsNullOrWhiteSpace(l.Z?.ToString()) ? l.Z : "#";
                        if (!result.ContainsKey(o)) result.Add(o, new List<object> { l.Y });
                        else result[o].Add(l.Y);
                    }
                    break;

                default:
                    foreach (var l in ldr)
                    {
                        var o = l.Z ?? "#";
                        if (!result.ContainsKey(o)) result.Add(o, new List<object> { l.Y });
                        else result[o].Add(l.Y);
                    }
                    break;
            }
            return result;
        }


        private object GetKey(object group)
        {
            switch (_sourceTable.DataTypes[8])
            {
                case "Double":
                case "System.Double":
                        return group is double ? group : "#";

                case "DateTime":
                case "System.DateTime":
                    return group is DateTime ? group : "#";

                case "String":
                case "System.String":
                    return !string.IsNullOrWhiteSpace(group?.ToString()) ? group : "#";

                default:
                    return group ?? "#";
            }
        }
        #endregion

        public StatInfo GetDataStatInfo()
        {
            return new StatInfo
            {
                N = _n,
                Step = _histogramStep,
                S = _stdev,
                X = _mean,
                BinMax = _binMax,
                BinMaxInPercent = 100 * _binMax / _n,
                DisplayMax = _displayMax,
                DisplayMin = _displayMin,
                Px = AQMath.NormalPD(_mean, _mean, _stdev),
                PercentOfTotal = 100,
                SourceData = _sourceData.GetLDR().Select(a => a.Y).Cast<object>().ToList()
        };
        }

        public List<StatInfo> GetGroupStatInfo()
        {
            _binMax = 0;
            var groups = GetGroups();
            var GroupStatInfo = new List<StatInfo>();
            var groupCounter = 0;
            
            foreach (var group in groups)
            {
                var groupData = _sourceData.GetLDR().Where(a => group != null && a.Z != null ? a.Z.ToString() == group.ToString() : a.Z == null).Select(a => a.Y).Cast<object>().ToList();
                var side = _rcfp.ColumnType != ReportChartColumnDiagramType.Opposite || groupCounter % 2 == 0;
                var statinfo = new StatInfo
                {
                    N = groupData.Count,
                    DisplayMax = _displayMax,
                    DisplayMin = _displayMin,
                    Group = group,
                    Step = _histogramStep,
                    S = AQMath.AggregateStDev(groupData),
                    X = AQMath.AggregateMean(groupData),
                    PercentOfTotal = _rcfp.YMode == ReportChartFrequencyYMode.FrequencyInPercentOfGroup
                    ? 100
                    : 100 * groupData.Count / _n,
                    ShowInversed = !side,
                    SourceData = groupData
                };
                statinfo.Px = AQMath.NormalPD(statinfo.X, statinfo.X, statinfo.S);
                GroupStatInfo.Add(statinfo);
                groupCounter++;
            }
            return GroupStatInfo;
        }

        #region Расчет параметров оси
        private void FindDisplayRange()
        {
            if (!_min.HasValue || !_max.HasValue)
            {
                _displayMin = 0;
                _displayMax = 0;
                return;
            }
            double lr, ur;
            double roundedmin = Math.Floor(_min.Value / _histogramStep) * _histogramStep;
            double roundedmax = Math.Ceiling(_max.Value / _histogramStep) * _histogramStep;

            switch (_rcfp.AlignMode)
            {
                case ReportChartAlignMode.AlignByLL:
                    if (_minLimit.HasValue)
                    {
                        lr = Math.Ceiling((_minLimit.Value - _min.Value) / _histogramStep) * _histogramStep;
                        ur = Math.Ceiling((_max.Value - _minLimit.Value) / _histogramStep) * _histogramStep;
                        roundedmin = _minLimit.Value - lr;
                        roundedmax = _minLimit.Value + ur;
                    }
                    break;

                case ReportChartAlignMode.AlignByUL:
                    if (_maxLimit.HasValue)
                    {
                        lr = Math.Ceiling((_maxLimit.Value - _min.Value) / _histogramStep) * _histogramStep;
                        ur = Math.Ceiling((_max.Value - _maxLimit.Value) / _histogramStep) * _histogramStep;
                        roundedmin = _maxLimit.Value - lr;
                        roundedmax = _maxLimit.Value + ur;
                    }
                    break;

                case ReportChartAlignMode.AlignByUser:
                    if (_rcfp.UserAlignValue.HasValue)
                    {
                        lr = Math.Ceiling((_rcfp.UserAlignValue.Value - _min.Value) / _histogramStep) * _histogramStep;
                        ur = Math.Ceiling((_max.Value - _rcfp.UserAlignValue.Value) / _histogramStep) * _histogramStep;
                        roundedmin = _rcfp.UserAlignValue.Value - lr;
                        roundedmax = _rcfp.UserAlignValue.Value + ur;
                    }
                    break;
            }

            _displayMin = roundedmin - _histogramStep;
            _displayMax = roundedmax + _histogramStep;
        }

        private void CalcStep()
        {
            double step = 0;

            if (!_stdev.HasValue)
            {
                _histogramStep = 1;
                return;
            }

            if (_rcfp.Step.HasValue)
            {
                step = _rcfp.Step.Value/_rcfp.DetailLevel*4;
                _stepIsAuto = false;
            }
            else
            {
                _stepIsAuto = true;
                if (_isInteger) step = 1;
                else
                {
                    double approximateStep = 3*Math.Round(_stdev.Value, 6)/(Math.Pow(_n, 1/3)) / _rcfp.DetailLevel; //Формула Скотта
                    double degree = Math.Pow(10, Math.Ceiling(Math.Log10(approximateStep)) - 1);
                    if (Math.Abs(degree) > 1e-8)
                    {
                        _autoStep = Math.Floor(approximateStep/degree)*degree;
                        step = _autoStep;
                    }
                }
            }
            _histogramStep = step;
        }

        private void CheckStepIsInteger()
        {
            var range = _max - _min;
            _isInteger = Math.Abs((from d in _columndata select (double)d - Math.Floor((double)d)).Sum()) < double.Epsilon &&
                   range < 10 && range > 1;
        }
        #endregion

        public SLDataTable GetFreqTable()
        {
            return _chartFrequencyTable;
        }

        public SLDataTable GetPartialFreqTable(object group)
        {
            var partialTable = new SLDataTable
            {
                ColumnNames = _chartFrequencyTable.ColumnNames,
                ColumnTags = _chartFrequencyTable.ColumnTags,
                CustomProperties = _chartFrequencyTable.CustomProperties,
                DataTypes = _chartFrequencyTable.DataTypes,
                SelectionString = _chartFrequencyTable.SelectionString,
                TableName = _chartFrequencyTable.TableName,
                Tag = _chartFrequencyTable.Tag,
                Table = (group == null) 
                ? _chartFrequencyTable.Table.Where(a => a.Row[5]==null).ToList()
                : _chartFrequencyTable.Table.Where(a => a.Row[5]!=null && a.Row[5].ToString() == group.ToString()).ToList()
            };

            return partialTable;
        }
    }
}