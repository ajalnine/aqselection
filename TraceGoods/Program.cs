﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Entity;

namespace TraceGoods
{
    class Program
    {
        private static PCUEntities PCU;
        
        private static void Main(string[] args)
        {
            PCU = new PCUEntities();
            DateTime StartDate = new DateTime(2012, 11, 20);
            WriteLog(String.Format("Поиск конечных контрольных партий..."));
            
            List<decimal> AllCPN = (from g in PCU.Control_Parts where g.PartCreationDate > StartDate select g.CPN).Distinct().ToList();
            List<decimal> AllPrevious = (from g in PCU.Control_Parts where g.PartCreationDate > StartDate && g.PreviousCPN.HasValue && g.PreviousCPN.Value>0 select g.PreviousCPN.Value).Distinct().ToList();

            List<decimal> EndPoints = (from a in AllCPN where !AllPrevious.Contains(a) select a).ToList();
            WriteLog(String.Format("Найдено {0} конечных контрольных партий.", EndPoints.Count));
            foreach (decimal good in EndPoints) ProcessUnit(good);
            WriteLog(String.Format("Обработка завершена"));

            return;
        }

        private static void WriteLog(string LogText)
        {
            Console.WriteLine(DateTime.Now.ToShortTimeString() + ":  " + LogText);
        }

        private static void ProcessUnit(decimal CPN)
        {
            decimal? CurrentPartCPN = CPN;
            string UnitName = string.Empty;

            while (CurrentPartCPN.HasValue)
            {
                Control_Parts CurrentPart = (from p in PCU.Control_Parts where p.CPN == CurrentPartCPN.Value select p).SingleOrDefault();
                if (string.IsNullOrEmpty(CurrentPart.BilletMark))
                {
                    
                }
                CurrentPartCPN = CurrentPart.PreviousCPN;
            }
        }
    }
}
