﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AQCalculationsLibrary;
using AQBasicControlsLibrary;
using AQControlsLibrary;
using AQCatalogueLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using System.Windows.Browser;

namespace IngotCalculator
{
    public partial class MainPage : UserControl, IAQModule
    {
        CommandCallBackDelegate ReadySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool ReadyForAcceptSignalsProcessed = false;

        public InteractiveParameters p
        {
            get { return (InteractiveParameters)GetValue(pProperty); }
            set { SetValue(pProperty, value); }
        }

        public static readonly DependencyProperty pProperty =
            DependencyProperty.Register("p", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        public InteractiveResults r
        {
            get { return (InteractiveResults)GetValue(rProperty); }
            set { SetValue(rProperty, value); }
        }

        public static readonly DependencyProperty rProperty =
            DependencyProperty.Register("r", typeof(InteractiveResults), typeof(MainPage), new PropertyMetadata(null));
        
        private bool NewItem;
        private readonly AQModuleDescription _aqmd;
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
        }

        #region расчет прямоугольного слитка

        private void DoRectangleCalculation()
        {
            try
            {
                double UpNarrowSize = p.GetValue("Узкая сторона верхней конусной части, мм");
                double UpWideSize = p.GetValue("Широкая сторона верхней конусной части, мм");
                double DownNarrowSize = p.GetValue("Узкая сторона нижней конусной части, мм");
                double DownWideSize = p.GetValue("Широкая сторона нижней конусной части, мм");
                double HeadHeight = p.GetValue("Высота налива надставки, мм");
                double UpHeight = p.GetValue("Высота верхней конусной части, мм");
                if (UpHeight < double.Epsilon) UpHeight = 1e-12;
                double DownHeight = p.GetValue("Высота нижней конусной части, мм");
                if (DownHeight < double.Epsilon) DownHeight = 1e-12;
                double deltaA = p.GetValue("Сужение узкой стороны надставки относительно слитка, мм");
                double deltaB = p.GetValue("Сужение широкой стороны надставки относительно слитка, мм");
                double HeadNarrowCone = p.GetValue("Конусность узкой стороны надставки, %");
                double HeadWideCone = p.GetValue("Конусность широкой стороны надставки, %");
                double DownNarrowCone = p.GetValue("Конусность узкой стороны нижней конусной части, %");
                double DownWideCone = p.GetValue("Конусность широкой стороны нижней конусной части, %");
                double RadiusHead = p.GetValue("Радиус закругления надставки, мм");
                double Radius = p.GetValue("Радиус закругления конусной части, мм");
                double CorkDiameter = p.GetValue("Диаметр пробки, мм");
                double Density = p.GetValue("Плотность металла, т/м³");
                double BurningLoss = p.GetValue("Угар, %");
                double TopLoss =  p.GetValue("Головная обрезь, %");
                double BottomLoss = p.GetValue("Донная обрезь, %");

                double HeadNarrowSize = UpNarrowSize - deltaA * 2;
                double HeadWideSize = UpWideSize - deltaB * 2;
                // Расчет надставки
                r.SetValue("Узкая сторона низа надставки, мм", HeadNarrowSize);
                r.SetValue("Широкая сторона низа надставки, мм", HeadWideSize);
                double TopNarrowSize = HeadNarrowSize - 0.02 * HeadNarrowCone * HeadHeight;
                double TopWideSize = HeadWideSize - 0.02 * HeadWideCone * HeadHeight;
                r.SetValue("Узкая сторона верха надставки, мм", TopNarrowSize);
                r.SetValue("Широкая сторона верха надставки, мм", TopWideSize);
                double HeadVolume = (HeadHeight / 6) * (HeadWideSize * HeadNarrowSize + TopWideSize * TopNarrowSize + 
                    (HeadWideSize + TopWideSize) * (HeadNarrowSize + TopNarrowSize)) 
                    - 0.215 * Math.Pow((2 * RadiusHead), 2) * HeadHeight;
                r.SetValue("Объем прибыльной части, мм³", HeadVolume);

                double WideAngle = Math.Atan(((HeadWideSize - TopWideSize) / 2) / HeadHeight) / Math.PI * 180;
                r.SetValue("Угол наклона по широкой стороне надставки, ⁰", WideAngle);

                double NarrowAngle = Math.Atan(((HeadNarrowSize - TopNarrowSize) / 2) / HeadHeight) / Math.PI * 180;
                r.SetValue("Угол наклона по узкой стороне надставки, ⁰", NarrowAngle);
                
                //Расчет конусных частей
                double MediumNarrowSize = DownNarrowSize + 0.02 * DownNarrowCone * DownHeight;
                double MediumWideSize = DownWideSize + 0.02 * DownWideCone * DownHeight;
                r.SetValue("Узкая сторона перехода между конусными частями, мм", MediumNarrowSize);
                r.SetValue("Широкая сторона перехода между конусными частями, мм", MediumWideSize);
                double UpNarrowCone = (UpNarrowSize - MediumNarrowSize) / UpHeight * 50;
                double UpWideCone = (UpWideSize - MediumWideSize) / UpHeight * 50;
                if (UpHeight > 1e-12)
                {
                    r.SetValue("Конусность узкой стороны верхней конусной части, %", UpNarrowCone);
                    r.SetValue("Конусность широкой стороны верхней конусной части, %", UpWideCone);
                }
                else
                {
                    r.SetValue("Конусность узкой стороны верхней конусной части, %", 0);
                    r.SetValue("Конусность широкой стороны верхней конусной части, %", 0);
                }
                double UpVolume = (UpHeight / 6) * (UpWideSize * UpNarrowSize + MediumWideSize * MediumNarrowSize +
                (UpWideSize + MediumWideSize) * (UpNarrowSize + MediumNarrowSize))
                - 0.215 * Math.Pow((2 * Radius), 2) * UpHeight;
                if (UpHeight > 1e-12)  r.SetValue("Объем верхней конусной части, мм³", UpVolume);
                else
                {
                    UpVolume = 0;
                    r.SetValue("Объем верхней конусной части, мм³", UpVolume);
                }

                double DownVolume = (DownHeight / 6) * (DownWideSize * DownNarrowSize + MediumWideSize * MediumNarrowSize +
                (DownWideSize + MediumWideSize) * (DownNarrowSize + MediumNarrowSize))
                - 0.215 * Math.Pow((2 * Radius), 2) * DownHeight;
                if (DownHeight > 1e-12) r.SetValue("Объем нижней конусной части, мм³", DownVolume);
                else
                {
                    DownHeight = 0;
                    r.SetValue("Объем нижней конусной части, мм³", DownVolume);
                }

                //Расчет донной части
                double R1 = DownWideSize / 2;
                double R2 = DownNarrowSize / 2;
                
                double CorkBevel = R1 - Math.Sqrt(R1 * R1 - Math.Pow(CorkDiameter / 2, 2));
                r.SetValue("Высота скоса дна, мм", CorkBevel);
                
                double BottomHeight = R1 - CorkBevel;
                r.SetValue("Высота донной части, мм", BottomHeight);
                r.SetValue("Высота центра дуги большего радиуса, мм", BottomHeight);
                double NarrowArkPosition = Math.Sqrt(R2 * R2 - Math.Pow(CorkDiameter/2,2));
                r.SetValue("Высота центра дуги меньшего радиуса, мм", NarrowArkPosition);

                double BottomVolume = (8/3)*Math.Pow(R1,3)-(Math.PI/3)*Math.Pow(CorkBevel,2)*(3*R1-CorkBevel)-(1/3)*((R1-R2)*(4*R1-CorkDiameter)+0.86*Math.Pow(Radius,2))*BottomHeight;
                r.SetValue("Объем донной части, мм³", BottomVolume);

                //Итоги расчета
                double Volume = HeadVolume + UpVolume + DownVolume + BottomVolume;
                r.SetValue("Объем слитка, мм³", Volume);
                double FullWeight = Volume / 1e9 * Density;
                r.SetValue("Масса полного слитка, т", FullWeight);
                double k = (BurningLoss + TopLoss + BottomLoss);
                r.SetValue("Расходный коэффициент, %", k);
                double SuitableWeight = FullWeight * (1-k/100);
                r.SetValue("Масса годной части слитка, т", SuitableWeight);
                double Relation = HeadVolume / Volume * 100;
                r.SetValue("Отношение объемов прибыли и слитка, %", Relation);
            }
            catch { }
        }

        #endregion

        #region расчет многогранного слитка

        private void DoOctagonCalculation()
        {
            try
            {
                
                double Edges = p.GetValue("Число граней, шт");
                double UpInternalDiameter = p.GetValue("Верхний диаметр вписанной окружности, мм");
                double DownInternalDiameter = p.GetValue("Нижний диаметр вписанной окружности, мм");
                double HeadHeight = p.GetValue("Высота налива надставки, мм");
                double HeadCone = p.GetValue("Конусность надставки, %");
                double delta = p.GetValue("Сужение радиуса надставки относительно вписанной окружности верха слитка, мм");

                double SphereHeight = p.GetValue("Высота сферической части, мм");
                double CorkDiameter = p.GetValue("Диаметр пробки, мм");
                double TotalConeHeight = p.GetValue("Общая высота до нижней сферы, мм");
                double CuttedHeight = p.GetValue("Высота нижней срезающей поверхности, мм");
                double Rstart = UpInternalDiameter / 2;
                double Rbottom = DownInternalDiameter / 2;
                double r1 = p.GetValue("Верхний радиус закругления, мм");
                double r2 = p.GetValue("Нижний радиус закругления, мм");
                double Rout1 = p.GetValue("Верхий радиус вогнутости, мм");
                double Rout2 = p.GetValue("Нижний радиус вогнутости, мм");
                double Density = p.GetValue("Плотность металла, т/м³");
                double BurningLoss = p.GetValue("Угар, %");
                double TopLoss = p.GetValue("Головная обрезь, %");
                double BottomLoss = p.GetValue("Донная обрезь, %");


                //Расчет надставки
                double HeadLowDiameter = UpInternalDiameter - 2*delta;
                r.SetValue("Диаметр низа надставки, мм", HeadLowDiameter);
                double HeadHiDiameter = HeadLowDiameter - 0.02 * HeadCone * HeadHeight;
                r.SetValue("Диаметр верха надставки, мм", HeadHiDiameter);
                double HeadVolume = Math.PI * HeadHeight * (Math.Pow(HeadHiDiameter/2, 2) + Math.Pow(HeadLowDiameter/2, 2) + HeadHiDiameter * HeadLowDiameter/4) / 3;
                r.SetValue("Объем прибыльной части, мм³", HeadVolume);

                //Расчет конической части до среза
                double NoCuttedHeight = TotalConeHeight - CuttedHeight;
                r.SetValue("Высота части с целыми гранями, мм", NoCuttedHeight);
                double Cone = (UpInternalDiameter - DownInternalDiameter) / TotalConeHeight * 50;
                r.SetValue("Конусность, %", Cone);
                double Rcut = Rstart - NoCuttedHeight / TotalConeHeight * (Rstart - Rbottom);
                r.SetValue("Диаметр вписанной окружности на уровне начала среза, мм", Rcut*2);
                double rcut = r1 - NoCuttedHeight / TotalConeHeight * (r1 - r2);
                r.SetValue("Радиус закругления на уровне начала среза, мм", rcut);
                double Routcut = Rout1 - NoCuttedHeight / TotalConeHeight * (Rout1 - Rout2);
                r.SetValue("Радиус вогнутости на уровне начала среза, мм", Routcut);
                
                double RoutsideHi = GetOutCircle(r1, Rstart, Rout1, Edges);
                r.SetValue("Верхний диаметр описанной окружности, мм", RoutsideHi * 2);

                double RoutsideCut = GetOutCircle(rcut, Rcut, Routcut, Edges);
                r.SetValue("Диаметр описанной окружности на уровне начала среза, мм", RoutsideCut * 2);
                
                double StartSurf = 0;
                double NoCutVolume = 0;
                
                for (double CurrentHeight = 0; CurrentHeight <= NoCuttedHeight; CurrentHeight+=NoCuttedHeight/20)
                {
                    double Rc = Rstart - CurrentHeight / TotalConeHeight * (Rstart - Rbottom);
                    double rc = r1 - CurrentHeight / TotalConeHeight * (r1 - r2);
                    double Routc = Rout1 - CurrentHeight / TotalConeHeight * (Rout1 - Rout2);
                    double surf = GetPolygonSurface(rc, Rc, Routc, 0, Edges);
                    if (CurrentHeight == 0)
                    {
                        StartSurf = surf;
                    }
                    else
                    {
                        NoCutVolume += (NoCuttedHeight / 20) * (surf + StartSurf) / 2;
                        StartSurf = surf;
                    }
                }
                r.SetValue("Объем части с целыми гранями, мм³", NoCutVolume);

                double CutVolume = 0;

                if (CuttedHeight > 0)
                {
                    StartSurf = 0;
                    for (double CurrentHeight = NoCuttedHeight; CurrentHeight <= TotalConeHeight; CurrentHeight += CuttedHeight / 20)
                    {
                        double Rc = Rstart - CurrentHeight / TotalConeHeight * (Rstart - Rbottom);
                        double rc = r1 - CurrentHeight / TotalConeHeight * (r1 - r2);
                        double Routc = Rout1 - CurrentHeight / TotalConeHeight * (Rout1 - Rout2);
                        double RCutting = RoutsideCut - (CurrentHeight - NoCuttedHeight) / CuttedHeight * (RoutsideCut - Rbottom);

                        double surf = GetPolygonSurface(rc, Rc, Routc, RCutting, Edges);

                        if (CurrentHeight == NoCuttedHeight)
                        {
                            StartSurf = surf;
                        }
                        else
                        {
                            CutVolume += (CuttedHeight / 20) * (surf + StartSurf) / 2;
                            StartSurf = surf;
                        }
                    }
                }
                r.SetValue("Объем части со срезаемыми гранями, мм³", CutVolume);
                r.SetValue("Объем всей конической части, мм³", CutVolume + NoCutVolume);


                //Расчет донной сферической части
                double a = CorkDiameter / 2;
                double b = DownInternalDiameter / 2;
                double h = SphereHeight;
                double SphereRadius = Math.Sqrt(Math.Pow((b*b-a*a-h*h)/(2*h),2)+b*b);
                double SphereVolume = (Math.PI * SphereHeight / 6) * (3 * Math.Pow(CorkDiameter / 2, 2) + 3 * Math.Pow(DownInternalDiameter / 2, 2) + Math.Pow(SphereHeight, 2));
                double h2 = Math.Sqrt(SphereRadius * SphereRadius - b * b);
                if (a * a + h * h > b * b)
                {
                    SphereRadius = Double.NaN;
                    h2 = Double.NaN;
                    SphereVolume = Double.NaN;
                }
                r.SetValue("Радиус сферы, мм", SphereRadius);
                r.SetValue("Расстояние от центра пробки до центра сферы, мм", h+h2);
                r.SetValue("Объем донной сферической части, мм³", SphereVolume);

                double Volume = HeadVolume + SphereVolume + CutVolume + NoCutVolume;

                r.SetValue("Объем слитка, мм³", Volume);
                double FullWeight = Volume / 1e9 * Density;
                r.SetValue("Масса полного слитка, т", FullWeight);
                double k = (BurningLoss + TopLoss + BottomLoss);
                r.SetValue("Расходный коэффициент, %", k);
                double SuitableWeight = FullWeight * (1 - k / 100);
                r.SetValue("Масса годной части слитка, т", SuitableWeight);
            }
            catch { }
        }

        public double GetPolygonSurface(double r, double R, double Rout, double Rcut, double edgeNumber)
        {
            double symmetry = edgeNumber * 2d;
            double a = R + Rout;
            double c = r + Rout;
            double a225 = (1.0d / edgeNumber) * Math.PI;
            double gamma = Math.PI - Math.Asin(a * Math.Sin(a225) / c);
            double alpha = (1.0d - (1.0d / edgeNumber)) * Math.PI - gamma;
            double beta = Math.PI - gamma;
            double b = Math.Sin(alpha) * c / Math.Sin(a225);
            double p = 0.5 * (a + b + c);
            double TriangleSurface = Math.Sqrt(p * (p - a) * (p - b) * (p - c));
            double LargeRadiusSurface = 0.5 * Rout * Rout * alpha;
            double SmallRadiusSurface = 0.5 * r * r * beta;
            double Surface = (TriangleSurface - LargeRadiusSurface + SmallRadiusSurface) * symmetry;
            double xc = 0, yc = 0, ac = 0, bc = 0, Sr = 0, SRcut = 0, xl = 0, yl = 0, hi = 0, psi = 0;

            if (Rcut == 0) return Surface;
            {
                if (Rcut > b + r) return Surface;
                double x = R + Rout - Rout * Math.Cos(alpha);
                double y = Rout * Math.Sin(alpha);
                double Rl = Math.Sqrt(x * x + y * y);
                if (Rcut > Rl)
                {
                    //Срез малого радиуса закругления
                    xc = (Rcut * Rcut - r * r + R * R) / (2 * R);
                    yc = Math.Sqrt(Math.Abs(Rcut * Rcut - xc * xc));
                    double lc = Math.Sqrt(xc * xc + yc * yc);
                    ac = Math.Atan(yc / xc);
                    bc = Math.Atan(yc / (xc - R));
                    SRcut = 0.5 * (Rcut * Rcut * ac - xc * yc);
                    Sr = 0.5 * (r * r * bc - (xc - R) * yc);
                    double CuttedS = (Sr - SRcut) * symmetry;
                    return Surface - CuttedS;
                }
                else
                {
                    //Срез малого радиуса закругления
                    xc = (Rl * Rl - r * r + R * R) / (2 * R);
                    yc = Math.Sqrt(Math.Abs(Rl * Rl - xc * xc));
                    double lc = Math.Sqrt(xc * xc + yc * yc);
                    ac = Math.Atan(yc / xc);
                    bc = Math.Atan(yc / (xc - R));
                    SRcut = 0.5 * (Rl * Rl * ac - xc * yc);
                    Sr = 0.5 * (r * r * bc - (xc - R) * yc);
                    double CuttedSSmall = (Sr - SRcut) * symmetry;
                    // Остаток по впадине...
                    //Сегментная часть
                    double CuttedRingSegment = symmetry/2 * ac * (Rl * Rl - Rcut * Rcut);
                    //Криволинейный треугольник
                    xl = 0.5 * (Rcut * Rcut - Rout * Rout + Math.Pow((R + Rout), 2)) / (R + Rout);
                    yl = Math.Sqrt(Rcut * Rcut - xl * xl);
                    hi = Math.Atan(yl / xl);
                    psi = Math.Atan(y / x) - Math.Atan(yl / xl);

                    //double S1 = 0.5 * Rcut * Rcut * hi - 0.5 * xl * yl;
                    double S2 = 0.5 * Math.Pow(Rl - Rcut, 2) * psi;
                    double CuttingTriangle = symmetry * (S2 / 2) * 0.6;

                    return Surface - CuttedSSmall - CuttedRingSegment - CuttingTriangle;
                }
            }
        }

        public double GetOutCircle(double r, double R, double Rout, double edgeNumber)
        {
            double a = R + Rout;
            double c = r + Rout;
            double a225 = (1.0d / edgeNumber) * Math.PI;
            double gamma = Math.PI - Math.Asin(a * Math.Sin(a225) / c);
            double alpha = (1.0d - (1.0d / edgeNumber)) * Math.PI - gamma;
            double b = Math.Sin(alpha) * c / Math.Sin(a225);
            return b + r;
        }

        #endregion

        #region расчет многогранного слитка с кюмпельной частью

        private void DoPolygonCalculation()
        {
            try
            {
                double Edges = p.GetValue("Число граней, шт");

                double HeadHeight = p.GetValue("Высота налива надставки, мм");
                double HeadCone = p.GetValue("Конусность надставки, %");
                double deltaHead = p.GetValue("Сужение радиуса надставки относительно описанной окружности многогранника, мм");
                
                double UpExternalDiameter = p.GetValue("Верхний диаметр описанной окружности многогранника, мм");
                double DownExternalDiameter = p.GetValue("Нижний диаметр описанной окружности многогранника, мм");
                
                double Upr = p.GetValue("Верхний радиус закругления, мм");
                double Downr = p.GetValue("Нижний радиус закругления, мм");

                double UpDelta = p.GetValue("Верхняя глубина вогнутости, мм");
                double DownDelta = p.GetValue("Нижняя глубина вогнутости, мм");

                double PolygonalHeight = p.GetValue("Высота многогранной части, мм");

                double CumpelTopHeight = p.GetValue("Высота верхней части, мм");
                double CumpelTopUpDiameter = p.GetValue("Верхний диаметр верхней части, мм");
                double CumpelTopDownDiameter = p.GetValue("Нижний диаметр верхней части, мм");
                double CumpelBottomHeight = p.GetValue("Высота нижней части, мм");
                double CumpelBottomUpDiameter = p.GetValue("Верхний диаметр нижней части, мм");
                double CumpelBottomDownDiameter = p.GetValue("Нижний диаметр нижней части, мм");
                
                double Density = p.GetValue("Плотность металла, т/м³");
                double BurningLoss = p.GetValue("Угар, %");
                double TopLoss = p.GetValue("Головная обрезь, %");
                double BottomLoss = p.GetValue("Донная обрезь, %");
                
                //Расчет надставки

                double HeadLowDiameter = UpExternalDiameter - 2.0d * deltaHead;
                r.SetValue("Диаметр низа надставки, мм", HeadLowDiameter);
                double HeadHiDiameter = HeadLowDiameter - 0.02 * HeadCone * HeadHeight;
                r.SetValue("Диаметр верха надставки, мм", HeadHiDiameter);
                double HeadVolume = Math.PI * HeadHeight * (Math.Pow(HeadHiDiameter / 2, 2) + Math.Pow(HeadLowDiameter / 2, 2) + HeadHiDiameter * HeadLowDiameter / 4) / 3;
                r.SetValue("Объем прибыльной части, мм³", HeadVolume);

                //Расчет конической части
                double Cone = (UpExternalDiameter - DownExternalDiameter) / PolygonalHeight * 50;
                r.SetValue("Конусность, %", Cone);
                r.SetValue("Радиус вогнутости по верху, мм", GetHRadius(Upr, UpExternalDiameter, UpDelta, Edges));
                r.SetValue("Радиус вогнутости по низу, мм", GetHRadius(Downr, DownExternalDiameter, DownDelta, Edges));

                double StartSurf = 0;
                double SemiEdgeConeVolume = 0;
                double parts = 200.0d;
                double surf = 0d;
                double surfup = 0d;
                for (double CurrentHeight = 0; Math.Abs(CurrentHeight - PolygonalHeight) > 1e-7; CurrentHeight += PolygonalHeight / parts)
                {
                    double Rb = (UpExternalDiameter - CurrentHeight / PolygonalHeight * (UpExternalDiameter - DownExternalDiameter))/2.0d;
                    double r = Upr - CurrentHeight / PolygonalHeight * (Upr - Downr);
                    double h = UpDelta - CurrentHeight / PolygonalHeight * (UpDelta - DownDelta);
                    var alpha = Math.PI / Edges;
                    double R = GetHRadius(r, Rb, h, alpha);
                    var beta = GetBeta(r, Rb, h, alpha);

                    surf = GetPolygonSurfaceCumpel(r, R, Rb, h, alpha, beta);
                    if (CurrentHeight == 0)
                    {
                        StartSurf = surf;
                        surfup = surf;
                    }
                    else
                    {
                        SemiEdgeConeVolume += (PolygonalHeight / parts) * (surf + StartSurf) / 2;
                        StartSurf = surf;
                    }
                }
                var ConeVolume = SemiEdgeConeVolume * 2.0d * Edges;
                r.SetValue("Объем конической части, мм³", ConeVolume);
                r.SetValue("Площадь нижнего сечения конической части, мм²", surf);
                r.SetValue("Площадь верхнего сечения конической части, мм²", surfup);
                //Расчет кюмпельной части

                var volume1 = 1.0d / 12.0d * Math.PI * CumpelTopHeight * (CumpelTopUpDiameter * CumpelTopUpDiameter + CumpelTopUpDiameter * CumpelTopDownDiameter + CumpelTopDownDiameter * CumpelTopDownDiameter);
                var volume2 = 1.0d / 12.0d * Math.PI * CumpelBottomHeight * (CumpelBottomUpDiameter * CumpelBottomUpDiameter + CumpelBottomUpDiameter * CumpelBottomDownDiameter + CumpelBottomDownDiameter * CumpelBottomDownDiameter);

                var cumpelVolume = volume1 + volume2;

                r.SetValue("Объем кюмпельной части, мм³", cumpelVolume);

                double Volume = HeadVolume + cumpelVolume + ConeVolume;

                r.SetValue("Объем слитка, мм³", Volume);
                double FullWeight = Volume / 1e9 * Density;
                r.SetValue("Масса полного слитка, т", FullWeight);
                double k = (BurningLoss + TopLoss + BottomLoss);
                r.SetValue("Расходный коэффициент, %", k);
                double SuitableWeight = FullWeight * (1 - k / 100);
                r.SetValue("Масса годной части слитка, т", SuitableWeight);
            }
            catch { }
        }

        public double GetHRadius(double r, double Rb, double h, double alpha)
        {
            var m = (Rb - r) * Math.Sin(alpha);
            var R = h / (1 - Math.Cos(2 * Math.Atan(h/m))) - r;
            return R;
        }

        public double GetBeta(double r, double Rb, double h, double alpha)
        {
            var m = (Rb - r) * Math.Sin(alpha);
            var beta = 2 * Math.Atan(h / m);
            return beta;
        }

        public double GetPolygonSurfaceCumpel(double r, double R, double Rb, double h, double alpha, double beta)
        {
            var st = 0.5 * (r + R) * (Rb - r) * Math.Sin(Math.PI - alpha - beta);
            var sr = r * r * (alpha + beta) / 2;
            var sR = R * R * (beta) / 2;
            return sr + st - sR;
        }

        public double GetOutCircleCumpel(double r, double R, double Rout, double edgeNumber)
        {
            double a = R + Rout;
            double c = r + Rout;
            double a225 = (1.0d / edgeNumber) * Math.PI;
            double gamma = Math.PI - Math.Asin(a * Math.Sin(a225) / c);
            double alpha = (1.0d - (1.0d / edgeNumber)) * Math.PI - gamma;
            double b = Math.Sin(alpha) * c / Math.Sin(a225);
            return b + r;
        }

        #endregion

        #region расчет прямоугольного слитка с вогнутыми или выпуклыми гранями

        private void DoRoundedCalculation()
        {
            double UpNarrowSize = p.GetValue("Узкая сторона верхней конусной части, мм");
            double UpWideSize = p.GetValue("Широкая сторона верхней конусной части, мм");
            double DownNarrowSize = p.GetValue("Узкая сторона нижней конусной части, мм");
            double DownWideSize = p.GetValue("Широкая сторона нижней конусной части, мм");
            double HeadHeight = p.GetValue("Высота налива надставки, мм");
            double UpHeight = p.GetValue("Высота верхней конусной части, мм");
            if (UpHeight == 0) UpHeight = 1e-12;
            double DownHeight = p.GetValue("Высота нижней конусной части, мм");
            if (DownHeight == 0) DownHeight = 1e-12;
            double deltaA = p.GetValue("Сужение узкой стороны надставки относительно слитка, мм");
            double deltaB = p.GetValue("Сужение широкой стороны надставки относительно слитка, мм");
            double HeadNarrowCone = p.GetValue("Конусность узкой стороны надставки, %");
            double HeadWideCone = p.GetValue("Конусность широкой стороны надставки, %");
            double DownNarrowCone = p.GetValue("Конусность узкой стороны нижней конусной части, %");
            double DownWideCone = p.GetValue("Конусность широкой стороны нижней конусной части, %");
            double RadiusHead = p.GetValue("Радиус закругления надставки, мм");
            double rTop = p.GetValue("Радиус закругления в верхней части, мм");
            double rBottom = p.GetValue("Радиус закругления в нижней части, мм");

            double deltaTopWide = p.GetValue("δ широкой стороны верха, мм");
            double deltaTopNarrow = p.GetValue("δ узкой стороны верха, мм");
            double deltaBottomNarrow = p.GetValue("δ узкой стороны низа, мм");
            double deltaBottomWide = p.GetValue("δ широкой стороны низа, мм");

            double BottomHeight = p.GetValue("Высота донной части, мм");
            if (BottomHeight == 0) BottomHeight = 1e-8;
            double BottomRadius = p.GetValue("Радиус закругления донной части, мм");
            double EdgeBottomRadius = p.GetValue("Радиус закругления ребра донной части, мм");

            double Density = p.GetValue("Плотность металла, т/м³");
            double BurningLoss = p.GetValue("Угар, %");
            double TopLoss = p.GetValue("Головная обрезь, %");
            double BottomLoss = p.GetValue("Донная обрезь, %");

            double HeadNarrowSize = UpNarrowSize - deltaA * 2;
            double HeadWideSize = UpWideSize - deltaB * 2;
            
            // Расчет надставки
            r.SetValue("Узкая сторона низа надставки, мм", HeadNarrowSize);
            r.SetValue("Широкая сторона низа надставки, мм", HeadWideSize);
            double TopNarrowSize = HeadNarrowSize - 0.02 * HeadNarrowCone * HeadHeight;
            double TopWideSize = HeadWideSize - 0.02 * HeadWideCone * HeadHeight;
            r.SetValue("Узкая сторона верха надставки, мм", TopNarrowSize);
            r.SetValue("Широкая сторона верха надставки, мм", TopWideSize);
            double HeadVolume = (HeadHeight / 6) * (HeadWideSize * HeadNarrowSize + TopWideSize * TopNarrowSize +
                (HeadWideSize + TopWideSize) * (HeadNarrowSize + TopNarrowSize))
                - 0.215 * Math.Pow((2 * RadiusHead), 2) * HeadHeight;
            r.SetValue("Объем прибыльной части, мм³", HeadVolume);

            //Расчет конусных частей
            double MediumNarrowSize = DownNarrowSize + 0.02 * DownNarrowCone * DownHeight;
            double MediumWideSize = DownWideSize + 0.02 * DownWideCone * DownHeight;
            
            r.SetValue("Узкая сторона перехода между конусными частями, мм", MediumNarrowSize);
            r.SetValue("Широкая сторона перехода между конусными частями, мм", MediumWideSize);
            
            double UpNarrowCone = (UpNarrowSize - MediumNarrowSize) / UpHeight * 50;
            double UpWideCone = (UpWideSize - MediumWideSize) / UpHeight * 50;
            if (UpHeight > 1e-12)
            {
                r.SetValue("Конусность узкой стороны верхней конусной части, %", UpNarrowCone);
                r.SetValue("Конусность широкой стороны верхней конусной части, %", UpWideCone);
            }
            else
            {
                r.SetValue("Конусность узкой стороны верхней конусной части, %", 0);
                r.SetValue("Конусность широкой стороны верхней конусной части, %", 0);
            }
            double deltaMediumWide = deltaTopWide - (deltaTopWide - deltaBottomWide) * UpHeight / (UpHeight + DownHeight);
            double deltaMediumNarrow = deltaTopNarrow - (deltaTopNarrow - deltaBottomNarrow) * UpHeight / (UpHeight + DownHeight);

            r.SetValue("δ узкой стороны перехода, мм", deltaMediumWide);
            r.SetValue("δ широкой стороны перехода, мм", deltaMediumNarrow);
            
            double RTopWide = (Math.Pow(UpWideSize / 2 - rTop, 2) + deltaTopWide * deltaTopWide) / (2 * deltaTopWide);
            double RTopNarrow = (Math.Pow(UpNarrowSize / 2 - rTop, 2) + deltaTopNarrow * deltaTopNarrow) / (2 * deltaTopNarrow);
            double RMediumWide = (Math.Pow(MediumWideSize / 2 - rTop, 2) + deltaMediumWide * deltaMediumWide) / (2 * deltaMediumWide);
            double RMediumNarrow = (Math.Pow(MediumNarrowSize / 2 - rTop, 2) + deltaMediumNarrow * deltaMediumNarrow) / (2 * deltaMediumNarrow);
            double RBottomWide = (Math.Pow(DownWideSize / 2 - rTop, 2) + deltaBottomWide * deltaBottomWide) / (2 * deltaBottomWide);
            double RBottomNarrow = (Math.Pow(DownNarrowSize / 2 - rTop, 2) + deltaBottomNarrow * deltaBottomNarrow) / (2 * deltaBottomNarrow);
            
            r.SetValue("Радиус выпуклости широкой грани в верхней части, мм", RTopWide);
            r.SetValue("Радиус выпуклости узкой грани в верхней части, мм", RTopNarrow);
            r.SetValue("Радиус выпуклости широкой грани в нижней части, мм", RBottomWide);
            r.SetValue("Радиус выпуклости узкой грани в нижней части, мм", RBottomNarrow);
            r.SetValue("Радиус выпуклости широкой грани в переходе, мм", RMediumWide);
            r.SetValue("Радиус выпуклости узкой грани в переходе, мм", RMediumNarrow);

            double rMedium = rTop - (rTop - rBottom) * UpHeight / (UpHeight + DownHeight);
            double StartSurf = 0;
            double UpVolume = 0;

            for (double CurrentHeight = 0; CurrentHeight <= UpHeight; CurrentHeight += UpHeight / 50)
            {
                double deltawide = deltaTopWide - CurrentHeight / UpHeight * (deltaTopWide - deltaMediumWide);
                double deltanarrow = deltaTopNarrow - CurrentHeight / UpHeight * (deltaTopNarrow - deltaMediumNarrow);
                double rc = rTop - CurrentHeight / UpHeight * (rTop - rMedium);
                double a  = UpWideSize - CurrentHeight / UpHeight * (UpWideSize - MediumWideSize);
                double b = UpNarrowSize - CurrentHeight / UpHeight * (UpNarrowSize - MediumNarrowSize);
                double Rwide = (Math.Pow(a / 2 - rc, 2) + deltawide * deltawide) / (2 * deltawide);
                double Rnarrow = (Math.Pow(b / 2 - rc, 2) + deltanarrow * deltanarrow) / (2 * deltanarrow);
                
                double surf = GetRoundedSurface(rc, Rwide, Rnarrow, a, b, 0, 0);
                if (CurrentHeight == 0)
                {
                    StartSurf = surf;
                }
                else
                {
                    UpVolume += (UpHeight / 50) * (surf + StartSurf) / 2;
                    StartSurf = surf;
                }
            }
            if (UpHeight > 1e-12) r.SetValue("Объем верхней конусной части, мм³", UpVolume);
            else
            {
                UpVolume = 0;
                r.SetValue("Объем верхней конусной части, мм³", UpVolume);
            }

            StartSurf = 0;
            double DownVolume = 0;

            for (double CurrentHeight = 0; CurrentHeight <= DownHeight; CurrentHeight += DownHeight / 50)
            {
                double deltawide = deltaMediumWide - CurrentHeight / DownHeight * (deltaMediumWide - deltaBottomWide);
                double deltanarrow = deltaMediumNarrow - CurrentHeight / DownHeight * (deltaMediumNarrow - deltaBottomNarrow);
                double rc = rMedium - CurrentHeight / DownHeight * (rMedium - rBottom);
                double a = MediumWideSize - CurrentHeight / DownHeight * (MediumWideSize - DownWideSize);
                double b = MediumNarrowSize - CurrentHeight / DownHeight * (MediumNarrowSize - DownNarrowSize);
                double Rwide = (Math.Pow(a / 2 - rc, 2) + deltawide * deltawide) / (2 * deltawide);
                double Rnarrow = (Math.Pow(b / 2 - rc, 2) + deltanarrow * deltanarrow) / (2 * deltanarrow);
                
                double surf = GetRoundedSurface(rc, Rwide, Rnarrow, a, b, 0, 0);

                if (CurrentHeight == 0)
                {
                    StartSurf = surf;
                }
                else
                {
                    DownVolume += (DownHeight / 50) * (surf + StartSurf) / 2;
                    StartSurf = surf;
                }
            }
            if (DownVolume > 1e-12) r.SetValue("Объем нижней конусной части, мм³", DownVolume);
            else
            {
                DownVolume = 0;
                r.SetValue("Объем нижней конусной части, мм³", DownVolume);
            }

            StartSurf = 0;
            double BottomVolume = 0;
            double CuttedBottomHeight = Math.Sqrt(BottomRadius * BottomRadius - Math.Pow(BottomRadius - Math.Max(Math.Abs(deltaBottomNarrow), Math.Abs(deltaBottomWide)), 2));
            double BottomS1 = GetRoundedSurface(rBottom, RBottomWide, RBottomNarrow, DownWideSize, DownNarrowSize, 0, 0);
            double CuttedWidth = 2 * (BottomRadius - Math.Sqrt(BottomRadius * BottomRadius - CuttedBottomHeight * CuttedBottomHeight));
            double CuttedA = DownWideSize - CuttedWidth;
            double CuttedB = DownNarrowSize - CuttedWidth;
            double BottomS2 = GetRoundedSurface(EdgeBottomRadius, double.PositiveInfinity, double.PositiveInfinity, CuttedA, CuttedB, 0, 0);
            double BottomCuttedVolume = 0.5 * (BottomS1 + BottomS2) * CuttedBottomHeight;

            for (double CurrentHeight = CuttedBottomHeight; CurrentHeight <= BottomHeight; CurrentHeight += (BottomHeight - CuttedBottomHeight) / 20)
            {
                double Rwide = double.PositiveInfinity;
                double Rnarrow = double.PositiveInfinity;
                double rc = EdgeBottomRadius;
                double CuttedW = 2 * (BottomRadius - Math.Sqrt(BottomRadius * BottomRadius - CurrentHeight * CurrentHeight));
                double a = DownWideSize - CuttedW;
                double b = DownNarrowSize - CuttedW;

                double surf = GetRoundedSurface(rc, Rwide, Rnarrow, a, b, 0, 0);

                if (CurrentHeight == 0)
                {
                    StartSurf = surf;
                }
                else
                {
                    BottomVolume += ((BottomHeight - CuttedBottomHeight) / 20) * (surf + StartSurf) / 2;
                    StartSurf = surf;
                }
            }

            BottomVolume += BottomCuttedVolume;
            r.SetValue("Объем донной части, мм³", BottomVolume);

            double CuttedWidthAtPlate = 2 * (BottomRadius - Math.Sqrt(BottomRadius * BottomRadius - BottomHeight * BottomHeight));
            double CuttedPlateWide = DownWideSize - CuttedWidthAtPlate;
            double CuttedPlateNarrow = DownNarrowSize - CuttedWidthAtPlate;
            r.SetValue("Широкая сторона нижней площадки, мм", CuttedPlateWide);
            r.SetValue("Узкая сторона нижней площадки, мм", CuttedPlateNarrow);

            double Volume = HeadVolume + UpVolume + DownVolume + BottomVolume;
            r.SetValue("Объем слитка, мм³", Volume);
            double FullWeight = Volume / 1e9 * Density;
            r.SetValue("Масса полного слитка, т", FullWeight);
            double k = (BurningLoss + TopLoss + BottomLoss);
            r.SetValue("Расходный коэффициент, %", k);
            double SuitableWeight = FullWeight * (1 - k / 100);
            r.SetValue("Масса годной части слитка, т", SuitableWeight);
            double Relation = HeadVolume / Volume * 100;
            r.SetValue("Отношение объемов прибыли и слитка, %", Relation);

        }

        private double GetRoundedSurface(double r, double Rw, double Rn, double A, double B, double acut, double bcut)
        {
            double a = A/2 - r;
            double b = B/2 - r;

            double x1=0, y1=0;
            if (double.IsPositiveInfinity(Rw) || double.IsNaN(Rw))
            {
                x1 = a;
                y1 = b + r;
            }
            else if (Rw > 0)
            {
                double x = Math.Sqrt(Math.Pow(Rw - r, 2) - Math.Pow(a, 2)) - b;
                double alpha = Math.Atan(a / (b + x));
                x1 = Rw * Math.Sin(alpha);
                y1 = Rw * Math.Cos(alpha)-x;
            }
            else
            {
                double x = Math.Sqrt(Math.Pow(Rw-r,2)-Math.Pow(a,2));
                double alpha = Math.Atan(a / x);
                x1 = - Rw * Math.Sin(alpha);
                y1 = b + x + Rw * Math.Cos(alpha);
            }

            double x2 = 0, y2 = 0;
            if (double.IsInfinity(Rn) || double.IsNaN(Rn))
            {
                x2 = a + r;
                y2 = b;
            }
            else if (Rn > 0)
            {
                double x = Math.Sqrt(Math.Pow(Rn - r, 2) - Math.Pow(b, 2)) - a;
                double alpha = Math.Atan(b / (a + x));
                x2 = Rn * Math.Cos(alpha) - x;
                y2 = Rn * Math.Sin(alpha) ;
            }
            else
            {
                double x = Math.Sqrt(Math.Pow(Rn - r, 2) - Math.Pow(b, 2));
                double alpha = Math.Atan(b / x);
                x2 = a + x + Rn * Math.Cos(alpha);
                y2 = - Rn * Math.Sin(alpha);
            }

            double m = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            double S11 = x1 * y1;
            double S12 = (x2 - x1) * (y1 + y2) / 2;
            double S2 = GetCircleSegmentHalfSquare(Rw, x1);
            double S3 = GetCircleSegmentHalfSquare(Rn, y2);
            double S4 = GetCircleSegmentHalfSquare(r, m/2 )*2;

            double k = (b - y1) / (y2 - y1) - (a - x1) / (x2 - x1);
            if (k < 0) S4 = Math.PI * r * r - S4;

            double S = S11 + S12 + ((double.IsNaN(Rw))?0:Math.Sign(Rw) * S2) + ((double.IsNaN(Rn))?0:Math.Sign(Rn) * S3) + S4;

            return S*4;
        }

        double GetCircleSegmentHalfSquare(double R, double HalfHord)
        {
            if (R == 0 || double.IsInfinity(R) || double.IsNaN(R)) return 0;
            double n = Math.Sqrt(R * R - HalfHord * HalfHord);
            double S = Math.Atan(HalfHord/n) * 0.5 * R * R - HalfHord * n / 2;
            return S;
        }
        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription SenderDescription, Command CommandType, object CommandArgument, CommandCallBackDelegate CommandCallBack)
        {
            switch (CommandType)
            {
            
                case Command.InsertStoredCalculation:
                    MainCatalogueDataEditor.LoadAbstractItem((int)CommandArgument);
                    break;

                default:
                    if (CommandCallBack != null) CommandCallBack.Invoke(CommandType, CommandArgument, null);
                    break;
            } 
        }
        
        private void NewCalculationRectangle_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["RectangleInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["RectangleOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoRectangleCalculation();
            DoRectangleCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        private void EnableCatalogueButtons()
        {
            ExportToXlsx.IsEnabled = true;
            SaveCalculationButton.IsEnabled = true;
            SaveAsCalculationButton.IsEnabled = true;
        }

        private void NewCalculationOctagon_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["OctagonInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["OctagonOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoOctagonCalculation();
            DoOctagonCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        private void NewCalculationPolygon_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["PolygonInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["PolygonOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoPolygonCalculation();
            DoPolygonCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        private void NewCalculationRectangleRounded_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["RoundedRectangleInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["RoundedRectangleOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoRoundedCalculation();
            DoRoundedCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        #region Сохранение расчета
        private void SaveCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainCatalogueDataEditor.CheckName())
            {
                MessageBox.Show("Для сохранения выберите папку и введите наименование слитка");
                return;
            }
            TextImageButtonBase tibb = sender as TextImageButtonBase;
            NewItem = tibb.Tag.ToString() == "NewItem"; 
            
            SaveCalculationButton.IsEnabled = false;
            SaveAsCalculationButton.IsEnabled = false;
            Task task = new Task(new string[] { "Сохранение расчета" }, "Сохранение расчета", TaskPanel); task.SetState(0, TaskState.Processing);
            task.CustomData = sender;
            task.SetState(0, TaskState.Processing);
            ItemDescription CatalogueData = CatalogueOperations.IngotCalculationToItem(p, r);
            this.Dispatcher.BeginInvoke(delegate()
            {
                MainCatalogueDataEditor.SaveAbstractItem(CatalogueData, task, true, NewItem);
            });
        }
        
        private void MainCatalogueDataEditor_CatalogueItemSaved(object o, AQCatalogueLibrary.CatalogueItemOperationEventArgs e)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Task task = (Task)e.State;
                task.SetState(0 , TaskState.Ready);
                SaveCalculationButton.IsEnabled = true;
                SaveAsCalculationButton.IsEnabled = true;
                DeleteCalculation.IsEnabled = true;
            });
        }
        #endregion

        #region Удаление расчета
        private void DeleteCalculation_Click(object sender, RoutedEventArgs e)
        {
            Task task = new Task(new string[] { "Удаление\r\nиз каталога" }, "Удаление запроса", TaskPanel);
            task.SetState(0, TaskState.Processing);
            DeleteCalculation.IsEnabled = false;
            MainCatalogueDataEditor.DeleteQuery(task, false);
        }

        private void MainCatalogueDataEditor_CatalogueItemDeleted(object o, AQCatalogueLibrary.CatalogueItemOperationEventArgs e)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                Task task = (Task)e.State;
                task.SetState(0, TaskState.Ready);
                DeleteCalculation.IsEnabled = false;
            });
        }
        #endregion

        #region Загрузка расчета

        private void MainCatalogueDataEditor_CatalogueItemLoaded(object o, CatalogueItemOperationEventArgs e)
        {
            ItemDescription Item = e.Cd as ItemDescription;
            if (Item != null)
            {
                InteractiveParameters P = null;
                InteractiveResults R = null;
                CatalogueOperations.ItemToIngot(Item, out P, out R);
                p = P;
                r = R;
                switch (p.AttachedCalculatorName)
                {
                    case "Прямоугольный слиток с двойной конусностью":
                        foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoRectangleCalculation(); 
                        break;
                    case "Восьмигранный кузнечный слиток":
                        foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoPolygonCalculation();
                        break;
                    case "Прямоугольный слиток с двойной конусностью и выпуклостью/вогнутостью":
                        foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoRoundedCalculation();
                        break;
                    default:
                        break;
                }
                CalculationPanel.Visibility = Visibility.Visible;
                EnableCatalogueButtons();
            }
        }
        #endregion

        #region Экспорт в Xlsx
        
        private void ExportToXlsx_Click(object sender, RoutedEventArgs e)
        {
            CalculationService cs = Services.GetCalculationService();
            
            Task task = new Task(new string[] { "Формирование результата", "Формирование файла Xlsx" }, "Экспорт в Xlsx", TaskPanel);
            task.SetState(0, TaskState.Processing);
            SLDataTable sl = new SLDataTable();
            sl.ColumnNames = new List<string>();
            sl.ColumnNames.Add("Параметр");
            sl.ColumnNames.Add("Значение");
            sl.DataTypes = new List<string>();
            sl.DataTypes.Add("String");
            sl.DataTypes.Add("Double");
            sl.TableName = "Расчет";
            sl.Table = new List<SLDataRow>();

            foreach (var a in p.Children)
            {
                SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                dr.Row.Add(a.Caption);
                dr.Row.Add(a.CurrentValue);
                sl.Table.Add(dr);
            }
            foreach (var a in r.Children)
            {
                SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                dr.Row.Add(a.Caption);
                dr.Row.Add(a.CurrentValue);
                sl.Table.Add(dr);
            }

            List<SLDataTable> sldt = new List<SLDataTable>();
            sldt.Add(sl);
            cs.BeginFillDataWithSLTables(sldt, (iar) =>
            {
                StepResult sr = cs.EndFillDataWithSLTables(iar);
                if (sr.Success)
                {
                    task.SetState(0, TaskState.Ready);
                    task.SetState(1, TaskState.Processing);
                    cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, (iar2) =>
                    {
                        sr = cs.EndGetAllResultsInXlsx(iar2);
                        task.SetState(1, TaskState.Ready);
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    cs.BeginDropData(sr.TaskDataGuid, null, null);
                }
            }, task);
        }
        #endregion

        private void MainCatalogueDataEditor_Loaded(object sender, RoutedEventArgs e)
        {
            MainCatalogueDataEditor.SetToEmptySelection();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReadyForAcceptSignals != null && !ReadyForAcceptSignalsProcessed) ReadyForAcceptSignals.Invoke(this, new ReadyForAcceptSignalsEventArg() { CommandCallBack = ReadySignalCallBack });
            ReadyForAcceptSignalsProcessed = true;
        }
    }

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class SmallMaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.SmallMaxValue - p.SmallMinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}