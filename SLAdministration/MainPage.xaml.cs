﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using OpenRiaServices.DomainServices.Client;
using System.Windows.Media.Imaging;
using ApplicationCore;
using AQControlsLibrary;
using AQMathClasses;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQChartLibrary;
using AQSelection;
using OpenRiaServices.Controls;

namespace SLAdministration
{
    public partial class MainPage : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private readonly List<RoleData> _roles = new List<RoleData>();
        private List<DayLoad> _data;
        List<SLDataTable> _chartSource;
        private ReportingDuplexServiceClient _rdsc;
        private readonly AQModuleDescription _aqmd;

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name }; 
            _roles = (from s in Security.AvailableRoles select new RoleData { IsChecked = false, Role = s }).ToList();  
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }

        private void AQ_UsersDomainDataSource_Loaded(object sender, RoutedEventArgs e)
        {
            ((RolesToStringValueConverter)Resources["RolesToStringValueConverter"]).DDS = AQ_UsersDomainDataSource;
        }

        
        #region Редактирование ролей
        private void SaveLogin_Click(object sender, RoutedEventArgs e)
        {
            var selectedRoles = _roles.Where(a => a.IsChecked).Select(b => b.Role).ToList();
            if (selectedRoles.Count==0) MessageBox.Show("Ни одной роли не выбрано.");
            else
            {
                var toDelete = WindowHeader.Text.StartsWith("Удаление");
                foreach (AQ_Users a in AQ_UsersDataGrid.SelectedItems)
                {
                    var existingRoles = Security.DecryptRoleString(a.Roles, a.Login);
                    foreach (var r in selectedRoles)
                    {
                        if (toDelete)
                        {
                            if (existingRoles.Contains(r)) existingRoles.Remove(r);
                        }
                        else
                        {
                            if (!existingRoles.Contains(r)) existingRoles.Add(r);
                        }
                    }
                    a.Roles = Security.EncryptRoleString(existingRoles, a.Login);
                }
                AQ_UsersDomainDataSource.SubmitChanges();
                AQ_UsersDataGrid.IsEnabled = true;
                EditRolesWindow.Visibility = Visibility.Collapsed;

            }
        }

        private void CancelEditLogin_Click(object sender, RoutedEventArgs e)
        {
            AQ_UsersDataGrid.IsEnabled = true;
            EditRolesWindow.Visibility = Visibility.Collapsed;
        }
        
        private void AQ_UsersDomainDataSource_SubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            AQ_UsersDomainDataSource.Load();
            AQ_UsersDataGrid.IsEnabled = true;
        }

        private void NewUserButton_Click(object sender, RoutedEventArgs e)
        {
            AQ_UsersDataGrid.IsEnabled = false;
            var aqu = new AQ_Users {Login = string.Format("{0}\\{1}", NewLoginDomain.Text, NewLogin.Text)};
            if (AQ_UsersDomainDataSource.DataView.Cast<AQ_Users>().Any(a => a.Login == aqu.Login))
            {
                MessageBox.Show(string.Format("Пользователь {0} уже есть.", aqu.Login));
                AQ_UsersDataGrid.IsEnabled = true;
                return;
            }
            var adc = (AdministrationDomainContext)AQ_UsersDomainDataSource.DomainContext;
            adc.GetUserName(aqu.Login, a =>
            {
                if (aqu.Login == a.Value)
                {
                    MessageBox.Show(string.Format("Иноформация по пользователю {0} не найдена. Логин добавлен без ФИО.", aqu.Login));
                    AQ_UsersDataGrid.IsEnabled = true;
                }
                else
                {
                    aqu.CachedUserName = a.Value;
                }
                AQ_UsersDomainDataSource.DataView.Add(aqu);
                AQ_UsersDomainDataSource.DataView.MoveCurrentTo(aqu);
                AQ_UsersDomainDataSource.SubmitChanges();
                NewLogin.Text = string.Empty;
            }, true);
        }

        private void RemoveRolesButton_Click(object sender, RoutedEventArgs e)
        {
            if (AQ_UsersDataGrid.SelectedItems.Count > 0)
            {
                foreach (var r in _roles) r.IsChecked = false;
                EditRolesWindow.Visibility = Visibility.Visible;
                WindowHeader.Text = string.Format("Удаление ролей у выбранных {0} пользователей. Выделенные роли будут удалены."
                    , AQ_UsersDataGrid.SelectedItems.Count);
                RoleList.ItemsSource = _roles;
            }
            else MessageBox.Show("Пользователи не отмечены для удаления ролей");
        }

        private void AddRolesButton_Click(object sender, RoutedEventArgs e)
        {
            if (AQ_UsersDataGrid.SelectedItems.Count > 0)
            {
                foreach (var r in _roles) r.IsChecked = false;
                EditRolesWindow.Visibility = Visibility.Visible;
                WindowHeader.Text = string.Format("Добавление ролей выбранным {0} пользователям. Выделенные роли будут добавлены."
                    , AQ_UsersDataGrid.SelectedItems.Count);
                RoleList.ItemsSource = _roles;
            }
            else MessageBox.Show("Пользователи не отмечены для добавления ролей");
        }

        private void RefreshLogButton_Click(object sender, RoutedEventArgs e)
        {
            AQ_UserLogDomainDataSource.Load();
        }

        private void RefreshRolesButton_Click(object sender, RoutedEventArgs e)
        {
            AQ_UsersDomainDataSource.Load();
        }
        #endregion

        #region Графики нагрузки
        private void LoadChart_Loaded(object sender, RoutedEventArgs e)
        {
            var checkedRb = (from c in LoadReportSelector.Children.Cast<RadioButton>() where c.IsChecked != null && c.IsChecked.Value select c).FirstOrDefault();
            if (checkedRb != null) DrawLoadReport(checkedRb.Tag.ToString());
        }

        private void DrawLoadReport(string keyWord)
        {
            foreach (var rb in LoadReportSelector.Children) ((RadioButton)rb).IsEnabled = false;
            var adc = new AdministrationDomainContext();
            var q = keyWord != "UserCount" ? adc.GetDayLoadQuery(keyWord) : adc.GetDayUserCountQuery();

            adc.Load(q, a =>
            {
                _data = a.Entities.OrderBy(s=>s.Date).ToList();
                var total = a.Entities.Sum(s => s.Load);
                var work =
                    a.Entities.Where(w => w.Date.DayOfWeek != DayOfWeek.Sunday && w.Date.DayOfWeek != DayOfWeek.Saturday)
                        .ToList();
                var totalwork = work.Sum(s => s.Load);
                var start = a.Entities.First().Date.ToString("dd.MM.yyyy");
                var end = a.Entities.Last().Date.ToString("dd.MM.yyyy");
                var count = a.Entities.Count();
                var countwork = work.Count();
                var avg = Math.Round(total / count, 0);
                var workavg = Math.Round(totalwork / countwork, 0);

                switch (keyWord)
                {
                    case "XLSX":
                        LoadChartTotals.Text = string.Format("За период с {0} по {1} выдано {2} отчетов в xlsx, в среднем {3} отчетов в день ({4} в будние дни).", start, end, total, avg, workavg);
                        break;
                    case "data readed":
                        LoadChartTotals.Text = string.Format("За период с {0} по {1} сформировано и отображено в окне системы {2} отчетов, в среднем {3} отчетов в день ({4} в будние дни).", start, end, total, avg, workavg);
                        break;
                    case "load":
                        LoadChartTotals.Text = string.Format("За период с {0} по {1} загрузка модулей системы выполнялась {2} раз, в среднем {3} раз в день ({4} в будние дни).", start, end, total, avg, workavg);
                        break;
                    case "SL Version":
                        LoadChartTotals.Text = string.Format("За период с {0} по {1} пользователи входили в систему {2} раз, в среднем {3} раз в день ({4} в будние дни).", start, end, total, avg, workavg);
                        break;
                    case "UserCount":
                        LoadChartTotals.Text = string.Format("За период с {0} по {1} каждые сутки системой пользовалось в среднем {2} человек ({3} в будние дни).", start, end, avg, workavg);
                        break;
                    default:
                        LoadChartTotals.Text = string.Empty;
                        break;
                }
                LoadChart.ShowTable = false;
                LoadChart.ShowLegend = false;
                LoadChart.ShowHorizontalGridlines = true;
                LoadChart.ShowVerticalGridlines = true;
                LoadChart.EnableCellCondense = true;

                LoadChart.ChartData = CreateChartDescription(keyWord);
                LoadChart.DataTables = GetChartSource(SmoothnessSlider.Value);
                LoadChart.MakeChart();
                
                foreach (var rb in LoadReportSelector.Children) ((RadioButton)rb).IsEnabled = true;
            }, true);
        }

        private List<SLDataTable> GetChartSource(double smoothing)
        {
            _chartSource = new List<SLDataTable>();
            var y = _data.Select(by => (object)by.Load).ToList();
            var x = _data.Select(bx => (object)((double)bx.Date.Ticks / 1e9)).ToList();

            var filtered = AQMath.LowessFilter(x, y, smoothing, 5);

            var sldt = new SLDataTable
            {
                ColumnNames = (new [] { "День", "Число отчетов", "Число отчетов сглаженное" }).ToList(),
                DataTypes = (new [] { "DateTime", "Double", "Double" }).ToList(),
                TableName = "Загрузка системы",
                Table = new List<SLDataRow>()
            };

            var c = 0;
            foreach (var en in _data)
            {
                var sldr = new SLDataRow {Row = new List<object> {en.Date, en.Load, (double) filtered[c]}};
                sldt.Table.Add(sldr);
                c++;
            }
            _chartSource.Add(sldt);
            return _chartSource;
        }

        private ChartDescription CreateChartDescription(string keyWord)
        {
            var cd = new ChartDescription();
            switch (keyWord)
            {
                case "XLSX":
                    cd.ChartTitle = "Дневная нагрузка системы (формирование xlsx)";        
                    break;
                case "data readed":
                    cd.ChartTitle = "Дневная нагрузка системы (формирование отчетов в окне системы)";        
                    break;
                case "load":
                    cd.ChartTitle = "Дневная нагрузка системы (загрузка модулей)";        
                    break;
                case "SL Version":
                    cd.ChartTitle = "Дневная нагрузка системы (число входов пользователей)";        
                    break;
                case "UserCount":
                    cd.ChartTitle = "Дневная нагрузка системы (число пользователей)";
                    break;
                default:
                    cd.ChartTitle = String.Empty;        
                    break;
            } 
            
            cd.ChartSeries = new List<ChartSeriesDescription>();
            
            var csd = new ChartSeriesDescription
            {
                SourceTableName = "Загрузка системы",
                XColumnName = "День",
                XIsNominal = false,
                XAxisTitle = "День",
                YColumnName = "Число отчетов",
                SeriesTitle = "Нагрузка системы",
                SeriesType = ChartSeriesType.LineXY,
                MarkerColor = "#ff4d8de3",
                LineColor = "#404d8de3",
                FillColor = "#084d8de3",
                LineSize = 1,
                MarkerSize = 7
            };
            cd.ChartSeries.Add(csd);

            var csd2 = new ChartSeriesDescription
            {
                SourceTableName = "Загрузка системы",
                XColumnName = "День",
                XIsNominal = false,
                XAxisTitle = "День",
                YColumnName = "Число отчетов сглаженное",
                SeriesTitle = "Нагрузка системы (сглаживание)",
                SeriesType = ChartSeriesType.LineXY,
                LineColor = "#ffFF4040",
                LineSize = 3,
                IsSmoothed = true
            };
            cd.ChartSeries.Add(csd2);

            switch (keyWord)
            {
                case "XLSX":
                    csd.YAxisTitle = "Число отчетов xlsx";
                    csd2.YAxisTitle = csd.YAxisTitle;
                    break;
                case "data readed":
                    csd.YAxisTitle = "Число отчетов";
                    csd2.YAxisTitle = csd.YAxisTitle;
                    break;
                case "load":
                    csd.YAxisTitle = "Число загрузок";
                    csd2.YAxisTitle = csd.YAxisTitle;
                    break;
                case "SL Version":
                    csd.YAxisTitle = "Число входов";
                    csd2.YAxisTitle = csd.YAxisTitle;
                    break;
                case "UserCount":
                    csd.YAxisTitle = "Число пользователей";
                    csd2.YAxisTitle = csd.YAxisTitle;
                    break;
                default:
                    cd.ChartTitle = String.Empty;
                    break;
            } 
            return cd;
        }

        private void LoadRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            if (LoadChart!=null && !LoadChart.IsBusy && rb != null) DrawLoadReport(rb.Tag.ToString());
        }

        private void SmoothnessSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (SmoothnessSlider == null) return;
            LoadChart.DataTables = GetChartSource(SmoothnessSlider.Value);
            LoadChart.MakeChart();
        }
        #endregion

        #region Отчет по пользователям
        private void RefreshLastUsersButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshUserList();
        }

        private void AQ_UsersListGrid_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshUserList();
        }

        private void RefreshUserList()
        {
            AQ_UsersListGrid.ItemsSource = null;
            AQ_UsersListGrid.UpdateLayout();
            var adc = new AdministrationDomainContext();
            var q = adc.GetUserActivityQuery();
            adc.Load(q, a =>
            {
                AQ_UsersListGrid.ItemsSource = a.Entities;
                AQ_UsersListGrid.UpdateLayout();
                UserListTotals.Text = string.Format("Зарегистрировано {0} пользователей.", a.Entities.Count());
            }, null);
        }
        #endregion

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if (MainTabControl.SelectedIndex != 0)
            {
                e.Row.Foreground = new SolidColorBrush(Colors.Black);
                return;
            }
            e.Row.Header = e.Row.GetIndex() + 1 + " ";
            var data = (e.Row.DataContext as AQ_Users)?.LastUpdated;
            if (data==null) e.Row.Foreground = new SolidColorBrush(Colors.Blue);
            else e.Row.Foreground = new SolidColorBrush((!data.HasValue || data.Value < DateTime.Now.AddDays(-90)) ? Colors.Gray : Colors.Black);
        }

        private void AQ_UserLogDataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex()+1+LogPager.PageIndex * LogPager.PageSize + " ";
        }

        private void NewSlideButton_Click(object sender, RoutedEventArgs e)
        {
            SmoothnessSlider.Opacity = 0; 
            UpdateLayout();
            var wb = new WriteableBitmap(ChartViewPanel, new ScaleTransform { ScaleX = 2, ScaleY = 2 });
            SmoothnessSlider.Opacity = 1;
            UpdateLayout();

            var sed = new SlideElementDescription { BreakSlide = true, Description = "График", SlideElementType = SlideElementTypes.Image, Width = wb.PixelWidth, Height = wb.PixelHeight };

            var signalArguments = new Dictionary<string, object> {{"Thumbnail", wb}, {"SlideElement", sed}};
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertSlideElement, signalArguments, null);
        }

        private void RecheckButton_OnClick(object sender, RoutedEventArgs e)
        {
            var adc = new AdministrationDomainContext();
            RecheckButton.IsEnabled = false;
            RecheckProgressIndicator.Visibility = Visibility.Visible;
            adc.RecheckDomainUserInfo(true, operation =>
            {
                RecheckButton.IsEnabled = true;
                AQ_UsersDomainDataSource.Load();
                RecheckProgressIndicator.Visibility = Visibility.Collapsed;
            }, null);
        }

        private void RecheckDateButton_OnClick(object sender, RoutedEventArgs e)
        {
            var adc = new AdministrationDomainContext();
            RecheckDateButton.IsEnabled = false;
            RecheckDateProgressIndicator.Visibility = Visibility.Visible;
            adc.RecheckDate(true, operation =>
            {
                RecheckDateButton.IsEnabled = true;
                AQ_UsersDomainDataSource.Load();
                RecheckDateProgressIndicator.Visibility = Visibility.Collapsed;
            }, null);
        }

        private void ExportRolesButton_OnClick(object sender, RoutedEventArgs e)
        {
            ExportExcel(ConvertRolesToReportTables());
        }

        private void ExportExcel(List<ReportDataTable> sourceTables)
        {
            var task = new Task(new[] {"Компоновка данных", "Формирование файла Xlsx"}, "Экспорт в Xlsx", TaskPanel);
            task.StartTask();

            var task1Guid = Guid.NewGuid();
            var task2Guid = Guid.NewGuid();
            _rdsc = Services.GetReportingService();
            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived")
                .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                .Subscribe(x =>
                {
                    var sr = x.EventArgs.or;
                    if (sr.Success)
                    {
                        task.AdvanceProgress();
                        _rdsc.GetAllResultsInXlsxAsync(task2Guid, (string) sr.ResultData);
                    }
                    else
                    {
                        task.StopTask(sr.Message);
                        if (sr.ResultData != null) _rdsc.DropDataAsync((Guid) sr.ResultData);
                    }
                });

            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived")
                .Where((o1, e1) => o1.EventArgs.OperationGuid == task2Guid)
                .Subscribe(x =>
                {
                    var sr = x.EventArgs.or;
                    if (sr.Success)
                    {
                        task.AdvanceProgress();
                        Dispatcher.BeginInvoke(
                            () => HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}",
                                    HttpUtility.UrlEncode(sr.ResultData.ToString()),
                                    HttpUtility.UrlEncode(string.Format("Расчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null))),
                                    HttpUtility.UrlEncode("application/msexcel")))));
                    }
                    else
                    {
                        task.StopTask(sr.Message);
                        if (sr.ResultData != null) _rdsc.DropDataAsync((Guid) sr.ResultData);
                    }
                });

            _rdsc.FillDataWithSLTablesAsync(task1Guid, sourceTables);
        }

        private List<ReportDataTable> ConvertRolesToReportTables()
        {
            var result = new List<ReportDataTable>();
            var data = AQ_UsersDomainDataSource.DomainContext.EntityContainer.GetEntitySet<AQ_Users>();
            var rdt = new ReportDataTable
            {
                ColumnNames = new List<string>{ "Логин", "Пользователь", "Роли", "Должность", "Подразделение", "Обновлено"  },
                DataTypes = new List<string> { "String", "String", "String", "String", "String", "DateTime" },
                Table = new List<ReportDataRow>()
            };
            foreach (var row in data)
            {
                var rdr = new ReportDataRow { Row = new List<object>{row.Login, 
                                                  row.CachedUserName, 
                                                  (row.Roles!=null 
                                                      ? AQMath.Concatenate(Security.DecryptRoleString(row.Roles, row.Login).OfType<object>().ToList(), ";") 
                                                      : string.Empty),
                                                  row.Title, 
                                                  row.Workplace, 
                                                  row.LastUpdated} };
                rdt.Table.Add(rdr);
            }
            rdt.TableName = "Роли пользователей";
            rdt.SelectionString = String.Empty;
            result.Add(rdt);
            return result;
        }

         private List<ReportDataTable> ConvertUsersToReportTables()
         {
             if (AQ_UsersListGrid.ItemsSource == null) return null;
             var result = new List<ReportDataTable>();
             var data = AQ_UsersListGrid.ItemsSource.OfType<UserActivity>();
            var rdt = new ReportDataTable
            {
                ColumnNames = new List<string>{ "Логин", "Пользователь", "Первый вход", "Последний вход", "Последний IP", "SL", "Активность", "Ошибки"  },
                DataTypes = new List<string> { "String", "String", "DateTime", "DateTime", "String", "Double", "Double", "Double" },
                Table = new List<ReportDataRow>()
            };
            foreach (var row in data)
            {
                var rdr = new ReportDataRow { Row = new List<object>{row.Login, 
                                                  row.UserName,
                                                  row.FirstAppeared,
                                                  row.LastRecordDate, 
                                                  row.LastKnownIP, 
                                                  row.LastSLVersion,
                                                  row.Activity, 
                                                  row.Errors  }};
                rdt.Table.Add(rdr);
            }
            rdt.TableName = "Роли пользователей";
            rdt.SelectionString = String.Empty;
            result.Add(rdt);
            return result;
        }


        private void DeleteUserButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (AQ_UsersDataGrid.SelectedItems.Count > 0)
            {
                var mbr =MessageBox.Show("Удалить отмеченных пользователей? Действие необратимо.", "Подтверждение",
                    MessageBoxButton.OKCancel);
                if (mbr != MessageBoxResult.OK) return;
                DeleteUserButton.IsEnabled = false;
                var deleteList = AQ_UsersDataGrid.SelectedItems.Cast<AQ_Users>().Select(a => AQ_UsersDomainDataSource.DataView.Cast<AQ_Users>().SingleOrDefault(i => i.Login == a.Login)).Where(toDelete => toDelete != null).ToList();
                var entitySet = AQ_UsersDomainDataSource.DomainContext.EntityContainer.GetEntitySet<AQ_Users>();
                foreach (var a in deleteList)
                {
                    entitySet.Remove(a);
                }
                AQ_UsersDomainDataSource.SubmitChanges();
                DeleteUserButton.IsEnabled = true;
            }
            else MessageBox.Show("Пользователи не отмечены для удаления");
        }

        private void ExportUsersButton_OnClick(object sender, RoutedEventArgs e)
        {
            ExportExcel(ConvertUsersToReportTables());
        }
    }

    public class RolesToStringValueConverter : System.Windows.Data.IValueConverter
    {
        public DomainDataSource DDS = null;
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (DDS == null) return null;
            var user = DDS.Data.OfType<AQ_Users>().SingleOrDefault(a => a.Login == value.ToString());
            if (user == null)
            {
                return null;
            }
            var r = Security.DecryptRoleString(user.Roles, value.ToString());
            var isFirst = true;
            var result = string.Empty;
            foreach (var s in r)
            {
                if (!isFirst) result += "; ";
                isFirst = false;
                result += s;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class RoleData : INotifyPropertyChanged
    {
        private string _role;
        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
                NotifyPropertyChanged("Role");
            }
        }

        private bool _ischecked;
        public bool IsChecked
        {
            get
            {
                return _ischecked;
            }
            set
            {
                _ischecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
