﻿using System;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore;
using AQSelection;

namespace SLAdministration
{
    public class CheckRoles
    {
        public static void ReadRolesAsync(string ip, Action start)
        {

            if (ip==null)start.Invoke();

            var adc = new AdministrationDomainContext();

            var q = adc.GetAQ_UserQuery(ip);
            adc.Load(q, a =>
            {
                var currentUser = a.Entities.SingleOrDefault();
                if (currentUser != null)
                {
                    Security.CurrentUser = currentUser.Login;
                    Security.CurrentUserName = currentUser.CachedUserName;
                    Security.CurrentTitle = currentUser.Title;
                    Security.CurrentWorkPlace = currentUser.Workplace;
                    Security.Roles = Security.DecryptRoleString(currentUser.Roles, currentUser.Login);
                    start.Invoke();
                }
            }, true);
        }
    }
}
