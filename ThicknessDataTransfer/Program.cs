﻿using System;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace ThicknessDataTransfer
{
    class Program
    {
        private static SqlConnection _raportConn;
        private static SqlConnection _f2500Conn;
        private static DateTime? _processingStartDate;
        static void Main(string[] args)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None); // Add an Application Setting.

            var sourcePath = config.AppSettings.Settings["SourcePath"].Value;
            _raportConn = new SqlConnection(config.ConnectionStrings.ConnectionStrings["RaportConnectionString"].ConnectionString);
            _raportConn.Open();
            _f2500Conn = new SqlConnection(config.ConnectionStrings.ConnectionStrings["F2500ConnectionString"].ConnectionString);
            _f2500Conn.Open();
            _processingStartDate = GetLastProcessedDate();

            Console.WriteLine($"Job started at {DateTime.Now.ToLongTimeString()}");

            foreach (var pathdir in Directory.GetDirectories(sourcePath))
            {
                var dir = Path.GetFileName(pathdir);
                var dirdate = DateTime.ParseExact(dir, "yyyy_MM", CultureInfo.InvariantCulture);

                if (_processingStartDate.HasValue &&
                    (dirdate < _processingStartDate.Value.AddDays(-32)))
                    continue;

                var files = Directory.GetFiles(pathdir);
                Console.WriteLine($"Started processing of {dir} with {files.Count()} files");
                foreach (var pathfile in files)
                {
                    var file = Path.GetFileName(pathfile);
                    if (Path.GetExtension(pathfile) != ".json") continue;
                    var fildate = DateTime.ParseExact(file.Split('.')[0], "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                    if (!_processingStartDate.HasValue || fildate > _processingStartDate) ProcessFile(pathfile);
                }
                Console.WriteLine($"Directory {dir} processed at {DateTime.Now.ToLongTimeString()}");
            }

            FinalizeTransfer();
            _f2500Conn.Close();
            _raportConn.Close();
            Console.WriteLine($"Job finished at {DateTime.Now.ToLongTimeString()}");
        }

        private static void FinalizeTransfer()
        {
            var c = @"update F2300_Catalogue set IsLast = case when Datekey in (select datekey from (
                    select datekey, passnumber, LAG (passnumber, 1) OVER (ORDER BY datekey desc) as prevpassnumber from F2300_Catalogue ) as t where t.PassNumber >= t.prevpassnumber) then 1 else 0 end
                    update F2300_Catalogue set ListNumber = null
                    update F2300_Catalogue set ListNumber = r.ln from F2300_Catalogue as c inner join 
                    (select datekey, ln from (select *, Row_Number() OVER (Partition by Melt ORDER BY datekey) as ln 
                    from (select * from  F2300_Catalogue where islast=1)as k  where islast=1)as t) as r on c.DateKey = r.DateKey
                    ";

            var cmd = new SqlCommand(c, _raportConn);
            cmd.ExecuteNonQuery();
        }

        private static void ProcessFile(string s)
        {
            var p = JsonConvert.DeserializeObject<Plate>(File.ReadAllText(s));
            var isEspo = p.Smelt_ID.StartsWith("19");
            var isEsp3 = p.Smelt_ID.Contains("В") || p.Smelt_ID.Contains("Ш") || p.Smelt_ID.Contains("И");
            var smelt = isEsp3 
                ? p.Smelt_ID
                : isEspo 
                    ? "Э" + p.Smelt_ID 
                    : "К" + p.Smelt_ID;
            var melttype = isEsp3 ? " ЭСПЦ-3" :
                    isEspo 
                        ? " ЭСПЦ-6" 
                        : " ККЦ";
            var catalogueCommand =
                $@"SET DATEFORMAT DMY 
                    INSERT INTO [dbo].[F2300_Catalogue]
                   ([Melt]
                   ,[Mark]
                   ,[Thickness]
                   ,[MinThicknessDev]
                   ,[MaxThicknessDev]
                   ,[ListNumber]
                   ,[PassNumber]
                   ,[MarkID]
                   ,[NtdID]
                   ,[MeltType]
                   ,[DateKey]
                   , [IsLast])
             VALUES
                   ('{smelt}'
                   ,'{p.Alloy_Code}'
                   ,{(p.Thickness_Nom / 1000).ToString(CultureInfo.InvariantCulture)}
                   ,{(p.Thickness_NomMin / 1000).ToString(CultureInfo.InvariantCulture)}
                   ,{(p.Thickness_NomMax / 1000).ToString(CultureInfo.InvariantCulture)}
                   ,null
                   ,{p.Pass_Number}
                   ,[dbo].GetLastCertMark('{smelt}', '{p.dateTime}')
                   ,[dbo].GetLastCertNtd('{smelt}', '{p.dateTime}')
                   ,'{melttype}'
                   ,'{p.dateTime}'
                   ,null)";

            var cmd = new SqlCommand(catalogueCommand, _raportConn);
            cmd.ExecuteNonQuery();
            var n = p.LP_Thickness_Center_Avg.points.Count;
            double offset = (double)p.LP_Thickness_Center_Avg.x0;
            double step = (double)p.LP_Thickness_Center_Avg.step;
            for (var i = 0; i < n; i++)
            {
                var measureCommand =
                    $@"SET DATEFORMAT DMY 
                        INSERT INTO [dbo].[2300]
                       ([DateKey]
                       ,[Offset]
                       ,[ThicknessRight]
                       ,[ThicknessCenter]
                       ,[ThicknessLeft]
                       ,[TemperatureRight]
                       ,[TemperatureCenter]
                       ,[TemperatureLeft]
                       ,[Width])
                 VALUES
                       ('{p.dateTime}'
                       ,{(offset/1000.0d).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Thickness_Right_Avg.points[i] / 1000d).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Thickness_Center_Avg.points[i] / 1000d).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Thickness_Left_Avg.points[i] / 1000d).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Temperature_Right_Avg.points[i]).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Temperature_Center_Avg.points[i]).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Temperature_Left_Avg.points[i]).ToString(CultureInfo.InvariantCulture)}
                       ,{(p.LP_Width?.points[i])?.ToString(CultureInfo.InvariantCulture) ?? "NULL"})";
                offset += step;
                var cmd2 = new SqlCommand(measureCommand, _f2500Conn);
                cmd2.ExecuteNonQuery();
            }
        }

        static DateTime? GetLastProcessedDate()
        {
            var cmd = new SqlCommand("select max(datekey) from F2300_Catalogue", _raportConn);
            try
            {
                return (DateTime?)cmd.ExecuteScalar();
            }
            catch (Exception)
            {
                return new DateTime(2001,1,1);
            }
        }
    }
}
