﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThicknessDataTransfer
{
    public class LPTemperatureCenterAvg
    {
        public double x0 { get; set; }
        public double step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPTemperatureLeftAvg
    {
        public double x0 { get; set; }
        public double step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPTemperatureRightAvg
    {
        public double x0 { get; set; }
        public double step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPThicknessCenterAvg
    {
        public double x0 { get; set; }
        public double step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPThicknessLeftAvg
    {
        public int x0 { get; set; }
        public int step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPThicknessRightAvg
    {
        public int x0 { get; set; }
        public int step { get; set; }
        public IList<double> points { get; set; }
    }

    public class LPWidth
    {
        public int x0 { get; set; }
        public int step { get; set; }
        public IList<double> points { get; set; }
    }

    public class Plate
    {
        public string Alloy_Code { get; set; }
        public LPTemperatureCenterAvg LP_Temperature_Center_Avg { get; set; }
        public LPTemperatureLeftAvg LP_Temperature_Left_Avg { get; set; }
        public LPTemperatureRightAvg LP_Temperature_Right_Avg { get; set; }
        public LPThicknessCenterAvg LP_Thickness_Center_Avg { get; set; }
        public LPThicknessLeftAvg LP_Thickness_Left_Avg { get; set; }
        public LPThicknessRightAvg LP_Thickness_Right_Avg { get; set; }
        public LPWidth LP_Width { get; set; }
        public int Pass_Number { get; set; }
        public string Smelt_ID { get; set; }
        public double Thickness_Nom { get; set; }
        public double Thickness_NomMax { get; set; }
        public double Thickness_NomMin { get; set; }
        public string Work_Card_ID { get; set; }
        public DateTime dateTime { get; set; }
    }
}
