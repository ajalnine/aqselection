﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputDateRange : UserControl, IAQParameterView
    {
        private Parameters p;

        public InputDateRange()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            Panel Custom = MainDateRangePicker.GetCustomPanel();

            if (p.Locked)
            {
                Custom.Children.Clear();
                Custom.Children.Add(new TextBlock() { Name = Guid.NewGuid().ToString(), Margin = new Thickness(5,5,5,5), TextWrapping = System.Windows.TextWrapping.Wrap, Text = p.Mode, VerticalAlignment = System.Windows.VerticalAlignment.Center, HorizontalAlignment = System.Windows.HorizontalAlignment.Center });
                MainDateRangePicker.ShowDateSelector(false);
                Custom.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                Custom.Visibility = System.Windows.Visibility.Collapsed;
                MainDateRangePicker.ShowDateSelector(true);
            }

            SetDateRange(p.Mode, MainDateRangePicker);
            
            this.UpdateLayout();
            Check();
        }

        private void SetDateRange(string Mode, DateRangePicker MainDateRangePicker)
        {
            DateTime From = DateTime.Now, To = DateTime.Now, Now = DateTime.Now, Today = new DateTime(Now.Year, Now.Month, Now.Day, 0, 0, 0);
            
            switch (Mode)
            {
                case "Сегодня":
                    From = Today;
                    break;

                case "Вчера":
                    From = Today.Subtract(new TimeSpan(1, 0, 0, 0));
                    To = From.Add(new TimeSpan(1, 0, 0, 0)).Subtract(new TimeSpan(1));
                    break;

                case "7 дней":
                    From = Today.Subtract(new TimeSpan(6, 0, 0, 0));
                    break;
            
                case "С начала текущего месяца":
                    From = new DateTime(Now.Year, Now.Month, 1, 0, 0, 0);
                    break;

                case "С начала прошлого месяца":
                    if (Now.Month>1) From = new DateTime(Now.Year, Now.Month - 1, 1, 0, 0, 0);
                    else From = new DateTime(Now.Year - 1, 12, 1, 0, 0, 0);
                    break;
                
                case "С начала прошлого года":
                    From = new DateTime(Now.Year - 1, 1, 1, 0, 0, 0);
                    break;

                case "Прошлый год":
                    From = new DateTime(Now.Year - 1, 1, 1, 0, 0, 0);
                    To = new DateTime(Now.Year, 1, 1, 0, 0, 0).Subtract(new TimeSpan(1));
                    break;

                case "Весь период":
                    From = new DateTime(2000, 1, 1, 0, 0, 0);
                    break;

                default:
                    break;
            }
            
            MainDateRangePicker.SetDate(From, To);
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string InputName = "InputDateRange";
            public string Caption = "DateRange";
            public string Mode;
            public bool Locked = false;
        }
    }
}
