﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.ConstructorServiceReference;
using Core.CalculationServiceReference;
using Core;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputListSQL : UserControl, IAQParameterView
    {
        private Parameters p;
        private List<ListData> KeyValues;
        private List<List<object>> SourceData;
        string Selected = string.Empty;
        bool IsFirstRun = true;
        private ConstructorService cs;
        private DateRangePicker ParentDateRange;

        public InputListSQL()
        {
            cs = Services.GetConstructorService();
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            ParentDateRange = MainDateRangePicker;
            if (KeyValues!=null) Selected = SetResultValue(); 
            p = ParseParameters(XMLParameters);
            Caption.Text = p.Caption;
            if (IsFirstRun) RefreshData();
        }

        private void RefreshData()
        {
            ((UserControl)Content).UpdateLayout();
            ProgressIndicator.Visibility = Visibility.Visible;
            KeyValues = new List<ListData>();
            string SQL = @"set dateformat ymd 
                           declare @date1 datetime 
                           declare @date2 datetime
                           set @date1='" + ParentDateRange.From.Value.ToString("yyyy-MM-dd") + "'  set @date2='" + ParentDateRange.To.Value.ToString("yyyy-MM-dd") + "' \r\n" + p.SQL;
            
            cs.BeginGetRows(new GetRowsRequest(SQL), (iar) => 
            {
                SourceData = cs.EndGetRows(iar).GetRowsResult;
                KeyValues = (from i in SourceData select new ListData(i.First().ToString(), (string)i.Last().ToString(), false)).ToList();
                List<string> PreviousSelected = Selected.Split(new string[]{"\n"}, StringSplitOptions.RemoveEmptyEntries).Select(a=>a.Trim()).ToList();
                foreach (var a in KeyValues)
                {
                    if (PreviousSelected.Contains(a.Key)) a.IsChecked = true;
                }

                this.Dispatcher.BeginInvoke(new Action(delegate()
                {
                    ResultContent.ItemsSource = KeyValues;
                    ResultContent.Focus();
                    ResultContent.UpdateLayout();
                    ProgressIndicator.Visibility = System.Windows.Visibility.Collapsed;
                    IsFirstRun = false;
                }));
            }
            , null);
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Selected = SetResultValue();
            Result.Add(new InputDescription() { Name = p.Variable, Value = Selected, Type = "String" });
            return Result;
        }

        private string SetResultValue()
        {
            string v = String.Empty;
            bool IsFirst = true;
            foreach (var a in KeyValues.Where(x => x.IsChecked))
            {
                v += (!IsFirst ? ", " : String.Empty) + "'"+a.Key+"'";
                IsFirst = false;
            }
            return v;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string InputName = "InputListSQL";
            public string SQL;
            public string Caption;
            public string Variable;
        }

        public class ListData :INotifyPropertyChanged
        {
            private string key;
            public string Key
            {
                get{return key;}
                set
                {
                    key = value;
                    if (PropertyChanged!=null)PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Key"));
                } 
            }

            private string val;
            public string Value
            {
                get{return val;}
                set
                {
                    val = value;
                    if (PropertyChanged!=null)PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Value"));
                } 
            }

            private bool ischeked;
            public bool IsChecked
            {
                get { return ischeked; }
                set
                {
                    ischeked = value;
                    if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsChecked"));
                }
            }

            public ListData(string key, string value, bool isChecked)
            {
                Key = key;
                Value = value;
                IsChecked = isChecked;
            }

            public ListData()
            {
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            if (KeyValues != null && KeyValues.Count > 0) foreach (var k in KeyValues) k.IsChecked = true;
        }

        private void DeselectAll_Click(object sender, RoutedEventArgs e)
        {
            if (KeyValues != null && KeyValues.Count > 0) foreach (var k in KeyValues) k.IsChecked = false;
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            Selected = SetResultValue();
            RefreshData();
        }
    }
}
