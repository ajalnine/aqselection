﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputString : UserControl, IAQParameterView
    {
        private Parameters p;

        public InputString()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            Caption.Text = p.Caption;
            if (Input.Text.Length==0) Input.Text = p.Default;
            Input.TextWrapping = p.Multiline ? TextWrapping.Wrap : TextWrapping.NoWrap;
            Input.AcceptsReturn = p.Multiline;
            Input.VerticalScrollBarVisibility = p.Multiline ? ScrollBarVisibility.Auto : ScrollBarVisibility.Hidden;
            if (p.Multiline) Input.Height = 80;
            this.UpdateLayout();
            Check();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Result.Add(new InputDescription() { Name = p.Variable, Value = Input.Text, Type = "String" });
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public const string InputName = "InputString";
            public string Caption;
            public string Variable;
            public string Default;
            public bool Multiline;
        }
    }
}
