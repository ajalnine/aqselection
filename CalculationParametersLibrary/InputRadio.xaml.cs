﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputRadio : UserControl, IAQParameterView
    {
        private Parameters p;
        private string Selected = string.Empty;

        public InputRadio()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            Caption.Text = p.Caption;

            RadioPanel.Children.Clear();
            List<String> Values = p.List.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            string Group = Guid.NewGuid().ToString();
            int c = 0;
            foreach (string s in Values)
            {
                string v = s.Trim();
                RadioButton rb = new RadioButton();
                rb.GroupName = Group;
                rb.Tag = v;
                rb.IsChecked = (Selected != String.Empty)? (s == Selected) : (c == 0);
                c++;
                rb.Content = new TextBlock() { Text = v};
                rb.Name = Guid.NewGuid().ToString();
                rb.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                rb.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
                rb.Style = this.Resources["RadioButtonBorderStyle"] as Style;
                rb.Checked += new RoutedEventHandler(rb_Checked);
                RadioPanel.Children.Add(rb);
            }
            this.UpdateLayout();
            Check();
        }

        void rb_Checked(object sender, RoutedEventArgs e)
        {
            Selected = ((RadioButton)sender).Tag.ToString();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Result.Add(new InputDescription() { Name = p.Variable, Value = Selected, Type = "String" });
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string InputName = "InputRadio";
            public string Caption;
            public string List;
            public string Variable;
        }
    }
}
