﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputSingleNumber : UserControl, IAQParameterView
    {
        private Parameters p;
        private double v;

        public InputSingleNumber()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            Caption.Text = p.Caption;
            if (!Check()) Input.Text = p.Default.ToString();
            this.UpdateLayout(); 
            Check();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            Result.Add(new InputDescription(){ Name = p.Variable, Value = v, Type = "Double"});
            return Result;
        }

        public bool Check()
        {
            string div = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
            string s = Input.Text.Replace(",", div).Replace(".", div);
            if (!Double.TryParse(s, out v)) return false;
            if (p.Min.HasValue && v < p.Min.Value) return false;
            if (p.Max.HasValue && v > p.Max.Value) return false;
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public const string InputName = "InputSingleNumber";
            public string Caption;
            public string Variable;
            public double? Min;
            public double? Max;
            public double Default;
        }

        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            Input.Background = new SolidColorBrush(Check() ?  Colors.White : Color.FromArgb(0,255,128,128));
        }
    }
}
