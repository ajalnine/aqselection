﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;
using ControlsLibrary;

namespace CalculationParametersLibrary
{
    public partial class InputHeader : UserControl, IAQParameterView
    {
        private Parameters p;

        public InputHeader()
        {
            InitializeComponent();
        }

        public void Setup(string XMLParameters, DateRangePicker MainDateRangePicker)
        {
            p = ParseParameters(XMLParameters);
            Header.Text = p.Caption;
            this.UpdateLayout();
            Check();
        }

        public List<InputDescription> GetValues()
        {
            List<InputDescription> Result = new List<InputDescription>();
            return Result;
        }

        public bool Check()
        {
            return true;
        }

        private Parameters ParseParameters(string XMLParameters)
        {
            if (XMLParameters == null) return null;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public const string InputName = "InputHeader";
            public string Caption;
        }
    }
}
