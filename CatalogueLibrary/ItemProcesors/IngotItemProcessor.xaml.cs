﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BasicControlsLibrary;
using ControlsLibrary;
using CalculationsLibrary;
using Core;
using Core.AQServiceReference;
using Core.CalculationServiceReference;

namespace CatalogueLibrary
{
    public partial class IngotItemProcessor : UserControl, ICatalogueItemProcessor
    {
        private int CurrentItemID = -1;
        
        public IngotItemProcessor()
        {
            DateRangeRequired = false;
            InitializeComponent();
        }

        public event CommandDelegate CommandEmitted;

        public void InitializeProcessor(int ItemID)
        {
            CurrentItemID = ItemID;
        }
        
        private void TextImageButtonBase_Click(object sender, RoutedEventArgs e)
        {
            if (CommandEmitted != null) CommandEmitted(this, new CommandEventArgs((sender as TextImageButtonBase).Tag.ToString(), CurrentItemID, "Calculation"));
        }
        public bool DateRangeRequired {get;set;}
    }
}
