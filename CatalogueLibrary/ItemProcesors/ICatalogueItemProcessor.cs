﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BasicControlsLibrary;
using ControlsLibrary;

namespace CatalogueLibrary
{
    public delegate void CommandDelegate(object o, CommandEventArgs e);

    public interface ICatalogueItemProcessor
    {
        event CommandDelegate CommandEmitted; 
        void InitializeProcessor(int ItemID);
    }

    public class CommandEventArgs : EventArgs
    {
        public string Command { get; set; }
        public string ItemType {get; set; }
        public int ItemID { get; set; }
        public Dictionary<string, object> CustomArguments { get; set; }

        public CommandEventArgs()
        {
            CustomArguments = new Dictionary<string, object>();
        }

        public CommandEventArgs(string command, int itemid, string itemtype)
        {
            Command = command;
            ItemID = itemid;
            ItemType = itemtype;
        }
    }
}
