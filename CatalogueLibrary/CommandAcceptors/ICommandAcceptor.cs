﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using BasicControlsLibrary;
using ControlsLibrary;

namespace CatalogueLibrary
{
    public interface ICommandAcceptor
    {
        bool PopupMustBeClosed { get; set; }
        void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl Parent, Action commandFinished);
    }
}
