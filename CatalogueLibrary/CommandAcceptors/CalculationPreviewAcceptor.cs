﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using ControlsLibrary;
using Core;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace CatalogueLibrary
{
    public class CalculationPreviewAcceptor : ICommandAcceptor
    {
        private UserControl _parent;
        private CalculationService _cs;
        DateTime? _from, _to;
        private string _description;

        public bool PopupMustBeClosed { get; set; }

        public CalculationPreviewAcceptor()
        {
            PopupMustBeClosed = true;
        }
        
        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            _parent = parent;
            var calculationID = e.ItemID;
            if (e.CustomArguments == null || !e.CustomArguments.ContainsKey("От") || !e.CustomArguments.ContainsKey("До")) return;
            _from = e.CustomArguments["От"] as DateTime?;
            _to = e.CustomArguments["До"] as DateTime?;
            var steps = e.CustomArguments["Расчет"] as List<Step>;
            _description = e.CustomArguments["Описание"].ToString();
            var parameters = e.CustomArguments.ContainsKey("Параметры") ? e.CustomArguments["Параметры"] as List<InputDescription> : null;
            _cs = Services.GetCalculationService();
            
            var task = new Task(new[] { "Выполнение расчета", "Прием данных" }, "Просмотр результата", taskPanel);
            task.SetState(0, TaskState.Processing);
            if (_to == null) _to = DateTime.Now;
            if (_from == null) _from = _to;
            _cs.BeginExecuteCalculation(calculationID, _from.Value, _to.Value, parameters, iar =>
            {
                var sr = _cs.EndExecuteCalculation(iar);
                if (sr.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(sr.Message);
                    _cs.BeginGetTablesSLCompatible(sr.TaskDataGuid, i =>
                    {
                        var tables = _cs.EndGetTablesSLCompatible(i);
                        if (tables == null) return;
                        task.AdvanceProgress();
                        task.SetMessage("Данные готовы");
                        var ca = new Dictionary<string, object>
                    {
                        {"From",_from}, 
                        {"To", _to}, 
                        {"SourceData", tables},
                        {"Calculation", steps},
                        {"Description", _description},
                        {"New", true},
                    };
                        _parent.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis,
                                ca, null);
                            if (commandFinished != null) commandFinished();
                        }));
                        
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    _cs.BeginDropData(sr.TaskDataGuid, null, null);
                }
            }, task);
        }
    }
}
