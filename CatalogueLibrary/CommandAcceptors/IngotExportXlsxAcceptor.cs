﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Windows.Controls;
using CalculationsLibrary;
using ControlsLibrary;
using Core;
using Core.CalculationServiceReference;
using Core.AQCatalogueDataServiceReference;

namespace CatalogueLibrary
{
    public class IngotExportXlsxAcceptor: ICommandAcceptor
    {
        private CalculationService cs;
        private UserControl Parent = null;
        public bool PopupMustBeClosed { get; set; }

        public IngotExportXlsxAcceptor()
        {
            PopupMustBeClosed = false;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            int CalculationID = e.ItemID;
            
            Parent = parent;
            cs = Services.GetCalculationService();
            catalogueEntities cdc = Services.GetCatalogueEntities();

            CatalogueOperations.GetItem(cdc, CalculationID, (ev, state) =>
            {
                ItemDescription Item = ev.Item;
                InteractiveParameters p = null;
                InteractiveResults r = null;
                CatalogueOperations.ItemToIngot(Item, out p, out r);

                Task task = new Task(new string[] { "Чтение данных", "Формирование файла Xlsx" }, "Экспорт в Xlsx", taskPanel);
                task.SetState(0, TaskState.Processing);
                SLDataTable sl = new SLDataTable();
                sl.ColumnNames = new List<string>();
                sl.ColumnNames.Add("Параметр");
                sl.ColumnNames.Add("Значение");
                sl.DataTypes = new List<string>();
                sl.DataTypes.Add("String");
                sl.DataTypes.Add("Double");
                sl.TableName = "Расчет";
                sl.Table = new List<SLDataRow>();

                foreach (var a in p.Children)
                {
                    SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                    dr.Row.Add(a.Caption);
                    dr.Row.Add(a.CurrentValue);
                    sl.Table.Add(dr);
                }
                foreach (var a in r.Children)
                {
                    SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                    dr.Row.Add(a.Caption);
                    dr.Row.Add(a.CurrentValue);
                    sl.Table.Add(dr);
                }

                List<SLDataTable> sldt = new List<SLDataTable>();
                sldt.Add(sl);
                cs.BeginFillDataWithSLTables(sldt, (iar) =>
                {
                    StepResult sr = cs.EndFillDataWithSLTables(iar);
                    if (sr.Success)
                    {
                        task.SetState(0, TaskState.Ready);
                        task.SetState(1, TaskState.Processing);
                        cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, (iar2) =>
                        {
                            sr = cs.EndGetAllResultsInXlsx(iar2);
                            task.SetState(1, TaskState.Ready);
                            Parent.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                if (commandFinished != null) commandFinished();
                                Process.Start(new Uri(@"GetFile.aspx?FileName=" + WebUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + WebUtility.UrlEncode("Расчет" + DateTime.Now.Ticks.ToString() + ".xlsx") + "&ContentType=" + WebUtility.UrlEncode("application/msexcel"), UriKind.Relative).ToString());
                            }));
                        }, task);
                    }
                    else
                    {
                        task.SetState(0, TaskState.Error);
                        if (commandFinished != null) commandFinished();
                        cs.BeginDropData(sr.TaskDataGuid, null, null);
                    }
                }, task);

            }, null, true, null);
        }
    }
}
