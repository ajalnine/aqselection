﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Core;

namespace CatalogueLibrary
{
    public class CalculationEditAcceptor : ICommandAcceptor
    {
        public bool PopupMustBeClosed { get; set; }

        public CalculationEditAcceptor()
        {
            PopupMustBeClosed = false;
        }
        
        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            IAQModule Parent = parent as IAQModule;
            if (Parent != null)
            {
                Signal.Send(Parent.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertStoredCalculation, e.ItemID, (ct, ca, r) => 
                {
                    if (commandFinished != null) commandFinished();
                });
            }
        }

    }
}
