﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ControlsLibrary;
using BasicControlsLibrary;
using Core;
using Core.CalculationServiceReference;
using System.Diagnostics;

namespace CatalogueLibrary
{
    public class CalculationExportXlsxAcceptor: ICommandAcceptor
    {
        private CalculationService cs;
        private UserControl Parent = null;
        public bool PopupMustBeClosed { get; set; }

        public CalculationExportXlsxAcceptor()
        {
            PopupMustBeClosed = false;
        }

        public void ExecuteCommand(StackPanel taskPanel, Panel placeHolder, CommandEventArgs e, UserControl parent, Action commandFinished)
        {
            Parent = parent;
            cs = Services.GetCalculationService();
            
            int CalculationID = e.ItemID;
            
            if (e.CustomArguments == null || !e.CustomArguments.ContainsKey("От") || !e.CustomArguments.ContainsKey("До")) return;
            DateTime? From = e.CustomArguments["От"] as DateTime?;
            DateTime? To = e.CustomArguments["До"] as DateTime?;
            List<InputDescription> Parameters = e.CustomArguments.ContainsKey("Параметры") ? e.CustomArguments["Параметры"] as List<InputDescription> : null;

            Task task = new Task(new string[] { "Выполнение расчета", "Формирование файла Xlsx" }, "Экспорт в Xlsx", taskPanel);
            task.SetState(0, TaskState.Processing);
            cs.BeginExecuteCalculation(CalculationID, From.Value, To.Value, Parameters, (iar)=>
            {
                StepResult sr = cs.EndExecuteCalculation(iar);
                if (sr.Success)
                {
                    task.SetState(0, TaskState.Ready);
                    task.SetState(1, TaskState.Processing);
                    cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, (iar2) => 
                    {
                        sr = cs.EndGetAllResultsInXlsx(iar2);
                        task.SetState(1, TaskState.Ready);
                        Parent.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            if (commandFinished != null) commandFinished();
                            Process.Start(new Uri(@"GetFile.aspx?FileName=" + WebUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + WebUtility.UrlEncode("Расчет" + DateTime.Now.Ticks.ToString() + ".xlsx") + "&ContentType=" + WebUtility.UrlEncode("application/msexcel"), UriKind.Relative).ToString());
                        }));
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    if (commandFinished != null) commandFinished(); 
                    cs.BeginDropData(sr.TaskDataGuid, null, null);
                }
            }, task);
        }
    }
}
