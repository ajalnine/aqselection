﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Core;
using Core.AQCatalogueDataServiceReference;
using Core.CalculationServiceReference;
using CalculationsLibrary;

namespace CatalogueLibrary
{
    public class ParametersScan
    {
        public delegate void ScanFinishedDelegate(object o, ScanFinishedEventArgs e);
        public delegate void ScanStepDelegate(object o, EventArgs e);

        public List<Step> GetFirstLevelParameters(List<Step> AllSteps) // 3.1
        {
            return AllSteps.Where(a=>a.Group=="Параметры").ToList();
        }

        public void GetFirstLevelParametersAsync(int ItemID, ScanFinishedDelegate ret) // 3.1
        {
            catalogueEntities cdc = Services.GetCatalogueEntities();
            List<Steps> Result = new List<Steps>();
            
            CatalogueOperations.GetItem(cdc, ItemID, (ev, state) =>
            {
                CalculationDescription cd = CatalogueOperations.ItemToCalculationDescription(ev.Item);
                Steps s = new Steps() { CalcStep = cd.StepData.Where(a => a.Group == "Параметры").ToList(), SourceCalculationName = cd.Name };
                Result.Add(s);
                if (ret!=null) ret(this, new ScanFinishedEventArgs() { Found = Result });

            }, null, true, null);
        }

        public void GetAllParametersAsync(int ItemID, ScanFinishedDelegate ret) //3.4
        {
            catalogueEntities cdc = Services.GetCatalogueEntities();
            List<Steps> Result = new List<Steps>();
            Counter.value = 1;
            RecursiveScanAsync(ItemID, cdc, Result, ret);
        }

        public void GetAllChildParametersAsync(List<Step> Steps, ScanFinishedDelegate ret)
        {
            catalogueEntities cdc = Services.GetCatalogueEntities();
            List<Steps> Result = new List<Steps>(); 
            List<Step> Includes = Steps.Where(a => a.Name == "Расчет из каталога").ToList();
            Counter.value = Includes.Count();
            if (Counter.value == 0 && ret != null) ret(this, new ScanFinishedEventArgs() { Found = Result });
            
            foreach (var i in Includes)
            {
                int CalcID = GetCalcIDFromParameters(i.ParametersXML);
                RecursiveScanAsync(CalcID, cdc, Result, ret);
            }
        }

        private void RecursiveScanAsync(int ItemID, catalogueEntities cdc, List<Steps> Result,  ScanFinishedDelegate ret)
        {
            if (Counter.value > 100) ret(this, new ScanFinishedEventArgs() { Found = null });
            CatalogueOperations.GetItem(cdc, ItemID, (ev, state) =>
            {
                CalculationDescription cd = CatalogueOperations.ItemToCalculationDescription(ev.Item);
                Steps s = new Steps() { CalcStep = cd.StepData.Where(a => a.Group == "Параметры").ToList(), SourceCalculationName = cd.Name };
                Result.Add(s);
                List<Step> Includes = cd.StepData.Where(a => a.Name == "Расчет из каталога").ToList();
                Counter.value += Includes.Count();
                Counter.value--;
                if (Counter.value == 0 && ret != null) ret(this, new ScanFinishedEventArgs() { Found = Result });

                foreach (var i in Includes)
                {
                    int CalcID = GetCalcIDFromParameters(i.ParametersXML);
                    RecursiveScanAsync(CalcID, cdc, Result, ret);
                }
            }, null, true, null);
        }

        private int GetCalcIDFromParameters(string XMLParameters)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(XMLParameters);
            Parameters cp = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            return cp.CurrentCalculationID;
        }

        public class Parameters
        {
            public int CurrentCalculationID;
            public ObservableCollection<Alias> TableAliases;
        }

        public class Alias
        {
            public string Name{get;set;}
            public string AliasName{get;set;}
        }
    }

    public static class Counter
    {
        public static int value;
    }

    public class Steps
    {
        public List<Step> CalcStep {get; set;}
        public string SourceCalculationName {get; set;}
    }

    public class ScanFinishedEventArgs : EventArgs
    {
        public List<Steps> Found;
    }
}
