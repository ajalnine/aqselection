﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using ControlsLibrary;
using CalculationsLibrary;
using Core.CalculationServiceReference;
using Core;
using Core.AQCatalogueDataServiceReference;
using StepFactoryLibrary;

namespace CatalogueLibrary
{
    public partial class CatalogueItemEditor : UserControl
    {
        public CatalogueItemEditor()
        {
            InitializeComponent();
            cdc = CatalogueOperations.CreateDomainContext(); 
            if (Initialized != null) Initialized(this, new EventArgs());
            if (TargetsLoaded != null) TargetsLoaded(this, new  CatalogueItemOperationEventArgs(null, null));
        }
        
        protected CalculationService cs;
        private catalogueEntities cdc;
        public delegate void CatalogueItemOperationDelegate(object o, CatalogueItemOperationEventArgs e);
        public delegate void InitializedDelegate(object o, EventArgs e);
        public event CatalogueItemOperationDelegate CatalogueItemLoaded;
        public event CatalogueItemOperationDelegate CatalogueItemSaved;
        public event CatalogueItemOperationDelegate CatalogueItemDeleted;
        public event CatalogueItemOperationDelegate TargetsLoaded;
        public event InitializedDelegate Initialized;
        public ItemDescription CurrentCatalogueItem;
        private bool NewItem = false;
        
        private string QuerySQL;

        public string Description 
        {
            get { return QueryDescription.Text; }
            set { QueryDescription.Text = value; }
        }

        
        public void LoadCalculation(int ID)
        {
            CatalogueOperations.GetItem(cdc, ID, (e, state) =>
            {
                LoadCalculationEnd(e, new CatalogueItemOperationEventArgs(CatalogueOperations.ItemToCalculationDescription(e.Item), state));
            }, null, true, null);
        }

        public void LoadAbstractItem(int ID)
        {
            CatalogueOperations.GetItem(cdc, ID, (e, state) =>
            {
                LoadCalculationEnd(e, new CatalogueItemOperationEventArgs(e.Item, state));
            }, null, true, null);
        }
        
        private void LoadCalculationEnd(ItemReadedEventArgs e, CatalogueItemOperationEventArgs CallbackEventArgs)
        {
            this.Dispatcher.BeginInvoke(new Action(delegate()
            {
                CurrentCatalogueItem = e.Item;
                QueryName.Text = e.Item.Name;
                QueryDescription.Text = e.Item.RevisionDescription;
                if (e.Item.ParentID.HasValue) MainCatalogue.SelectItem(e.Item.ParentID.Value, false);
                if (CatalogueItemLoaded != null) CatalogueItemLoaded(this, CallbackEventArgs);
            }));
        }
                
        public void SaveCalculation(List<Step> StepData, List<DataItem> Results, Task task, bool CanBeDeleted, bool NewItem)
        {
            if (CurrentCatalogueItem != null && !NewItem)
            {
                UpdateCurrentCatalogueCalculation(StepData, Results);
                CatalogueOperations.CreateRevision(cdc, CurrentCatalogueItem.ID, QueryName.Text, QueryDescription.Text, (e, Task) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        if (CatalogueItemSaved != null) CatalogueItemSaved(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, Task));
                    }));
                }, task, CurrentCatalogueItem.AttachmentDescriptions);
                
            }
            else
            {
                CurrentCatalogueItem = new ItemDescription();
                UpdateCurrentCatalogueCalculation(StepData, Results);
                CatalogueOperations.CreateItem(cdc, CurrentCatalogueItem, (e, Task) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        CurrentCatalogueItem.ID = e.ItemID;
                        if (CatalogueItemSaved != null) CatalogueItemSaved(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, Task));
                    }));
                }, task);
            }
        }

        public void SaveAbstractItem(ItemDescription Item, Task task, bool CanBeDeleted, bool newItem)
        {
            NewItem = newItem;
            if (CurrentCatalogueItem != null && !NewItem)
            {
                UpdateCurrentCatalogueItem(Item);
                CatalogueOperations.CreateRevision(cdc, CurrentCatalogueItem.ID, QueryName.Text, QueryDescription.Text, (e, Task) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        if (CatalogueItemSaved != null) CatalogueItemSaved(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, Task));
                    }));
                }, task, CurrentCatalogueItem.AttachmentDescriptions);

            }
            else
            {
                CurrentCatalogueItem = new ItemDescription();
                UpdateCurrentCatalogueItem(Item);
                CatalogueOperations.CreateItem(cdc, CurrentCatalogueItem, (e, Task) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        CurrentCatalogueItem.ID = e.ItemID;
                        if (CatalogueItemSaved != null) CatalogueItemSaved(this, new CatalogueItemOperationEventArgs(CurrentCatalogueItem, Task));
                    }));
                }, task);
            }
        }

        public void SaveSQLQuery(string SQL, Task task, bool CanBeDeleted, bool newitem)
        {
            NewItem = newitem;
            QuerySQL = SQL;
            if (cs == null) cs = Services.GetCalculationService();
            cs.BeginGetQueryResults(SQL, QueryName.Text, null, GetQueryResultsDone, task);
        }

        public void GetQueryResultsDone(IAsyncResult iar)
        {
            List<DataItem> Results = cs.EndGetQueryResults(iar);
            this.Dispatcher.BeginInvoke(new Action(delegate() { SaveCalculation(StepFactory.CreateSQLQuery(QuerySQL, QueryName.Text), Results, iar.AsyncState as Task, true, NewItem); }));
        }

        public void DeleteQuery(Task task, bool ClearEditor)
        {
            if (CurrentCatalogueItem != null)
            {
                CatalogueOperations.DeleteItem(cdc, CurrentCatalogueItem.ID, (e, State) =>
                {
                    this.Dispatcher.BeginInvoke(new Action(delegate()
                    {
                            if (ClearEditor)
                            {
                                QueryName.Text = string.Empty;
                                QueryDescription.Text = string.Empty;
                            }
                            CurrentCatalogueItem = null;
                            if (CatalogueItemDeleted != null) CatalogueItemDeleted(this, new CatalogueItemOperationEventArgs(null, State));
                     }));
                 }, task);
             }
        }

        public void MainCatalogue_ItemDoubleClick(object o, ItemOperationEventArgs e)
        {
            FolderSelector.Visibility = Visibility.Collapsed;
        }

        private void UpdateCurrentCatalogueCalculation(List<Step> StepData, List<DataItem> Results)
        {
            CalculationDescription cd = new CalculationDescription();
            
            cd.StepData = StepData;
            cd.Results = Results;
            cd.Name = QueryName.Text;
            cd.Parameters = QueryDescription.Text;
            if (MainCatalogue.SelectedFolderID.HasValue) cd.TargetID = MainCatalogue.SelectedFolderID.Value;
            int id = CurrentCatalogueItem.ID;
            CurrentCatalogueItem = CatalogueOperations.CalculationDescriptionToItem(cd);
            CurrentCatalogueItem.ID = id;
        }

        private void UpdateCurrentCatalogueItem(ItemDescription Item)
        {
            CurrentCatalogueItem.AttachmentDescriptions = Item.AttachmentDescriptions;
            CurrentCatalogueItem.Name = QueryName.Text;
            CurrentCatalogueItem.RevisionDescription = QueryDescription.Text;
            CurrentCatalogueItem.ItemType = Item.ItemType;
            if (MainCatalogue.SelectedFolderID.HasValue) CurrentCatalogueItem.ParentID = MainCatalogue.SelectedFolderID.Value;
        }

        public string GetQueryName()
        {
            return QueryName.Text;
        }

        public IEnumerable<string> GetQueryUrl()
        {
            return MainCatalogue.URL;
        }

        public string GetQueryDescription()
        {
            return QueryDescription.Text;
        }

        public class Parameters
        {
            public string SQL;
            public string TableName;
        }

        private void ClosePopupButton_Click(object sender, RoutedEventArgs e)
        {
            FolderSelector.Visibility = Visibility.Collapsed;
        }

        private void OpenPopupButton_Click(object sender, RoutedEventArgs e)
        {
            if (!FolderPopup.IsOpen) FolderSelector.Visibility = Visibility.Visible;
            else FolderSelector.Visibility = (FolderSelector.Visibility==Visibility.Collapsed)? Visibility.Visible : Visibility.Collapsed;
            FolderPopup.IsOpen = true;
        }
    }

    public class CatalogueItemOperationEventArgs : EventArgs
    {
        public object CD;
        public object State;
        public CatalogueItemOperationEventArgs(object cd, object state)
        {
            CD = cd;
            State = state;
        }
    }
}
