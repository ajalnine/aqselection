﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using Core.CalculationServiceReference;
using CalculationsLibrary;
using Core;
using Core.AQCatalogueDataServiceReference;

namespace CatalogueLibrary
{
    public static class CatalogueOperations
    {
        public delegate void ItemOperationFinishedDelegate(ItemOperationEventArgs e, object State);
        public delegate void ItemListingFinishedDelegate<T>(ItemListingEventArgs<T> e, object State);
        public delegate void ItemDataFinishedDelegate(ItemDataEventArgs e, object State);
        public delegate void ItemCheckedDelegate(ItemCheckEventArgs e, object State);
        public delegate void ItemURLDelegate(ItemURLEventArgs e, object State);
        public delegate void ItemReadedDelegate(ItemReadedEventArgs e, object State);
        public delegate void ItemOperationErrorDelegate(ItemOperationErrorEventArgs e, object State);

        public static event ItemOperationErrorDelegate ItemOperationError;

        #region Операции с данными

        public static catalogueEntities CreateDomainContext()
        {
            catalogueEntities cdc = Services.GetCatalogueEntities();
            cdc.Timeout = 0xffffff;
            return cdc;
        }

        public static void CreateItem(catalogueEntities cdc, int FolderID, string Name, string Type, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, List<AttachmentDescription> AttachmentList)
        {
                var Data = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={FolderID}", UriKind.Relative)).SingleOrDefault();

                AQ_CatalogueItems ParentItem = Data;
                AQ_CatalogueItems NewItem = new AQ_CatalogueItems();
                NewItem.IsDeleted = false;
                NewItem.Name = Name;
                NewItem.Type = Type;
                if (ParentItem != null) NewItem.ParentId = FolderID;
                cdc.AddToAQ_CatalogueItems(NewItem);
                ParentItem.AQ_CatalogueItems1.Add(NewItem);
                cdc.SaveChanges();
                AQ_CatalogueRevisions NewRevision = AddRevision(cdc, RevisionDescription, NewItem);

                foreach (var attachment in AttachmentList)
                {
                    AddAttachment(cdc, attachment, NewRevision);
                }

                cdc.SaveChanges();
                ItemOperationEventArgs e = new ItemOperationEventArgs(NewItem.id);
                if (OperationCallBack != null) OperationCallBack(e, State);
        }

        private static AQ_CatalogueRevisions AddRevision(catalogueEntities cdc, string RevisionDescription, AQ_CatalogueItems NewItem)
        {
            AQ_CatalogueRevisions NewRevision = new AQ_CatalogueRevisions();
            NewRevision.CalculationID = NewItem.id;
            NewRevision.Revision = (NewItem.AQ_CatalogueRevisions == null) ? 1 : NewItem.AQ_CatalogueRevisions.Count + 1;
            NewRevision.RevisionDescription = RevisionDescription;
            NewRevision.RevisionDate = DateTime.Now;
            NewRevision.RevisionAuthor = Security.CurrentUserName;
            cdc.AddToAQ_CatalogueRevisions(NewRevision);
            NewItem.AQ_CatalogueRevisions.Add(NewRevision);
            cdc.SaveChanges();
            return NewRevision;
        }

        private static AQ_CatalogueData AddAttachment(catalogueEntities cdc, AttachmentDescription Attachment, AQ_CatalogueRevisions Revision)
        {
            AQ_CatalogueData NewAttachment = new AQ_CatalogueData();
            NewAttachment.BinaryData = Attachment.BinaryData;
            NewAttachment.XmlData = Attachment.XmlData;
            NewAttachment.BinaryDataMIME = Attachment.BinaryDataMIME;
            NewAttachment.DataDescription = Attachment.DataDescription;
            NewAttachment.DataItemName = Attachment.DataItemName;
            NewAttachment.IsDeleted = false;
            NewAttachment.AttachedAt = DateTime.Now;
            NewAttachment.AttachedBy = Security.CurrentUserName;
            cdc.AddToAQ_CatalogueData(NewAttachment);
            Revision.AQ_CatalogueData.Add(NewAttachment);
            cdc.SaveChanges();
            return NewAttachment;
        }

        
        public static void CreateItem(catalogueEntities cdc, int FolderID, string Name, string Type, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, params AttachmentDescription[] AttachmentList)
        {
            CreateItem(cdc, FolderID, Name, Type, RevisionDescription, OperationCallBack, State, AttachmentList);
        }

        public static void CreateItem(catalogueEntities cdc, ItemDescription Item, ItemOperationFinishedDelegate OperationCallBack, object State)
        {
            CreateItem(cdc, Item.ParentID.Value, Item.Name, Item.ItemType, Item.RevisionDescription, OperationCallBack, State, Item.AttachmentDescriptions);
        }

        public static IEnumerable<string> GetURL(AQ_CatalogueItems si)
        {
            if (si == null) return null;
            List<object> ti = new List<object>();
            if (si.ParentId != null) ti.Add(si);
            return (from u in GetItemChain(si).Concat(ti) select (u as AQ_CatalogueItems).Name);
        }

        public static List<object> GetItemChain(AQ_CatalogueItems Item)
        {
            List<object> Result = new List<object>();
            AQ_CatalogueItems CurrentItem = Item;
            if (CurrentItem.AQ_CatalogueItems2 != null)
            {
                do
                {
                    if (Result.Count == 0) Result.Add(CurrentItem.AQ_CatalogueItems2);
                    else Result.Insert(0, CurrentItem.AQ_CatalogueItems2);
                    CurrentItem = CurrentItem.AQ_CatalogueItems2;
                }
                while (CurrentItem.ParentId != null);
            }
            else Result.Add(CurrentItem);
            return Result;
        }

        public static void MoveItem(catalogueEntities cdc, int NewFolderID, int ItemID, ItemURLDelegate CallBack, object State)
        {
                AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

                if (CurrentItem != null)
                {
                    CurrentItem.ParentId = NewFolderID;

                    cdc.SaveChanges();
                    if (CallBack != null) CallBack(new ItemURLEventArgs() { URL = GetURL(CurrentItem) }, State);
                }
                else
                {
                    if (CallBack != null) CallBack(new ItemURLEventArgs() { URL = null }, State);
                    if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
                }
        }
                
        public static void UpdateItemWithNewRevisionCreation(catalogueEntities cdc, int ItemID, int NewFolderID, string Name, string Type, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, List<AttachmentDescription> AttachmentList)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                CurrentItem.ParentId = NewFolderID;
                CurrentItem.IsDeleted = false;
                CurrentItem.Name = Name;
                CurrentItem.Type = Type;

                AQ_CatalogueRevisions NewRevision = AddRevision(cdc, RevisionDescription, CurrentItem);
                NewRevision.RevisionAuthor = Security.CurrentUserName;
                NewRevision.RevisionDate = DateTime.Now;
                foreach (var attachment in AttachmentList)
                {
                    AddAttachment(cdc, attachment, NewRevision);
                }
                cdc.SaveChanges();
                ItemOperationEventArgs e = new ItemOperationEventArgs(CurrentItem.id);
                if (OperationCallBack != null) OperationCallBack(e, State);
            }
            else
            {
                ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                if (OperationCallBack != null) OperationCallBack(e, State);
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        public static void UpdateItemLastRevision(catalogueEntities cdc, int ItemID, int NewFolderID, string Name, string Type, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, List<AttachmentDescription> AttachmentList)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                CurrentItem.ParentId = NewFolderID;
                CurrentItem.IsDeleted = false;
                CurrentItem.Name = Name;
                CurrentItem.Type = Type;

                AQ_CatalogueRevisions LastRevision = CurrentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();

                if (LastRevision != null)
                {
                    LastRevision.RevisionDescription = RevisionDescription;
                    LastRevision.RevisionAuthor = Security.CurrentUserName;
                    LastRevision.RevisionDate = DateTime.Now;

                    List<AQ_CatalogueData> Temp = new List<AQ_CatalogueData>();

                    foreach (var data in LastRevision.AQ_CatalogueData) Temp.Add(data);
                    foreach (var ToDelete in Temp) LastRevision.AQ_CatalogueData.Remove(ToDelete);
                    foreach (var attachment in AttachmentList) AddAttachment(cdc, attachment, LastRevision);

                    cdc.SaveChanges();
                    ItemOperationEventArgs e = new ItemOperationEventArgs(CurrentItem.id);
                    if (OperationCallBack != null) OperationCallBack(e, State);
                }
                else
                {
                    ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                    if (OperationCallBack != null) OperationCallBack(e, State);
                    if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "Для указанного элемента в каталоге нет сохраненных данных" }, State);
                }
            }
            else
            {
                ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                if (OperationCallBack != null) OperationCallBack(e, State);
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }
        
        public static void GetItemURL(catalogueEntities cdc, int ItemID, ItemURLDelegate CallBack, object State)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                if (CallBack != null) CallBack(new ItemURLEventArgs() { URL = GetURL(CurrentItem) }, State);
            }
            else
            {
                if (CallBack != null) CallBack(new ItemURLEventArgs() { URL = null }, State);
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        public static void CreateRevision(catalogueEntities cdc, int ItemID, string NewName, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, List<AttachmentDescription> AttachmentList)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                CurrentItem.Name = NewName;
                AQ_CatalogueRevisions NewRevision = AddRevision(cdc, RevisionDescription, CurrentItem);
                NewRevision.RevisionAuthor = Security.CurrentUserName;
                NewRevision.RevisionDate = DateTime.Now;

                foreach (var attachment in AttachmentList)
                {
                    AddAttachment(cdc, attachment, NewRevision);
                }

                cdc.SaveChanges();
                ItemOperationEventArgs e = new ItemOperationEventArgs(CurrentItem.id);
                if (OperationCallBack != null) OperationCallBack(e, State);
            }
            else
            {
                ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                if (OperationCallBack != null) OperationCallBack(e, State);
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        public static void CreateRevision(int ItemID, string NewName, string RevisionDescription, ItemOperationFinishedDelegate OperationCallBack, object State, params AttachmentDescription[] AttachmentList)
        {
            CreateRevision(ItemID, NewName, RevisionDescription, OperationCallBack, State, AttachmentList);
        }

        public static void DeleteItem(catalogueEntities cdc, int ItemID, ItemOperationFinishedDelegate CallBack, object State)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                DeleteItem(cdc, CurrentItem, CallBack, State);
            }
            else
            {
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        private static AttachmentDescription GetAttachmentDescription(AQ_CatalogueData Attachment)
        {
            AttachmentDescription atd = new AttachmentDescription();
            atd.BinaryData = Attachment.BinaryData;
            atd.XmlData = Attachment.XmlData;
            atd.BinaryDataMIME = Attachment.BinaryDataMIME;
            atd.DataDescription = Attachment.DataDescription;
            atd.DataItemName = Attachment.DataItemName;
            return atd;
        }

        public static void AddAttachmentToItem(catalogueEntities cdc, AttachmentDescription Attachment, int ItemID, ItemOperationFinishedDelegate CallBack, object State)
        {
            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                AQ_CatalogueRevisions CurrentRevision = CurrentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();
                AddAttachment(cdc, Attachment, CurrentRevision);

                cdc.SaveChanges();   
                if (CallBack != null) CallBack(new ItemOperationEventArgs(CurrentItem.id), State);
            }
            else
            {
                ItemOperationEventArgs e = new ItemOperationEventArgs(-1);
                if (CallBack != null) CallBack(e, State);
                if (ItemOperationError != null) ItemOperationError( new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        public static void GetItem(catalogueEntities cdc, int ItemID, ItemReadedDelegate CallBack, int? Revision, bool IncludeDocuments, object State)
        {
            ItemDescription Result = new ItemDescription();

            AQ_CatalogueItems CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?$expand=AQ_CatalogueRevisions&CalculationID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CurrentItem != null)
            {
                AQ_CatalogueRevisions CurrentRevision = null;

                if (!Revision.HasValue)
                {
                    CurrentRevision = CurrentItem.AQ_CatalogueRevisions.OrderByDescending(a => a.RevisionDate).FirstOrDefault();
                }
                else
                {
                    CurrentRevision = CurrentItem.AQ_CatalogueRevisions.Where(a => a.Revision == Revision.Value).SingleOrDefault();
                }

                if (CurrentRevision != null)
                {
                    Result.ID = CurrentItem.id;
                    Result.ItemType = CurrentItem.Type;
                    Result.Name = CurrentItem.Name;
                    Result.RevisionAuthor = CurrentRevision.RevisionAuthor;
                    Result.RevisionDate = CurrentRevision.RevisionDate;
                    Result.RevisionDescription = CurrentRevision.RevisionDescription;
                    Result.ParentID = CurrentItem.ParentId;

                    if (IncludeDocuments)
                    {
                        var o2 = cdc.Execute<AQ_CatalogueData>(new Uri($"GetAQ_CatalogueDataList?CalculationID={ItemID}&IncludeContent=true&RevisionNumber={Revision}&IncludeDeleted=true", UriKind.Relative));
                        if (CallBack != null)
                        {
                            foreach (var a in o2)
                            {
                                Result.AttachmentDescriptions.Add(GetAttachmentDescription(a));
                            }
                            ItemReadedEventArgs e = new ItemReadedEventArgs() { Item = Result };
                            if (CallBack != null) CallBack(e, State);
                        }
                    }
                    else
                    {
                        ItemReadedEventArgs e = new ItemReadedEventArgs() { Item = Result };
                        if (CallBack != null) CallBack( e, State);
                    }
                }
                else
                {
                    ItemReadedEventArgs e = new ItemReadedEventArgs() { Item = null };
                    if (CallBack != null) CallBack(e, State);
                    if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "Для указанного элемента в каталоге нет сохраненных данных" }, State);
                }
            }
            else
            {
                ItemReadedEventArgs e = new ItemReadedEventArgs() { Item = null };
                if (CallBack != null) CallBack(e, State);
                if (ItemOperationError != null) ItemOperationError(new ItemOperationErrorEventArgs() { Message = "В каталоге нет элемента с указанным идентификатором" }, State);
            }
        }

        public static void CloneItem(catalogueEntities cdc, int ItemID, ItemReadedDelegate OperationCallBack, object State)
        {
            GetItem(cdc, ItemID, (e, state) => 
            {
                ItemDescription ClonedItem = e.Item;
                ClonedItem.Name += " (Копия)";
                CreateItem(cdc, ClonedItem, (ev, state2)=>
                {
                    ItemReadedEventArgs irea = new ItemReadedEventArgs();
                    irea.Item = ClonedItem;
                    if (OperationCallBack != null) OperationCallBack(irea, state2);
                }, state);
            }, null, true, State);
        }

        public static void GetDocuments(catalogueEntities cdc, int ItemID, ItemListingFinishedDelegate<AttachmentDescription> CallBack, bool IncludeContent, int? RevisionNumber, bool IncludeDeleted, object State)
        {
            var o = cdc.Execute<AQ_CatalogueData>(new Uri($"GetAQ_CatalogueDataList?ItemID={ItemID}&IncludeContent={IncludeContent}&RevisionNumber={RevisionNumber}&IncludeDeleted={IncludeDeleted}", UriKind.Relative));

            if (CallBack != null)
            {
                ItemListingEventArgs<AttachmentDescription> e = new ItemListingEventArgs<AttachmentDescription>();

                if (o == null)
                {
                    if (CallBack != null) CallBack(e, State);
                }
                else
                {
                    foreach (var a in o)
                    {
                        e.Result.Add(GetAttachmentDescription(a));
                    }
                    if (CallBack != null) CallBack(e, State);
                }
            }
        }

        public static void GetDocument(catalogueEntities cdc, int ItemID, ItemDataFinishedDelegate CallBack, int? RevisionNumber, string DocumentName, object State)
        {
            var o = cdc.Execute<AQ_CatalogueData>(new Uri($"GetAQ_CatalogueDataSingle?ItemID={ItemID}&RevisionNumber={RevisionNumber}&DocumentName={DocumentName}", UriKind.Relative));

            ItemDataEventArgs e = new ItemDataEventArgs();
            if (o == null)
            {
                if (CallBack != null) CallBack(e, State);
            }
            else
            {
                if (CallBack != null)
                {
                    e.Result = GetAttachmentDescription(o.SingleOrDefault());
                    CallBack(e, State);
                }
            }
        }

        public static void DeleteDocumentInLastRevision(catalogueEntities cdc, int ItemID, ItemOperationFinishedDelegate CallBack, string DocumentName, object State)
        {
            var o = cdc.Execute<AQ_CatalogueData>(new Uri($"DeleteAQ_CatalogueData?ItemID={ItemID}&DocumentName={DocumentName}", UriKind.Relative));

            if (CallBack != null)
            {
                ItemOperationEventArgs e = new ItemOperationEventArgs(ItemID);
                CallBack(e, State);
            }
        }

        public static void CheckIsItemExistingOrDeleted(catalogueEntities cdc, int ItemID, ItemCheckedDelegate CallBack, object State)
        {
            var o = cdc.Execute<bool?>(new Uri($"CheckAQ_CatalogueItems?ItemID={ItemID}", UriKind.Relative)).SingleOrDefault();

            if (CallBack != null)
            {
                ItemCheckEventArgs e = new ItemCheckEventArgs();
                if (!o.HasValue)
                {
                    e.IsDeleted = null;
                    e.IsExisting = false;
                }
                else
                {
                    e.IsDeleted = o.Value;
                    e.IsExisting = true;
                }
                CallBack(e, State);
            }
        }

        public static void CheckIsItemDataExistingOrDeleted(catalogueEntities cdc, int ItemID, string DocumentName, ItemCheckedDelegate CallBack, object State)
        {
            var o = cdc.Execute<bool?>(new Uri($"CheckAQ_CatalogueData?ItemID={ItemID}&DocumentName={DocumentName}", UriKind.Relative)).SingleOrDefault();

            if (CallBack != null)
            {
                ItemCheckEventArgs e = new ItemCheckEventArgs();
                if (!o.HasValue)
                {
                    e.IsDeleted = null;
                    e.IsExisting = false;
                }
                else
                {
                    e.IsDeleted = o.Value;
                    e.IsExisting = true;
                }
                CallBack(e, State);
            }
        }

        public static void DeleteItem(catalogueEntities cdc, AQ_CatalogueItems aqci, ItemOperationFinishedDelegate CallBack, object State)
        {
            if (aqci != null)
            {
                int ID = aqci.id;
                aqci.IsDeleted = true;
                cdc.UpdateObject(aqci);
                cdc.SaveChanges();
                if (CallBack != null) CallBack(new ItemOperationEventArgs(ID), State);
            }
        }
        #endregion

        #region Преобразование данных

        public static CalculationDescription ItemToCalculationDescription(ItemDescription Item)
        {
            CalculationDescription Result = new CalculationDescription();
            Result.ID = Item.ID;
            Result.CanBeDeleted = true;
            Result.Name = Item.Name;
            Result.TargetID = Item.ParentID.Value;
            Result.Parameters = Item.RevisionDescription;

            string StepDataText = Item.AttachmentDescriptions.Where(a => a.DataItemName == "StepData").SingleOrDefault().XmlData;
            if (StepDataText != null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<Step>));
                Result.StepData = (List<Step>)xs.Deserialize(XmlReader.Create(new StringReader(StepDataText)));
            }

            string ResultDataText = Item.AttachmentDescriptions.Where(a => a.DataItemName == "Results").SingleOrDefault().XmlData;
            if (ResultDataText != null)
            {
                XmlSerializer xs2 = new XmlSerializer(typeof(List<DataItem>));
                Result.Results = (List<DataItem>)xs2.Deserialize(XmlReader.Create(new StringReader(ResultDataText)));
            }
            return Result;
        }

        public static void ItemToIngot(ItemDescription Item, out InteractiveParameters p, out InteractiveResults r)
        {
            string ParametersDataText = Item.AttachmentDescriptions.Where(a => a.DataItemName == "Parameters").SingleOrDefault().XmlData;
            if (ParametersDataText != null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(InteractiveParameters));
                p = (InteractiveParameters)xs.Deserialize(XmlReader.Create(new StringReader(ParametersDataText)));
            }
            else p = null;

            string ResultDataText = Item.AttachmentDescriptions.Where(a => a.DataItemName == "Results").SingleOrDefault().XmlData;
            if (ResultDataText != null)
            {
                XmlSerializer xs2 = new XmlSerializer(typeof(InteractiveResults));
                r = (InteractiveResults)xs2.Deserialize(XmlReader.Create(new StringReader(ResultDataText)));
            }
            else r = null;
        }

        public static ItemDescription CalculationDescriptionToItem(CalculationDescription CD)
        {
            ItemDescription Result = new ItemDescription();
            Result.ItemType = "Calculation";
            Result.Name = CD.Name;
            Result.ParentID = CD.TargetID;
            Result.RevisionDescription = CD.Parameters;
            XmlSerializer xs = new XmlSerializer(typeof(List<Step>));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CD.StepData);
            AttachmentDescription StepData = new AttachmentDescription();
            StepData.XmlData = sb.ToString();
            StepData.DataItemName = "StepData";
            Result.AttachmentDescriptions.Add(StepData);

            XmlSerializer xs2 = new XmlSerializer(typeof(List<DataItem>));
            StringBuilder sb2 = new StringBuilder();
            xs2.Serialize(XmlWriter.Create(sb2), CD.Results);
            AttachmentDescription Results = new AttachmentDescription();
            Results.XmlData = sb2.ToString();
            Results.DataItemName = "Results";
            Result.AttachmentDescriptions.Add(Results);

            return Result;
        }

        public static ItemDescription IngotCalculationToItem(InteractiveParameters p, InteractiveResults r)
        {
            ItemDescription Result = new ItemDescription();
            Result.ItemType = "Ingot";
            XmlSerializer xs = new XmlSerializer(typeof(InteractiveParameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), p);
            AttachmentDescription StepData = new AttachmentDescription();
            StepData.XmlData = sb.ToString();
            StepData.DataItemName = "Parameters";
            Result.AttachmentDescriptions.Add(StepData);

            XmlSerializer xs2 = new XmlSerializer(typeof(InteractiveResults));
            StringBuilder sb2 = new StringBuilder();
            xs2.Serialize(XmlWriter.Create(sb2), r);
            AttachmentDescription Results = new AttachmentDescription();
            Results.XmlData = sb2.ToString();
            Results.DataItemName = "Results";
            Result.AttachmentDescriptions.Add(Results);
            return Result;
        }
        #endregion
    }

    public class ItemOperationEventArgs : EventArgs
    {
        public int ItemID { get; set; }
        public Rect ItemRectangle { get; set; }
        public string ItemType { get; set; }

        public ItemOperationEventArgs(int id)
        {
            ItemID = id;
        }

        public ItemOperationEventArgs(int id, Rect itemrectangle, string itemtype)
        {
            ItemID = id;
            ItemRectangle = itemrectangle;
            ItemType = itemtype;
        }
    }

    public class ItemListingEventArgs<T> : EventArgs
    {
        public List<T> Result { get; set; }

        public ItemListingEventArgs()
        {
            Result = new List<T>();
        }
    }

    public class ItemDataEventArgs : EventArgs
    {
        public AttachmentDescription Result;

        public ItemDataEventArgs()
        {
            Result = new AttachmentDescription();
        }
    }

    public class ItemReadedEventArgs : EventArgs
    {
        public ItemDescription Item;

        public ItemReadedEventArgs()
        {
            Item = new ItemDescription();
        }
    }

    public class ItemURLEventArgs : EventArgs
    {
        public IEnumerable<string> URL;
    }

    public class ItemCheckEventArgs : EventArgs
    {
        public bool IsExisting;
        public bool? IsDeleted;
    }

    public class ItemOperationErrorEventArgs : EventArgs
    {
        public string Message;
    }

    public class AttachmentDescription
    {
        public byte[] BinaryData { get; set; }
        public string XmlData { get; set; }
        public string DataItemName { get; set; }
        public string DataDescription { get; set; }
        public string BinaryDataMIME { get; set; }
    }

    public class ItemDescription
    {
        public string Name { get; set; }
        public string RevisionDescription { get; set; }
        public DateTime? RevisionDate { get; set; }
        public string RevisionAuthor { get; set; }
        public string ItemType { get; set; }
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public List<AttachmentDescription> AttachmentDescriptions { get; set; }

        public ItemDescription()
        {
            AttachmentDescriptions = new List<AttachmentDescription>();
        }
    }
}
