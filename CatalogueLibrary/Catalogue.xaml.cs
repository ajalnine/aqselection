﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using BasicControlsLibrary;
using Core.AQCatalogueDataServiceReference;
using System.Data.Services.Client;

namespace CatalogueLibrary
{
    public partial class Catalogue : UserControl
    {
        #region Свойства и события

        public delegate void SelectedItemDelegate(object o, ItemOperationEventArgs e);
        public event SelectedItemDelegate FolderSelected;
        public event SelectedItemDelegate ItemSelected;
        public event SelectedItemDelegate ItemDoubleClick;
        public event SelectedItemDelegate SelectedItemGeometryPossibleChanged;

        private catalogueEntities cdc;
        private IQueryable<AQ_CatalogueItems> Data;
        public bool TreeIsPopulated = false;

        private double _xContext, _yContext;

        public IEnumerable<string> URL
        {
            get { return (IEnumerable<string>)GetValue(URLProperty); }
            set { SetValue(URLProperty, value); }
        }

        public static readonly DependencyProperty URLProperty =
            DependencyProperty.Register("URL", typeof(IEnumerable<string>), typeof(Catalogue), new PropertyMetadata(null));

        public int? SelectedFolderID
        {
            get { return (int?)GetValue(SelectedFolderIDProperty); }
            set { SetValue(SelectedFolderIDProperty, value); }
        }

        public static readonly DependencyProperty SelectedFolderIDProperty =
            DependencyProperty.Register("SelectedFolderID", typeof(int?), typeof(Catalogue), new PropertyMetadata(null));

        public int? SelectedItemID
        {
            get { return (int?)GetValue(SelectedItemIDProperty); }
            set { SetValue(SelectedItemIDProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemIDProperty =
            DependencyProperty.Register("SelectedItemID", typeof(int?), typeof(Catalogue), new PropertyMetadata(null));

        public bool SelectedItemIsFolder
        {
            get { return (bool)GetValue(SelectedItemIsFolderProperty); }
            set { SetValue(SelectedItemIsFolderProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemIsFolderProperty =
            DependencyProperty.Register("SelectedItemIsFolder", typeof(bool), typeof(Catalogue), new PropertyMetadata(false));

        public string ItemFilter
        {
            get { return (string)GetValue(ItemFilterProperty); }
            set { SetValue(ItemFilterProperty, value); }
        }

        public static readonly DependencyProperty ItemFilterProperty =
            DependencyProperty.Register("ItemFilter", typeof(string), typeof(Catalogue), new PropertyMetadata(String.Empty, new PropertyChangedCallback(ItemFilterChangedCallback)));

        public bool AutoPopulate
        {
            get { return (bool)GetValue(AutoPopulateProperty); }
            set { SetValue(AutoPopulateProperty, value); }
        }

        public static readonly DependencyProperty AutoPopulateProperty =
            DependencyProperty.Register("AutoPopulate", typeof(bool), typeof(Catalogue), new PropertyMetadata(false));

        public bool FullViewMode
        {
            get { return (bool)GetValue(FullViewModeProperty); }
            set { SetValue(FullViewModeProperty, value); }
        }

        public static readonly DependencyProperty FullViewModeProperty =
            DependencyProperty.Register("FullViewMode", typeof(bool), typeof(Catalogue), new PropertyMetadata(true));



        public bool PreselectRequired
        {
            get { return (bool)GetValue(PreselectRequiredProperty); }
            set { SetValue(PreselectRequiredProperty, value); }
        }

        public static readonly DependencyProperty PreselectRequiredProperty =
            DependencyProperty.Register("PreselectRequired", typeof(bool), typeof(Catalogue), new PropertyMetadata(true));

        private AQ_CatalogueItems DefaultItem;
        
        #endregion

        public Catalogue()
        {
            InitializeComponent();
            cdc = CatalogueOperations.CreateDomainContext();
            ((RemoveDeletedConverter)Resources["RemoveDeletedConverter"]).cdc = cdc;
        }
        #region Обновление дерева

        public void RenewData()
        {
            var operation = (ItemFilter == String.Empty) ? "GetAQ_CatalogueItems" : "GetAQ_CatalogueItemsFiltered";
            var Data = cdc.Execute<AQ_CatalogueItems>(new Uri(operation + "?$expand=AQ_CatalogueRevisions", UriKind.Relative)).Where(a =>a.ParentId == null).ToList();
            MainTreeView.ItemsSource = Data;
            if (DefaultItem == null) DefaultItem = Data.FirstOrDefault();
            SetSelectedItems(DefaultItem);
            if (MainTreeView.Items.Count > 0 && PreselectRequired)
            {
                MainTreeView.SetSelectedItem(DefaultItem, CatalogueOperations.GetItemChain(DefaultItem));
                URL = CatalogueOperations.GetURL(DefaultItem);
                TreeIsPopulated = true;
            }
        }

        private void SetSelectedItems(AQ_CatalogueItems DefaultItem)
        {
            if (DefaultItem.Type.ToLower() == "folder")
            {
                SelectedFolderID = DefaultItem.id;
                SelectedItemIsFolder = true;
            }
            else
            {
                SelectedItemIsFolder = false;
                SelectedItemID = DefaultItem.id;
            }
        }

        public static void ItemFilterChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            Catalogue c = o as Catalogue;
            ((Catalogue)o).RenewData();
        }
        
        #endregion

        #region Drag & Drop

        private void MainTreeView_DragFinished(object sender, DragFinishedEventArgs e)
        {
            if (e.Source == null) return;
            AQ_CatalogueItems source = e.Source.DataContext as AQ_CatalogueItems;
            AQ_CatalogueItems destination = e.Destination.DataContext as AQ_CatalogueItems;
            if (CheckIfDragAndDropPossible(source, destination))
            {
                source.ParentId = destination.id;
                cdc.SaveChanges();
            }

            this.Dispatcher.BeginInvoke(new Action( ()=>
            {
                MainTreeView.SetSelectedItem(source, CatalogueOperations.GetItemChain(source));
                MainDragView.RemoveDragItem();
            }));
        }

        private bool CheckIfDragAndDropPossible(AQ_CatalogueItems source, AQ_CatalogueItems destination)
        {
            if (!(source != null && destination != null && destination.Type.ToLower() == "folder" && source != destination))return false;
            List<object> IC = CatalogueOperations.GetItemChain(destination);
            return !IC.Contains(source);
        }

        private void MainTreeView_DragStarted(object sender, MouseEventArgs e)
        {
            AQ_CatalogueItems source = (sender as TreeViewItemExtended).DataContext  as AQ_CatalogueItems;
            if (source.ParentId == null) MainTreeView.CancelDrag();
            else
            {
                MainDragView.SetDragItem(new CatalogueItemPresenter() { ItemName = source.Name, Description = String.Empty, 
                    Style = (this.Resources["ViewModeConverter"] as ViewModeConverter).Convert(FullViewMode, typeof(Style), null, null) as Style, 
                    ItemSign = CatalogueHelper.GetIconByType(source.Type) });
            }
            e.GetHashCode();
        }

        private void MainTreeView_DragCandidateChanged(object sender, DragCandidateEventArgs e)
        {
            AQ_CatalogueItems destination = (sender as TreeViewItemExtended).DataContext as AQ_CatalogueItems;

            if (e.Destination != null)
            {
                AQ_CatalogueItems source = (e.Source as TreeViewItemExtended).DataContext as AQ_CatalogueItems;
                string ItemType = destination.Type.ToLower();
                MainTreeView.SetSelectedItem(destination, CatalogueOperations.GetItemChain(destination));
                MainDragView.SetDropPossibilityIcon(CheckIfDragAndDropPossible(source, destination));
            }
            else MainDragView.SetDropPossibilityIcon(null);
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            var Position = e.GetPosition(LayoutRoot);
            MainDragView.SetDragItemPosition(Position);
            CloseContextMenuOnMouseOut(Position);
        }

        private void CloseContextMenuOnMouseOut(Point Position)
        {
            if (MainToolBar.Visibility == Visibility.Collapsed) return;
            if (_xContext > Position.X + 10 ||
                _yContext > Position.Y + 10 ||
                _xContext < Position.X - MainToolBar.ActualWidth - 10 ||
                _yContext < Position.Y - MainToolBar.ActualHeight - 10)
            {
                MainToolBar.Visibility = Visibility.Collapsed;
            }
        }

        private void CatalogueItemPresenter_EditFinished(object o, CatalogueItemPresenterEditEventArgs e)
        {
            AQ_CatalogueItems source = e.DataItem  as AQ_CatalogueItems;
            source.Name = e.NewName;
            cdc.SaveChanges();
            URL = CatalogueOperations.GetURL(source);
        }

        private void LayoutRoot_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainTreeView.CancelDrag();
            MainDragView.RemoveDragItem();
        }

        private void LayoutRoot_MouseLeave(object sender, MouseEventArgs e)
        {
            MainTreeView.CancelDrag();
            MainDragView.RemoveDragItem();
        }
        #endregion

        #region Операции, вызываемые с тулбара

        private void CopyToolButton_Click(object sender, RoutedEventArgs e)
        {
            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci != null)
            {
                CatalogueOperations.CloneItem(cdc, aqci.id, (ev, state) => 
                {
                }, null);
            }
        }
        
        private void AddToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            if (aqci != null)
            {
                AQ_CatalogueItems NewItem = new AQ_CatalogueItems() { IsDeleted = false, Name = "Новая папка", ParentId = aqci.id, Type = "Folder" };
                cdc.AddToAQ_CatalogueItems(NewItem);
                aqci.AQ_CatalogueItems1.Add(NewItem);
                cdc.UpdateObject(aqci);
                cdc.SaveChanges();
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    MainTreeView.SetSelectedItem(NewItem, CatalogueOperations.GetItemChain(NewItem));
                }));
            }
        }

        private void DeleteToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed; 
            AQ_CatalogueItems aqci = MainTreeView.SelectedItem as AQ_CatalogueItems;
            CatalogueOperations.DeleteItem(cdc, aqci, null, null);
        }

        private void RefreshToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            DefaultItem = MainTreeView.SelectedItem as AQ_CatalogueItems;
            PreselectRequired = true;
            this.RenewData();
        }
        
        private void RenameToolButton_Click(object sender, RoutedEventArgs e)
        {
            MainToolBar.Visibility = Visibility.Collapsed;
            MainTreeView.StartRename();
        }

        public void SelectItem(int ItemID, bool SelectParentFolder)
        {
            var CurrentItem = cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItem?CalculationID={ItemID}", UriKind.Relative)).SingleOrDefault();
            if (SelectParentFolder) CurrentItem = CurrentItem.AQ_CatalogueItems2;
            DefaultItem = CurrentItem;

            if (CurrentItem != null)
            {
                MainTreeView.SetSelectedItem(CurrentItem, CatalogueOperations.GetItemChain(CurrentItem));
                SetSelectedItems(CurrentItem);
                URL = CatalogueOperations.GetURL(CurrentItem);
            }
        }

        #endregion

        #region Обработка событий

        private void MainTreeView_ContextMenuCall(object sender, ContextMenuCallEventArgs e)
        {
            double x = e.CallPoint.X - 2;
            double y = e.CallPoint.Y - 2;
            double sizeX = MainToolBar.ActualWidth;
            double sizeY = MainToolBar.ActualHeight;
            double ContX = this.ActualWidth;
            double ContY = this.ActualHeight;
            if (x + sizeX > ContX) x = ContX - sizeX;
            if (y + sizeY > ContY) y = ContY - sizeY;
            _xContext = x;
            _yContext = y;
            Canvas.SetLeft(MainToolBar, x);
            Canvas.SetTop(MainToolBar, y);
            AQ_CatalogueItems aqci = e.SelectedItem.DataContext as AQ_CatalogueItems;
            DeleteToolButton.IsEnabled = (aqci.ParentId != null && aqci.AQ_CatalogueItems1.Count==0);
            AddToolButton.IsEnabled = (aqci.Type.ToLower() == "folder");
            CopyToolButton.IsEnabled = !AddToolButton.IsEnabled;
            MainToolBar.Visibility = Visibility.Visible;
        }
        
        private void MainTreeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (AutoPopulate)RenewData();
            ViewModeConverter vmc = this.Resources["ViewModeConverter"] as ViewModeConverter;
            vmc.FullModeStyle = this.Resources["FullItemView"] as Style;
            vmc.CompactModeStyle = this.Resources["CompactItemView"] as Style;
            AutoPopulate = false;
        }
        #endregion

        #region Навигация по дереву

        private void MainTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            AQ_CatalogueItems si = ((AQ_CatalogueItems)MainTreeView.SelectedItem);
            if (si == null) return;
            TreeViewItem tvi = TreeViewWorkarounds.ContainerFromItem(MainTreeView, si);
            
            TreeViewExtended tv = MainTreeView;
            Rect r = new Rect();
            try
            {
                FrameworkElement f = VisualTreeHelper.GetChild(tvi, 0) as FrameworkElement;
                f = VisualTreeHelper.GetChild(f, 1) as FrameworkElement;
                GeneralTransform gt = tvi.TransformToVisual(tv);
                Point TopLeftOfItem = gt.Transform(new Point(0, 0));
                r = new Rect(TopLeftOfItem, new Size(f.ActualWidth, f.ActualHeight));
            }
            catch { }
            
            URL = CatalogueOperations.GetURL(si);
            DefaultItem = si;
            if (si.Type.ToLower() == "folder")
            {
                SelectedFolderID = si.id;
                SelectedItemIsFolder = true;
                SelectedItemID = si.id;
                if (FolderSelected != null) FolderSelected(this, new ItemOperationEventArgs(si.id, r, si.Type));
            }
            else
            {
                SelectedItemIsFolder = false;
                SelectedItemID = si.id;
                if (si.AQ_CatalogueItems2 != null)
                {
                    SelectedFolderID = si.AQ_CatalogueItems2.id;
                }
                else SelectedFolderID = null;
                if (ItemSelected != null)
                {
                    ItemSelected(this, new ItemOperationEventArgs(si.id, r, si.Type));
                }
            }
        }
        
        private void MainTreeView_OnMouseLeftButtonDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ItemDoubleClick!=null)ItemDoubleClick(this, new ItemOperationEventArgs(((sender as TreeViewItemExtended).DataContext as AQ_CatalogueItems).id));
        }

        private void MainTreeView_SelectedItemChanged<T>(object sender, RoutedPropertyChangedEventArgs<T> e)
        {
            AQ_CatalogueItems si = ((AQ_CatalogueItems)MainTreeView.SelectedItem);
            if (si == null) return;
            TreeViewItem tvi = TreeViewWorkarounds.ContainerFromItem(MainTreeView, si);

            TreeViewExtended tv = MainTreeView;
            Rect r = new Rect();
            try
            {
                FrameworkElement f = VisualTreeHelper.GetChild(tvi, 0) as FrameworkElement;
                f = VisualTreeHelper.GetChild(f, 1) as FrameworkElement;
                GeneralTransform gt = tvi.TransformToVisual(tv);
                Point TopLeftOfItem = gt.Transform(new Point(0, 0));
                r = new Rect(TopLeftOfItem, new Size(f.ActualWidth, f.ActualHeight));
            }
            catch { }

            URL = CatalogueOperations.GetURL(si);
            DefaultItem = si;
            if (si.Type.ToLower() == "folder")
            {
                SelectedFolderID = si.id;
                SelectedItemIsFolder = true;
                SelectedItemID = si.id;
                if (FolderSelected != null) FolderSelected(this, new ItemOperationEventArgs(si.id, r, si.Type));
            }
            else
            {
                SelectedItemIsFolder = false;
                SelectedItemID = si.id;
                if (si.AQ_CatalogueItems2 != null)
                {
                    SelectedFolderID = si.AQ_CatalogueItems2.id;
                }
                else SelectedFolderID = null;
                if (ItemSelected != null)
                {
                    ItemSelected(this, new ItemOperationEventArgs(si.id, r, si.Type));
                }
            }
        }

        private void MainTreeView_SelectedItemGeometryPossiblyChanged(object sender, TreeViewEventArgs e)
        {
            if (SelectedItemGeometryPossibleChanged != null)
            {
                TreeViewItem tvi = TreeViewWorkarounds.ContainerFromItem(MainTreeView, e.Item);
                if (tvi == null) return;
                TreeViewExtended tv = MainTreeView;
                GeneralTransform gt = tvi.TransformToVisual(tv);
                Point TopLeftOfItem = gt.Transform(new Point(0, 0));
                Rect r = new Rect(TopLeftOfItem, new Size(tvi.ActualWidth, tvi.ActualHeight));
                SelectedItemGeometryPossibleChanged(this, new ItemOperationEventArgs((e.Item as AQ_CatalogueItems).id, r, ((AQ_CatalogueItems)e.Item).Type ));
            }
        }
        #endregion
    }

    #region Вспомогательные классы и конвертеры

    public class TypeToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CatalogueHelper.GetIconByType(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class RemoveDeletedConverter : IValueConverter
    {
        public catalogueEntities cdc;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            return cdc.Execute<AQ_CatalogueItems>(new Uri($"GetAQ_CatalogueItemsFromParent?ParentId={value.ToString()}", UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }

    public class ViewModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value)? FullModeStyle: CompactModeStyle;
        }

        public Style FullModeStyle { get; set; }
        public Style CompactModeStyle { get; set; }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class RevisionsToDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var a = value as IEnumerable<AQ_CatalogueRevisions>;
            if (a != null && a.Count()>0) 
            {
                var LastEdition=a.OrderByDescending(x => x.RevisionDate).FirstOrDefault();
                if (LastEdition != null) return "Ред. " + a.Count() + ", автор: " + LastEdition.RevisionAuthor + " от "+ 
                    ((LastEdition.RevisionDate.HasValue)?LastEdition.RevisionDate.Value.ToString():String.Empty) +"\r\n"+ LastEdition.RevisionDescription;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    #endregion
}