﻿using System;
using System.Reflection;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CatalogueLibrary
{
    public static class CatalogueHelper
    {
        public static BitmapImage GetIconByType(string t)
        {
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(@"/Resources;component/Images/catalogue/" + t + ".png", UriKind.Relative);
            bi.EndInit();
            return bi;
        }

        public static ICatalogueItemProcessor GetItemProcessorByType(string t)
        {
            Assembly CurrentAssembly = Assembly.GetExecutingAssembly();
            return CurrentAssembly.CreateInstance("CatalogueLibrary." + t + "ItemProcessor") as ICatalogueItemProcessor;
        }

        public static ICommandAcceptor GetCommandAcceptorByType(string Command)
        {
            Assembly CurrentAssembly = Assembly.GetExecutingAssembly();
            return CurrentAssembly.CreateInstance("CatalogueLibrary." + Command + "Acceptor") as ICommandAcceptor;
        }
    }
}
