﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Markup;
using MathClasses;

namespace CalculationsLibrary
{
    public class InputParameter:INotifyPropertyChanged
    {
        public delegate void ValueChangedDelegate(object o, EventArgs e);
        public event ValueChangedDelegate ValueChanged;
        public event ValueChangedDelegate ValueChanged2;
        public event PropertyChangedEventHandler PropertyChanged;

        public bool StrongCheckIsEnabled = false;
        public bool CheckIsEnabled = true;
        public bool SliderUpdateIsEnabled = true;

        private double _controloffset;
        public double ControlOffset
        {
            get
            {
                return _controloffset;
            }
            set
            {
                _controloffset = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ControlOffset"));
            }
        }

        public double PSOVelocity { get; set;}
        public double PSOVelocity2 { get; set;}
        public double PSOBestValue { get; set; }
        public double PSOBestValue2 { get; set; }
        public double PSOTime { get; set; }
        public double PSOWeight { get; set; }
        
        public object Parameter { get { return this; } }

        private bool _unlocked;
        public bool Unlocked
        {
            get
            {
                return _unlocked;
            }
            set
            {
                _unlocked = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Unlocked"));
            }
        }

        private bool _isinallowedrange;
        public bool IsInAllowedRange
        {
            get
            {
                return _isinallowedrange;
            }
            set
            {
                _isinallowedrange = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsInAllowedRange"));
                if (!value && StrongCheckIsEnabled) throw new Exception("Выход за допустимые пределы");
            }
        }

        private bool _isinallowedrange2;
        public bool IsInAllowedRange2
        {
            get
            {
                return _isinallowedrange2;
            }
            set
            {
                _isinallowedrange2 = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsInAllowedRange2"));
                if (!value && StrongCheckIsEnabled) throw new Exception("Выход за допустимые пределы");
            }
        }

        private bool _unlocked2;
        public bool Unlocked2
        {
            get
            {
                return _unlocked2;
            }
            set
            {
                _unlocked2 = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Unlocked2"));
            }
        }

        private bool _nonrangeless;
        
        public bool NonRangeLess
        {
            get
            {
                return _nonrangeless;
            }
            set
            {
                _nonrangeless = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("NonRangeLess"));
            }
        }

        private double _minvalue;
        public double MinValue 
        { 
            get
            {
                return _minvalue;
            } 
            set
            {
                _minvalue = Math.Round(value, 5);
                CheckRangeless();
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MinValue"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Parameter"));
                }
            } 
        }

        private double _maxvalue;
        public double MaxValue
        {
            get
            {
                return _maxvalue;
            }
            set
            {
                _maxvalue = Math.Round(value, 5);
                CheckRangeless();
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MaxValue"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Parameter"));
                }
            }
        }

        private double _minallowedvalue;
        public double MinAllowedValue
        {
            get
            {
                return _minallowedvalue;
            }
            set
            {
                _minallowedvalue = Math.Round(value, 5);
                IsInAllowedRange = (CurrentValue >= MinAllowedValue && CurrentValue <= MaxAllowedValue);
                IsInAllowedRange2 = (CurrentValue2 >= MinAllowedValue && CurrentValue2 <= MaxAllowedValue);
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs("MinAllowedValue"));
                PropertyChanged(this, new PropertyChangedEventArgs("Parameter"));
            }
        }

        private double _maxallowedvalue;
        public double MaxAllowedValue
        {
            get
            {
                return _maxallowedvalue;
            }
            set
            {
                _maxallowedvalue = Math.Round(value, 5);
                IsInAllowedRange = (CurrentValue >= MinAllowedValue && CurrentValue <= MaxAllowedValue);
                IsInAllowedRange2 = (CurrentValue2 >= MinAllowedValue && CurrentValue2 <= MaxAllowedValue);
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MaxAllowedValue"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Parameter"));
                }
            }
        }

        private double _step;
        public double Step
        {
            get
            {
                return _step;
            }
            set
            {
                _step = Math.Round(value, 5);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Step"));
            }
        }

        private double _smallminvalue;
        public double SmallMinValue
        {
            get
            {
                return _smallminvalue;
            }
            set
            {
                _smallminvalue = Math.Round(value, 5); 
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SmallMinValue"));
            }
        }

        private double _smallmaxvalue;
        public double SmallMaxValue
        {
            get
            {
                return _smallmaxvalue;
            }
            set
            {
                _smallmaxvalue = Math.Round(value, 5); 
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SmallMaxValue"));
            }
        }

        private double _smallstep;
        public double SmallStep
        {
            get
            {
                return _smallstep;
            }
            set
            {
                _smallstep = Math.Round(value,5);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SmallStep"));
            }
        }

        private double _currentvalue;
        public double CurrentValue
        {
            get
            {
                return _currentvalue;
            }
            set
            {
                _currentvalue = Math.Round(value, 5);
                if (CheckIsEnabled)
                {
                    if ((_currentvalue < MinValue || _currentvalue > MaxValue) && MinValue < MaxValue) throw new Exception("Выход за пределы");
                    
                }
                IsInAllowedRange = (CurrentValue >= MinAllowedValue && CurrentValue <= MaxAllowedValue);
                if (SliderUpdateIsEnabled) SliderPosition = CurrentValue - MinValue - Correction; 
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("CurrentValue"));
                if (ValueChanged != null) ValueChanged(this, new EventArgs());
            }
        }

        private double _currentvalue2;
        public double CurrentValue2
        {
            get
            {
                return _currentvalue2;
            }
            set
            {
                _currentvalue2 = Math.Round(value, 5);
                if (CheckIsEnabled)
                {
                    if ((_currentvalue2 < MinValue || _currentvalue2 > MaxValue) && MinValue < MaxValue) throw new Exception("Выход за пределы");
                }
                IsInAllowedRange2 = (CurrentValue2 >= MinAllowedValue && CurrentValue2 <= MaxAllowedValue);
                if (SliderUpdateIsEnabled) SliderPosition2 = Math.Round(CurrentValue2 - MinValue - Correction2, 4);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("CurrentValue2"));
                if (ValueChanged2 != null) ValueChanged2(this, new EventArgs());
            }
        }

        private void CheckRangeless()
        {
            NonRangeLess = (MinValue < MaxValue);
        }

        #region Свойства для привязки к слайдеру

        private double _correction;
        public double Correction
        {
            get
            {
                return _correction;
            }
            set
            {
                _correction = Math.Round(value, 5);
                CurrentValue = Math.Round(Math.Round((MinValue + SliderPosition) / Step, 0) * Step + Correction, 3);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Correction"));
            }
        }

        private double _correction2;
        public double Correction2
        {
            get
            {
                return _correction2;
            }
            set
            {
                _correction2 = Math.Round(value, 5);
                CurrentValue2 = Math.Round(Math.Round((MinValue + SliderPosition2) / Step, 0) * Step + Correction2, 3);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Correction2"));
            }
        }

        public double SliderPosition
        {
            get
            {
                return Math.Round(CurrentValue - MinValue - Correction, 4);
            }
            set
            {
                if (Math.Abs(CurrentValue - (value + MinValue + Correction)) < 1e-7 || Double.IsNaN(value))
                {
                    if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SliderPosition"));
                    return;
                }
                CurrentValue = Math.Round(Math.Round((MinValue + value) / Step, 0) * Step + Correction, 4);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SliderPosition"));
            }
        }

        public double SmallSliderPosition
        {
            get
            {
                return Correction - SmallMinValue;
            }
            set
            {
                Correction = Math.Round(Math.Round((SmallMinValue + value) / SmallStep, 0) * SmallStep, 3);
                CurrentValue = Math.Round(Math.Round((MinValue + SliderPosition) / Step, 0) * Step + Correction, 4);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SmallSliderPosition"));
            }
        }

        public double SliderPosition2
        {
            get
            {
                return Math.Round(CurrentValue2 - MinValue - Correction2, 4);
            }
            set
            {
                if (Math.Abs(CurrentValue2 - (value + MinValue + Correction2))<1e-7 || Double.IsNaN(value))
                {
                    if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SliderPosition2")); 
                    return;
                }
                CurrentValue2 = Math.Round(Math.Round((MinValue + value) / Step, 0) * Step + Correction2, 4);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SliderPosition2"));
            }
        }

        public double SmallSliderPosition2
        {
            get
            {
                return Correction2 - SmallMinValue;
            }
            set
            {
                Correction2 = Math.Round(Math.Round((SmallMinValue + value) / SmallStep, 0) * SmallStep, 3);
                CurrentValue2 = Math.Round(Math.Round((MinValue + SliderPosition2) / Step, 0) * Step + Correction2, 4);
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("SmallSliderPosition2"));
            }
        }
        #endregion

        private string _caption;
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Caption"));
            }
        }

        private string _group;
        public string Group
        {
            get
            {
                return _group;
            }
            set
            {
                _group = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Group"));
            }
        }
    }

    public class OutputResult : INotifyPropertyChanged
    {
        private double _currentvalue;
        public double CurrentValue
        {
            get
            {
                return _currentvalue;
            }
            set
            {
                _currentvalue = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("CurrentValue"));
            }
        }

        private string _caption;
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Caption"));
            }
        }

        private string _group;
        public string Group
        {
            get
            {
                return _group;
            }
            set
            {
                _group = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Group"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    [ContentProperty("Children")]
    public class InteractiveParameters
    {
        public ObservableCollection<InputParameter> Children { get; set; }

        public string AttachedCalculatorName { get; set; }
        public double PSOBestParticleFitness { get; set; }
        
        public IEnumerable<ParameterGroup<InputParameter>> GroupedParameters
        {
            get
            {
                return from a in Children group a by a.Group into groups select new ParameterGroup<InputParameter> { GroupName = groups.Key, ParametersInGroup = groups.AsEnumerable() }; 
            }
        }

        public InteractiveParameters()
        {
            Children = new ObservableCollection<InputParameter>();
        }
        
        public double GetValue(string caption)
        {
            return Children != null ? Children.Where(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase)).Select(a => a.CurrentValue).SingleOrDefault() : double.NaN;
        }

        public void SetValue(string caption, double value)
        {
            var p = Children.SingleOrDefault(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase));
            if (p != null) p.CurrentValue = value;
        }

        public double GetValue2(string caption)
        {
            return Children.Where(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase)).Select(a => a.CurrentValue2).SingleOrDefault();
        }

        public void SetValue2(string caption, double value)
        {
            var p = Children.SingleOrDefault(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase));
            if (p != null) p.CurrentValue2 = value;
        }

        #region Оптимизация методом PSO

        public double PSOGetFitness()
        {
            return Children.Where(p => Math.Abs(p.MaxValue - p.MinValue) > 1e-8)
                .Sum(p => (p.CurrentValue2 - p.CurrentValue) / (p.MaxValue - p.MinValue) * p.PSOWeight);
        }

        public double PSOGetOutRangePenalty()
        {
            return (from p in Children let outmax = Math.Max(0, p.CurrentValue2 - p.MaxValue) 
                    let outmin = Math.Max(0, p.MinValue - p.CurrentValue) 
                    where Math.Abs(p.MaxValue - p.MinValue) > 1e-8 
                    select (outmax + outmin)/(p.MaxValue - p.MinValue)).Sum();
        }

        public double PSOGetStepPenalty()
        {
            return Children.Select(p => p.CurrentValue2 - p.CurrentValue - p.Step).Where(delta => delta < 0)
                .Aggregate<double, double>(0, (current, delta) => current + 1);
        }

        public void PSOSetup()
        {
            TurnCheck(false);
            SliderUpdateIsEnabled(false);
            foreach (var p in Children)
            {
                if (Math.Abs(p.MinValue - p.MaxValue) < 1e-8)
                {
                    p.Unlocked = false;
                    p.Unlocked2 = false;
                }
                var v1 = AQMath.Rnd() * (p.MaxValue - p.MinValue) + p.MinValue;
                var v2 = AQMath.Rnd() * (p.MaxValue - p.MinValue) + p.MinValue;
                var v3 = (AQMath.Rnd() - 0.5) * (p.MaxValue - p.MinValue) * 2;
                var v4 = (AQMath.Rnd() - 0.5) * (p.MaxValue - p.MinValue) * 2;
                p.CurrentValue = p.Unlocked ? Math.Min(v1, v2) : p.CurrentValue;
                p.CurrentValue2 = p.Unlocked2 ? Math.Max(v1, v2) : p.CurrentValue2; 
                p.PSOTime = (p.MaxValue - p.MinValue) / p.Step /10;
                p.PSOVelocity = p.Unlocked ? v3 / p.PSOTime : 0;
                p.PSOVelocity2 = p.Unlocked2 ? v4 / p.PSOTime : 0;
                p.PSOBestValue = p.CurrentValue;
                p.PSOBestValue2 = p.CurrentValue2;
            }
            PSOBestParticleFitness =  0;
        }

        public void PSOVelocityUpdate(InteractiveParameters bestParameters)
        {
            var i = 0;
            foreach (var p in Children)
            {
                p.PSOVelocity = p.Unlocked ? 0.5 * p.PSOVelocity + 1.5 * AQMath.Rnd() * (p.PSOBestValue - p.CurrentValue) 
                    / p.PSOTime + 1.5 * AQMath.Rnd() * (bestParameters.Children[i].PSOBestValue - p.CurrentValue) / p.PSOTime : 0;
                p.PSOVelocity2 = p.Unlocked2 ? 0.5 * p.PSOVelocity2 + 1.5 * AQMath.Rnd() * (p.PSOBestValue2 - p.CurrentValue2) 
                    / p.PSOTime + 1.5 * AQMath.Rnd() * (bestParameters.Children[i].PSOBestValue2 - p.CurrentValue2) / p.PSOTime : 0;
                i++;
            }
        }

        public void PSOValueUpdate()
        {
            foreach (var p in Children)
            {
                p.CurrentValue = p.CurrentValue + p.PSOVelocity * p.PSOTime;
                p.CurrentValue2 = p.CurrentValue2 + p.PSOVelocity2 * p.PSOTime;
            }
        }

        public void PSOStep(InteractiveParameters bestParameters, double externalPenalty)
        {
            PSOGetFitness();
            var orPenalty = PSOGetOutRangePenalty();
            var newFitness = PSOGetFitness() - orPenalty * 15;
            if (newFitness > PSOBestParticleFitness && Math.Abs(externalPenalty) < 1e-8)
            {
                PSOBestParticleFitness = newFitness;
                PSOSetCurrentAsBest();
            }
            PSOVelocityUpdate(bestParameters);
            PSOValueUpdate();
        }

        public void PSOSetCurrentAsBest()
        {
            foreach (var p in Children)
            {
                p.PSOBestValue = p.CurrentValue;
                p.PSOBestValue2 = p.CurrentValue2;
            }
        }
        #endregion

        public void TurnCheck(bool isEnabled)
        {
            foreach (var p in Children) p.CheckIsEnabled = isEnabled;
        }

        public void SliderUpdateIsEnabled(bool isEnabled)
        {
            foreach (var p in Children) p.SliderUpdateIsEnabled = isEnabled;
        }

        public void FinalizeValues(bool round)
        {
            foreach (var p in Children)
            {
                p.CurrentValue = (p.CurrentValue > p.MaxValue) ? p.MaxValue : p.CurrentValue;
                p.CurrentValue = (p.CurrentValue < p.MinValue) ? p.MinValue : p.CurrentValue;
                p.CurrentValue2 = (p.CurrentValue2 < p.MinValue) ? p.MinValue : p.CurrentValue2;
                p.CurrentValue2 = (p.CurrentValue2 < p.MaxValue) ? p.CurrentValue2 : p.MaxValue;

                if (!round) continue;
                p.CurrentValue = Math.Round(Math.Round((p.CurrentValue) / p.Step, 0) * p.Step, 3);
                p.CurrentValue2 = Math.Round(Math.Round((p.CurrentValue2) / p.Step, 0) * p.Step, 3);
            }
        }

        public static InteractiveParameters GetClone(InteractiveParameters source)
        {
            var p = new InteractiveParameters
            {
                Children = new ObservableCollection<InputParameter>(),
                AttachedCalculatorName = source.AttachedCalculatorName
            };
            foreach (var sourceip in source.Children)
            {
                var ip = new InputParameter
                {
                    Caption = sourceip.Caption,
                    Correction = sourceip.Correction,
                    Correction2 = sourceip.Correction2,
                    CurrentValue = sourceip.CurrentValue,
                    CurrentValue2 = sourceip.CurrentValue2,
                    MinAllowedValue = sourceip.MinAllowedValue,
                    MaxAllowedValue = sourceip.MaxAllowedValue,
                    Group = sourceip.Group,
                    MaxValue = sourceip.MaxValue,
                    MinValue = sourceip.MinValue,
                    SmallMaxValue = sourceip.SmallMaxValue,
                    SmallMinValue = sourceip.SmallMinValue,
                    SmallStep = sourceip.SmallStep,
                    Step = sourceip.Step,
                    Unlocked = sourceip.Unlocked,
                    Unlocked2 = sourceip.Unlocked2,
                    PSOWeight = sourceip.PSOWeight
                };
                p.Children.Add(ip);
            }
            return p;
        }

        public static Dictionary<string, double> GetValues(InteractiveParameters source)
        {
            return source.Children.ToDictionary(d => d.Caption, d => d.CurrentValue);
        }

        public static string GetDescriptions(InteractiveParameters source)
        {
            return source.Children.Aggregate(string.Empty, (current, param) => param.Caption != null ? string.Format("{0}{1}", current, (param.Caption + " = " + param.CurrentValue.ToString(CultureInfo.InvariantCulture) + "; ")) : null);
        }

        public static InteractiveParameters PSOGetParticle(InteractiveParameters origin)
        {
            InteractiveParameters p = GetClone(origin);
            p.PSOSetup();
            return p;
        }

        public static void PSOUnlockParticle(InteractiveParameters origin, bool round)
        {
            origin.FinalizeValues(round);
            origin.SliderUpdateIsEnabled(true);
            origin.TurnCheck(true);
        }
    }

    public class ParameterGroup<T>
    {
        public string GroupName { get; set; }
        public IEnumerable<T> ParametersInGroup { get; set; }
    }

    [ContentProperty("Children")]
    public class InteractiveResults
    {
        public List<OutputResult> Children { get; set; }

        public string AttachedCalculatorName { get; set; }

        public IEnumerable<ParameterGroup<OutputResult>> GroupedResults
        {
            get
            {
                return from a in Children group a by a.Group into groups select new ParameterGroup<OutputResult> { GroupName = groups.Key, ParametersInGroup = groups.AsEnumerable() };
            }
        }

        public InteractiveResults()
        {
            Children = new List<OutputResult>();
        }

        public double GetValue(string caption)
        {
            return Children.Where(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase)).Select(a => a.CurrentValue).SingleOrDefault();
        }

        public void SetValue(string caption, double value)
        {
            var p = Children.SingleOrDefault(a => String.Equals(a.Caption, caption, StringComparison.CurrentCultureIgnoreCase));
            if (p != null) p.CurrentValue = Math.Round(value,5);
        }

        public static InteractiveResults GetClone(InteractiveResults source)
        {
            var r = new InteractiveResults
            {
                AttachedCalculatorName = source.AttachedCalculatorName,
                Children = new List<OutputResult>()
            };
            foreach (var sourceir in source.Children)
            {
                var ir = new OutputResult
                {
                    Caption = sourceir.Caption,
                    CurrentValue = sourceir.CurrentValue,
                    Group = sourceir.Group
                };
                r.Children.Add(ir);
            }
            return r;
        }
    }
}
