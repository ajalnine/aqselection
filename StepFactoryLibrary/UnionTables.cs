﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
       public static Step CreateUnionTables(bool useList, List<string> toUnion, string nameStartsWith, string resultTableName, bool deleteSources )
        {
            var currentParameters = new UnionTablesParameters
            {
                UseList = useList,
                DeleteSources = deleteSources,
                NameStartsWith = nameStartsWith,
                ResultTableName = resultTableName,
                ToUnion = toUnion
            };
            var xs = new XmlSerializer(typeof(UnionTablesParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_UnionTables",
                EditorAssemblyPath = "SLCalc_UnionTables",
                Group = "Преобразование",
                Name = "Сложение данных",
                ImagePath = "/Resources;component/Images/Modules/Union.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("UnionTablesParameters", "Parameters");
            return currentStep;
        }
    }

    public class UnionTablesParameters
    {
        public bool UseList;
        public List<string> ToUnion;
        public string NameStartsWith;
        public string ResultTableName;
        public bool DeleteSources;
    }
}
