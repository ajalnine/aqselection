﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateCalculationFromCatalogue(int calculationID, List<Alias> tableAliases )
        {
            var currentParameters = new CalculationParameters { CurrentCalculationID = calculationID, TableAliases  = tableAliases};
            var xs = new XmlSerializer(typeof(CalculationParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_DataSourceCalculation",
                EditorAssemblyPath = "SLCalc_DataSourceCalculation",
                Group = "Источники данных",
                Name = "Расчет из каталога",
                ImagePath = "/Resources;component/Images/Modules/DataSourceCalculation.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("CalculationParameters", "Parameters");
            return currentStep;
        }
    }
    public class CalculationParameters
    {
        public int CurrentCalculationID;
        public List<Alias> TableAliases;
    }

    public class Alias
    {
        public string Name;
        public string AliasName;
    }
}
