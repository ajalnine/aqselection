﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateExpressionFields(string tableName, string insertAfterField, List<ExpressionFieldDescription> expressionFields)
        {
            var currentParameters = new ExpressionFieldsParameters
            {
                TableName = tableName,
                InsertAfterField = insertAfterField,
                ExpressionFields = expressionFields
            };
            var xs = new XmlSerializer(typeof(ExpressionFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_ExpressionFields",
                EditorAssemblyPath = "SLCalc_ExpressionFields",
                Group = "Расчеты",
                Name = "Группа вычисляемых полей",
                ImagePath = "/Resources;component/Images/Modules/ExpressionFields.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("ExpressionFieldsParameters", "Parameters");
            return currentStep;
        }
    }
    public class ExpressionFieldsParameters
    {
        public string TableName;
        public string InsertAfterField;
        public List<ExpressionFieldDescription> ExpressionFields;
    }
    public class ExpressionFieldDescription
    {
        public string FieldName;
        public string ExpressionType;
        public string Expression;
        public string Code;
    }
}
