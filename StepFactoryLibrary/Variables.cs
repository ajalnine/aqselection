﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateVariables(List<VariableDescription> variables)
        {
            var currentParameters = new VariablesParameters
            {
                 Variables = variables
            };
            var xs = new XmlSerializer(typeof(VariablesParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Variables",
                EditorAssemblyPath = "SLCalc_Variables",
                Group = "Расчеты",
                Name = "Группа переменных",
                ImagePath = "/Resources;component/Images/Modules/Variables.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("VariablesParameters", "Parameters");
            return currentStep;
        }
    }
    public class VariablesParameters
    {
        public List<VariableDescription> Variables;
    }

    public class VariableDescription
    {
        public string VariableName;
        public string VariableType;
        public string Expression;
        public string Code;
    }
}
