﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateAggregates(string tableName, string insertAfterField, List<AggregateFieldDescription> aggregateField, List<string> groupingFields  )
        {
            var currentParameters = new AggregatesParameters
            {
                TableName = tableName,
                InsertAfterField = insertAfterField,
                AggregateFields = aggregateField,
                GroupingFields = groupingFields
            };
            var xs = new XmlSerializer(typeof(AggregatesParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Aggregates",
                EditorAssemblyPath = "SLCalc_Aggregates",
                Group = "Расчеты",
                Name = "Агрегатные поля",
                ImagePath = "/Resources;component/Images/Modules/Aggregates.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("AggregatesParameters", "Parameters");
            return currentStep;
        }
    }
    public class AggregatesParameters
    {
        public string TableName;
        public string InsertAfterField;
        public List<AggregateFieldDescription> AggregateFields;
        public List<string> GroupingFields;
    }
    public class AggregateFieldDescription
    {
        public string FieldName;
        public string ExpressionType;
        public string Expression;
        public string Code;
    }
}
