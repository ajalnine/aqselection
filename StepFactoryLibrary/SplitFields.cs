﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
   
        public static Step CreateSplitFields(string tableName, List<SplitFieldData> splitFields)
        {
            var currentParameters = new SplitFieldsParameters {TableName = tableName, SplitFields = splitFields };
            var xs = new XmlSerializer(typeof(SplitFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_SplitFields",
                EditorAssemblyPath = "SLCalc_SplitFields",
                Group = "Преобразование",
                Name = "Деление текстовых полей",
                ImagePath = "/Resources;component/Images/Modules/SplitColumns.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SplitFieldsParameters", "Parameters");
            return currentStep;
        }
    }

    public class SplitFieldsParameters
    {
        public List<SplitFieldData> SplitFields;
        public string TableName;
    }

    public class SplitFieldData
    {
        public string FieldName;
        public string Divider;
        public int Number;
        public bool ProcessErrors;
        public bool IsDouble;
    }

}
