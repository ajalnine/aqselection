﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateFormatCombine(string tableName, List<string> columnsToCombine,
            bool enableHierarchySubdivision)
        {
            var currentParameters = new FormatCombineParameters
            {
                TableName = tableName,
                ColumnsToCombine = columnsToCombine,
                EnableHierarchySubdivision = enableHierarchySubdivision
            };
            var xs = new XmlSerializer(typeof (FormatCombineParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_FormatCombine",
                EditorAssemblyPath = "SLCalc_FormatCombine",
                Group = "Формат",
                Name = "Смежные ячейки",
                ImagePath = "/Resources;component/Images/Modules/RowCombine.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("FormatCombineParameters", "Parameters");
            return currentStep;
        }
    }

    public class FormatCombineParameters
    {
        public string TableName;
        public List<string> ColumnsToCombine;
        public bool EnableHierarchySubdivision;
    }
}
