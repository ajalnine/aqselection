﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateFormatBorders(string tableName, List<string> rightBordered, List<string> leftBordered,
            List<string> horizontalBordered, List<string> changeBordered)
        {
            var currentParameters = new FormatBordersParameters
            {
                TableName = tableName,
                RightBordered = rightBordered,
                LeftBordered = leftBordered,
                HorizontalBordered = horizontalBordered,
                ChangeBordered = changeBordered
            };
            var xs = new XmlSerializer(typeof (FormatBordersParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_FormatBorders",
                EditorAssemblyPath = "SLCalc_FormatBorders",
                Group = "Формат",
                Name = "Рамки ячеек",
                ImagePath = "/Resources;component/Images/Modules/Borders.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("FormatBordersParameters", "Parameters");
            return currentStep;
        }
    }
    public class FormatBordersParameters
    {
        public string TableName;
        public List<string> RightBordered;
        public List<string> LeftBordered;
        public List<string> HorizontalBordered;
        public List<string> ChangeBordered;
    }
}
