﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateGrouping(string tableName, List<GroupTableData> groupTableData)
        {
            var currentParameters = new GroupingParameters {TableName = tableName, GroupingFields = groupTableData};
            var xs = new XmlSerializer(typeof (GroupingParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Grouping",
                EditorAssemblyPath = "SLCalc_Grouping",
                Group = "Преобразование",
                Name = "Группировка записей",
                ImagePath = "/Resources;component/Images/Modules/Grouping.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("GroupingParameters", "Parameters");
            return currentStep;
        }

    }

    public class GroupingParameters
    {
        public List<GroupTableData> GroupingFields;
        public string TableName;
    }
    public class GroupTableData
    {
        public string FieldName;
        public bool IsGrouping;
        public bool EmitMin;
        public bool EmitMax;
        public bool EmitRange;
        public bool EmitAvg;
        public bool EmitMed;
        public bool EmitSum;
        public bool EmitStdev;
        public bool EmitCount;
        public bool EmitList;
        public bool EmitFirst;
        public bool EmitLast;
        public bool DisableAvg;
    }
}
