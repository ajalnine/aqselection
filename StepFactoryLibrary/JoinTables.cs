﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {

        //joinType : INNER, FULL, RIGHT, LEFT,
        public static Step CreateJoinTables(string firstTable, string firstKey, string secondTable, string secondKey, string joinType)
        {
            var currentParameters = new JoinTableParameters {FirstKey = firstKey, FirstTable = firstTable, SecondKey = secondKey, SecondTable = secondTable, JoinType = joinType};
            var xs = new XmlSerializer(typeof(JoinTableParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_JoinTables",
                EditorAssemblyPath = "SLCalc_JoinTables",
                Group = "Преобразование",
                Name = "Объединение двух таблиц",
                ImagePath = "/Resources;component/Images/Modules/TableJoin.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("JoinTableParameters", "Parameters");
            return currentStep;
        }
    }

    public class JoinTableParameters
    {
        public string FirstTable;
        public string FirstKey;
        public string SecondTable;
        public string SecondKey;
        public string JoinType;
    }
}
