﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateLimitSize(string tableName, List<string> grouping, int size, bool deleteSmallGroups, string mode, string zField)
        {
            var currentParameters = new LimitSizeParameters
            {
                TableName = tableName,
                Grouping = grouping,
                Size = size, 
                Mode = mode,
                DeleteSmallGroups = deleteSmallGroups,
                ZField = zField
            };
            var xs = new XmlSerializer(typeof(LimitSizeParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_LimitSize",
                EditorAssemblyPath = "SLCalc_LimitSize",
                Group = "Фильтрация",
                Name = "Ограничение размера групп",
                ImagePath = "/Resources;component/Images/Modules/LimitSize.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("LimitSizeParameters", "Parameters");
            return currentStep;
        }
    }
    public class LimitSizeParameters
    {
        public string TableName;
        public List<string> Grouping;
        public int Size;
        public bool DeleteSmallGroups;
        public string Mode;
        public string ZField;
    }
}
