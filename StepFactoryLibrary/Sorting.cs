﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using Core.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace StepFactoryLibrary 
{
    public static partial class StepFactory
    {
        public static Step CreateSorting(string tableName, List<string> sortOrder, bool isAscending)
        {
            var currentParameters = new SortingParameters
            {
                TableName = tableName,
                SortOrder = sortOrder,
                IsAscending = isAscending
            };
            var xs = new XmlSerializer(typeof (SortingParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Sorting",
                EditorAssemblyPath = "SLCalc_Sorting",
                Group = "Преобразование",
                Name = "Сортировка записей",
                ImagePath = "/Resources;component/Images/Modules/Sorting.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("SortingParameters", "Parameters");
            return currentStep;
        }
    }
    public class SortingParameters
    {
        public string TableName;
        public List<string> SortOrder;
        public bool IsAscending;
    }
}
