﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.EntityClient;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace AQSelection
{
    public partial class AQ_CatalogueItems
    {
        #region Navigation Properties

        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        public EntityCollection<AQ_CatalogueItems> AQ_CatalogueItems3
        {
            get
            {
                return AQ_CatalogueItems1;
            }
        }
        #endregion
    }
}
