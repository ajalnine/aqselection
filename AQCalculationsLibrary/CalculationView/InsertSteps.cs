﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {

        private void ProcessOperations(Action callback)
        {
            if (_currentCalculation?.StepData == null)
            {
                _results = _inputs.ToList();
                callback.Invoke();
                return;
            }
            var oviews = new List<OperationView>();
            foreach (var s in _currentCalculation.StepData)
            {
                var ov = new OperationView
                {
                    AutoEdit = false,
                    OperationBorderBrush = OperationsToolBox.GetBackgroundByGroupName(s.Group),
                    PreviousResultDataItems = _results,
                    PresentedStep = s,
                    BoundModule = new CalculationStep
                    {
                        Calculator = s.Calculator,
                        EditorAssembly = s.EditorAssemblyPath,
                        ImageUrl =
                            s.ImagePath.Replace("/AQSelection/Images/", "/AQResources;component/Images/"),
                        Name = s.Name,
                        Group = StepGroupConvert.NameToGroup(s.Group),
                        Wide = OperationsToolBox.GetWide(s.EditorAssemblyPath)
                    },
                    StepReadyToSave = true
                };
                var m = OperationsToolBox.GetModuleByName(s.Name);
                if (m!=null) ov.BoundModule.Wide = m.Wide;
                ov.StateChanged += OperationView_StateChanged;
                ov.StepAboutToDelete += OperationView_StepAboutToDelete;
                ov.RequestForPreview += OperationView_RequestForPreview;
                ov.RequestForData += OperationView_RequestForData;
                ov.AskForContextMenu += AskForContextMenu_Invoked;
                ov.GeometryChanged += OperationView_GeometryChanged;
                ov.DataUpdated += OperationView_DataUpdated;
                ov.HorizontalAlignment = ov.BoundModule.Wide ? HorizontalAlignment.Stretch : HorizontalAlignment.Center;
                ov.ControlPreprocess();
                oviews.Add(ov);
            }
            _loadedOperationViews = oviews;
            _currentInsertionPointView = _loadedOperationViews.Last();
            ShowToolBoxAfter(_loadedOperationViews.LastOrDefault());
            ResetProgressInScheme();
            CreateOperationsGeometry();
            TraceDataFlow(callback);
        }
    }
}
