﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;


namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        #region Связи между модулями

        public Dictionary<int, List<int>> DataLinks;
        private bool _linkUpdateRequired;

        private void RenewScheme(bool recreateGeometry)
        {
            UpdateLinkInformation();
            if (recreateGeometry) CreateOperationsGeometry();
            else UpdateOperationsGeometry();
            RefreshToolBox();
            UpdateLinkView();
            _geometryTrackingIsDisabled = false;
        }
        
        private void UpdateLinkInformation()
        {
            DataLinks = new Dictionary<int, List<int>>();

            if (_loadedOperationViews == null) return;

            for (var i = 1; i < _loadedOperationViews.Count(); i++)
            {
                var l = new List<int>();
                if (_loadedOperationViews[i].PresentedStep.Inputs != null)
                {
                    foreach (var di in _loadedOperationViews[i].PresentedStep.Inputs)
                    {
                        var ov = GetOperationViewForInput(_loadedOperationViews[i], di);
                        if (ov == null) l.Add(-1);
                        var index = _loadedOperationViews.IndexOf(ov);
                        if (!l.Contains(index)) l.Add(index);
                    }
                }
                if (l.Count > 0) DataLinks.Add(i, l);
            }
        }

        private void UpdateLinkView()
        {
            if (LinksLayer != null && LinksLayer.Children.Count > 0) LinksLayer.Children.Clear();
            if (DataLinks == null || _loadedOperationViews == null) return;

            foreach (var i in DataLinks.Keys)
            {
                var inputs = DataLinks[i];
                foreach (var j in inputs)
                {
                    DrawLink(
                        j >= 0
                            ? _loadedOperationViews[j].GetOutputsLinkPoint(LinksLayer)
                            : new Point(LinksLayer.ActualWidth/2 - 150, -35),
                        _loadedOperationViews[i].GetInputsLinkPoint(LinksLayer));
                }
            }

            try
            {
                LinksLayer?.UpdateLayout();
            }
            catch (Exception)
            {

            }

            _linkUpdateRequired = false;
        }

        private void DrawLink(Point @from, Point to)
        {
            var p = new System.Windows.Shapes.Path
            {
                Stroke = new SolidColorBrush(Color.FromArgb(0xff, 0x4a, 0x7e, 0xb0)),
                Fill = new SolidColorBrush(Color.FromArgb(0xff, 0x4a, 0x7e, 0xb0)),
                StrokeThickness = 0.5,
                StrokeEndLineCap = PenLineCap.Triangle
            };
            var caps = new DoubleCollection {6, 6};
            p.StrokeDashArray = caps;
            var binding = new System.Windows.Data.Binding
            {
                Source = Resources["PositionAnimator"],
                Path = new PropertyPath("Offset")
            };
            p.SetBinding(System.Windows.Shapes.Shape.StrokeDashOffsetProperty, binding);
            
            p.UseLayoutRounding = true;
            var pg = new PathGeometry();
            p.Data = pg;
            var pf = new PathFigure {IsClosed = false, IsFilled = false, StartPoint = @from};

            var arrow = new PathFigure {IsClosed = true, IsFilled = true, StartPoint = to};

            pg.Figures = new PathFigureCollection {pf, arrow};
            var ls2 = new LineSegment {Point = new Point(@from.X, @from.Y + 20)};
            var ls3 = new BezierSegment
            {
                Point1 = new Point(@from.X, @from.Y + 120),
                Point3 = new Point(to.X, to.Y - 20),
                Point2 = new Point(to.X, to.Y - 100)
            };
            var ls4 = new LineSegment {Point = new Point(to.X, to.Y - 20)};
            var ls5 = new LineSegment {Point = new Point(to.X, to.Y - 8)};

            var as1 = new LineSegment {Point = new Point(to.X - 4, to.Y - 10)};
            var as2 = new LineSegment {Point = new Point(to.X, to.Y - 8)};
            var as3 = new LineSegment {Point = new Point(to.X + 4, to.Y - 10)};

            pf.Segments = new PathSegmentCollection {ls2, ls3, ls4, ls5};

            arrow.Segments = new PathSegmentCollection {as1, as2, as3};
            Canvas.SetZIndex(p, 0);
            LinksLayer.Children.Add(p);
        }

        #endregion

        #region Отображение схемы расчета

        private void CreateOperationsGeometry()
        {
            _geometryTrackingIsDisabled = true;
            if (_loadedOperationViews == null) return;
            EditorsLayer.RowDefinitions.Clear();
            EditorsLayer.Children.Clear();
            foreach (var o in _loadedOperationViews)EditorsLayer.RowDefinitions.Add(new RowDefinition{ Height = GridLength.Auto });
            var insertionPoint = new Point(EditorsLayer.ActualWidth / 2, 0);
            var row = 0;
            foreach (var ov in _loadedOperationViews)
            {
                EditorsLayer.Children.Add(ov);
                ov.UpdateLayout();
                Grid.SetRow(ov, row);
                insertionPoint = ov.GetNextOperationPoint(EditorsLayer, 0);
                row++;
            }
            UpdateLayout();
            CalculationLayout.Height = insertionPoint.Y;
            _geometryTrackingIsDisabled = false;
        }

        private void UpdateOperationsGeometry()
        {
            if (EditorsLayer == null) return;
            _geometryTrackingIsDisabled = true;
            var insertionPoint = new Point(EditorsLayer.ActualWidth / 2, 0);
            EditorsLayer.RowDefinitions.Clear();
            foreach (var o in EditorsLayer.Children) EditorsLayer.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            var row = 0;
            foreach (var uiElement in EditorsLayer.Children)
            {
                var ov = (OperationView) uiElement;
                try
                {
                    ov.UpdateLayout();
                }
                catch { }
                Grid.SetRow(ov, row);
                insertionPoint = ov.GetNextOperationPoint(EditorsLayer, 0);
                row++;
            }
            UpdateLayout();
            CalculationLayout.Height = insertionPoint.Y;
            _geometryTrackingIsDisabled = false;
        }

        private void AddOperationInGeometry(int index, OperationView ov)
        {
            if (EditorsLayer.Children.Count == 0) EditorsLayer.Children.Add(ov);
            else EditorsLayer.Children.Insert(index, ov);
            UpdateOperationsGeometry();
        }
        #endregion
    }
}
