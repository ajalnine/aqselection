﻿ // ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        private void OperationView_RequestForData(object o, GenericEventArgs e)
        {
            e.CustomData = GetOperationViewsBefore((OperationView)o).Count;
            RequestForData?.Invoke(o, e);
        }

        private void OperationView_RequestForPreview(object o, GenericEventArgs e)
        {
            e.CustomData = GetOperationViewsBefore((OperationView)o).Count;
            RequestForPreview?.Invoke(o, e);
        }
    }
}
