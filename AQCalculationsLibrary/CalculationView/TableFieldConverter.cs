﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQCalculationsLibrary
{
    public class TableFieldConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = GetFieldTypeName(value?.ToString());
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }

        public static string GetFieldTypeName(string sr)
        {
            if (sr == null) return "Текст";
            string typeDescription;
            sr = sr.Replace("System.", "");
            switch (sr.ToLower())
            {
                case "datetime":
                    typeDescription = "Дата";
                    break;
                case "timespan":
                    typeDescription = "00:00";
                    break;
                case "string":
                    typeDescription = "Текст";
                    break;
                case "boolean":
                case "bool":
                    typeDescription = "1/0";
                    break;
                default:
                    typeDescription = "Число";
                    break;
            }
            return typeDescription;
        }
    }
}
