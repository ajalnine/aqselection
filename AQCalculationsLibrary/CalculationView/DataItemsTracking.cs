﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        #region Трекинг источников данных

        private bool _updateInProgress;

        public void TraceDataFlow(Action callback)
        {
            if (_loadedOperationViews == null) return;

            _updateInProgress = true;
            EnableOperationViews(true);
            _geometryTrackingIsDisabled = true;

            _results = _inputs.ToList();
            IsEnabled = false;
            if (_loadedOperationViews.Count == 0) DataFlowFinalize(true, callback);
            else
            {
                _loadedOperationViews[0].ShowProcessingState(TaskState.Preprocessing);
                _loadedOperationViews[0].PreviousResultDataItems = _results.ToList();
                _loadedOperationViews[0].DataFlow(callback);
                
            }
        }
        
        private void OperationView_DataUpdated(object o, GenericEventArgs e)
        {
            if (!_updateInProgress)
            {
                IsEnabled = false;
                TraceDataFlow(e.Callback);
            }
            else
            {
                IsEnabled = false;
                var v = o as OperationView;
                v?.ShowProcessingState(TaskState.Preprocessing);
                UpdateCurrentResults(v);

                var currentIndex = _loadedOperationViews.IndexOf(v);
                if (currentIndex == _loadedOperationViews.Count - 1) DataFlowFinalize(true, e.Callback);
                else
                {
                    _loadedOperationViews[currentIndex + 1].PreviousResultDataItems = _results.ToList();
                    _loadedOperationViews[currentIndex + 1].DataFlow(e.Callback);
                }
            }
        }
        
        private void DataFlowFinalize(bool recreateGeometry, Action callback)
        {
            _updateInProgress = false;
            _currentCalculation = new CalculationDescription
            {
                CanBeDeleted = true,
                StepData = _loadedOperationViews.Select(a => a.GetPresentedStep()).ToList(),
                Results = _results.ToList()
            };
            foreach (var o in _loadedOperationViews)
            {
                o.ShowProcessingState(TaskState.Inactive);
            }
            IsEnabled = true;
            EnableOperationViews(true);
            callback?.Invoke();
            RenewScheme(recreateGeometry);

            DataFlowChanged?.Invoke(this, new GenericEventArgs());
        }

        private void UpdateCurrentResults(OperationView v)
        {
            if (v?.PresentedStep.Outputs != null)
            {
                foreach (var s in v.PresentedStep.Outputs)
                {
                    var existed = _results.Where(a => a.Name == s.Name);
                    var dataItems = existed as DataItem[] ?? existed.ToArray();
                    if (dataItems.Any()) _results.Remove(dataItems.Single());
                    _results.Add(s);
                }
            }

            if (v?.PresentedStep.DeletedItems != null)
            {
                foreach (var s in v.PresentedStep.DeletedItems)
                {
                    var res = _results.SingleOrDefault(a => a.Name == s);
                    if (res != null) _results.Remove(res);
                }
            }
        }

        private void EnableOperationViews(bool enable)
        {
            foreach (var o in _loadedOperationViews) o.IsEnabled = enable;
        }
        #endregion
    }

    public class GenericEventArgs : EventArgs
    {
        public Action Callback;
        public object CustomData;
    }
}
