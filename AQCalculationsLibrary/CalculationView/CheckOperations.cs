﻿using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        #region Проверка расчета

        public string CheckOperations()
        {
            byte consistency = 0;
            if (_loadedOperationViews == null) return string.Empty;
            if (_loadedOperationViews.Count == 0) consistency |= (byte)CalculationConsistencyFlags.NoSteps;
            foreach (var v in _loadedOperationViews)
            {
                if (v.CurrentInconsistenceState == InconsistenceState.LostInput) consistency |= (byte)CalculationConsistencyFlags.HasLostSources;
                if (v.CurrentInconsistenceState == InconsistenceState.LostInputFields) consistency |= (byte)CalculationConsistencyFlags.HasLostSourceFields;
                if (!v.StepReadyToSave)
                {
                    consistency |= (byte)CalculationConsistencyFlags.HasEmptySteps;
                }
                v.EnableDeleting(true);
            }
            var message = string.Empty;

            if ((consistency & (byte)CalculationConsistencyFlags.HasEmptySteps) != 0)
            {
                message += "У некоторых шагов расчета не заданы параметры \r\n";
            }

            if ((consistency & (byte)CalculationConsistencyFlags.HasLostSources) != 0)
            {
                message += "Некоторые шаги расчета ссылаются на отсутствующие источники данных \r\n";
            }

            if ((consistency & (byte)CalculationConsistencyFlags.HasLostSourceFields) != 0)
            {
                message += "В некоторых источниках данных отсутствуют требуемые поля \r\n";
            }

            if ((consistency & (byte)CalculationConsistencyFlags.NoSteps) != 0)
            {
                message += "Расчет не задан \r\n";
            }
            return message;
        }

        private List<OperationView> GetOperationViewsBefore(OperationView currentView)
        {
            var index = _loadedOperationViews.IndexOf(currentView);
            var result = new List<OperationView>();
            for (var i = 0; i < index; i++)
            {
                result.Add(_loadedOperationViews[i]);
            }
            return result;
        }

        private OperationView GetOperationViewForInput(OperationView currentView, DataItem input)
        {
            var index = _loadedOperationViews.IndexOf(currentView);
            if (index == 0) return null;
            for (var i = index - 1; i >= 0; i--)
            {
                if (_loadedOperationViews[i].PresentedStep.Outputs == null) continue;
                if ((from o in _loadedOperationViews[i].PresentedStep.Outputs where DataItemHelper.IsEqual(input, o) == DataItemCompareResult.Equal select o).Count() == 1)
                {
                    return _loadedOperationViews[i];
                }
            }
            return null;
        }

        #endregion
    }
}
