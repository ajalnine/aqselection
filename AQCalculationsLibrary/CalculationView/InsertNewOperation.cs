﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;


namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        #region Добавление операции к расчету

        private OperationView _currentInsertionPointView;
        
        private void OperationsToolBox_ModuleSelected(object sender, ModuleSelectedEventArgs e)
        {
            if (_loadedOperationViews == null) return;
            InsertNewModule(e.SelectedModule, _currentInsertionPointView);
        }

        private void InsertNewModule(CalculationStep md, OperationView currentView)
        {
            var ov = new OperationView();
            ov.StateChanged += OperationView_StateChanged;
            ov.StepAboutToDelete += OperationView_StepAboutToDelete;
            ov.GeometryChanged += OperationView_GeometryChanged;
            ov.RequestForPreview += OperationView_RequestForPreview;
            ov.RequestForData += OperationView_RequestForData;
            ov.AskForContextMenu += AskForContextMenu_Invoked;
            ov.DataUpdated += OperationView_DataUpdated;
            ov.AutoEdit = true;
            ov.OperationBorderBrush = OperationsToolBox.GetBackgroundByGroup(md.Group);

            InsertIntoSchema(currentView, ov);
            UpdatePreviousResults(ov);
            OperationsToolBox.Visibility = Visibility.Collapsed;
            ov.CreateViewForModule(md);
            _currentInsertionPointView = _loadedOperationViews.Last();
            ov.HorizontalAlignment = md.Wide ? HorizontalAlignment.Stretch : HorizontalAlignment.Center;
        }

        private void UpdatePreviousResults(OperationView ov)
        {
            int i = _loadedOperationViews.IndexOf(ov);

            if (i == 0) ov.PreviousResultDataItems = _inputs;
            else
            {
                ov.PreviousResultDataItems = _loadedOperationViews[i - 1].PresentedStep.Outputs;
                foreach (var r in _loadedOperationViews[i - 1].PreviousResultDataItems)
                {
                    if (ov.PreviousResultDataItems.All(a => a.Name != r.Name))
                    {
                        ov.PreviousResultDataItems.Add(r);
                    }
                }

                if (_loadedOperationViews[i - 1].PresentedStep.DeletedItems != null && _loadedOperationViews[i - 1].PresentedStep.DeletedItems.Count > 0)
                {
                    foreach (var d in _loadedOperationViews[i - 1].PresentedStep.DeletedItems)
                    {
                        ov.PreviousResultDataItems.Remove(ov.PreviousResultDataItems.Single(a => a.Name == d));
                    }
                }
            }
        }

        private void InsertIntoSchema(OperationView currentView, OperationView ov)
        {
            if (currentView == null)
            {
                if (_loadedOperationViews.Count == 0)
                {
                    _loadedOperationViews.Add(ov);
                }
                else
                {
                    _loadedOperationViews.Insert(0, ov);
                }
                AddOperationInGeometry(0, ov);
            }
            else
            {
                int index = _loadedOperationViews.IndexOf(currentView);
                if (index == _loadedOperationViews.Count)
                {
                    _loadedOperationViews.Add(ov);
                }
                else
                {
                    _loadedOperationViews.Insert(index + 1, ov);
                }
                AddOperationInGeometry(index + 1, ov);
            }
        }

        void OperationView_GeometryChanged(object o, EventArgs e)
        {
            UpdateOperationsGeometry();
        }

        private void OperationsToolBox_ToolBoxClosing(object sender, EventArgs e)
        {
            CloseToolBox();
        }

        private void CloseToolBox()
        {
            OperationsToolBox.Visibility = Visibility.Collapsed;
            EnableModuleInsertion(false, null);
        }

        private void OperationsToolBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Canvas.SetLeft(OperationsToolBox, ToolBoxLayer.ActualWidth / 2 - OperationsToolBox.ActualWidth - 8);
        }

        private void OperatingToolBox_ModeChanged(object sender, EventArgs e)
        {
            RefreshToolBox();
        }

        private void AskForContextMenu_Invoked(object o, EventArgs e)
        {
            var ov = o as OperationView;
            _currentInsertionPointView = ov;
            EnableModuleInsertion(true, ov);
            ShowToolBoxAfter(ov);
        }

        private void AskForContextMenuForBegin_Invoked(object o, EventArgs e)
        {
            ShowToolBoxForFirstItem();
        }

        private void ShowToolBoxForFirstItem()
        {
            EnableModuleInsertion(true, null);
            _currentInsertionPointView = null;
            OperationsToolBox.Visibility = Visibility.Visible;
            ToolBoxLayer.UpdateLayout();
            Canvas.SetLeft(OperationsToolBox, ToolBoxLayer.ActualWidth / 2 - OperationsToolBox.ActualWidth - 8);
            Canvas.SetTop(OperationsToolBox, -20);
        }

        private void ShowToolBoxForLastItem()
        {
            OperationView ov = _loadedOperationViews.LastOrDefault();
            if (ov == null) ShowToolBoxForFirstItem();
            else SetToolBoxToInsertionPoint(ov);
        }

        private void SetToolBoxToInsertionPoint(OperationView ov)
        {
            EnableModuleInsertion(true, ov);
            _currentInsertionPointView = ov;
            var newPosition = ov.GetContextMenuCallPoint(EditorsLayer);
            UpdateLayout();
            Canvas.SetLeft(OperationsToolBox, ToolBoxLayer.ActualWidth / 2 - OperationsToolBox.ActualWidth - 8);
            Canvas.SetTop(OperationsToolBox, newPosition.Y + 16);
            OperationsToolBox.Visibility = Visibility.Visible;
        }

        private void ShowToolBoxAfter(OperationView ov)
        {
            if (ov != null) SetToolBoxToInsertionPoint(ov);
            else ShowToolBoxForFirstItem();
        }

        private void EnableModuleInsertion(bool isEnabled, OperationView ov)
        {
            foreach (var m in OperationsToolBox.AvailableModules.Children)
            {
                OperationsToolBox.EnableModule(m, isEnabled && CheckPossibilityForInsertion(m, ov));
            }
        }

        private void RefreshToolBox()
        {
            EnableModuleInsertion(true, _currentInsertionPointView);
            ShowToolBoxAfter(_currentInsertionPointView);
        }

        private bool CheckPossibilityForInsertion(CalculationStep md, OperationView ov)
        {
            if (md.Disabled) return false;
            if (!string.IsNullOrEmpty(md.RequiresRole) && !Security.IsInRole(md.RequiresRole)) return false;
            if (!md.RequiresSelection) return true;
            var currentOutputsBefore = (ov?.PresentedStep.Outputs != null) 
                ? ov.PreviousResultDataItems?.Union(ov.PresentedStep.Outputs) ?? _inputs
                : _inputs;
            var outputsBefore = currentOutputsBefore as IList<DataItem> ?? currentOutputsBefore?.ToList();
            if (md.RequiresMultipleSources && outputsBefore?.Where(a => a != null).Count(s => s.DataItemType == DataType.Selection) < 2) return false;
            return outputsBefore != null && (from a in outputsBefore where a.DataItemType == DataType.Selection select a).Any();
        }

        #endregion
    }
}
