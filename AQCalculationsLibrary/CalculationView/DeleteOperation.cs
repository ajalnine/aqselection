﻿using System;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        private void EnableDelete(bool enable)
        {
            var displayed = _loadedOperationViews.ToList();
            for (var i = 0; i < displayed.Count - 1; i++)
            {
                displayed[i].EnableDeleting(enable);
            }
        }

        private void OperationView_StepAboutToDelete(object sender, EventArgs e)
        {
            CloseToolBox();
            var ov = (OperationView) sender;
      //    if (ov.GetPresentedStep().Group == "Параметры") MainDateRangePicker.ResetView();
            var index = _loadedOperationViews.IndexOf(ov);
            var isLast = (index == _loadedOperationViews.Count - 1);
            _loadedOperationViews.RemoveAt(index);
            EditorsLayer.Children.RemoveAt(index);
            TraceDataFlow(null);
            if (isLast) ShowToolBoxForLastItem();
            else if (_loadedOperationViews.Count > 0) RefreshToolBox();
            else ShowToolBoxForFirstItem();
        }
    }
}