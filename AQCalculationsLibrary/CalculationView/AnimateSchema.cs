﻿using System;
using System.ComponentModel;
using System.Windows.Threading;


namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        readonly DispatcherTimer _animationTimer;
        private bool _geometryTrackingIsDisabled;

        void AnimationTimer_Tick(object sender, EventArgs e)
        {
            ((PositionAnimator)Resources["PositionAnimator"]).Offset -= 1;
            if (!_linkUpdateRequired || _geometryTrackingIsDisabled) return;
            _animationTimer.Stop();
            UpdateLinkView();
            _animationTimer.Start();
        }
    }

    public class PositionAnimator : INotifyPropertyChanged
    {
        private double _offset;
        public double Offset
        {
            get
            {
                return _offset;
            }
            set
            {
                _offset = value;
                NotifyPropertyChanged("Offset");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
