﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        private List<OperationView> _loadedOperationViews;
        
        public delegate void GenericDelegate(object o, GenericEventArgs e);

        public event GenericDelegate DataFlowChanged;
        public event GenericDelegate RequestForPreview;
        public event GenericDelegate RequestForData;
        public event GenericDelegate BlockSaving;

        private List<DataItem> _results;
        private List<DataItem> _inputs;
        private CalculationDescription _currentCalculation;
        
        public double ScrollOffset
        {
            get { return (double)GetValue(ScrollOffsetProperty); }
            set { SetValue(ScrollOffsetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScrollOffset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScrollOffsetProperty =
            DependencyProperty.Register("ScrollOffset", typeof(double), typeof(CalculationView), new PropertyMetadata(0.0d));


        public bool DisableScrollControlling
        {
            get { return (bool)GetValue(DisableScrollControllingProperty); }
            set { SetValue(DisableScrollControllingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisableScrollControlling.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableScrollControllingProperty =
            DependencyProperty.Register("DisableScrollControlling", typeof(bool), typeof(CalculationView), new PropertyMetadata(true));


        public CalculationDescription GetCalculation()
        {
            return _currentCalculation;
        }

        public List<DataItem> GetResults()
        {
            return _results;
        }

        public List<string> GetStepNames()
        {
            if (_currentCalculation?.StepData == null || _currentCalculation.StepData.Count==0) return new List<string>();
            return _currentCalculation.StepData.Select(a => a.Name).ToList();
        }

        public CalculationView()
        {
            InitializeComponent();
            _loadedOperationViews = new List<OperationView>();
            _animationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 200) };
            _animationTimer.Tick += AnimationTimer_Tick;
            _animationTimer.Start();
            OperationsToolBox.ReconstructToolBox();
        }

        public void InsertSteps(List<Step> steps, List<DataItem> inputs, Action callback)
        {
            if (inputs != null) _inputs = inputs;
            if (_loadedOperationViews != null && _currentCalculation?.StepData != null && _currentCalculation.StepData.Count > 0 )
            {
                var insertionIndex = (_currentInsertionPointView != null)
                    ? _loadedOperationViews.IndexOf(_currentInsertionPointView) + 1
                    : 0;
                if (steps!=null)_currentCalculation.StepData.InsertRange(insertionIndex, steps);
            }
            else
            {
                _currentCalculation = new CalculationDescription { StepData = steps };
            }
            if (callback!=null) ProcessOperations(callback);
        }
        
        #region Отображение прогресса

        public void ShowProgressInScheme(int number, TaskState taskState)
        {
            if (number >= _loadedOperationViews.Count || number>=_currentCalculation.StepData.Count) return;
            if (_loadedOperationViews[number]?.PresentedStep == _currentCalculation?.StepData[number])
            {
                Dispatcher.BeginInvoke(() => { _loadedOperationViews[number].ShowProcessingState(taskState); });
            }
        }

        public void ResetProgressInScheme()
        {
            Dispatcher.BeginInvoke(() =>
            {
                foreach (var v in _loadedOperationViews) v.ShowProcessingState(TaskState.Inactive);
            });
        }

        public void FinalizeProgressInScheme()
        {
            Dispatcher.BeginInvoke(() =>
            {
                var allTime = _loadedOperationViews.Sum(a => a.TotalTime.TotalSeconds);
                foreach (var v in _loadedOperationViews.Take(_loadedOperationViews.Count - 1)) v.SetPartialTime(allTime);
                _loadedOperationViews[_loadedOperationViews.Count - 1].SetTotalTime(allTime);
            });
        }
        #endregion
    }
}
