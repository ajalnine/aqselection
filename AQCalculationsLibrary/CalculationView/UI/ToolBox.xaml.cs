﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.IO.IsolatedStorage;
using System.Windows.Controls;
using System.Windows.Media;
using AQCalculationsLibrary;
using AQBasicControlsLibrary;

// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public partial class ToolBox
    {
        public enum ToolBoxMode { SmallButtons, LargeButtons, Tabbed }

        public ToolBoxMode CurrentToolBoxMode
        {
            get
            {
                if ((from a in IsolatedStorageSettings.ApplicationSettings where a.Key == "SLCalculationConstructorToolBoxMode" select a).Count() != 0)
                {
                    return (ToolBoxMode)IsolatedStorageSettings.ApplicationSettings["SLCalculationConstructorToolBoxMode"];
                }
                return ToolBoxMode.Tabbed;
            }
            set
            {
                IsolatedStorageSettings.ApplicationSettings["SLCalculationConstructorToolBoxMode"] = value;
            }
        }

        public delegate void ModuleSelectedDelegate(object o, ModuleSelectedEventArgs e);
        public event ModuleSelectedDelegate ModuleSelected;
        public delegate void ToolBoxClosingDelegate(object o, EventArgs e);
        public event ToolBoxClosingDelegate ToolBoxClosing;
        public delegate void ModeChangedDelegate(object o, EventArgs e);
        public event ModeChangedDelegate ModeChanged;

        public CalculationStepsRoot AvailableModules { get; set; }
        private List<Control> _moduleSelectors;

        public ToolBox()
        {
            InitializeComponent();
        }

        public void ReconstructToolBox()
        {
            switch (CurrentToolBoxMode)
            {
                case ToolBoxMode.SmallButtons:
                    ConstructToolBoxSmallButtons();
                    break;
                case ToolBoxMode.LargeButtons:
                    ConstructToolBoxLargeButtons();
                    break;
                case ToolBoxMode.Tabbed:
                    ConstructToolBoxTabbed();
                    break;
            }
        }

        public CalculationStep GetQueryModule()
        {
            return AvailableModules.Children.SingleOrDefault(a => a.Name == "Запрос SQL");
        }

        public CalculationStep GetModuleByName(string name)
        {
            return AvailableModules.Children.SingleOrDefault(a => a.Name == name);
        }

        public Brush GetBackgroundByGroup(StepGroup @group)
        {
            switch (@group)
            {
                case StepGroup.DataSource:
                    return Resources["Group1Background"] as Brush;
                case StepGroup.Transform:
                    return Resources["Group2Background"] as Brush;
                case StepGroup.Filtering:
                    return Resources["Group4Background"] as Brush;
                case StepGroup.Calculation:
                    return Resources["Group5Background"] as Brush;
                case StepGroup.Parameters:
                    return Resources["Group6Background"] as Brush;
                case StepGroup.Format:
                    return Resources["Group7Background"] as Brush;
                case StepGroup.Report:
                    return Resources["Group8Background"] as Brush;
                default:
                    return Resources["Group1Background"] as Brush;
            }
        }

        public Brush GetBackgroundByGroupName(string groupName)
        {
            return GetBackgroundByGroup(StepGroupConvert.NameToGroup(groupName));
        }

        public bool GetWide(string eap)
        {
            return (AvailableModules.Children.SingleOrDefault(a => a.EditorAssembly == eap))?.Wide ?? false;
        }

        private void ConstructToolBoxSmallButtons()
        {
            OperationsPanel.Children.Clear();
            _moduleSelectors = new List<Control>();
            var groups = (from a in AvailableModules.Children select a.Group).Distinct().OrderBy(a => a);
            foreach (var g in groups)
            {
                var sp = new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Right };
                var currentGroup = g;
                var modules = from m in AvailableModules.Children where m.Group == currentGroup orderby m.Name select m;
                foreach (var m in modules)
                {
                    var mib = new TextImageButtonBase
                    {
                        Style = Resources["MiniImageButton"] as Style,
                        BorderThickness = new Thickness(0.5),
                        BorderBrush = new SolidColorBrush(Colors.Gray),
                        Tag = m,
                        ImageUrl = m.ImageUrl,
                        Width = 32,
                        Height = 32
                    };
                    mib.Click += LoadControlsAdditional_Click;
                    sp.Children.Add(mib);
                    _moduleSelectors.Add(mib);
                }
                OperationsPanel.Children.Add(sp);
            }
        }

        private void ConstructToolBoxLargeButtons()
        {
            OperationsPanel.Children.Clear();
            _moduleSelectors = new List<Control>();
            var wp = new WrapPanel { Orientation = Orientation.Horizontal, MaxWidth = 350 };

            foreach (var m in AvailableModules.Children)
            {
                var ib = new TextImageButtonBase
                {
                    Style = Resources["ImageButton"] as Style,
                    Text = m.Name,
                    BorderThickness = new Thickness(0.5),
                    BorderBrush = new SolidColorBrush(Colors.Gray),
                    Tag = m,
                    ImageUrl = m.ImageUrl,
                    Width = 65
                };
                ib.Click += LoadControlsAdditional_Click;
                wp.Children.Add(ib);
                _moduleSelectors.Add(ib);
            }
            OperationsPanel.Children.Add(wp);
        }

        private void ConstructToolBoxTabbed()
        {
            OperationsPanel.Children.Clear();
            var tc = new TabControl();
            _moduleSelectors = new List<Control>();
            var groups = (from a in AvailableModules.Children select a.Group).Distinct().OrderBy(a => a);
            foreach (var g in groups)
            {
                var tb = new TabItem
                {
                    Style = Resources["TabItemStyle"] as Style,
                    Header = new TextBlock { Text = StepGroupConvert.GroupToName(g), FontSize = 10}
                };
                var wp = new WrapPanel { Orientation = Orientation.Horizontal, MaxWidth = 450 };
                var currentGroup = g;
                var modules = from m in AvailableModules.Children where m.Group == currentGroup orderby m.Name select m;
                foreach (var m in modules)
                {
                    var ib = new TextImageButtonBase
                    {
                        Style = Resources["ImageButton"] as Style,
                        Text = m.Name,
                        BorderThickness = new Thickness(0.5),
                        BorderBrush = new SolidColorBrush(Colors.Gray),
                        Tag = m,
                        ImageUrl = m.ImageUrl,
                        Width = 75
                    };
                    ib.Click += LoadControlsAdditional_Click;
                    wp.Children.Add(ib);
                    _moduleSelectors.Add(ib);
                }
                tb.Content = wp;
                tc.Items.Add(tb);
            }
            OperationsPanel.Children.Add(tc);
        }

        public void EnableModule(CalculationStep md, bool isEnabled)
        {
            if (_moduleSelectors == null) return;
            var module = (from m in _moduleSelectors where (m.Tag as CalculationStep) == md select m).SingleOrDefault();
            if (module != null) module.IsEnabled = isEnabled;
        }

        private void LoadControlsAdditional_Click(object o, EventArgs e)
        {
            var c = o as Control;
            if (c == null) return;
            var m = c.Tag as CalculationStep;
            ModuleSelected?.Invoke(this, new ModuleSelectedEventArgs { SelectedModule = m });
        }

        private void ClosePanelButton_Click(object sender, RoutedEventArgs e)
        {
            ToolBoxClosing?.Invoke(this, new EventArgs());
        }

        private void ModeSmallButtons_Click(object sender, RoutedEventArgs e)
        {
            CurrentToolBoxMode = ToolBoxMode.SmallButtons;
            ReconstructToolBox();
            ModeChanged?.Invoke(this, new EventArgs());
        }

        private void ModeLargeButtons_Click(object sender, RoutedEventArgs e)
        {
            CurrentToolBoxMode = ToolBoxMode.LargeButtons;
            ReconstructToolBox();
            ModeChanged?.Invoke(this, new EventArgs());
        }

        private void ModeTabbed_Click(object sender, RoutedEventArgs e)
        {
            CurrentToolBoxMode = ToolBoxMode.Tabbed;
            ReconstructToolBox();
            ModeChanged?.Invoke(this, new EventArgs());
        }
    }

    public class ModuleSelectedEventArgs : EventArgs
    {
        public CalculationStep SelectedModule { get; set; }
    }
}
