﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace AQCalculationsLibrary
{
    public partial class OperationDivider
    {
        public delegate void AskForContextPopupDelegate(object o, EventArgs e);
        public event AskForContextPopupDelegate AskForContextMenu;

        void AnimationTimer_Tick(object sender, EventArgs e)
        {
            ((PositionAnimator)Resources["PositionAnimator"]).Offset -= 1;
        }

        public OperationDivider()
        {
            InitializeComponent();
            var animationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 50) };
            animationTimer.Tick += AnimationTimer_Tick;
            animationTimer.Start();
        }

        private void InsertModuleButton_Click(object sender, RoutedEventArgs e)
        {
            AskForContextMenu?.Invoke(this, new EventArgs());
        }

        private void Layout_MouseEnter(object sender, MouseEventArgs e)
        {
            InsertModuleButton.Opacity = 1;
            Sublight.Opacity = 1;
        }

        private void Layout_MouseLeave(object sender, MouseEventArgs e)
        {
            Sublight.Opacity = 0.4;
            InsertModuleButton.Opacity = 0.4;
        }
    }
}
