﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;

namespace AQCalculationsLibrary
{
    public partial class OperationView
    {
        public Step PresentedStep;

        private DateTime startOfExecution, endOfExecution;

        public CalculationStep BoundModule;
        private IDataFlowUI _loadedDataFlowUI;
        private IDataFlow _loadedDataFlow;

        public List<DataItem> PreviousResultDataItems;

        public delegate void GenericDelegate(object o, GenericEventArgs e);

        public event GenericDelegate StateChanged;
        public event GenericDelegate DataUpdated;
        public event GenericDelegate AskForContextMenu;
        public event GenericDelegate GeometryChanged;
        public event GenericDelegate StepAboutToDelete;
        public event GenericDelegate RequestForPreview;
        public event GenericDelegate RequestForData;
        public bool StepReadyToSave;
        public bool AutoEdit = true;
        private OperationViewMode _currentmode;

        public TimeSpan TotalTime => endOfExecution - startOfExecution;

        public OperationViewMode CurrentViewMode
        {
            get
            {
                return _currentmode;
            }
            set
            {
                _currentmode = value;
                ReflectStateChanges(value);
                StateChanged?.Invoke(this, new GenericEventArgs());
                UpdateLayout();
                GeometryChanged?.Invoke(this, new GenericEventArgs());
            }
        }

        public Brush OperationBorderBrush
        {
            get
            {
                return OperationPanel.BorderBrush;
            }
            set
            {
                var c = ((SolidColorBrush)value).Color;
                var bg = new SolidColorBrush(Color.FromArgb(0x10, c.R, c.G, c.B));
                var bg2 = new SolidColorBrush(Color.FromArgb(0xe0
                    , (byte)(c.R + (255 - (double)c.R) * 0.98)
                    , (byte)(c.G + (255 - (double)c.G) * 0.98)
                    , (byte)(c.B + (255 - (double)c.B) * 0.98)));
                OperationPanel.BorderBrush = value;
                InputsListPanel.BorderBrush = value;
                ControlPanel.BorderBrush = value;
                OutputsListPanel.BorderBrush = value;
                OperationPanel.Background = bg;
                InputsListPanel.Background = bg;
                OutputsListPanel.Background = bg;
                ControlPanel.Background = bg2;
            }
        }

        private InconsistenceState _currentinconsistencestate;
        public InconsistenceState CurrentInconsistenceState
        {
            get
            {
                return _currentinconsistencestate;
            }
            set
            {
                _currentinconsistencestate = value;
                ReflectInconsistenceState(value);
                _currentinconsistencestate = value;
            }
        }

        public OperationView()
        {
            InitializeComponent();
            CurrentViewMode = OperationViewMode.Empty;
            CurrentInconsistenceState = InconsistenceState.Norm;
        }

        #region Загрузка сборки

        public void CreateViewForModule(CalculationStep module)
        {
            BoundModule = module;
            PresentedStep = new Step
            {
                Calculator = BoundModule.Calculator,
                EditorAssemblyPath = BoundModule.EditorAssembly,
                ImagePath = BoundModule.ImageUrl,
                Name = Label.Text,
                Group = StepGroupConvert.GroupToName(BoundModule.Group),
                Inputs = new List<DataItem>(),
                Outputs = new List<DataItem>(),
                DeletedItems = new List<string>()
            };
            ControlPreprocess();
        }

        public void ControlPreprocess()
        {
            Label.Text = BoundModule.Name;
            Icon.Source = new BitmapImage { UriSource = new Uri(BoundModule.ImageUrl, UriKind.Relative) };
            CurrentViewMode = (AutoEdit) ? OperationViewMode.Edit : OperationViewMode.Display;
        }
        #endregion

        #region Обработка данных
        void LoadedDataFlowUI_ParametersAccepted(object o, ParametersAcceptedEventArgs e)
        {
            AutoEdit = false;
            PresentedStep.ParametersXML = e.ParametersXML;
            StepReadyToSave = true;
            if (e.CloseUI) CurrentViewMode = OperationViewMode.Display;
            DataFlow(null);
        }

        void LoadedDataFlow_DataFlowFinished(object o, DataFlowFinishedEventArgs e)
        {
            PresentedStep.Inputs = e.Inputs;
            PresentedStep.Outputs = e.Outputs;
            PresentedStep.DeletedItems = e.Deleted ?? new List<string>();
            RefreshDataPanels();
            if (DataUpdated != null) Dispatcher.BeginInvoke(() => DataUpdated.Invoke(this, new GenericEventArgs { Callback = _dataflowCallback }));
        }

        private Action _dataflowCallback;
        public void DataFlow(Action callback)
        {
            _dataflowCallback = callback;
            var name = $"{ BoundModule.EditorAssembly.Replace("SLCalc_DataSourceQuery", "SLCalc_DataSourceCalculation") + ".DataFlow"}, AQOperations, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            _loadedDataFlow = (IDataFlow)Activator.CreateInstance(Type.GetType(name));
            _loadedDataFlow.DataFlowFinished += LoadedDataFlow_DataFlowFinished;
            _loadedDataFlow.DataFlowError += LoadedDataFlow_DataFlowError;
            CurrentInconsistenceState = InconsistenceState.Norm;
            _loadedDataFlow.DataProcess(PreviousResultDataItems, PresentedStep.ParametersXML);
        }

        void LoadedDataFlow_DataFlowError(object o, DataFlowErrorEventArgs e)
        {
            if (CurrentInconsistenceState == InconsistenceState.Norm)
            {
                CurrentInconsistenceState = e.Error;
            }
        }
        #endregion

        #region Обработка событий интерфейса

        void LoadedEditorModule_NameChanged(object o, NameChangedEventArgs e)
        {
            Label.Text = e.NewName;
        }

        void LoadedEditorModule_DataRequest(object o, EventArgs e)
        {
            RequestForData?.Invoke(this, new GenericEventArgs());
        }

        public void ReflectStateChanges(OperationViewMode newState)
        {
            switch (CurrentViewMode)
            {
                case OperationViewMode.Empty:
                    InputsPanel.Visibility = Visibility.Collapsed;
                    OutputsPanel.Visibility = Visibility.Collapsed;
                    StepPreview.Visibility = Visibility.Collapsed;
                    ExpandButton.ImageUrl = "/AQResources;component/Images/Down.png";
                    ExpandButton.Visibility = Visibility.Collapsed;
                    ControlPanel.Visibility = Visibility.Collapsed;
                    LeftPanelColumnDefinition.Width = new GridLength(270, GridUnitType.Pixel);
                    RightPanelColumnDefinition.Width = new GridLength(270, GridUnitType.Pixel);
                    break;

                case OperationViewMode.Display:
                    RefreshDataPanels();
                    ControlPanel.Child = null;
                    _loadedDataFlowUI = null;
                    ExpandButton.ImageUrl = "/AQResources;component/Images/Down.png";
                    StepPreview.Visibility = Visibility.Visible;
                    ExpandButton.Visibility = Visibility.Visible;
                    ControlPanel.Visibility = Visibility.Collapsed;
                    LeftPanelColumnDefinition.Width = new GridLength(270, GridUnitType.Pixel);
                    RightPanelColumnDefinition.Width = new GridLength(270, GridUnitType.Pixel);
                    break;

                case OperationViewMode.Edit:
                    CreateDataFlowUI();
                    InputsPanel.Visibility = Visibility.Collapsed;
                    OutputsPanel.Visibility = Visibility.Collapsed;
                    StepPreview.Visibility = Visibility.Collapsed;
                    ExpandButton.Visibility = Visibility.Visible;
                    ExpandButton.ImageUrl = "/AQResources;component/Images/Up.png";
                    ControlPanel.Visibility = Visibility.Visible;
                    LeftPanelColumnDefinition.Width = new GridLength(0, GridUnitType.Auto);
                    RightPanelColumnDefinition.Width = new GridLength(0, GridUnitType.Auto);
                    break;
            }
        }

        private void CreateDataFlowUI()
        {
            var name = $"{ BoundModule.EditorAssembly.Replace("SLCalc_DataSourceQuery", "SLCalc_DataSourceCalculation") + ".Page"}, AQOperations, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            var uc = (UserControl)Activator.CreateInstance(Type.GetType(name));
            uc.HorizontalAlignment = HorizontalAlignment.Center;
            _loadedDataFlowUI = uc as IDataFlowUI;
            ControlPanel.Child = uc;
            uc.HorizontalAlignment = HorizontalAlignment.Stretch;
            if (_loadedDataFlowUI == null) return;
            _loadedDataFlowUI.NameChanged += LoadedEditorModule_NameChanged;
            _loadedDataFlowUI.DataRequest += LoadedEditorModule_DataRequest;
            _loadedDataFlowUI.ParametersAccepted += LoadedDataFlowUI_ParametersAccepted;
            ((UserControl)_loadedDataFlowUI).SizeChanged += OperationView_SizeChanged;
            if (!AutoEdit) _loadedDataFlowUI.SetupUI(PreviousResultDataItems, PresentedStep.ParametersXML);
            else _loadedDataFlowUI.SetupUI(PreviousResultDataItems);
        }

        private void OperationView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GeometryChanged?.Invoke(this, new GenericEventArgs());
        }

        public void EnablePreview(bool enabled)
        {
            StepPreview.IsEnabled = enabled;
        }

        private void ReflectInconsistenceState(InconsistenceState newState)
        {
            Dispatcher.BeginInvoke(() =>
            {
                switch (newState)
                {
                    case InconsistenceState.Norm:
                        ConsistenceStateImage.Visibility = Visibility.Collapsed;
                        //StepPreview.Visibility = Visibility.Visible;
                        break;
                    case InconsistenceState.LostInput:
                        ConsistenceStateImage.Visibility = Visibility.Visible;
                        StepPreview.Visibility = Visibility.Collapsed;
                        ConsistenceStateImage.Source =
                            new BitmapImage { UriSource = new Uri("/AQResources;component/Images/NoSource.png", UriKind.Relative) };
                        break;
                    case InconsistenceState.LostInputFields:
                        ConsistenceStateImage.Visibility = Visibility.Visible;
                        StepPreview.Visibility = Visibility.Collapsed;
                        ConsistenceStateImage.Source =
                            new BitmapImage { UriSource = new Uri("/AQResources;component/Images/NoSourceField.png", UriKind.Relative) };
                        break;
                }
            });
        }

        public void EnableDeleting(bool isEnabled)
        {
            StepDeleteButton.Visibility = (isEnabled) ? Visibility.Visible : Visibility.Collapsed;
        }

        public void RefreshDataPanels()
        {
            Dispatcher.BeginInvoke(() =>
            {
                if (PresentedStep.Inputs != null)
                {
                    var inputNames = (from i in PresentedStep.Inputs where i != null select i).ToList();
                    if (inputNames.Any())
                    {
                        InputsList.ItemsSource = inputNames;
                        InputsList.UpdateLayout();
                        InputsPanel.Visibility = Visibility.Visible;
                    }
                }
                if (PresentedStep.Outputs != null)
                {
                    var outputNames = (from i in PresentedStep.Outputs where i != null select i).ToList();
                    if (outputNames.Any())
                    {
                        OutputsList.ItemsSource = outputNames;
                        OutputsList.UpdateLayout();
                        OutputsPanel.Visibility = Visibility.Visible;
                    }
                }
            });
        }

        public Step GetPresentedStep()
        {
            return PresentedStep;
        }

        public void UpdatePresentedStep()
        {
            _loadedDataFlow.DataProcess(PreviousResultDataItems, PresentedStep.ParametersXML);
        }

        private void ExpandButton_Click(object sender, RoutedEventArgs e)
        {
            switch (CurrentViewMode)
            {
                case OperationViewMode.Display:
                    CurrentViewMode = OperationViewMode.Edit;
                    break;
                case OperationViewMode.Edit:
                    CurrentViewMode = OperationViewMode.Display;
                    break;
            }
        }

        public void DataReceived(string fileName)
        {
            _loadedDataFlowUI?.DataRequestReadyCallback(fileName);
        }

        private void Label_TextChanged(object sender, TextChangedEventArgs e)
        {
            PresentedStep.Name = Label.Text;
        }

        private void StepDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            StepAboutToDelete?.Invoke(this, new GenericEventArgs());
        }

        private void StepPreview_Click(object sender, RoutedEventArgs e)
        {
            RequestForPreview?.Invoke(this, new GenericEventArgs());
        }

        private void AskForContextMenu_Click(object sender, EventArgs e)
        {
            AskForContextMenu?.Invoke(this, new GenericEventArgs());
        }

        #endregion

        #region Геометрия
        public Point GetInputsLinkPoint(UIElement parent)
        {
            try
            {
                if (CurrentViewMode != OperationViewMode.Edit)
                {
                    var gt = InputsPanel.TransformToVisual(parent);
                    return gt.Transform(new Point(InputsPanel.ActualWidth / 2 - 20, 0));
                }
                else
                {
                    var gt = OperationPanel.TransformToVisual(parent);
                    return gt.Transform(new Point(OperationPanel.ActualWidth / 2 - 30, 0));
                }
            }
            catch
            {
                return new Point(0, 0);
            }
        }

        public Point GetOutputsLinkPoint(UIElement parent)
        {
            try
            {
                if (CurrentViewMode != OperationViewMode.Edit)
                {
                    var gt = OutputsPanel.TransformToVisual(parent);
                    return gt.Transform(new Point(OutputsPanel.ActualWidth / 2 + 20, OutputsPanel.ActualHeight));
                }
                else
                {
                    var gt = OperationPanel.TransformToVisual(parent);
                    return gt.Transform(new Point(OperationPanel.ActualWidth / 2 + 30, OperationPanel.ActualHeight));
                }
            }
            catch
            {
                return new Point(0, 0);
            }
        }

        public Point GetContextMenuCallPoint(UIElement parent)
        {
            try
            {
                var gt = NextOperationDivider.TransformToVisual(parent);
                return gt.Transform(new Point(NextOperationDivider.ActualWidth / 2 - 30, NextOperationDivider.ActualHeight / 2 - 20));
            }
            catch
            {
                return new Point(0, 0);
            }
        }

        public Point GetNextOperationPoint(UIElement parent, int outputNumber)
        {
            if (outputNumber > 0) return new Point(0, 0);
            try
            {
                var gt = NextOperationDivider.TransformToVisual(parent);
                return gt.Transform(new Point(NextOperationDivider.ActualWidth / 2 - 30, NextOperationDivider.ActualHeight));
            }
            catch
            {
                return new Point(0, 0);
            }
        }

        private void InputsPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GeometryChanged?.Invoke(this, new GenericEventArgs());
        }

        private void Border_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GeometryChanged?.Invoke(this, new GenericEventArgs());
        }

        private void OutputsPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GeometryChanged?.Invoke(this, new GenericEventArgs());
        }

        #endregion

        #region Отображение исполнения

        public void ShowProcessingState(TaskState taskState)
        {
            Dispatcher.BeginInvoke(() =>
            {
                switch (taskState)
                {
                    case TaskState.Error:
                        endOfExecution = DateTime.Now;
                        TimeTextBlock.Text = $"{Math.Round((endOfExecution - startOfExecution).TotalSeconds, 3)} сек";
                        OperationInfoPanel.Background = new SolidColorBrush(Color.FromArgb(0x30, 0xff, 0xc0, 0xc0));
                        break;
                    case TaskState.Processing:
                        startOfExecution = DateTime.Now;
                        TimeTextBlock.Text = string.Empty;
                        OperationInfoPanel.Background = new SolidColorBrush(Color.FromArgb(0x30, 0xff, 0xff, 0xc0)); break;
                    case TaskState.Preprocessing:
                        TimeTextBlock.Text = string.Empty;
                        OperationInfoPanel.Background = new SolidColorBrush(Color.FromArgb(0x8, 0x26, 0x8f, 0x97)); break;
                    case TaskState.Ready:
                        endOfExecution = DateTime.Now;
                        TimeTextBlock.Text = $"{Math.Round((endOfExecution - startOfExecution).TotalSeconds, 3)} сек";
                        OperationInfoPanel.Background = new SolidColorBrush(Color.FromArgb(0x30, 0xc0, 0xff, 0xc0)); break;
                    case TaskState.Inactive:
                        TimeTextBlock.Text = string.Empty;
                        OperationInfoPanel.Background = null;
                        break;
                }
            });
        }

        public void SetTotalTime(double seconds)
        {
            var parttime = (endOfExecution - startOfExecution).TotalSeconds;
            TimeTextBlock.Text = $"{Math.Round(parttime, 3)} сек, ({Math.Round(100d * parttime / seconds, 3)} %)\r\nвсего: {Math.Round(seconds, 3)} сек";
        }
        public void SetPartialTime(double seconds)
        {
            var parttime = (endOfExecution - startOfExecution).TotalSeconds;
            TimeTextBlock.Text = $"{Math.Round(parttime, 3)} сек, ({Math.Round(100d * parttime / seconds, 3)} %)";
        }

        #endregion
    }
}
