﻿using System;
using System.Windows.Threading;

namespace AQCalculationsLibrary
{
    public partial class DataDivider
    {
        void AnimationTimer_Tick(object sender, EventArgs e)
        {
            ((PositionAnimator)Resources["PositionAnimator"]).Offset -= 1;
        }

        public DataDivider()
        {
            InitializeComponent();
            var animationTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 50) };
            animationTimer.Tick += AnimationTimer_Tick;
            animationTimer.Start();
        }
    }
}
