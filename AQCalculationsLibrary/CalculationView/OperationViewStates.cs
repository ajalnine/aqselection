﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore;
using AQCalculationsLibrary;

namespace AQCalculationsLibrary
{
    public partial class CalculationView
    {
        private void OperationView_StateChanged(object sender, EventArgs e)
        {
            ResetProgressInScheme();
            var source = (OperationView)sender;
            switch (source.CurrentViewMode)
            {
                case OperationViewMode.Empty:
                    BlockSaving?.Invoke(this, new GenericEventArgs { CustomData = false });
                    EnableDelete(false);
                    CloseToolBox();
                    Canvas.SetZIndex(source, 0);
                    break;

                case OperationViewMode.Edit:
                    BlockSaving?.Invoke(this, new GenericEventArgs {CustomData = false});
                    EnableDelete(false);
                    CloseToolBox();
                    if (source.BoundModule.Wide && !DisableScrollControlling) Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.SetScroller, GetOperationViewOffset(source) - ScrollOffset, null);
                    Canvas.SetZIndex(source, 1);
                    break;

                case OperationViewMode.Display:
                    BlockSaving?.Invoke(this, new GenericEventArgs { CustomData = true });
                    EnableDelete(true);
                    UpdateLayout();
                    _currentInsertionPointView = source;
                    EnableModuleInsertion(true, source);
                    ShowToolBoxAfter(source);
                    Canvas.SetZIndex(source, 0);
                    break;
            }
            
            UpdateLinkInformation();
            source.UpdateLayout();
            UpdateOperationsGeometry();
            UpdateLinkView();
            _currentInsertionPointView = source;
        }

        private static double GetOperationViewOffset(OperationView source)
        {
            var gt = source.TransformToVisual((Grid)source.Parent);  
            return gt.Transform(new Point(0, 0)).Y;
        }
    }
}
