﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using ApplicationCore.CalculationServiceReference;

namespace AQCalculationsLibrary
{
    public partial class ExpressionBuilder
    {
        #region Свойства и поля
        private DataItem _currentTable = new DataItem();
        private List<DataItem> _allTables = new List<DataItem>();
        public delegate void EditFinishedDelegate(object o, EditFinishedEventArgs e);
        public event EditFinishedDelegate EditFinished;
        public delegate void ExpressionChangedDelegate(object o, EventArgs e);
        public event ExpressionChangedDelegate ExpressionChanged;
        ScrollViewer _popupScrollViewer;
        private string _currentField;
        List<Token> _resultingTokens;
        List<Token> _defaultPopupTokens;
        private bool _popupEnabled;
        private bool _popupAccepted = true;
        private bool _downToPopup;
        private int _returnPosition;
        public Point OldCaretPosition;
        readonly DispatcherTimer _popupTimer;
        #endregion

        public ExpressionBuilder()
        {
            InitializeComponent();
            _popupTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 200)};
            _popupTimer.Tick += PopupTimer_Tick;
            _currentTable.Fields = new List<DataField>();
        }
        
        #region Методы инициализации

        public void SetDataItems(DataItem currentTable, List<DataItem> allTables, string currentField)
        {
            _currentTable = currentTable;
            _allTables = allTables;
            _currentField = currentField;
            _defaultPopupTokens = (from c in Tokens.GetAllTokensList(_allTables, _currentTable, _currentField).OrderByDescending(n => n.TokenName.Length).ToList() where c.Type != TokenType.Operator && c.Type != TokenType.Bracket && c.Type != TokenType.CurrentFieldList && c.Type != TokenType.Field && c.Type!= TokenType.PreviousRowField && c.Type != TokenType.NextRowField orderby c.TokenName select c).ToList();
            _resultingTokens = new List<Token>();
            ExpressionEditor.Focus();
            UpdateEditor();
        }
        
        public void SetExpressionEditor(string text)
        {
            ExpressionEditor.Text = text;
            UpdateEditor();
        }

        public string GetExpression()
        {
            return ExpressionEditor.Text;
        }

        public string GetCode()
        {
            return Code.Text;
        }

        public void SetCodeEditor(string text)
        {
            Code.Text = text ;
        }

        public void StopHelp()
        {
            _popupEnabled = false;
            MainPopup.IsOpen = false;
        }

        public void StartHelp()
        {
            _popupEnabled = true;
        }
        #endregion

        #region Обработка токенов

        private List<Token> AutoReplaceTokens(ref int position, IEnumerable<Token> tokens)
        {
            _resultingTokens = new List<Token>();

            foreach (var t in tokens)
            {
                if (t.Type == TokenType.Operator && t.TokenName == "^")
                {
                    position--;
                    if (_resultingTokens.Count >= 1)
                    {
                        var toMove = new List<Token>();
                        var bracketCount = 0;
                        for (var i = _resultingTokens.Count - 1; i >= 0; i--)
                        {
                            var tp = _resultingTokens[i];
                            if (tp.Type == TokenType.Space || tp.Type == TokenType.Unknown)
                            {
                                toMove.Insert(0, tp);
                                continue;
                            }
                            if (tp.Type == TokenType.Bracket)
                            {
                                if (tp.TokenName == ")")
                                {
                                    bracketCount++;
                                    toMove.Insert(0, tp);
                                    continue;
                                }
                                if (tp.TokenName == "(")
                                {
                                    bracketCount--;
                                    toMove.Insert(0, tp);
                                    continue;
                                }
                            }
                            toMove.Insert(0, tp);
                            if (bracketCount == 0) break;
                        }

                        foreach (Token tm in toMove)
                        {
                            _resultingTokens.Remove(tm);
                            position -= tm.TokenName.Length;
                        }

                        _resultingTokens.Add(new Token { TokenName = "Pow", CodeReplace = "Math.Pow", MathArguments = 2, Type = TokenType.Math });
                        _resultingTokens.Add(new Token { TokenName = "(", CodeReplace = "(", Type = TokenType.Bracket });

                        foreach (Token tm in toMove)
                        {
                            _resultingTokens.Add(tm);
                            position += tm.TokenName.Length;
                        }
                        _resultingTokens.Add(new Token { TokenName = ",", CodeReplace = ",", Type = TokenType.Operator });
                        position += 5;
                    }
                    else
                    {
                        _resultingTokens.Add(new Token { TokenName = "Pow", CodeReplace = "Math.Pow", MathArguments = 2, Type = TokenType.Math });
                        _resultingTokens.Add(new Token { TokenName = "(", CodeReplace = "(", Type = TokenType.Bracket });
                        position += 4;
                    }
                }
                else
                {
                    if ((new[] { TokenType.Number, TokenType.Variable, TokenType.Text }).Contains(t.Type))
                    {
                        var nonEmptyTokens = (_resultingTokens.Where(rt => rt.Type != TokenType.Space)).ToList();
                        if (nonEmptyTokens.Any())
                        {
                            var lastToken = nonEmptyTokens.Last();
                            if (lastToken != null && (new[] { TokenType.Field, TokenType.Number, TokenType.Variable, TokenType.Text }).Contains(lastToken.Type))
                            {
                                if (t.Type != TokenType.Text && lastToken.Type != TokenType.Text)
                                {
                                    _resultingTokens.Add(new Token { TokenName = "*", CodeReplace = "*", Type = TokenType.Operator });
                                }
                                else
                                {
                                    _resultingTokens.Add(new Token { TokenName = "+", CodeReplace = "+", Type = TokenType.Operator });
                                }
                                position++;
                            }
                        }
                    }
                    _resultingTokens.Add(t);
                }
            }
            return _resultingTokens;
        }

        private void UpdateEditor()
        {
            var parsedTokens = Tokens.ParseTokens(ExpressionEditor.Text, _allTables, _currentTable, _currentField);
            var position = ExpressionEditor.SelectionStart;
            _resultingTokens = AutoReplaceTokens(ref position, parsedTokens);
            Tokens.RenderTextBlockWithTokens(ExpressionPresenter, _resultingTokens);
            var c = Tokens.GetCSharpCodeForTokens(_resultingTokens);
            CheckButton.IsEnabled = ((from r in _resultingTokens where r.Type != TokenType.Space select r).Any() 
                && !(from r in _resultingTokens where r.Type == TokenType.Unknown select r).Any());
            if (Code != null && Code.Text != c) Code.Text = c;

            ExpressionEditor.Text = ExpressionPresenter.Text;
            ExpressionEditor.SelectionStart = position;
        }

        #endregion;

        #region Позиционирование подсказки

        public double EstimateCaretXOffset(double yOffset)
        {
            if (ExpressionEditor.Text == string.Empty || ExpressionEditor.SelectionStart == 0) return 1;
            var oldHeight = yOffset;
            var textBeforeCaret = ExpressionEditor.Text.Substring(0, ExpressionEditor.SelectionStart);
            textBeforeCaret = textBeforeCaret.Split('\n').LastOrDefault();
            if (textBeforeCaret != null)
            {
                var index = textBeforeCaret.Length;
                var width = index;
                double newHeight;
                do
                {
                    width /= 2;
                    index -= width;
                    SizeTester.Text = textBeforeCaret.Substring(0, index);
                    SizeTester.UpdateLayout();
                    newHeight = SizeTester.RenderSize.Height;
                    if (Math.Abs(oldHeight - newHeight) > 1e-8)
                    {
                        index += width;
                    }
                }
                while (width > 1);
                do
                {
                    index--;
                    SizeTester.Text = textBeforeCaret.Substring(0, index);
                    SizeTester.UpdateLayout();
                    newHeight = SizeTester.RenderSize.Height;
                }
                while (Math.Abs(newHeight - oldHeight) < 1e-8 && index != 0);

                var textOfLastLine = textBeforeCaret.Substring(index);
                SizeTester.Text = textOfLastLine;
            }
            SizeTester.UpdateLayout();
            return SizeTester.RenderSize.Width;
        }

        public double EstimateCaretYOffset()
        {
            if (ExpressionEditor.Text == string.Empty || ExpressionEditor.SelectionStart == 0) return 19;
            var textBeforeCaret = ExpressionEditor.Text.Substring(0, ExpressionEditor.SelectionStart);
            SizeTester.Text = textBeforeCaret;
            SizeTester.UpdateLayout();
            return SizeTester.RenderSize.Height;
        }

        private void UpdateScroll(Point offset)
        {
            var yOffset = offset.Y;
            var windowHeight = MainScrollViewer.ViewportHeight;
            var windowOffset = MainScrollViewer.VerticalOffset;

            var bottomOutScreen = windowHeight + windowOffset - yOffset;
            if (bottomOutScreen < 0) MainScrollViewer.ScrollToVerticalOffset(yOffset - windowHeight);
            else
            {
                if (yOffset - 19 <= windowOffset) MainScrollViewer.ScrollToVerticalOffset(yOffset - 19);
            }
        }

        void PopupTimer_Tick(object sender, EventArgs e)
        {
            if (!_popupEnabled) return;
            var x = EstimateCaretXOffset(OldCaretPosition.Y);
            MainPopup.HorizontalOffset = x;
            OldCaretPosition.X = x;
            MainPopup.IsOpen = true;
            _popupTimer.Stop();
        }
        #endregion

        #region Подсказка

        private void UpdatePopupData(List<Token> list, bool allowAutoInsert)
        {
            if (!_popupAccepted)
            {
                CurrentTokenHelper.Visibility = Visibility.Collapsed;
                CurrentTokenView.Visibility = Visibility.Collapsed;
                return;
            }

            var tokenInWhichCursor = TokensAroundCursorInfo.GetTokenInWhichCursor(_resultingTokens, ExpressionEditor.SelectionStart);
            
            if (tokenInWhichCursor != null && tokenInWhichCursor.Type != TokenType.Unknown)
            {
                CurrentTokenHelper.Visibility = Visibility.Collapsed;
                SetupTokenHelp(tokenInWhichCursor);
                CurrentTokenView.Visibility = Visibility.Visible;
                return;
            }

            var wordTokenBeforeCursor = TokensAroundCursorInfo.GetWordTokenBeforeCursor(_resultingTokens, ExpressionEditor.SelectionStart);

            if (tokenInWhichCursor != null && tokenInWhichCursor.Type == TokenType.Unknown || wordTokenBeforeCursor != null)
            {
                var activeToken = tokenInWhichCursor ?? wordTokenBeforeCursor;
                if (wordTokenBeforeCursor != null && wordTokenBeforeCursor.Type == TokenType.Table)
                {
                    var tableName = Regex.Replace(Regex.Replace(wordTokenBeforeCursor.TokenName, @"^\[", string.Empty), @"\]\.$", string.Empty);
                    var fields = _allTables.First(n => n.Name == tableName).Fields;
                    var fieldList = (from f in fields select new Token { TokenName = f.Name, Inject = "[" + f.Name + "]", Type = TokenType.Field }).ToList();
                    fieldList.AddRange(new []{new Token
                    {
                        CodeReplace = "Cell", 
                        Inject = "Cell", 
                        TokenName = "Cell",
                        Type = TokenType.Math
                    }, new Token
                    {
                        CodeReplace = "CellRange",
                        Inject = "CellRange",
                        TokenName = "CellRange",
                        Type = TokenType.Aggregate
                    }});
                    CurrentTokenHelper.Visibility = Visibility.Visible;
                    bool unique;

                    var index = FindClosestTokenInList(wordTokenBeforeCursor.TokenName, list, out unique);
                    if ( !allowAutoInsert)
                    {
                        UpdateTokenHelper(fieldList, 0);
                    }
                    else
                    {
                        InjectToken(fieldList.ToList()[index], activeToken);
                    }
                }
                else
                {
                    CurrentTokenHelper.Visibility = Visibility.Visible;
                    bool unique;

                    var index = FindClosestTokenInList(activeToken.TokenName, list, out unique);
                    if (!allowAutoInsert)
                    {
                        UpdateTokenHelper(list, index);
                    }
                    else
                    {
                        InjectToken(list.ToList()[index], activeToken);
                    }
                }
            }
            else
            {
                CurrentTokenHelper.Visibility = Visibility.Visible;
                UpdateTokenHelper(list, -1);
            }
        }

        private void SetupTokenHelp(Token t)
        {
            CurrentToken.Text = t.TokenName;
            CurrentToken.Foreground = Tokens.GetTokenColor(t.Type);
            switch (t.Type)
            {
                case TokenType.Variable:
                    CurrentTokenHelp.Text = " - Переменная";
                    break;
                case TokenType.Number:
                    CurrentTokenHelp.Text = " - Числовая константа";
                    break;
                case TokenType.Text:
                    CurrentTokenHelp.Text = " - Текст";
                    break;
                case TokenType.Field:
                case TokenType.CurrentFieldList:
                    CurrentTokenHelp.Text = " - Поле таблицы (для статистических функций).";
                    break;
                case TokenType.CurrentField:
                    CurrentTokenHelp.Text = " - Значение поля текущей таблицы.";
                    break;
                case TokenType.Table:
                    CurrentTokenHelp.Text = " - Таблица.";
                    break;
                default:
                    CurrentTokenHelp.Text = t.Help;
                    break;
            }
            CurrentTokenView.Visibility = Visibility.Visible;
        }

        private void UpdateTokenHelper(IEnumerable<Token> list, int index)
        {
            TokenHelper.ItemsSource = list;
            TokenHelper.UpdateLayout();
            TokenHelper.SelectedIndex = index;
            if (TokenHelper.SelectedIndex == -1) TokenHelper.SelectedIndex = 0;
            TokenHelper.UpdateLayout();
            UpdateTokenHelperScroll();
        }

        private static int FindClosestTokenInList(string begin, List<Token> list, out bool unique)
        {
            if (begin != string.Empty)
            {
                for (var i = begin.Length; i > 0; i--)
                {
                    var toFind = begin.Substring(0, i).ToLower();

                    var found = (from n in list where n.TokenName.ToLower().StartsWith(toFind) select n).ToList();
                    switch (found.Count())
                    {
                        case 0:
                            break;
                        case 1:
                            unique = true;
                            return list.ToList().IndexOf(found.First());
                        default:
                            unique = false;
                            return list.ToList().IndexOf(found.First());
                    }
                }
            }
            unique = false;
            return -1;
        }

        private void UpdateTokenHelperScroll()
        {
            if (_popupScrollViewer == null) return;
            var offset = 12*(double)TokenHelper.SelectedIndex;
            _popupScrollViewer.ScrollToVerticalOffset(offset);
        }

        private void InjectToken(Token t, Token insteadOf)
        {
            var text = ExpressionEditor.Text;
            var toInject = t.Inject ?? t.TokenName;
            int index;
            if (insteadOf != null)
            {
                index = TokensAroundCursorInfo.GetStartPositionOfToken(_resultingTokens, insteadOf);
                text = text.Remove(index, insteadOf.TokenName.Length);
            }
            else
            {
                index = _returnPosition;
            }
            text = text.Insert(index, toInject);
            ExpressionEditor.Text = text;
            CurrentTokenHelper.Visibility = Visibility.Collapsed;
            CurrentTokenView.Visibility = Visibility.Collapsed;
            ExpressionEditor.SelectionStart = index + toInject.Length;
            ExpressionEditor.Focus();
        }

        #endregion
        
        #region Обработка событий интерфейса

        private void CheckExpressionButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentTokenHelper.Visibility = Visibility.Collapsed;
            CurrentTokenView.Visibility = Visibility.Collapsed;
            EditFinished?.Invoke(this, new EditFinishedEventArgs { CSharpText = Code.Text, ExpressionText = ExpressionPresenter.Text });
        }

        private void ExpressionEditor_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateEditor();
            if (ExpressionChanged != null) ExpressionChanged.Invoke(this, new EventArgs());
        }
        
        private void ExpressionEditor_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ExpressionPresenter.Width = e.NewSize.Width;
            ExpressionPresenter.Height = e.NewSize.Height;
        }
        
        private void ExpressionEditor_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Escape:
                    _popupAccepted = false;
                    UpdatePopupData(_defaultPopupTokens, false);
                    break;

                case Key.Down:
                    if (CurrentTokenHelper.Visibility == Visibility.Visible)
                    {
                        _downToPopup = true;
                        e.Handled = true; 
                        Dispatcher.BeginInvoke(() =>
                        {
                            TokenHelper.Focus();
                            TokenHelper.UpdateLayout();
                            var lbi =
                                ((ListBoxItem) TokenHelper.GetUIElementByItemIndex(TokenHelper.SelectedIndex));
                            lbi.Focus();
                            lbi.UpdateLayout();
                            TokenHelper.UpdateLayout();
                        });
                    }
                    break;

                case Key.Space:
                    if (Keyboard.Modifiers == ModifierKeys.Control)
                    {
                        _popupAccepted = true;
                        UpdatePopupData(_defaultPopupTokens, true);
                        CurrentTokenHelper.Visibility = Visibility.Visible;
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            _popupScrollViewer = sender as ScrollViewer;
            UpdateTokenHelperScroll();
        }

        private void TokenHelper_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TokenHelper.SelectedItem != null)
            {
                CurrentTokenHelper.Visibility = Visibility.Visible;
                SetupTokenHelp(TokenHelper.SelectedItem as Token);
            }                   
        }

        private void TokenHelper_KeyDown(object sender, KeyEventArgs e)
        {
            var tokenInWhichCursor = TokensAroundCursorInfo.GetTokenInWhichCursor(_resultingTokens, ExpressionEditor.SelectionStart);
            var wordTokenBeforeCursor = TokensAroundCursorInfo.GetTokenInWhichCursor(_resultingTokens, ExpressionEditor.SelectionStart);
            var activeToken = tokenInWhichCursor ?? wordTokenBeforeCursor;
            switch (e.Key)
            {
                case Key.Escape:
                    CurrentTokenHelper.Visibility = Visibility.Collapsed;
                    CurrentTokenView.Visibility = Visibility.Collapsed;
                    _popupAccepted = false;
                    ExpressionEditor.SelectionStart = _returnPosition; 
                    ExpressionEditor.Focus();
                    break;
                case Key.Enter:
                case Key.Tab:
                    e.Handled = true; 
                    InjectToken(TokenHelper.SelectedItem as Token, activeToken);
                    break;
                case Key.Space:
                    if (Keyboard.Modifiers == ModifierKeys.Control)
                    {
                        e.Handled = true;
                        InjectToken(TokenHelper.SelectedItem as Token, activeToken);
                    
                    }
                    break;
            }
        }

        private void TokenHelper_GotFocus(object sender, RoutedEventArgs e)
        {
            _downToPopup = false;
            TokenHelper.UpdateLayout();
            var lbi = ((ListBoxItem)TokenHelper.GetUIElementByItemIndex(TokenHelper.SelectedIndex));
            if (lbi!=null)lbi.Focus();
        }
        
        private void ExpressionEditor_SelectionChanged(object sender, RoutedEventArgs e)
        {
            RefreshPopupPosition(0);
        }

        private void RefreshPopupPosition(double verticalOffset)
        {
            _returnPosition = ExpressionEditor.SelectionStart;
            if (_downToPopup) return;
            var y = EstimateCaretYOffset() + verticalOffset;
            if (Math.Abs(OldCaretPosition.Y - y) > 1e-8)
            {
                OldCaretPosition.Y = y;
                UpdateScroll(OldCaretPosition);
            }
            else
            {
                OldCaretPosition.Y = y;
            }

            MainPopup.VerticalOffset = y;
            MainPopup.IsOpen = false;
            UpdatePopupData(_defaultPopupTokens, false);
            _popupTimer.Start();
        }

        private void Code_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ExpressionChanged != null) ExpressionChanged.Invoke(this, new EventArgs());
        }

        private void TokenHelperItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            if (grid == null) return;
            var t = grid.Tag as Token;
            var index = TokenHelper.Items.IndexOf(t);
            var listBoxItem = TokenHelper.GetUIElementByItemIndex(index) as ListBoxItem;
            if (listBoxItem == null || !listBoxItem.IsSelected) return;
            var tokenAtCursor = TokensAroundCursorInfo.GetTokenInWhichCursor(_resultingTokens,
                ExpressionEditor.SelectionStart);
            var tokenBeforeCursor = TokensAroundCursorInfo.GetTokenFullyBeforeCursor(_resultingTokens,
                ExpressionEditor.SelectionStart);
            if (tokenAtCursor != null) InjectToken(t, tokenAtCursor);
            else if (tokenBeforeCursor !=null && t != null && t.TokenName.ToLower().StartsWith(tokenBeforeCursor.TokenName.ToLower())) InjectToken(t, tokenBeforeCursor);
            else InjectToken(t, null);
        }

        #endregion
    }

    #region Вспомогательные классы

    public class TokenToImageConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            const string prefix = @"/AQResources;component/Images/Expressions/";
            var uri = (new Uri(prefix + ((TokenType)value)+".png", UriKind.Relative));
            return new BitmapImage {UriSource = uri};
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    
    public class EditFinishedEventArgs
    {
        public string ExpressionText;
        public string CSharpText;
        public List<DataItem> UsedTables;
    }

    public class UsedField
    {
        public DataItem Table;
        public DataField Field;
    }
    #endregion
}