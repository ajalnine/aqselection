﻿using System;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;

namespace AQCalculationsLibrary
{
    public delegate void DataRequestDelegate(object o, EventArgs e);
    public delegate void NameChangedDelegate(object o, NameChangedEventArgs e);
    public delegate void ParametersAcceptedDelegate(object o, ParametersAcceptedEventArgs e);

    public interface IDataFlowUI
    {
        event ParametersAcceptedDelegate ParametersAccepted;
        event DataRequestDelegate DataRequest;
        event NameChangedDelegate NameChanged;

        void SetupUI(List<DataItem> availableData);
        void SetupUI(List<DataItem> availableData, string parametersXml);
        void ParametersToUI(List<DataItem> availableData);
        
        void DataRequestReadyCallback(string fileName);
        string GetParametersXML();
    }

    public class NameChangedEventArgs
    {
        public string NewName { get; set; }
    }

    public class ParametersAcceptedEventArgs
    {
        public string ParametersXML { get; set; }
        public bool CloseUI { get; set; }
    }
    
    public delegate void DataFlowFinishedDelegate(object o, DataFlowFinishedEventArgs e);
    public delegate void DataFlowErrorDelegate(object o, DataFlowErrorEventArgs e);
    
    public interface IDataFlow
    {
        event DataFlowFinishedDelegate DataFlowFinished;
        event DataFlowErrorDelegate DataFlowError;
        void DataProcess(List<DataItem> availableData, string parametersXml);
    }

    public class DataFlowFinishedEventArgs
    {
        public List<DataItem> Inputs { get; set; }
        public List<DataItem> Outputs { get; set; }
        public List<string> Deleted { get; set; }
        public List<Step> GeneratedSteps { get; set; }
    }
    
    public class DataFlowErrorEventArgs
    {
        public InconsistenceState Error { get; set; }
    }

    public enum OperationViewMode { Edit, Display, Empty }
    public enum InconsistenceState { Norm, LostInput, LostInputFields }
}
