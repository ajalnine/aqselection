﻿using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;

namespace AQCalculationsLibrary
{
    public class CalculationDescription
    {
        public int ID;
        public string Name;
        public string Parameters;
        public int TargetID = -1;
        public string TargetName = string.Empty;
        public bool CanBeDeleted = true;
        public List<Step> StepData;
        public List<DataItem> Results;

        public CalculationDescription(int id, string name, string parameters, int targetid, bool canbedeleted, string targetname, List<Step> stepdata, List<DataItem> results)
        {
            ID = id;
            Name = name;
            Parameters = parameters;
            TargetID = targetid;
            TargetName = targetname;
            CanBeDeleted = canbedeleted;
            StepData = stepdata;
            Results = results;
        }

        public CalculationDescription(int id, string name, string parameters)
        {
            ID = id;
            Name = name;
            Parameters = parameters;
        }

        public CalculationDescription()
        {
            StepData = new List<Step>();
            Results = new List<DataItem>();
        }
    }
}
