﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ApplicationCore.CalculationServiceReference;

namespace AQCalculationsLibrary
{
    public class ClipboardDataHelper
    {
        public static SLDataTable GetSLDataTableFromClipboard(string textData, string tableName)
        {
            if (!string.IsNullOrEmpty(textData))
            {
                var insertedRows = textData.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                var height = insertedRows.Count;
                if (height <= 1)
                {
                    MessageBox.Show("Буфер обмена не содержит табличных данных.", "Ошибка вставки данных",
                    MessageBoxButton.OK);
                    return null;
                }
                var headers = insertedRows[0].Split(new[] { "\t" }, StringSplitOptions.None).ToList();

                if (headers.Any(String.IsNullOrWhiteSpace))
                {
                    MessageBox.Show("Таблица не должна содержать пустые заголовки", "Ошибка вставки данных",
                        MessageBoxButton.OK);
                    return null;
                }
                if (headers.Count != headers.Distinct().Count())
                {
                    MessageBox.Show("Таблица не должна содержать повторяющиеся заголовки", "Ошибка вставки данных",
                        MessageBoxButton.OK);
                    return null;
                }

                var data = new List<Row>();
                var dataTypes = new List<string>();

                foreach (var row in insertedRows.Skip(1))
                {
                    var r = new Row { RowData = new List<string>() };
                    r.RowData.AddRange(row.Split(new[] { "\t" }, StringSplitOptions.None));
                    data.Add(r);
                }

                for (int i = 0; i < headers.Count; i++)
                {
                    dataTypes.Add(GetDataTypeForColumn(data, i));
                }
                var sldt = ConvertStringsToSLDT(data, headers, dataTypes, tableName);
                return sldt;
            }
            MessageBox.Show("Буфер обмена не содержит данных.", "Ошибка вставки данных", MessageBoxButton.OK);
            return null;
        }

        private static SLDataTable ConvertStringsToSLDT(IEnumerable<Row> data, List<string> headers, List<string> dataTypes, string tableName)
        {
            if (data == null) throw new ArgumentNullException("data");
            var destination = new SLDataTable
            {
                ColumnNames = headers,
                DataTypes = dataTypes,
                TableName = tableName,
                Tag = null,
                SelectionString = String.Empty,
                Table = new List<SLDataRow>()
            };
            var divider = NumberFormatInfo.CurrentInfo.NumberDecimalSeparator.ToCharArray()[0];
            foreach (var t in data)
            {
                var sldr = new SLDataRow { Row = new List<object>() };
                destination.Table.Add(sldr);

                for (var i = 0; i < headers.Count; i++)
                {
                    var value = t.RowData[i].Trim();
                    if (string.IsNullOrEmpty(value))
                    {                         
                        sldr.Row.Add(DBNull.Value);
                        continue;
                    }
                    switch (dataTypes[i])
                    {
                        case "Double":
                            sldr.Row.Add(double.Parse(value.Replace(',', divider).Replace('.', divider)));
                            break;
                        case "DateTime":
                            DateTime dateTimeResult;
                            var isDateTime = DateTime.TryParseExact(value, "MMMM yyyy", CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                            if (isDateTime)
                            {
                                sldr.Row.Add(dateTimeResult);
                                break;
                            }
                            isDateTime = DateTime.TryParseExact(value, "MMM.yy", CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                            if (isDateTime)
                            {
                                sldr.Row.Add(dateTimeResult);
                                break;
                            }
                            isDateTime = DateTime.TryParse(value, CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTimeResult);
                            if (isDateTime)
                            {
                                sldr.Row.Add(dateTimeResult);
                            }

                            break;
                        case "Boolean":
                            sldr.Row.Add(bool.Parse(value));
                            break;
                        case "Int32":
                            sldr.Row.Add(int.Parse(value));
                            break;
                        default:
                            if (value.StartsWith("\"") && value.EndsWith("\""))value = value.Substring(1, value.Length - 2);
                            sldr.Row.Add(value);
                            break;
                    }
                }
            }
            return destination;
        }

        private static string GetDataTypeForColumn(IList<Row> data, int j)
        {
            if (data == null) throw new ArgumentNullException("data");
            var recordsNumber = data.Count;
            var isDouble = true;
            var isBoolean = true;
            var isDateTime = true;
            var isTimeSpan = true;
            var hasValues = false;
            double doubleResult;
            string value;
            DateTime dateTimeResult;
            TimeSpan timeSpanResult;
            bool booleanResult;
            var div = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0];
            var baddiv = div == ',' ? '.' : ',';
            for (var i = 1; i < recordsNumber; i++)
            {
                value = data[i].RowData[j];
                if (string.IsNullOrEmpty(value)) continue;
                else hasValues = true;

                if (isDouble)
                {
                    isDouble = double.TryParse(value.Replace(baddiv, div), out doubleResult);
                }
                if (isDateTime)
                {
                    isDateTime = DateTime.TryParseExact(value, "MMMM yyyy", CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    if (!isDateTime) isDateTime = DateTime.TryParseExact(value, "MMM.yy", CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    if (!isDateTime) isDateTime = DateTime.TryParse(value, CultureInfo.CurrentCulture, DateTimeStyles.None,out dateTimeResult);
                }
                if (isTimeSpan)
                {
                    isTimeSpan = TimeSpan.TryParse(value, out timeSpanResult);
                }
                if (isBoolean)
                {
                    isBoolean = bool.TryParse(value, out booleanResult);
                }
            }
            if (!hasValues) return "String";
            if (isDouble) return "Double";
            if (isBoolean) return "Boolean";
            if (isTimeSpan) return "TimeSpan";
            if (isDateTime) return "DateTime";
            return "String";
        }

        public class Row
        {
            public List<string> RowData;
        }

    }
}
