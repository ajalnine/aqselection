﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;

namespace AQCalculationsLibrary
{
    public static class DataItemHelper
    {
        public static bool IsSameSources(DataItem one, DataItem two)
        {
            if (one == null && two == null) return true;
            if (one == null || two == null) return false;
            return one.Name == two.Name && one.DataItemType == two.DataItemType;
        }

        public static DataItemCompareResult FirstHasAllFieldsOfTwo(DataItem one, DataItem two)
        {
            var isEqual = two.Fields.All(f2 => (from f1 in one.Fields where f1.DataType == f2.DataType && f1.Name == f2.Name select f1).Any());
            return (isEqual) ? DataItemCompareResult.Equal : DataItemCompareResult.LostFields;
        }

        public static DataItemCompareResult IsEqual(DataItem one, DataItem two)
        {
            if (one == null && two == null) return DataItemCompareResult.Equal;
            if (one == null || two == null) return DataItemCompareResult.NotEqual;

            if (one.DataItemType != two.DataItemType || one.Name != two.Name || one.TableName != two.TableName) return DataItemCompareResult.NotEqual;
            if (one.Fields.Count != two.Fields.Count) return DataItemCompareResult.NotEqual;
            return FirstHasAllFieldsOfTwo(one, two);
        }

        public static DataItemCompareResult IsInputExistsInSources(List<DataItem> sources, DataItem input)
        {
            foreach (var di in sources)
            {
                if (IsSameSources(di, input))
                {
                    return FirstHasAllFieldsOfTwo(di, input);
                }
            }
            return DataItemCompareResult.NotEqual;
        }

        public static DataItem GetCopy(DataItem one)
        {
            var result = new DataItem
            {
                DataItemType = one.DataItemType,
                Name = one.Name,
                TableName = one.TableName,
                Fields = new List<DataField>()
            };
            foreach (var f in one.Fields)
            {
                result.Fields.Add(new DataField { DataType = f.DataType, Name = f.Name });
            }
            return result;
        }

        public static void AddField(DataItem one, string name, string datatype)
        {
            one.Fields.Add(new DataField { DataType = datatype, Name = name });
        }
    }

    public enum DataItemCompareResult { Equal, LostFields, NotEqual }
    public enum CalculationConsistencyFlags { HasEmptySteps = 1, HasLostSources = 2, HasLostSourceFields = 4, NoSteps = 8 }
}
