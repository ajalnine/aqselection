﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public static class Tokens
    {
        public static Token[] Collection = 
         { 
             new Token { TokenName = "Abs", CodeReplace="AQMath.Abs", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Модуль аргумента. " }, 
             new Token { TokenName = "Acos", CodeReplace="AQMath.Acos", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Арккосинус аргумента. " },
             new Token { TokenName = "Asin", CodeReplace="AQMath.Asin", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Арксинус аргумента. " },
             new Token { TokenName = "Atan", CodeReplace="AQMath.Atan", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Арктангенс аргумента. " },
             new Token { TokenName = "Ceiling", CodeReplace="AQMath.Ceiling", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Округление аргумента в большую сторону. " },
             new Token { TokenName = "Cos", CodeReplace="AQMath.Cos", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Косинус аргумента. " },
             new Token { TokenName = "ConstantE", CodeReplace="Math.E", Type = TokenType.Math, MathArguments=0, Help=" - Константа e=2.718281828... "},
             new Token { TokenName = "Exp", CodeReplace="AQMath.Exp", Type = TokenType.Math, MathArguments=1, Help="(Степень)\r\n e в степени. " },
             new Token { TokenName = "Floor", CodeReplace="AQMath.Floor", Type = TokenType.Math, MathArguments=1, Help="(Аргумент)\r\n - Округление аргумента в меньшую сторону."},
             new Token { TokenName = "IEEEReminder", CodeReplace="AQMath.IEEEReminder", Type = TokenType.Math, MathArguments=2,  Help="(Делимое, Делитель)\r\n - Остаток от деления."},
             new Token { TokenName = "Log10", CodeReplace="AQMath.Log10", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n - Логарифм аргумента с основанием 10."  },
             new Token { TokenName = "Log", CodeReplace="AQMath.Log", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Основание)\r\n - Логарифм." },
             new Token { TokenName = "ConstantPI", CodeReplace="Math.PI", Type = TokenType.Math, MathArguments=0,  Help=" - Константа Пи = 3.1415926..."  },
             new Token { TokenName = "Pow", CodeReplace="AQMath.Pow", Type = TokenType.Math, MathArguments=2 ,  Help="(Аргумент, степень)\r\n - Возведение аргумента в степень." },
             new Token { TokenName = "Round", CodeReplace="AQMath.Round", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент, число знаков)\r\n - Округление аргумента до требуемого числа знаков."},
             new Token { TokenName = "Sign", CodeReplace="AQMath.Sign", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n возвращает -1 если число отрицательное, 1 если положительное, 0 если аргумент равен нулю." },
             new Token { TokenName = "Sin", CodeReplace="AQMath.Sin", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Синус аргумента."  },
             new Token { TokenName = "Sqrt", CodeReplace="AQMath.Sqrt", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Квадратный корень из аргумента." },
             new Token { TokenName = "Tan", CodeReplace="AQMath.Tan", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Тангенс аргумента." },
             new Token { TokenName = "Min", CodeReplace="AQMath.AggregateMin", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Минимальное значение поля заданной таблицы."  },
             new Token { TokenName = "Max", CodeReplace="AQMath.AggregateMax", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Максимальное значение поля заданной таблицы."   },
             new Token { TokenName = "MinDate", CodeReplace="AQMath.AggregateMinDate", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Минимальное значение поля заданной таблицы."  },
             new Token { TokenName = "MaxDate", CodeReplace="AQMath.AggregateMaxDate", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Максимальное значение поля заданной таблицы."   },
             new Token { TokenName = "Sum", CodeReplace="AQMath.AggregateSum", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Сумма значений поля заданной таблицы."   },
             new Token { TokenName = "Mean", CodeReplace="AQMath.AggregateMean", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Среднее арифметическое значений поля заданной таблицы."   },
             new Token { TokenName = "Median", CodeReplace="AQMath.AggregateMedian", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Медиана поля заданной таблицы."   },
             new Token { TokenName = "Percentile", CodeReplace="AQMath.AggregatePercentile", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля], Аргумент)\r\n - Перцентиль поля заданной таблицы."   },
             new Token { TokenName = "PercentileInc", CodeReplace="AQMath.AggregatePercentileInc", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля], Аргумент)\r\n - Перцентиль поля заданной таблицы с использованием интерполяции между соседними значениями."   },
             new Token { TokenName = "PercentInRange", CodeReplace="AQMath.PercentInRange", Type = TokenType.Aggregate, MathArguments=3,  Help="([Имя таблицы].[Имя поля], Начало диапазона, Конец диапазона)\r\n - Процент попадания значений поля в указанный диапазон (включая границы)."   },
             new Token { TokenName = "CheckInRange", CodeReplace="AQMath.CheckInRange", Type = TokenType.Aggregate, MathArguments=3,  Help="(Значение, Начало диапазона, Конец диапазона)\r\n - При попадании значений поля в указанный диапазон (включая границы) возвращает значение, иначе - null."   },

             new Token { TokenName = "StDev", CodeReplace="AQMath.AggregateStDev", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Выборочное стандартное отклонение поля заданной таблицы."   },
             new Token { TokenName = "StDevRangeEstimation", CodeReplace="AQMath.AggregateStDevRangeEstimation", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Оценка отклонения по разнице смежных значений."   },
             new Token { TokenName = "Variance", CodeReplace="AQMath.AggregateVariance", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Дисперсия поля заданной таблицы."   },
             new Token { TokenName = "Moment2", CodeReplace="AQMath.Moment2", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - 2й центральный момент поля заданной таблицы."   },
             new Token { TokenName = "Moment3", CodeReplace="AQMath.Moment3", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - 3й центральный момент поля заданной таблицы."   },
             new Token { TokenName = "Moment4", CodeReplace="AQMath.Moment4", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - 4й центральный момент поля заданной таблицы."   },
             new Token { TokenName = "Kurtosis", CodeReplace="AQMath.Kurtosis", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Эксцесс поля заданной таблицы."   },
             new Token { TokenName = "Skewness", CodeReplace="AQMath.Skewness", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Асимметрия поля заданной таблицы."   },
             new Token { TokenName = "KurtosisStdErr", CodeReplace="AQMath.KurtosisStdErr", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Ошибка эксцесса поля заданной таблицы."   },
             new Token { TokenName = "SkewnessStdErr", CodeReplace="AQMath.SkewnessStdErr", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Ошибка асимметрии поля заданной таблицы."   },
             new Token { TokenName = "UTestP", CodeReplace="AQMath.UTestP", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], [Имя таблицы].[Имя поля])\r\n - U-тест Манна-Уитни."   },
             new Token { TokenName = "TTestWelchP", CodeReplace="AQMath.TTestWelchP", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], [Имя таблицы].[Имя поля])\r\n - T-тест независимых выборок с разной дисперсией и средними (Welch's t-test)."   },
             new Token { TokenName = "TTestP", CodeReplace="AQMath.TTestP", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], [Имя таблицы].[Имя поля])\r\n - T-тест независимых выборок с одинаковой дисперсией."   },

             new Token { TokenName = "ShapiroWilkW", CodeReplace="AQMath.ShapiroWilkW", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Значение критерия W Шапиро-Уилка."   },
             new Token { TokenName = "ShapiroWilkP", CodeReplace="AQMath.ShapiroWilkP", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Уровень значимости критерия W Шапиро-Уилка."   },
             new Token { TokenName = "AndersonDarlingA", CodeReplace="AQMath.AndersonDarlingA", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Значение критерия A* Андерсона-Дарлинга."   },
             new Token { TokenName = "AndersonDarlingP", CodeReplace="AQMath.AndersonDarlingP", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Уровень значимости критерия А* Андерсона-Дарлинга."   },
             new Token { TokenName = "KolmogorovD", CodeReplace="AQMath.KolmogorovD", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Значение критерия D Колмогорова."   },
             new Token { TokenName = "KolmogorovP", CodeReplace="AQMath.KolmogorovP", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Уровень значимости критерия D Колмогорова."   },
             new Token { TokenName = "LillieforsTest", CodeReplace="AQMath.LillieforsTest", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Проверка гипотезы о нормальности по критерию Лиллиефорса."   },
             new Token { TokenName = "Count", CodeReplace="AQMath.AggregateCount", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Число непустых значений полей из заданной таблицы."   },
             new Token { TokenName = "DistinctCount", CodeReplace="AQMath.AggregateDistinctCount", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Число уникальных значений полей из заданной таблицы, кроме пустых."   },
             new Token { TokenName = "CountOfValues", CodeReplace="AQMath.AggregateCountOfValues", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], значение)\r\n - Число заданных значений полей из заданной таблицы."   },
             new Token { TokenName = "ContinuousCountOfValues", CodeReplace="AQMath.AggregateContinuousCountOfValues", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], значение)\r\n - Длина максимально продолжительной последовательности заданных значений полей из заданной таблицы."   },
             new Token { TokenName = "MinIntervalBetween", CodeReplace="AQMath.AggregateMinIntervalBetween", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], значение)\r\n - Длина максимально продолжительной последовательности заданных значений полей из заданной таблицы."   },
             new Token { TokenName = "CountToPrevious", CodeReplace="AQMath.CountToPrevious", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], значение, Row)\r\n - Число записей до предыдущего повтора значения." },
             new Token { TokenName = "CountOfValuesInPercent", CodeReplace="AQMath.AggregateCountOfValuesInPercent", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], значение)\r\n - Процент заданных значений полей из заданной таблицы."   },
             new Token { TokenName = "ValidCases", CodeReplace="AQMath.AggregateValidCases", Type = TokenType.Aggregate,  Help="([Имя таблицы].[Имя поля], [Имя таблицы].[Имя поля] ...)\r\n - Минимальное число записей между повторами значения."   },
             new Token { TokenName = "FloorToList", CodeReplace="AQMath.FloorToList", Type = TokenType.Aggregate,  Help="(a, List())\r\n - Выбор из списка ближайшего наименьшего значения."},
             new Token { TokenName = "CheckList", CodeReplace="AQMath.CheckList", Type = TokenType.Aggregate,  Help="(value, a, b, ...)\r\n - Возвращает значение value, если оно в списке, либо null."   },
             new Token { TokenName = "ReplaceList", CodeReplace="AQMath.ReplaceList", Type = TokenType.Aggregate,  Help="(value, new value, a, b, ...)\r\n - Возвращает значение new value, если value есть в списке, иначе value."   },
             new Token { TokenName = "FromFirst", CodeReplace="AQMath.AggregateFromFirst", Type = TokenType.Aggregate,  Help="(Список, индекс)\r\n - Возвращает значение из списка с указанным индексом."   },
             new Token { TokenName = "FromLast", CodeReplace="AQMath.AggregateFromLast", Type = TokenType.Aggregate,  Help="(Список, индекс)\r\n - Возвращает значение из списка с указанным отступом от конца (0 - последнее значение)."   },
             new Token { TokenName = "CeilingToList", CodeReplace="AQMath.CeilingToList", Type = TokenType.Aggregate,  Help="(value, a, b)\r\n - Выбор из списка ближайшего наибольшего значения."},
             new Token { TokenName = "ListFromQuotedStrings", CodeReplace="AQMath.ListFromQuotedStrings", Type = TokenType.Aggregate,  Help="(значение)\r\n - Преобразует текст, \r\nвозвращаемый операцией Параметр-SQL \r\nвида 'a','b','c' в список значений. "   },
             new Token { TokenName = "Exists", CodeReplace="AQMath.Exists", Type = TokenType.Aggregate,  Help="(Список, значение)\r\n - Проверка списка на содержание заданного значения."   },
             new Token { TokenName = "SearchFile", CodeReplace="AQMath.SearchFile", Type = TokenType.Math,  Help="(Путь корневой директории, начало имени файла, расширение)\r\n - Поиск файла на сервере, выдает полный путь."   },

             new Token { TokenName = "ListFromStrings", CodeReplace="AQMath.ListFromStrings", Type = TokenType.Aggregate,  Help="(значение)\r\n - Преобразует многострочный текст в список значений (одно значение на строку)."   },
             new Token { TokenName = "Filter", CodeReplace="AQMath.Filter", Type = TokenType.Aggregate,  Help="(Список, значение)\r\n - Фильтрация списка, остаются только значения, содержащие указанную подстроку."   },
             new Token { TokenName = "List", CodeReplace="AQMath.List", Type = TokenType.Aggregate,  Help="(a, b, ...)\r\n - Список значений."   },
             new Token { TokenName = "Distinct", CodeReplace="AQMath.Distinct", Type = TokenType.Aggregate,  Help="(a, b, ...)\r\n - Преобразует список значений в список уникальных значений, повторы исключаются."   },
             new Token { TokenName = "DistinctNoEmptyStrings", CodeReplace="AQMath.DistinctNoEmptyStrings", Type = TokenType.Aggregate,  Help="(a, b, ...)\r\n - Преобразует список значений в список уникальных, отсортированных, не пустых значений."},
             new Token { TokenName = "SplitText", CodeReplace="AQMath.SplitText", Type = TokenType.String, MathArguments=2,  Help="(Аргумент, Разделитель) - Делит текст по указанному разделителю и преобразует в список."   },
             new Token { TokenName = "NumberSequence", CodeReplace="AQMath.NumberSequence", Type = TokenType.Aggregate, MathArguments=3,  Help="(Начало, шаг, длина) - Создает последовательность чисел в виде списка."   },
             new Token { TokenName = "DateSequence", CodeReplace="AQMath.DateSequence", Type = TokenType.Aggregate, MathArguments=3,  Help="(Начало, шаг, длина) - Создает последовательность дат в виде списка."   },

            new Token
            {
                CodeReplace = "Cell", 
                Help = "(Имя ячейки)\r\n Возвращает значение ячейки таблицы. Формат аналогичен MS Excel.",
                Inject = "Cell", 
 
                TokenName = "Cell",
                Type = TokenType.Math
            }, new Token
            {
                CodeReplace = "CellRange",
                Help = "(Диапазон)\r\n Возвращает список значений диапазона ячеек таблицы. Формат аналогичен MS Excel.",
                Inject = "CellRange",
                TokenName = "CellRange",
                Type = TokenType.Aggregate
            },
             new Token { TokenName = "SQLFilterFromStrings", CodeReplace="AQMath.SQLFilterFromStrings", Type = TokenType.Text,  Help="(Аргумент)\r\n - Возвращает фрагмент SQL-выражения - перечня значений, сформированного из строк аргумента."   },
             new Token { TokenName = "SQLFilterFromMeltList", CodeReplace="AQMath.SQLFilterFromMeltList", Type = TokenType.Text,  Help="(Аргумент, Имя поля)\r\n - Возвращает фрагмент SQL-выражения для отбора по заданному списку плавок."   },

             new Token { TokenName = "RowCount", CodeReplace="(Double)row.Table.Rows.Count", Type = TokenType.Math, MathArguments=0,  Help=" - Число записей (наблюдений) в текущей таблице."   },
             new Token { TokenName = "RowIndex", CodeReplace="(Double)row.Table.Rows.IndexOf(row)", Type = TokenType.Math, MathArguments=0,  Help=" - Номер текущей записи (наблюдения), нумерация начинается с 0."   },
             new Token { TokenName = "Row", CodeReplace="row", Type = TokenType.Math, MathArguments=0,  Help=" - Текущая запись (наблюдение)"   },
             new Token { TokenName = "Text", CodeReplace="AQMath.Text", Type = TokenType.String, MathArguments=1,  Help=" - Преобразование в текст."   },
             new Token { TokenName = "ParseNumber", CodeReplace="AQMath.ParseNumber", Type = TokenType.String, MathArguments=1,  Help=" - Преобразование в число."   },
             new Token { TokenName = "ContainsSubstring", CodeReplace="AQMath.ContainsSubstring", Type = TokenType.String, MathArguments=2,  Help=" - Преобразует значение в строку и выдает результат поиска."   },
             new Token { TokenName = "ContainsSubstringAndNonEmptyCI", CodeReplace="AQMath.ContainsSubstringAndNonEmptyCI", Type = TokenType.String, MathArguments=2,  Help=" - Преобразует значение в строку и выдает результат поиска независимо от регистра. При пустых значениях результат - False."   },
             new Token { TokenName = "ContainsSubstringCI", CodeReplace="AQMath.ContainsSubstringCI", Type = TokenType.String, MathArguments=2,  Help=" - Преобразует значение в строку и выдает результат поиска независимо от регистра."   },
             new Token { TokenName = "ContainsSubstringOrEmptyCI", CodeReplace="AQMath.ContainsSubstringOrEmptyCI", Type = TokenType.String, MathArguments=2,  Help=" - Преобразует значение в строку и выдает результат поиска независимо от регистра. Допускает пустые значения."   },
             new Token { TokenName = "IsNullOrEmpty", CodeReplace="(bool)String.IsNullOrEmpty", Type = TokenType.String, MathArguments=1,  Help=" - Проверяет строку на null или пустое значение."   },
             new Token { TokenName = "IsNotNullOrEmpty", CodeReplace="(bool)!String.IsNullOrEmpty", Type = TokenType.String, MathArguments=1,  Help=" - Проверяет строку на null или пустое значение."   },
             new Token { TokenName = "Concatenate", CodeReplace="AQMath.Concatenate", Type = TokenType.String, MathArguments=2,  Help="(список, разделитель) - Преобразует список в строку с заданным разделителем."   },
             new Token { TokenName = "ConcatenateDistinct", CodeReplace="AQMath.ConcatenateDistinct", Type = TokenType.String, MathArguments=2,  Help="(список, разделитель) - Преобразует список в строку с заданным разделителем. Дублирование данных исключается."   },
             new Token { TokenName = "Substring", CodeReplace="AQMath.Substring", Type = TokenType.String, MathArguments=3,  Help="(Аргумент, начало, длина) - Возвращает подстроку с заданным началом и длиной."   },
             new Token { TokenName = "AtStart", CodeReplace="AQMath.First", Type = TokenType.String, MathArguments=2,  Help="(Аргумент, длина) - Возвращает подстроку от начала с заданной длиной."   },
             new Token { TokenName = "AtEnd", CodeReplace="AQMath.Last", Type = TokenType.String, MathArguments=2,  Help="(Аргумент, длина) - Возвращает подстроку от конца строки с заданной длиной."   },
             new Token { TokenName = "SkipAtStart", CodeReplace="AQMath.Skip", Type = TokenType.String, MathArguments=2,  Help="(Аргумент, длина) - Возвращает подстроку, пропуская заданное число символов."   },
             new Token { TokenName = "TruncateAtEnd", CodeReplace="AQMath.Truncate", Type = TokenType.String, MathArguments=2,  Help="(Аргумент, длина) - Возвращает подстроку, обрезая заданное число символов в конце."   },
             new Token { TokenName = "Replace", CodeReplace="AQMath.Replace", Type = TokenType.String, MathArguments=3,  Help="(Аргумент, что найти, на что заменить) - Возвращает подстроку с выполненой заменой найденных фрагментов."   },
             new Token { TokenName = "Trim", CodeReplace="AQMath.Trim", Type = TokenType.String, MathArguments=1,  Help="(Аргумент) - Обрезает пробелы слева и справа строки."   },

             new Token { TokenName = "Sinh", CodeReplace="AQMath.Sinh", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Гиперболический синус аргумента."  },
             new Token { TokenName = "Cosh", CodeReplace="AQMath.Cosh", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Гиперболический косинус аргумента."  },
             new Token { TokenName = "Tanh", CodeReplace="AQMath.Tanh", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Гиперболический тангенс аргумента."  },
             new Token { TokenName = "Coth", CodeReplace="AQMath.Coth", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Гиперболический котангенс аргумента."  },
             new Token { TokenName = "Rnd", CodeReplace="AQMath.Rnd()", Type = TokenType.Math, MathArguments=0,  Help=" - Равномерно распределенное случайное число от 0 до 1."  },
             new Token { TokenName = "RndNormal", CodeReplace="AQMath.RndNormal", Type = TokenType.Math, MathArguments=2,  Help="(Среднее, Стандартное отклонение)\r\n Нормально распределенное случайное число."  },
             
             new Token { TokenName = "Gamma", CodeReplace="AQMath.Gamma", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Гамма-функция аргумента (аппроксимация)."  },
             new Token { TokenName = "GammaIncomplete", CodeReplace="AQMath.GammaIncomplete", Type = TokenType.Math, MathArguments=2,  Help="(К, Аргумент)\r\n Неполная гамма-функция аргумента (аппроксимация)."  },
             new Token { TokenName = "Beta", CodeReplace="AQMath.Beta", Type = TokenType.Math, MathArguments=2,  Help="(A,B)\r\n Бета-функция (аппроксимация)."  },
             new Token { TokenName = "BetaIncomplete", CodeReplace="AQMath.BetaIncomplete", Type = TokenType.Math, MathArguments=3,  Help="(A,B, Аргумент)\r\n Неполная бета-функция аргумента (аппроксимация)."  },
             new Token { TokenName = "Erf", CodeReplace="AQMath.Erf", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Функция ошибок (аппроксимация)."  },
             new Token { TokenName = "Factorial", CodeReplace="AQMath.Factorial", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Факториал (Гамма-функция аргумента + 1)."  },
             
             new Token { TokenName = "NormalPD", CodeReplace="AQMath.NormalPD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Среднее, Стандартное отклонение)\r\n Плотность вероятности нормального распределения."  },
             new Token { TokenName = "NormalCD", CodeReplace="AQMath.NormalCD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Среднее, Стандартное отклонение)\r\n Кумулятивная функция нормального распределения."  },
             new Token { TokenName = "NormalZbyP", CodeReplace="AQMath.NormalZbyP", Type = TokenType.Math, MathArguments=1,  Help="(Аргумент)\r\n Обратная функция нормального распределения, расчет Z по известному p."  },
             new Token { TokenName = "NormalCDInverse", CodeReplace="AQMath.NormalCDInverse", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент , Среднее, Стандартное отклонение)\r\n Кумулятивная функция нормального распределения 1-P."  },
             new Token { TokenName = "NormalCDBetween", CodeReplace="AQMath.NormalCDBetween", Type = TokenType.Math, MathArguments=4,  Help="(Аргумент, Аргумент2, Среднее, Стандартное отклонение)\r\n Кумулятивная функция нормального распределения между Аргументом и Аргументом2.\r\n Если Аргумент2 меньше Аргумента1 - возвращается функция 1-p."  },
             new Token { TokenName = "ChiSquarePD", CodeReplace="AQMath.ChiSquarePD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Число степеней свободы)\r\n Плотность вероятности распределения Хи-квадрат."  },
             new Token { TokenName = "ChiSquareCD", CodeReplace="AQMath.ChiSquareCD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Число степеней свободы)\r\n Кумулятивная функция распределения Хи-квадрат."  },
             new Token { TokenName = "StudentPD", CodeReplace="AQMath.StudentPD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Число степеней свободы)\r\n Плотность вероятности распределения Стьюдента."  },
             new Token { TokenName = "StudentCD", CodeReplace="AQMath.StudentCD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Число степеней свободы)\r\n Кумулятивная функция распределения Стьюдента."  },
             new Token { TokenName = "FisherPD", CodeReplace="AQMath.FisherPD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Число степеней свободы, Число степеней свободы2)\r\n Плотность вероятности F - распределения."  },
             new Token { TokenName = "FisherCD", CodeReplace="AQMath.FisherCD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Число степеней свободы, Число степеней свободы2)\r\n Кумулятивная функция F - распределения."  },
             new Token { TokenName = "BetaPD", CodeReplace="AQMath.BetaPD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Параметр1, Параметр2)\r\n Плотность вероятности Beta - распределения."  },
             new Token { TokenName = "BetaCD", CodeReplace="AQMath.BetaCD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Параметр1, Параметр2)\r\n Кумулятивная функция Beta - распределения."  },
             new Token { TokenName = "GammaPD", CodeReplace="AQMath.GammaPD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Параметр1)\r\n Плотность вероятности Gamma - распределения."  },
             new Token { TokenName = "GammaCD", CodeReplace="AQMath.GammaCD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Параметр1)\r\n Кумулятивная функция Gamma - распределения."  },
             new Token { TokenName = "PoissonPD", CodeReplace="AQMath.PoissonPD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Лямбда)\r\n Плотность вероятности распределения Пуассона."  },
             new Token { TokenName = "PoissonCD", CodeReplace="AQMath.PoissonCD", Type = TokenType.Math, MathArguments=2,  Help="(Аргумент, Лямбда)\r\n Кумулятивная функция распределения Пуассона."  },
             new Token { TokenName = "WeibullPD", CodeReplace="AQMath.WeibullPD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Лямбда, Параметр k)\r\n Плотность вероятности распределения Вейбулла."  },
             new Token { TokenName = "WeibullCD", CodeReplace="AQMath.WeibullCD", Type = TokenType.Math, MathArguments=3,  Help="(Аргумент, Лямбда, Параметр k)\r\n Кумулятивная функция распределения Вейбулла."  },
             new Token { TokenName = "WeibullGetKFromData", CodeReplace="AQMath.WeibullGetKFromData", Type = TokenType.Aggregate, MathArguments=1,  Help="([Имя таблицы].[Имя поля])\r\n - Параметр k распределения Вейбулла"  },
             new Token { TokenName = "WeibullGetLambdaFromData", CodeReplace="AQMath.WeibullGetLambdaFromData", Type = TokenType.Aggregate, MathArguments=2,  Help="([Имя таблицы].[Имя поля], Параметр k)\r\n - Параметр k распределения Вейбулла"  },

             new Token { TokenName = "CustomerRiskForP2PLeft", CodeReplace="AQMath.CustomerRiskForP2PLeft", Type = TokenType.Math, MathArguments=2,  Help="(s0, Среднее, Стандартное отклонение, Граница, Число первоначальных испытаний)\r\n Риск потребителя для плана испытаний п2п при ограничении слева."  },
             new Token { TokenName = "CustomerRiskForP2PRight", CodeReplace="AQMath.CustomerRiskForP2PRight", Type = TokenType.Math, MathArguments=2,  Help="(s0, Среднее, Стандартное отклонение, Граница, Число первоначальных испытаний)\r\n Риск потребителя для плана испытаний п2п при ограничении справа."  },
             new Token { TokenName = "CustomerRiskForRegressionLeft", CodeReplace="AQMath.CustomerRiskForRegressionLeft", Type = TokenType.Math, MathArguments=3,  Help="(Стандартное отклонение остатков, \r\nСреднее расчетных значений, Стандартное отклонение расчетных значений, \r\nСреднее фактических значений, Стандартное отклонение фактических значений, Граница)\r\n Риск потребителя для статистического контроля при ограничении слева."  },
             new Token { TokenName = "CustomerRiskForRegressionRight", CodeReplace="AQMath.CustomerRiskForRegressionRight", Type = TokenType.Math, MathArguments=3,  Help="(Стандартное отклонение остатков, \r\nСреднее расчетных значений, Стандартное отклонение расчетных значений, \r\nСреднее фактических значений, Стандартное отклонение фактических значений, Граница)\r\n Риск потребителя для статистического контроля при ограничении справа."  },

             new Token { TokenName = "HardenabilityNonBoronMetric", CodeReplace="AQMath.GetHardenabilityNonBoronMetric", Type = TokenType.Math, MathArguments=9,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V))\r\n Расчет прокаливаемости в заданной точке.\r\nНебористые марки, миллиметры"  },
             new Token { TokenName = "HardenabilityNonBoronInches", CodeReplace="AQMath.GetHardenabilityNonBoronInches", Type = TokenType.Math, MathArguments=9,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V))\r\n Расчет прокаливаемости в заданной точке.\r\nНебористые марки, дюймы"  },
             new Token { TokenName = "HardenabilityBoronMetric", CodeReplace="AQMath.GetHardenabilityBoronMetric", Type = TokenType.Math, MathArguments=9,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V))\r\n Расчет прокаливаемости в заданной точке.\r\nБористые марки, миллиметры"  },
             new Token { TokenName = "HardenabilityBoronInches", CodeReplace="AQMath.GetHardenabilityBoronInches", Type = TokenType.Math, MathArguments=9,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V))\r\n Расчет прокаливаемости в заданной точке.\r\nБористые марки, дюймы"  },
             
             new Token { TokenName = "HardenabilityNonBoronMetricGrain", CodeReplace="AQMath.GetHardenabilityNonBoronMetricWithGrain", Type = TokenType.Math, MathArguments=10,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V, Зерно))\r\n Расчет прокаливаемости в заданной точке с заданным зерном.\r\nНебористые марки, миллиметры"  },
             new Token { TokenName = "HardenabilityNonBoronInchesGrain", CodeReplace="AQMath.GetHardenabilityNonBoronInchesWithGrain", Type = TokenType.Math, MathArguments=10,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V, Зерно))\r\n Расчет прокаливаемости в заданной точке с заданным зерном.\r\nНебористые марки, дюймы"  },
             new Token { TokenName = "HardenabilityBoronMetricGrain", CodeReplace="AQMath.GetHardenabilityBoronMetricWithGrain", Type = TokenType.Math, MathArguments=10,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V, Зерно))\r\n Расчет прокаливаемости в заданной точке с заданным зерном.\r\nБористые марки, миллиметры"  },
             new Token { TokenName = "HardenabilityBoronInchesGrain", CodeReplace="AQMath.GetHardenabilityBoronInchesWithGrain", Type = TokenType.Math, MathArguments=10,  Help="(Точка, C, Mn, Si, Ni, Cr, Mo, Cu, V, Зерно))\r\n Расчет прокаливаемости в заданной точке с заданным зерном.\r\nБористые марки, дюймы"  },
             
             new Token { TokenName = "Today", CodeReplace="AQMath.Today()", Type = TokenType.Date, MathArguments=0,  Help=" - Текущая дата."  },
             new Token { TokenName = "Now", CodeReplace="AQMath.Now()", Type = TokenType.Date, MathArguments=0,  Help=" - Текущие дата и время."  },
             new Token { TokenName = "Date", CodeReplace="AQMath.Date", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразование в дату."   },
             new Token { TokenName = "Time", CodeReplace="AQMath.Time", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразует строку в формате hh:mm:ss во время." },
             new Token { TokenName = "Hour", CodeReplace="AQMath.Hour()", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает 1 час." },
             new Token { TokenName = "Minute", CodeReplace="AQMath.Minute()", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает 1 минуту." },
             new Token { TokenName = "Second", CodeReplace="AQMath.Second()", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает 1 секунду." },
             new Token { TokenName = "Day", CodeReplace="AQMath.Day()", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает 1 день." },
             new Token { TokenName = "DayOfYear", CodeReplace="AQMath.DayOfYear", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает номер дня в году." },
             new Token { TokenName = "DayOfMonth", CodeReplace="AQMath.DayOfMonth", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает номер дня в месяце." },
             new Token { TokenName = "DayOfWeek", CodeReplace="AQMath.DayOfWeek", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает название дня недели." },
             new Token { TokenName = "WeekOfYear", CodeReplace="AQMath.WeekOfYear", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает номер недели в году." },
             new Token { TokenName = "Year", CodeReplace="AQMath.Year", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает год." },
             new Token { TokenName = "Month", CodeReplace="AQMath.Month", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает номер месяца в году." },

             new Token { TokenName = "DateDifferenceInSeconds", CodeReplace="AQMath.DateDifferenceInSeconds", Type = TokenType.Date, MathArguments=2,  Help=" - (Начало периода, конец периода) Возвращает временной интервал в секундах." },
             new Token { TokenName = "DateDifferenceInMinutes", CodeReplace="AQMath.DateDifferenceInMinutes", Type = TokenType.Date, MathArguments=2,  Help=" - (Начало периода, конец периода) Возвращает временной интервал в минутах." },
             new Token { TokenName = "DateDifferenceInHours", CodeReplace="AQMath.DateDifferenceInHours", Type = TokenType.Date, MathArguments=2,  Help=" - (Начало периода, конец периода) Возвращает временной интервал в часах." },
             new Token { TokenName = "DateDifferenceInDays", CodeReplace="AQMath.DateDifferenceInDays", Type = TokenType.Date, MathArguments=2,  Help=" - (Начало периода, конец периода) Возвращает временной интервал во днях." },
             new Token { TokenName = "MinutesToTimeString", CodeReplace="AQMath.MinutesToTimeString", Type = TokenType.Date, MathArguments=1,  Help=" - (Минуты) Возвращает строку со значением времени." },
             new Token { TokenName = "DateTimeToDateString", CodeReplace="AQMath.DateTimeToDateString", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает строку со значением даты." },
             new Token { TokenName = "DateTimeToFormattedString", CodeReplace="AQMath.DateTimeToFormattedString", Type = TokenType.Date, MathArguments=2,  Help=" - Возвращает строку со значением даты в указанном формате." },
             new Token { TokenName = "DateTimeToTimeString", CodeReplace="AQMath.DateTimeToTimeString", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает строку со значением времени." },
             new Token { TokenName = "GetBrigadeNumberByDateTime", CodeReplace="AQMath.GetBrigadeNumberByDateTime", Type = TokenType.Date, MathArguments=1,  Help=" - Возвращает номер бригады в соответствии с коллективным договором, для 11.5-часовой смены. Дата должна быть указана не ранее 2010.01.01 08:00:00." },
             new Token { TokenName = "ParseDateTimeDMY", CodeReplace="AQMath.ParseDateTimeDMY", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразует строку в формате dd.MM.yyyy hh:mm:ss в дату." },
             new Token { TokenName = "ParseDateTimeYMD", CodeReplace="AQMath.ParseDateTimeYMD", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразует строку в формате yyyy/MM/dd hh:mm:ss в дату." },
             new Token { TokenName = "ParseDateTimeFormatted", CodeReplace="AQMath.ParseDateTimeFormatted", Type = TokenType.Date, MathArguments=2,  Help=" - Преобразует строку в указанном формате в дату." },
             new Token { TokenName = "ParseDateTimeAuto", CodeReplace="AQMath.ParseDateTimeAuto", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразует строку в дату." },
             new Token { TokenName = "RoundDateTimeToFloor", CodeReplace="AQMath.RoundDateTimeToFloor", Type = TokenType.Date, MathArguments=2,  Help="(значение, часть) - Округляет дату в меньшую сторону до указанной части.\r\nСекунды (s), минуты (m), часа (H), дня (d), месяца (M), года (Y)." },
             new Token { TokenName = "Time", CodeReplace="AQMath.Time", Type = TokenType.Date, MathArguments=1,  Help=" - Преобразует строку в формате hh:mm:ss во время." },
             

             new Token { TokenName = "null", Type = TokenType.Operator,  Help=" - Нет значения." },
             new Token { TokenName = "true", Type = TokenType.Operator,  Help=" - Истина." },
             new Token { TokenName = "false", Type = TokenType.Operator,  Help=" - Ложь." },
             new Token { TokenName = "'", Type = TokenType.Operator,  Help=" - Обращение к предыдущей записи." },
             new Token { TokenName = "`", Type = TokenType.Operator,  Help=" - Обращение к предыдущей записи." },
             new Token { TokenName = "~", Type = TokenType.Operator,  Help=" - Обращение к следующей записи." },
             new Token { TokenName = "+", Type = TokenType.Operator },
             new Token { TokenName = "?", Type = TokenType.Operator,  Help=" - Выражение1 : Выражение 2\r\n - Проверка условия, если значение в скобках перед ? истина, то на выходе будет результат Выражение1, иначе - Выражение2." },
             new Token { TokenName = ":", Type = TokenType.Operator,  Help=" - разделитель выражений оператора ? (проверка условия)." },
             new Token { TokenName = "&&", Type = TokenType.Operator,  Help=" - логическое И." },
             new Token { TokenName = "&", Type = TokenType.Operator,  Help=" - двоичное И." },
             new Token { TokenName = "^^", Type = TokenType.Operator,  Help=" - логическое ИСКЛЮЧАЮЩЕЕ ИЛИ." },
             new Token { TokenName = "^", Type = TokenType.Operator,  Help=" - двоичное ИСКЛЮЧАЮЩЕЕ ИЛИ." },
             new Token { TokenName = "And", CodeReplace="&&", Type = TokenType.Operator,  Help=" - логическое И." },
             new Token { TokenName = "||", Type = TokenType.Operator,  Help=" - логическое ИЛИ." },
             new Token { TokenName = "|", Type = TokenType.Operator,  Help=" - двоичное ИЛИ." },
             new Token { TokenName = "Or", CodeReplace="||", Type = TokenType.Operator,  Help=" - логическое ИЛИ." },
             new Token { TokenName = "!", Type = TokenType.Operator,  Help=" - логическое НЕ." },
             new Token { TokenName = "!=", Type = TokenType.Operator,  Help=" - проверка неравенства."  },
             new Token { TokenName = "==", Type = TokenType.Operator,  Help=" - равно."  },
             new Token { TokenName = "Not", CodeReplace="!", Type = TokenType.Operator,  Help=" - логическое НЕ." },
             new Token { TokenName = "-", Type = TokenType.Operator },
             new Token { TokenName = "*", Type = TokenType.Operator },
             new Token { TokenName = "/", Type = TokenType.Operator },
             new Token { TokenName = "^", Type = TokenType.Operator },
             new Token { TokenName = "=", CodeReplace="==", Type = TokenType.Operator,  Help=" - проверка равенства."   },
             new Token { TokenName = "<>", CodeReplace="!=", Type = TokenType.Operator,  Help=" - проверка неравенства." },
             new Token { TokenName = ">", Type = TokenType.Operator,  Help=" - больше." },
             new Token { TokenName = "<", Type = TokenType.Operator,  Help=" - меньше." },
             new Token { TokenName = ">=", Type = TokenType.Operator,  Help=" - больше или равно." },
             new Token { TokenName = "<=", Type = TokenType.Operator,  Help=" - меньше или равно." },
             new Token { TokenName = ",", Type = TokenType.Operator,  Help=" - разделитель аргументов." },
             new Token { TokenName = "(", Type = TokenType.Bracket },
             new Token { TokenName = ")", Type = TokenType.Bracket },
             new Token { TokenName = "[", Type = TokenType.Bracket },
             new Token { TokenName = "]", Type = TokenType.Bracket },
             new Token { TokenName = "{", CodeReplace="AQMath.AggregateCT(row, (a)=>{DataRow temprow = row; row=a; var res= ", Type = TokenType.Operator},
             new Token { TokenName = "}", CodeReplace="; row = temprow; return res;})", Type = TokenType.Operator}
         };

        public static Brush GetTokenColor(TokenType tt)
        {
            switch (tt)
            {
                case TokenType.Field:
                case TokenType.CurrentField:
                case TokenType.CurrentFieldList:
                case TokenType.PreviousRowField:
                case TokenType.NextRowField:
                case TokenType.Table:
                    return new SolidColorBrush(Colors.Brown);
                
                case TokenType.Variable:
                    return new SolidColorBrush(Colors.Magenta);

                case TokenType.KeyWord:
                case TokenType.Math:
                case TokenType.Aggregate:
                case TokenType.String:
                case TokenType.Date:
                    return new SolidColorBrush(Colors.Blue);

                case TokenType.Number:
                    return new SolidColorBrush(Colors.Green);

                case TokenType.Space:
                    return new SolidColorBrush(Colors.Transparent);

                case TokenType.Text:
                    return new SolidColorBrush(Colors.Red);

                case TokenType.Bracket:
                case TokenType.Operator:
                    return new SolidColorBrush(Colors.Black);

                case TokenType.Unknown:
                    return new SolidColorBrush(Colors.Gray);
                default:
                    return new SolidColorBrush(Colors.Transparent);
            }
        }

        public static IEnumerable<Token> GetAllTokensList(List<DataItem> allTables, DataItem currentTable, string currentField)
        {
            var tokens = Collection;
            var variableTable = allTables.Where(a => a != null).SingleOrDefault(a => a.DataItemType == DataType.Variables);
            var variables = (variableTable == null) ? new List<Token>() : variableTable.Fields.Select(f => new Token { TokenName = "@" + f.Name, Type = TokenType.Variable, CodeReplace = "(AQMath.GetFieldValue<" + GetCommonType(f.DataType) + ">(AQMath.GetTable(\"Переменные\",row),\"" + f.Name + "\", 0))", Inject = "@" + f.Name, UsedTable = variableTable, UsedField = new UsedField { Field = f, Table = variableTable } });

            IEnumerable<Token> currentfields;
            if (currentTable == null) currentfields = new List<Token>();
            else
            {
                currentfields = currentTable.Fields.Select(f => new Token { TokenName = "[" + f.Name + "]", Type = TokenType.CurrentField, Inject = string.Format("[{0}]", f.Name), CodeReplace = "(AQMath.GetFieldValue<" + GetCommonType(f.DataType) + ">(\"" + f.Name + "\", row))", UsedTable = currentTable, UsedField = new UsedField { Field = f, Table = currentTable } }).ToList();
                currentfields = currentfields.Concat(currentTable.Fields.Select(f => new Token { TokenName = string.Format(".[{0}]", f.Name), Type = TokenType.CurrentFieldList, Inject = string.Format(".[{0}]", f.Name), CodeReplace = "AQMath.GetFieldValues<" + GetCommonType(f.DataType) + ">(AQMath.GetTable(\"" + currentTable.Name + "\",row),\"" + f.Name + "\")", UsedTable = currentTable, UsedField = new UsedField { Field = f, Table = currentTable } })).ToList();
                currentfields = currentfields.Concat(currentTable.Fields.Select(f => new Token { TokenName = string.Format("`[{0}]", f.Name), Type = TokenType.PreviousRowField, Inject = string.Format("`[{0}]", f.Name), CodeReplace = "(AQMath.GetPreviousRowFieldValue<" + GetCommonType(f.DataType) + ">(\"" + f.Name + "\", row))", UsedTable = currentTable, UsedField = new UsedField { Field = f, Table = currentTable } })).ToList();
                currentfields = currentfields.Concat(currentTable.Fields.Select(f => new Token { TokenName = string.Format("'[{0}]", f.Name), Type = TokenType.PreviousRowField, Inject = string.Format("`[{0}]", f.Name), CodeReplace = "(AQMath.GetPreviousRowFieldValue<" + GetCommonType(f.DataType) + ">(\"" + f.Name + "\", row))", UsedTable = currentTable, UsedField = new UsedField { Field = f, Table = currentTable } })).ToList();
                currentfields = currentfields.Concat(currentTable.Fields.Select(f => new Token { TokenName = string.Format("~[{0}]", f.Name), Type = TokenType.NextRowField, Inject = string.Format("~[{0}]", f.Name), CodeReplace = "(AQMath.GetNextRowFieldValue<" + GetCommonType(f.DataType) + ">(\"" + f.Name + "\", row))", UsedTable = currentTable, UsedField = new UsedField { Field = f, Table = currentTable } })).ToList();
            }
            var tables = allTables.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).Select(f => new Token { TokenName = string.Format("[{0}].", f.Name), Type = TokenType.Table, Inject = string.Format("[{0}].", f.Name), UsedTable = f, CodeReplace = "AQMath.GetTable(\"" + f.Name + "\", row)." }).ToList();
            tables = allTables.Where(a => a != null)
                .Where(t => t.DataItemType == DataType.Selection)
                .Select(a => a.Fields.Select(f => new Token {TokenName = string.Format("[{0}].[{1}]", a.Name, f.Name), Type = TokenType.Field, Inject = "[" + a.Name + "].[" + f.Name + "]", CodeReplace = "AQMath.GetFieldValues<" + GetCommonType(f.DataType) + ">(AQMath.GetTable(\"" + a.Name + "\",row),\"" + f.Name + "\")", UsedTable = a, UsedField = new UsedField {Field = f, Table = a}}).ToList()).Aggregate(tables, (current, tables2) => current.Concat(tables2).ToList());
            if (currentTable == null) return tokens.Concat(variables).Concat(currentfields).Concat(tables).OrderBy(f => f.TokenName);
            
            var cellToken = new Token
            {
                CodeReplace = "AQMath.GetTable(\"" + currentTable.Name + "\", row).Cell", 
                Help = "(Имя ячейки)\r\n Возвращает значение ячейки текущей таблицы. Формат аналогичен MS Excel.",
                Inject = ".Cell", 
                UsedTable = currentTable, 
                TokenName = ".Cell",
                Type = TokenType.Math
            };
            var cellRangeToken = new Token
            {
                CodeReplace = "AQMath.GetTable(\"" + currentTable.Name + "\", row).CellRange",
                Help = "(Диапазон)\r\n Возвращает список значений диапазона ячеек текущей таблицы. Формат аналогичен MS Excel.",
                Inject = ".CellRange",
                UsedTable = currentTable,
                TokenName = ".CellRange",
                Type = TokenType.Aggregate
            };

            if (string.IsNullOrEmpty(currentField)) return tokens.Concat(new[] { cellToken, cellRangeToken }).Concat(variables).Concat(currentfields).Concat(tables).OrderBy(f => f.TokenName);
            var dt = currentTable.Fields.Where(a => a.Name == currentField).FirstOrDefault();
            var currentFieldToken = new Token
            {
                CodeReplace = "AQMath.GetFieldValue<" + GetCommonType(dt?.DataType) + ">(\"" + currentField + "\", row)",
                Help = "(Диапазон)\r\n Возвращает текущее значение изменяемой ячейки текущей таблицы.",
                Inject = "v",
                UsedTable = currentTable,
                UsedField =  new UsedField { Field = dt, Table = currentTable },
                TokenName = "v",
                Type = TokenType.CurrentField
            };
            var previousRowcurrentFieldToken = new Token
            {
                CodeReplace = "AQMath.GetPreviousRowFieldValue<" + GetCommonType(dt?.DataType) + ">(\"" + currentField + "\", row)",
                Help = "(Диапазон)\r\n Возвращает текущее значение изменяемой ячейки текущей таблицы из предыдущей строки.",
                Inject = "'v",
                UsedTable = currentTable,
                UsedField = new UsedField { Field = dt, Table = currentTable },
                TokenName = "'v",
                Type = TokenType.CurrentField
            };
            var previousRowcurrentFieldToken2 = new Token
            {
                CodeReplace = "AQMath.GetPreviousRowFieldValue<" + GetCommonType(dt?.DataType) + ">(\"" + currentField + "\", row)",
                Help = "(Диапазон)\r\n Возвращает текущее значение изменяемой ячейки текущей таблицы из предыдущей строки.",
                Inject = "`v",
                UsedTable = currentTable,
                UsedField = new UsedField { Field = dt, Table = currentTable },
                TokenName = "`v",
                Type = TokenType.CurrentField
            };
            var nextRowcurrentFieldToken = new Token
            {
                CodeReplace = "AQMath.GetNextRowFieldValue<" + GetCommonType(dt?.DataType) + ">(\"" + currentField + "\", row)",
                Help = "(Диапазон)\r\n Возвращает текущее значение изменяемой ячейки текущей таблицы из следующей строки.",
                Inject = "~v",
                UsedTable = currentTable,
                UsedField = new UsedField { Field = dt, Table = currentTable },
                TokenName = "~v",
                Type = TokenType.CurrentField
            };
            return tokens.Concat(new[] { cellToken, cellRangeToken, currentFieldToken, previousRowcurrentFieldToken, previousRowcurrentFieldToken2, nextRowcurrentFieldToken }).Concat(variables).Concat(currentfields).Concat(tables).OrderBy(f => f.TokenName);
        }

        private static string GetCommonType(string typeName)
        {
            if (typeName == null) return null;
            switch (typeName.ToLower())
            {
                case "timespan":
                    return "TimeSpan?";
                case "datetime":
                    return "DateTime?";
                case "string":
                    return "String";
                case "boolean":
                case "bool":
                    return "Boolean?";
                default:
                    return "Double?";
            }
        }

        public static List<Token> ParseTokens(string text, List<DataItem> allTables, DataItem currentTable, string currentField = null)
        {
            var position = 0;
            var result = new List<Token>();
            var commonTokens = GetAllTokensList(allTables, currentTable, currentField).OrderByDescending(n => n.TokenName.Length).ToList();
            RecursiveTokenFinder(text, new Token { Type = TokenType.Unknown, TokenName = string.Empty }, ref result, ref position, text.Length, commonTokens);
            return result;
        }

        public static bool CheckTokens(List<Token> tokensForCheck)
        {
            return ((from r in tokensForCheck where r.Type != TokenType.Space select r).Any() && !(from r in tokensForCheck where r.Type == TokenType.Unknown select r).Any());
        }

        private static void RecursiveTokenFinder(string t, Token unknownToken, ref List<Token> result, ref int position, int stopAt, IEnumerable<Token> commonTokens)
        {
            if (position >= stopAt)
            {
                FlushUnknownToken(ref unknownToken, ref result);
                return;
            }
            string text = t.Substring(position);
            var token = (from c in commonTokens where text.ToLower().StartsWith(c.TokenName.ToLower()) orderby c.TokenName.Length descending select c).FirstOrDefault();
            if (token != null)
            {
                FlushUnknownToken(ref unknownToken, ref result);
                result.Add(token);
                position += token.TokenName.Length;
            }
            else
            {
                //Text
                var m = Regex.Match(text, "^[\"][\\\\\\w\\s\\(\\)\\[\\]\\{\\}.,+-/*=<>?|:;'!@#$%^&]+[\"]", RegexOptions.IgnoreCase);
                if (m.Success)
                {
                    FlushUnknownToken(ref unknownToken, ref result);
                    result.Add(new Token { TokenName = m.Value, Type = TokenType.Text });
                    position += m.Value.Length;
                }
                else
                {
                    //Number
                    m = Regex.Match(text, @"^[0-9]+[.]?[0-9]*", RegexOptions.Multiline);
                    if (m.Success)
                    {
                        FlushUnknownToken(ref unknownToken, ref result);
                        result.Add(new Token { TokenName = m.Value, CodeReplace = m.Value + (m.Value.Contains(".") ? string.Empty : ".0"), Type = TokenType.Number });
                        position += m.Value.Length;
                    }
                    else
                    {
                        //Space
                        m = Regex.Match(text, @"^[\s]+", RegexOptions.Multiline);
                        if (m.Success)
                        {
                            FlushUnknownToken(ref unknownToken, ref result);
                            result.Add(new Token { TokenName = m.Value, Type = TokenType.Space });
                            position += m.Value.Length;
                        }
                        else
                        {
                            unknownToken.TokenName += t.Substring(position, 1);
                            position++;
                        }
                    }
                }
            }
            RecursiveTokenFinder(t, unknownToken, ref result, ref position, stopAt, commonTokens);
        }

        private static void FlushUnknownToken(ref Token unknownToken, ref List<Token> result)
        {
            if (unknownToken.TokenName != string.Empty)
            {
                result.Add(unknownToken);
                unknownToken = new Token { Type = TokenType.Unknown, TokenName = string.Empty };
            }
        }

        public static string GetCSharpCodeForTokens(List<Token> resultingTokens)
        {
            var c = resultingTokens.Aggregate("var Result=(", (current, t) => current + (t.CodeReplace ?? t.TokenName));
            if (c == null) return null;
            c += ");\r\nreturn Result;";
            return c;
        }

        public static string GetCSharpCodeForExpression(string text, List<DataItem> allTables, DataItem currentTable, string currentField)
        {
            return GetCSharpCodeForTokens(ParseTokens(text, allTables, currentTable, currentField));
        }

        public static void RenderTextBlockWithTokens(TextBlock presenterTextBox, List<Token> tokensToRender)
        {
            presenterTextBox.Inlines.Clear();
            foreach (var t in tokensToRender)
            {
                var r = new Run {Text = t.TokenName, Foreground = GetTokenColor(t.Type)};
                presenterTextBox.Inlines.Add(r);
            }
        }
    }

    public enum TokenType { Math, String, Date, Operator, Text, KeyWord, Number, Aggregate, Field, CurrentField, CurrentFieldList, Variable, Space, Bracket, Table, Unknown, PreviousRowField, NextRowField }
    
    public class Token : IEquatable<Token>
    {
        public string TokenName { get; set; }
        public string CodeReplace { get; set; }
        public TokenType Type { get; set; }
        public int MathArguments { get; set; }
        public string Help { get; set; }
        public string Inject { get; set; }
        public DataItem UsedTable { get; set; }
        public UsedField UsedField { get; set; }

        public bool Equals(Token t)
        {
            return (t.TokenName == TokenName && t.Type == Type);
        }
    }
}
