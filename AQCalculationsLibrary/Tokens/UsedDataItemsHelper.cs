﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.CalculationServiceReference;


// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public static class UsedDataItemsHelper
    {
        public static List<DataItem> GetUsedTables(List<Token> resultingTokens, DataItem selectedTable)
        {
            if (resultingTokens == null) return null;
            var a = (from r in resultingTokens where r.UsedTable != null select r.UsedTable).Distinct().ToList();
            if (selectedTable == null) return a;
            if (!a.Select(b=>b.Name).Contains(selectedTable.Name)) a.Add(selectedTable);
            return a;
        }

        public static List<DataItem> GetUsedTables(string expression, List<DataItem> allTables, DataItem selectedTable)
        {
            var resultingTokens = Tokens.ParseTokens(expression, allTables, selectedTable, null);
            if (resultingTokens == null) return null;
            var a = (from r in resultingTokens where r.UsedTable != null select r.UsedTable).Distinct().ToList();
            if (!a.Contains(selectedTable) && selectedTable != null) a.Add(selectedTable);
            return a;
        }

        public static List<UsedField> GetUsedFields(List<Token> resultingTokens)
        {
            return resultingTokens != null ? (from r in resultingTokens where r.UsedField != null select r.UsedField).Distinct().ToList() : null;
        }

        public static List<UsedField> GetUsedVariables(List<Token> resultingTokens, DataItem variableTable)
        {
            return resultingTokens != null ? (from r in resultingTokens where r.UsedTable == variableTable && r.UsedField != null select r.UsedField).Distinct().ToList() : null;
        }

        public static DataItem GetUsedVariableTable(List<Token> resultingTokens, DataItem variableTable)
        {
            if (resultingTokens != null)
            {
                var di = new DataItem
                {
                    DataItemType = variableTable.DataItemType,
                    Name = variableTable.Name,
                    TableName = variableTable.TableName,
                    Fields =
                        variableTable.Fields.Where(
                            b =>
                                GetUsedVariables(resultingTokens, variableTable)
                                    .Select(a => a.Field.Name)
                                    .Contains(b.Name)).ToList()
                };
                return di;
            }
            return null;
        }
    }
}
