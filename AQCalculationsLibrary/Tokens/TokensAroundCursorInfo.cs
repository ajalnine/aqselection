﻿using System.Linq;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace AQCalculationsLibrary
{
    public static class TokensAroundCursorInfo
    {
        public static Token GetTokenFullyBeforeCursor(List<Token> resultingTokens, int position)
        {
            if (resultingTokens.Count == 0) return null;

            var currentLength = 0;
            foreach (var t in resultingTokens)
            {
                currentLength += t.TokenName.Length;
                if (position == currentLength) return t;
            }
            return null;
        }

        public static Token GetTokenAfterCursor(List<Token> resultingTokens, int position)
        {
            var t = GetClosestToCursorToken(resultingTokens, position);
            return resultingTokens[resultingTokens.IndexOf(t) + 1];
        }

        public static Token GetWordTokenBeforeCursor(List<Token> resultingTokens, int position)
        {
            var t = GetClosestToCursorToken(resultingTokens, position);
            var index = resultingTokens.IndexOf(t);
            return resultingTokens.Take(index + 1).LastOrDefault(s => !(new[] { TokenType.Bracket, TokenType.Number, TokenType.Text, TokenType.Space, TokenType.Operator }).Contains(s.Type));
        }

        public static Token GetWordTokenAfterCursor(List<Token> resultingTokens, int position)
        {
            var t = GetClosestToCursorToken(resultingTokens, position);
            var index = resultingTokens.IndexOf(t);
            return resultingTokens.Skip(index + 1).FirstOrDefault(s => !(new[] { TokenType.Bracket, TokenType.Number, TokenType.Text, TokenType.Space, TokenType.Operator }).Contains(s.Type));
        }

        public static Token GetTokenInWhichCursor(List<Token> resultingTokens, int position)
        {
            if (resultingTokens == null || resultingTokens.Count == 0) return null;

            var currentLength = 0;

            foreach (var t in resultingTokens)
            {
                currentLength += t.TokenName.Length;
                if (position < currentLength) return t;
            }
            return null;
        }

        public static Token GetClosestToCursorToken(List<Token> resultingTokens, int position)
        {
            var t = GetTokenInWhichCursor(resultingTokens, position);
            if (t != null) return t;
            t = GetTokenFullyBeforeCursor(resultingTokens, position);
            if (t == null) resultingTokens.FirstOrDefault();
            return t;
        }

        public static int GetStartPositionOfToken(List<Token> resultingTokens, Token token)
        {
            return resultingTokens.Take(resultingTokens.IndexOf(token)).Sum(t => t.TokenName.Length);
        }
    }
}
