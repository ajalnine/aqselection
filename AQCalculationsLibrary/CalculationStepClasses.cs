﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Windows.Markup;

namespace AQCalculationsLibrary
{
    public class CalculationStep
    {
        public string ImageUrl { get; set; }
        public string EditorAssembly { get; set; }
        public string Calculator { get; set; }
        public string Name { get; set; }
        public StepGroup Group { get; set; }
        public bool RequiresMultipleSources { get; set; }
        public bool RequiresSelection {get; set; }
        public string RequiresRole { get; set; }
        public bool Disabled { get; set; }
        public bool Wide { get; set; }
        public int ID { get { return GetHashCode(); } }
    }

    [ContentProperty("Children")]
    public class CalculationStepsRoot
    {
        public CalculationStepsRoot()
        {
            Children = new List<CalculationStep>();
        }
        public List<CalculationStep> Children { get; set; }
    }

    public enum StepGroup 
    {
        [DisplayName("Источники данных")]
        DataSource, 
        [DisplayName("Фильтрация")]
        Filtering, 
        [DisplayName("Преобразование")]
        Transform, 
        [DisplayName("Расчеты")]
        Calculation,
        [DisplayName("Параметры")]
        Parameters,
        [DisplayName("Формат")]
        Format,
        [DisplayName("Отчет")]
        Report

    }

    public static class StepGroupConvert
    {
        public static StepGroup NameToGroup(string name)
        {
            var groupName = (typeof (StepGroup).GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.GetField)
                .Where(
                    fi =>
                    {
                        var displayNameAttribute = (DisplayNameAttribute)
                            fi.GetCustomAttributes(typeof (DisplayNameAttribute), true).SingleOrDefault();
                        return displayNameAttribute != null && displayNameAttribute.Name == name;
                    })
                .Select(fi => fi.Name)).SingleOrDefault();
            return (StepGroup)Enum.Parse(typeof(StepGroup), groupName, true);
        }

        public static string GroupToName(StepGroup @group)
        {
            return (typeof (StepGroup).GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.GetField)
                .Where(fi => fi.Name == @group.ToString())
                .Select(
                    fi =>
                    {
                        var displayNameAttribute     = (DisplayNameAttribute)
                            fi.GetCustomAttributes(typeof (DisplayNameAttribute), true).SingleOrDefault();
                        return displayNameAttribute != null ? displayNameAttribute.Name : null;
                    })).SingleOrDefault();
        }
    }

    public class DisplayNameAttribute : Attribute
    {
        public DisplayNameAttribute(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}
