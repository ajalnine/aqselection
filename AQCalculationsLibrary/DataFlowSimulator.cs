﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

namespace AQCalculationsLibrary
{
    public delegate void DataFlowSimulationFinishedDelegate(object o, DataFlowSimulationEventArgs e);
    public delegate void DataFlowSimulationErrorDelegate(object o, DataFlowSimulationErrorEventArgs e);

    public class GuidStepPair
    {
        public Step Operation;
        public Guid Guid;
    }

    public class OperationIndex
    {
        public int PositionOfOperation;
        public Guid Guid;
    }

    public class NewTableIndex
    {
        public int PositionOfOperation;
        public SLDataTable NewTable;
    }

    public class DataFlowSimulationEventArgs
    {
        public List<DataItem> Results;
        public List<DataItem> UsedInputs;
        public List<string> Deleted;
    }

    public class DataFlowSimulationErrorEventArgs
    {
        public InconsistenceState Error;
    }

    public class DataFlowSimulator
    {
        #region Трекинг источников данных

        public event DataFlowSimulationFinishedDelegate DataFlowSimulationFinished;
        public event DataFlowSimulationErrorDelegate DataFlowSimulationError;
        private List<DataItem> _results;
        private List<DataItem> _usedInputs;
        private List<DataItem> _inputs;
        private List<string> _deleted;
        private List<OperationView> _loadedOperationViews;
        private List<NewTableIndex> _newTables;

        public void SimulateDataFlow(CalculationDescription cd, List<DataItem> inputs, List<NewTableIndex> newTables = null)
        {
            if (cd == null)
            {
                DataFlowSimulationFinished?.Invoke(this, new DataFlowSimulationEventArgs ());
                return;
            }

            MakeOperationViews(cd);

            _usedInputs = new List<DataItem>();
            _deleted = new List<string>();
            _inputs = inputs;
            _results = new List<DataItem>();
            _newTables = newTables;
            if (cd.StepData.Count == 0)
            {
                var toEmitTables = newTables.Where(a => a.PositionOfOperation == 0).ToList();
                _results.AddRange(toEmitTables.Select(a => a.NewTable.GetDataItem()));
                DataFlowFinalize();
            }
            else
            {
                if (_newTables != null)
                {
                    var toEmitTables = newTables.Where(a => a.PositionOfOperation == 0).ToList();
                    _results.AddRange(toEmitTables.Select(a => a.NewTable.GetDataItem()));
                }
                _loadedOperationViews[0].PreviousResultDataItems = inputs.Union(_results).ToList();
                _loadedOperationViews[0].DataFlow(() =>
                {
                    DataFlowSimulationFinished?.Invoke(this,
                        new DataFlowSimulationEventArgs
                        {
                            UsedInputs = _usedInputs,
                            Deleted = _deleted,
                            Results = _results
                        });
                });
            }
        }

        private void OperationView_DataUpdated(object o, GenericEventArgs e)
        {
            var v = o as OperationView;
            if (v != null && v.CurrentInconsistenceState != InconsistenceState.Norm)
            {
                DataFlowSimulationError?.Invoke(this, new DataFlowSimulationErrorEventArgs {Error = v.CurrentInconsistenceState});
            }
            UpdateCurrentResults(v);

            var currentIndex = _loadedOperationViews.IndexOf(v);
            
            if (_newTables != null)
            {
                var toEmitTables = _newTables.Where(a => a.PositionOfOperation == currentIndex + 1).ToList();
                _results.AddRange(toEmitTables.Select(a => a.NewTable.GetDataItem()));
            }

            if (currentIndex == _loadedOperationViews.Count - 1) DataFlowFinalize();
            else
            {
                _loadedOperationViews[currentIndex + 1].PreviousResultDataItems = _results.Union(_inputs).ToList();
                _loadedOperationViews[currentIndex + 1].DataFlow(e.Callback);
            }
        }
        
        private void DataFlowFinalize()
        {
            DataFlowSimulationFinished?.Invoke(this,
                       new DataFlowSimulationEventArgs
                       {
                           UsedInputs = _usedInputs,
                           Deleted = _deleted,
                           Results = _results
                       });
        }

        private void UpdateCurrentResults(OperationView v)
        {
            if (v?.PresentedStep.Inputs != null)
            {
                foreach (var s in v.PresentedStep.Inputs)
                {
                    var existed = _inputs.SingleOrDefault(a => a.Name == s.Name);
                    if (existed == null) continue;
                    var i = _usedInputs.SingleOrDefault(a => a.Name == existed.Name);
                    if (i != null)
                    {
                        i.Fields = i.Fields.Union(existed.Fields).Distinct().ToList();
                    }
                    else _usedInputs.Add(existed);
                }
            }

            if (v?.PresentedStep.DeletedItems != null)
            {
                foreach (var s in v.PresentedStep.DeletedItems)
                {
                    var inp = _inputs.SingleOrDefault(a => a.Name == s);
                    if (inp != null) _deleted.Add(inp.Name);
                    else
                    {
                        var res = _results.SingleOrDefault(a => a.Name == s);
                        if (res != null) _results.Remove(res);
                    }
                }
            }

            if (v?.PresentedStep.Outputs != null)
            {
                foreach (var s in v.PresentedStep.Outputs)
                {
                    var existed = _inputs.Union(_results).Where(a => a.Name == s.Name);
                    var dataItems = existed as DataItem[] ?? existed.ToArray();
                    if (dataItems.Any())
                    {
                        _inputs.Remove(dataItems.Single());
                        _results.Remove(dataItems.Single());
                    }
                    _results.Add(s);
                }
            }
        }

        private void MakeOperationViews(CalculationDescription cd)
        {
            _loadedOperationViews = new List<OperationView>();
            foreach (var s in cd.StepData)
            {
                var ov = new OperationView
                {
                    AutoEdit = false,
                    PreviousResultDataItems = _results,
                    PresentedStep = s,
                    BoundModule = new CalculationStep
                    {
                        EditorAssembly = s.EditorAssemblyPath,
                        Group = StepGroupConvert.NameToGroup(s.Group)
                    },
                    StepReadyToSave = true
                };
                ov.DataUpdated += OperationView_DataUpdated;
                _loadedOperationViews.Add(ov);
            }
        }
        #endregion
    }
}
