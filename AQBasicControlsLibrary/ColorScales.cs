﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AQBasicControlsLibrary
{
    [Flags]
    public enum ColorScales
    {
        HSV2 = 1,
        HSV = 2,
        Blue = 3,
        Red = 4,
        RedGreen = 5,
        Gray = 6,
        Metal = 7,
        Warm = 8,
        Cold = 9, 
        Protanopia = 10,
        BW = 11,
        Brand = 12,
        Reversed = 0x100
    }

    public enum ColorScaleMode
    {
        Full = 0,
        FirstHalf = 1,
        QuartOffset = 2,
        LastHalf = 3,
    }
}
