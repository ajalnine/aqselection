﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace AQBasicControlsLibrary
{
    public class ExtendedTextBoxBase : TextBox, INotifyPropertyChanged
    {
        public delegate void TextChangedDelegate(object o, EventArgs e);
        public event TextChangedDelegate TextChanged;
        
        public string Tip
        {
            get { return (string)GetValue(TipProperty); }
            set { SetValue(TipProperty, value); }
        }
        public static readonly DependencyProperty TipProperty =
            DependencyProperty.Register("Tip", typeof(string), typeof(ExtendedTextBoxBase), new PropertyMetadata(String.Empty));

        public new string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public new static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ExtendedTextBoxBase), new PropertyMetadata(String.Empty, new PropertyChangedCallback(TextProperty_Changed)));

        
        public bool NumberOnly
        {
            get { return (bool)GetValue(NumberOnlyProperty); }
            set { SetValue(NumberOnlyProperty, value); }
        }
        public static readonly DependencyProperty NumberOnlyProperty =
            DependencyProperty.Register("NumberOnly", typeof(bool), typeof(ExtendedTextBoxBase), new PropertyMetadata(false));
        
        public bool MultiLine
        {
            get { return (bool)GetValue(MultiLineProperty); }
            set { SetValue(MultiLineProperty, value); }
        }
        public static readonly DependencyProperty MultiLineProperty =
            DependencyProperty.Register("MultiLine", typeof(bool), typeof(ExtendedTextBoxBase), new PropertyMetadata(false, new PropertyChangedCallback(MultiLineProperty_Changed)));

        public Visibility TipVisibility
        {
            get { return (Visibility)GetValue(TipVisibilityProperty); }
            set { SetValue(TipVisibilityProperty, value); }
        }
        public static readonly DependencyProperty TipVisibilityProperty =
            DependencyProperty.Register("TipVisibility", typeof(Visibility), typeof(ExtendedTextBoxBase), new PropertyMetadata(Visibility.Visible));

        public Visibility ErrorMarkVisibility
        {
            get { return (Visibility)GetValue(ErrorMarkVisibilityProperty); }
            set { SetValue(ErrorMarkVisibilityProperty, value); }
        }
        public static readonly DependencyProperty ErrorMarkVisibilityProperty =
            DependencyProperty.Register("ErrorMarkVisibility", typeof(Visibility), typeof(ExtendedTextBoxBase), new PropertyMetadata(Visibility.Collapsed));

        private static void MultiLineProperty_Changed(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ExtendedTextBoxBase sender = ((ExtendedTextBoxBase)o);
            if ((bool)e.NewValue == true)
            {
                 sender.TextWrapping = TextWrapping.Wrap;
                 sender.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                 sender.AcceptsReturn = true;
            }
        }

        private static void TextProperty_Changed(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ExtendedTextBoxBase sender = ((ExtendedTextBoxBase)o);
            sender.Text = e.NewValue.ToString();
            sender.TipVisibility = (e.NewValue.ToString().Trim() == string.Empty) ? Visibility.Visible : Visibility.Collapsed;
            sender.NotifyPropertyChanged("Text");
            if (sender.TextChanged != null) sender.TextChanged(sender, new EventArgs());
        }

        private bool CheckError()
        {
            double Temp;
            bool IsGood = double.TryParse(Text, out Temp);
            if (!IsGood && NumberOnly && Text != string.Empty)
            {
                ErrorMarkVisibility = Visibility.Visible;
                this.Focus();
                return false;
            }
            else
            {
                ErrorMarkVisibility = Visibility.Collapsed;
                return true;
            }
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            TipVisibility = Visibility.Collapsed;
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            if (CheckError() && this.Text.Trim() == string.Empty) TipVisibility = Visibility.Visible; 
            base.OnLostFocus(e);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}