﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public partial class ExtendedListBox : ListBox
    {
        public ExtendedListBox() : base()
        {
            I = new List<DependencyObject>();
            OldSource = this.ItemsSource;
        }

        public DependencyObject GetUIElementByItemIndex(int itemIndex)
        {
            if (I.Count <= itemIndex) return null;
            return (I[itemIndex]);
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            if (this.ItemsSource != OldSource)
            {
                I.Clear();
                OldSource = this.ItemsSource;
            }
            DependencyObject o = base.GetContainerForItemOverride();
            I.Add(o);
            return (o);
        }

        List<DependencyObject> I;
        object OldSource;
    }
}
