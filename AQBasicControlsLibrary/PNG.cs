﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ImageTools;
using ImageTools.IO.Png;

namespace AQBasicControlsLibrary
{
    public static class PNG
    {
        public static byte[] WriteableBitmapToPNG(WriteableBitmap wb)
        {
            byte[] Array = new byte[wb.PixelHeight * wb.PixelWidth * 4];
            int c = 0;
            for (int i = 0; i < Array.Length; i += 4)
            {
                byte[] b = BitConverter.GetBytes(wb.Pixels[c]);
                Array[i] =b[2];
                Array[i+1] = b[1];
                Array[i+2] = b[0];
                Array[i+3] = b[3];
                c++;
            }

            ExtendedImage ei = new ExtendedImage();
            ei.SetPixels(wb.PixelWidth, wb.PixelHeight, Array);
            PngEncoder pe = new PngEncoder();
            MemoryStream ms = new MemoryStream();
            pe.Encode(ei, ms);
            return ms.GetBuffer();
        }
    }
}
