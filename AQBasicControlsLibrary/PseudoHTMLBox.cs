﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace AQBasicControlsLibrary
{
    
    public class PseudoHTMLBox : UserControl
    {
        public delegate void PicturesLoadedDelegate(object o, EventArgs e);

        public event PicturesLoadedDelegate PicturesLoaded;
        private int TotalPictures, PicturesReady;
        
        public string Source
        {
            get { return (string)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(string), typeof(PseudoHTMLBox), new PropertyMetadata(String.Empty, new PropertyChangedCallback(SourcePropertyChangedCallback)));

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }
        public static readonly DependencyProperty TextAlignmentProperty =
            DependencyProperty.Register("TextAlignment", typeof(TextAlignment), typeof(PseudoHTMLBox), new PropertyMetadata(TextAlignment.Left, new PropertyChangedCallback(TextAlignmentPropertyChangedCallback)));

        public string DateHeader
        {
            get { return (string)GetValue(DateHeaderProperty); }
            set { SetValue(DateHeaderProperty, value); }
        }
        public static readonly DependencyProperty DateHeaderProperty =
            DependencyProperty.Register("DateHeader", typeof(string), typeof(PseudoHTMLBox), new PropertyMetadata(String.Empty, new PropertyChangedCallback(HeaderPropertyChangedCallback)));


        private static void TextAlignmentPropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PseudoHTMLBox SourceUC = o as PseudoHTMLBox;
            SourceUC.RefreshLayout(SourceUC.Source, SourceUC.DateHeader);
        }

        private static void SourcePropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PseudoHTMLBox SourceUC = o as PseudoHTMLBox;
            SourceUC.RefreshLayout(e.NewValue.ToString(), SourceUC.DateHeader);
        }

        private static void HeaderPropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
                   "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                   "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                   "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                   "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};

            PseudoHTMLBox SourceUC = o as PseudoHTMLBox;

            DateTime Date;
            DateTime.TryParseExact(e.NewValue.ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.AllowWhiteSpaces, out Date);
            SourceUC.RefreshLayout(SourceUC.Source, Date.ToShortDateString());
        }

        public PseudoHTMLBox()
        {
            this.Content = new StackPanel() { Orientation = Orientation.Vertical, Background = new SolidColorBrush(Colors.Transparent) };
        }

        public void RefreshLayout(string HTML, string Header)
        {
            ((StackPanel)this.Content).Children.Clear();
            if (Header != string.Empty)
            {
                TextBlock tb = new TextBlock();
                tb.TextWrapping = TextWrapping.Wrap;
                tb.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                Run r = new Run();
                r.Text = Header;
                r.Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0x26, 0x8f, 0x97));
                r.FontSize = 12;
                tb.Inlines.Add(r);
                ((StackPanel)this.Content).Children.Add(tb);
            }
            RichTextBox rtb = new RichTextBox();
            rtb.IsReadOnly = true;
            rtb.BorderThickness = new Thickness(0);
            rtb.Background = new SolidColorBrush(Colors.Transparent);
            ((StackPanel)this.Content).Children.Add(rtb);
            ParseHTML(HTML, rtb);
        }

        private void ParseHTML(string HTML, RichTextBox rtb)
        {
            Paragraph wp = StartNewLine(rtb);
            HTML = HTML.Replace("<li>", "<br />● ");
            bool IsBold = false;
            bool IsItalic = false;
            bool IsInHyperlink = false;
            bool IsRed = false;
            string color = string.Empty;
            Hyperlink CurrentHref = new Hyperlink();
            string CurrentURI = string.Empty;

            MatchCollection m = Regex.Matches(HTML, @"(<[^<>]+>)");
            if (m.Count != 0)
            {
                int Pos = 0;
                int Pos2 = 0;

                foreach (Match match in m)
                {
                    foreach (Capture c in match.Captures)
                    {
                        Pos2 = c.Index;
                        if (!IsInHyperlink) AddText(wp, HTML.Substring(Pos, Pos2 - Pos), IsBold, IsItalic, IsRed, color);
                        else AddHyperlinkText(CurrentHref, HTML.Substring(Pos, Pos2 - Pos));
                        string Token = c.Value.Replace(" ", "").ToLower();
                        switch (Token)
                        {
                            case "<b>":
                                IsBold = true;
                                break;
                            case @"</b>":
                                IsBold = false;
                                break;
                            case "<red>":
                                IsRed = true;
                                break;
                            case @"</red>":
                                IsRed = false;
                                break;
                            case "<i>":
                                IsItalic = true;
                                break;
                            case @"</i>":
                                IsItalic = false;
                                break;
                            case "<br/>":
                                wp = StartNewLine(rtb);
                                break;
                            case "</a>":
                                IsInHyperlink  = false;
                                break;
                            case "</#>":
                                color = string.Empty;
                                break;
                            default:
                                if (c.Value.StartsWith("<a"))
                                {
                                    if (c.Value.Contains("href="))
                                    {
                                        CurrentHref = new Hyperlink();
                                        CurrentHref.NavigateUri = new Uri(GetURIForLink(c.Value));
                                        CurrentHref.TargetName = "_blank";
                                        IsInHyperlink = true;
                                        wp.Inlines.Add(CurrentHref);
                                    }
                                }
                                if (c.Value.StartsWith("<img"))
                                {
                                    if (c.Value.Contains("src=")) AddImage(wp, c.Value);
                                }
                                else if (c.Value.StartsWith("<#"))
                                {
                                    color = "#"+c.Value.Substring(2, c.Value.Length -3);
                                }
                                break;
                        }
                        Pos = Pos2 + c.Length;
                        Pos2 = Pos;
                    }
                }
                if (Pos2 < HTML.Length)
                {
                    AddText(wp, HTML.Substring(Pos2, HTML.Length - Pos2), false, false, false, color);
                }
            }
            else
            {
                AddText(wp, HTML, false, false, false, color);
            }
            if (TotalPictures == 0 && PicturesLoaded != null) PicturesLoaded.Invoke(this, new EventArgs());
        }

        private Paragraph StartNewLine(RichTextBox rtb)
        {
            Paragraph p = new Paragraph();
            p.TextAlignment = TextAlignment;
            rtb.Blocks.Add(p);
            return p;
        }

        private void AddText(Paragraph wp, string Text, bool IsBold, bool IsItalic, bool IsRed, string color)
        {
            Run r = new Run();
            
            r.Text = Text;
            r.FontWeight = (IsBold) ? FontWeights.Bold : FontWeights.Light;
            r.FontStyle = (IsItalic) ? FontStyles.Italic : FontStyles.Normal;
            if (IsRed) r.Foreground = new SolidColorBrush(Colors.Red);
            else r.Foreground = this.Foreground;
            if (!string.IsNullOrEmpty(color)) r.Foreground = new SolidColorBrush(ConvertStringToColor(color));
            wp.Inlines.Add(r);
        }

        public static Color ConvertStringToColor(string c)
        {
            if (String.IsNullOrEmpty(c)) return Colors.Transparent;
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            return Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff));
        }

        private void AddHyperlinkText(Hyperlink CurrentHref, string Text)
        {
            Run r = new Run();
            r.Text = Text;
            CurrentHref.Inlines.Add(r);
        }

        private void AddImage(Paragraph wp, string TokenCode)
        {
            Match m = Regex.Match(TokenCode, "img src=\"(.+)\"");
            if (m.Groups.Count > 1)
            {
                string uri = m.Groups[1].Value;
                BitmapImage bi = new BitmapImage {UriSource = new Uri(uri, UriKind.RelativeOrAbsolute)};
                TotalPictures++;
                Image i = new Image();
                i.Source = bi;
                i.Width = 0;
                i.Height = 0;
                InlineUIContainer iuic = new InlineUIContainer();
                wp.Inlines.Add(iuic);
                iuic.Child = i;
                i.ImageOpened += bi_ImageOpened;
            }
        }

        private string GetURIForLink(string TokenCode)
        {
            Match m = Regex.Match(TokenCode, "a href=\"(.+)\"");
            if (m.Groups.Count > 1)
            {
                string uri = m.Groups[1].Value;
                return uri;
            }
            return null;
        }

        void bi_ImageOpened(object sender, RoutedEventArgs e)
        {
            Image i = (Image)sender;
            BitmapImage bi = i.Source as BitmapImage;
            i.Width = bi.PixelWidth;
            i.Height = bi.PixelHeight;
            ((StackPanel)this.Content).UpdateLayout();
            PicturesReady++;
            if (TotalPictures==PicturesReady && PicturesLoaded!=null)PicturesLoaded.Invoke(this, new EventArgs());
        }
    }
}
