﻿using System.Windows;

namespace AQBasicControlsLibrary
{
    public partial class DataGridColumnFilter
    {

        private static FilterDispatcher _filterDispatcher;
        public string ColumnName
        {
            get { return (string)GetValue(ColumnNameProperty); }
            set { SetValue(ColumnNameProperty, value); }
        }
        public static readonly DependencyProperty ColumnNameProperty =
            DependencyProperty.Register("ColumnName", typeof(string), typeof(DataGridColumnFilter), new PropertyMetadata(string.Empty, null));

        public bool InDataGrid
        {
            get { return (bool)GetValue(InDataGridProperty); }
            set { SetValue(InDataGridProperty, value); }
        }
        public static readonly DependencyProperty InDataGridProperty =
            DependencyProperty.Register("InDataGrid", typeof(bool), typeof(DataGridColumnFilter), new PropertyMetadata(true, null));

        public static FilterDispatcher GetFilterDispatcher(DependencyObject obj)
        {
            return (FilterDispatcher)obj.GetValue(FilterDispatcherProperty);
        }

        public static void SetFilterDispatcher(DependencyObject obj, FilterDispatcher value)
        {
            obj.SetValue(FilterDispatcherProperty, value);
        }

        public static readonly DependencyProperty FilterDispatcherProperty =
            DependencyProperty.RegisterAttached("FilterDispatcher", typeof(FilterDispatcher), typeof(DataGridColumnFilter), new PropertyMetadata(Target));

        private static void Target(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            _filterDispatcher = (FilterDispatcher)dependencyPropertyChangedEventArgs.NewValue;
        }

        public DataGridColumnFilter()
        {
            InitializeComponent();
        }
        
        public void CloseFilter()
        {
            FilterButton.IsChecked = false;
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            _filterDispatcher.HideFiltersExcept(ColumnName);
            _filterDispatcher.SetupFilter(this);
        }

        private void ToggleButton_OnUnchecked(object sender, RoutedEventArgs e)
        {
            _filterDispatcher.HideFilter(ColumnName);
        }

        public void ViewAsFiltered(bool isFiltered)
        {
            FilterIcon.Fill = isFiltered ? FilterIcon.Stroke : null;
        }

        private void FilterButton_OnLoaded(object sender, RoutedEventArgs e)
        {
            _filterDispatcher.Subscribe(this);
        }
    }
}
