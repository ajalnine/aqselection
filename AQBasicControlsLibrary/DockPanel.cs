﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public class DockPanel : Panel
    {
        public static Dock GetDock(DependencyObject obj)
        {
            return (Dock)obj.GetValue(DockProperty);
        }

        public static void SetDock(DependencyObject obj, Dock value)
        {
            obj.SetValue(DockProperty, value);
        }

        public static readonly DependencyProperty DockProperty =
            DependencyProperty.RegisterAttached("Dock", typeof(Dock), typeof(DockPanel), new PropertyMetadata(Dock.Left, new PropertyChangedCallback(DockPropertyChanged)));

        public DockPanel()
        {
        }

        private static void DockPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement Element = d as UIElement;
            if (Element == null) return;

            DockPanel DP = VisualTreeHelper.GetParent(d) as DockPanel;
            if (DP != null)
            {
                DP.InvalidateMeasure();
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            double UsedWidth = 0;
            double UsedHeight = 0;
            double MaxWidth = 0;
            double MaxHeight = 0;

            foreach (UIElement Element in Children)
            {
                Size FreeSpace = new Size(Math.Max(0, availableSize.Width - UsedWidth), Math.Max(0, availableSize.Height - UsedHeight));
                Element.Measure(FreeSpace);
                
                switch (GetDock(Element))
                {
                    case Dock.Left:
                    case Dock.Right:
                        MaxHeight = Math.Max(MaxHeight, UsedHeight + Element.DesiredSize.Height);
                        UsedWidth += Element.DesiredSize.Width;
                        break;
                    case Dock.Top:
                    case Dock.Bottom:
                        MaxWidth = Math.Max(MaxWidth, UsedWidth + Element.DesiredSize.Width);
                        UsedHeight += Element.DesiredSize.Height;
                        break;
                }
            }
            return new Size(Math.Max(MaxWidth, UsedWidth), Math.Max(MaxHeight, UsedHeight));
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            Double Left = 0;
            Double Right = finalSize.Width;
            Double Top = 0;
            Double Bottom = finalSize.Height;

            int Index = 0;
            foreach (var Element in this.Children)
            {
                Index++;
                if (Index != this.Children.Count)
                {
                    switch (GetDock(Element))
                    {
                        case Dock.Left:
                            Element.Arrange(new Rect(Left, Top, Element.DesiredSize.Width, Bottom - Top));
                            Left += Element.DesiredSize.Width;
                            break;
                        case Dock.Top:
                            Element.Arrange(new Rect(Left, Top, Right - Left, Element.DesiredSize.Height));
                            Top += Element.DesiredSize.Height;
                            break;
                        case Dock.Right:
                            Element.Arrange(new Rect(Right - Element.DesiredSize.Width, Top, Element.DesiredSize.Width, Bottom - Top));
                            Right -= Element.DesiredSize.Width;
                            break;
                        case Dock.Bottom:
                            Element.Arrange(new Rect(Left, Bottom - Element.DesiredSize.Height, Right - Left, Element.DesiredSize.Height));
                            Bottom -= Element.DesiredSize.Height;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Element.Arrange(new Rect(Left, Top, Right-Left, Bottom-Top));
                }
            }
            return finalSize;
        }
    }

    public enum Dock { Left, Top, Right, Bottom }
}
