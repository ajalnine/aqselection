﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Browser;
using System.Windows.Threading;

namespace AQBasicControlsLibrary
{
    public class TextImageButtonBase : Button
    {
        DispatcherTimer ClickTimer;

        public delegate void ButtonAutoClick(object sender, RoutedEventArgs e);
        public event ButtonAutoClick AutoClick;
        public delegate void ButtonClick(object sender, RoutedEventArgs e);
        public event ButtonClick Click;
        private float TickNumber = 0;
        
        public string ImageUrl
        {
            get { return (string)GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); ImageUrlChanged(value); }
        }

        public static readonly DependencyProperty ImageUrlProperty =
            DependencyProperty.Register("ImageUrl", typeof(string), typeof(TextImageButtonBase), new PropertyMetadata(String.Empty));

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(TextImageButtonBase), new PropertyMetadata(null));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(TextImageButtonBase), new PropertyMetadata(String.Empty));

        public TextImageButtonBase()
        {
            ClickTimer = new DispatcherTimer();
            ClickTimer.Tick += new EventHandler(ClickTimer_Tick);
        }
        
        private void ImageUrlChanged(string NewValue)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            ImageSource = new BitmapImage {UriSource = new Uri(NewValue, UriKind.Relative)};
        }

        private void ClickTimer_Tick(object sender, EventArgs e)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                ClickTimer.Interval = (TickNumber==0)? new TimeSpan(0, 0, 0, 0, 100):new TimeSpan(0, 0, 0, 0, (int)(100 / (1.0f + TickNumber))); 

                if (AutoClick != null) AutoClick.Invoke(this, new RoutedEventArgs());
                TickNumber += 0.1f;
                if (TickNumber > 2) TickNumber = 2;
            });
        }
      
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            e.Handled = false;
            if (AutoClick != null) AutoClick.Invoke(this, new RoutedEventArgs());
            if (Click != null) Click.Invoke(this, new RoutedEventArgs());
            ClickTimer.Interval = new TimeSpan(0, 0, 0, 0, 400);
            TickNumber = 0;
            ClickTimer.Start();
        }
        
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);
            e.Handled = false;
            ClickTimer.Stop();
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            ClickTimer.Stop();
            base.OnMouseLeave(e);
        }
    }
}
