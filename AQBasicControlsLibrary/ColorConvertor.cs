﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public static class ColorConvertor
    {
        static readonly List<Color> _coolColors = new List<Color>
        {
            Color.FromArgb(0xff, 000, 000, 012),
            Color.FromArgb(0xff, 107, 116, 123),
            Color.FromArgb(0xff, 178, 183, 187),
            Color.FromArgb(0xff, 241, 149, 190),
            Color.FromArgb(0xff, 231, 061, 150),
            Color.FromArgb(0xff, 178, 001, 092),
            Color.FromArgb(0xff, 119, 029, 125),
            Color.FromArgb(0xff, 027, 063, 149),
            Color.FromArgb(0xff, 008, 035, 102),
            Color.FromArgb(0xff, 073, 099, 174),
            Color.FromArgb(0xff, 000, 154, 200),
            Color.FromArgb(0xff, 000, 162, 177),
            Color.FromArgb(0xff, 000, 106, 096)
        };

        private static readonly List<Color> _warmColors = new List<Color>
        {
            Color.FromArgb(0xff, 087, 003, 000),
            Color.FromArgb(0xff, 156, 095, 012),
            Color.FromArgb(0xff, 240, 216, 190),
            Color.FromArgb(0xff, 248, 157, 136),
            Color.FromArgb(0xff, 179, 032, 024),
            Color.FromArgb(0xff, 205, 102, 025),
            Color.FromArgb(0xff, 242, 137, 030),
            Color.FromArgb(0xff, 229, 181, 057),
            Color.FromArgb(0xff, 102, 144, 068),
            Color.FromArgb(0xff, 053, 102, 057),
            Color.FromArgb(0xff, 000, 115, 134)
        };

        private static readonly List<Color> _protanopiaColors = new List<Color>
        {
            Color.FromArgb(0xff, 000, 000, 000),
            Color.FromArgb(0xff, 230, 159, 000),
            Color.FromArgb(0xff, 086, 180, 233),
            Color.FromArgb(0xff, 000, 158, 115),
            Color.FromArgb(0xff, 240, 228, 66),
            Color.FromArgb(0xff, 000, 114, 178),
            Color.FromArgb(0xff, 213, 094, 000),
            Color.FromArgb(0xff, 204, 121, 167)
        };
        
        private static readonly List<Color> _blueColors = new List<Color>
        {
            Color.FromArgb(0xff, 0x16, 0x16, 0x76),
            Color.FromArgb(0xff, 0x26, 0x8e, 0x96),
            Color.FromArgb(0xff, 0x96, 0x26, 0x8e),
            Color.FromArgb(0xff, 0xe6, 0xe6, 0xee),
        };

        private static readonly List<Color> _brandColors = new List<Color>
        {
            Color.FromArgb(0xff, 0xae, 0xaf, 0xb2),
            Color.FromArgb(0xff, 0x00, 0x8f, 0xd1),
            Color.FromArgb(0xff, 0x6e, 0xbb, 0x86),
            Color.FromArgb(0xff, 0xCE, 0xD0, 0xD2),
            Color.FromArgb(0xff, 0xD9, 0xF2, 0xFF),
        };

        public static Brush GetPatternBrush(double value)
        {
            var s = new LinearGradientBrush {SpreadMethod = GradientSpreadMethod.Repeat, MappingMode = BrushMappingMode.Absolute};
            var k = (int)(value * 20);
            var rotate = (double)((k % 4) / 4.0d) * Math.PI;
            var thickness = 1 + Math.Floor(k / 4) / 2;
            var inversion = k > 10;
            var wide = k % 10 > 5 ? 0.2 : 0.5;
            s.EndPoint = new Point(-thickness * Math.Sin(rotate), -thickness * Math.Cos(rotate));
            s.StartPoint = new Point(thickness * Math.Sin(rotate), thickness * Math.Cos(rotate));
            s.GradientStops = new GradientStopCollection
            {
                new GradientStop {Color = inversion ? Colors.White : Colors.Black, Offset = 0},
                new GradientStop {Color = inversion ? Colors.White : Colors.Black, Offset = 1/thickness*wide},
                new GradientStop {Color = inversion ? Colors.Black : Colors.White, Offset = 1/thickness*wide},
                new GradientStop {Color = inversion ? Colors.Black : Colors.White, Offset = 1},
            };

            return s;
        }

        private static readonly List<Color> _redColors = new List<Color>
        {
            Color.FromArgb(0xff, 0x00, 0x00, 0x00),
            Color.FromArgb(0xff, 0x7f, 0x2f, 0x00),
            Color.FromArgb(0xff, 0xff, 0x2f, 0x2f),
            Color.FromArgb(0xff, 0xff, 0x8f, 0x8f)
        };

        private static readonly List<Color> _redGreenColors = new List<Color>
        {
            Color.FromArgb(0xff, 0x10, 0xa0, 0x00),
            Color.FromArgb(0xff, 0xe6, 0xd7, 0x00),
            Color.FromArgb(0xff, 0xff, 0x01, 0x00)
        };
        
        static readonly Dictionary<ColorScales, string> DefaultColors =
        new Dictionary<ColorScales, string>
        {
                {(ColorScales)0, "#ff237cb5"},
                {ColorScales.HSV2, "#ff237cb5"},
                {ColorScales.HSV2 | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.HSV, "#ff237cb5"},
                {ColorScales.HSV | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Blue, "#ff268e96"},
                {ColorScales.Blue | ColorScales.Reversed, "#ff9D92CB"},
                {ColorScales.Red, "#ffcf2f2f"},
                {ColorScales.Red | ColorScales.Reversed, "#ff3f2f2f"},
                {ColorScales.RedGreen, "#ffb02000"},
                {ColorScales.RedGreen | ColorScales.Reversed, "#ff20b000"},
                {ColorScales.Metal, "#ff2f1f1f"},
                {ColorScales.Metal | ColorScales.Reversed, "#ffB16200"},
                {ColorScales.Gray, "#FF000000"},
                {ColorScales.Gray | ColorScales.Reversed, "#ff000000"},
                {ColorScales.Warm, GetGradientValue(_warmColors, 0.25)},
                {ColorScales.Warm | ColorScales.Reversed, GetGradientValue(_warmColors, 0.75)},
                {ColorScales.Cold, GetGradientValue(_coolColors, 0.25)},
                {ColorScales.Cold | ColorScales.Reversed, GetGradientValue(_coolColors, 0.75)},
                {ColorScales.Protanopia, GetGradientValue(_protanopiaColors, 0.25)},
                {ColorScales.Protanopia | ColorScales.Reversed, GetGradientValue(_protanopiaColors, 0.75)},
                {ColorScales.BW, "#FF000000"},
                {ColorScales.BW | ColorScales.Reversed, "#FF000000"},
                {ColorScales.Brand, "#FF008FD1"},
                {ColorScales.Brand | ColorScales.Reversed, "#FF008FD1"}
            };

        static readonly Dictionary<ColorScales, string> SelectionColors =
        new Dictionary<ColorScales, string>
        {
                {(ColorScales)0, "#ffBA15AD"},
                {ColorScales.HSV2, "#ffBA15AD"},
                {ColorScales.HSV2 | ColorScales.Reversed, "#ffffa500"},
                {ColorScales.HSV, "#ffBA15AD"},
                {ColorScales.HSV | ColorScales.Reversed, "#ffffa500"},
                {ColorScales.Blue, "#ffffa500"},
                {ColorScales.Blue | ColorScales.Reversed, "#ffffa500"},
                {ColorScales.Red, "#ffBA15AD"},
                {ColorScales.Red | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.RedGreen, "#ffBA15AD"},
                {ColorScales.RedGreen | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Metal, "#ffBA15AD"},
                {ColorScales.Metal | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Gray, "#ffBA15AD"},
                {ColorScales.Gray | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Cold, "#ffBA15AD"},
                {ColorScales.Cold | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Warm, "#ffBA15AD"},
                {ColorScales.Warm | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Protanopia, "#ffBA15AD"},
                {ColorScales.Protanopia | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.BW, "#ffBA15AD"},
                {ColorScales.BW | ColorScales.Reversed, "#ffBA15AD"},
                {ColorScales.Brand, "#ffBA15AD"},
                {ColorScales.Brand | ColorScales.Reversed, "#ffBA15AD"}
            };

        

        public static string HSVtoRGBString(double H, double S, double V)
        {
            var C = V * S;
            var Hx = H * 6d;
            var Hxmod = Hx - Math.Floor(Hx / 2) * 2;
            var X = C * (1 - Math.Abs(Hxmod - 1));
            var mode = (int)Math.Floor(Hx);
            double m = V - C;
            double R = 0, G = 0, B = 0;
            switch (mode)
            {
                case 0:
                case 6:
                    R = C + m;
                    G = X + m;
                    B = m;
                    break;

                case 1:
                    R = X + m;
                    G = C + m;
                    B = m;
                    break;

                case 2:
                    R = m;
                    G = C + m;
                    B = X + m;
                    break;

                case 3:
                    R = m;
                    G = X + m;
                    B = C + m;
                    break;

                case 4:
                    R = X + m;
                    G = m;
                    B = C + m;
                    break;

                case 5:

                    R = C + m;
                    G = m;
                    B = X + m;
                    break;

            }
            return Color.FromArgb(0xff, Convert.ToByte((R < 0 || R > 1 ? 0:R) * 255.0), Convert.ToByte((G < 0 || G > 1 ? 0 : G) * 255.0), Convert.ToByte((B < 0 || B > 1 ? 0 : B) * 255.0)).ToString();
        }

        public static string GetColorStringForScale(ColorScales scale, double value)
        {
            var isReversed = (scale & ColorScales.Reversed) == ColorScales.Reversed;
            if (isReversed) value = 1 - value;
            if (value > 1) value = 1;
            if (value < 0) value = 0;
            switch ((ColorScales)(((int)scale) & 0xff))
            {
                case ColorScales.HSV2:
                    return HSVtoRGBString(value < 0.68 ? 0.32 + value : value - 0.68, 0.7 + value * 0.3, 0.6 + value * 0.3);
                case ColorScales.HSV:
                    return HSVtoRGBString(value < 0.83 ? 0.17 + value : value - 0.83, 0.7 + value * 0.3, 0.6 + value * 0.3);
                case ColorScales.Blue:
                    return GetGradientValue(_blueColors, value);
                case ColorScales.Red:
                    return GetGradientValue(_redColors, value);
                case ColorScales.RedGreen: //
                    return GetGradientValue(_redGreenColors, value);
                case ColorScales.Metal:
                    return GetMetalColor(value);
                case ColorScales.Warm:
                    return GetGradientValue(_warmColors, value);
                case ColorScales.Cold:
                    return GetGradientValue(_coolColors, value);
                case ColorScales.Protanopia:
                    return GetGradientValue(_protanopiaColors, value);
                case ColorScales.BW:
                    return Colors.Black.ToString();
                case ColorScales.Brand:
                    return GetGradientValue(_brandColors, value);
                default:
                    return Color.FromArgb(0xff, Convert.ToByte(value * 0xef), Convert.ToByte(value * 0xef), Convert.ToByte(value * 0xef)).ToString();
            }
        }

        public static Color ConvertStringToColor(string c)
        {
            if (String.IsNullOrEmpty(c)) return Colors.Transparent;
            var cu = Convert.ToInt32(c.Replace("#", "0x"), 16);
            return Color.FromArgb((byte)((cu >> 0x18) & 0xff), (byte)((cu >> 0x10) & 0xff), (byte)((cu >> 8) & 0xff), (byte)(cu & 0xff));
        }

        public static string ConvertColorToString(Color color)
        {
            return color.ToString();
        }

        public static string GetDefaultColorForScale(ColorScales scale)
        {
            return DefaultColors[(ColorScales)((int)scale & 0x0fff)];
        }

        public static Color GetMixedColor(Color c1, Color c2)
        {
            return Color.FromArgb((byte)((c1.A + c2.A)/2), (byte)((c1.R + c2.R)/2), (byte)((c1.G + c2.G)/2), (byte)((c1.B + c2.B)/2));
        }

        public static string GetSelectionColorForScale(ColorScales scale)
        {
            return SelectionColors[(ColorScales)((int)scale & 0xffff)];
        }

        public static string GetMetalColor(double a)
        {
            if (a >= 0 && a < 0.2)   return Color.FromArgb(0xff, Convert.ToByte(0.1 * a * 5 * 0xff), Convert.ToByte(0), Convert.ToByte(0)).ToString();
            if (a >= 0.2 && a < 0.4) return Color.FromArgb(0xff, Convert.ToByte((0.1 + 0.3 * (a - 0.2) * 5) * 0xff), Convert.ToByte(0), Convert.ToByte(0)).ToString();
            if (a >= 0.4 && a < 0.6) return Color.FromArgb(0xff, Convert.ToByte((0.4 + 0.3 * (a - 0.4) * 5) * 0xff), Convert.ToByte((0.2 * (a - 0.4) * 5) * 0xff), Convert.ToByte(0)).ToString();
            if (a >= 0.6 && a < 0.8) return Color.FromArgb(0xff, Convert.ToByte(0.7 * 0xff), Convert.ToByte((0.2 + 0.5 * (a- 0.6) * 5) * 0xff), Convert.ToByte(0)).ToString();
            if (a >= 0.8 && a <= 1) return Color.FromArgb(0xff, Convert.ToByte((0.7 + 0.3 * (a - 0.8) * 5) * 0xef), Convert.ToByte((0.7 + 0.3 * (a - 0.8) * 5) * 0xef), Convert.ToByte((1 * (a - 0.8) * 5) * 0xef)).ToString();
            return "#00000000";
        }

        public static string GetGradientValue(List<Color> colors, double a)
        {
            var st = 1.0d / (colors.Count - 1);
            var i = (int)Math.Floor(a / st);
            var c1 = colors[i];
            var c2 = i + 1 < colors.Count ? colors[i + 1] : colors[i];
            var interpolate = (a - i * st) / st;
            return Color.FromArgb(0xff, 
                Convert.ToByte(c1.R + (c2.R - c1.R) * interpolate), 
                Convert.ToByte(c1.G + (c2.G - c1.G) * interpolate), 
                Convert.ToByte(c1.B + (c2.B - c1.B) * interpolate)).ToString();
        }
        
        public static Dictionary<ColorScaleMode, double> ColorOffset = new Dictionary<ColorScaleMode, double>
        {
            {ColorScaleMode.Full, 0.0d },
            {ColorScaleMode.FirstHalf, 0.0d },
            {ColorScaleMode.QuartOffset, 0.25d },
            {ColorScaleMode.LastHalf, 0.5d }
        };

        public static Dictionary<ColorScaleMode, double> ColorEnd = new Dictionary<ColorScaleMode, double>
        {
            {ColorScaleMode.Full, 1.0d },
            {ColorScaleMode.FirstHalf, 0.5d },
            {ColorScaleMode.QuartOffset, 0.75d },
            {ColorScaleMode.LastHalf, 1.0d }
        };

        public static Dictionary<ColorScaleMode, double> ColorPart = new Dictionary<ColorScaleMode, double>
        {
            {ColorScaleMode.Full, 1.0d },
            {ColorScaleMode.FirstHalf, 0.5d },
            {ColorScaleMode.QuartOffset, 0.5d },
            {ColorScaleMode.LastHalf, 0.5d }
        };
    }

    public class StringToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ColorConvertor.ConvertStringToColor(value?.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ColorConvertor.ConvertColorToString((Color)value);
        }
    }
}
