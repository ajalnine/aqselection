﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

namespace AQBasicControlsLibrary
{
    public class CatalogueItemPresenter : Control
    {
        public delegate void EditFinishedDelegate(object o, CatalogueItemPresenterEditEventArgs e);
        public event EditFinishedDelegate EditFinished;

        private string OldName;
        
        public CatalogueItemPresenter()
        {
            this.DefaultStyleKey = typeof(CatalogueItemPresenter);
        }

        public TreeViewExtended ParentTreeView
        {
            get { return (TreeViewExtended)GetValue(ParentTreeViewProperty); }
            set { SetValue(ParentTreeViewProperty, value); }
        }

        public static readonly DependencyProperty ParentTreeViewProperty =
            DependencyProperty.Register("ParentTreeView", typeof(TreeViewExtended), typeof(CatalogueItemPresenter), new PropertyMetadata(null));

        public string ItemName
        {
            get { return (string)GetValue(ItemNameProperty); }
            set { SetValue(ItemNameProperty, value); }
        }

        public static readonly DependencyProperty ItemNameProperty =
            DependencyProperty.Register("ItemName", typeof(string), typeof(CatalogueItemPresenter), new PropertyMetadata(String.Empty));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(CatalogueItemPresenter), new PropertyMetadata(String.Empty));

        public object ItemSign
        {
            get { return (object)GetValue(ItemSignProperty); }
            set { SetValue(ItemSignProperty, value); }
        }

        public static readonly DependencyProperty ItemSignProperty =
            DependencyProperty.Register("ItemSign", typeof(object), typeof(CatalogueItemPresenter), new PropertyMetadata(null));

        public override void OnApplyTemplate()
        {
            TextBox tb = ((TextBox)this.GetTemplateChild("ItemNameTextBox"));
            Observable.FromEvent<KeyEventArgs>(tb, "KeyDown").Subscribe(e=>
            {
                switch (e.EventArgs.Key)
                {
                    case Key.Enter:
                        tb.IsHitTestVisible = false;
                        OldName = null;
                        if (EditFinished != null) EditFinished(this, new CatalogueItemPresenterEditEventArgs() { NewName = tb.Text, DataItem = this.DataContext });
                        ParentTreeView.Focus();
                        break;

                    case Key.Escape:
                        tb.IsHitTestVisible = false;
                        tb.Text = OldName;
                        ParentTreeView.Focus();
                        break;

                    default:
                        break;
                }
            });

            if (ParentTreeView != null && this.DataContext!=null)
            {
                TreeViewItemExtended tvi = TreeViewWorkarounds.ContainerFromItem(ParentTreeView, this.DataContext) as TreeViewItemExtended;
                if (tvi != null)
                {
                    var EditStarts = Observable.FromEvent<EventArgs>(tvi, "EditModeStarted");
                    
                    EditStarts.Subscribe(e =>
                    {
                        tb.IsHitTestVisible = true;
                        OldName = tb.Text;
                        tb.Focus();
                    });

                    var FocusLost = Observable.FromEvent<RoutedEventArgs>(tb, "LostFocus");

                    var Cycle = EditStarts.Select(a=>a.Sender).Merge(FocusLost.Select(a=>a.Sender)).TimeInterval();
                    
                    Cycle.Subscribe(e =>
                    {
                        if (e.Interval.TotalMilliseconds > 50)
                        {
                            tb.IsHitTestVisible = false;
                            if (OldName != null) tb.Text = OldName;
                        }
                        else tb.Focus();
                    });
                }
            }
            base.OnApplyTemplate();
        }
    }

    public class CatalogueItemPresenterEditEventArgs : EventArgs
    {
        public string NewName { get; set; }
        public object DataItem { get; set; }
    }
}
