﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace AQBasicControlsLibrary
{
    public class UniformGrid : Panel
    {
        private CondenseMethod _condenseMethodBasedOnMeasure = CondenseMethod.NoChange;
        private double _maxCellWidth;
        private double _maxCellHeight;
        private double _condenseCoefficientNormal;
        private double _condenseCoefficientRotated;

        public int Rows
        {
            get { return (int)GetValue(RowsProperty); }
            set { SetValue(RowsProperty, value); }
        }

        public static readonly DependencyProperty RowsProperty =
            DependencyProperty.Register("Rows", typeof(int), typeof(UniformGrid), new PropertyMetadata(1, PropertiesChanged));

        public int Columns
        {
            get { return (int)GetValue(ColumnsProperty); }
            set { SetValue(ColumnsProperty, value); }
        }

        public static readonly DependencyProperty ColumnsProperty =
            DependencyProperty.Register("Columns", typeof(int), typeof(UniformGrid), new PropertyMetadata(1, PropertiesChanged));

        public bool HorizontalNodeCentered
        {
            get { return (bool)GetValue(HorizontalNodeCenteredProperty); }
            set { SetValue(HorizontalNodeCenteredProperty, value); }
        }

        public static readonly DependencyProperty HorizontalNodeCenteredProperty =
            DependencyProperty.Register("HorizontalNodeCentered", typeof(bool), typeof(UniformGrid), new PropertyMetadata(false, PropertiesChanged));

        public bool VerticalNodeCentered
        {
            get { return (bool)GetValue(VerticalNodeCenteredProperty); }
            set { SetValue(VerticalNodeCenteredProperty, value); }
        }

        public static readonly DependencyProperty VerticalNodeCenteredProperty =
            DependencyProperty.Register("VerticalNodeCentered", typeof(bool), typeof(UniformGrid), new PropertyMetadata(false, PropertiesChanged));

        public bool EnableCellCondense
        {
            get { return (bool)GetValue(EnableCellCondenseProperty); }
            set { SetValue(EnableCellCondenseProperty, value); }
        }

        public static readonly DependencyProperty EnableCellCondenseProperty =
            DependencyProperty.Register("EnableCellCondense", typeof(bool), typeof(UniformGrid), new PropertyMetadata(false, PropertiesChanged));

        public VerticalAlignment RotatedLabelsAlignment
        {
            get { return (VerticalAlignment)GetValue(RotatedLabelsAlignmentProperty); }
            set { SetValue(RotatedLabelsAlignmentProperty, value); }
        }

        public static readonly DependencyProperty RotatedLabelsAlignmentProperty =
            DependencyProperty.Register("RotatedLabelsAlignment", typeof(VerticalAlignment), typeof(UniformGrid), new PropertyMetadata(VerticalAlignment.Center, PropertiesChanged));

        public bool Reversed
        {
            get { return (bool)GetValue(ReversedProperty); }
            set { SetValue(ReversedProperty, value); }
        }

        public static readonly DependencyProperty ReversedProperty =
            DependencyProperty.Register("Reversed", typeof(bool), typeof(UniformGrid), new PropertyMetadata(false, PropertiesChanged));

        private static void PropertiesChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var ug = o as UniformGrid;
            if (ug == null) return;
            ug.InvalidateMeasure();
            ug.InvalidateArrange();
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            var cellMeasureWidth = availableSize.Width;
            var cellMeasureHeight = availableSize.Height;
            _maxCellWidth = 0;
            _maxCellHeight = 0;

            foreach (UIElement element in Children)
            {
                element.Measure(new Size(cellMeasureWidth, cellMeasureHeight));
                _maxCellWidth = Math.Max(element.DesiredSize.Width, _maxCellWidth);
                _maxCellHeight = Math.Max(element.DesiredSize.Height, _maxCellHeight);
            }
            var resultWidth = double.IsInfinity(availableSize.Width) ? _maxCellWidth * Columns : availableSize.Width;
            var resultHeight = double.IsInfinity(availableSize.Height) ? _maxCellHeight * Rows : availableSize.Height;

            _condenseMethodBasedOnMeasure = CondenseMethod.NoChange;

            if (EnableCellCondense)
            {
                if (Columns == 1 && (_maxCellHeight * Rows > availableSize.Height))
                {
                    _condenseCoefficientNormal = (_maxCellHeight * Rows) / availableSize.Height;
                    _condenseMethodBasedOnMeasure = CondenseMethod.SkipVerticalValues;
                }
                else if (Rows == 1)
                {
                    _condenseCoefficientNormal = (_maxCellWidth * Columns) / availableSize.Width;
                    _condenseCoefficientRotated = (_maxCellHeight * Columns) / availableSize.Width;

                    if (_condenseCoefficientNormal > 1 && _condenseCoefficientNormal < 2)
                    {
                        _condenseMethodBasedOnMeasure = CondenseMethod.ChessBoard;
                        return new Size(resultWidth, _maxCellHeight * 2);
                    }
                    if (_condenseCoefficientNormal > 2 && _condenseCoefficientRotated <= 1)
                    {
                        _condenseMethodBasedOnMeasure = CondenseMethod.AutoRotate;
                        foreach (var element in Children)
                        {
                            element.Measure(new Size(_maxCellWidth, _maxCellWidth));
                        }
                        return new Size(resultWidth, _maxCellWidth);
                    }
                    if (!(_condenseCoefficientRotated > 1)) return new Size(resultWidth, resultHeight);
                    _condenseMethodBasedOnMeasure = CondenseMethod.SkipRotatedHorizontalValues;
                    foreach (var element in Children)
                    {
                        element.Measure(new Size(_maxCellWidth, _maxCellWidth));
                    }
                    return new Size(resultWidth, _maxCellWidth);
                }
            }
            return new Size(resultWidth, resultHeight);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (Columns == 0) return finalSize;
            var cellSize = new Size(finalSize.Width / Columns, finalSize.Height / Rows);
            var realColumns = HorizontalNodeCentered ? Columns + 1 : Columns;
            var realRows = VerticalNodeCentered ? Rows + 1 : Rows;
            double cellHorizontalOffset;
            double cellVerticalOffset;
            var currentColumn = 1;
            var currentRow = 1;
            double xOffset = 0;
            double yOffset = 0;
            int cellsToSkip;
            bool toSkip;
            var controls = Reversed ? Children.Reverse().ToList() : Children.ToList();

            switch (_condenseMethodBasedOnMeasure)
            {
                case CondenseMethod.NoChange:
                    cellHorizontalOffset = HorizontalNodeCentered ? -cellSize.Width / 2 : 0;
                    cellVerticalOffset = VerticalNodeCentered ? -cellSize.Height / 2 : 0;

                    foreach (var element in controls)
                    {
                        element.Arrange(new Rect(xOffset + cellHorizontalOffset, yOffset + cellVerticalOffset, cellSize.Width, cellSize.Height));
                        element.RenderTransform = null;
                        currentColumn++;

                        if (currentColumn > realColumns)
                        {
                            currentRow++;
                            if (currentRow > realRows)
                            {
                                currentRow = 1;
                                yOffset = 0;
                            }
                            else yOffset += cellSize.Height;

                            xOffset = 0;
                            currentColumn = 1;
                        }
                        else xOffset += cellSize.Width;
                    }
                    break;

                case CondenseMethod.ChessBoard:

                    cellHorizontalOffset = HorizontalNodeCentered ? -cellSize.Width : -cellSize.Width / 2;
                    var isEven = false;
                    foreach (var element in controls)
                    {
                        cellVerticalOffset = (isEven) ? _maxCellHeight : 0;
                        element.Arrange(new Rect(xOffset + cellHorizontalOffset, yOffset + cellVerticalOffset, cellSize.Width * 2, _maxCellHeight));
                        element.RenderTransform = null;
                        currentColumn++;
                        xOffset += cellSize.Width;
                        isEven ^= true;

                        if (currentColumn <= realColumns) continue;
                        isEven = false;
                        xOffset = 0;
                        currentColumn = 1;
                    }
                    break;

                case CondenseMethod.AutoRotate:

                    cellHorizontalOffset = HorizontalNodeCentered ? -_maxCellHeight / 2 : cellSize.Width / 2 - _maxCellHeight / 2;
                    foreach (var element in controls)
                    {
                        cellVerticalOffset = 0;
                        switch (RotatedLabelsAlignment)
                        {
                            case VerticalAlignment.Top:
                                cellVerticalOffset = _maxCellWidth / 2 - element.DesiredSize.Width / 2;
                                break;
                            case VerticalAlignment.Bottom:
                                cellVerticalOffset = -_maxCellWidth / 2 + element.DesiredSize.Width / 2;
                                break;
                        }
                        element.Arrange(new Rect(xOffset + cellHorizontalOffset, 0, _maxCellWidth, _maxCellWidth));
                        element.RenderTransformOrigin = new Point(0.5, 0.5);
                        var rotateTransform = new RotateTransform { Angle = -90 };
                        var translateTransform = new TranslateTransform { X = -_maxCellWidth / 2 + _maxCellHeight / 2, Y = _maxCellWidth / 2 - _maxCellHeight / 2 - cellVerticalOffset };
                        var transformGroup = new TransformGroup();
                        transformGroup.Children.Add(rotateTransform);
                        transformGroup.Children.Add(translateTransform);
                        element.RenderTransform = transformGroup;
                        currentColumn++;
                        xOffset += cellSize.Width;

                        if (currentColumn <= realColumns) continue;
                        xOffset = 0;
                        currentColumn = 1;
                    }
                    break;

                case CondenseMethod.SkipRotatedHorizontalValues:

                    cellsToSkip = (int)Math.Ceiling(_condenseCoefficientRotated) - 1;
                    cellHorizontalOffset = HorizontalNodeCentered ? -_maxCellHeight / 2 : cellSize.Width / 2;
                    var count = controls.Count();
                    foreach (var element in controls)
                    {
                        toSkip = Reversed ? (Math.Abs(Math.IEEERemainder(count - currentColumn, cellsToSkip + 1)) < 1e-8)
                                            : (Math.Abs(Math.IEEERemainder(currentColumn - 1, cellsToSkip + 1)) < 1e-8);

                        if (toSkip)
                        {
                            cellVerticalOffset = 0;
                            switch (RotatedLabelsAlignment)
                            {
                                case VerticalAlignment.Top:
                                    cellVerticalOffset = _maxCellWidth / 2 - element.DesiredSize.Width / 2;
                                    break;
                                case VerticalAlignment.Bottom:
                                    cellVerticalOffset = -_maxCellWidth / 2 + element.DesiredSize.Width / 2;
                                    break;
                            }
                            element.Arrange(new Rect(xOffset + cellHorizontalOffset, 0, _maxCellWidth, _maxCellWidth));
                            element.RenderTransformOrigin = new Point(0.5, 0.5);
                            var rotateTransform = new RotateTransform { Angle = -90 };
                            var translateTransform = new TranslateTransform { X = (HorizontalNodeCentered ? -_maxCellWidth / 2 + _maxCellHeight / 2 : -_maxCellWidth / 2), Y = _maxCellWidth / 2 - _maxCellHeight / 2 - cellVerticalOffset };
                            var transformGroup = new TransformGroup();
                            transformGroup.Children.Add(rotateTransform);
                            transformGroup.Children.Add(translateTransform);
                            element.RenderTransform = transformGroup;
                        }
                        else
                        {
                            element.Arrange(new Rect(0, 0, 0, 0));
                        }
                        currentColumn++;
                        xOffset += cellSize.Width;

                        if (currentColumn > realColumns)
                        {
                            xOffset = 0;
                            currentColumn = 1;
                        }
                    }
                    break;

                case CondenseMethod.SkipVerticalValues:

                    cellsToSkip = (int)Math.Ceiling(_condenseCoefficientNormal) - 1;
                    cellHorizontalOffset = 0;
                    cellVerticalOffset = VerticalNodeCentered ? -cellSize.Height / 2 : 0;

                    foreach (var element in controls)
                    {
                        toSkip = Reversed ? (Math.Abs(Math.IEEERemainder(controls.Count() - currentColumn, cellsToSkip + 1)) < 1e-8) :
                                                 (Math.Abs(Math.IEEERemainder(currentColumn - 1, cellsToSkip + 1)) < 1e-8);

                        element.Arrange(toSkip
                            ? new Rect(xOffset + cellHorizontalOffset, yOffset + cellVerticalOffset, _maxCellWidth,
                                _maxCellHeight)
                            : new Rect(0, 0, 0, 0));

                        element.RenderTransform = null;
                        currentColumn++;

                        currentRow++;
                        if (currentRow > realRows)
                        {
                            currentRow = 1;
                            yOffset = 0;
                        }
                        else yOffset += cellSize.Height;
                    }
                    break;
            }
            return finalSize;
        }
    }
    public enum CondenseMethod { NoChange, SkipVerticalValues, ChessBoard, SkipRotatedHorizontalValues, AutoRotate }
}