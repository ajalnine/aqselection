﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public class WrapPanel : Panel
    {
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(WrapPanel), new PropertyMetadata(Orientation.Horizontal, new PropertyChangedCallback(OrientationPropertyChanged)));
        
        public WrapPanel() 
        {

        }

        public static void OrientationPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            WrapPanel wp = o as WrapPanel;
            if (wp!=null)wp.InvalidateMeasure();
        }
        
        protected override Size MeasureOverride(Size availableSize)
        {
            double LineSize = 0;
            double MaxInLineSize = 0;
            double MaxWidth = 0;
            double MaxHeight = 0;

            if (Orientation == Orientation.Horizontal)
            {
                foreach (var Element in this.Children)
                {
                    Element.Measure(availableSize);
                    if (LineSize + Element.DesiredSize.Width > availableSize.Width)
                    {
                        MaxWidth = Math.Max(MaxWidth , LineSize);
                        LineSize = 0;
                        MaxHeight += Math.Max(MaxInLineSize, Element.DesiredSize.Height);
                        MaxInLineSize = 0;
                    }
                    MaxInLineSize = Math.Max(MaxInLineSize, Element.DesiredSize.Height);
                    LineSize += Element.DesiredSize.Width;
                    MaxWidth = Math.Max(MaxWidth, LineSize);
                }
                MaxHeight += MaxInLineSize;
            }
            else
            {
                foreach (var Element in this.Children)
                {
                    Element.Measure(availableSize);
                    if (LineSize + Element.DesiredSize.Height > availableSize.Height)
                    {
                        MaxHeight = Math.Max(MaxHeight, LineSize);
                        LineSize = 0;
                        MaxWidth += Math.Max(MaxInLineSize, Element.DesiredSize.Width);
                        MaxInLineSize = 0;
                    }
                    MaxInLineSize = Math.Max(MaxInLineSize, Element.DesiredSize.Width);
                    LineSize += Element.DesiredSize.Height;
                    MaxHeight = Math.Max(MaxHeight, LineSize);
                }
                MaxWidth += MaxInLineSize;
            }
            return new Size(MaxWidth, MaxHeight);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            double LineOffset = 0;
            double CurrentInLineOffset = 0;
            double CurrentMaxLineOffset = 0;

            if (Orientation == Orientation.Horizontal)
            {
                foreach(var Element in this.Children)
                {
                    CurrentMaxLineOffset = Math.Max(CurrentMaxLineOffset, Element.DesiredSize.Height);
                    if (CurrentInLineOffset + Element.DesiredSize.Width > finalSize.Width)
                    {
                        LineOffset += CurrentMaxLineOffset;
                        Element.Arrange(new Rect(0, LineOffset, Element.DesiredSize.Width, Element.DesiredSize.Height));
                        CurrentInLineOffset = Element.DesiredSize.Width;
                        CurrentMaxLineOffset = Element.DesiredSize.Height;
                    }
                    else
                    {
                        Element.Arrange(new Rect(CurrentInLineOffset, LineOffset, Element.DesiredSize.Width, Element.DesiredSize.Height));
                        CurrentInLineOffset += Element.DesiredSize.Width;
                    }
                }
            }
            else
            {
                foreach (var Element in this.Children)
                {
                    CurrentMaxLineOffset = Math.Max(CurrentMaxLineOffset, Element.DesiredSize.Width);
                    if (CurrentInLineOffset + Element.DesiredSize.Height > finalSize.Height)
                    {
                        LineOffset += CurrentMaxLineOffset;
                        Element.Arrange(new Rect(LineOffset, 0, Element.DesiredSize.Width, Element.DesiredSize.Height));
                        CurrentInLineOffset = Element.DesiredSize.Height;
                        CurrentMaxLineOffset = Element.DesiredSize.Width;
                    }
                    else
                    {
                        Element.Arrange(new Rect(LineOffset, CurrentInLineOffset, Element.DesiredSize.Width, Element.DesiredSize.Height));
                        CurrentInLineOffset += Element.DesiredSize.Height;
                    }
                }
            }
            return finalSize;
        }
    }
}
