﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public partial class VerticalTextBlock : UserControl
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(VerticalTextBlock), new PropertyMetadata(String.Empty));

        public VerticalTextBlock()
        {
            InitializeComponent();
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            double width = 0;
            double height = 0;
            Content.Measure(new Size(Double.PositiveInfinity, availableSize.Width));
            height += Content.DesiredSize.Width;
            width = Math.Max(width, Content.DesiredSize.Height);
            return new Size(width, height);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
                Rect arrangeRect = new Rect();
                arrangeRect.X = 0;
                arrangeRect.Y = 0;
                arrangeRect.Width = Content.DesiredSize.Width;
                arrangeRect.Height = finalSize.Width;
                Content.Arrange(arrangeRect);
                
                RotateTransform rotateTransform = new RotateTransform
                {
                    Angle = -90,
                };

                TranslateTransform translateTransform = new TranslateTransform
                {
                    X = 0,
                    Y = finalSize.Height
                };

                TransformGroup transformGroup = new TransformGroup();
                transformGroup.Children.Add(rotateTransform);
                transformGroup.Children.Add(translateTransform);
                Content.RenderTransform = transformGroup;

            return finalSize;
        }
    }
}
