﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AQBasicControlsLibrary
{
    public class TextImageRadioButtonBase : RadioButton
    {
        public string ImageUrl
        {
            get { return (string)GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); ImageUrlChanged(value); }
        }

        public static readonly DependencyProperty ImageUrlProperty =
            DependencyProperty.Register("ImageUrl", typeof(string), typeof(TextImageRadioButtonBase), new PropertyMetadata(String.Empty));

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(TextImageRadioButtonBase), new PropertyMetadata(null));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(TextImageRadioButtonBase), new PropertyMetadata(String.Empty));

        public TextImageRadioButtonBase() : base()
        {
        }

        private void ImageUrlChanged(string NewValue)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            ImageSource = new BitmapImage { UriSource = new Uri(NewValue, UriKind.Relative) };
        }
    }
}
