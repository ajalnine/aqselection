﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public static class RichTextBoxHelper
    {
        public static void SetText(RichTextBox rtb, string Text)
        {
            Paragraph p = new Paragraph();
            Run r = new Run();
            r.Text = Text;
            p.Inlines.Add(r);
            rtb.Blocks.Clear();
            rtb.Blocks.Add(p);
        }

        public static string GetText(RichTextBox rtb)
        {
            string Text = string.Empty;
            foreach (var b in rtb.Blocks)
            {
                Paragraph p = b as Paragraph;
                if (p!=null)
                {
                    foreach (var i in p.Inlines)
                    {
                        Run r = i as Run;
                        if (r != null) Text += r.Text;
                    }
                }
                
            }
            return Text;
        }
    }
}
