﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace AQBasicControlsLibrary
{
    public partial class ExtendedTextBox : UserControl
    {
        public string Tip
        {
            get
            {
                return TipBlock.Text;
            }
            set
            {
                TipBlock.Text = value;
            }
        }

        public bool NumberOnly { get; set; }
        public bool MultiLine { get; set; }

        public delegate void TextChangedDelegate(object o, EventArgs e);

        public event TextChangedDelegate TextChanged;

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ExtendedTextBox), new PropertyMetadata(string.Empty));

        public ExtendedTextBox()
        {
            MultiLine = false;
            InitializeComponent();
        }

        private void MainTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TipBlock.Visibility = Visibility.Collapsed;
        }

        private void MainTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            CheckError();
            if (CheckError() && MainTextBox.Text.Trim() == string.Empty) TipBlock.Visibility = Visibility.Visible;
        }

        private void MainTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TipBlock.Visibility = (String.IsNullOrWhiteSpace(MainTextBox.Text)) ? Visibility.Visible : Visibility.Collapsed;
            Text = MainTextBox.Text;
            if (TextChanged != null) TextChanged(this, new EventArgs());
        }

        private bool CheckError()
        {
            double Temp;
            Text = MainTextBox.Text;
            var divider = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var t = Text.Replace(",", divider).Replace(".", divider);
            bool IsGood = double.TryParse(t, out Temp);
            if (!IsGood && NumberOnly && t != string.Empty)
            {
                ErrorPlate.Visibility = Visibility.Visible;
                MainTextBox.Focus();
                return false;
            }
            else
            {
                ErrorPlate.Visibility = Visibility.Collapsed;
                return true;
            }
        }

        private void MainTextBox_Loaded(object sender, RoutedEventArgs e)
        {
            LayoutRoot.Background = base.Background;
            if (MultiLine)
            {
                MainTextBox.TextWrapping = TextWrapping.Wrap;
                MainTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                MainTextBox.AcceptsReturn = true;
            }
        }
    }
}
