﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public class CheckableItem :INotifyPropertyChanged
    {
        private object val;
        public object Value
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
                NotifyPropertyChanged("Value");
            }
        }

        private bool? ischecked;
        public bool? IsChecked
        {
            get
            {
                return ischecked;
            }
            set
            {
                ischecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
       
    }
}
