﻿using System.Collections.Generic;
using System.Linq;

namespace AQBasicControlsLibrary
{
    public class FilterDispatcher
    {
        public event FilterRequestDelegate FilterRequest;

        private readonly Dictionary<string, DataGridColumnFilter> _filters = new Dictionary<string, DataGridColumnFilter>();

        public void ClearSubscription()
        {
            _filters.Clear();
        }

        public void Subscribe(DataGridColumnFilter dgcf)
        {
            if (!_filters.ContainsKey(dgcf.ColumnName)) _filters.Add(dgcf.ColumnName, dgcf);
            else _filters[dgcf.ColumnName] = dgcf;
            if (!_filters.ContainsKey("#" + dgcf.ColumnName+ "_Цвет")) _filters.Add("#" + dgcf.ColumnName + "_Цвет", dgcf);
            else _filters["#" + dgcf.ColumnName + "_Цвет"] = dgcf;
        }

        public void HideFiltersExcept(string columnName)
        {
            foreach (var f in _filters.Values.Where(f => f.ColumnName != columnName))
            {
                f.CloseFilter();
            }
        }

        public void HideFilters()
        {
            foreach (var f in _filters.Values)
            {
                f.CloseFilter();
            }
        }

        public void RenameColumn(string oldName, string newName)
        {
            if (!_filters.ContainsKey(oldName)) return;
            var c = _filters[oldName];
            _filters.Remove(oldName);
            _filters.Add(newName, c);
            c.ColumnName = newName;
        }
        public void HideFilter(string columnName)
        {
            foreach (var f in _filters.Values.Where(f => f.ColumnName == columnName))
            {
                f.CloseFilter();
                FilterRequest?.Invoke(this, new FilterRequestEventArg { FilterControl = f, FilterCommand = FilterCommand.Close });
            }
        }

        public void SetupFilter(DataGridColumnFilter dgcf)
        {
            FilterRequest?.Invoke(this, new FilterRequestEventArg {FilterControl = dgcf, FilterCommand  = FilterCommand.Edit});
        }

        public void SetAsFiltered(List<string> columnNames)
        {
            if (columnNames == null || columnNames.Count == 0) ResetFiltered();
            else
            {
                foreach (var f in _filters)
                {
                    f.Value.ViewAsFiltered(columnNames.Contains(f.Key) || columnNames.Contains("#" + f.Key + "_Цвет"));
                }
            }
        }

        public void ResetFiltered()
        {
            foreach (var f in _filters)
            {
                f.Value.ViewAsFiltered(false);
            }
        }

        public void UnSubscribe(DataGridColumnFilter dataGridColumnFilter)
        {
            _filters.Remove(dataGridColumnFilter.ColumnName);
            _filters.Remove("#"+dataGridColumnFilter.ColumnName+"_Цвет");
        }
    }

    public delegate void FilterRequestDelegate(object o, FilterRequestEventArg e);

    public class FilterRequestEventArg
    {
        public DataGridColumnFilter FilterControl;
        public FilterCommand FilterCommand;
    }

    public enum FilterCommand {Edit, Close }
}
