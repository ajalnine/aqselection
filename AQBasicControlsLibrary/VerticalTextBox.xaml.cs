﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQBasicControlsLibrary
{
    public partial class VerticalTextBox : UserControl
    {
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(VerticalTextBox), new PropertyMetadata(String.Empty));




        public Brush Foreground
        {
            get { return (Brush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Foreground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ForegroundProperty =
            DependencyProperty.Register("Foreground", typeof(Brush), typeof(VerticalTextBox), new PropertyMetadata(new SolidColorBrush(Colors.Black), ForegroundPropertyChangedCallback));

        private static void ForegroundPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((VerticalTextBox) dependencyObject).Content.Foreground = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public VerticalTextBox()
        {
            InitializeComponent();
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            var t = ((TextBox) Content);
            t.Text = Text;
            t.FontSize = FontSize;
            t.MaxWidth = MaxHeight;
            t.UpdateLayout();
            t.Measure(new Size(availableSize.Height, availableSize.Width));
            return new Size(t.DesiredSize.Height, t.DesiredSize.Width);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
                Rect arrangeRect = new Rect();
                arrangeRect.X = 0;
                arrangeRect.Y = 0;
                arrangeRect.Width = finalSize.Height;
                arrangeRect.Height = finalSize.Width;
                Content.Arrange(arrangeRect);
                
                RotateTransform rotateTransform = new RotateTransform
                {
                    Angle = -90,
                };

                TranslateTransform translateTransform = new TranslateTransform
                {
                    X = 0,
                    Y = finalSize.Height
                };

                TransformGroup transformGroup = new TransformGroup();
                transformGroup.Children.Add(rotateTransform);
                transformGroup.Children.Add(translateTransform);
                Content.RenderTransform = transformGroup;

            return finalSize;
        }
    }
}
