﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TechnologyDataLibrary;
using System.IO;

namespace TechnologyDataFill
{
    class Program
    {
        private static ProcessData PD;

        static void Main(string[] args)
        {
            DateTime? From=null, To=null;

            foreach (string a in args)
            {
                string[] Value = a.Split('=');
                if (Value.Count() == 2)
                {
                    switch(Value[0].ToLower().Trim())
                    {
                        case "from":
                            From = DateTime.Parse(Value[1]);
                            break;
                        case "to":
                            To = DateTime.Parse(Value[1]);
                            break;
                        default:
                            break;
                    }
                }
            }
            PD = new ProcessData();
            PD.NewMessage += new ProcessData.NewMessageDelegate(PD_NewMessage);
            PD.Process(From, To, ConfigurationManager.ConnectionStrings["raportConnectionString"].ConnectionString, false, null);
        }

        static void PD_NewMessage(object o, NewMessageEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
