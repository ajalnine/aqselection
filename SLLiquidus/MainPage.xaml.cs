﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Threading;
using AQConstructorsLibrary;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using ApplicationCore;
using AQCalculationsLibrary;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.CalculationServiceReference;
using OpenRiaServices.Controls;
using OpenRiaServices.DomainServices;
using AQSelection;
using AQStepFactoryLibrary;


namespace SLLiquidus
{
    public partial class MainPage : IAQModule
    {
        CommandCallBackDelegate ReadySignalCallBack;
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private List<SLDataTable> _tableResults;
        private BackgroundWorker _calculationWorker;
        private readonly DispatcherTimer _dt;
        private ReportingDuplexServiceClient _rdsc;
        private Liquidus _currentLiquidus;
        private Filters _currentFilters;
        private string _currentMode = "Single";
        private readonly SelectionLiquidusNTDParametersList _liquidusParametersNTD = new SelectionLiquidusNTDParametersList();
        private readonly SelectionLiquidusChemistryParametersList _liquidusParametersChemistry = new SelectionLiquidusChemistryParametersList();
        private readonly AQModuleDescription _aqmd;
        public InteractiveParameters P
        {
            get { return (InteractiveParameters)GetValue(PProperty); }
            set { SetValue(PProperty, value); }
        }

        public static readonly DependencyProperty PProperty =
            DependencyProperty.Register("P", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            P = InteractiveParameters.GetClone(Resources["ChemistryParameters1"] as InteractiveParameters);
            ColumnsPresenter.SelectionParameters = _liquidusParametersNTD;
            InitCalculationWorker();
            ChemistryRadioButton.IsEnabled = Security.IsInRole("Выборки");
            SetCoefficients.IsEnabled = Security.IsInRole("Правка_параметров");
            XMLPresenter.Hide();
            foreach (var a in P.Children)
            {
                a.PropertyChanged += ParametersChanged;
            }

            _dt = new DispatcherTimer {Interval = new TimeSpan(100)};
            _dt.Tick += (o, e) =>
            {
                if (!_calculationWorker.IsBusy)
                {
                    _calculationWorker.RunWorkerAsync(new object[] { P});
                    _dt.Stop();
                }
            };
            DoCalculation();
        }

        void ParametersChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Contains("Value"))
            {
                if (_currentMode == "Single") DoCalculation();
                else
                {
                    if (ColumnsPresenter.IsFinished)RefreshQuery.Visibility = Visibility.Visible;
                    XMLPresenter.Hide();
                } 
            }
        }

        #region Расчет
        private void DoCalculation()
        {
            if (_calculationWorker == null) return;
            if (_calculationWorker.IsBusy)
            {
                _calculationWorker.CancelAsync();
                _dt.Start();
            }
            else _calculationWorker.RunWorkerAsync(new object[] { P });
        }

        private void InitCalculationWorker()
        {
            _calculationWorker = new BackgroundWorker();

            _calculationWorker.DoWork += (o, e) =>
            {
                Dispatcher.BeginInvoke(
                    delegate {ExportToXlsxEx.IsEnabled = false;
                                 CalcToConstructor.IsEnabled = false;
                    });
                var objects = e.Argument as object[];
                if (objects == null) return;
                var parameters = InteractiveParameters.GetClone(objects[0] as InteractiveParameters);
                var l = _currentLiquidus;
                if (l == null) return;
                var tmax = l.Tref
                           - (l.C ?? 0) * parameters.GetValue("C")
                           - (l.Mn ?? 0) * parameters.GetValue("Mn")
                           - (l.Si ?? 0) * parameters.GetValue("Si")
                           - (l.S ?? 0) * parameters.GetValue("S")
                           - (l.P ?? 0) * parameters.GetValue("P")
                           - (l.Cr ?? 0) * parameters.GetValue("Cr")
                           - (l.Ni ?? 0) * parameters.GetValue("Ni")
                           - (l.Mo ?? 0) * parameters.GetValue("Mo")
                           - (l.Cu ?? 0) * parameters.GetValue("Cu")
                           - (l.V ?? 0) * parameters.GetValue("V")
                           - (l.Al ?? 0) * parameters.GetValue("Al")
                           - (l.W ?? 0) * parameters.GetValue("W")
                           - (l.Nb ?? 0) * parameters.GetValue("Nb")
                           - (l.N ?? 0) * parameters.GetValue("N")
                           - (l.Co ?? 0) * parameters.GetValue("Co");
                var tmin = l.Tref
                           - (l.C ?? 0) * parameters.GetValue2("C")
                           - (l.Mn ?? 0) * parameters.GetValue2("Mn")
                           - (l.Si ?? 0) * parameters.GetValue2("Si")
                           - (l.S ?? 0) * parameters.GetValue2("S")
                           - (l.P ?? 0) * parameters.GetValue2("P")
                           - (l.Cr ?? 0) * parameters.GetValue2("Cr")
                           - (l.Ni ?? 0) * parameters.GetValue2("Ni")
                           - (l.Mo ?? 0) * parameters.GetValue2("Mo")
                           - (l.Cu ?? 0) * parameters.GetValue2("Cu")
                           - (l.V ?? 0) * parameters.GetValue2("V")
                           - (l.Al ?? 0) * parameters.GetValue2("Al")
                           - (l.W ?? 0) * parameters.GetValue2("W")
                           - (l.Nb ?? 0) * parameters.GetValue2("Nb")
                           - (l.N ?? 0) * parameters.GetValue2("N")
                           - (l.Co ?? 0) * parameters.GetValue2("Co");

                _tableResults = new List<SLDataTable>{new SLDataTable
                {
                    TableName = "Ликвидус",
                    ColumnNames = new List<string>{"Состав", "C", "Mn", "Si", "S", "P", "Cr", "Ni", "Mo", "Cu", "V", "Al", "W", "Nb", "N", "Co", "t ликв."},
                    DataTypes = new List<string>{"String", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double", "Double"},
                    Table = new List<SLDataRow>
                    {
                        new SLDataRow
                        {
                            Row = new List<object>
                            {
                                "Min", parameters.GetValue("C"), parameters.GetValue("Mn"), parameters.GetValue("Si"), parameters.GetValue("S"), parameters.GetValue("P"), parameters.GetValue("Cr")
                                , parameters.GetValue("Ni"), parameters.GetValue("Mo"), parameters.GetValue("Cu"), parameters.GetValue("V"), parameters.GetValue("Al"), parameters.GetValue("W"), parameters.GetValue("Nb"), parameters.GetValue("N"), parameters.GetValue("Co"), tmax
                            }
                        },
                        new SLDataRow
                        {
                            Row = new List<object>
                            {    "Max", parameters.GetValue2("C"), parameters.GetValue2("Mn"), parameters.GetValue2("Si"), parameters.GetValue2("S"), parameters.GetValue2("P"), parameters.GetValue2("Cr")
                                , parameters.GetValue2("Ni"), parameters.GetValue2("Mo"), parameters.GetValue2("Cu"), parameters.GetValue2("V"), parameters.GetValue2("Al"), parameters.GetValue2("W"), parameters.GetValue2("Nb"), parameters.GetValue2("N"), parameters.GetValue2("Co"), tmin
                            }
                        },
                        new SLDataRow
                        {
                            Row = new List<object>
                            {    "Среднее", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (tmin + tmax)/2
                            }
                        }
                    }
                }};
            };

            _calculationWorker.RunWorkerCompleted += (o, e) => Dispatcher.BeginInvoke(delegate
            {
                ExportToXlsxEx.IsEnabled = true;
                CalcToConstructor.IsEnabled = true;
                XMLPresenter.DirectLoadData(_tableResults);
            });

            _calculationWorker.WorkerSupportsCancellation = true;
        }

        #endregion
        private void ExportToXlsxEx_Click(object sender, RoutedEventArgs e)
        {
            if (_tableResults == null) return;
            var task = new Task(new[] { "Формирование результата", "Формирование файла Xlsx" }, "Экспорт в Xlsx", TaskPanel);
            task.StartTask();

            var task1Guid = Guid.NewGuid();
            var task2Guid = Guid.NewGuid();
            _rdsc = Services.GetReportingService();
            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived").Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid).Subscribe(x =>
            {
                OperationResult sr = x.EventArgs.or;
                if (sr.Success)
                {
                    task.AdvanceProgress();
                    _rdsc.GetAllResultsInXlsxAsync(task2Guid, (string)sr.ResultData);
                }
                else
                {
                    task.StopTask(sr.Message);
                    if (sr.ResultData != null) _rdsc.DropDataAsync((Guid)sr.ResultData);
                }
            });

            Observable.FromEvent<ReceiveReceivedEventArgs>(_rdsc, "ReceiveReceived").Where((o1, e1) => o1.EventArgs.OperationGuid == task2Guid).Subscribe(x =>
            {
                var sr = x.EventArgs.or;
                if (sr.Success)
                {
                    task.AdvanceProgress();
                    Dispatcher.BeginInvoke(() => HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.ResultData.ToString()) + "&FName=" + HttpUtility.UrlEncode(string.Format("Расчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null))) + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel"))));
                }
                else
                {
                    task.StopTask(sr.Message);
                    if (sr.ResultData != null) _rdsc.DropDataAsync((Guid)sr.ResultData);
                }
            });

            var r = new List<ReportDataTable>();

            foreach (var a in _tableResults)
            {
                var rdt = new ReportDataTable {ColumnNames = a.ColumnNames, DataTypes = new List<string>()};
                foreach (var d in a.DataTypes)
                {
                    rdt.DataTypes.Add(d != "InputParameter" ? d : "Double");
                }
                rdt.SelectionString = a.SelectionString;
                rdt.Table = new List<ReportDataRow>();
                foreach (var dr in a.Table)
                {
                    var rdr = new ReportDataRow {Row = new List<object>()};
                    for (int index = 0; index < dr.Row.Count; index++)
                    {
                        object b = dr.Row[index];
                        if (b != null && b.GetType().ToString() == "AQCalculationsLibrary.InputParameter")
                        {
                            var i = ((InputParameter)b);
                            string name = rdt.ColumnNames[index];
                            bool isMax = name.Contains("Max");
                            rdr.Row.Add((isMax) ? i.CurrentValue2 : i.CurrentValue);
                        }
                        else rdr.Row.Add(b);
                    }
                    rdt.Table.Add(rdr);
                }
                rdt.TableName = a.TableName;
                r.Add(rdt);
            }
            _rdsc.FillDataWithSLTablesAsync(task1Guid, r);
        }

        #region Обработка сигналов

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }

        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }

        #endregion

        #region Обработка смены режима

        private void MethodIsChanged(object sender, RoutedEventArgs e)
        {
            var rb = sender as RadioButton;
            if (rb == null) return;
            _currentMode = rb.Tag.ToString();
            ChangeMode();
        }

        private void ChangeMode()
        {
            RefreshQuery.Visibility = Visibility.Collapsed;
            switch (_currentMode)
            {
                case "Single":
                    DoCalculation();
                    ColumnsPresenter.Visibility = Visibility.Collapsed;
                    UseInput.Visibility = Visibility.Collapsed;
                    break;

                case "Reference":
                    ColumnsPresenter.SelectionParameters = _liquidusParametersNTD;
                    ColumnsPresenter.Visibility = Visibility.Visible;
                    UseInput.Visibility = Visibility.Visible;
                    XMLPresenter.Hide();
                    ExportToXlsxEx.IsEnabled = false;
                    CalcToConstructor.IsEnabled = false;
                    break;

                case "Chemistry":
                    ColumnsPresenter.SelectionParameters = _liquidusParametersChemistry;
                    ColumnsPresenter.Visibility = Visibility.Visible;
                    UseInput.Visibility = Visibility.Visible;
                    XMLPresenter.Hide();
                    ExportToXlsxEx.IsEnabled = false;
                    CalcToConstructor.IsEnabled = false;
                    break;
            }
        }

        #endregion

        #region Редактор методов
        private void LiquidusDomainDataSource_OnLoadedData(object sender, LoadedDataEventArgs e)
        {
            ConstructMethodPanel();
            DoCalculation();
        }

        private void ConstructMethodPanel()
        {
            RadioButtonsPanel.Children.Clear();
            var isFirst = true;
            foreach (var l in LiquidusDomainDataSource.DataView.Cast<Liquidus>())
            {
                var rb = new RadioButton
                {
                    Style = Resources["RadioButtonBorderStyle"] as Style,
                    Width = 200,
                    Height = 18,
                    Content =
                        new TextBlock
                        {
                            Text = l.Name,
                            HorizontalAlignment = HorizontalAlignment.Left,
                            VerticalAlignment = VerticalAlignment.Center
                        },
                    IsChecked = isFirst,
                    Tag = l.Name,
                    GroupName = "MethodSelectorGroup",
                };
                if (isFirst) SetCurrentMethod(l.Name);
                rb.Click += RbOnClick;
                RadioButtonsPanel.Children.Add(rb);
                isFirst = false;
            }
        }

        private void RbOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            RefreshQuery.Visibility = Visibility.Collapsed; 
            var rb = sender as RadioButton;
            if (rb == null) return;
            var method = rb.Tag.ToString();
            SetCurrentMethod(method);
            if (_currentMode == "Single") DoCalculation();
            else
            {
                if (!ColumnsPresenter.IsFinished)XMLPresenter.Hide();
                else DoSQLQuery();
            }
        }

        private void SetCurrentMethod(string method)
        {
            _currentLiquidus = LiquidusDomainDataSource.DataView.Cast<Liquidus>().SingleOrDefault(a => a.Name == method);
        }

        private void DomainDataSource_OnSubmittedChanges(object sender, SubmittedChangesEventArgs e)
        {
            ConstructMethodPanel();
            DoCalculation();
        }

        private void LiquidusDataGrid_OnLoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1 + " ";
        }

        private void EditCoefficients_OnClick(object sender, RoutedEventArgs e)
        {
            LiquidusDataGrid.IsEnabled = false;
            NewCoefficientsButton.IsEnabled = false;
            _currentLiquidus = LiquidusDomainDataSource.DataView.Cast<Liquidus>().SingleOrDefault(i => i.id == ((Byte)((TextImageButtonBase)sender).Tag));
            EditCoefficientsWindow.Visibility = Visibility.Visible;
        }

        private void DeleteCoefficients_OnClick(object sender, RoutedEventArgs e)
        {
            var ci = (Byte)((TextImageButtonBase)sender).Tag;
            var toDelete = LiquidusDomainDataSource.DataView.Cast<Liquidus>().SingleOrDefault(i => i.id == ci);
            if (toDelete == null) return;
            var mbr = MessageBox.Show("Удалить методику " + toDelete.Name + "?", "Удаление методики", MessageBoxButton.OKCancel);
            if (mbr != MessageBoxResult.OK) return;
            LiquidusDomainDataSource.DataView.Remove(toDelete);
            LiquidusDomainDataSource.SubmitChanges();
            _currentLiquidus = null;
        }

        private void NewCoefficientsButton_OnClick(object sender, RoutedEventArgs e)
        {
            EditCoefficientsWindow.Visibility = Visibility.Visible;
            LiquidusDataGrid.IsEnabled = false;
            NewCoefficientsButton.IsEnabled = false;
            var newLiquidus = new Liquidus();
            LiquidusDomainDataSource.DataView.Add(newLiquidus);
            LiquidusDomainDataSource.DataView.MoveCurrentTo(newLiquidus);
            _currentLiquidus = newLiquidus;
            RefreshResults(); 
        }

        private void CancelCoefficientsEdit_OnClick(object sender, RoutedEventArgs e)
        {
            LiquidusDomainDataSource.RejectChanges();
            EditCoefficientsWindow.Visibility = Visibility.Collapsed;
            LiquidusDataGrid.IsEnabled = true;
            NewCoefficientsButton.IsEnabled = true;
        }

        private void SaveCoefficientsEdit_OnClick(object sender, RoutedEventArgs e)
        {
            var name= _currentLiquidus.Name; 
            if (String.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Поле 'Наименование' не заполнено");
                return;
            }
            if (_currentLiquidus.Tref==null)
            {
                MessageBox.Show("Поле 'Треф' не заполнено");
                return;
            }
            NewCoefficientsButton.IsEnabled = true;
            EditCoefficientsWindow.Visibility = Visibility.Collapsed;
            LiquidusDataGrid.IsEnabled = true;
            LiquidusDataGrid.ScrollIntoView(LiquidusDomainDataSource.DataView[0], null);
            LiquidusDomainDataSource.SubmitChanges();

            RefreshResults(); 
        }

        private void RefreshResults()
        {
            if (_currentMode == "Single") DoCalculation();
            else
            {
                if (ColumnsPresenter.IsFinished) DoSQLQuery();
            }
        }

        #endregion

        private void RefreshFilters()
        {
            if (ColumnsPresenter == null) return;
            _currentFilters = ColumnsPresenter.GetFilters();
        }

        private void ColumnsPresenter_OnConstructionNotCompleted(object o, ConstructionNotCompletedEventArgs e)
        {
             XMLPresenter.Hide();
             RefreshQuery.Visibility = Visibility.Collapsed;
             ExportToXlsxEx.IsEnabled = false;
             CalcToConstructor.IsEnabled = false;
        }

        private void ColumnsPresenter_OnConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            DoSQLQuery();
        }

        private void DoSQLQuery()
        {
            RefreshFilters();
            ColumnsPresenter.EnableFinishSelection(false);
            FillResults(ConstructSQL(true));
        }

        private string ConstructSQL(bool forWork)
        {
            if (forWork)
            return _currentMode == "Reference"
                ? _liquidusParametersNTD.GetSelectForNTD(_currentFilters.ChemistryFilter, _currentLiquidus.Name, P, UseInput.IsChecked != null && UseInput.IsChecked.Value)
                : _liquidusParametersChemistry.GetSelectForChemistry(_currentFilters.ChemistryWorkFilter, _currentLiquidus.Name, P, UseInput.IsChecked != null && UseInput.IsChecked.Value);
            return _currentMode == "Reference"
                ? _liquidusParametersNTD.GetSelectForNTD(_currentFilters.ChemistryFilter, _currentLiquidus.Name, P, UseInput.IsChecked != null && UseInput.IsChecked.Value)
                : _liquidusParametersChemistry.GetSelectForChemistry(_currentFilters.ChemistrySaveFilter, _currentLiquidus.Name, P, UseInput.IsChecked != null && UseInput.IsChecked.Value);

        }


        private void FillResults(string sql)
        {
            if (_tableResults == null) return;
            var task = new Task(new[] { "Расчет"}, "Просмотр результата", TaskPanel);
            task.StartTask();

            var cs = Services.GetCalculationService();

            cs.BeginFillDataWithSQL(sql, DateTime.Now, DateTime.Now, iar =>
            {
                var result = cs.EndFillDataWithSQL(iar);
                if (result.Success)
                {
                    Dispatcher.BeginInvoke(() => cs.BeginGetTablesSLCompatible(result.TaskDataGuid, iar2 => Dispatcher.BeginInvoke(() =>
                    {
                        _tableResults = cs.EndGetTablesSLCompatible(iar2);
                        XMLPresenter.DirectLoadData(_tableResults);
                        task.AdvanceProgress();
                        ColumnsPresenter.EnableFinishSelection(true);
                        ExportToXlsxEx.IsEnabled = true;
                        CalcToConstructor.IsEnabled = true;

                    }), null));
                }
                else
                {
                    Dispatcher.BeginInvoke(() => 
                    {
                        task.SetState(1, TaskState.Error);
                        task.SetMessage(result.Message);
                        ColumnsPresenter.EnableFinishSelection(true);
                    });
                }
            }, null);
        }

        private void XMLPresenter_OnDataLoaded(object sender, TaskStateEventArgs e)
        {
            
        }

        private void XMLPresenter_OnDataProcessed(object sender, TaskStateEventArgs e)
        {
            
        }

        private void ToCalcButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentMode == "Single")
            {
                var signalArguments = new Dictionary<string, object> {{"Table", _tableResults[0]}};
                Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(),
                    Command.InsertTable, signalArguments, null);
            }
            else
            {
                var signalArguments = new Dictionary<string, object> { { "Calculation", StepFactory.CreateSQLQuery(ConstructSQL(false), "Ликвидус") } };
                Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(),
                       Command.InsertCalculation, signalArguments, null);
            }
        }

        private void UseInput_OnClick(object sender, RoutedEventArgs e)
        {
            if (!ColumnsPresenter.IsFinished) XMLPresenter.Hide();
            else DoSQLQuery();
        }

        private void RefreshQuery_OnClick(object sender, RoutedEventArgs e)
        {
            RefreshQuery.Visibility = Visibility.Collapsed;
            DoSQLQuery();
        }
    }

    #region Конвертеры

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class MinValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class MinAllowedValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var p = (InputParameter)value;
            return p.MinAllowedValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class MaxAllowedValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var p = (InputParameter)value;
            return p.MaxAllowedValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class InRangeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = (bool)value;
            Color c = v ? Colors.Black : Colors.Red;
            return new SolidColorBrush(c);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    #endregion
}