﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using AQConstructorsLibrary;

namespace SLLiquidus
{
    public partial class SelectionLiquidusNTDParametersList : SelectionParametersList
    {
        public SelectionLiquidusNTDParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            DateField = null;
            var spd1 = new SelectionParameterDescription()
            {
                ID = 1,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark AS M 
                                INNER JOIN 
                                (
	                                select distinct MarkID from Requirements @PreviousFilter  and paramtypeid=722 group by MarkID
                                ) as t1 on t1.MarkID=M.id 
                                where @AdditionalFilter
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "Requirements.MarkID",
                 LastOnly = false,
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription()
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                 select distinct NtdID from Requirements @PreviousFilter  and paramtypeid=722 group by NtdID
                                ) as t1 on t1.NtdID = G.id 
                                where @AdditionalFilter 
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "Requirements.NtdID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
        }
    }
}
