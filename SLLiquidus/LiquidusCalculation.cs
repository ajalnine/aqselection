﻿using System.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using System.Windows.Threading;
using AQConstructorsLibrary;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using ApplicationCore;
using AQCalculationsLibrary;
using AQMathClasses;
using ApplicationCore.AQServiceReference;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLLiquidus
{
    public class ChemistrySet
    {
        public double C { get; set; }
        public double Si { get; set; }
        public double Mn { get; set; }
        public double P { get; set; }
        public double S { get; set; }
        public double Cr { get; set; }
        public double Mo { get; set; }
        public double Ni { get; set; }
        public double Cu { get; set; }
        public double V { get; set; }
        public double Al { get; set; }
        public double W { get; set; }
        public double Nb { get; set; }
        public double N { get; set; }
        public double Co { get; set; }
    }
}
