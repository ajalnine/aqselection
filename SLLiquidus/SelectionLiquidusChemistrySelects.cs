﻿using System;
using System.Globalization;
using ApplicationCore;
using AQCalculationsLibrary;
using AQConstructorsLibrary;

namespace SLLiquidus
{
    public partial class SelectionLiquidusChemistryParametersList : SelectionParametersList
    {
        public string GetSelectForChemistry(string sqlFilter, string methodName, InteractiveParameters p, bool useInteractive)
        {
            var c = useInteractive ?  p.GetValue("C").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var mn = useInteractive ? p.GetValue("Mn").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var si = useInteractive ? p.GetValue("Si").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var p_ = useInteractive ? p.GetValue("P").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var s = useInteractive ? p.GetValue("S").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var ni = useInteractive ? p.GetValue("Ni").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var cr = useInteractive ? p.GetValue("Cr").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var mo = useInteractive ? p.GetValue("Mo").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var cu = useInteractive ? p.GetValue("Cu").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var ti = useInteractive ? p.GetValue("Ti").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var v = useInteractive ? p.GetValue("V").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var al = useInteractive ? p.GetValue("Al").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var w = useInteractive ? p.GetValue("W").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var nb = useInteractive ? p.GetValue("Nb").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var n = useInteractive ? p.GetValue("N").ToString(CultureInfo.InvariantCulture) : " 0 ";
            var co = useInteractive ? p.GetValue("Co").ToString(CultureInfo.InvariantCulture) : " 0 ";

            var c2 = useInteractive ? p.GetValue("C").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var mn2 = useInteractive ? p.GetValue("Mn").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var si2 = useInteractive ? p.GetValue("Si").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var p2 = useInteractive ? p.GetValue("P").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var s2 = useInteractive ? p.GetValue("S").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var ni2 = useInteractive ? p.GetValue("Ni").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var cr2 = useInteractive ? p.GetValue("Cr").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var mo2 = useInteractive ? p.GetValue("Mo").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var cu2 = useInteractive ? p.GetValue("Cu").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var ti2 = useInteractive ? p.GetValue("Ti").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var v2 = useInteractive ? p.GetValue("V").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var al2 = useInteractive ? p.GetValue("Al").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var w2 = useInteractive ? p.GetValue("W").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var nb2 = useInteractive ? p.GetValue("Nb").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var n2 = useInteractive ? p.GetValue("N").ToString(CultureInfo.InvariantCulture) : " '-' ";
            var co2 = useInteractive ? p.GetValue("Co").ToString(CultureInfo.InvariantCulture) : " '-' ";
            
            string sql = String.Format(@"
                    ;
                    WITH marklimits ([Плавка], [Дата], [Марка], [НТД], [Проба], [Тип], C, Mn, Si, P, S, Ni, Cr, Mo, Cu, Ti, V, Al, W, Nb, N, Co)
                    AS
                    (
                    SELECT
                    melt as 'Плавка'
                    ,test_date as 'Дата'
                    ,Common_Mark.Mark as 'Марка'
                    ,ntd.ntd as 'НТД'
                    ,OEL_Probes.DisplayProbeName as 'Проба'
                    ,OEL_ProbeTypes.Type as 'Тип испытания'
                    ,MIN(case when cp.DisplayParameterName = 'C' then [raport].[dbo].[OEL_Elements].value else null end) as 'C'
                    ,MIN(case when cp.DisplayParameterName = 'Mn' then [raport].[dbo].[OEL_Elements].value else null end) as 'Mn min'
                    ,MIN(case when cp.DisplayParameterName = 'Si' then [raport].[dbo].[OEL_Elements].value else null end) as 'Si min'
                    ,MIN(case when cp.DisplayParameterName = 'P' then [raport].[dbo].[OEL_Elements].value else null end) as 'P min'
                    ,MIN(case when cp.DisplayParameterName = 'S' then [raport].[dbo].[OEL_Elements].value else null end) as 'S min'
                    ,MIN(case when cp.DisplayParameterName = 'Ni' then [raport].[dbo].[OEL_Elements].value else null end) as 'Ni min'
                    ,MIN(case when cp.DisplayParameterName = 'Cr' then [raport].[dbo].[OEL_Elements].value else null end) as 'Cr min'
                    ,MIN(case when cp.DisplayParameterName = 'Mo' then [raport].[dbo].[OEL_Elements].value else null end) as 'Mo min'
                    ,MIN(case when cp.DisplayParameterName = 'Cu' then [raport].[dbo].[OEL_Elements].value else null end) as 'Cu min'
                    ,MIN(case when cp.DisplayParameterName = 'Ti' then [raport].[dbo].[OEL_Elements].value else null end) as 'Ti min'
                    ,MIN(case when cp.DisplayParameterName = 'V' then [raport].[dbo].[OEL_Elements].value else null end) as 'V min'
                    ,MIN(case when cp.DisplayParameterName = 'Al' then [raport].[dbo].[OEL_Elements].value else null end) as 'Al min'
                    ,MIN(case when cp.DisplayParameterName = 'W' then [raport].[dbo].[OEL_Elements].value else null end) as 'W min'
                    ,MIN(case when cp.DisplayParameterName = 'Nb' then [raport].[dbo].[OEL_Elements].value else null end) as 'Nb min'
                    ,MIN(case when cp.DisplayParameterName = 'N' then [raport].[dbo].[OEL_Elements].value else null end) as 'N min'
                    ,MIN(case when cp.DisplayParameterName = 'Co' then [raport].[dbo].[OEL_Elements].value else null end) as 'Co min'
                    FROM [raport].[dbo].[OEL_Head]
                    inner join [raport].[dbo].[OEL_Elements] on [raport].[dbo].[OEL_Elements].id_test = [raport].[dbo].[OEL_Head].id_test
                    inner join [raport].[dbo].[OEL_Probes] on [raport].[dbo].[OEL_Head].id_probe = [raport].[dbo].[OEL_Probes].id
                    inner join [raport].[dbo].[OEL_ProbeTypes] on [raport].[dbo].[OEL_Head].type = [raport].[dbo].[OEL_ProbeTypes].id
                    inner join Common_Parameters as cp on cp.id = [raport].[dbo].[OEL_Elements].id_Parameter
                    inner join Common_Ntd as ntd on ntd.id = [raport].[dbo].[OEL_Head].id_ntd
                    inner join Common_Mark on Common_Mark.id = [raport].[dbo].[OEL_Head].id_cmark
                    where " + Security.GetMarkSQLCheck("Контроль") + @" and ({0})
                    group by melt, test_date, Common_Mark.Mark, ntd.ntd, OEL_Probes.DisplayProbeName, OEL_ProbeTypes.Type, onrs
                    )

                    select 
                    [Плавка], [Дата], [Марка], [НТД], [Проба], [Тип]
                    ,isnull (replace(m.c, ',','.'), {2}) as C
                    ,isnull (replace(m.si, ',','.'), {3}) as Si
                    ,isnull (replace(m.mn, ',','.'), {4}) as Mn
                    ,isnull (replace(m.cr, ',','.'), {5}) as Cr
                    ,isnull (replace(m.ni, ',','.'), {6}) as Ni
                    ,isnull (replace(m.cu, ',','.'), {7}) as Cu
                    ,isnull (replace(m.p, ',','.'), {8}) as P
                    ,isnull (replace(m.s, ',','.'), {9}) as S
                    ,isnull (replace(m.al, ',','.'), {10}) as Al
                    ,isnull (replace(m.mo, ',','.'), {11}) as Mo
                    ,isnull (replace(m.v, ',','.'), {12}) as V
                    ,isnull (replace(m.w, ',','.'), {13}) as W
                    ,isnull (replace(m.ti, ',','.'), {14}) as Ti
                    ,isnull (replace(m.nb, ',','.'), {15}) as Nb
                    ,isnull (replace(m.n, ',','.'), {16}) as N
                    ,isnull (replace(m.co, ',','.'), {17}) as Co
                    , Tref
                    - isnull(dbo.ToNumeric(m.c), {18}) * isnull(l.c, 0)
                    - isnull(dbo.ToNumeric(m.si), {19}) * isnull(l.si, 0)
                    - isnull(dbo.ToNumeric(m.mn), {20}) * isnull(l.mn, 0)
                    - isnull(dbo.ToNumeric(m.cr), {21}) * isnull(l.cr, 0)
                    - isnull(dbo.ToNumeric(m.ni), {22}) * isnull(l.ni, 0)
                    - isnull(dbo.ToNumeric(m.cu), {23}) * isnull(l.cu, 0)
                    - isnull(dbo.ToNumeric(m.p), {24}) * isnull(l.p, 0)
                    - isnull(dbo.ToNumeric(m.s), {25}) * isnull(l.s, 0)
                    - isnull(dbo.ToNumeric(m.al), {26}) * isnull(l.al, 0)
                    - isnull(dbo.ToNumeric(m.mo), {27}) * isnull(l.mo, 0)
                    - isnull(dbo.ToNumeric(m.v), {28}) * isnull(l.v, 0)
                    - isnull(dbo.ToNumeric(m.w), {29}) * isnull(l.w, 0)
                    - isnull(dbo.ToNumeric(m.ti), {30}) * isnull(l.ti, 0)
                    - isnull(dbo.ToNumeric(m.nb), {31}) * isnull(l.nb, 0)
                    - isnull(dbo.ToNumeric(m.n), {32}) * isnull(l.n, 0)
                    - isnull(dbo.ToNumeric(m.co), {33}) * isnull(l.co, 0) as 'T ликв.'
                    from marklimits as m cross join Liquidus as l where l.Name='{1}' and m.c is not null
                    order by  [Плавка], [Дата] ", sqlFilter, methodName
                                                           , c2.ToString(CultureInfo.InvariantCulture), si2.ToString(CultureInfo.InvariantCulture), mn2.ToString(CultureInfo.InvariantCulture), cr2.ToString(CultureInfo.InvariantCulture), ni2.ToString(CultureInfo.InvariantCulture), cu2.ToString(CultureInfo.InvariantCulture), p2.ToString(CultureInfo.InvariantCulture), s2.ToString(CultureInfo.InvariantCulture), al2.ToString(CultureInfo.InvariantCulture), mo2.ToString(CultureInfo.InvariantCulture), v2.ToString(CultureInfo.InvariantCulture), w2.ToString(CultureInfo.InvariantCulture), ti2.ToString(CultureInfo.InvariantCulture), nb2.ToString(CultureInfo.InvariantCulture), n2.ToString(CultureInfo.InvariantCulture), co2.ToString(CultureInfo.InvariantCulture)
                                                           , c.ToString(CultureInfo.InvariantCulture), si.ToString(CultureInfo.InvariantCulture), mn.ToString(CultureInfo.InvariantCulture), cr.ToString(CultureInfo.InvariantCulture), ni.ToString(CultureInfo.InvariantCulture), cu.ToString(CultureInfo.InvariantCulture), p_.ToString(CultureInfo.InvariantCulture), s.ToString(CultureInfo.InvariantCulture), al.ToString(CultureInfo.InvariantCulture), mo.ToString(CultureInfo.InvariantCulture), v.ToString(CultureInfo.InvariantCulture), w.ToString(CultureInfo.InvariantCulture), ti.ToString(CultureInfo.InvariantCulture), nb.ToString(CultureInfo.InvariantCulture), n.ToString(CultureInfo.InvariantCulture), co.ToString(CultureInfo.InvariantCulture)
                                                           );
            return sql;
        }
    }
}