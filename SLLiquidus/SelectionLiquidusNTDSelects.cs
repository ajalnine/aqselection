﻿using System;
using System.Globalization;
using AQCalculationsLibrary;
using AQConstructorsLibrary;

namespace SLLiquidus
{
    public partial class SelectionLiquidusNTDParametersList : SelectionParametersList
    {
        public string GetSelectForNTD(string sqlFilter, string methodName, InteractiveParameters p, bool useInteractive)
        {

            var c = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("C").ToString(CultureInfo.InvariantCulture), p.GetValue2("C").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var mn = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Mn").ToString(CultureInfo.InvariantCulture), p.GetValue2("Mn").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var si = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Si").ToString(CultureInfo.InvariantCulture), p.GetValue2("Si").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var p_ = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("P").ToString(CultureInfo.InvariantCulture), p.GetValue2("P").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var s = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("S").ToString(CultureInfo.InvariantCulture), p.GetValue2("S").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var ni = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Ni").ToString(CultureInfo.InvariantCulture), p.GetValue2("Ni").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var cr = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Cr").ToString(CultureInfo.InvariantCulture), p.GetValue2("Cr").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var mo = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Mo").ToString(CultureInfo.InvariantCulture), p.GetValue2("Mo").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var cu = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Cu").ToString(CultureInfo.InvariantCulture), p.GetValue2("Cu").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var ti = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Ti").ToString(CultureInfo.InvariantCulture), p.GetValue2("Ti").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var v = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("V").ToString(CultureInfo.InvariantCulture), p.GetValue2("V").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var al = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Al").ToString(CultureInfo.InvariantCulture), p.GetValue2("Al").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var w = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("W").ToString(CultureInfo.InvariantCulture), p.GetValue2("W").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var nb = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Nb").ToString(CultureInfo.InvariantCulture), p.GetValue2("Nb").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var n = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("N").ToString(CultureInfo.InvariantCulture), p.GetValue2("N").ToString(CultureInfo.InvariantCulture)) : " 0 ";
            var co = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Co").ToString(CultureInfo.InvariantCulture), p.GetValue2("Co").ToString(CultureInfo.InvariantCulture)) : " 0 ";

            var c2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("C").ToString(CultureInfo.InvariantCulture), p.GetValue2("C").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var mn2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Mn").ToString(CultureInfo.InvariantCulture), p.GetValue2("Mn").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var si2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Si").ToString(CultureInfo.InvariantCulture), p.GetValue2("Si").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var p2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("P").ToString(CultureInfo.InvariantCulture), p.GetValue2("P").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var s2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("S").ToString(CultureInfo.InvariantCulture), p.GetValue2("S").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var ni2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Ni").ToString(CultureInfo.InvariantCulture), p.GetValue2("Ni").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var cr2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Cr").ToString(CultureInfo.InvariantCulture), p.GetValue2("Cr").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var mo2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Mo").ToString(CultureInfo.InvariantCulture), p.GetValue2("Mo").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var cu2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Cu").ToString(CultureInfo.InvariantCulture), p.GetValue2("Cu").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var ti2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Ti").ToString(CultureInfo.InvariantCulture), p.GetValue2("Ti").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var v2 = useInteractive ? String.Format(" case when [предел]='Min' then  {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("V").ToString(CultureInfo.InvariantCulture), p.GetValue2("V").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var al2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Al").ToString(CultureInfo.InvariantCulture), p.GetValue2("Al").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var w2 = useInteractive ? String.Format(" case when [предел]='Min' then  {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("W").ToString(CultureInfo.InvariantCulture), p.GetValue2("W").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var nb2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Nb").ToString(CultureInfo.InvariantCulture), p.GetValue2("Nb").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var n2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ",  p.GetValue("N").ToString(CultureInfo.InvariantCulture), p.GetValue2("N").ToString(CultureInfo.InvariantCulture)) : " '-' ";
            var co2 = useInteractive ? String.Format(" case when [предел]='Min' then {0} when [предел]='Max' then {1} else 0 end ", p.GetValue("Co").ToString(CultureInfo.InvariantCulture), p.GetValue2("Co").ToString(CultureInfo.InvariantCulture)) : " '-' ";

            string sql = String.Format(@"
                    ;
                    WITH marklimits ([Марка], [НТД], [Предел], C, Mn, Si, P, S, Ni, Cr, Mo, Cu, Ti, V, Al, W, Nb, N, Co)
                    AS
                    (
                    SELECT
                    mark.Mark as 'Марка'
                    ,ntd.ntd as 'НТД'
                    ,'Min' as 'Предел'
                    ,MIN(case when cp.DisplayParameterName = 'C' then [raport].[dbo].[Requirements].Min else null end) as 'C min'
                    ,MIN(case when cp.DisplayParameterName = 'Mn' then [raport].[dbo].[Requirements].Min else null end) as 'Mn min'
                    ,MIN(case when cp.DisplayParameterName = 'Si' then [raport].[dbo].[Requirements].Min else null end) as 'Si min'
                    ,MIN(case when cp.DisplayParameterName = 'P' then [raport].[dbo].[Requirements].Min else null end) as 'P min'
                    ,MIN(case when cp.DisplayParameterName = 'S' then [raport].[dbo].[Requirements].Min else null end) as 'S min'
                    ,MIN(case when cp.DisplayParameterName = 'Ni' then [raport].[dbo].[Requirements].Min else null end) as 'Ni min'
                    ,MIN(case when cp.DisplayParameterName = 'Cr' then [raport].[dbo].[Requirements].Min else null end) as 'Cr min'
                    ,MIN(case when cp.DisplayParameterName = 'Mo' then [raport].[dbo].[Requirements].Min else null end) as 'Mo min'
                    ,MIN(case when cp.DisplayParameterName = 'Cu' then [raport].[dbo].[Requirements].Min else null end) as 'Cu min'
                    ,MIN(case when cp.DisplayParameterName = 'Ti' then [raport].[dbo].[Requirements].Min else null end) as 'Ti min'
                    ,MIN(case when cp.DisplayParameterName = 'V' then [raport].[dbo].[Requirements].Min else null end) as 'V min'
                    ,MIN(case when cp.DisplayParameterName = 'Al' then [raport].[dbo].[Requirements].Min else null end) as 'Al min'
                    ,MIN(case when cp.DisplayParameterName = 'W' then [raport].[dbo].[Requirements].Min else null end) as 'W min'
                    ,MIN(case when cp.DisplayParameterName = 'Nb' then [raport].[dbo].[Requirements].Min else null end) as 'Nb min'
                    ,MIN(case when cp.DisplayParameterName = 'N' then [raport].[dbo].[Requirements].Min else null end) as 'N min'
                    ,MIN(case when cp.DisplayParameterName = 'Co' then [raport].[dbo].[Requirements].Min else null end) as 'Co min'
                    FROM [raport].[dbo].[Requirements] 
                    inner join Common_Parameters as cp on cp.id = [raport].[dbo].[Requirements].ParamID
                    inner join Common_Ntd as ntd on ntd.id = [raport].[dbo].[Requirements].NtdID
                    inner join Common_Mark as mark on mark.id = [raport].[dbo].[Requirements].MarkID
                    where ParamTypeID=722 and IsExtended = 0 and IsDeleted = 0 and ({0})

                    group by mark.Mark, ntd.ntd

                    union 

                    SELECT
                    mark.Mark as 'Марка'
                    ,ntd.ntd as 'НТД'
                    ,'Max' as 'Предел'
                    ,MAX(case when cp.DisplayParameterName = 'C' then [raport].[dbo].[Requirements].Max else null end) as 'C max'
                    ,MAX(case when cp.DisplayParameterName = 'Mn' then [raport].[dbo].[Requirements].Max else null end) as 'Mn max'
                    ,MAX(case when cp.DisplayParameterName = 'Si' then [raport].[dbo].[Requirements].Max else null end) as 'Si max'
                    ,MAX(case when cp.DisplayParameterName = 'P' then [raport].[dbo].[Requirements].Max else null end) as 'P max'
                    ,MAX(case when cp.DisplayParameterName = 'S' then [raport].[dbo].[Requirements].Max else null end) as 'S max'
                    ,MAX(case when cp.DisplayParameterName = 'Ni' then [raport].[dbo].[Requirements].Max else null end) as 'Ni max'
                    ,MAX(case when cp.DisplayParameterName = 'Cr' then [raport].[dbo].[Requirements].Max else null end) as 'Cr max'
                    ,MAX(case when cp.DisplayParameterName = 'Mo' then [raport].[dbo].[Requirements].Max else null end) as 'Mo max'
                    ,MAX(case when cp.DisplayParameterName = 'Cu' then [raport].[dbo].[Requirements].Max else null end) as 'Cu max'
                    ,MAX(case when cp.DisplayParameterName = 'Ti' then [raport].[dbo].[Requirements].Max else null end) as 'Ti max'
                    ,MAX(case when cp.DisplayParameterName = 'V' then [raport].[dbo].[Requirements].Max else null end) as 'V max'
                    ,MAX(case when cp.DisplayParameterName = 'Al' then [raport].[dbo].[Requirements].Max else null end) as 'Al max'
                    ,MAX(case when cp.DisplayParameterName = 'W' then [raport].[dbo].[Requirements].Max else null end) as 'W max'
                    ,MAX(case when cp.DisplayParameterName = 'Nb' then [raport].[dbo].[Requirements].Max else null end) as 'Nb max'
                    ,MAX(case when cp.DisplayParameterName = 'N' then [raport].[dbo].[Requirements].Max else null end) as 'N max'
                    ,MAX(case when cp.DisplayParameterName = 'Co' then [raport].[dbo].[Requirements].Max else null end) as 'Co max'
                    FROM [raport].[dbo].[Requirements] 
                    inner join Common_Parameters as cp on cp.id = [raport].[dbo].[Requirements].ParamID
                    inner join Common_Ntd as ntd on ntd.id = [raport].[dbo].[Requirements].NtdID
                    inner join Common_Mark as mark on mark.id = [raport].[dbo].[Requirements].MarkID
                    where ParamTypeID=722 and IsExtended = 0 and IsDeleted = 0 and ({0})

                    group by mark.Mark, ntd.ntd

                    )

                    select 
                    [Марка],[НТД],[Предел]
                    ,isnull (replace(m.c, ',','.'), {2}) as C
                    ,isnull (replace(m.si, ',','.'), {3}) as Si
                    ,isnull (replace(m.mn, ',','.'), {4}) as Mn
                    ,isnull (replace(m.cr, ',','.'), {5}) as Cr
                    ,isnull (replace(m.ni, ',','.'), {6}) as Ni
                    ,isnull (replace(m.cu, ',','.'), {7}) as Cu
                    ,isnull (replace(m.p, ',','.'), {8}) as P
                    ,isnull (replace(m.s, ',','.'), {9}) as S
                    ,isnull (replace(m.al, ',','.'), {10}) as Al
                    ,isnull (replace(m.mo, ',','.'), {11}) as Mo
                    ,isnull (replace(m.v, ',','.'), {12}) as V
                    ,isnull (replace(m.w, ',','.'), {13}) as W
                    ,isnull (replace(m.ti, ',','.'), {14}) as Ti
                    ,isnull (replace(m.nb, ',','.'), {15}) as Nb
                    ,isnull (replace(m.n, ',','.'), {16}) as N
                    ,isnull (replace(m.co, ',','.'), {17}) as Co
                    , Tref
                    - isnull(dbo.ToNumeric(m.c), {18}) * isnull(l.c, 0)
                    - isnull(dbo.ToNumeric(m.si), {19}) * isnull(l.si, 0)
                    - isnull(dbo.ToNumeric(m.mn), {20}) * isnull(l.mn, 0)
                    - isnull(dbo.ToNumeric(m.cr), {21}) * isnull(l.cr, 0)
                    - isnull(dbo.ToNumeric(m.ni), {22}) * isnull(l.ni, 0)
                    - isnull(dbo.ToNumeric(m.cu), {23}) * isnull(l.cu, 0)
                    - isnull(dbo.ToNumeric(m.p), {24}) * isnull(l.p, 0)
                    - isnull(dbo.ToNumeric(m.s), {25}) * isnull(l.s, 0)
                    - isnull(dbo.ToNumeric(m.al), {26}) * isnull(l.al, 0)
                    - isnull(dbo.ToNumeric(m.mo), {27}) * isnull(l.mo, 0)
                    - isnull(dbo.ToNumeric(m.v), {28}) * isnull(l.v, 0)
                    - isnull(dbo.ToNumeric(m.w), {29}) * isnull(l.w, 0)
                    - isnull(dbo.ToNumeric(m.ti), {30}) * isnull(l.ti, 0)
                    - isnull(dbo.ToNumeric(m.nb), {31}) * isnull(l.nb, 0)
                    - isnull(dbo.ToNumeric(m.n), {32}) * isnull(l.n, 0)
                    - isnull(dbo.ToNumeric(m.co), {33}) * isnull(l.co, 0) as 'T ликв.'
                    from marklimits as m cross join Liquidus as l where l.Name='{1}' and m.c is not null
                    order by [Марка], [НТД], [Предел] desc", sqlFilter, methodName
                                                           , c2.ToString(CultureInfo.InvariantCulture), si2.ToString(CultureInfo.InvariantCulture), mn2.ToString(CultureInfo.InvariantCulture), cr2.ToString(CultureInfo.InvariantCulture), ni2.ToString(CultureInfo.InvariantCulture), cu2.ToString(CultureInfo.InvariantCulture), p2.ToString(CultureInfo.InvariantCulture), s2.ToString(CultureInfo.InvariantCulture), al2.ToString(CultureInfo.InvariantCulture), mo2.ToString(CultureInfo.InvariantCulture), v2.ToString(CultureInfo.InvariantCulture), w2.ToString(CultureInfo.InvariantCulture), ti2.ToString(CultureInfo.InvariantCulture), nb2.ToString(CultureInfo.InvariantCulture), n2.ToString(CultureInfo.InvariantCulture), co2.ToString(CultureInfo.InvariantCulture)
                                                           , c.ToString(CultureInfo.InvariantCulture), si.ToString(CultureInfo.InvariantCulture), mn.ToString(CultureInfo.InvariantCulture), cr.ToString(CultureInfo.InvariantCulture), ni.ToString(CultureInfo.InvariantCulture), cu.ToString(CultureInfo.InvariantCulture), p_.ToString(CultureInfo.InvariantCulture), s.ToString(CultureInfo.InvariantCulture), al.ToString(CultureInfo.InvariantCulture), mo.ToString(CultureInfo.InvariantCulture), v.ToString(CultureInfo.InvariantCulture), w.ToString(CultureInfo.InvariantCulture), ti.ToString(CultureInfo.InvariantCulture), nb.ToString(CultureInfo.InvariantCulture), n.ToString(CultureInfo.InvariantCulture), co.ToString(CultureInfo.InvariantCulture)
                                                           );
            return sql;
        }
    }
}