﻿// ReSharper disable once CheckNamespace
namespace AQStepFactory 
{
    public static partial class StepFactory
    {
        public static int Counter = 1;
        public static string GetFieldTypeDescription(string sr)
        {
            string typeDescription;
            switch (sr.ToLower())
            {
                case "datetime":
                    typeDescription = "01.01.2015";
                    break;
                case "timespan":
                    typeDescription = "08:00:00";
                    break;
                case "string":
                    typeDescription = "Abc";
                    break;
                case "boolean":
                case "bool":
                    typeDescription = "1/0";
                    break;
                default:
                    typeDescription = "123";
                    break;
            }
            return typeDescription;
        }
        public static string GetFieldType(string sr)
        {
            string type;
            switch (sr)
            {
                case "01.01.2015":
                    type = "DateTime";
                    break;
                case "08:00:00":
                    type = "TimeSpan";
                    break;
                case "Abc":
                    type = "String";
                    break;
                case "1/0":
                    type = "Boolean";
                    break;
                case "Неизвестен":
                    type = null;
                    break;
                default:
                    type = "Double";
                    break;
            }
            return type;
        }
    }
}
