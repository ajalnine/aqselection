﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactory 
{
    public static partial class StepFactory
    {

        /* FilterMethods
                  
          "AutoK"
          "AutoAD"
          "AutoSW"
          "3sigma"
          "1.5H"
         
          */

        public static Step CreateOutliers(string tableName, List<string> filtering, string filterMethod, bool isCyclic, bool removeValueOnly, double k, double kz, bool generateReport, string reportName, List<string> includeInReport)
        {
            var currentParameters = new OutliersParameters
            {
                TableName = tableName,
                FilterMethod = filterMethod,
                Filtering = filtering,
                GenerateReport = generateReport,
                IncludeInReport = includeInReport,
                ReportName = reportName,
                IsCyclic = isCyclic,
                K = k,
                KZ = kz,
                RemoveValueOnly = removeValueOnly
            };
            var xs = new XmlSerializer(typeof(OutliersParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Outliers",
                EditorAssemblyPath = "SLCalc_Outliers",
                Group = "Фильтрация",
                Name = "Удаление выбросов",
                ImagePath = "/AQSelection/Images/Modules/6sigma.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("OutliersParameters", "Parameters");
            return currentStep;
        }
    }
    public class OutliersParameters
    {
        public string TableName;

        public List<string> Filtering;
        public string FilterMethod;
        public bool IsCyclic;
        public bool RemoveValueOnly;

        public double K;  //коэффициент при размахе
        public double KZ; //коэффициент при сигме

        public bool GenerateReport;
        public string ReportName;
        public List<string> IncludeInReport;
    }
}
