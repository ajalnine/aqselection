﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactory 
{
    public static partial class StepFactory
    {
        public static Step CreateConstants(List<ConstantDescription> constants)
        {
            var currentParameters = new ConstantParameters { Constants = constants };
            var xs = new XmlSerializer(typeof(ConstantParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_Constants",
                EditorAssemblyPath = "SLCalc_Constants",
                Group = "Расчеты",
                Name = "Константы",
                ImagePath = "/AQSelection/Images/Modules/Constants.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("ConstantParameters", "Parameters");
            return currentStep;
        }
    }

    public class ConstantParameters
    {
        public List<ConstantDescription> Constants;
    }
    public class ConstantDescription
    {
        public string VariableName;
        public string VariableType;
        public object Value;
    }
}
