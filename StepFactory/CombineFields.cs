﻿using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactory 
{
    public static partial class StepFactory
    {
        public static Step CreateCombineFields(string tableName, List<FieldGroupData> groups)
        {
            var currentParameters = new CombineFieldsParameters
            {
                TableName = tableName,
                Groups = groups
            };
            var xs = new XmlSerializer(typeof(CombineFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_CombineFields",
                EditorAssemblyPath = "SLCalc_CombineFields",
                Group = "Преобразование",
                Name = "Группировка полей",
                ImagePath = "/AQSelection/Images/Modules/ColumnCombination.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("CombineFieldsParameters", "Parameters");
            return currentStep;
        }
    }

    public class CombineFieldsParameters
    {
        public List<FieldGroupData> Groups;
        public string TableName;
    }

    public class FieldGroupData
    {
        public List<string> CombinedFields;
        public string Name;
        public bool EmitFirst;
        public bool EmitLast;
        public bool EmitSum;
        public bool EmitRange;
        public bool EmitAverage;
        public bool EmitMin;
        public bool EmitMax;
        public bool EmitCount;
        public bool DisableAvg;
        public bool DisableSum;
    }
}
