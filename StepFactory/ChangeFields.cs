﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQStepFactory 
{
    public static partial class StepFactory
    {
        public static Step CreateChangeFields(string tableName, List<NewTableData> newTableDataList)
        {
            var currentParameters = new ChangeFieldsParameters
            {
                TableName = tableName,
                FieldTransformation = newTableDataList
            };
            var xs = new XmlSerializer(typeof (ChangeFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);

            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_ChangeFields",
                EditorAssemblyPath = "SLCalc_ChangeFields",
                Group = "Преобразование",
                Name = "Правка полей таблицы",
                ImagePath = "/AQSelection/Images/Modules/ColumnReordering.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("ChangeFieldsParameters", "Parameters");
            return currentStep;
        }

        public static IEnumerable<Step> CreateRenameFields(SLDataTable selectedTable,
            Dictionary<string, string> columnName)
        {
            var currentParameters = new ChangeFieldsParameters
            {
                FieldTransformation = new List<NewTableData>(),
                TableName = selectedTable.TableName
            };

            foreach (var c in selectedTable.ColumnNames)
            {
                currentParameters.FieldTransformation.Add(columnName.ContainsKey(c)
                    ? new NewTableData {OldFieldName = columnName[c], NewFieldName = c}
                    : new NewTableData {OldFieldName = c, NewFieldName = c});
            }
            return TransformFields(currentParameters);
        }

        private static IEnumerable<Step> TransformFields(ChangeFieldsParameters currentParameters)
        {
            var steps = new List<Step>();
            var xs = new XmlSerializer(typeof (ChangeFieldsParameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), currentParameters);
            var currentStep = new Step
            {
                ParametersXML = sb.ToString(),
                Calculator = "Calc_ChangeFields",
                EditorAssemblyPath = "SLCalc_ChangeFields",
                Group = "Преобразование",
                Name = "Правка полей таблицы",
                ImagePath = "/AQSelection/Images/Modules/ColumnReordering.png",
                Inputs = null,
                Outputs = null
            };
            currentStep.ParametersXML = currentStep.ParametersXML.Replace("ChangeFieldsParameters", "Parameters");
            steps.Add(currentStep);
            Counter++;
            return steps;
        }
    }

    public class NewTableData
    {
        public string OldTableName;
        public string NewTableName;
        public string OldFieldName;
        public string NewFieldName;
    }

    public class ChangeFieldsParameters
    {
        public string TableName;
        public List<string> DeletedFields;
        public List<NewTableData> FieldTransformation;
    }
}
