﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;
using AQStepFactoryLibrary;

namespace SLConstructor_PerUnitControl
{
    public static class ControlSelects
    {
        public static string GetPerUnitControlSQL(Filters filters, ParametersList aqpl, FieldListCombination flc, SelectionTypes currentSelectionType, bool addGroupName, bool includeControlRange)
        {
            switch(currentSelectionType)
            {
                case SelectionTypes.LinePerUnit:
                    return GetLinePerUnitSQL(filters, aqpl, flc, addGroupName, includeControlRange);
               
                default:
                    return null;       
            }
        }

        private static string GetLinePerUnitSQL(Filters filters, ParametersList aqpl, FieldListCombination flc, bool addGroupName, bool includeControlRange)
        {
            var sql = String.Empty;
            string sqlLevel0 = String.Empty, havingLevel0 = String.Empty;
            string sqlLevel1 = String.Empty, havingLevel1 = String.Empty;
            string sqlLevel2 = String.Empty, havingLevel2 = String.Empty;

            var paramListLevel0 = new List<FieldName>();
            var paramListLevel1 = new List<FieldName>();
            var paramListLevel2 = new List<FieldName>();

            if (aqpl.AllParameters.Count > 0)
            {
                string aggregateCompare;
                string processingCompare;
                string categoryName;
                foreach (var k in aqpl.AllParameters.Where(a => a.Level == 0))
                {
                    var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ".пл)") : String.Empty);

                    foreach (var pvd in k.ParameterVariants)
                    {
                        processingCompare = (pvd.VariatedValue1 != null) ? string.Format("='{0}' ", pvd.VariatedValue1) : " IS NULL ";
                        aggregateCompare = (pvd.VariatedValue2 != null) ? string.Format("='{0}' ", pvd.VariatedValue2) : " IS NULL ";
                        categoryName = groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName));
                        ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlLevel0, ref havingLevel0, paramListLevel0, k, "Level0", aggregateCompare, processingCompare, categoryName, SelectionTypes.LinePerUnit, includeControlRange);
                    }
                }

                foreach (var k in aqpl.AllParameters.Where(a => a.Level == 1))
                {
                    var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ".п)") : String.Empty);
                    foreach (var pvd in k.ParameterVariants)
                    {
                        processingCompare = (pvd.VariatedValue1 != null) ? string.Format("='{0}' ", pvd.VariatedValue1) : " IS NULL ";
                        aggregateCompare = (pvd.VariatedValue2 != null) ? string.Format("='{0}' ", pvd.VariatedValue2) : " IS NULL ";
                        categoryName = groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName));
                        ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlLevel1, ref havingLevel1, paramListLevel1, k, "Level1", aggregateCompare, processingCompare, categoryName, SelectionTypes.LinePerUnit, includeControlRange);
                    }
                }
                
                foreach (var k in aqpl.AllParameters.Where(a => a.Level == 2))
                {
                    var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + ")") : String.Empty);
                    foreach (var pvd in k.ParameterVariants)
                    {
                        processingCompare = (pvd.VariatedValue1 != null) ? string.Format("='{0}' ", pvd.VariatedValue1) : " IS NULL ";
                        aggregateCompare = (pvd.VariatedValue2 != null) ? string.Format("='{0}' ", pvd.VariatedValue2) : " IS NULL ";
                        categoryName = groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName));
                        ConstructSingleParameterSQL(filters.ValueFilter, flc, ref sqlLevel2, ref havingLevel2, paramListLevel2, k, "Level2", aggregateCompare, processingCompare, categoryName, SelectionTypes.LinePerUnit, includeControlRange);
                    }
                }

                string subQueryLevel0 = @"SELECT Technology_Smelts.SmeltID as 'SmeltID',
                                        Control_Parts.ProcessingNumber  as 'Обработка',
                                        Technology_Smelts.RegisterDate as 'Дата выплавки', 
                                        Technology_Smelts.SmeltNumber as 'Плавка', 
                                        Technology_WorkShops.WorkShop as 'Цех',
                                        Common_Mark.mark as 'Марка полученная', 
                                        Common_Mark3.mark as 'Марка сертификата', 
                                        Common_NTD.ntd as 'НТД', 
                                        Common_NTD2.ntd as 'НТД технологии', 
                                        Technology_BottlingTypes.BottlingType as 'Разливка' " + sqlLevel0 + @" FROM Technology_Smelts 
                                        INNER JOIN Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                        INNER JOIN Control_Values ON Control_Parts.CPN = Control_Values.CPN
                                        LEFT OUTER JOIN Common_Mark ON Technology_Smelts.MarkID = Common_Mark.id 
                                        LEFT OUTER JOIN Common_NTD ON Technology_Smelts.NtdID = Common_NTD.id 
                                        LEFT OUTER JOIN Common_Mark as Common_Mark3 ON Technology_Smelts.CertMarkID = Common_Mark3.id 
                                        LEFT OUTER JOIN Common_NTD as Common_NTD2 ON Technology_Smelts.TechnologyNtdID = Common_NTD2.id 
                                        LEFT OUTER JOIN Technology_WorkShops ON Technology_Smelts.WorkShopID = Technology_WorkShops.ID
                                        LEFT OUTER JOIN Technology_BottlingTypes ON Technology_Smelts.BottlingTypeID = Technology_BottlingTypes.ID
                                        INNER JOIN Common_Parameters ON Common_Parameters.id=Control_Values.ParameterID
                                        INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id
                                        WHERE " + Security.GetMarkSQLCheck(null) + " AND "
                                                + Security.GetMarkSQLCheck(null).Replace("Common_Mark", "Common_Mark3") + " AND "
                                                + Security.GetParametersCheck() + " AND "
                                                + filters.SaveFilter + @" GROUP BY Technology_Smelts.SmeltID,Control_Parts.ProcessingNumber, Technology_Smelts.SmeltNumber, Technology_WorkShops.WorkShop, Technology_Smelts.RegisterDate, Common_Mark.mark, Common_Mark3.mark, Common_NTD.ntd, Common_NTD2.ntd, Technology_BottlingTypes.BottlingType";
                if (havingLevel0.Length > 0) subQueryLevel0 += " HAVING " + havingLevel0;

                var subQueryLevel1 = @"SELECT Technology_Smelts.SmeltID as 'SmeltID' " + sqlLevel1 + @" FROM Technology_Smelts 
                                        INNER JOIN Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                        INNER JOIN Control_Values ON Control_Parts.CPN = Control_Values.CPN
                                        LEFT OUTER JOIN Common_Mark ON Technology_Smelts.MarkID = Common_Mark.id 
                                        LEFT OUTER JOIN Common_NTD ON Technology_Smelts.NtdID = Common_NTD.id 
                                        LEFT OUTER JOIN Common_Mark as Common_Mark3 ON Technology_Smelts.CertMarkID = Common_Mark3.id 
                                        LEFT OUTER JOIN Common_NTD as Common_NTD2 ON Technology_Smelts.TechnologyNtdID = Common_NTD2.id 
                                        LEFT OUTER JOIN Technology_WorkShops ON Technology_Smelts.WorkShopID = Technology_WorkShops.ID
                                        LEFT OUTER JOIN Technology_BottlingTypes ON Technology_Smelts.BottlingTypeID = Technology_BottlingTypes.ID
                                        INNER JOIN Common_Parameters ON Common_Parameters.id=Control_Values.ParameterID
                                        INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id
                                        WHERE " + Security.GetMarkSQLCheck(null) + " AND "
                                                + Security.GetMarkSQLCheck(null).Replace("Common_Mark", "Common_Mark3") + " AND "
                                                + Security.GetParametersCheck() + " AND "
                                                + filters.SaveFilter + @" and ControlLevel=1  GROUP BY Technology_Smelts.SmeltID ";
                if (havingLevel1.Length > 0) subQueryLevel1 += " HAVING " + havingLevel1;

                string subQueryLevel2 = @"SELECT Technology_Smelts.SmeltID as 'SmeltID',
                                        Control_Parts.ProcessingNumber  as 'Обработка',
                                        Control_Parts.BilletMark as 'Клеймо заготовки' " + sqlLevel2 + @" FROM Technology_Smelts 
                                        INNER JOIN Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                        INNER JOIN Control_Values ON Control_Parts.CPN = Control_Values.CPN
                                        LEFT OUTER JOIN Common_Mark ON Technology_Smelts.MarkID = Common_Mark.id 
                                        LEFT OUTER JOIN Common_NTD ON Technology_Smelts.NtdID = Common_NTD.id 
                                        LEFT OUTER JOIN Common_Mark as Common_Mark3 ON Technology_Smelts.CertMarkID = Common_Mark3.id 
                                        LEFT OUTER JOIN Common_NTD as Common_NTD2 ON Technology_Smelts.TechnologyNtdID = Common_NTD2.id 
                                        LEFT OUTER JOIN Technology_WorkShops ON Technology_Smelts.WorkShopID = Technology_WorkShops.ID
                                        LEFT OUTER JOIN Technology_BottlingTypes ON Technology_Smelts.BottlingTypeID = Technology_BottlingTypes.ID
                                        INNER JOIN Common_Parameters ON Common_Parameters.id=Control_Values.ParameterID
                                        INNER JOIN Common_ParameterTypes ON Common_Parameters.id_paramtype = Common_ParameterTypes.id
                                        WHERE " + Security.GetMarkSQLCheck(null) + " AND "
                                                + Security.GetMarkSQLCheck(null).Replace("Common_Mark", "Common_Mark3") + " AND "
                                                + Security.GetParametersCheck() + " AND "
                                                + filters.SaveFilter + @" and ControlLevel=2 GROUP BY Technology_Smelts.SmeltID, Control_Parts.ProcessingNumber, Control_Parts.BilletMark";
                if (havingLevel2.Length > 0) subQueryLevel2 += " HAVING " + havingLevel2;

                sql = @" SELECT [Level0].[Плавка], 
                                [Level2].[Клеймо заготовки], 
                                [Level0].[Обработка],
                                [Level0].[Дата выплавки], 
                                [Level0].[Цех],
                                [Level0].[Марка полученная], 
                                [Level0].[Марка сертификата], 
                                [Level0].[НТД], 
                                [Level0].[НТД технологии], 
                                [Level0].[Разливка] ";
                var paramList = String.Empty;
                var commonParameterList = paramListLevel0.Union(paramListLevel1.Union(paramListLevel2));

                paramList = commonParameterList.OrderBy(a => a.Position).Select(a => a.Name).Distinct().Aggregate(paramList, (current, fn) => current + fn);

                sql += paramList + "  FROM (" + subQueryLevel0 + ") as Level0 left outer JOIN (" + subQueryLevel1 + ") as Level1 on Level0.SmeltID = Level1.SmeltID left outer join (" + subQueryLevel2 + ") as Level2 on Level2.SmeltID = Level0.SmeltID and Level2.[Обработка] = Level0.[Обработка] ";

            }
            return sql + " ORDER BY [Level0].[Плавка], [Level2].[Клеймо заготовки] ";
        }

        private static void ConstructSingleParameterSQL(string valueFilter, FieldListCombination flc, ref string sql, ref string having, ICollection<FieldName> paramList, ParamDescription k, string subQueryName, string aggregateCompare, string processingCompare, string fieldSubName, SelectionTypes currentSelectionType, bool includeControlRange)
        {

            if (k.IsNumber)
            {
                if (!FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID))
                {
                    if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerUnit:

                                if (includeControlRange)
                                {
                                    sql += string.Format(",max(case when Control_Values.ParameterID='{0}' and Control_Parts.Aggregate {1} and Control_Values.PerBilletProcessingNumber {2} then  dbo.ToNumeric(Control_Values.Min) else null end) as '{3}{4}_НГ' \r\n", k.ParamID, aggregateCompare, processingCompare, k.Param, fieldSubName);
                                    paramList.Add(new FieldName
                                    {
                                        Name = string.Format(@"
, [{0}].[{1}{2}_НГ]", subQueryName, k.Param, fieldSubName),
                                        GroupPosition = k.GroupOrder,
                                        ParameterPosition = k.ParameterOrder,
                                        AggregatePosition = 1,
                                        ProcessingPosition = 1
                                    });
                                }

                                sql += string.Format(",max(case when Control_Values.ParameterID='{0}' and Control_Parts.Aggregate {1} and Control_Values.PerBilletProcessingNumber {2} then  dbo.ToNumeric(Control_Values.Value) else null end) as '{3}{4}' \r\n", k.ParamID, aggregateCompare, processingCompare, k.Param, fieldSubName);
                                paramList.Add(new FieldName
                                {
                                    Name= string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName), 
                                     GroupPosition=k.GroupOrder, 
                                     ParameterPosition = k.ParameterOrder, 
                                     AggregatePosition=1,
                                     ProcessingPosition=1
                                });


                                if (includeControlRange)
                                {
                                    sql += string.Format(",max(case when Control_Values.ParameterID='{0}' and Control_Parts.Aggregate {1} and Control_Values.PerBilletProcessingNumber {2} then  dbo.ToNumeric(Control_Values.Max) else null end) as '{3}{4}_ВГ' \r\n", k.ParamID, aggregateCompare, processingCompare, k.Param, fieldSubName);
                                    paramList.Add(new FieldName
                                    {
                                        Name = string.Format(@"
, [{0}].[{1}{2}_ВГ]", subQueryName, k.Param, fieldSubName),
                                        GroupPosition = k.GroupOrder,
                                        ParameterPosition = k.ParameterOrder,
                                        AggregatePosition = 1,
                                        ProcessingPosition = 1
                                    });
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch (currentSelectionType)
                        {
                            case SelectionTypes.LinePerUnit:
                                if (includeControlRange)
                                {
                                    sql += string.Format(",max(case when Control_Values.PerBilletProcessingNumber {0} and Control_Parts.Aggregate {1}  then dbo.MultipleToNumeric(Control_Values.Min, Control_Values.ParameterID , '{2}') else null end) as '{3}{4}_НГ' \r\n", processingCompare, aggregateCompare, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, fieldSubName);
                                    paramList.Add(new FieldName
                                    {
                                        Name = string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName),
                                        GroupPosition = k.GroupOrder,
                                        ParameterPosition = k.ParameterOrder,
                                        AggregatePosition = 1,
                                        ProcessingPosition = 1
                                    });
                                }

                                    sql += string.Format(",max(case when Control_Values.PerBilletProcessingNumber {0} and Control_Parts.Aggregate {1}  then dbo.MultipleToNumeric(Control_Values.Value, Control_Values.ParameterID , '{2}') else null end) as '{3}{4}' \r\n", processingCompare, aggregateCompare, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, fieldSubName);
                                paramList.Add(new FieldName
                                {
                                    Name= string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName), 
                                        GroupPosition=k.GroupOrder, 
                                        ParameterPosition = k.ParameterOrder, 
                                        AggregatePosition=1, 
                                        ProcessingPosition=1
                                });

                                if (includeControlRange)
                                {
                                    sql += string.Format(",max(case when Control_Values.PerBilletProcessingNumber {0} and Control_Parts.Aggregate {1}  then dbo.MultipleToNumeric(Control_Values.Max, Control_Values.ParameterID , '{2}') else null end) as '{3}{4}_ВГ' \r\n", processingCompare, aggregateCompare, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, fieldSubName);
                                    paramList.Add(new FieldName
                                    {
                                        Name = string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName),
                                        GroupPosition = k.GroupOrder,
                                        ParameterPosition = k.ParameterOrder,
                                        AggregatePosition = 1,
                                        ProcessingPosition = 1
                                    });
                                }
                                break;
                        }
                    }
                }
                var hs = FilterUtility.GetValueFilterString(valueFilter, k.ParamID);
                if (string.IsNullOrEmpty(hs)) return;
                if (having != String.Empty) having += " AND ";
                having += hs;
            }
            else
            {
                if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) return;
                if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                {
                    switch (currentSelectionType)
                    {
                        case SelectionTypes.LinePerUnit:
                            sql += string.Format(",max(case when Control_Values.ParameterID='{0}' and Control_Parts.Aggregate {1} and  Control_Values.PerBilletProcessingNumber {2}  then Control_Values.Value else null end) as '{3}{4}'\r\n", k.ParamID, aggregateCompare, processingCompare, k.Param, fieldSubName);
                            paramList.Add(new FieldName
                            {
                                 Name= string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName), 
                                 GroupPosition=k.GroupOrder, 
                                 ParameterPosition = k.ParameterOrder, 
                                 AggregatePosition=1, 
                                 ProcessingPosition=1
                            });
                            break;
                    }
                }
                else
                {
                    switch (currentSelectionType)
                    {
                        case SelectionTypes.LinePerUnit:
                            sql += string.Format(",max(case when Control_Values.PerBilletProcessingNumber {0} and Control_Parts.Aggregate {1}  then dbo.MultipleToString(Control_Values.Value, Control_Values.ParameterID , '{2}') else null end) as '{3}{4}' \r\n", processingCompare, aggregateCompare, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, fieldSubName);
                            paramList.Add(new FieldName
                            {
                                 Name= string.Format(@"
, [{0}].[{1}{2}]", subQueryName, k.Param, fieldSubName), 
                                 GroupPosition=k.GroupOrder, 
                                 ParameterPosition = k.ParameterOrder, 
                                 AggregatePosition=1, 
                                 ProcessingPosition=1});
                            break;
                    }
                }
            }
        }

        #region Create Step Parameters
        public static List<NewTableData> GetChangeFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Марка полученная", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
                new NewTableData{OldFieldName = "Обработка", NewFieldName = "Обработка"}, 
                new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Дата выплавки", NewFieldName = "Дата"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"},
                new NewTableData{OldFieldName = "Клеймо заготовки", NewFieldName = "Клеймо заготовки"} 
            });

            foreach (var k in aqpl.AllParameters.OrderBy(a=>a.Level))
            {
                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                foreach (var pvd in k.ParameterVariants)
                {
                    var categoryName = groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName));
                    string fieldName;
                    if (includeControlRange && k.IsNumber)
                    {
                        fieldName = string.Format("{0}{1}_НГ", k.Param, categoryName);
                        result.Add(new NewTableData {OldFieldName = fieldName, NewFieldName = fieldName});
                    }
                    fieldName = string.Format("{0}{1}", k.Param, categoryName);
                    result.Add(new NewTableData { OldFieldName = fieldName, NewFieldName = fieldName });
                    if (includeControlRange && k.IsNumber)
                    {
                        fieldName = string.Format("{0}{1}_ВГ", k.Param, categoryName);
                        result.Add(new NewTableData { OldFieldName = fieldName, NewFieldName = fieldName });
                    }
                }
            }
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
                "Клеймо заготовки"
            });
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка"
            });

            return result;
        }

        public static List<string> GetRightBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
                "Клеймо заготовки"
            });
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
                "Плавка",
            });
            var oldGroup = string.Empty;
            foreach (var k in aqpl.AllParameters.Where(a => a.IsNumber))
            {
                if (k.ParamType == oldGroup) continue;
                oldGroup = k.ParamType;

                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                result.AddRange(k.ParameterVariants.Select(pvd => groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName)))
                    .Select(categoryName => string.Format("{0}{1}", k.Param, categoryName)));
            }
            return result;
        }

        public static List<string> GetHorizontalBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
            });

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput()
        {
            var result = new List<string>();
            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
            });
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<ScaleRule>();
            foreach (var k in aqpl.AllParameters.Where(a=>a.IsNumber))
            {
                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                var k1 = k;
                result.AddRange(from pvd in k.ParameterVariants
                    select groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName))
                    into categoryName
                    select string.Format("{0}{1}", k1.Param, categoryName)
                    into fieldName
                    select new ScaleRule
                    {
                        Mode = ScaleRuleMode.MinMax, 
                        FieldName = fieldName, 
                        Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10), 
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), 
                        Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                    });
            }
            return result;
        }
        #endregion
    }
    public class FieldName
    {
        public string Name;
        public int GroupPosition;
        public int ParameterPosition;
        public int AggregatePosition;
        public int ProcessingPosition;
        public Int64 Position
        {
            get 
            {
                return GroupPosition * 100000 + ParameterPosition * 1000 + AggregatePosition * 10 + ProcessingPosition;
            }
        }
    }
}