﻿using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;

namespace SLConstructor_PerUnitControl
{
    public class ControlParametersList : SelectionParametersList
    {
        public ControlParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            
            DateField = " Technology_Smelts.RegisterDate";

            DetailsJoin = @" inner join Control_Values on Control_Parts.CPN=Control_Values.CPN 
                             inner join Common_Parameters on Common_Parameters.id=Control_Values.ParameterID ";

            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark
                                INNER JOIN 
                                (
	                                select distinct MarkID from Technology_Smelts 
                                    inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                    @DetailsJoin
                                    @PreviousFilter
                                ) as t1 on t1.MarkID=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck(null) +  @" AND @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "Technology_Smelts.MarkID",
                 LastOnly = false,
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка сертификата",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark 
                                INNER JOIN 
                                (
	                                select distinct CertMarkID from Technology_Smelts 
                                    inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                    @DetailsJoin
                                    @PreviousFilter
                                ) as t1 on t1.CertMarkID=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck(null) + @" AND @AdditionalFilter 
                                order by mark
                                ",
                Field = "mark",
                ResultedFilterField = "Technology_Smelts.CertMarkID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            }; 
            
            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS N 
                                INNER JOIN 
                                (
	                                select distinct NtdID from Technology_Smelts 
                                    inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                    @DetailsJoin
                                    @PreviousFilter
                                ) as t1 on t1.NtdID=N.id 
                                where @AdditionalFilter 
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "Technology_Smelts.NtdID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД технологии",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS N 
                                INNER JOIN 
                                (
	                                select distinct TechnologyNtdID from Technology_Smelts 
                                    inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                    @DetailsJoin
                                    @PreviousFilter
                                ) as t1 on t1.TechnologyNtdID=N.id 
                                where @AdditionalFilter 
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "Technology_Smelts.TechnologyNtdID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                Name = "Группа параметров",
                EnableDetailsJoin = true,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
create table #tquery   (pt_id int)
insert into #tquery 
select distinct id_paramtype from Common_Parameters 
    inner join Control_Values on Common_Parameters.id = Control_Values.ParameterID
    inner join Control_Parts on Control_Parts.CPN = Control_Values.CPN
    inner join Technology_Smelts on Technology_Smelts.SmeltID =Control_Parts.TechnologySmeltID
   @PreviousFilter
select id, paramtype as name from Common_ParameterTypes 
where id in (select pt_id from #tquery) and " + Security.GetParametersCheck() + @" AND  @AdditionalFilter order by paramtype
                        drop table #tquery",
                Field = "paramtype",
                ResultedFilterField = "Common_Parameters.id_paramtype",
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd6 = new SelectionParameterDescription
            {
                ID = 6,
                Selector =  ColumnTypes.Parameters,
                Name = "Параметры и значения",
                EnableDetailsJoin = true,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
                                create table #tquery
                                (number int IDENTITY(1,1), pt_id int, name nvarchar(max), paramtype nvarchar(max), IsNumber bit, Parameter_ID int)
                                insert into #tquery 
                                select max(Common_Parameters.id_paramtype) as pt_id, Common_Parameters.DisplayParameterName,
                                Common_ParameterTypes.Paramtype as Name, Common_Parameters.IsNumber as IsNumber, Common_Parameters.id as id from Technology_Smelts 
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                inner join Control_Values on Control_Parts.CPN=Control_Values.CPN 
                                inner join Common_Parameters on Common_Parameters.id=Control_Values.ParameterID 
                                inner join Common_ParameterTypes on Common_ParameterTypes.id = Common_Parameters.id_paramtype
                                @PreviousFilter and @AdditionalFilter
                                group by Common_ParameterTypes.Paramtype, Common_Parameters.DisplayParameterName, Common_Parameters.IsNumber, Common_Parameters.id
                                order by Name, DisplayParameterName
                                select t1.number as id, t1.name as name, t1.pt_id, t1.paramtype, t1.IsNumber, cast (case when t2.paramtype=t1.paramtype then 0 else 1 end as bit) as GroupChanged, cast (case when t2.name=t1.name and t2.paramtype=t1.paramtype then 0 else 1 end as bit) as ParameterChanged, t1.Parameter_ID from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 
                                order by paramtype, name
                                drop table #tquery
                                ",
                Field = "DisplayParameterName",
                ResultedFilterField = "Control_Values.ParameterID",
                OptionalField = "Control_Values.Value",
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            var spd7 = new SelectionParameterDescription
            {
                ID = 7,
                Selector = ColumnTypes.MeltList,
                EnableDetailsJoin = false,
                Name = "Номер плавки",

                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct SmeltNumber , max(WorkShop) as melttype from Technology_Smelts
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                @DetailsJoin
                                inner join Technology_Workshops on Technology_Smelts.WorkshopID=Technology_Workshops.ID
                                @PreviousFilter @AdditionalFilter group by SmeltNumber having SmeltNumber is not null and max(WorkShop) is not null order by max(WorkShop), SmeltNumber

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null

                                drop table #tquery
                                ",
                Field = "SmeltNumber",
                ResultedFilterField = "Technology_Smelts.SmeltNumber",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd8 = new SelectionParameterDescription
            {
                ID = 8,
                Selector = ColumnTypes.Discrete,
                Name = "Цех",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct WorkshopID as id, Workshop as name from Technology_Smelts 
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                inner join Technology_Workshops on Technology_Smelts.WorkshopID=Technology_Workshops.ID
                                @DetailsJoin
                                @PreviousFilter and WorkshopID is not null",
                Field = "WorkshopID",
                ResultedFilterField = "Technology_Smelts.WorkshopID",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd9 = new SelectionParameterDescription
            {
                ID = 9,
                Selector = ColumnTypes.Discrete,
                Name = "Способ разливки",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct BottlingTypeID as id,BottlingType as name from Technology_Smelts 
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                inner join Technology_BottlingTypes on Technology_Smelts.BottlingTypeID=Technology_BottlingTypes.ID
                                @DetailsJoin
                                @PreviousFilter and BottlingTypeID is not null",
                Field = "BottlingTypeID",
                ResultedFilterField = "Technology_Smelts.BottlingTypeID",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd10 = new SelectionParameterDescription
            {
                ID = 10,
                Selector = ColumnTypes.Discrete,
                Name = "Агрегат",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct Aggregate as id, Aggregate as name from Technology_Smelts 
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                @DetailsJoin
                                @PreviousFilter and Aggregate is not null",
                Field = "Aggregate",
                ResultedFilterField = "Control_Parts.Aggregate",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd11 = new SelectionParameterDescription
            {
                ID = 11,
                Selector = ColumnTypes.Discrete,
                Name = "Уровень параметра",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct controllevel as id, case controllevel when 0 then 'Плавка' when 1 then 'Партия' when 2 then 'Единица' end as name from Technology_Smelts 
                                inner join Control_Parts on Technology_Smelts.SmeltID = Control_Parts.TechnologySmeltID
                                @DetailsJoin
                                @PreviousFilter",
                Field = "ControlLevel",
                ResultedFilterField = "Control_Parts.ControlLevel",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
            ParametersList.Add(spd6);
            ParametersList.Add(spd11);
            ParametersList.Add(spd7);
            ParametersList.Add(spd8);
            ParametersList.Add(spd9);
            ParametersList.Add(spd10);
        }
    }
}
