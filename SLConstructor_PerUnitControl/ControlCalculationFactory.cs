﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;

namespace SLConstructor_PerUnitControl
{
    public static class ControlCalculationFactory
    {
        public static List<NewTableData> GetChangeFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Марка полученная", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
                new NewTableData{OldFieldName = "Обработка", NewFieldName = "Обработка"}, 
                new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Дата выплавки", NewFieldName = "Дата"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"},
                new NewTableData{OldFieldName = "Клеймо заготовки", NewFieldName = "Клеймо заготовки"} 
            });

            foreach (var k in aqpl.AllParameters.OrderBy(a=>a.Level))
            {
                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                foreach (var pvd in k.ParameterVariants)
                {
                    var categoryName = groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName));
                    string fieldName;
                    if (includeControlRange && k.IsNumber)
                    {
                        fieldName = string.Format("{0}{1}_НГ", k.Param, categoryName);
                        result.Add(new NewTableData {OldFieldName = fieldName, NewFieldName = fieldName});
                    }
                    fieldName = string.Format("{0}{1}", k.Param, categoryName);
                    result.Add(new NewTableData { OldFieldName = fieldName, NewFieldName = fieldName });
                    if (includeControlRange && k.IsNumber)
                    {
                        fieldName = string.Format("{0}{1}_ВГ", k.Param, categoryName);
                        result.Add(new NewTableData { OldFieldName = fieldName, NewFieldName = fieldName });
                    }
                }
            }
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
                "Клеймо заготовки"
            });
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка"
            });

            return result;
        }

        public static List<string> GetRightBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
                "Клеймо заготовки"
            });
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Цех",
                "Марка",
                "НТД",
                "Разливка",
                "Дата",
                "Плавка",
            });
            var oldGroup = string.Empty;
            foreach (var k in aqpl.AllParameters.Where(a => a.IsNumber))
            {
                if (k.ParamType == oldGroup) continue;
                oldGroup = k.ParamType;

                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                result.AddRange(k.ParameterVariants.Select(pvd => groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName)))
                    .Select(categoryName => string.Format("{0}{1}", k.Param, categoryName)));
            }
            return result;
        }

        public static List<string> GetHorizontalBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
            });

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput()
        {
            var result = new List<string>();
            result.AddRange(new List<string>
            {
                "Марка", 
                "НТД", 
                "Обработка", 
                "Разливка", 
                "Дата", 
                "Плавка",
            });
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(ParametersList aqpl, FieldListCombination flc, SelectionTypes selectionType, bool addGroupName, bool includeControlRange)
        {
            var result = new List<ScaleRule>();
            foreach (var k in aqpl.AllParameters.Where(a=>a.IsNumber))
            {
                var groupName = ((addGroupName || ParametersUtility.IsDuplicatedParameter(aqpl, k.Param)) ? (" (" + k.ParamType + (k.Level == 0 ? ".пл" : (k.Level == 1 ? ".п" : String.Empty)) + ")") : String.Empty);

                var k1 = k;
                result.AddRange(from pvd in k.ParameterVariants
                    select groupName + (string.IsNullOrEmpty(pvd.VariantName) ? String.Empty : string.Format("({0})", pvd.VariantName))
                    into categoryName
                    select string.Format("{0}{1}", k1.Param, categoryName)
                    into fieldName
                    select new ScaleRule
                    {
                        Mode = ScaleRuleMode.MinMax, 
                        FieldName = fieldName, 
                        Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10), 
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), 
                        Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                    });
            }
            return result;
        }
    }
}