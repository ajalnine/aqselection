﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_PerUnitControl
{
    public partial class MainPage
    {
        private const string TableName = "Контроль производства";

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            PreviewButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Aнализ", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                    {
                        var tables = _cas.EndGetTablesSLCompatible(i);
                        if (tables == null) return;
                        task.AdvanceProgress();
                        task.SetMessage("Данные готовы");
                        var ca = new Dictionary<string, object>
                        {
                            {"From", ColumnsPresenter.From}, 
                            {"To", ColumnsPresenter.To}, 
                            {"SourceData", tables},
                            {"Calculation", _steps},
                            {"Description", _filters.Description},
                            {"New", true},
                        };
                        Dispatcher.BeginInvoke(() =>
                        {
                            PreviewButton.IsEnabled = true;
                            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
                }
            }, task);
        }

        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            XlsxExportButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт Xlsx", TaskPanel);
            task.SetState(0, TaskState.Processing);
            task.CustomData = sender;
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, iar =>
            {
                var t = _cas.EndExecuteSteps(iar);
                if (t.Success)
                {
                    task.AdvanceProgress();
                    task.SetMessage(t.Message);
                    _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, iar2 =>
                    {
                        try
                        {
                            var t2 = _cas.EndGetAllResultsInXlsx(iar2);
                            task.AdvanceProgress();
                            task.SetMessage(t2.Message);
                            Dispatcher.BeginInvoke(() =>
                            {
                                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                    string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}"
                                    , HttpUtility.UrlEncode(t2.TaskDataGuid)
                                    , HttpUtility.UrlEncode(string.Format("Отчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)))
                                    , HttpUtility.UrlEncode("application/msexcel"))));
                                XlsxExportButton.IsEnabled = true;
                            });
                        }
                        catch
                        {
                            task.SetState(1, TaskState.Error);
                            task.SetMessage("Ошибка создания XLSX");
                        }
                    }, task);
                }
                else
                {
                    _cas.BeginDropData(t.TaskDataGuid, null, null);
                    task.SetState(0, TaskState.Error);
                    task.SetMessage(t.Message);
                    Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
                }
            }, task);
        }
        private void SaveQueryButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", TableName},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation, signalArguments, null);
        }

        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            CreateCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", TableName},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }
        
        private void CreateCalculation()
        {
            RefreshFlags();
            RefreshFilters(false);
            _currentSQL = ControlSelects.GetPerUnitControlSQL(_filters, _parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange);
            Services.GetAQService().BeginWriteLog("Calc: PerUnitControl, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);
            _steps = StepFactory.CreateSQLQuery(_currentSQL, TableName);
            AppendDescriptionStep();
            if (_formatted) AppendFormatSteps();
            if (_colored) AppendScales();
        }

        private void AppendDescriptionStep()
        {
            _steps.Add(StepFactory.CreateConstants(new List<ConstantDescription>
            {
                new ConstantDescription{VariableName = "Описание", VariableType = "String", Value = _filters.Description},
                new ConstantDescription{VariableName = "Автор", VariableType = "String", Value = Security.CurrentUser},
                new ConstantDescription{VariableName = "Фиксированный столбец", VariableType = "Int32", Value =  _formatted ? 7 : 1}
            }));
        }

        private void AppendFormatSteps()
        {
            _steps.Add(StepFactory.CreateChangeFields(TableName, ControlCalculationFactory.GetChangeFieldsFormattedOutput(_parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange)));
            _steps.Add(StepFactory.CreateSorting(TableName, ControlCalculationFactory.GetSortingFieldsFormattedOutput(_parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange), true));
            _steps.Add(StepFactory.CreateFormatCombine(TableName, ControlCalculationFactory.GetCombineFieldsFormattedOutput(_parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange), true));

            _steps.Add(StepFactory.CreateFormatBorders(TableName,
                  ControlCalculationFactory.GetRightBordersFormattedOutput(),
                  ControlCalculationFactory.GetLeftBordersFormattedOutput(_parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange),
                  ControlCalculationFactory.GetHorizontalBordersFormattedOutput(),
                  ControlCalculationFactory.GetChangeBordersFormattedOutput()));
        }

        private void AppendScales()
        {
            _steps.Add(StepFactory.CreateFormatScale(TableName, ControlCalculationFactory.GetScaleRulesFormattedOutput(_parametersList, _fieldListCombination, _currentSelectionType, _addGroupName, _includeRange)));
        }
    }
}
