﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AQCalculationsLibrary;
using AQBasicControlsLibrary;
using AQControlsLibrary;
using AQCatalogueLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using System.Windows.Browser;

namespace SLSFM
{
    public partial class MainPage : UserControl, IAQModule
    {
        CommandCallBackDelegate ReadySignalCallBack;
        public void InitSignaling(CommandCallBackDelegate ccbd) { ReadySignalCallBack = ccbd; }
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool ReadyForAcceptSignalsProcessed = false;

        public InteractiveParameters p
        {
            get { return (InteractiveParameters)GetValue(pProperty); }
            set { SetValue(pProperty, value); }
        }

        public static readonly DependencyProperty pProperty =
            DependencyProperty.Register("p", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        public InteractiveResults r
        {
            get { return (InteractiveResults)GetValue(rProperty); }
            set { SetValue(rProperty, value); }
        }

        public static readonly DependencyProperty rProperty =
            DependencyProperty.Register("r", typeof(InteractiveResults), typeof(MainPage), new PropertyMetadata(null));

        private bool NewItem;
        private readonly AQModuleDescription _aqmd;
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
        }

        #region расчет смеси без бора

        private void DoSFMCalculation()
        {
            try
            {
                double PortlandCementKG = p.GetValue("Портландцемент, кг");
                double FF92KG = p.GetValue("Флюоритовый концентрат ФФ-92 по ТИ ПС-09-2016, кг");
                double GraphiteKG = p.GetValue("Графит ГЛС-2, кг");
                double PSHSKG = p.GetValue("Полевошпатный концентрат ПШС-0.20-17, кг");
                double PotassiumKG = p.GetValue("Кальцинированная сода, кг");
                double MarshalithKG = p.GetValue("Молотый кварц (маршалит), кг");

                var total = PortlandCementKG + FF92KG + GraphiteKG + PSHSKG + PotassiumKG + MarshalithKG;

                double PortlandCement = PortlandCementKG / total * 100;
                double FF92 = FF92KG / total * 100;
                double Graphite = GraphiteKG / total * 100;
                double PSHS = PSHSKG / total * 100;
                double Potassium = PotassiumKG / total * 100;
                double Marshalith = MarshalithKG / total * 100;

                r.SetValue("Портландцемент, %", PortlandCement);
                r.SetValue("Флюоритовый концентрат ФФ-92 по ТИ ПС-09-2016, %", FF92);
                r.SetValue("Графит ГЛС-2, %", Graphite);
                r.SetValue("Полевошпатный концентрат ПШС-0.20-17, %", PSHS);
                r.SetValue("Кальцинированная сода, %", Potassium);
                r.SetValue("Молотый кварц (маршалит), %", Marshalith);
                r.SetValue("Портландцемент, %", PortlandCement);

                var CaO = 0.0d;  //
                var SiO2 = 0.0d; //
                var Al2O3 = 0.0d; //
                var Fe2O3 = 0.0d; //
                var MgO = 0.0d; //
                var FeO = 0.0d; 
                var Na2O = 0.0d;
                var Na2CO3 = 0.0d;
                var K2O = 0.0d;
                var CaF2 = 0.0d;
                var CO2 = 0.0d;
                var Fion = 0.0d; // 
                var Cfree = 0.0d; //
                var Ccommon = 0.0d; //

                var Klinker = PortlandCement * 0.87;
                var DomSlag = PortlandCement * 0.13;

                var TripleCaOSiO2 = Klinker * 0.3824;
                var BiCaOSiO2 = Klinker * 0.1557;
                var TripleCaOAl2O3 = Klinker * 0.0666;
                var QuadCaOAl2O3Fe2O3 = Klinker * 0.16;

                // Из клинкера
                CaO += 0.7368 * TripleCaOSiO2 + 0.6512 * BiCaOSiO2 + 0.6222 * TripleCaOAl2O3 + 0.4609 * QuadCaOAl2O3Fe2O3;
                MgO += Klinker * 0.0151;
                SiO2 += 0.2632 * TripleCaOSiO2 + 0.3488 * BiCaOSiO2;
                Al2O3 += 0.3777 * TripleCaOAl2O3 + 0.1854 * QuadCaOAl2O3Fe2O3;
                Fe2O3 += 0.2909 * QuadCaOAl2O3Fe2O3;

                // Из доменного шлака
                CaO += 0.39 * DomSlag;
                SiO2 += 0.37 * DomSlag;
                Al2O3 += 0.125 * DomSlag;
                MgO += 0.09 * DomSlag;
                Na2O += 0.005 * DomSlag;
                K2O += 0.007 * DomSlag;
                FeO += 0.02 * DomSlag;

                // Из ФФ92
                CaF2 += 0.92 * FF92;
                CaO += 0.03 * FF92;
                SiO2 += 0.025 * FF92;

                // Из Графита
                Cfree += 0.80 * Graphite;
                SiO2 += 0.06 * Graphite;
                Al2O3 += 0.04 * Graphite;
                CaO += 0.04 * Graphite;
                FeO += 0.025 * Graphite;

                // Из Шпата
                SiO2 += 0.697 * PSHS;
                Al2O3 += 0.17 * PSHS;
                CaO += 0.005 * PSHS;
                MgO += 0.002 * PSHS;
                Na2O += 0.068 * PSHS;
                K2O += 0.043 * PSHS;

                // Из соды
                Na2CO3 += 1d * Potassium;

                // Из Маршалита
                SiO2 += 1d * Marshalith;
                
                // Финальный пересчет
                Fe2O3 += 1.1111 * FeO;
                Fion += 0.4871 * CaF2;
                CaO += 0.5128 * CaF2 * 1.4;
                Na2O += 0.5849 * Na2CO3;
                CO2 += 0.4150 * Na2CO3;
                Ccommon += 0.2727 * CO2;
                Ccommon += Cfree;

                r.SetValue("C свободный, %", Cfree);
                r.SetValue("C общий, %", Ccommon);
                r.SetValue("SiO2, %", SiO2);
                r.SetValue("CaO, %", CaO);
                r.SetValue("F-ион, %", Fion);
                r.SetValue("Na2O + K2O, %", Na2O + K2O);
                r.SetValue("Al2O3, %", Al2O3);
                r.SetValue("Fe2O3, %", Fe2O3);
                r.SetValue("MgO, %", MgO);
                r.SetValue("Основность СаО/SiO2", CaO/SiO2);
            }
            catch
            {
            }
        }

        #endregion

        #region расчет смеси с бором

        private void DoSFMBCalculation()
        {
            try
            {
                double PortlandCementKG = p.GetValue("Портландцемент, кг");
                double FF92KG = p.GetValue("Флюоритовый концентрат ФФ-92 по ТИ ПС-09-2016, кг");
                double GraphiteKG = p.GetValue("Графит ГЛС-2, кг");
                double PSHSKG = p.GetValue("Полевошпатный концентрат ПШС-0.20-17, кг");
                double PotassiumKG = p.GetValue("Кальцинированная сода, кг");
                double MarshalithKG = p.GetValue("Молотый кварц (маршалит), кг");
                double BoratKG = p.GetValue("Борат кальция, кг");
                double DatolithKG = p.GetValue("Датолитовый концентрат, кг");

                var total = PortlandCementKG + FF92KG + GraphiteKG + PSHSKG + PotassiumKG + MarshalithKG + BoratKG + DatolithKG;

                double PortlandCement = PortlandCementKG / total * 100;
                double FF92 = FF92KG / total * 100;
                double Graphite = GraphiteKG / total * 100;
                double PSHS = PSHSKG / total * 100;
                double Potassium = PotassiumKG / total * 100;
                double Marshalith = MarshalithKG / total * 100;
                double Borat = BoratKG / total * 100;
                double Datolith = DatolithKG / total * 100;

                r.SetValue("Портландцемент, %", PortlandCement);
                r.SetValue("Флюоритовый концентрат ФФ-92 по ТИ ПС-09-2016, %", FF92);
                r.SetValue("Графит ГЛС-2, %", Graphite);
                r.SetValue("Полевошпатный концентрат ПШС-0.20-17, %", PSHS);
                r.SetValue("Кальцинированная сода, %", Potassium);
                r.SetValue("Молотый кварц (маршалит), %", Marshalith);
                r.SetValue("Портландцемент, %", PortlandCement);
                r.SetValue("Борат кальция, %", Borat);
                r.SetValue("Датолитовый концентрат, %", Datolith);

                var CaO = 0.0d;  //
                var SiO2 = 0.0d; //
                var Al2O3 = 0.0d; //
                var Fe2O3 = 0.0d; //
                var MgO = 0.0d; //
                var FeO = 0.0d;
                var Na2O = 0.0d;
                var Na2CO3 = 0.0d;
                var K2O = 0.0d;
                var CaF2 = 0.0d;
                var CO2 = 0.0d;
                var Fion = 0.0d; // 
                var Cfree = 0.0d; //
                var Ccommon = 0.0d; //
                var B2O3 = 0.0d; //

                var Klinker = PortlandCement * 0.87;
                var DomSlag = PortlandCement * 0.13;

                var TripleCaOSiO2 = Klinker * 0.3824;
                var BiCaOSiO2 = Klinker * 0.1557;
                var TripleCaOAl2O3 = Klinker * 0.0666;
                var QuadCaOAl2O3Fe2O3 = Klinker * 0.16;

                // Из клинкера
                CaO += 0.7368 * TripleCaOSiO2 + 0.6512 * BiCaOSiO2 + 0.6222 * TripleCaOAl2O3 + 0.4609 * QuadCaOAl2O3Fe2O3;
                MgO += Klinker * 0.0151;
                SiO2 += 0.2632 * TripleCaOSiO2 + 0.3488 * BiCaOSiO2;
                Al2O3 += 0.3777 * TripleCaOAl2O3 + 0.1854 * QuadCaOAl2O3Fe2O3;
                Fe2O3 += 0.2909 * QuadCaOAl2O3Fe2O3;

                // Из доменного шлака
                CaO += 0.39 * DomSlag;
                SiO2 += 0.37 * DomSlag;
                Al2O3 += 0.125 * DomSlag;
                MgO += 0.09 * DomSlag;
                Na2O += 0.005 * DomSlag;
                K2O += 0.007 * DomSlag;
                FeO += 0.02 * DomSlag;

                // Из ФФ92
                CaF2 += 0.92 * FF92;
                CaO += 0.03 * FF92;
                SiO2 += 0.025 * FF92;

                // Из Графита
                Cfree += 0.80 * Graphite;
                SiO2 += 0.06 * Graphite;
                Al2O3 += 0.04 * Graphite;
                CaO += 0.04 * Graphite;
                FeO += 0.025 * Graphite;

                // Из Шпата
                SiO2 += 0.697 * PSHS;
                Al2O3 += 0.17 * PSHS;
                CaO += 0.005 * PSHS;
                MgO += 0.002 * PSHS;
                Na2O += 0.068 * PSHS;
                K2O += 0.043 * PSHS;

                // Из соды
                Na2CO3 += 1d * Potassium;

                // Из Маршалита
                SiO2 += 1d * Marshalith;

                // Из Бората
                B2O3 += 0.456 * Borat;
                CaO += 0.365 * Borat;

                // Из Датолита
                CaO += 0.285 * Datolith;
                SiO2 += 0.46 * Datolith;
                Al2O3 += 0.07 * Datolith;
                MgO += 0.01 * Datolith;
                FeO += 0.01 * Datolith;
                B2O3 += 0.93 * Datolith;
                Na2O += 0.04 * Datolith;
                K2O += 0.045 * Datolith;
                Cfree += 0.01 * Datolith;

                // Финальный пересчет
                Fe2O3 += 1.1111 * FeO;
                Fion += 0.4871 * CaF2;
                CaO += 0.5128 * CaF2 * 1.4;
                Na2O += 0.5849 * Na2CO3;
                CO2 += 0.4150 * Na2CO3;
                Ccommon += 0.2727 * CO2;
                Ccommon += Cfree;

                r.SetValue("C свободный, %", Cfree);
                r.SetValue("C общий, %", Ccommon);
                r.SetValue("SiO2, %", SiO2);
                r.SetValue("CaO, %", CaO);
                r.SetValue("F-ион, %", Fion);
                r.SetValue("Na2O, %", Na2O);
                r.SetValue("K2O, %", K2O);
                r.SetValue("B2O3, %", B2O3);
                r.SetValue("Al2O3, %", Al2O3);
                r.SetValue("Fe2O3, %", Fe2O3);
                r.SetValue("MgO, %", MgO);
                r.SetValue("Основность СаО/SiO2", CaO / SiO2);
            }
            catch
            {
            }
        }

        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription SenderDescription, Command CommandType, object CommandArgument, CommandCallBackDelegate CommandCallBack)
        {
            switch (CommandType)
            {

                case Command.InsertStoredCalculation:
                    MainCatalogueDataEditor.LoadAbstractItem((int)CommandArgument);
                    break;

                default:
                    if (CommandCallBack != null) CommandCallBack.Invoke(CommandType, CommandArgument, null);
                    break;
            }
        }

        private void NewCalculationSFM_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["SFMInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["SFMOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoSFMCalculation();
            DoSFMCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        private void EnableCatalogueButtons()
        {
            ExportToXlsx.IsEnabled = true;
            SaveCalculationButton.IsEnabled = true;
            SaveAsCalculationButton.IsEnabled = true;
        }

        private void NewCalculationSFMB_Click(object sender, RoutedEventArgs e)
        {
            p = InteractiveParameters.GetClone(this.Resources["SFMBInputParameters"] as InteractiveParameters);
            r = InteractiveResults.GetClone(this.Resources["SFMBOutputResults"] as InteractiveResults);
            foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoSFMBCalculation();
            DoSFMBCalculation();
            CalculationPanel.Visibility = Visibility.Visible;
            EnableCatalogueButtons();
        }

        #region Сохранение расчета
        private void SaveCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainCatalogueDataEditor.CheckName())
            {
                MessageBox.Show("Для сохранения выберите папку и введите наименование ШОС");
                return;
            }
            TextImageButtonBase tibb = sender as TextImageButtonBase;
            NewItem = tibb.Tag.ToString() == "NewItem";

            SaveCalculationButton.IsEnabled = false;
            SaveAsCalculationButton.IsEnabled = false;
            Task task = new Task(new string[] { "Сохранение расчета" }, "Сохранение расчета", TaskPanel); task.SetState(0, TaskState.Processing);
            task.CustomData = sender;
            task.SetState(0, TaskState.Processing);
            ItemDescription CatalogueData = CatalogueOperations.SFMCalculationToItem(p, r);
            this.Dispatcher.BeginInvoke(delegate ()
            {
                MainCatalogueDataEditor.SaveAbstractItem(CatalogueData, task, true, NewItem);
            });
        }

        private void MainCatalogueDataEditor_CatalogueItemSaved(object o, AQCatalogueLibrary.CatalogueItemOperationEventArgs e)
        {
            this.Dispatcher.BeginInvoke(delegate ()
            {
                Task task = (Task)e.State;
                task.SetState(0, TaskState.Ready);
                SaveCalculationButton.IsEnabled = true;
                SaveAsCalculationButton.IsEnabled = true;
                DeleteCalculation.IsEnabled = true;
            });
        }
        #endregion

        #region Удаление расчета
        private void DeleteCalculation_Click(object sender, RoutedEventArgs e)
        {
            Task task = new Task(new string[] { "Удаление\r\nиз каталога" }, "Удаление запроса", TaskPanel);
            task.SetState(0, TaskState.Processing);
            DeleteCalculation.IsEnabled = false;
            MainCatalogueDataEditor.DeleteQuery(task, false);
        }

        private void MainCatalogueDataEditor_CatalogueItemDeleted(object o, AQCatalogueLibrary.CatalogueItemOperationEventArgs e)
        {
            this.Dispatcher.BeginInvoke(delegate ()
            {
                Task task = (Task)e.State;
                task.SetState(0, TaskState.Ready);
                DeleteCalculation.IsEnabled = false;
            });
        }
        #endregion

        #region Загрузка расчета

        private void MainCatalogueDataEditor_CatalogueItemLoaded(object o, CatalogueItemOperationEventArgs e)
        {
            ItemDescription Item = e.Cd as ItemDescription;
            if (Item != null)
            {
                InteractiveParameters P = null;
                InteractiveResults R = null;
                CatalogueOperations.ItemToSFM(Item, out P, out R);
                p = P;
                r = R;
                switch (p.AttachedCalculatorName)
                {
                    case "ШОС":
                        foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoSFMCalculation();
                        break;
                    case "ШОС с бором":
                        foreach (var a in p.Children) a.ValueChanged += (s, ev) => DoSFMBCalculation();
                        break;
                    default:
                        break;
                }
                CalculationPanel.Visibility = Visibility.Visible;
                EnableCatalogueButtons();
            }
        }
        #endregion

        #region Экспорт в Xlsx

        private void ExportToXlsx_Click(object sender, RoutedEventArgs e)
        {
            CalculationService cs = Services.GetCalculationService();

            Task task = new Task(new string[] { "Формирование результата", "Формирование файла Xlsx" }, "Экспорт в Xlsx", TaskPanel);
            task.SetState(0, TaskState.Processing);
            SLDataTable sl = new SLDataTable();
            sl.ColumnNames = new List<string>();
            sl.ColumnNames.Add("Параметр");
            sl.ColumnNames.Add("Значение");
            sl.DataTypes = new List<string>();
            sl.DataTypes.Add("String");
            sl.DataTypes.Add("Double");
            sl.TableName = "Расчет";
            sl.Table = new List<SLDataRow>();

            foreach (var a in p.Children)
            {
                SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                dr.Row.Add(a.Caption);
                dr.Row.Add(a.CurrentValue);
                sl.Table.Add(dr);
            }
            foreach (var a in r.Children)
            {
                SLDataRow dr = new SLDataRow() { Row = new List<object>() };
                dr.Row.Add(a.Caption);
                dr.Row.Add(a.CurrentValue);
                sl.Table.Add(dr);
            }

            List<SLDataTable> sldt = new List<SLDataTable>();
            sldt.Add(sl);
            cs.BeginFillDataWithSLTables(sldt, (iar) =>
            {
                StepResult sr = cs.EndFillDataWithSLTables(iar);
                if (sr.Success)
                {
                    task.SetState(0, TaskState.Ready);
                    task.SetState(1, TaskState.Processing);
                    cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, (iar2) =>
                    {
                        sr = cs.EndGetAllResultsInXlsx(iar2);
                        task.SetState(1, TaskState.Ready);
                        this.Dispatcher.BeginInvoke(() =>
                        {
                            HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri, @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" + HttpUtility.UrlEncode("Расчет " + DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) + ".xlsx") + "&ContentType=" + HttpUtility.UrlEncode("application/msexcel")));
                        });
                    }, task);
                }
                else
                {
                    task.SetState(0, TaskState.Error);
                    cs.BeginDropData(sr.TaskDataGuid, null, null);
                }
            }, task);
        }
        #endregion

        private void MainCatalogueDataEditor_Loaded(object sender, RoutedEventArgs e)
        {
            MainCatalogueDataEditor.SetToEmptySelection();
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReadyForAcceptSignals != null && !ReadyForAcceptSignalsProcessed) ReadyForAcceptSignals.Invoke(this, new ReadyForAcceptSignalsEventArg() { CommandCallBack = ReadySignalCallBack });
            ReadyForAcceptSignalsProcessed = true;
        }
    }

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class SmallMaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            InputParameter p = (InputParameter)value;
            return p.SmallMaxValue - p.SmallMinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}