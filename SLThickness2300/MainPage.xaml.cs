﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using AQConstructorsLibrary;
using AQControlsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQAnalysisLibrary;


namespace SLThickness2300
{
    public partial class MainPage : IAQModule
    {
        public enum ModuleStates
        {
            UserReady,
            ApplicationBusy,
            Error
        };

        private StateDispatcher<ModuleStates> _moduleStateDispatcher;
        private UserCommand _viewDataUserCommand;
        private UserCommand _precheckDataUserCommand;
        private int _rollCount;
        private DataAnalysis _attachedDataAnalysis;
        private List<SLDataTable> _currentRollInfo;
        private DateTime _currentRollDateKey;
        private readonly AQModuleDescription _aqmd;
        private ThicknessReportTypes CurrentReportMode
        {
            get
            {
                if (ReportSelector?.SelectedItem == null) return ThicknessReportTypes.Unknown;
                string modeName = ((TabItem)ReportSelector.SelectedItem).Name;
                ThicknessReportTypes reportMode;
                if (!Enum.TryParse(modeName, true, out reportMode)) throw new Exception("Ошибка имени отчета");
                return reportMode;
            }
        }

        #region Dependency properties

        public InteractiveParameters CurrentParameters
        {
            get { return (InteractiveParameters)GetValue(CurrentParametersProperty); }
            set { SetValue(CurrentParametersProperty, value); }
        }

        public static readonly DependencyProperty CurrentParametersProperty =
            DependencyProperty.Register("CurrentParameters", typeof(InteractiveParameters), typeof(MainPage), new PropertyMetadata(null));

        #endregion

       public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            InitializeStateDispatcher();
            InitializeUserCommands();
            ColumnsPresenter.SelectionParameters = new SelectionThicknessParametersList();
        }

        private void InitializeStateDispatcher()
        {
            _moduleStateDispatcher = new StateDispatcher<ModuleStates>();
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.ApplicationBusy, () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = false; }));
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.UserReady, () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = true; }));
            _moduleStateDispatcher.SetStateProcessor(ModuleStates.Error, () => Dispatcher.BeginInvoke(delegate { PreviewButton.IsEnabled = false; }));
        }

        #region Обработка сигналов IAQModule

        CommandCallBackDelegate _readySignalCallBack;
        private bool _readyForAcceptSignalsProcessed;
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            commandCallBack?.Invoke(commandType, commandArgument, null);
        }

        public void InitSignaling(CommandCallBackDelegate ccbd) { _readySignalCallBack = ccbd; }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReadyForAcceptSignals != null && !_readyForAcceptSignalsProcessed)
            {
                ReadyForAcceptSignals.Invoke(this, new ReadyForAcceptSignalsEventArg { CommandCallBack = _readySignalCallBack });
            }
            _readyForAcceptSignalsProcessed = true;
        }

        #endregion

        #region Установка числовых параметров по режиму отчета

        private void ReportSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentReportMode != ThicknessReportTypes.RollList)
            {
                _attachedDataAnalysis?.SetCustomTableDetailViewer(null);
            }
            UpdateCurrentParameters();
        }

        private void ReportSelector_OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateCurrentParameters();
        }

        private void UpdateCurrentParameters()
        {
            if (ReportSelector?.SelectedItem == null) return;
            CurrentParameters = Resources[CurrentReportMode.ToString()] as InteractiveParameters;
        }
        #endregion

        #region Состояния интерфейса и исполнение команд

        private void ColumnsPresenter_ConstructionNotCompleted(object o, ConstructionNotCompletedEventArgs e)
        {
            _moduleStateDispatcher.GoState(ModuleStates.ApplicationBusy);
        }

        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            _moduleStateDispatcher.GoState(ModuleStates.UserReady);
            _precheckDataUserCommand.Execute();
        }

        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckReportPossibility())
            {
                _moduleStateDispatcher.GoState(ModuleStates.ApplicationBusy);
                _viewDataUserCommand.Execute();
            }
        }
        
        #endregion

        private bool CheckReportPossibility()
        {
            if (CurrentReportMode == ThicknessReportTypes.Accuracy)
            {
                var maxCount = 1100;
                if (maxCount >= _rollCount) return true;

                var mbr =
                    MessageBox.Show(
                        $"Рекомендуемое число листов для обработки - не более {maxCount}. При расчете по {_rollCount} листам возможно аварийное прекращение работы системы. Продолжить?", "Внимание", MessageBoxButton.OKCancel);
                return mbr == MessageBoxResult.OK;
            }
            return true;
        }
    }

    #region Конвертеры

    public class MaxValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var p = (InputParameter)value;
            return p.MaxValue - p.MinValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    #endregion
}
