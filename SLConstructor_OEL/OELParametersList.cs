﻿using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;
namespace SLConstructor_OEL
{
    public partial class OELParametersList : SelectionParametersList
    {
        public OELParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            
            DateField = " oel_head.test_date";

            DetailsJoin = " inner join oel_elements on oel_elements.id_test=oel_head.id_test ";

            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark
                                INNER JOIN 
                                (
	                                select distinct id_cmark from oel_head @DetailsJoin @PreviousFilter group by id_cmark
                                ) as t1 on t1.id_cmark=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck("Контроль") + @" AND @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "oel_head.id_cmark",
                 LastOnly = false,
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                 select distinct id_ntd from oel_head @DetailsJoin @PreviousFilter group by id_ntd
                                ) as t1 on t1.id_ntd=G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "oel_head.id_ntd",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Parameters,
                EnableDetailsJoin = true,
                Name = "Элементы и значения",
                
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
                                create table #tquery
                                (number int IDENTITY(1,1), pt_id int, name nvarchar(max), paramtype nvarchar(max), IsNumber bit, Parameter_ID int, l int)

                                insert into #tquery 
                                select distinct 1 as pt_id, tbl_Parameters.DisplayParameterName,
                                tbl_Paramtype.Paramtype as Name, tbl_Parameters.IsNumber, tbl_parameters.id, 1 as l from oel_head 
                                inner join tbl_Paramtype on tbl_Paramtype.id = oel_head.id_paramtype
                                inner join oel_elements on oel_elements.id_test=oel_head.id_test 
                                inner join tbl_Parameters on tbl_Parameters.id = oel_elements.id_parameter @PreviousFilter and @AdditionalFilter and (tbl_parameters.displayparametername not like '%погрешность'  and tbl_parameters.displayparametername not like '%НТД определения') 
                                
                                union

                                select distinct 2 as pt_id, tbl_Parameters.DisplayParameterName, 'Погрешности', tbl_Parameters.IsNumber, tbl_parameters.id, 2 as l from oel_head 
                                inner join tbl_Paramtype on tbl_Paramtype.id = oel_head.id_paramtype
                                inner join oel_elements on oel_elements.id_test=oel_head.id_test 
                                inner join tbl_Parameters on tbl_Parameters.id = oel_elements.id_parameter @PreviousFilter and @AdditionalFilter and (tbl_parameters.displayparametername  like '%погрешность' ) 

                                union

                                select distinct 3 as pt_id, tbl_Parameters.DisplayParameterName,'НТД определения', tbl_Parameters.IsNumber, tbl_parameters.id, 3 as l from oel_head 
                                inner join tbl_Paramtype on tbl_Paramtype.id = oel_head.id_paramtype
                                inner join oel_elements on oel_elements.id_test=oel_head.id_test 
                                inner join tbl_Parameters on tbl_Parameters.id = oel_elements.id_parameter @PreviousFilter and @AdditionalFilter and (tbl_parameters.displayparametername  like '%НТД определения') 

                                order by l, displayparametername 
                                
                                
                                select t1.number as id, t1.name as name, t1.pt_id, t1.paramtype, t1.IsNumber, cast (case when t2.paramtype=t1.paramtype then 0 else 1 end as bit) as GroupChanged, cast (case when t2.name=t1.name and t2.paramtype=t1.paramtype then 0 else 1 end as bit) as ParameterChanged, t1.Parameter_ID from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1
                                
                                drop table #tquery
                                ",
                Field = "DisplayParameterName",
                ResultedFilterField = "oel_elements.id_parameter",
                OptionalField = "oel_elements.value",
                LastOnly = true,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.MeltList,
                EnableDetailsJoin = false,
                Name = "Номер плавки",

                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct melt , max(melttype) as melttype from oel_head @DetailsJoin @PreviousFilter @AdditionalFilter group by melt having melt is not null and max(melttype) is not null order by max(melttype), melt

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null

                                drop table #tquery
                                ",
                Field = "oel_head.melt",
                ResultedFilterField = "melt",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                Name = "Лаборатория",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct oel_head.lab as id, oel_head.lab as name from oel_head @DetailsJoin  @PreviousFilter and lab is not null",
                Field = "oel_head.lab",
                ResultedFilterField = "lab",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd6 = new SelectionParameterDescription
            {
                ID = 6,
                Selector = ColumnTypes.Discrete,
                Name = "Проба",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, DisplayProbeName as name from OEL_Probes AS M 
                                INNER JOIN 
                                (
	                                select distinct id_probe from oel_head @DetailsJoin @PreviousFilter group by id_probe
                                ) as t1 on t1.id_probe=M.id 
                                where @AdditionalFilter 
                                order by DisplayProbeName
                                ",
                Field = "probename",
                ResultedFilterField = "oel_head.id_probe",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd7 = new SelectionParameterDescription
            {
                ID = 7,
                Selector = ColumnTypes.Discrete,
                Name = "Тип испытания",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, M.Type as name from OEL_ProbeTypes AS M 
                                INNER JOIN 
                                (
	                                select distinct type from oel_head @DetailsJoin @PreviousFilter group by type
                                ) as t1 on t1.type=M.id 
                                where @AdditionalFilter 
                                order by M.Type
                                ",
                Field = "type",
                ResultedFilterField = "oel_head.type",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd8 = new SelectionParameterDescription
            {
                ID = 8,
                Selector = ColumnTypes.Discrete,
                Name = "Цех / материал",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct melttype as id, melttype as name from oel_head @DetailsJoin @PreviousFilter and melttype is not null group by melttype",
                Field = "oel_head.melttype",
                ResultedFilterField = "melttype",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd9 = new SelectionParameterDescription
            {
                ID = 9,
                Selector = ColumnTypes.Discrete,
                Name = "Способ разливки",
                EnableDetailsJoin = false,
                SearchAvailable = false,
                SQLTemplate = @"set dateformat ymd select distinct onrs as id, onrs as name from oel_head @DetailsJoin @PreviousFilter and onrs is not null group by onrs",
                Field = "oel_head.onrs",
                ResultedFilterField = "onrs",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
            ParametersList.Add(spd6);
            ParametersList.Add(spd7);
            ParametersList.Add(spd8);
            ParametersList.Add(spd9);
        }
    }
}
