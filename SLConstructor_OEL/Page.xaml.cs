﻿using System;
using System.Collections.Generic;
using System.Windows;
using AQControlsLibrary;
using AQConstructorsLibrary;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLConstructor_OEL
{
    public partial class Page : IAQModule
    {
        #region Переменные

        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals; 
        
        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;
        private readonly OELParametersList _selectionOEL = new OELParametersList();
        private ParametersList _parametersList;
        private FieldListCombination _fieldListCombination;
        private List<ProbeDescription> _probeList;
        private Filters _filters;
        private string _sql = string.Empty;
        private bool _probeProcessing;
        private bool _showFvc;
        private bool _formatted;
        private bool _colored;
        private List<Step> _steps;
        private readonly AQModuleDescription _aqmd;

        #endregion

        public Page()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            ColumnsPresenter.SelectionParameters = new OELParametersList();
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
        }

        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery(true);
        }

        private void RefreshQuery(bool full)
        {
            RefreshFlags();
            RefreshFilters(full);
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;
            var task = new Task(new[] { "Формирование списка полей", "Слияние полей" }, "Проверка условий", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _cs.BeginGetOELParametersList(_filters.ParametersFilter, GetOELParametersListDone, task);
        }

        private void RefreshFlags()
        {
            _probeProcessing = ProbeInColumns.IsChecked.HasValue && ProbeInColumns.IsChecked.Value;
            if (NoFormatButton.IsChecked != null) _formatted = !NoFormatButton.IsChecked.Value;
            if (ScaleButton.IsChecked != null) _colored = ScaleButton.IsChecked.Value;
            if (ShowFieldCombineEditorCheckBox.IsChecked != null)_showFvc = ShowFieldCombineEditorCheckBox.IsChecked.Value;
        }

        private void RefreshFilters(bool full)
        {
            _filters = full ? ColumnsPresenter.GetFilters() : ColumnsPresenter.UpdateFilters(_filters);
        }

        private void GetOELParametersListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetOELParametersList(iar);
            _parametersList = t.ParametersList;
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _cs.BeginGetOELCorrectionList(_filters.ParametersFilter, _parametersList, _probeProcessing, GetOELCorrectionListDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() => ColumnsPresenter.EnableFinishSelection(true));
        }

        private void GetOELCorrectionListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetOELCorrectionList(iar);
            _fieldListCombination = t.FieldListCombination;
            WorkParametersList.ParametersList = _parametersList; 
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
            }
            else
            {
                task.SetState(1, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() =>
            {
                EnableSaveAndExport(true);
                RefreshQueryButton.IsEnabled = true;
                ColumnsPresenter.EnableFinishSelection(true);
                MainFieldCombineView.PL = _parametersList;
                MainFieldCombineView.FLC = _fieldListCombination;
                MainFieldCombineView.Refresh();
                MainFieldCombineView.Visibility = _showFvc ? Visibility.Visible : Visibility.Collapsed;
            });
        }

        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery(false);
        }

        #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(() =>
            {
                XlsxExportButton.IsEnabled = allow;
                PreviewButton.IsEnabled = allow;
                SaveQueryButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
            });
        }

        #endregion

     
        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }
    }
}
