﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;

namespace SLConstructor_OEL
{
    public static class OELCalculationFactory
    {
        public static List<NewTableData> GetChangeFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Марка", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
      //          new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"}, 
                new NewTableData{OldFieldName = "Дата", NewFieldName = "Дата"}, 
            });
            
            if (!probeInColumns)
            {
                result.AddRange(new List<NewTableData> 
                { 
                    new NewTableData{OldFieldName = "Проба", NewFieldName = "Проба"}, 
                    new NewTableData{OldFieldName = "Тип испытания", NewFieldName = "Тип испытания"}
                }); 
                result.AddRange(from k in aqpl.AllParameters
                    where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                    select new NewTableData {NewFieldName = k.Param, OldFieldName = k.Param});
            }
            else
            {
                foreach (var k in aqpl.AllParameters)
                {
                    if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                    result.AddRange(probes.Select(p => string.Format("{0} ({1})", k.Param, p.Name))
                        .Select(
                            parameterName =>
                                new NewTableData {NewFieldName = parameterName, OldFieldName = parameterName}));
                }
            }
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(bool probeInColumns)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
    //            "Разливка",
                "Плавка",
                "Дата"
            });
            if (!probeInColumns)
            {
                result.AddRange(new List<string>
                { 
                    "Проба",
                    "Тип испытания"
                });
            }
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка",
                "Дата"
            });
            
            return result;
        }

        public static List<string> GetRightBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка"
            });
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(ParametersList aqpl, FieldListCombination flc, bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка",
                "Дата"
            });

            if (probeInColumns)
            {
                result.AddRange(from k in aqpl.AllParameters where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID) select String.Format("{0} ({1})", k.Param, probes.First().Name));
            }
            return result;
        }

        public static List<string> GetHorizontalBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
      //          "Разливка",
       //         "Плавка"
            });

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput(bool probeInColumn)
        {
            var result = new List<string>();
            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
    //            "Разливка",
            });
            if (!probeInColumn) result.Add("Плавка"); 
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(ParametersList aqpl, FieldListCombination flc,
            bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<ScaleRule>();

            if (!probeInColumns)
            {
                result.AddRange(from k in aqpl.AllParameters
                    where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                    select new ScaleRule
                    {
                        Mode = ScaleRuleMode.MinMax,
                        FieldName = k.Param,
                        Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                    });
            }
            else
            {
                foreach (var k in aqpl.AllParameters)
                {
                    if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                    result.AddRange(probes.Select(p => string.Format("{0} ({1})", k.Param, p.Name))
                        .Select(
                            parameterName =>
                                new ScaleRule
                                {
                                    Mode = ScaleRuleMode.MinMax,
                                    FieldName = parameterName,
                                    Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                    Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                    Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                                }));
                }
            }
            return result;
        }
    }
}