﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_OEL
{
    public partial class Page
    {
        private const string TableName = "Химический состав";

        #region Просмотр
        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            PreviewButton.IsEnabled = false;
            if (!_probeProcessing)
            {
                var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _sql = OELSelects.GetChemistrySQL(_filters, _parametersList, _fieldListCombination);
                CreateCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadDone, task);
            }
            else
            {
                var task = new Task(new[] { "Список проб", "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _cs.BeginGetProbes(_filters.WorkFilter, GetProbesDone, task);
            }
        }

        private void GetProbesDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetProbes(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _probeList = t.ProbeList;
                Dispatcher.BeginInvoke(() =>
                {
                    _sql = OELSelects.GetChemistrySQLLine(_filters, _parametersList, _fieldListCombination, _probeList);
                    CreateCalculation();
                    _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadDone, task);
                });
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void DataReadDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndExecuteSteps(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                {
                    var tables = _cas.EndGetTablesSLCompatible(i);
                    if (tables == null) return;
                    task.AdvanceProgress();
                    task.SetMessage("Данные готовы");
                    var ca = new Dictionary<string, object>
                    {
                        {"From", ColumnsPresenter.From}, 
                        {"To", ColumnsPresenter.To}, 
                        {"SourceData", tables},
                        {"Calculation", _steps},
                        {"Description", _filters.Description},
                        {"New", true},
                    };
                    Dispatcher.BeginInvoke(() =>
                    {
                        PreviewButton.IsEnabled = true;
                        Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                    });
                }, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
                Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
            }
        }

        #endregion

        #region Экспорт XLSX
        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            XlsxExportButton.IsEnabled = false;

            if (!_probeProcessing)
            {
                var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _sql = OELSelects.GetChemistrySQL(_filters, _parametersList, _fieldListCombination);
                task.CustomData = sender;
                CreateCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadForXlsxDone, task);

            }
            else
            {
                var task = new Task(new[] { "Список проб", "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
                task.SetState(0, TaskState.Processing);
                task.CustomData = sender;
                _cs.BeginGetProbes(_filters.WorkFilter, GetProbesDoneForXlsx, task);
            }
        }

        private void GetProbesDoneForXlsx(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetProbes(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _probeList = t.ProbeList;
                Dispatcher.BeginInvoke(() =>
                {
                    _sql = OELSelects.GetChemistrySQLLine(_filters, _parametersList, _fieldListCombination, _probeList);
                    CreateCalculation();
                    _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadForXlsxDone, task);
                });
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void DataReadForXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndExecuteSteps(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, CreateXlsxDone, task);
            }
            else
            {
                _cas.BeginDropData(t.TaskDataGuid, null, null);
                task.SetState(0 + ((_probeProcessing) ? 1 : 0), TaskState.Error);
                task.SetMessage(t.Message);
                Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
            }
        }

        private void CreateXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndGetAllResultsInXlsx(iar);
            task.AdvanceProgress();
            task.SetMessage(t.Message);

            Dispatcher.BeginInvoke(() =>
            {
                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                    string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}",
                    HttpUtility.UrlEncode(t.TaskDataGuid),
                    HttpUtility.UrlEncode(string.Format("Отчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null))),
                    HttpUtility.UrlEncode("application/msexcel"))));
                XlsxExportButton.IsEnabled = true;
            });

        }
        #endregion

        #region Сохранение запроса
        private void SaveQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            if (!_probeProcessing)
            {
                _sql = OELSelects.GetChemistrySQL(_filters, _parametersList, _fieldListCombination);
                SendSaveCommand();
            }
            else
            {
                SaveQueryButton.IsEnabled = false;
                var task = new Task(new[] { "Список проб" }, "Сохранение запроса", TaskPanel); task.SetState(0, TaskState.Processing);
                task.SetState(0, TaskState.Processing);
                task.CustomData = sender;
                _cs.BeginGetProbes(_filters.WorkFilter, GetProbesDoneForSave, task);
            }
        }

        private void GetProbesDoneForSave(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetProbes(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _probeList = t.ProbeList;
                Dispatcher.BeginInvoke(() =>
                {
                    _sql = OELSelects.GetChemistrySQLLine(_filters, _parametersList, _fieldListCombination, _probeList);
                    SaveQueryButton.IsEnabled = true;
                    SendSaveCommand();
                });
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void SendSaveCommand()
        {
            CreateCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", "Списание брака"},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation,
                signalArguments, null);
        }
        #endregion

        #region Начало расчета
        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            if (!_probeProcessing)
            {
                _sql = OELSelects.GetChemistrySQL(_filters, _parametersList, _fieldListCombination);
                OpenCalculation();
            }
            else
            {
                var task = new Task(new[] { "Список проб" }, "Начало расчета", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _cs.BeginGetProbes(_filters.WorkFilter, GetProbesDoneForBeginCalculation, task);
            }
        }

        private void GetProbesDoneForBeginCalculation(IAsyncResult iar)
        {
            var t = _cs.EndGetProbes(iar);
            var task = (Task)iar.AsyncState;
            if (t.Success)
            {
                _probeList = t.ProbeList;
                _sql = OELSelects.GetChemistrySQLLine(_filters, _parametersList, _fieldListCombination, _probeList);
                task.SetState(0, TaskState.Ready);
                Dispatcher.BeginInvoke(OpenCalculation);
            }
            else task.SetState(0, TaskState.Ready);

        }

        private void OpenCalculation()
        {
            Dispatcher.BeginInvoke(() =>
            {
                RefreshFlags();
                CreateCalculation();
                var signalArguments = new Dictionary<string, object>
                {
                    {"Name", "Списание брака"},
                    {"From", ColumnsPresenter.From},
                    {"To", ColumnsPresenter.To},
                    {"Calculation", _steps}
                };
                Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
            });
        }
        #endregion
        #region CreateCalculation

        private void CreateCalculation()
        {
            RefreshFilters(false);
            Services.GetAQService().BeginWriteLog("Calc: OEL, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);
            _steps = StepFactory.CreateSQLQuery(_sql, TableName);
            AppendDescriptionStep();
            if (_formatted) AppendFormatSteps();
            if (_colored) AppendScales();
        }

        private void AppendDescriptionStep()
        {
            _steps.Add(StepFactory.CreateConstants(new List<ConstantDescription>
            {
                new ConstantDescription{VariableName = "Описание", VariableType = "String", Value = _filters.Description},
                new ConstantDescription{VariableName = "Автор", VariableType = "String", Value = Security.CurrentUser},
                new ConstantDescription{VariableName = "Фиксированный столбец", VariableType = "Int32", Value =  _formatted ? (_probeProcessing ? 4: 6 ): 1}
            }));
        }

        private void AppendFormatSteps()
        {
            _steps.Add(StepFactory.CreateChangeFields(TableName, OELCalculationFactory.GetChangeFieldsFormattedOutput(_parametersList, _fieldListCombination, _probeProcessing, _probeList)));
            _steps.Add(StepFactory.CreateSorting(TableName, OELCalculationFactory.GetSortingFieldsFormattedOutput(_probeProcessing), true));
            _steps.Add(StepFactory.CreateFormatCombine(TableName, OELCalculationFactory.GetCombineFieldsFormattedOutput(_parametersList, _fieldListCombination), true));

            _steps.Add(StepFactory.CreateFormatBorders(TableName,
                  OELCalculationFactory.GetRightBordersFormattedOutput(),
                  OELCalculationFactory.GetLeftBordersFormattedOutput(_parametersList, _fieldListCombination, _probeProcessing, _probeList),
                  OELCalculationFactory.GetHorizontalBordersFormattedOutput(),
                  OELCalculationFactory.GetChangeBordersFormattedOutput(_probeProcessing)));
        }

        private void AppendScales()
        {
            _steps.Add(StepFactory.CreateFormatScale(TableName, OELCalculationFactory.GetScaleRulesFormattedOutput(_parametersList, _fieldListCombination, _probeProcessing, _probeList)));
        } 
        #endregion
    }
}
