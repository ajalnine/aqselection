﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using AQConstructorsLibrary;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;
using AQStepFactoryLibrary;

namespace SLConstructor_OEL
{
    public static class OELSelects
    {
        public static string GetChemistrySQL(Filters filter, ParametersList aqpl, FieldListCombination flc)
        {
            string sqlOther = String.Empty, havingOther = String.Empty;

            if (aqpl.AllParameters.Count <= 0) return sqlOther + " order by max(oel_head.test_date) ";
            sqlOther = @"SELECT max(oel_head.melt) as 'Плавка', 
                                max(oel_head.test_date) as 'Дата', 
                                max(oel_head.lab) as 'Лаборатория', 
                                max(Common_Mark.mark) as 'Марка', 
                                max(Common_NTD.ntd) as 'НТД', 
                                max(oel_probes.displayprobename) as 'Проба',  
                                max(oel_probetypes.type) as 'Тип испытания',  
                                max(oel_head.onrs) as 'Разливка' ";

            foreach (var k in aqpl.AllParameters)
            {
                if (k.IsNumber)
                {
                    if (!FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID))
                    {
                        if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                        {
                            sqlOther += string.Format(",max(case when tbl_Parameters.id='{0}' then  dbo.ToNumeric(oel_elements.value) else null end) as '{1}'\r\n", k.ParamID, k.Param);
                        }
                        else
                        {
                            sqlOther += string.Format(",max(dbo.MultipleToNumeric(oel_elements.value, tbl_Parameters.id , '{0}')) as '{1}'\r\n", FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param);
                        }
                    }
                    var hs = FilterUtility.GetValueFilterString( filter.ValueFilter, k.ParamID);
                    if (string.IsNullOrEmpty(hs)) continue;
                    if (havingOther != String.Empty) havingOther += " AND ";
                    havingOther += hs;
                }
                else
                {
                    if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                    if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                    {
                        sqlOther += string.Format(",max(case when tbl_Parameters.id='{0}' then oel_elements.value else null end) as '{1}'\r\n", k.ParamID, k.Param);
                    }
                    else
                    {
                        sqlOther += string.Format(",max(dbo.MultipleToString(oel_elements.value, tbl_Parameters.id , '{0}')) as '{1}'\r\n", FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param);
                    }
                }
            }

            sqlOther += string.Format(@" FROM oel_head INNER JOIN
                                  oel_elements ON oel_head.id_test = oel_elements.id_test left outer JOIN
                                  Common_Mark ON oel_head.id_cmark = Common_Mark.id left outer join
                                  Common_NTD ON oel_head.id_ntd = Common_NTD.id INNER JOIN
                                  tbl_Paramtype ON oel_head.id_paramtype = tbl_Paramtype.id INNER JOIN
                                  tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND oel_elements.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                                  oel_probes on oel_probes.id=oel_head.id_Probe LEFT OUTER JOIN
                                  oel_probetypes on oel_probetypes.id=oel_head.type
                                  where {0} AND {1} group by oel_head.id_test", Security.GetMarkSQLCheck("Контроль"), filter.SaveFilter);
            if (havingOther.Length > 0) sqlOther += " having " + havingOther;
            return sqlOther + " order by max(oel_head.test_date) ";
        }

        public static string GetChemistrySQLLine(Filters filter, ParametersList aqpl, FieldListCombination flc, List<ProbeDescription> probes)
        {
            string sqlOther = String.Empty, havingOther = String.Empty;

            if (aqpl.AllParameters.Count <= 0) return string.Format("{0}{1}", sqlOther, " order by max(oel_head.test_date) ");
            sqlOther = @"SELECT max(oel_head.melt) as 'Плавка', 
                                max(oel_head.test_date) as 'Дата', 
                                max(Common_Mark.mark) as 'Марка', 
                                max(Common_NTD.ntd) as 'НТД', 
                                max(oel_head.onrs) as 'Разливка' ";

            foreach (var k in aqpl.AllParameters)
            {
                foreach (var p in probes)
                {
                    if (k.IsNumber)
                    {
                        if (!FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID))
                        {
                            if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                            {
                                sqlOther += string.Format(",max(case when tbl_Parameters.id='{0}' and oel_probes.id={1} then  dbo.ToNumeric(oel_elements.value) else null end) as '{2} ({3})'\r\n", k.ParamID, p.ID, k.Param, p.Name);
                            }
                            else
                            {
                                sqlOther += string.Format(",max(case when oel_probes.id={0} then dbo.MultipleToNumeric(oel_elements.value, tbl_Parameters.id , \'{1}') else null end) as '{2} ({3})' \r\n", p.ID, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, p.Name);
                            }
                        }
                        var hs = FilterUtility.GetValueFilterString(filter.ValueFilter, k.ParamID);
                        if (string.IsNullOrEmpty(hs)) continue;
                        if (havingOther != String.Empty) havingOther += " AND ";
                        havingOther += hs;
                    }
                    else
                    {
                        if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                        if (!FieldListOperations.IsRequireToCombine(flc.FieldListCombined, k.ParamID))
                        {
                            sqlOther += string.Format(",max(case when tbl_Parameters.id='{0}'  and oel_probes.id={1} then oel_elements.value else null end) as \'{2} ({3})'\r\n", k.ParamID, p.ID, k.Param, p.Name);
                        }
                        else
                        {
                            sqlOther += string.Format(",max(case when oel_probes.id={0} then dbo.MultipleToString(oel_elements.value, tbl_Parameters.id , \'{1}') else null end) as '{2} ({3})' \r\n", p.ID, FieldListOperations.GetCombineString(flc.FieldListCombined, k.ParamID), k.Param, p.Name);
                        }
                    }
                }
            }

            sqlOther += string.Format(@" FROM oel_head INNER JOIN
                                  oel_elements ON oel_head.id_test = oel_elements.id_test left outer JOIN
                                  Common_Mark ON oel_head.id_cmark = Common_Mark.id left outer join
                                  Common_NTD ON oel_head.id_ntd = Common_NTD.id INNER JOIN
                                  tbl_Paramtype ON oel_head.id_paramtype = tbl_Paramtype.id INNER JOIN
                                  tbl_Parameters ON tbl_Paramtype.id = tbl_Parameters.id_paramtype AND oel_elements.id_parameter = tbl_Parameters.id LEFT OUTER JOIN
                                  oel_probes on oel_probes.id=oel_head.id_Probe LEFT OUTER JOIN
                                  oel_probetypes on oel_probetypes.id=oel_head.type
                                  where {0} AND {1} group by oel_head.melt", Security.GetMarkSQLCheck("Контроль"), filter.SaveFilter);
            if (havingOther.Length > 0) sqlOther += " having " + havingOther;
            return sqlOther + " order by max(oel_head.test_date) ";
        }

        #region Create Step Parameters

        public static List<NewTableData> GetChangeFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc, bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<NewTableData>();

            result.AddRange(new List<NewTableData> 
            { 
                new NewTableData{OldFieldName = "Марка", NewFieldName = "Марка"}, 
                new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
      //          new NewTableData{OldFieldName = "Разливка", NewFieldName = "Разливка"}, 
                new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"}, 
                new NewTableData{OldFieldName = "Дата", NewFieldName = "Дата"}, 
            });
            
            if (!probeInColumns)
            {
                result.AddRange(new List<NewTableData> 
                { 
                    new NewTableData{OldFieldName = "Проба", NewFieldName = "Проба"}, 
                    new NewTableData{OldFieldName = "Тип испытания", NewFieldName = "Тип испытания"}
                }); 
                result.AddRange(from k in aqpl.AllParameters
                    where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                    select new NewTableData {NewFieldName = k.Param, OldFieldName = k.Param});
            }
            else
            {
                foreach (var k in aqpl.AllParameters)
                {
                    if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                    result.AddRange(probes.Select(p => string.Format("{0} ({1})", k.Param, p.Name))
                        .Select(
                            parameterName =>
                                new NewTableData {NewFieldName = parameterName, OldFieldName = parameterName}));
                }
            }
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(bool probeInColumns)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
    //            "Разливка",
                "Плавка",
                "Дата"
            });
            if (!probeInColumns)
            {
                result.AddRange(new List<string>
                { 
                    "Проба",
                    "Тип испытания"
                });
            }
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(ParametersList aqpl, FieldListCombination flc)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка",
                "Дата"
            });
            
            return result;
        }

        public static List<string> GetRightBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка"
            });
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(ParametersList aqpl, FieldListCombination flc, bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
     //           "Разливка",
                "Плавка",
                "Дата"
            });

            if (probeInColumns)
            {
                result.AddRange(from k in aqpl.AllParameters where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID) 
                                select String.Format("{0} ({1})", k.Param, probes.First().Name));
            }
            return result;
        }

        public static List<string> GetHorizontalBordersFormattedOutput()
        {
            var result = new List<string>();

            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
      //          "Разливка",
       //         "Плавка"
            });

            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput(bool probeInColumn)
        {
            var result = new List<string>();
            result.AddRange(new List<string>
            {
                "Марка",
                "НТД",
    //            "Разливка",
            });
            if (!probeInColumn) result.Add("Плавка"); 
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(ParametersList aqpl, FieldListCombination flc,
            bool probeInColumns, List<ProbeDescription> probes)
        {
            var result = new List<ScaleRule>();

            if (!probeInColumns)
            {
                result.AddRange(from k in aqpl.AllParameters
                    where !FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)
                    select new ScaleRule
                    {
                        Mode = ScaleRuleMode.MinMax,
                        FieldName = k.Param,
                        Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                    });
            }
            else
            {
                foreach (var k in aqpl.AllParameters)
                {
                    if (FieldListOperations.IsCombined(flc.FieldListCombined, k.ParamID)) continue;
                    result.AddRange(probes.Select(p => string.Format("{0} ({1})", k.Param, p.Name))
                        .Select(
                            parameterName =>
                                new ScaleRule
                                {
                                    Mode = ScaleRuleMode.MinMax,
                                    FieldName = parameterName,
                                    Color1 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                                    Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                                    Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
                                }));
                }
            }
            return result;
        }

        #endregion
    }
}