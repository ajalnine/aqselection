﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

// ReSharper disable once CheckNamespace
namespace ControlsLibrary
{
    public partial class TaskStatePresenter : UserControl
    {
        public bool IsFinished;

        public TaskStatePresenter()
        {
            InitializeComponent();
        }

        public void SetTaskSteps(string[] steps, string taskName)
        {
            //TaskSteps.Children.Clear();
            var index = 0;
            foreach (var s in steps)
            {
                var tsi = new TaskStateItem {TaskName = s, State = TaskState.Inactive};
                TaskSteps.Children.Insert(index, tsi);
                index++;
            }
            TaskNameLabel.Text = taskName;
        }

        public void SetTaskState(int number, TaskState state)
        {
            var currentItem = TaskSteps.Children[number] as TaskStateItem;
            if (currentItem != null) currentItem.State = state;
            RefreshGlobalState();
        }

        public TaskState GetTaskState(int number)
        {
            var currentItem = TaskSteps.Children[number] as TaskStateItem;
            if (currentItem != null) return currentItem.State;
            throw new Exception();
        }

        public int GetTaskCount()
        {
            return TaskSteps.Children.Count;
        }

        public void SetTaskMessage(string message)
        {
            MessageLabel.Text = message;
        }

        private void RefreshGlobalState()
        {
            var hasErrors = false;
            var processed = true;
            foreach (var uiElement in TaskSteps.Children.OfType<UserControl>().Where(a => a is TaskStateItem))
            {
                var currentItem = (TaskStateItem) uiElement;
                if (currentItem.State == TaskState.Error) hasErrors = true;
                if (currentItem.State != TaskState.Ready) processed = false;
            }
            if (hasErrors)
            {
                StatusLabel.Text = "Ошибка";
                GlobalState.Visibility = Visibility.Visible;
                HighLight.Color = Color.FromArgb(0xcf, 0xef, 0x68, 0x68);
                IsFinished = true;
                StopTask.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (processed)
                {
                    StatusLabel.Text = "Удачно завершено";
                    GlobalState.Visibility = Visibility.Visible;
                    HighLight.Color = Color.FromArgb(0xcf, 0x64, 0xef, 0x15);
                    IsFinished = true;
                    StopTask.Visibility = Visibility.Collapsed;
                }
                else
                {
                    StatusLabel.Text = String.Empty;
                    GlobalState.Visibility = Visibility.Visible;
                    HighLight.Color = Color.FromArgb(0xcf, 0xe0, 0xe0, 0xe0);
                    IsFinished = false;
                    StopTask.Visibility = Visibility.Collapsed; // Visibility.Visible;
                }
            }
        }

        private void StopTask_Click(object sender, RoutedEventArgs e)
        {
        }
    }

    public class TaskStateEventArgs : EventArgs
    {
        public TaskStateEventArgs(object o)
        {
            State = o;
        }

        public object State { get; set; }
    }
}