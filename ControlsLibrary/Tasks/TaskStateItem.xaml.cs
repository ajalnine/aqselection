﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

// ReSharper disable once CheckNamespace
namespace ControlsLibrary
{
    public partial class TaskStateItem
    {
        public static readonly DependencyProperty TaskNameProperty =
            DependencyProperty.Register("TaskName", typeof (string), typeof (TaskStateItem),
                                        new PropertyMetadata(String.Empty));

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("State", typeof (TaskState), typeof (TaskStateItem), new PropertyMetadata(null));

        public TaskStateItem()
        {
            InitializeComponent();
        }

        public string TaskName
        {
            get { return (string) GetValue(TaskNameProperty); }
            set
            {
                SetValue(TaskNameProperty, value);
                TaskNameLabel.Text = value;
            }
        }


        public TaskState State
        {
            get { return (TaskState) GetValue(StateProperty); }
            set
            {
                SetValue(StateProperty, value);
                SetState(value);
            }
        }

        private void SetState(TaskState newState)
        {
            switch (newState)
            {
                case TaskState.Inactive:
                    HighLight.Color = Color.FromArgb(0xcf, 0xdf, 0xdf, 0xdf);
                    TaskNameLabel.Foreground = new SolidColorBrush(Color.FromArgb(0xcf, 0x40, 0x40, 0x40));
                    HexFigure.Stroke = new SolidColorBrush(Color.FromArgb(0x00, 0xff, 0xff, 0xff));
                    HexFigure.StrokeDashArray = null;
                    HexFigure.Effect = null;
                    break;

                case TaskState.Processing:
                    HighLight.Color = Color.FromArgb(0xcf, 0xef, 0x68, 0xef);
                    TaskNameLabel.Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0x00, 0x00));
                    HexFigure.Stroke = new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0x00, 0x00));

                    var stroke = new DoubleCollection {10, 10};
                    HexFigure.StrokeDashArray = stroke;
                    HexFigure.Effect = new DropShadowEffect{BlurRadius = 5, Color = Colors.Black, Direction = 300, Opacity = 0.3, ShadowDepth = 3};

                    var strokeAnimation = new DoubleAnimation {From = 0, To = 100};

                    var sb = new Storyboard {Duration = new Duration(new TimeSpan(0, 0, 1))};

                    sb.Children.Add(strokeAnimation);

                    Storyboard.SetTarget(strokeAnimation, HexFigure);

                    Storyboard.SetTargetProperty(strokeAnimation, new PropertyPath("(StrokeDashOffset)"));
                    sb.Begin();
                    sb.RepeatBehavior = new RepeatBehavior(1000);
                    break;

                case TaskState.Ready:
                    HighLight.Color = Color.FromArgb(0xcf, 0x53, 0xe7, 0xff);
                    TaskNameLabel.Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0x00, 0x00));
                    HexFigure.Stroke = new SolidColorBrush(Color.FromArgb(0x00, 0xff, 0xff, 0xff));
                    HexFigure.StrokeDashArray = null;
                    HexFigure.Effect = null;
                    break;

                case TaskState.Error:
                    HighLight.Color = Color.FromArgb(0xcf, 0xef, 0x38, 0x38);
                    TaskNameLabel.Foreground = new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0x00, 0x00));
                    HexFigure.Stroke = new SolidColorBrush(Color.FromArgb(0x00, 0xff, 0xff, 0xff));
                    HexFigure.StrokeDashArray = null;
                    HexFigure.Effect = null;
                    break;
            }
        }
    }
}