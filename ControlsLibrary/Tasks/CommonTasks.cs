﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using BasicControlsLibrary;
using Core;
using Core.ReportingServiceReference;

// ReSharper disable once CheckNamespace
namespace ControlsLibrary
{
    public static class CommonTasks
    {
        /*

        public static void SendPNGFile(UIElement sender, WriteableBitmap wb, StackPanel taskView, string prefix, Action callback)
        {
            var task = new Task(new[] {"Сжатие изображения", "Отправка PNG"}, "Экспорт в PNG", taskView);
            task.StartTask();

            var task2Guid = Guid.NewGuid();
            var rdsc = Services.GetReportingService();

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task2Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  task.AdvanceProgress();
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           @"GetFile.aspx?FileName=" +
                                                                           HttpUtility.UrlEncode(
                                                                               sr.ResultData.ToString()) + "&FName=" +
                                                                           HttpUtility.UrlEncode(prefix + " " +
                                                                                                 DateTime.Now.Ticks +
                                                                                                 ".png") +
                                                                           "&ContentType=" +
                                                                           HttpUtility.UrlEncode("image/png")));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  task.StopTask(sr.Message);
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });

            var bw = new BackgroundWorker();
            bw.DoWork += (s, ebw) =>
                {
                    var png = PNG.WriteableBitmapToPNG(wb).ToList();
                    ebw.Result = png;
                };
            bw.RunWorkerCompleted += (s, ebw) =>
                {
                    task.AdvanceProgress();

                    rdsc.SendBinaryAsync(task2Guid, (List<byte>) ebw.Result);
                };
            bw.RunWorkerAsync();
        }

        public static void SendReadyPNGFile(UIElement sender, List<byte> png, string prefix, Action callback)
        {
            ReportingDuplexServiceClient rdsc = Services.GetReportingService();

            Guid task1Guid = Guid.NewGuid();

            rdsc.SendBinaryAsync(task1Guid, png);

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           @"GetFile.aspx?FileName=" +
                                                                           HttpUtility.UrlEncode(
                                                                               sr.ResultData.ToString()) + "&FName=" +
                                                                           HttpUtility.UrlEncode(prefix + " " +
                                                                                                 DateTime.Now.Ticks +
                                                                                                 ".png") +
                                                                           "&ContentType=" +
                                                                           HttpUtility.UrlEncode("image/png")));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });
        }

        public static void CreatePPTX(UIElement sender, PresentationDescription pd, string prefix, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           @"GetFile.aspx?FileName=" +
                                                                           HttpUtility.UrlEncode(
                                                                               sr.ResultData.ToString()) + "&FName=" +
                                                                           HttpUtility.UrlEncode(prefix + " " +
                                                                                                 DateTime.Now.Ticks +
                                                                                                 ".pptx") +
                                                                           "&ContentType=" +
                                                                           HttpUtility.UrlEncode(
                                                                               "application/mspowerpoint")));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });

            rdsc.GetPPTXAsync(task1Guid, pd);
        }

        public static void CreateDOCX(UIElement sender, PresentationDescription pd, string prefix, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}",
                                                                           HttpUtility.UrlEncode(sr.ResultData.ToString()), 
                                                                           HttpUtility.UrlEncode(string.Format("{0} {1}.docx", prefix, DateTime.Now.Ticks)), HttpUtility.UrlEncode("application/msword"))));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });

            rdsc.GetDOCXAsync(task1Guid, pd);
        }
        */
    }
}