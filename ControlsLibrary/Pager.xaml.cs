﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace ControlsLibrary
{
    public partial class Pager : UserControl
    {
        public delegate void CurrentPageChangedDelegate(object o, EventArgs e);
        public event CurrentPageChangedDelegate CurrentPageChanged;

        public int TotalRecords
        {
            get { return (int)GetValue(TotalRecordsProperty); }
            set { SetValue(TotalRecordsProperty, value); }
        }
        public static readonly DependencyProperty TotalRecordsProperty =
            DependencyProperty.Register("TotalRecords", typeof(int), typeof(Pager), new PropertyMetadata(0, TotalRecordsPropertyChanged));

        public int PageSize
        {
            get { return (int)GetValue(PageSizeProperty); }
            set { SetValue(PageSizeProperty, value); }
        }
        public static readonly DependencyProperty PageSizeProperty =
            DependencyProperty.Register("PageSize", typeof(int), typeof(Pager), new PropertyMetadata(0, PageSizePropertyChanged));

        public int CurrentPage
        {
            get { return (int)GetValue(CurrentPageProperty); }
            set { SetValue(CurrentPageProperty, value); }
        }
        public static readonly DependencyProperty CurrentPageProperty =
            DependencyProperty.Register("CurrentPage", typeof(int), typeof(Pager), new PropertyMetadata(0, CurrentPagePropertyChanged));

        public int GroupSize
        {
            get { return (int)GetValue(GroupSizeProperty); }
            set { SetValue(GroupSizeProperty, value); }
        }
        public static readonly DependencyProperty GroupSizeProperty =
            DependencyProperty.Register("GroupSize", typeof(int), typeof(Pager), new PropertyMetadata(10, new PropertyChangedCallback(GroupSizePropertyChanged) ));

        private int NumberOfPages;
        private int FirstRecordOnCurrentPage;
        private int FirstPageInCurrentGroup;
        private int LastPageInCurrentGroup;

        public Pager()
        {
            InitializeComponent();
        }

        private static void TotalRecordsPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            Pager This = (o as Pager);
            This.RecalcNumberOfPages();
            This.RefreshControlView();
        }

        private static void PageSizePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            Pager This = (o as Pager);
            This.RecalcNumberOfPages();
            This.RefreshControlView();
        }

        private static void CurrentPagePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            Pager This = (o as Pager);
            This.RefreshControlView();
            This.FirstRecordOnCurrentPage = This.CurrentPage * This.PageSize;
            if (This.CurrentPageChanged!=null)This.CurrentPageChanged.Invoke(This, new EventArgs());
        }
        
        private static void GroupSizePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            Pager This = (o as Pager);
            This.RefreshControlView();
        }
        
        private void RefreshControlView()
        {
            if (NumberOfPages==1)
            {
                PageSelectorPanel.Visibility = Visibility.Collapsed;
                return;
            }
            PageSelectorPanel.Visibility = Visibility.Visible;
            FirstPageInCurrentGroup = (CurrentPage / GroupSize) * GroupSize;
            int EndOfGroup = FirstPageInCurrentGroup + GroupSize - 1;
            LastPageInCurrentGroup = (EndOfGroup < NumberOfPages) ? EndOfGroup : NumberOfPages-1 ;
            
            PageSelectorPanel.Children.Clear();

            if (FirstPageInCurrentGroup > 0)
            {
                Button PreviousButton = new Button();
                PreviousButton.Style = Resources["ButtonBorderStyle"] as Style;
                TextBlock tb = new TextBlock();
                tb.Text = ("...").ToString();
                tb.Margin = new Thickness(2, 0, 2, 0);
                tb.Padding = new Thickness(3);
                PreviousButton.Content = tb;
                PreviousButton.Click += PreviousGroup_Click;
                PageSelectorPanel.Children.Add(PreviousButton);
            }

            for (int i = FirstPageInCurrentGroup; i <= LastPageInCurrentGroup; i++)
            {
                RadioButton pi = new RadioButton();
                pi.Style = Resources["RadioButtonBorderStyle"] as Style;
                TextBlock tb = new TextBlock();
                tb.Text = (i+1).ToString();
                tb.Margin = new Thickness(2, 0, 2, 0);
                tb.Padding = new Thickness(3);
                tb.VerticalAlignment = VerticalAlignment.Center;
                if (i == CurrentPage) pi.IsChecked = true;
                pi.Tag = i;
                pi.Content = tb;
                pi.VerticalContentAlignment = VerticalAlignment.Center;
                pi.Checked += new RoutedEventHandler(pi_Checked);
                PageSelectorPanel.Children.Add(pi);
            }

            if (LastPageInCurrentGroup < NumberOfPages - 1)
            {
                Button NextButton = new Button();
                NextButton.Style = Resources["ButtonBorderStyle"] as Style;
                TextBlock tb = new TextBlock();
                tb.Text = ("...").ToString();
                tb.Margin = new Thickness(2, 0, 2, 0);
                tb.Padding = new Thickness(3);
                NextButton.Content = tb;
                NextButton.Click += NextGroup_Click;
                PageSelectorPanel.Children.Add(NextButton);
            }
        }

        void pi_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton pi = sender as RadioButton;
            CurrentPage = (int)pi.Tag;
        }

        private void RecalcNumberOfPages()
        {
            NumberOfPages = (int)Math.Ceiling((double)TotalRecords / (double)PageSize);
        }

        private void PreviousGroup_Click(object sender, RoutedEventArgs e)
        {
            CurrentPage = (CurrentPage / GroupSize) * GroupSize - 1;
        }

        private void NextGroup_Click(object sender, RoutedEventArgs e)
        {
            CurrentPage = (CurrentPage / GroupSize) * GroupSize + GroupSize;
        }
    }
}
