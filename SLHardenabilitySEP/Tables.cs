﻿using System.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Data;
using System.Windows.Threading;
using AQConstructorsLibrary;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using ApplicationCore;
using AQCalculationsLibrary;
using AQMathClasses;
using ApplicationCore.AQServiceReference;
using ApplicationCore.ReportingServiceReference;
using ApplicationCore.CalculationServiceReference;


namespace SLHardenabilitySEP
{
    public partial class MainPage : UserControl, IAQModule
    {
        private List<List<double>> PointList = new List<List<double>>()
        {
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20, 25, 30 },
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20, 25, 30, 35, 40 },
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20, 25 },
            new List<double> { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20},
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20},
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20, 25, 30, 35, 40, 45, 50 },
            new List<double> { 1.5, 3, 5, 7, 9, 11, 13, 15, 20, 25, 30, 35, 40, 45}
        };

        Dictionary<string, List<double>> MinAlowed = new Dictionary<string, List<double>>();
        Dictionary<string, List<double>> MaxAlowed = new Dictionary<string, List<double>>();
        
        public void InitTablesWithAllowed()
        {
            MinAlowed = new Dictionary<string, List<double>>();
            MaxAlowed = new Dictionary<string, List<double>>();

            MinAlowed.Add("C", new List<double>()  {0.220, 0.160, 0.130, 0.320, 0.166, 0.091, 0.100, 0.227 });
            MaxAlowed.Add("C", new List<double>()  {0.470, 0.460, 0.231, 0.560, 0.280, 0.240, 0.230, 0.420 });
            MinAlowed.Add("Si", new List<double>() {0.020, 0.100, 0.020, 0.110, 0.030, 0.030, 0.150, 0.180 });
            MaxAlowed.Add("Si", new List<double>() {0.360, 0.350, 0.380, 0.320, 0.360, 0.063, 0.350, 0.440 });
            MinAlowed.Add("Mn", new List<double>() {0.590, 0.580, 1.020, 0.500, 0.610, 0.420, 0.410, 1.020 });
            MaxAlowed.Add("Mn", new List<double>() {0.970, 0.920, 1.480, 0.900, 1.020, 1.100, 1.090, 1.660 });
            MinAlowed.Add("P", new List<double>()  {0.005, 0.005, 0.006, 0.004, 0.004, 0.005, 0.004, 0.008 });
            MaxAlowed.Add("P", new List<double>()  {0.037, 0.028, 0.033, 0.033, 0.049, 0.036, 0.025, 0.037 });
            MinAlowed.Add("S", new List<double>()  {0.003, 0.003, 0.002, 0.005, 0.002, 0.001, 0.001, 0.003 });
            MaxAlowed.Add("S", new List<double>()  {0.038, 0.059, 0.044, 0.042, 0.049, 0.052, 0.060, 0.038 });
            MinAlowed.Add("Cr", new List<double>() {0.800, 0.810, 0.820, 0.030, 0.350, 0.410, 0.740, 0.260 });
            MaxAlowed.Add("Cr", new List<double>() {1.240, 1.220, 1.290, 0.290, 1.040, 1.920, 2.030, 0.620 });
            MinAlowed.Add("Mo", new List<double>() {0.005, 0.120, 0.010, 0.005, 0.150, 0.071, 0.004, 0.006 });
            MaxAlowed.Add("Mo", new List<double>() {0.090, 0.280, 0.090, 0.090, 0.490, 0.560, 0.100, 0.080 });
            MinAlowed.Add("Ni", new List<double>() {0.010, 0.050, 0.020, 0.030, 0.020, 0.400, 0.800, 0.020 });
            MaxAlowed.Add("Ni", new List<double>() {0.280, 0.340, 0.300, 0.200, 0.700, 1.800, 2.010, 0.220 });
            MinAlowed.Add("Al", new List<double>() {0.012, 0.006, 0.012, 0.015, 0.013, 0.012, 0.018, 0.021 });
            MaxAlowed.Add("Al", new List<double>() {0.062, 0.340, 0.063, 0.052, 0.069, 0.052, 0.058, 0.080 });
            MinAlowed.Add("Cu", new List<double>() {0.017, 0.050, 0.040, 0.029, 0.010, 0.010, 0.010, 0.010 });
            MaxAlowed.Add("Cu", new List<double>() {0.320, 0.490, 0.350, 0.340, 0.310, 0.310, 0.400, 0.250 });
            MinAlowed.Add("N", new List<double>()  {0.006, 0.005, 0.006, 0.005, 0.004, 0.005, 0.005, 0.0025});
            MaxAlowed.Add("N", new List<double>()  {0.014, 0.021, 0.018, 0.015, 0.019, 0.016, 0.016, 0.011 });
            MinAlowed.Add("B", new List<double>()  {0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0016});
            MaxAlowed.Add("B", new List<double>()  {0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0016, 0.0044});
            MinAlowed.Add("Ti", new List<double>() {0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010 });
            MaxAlowed.Add("Ti", new List<double>() {0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.010, 0.060 });
        }
    }
}
