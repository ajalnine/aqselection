﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_ParameterDoubleSlider
{
    public class Parameters
    {
        public string InputName = "InputDoubleNumberViaRangeSlider";
        public string Caption;
        public string VariableStart;
        public string VariableEnd;
        public double Min;
        public double Max;
        public double GreenStart;
        public double GreenEnd;
        public double Step;
        public double DefaultStart;
        public double DefaultEnd;
    }
}
