﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterDoubleSlider
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        DataItem CurrentTable;
        private CalculationService cs;

        public Page()
        {
            InitializeComponent();
            cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { Caption = "Числовой интервал", DefaultStart = 0, DefaultEnd = 100, GreenEnd = 100, GreenStart=0, Min = 0, Max = 100, VariableStart = "X1", VariableEnd = "X2", Step = 1 };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData;
            CurrentTable = DataSources.Where(s => s.Name == "Переменные").SingleOrDefault();
            PCaption.Text = CurrentParameters.Caption;
            PDefaultStart.Text = CurrentParameters.DefaultStart.ToString();
            PDefaultEnd.Text = CurrentParameters.DefaultEnd.ToString();
            PGreenStart.Text = CurrentParameters.GreenStart.ToString();
            PGreenEnd.Text = CurrentParameters.GreenEnd.ToString();
            PMax.Text = CurrentParameters.Max.ToString();
            PMin.Text = CurrentParameters.Min.ToString();
            PStep.Text = CurrentParameters.Step.ToString();
            PVariable.Text = CurrentParameters.VariableStart;
            PVariable2.Text = CurrentParameters.VariableEnd;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            string Name = "@" + CurrentParameters.VariableStart + "=" + CurrentParameters.DefaultStart.ToString();
            Name += "\r\n@" + CurrentParameters.VariableEnd + "=" + CurrentParameters.DefaultEnd.ToString();
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult())
            {
                UpdateParameters();
                this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
            }
        }

        private bool CheckResult()
        {
            List<string> ResultingNames = CurrentTable.Fields.Select(s => s.Name).ToList();

            if (PVariable.Text == string.Empty)
            {
                ShowError(true, "Не задано имя переменной");
                return false;
            }

            if (ResultingNames.Contains(PVariable.Text))
            {
                ShowError(true, "Переменная с таким именем уже есть");
                return false;
            }

            if (PCaption.Text == string.Empty)
            {
                ShowError(true, "Не задано название поля ввода");
                return false;
            }

            double min, max, def1, def2, step, green1, green2;
            if (!Double.TryParse(PMin.Text, out min))
            {
                ShowError(true, "Минимум - обязательный параметр");
                return false;
            }

            if (!Double.TryParse(PMax.Text, out max))
            {
                ShowError(true, "Максимум - обязательный параметр");
                return false;
            }
            if (!Double.TryParse(PDefaultStart.Text, out def1))
            {
                ShowError(true, "Значение по умолчанию обязательно должно быть указано");
                return false;
            }
            if (!Double.TryParse(PDefaultEnd.Text, out def2))
            {
                ShowError(true, "Оба значения по умолчанию обязательно должны быть указаны");
                return false;
            }
            if (!Double.TryParse(PGreenStart.Text, out green1))
            {
                ShowError(true, "Начало рекомендуемого интервала обязательно должно быть указано");
                return false;
            }
            if (!Double.TryParse(PGreenEnd.Text, out green2))
            {
                ShowError(true, "Конец рекомендуемого интервала обязательно должен быть указан");
                return false;
            } 
            if (!Double.TryParse(PStep.Text, out step))
            {
                ShowError(true, "Шаг обязательно должен быть указан");
                return false;
            }

            if (max < min)
            {
                ShowError(true, "Верхний предел меньше нижнего");
                return false;
            }

            if ((max - min) / step < 1)
            {
                ShowError(true, "Шаг больше диапазона");
                return false;
            }

            ShowError(false, string.Empty);
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void UpdateParameters()
        {
            CurrentParameters.Caption = PCaption.Text;
            CurrentParameters.VariableStart = PVariable.Text;
            CurrentParameters.VariableEnd = PVariable2.Text;
            try
            {
                CurrentParameters.Min = Double.Parse(PMin.Text);
                CurrentParameters.Max = Double.Parse(PMax.Text);
                CurrentParameters.DefaultStart = Double.Parse(PDefaultStart.Text);
                CurrentParameters.DefaultEnd = Double.Parse(PDefaultEnd.Text);
                CurrentParameters.GreenStart = Double.Parse(PGreenStart.Text);
                CurrentParameters.GreenEnd = Double.Parse(PGreenEnd.Text);
                CurrentParameters.Step = Double.Parse(PStep.Text);
            }
            catch { };
        }

        public void DataRequestReadyCallback(string fileName) { }
    }
}