﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterDateRange
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private string Selected = String.Empty;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { Mode = (Selected==String.Empty) ? "7 дней" : Selected, Locked = false };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            var ToCheck = (from r in RadioPanel.Children where ((Control)r).Tag!=null && ((Control)r).Tag.ToString() == CurrentParameters.Mode select r).SingleOrDefault();
            if (ToCheck != null) ((RadioButton)ToCheck).IsChecked = true;
            else ((RadioButton)RadioPanel.Children[0]).IsChecked = true;
            Locked.IsChecked = CurrentParameters.Locked;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            string Name = "Период: " + CurrentParameters.Mode;
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private bool CheckResult()
        {
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void UpdateParameters()
        {
            CurrentParameters.Mode = Selected;
            CurrentParameters.Locked = Locked.IsChecked.Value;
        }

        public void DataRequestReadyCallback(string fileName) { }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton r = ((RadioButton)sender);
            if (r == null || r.Tag == null) Selected = "7 дней";
            else Selected = ((RadioButton)sender).Tag.ToString();
        }
    }
}