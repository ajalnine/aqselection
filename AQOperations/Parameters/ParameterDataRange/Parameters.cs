﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_ParameterDateRange
{
    public class Parameters
    {
        public string InputName = "InputDateRange";
        public string Caption = "DateRange";
        public string Mode;
        public bool Locked = false;
    }
}
