﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_ParameterRadio
{
    public class Parameters
    {
        public string InputName = "InputRadio";
        public string Caption;
        public string List;
        public string Variable;
    }
}
