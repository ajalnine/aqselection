﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_ParameterSlider
{
    public class Parameters
    {
        public string InputName = "InputSingleNumberViaSlider";
        public string Caption;
        public string Variable;
        public double Min;
        public double Max;
        public double Step;
        public double Default;
    }
}
