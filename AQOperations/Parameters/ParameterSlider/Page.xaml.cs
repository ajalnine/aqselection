﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterSlider
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        DataItem CurrentTable;
        private CalculationService cs;

        public Page()
        {
            InitializeComponent();
            cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { Caption = "Числовой параметр", Default = 0, Min = 0, Max = 100, Variable = "X", Step = 1 };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData;
            CurrentTable = DataSources.Where(s => s.Name == "Переменные").SingleOrDefault();
            PCaption.Text = CurrentParameters.Caption;
            PDefault.Text = CurrentParameters.Default.ToString();
            PMax.Text = CurrentParameters.Max.ToString();
            PMin.Text = CurrentParameters.Min.ToString();
            PStep.Text = CurrentParameters.Step.ToString();
            PVariable.Text = CurrentParameters.Variable;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            string Name = "@" + CurrentParameters.Variable + "=" + CurrentParameters.Default.ToString();
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult())
            {
                UpdateParameters();
                this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
            }
        }

        private bool CheckResult()
        {
            List<string> ResultingNames = CurrentTable.Fields.Select(s => s.Name).ToList();

            if (PVariable.Text == string.Empty)
            {
                ShowError(true, "Не задано имя переменной");
                return false;
            }

            if (ResultingNames.Contains(PVariable.Text))
            {
                ShowError(true, "Переменная с таким именем уже есть");
                return false;
            }

            if (PCaption.Text == string.Empty)
            {
                ShowError(true, "Не задано название поля ввода");
                return false;
            }

            double min, max, def, step;
            if (!Double.TryParse(PMin.Text, out min))
            {
                ShowError(true, "Минимум - обязательный параметр");
                return false;
            }

            if (!Double.TryParse(PMax.Text, out max))
            {
                ShowError(true, "Максимум - обязательный параметр");
                return false;
            }
            if (!Double.TryParse(PDefault.Text, out def))
            {
                ShowError(true, "Значение по умолчанию обязательно должно быть указано");
                return false;
            }
            if (!Double.TryParse(PStep.Text, out step))
            {
                ShowError(true, "Шаг обязательно должен быть указан");
                return false;
            }

            if (max < min)
            {
                ShowError(true, "Верхний предел меньше нижнего");
                return false;
            }

            if ((max - min)/step < 1)
            {
                ShowError(true, "Шаг больше диапазона");
                return false;
            }

            ShowError(false, string.Empty);
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void UpdateParameters()
        {
            CurrentParameters.Caption = PCaption.Text;
            CurrentParameters.Variable = PVariable.Text;
            try
            {
                CurrentParameters.Min = Double.Parse(PMin.Text);
                CurrentParameters.Max = Double.Parse(PMax.Text);
                CurrentParameters.Default = Double.Parse(PDefault.Text);
                CurrentParameters.Step = Double.Parse(PStep.Text);
            }
            catch { };
        }

        public void DataRequestReadyCallback(string fileName) { }
    }
}