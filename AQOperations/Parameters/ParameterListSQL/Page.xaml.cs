﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterListSQL
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private CalculationService cs;
        private List<DataItem> QueryResult;
        private List<DataItem> DataSources;
        private DataItem CurrentTable;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml != null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(Parameters));
                StringReader sr = new StringReader(parametersXml);
                CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            }
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters();
            CurrentParameters.SQL = string.Empty;
            CurrentParameters.Caption = "Список";
            CurrentParameters.Variable = "X";
            CurrentParameters.VariableCaption = "Xname";
            CurrentParameters.IsSingle = false;
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            CurrentTable = availableData.Where(s => s.Name == "Переменные").SingleOrDefault();
            SQL.Text = CurrentParameters.SQL;
            PVariable.Text = CurrentParameters.Variable;
            PVariableCaption.Text = CurrentParameters.VariableCaption ?? string.Empty;
            PCaption.Text = CurrentParameters.Caption;
            PSingle.IsChecked = CurrentParameters.IsSingle;
        }


        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            if (!CheckSQL()) return;
            cs = Services.GetCalculationService();
            cs.BeginGetQueryResults(CurrentParameters.SQL, "SampleTable", null, null, GetQueryResultsDone, false);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            if (!CheckSQL()) return; cs = Services.GetCalculationService();
            cs.BeginGetQueryResults(CurrentParameters.SQL, "SampleTable", null, null, GetQueryResultsDone, true);
        }

        private void GetQueryResultsDone(IAsyncResult iar)
        {
            try
            {
                var res = cs.EndGetQueryResults(iar);
                if (res.Success) QueryResult = res.Fields;
                Dispatcher.BeginInvoke(delegate()
                {
                    if (!res.Success) ShowTableNameError(true, "Ошибка в запросе: "+res.Message);
                    else if (CheckIsOutputValid())
                    {
                        ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)iar.AsyncState, ParametersXML = GetParametersXML() });
                    }
                });
            }
            catch (Exception e)
            {
                ShowTableNameError(true, "Ошибка: " + e.Message);
            }
        }

        private void ShowTableNameError(bool Show, string Message)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
                ErrorLabel.Text = Message;
            });
        }

        private bool CheckIsOutputValid()
        {
            List<string> ResultingNames = CurrentTable.Fields.Select(s => s.Name).ToList();

            if (QueryResult[0].Fields.Count != 2)
            {
                ShowError(true, "Запрос должен возвращать 2 поля - ключ и значение");
                return false;
            }

            if (CurrentParameters.Variable == string.Empty)
            {
                ShowError(true, "Не задано имя переменной");
                return false;
            }

            if (ResultingNames.Contains(CurrentParameters.Variable))
            {
                ShowError(true, "Переменная с таким именем уже есть");
                return false;
            }

            if (CurrentParameters.Caption == string.Empty)
            {
                ShowError(true, "Не задано название поля ввода");
                return false;
            }

            ShowError(false, string.Empty);
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }


        private bool CheckSQL()
        {
            string[] ProhibitedKeywords = { "drop", "delete", "insert", "update", "truncate" };
            foreach (var KeyWord in ProhibitedKeywords)
            {
                if (CurrentParameters.SQL.ToLower().Contains(KeyWord))
                {
                    ShowTableNameError(true, "Запрос содержит недопустимое ключевое слово: " + KeyWord);
                    return false;
                }
            }
            return true;
        }

        private void UpdateParameters()
        {
            CurrentParameters.SQL = SQL.Text;
            CurrentParameters.Variable = PVariable.Text;
            CurrentParameters.VariableCaption = PVariableCaption.Text;
            CurrentParameters.Caption = PCaption.Text;
            CurrentParameters.IsSingle = PSingle.IsChecked.Value;
            CurrentParameters.InputName = PSingle.IsChecked.Value ? "InputListSQLSingle" : "InputListSQL";
        }

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode OVM) { }
    }
}
