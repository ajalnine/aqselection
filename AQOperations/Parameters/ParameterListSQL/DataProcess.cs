﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_ParameterListSQL
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataItem Current = AvailableData.Where(n => n.Name == "Переменные").Single();
            DataItem Output = new DataItem();
            Output.DataItemType = Current.DataItemType;
            Output.Name = Current.Name;
            Output.TableName = Current.Name;
            Output.Fields = Current.Fields.ToList();
            DataField df = new DataField() { DataType = "String", Name = CurrentParameters.Variable };
            Output.Fields.Add(df);
            DataField df2 = new DataField() { DataType = "String", Name = CurrentParameters.VariableCaption };
            Output.Fields.Add(df2);
            Outputs = new List<DataItem>();
            Outputs.Add(Output);
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = new List<DataItem>(), Outputs = Outputs, Deleted = null });
        }
    }
}
