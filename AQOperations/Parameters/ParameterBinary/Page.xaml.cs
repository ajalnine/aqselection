﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterBinary
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        DataItem CurrentTable;
        private CalculationService cs;

        public Page()
        {
            InitializeComponent();
            cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { Caption = "Двоичный параметр", Default = false, Variable = "X" };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData;
            CurrentTable = DataSources.Where(s => s.Name == "Переменные").SingleOrDefault();
            PCaption.Text = CurrentParameters.Caption;
            PDefault.IsChecked = CurrentParameters.Default;
            PVariable.Text = CurrentParameters.Variable;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            string Name = "@" + CurrentParameters.Variable + "=" + CurrentParameters.Default.ToString();
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private bool CheckResult()
        {
            List<string> ResultingNames = CurrentTable.Fields.Select(s => s.Name).ToList();

            if (CurrentParameters.Variable == string.Empty)
            {
                ShowError(true, "Не задано имя переменной");
                return false;
            }

            if (ResultingNames.Contains(CurrentParameters.Variable))
            {
                ShowError(true, "Переменная с таким именем уже есть");
                return false;
            }

            if (CurrentParameters.Caption == string.Empty)
            {
                ShowError(true, "Не задано название поля ввода");
                return false;
            }

            ShowError(false, string.Empty);
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void UpdateParameters()
        {
            CurrentParameters.Caption = PCaption.Text;
            CurrentParameters.Variable = PVariable.Text;
            CurrentParameters.Default = PDefault.IsChecked.Value;
        }

        public void DataRequestReadyCallback(string fileName) { }
    }
}