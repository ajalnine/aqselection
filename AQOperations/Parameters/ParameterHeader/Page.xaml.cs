﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_ParameterHeader
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() {Caption = "Заголовок" };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            PHeader.Text = CurrentParameters.Caption;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            string Name = "Заголовок";
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private bool CheckResult()
        {
            return true;
        }

        private void ShowError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void UpdateParameters()
        {
            CurrentParameters.Caption = PHeader.Text;
        }

        public void DataRequestReadyCallback(string fileName) { }
    }
}