﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_Grouping
{
    public class Parameters
    {
        public List<GroupTableData> GroupingFields;
        public string TableName;
        public ObservableCollection<string> GroupOrder2;
    }

    public class GroupTableData : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool _isgrouping;
        public bool IsGrouping
        {
            get
            {
                return _isgrouping;
            }
            set
            {
                _isgrouping = value;
                NotifyPropertyChanged("IsGrouping");
            }
        }

        private bool _emitmin;
        public bool EmitMin
        {
            get
            {
                return _emitmin;
            }
            set
            {
                _emitmin = value;
                NotifyPropertyChanged("EmitMin");
            }
        }

        private bool _emitmax;
        public bool EmitMax
        {
            get
            {
                return _emitmax;
            }
            set
            {
                _emitmax = value;
                NotifyPropertyChanged("EmitMax");
            }
        }

        private bool _emitrange;
        public bool EmitRange
        {
            get
            {
                return _emitrange;
            }
            set
            {
                _emitrange = value;
                NotifyPropertyChanged("EmitRange");
            }
        }

        private bool _emitavg;
        public bool EmitAvg
        {
            get
            {
                return _emitavg;
            }
            set
            {
                _emitavg = value;
                NotifyPropertyChanged("EmitAvg");
            }
        }

        private bool _emitmed;
        public bool EmitMed
        {
            get
            {
                return _emitmed;
            }
            set
            {
                _emitmed = value;
                NotifyPropertyChanged("EmitMed");
            }
        }

        private bool _emitsum;
        public bool EmitSum
        {
            get
            {
                return _emitsum;
            }
            set
            {
                _emitsum = value;
                NotifyPropertyChanged("EmitSum");
            }
        }

        private bool _emitstdev;
        public bool EmitStdev
        {
            get
            {
                return _emitstdev;
            }
            set
            {
                _emitstdev = value;
                NotifyPropertyChanged("EmitStdev");
            }
        }

        private bool _emitcount;
        public bool EmitCount
        {
            get
            {
                return _emitcount;
            }
            set
            {
                _emitcount = value;
                NotifyPropertyChanged("EmitCount");
            }
        }

        private bool _emitfirst;
        public bool EmitFirst
        {
            get
            {
                return _emitfirst;
            }
            set
            {
                _emitfirst = value;
                NotifyPropertyChanged("EmitFirst");
            }
        }

        private bool _emitlast;
        public bool EmitLast
        {
            get
            {
                return _emitlast;
            }
            set
            {
                _emitlast = value;
                NotifyPropertyChanged("EmitLast");
            }
        }
        
        private bool _emitlist;
        public bool EmitList
        {
            get
            {
                return _emitlist;
            }
            set
            {
                _emitlist = value;
                NotifyPropertyChanged("EmitList");
            }
        }

        private bool _disableavg;
        public bool DisableAvg
        {
            get
            {
                return _disableavg;
            }
            set
            {
                _disableavg = value;
                NotifyPropertyChanged("DisableAvg");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
