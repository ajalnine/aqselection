﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_Grouping
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources; 
        
        private DataItem _selectedTable;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            foreach (var g in _currentParameters.GroupingFields) g.PropertyChanged += gdt_PropertyChanged;
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                var existingFields = from s in _currentParameters.GroupingFields select s.FieldName;
                var newSource = _dataSources.Where(s => s.Name == _currentParameters.TableName);
                if (newSource.Any())
                {
                    var NewFields = from s in newSource.First().Fields select s.Name;
                    if (existingFields.Intersect(NewFields).Count() != existingFields.Count())
                    {
                        UpdateFieldsSource();
                        CheckResult();
                        return;
                    }
                }
                UpdateFieldsSource();
                CheckResult();
            }
            else
            {
                var source = (from di in _dataSources select di.Name);
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    DataSourceSelector.UpdateLayout();
                }
                UpdateFieldsSource();
                CheckResult();
            }
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            CleanGrouping();
        }

        private void CleanGrouping()
        {
            foreach (var s in _currentParameters.GroupingFields)
            {
                s.EmitAvg = false;
                s.EmitMed = false;
                s.EmitCount = false;
                s.EmitList = false;
                s.EmitMax = false;
                s.EmitMin = false;
                s.EmitRange = false;
                s.EmitStdev = false;
                s.EmitSum = false;
                s.EmitFirst = false;
                s.EmitLast = false;
                s.IsGrouping = false;
            }
            CheckResult();
        }

        private void DefaultButton_Click(object sender, RoutedEventArgs e)
        {
            _currentParameters.GroupingFields = null;
            UpdateFieldsSource();
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void UpdateFieldsSource()
        {
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName); ////////
            if (_currentParameters.GroupingFields == null)
            {
                _currentParameters.GroupingFields = new List<GroupTableData>();
                if (_selectedTable != null)
                {
                    foreach (var a in _selectedTable.Fields)
                    {
                        var disable = (a.DataType.ToLower() == "string" || a.DataType.ToLower() == "datetime");
                        var gdt = new GroupTableData
                        {
                            FieldName = a.Name,
                            IsGrouping = false,
                            DisableAvg = disable,
                            EmitMax = disable,
                            EmitAvg = !disable,
                            EmitMed = false,
                            EmitRange = false,
                            EmitMin = false,
                            EmitCount = false,
                            EmitSum = false,
                            EmitStdev = false,
                            EmitFirst = false,
                            EmitLast = false,
                            EmitList = false
                        };
                        gdt.PropertyChanged += gdt_PropertyChanged;
                        _currentParameters.GroupingFields.Add(gdt);
                    }
                }
            }
            else
            {
                if (_dataSources==null || _dataSources.Count == 0  || _selectedTable == null) return;
                var groupedFields = from c in _currentParameters.GroupingFields select c.FieldName;
                var existingFields = from s in _dataSources.Where(a=>a!=null).LastOrDefault(d => d.Name==_selectedTable.Name).Fields where s!=null select s.Name;
                var unprocessedFields = existingFields.Except(groupedFields);
                var toRemoveFields = groupedFields.Except(existingFields).ToList();
                foreach (var u in unprocessedFields)
                {
                    var dt = _selectedTable.Fields.SingleOrDefault(f => f.Name == u).DataType.ToLower();
                    var gdt = new GroupTableData
                    {
                        FieldName = u,
                        IsGrouping = false,
                        DisableAvg = (dt == "string" || dt == "datetime"),
                        EmitMed = false,
                        EmitMax = false,
                        EmitRange = false,
                        EmitAvg = false,
                        EmitMin = false,
                        EmitCount = false,
                        EmitSum = false,
                        EmitStdev = false,
                        EmitFirst = false,
                        EmitLast = false,
                        EmitList = false
                    };
                    gdt.PropertyChanged += gdt_PropertyChanged;
                    _currentParameters.GroupingFields.Add(gdt);
                }
                foreach (var r in toRemoveFields)
                {
                    _currentParameters.GroupingFields.Remove(_currentParameters.GroupingFields.SingleOrDefault(a => a.FieldName == r));
                }
            }
            GroupSelector.ItemsSource = null;
            GroupSelector.UpdateLayout();
            GroupSelector.ItemsSource = _currentParameters.GroupingFields;
            GroupSelector.UpdateLayout();
        }

        void gdt_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void CheckResult()
        {
            if (_currentParameters.GroupingFields == null) return;
            EnableExit((from cr in _currentParameters.GroupingFields
                        where cr.IsGrouping || cr.EmitAvg || cr.EmitCount || 
                        cr.EmitFirst || cr.EmitLast || cr.EmitList || 
                        cr.EmitMax || cr.EmitMed || cr.EmitMin || 
                        cr.EmitRange || cr.EmitStdev || cr.EmitSum
                        select cr).Any());
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            UpdateFieldsSource();
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        private void HeaderCheckBox_OnClick(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb == null || cb.IsChecked == null) return;
            switch (cb.Tag.ToString())
            {
                case "Min":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping)) s.EmitMin = cb.IsChecked.Value;
                    break;
                case "Max":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping)) s.EmitMax = cb.IsChecked.Value;
                    break;
                case "Range":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping && !a.DisableAvg)) s.EmitRange = cb.IsChecked.Value;
                    break;
                case "Avg":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping && !a.DisableAvg)) s.EmitAvg = cb.IsChecked.Value;
                    break;
                case "Med":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping && !a.DisableAvg)) s.EmitMed = cb.IsChecked.Value;
                    break;
                case "Sum":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping && !a.DisableAvg)) s.EmitSum = cb.IsChecked.Value;
                    break;
                case "Stdev":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping && !a.DisableAvg)) s.EmitStdev = cb.IsChecked.Value;
                    break;
                case "First":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping)) s.EmitFirst = cb.IsChecked.Value;
                    break;
                case "Last":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping)) s.EmitLast = cb.IsChecked.Value;
                    break;
                case "Count":
                    foreach (var s in _currentParameters.GroupingFields) s.EmitCount = cb.IsChecked.Value;
                    break;
                case "List":
                    foreach (var s in _currentParameters.GroupingFields.Where(a => !a.IsGrouping)) s.EmitList = cb.IsChecked.Value;
                    break;
                case "Group":
                    if (cb.IsChecked.Value)CleanGrouping();
                    foreach (var s in _currentParameters.GroupingFields) s.IsGrouping = cb.IsChecked.Value;
                    break;
            }
            CheckResult();
        }
    }

    public class InverseVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Collapsed : Visibility.Visible;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Visibility)value != Visibility.Visible);
        }
    }

    public class InverseBooleanConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
    }
}