﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_Grouping
{
    
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> Inputs;
        private List<DataItem> Outputs;
        private Parameters CurrentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            bool HasErrors = false;
            Inputs = new List<DataItem>();
            DataItem Output = null;

            DataItem SelectedTable = availableData.Where(a => a != null).Where(s => s.Name == CurrentParameters.TableName).LastOrDefault();
            if (SelectedTable != null) 
            {
                Inputs.Add(SelectedTable);
                Output = new DataItem();
                Output.Fields = new List<DataField>();
                foreach (var gf in CurrentParameters.GroupingFields)
                {
                    string dataType = (from s in SelectedTable.Fields where s.Name == gf.FieldName select s.DataType).LastOrDefault();
                    if (dataType == null)
                    {
                        HasErrors = true;
                        if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
                    }

                    if (gf.IsGrouping && dataType != null)
                    {
                        Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = dataType });
                        if (gf.EmitCount) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Число", DataType = "Int32" });
                    }
                    else
                    {
                        int a1 = gf.EmitMax ? 1 : 0;
                        int a2 = gf.EmitMin ? 1 : 0;
                        int a3 = gf.EmitAvg ? 1 : 0;
                        int a4 = gf.EmitSum ? 1 : 0;
                        int a5 = gf.EmitStdev ? 1 : 0;
                        int a6 = gf.EmitCount ? 1 : 0;
                        int a7 = gf.EmitList ? 1 : 0;
                        int a8 = gf.EmitFirst ? 1 : 0;
                        int a9 = gf.EmitLast ? 1 : 0;
                        int a10 = gf.EmitRange ? 1 : 0;
                        if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10 != 1)
                        {
                            if (gf.EmitMin) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Min", DataType = dataType });
                            if (gf.EmitMax) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Max", DataType = dataType });
                            if (gf.EmitRange) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Размах", DataType = "Double" });
                            if (gf.EmitAvg) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Среднее", DataType = "Double" });
                            if (gf.EmitSum) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Сумма", DataType = "Double" });
                            if (gf.EmitStdev) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_СКО", DataType = "Double" });
                            if (gf.EmitFirst) Output.Fields.Add(new DataField() { Name = gf.FieldName + "[0]", DataType = dataType });
                            if (gf.EmitLast) Output.Fields.Add(new DataField() { Name = gf.FieldName + "[n]", DataType = dataType });
                            if (gf.EmitCount) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Число", DataType = "Double" });
                            if (gf.EmitList) Output.Fields.Add(new DataField() { Name = gf.FieldName + "_Список", DataType = "String" });
                        }
                        else
                        {
                            if (gf.EmitMin) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = dataType });
                            else if (gf.EmitMax) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = dataType });
                            else if (gf.EmitRange) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "Double" });
                            else if (gf.EmitAvg) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "Double" });
                            else if (gf.EmitSum) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "Double" });
                            else if (gf.EmitStdev) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "Double" });
                            else if (gf.EmitFirst) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = dataType });
                            else if (gf.EmitLast) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = dataType });
                            else if (gf.EmitCount) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "Double" });
                            else if (gf.EmitList) Output.Fields.Add(new DataField() { Name = gf.FieldName, DataType = "String" });
                        }
                    }
                }
                Output.DataItemType = SelectedTable.DataItemType;
                Output.Name = SelectedTable.Name;
                Output.TableName = SelectedTable.TableName;
            }
            else
            {
                HasErrors = true;
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (!HasErrors)
            {
                Outputs = new List<DataItem>();
                Outputs.Add(Output);
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
