﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_CopyTable
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            foreach (var di in _currentParameters.Copies)
            {
                var source = _dataSources.FirstOrDefault(dc => dc.Name == di.OriginalName);
                if (source != null)
                {
                    var destination = DataItemHelper.GetCopy(source);
                    destination.Name = di.CopyName;
                    if (destination.Name == source.Name) continue;
                    _inputs.Add(source);
                    _outputs.Add(destination);
                }
                else
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
                }
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
