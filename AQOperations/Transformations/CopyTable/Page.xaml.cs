﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_CopyTable
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<CopyTableData> _displayCopies;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters {Copies = new List<CopyTableData>()};
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            _displayCopies = new List<CopyTableData>();

            if (!(_currentParameters?.Copies == null || _currentParameters.Copies.Count == 0))
            {
                foreach (var di in _dataSources)
                {
                    var a = (from c in _currentParameters.Copies where c.OriginalName == di.Name select c).ToList();
                    var ctd = !a.Any() 
                        ? new CopyTableData { OriginalName = di.Name, CopyName = di.Name + " копия", NeedToCopy = false } 
                        : new CopyTableData{ OriginalName = di.Name, CopyName = a.First().CopyName, NeedToCopy = a.First().NeedToCopy };
                    ctd.PropertyChanged += ctd_PropertyChanged;
                    _displayCopies.Add(ctd);
                }
            }
            else
            {
                foreach (var di in _dataSources)
                {
                    var ctd = new CopyTableData { OriginalName = di.Name, CopyName = di.Name + " копия", NeedToCopy = _dataSources.Count == 1 };
                    ctd.PropertyChanged += ctd_PropertyChanged;
                    _displayCopies.Add(ctd);
                }
            }

            if (_currentParameters?.Copies != null && _currentParameters.Copies.Select(a => a.OriginalName).Intersect(_dataSources.Select(b => b.Name)).Count() == _currentParameters.Copies.Count)
            {
                CheckData();
            }

            CopySelector.ItemsSource = _displayCopies;
            CopySelector.UpdateLayout();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }
       
        void ctd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Bad") CheckData();
        }

        private void CheckData()
        {
            if (!(from a in _displayCopies where a.NeedToCopy select a).Any())
            {
                EnableExit(false);
                return;
            }
            
            var hasError = false;
            
            foreach (var a in _displayCopies.Where(c=>c.NeedToCopy))
            {
                if ((from d in _dataSources select d.Name).Contains(a.CopyName))
                {
                    a.Bad = true;
                    hasError = true;
                }
                else a.Bad = false;
            }

            foreach (var a in _displayCopies.Where(c => !c.NeedToCopy))
            {
                a.Bad = false;
            }
            EnableExit(!hasError);
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void UpdateParameters()
        {
            _currentParameters.Copies = new List<CopyTableData>();
            foreach (var di in _displayCopies.Where(c => c.NeedToCopy))
            {
                var source = _dataSources.First(dc => dc.Name==di.OriginalName);
                var destination = DataItemHelper.GetCopy(source);
                destination.Name = di.CopyName;
                if (destination.Name != source.Name)
                {
                    _currentParameters.Copies.Add(new CopyTableData { OriginalName = source.Name, CopyName = destination.Name, NeedToCopy = true });
                }
            }
        }

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }

    public class VisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Visibility)value == Visibility.Visible);
        }
    }
}
