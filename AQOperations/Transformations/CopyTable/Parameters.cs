﻿using System.Collections.Generic;
using System.ComponentModel;

// ReSharper disable once CheckNamespace
namespace SLCalc_CopyTable
{
    public class Parameters
    {
        public List<CopyTableData> Copies;
    }

    public class CopyTableData : INotifyPropertyChanged
    {
        private string _originalname;
        public string OriginalName
        {
            get
            {
                return _originalname;
            }
            set
            {
                _originalname = value;
                NotifyPropertyChanged("OriginalName");
            }
        }

        private string _copyname;
        public string CopyName
        {
            get
            {
                return _copyname;
            }
            set
            {
                _copyname = value;
                NotifyPropertyChanged("CopyName");
            }
        }

        private bool _needtocopy;
        public bool NeedToCopy
        {
            get
            {
                return _needtocopy;
            }
            set
            {
                _needtocopy = value;
                NotifyPropertyChanged("NeedToCopy");
            }
        }

        private bool _bad;
        public bool Bad
        {
            get
            {
                return _bad;
            }
            set
            {
                _bad = value;
                NotifyPropertyChanged("Bad");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
