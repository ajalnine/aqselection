﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_Sorting
{
    public class Parameters
    {
        public string TableName;
        public ObservableCollection<string> SortOrder;
        public bool IsAscending;
    }

    public class SortTableData : INotifyPropertyChanged
    {
        private string fieldname;
        public string FieldName
        {
            get
            {
                return fieldname;
            }
            set
            {
                fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool issorting;
        public bool IsSorting
        {
            get
            {
                return issorting;
            }
            set
            {
                issorting = value;
                NotifyPropertyChanged("IsSorting");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
