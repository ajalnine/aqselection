﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_Sorting
{
    public partial class Page :  IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private ObservableCollection<SortTableData> _sortingFields;
        private DataItem _selectedTable;

        public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters {SortOrder = new ObservableCollection<string>(), IsAscending = true};
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!_dataSources.Select(a => a.Name).Contains(_currentParameters.TableName))
            {
                UpdateDataSourceSelector(null);
                _currentParameters.SortOrder = new ObservableCollection<string>();
                UpdateFieldsSource();
                return;

            }
            if (UpdateDataSourceSelector(_currentParameters.TableName))
            {
                UpdateFieldsSource();
                var existingFields = (from s in _sortingFields select s.FieldName).ToList();
                var newSource = _dataSources.Where(s => s.Name == _currentParameters.TableName).ToList();
                if (newSource.Any())
                {
                    var newFields = (from s in newSource.First().Fields select s.Name).ToList();
                    if (existingFields.Intersect(newFields).Count() != existingFields.Count())
                    {
                        UpdateFieldsSource();
                        if (newFields.Intersect(_currentParameters.SortOrder).Count() != _currentParameters.SortOrder.Count) return;
                    }
                }
            }

            if (AscendingButton != null && DescendingButton != null)
            {
                AscendingButton.IsChecked = _currentParameters.IsAscending;
                DescendingButton.IsChecked = !_currentParameters.IsAscending;
            }
            UpdateFieldsSource();
            UpdateSortReorder();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }
        
        private bool UpdateDataSourceSelector(string sourceToSelect)
        {
            if (_dataSources.Select(s => s.Name).Contains(sourceToSelect) && sourceToSelect!=null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = sourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                return true;
            }
            if (_dataSources.Count <= 0) return false;
            DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
            DataSourceSelector.SelectedIndex = 0;
            return false;
        }
       
        private void UpdateSortReorder()
        {
            if (_currentParameters.SortOrder == null) _currentParameters.SortOrder = new ObservableCollection<string>();
            else
            {
                _selectedTable = (from d in _dataSources where d.Name == _currentParameters.TableName select d).SingleOrDefault();
                if (_selectedTable == null) return;
                var allFields = from f in _selectedTable.Fields select f.Name;
                var fieldsToRemove = _currentParameters.SortOrder.Except(allFields).ToList();

                if (fieldsToRemove.Count > 0)
                {
                    var newFields = new ObservableCollection<string>();
                    foreach (var s in _currentParameters.SortOrder.Where(s => !fieldsToRemove.Contains(s)))
                    {
                        newFields.Add(s);
                    }
                    _currentParameters.SortOrder = newFields;
                }

                SortingReorder.ItemsSource = null;
                SortingReorder.UpdateLayout();
                SortingReorder.ItemsSource = _currentParameters.SortOrder;
                SortingReorder.UpdateLayout();
            }
        }

        private void UpdateSortSelector()
        {
            if (_sortingFields == null) _sortingFields = new ObservableCollection<SortTableData>();
            else
            {
                foreach (var s in _sortingFields)
                {
                    s.PropertyChanged -= gdt_PropertyChanged;
                    s.IsSorting = (_currentParameters.SortOrder.Contains(s.FieldName));
                    s.PropertyChanged += gdt_PropertyChanged;
                }
                SortSelector.ItemsSource = null;
                SortSelector.UpdateLayout();
                SortSelector.ItemsSource = _sortingFields;
                SortSelector.UpdateLayout();
            }
        }

        private void UpdateFieldsSource()
        {
            _selectedTable = _dataSources.FirstOrDefault(s => s.Name == _currentParameters.TableName);
            _sortingFields = new ObservableCollection<SortTableData>();
            if (_selectedTable != null)
            {
                if (_currentParameters.SortOrder != null)
                {
                    foreach (var a in _selectedTable.Fields)
                    {
                        var gdt = new SortTableData
                        {
                            FieldName = a.Name,
                            IsSorting = _currentParameters.SortOrder.Contains(a.Name)
                        };
                        gdt.PropertyChanged += gdt_PropertyChanged;
                        _sortingFields.Add(gdt);
                    }
                }
            }
            SortSelector.ItemsSource = _sortingFields;
            SortingReorder.ItemsSource = _currentParameters.SortOrder;
            CheckResult();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        void gdt_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsSorting":
                    CheckResult();
                    var sdt = (SortTableData)sender;
                    if (sdt.IsSorting)
                    {
                        _currentParameters.SortOrder.Add(sdt.FieldName);
                    }
                    else
                    {
                        _currentParameters.SortOrder.Remove(sdt.FieldName);
                    }
                    break;
            }
        }

        private void CheckResult()
        {
            var a = from cr in _sortingFields where cr.IsSorting select cr;
            EnableExit(a.Count() != 0);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            UpdateFieldsSource();
            UpdateSortSelector();
            UpdateSortReorder();
            CheckResult();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }
        
        private void UpSort_Click(object sender, RoutedEventArgs e)
        {
            if (SortingReorder.SelectedItem == null) return;
            var selected = SortingReorder.SelectedItem.ToString();
            if (selected == null) return;
            var selectedIndex = SortingReorder.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : SortingReorder.Items.Count - 1;
            MoveSort(selectedIndex, nextIndex);
        }

        private void DownSort_Click(object sender, RoutedEventArgs e)
        {
            if (SortingReorder.SelectedItem == null) return;
            var selected = SortingReorder.SelectedItem.ToString();
            if (selected == null) return;
            var selectedIndex = SortingReorder.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < SortingReorder.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveSort(selectedIndex, nextIndex);
        }

        private void TopSort_Click(object sender, RoutedEventArgs e)
        {
            if (SortingReorder.SelectedItem == null) return;
            var selected = SortingReorder.SelectedItem.ToString();
            if (selected == null) return;
            var selectedIndex = SortingReorder.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveSort(selectedIndex, nextIndex);
        }

        private void BottomSort_Click(object sender, RoutedEventArgs e)
        {
            if (SortingReorder.SelectedItem == null) return;
            var selected = SortingReorder.SelectedItem.ToString();
            if (selected == null) return;
            var selectedIndex = SortingReorder.Items.IndexOf(selected);
            var nextIndex = SortingReorder.Items.Count - 1;
            MoveSort(selectedIndex, nextIndex);
        }

        private void MoveSort(int selectedIndex, int nextIndex)
        {
            var newItem = SortingReorder.SelectedItem.ToString();
            _currentParameters.SortOrder.RemoveAt(selectedIndex);
            _currentParameters.SortOrder.Insert(nextIndex, newItem);
            SortingReorder.SelectedItem = newItem;
            SortingReorder.ScrollIntoView(SortingReorder.SelectedItem);
            SortingReorder.UpdateLayout();
        }

        private void AscendingButton_Checked(object sender, RoutedEventArgs e)
        {
            if (_currentParameters == null) return;
            _currentParameters.IsAscending = true;
        }

        private void DescendingButton_Checked(object sender, RoutedEventArgs e)
        {
            if (_currentParameters == null) return; 
            _currentParameters.IsAscending = false;
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }
}