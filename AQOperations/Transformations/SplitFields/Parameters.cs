﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_SplitFields
{
    public class Parameters
    {
        public ObservableCollection<SplitFieldData> SplitFields;
        public string TableName;
    }

    public class SplitFieldData : INotifyPropertyChanged
    {
        private string fieldname;
        public string FieldName
        {
            get
            {
                return fieldname;
            }
            set
            {
                fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private string divider;
        public string Divider
        {
            get
            {
                return divider;
            }
            set
            {
                divider = value;
                NotifyPropertyChanged("Divider");
            }
        }

        private int number;
        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
                NotifyPropertyChanged("Number");
            }
        }

        private bool processerrors;
        public bool ProcessErrors
        {
            get
            {
                return processerrors;
            }
            set
            {
                processerrors = value;
                NotifyPropertyChanged("ProcessErrors");
            }
        }

        private bool isdouble;
        public bool IsDouble
        {
            get
            {
                return isdouble;
            }
            set
            {
                isdouble = value;
                NotifyPropertyChanged("IsDouble");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
