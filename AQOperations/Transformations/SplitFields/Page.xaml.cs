﻿using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Browser;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_SplitFields
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;
        private DataItem SelectedTable;
        Task Analize;
        DataLoader Data;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
            CurrentParameters.SplitFields = new ObservableCollection<SplitFieldData>();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            CurrentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();

            if (DataSources.Select(s => s.Name).Contains(CurrentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedItem = CurrentParameters.TableName;
                SelectedTable = DataSources.Where(i => i.Name == CurrentParameters.TableName).SingleOrDefault();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                var RequiredFields = CurrentParameters.SplitFields.Select(a => a.FieldName);
                var ExistingFields = DataSources.Where(a => a.Name == CurrentParameters.TableName).SingleOrDefault().Fields.Select(b => b.Name);
                if (RequiredFields.Intersect(ExistingFields).Count() != RequiredFields.Count())
                {
                    UpdateFieldsSource();
                    CleanSplitFields();
                    return;
                }
                UpdateFieldsSource();
                CleanSplitFields();
            }
            else
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            Data = null;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }
        
        public void CleanSplitFields()
        {
               var ExistingFields = from s in CurrentParameters.SplitFields select s.FieldName;
                var NewSource = DataSources.Where(s => s.Name == CurrentParameters.TableName);
                if (NewSource.Count() > 0)
                {
                    var NewFields = from s in NewSource.First().Fields select s.Name;
                    if (ExistingFields.Intersect(NewFields).Count() != ExistingFields.Count())
                    {
                        UpdateFieldsSource();
                        var ToRemove = (from c in CurrentParameters.SplitFields where !NewFields.Contains(c.FieldName) select c).ToList();
                        foreach (var c in ToRemove)
                        {
                            CurrentParameters.SplitFields.Remove(c);
                        }
                    }
                }
         }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckData()) return;
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private void UpdateFieldsSource()
        {
            if (CurrentParameters.SplitFields == null) CurrentParameters.SplitFields = new ObservableCollection<SplitFieldData>();
            else
            {
                SplitSelector.ItemsSource = null;
                SplitSelector.UpdateLayout();
                SplitSelector.ItemsSource = CurrentParameters.SplitFields;
                SplitSelector.UpdateLayout();
            }

            if (CurrentParameters.TableName != null)
            {
                if (DataSourceSelector.Items.Contains(CurrentParameters.TableName))
                {
                    DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                    DataSourceSelector.SelectedItem = CurrentParameters.TableName;
                    DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                }
                else DataSourceSelector.SelectedIndex = 0;
            }
            
            SplitFieldSelector.ItemsSource = from f in SelectedTable.Fields where f.DataType.ToLower() == "string" && !(from s in CurrentParameters.SplitFields select s.FieldName).Contains(f.Name) select f.Name;
            SplitFieldSelector.UpdateLayout();
            SplitSelector.ItemsSource = CurrentParameters.SplitFields;
            SplitSelector.UpdateLayout();
        }

        private bool CheckData()
        {
            List<string> ResultingNames = GetResultNames();

            bool IsDoubled = (from s in ResultingNames group s by s into g where g.Count() > 1 select g).Count() > 0;
            if (IsDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся поля");
                return false;
            }

            bool IsEmpty = (from s in ResultingNames where s.Trim() == string.Empty select s).Count() > 0;

            if (CurrentParameters.SplitFields.Count() == 0)
            {
                ShowTableNameError(true, "Ни одно поле не выбрано для разделения");
                return false;
            }

            if (CurrentParameters.SplitFields.Select(f=>f.Number).Sum()>100)
            {
                ShowTableNameError(true, "Результат содержит слишком много полей");
                return false;
            }
            ShowTableNameError(false, "");
            return true;
        }

        private List<string> GetResultNames()
        {
            List<string>  ResultNames = new List<string>();

            foreach (var f in SelectedTable.Fields)
            {
                if ((from s in CurrentParameters.SplitFields select s.FieldName).Contains(f.Name))
                {
                    SplitFieldData sfd = (from s in CurrentParameters.SplitFields where s.FieldName == f.Name select s).First();
                    string dt = (sfd.IsDouble) ? "Decimal" : "String";
                    for (int i = 0; i < sfd.Number; i++)
                    {
                        ResultNames.Add( f.Name + "_" + (i + 1).ToString());
                    }
                }
                else
                {
                    ResultNames.Add(f.Name);
                }
            }
            return ResultNames;
        }

        private void ShowTableNameError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.SplitFields = new ObservableCollection<SplitFieldData>();
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            SelectedTable = DataSources.Where(i => i.Name == CurrentParameters.TableName).SingleOrDefault();
            UpdateFieldsSource();
            CleanSplitFields();
        }

        public void DataRequestReadyCallback(string fileName)
        {
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Processing); 
            Data = new DataLoader();
            Data.DataProcessed += new DataLoader.DataProcessDelegate(Data_DataProcessed);
            Data.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            DoAnalize();
        }

        private void DoAnalize()
        {
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Ready);
            Analize.SetState(2, TaskState.Processing);
            this.Dispatcher.BeginInvoke(delegate()
            {
                CurrentParameters.SplitFields = new ObservableCollection<SplitFieldData>();
                AutoSetParameters(Data.GetTable(CurrentParameters.TableName));
            });
        }

        private void AutoSetParameters(SLDataTable sldt)
        {
            IEnumerable<string> Columns = from f in SelectedTable.Fields where f.DataType.ToLower()=="string" && !(from s in CurrentParameters.SplitFields select s.FieldName).Contains(f.Name) select f.Name;
            foreach (string c in Columns)
            {
                AnalizeColumn(c, sldt);
            }

            Analize.SetState(2, TaskState.Ready);
            AutoButton.IsEnabled = true;
            RefreshButton.IsEnabled = true;
            UpdateFieldsSource();
            CheckData();
        }

        private void AnalizeColumn(string Name, SLDataTable sldt)
        {
            int ColumnNumber = sldt.ColumnNames.ToList().IndexOf(Name);
            List<Hypotize> Hypotizes = new List<Hypotize>();

            foreach (var r in sldt.Table)
            {
                string Value = r.Row[ColumnNumber].ToString();
                if (Value == null) continue;
                Hypotize hv = AnalizeValue(Value);
                if (hv!=null)Hypotizes.Add(hv);
            }
            if (Hypotizes.Count == 0) return;

            Hypotize h = AnalizeHypotizes(Hypotizes);
            if (h == null) return;

            if (h.CanBeSplitted)
            {
                SplitFieldData sfd = new SplitFieldData();
                sfd.FieldName = Name;
                sfd.ProcessErrors = h.HasErrors;
                sfd.IsDouble = true;
                sfd.Number = h.Count;
                sfd.Divider = h.Divider;
                CurrentParameters.SplitFields.Add(sfd);
            }
        }

        private Hypotize AnalizeValue(string Value)
        {
            string v = Value.Trim();
            string divs = Regex.Replace(Value.Trim(), @"[0-9\w]*", string.Empty);
            bool HasWords = Regex.Matches(Value, @"([a-zА-Я]+)", RegexOptions.IgnoreCase).Count>0;
            if (HasWords) return new Hypotize() { CanBeSplitted = false, Count = 0, Divider = "", Weight = 1 };
            if (divs == string.Empty || divs == ".") return null;
            if (divs == ",-," || divs == ".-.") return new Hypotize() { CanBeSplitted = true, Count = 2, Divider = "-", Weight=1 };
            if (divs == ",")
            {
                double a1, a2;
                string[] Splitted = v.Split(',');
                if (double.TryParse(Splitted[0], out a1) && double.TryParse(Splitted[1], out a2))
                {
                    if (a1 == 0 || a2 == 0) return null;
                    double weight = (a1 > a2) ? a2 / a1 : a1 / a2;  
                    return new Hypotize() { CanBeSplitted = true, Count = 2, Divider = ",", Weight=weight };
                }
                else return null;
            }
            char[] c = divs.ToCharArray();
            switch (c.GroupBy(a => a).Count())
            {
                case 1:
                double a1;
                string[] Splitted = v.Split(c[0]);
                bool IsNumber = true;
                int Cnt = 0;
                foreach (string s in Splitted)
                {
                    if (s != string.Empty)
                    {
                        if (!double.TryParse(s, out a1)) IsNumber = false;
                        Cnt++;
                    }
                }
                if (!IsNumber || Cnt==0) return new Hypotize() { CanBeSplitted = false, Count = Cnt, Divider = c[0].ToString(), Weight = 1 };
                return new Hypotize() { CanBeSplitted = true, Count = Cnt, Divider = c[0].ToString(), Weight = 1 };
                
                case 0:
                    return null;

                default:
                    MatchCollection m = Regex.Matches(Value, @"([0-9.]*)");
                    bool IsParseable = true;
                    double a2;
                    foreach (var val in m)
                    {
                        string vn = Regex.Replace(val.ToString().Trim(), "[,.]", System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator);
                        if (vn != string.Empty) if (!double.TryParse(vn, out a2)) IsParseable = false;
                    }
                    if (IsParseable) return new Hypotize() { CanBeSplitted = true, Count = c.Count() + 1, Divider = c[0].ToString(), Weight = 1, HasErrors = true };
                    else return null;
            }
        }

        private Hypotize AnalizeHypotizes(List<Hypotize> Hypotizes)
        {
            if ((from h in Hypotizes where h.CanBeSplitted == false select h).Count()>0)return null;
            bool issingle = Hypotizes.GroupBy(h => h.Divider).Count() == 1;
            int MaxCount = (from h in Hypotizes select h.Count).Max();
            if (issingle && MaxCount > 1 && MaxCount < 10)
            {
                bool HasError = (from h in Hypotizes select h.Weight).Min() < 0.1 || (from h in Hypotizes where h.HasErrors select h).Count()>0;
                return new Hypotize() { CanBeSplitted = true, Count = MaxCount, Divider = Hypotizes[0].Divider, Weight = 1, HasErrors = HasError };
            }
            else
            {
                bool HasError = true;
                var MostPopularDivider = (from h in Hypotizes group h by h.Divider into g select new { Cnt = g.Count(), Div = g.Key }).OrderByDescending(g=>g.Cnt).First().Div;
                return new Hypotize() { CanBeSplitted = true, Count = MaxCount, Divider = MostPopularDivider, Weight = 1, HasErrors = HasError };
            }
        }

        private class Hypotize
        {
            public string Divider;
            public int Count;
            public bool CanBeSplitted;
            public bool HasErrors;
            public double Weight;
        }
        
        private void AddSplitButton_Click(object sender, RoutedEventArgs e)
        {
            string fieldName = (sender as TextImageButtonBase).Tag.ToString();
            if (CurrentParameters == null) CurrentParameters = new Parameters();
            if (CurrentParameters.SplitFields == null) CurrentParameters.SplitFields = new ObservableCollection<SplitFieldData>();
            CurrentParameters.SplitFields.Add(new SplitFieldData() { FieldName = fieldName, Divider = ";", Number = 2, ProcessErrors = true, IsDouble = true });
            UpdateFieldsSource();
        }

        private void RemoveFromSplitListButton_Click(object sender, RoutedEventArgs e)
        {
            string FieldName = (sender as TextImageButtonBase).Tag.ToString();
            CurrentParameters.SplitFields.Remove(CurrentParameters.SplitFields.Where(f => f.FieldName == FieldName).First());
            UpdateFieldsSource();
        }

        private void AutoButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateFieldsSource();
            if (DataRequest != null)
            {
                Analize = new Task(new string[] { "Исполнение расчета", "Прием данных", "Анализ данных" }, "Автоопределение параметров", TaskPanel);
                Analize.SetState(0, TaskState.Processing);
                AutoButton.IsEnabled = false;
                RefreshButton.IsEnabled = false;
                if (Data == null) DataRequest.Invoke(this, new EventArgs());
                else DoAnalize();
            }
        }
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateFieldsSource(); 
            if (DataRequest != null)
            {
                Analize = new Task(new string[] { "Исполнение расчета", "Прием данных", "Анализ данных" }, "Автоопределение параметров", TaskPanel);
                Analize.SetState(0, TaskState.Processing);
                AutoButton.IsEnabled = false;
                RefreshButton.IsEnabled = false;
                DataRequest.Invoke(this, new EventArgs());
            }
        }

        private void Number_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            CheckTextBoxContentIsNumber(tb);
        }

        private void Number_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox; 
            CheckTextBoxContentIsNumber(tb);
        }

        private void CheckTextBoxContentIsNumber(TextBox tb)
        {
            int N = 0;
            if (!int.TryParse(tb.Text, out N))
            {
                tb.Background = new SolidColorBrush(Color.FromArgb(255,255,127,127));
                tb.Focus();
            }
            else
            {
                tb.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        public void StateChanged(OperationViewMode OVM){}
    }
 }