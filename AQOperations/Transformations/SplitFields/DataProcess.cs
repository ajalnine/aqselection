﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_SplitFields
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataSources = AvailableData;
            Inputs = new List<DataItem>();
            DataItem SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();

            if (SelectedTable != null)
            {
                Inputs.Add(SelectedTable);

                DataItem Output = new DataItem();
                Output.Fields = new List<DataField>();

                foreach (var f in SelectedTable.Fields)
                {
                    if ((from s in CurrentParameters.SplitFields select s.FieldName).Contains(f.Name))
                    {
                        SplitFieldData sfd = (from s in CurrentParameters.SplitFields where s.FieldName == f.Name select s).First();
                        string dt = (sfd.IsDouble) ? "Decimal" : "String";
                        for (int i = 0; i < sfd.Number; i++)
                        {
                            Output.Fields.Add(new DataField() { Name = f.Name + "_" + (i + 1).ToString(), DataType = dt });
                        }
                    }
                    else
                    {
                        Output.Fields.Add(f);
                    }
                }
                Output.DataItemType = SelectedTable.DataItemType;
                Output.Name = SelectedTable.Name;
                Output.TableName = SelectedTable.TableName;
                Outputs = new List<DataItem>();
                Outputs.Add(Output);
            }
            else
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
