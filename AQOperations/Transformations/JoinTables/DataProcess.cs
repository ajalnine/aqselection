﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_JoinTables
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private List<string> _delete;
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            
            _inputs = new List<DataItem>();
            var t1 = availableData.SingleOrDefault(d => d.Name == _currentParameters.FirstTable);
            var t2 = availableData.SingleOrDefault(d => d.Name == _currentParameters.SecondTable);
            var hasErrors = false;
            if (t1 != null)
            {
                if (!t1.Fields.Select(a => a.Name).Contains(_currentParameters.FirstKey))
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    hasErrors = true;
                }
                _inputs.Add(t1);
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
                hasErrors = true;
            }

            if (t2 != null)
            {
                if (!t2.Fields.Select(a => a.Name).Contains(_currentParameters.SecondKey))
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    hasErrors = true;
                }
                _inputs.Add(t2);
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
                hasErrors = true;
            }
            
            _outputs = new List<DataItem>();
            if (_currentParameters.RemoveRightTable) _delete = new List<string> { _currentParameters.SecondTable };
            if (!hasErrors)
            {
                var di = new DataItem
                {
                    DataItemType = _inputs[0].DataItemType,
                    Name = _inputs[0].Name,
                    TableName = _inputs[0].TableName
                };
                var firstFieldData = _inputs[0].Fields;
                var secondKey = _currentParameters.SecondKey;

                var secondFieldData = new List<DataField>();
                if (_inputs != null && _inputs.Count > 1 && _inputs[1]?.Fields != null)
                {
                    foreach (var df in _inputs[1].Fields)
                    {
                        var name = df.Name;
                        if (name == secondKey && _currentParameters.RemoveRightKey) continue;
                        while (firstFieldData.Select(f => f.Name).Contains(name) || _inputs[1].Fields.Count(s => s.Name == name) > 1 || secondFieldData.Select(f => f.Name).Contains(name))
                        {
                            name += "_";
                        }
                        secondFieldData.Add(new DataField { Name = name, DataType = df.DataType });
                    }
                }
                if (_inputs?[0]?.Fields != null)
                {
                    di.Fields = _inputs[0].Fields.Concat(secondFieldData).ToList();
                    _outputs.Add(di);
                }
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = _inputs, Outputs = _outputs, Deleted = _delete });
        }
    }
}
