﻿using System.Collections.Generic;

namespace SLCalc_JoinTables
{
    public class Parameters
    {
        public string FirstTable;
        public string FirstKey;
        public string SecondTable;
        public string SecondKey;
        public List<Alias> SecondTableAliases;
        public string JoinType;
        public bool RemoveRightTable;
        public bool RemoveRightKey;
    }

    public class Alias
    {
        public string OriginalName;
        public string AliasName;
    }
}
