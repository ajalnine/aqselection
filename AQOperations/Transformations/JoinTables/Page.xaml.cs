﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_JoinTables
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<DataItem> _availableData;
        private bool _disableProcessing;

         public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters { JoinType = "INNER", RemoveRightTable = true, RemoveRightKey = true };
            InnerJoinImage.Source = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/InnerJoin.png", UriKind.Relative)};
            LeftJoinImage.Source = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/LeftJoin.png", UriKind.Relative)};
            RightJoinImage.Source = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/RightJoin.png", UriKind.Relative)};
            FullJoinImage.Source = new BitmapImage { UriSource = new Uri("/AQResources;component/Images/FullJoin.png", UriKind.Relative)};
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _currentParameters = new Parameters {JoinType = "INNER", RemoveRightTable = true, RemoveRightKey = true};
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _disableProcessing = _currentParameters.FirstTable != null;
            RemoveRightTable.IsChecked = _currentParameters.RemoveRightTable;
            RemoveRightKey.IsChecked = _currentParameters.RemoveRightKey;
            var joins = (from i in JoinSelector.Items select i as ComboBoxItem).ToList();
            JoinSelector.SelectedItem = joins.Single(t => t.Tag.ToString() == _currentParameters.JoinType);

            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            if (_dataSources.Select(n => n.Name).Contains(_currentParameters.FirstTable) && _dataSources.Select(n => n.Name).Contains(_currentParameters.SecondTable))
            {
                FirstTable.ItemsSource = (from d in _dataSources select d.Name);
                FirstTable.SelectedItem = _currentParameters.FirstTable;

                if (!_disableProcessing) return;

                var firstFieldsCandidate = (from d in _dataSources where d.Name == _currentParameters.FirstTable select d).SingleOrDefault();
                if (firstFieldsCandidate!=null)
                {
                    FirstField.ItemsSource = from f in firstFieldsCandidate.Fields select f.Name;
                    FirstField.UpdateLayout();
                    FirstField.SelectedItem = _currentParameters.FirstKey;
                }
                else
                {
                    FirstField.ItemsSource = null;
                }
                var secondTableCandidate = _dataSources.Where(s => s.Name == _currentParameters.FirstTable).ToList();
                if (secondTableCandidate.Count == 1)
                {
                    var field = secondTableCandidate.Single().Fields.SingleOrDefault(f => f.Name == _currentParameters.FirstKey);
                    if (field != null)
                    {
                        var requiredType = field.DataType.Split('.').Last();
                        if (_currentParameters.SecondTable != null)
                        {
                            SecondTable.ItemsSource = (from d in _dataSources where d.Name != _currentParameters.FirstTable select d.Name);
                            SecondTable.UpdateLayout();
                            SecondTable.SelectedItem = _currentParameters.SecondTable;

                            var secondFieldsCandidate = (from d in _dataSources where d.Name == _currentParameters.SecondTable select d).SingleOrDefault();
                            if (secondFieldsCandidate !=null)
                            {
                                SecondField.ItemsSource = from f in secondFieldsCandidate.Fields where f.DataType.Split('.').Last() == requiredType select f.Name;
                                SecondField.UpdateLayout();
                                SecondField.SelectedItem = _currentParameters.SecondKey;
                            }
                        }
                    }
                }
                CheckData();
            }
            else
            {
                FirstTable.ItemsSource = (from d in _dataSources select d.Name);
                FirstTable.SelectedIndex = 0;
            }
            _disableProcessing = false;
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }
        
        private void UIToParameters()
        {
            _currentParameters.FirstTable = FirstTable.SelectedItem?.ToString();
            _currentParameters.SecondTable = SecondTable.SelectedItem?.ToString();
            _currentParameters.FirstKey = FirstField.SelectedItem?.ToString();
            _currentParameters.SecondKey = SecondField.SelectedItem?.ToString();
            var comboBoxItem = JoinSelector.SelectedItem as ComboBoxItem;
            if (comboBoxItem != null)
                _currentParameters.JoinType = comboBoxItem.Tag.ToString();
            UpdateAliases();
            if (RemoveRightTable.IsChecked == null) return;
            _currentParameters.RemoveRightTable = RemoveRightTable.IsChecked.Value;
            if (RemoveRightKey.IsChecked == null) return;
            _currentParameters.RemoveRightKey = RemoveRightKey.IsChecked.Value;
        }

        private void UpdateAliases()
        {
            var firstFieldData = _dataSources.Single(a => a.Name == _currentParameters.FirstTable).Fields;
            var secondFieldData = new List<DataField>();
            _currentParameters.SecondTableAliases = new List<Alias>();
            foreach (var df in _dataSources.Single(a => a.Name == _currentParameters.SecondTable).Fields)
            {
                var name = df.Name;
                var nameChanged = false;
                while (firstFieldData.Select(f => f.Name).Contains(name) ||
                       _dataSources.Single(a => a.Name == _currentParameters.SecondTable).Fields.Count(s => s.Name == name) > 1 ||
                       secondFieldData.Select(f => f.Name).Contains(name))
                {
                    name += "_";
                    nameChanged = true;
                }
                secondFieldData.Add(new DataField {Name = name, DataType = df.DataType});
                if (nameChanged)
                    _currentParameters.SecondTableAliases.Add(new Alias {OriginalName = df.Name, AliasName = name});
            }
        }

        private void CheckData()
        {
            if (_currentParameters.SecondTable == null || _currentParameters.SecondKey == null || _currentParameters.FirstTable == null || _currentParameters.FirstKey == null)
            {
                EnableExit(false);
                ErrorLabel.Text = "Нет данных для соединения";
            }
            else
            {
                EnableExit(true);
                ErrorLabel.Text = "";
            }
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            if (NameChanged != null)
            {
                var name = $"[{_currentParameters.FirstTable}].[{_currentParameters.FirstKey}]=\r\n[{_currentParameters.SecondTable}].[{_currentParameters.SecondKey}]";
                NameChanged.Invoke(this, new NameChangedEventArgs { NewName = name });
            } 
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        #region Последовательный выбор элементов

        private void FirstTable_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disableProcessing) return;
            var cbeh = sender as ComboBox;
            if (cbeh?.SelectedItem == null) return;
            _currentParameters.FirstTable = cbeh.SelectedItem?.ToString();

            var source = (from f in (from d in _dataSources where d.Name == _currentParameters.FirstTable select d).Single().Fields select f.Name).ToList();
            FirstField.ItemsSource = source;
            if (source.Any()) FirstField.SelectedIndex = 0;

            if (SecondTable.ItemsSource != null && SecondTable.SelectedItem.ToString() != _currentParameters.FirstTable)return;
            var source2 = (from d in _dataSources where d.Name != _currentParameters.FirstTable select d.Name).ToList();
            SecondTable.ItemsSource = source2;
            if (source2.Any()) SecondTable.SelectedIndex = 0;
            SecondTable.UpdateLayout();
        }

        private void SecondTable_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disableProcessing) return;
            var cbeh = sender as ComboBox;
            if (cbeh?.SelectedItem != null)
            {
                _currentParameters.SecondTable = cbeh.SelectedItem?.ToString();
                var requiredType = _dataSources.Single(s => s.Name == _currentParameters.FirstTable).Fields.Single(f => f.Name == _currentParameters.FirstKey).DataType.Split('.').Last(); 
                var source = (from f in (from d in _dataSources where d.Name == _currentParameters.SecondTable select d).Single().Fields where f.DataType.Split('.').Last() == requiredType select f.Name).ToList();
                SecondField.ItemsSource = source;
                if (source.Any(a => a == _currentParameters.SecondKey)) SecondField.SelectedItem = _currentParameters.SecondKey;
                else if (source.Any()) SecondField.SelectedIndex = 0;
            }
            SecondField.UpdateLayout();
        }

        private void FirstField_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disableProcessing) return;
            var cbeh = sender as ComboBox;

            if (cbeh?.SelectedItem == null) return;
            _currentParameters.FirstKey = cbeh.SelectedItem.ToString();

            if (_currentParameters.SecondTable == null) return;

            var requiredType = _dataSources.Single(s => s.Name == _currentParameters.FirstTable).Fields.Single(f => f.Name == _currentParameters.FirstKey).DataType.Split('.').Last();
            var secondTable = (from d in _dataSources where d.Name == _currentParameters.SecondTable select d).SingleOrDefault();
            if (secondTable == null || SecondField == null) return;

            var existingSecondField = secondTable.Fields.SingleOrDefault(f => f.Name == _currentParameters.SecondKey);
            var existingType = existingSecondField?.DataType.Split('.').LastOrDefault();
            if (existingType == requiredType && SecondField?.SelectedItem!=null)
            {
                return;
            }

            var source = (from f in secondTable.Fields where f.DataType.Split('.').Last() == requiredType select f.Name).ToList();
            SecondField.ItemsSource = source;

            if (source.Any(a=>a == _currentParameters.SecondKey)) SecondField.SelectedItem = _currentParameters.SecondKey;
            else if (source.Any()) SecondField.SelectedIndex = 0;

            SecondField.UpdateLayout();
        }

        private void SecondField_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disableProcessing) return;
            var cbeh = sender as ComboBox;
            if (cbeh?.SelectedItem == null) return;
            _currentParameters.SecondKey = cbeh.SelectedItem?.ToString();
            EnableExit(true);
            ErrorLabel.Text = string.Empty;
        }

        private void JoinSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disableProcessing) return;
            var cbeh = sender as ComboBox;
            if (cbeh?.SelectedItem == null) return;
            _currentParameters.JoinType = ((ComboBoxItem)cbeh.SelectedItem).Tag.ToString();
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }

        public void StateChanged(OperationViewMode ovm) { }

        private void FlipButton_OnClickOKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            var fk = _currentParameters.FirstKey;
            var ft = _currentParameters.FirstTable;
            _currentParameters.FirstKey = _currentParameters.SecondKey;
            _currentParameters.SecondKey = fk;
            _currentParameters.FirstTable = _currentParameters.SecondTable;
            _currentParameters.SecondTable = ft;
            switch (_currentParameters.JoinType)
            {
                case "RIGHT":
                    _currentParameters.JoinType = "LEFT";
                    break;

                case "LEFT":
                    _currentParameters.JoinType = "RIGHT";
                    break;
            }
            ParametersToUI(_availableData);
            UpdateAliases();
        }
    }
}
