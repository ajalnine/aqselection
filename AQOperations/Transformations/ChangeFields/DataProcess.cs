﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_ChangeFields
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataSources = AvailableData; 
            
            Inputs = new List<DataItem>();
            DataItem SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();
            if (SelectedTable != null)
            {
                Inputs.Add(SelectedTable);

                DataItem Output = new DataItem();
                Output.DataItemType = Inputs[0].DataItemType;
                Output.Name = Inputs[0].Name;
                Output.TableName = Inputs[0].TableName;
                Output.Fields = new List<DataField>();
                foreach (var f in CurrentParameters.FieldTransformation)
                {
                    var DT = (from i in Inputs[0].Fields where i.Name == f.OldFieldName select i).FirstOrDefault();
                    if (DT != null)
                    {
                        DataField df = new DataField();
                        df.Name = f.NewFieldName;
                        df.DataType = DT.DataType;
                        Output.Fields.Add(df);
                    }
                    else
                    {
                        if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
                    }
                }
                Outputs = new List<DataItem>();
                Outputs.Add(Output);
            }
            else
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
