﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQControlsLibrary;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_ChangeFields
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        
        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private DataItem SelectedTable;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            CurrentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
            UpdateLayout();
            EnableExit(true);
            
            if (DataSources.Select(s => s.Name).Contains(CurrentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedItem = CurrentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;

                if (CurrentParameters.FieldTransformation != null)
                {
                    var ExistingFields = from s in CurrentParameters.FieldTransformation select s.OldFieldName;
                    var NewSource = DataSources.Where(s => s.Name == CurrentParameters.TableName);
                    if (NewSource.Count() > 0)
                    {
                        var NewFields = from s in NewSource.First().Fields select s.Name;
                        if (ExistingFields.Intersect(NewFields).Count() != ExistingFields.Count())
                        {
                            UpdateFieldsSource();
                            return;
                        }
                    }
                }
                UpdateFieldsSource();
            }
            else
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
                DataSourceSelector.UpdateLayout();
            }
            CheckResult();
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckResult()) return;
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }
        
        private void UpdateFieldsSource()
        {
            if (CurrentParameters.FieldTransformation == null)
            {
                CurrentParameters.FieldTransformation = new ObservableCollection<NewTableData>();
                CurrentParameters.DeletedFields = new ObservableCollection<string>();
                foreach (var a in SelectedTable.Fields)
                {
                    NewTableData ndt = new NewTableData();
                    ndt.NewFieldName = a.Name;
                    ndt.OldFieldName = a.Name;
                    CurrentParameters.FieldTransformation.Add(ndt);
                }
            }
            else
            {
                SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();
                var ExistingFields = from s in SelectedTable.Fields select s.Name;
                var TransformedFields = from c in CurrentParameters.FieldTransformation select c.OldFieldName;
                var Deletedfields = from d in CurrentParameters.DeletedFields select d;

                var NewAddedFields = ExistingFields.Except(TransformedFields.Concat(Deletedfields));
                var NonExistingFields = TransformedFields.Concat(Deletedfields).Except(ExistingFields);
                foreach (var a in NewAddedFields)
                {
                    CurrentParameters.DeletedFields.Add(a);
                }
                ObservableCollection<NewTableData> UpdatedFieldTransformation = new ObservableCollection<NewTableData>();
                ObservableCollection<string> UpdatedDeletedFields = new ObservableCollection<string>();

                foreach (var f in CurrentParameters.FieldTransformation)
                {
                    if (!NonExistingFields.Contains(f.OldFieldName)) UpdatedFieldTransformation.Add(f);
                }
                foreach (var f in CurrentParameters.DeletedFields)
                {
                    if (!NonExistingFields.Contains(f)) UpdatedDeletedFields.Add(f);
                }
                CurrentParameters.FieldTransformation = UpdatedFieldTransformation;
                CurrentParameters.DeletedFields = UpdatedDeletedFields;
            }
            FieldEditor.ItemsSource = CurrentParameters.FieldTransformation;
            FieldEditor.UpdateLayout();
            DeletedFields.ItemsSource = CurrentParameters.DeletedFields;
            DeletedFields.UpdateLayout();
        }

        private bool CheckResult()
        {
            if (CurrentParameters.FieldTransformation.Count == 0)
            {
                ShowTableNameError(true, "Не выбрано ни одного поля");
                return false;
            }
            bool IsDoubled = (from s in CurrentParameters.FieldTransformation group s by s.NewFieldName.ToString() into g where g.Count() > 1 select g).Count() > 0;
            if (IsDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся поля");
                return false;
            }
            return true;
        }
        
        private void ShowTableNameError(bool Show, string Message)
        {
            ErrorLabel.Text = Message;
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();
            UpdateFieldsSource();
        }

        #region Обработка событий кнопок
        private void UpSort_Click(object sender, RoutedEventArgs e)
        {
            if (FieldEditor.SelectedItem == null) return;
            NewTableData Selected = (NewTableData)FieldEditor.SelectedItem;
            if (Selected == null) return;
            int SelectedIndex = CurrentParameters.FieldTransformation.IndexOf(Selected);
            int NextIndex = (SelectedIndex > 0) ? SelectedIndex - 1 : CurrentParameters.FieldTransformation.Count - 1;
            MoveSort(SelectedIndex, NextIndex);
        }

        private void DownSort_Click(object sender, RoutedEventArgs e)
        {
            if (FieldEditor.SelectedItem == null) return;
            NewTableData Selected = (NewTableData)FieldEditor.SelectedItem;
            if (Selected == null) return;
            int SelectedIndex = CurrentParameters.FieldTransformation.IndexOf(Selected);
            int NextIndex = (SelectedIndex < CurrentParameters.FieldTransformation.Count - 1) ? SelectedIndex + 1 : 0;
            MoveSort(SelectedIndex, NextIndex);
        }

        private void TopSort_Click(object sender, RoutedEventArgs e)
        {
            if (FieldEditor.SelectedItem == null) return;
            NewTableData Selected = (NewTableData)FieldEditor.SelectedItem;
            if (Selected == null) return;
            int SelectedIndex = CurrentParameters.FieldTransformation.IndexOf(Selected);
            int NextIndex = 0;
            MoveSort(SelectedIndex, NextIndex);
        }

        private void BottomSort_Click(object sender, RoutedEventArgs e)
        {
            if (FieldEditor.SelectedItem == null) return;
            NewTableData Selected = (NewTableData)FieldEditor.SelectedItem;
            if (Selected == null) return;
            int SelectedIndex = CurrentParameters.FieldTransformation.IndexOf(Selected);
            int NextIndex = CurrentParameters.FieldTransformation.Count - 1;
            MoveSort(SelectedIndex, NextIndex); 
        }

        private void MoveSort(int SelectedIndex, int NextIndex)
        {
            NewTableData NewItem = CurrentParameters.FieldTransformation[SelectedIndex];
            
            CurrentParameters.FieldTransformation.RemoveAt(SelectedIndex);
            CurrentParameters.FieldTransformation.Insert(NextIndex, NewItem);
            FieldEditor.SelectedItem = NewItem;
            FieldEditor.UpdateLayout(); 
            FieldEditor.ScrollIntoView(FieldEditor.SelectedItem, FieldEditor.Columns[0]);
            FieldEditor.UpdateLayout();
        }

        private void RemoveFieldButton_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            string FieldName = mib.Tag.ToString();
            CurrentParameters.FieldTransformation.Remove((from c in CurrentParameters.FieldTransformation where c.OldFieldName == FieldName select c).SingleOrDefault());
            CurrentParameters.DeletedFields.Add(FieldName);
        }

        private void RestoreFieldButton_Click(object sender, RoutedEventArgs e)
        {
            TextImageButtonBase mib = sender as TextImageButtonBase;
            string FieldName = mib.Tag.ToString();
            NewTableData ntd = new NewTableData() { OldFieldName = FieldName, NewFieldName = FieldName };
            CurrentParameters.FieldTransformation.Add(ntd);
            FieldEditor.ScrollIntoView(ntd, FieldEditor.Columns[0]);
            CurrentParameters.DeletedFields.Remove(FieldName);
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode OVM) { }
    }
}