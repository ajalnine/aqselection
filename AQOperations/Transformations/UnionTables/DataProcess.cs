﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_UnionTables
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<string> Deleted;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataSources = AvailableData;

            Outputs = new List<DataItem>();

            if (CurrentParameters.UseList)
            {
                Inputs = DataSources.Where(a => CurrentParameters.ToUnion.Contains(a.Name)).ToList();
            }
            else
            {
                Inputs = DataSources.Where(a => a.Name.StartsWith(CurrentParameters.NameStartsWith)).ToList();
            }

            List<DataField> ResultFields = new List<DataField>();
            
            foreach (var i in Inputs)
            {
                foreach (var f in i.Fields)
                {
                    if (ResultFields.Where(a => a.Name == f.Name).Count() == 0)
                    {
                        ResultFields.Add(f);
                    }
                    else
                    {
                        var rf = ResultFields.Where(a => a.Name == f.Name).Single();
                        //rf.DataType = "String";
                    }
                }
            }
            
            if (CurrentParameters.DeleteSources)
            {
                Deleted = Inputs.Select(a => a.Name).ToList();
            }

            DataItem di = new DataItem() { Name = CurrentParameters.ResultTableName, TableName = CurrentParameters.ResultTableName, DataItemType = DataType.Selection, Fields = ResultFields };
            if (!string.IsNullOrWhiteSpace(CurrentParameters.SourceTableNameField))
            {
                di.Fields.Add(new DataField { DataType = "String", Name = CurrentParameters.SourceTableNameField });
            }
            Outputs.Add(di);

            if (CurrentParameters.ToUnion.Count != CurrentParameters.ToUnion.Intersect(Inputs.Select(a=>a.Name)).Count())
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = Deleted });
        }
    }
}
