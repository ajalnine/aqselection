﻿using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_UnionTables
{
    public class Parameters
    {
        public bool UseList;
        public List<string> ToUnion;
        public string NameStartsWith;
        public string ResultTableName;
        public bool DeleteSources;
        public string SourceTableNameField;
    }
}
