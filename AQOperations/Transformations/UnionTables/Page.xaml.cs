﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQMathClasses;

namespace SLCalc_UnionTables
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<UnionTableData> UnionTables;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { DeleteSources = false, NameStartsWith = String.Empty, ResultTableName = "Результат сложения", ToUnion = new List<string>(), UseList = true, SourceTableNameField = string.Empty };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            EnablePanels();

            NameStartsWith.Text = CurrentParameters.NameStartsWith;
            SourceFieldName.Text = CurrentParameters.SourceTableNameField ?? string.Empty;
            ResultName.Text = CurrentParameters.ResultTableName;
            DeleteSources.IsChecked = CurrentParameters.DeleteSources;
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            UnionTables = (from d in DataSources select new UnionTableData() { Name = d.Name, ToUnion = CurrentParameters.ToUnion.Contains(d.Name) }).ToList<UnionTableData>();
            UnionSelector.ItemsSource = UnionTables;
            UnionSelector.UpdateLayout();
        }

        private void EnablePanels()
        {
            ModeList.IsChecked = CurrentParameters.UseList;
            ModeName.IsChecked = !CurrentParameters.UseList;
            UnionSelector.IsEnabled = CurrentParameters.UseList;
            NameStartsWith.IsEnabled = !CurrentParameters.UseList;
            CheckNone.IsEnabled = CurrentParameters.UseList;
            CheckAll.IsEnabled = CurrentParameters.UseList;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private bool UpdateParameters()
        {
            ErrorMessage.Text = String.Empty;

            CurrentParameters.DeleteSources = DeleteSources.IsChecked.Value;
            CurrentParameters.UseList = ModeList.IsChecked.Value;
            CurrentParameters.NameStartsWith = NameStartsWith.Text;
            CurrentParameters.ResultTableName = ResultName.Text;
            CurrentParameters.SourceTableNameField = SourceFieldName.Text;
            CurrentParameters.ToUnion = (from u in UnionTables where u.ToUnion select u.Name).ToList<string>();

            if (String.IsNullOrWhiteSpace(CurrentParameters.ResultTableName))
            {
                ErrorMessage.Text = "Имя таблицы c результатом не может быть пустым";
                return false;
            }

            if (DataSources.Select(s => s.Name).Contains(CurrentParameters.ResultTableName) )
            {
                ErrorMessage.Text = "Имя отчета с результатом уже используется в расчете";
                return false;
            }

            if (DataSources.SelectMany(s => s.Fields).Any(a=>a.Name == CurrentParameters.SourceTableNameField))
            {
                ErrorMessage.Text = "Имя поля с именем исходной таблицы уже есть во входных данных";
                return false;
            }

            return true;
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (UpdateParameters()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private void CheckNone_Click(object sender, RoutedEventArgs e)
        {
            if (UnionTables != null) foreach (var f in UnionTables) f.ToUnion = false;
        }

        private void CheckAll_Click(object sender, RoutedEventArgs e)
        {
            if (UnionTables != null) foreach (var f in UnionTables) f.ToUnion = true;
        }
        private void ModeList_Checked(object sender, RoutedEventArgs e)
        {
            if (CurrentParameters != null)
            {
                CurrentParameters.UseList = true;
                EnablePanels();
            }
        }

        private void ModeName_Checked(object sender, RoutedEventArgs e)
        {
            if (CurrentParameters != null)
            {
                CurrentParameters.UseList = false;
                EnablePanels();
            }
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode OVM) { }
    }

    public class UnionTableData : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool tounion;
        public bool ToUnion
        {
            get
            {
                return tounion;
            }
            set
            {
                tounion = value;
                NotifyPropertyChanged("ToUnion");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}