﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_SplitTable
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private List<string> _deleted;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            
            var currentTable = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable != null)
            {
                _inputs.Add(currentTable);
                if (currentTable.Fields.All(a => a.Name != _currentParameters.CategoryFieldName))
                {
                    if (DataFlowError != null)DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = null, Deleted = null });
                    return;
                }

                if (_currentParameters.Categories.Count!=0)
                {
                    foreach (var s in _currentParameters.Categories)
                    {
                        var categoryTableName = s.Replace("|", " или ").Replace("%", "~").Trim();
                        var di = new DataItem {DataItemType = DataType.Selection};
                        var name = _currentParameters.OutputTablePrefix + categoryTableName;
                        di.TableName = name;
                        di.Name = name;
                        di.Fields = new List<DataField>();
                        foreach (var df in currentTable.Fields)
                        {
                            if (_currentParameters.RemoveCategory && df.Name == _currentParameters.CategoryFieldName) continue;
                            di.Fields.Add(new DataField { Name = df.Name, DataType = df.DataType });
                        }
                        _outputs.Add(di);
                    }
                }
                if (_currentParameters.RemoveTable)_deleted = new List<string>{_currentParameters.TableName};
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = _deleted });
        }
    }
}
