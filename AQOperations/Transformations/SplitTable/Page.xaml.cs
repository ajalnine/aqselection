﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace SLCalc_SplitTable
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private DataItem SelectedTable;
        Task Analize;
        DataLoader Data;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters();
            CurrentParameters.Categories = new ObservableCollection<string>();
            CurrentParameters.RemoveCategory = true;
            CurrentParameters.RemoveTable = false;
            ParametersToUI(availableData);
            SetCategoryPrefix();
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!DataSources.Select(a => a.Name).Contains(CurrentParameters.TableName)) UpdateDataSourceSelector(null);
            else UpdateDataSourceSelector(CurrentParameters.TableName);
            
            UpdateFieldsSource();

            string Content = string.Empty;
            foreach (string s in CurrentParameters.Categories)
            {
                Content += s + "\r";
            }
            
            CategoriesInRows.Text = Content;
            TablePrefix.Text = CurrentParameters.OutputTablePrefix;
            RemoveCategory.IsChecked = CurrentParameters.RemoveCategory;
            RemoveTable.IsChecked = CurrentParameters.RemoveTable;
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private bool UpdateDataSourceSelector(string SourceToSelect)
        {
            if (DataSources.Select(s => s.Name).Contains(SourceToSelect) && SourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = SourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                return true;
            }
            else if (DataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            return false;
        }

       
        private void UpdateFieldsSource()
        {
            DataItem CurrentTable = DataSources.Where(a => a != null).Where(s => s.Name == CurrentParameters.TableName).SingleOrDefault();
            if (CurrentTable == null) return;
            var Fields = CurrentTable.Fields.Where(a => a != null).Select(s => s.Name);
            CategoryFieldSelector.SelectionChanged -= CategoryFieldSelector_SelectionChanged;
            CategoryFieldSelector.ItemsSource = Fields;
            CategoryFieldSelector.UpdateLayout();
            if (Fields.Count() > 0)
            {
                if (CurrentParameters.CategoryFieldName != null && Fields.Contains(CurrentParameters.CategoryFieldName)) CategoryFieldSelector.SelectedItem = CurrentParameters.CategoryFieldName;
                else CategoryFieldSelector.SelectedIndex = 0;
                CategoryFieldSelector.UpdateLayout();
            }
            CategoryFieldSelector.SelectionChanged += CategoryFieldSelector_SelectionChanged;
        }

        private void UpdateCurrentParameters()
        {
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            CurrentParameters.CategoryFieldName = CategoryFieldSelector.SelectedItem.ToString();
            CurrentParameters.Categories= new ObservableCollection<string>();
            var rows = CategoriesInRows.Text.Split(new string[] { "\r" }, StringSplitOptions.None).Distinct();
            foreach (string s in rows)
            {
                string Category = s.Trim();
                if (!String.IsNullOrEmpty(Category)) CurrentParameters.Categories.Add(Category);
            }
            CurrentParameters.OutputTablePrefix = TablePrefix.Text;
            CurrentParameters.RemoveCategory = RemoveCategory.IsChecked.Value;
            CurrentParameters.RemoveTable = RemoveTable.IsChecked.Value;
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

       
        private void CheckResult()
        {
            EnableExit(true);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            UpdateFieldsSource();
            SetCategoryPrefix();
            CheckResult();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateCurrentParameters(); 
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }
        #endregion

        #region Автозаполнение категорий

        public void DataRequestReadyCallback(string fileName)
        {
            UpdateCurrentParameters();
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Processing);
            Data = new DataLoader();
            Data.DataProcessed += new DataLoader.DataProcessDelegate(Data_DataProcessed);
            Data.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Ready);
            Analize.SetState(2, TaskState.Processing);
            this.Dispatcher.BeginInvoke(delegate()
            {
                AutoSetCategories(Data.GetTable(CurrentParameters.TableName));
            });
        }

        private void AutoSetCategories(SLDataTable sldt)
        {
            int i = sldt.ColumnNames.IndexOf(CurrentParameters.CategoryFieldName);

            var a = (from row in sldt.Table select row.Row[i].ToString()).Distinct();
            string Result = String.Empty;
            foreach (var s in a)
            {
                Result += s + "\r";
            }
            CategoriesInRows.Text = Result;
            Analize.SetState(2, TaskState.Ready);
            GetCurrentCategories.IsEnabled = true;
        }

        private void GetCurrentCategories_Click(object sender, RoutedEventArgs e)
        {
            if (DataRequest != null)
            {
                Analize = new Task(new string[] { "Исполнение расчета", "Прием данных", "Анализ данных" }, "Автоопределение параметров", TaskPanel);
                Analize.SetState(0, TaskState.Processing);
                GetCurrentCategories.IsEnabled = false;
                DataRequest.Invoke(this, new EventArgs());
            }
        }

        #endregion
        public void StateChanged(OperationViewMode OVM) { }

        private void CategoryFieldSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CategoryFieldSelector.SelectedItem != null && DataSourceSelector.SelectedItem != null)
            {
                SetCategoryPrefix();
            }
        }

        private void SetCategoryPrefix()
        {
            var table = DataSourceSelector.SelectedItem.ToString();
            var field = CategoryFieldSelector.SelectedItem.ToString();
            TablePrefix.Text = (table.StartsWith("Таблица", StringComparison.InvariantCultureIgnoreCase) ? string.Empty : (table + ", ")) + field + ": ";
            CurrentParameters.OutputTablePrefix = TablePrefix.Text;
        }
    }
}