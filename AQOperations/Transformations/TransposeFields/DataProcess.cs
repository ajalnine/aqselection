﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_TransposeFields
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;
            _inputs = new List<DataItem>();
            var selectedTable = _dataSources.FirstOrDefault(s => s.Name == _currentParameters.TableName);

            if (selectedTable != null)
            {
                _inputs.Add(selectedTable);

                var output = new DataItem
                {
                    Fields = new List<DataField>(),
                    DataItemType = selectedTable.DataItemType,
                    Name = selectedTable.Name,
                    TableName = selectedTable.TableName
                };
                var usedFields = new List<string>();
                var sourceFields = selectedTable.Fields.Select(a => a.Name).ToList();
                foreach (var g in _currentParameters.TransposeGroups)
                {
                    if (!g.DeleteSourceFields) continue;
                    usedFields.AddRange(g.FieldList);
                }
                var fixedFields = selectedTable.Fields.Where(a=>!usedFields.Contains(a.Name)).ToList();
                output.Fields.AddRange(fixedFields);
                var noFields = false;
                foreach (var t in _currentParameters.TransposeGroups)
                {
                    var name = t.FieldList.First();
                    var dt = selectedTable.Fields.SingleOrDefault(a => a.Name == name);
                    if (dt == null) break;
                    foreach (var field in t.FieldList)
                    {
                        var singleOrDefaultField = selectedTable.Fields.SingleOrDefault(a => a.Name == field);
                        if (singleOrDefaultField == null) continue;
                        var dt2 = singleOrDefaultField.DataType;
                        if (dt.DataType == dt2) continue;
                        DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                        noFields = true;
                        break;
                    }
                    output.Fields.Add(new DataField { Name = t.CategoryName, DataType = "String" });
                    output.Fields.Add(new DataField { Name = t.ValueName, DataType = dt.DataType });
                    if (noFields) break;
                }
                _outputs = new List<DataItem> {output};
                if (usedFields.Any(usedField => !sourceFields.Contains(usedField)))
                {
                    if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                }
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
