﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_TransposeFields
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private DataItem _selectedTable;
        private int _editingIndex = -1;
        private bool _lastDeleteField;

        public static readonly DependencyProperty NewGroupProperty =
        DependencyProperty.Register("NewGroup", typeof(TransposeGroup), typeof(Page), new PropertyMetadata(new TransposeGroup
        {
            CategoryName = "Имя",
            FieldList = new ObservableCollection<string>(),
            ValueName = "Значения",
            DeleteSourceFields = true
        }));

        public TransposeGroup NewGroup
        {
            get { return (TransposeGroup)GetValue(NewGroupProperty); }
            set { SetValue(NewGroupProperty, value); }
        }

        public static readonly DependencyProperty AvailableFieldsProperty =
        DependencyProperty.Register("AvailableFields", typeof(ObservableCollection<string>), typeof(Page), new PropertyMetadata(new ObservableCollection<string>()));

        public ObservableCollection<string> AvailableFields
        {
            get { return (ObservableCollection<string>)GetValue(AvailableFieldsProperty); }
            set { SetValue(AvailableFieldsProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            NewGroup.PropertyChanged += NewGroup_PropertyChanged;
            NewGroupPanel.DataContext = NewGroup;
            _lastDeleteField = true;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ResetNewGroup();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { TableName = _selectedTable.Name, TransposeGroups = new ObservableCollection<TransposeGroup>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            CheckParameters();
            ResetNewGroup();
            ParametersToUI(availableData);
        }

        private void CheckParameters()
        {
            var correctedGroups = new ObservableCollection<TransposeGroup>();
            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (newSource == null) return;
            var existingFields = newSource.Fields.Select(a => a.Name).ToList();
            foreach (var g in _currentParameters.TransposeGroups)
            {
                var tg = new TransposeGroup
                {
                    FieldList = new ObservableCollection<string>(),
                    CategoryName = g.CategoryName,
                    ValueName = g.ValueName,
                    DeleteSourceFields = g.DeleteSourceFields
                };
                foreach (var field in g.FieldList)
                {
                    if (existingFields.Contains(field))tg.FieldList.Add(field);
                }
                if (tg.FieldList.Count>1)correctedGroups.Add(tg);
            }
            _currentParameters.TransposeGroups = correctedGroups;
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }
            UpdateFieldSelector();
            TransposeGroupsListBox.ItemsSource = _currentParameters.TransposeGroups;
            CheckResult();
        }

        private void UpdateFieldSelector()
        {
            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (newSource != null)
            {
                var usedFields = NewGroup.FieldList.ToList();
                foreach (var g in _currentParameters.TransposeGroups)
                {
                    usedFields.AddRange(g.FieldList);
                }
                if (NewGroup.FieldList.Any())
                {
                    var dt = newSource.Fields.Where(a => a.Name == NewGroup.FieldList[0])
                            .Select(a => a.DataType)
                            .SingleOrDefault();
                    AvailableFields = new ObservableCollection<string>(newSource.Fields.Where(a => !a.Name.StartsWith("#")&& a.DataType==dt).Select(a => a.Name).Except(usedFields));
                }
                else
                {
                    AvailableFields = new ObservableCollection<string>(newSource.Fields.Where(a => !a.Name.StartsWith("#")).Select(a => a.Name).Except(usedFields));
                }
            }
            FieldSelector.ItemsSource = AvailableFields;
            FieldSelector.UpdateLayout();
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null)
                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            UIToParameters();
            var sourceFields = _selectedTable.Fields.Select(a => a.Name).ToList();
            var usedFields = new List<string>();
            foreach (var g in _currentParameters.TransposeGroups)
            {
                if (!g.DeleteSourceFields) continue;
                usedFields.AddRange(g.FieldList);
            }
            var fixedFields = sourceFields.Except(usedFields).ToList();
            var fieldIsExists = usedFields.Contains(NewGroup.ValueName) 
                || usedFields.Contains(NewGroup.CategoryName) 
                || fixedFields.Contains(NewGroup.ValueName) 
                || fixedFields.Contains(NewGroup.CategoryName);
            AddGroupButton.IsEnabled = NewGroup.FieldList.Count > 1 && !fieldIsExists;
            EnableExit(_currentParameters.TransposeGroups.Count() != 0);
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null || DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.TransposeGroups.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            ResetNewGroup();
            ParametersToUI();
            CheckResult();
        }

        private void ResetNewGroup()
        {
            var number = _currentParameters.TransposeGroups.Count;
            NewGroup = new TransposeGroup
            {
                CategoryName = "Имя"+ (number > 0 ? "_" + (number + 1) : string.Empty) ,
                FieldList = new ObservableCollection<string>(),
                ValueName = "Значения" + (number > 0 ? "_" + (number + 1) : string.Empty),
                DeleteSourceFields = _lastDeleteField
            };
            NewGroup.PropertyChanged += NewGroup_PropertyChanged;
            NewGroupPanel.DataContext = NewGroup;
        }

        void NewGroup_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DeleteSourceFields") _lastDeleteField = ((TransposeGroup) sender).DeleteSourceFields;
            CheckResult();
        }

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        #region Editor button events
        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as TransposeGroup;
            _currentParameters.TransposeGroups.Remove(toDelete);
            _editingIndex = -1;
            UpdateFieldSelector();
            CheckResult();
        }
        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1)
            {
                StoreGroup();
            }
            var toEdit = ((TextImageButtonBase)sender).Tag as TransposeGroup;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.TransposeGroups.IndexOf(toEdit);
            _currentParameters.TransposeGroups.Remove(toEdit);
            NewGroup = toEdit;
            NewGroupPanel.DataContext = NewGroup;
            CheckResult();
        }
        
        private void AddGroupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var group = StoreGroup();
            ResetNewGroup();
            CheckResult();
            UpdateFieldSelector();
            TransposeGroupsListBox.SelectedValue = group;
            TransposeGroupsListBox.ScrollIntoView(TransposeGroupsListBox.SelectedItem);
        }

        private TransposeGroup StoreGroup()
        {
            var group = new TransposeGroup
            {
                CategoryName = NewGroup.CategoryName,
                ValueName = NewGroup.ValueName,
                FieldList = NewGroup.FieldList,
                DeleteSourceFields = NewGroup.DeleteSourceFields
            };
            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.TransposeGroups.Add(@group);
                    break;
                default:
                    _currentParameters.TransposeGroups.Insert(_editingIndex, @group);
                    _editingIndex = -1;
                    break;
            }
            return group;
        }

        private void UpGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : TransposeGroupsListBox.Items.Count - 1;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void DownGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < TransposeGroupsListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void TopGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void BottomGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = TransposeGroupsListBox.Items.Count - 1;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void MoveGroup(int selectedIndex, int nextIndex)
        {
            var newItem = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            _currentParameters.TransposeGroups.RemoveAt(selectedIndex);
            _currentParameters.TransposeGroups.Insert(nextIndex, newItem);
            TransposeGroupsListBox.SelectedItem = newItem;
            TransposeGroupsListBox.ScrollIntoView(TransposeGroupsListBox.SelectedItem);
            TransposeGroupsListBox.UpdateLayout();
        }

        private void AddFieldButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as TextImageButtonBase;
            if (button == null) return;
            var newFieldName = button.Tag.ToString();
            NewGroup.FieldList.Add(newFieldName);
            UpdateFieldSelector();
            CheckResult();
        }

        private void RemoveFieldButton_OnClick(object sender, RoutedEventArgs e)
        {
            var button = sender as TextImageButtonBase;
            if (button == null) return;
            var newFieldName = button.Tag.ToString();
            NewGroup.FieldList.Remove(newFieldName);
            UpdateFieldSelector();
            CheckResult();
        }
        #endregion
    }
}