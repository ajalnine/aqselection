﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_TransposeFields
{
    public class Parameters
    {
        public ObservableCollection<TransposeGroup> TransposeGroups;
        public string TableName;
    }

    public class TransposeGroup : INotifyPropertyChanged
    {
        private bool _deleteSourceFields;
        public bool DeleteSourceFields
        {
            get
            {
                return _deleteSourceFields;
            }
            set
            {
                _deleteSourceFields = value;
                NotifyPropertyChanged("DeleteSourceFields");
            }
        }
        
        private string _categoryName;
        public string CategoryName
        {
            get
            {
                return _categoryName;
            }
            set
            {
                _categoryName = value;
                NotifyPropertyChanged("CategoryName");
            }
        }

        private string _valueName;
        public string ValueName
        {
            get
            {
                return _valueName;
            }
            set
            {
                _valueName = value;
                NotifyPropertyChanged("ValueName");
            }
        }

        private ObservableCollection<string> _fieldList;
        public ObservableCollection<string> FieldList
        {
            get
            {
                return _fieldList;
            }
            set
            {
                _fieldList = value;
                NotifyPropertyChanged("FieldList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
