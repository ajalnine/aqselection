﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace SLCalc_TransposeRows
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private TransposeMode _lastMode;
        private bool _lastDeleteField;
        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private DataItem _selectedTable;
        private bool _dataLoaded;
        private SLDataTable _dataTable;
        private int _editingIndex = -1;
        Task _autoFillTask;
        DataLoader _dataSetLoader;

        public static readonly DependencyProperty NewGroupProperty =
        DependencyProperty.Register("NewGroup", typeof(TransposeGroup), typeof(Page), new PropertyMetadata(new TransposeGroup
        {
            CategoryField = null,
            ValuesList = String.Empty,
            ValueField = null,
            Prefix = String.Empty,
            Mode = TransposeMode.Manual,
            DeleteSourceFields = true
        }));

        public TransposeGroup NewGroup
        {
            get { return (TransposeGroup)GetValue(NewGroupProperty); }
            set { SetValue(NewGroupProperty, value); }
        }

        public static readonly DependencyProperty AvailableFieldsProperty =
        DependencyProperty.Register("AvailableFields", typeof(ObservableCollection<string>), typeof(Page), new PropertyMetadata(new ObservableCollection<string>()));

        public ObservableCollection<string> AvailableFields
        {
            get { return (ObservableCollection<string>)GetValue(AvailableFieldsProperty); }
            set { SetValue(AvailableFieldsProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            NewGroup.PropertyChanged += NewGroup_PropertyChanged;
            NewGroupPanel.DataContext = NewGroup;
            _dataLoaded = false;
            _lastDeleteField = true;
            _lastMode = TransposeMode.Manual;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ResetNewGroup();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { TableName = _selectedTable.Name, TransposeGroups = new ObservableCollection<TransposeGroup>(), EnableGrouping = true};
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            CheckParameters();
            ParametersToUI();
            ResetNewGroup();
            ParametersToUI(availableData);
        }

        private void CheckParameters()
        {
            var correctedGroups = new ObservableCollection<TransposeGroup>();
            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (newSource == null) return;
            var existingFields = newSource.Fields.Select(a => a.Name).ToList();
            foreach (var g in _currentParameters.TransposeGroups
                .Where(g => existingFields.Contains(g.ValueField) && existingFields.Contains(g.CategoryField)))
            {
                correctedGroups.Add(g);
            }
            _currentParameters.TransposeGroups = correctedGroups;
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }
            UpdateFieldSelector();
            TransposeGroupsListBox.ItemsSource = _currentParameters.TransposeGroups;
            GroupingEnableCheckBox.IsChecked = _currentParameters.EnableGrouping;
        }

        private void UpdateFieldSelector()
        {
            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (newSource == null) return;
            AvailableFields = new ObservableCollection<string>(newSource.Fields.Where(a => !a.Name.StartsWith("#")).Select(a => a.Name));
            NameFieldSelector.ItemsSource = AvailableFields;
            NameFieldSelector.SelectedIndex = 0;
            NameFieldSelector.UpdateLayout();
            ValueFieldSelector.ItemsSource = AvailableFields;
            ValueFieldSelector.SelectedIndex = AvailableFields.Count > 1 ? 1 : 0; 
            ValueFieldSelector.UpdateLayout();
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null) _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            if (GroupingEnableCheckBox.IsChecked != null) _currentParameters.EnableGrouping = GroupingEnableCheckBox.IsChecked.Value;
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            var usedFields = new List<string>();
            foreach (var g in _currentParameters.TransposeGroups.Union(new[]{NewGroup}))
            {
                usedFields.AddRange(g.ValuesList.Split(new[] { "\r", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).Select(v => g.Prefix + v));
            }
            AddGroupButton.IsEnabled = usedFields.Count == usedFields.Distinct().Count() 
                && !(string.IsNullOrEmpty(NewGroup.ValuesList) && NewGroup.Mode != TransposeMode.NoTable);
            EnableExit(_currentParameters.TransposeGroups.Count() != 0);
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null || DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.TransposeGroups.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            _dataTable = _dataLoaded && _selectedTable != null
                ? _dataSetLoader.GetTable(_selectedTable.TableName)
                : null;
            ResetNewGroup();
            ParametersToUI();
            CheckResult();
        }

        private void ResetNewGroup()
        {
            var value = ValueFieldSelector.SelectedItem != null
                ? ValueFieldSelector.SelectedItem.ToString()
                : string.Empty;
            var name = NameFieldSelector.SelectedItem != null 
                ? NameFieldSelector.SelectedItem.ToString() 
                : string.Empty;
            NewGroup = new TransposeGroup
            {
                Mode = _lastMode,
                ValueField = value,
                CategoryField = name,
                Prefix = string.Format("{0}, {1}=", value, name),
                ValuesList = _lastMode == TransposeMode.Auto ? GetValuesForField(name) : string.Empty,
                DeleteSourceFields = _lastDeleteField
            };
            NewGroup.PropertyChanged += NewGroup_PropertyChanged;
            NewGroupPanel.DataContext = NewGroup;
            CheckResult();
        }

        private string GetValuesForField(string name)
        {
            if (_dataTable == null || !_dataTable.ColumnNames.Contains(name)) return string.Empty;
            var index = _dataTable.ColumnNames.IndexOf(name);
            var values = (from a in _dataTable.Table 
                          where a!=null && a.Row!=null && a.Row[index]!=null && !string.IsNullOrWhiteSpace(a.Row[index].ToString()) 
                          select a.Row[index].ToString()).Distinct().OrderBy(a => a);
            var result = String.Empty;
            var isFirst = true;
            foreach (var v in values)
            {
                if (!isFirst) result += "\r";
                result += v;
                isFirst = false;
            }
            return result;
        }

        void NewGroup_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DeleteSourceFields") _lastDeleteField = ((TransposeGroup) sender).DeleteSourceFields;
            if (e.PropertyName == "CategoryField" || e.PropertyName == "ValueField")
            {
                var group = ((TransposeGroup) sender);
                if (e.PropertyName == "CategoryField") NewGroup.ValuesList = group.Mode == TransposeMode.Auto ? GetValuesForField(group.CategoryField) : string.Empty;
                NewGroup.Prefix = string.Format("{0}, {1}=", @group.ValueField, @group.CategoryField);
            }
            CheckResult();
        }

        public void StateChanged(OperationViewMode ovm) { }

        #region Read of data
        private void AutoButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataRequest != null)
            {
                _autoFillTask = new Task(new [] { "Исполнение расчета", "Прием данных" }, "Данные для автозаполнения", TaskPanel);
                _autoFillTask.SetState(0, TaskState.Processing);
                AutoButton.IsEnabled = false;
                AutoModeRadioButton.IsEnabled = false;
                DataRequest.Invoke(this, new EventArgs());
            }
        }
        private void RemoveDataButton_OnClick(object sender, RoutedEventArgs e)
        {
            AutoButton.IsEnabled = true;
            RemoveDataButton.Visibility = Visibility.Collapsed;
            AutoButton.Visibility = Visibility.Visible;
            AutoModeRadioButton.IsEnabled = false;
            if (NewGroup.Mode==TransposeMode.Auto)NewGroup.Mode = TransposeMode.Manual;
            DataRequiredTextBlock.Visibility = Visibility.Visible;
            DataExistsTextBlock.Visibility = Visibility.Collapsed;
            _dataLoaded = false;
            _dataTable = null;
        }
        public void DataRequestReadyCallback(string fileName)
        {
            _autoFillTask.SetState(0, TaskState.Ready);
            _autoFillTask.SetState(1, TaskState.Processing);
            _dataSetLoader = new DataLoader();
            _dataSetLoader.DataProcessed += Data_DataProcessed;
            _dataSetLoader.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            _autoFillTask.SetState(0, TaskState.Ready);
            _autoFillTask.SetState(1, TaskState.Ready);
            Dispatcher.BeginInvoke(() =>
            {
                RemoveDataButton.Visibility = Visibility.Visible;
                AutoModeRadioButton.IsEnabled = true;
                DataRequiredTextBlock.Visibility = Visibility.Collapsed;
                DataExistsTextBlock.Visibility = Visibility.Visible;
                AutoButton.Visibility = Visibility.Collapsed;
                _dataTable = _dataSetLoader.GetTable(_currentParameters.TableName);
                _dataLoaded = true;
            });
        }
        #endregion

        #region Editor button events
        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as TransposeGroup;
            _currentParameters.TransposeGroups.Remove(toDelete);
            _editingIndex = -1;
            UpdateFieldSelector();
            CheckResult();
        }
        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1)
            {
                StoreGroup();
            }
            var toEdit = ((TextImageButtonBase)sender).Tag as TransposeGroup;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.TransposeGroups.IndexOf(toEdit);
            _currentParameters.TransposeGroups.Remove(toEdit);
            NewGroup = toEdit;
            NewGroupPanel.DataContext = NewGroup;
            CheckResult();
        }

        private void AddGroupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var group = StoreGroup();
            ResetNewGroup();
            CheckResult();
            TransposeGroupsListBox.SelectedValue = group;
            TransposeGroupsListBox.ScrollIntoView(TransposeGroupsListBox.SelectedItem);
        }

        private TransposeGroup StoreGroup()
        {
            var group = new TransposeGroup
            {
                CategoryField = NewGroup.CategoryField,
                ValueField = NewGroup.ValueField,
                ValuesList = NewGroup.ValuesList,
                Mode = NewGroup.Mode,
                Prefix = NewGroup.Prefix,
                DeleteSourceFields = NewGroup.DeleteSourceFields
            };
            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.TransposeGroups.Add(group);
                    break;
                default:
                    _currentParameters.TransposeGroups.Insert(_editingIndex, group);
                    _editingIndex = -1;
                    break;
            }
            return group;
        }

        private void UpGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : TransposeGroupsListBox.Items.Count - 1;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void DownGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < TransposeGroupsListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void TopGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void BottomGroup_Click(object sender, RoutedEventArgs e)
        {
            if (TransposeGroupsListBox.SelectedItem == null) return;
            var selected = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            if (selected == null) return;
            var selectedIndex = TransposeGroupsListBox.Items.IndexOf(selected);
            var nextIndex = TransposeGroupsListBox.Items.Count - 1;
            MoveGroup(selectedIndex, nextIndex);
        }

        private void MoveGroup(int selectedIndex, int nextIndex)
        {
            var newItem = TransposeGroupsListBox.SelectedItem as TransposeGroup;
            _currentParameters.TransposeGroups.RemoveAt(selectedIndex);
            _currentParameters.TransposeGroups.Insert(nextIndex, newItem);
            TransposeGroupsListBox.SelectedItem = newItem;
            TransposeGroupsListBox.ScrollIntoView(TransposeGroupsListBox.SelectedItem);
            TransposeGroupsListBox.UpdateLayout();
        }

        #endregion

        private void ModeButton_OnClick(object sender, RoutedEventArgs e)
        {
            var p = ((RadioButton)sender).Tag.ToString();
            _lastMode = (TransposeMode)Enum.Parse(typeof(TransposeMode), p, true);
            switch (p)
            {
                case "Auto":
                    NewGroup.ValuesList = GetValuesForField(NewGroup.CategoryField);
                    WarningNoTableTextBlock.Visibility = Visibility.Collapsed;
                    return;

                case "NoTable":
                    NewGroup.ValuesList = string.Empty;
                    WarningNoTableTextBlock.Visibility = Visibility.Visible;
                    return;

                case "Manual":
                    WarningNoTableTextBlock.Visibility = Visibility.Collapsed;
                    return;
            }
        }
    }
    public class ModeButtonConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = parameter.ToString();
            switch (p)
            {
                case "Auto":
                    return (TransposeMode)value == TransposeMode.Auto;
                case "Manual":
                    return (TransposeMode)value == TransposeMode.Manual;
                case "NoTable":
                    return (TransposeMode)value == TransposeMode.NoTable;
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = parameter.ToString();
            switch (p)
            {
                case "Auto":
                    return (bool)value ? (object)TransposeMode.Auto : null;
                case "Manual":
                    return (bool)value ? (object)TransposeMode.Manual : null;
                case "NoTable":
                    return (bool)value ? (object)TransposeMode.NoTable : null;
            }
            return null;
        }
    }

    public class ModeConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = value.ToString();
            switch (p)
            {
                case "Auto":
                    return "Авто";
                case "Manual":
                    return "Ввод";
                case "NoTable":
                    return "По факту";
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
           
            return null;
        }
    }

    public class PrefixValuesConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (TransposeGroup) value;
            var strings = p.ValuesList.Split(new[] { "\r", "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            var result = string.Empty;
            var isFirst = true;
            foreach (var s in strings)
            {
                if (!isFirst) result += "\r";
                result += p.Prefix + s;
                isFirst = false;
            }
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}