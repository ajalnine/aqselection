﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_TransposeRows
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null && DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;
            _inputs = new List<DataItem>();
            var selectedTable = _dataSources.FirstOrDefault(s => s.Name == _currentParameters.TableName);

            if (selectedTable != null)
            {
                _inputs.Add(selectedTable);

                var output = new DataItem
                {
                    Fields = new List<DataField>(),
                    DataItemType = selectedTable.DataItemType,
                    Name = selectedTable.Name,
                    TableName = selectedTable.TableName
                };
                var usedFields = new List<string>();
                var sourceFields = selectedTable.Fields.Select(a => a.Name).ToList();
                foreach (var g in _currentParameters.TransposeGroups.Where(a=>a.DeleteSourceFields))
                {
                    usedFields.Add(g.CategoryField);
                    usedFields.Add(g.ValueField);
                    if (selectedTable.Fields.Any(a => a.Name == "#" + g.ValueField + "_Цвет"))
                    {
                        usedFields.Add("#" + g.ValueField + "_Цвет");
                    }
                }
                var fixedFields = selectedTable.Fields.Where(a => !usedFields.Contains(a.Name)).ToList();
                output.Fields.AddRange(fixedFields);
                
                foreach (var t in _currentParameters.TransposeGroups)
                {
                    var singleOrDefaultField = selectedTable.Fields.SingleOrDefault(a => a.Name == t.ValueField);
                    if (singleOrDefaultField == null) continue;
                    var dt = singleOrDefaultField.DataType;

                    foreach (var value in t.ValuesList.Split(new[] {"\r\n", "\r", "\n"}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        output.Fields.Add(new DataField {Name = t.Prefix + value, DataType = dt});
                    }
                }
                _outputs = new List<DataItem> { output };
                if (usedFields.Any(usedField => !sourceFields.Contains(usedField)))
                {
                    if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                }
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
