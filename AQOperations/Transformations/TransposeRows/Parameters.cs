﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_TransposeRows
{
    public class Parameters
    {
        public ObservableCollection<TransposeGroup> TransposeGroups;
        public string TableName;
        public bool EnableGrouping;
    }

    public class TransposeGroup : INotifyPropertyChanged
    {
        private bool _deleteSourceFields;
        public bool DeleteSourceFields
        {
            get
            {
                return _deleteSourceFields;
            }
            set
            {
                _deleteSourceFields = value;
                NotifyPropertyChanged("DeleteSourceFields");
            }
        }
        
        private TransposeMode _mode;
        public TransposeMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                NotifyPropertyChanged("Mode");
            }
        }

        private string _categoryField;
        public string CategoryField
        {
            get
            {
                return _categoryField;
            }
            set
            {
                _categoryField = value;
                NotifyPropertyChanged("CategoryField");
            }
        }

        private string _valueField;
        public string ValueField
        {
            get
            {
                return _valueField;
            }
            set
            {
                _valueField = value;
                NotifyPropertyChanged("ValueField");
            }
        }

        private string _prefix;
        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                _prefix = value;
                NotifyPropertyChanged("Prefix");
            }
        }

        private string _valuesList;
        public string ValuesList
        {
            get
            {
                return _valuesList;
            }
            set
            {
                _valuesList = value;
                NotifyPropertyChanged("ValuesList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    public enum TransposeMode
    {
        Auto, NoTable, Manual
    }
}
