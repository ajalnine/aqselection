﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_DeleteTables
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private List<string> _deleted;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData.OrderBy(a=>a.TableName).ToList();

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            _deleted = new List<string>();

            var dataIsCorrect = _dataSources.Select(d => d.Name).Intersect(_currentParameters.TablesToDelete).Count() == _currentParameters.TablesToDelete.Count;

            foreach (var d in _currentParameters.TablesToDelete)
            {
                _inputs.Add(_dataSources.SingleOrDefault(n => n.Name == d));
                _deleted.Add(d);
            }
            var modified = from a in _currentParameters.TablesTransformation where a.NewTableName != a.OldTableName select a;
            foreach (var table in modified)
            {
                var oldtable = _dataSources.SingleOrDefault(a => a.Name == table.OldTableName);
                if (oldtable == null) continue;
                _inputs.Add(oldtable);

                var newtable = new DataItem
                {
                    DataItemType = DataType.Selection,
                    TableName = table.NewTableName,
                    Name = table.NewTableName,
                    Fields = oldtable.Fields.ToList()
                };
                _outputs.Add(newtable);
                _deleted.Add(table.OldTableName);
            }

            if (DataFlowError != null && !dataIsCorrect) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = _deleted });
        }
    }
}
