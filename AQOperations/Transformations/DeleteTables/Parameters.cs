﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_DeleteTables
{
    public class Parameters
    {
        public ObservableCollection<string> TablesToDelete;
        public ObservableCollection<NewTableData> TablesTransformation;
    }

    public class NewTableData : INotifyPropertyChanged
    {
        private string _oldtablename;
        public string OldTableName
        {
            get
            {
                return _oldtablename;
            }
            set
            {
                _oldtablename = value;
                NotifyPropertyChanged("OldTableName");
            }
        }

        private string _newtablename;
        public string NewTableName
        {
            get
            {
                return _newtablename;
            }
            set
            {
                _newtablename = value;
                NotifyPropertyChanged("NewTableName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
