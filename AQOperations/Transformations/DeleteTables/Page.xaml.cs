﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_DeleteTables
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;


        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            _currentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            UpdateLayout();
            EnableExit(true);

            UpdateTablesSource();
            CheckResult();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckResult()) return;
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void UpdateTablesSource()
        {
            if (_currentParameters.TablesTransformation == null)
            {
                _currentParameters.TablesTransformation = new ObservableCollection<NewTableData>();
                _currentParameters.TablesToDelete = new ObservableCollection<string>();
                foreach (var a in _dataSources)
                {
                    var ndt = new NewTableData { NewTableName = a.Name, OldTableName = a.Name };
                    _currentParameters.TablesTransformation.Add(ndt);
                }
            }
            else
            {
                var existingTables = (from s in _dataSources select s.Name).ToList();
                var transformedTables = (from c in _currentParameters.TablesTransformation select c.OldTableName).ToList();
                var tablesToDelete = (from d in _currentParameters.TablesToDelete select d).ToList();

                List<string> newAddedTables;
                if (transformedTables!=null && transformedTables.Count>0)newAddedTables = existingTables.Except(transformedTables.Concat(tablesToDelete)).ToList();
                else newAddedTables = existingTables.Except(tablesToDelete).ToList();
                var nonExistingTables = transformedTables.Concat(tablesToDelete).Except(existingTables).ToList();
                foreach (var a in newAddedTables)
                {
                    _currentParameters.TablesTransformation.Add(new NewTableData { OldTableName = a, NewTableName = a});
                }

                var updatedFieldTransformation = new ObservableCollection<NewTableData>();
                var updatedDeletedFields = new ObservableCollection<string>();

                foreach (var f in _currentParameters.TablesTransformation)
                {
                    if (!nonExistingTables.Contains(f.OldTableName)) updatedFieldTransformation.Add(f);
                }
                foreach (var f in _currentParameters.TablesToDelete)
                {
                    if (!nonExistingTables.Contains(f)) updatedDeletedFields.Add(f);
                }
                _currentParameters.TablesTransformation = updatedFieldTransformation;
                _currentParameters.TablesToDelete = updatedDeletedFields;
            }
            TablesEditor.ItemsSource = _currentParameters.TablesTransformation;
            TablesEditor.UpdateLayout();
            DeletedTables.ItemsSource = _currentParameters.TablesToDelete;
            DeletedTables.UpdateLayout();
        }

        private bool CheckResult()
        {
            if (_currentParameters.TablesTransformation.Count == 0)
            {
                ShowTableNameError(true, "Не остается ни одной таблицы");
                return false;
            }
            var isDoubled = (from s in _currentParameters.TablesTransformation group s by s.NewTableName into g where g.Count() > 1 select g).Any();
            if (!isDoubled) return true;
            ShowTableNameError(true, "Есть повторяющиеся имена таблиц");
            return false;
        }

        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Text = message;
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
        }

        #region Обработка событий кнопок
        private void UpSort_Click(object sender, RoutedEventArgs e)
        {
            if (TablesEditor.SelectedItem == null) return;
            var selected = (NewTableData)TablesEditor.SelectedItem;
            if (selected == null) return;
            var selectedIndex = _currentParameters.TablesTransformation.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : _currentParameters.TablesTransformation.Count - 1;
            MoveSort(selectedIndex, nextIndex);
        }

        private void DownSort_Click(object sender, RoutedEventArgs e)
        {
            if (TablesEditor.SelectedItem == null) return;
            var selected = (NewTableData)TablesEditor.SelectedItem;
            if (selected == null) return;
            var selectedIndex = _currentParameters.TablesTransformation.IndexOf(selected);
            var nextIndex = (selectedIndex < _currentParameters.TablesTransformation.Count - 1) ? selectedIndex + 1 : 0;
            MoveSort(selectedIndex, nextIndex);
        }

        private void TopSort_Click(object sender, RoutedEventArgs e)
        {
            if (TablesEditor.SelectedItem == null) return;
            var selected = (NewTableData)TablesEditor.SelectedItem;
            if (selected == null) return;
            var selectedIndex = _currentParameters.TablesTransformation.IndexOf(selected);
            MoveSort(selectedIndex, 0);
        }

        private void BottomSort_Click(object sender, RoutedEventArgs e)
        {
            if (TablesEditor.SelectedItem == null) return;
            var selected = (NewTableData)TablesEditor.SelectedItem;
            if (selected == null) return;
            var selectedIndex = _currentParameters.TablesTransformation.IndexOf(selected);
            var nextIndex = _currentParameters.TablesTransformation.Count - 1;
            MoveSort(selectedIndex, nextIndex);
        }

        private void MoveSort(int selectedIndex, int nextIndex)
        {
            var newItem = _currentParameters.TablesTransformation[selectedIndex];

            _currentParameters.TablesTransformation.RemoveAt(selectedIndex);
            _currentParameters.TablesTransformation.Insert(nextIndex, newItem);
            TablesEditor.SelectedItem = newItem;
            TablesEditor.UpdateLayout();
            TablesEditor.ScrollIntoView(TablesEditor.SelectedItem, TablesEditor.Columns[0]);
            TablesEditor.UpdateLayout();
        }

        private void RemoveTableButton_Click(object sender, RoutedEventArgs e)
        {
            var mib = sender as TextImageButtonBase;
            if (mib == null) return; 
            var tableName = mib.Tag.ToString();
            _currentParameters.TablesTransformation.Remove((from c in _currentParameters.TablesTransformation where c.OldTableName == tableName select c).SingleOrDefault());
            _currentParameters.TablesToDelete.Add(tableName);
        }

        private void RestoreTableButton_Click(object sender, RoutedEventArgs e)
        {
            var mib = sender as TextImageButtonBase;
            if (mib == null) return; 
            var tableName = mib.Tag.ToString();
            var ntd = new NewTableData { OldTableName = tableName, NewTableName = tableName };
            _currentParameters.TablesTransformation.Add(ntd);
            TablesEditor.ScrollIntoView(ntd, TablesEditor.Columns[0]);
            _currentParameters.TablesToDelete.Remove(tableName);
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }
}