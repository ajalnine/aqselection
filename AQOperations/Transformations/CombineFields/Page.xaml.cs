﻿using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_CombineFields
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            CurrentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            DataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();

            if (DataSources.Select(s => s.Name).Contains(CurrentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedItem = CurrentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                var RequiredFields = GetAllUsedFields();
                if (RequiredFields != null)
                {
                    var NewSource = DataSources.Where(s => s.Name == CurrentParameters.TableName);
                    if (NewSource.Count() > 0)
                    {
                        var NewFields = from s in NewSource.First().Fields select s.Name;
                        if (RequiredFields.Intersect(NewFields).Count() != RequiredFields.Count())
                        {
                            RemoveGroupsWithLostFields();
                            UpdateFieldsSource();
                            ResetInterface();
                            return;
                        }
                    }
                }
                UpdateFieldsSource();
                ResetInterface();
                UpdateData();
                UpdateAddGroupButtonState();
            }
            else
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
                UpdateFieldsSource();
                ResetInterface();
            }
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void RemoveGroupsWithLostFields()
        {
            var NewSource = DataSources.Where(s => s.Name == CurrentParameters.TableName);
            var NewFields = from s in NewSource.First().Fields select s.Name;
            ObservableCollection<FieldGroupData> NewGroups = new ObservableCollection<FieldGroupData>();
            foreach (var g in CurrentParameters.Groups)
            {
                if (g.CombinedFields.Except(NewFields).Count() > 0)
                {
                    var NewCombined = g.CombinedFields.Intersect(NewFields).ToList();
                    if (NewCombined.Count >= 2)
                    {
                        g.CombinedFields = new ObservableCollection<string>();
                        foreach (var a in NewCombined)
                        {
                            g.CombinedFields.Add(a);
                        }
                        NewGroups.Add(g);
                    }
                }
            }
            if (CurrentParameters.UnfinishedGroup.CombinedFields.Except(NewFields).Count() > 0)
            {
                var NewCombinedForUnfinished = CurrentParameters.UnfinishedGroup.CombinedFields.Intersect(NewFields).ToList();
                if (NewCombinedForUnfinished.Count >= 2)
                {
                    CurrentParameters.UnfinishedGroup.CombinedFields = new ObservableCollection<string>();
                    foreach (var a in NewCombinedForUnfinished)
                    {
                        CurrentParameters.UnfinishedGroup.CombinedFields.Add(a);
                    }
                }
            }
            CurrentParameters.Groups = NewGroups;
        }

        private void ResetInterface()
        {
            if (CurrentParameters.Groups == null) CurrentParameters.Groups = new ObservableCollection<FieldGroupData>();
            else
            {
                GroupView.ItemsSource = null;
                GroupView.UpdateLayout();
                GroupView.ItemsSource = CurrentParameters.Groups;
                GroupView.UpdateLayout();
            }
            if (CurrentParameters.UnfinishedGroup == null)
            {
                CurrentParameters.UnfinishedGroup = new FieldGroupData();
            }
            else
            {
                NewGroupFields.ItemsSource = null;
                NewGroupFields.UpdateLayout();
                NewGroupFields.ItemsSource = CurrentParameters.UnfinishedGroup.CombinedFields;
                NewGroupFields.UpdateLayout();
                NewGroupName.Text = CurrentParameters.UnfinishedGroup.Name ?? String.Empty;
            }
            UpdateNewGroupPanelVisibility();
        }

        private void UpdateNewGroupPanelVisibility()
        {
            var c = GetUnusedFieldList();
            NewGroupPanel.Visibility = (c != null && c.Count() > 0) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void UpdateFieldsSource()
        {
            FieldSelector.ItemsSource = GetUnusedFieldList();
            FieldSelector.UpdateLayout();
        }

        private IEnumerable<string> GetUnusedFieldList()
        {
            var NewSource = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();
            if (NewSource == null) return null;
            if (CurrentParameters == null || CurrentParameters.UnfinishedGroup == null || CurrentParameters.UnfinishedGroup.CombinedFields == null) return null;
            if (CurrentParameters.UnfinishedGroup.CombinedFields != null)
            {
                IEnumerable<string> Restricted = GetAllUsedFields();
                if (CurrentParameters.UnfinishedGroup.CombinedFields.Count > 0)
                {
                    return (from f in NewSource.Fields where f.DataType == (from n in NewSource.Fields where n.Name == CurrentParameters.UnfinishedGroup.CombinedFields[0] select n.DataType).Single() select f.Name).Except(Restricted);
                }
                else
                {
                    return (from f in NewSource.Fields select f.Name).Except(Restricted);
                }
            }
            else return from f in NewSource.Fields select f.Name;
        }

        private IEnumerable<string> GetAllUsedFields()
        {
            if (CurrentParameters.Groups == null) return null;
            List<string> Used = new List<string>();
            foreach (var g in CurrentParameters.Groups)
            {
                if (g == null) continue;
                Used = Used.Union(g.CombinedFields).ToList();
            }
            if (CurrentParameters.UnfinishedGroup == null || CurrentParameters.UnfinishedGroup.CombinedFields == null) return Used.AsEnumerable<string>();
            else return Used.AsEnumerable<string>().Union(CurrentParameters.UnfinishedGroup.CombinedFields);
        }

        private bool CheckData()
        {
            List<string> ResultingNames = GetResultNames();

            bool IsDoubled = (from s in ResultingNames group s by s into g where g.Count() > 1 select g).Count() > 0;
            if (IsDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся поля");
                return false;
            } 
            
            bool IsEmpty = (from s in ResultingNames where s.Trim()==string.Empty select s).Count()>0;

            if (IsEmpty)
            {
                ShowTableNameError(true, "Имена некоторых групп не заданы");
                return false;
            }

            if (CurrentParameters.UnfinishedGroup.CombinedFields.Count()>0)
            {
                ShowTableNameError(true, "Создание группы не завершено");
                return false;
            }

            if (CurrentParameters.Groups.Count() == 0)
            {
                ShowTableNameError(true, "Ни одной группы не задано");
                return false;
            }
            
            ShowTableNameError(false, "");
            return true;
        }

        private void ShowTableNameError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private List<string> GetResultNames()
        {
            DataItem SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).First();
            List<string> ProcessedFields = new List<string>();
            List<string> ResultNames = new List<string>();
            
            foreach (DataField df in SelectedTable.Fields)
            {
                if (!ProcessedFields.Contains(df.Name))
                {
                    FieldGroupData CurrentGroup = FieldGroupDataHelper.GetFieldGroup(df.Name, CurrentParameters);

                    if (CurrentGroup == null)
                    {
                        ResultNames.Add(df.Name);
                    }
                    else
                    {
                        int a1 = CurrentGroup.EmitMax ? 1 : 0;
                        int a2 = CurrentGroup.EmitMin ? 1 : 0;
                        int a3 = CurrentGroup.EmitAverage ? 1 : 0;
                        int a4 = CurrentGroup.EmitSum ? 1 : 0;
                        int a5 = CurrentGroup.EmitFirst ? 1 : 0;
                        int a6 = CurrentGroup.EmitCount ? 1 : 0;
                        int a7 = CurrentGroup.EmitLast ? 1 : 0;
                        int a8 = CurrentGroup.EmitRange ? 1 : 0;
                        int a9 = CurrentGroup.EmitList ? 1 : 0;
                        if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9!= 1)
                        {
                            if (CurrentGroup.EmitMin) ResultNames.Add(CurrentGroup.Name + "_Min");
                            if (CurrentGroup.EmitMax) ResultNames.Add(CurrentGroup.Name + "_Max");
                            if (CurrentGroup.EmitRange) ResultNames.Add(CurrentGroup.Name + "_Размах");
                            if (CurrentGroup.EmitAverage) ResultNames.Add(CurrentGroup.Name + "_Среднее");
                            if (CurrentGroup.EmitSum) ResultNames.Add(CurrentGroup.Name + "_Сумма");
                            if (CurrentGroup.EmitFirst) ResultNames.Add(CurrentGroup.Name + "_Первое");
                            if (CurrentGroup.EmitFirst) ResultNames.Add(CurrentGroup.Name + "_Последнее");
                            if (CurrentGroup.EmitCount) ResultNames.Add(CurrentGroup.Name + "_Число");
                            if (CurrentGroup.EmitList) ResultNames.Add(CurrentGroup.Name + "_Список");
                        }
                        else
                        {
                            ResultNames.Add(CurrentGroup.Name); 
                        }
                        ProcessedFields = ProcessedFields.Concat(CurrentGroup.CombinedFields).ToList();
                    }
                }
            }
            return ResultNames;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckData()) return;
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private void UpdateData()
        {
            Inputs = new List<DataItem>();
            DataItem SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).First();
            Inputs.Add(SelectedTable);
            
            DataItem Output = new DataItem();
            Output.Fields = new List<DataField>();

            List<string> ProcessedFields = new List<string>();

            foreach (DataField df in SelectedTable.Fields)
            {
                if (!ProcessedFields.Contains(df.Name))
                {
                    FieldGroupData CurrentGroup = FieldGroupDataHelper.GetFieldGroup(df.Name, CurrentParameters);

                    if (CurrentGroup == null)
                    {
                        Output.Fields.Add(new DataField() { Name = df.Name, DataType = df.DataType });
                    }
                    else
                    {
                        string dataType = df.DataType;

                        int a1 = CurrentGroup.EmitMax ? 1 : 0;
                        int a2 = CurrentGroup.EmitMin ? 1 : 0;
                        int a3 = CurrentGroup.EmitAverage ? 1 : 0;
                        int a4 = CurrentGroup.EmitSum ? 1 : 0;
                        int a5 = CurrentGroup.EmitFirst ? 1 : 0;
                        int a6 = CurrentGroup.EmitCount ? 1 : 0;
                        int a7 = CurrentGroup.EmitLast ? 1 : 0;
                        int a8 = CurrentGroup.EmitRange ? 1 : 0;
                        int a9 = CurrentGroup.EmitList ? 1 : 0;

                        if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 != 1)
                        {
                            if (CurrentGroup.EmitMin) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Min", DataType = dataType });
                            if (CurrentGroup.EmitMax) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Max", DataType = dataType });
                            if (CurrentGroup.EmitRange) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Размах", DataType = "Double" });
                            if (CurrentGroup.EmitAverage) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Среднее", DataType = "Double" });
                            if (CurrentGroup.EmitSum) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Сумма", DataType = dataType });
                            if (CurrentGroup.EmitFirst) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Первое", DataType = dataType });
                            if (CurrentGroup.EmitLast) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Последнее", DataType = dataType });
                            if (CurrentGroup.EmitCount) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Число", DataType = "Int32" });
                            if (CurrentGroup.EmitList) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Список", DataType = "String" });
                        }
                        else
                        {
                            Output.Fields.Add(new DataField() { Name = CurrentGroup.Name, DataType = dataType });
                        }
                        ProcessedFields = ProcessedFields.Concat(CurrentGroup.CombinedFields).ToList();
                    }
                }
            }
            Output.DataItemType = SelectedTable.DataItemType;
            Output.Name = SelectedTable.Name;
            Output.TableName = SelectedTable.TableName;
            Outputs = new List<DataItem>();
            Outputs.Add(Output);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentParameters.Groups = new ObservableCollection<FieldGroupData>();

            if (DataSourceSelector.SelectedItem != null)
            {
                CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
                UpdateFieldsSource();
            }
            ClearUnfinishedGroupInterface();
            UpdateAddGroupButtonState();
            UpdateNewGroupPanelVisibility();
        }

        private void AddField_Click(object sender, RoutedEventArgs e)
        {
            string FieldName = (sender as TextImageButtonBase).Tag.ToString();
            if (CurrentParameters == null) CurrentParameters = new Parameters();
            if (CurrentParameters.UnfinishedGroup == null) CurrentParameters.UnfinishedGroup = new FieldGroupData();
            if (CurrentParameters.UnfinishedGroup.CombinedFields == null) CurrentParameters.UnfinishedGroup.CombinedFields = new ObservableCollection<string>();
            CurrentParameters.UnfinishedGroup.CombinedFields.Add(FieldName);
            if (CurrentParameters.UnfinishedGroup.Name == string.Empty || CurrentParameters.UnfinishedGroup.Name == null)
            {
                CurrentParameters.UnfinishedGroup.Name = FieldName;
                NewGroupName.Text = FieldName;
            }
            UpdateFieldsSource();
            UpdateAddGroupButtonState();
        }

        private void NewGroupName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CurrentParameters.UnfinishedGroup.Name = NewGroupName.Text;
        }
        
        private void RemoveValueFromListButton_Click(object sender, RoutedEventArgs e)
        {
            string FieldName = (sender as TextImageButtonBase).Tag.ToString();

            CurrentParameters.UnfinishedGroup.CombinedFields.Remove(FieldName);
            if (CurrentParameters.UnfinishedGroup.CombinedFields.Count == 0)
            {
                NewGroupName.Text = string.Empty;
                CurrentParameters.UnfinishedGroup.Name = string.Empty;
            }
            UpdateFieldsSource();
            UpdateAddGroupButtonState();
            UpdateNewGroupPanelVisibility();
        }

        private void UpdateAddGroupButtonState()
        {
            AddGroup.IsEnabled = CurrentParameters.UnfinishedGroup.CombinedFields.Count > 1;
        }

        private void AddGroup_Click(object sender, RoutedEventArgs e)
        {
            string CurrentDataType = (from n in DataSources.Where(s => s.Name == CurrentParameters.TableName).First().Fields where n.Name == CurrentParameters.UnfinishedGroup.CombinedFields[0] select n.DataType).Single();
            switch (CurrentDataType.ToLower())
            {
                case "string":
                    CurrentParameters.UnfinishedGroup.DisableAvg = true;
                    CurrentParameters.UnfinishedGroup.DisableSum = false; 
                    CurrentParameters.UnfinishedGroup.EmitFirst = true;
                    break;
                case "datetime":
                    CurrentParameters.UnfinishedGroup.DisableAvg = true;
                    CurrentParameters.UnfinishedGroup.DisableSum = true;
                    CurrentParameters.UnfinishedGroup.EmitMax = true;
                    break;
                default:
                    CurrentParameters.UnfinishedGroup.DisableAvg = false;
                    CurrentParameters.UnfinishedGroup.DisableSum = false;
                    CurrentParameters.UnfinishedGroup.EmitAverage = true;
                    break;
            }
            CurrentParameters.Groups.Add(CurrentParameters.UnfinishedGroup);
            
            ClearUnfinishedGroupInterface();
            UpdateNewGroupPanelVisibility();
            CheckData();
        }

        private void ClearUnfinishedGroupInterface()
        {
            CurrentParameters.UnfinishedGroup = new FieldGroupData();
            CurrentParameters.UnfinishedGroup.CombinedFields = new ObservableCollection<string>();
            CurrentParameters.UnfinishedGroup.Name = string.Empty;
            NewGroupName.Text = string.Empty;
            NewGroupFields.ItemsSource = CurrentParameters.UnfinishedGroup.CombinedFields;
            NewGroupFields.UpdateLayout();
            UpdateAddGroupButtonState();
            UpdateFieldsSource();
            GroupView.ItemsSource = CurrentParameters.Groups;
            GroupView.UpdateLayout();
        }

        private void RemoveGroupFromListButton_Click(object sender, RoutedEventArgs e)
        {
            string GroupName = (sender as TextImageButtonBase).Tag.ToString();
            CurrentParameters.Groups.Remove((from g in CurrentParameters.Groups where g.Name == GroupName select g).Single());
            UpdateFieldsSource();
            UpdateAddGroupButtonState();
            UpdateNewGroupPanelVisibility();
        }

        private void RemoveValueFromReadyGroup_Click(object sender, RoutedEventArgs e)
        {
            string FieldName = (sender as TextImageButtonBase).Tag.ToString();
            FieldGroupData fgd = FieldGroupDataHelper.GetFieldGroup(FieldName, CurrentParameters);
            if (fgd != null)
            {
                fgd.CombinedFields.Remove(FieldName);
                if (fgd.CombinedFields.Count() < 2) CurrentParameters.Groups.Remove(fgd);
                UpdateFieldsSource();
            }
            UpdateAddGroupButtonState();
            UpdateNewGroupPanelVisibility();
        }

        private void AutoButton_Click(object sender, RoutedEventArgs e)
        {
             var FieldList = from s in GetUnusedFieldList().ToList() where Regex.Match(s, @".+_[0-9]+").Success select s;
             var FieldList2 = from s in GetUnusedFieldList().ToList() where Regex.Match(s, @".+\(.+\)").Success select s;

             var FieldNames = (from f in FieldList select Regex.Replace(f, @"_[0-9]+$", string.Empty)).Distinct();
             var FieldNames2 = (from f in FieldList2 select Regex.Replace(f, @"\(.+\)$", string.Empty)).Distinct();

             foreach (string Name in FieldNames.Union(FieldNames2))
             {
                 List<string> GroupNames = (from f in FieldList.Union(FieldList2) where f.StartsWith(Name) orderby f select f ).ToList();
                 if (GroupNames.Count > 1)
                 {
                     var CurrentDataTypes = (from n in DataSources.Where(s => s.Name == CurrentParameters.TableName).First().Fields where GroupNames.Contains(n.Name) select n.DataType).Distinct();
                     if (CurrentDataTypes.Count() == 1)
                     {
                         FieldGroupData fgd = new FieldGroupData();
                         string CurrentDataType = CurrentDataTypes.Single();
                         fgd.Name = Name;
                         fgd.CombinedFields = new ObservableCollection<string>();
                         foreach(string gn in GroupNames)
                         {
                             fgd.CombinedFields.Add(gn);
                         }
                         switch (CurrentDataType.ToLower())
                         {
                             case "string":
                                 fgd.DisableAvg = true;
                                 fgd.DisableSum = false;
                                 fgd.EmitFirst = true;
                                 break;
                             case "datetime":
                                 fgd.DisableAvg = true;
                                 fgd.DisableSum = true;
                                 fgd.EmitMax = true;
                                 break;
                             default:
                                 fgd.DisableAvg = false;
                                 fgd.DisableSum = false;
                                 fgd.EmitAverage = true;
                                 break;
                         }
                         CurrentParameters.Groups.Add(fgd);
                     }
                 }
             }
             UpdateAddGroupButtonState();
             UpdateFieldsSource();
             GroupView.ItemsSource = CurrentParameters.Groups;
             GroupView.UpdateLayout();
             CheckData();
        }

        public void StateChanged(OperationViewMode OVM) { }
        public void DataRequestReadyCallback(string fileName) { }
    }

    public class InverseBooleanConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
    }
}
