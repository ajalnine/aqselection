﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_CombineFields
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (string.IsNullOrEmpty(parametersXml))
            {
                if (this.DataFlowFinished != null)
                {
                    this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = null, Outputs = null, Deleted = null });
                }
                return;
            }
            
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataSources = AvailableData;

            Inputs = new List<DataItem>();
            DataItem SelectedTable = DataSources.Where(s => s.Name == CurrentParameters.TableName).FirstOrDefault();

            if (SelectedTable != null)
            {
                Inputs.Add(SelectedTable);

                DataItem Output = new DataItem();
                Output.Fields = new List<DataField>();

                List<string> ProcessedFields = new List<string>();
                bool AllFieldsExisting = true;
                        
                foreach (DataField df in SelectedTable.Fields)
                {
                    if (!ProcessedFields.Contains(df.Name))
                    {
                        FieldGroupData CurrentGroup = FieldGroupDataHelper.GetFieldGroup(df.Name, CurrentParameters);

                        if (CurrentGroup == null)
                        {
                            Output.Fields.Add(new DataField() { Name = df.Name, DataType = df.DataType });
                        }
                        else
                        {
                            foreach (var f in CurrentGroup.CombinedFields)
                            {
                                if (!SelectedTable.Fields.Select(a => a.Name).Contains(f))
                                {
                                    AllFieldsExisting = false;
                                    if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
                                    break;
                                }
                            }
                            if (!AllFieldsExisting) break;
                            
                            string dataType = df.DataType;

                            int a1 = CurrentGroup.EmitMax ? 1 : 0;
                            int a2 = CurrentGroup.EmitMin ? 1 : 0;
                            int a3 = CurrentGroup.EmitAverage ? 1 : 0;
                            int a4 = CurrentGroup.EmitSum ? 1 : 0;
                            int a5 = CurrentGroup.EmitFirst ? 1 : 0;
                            int a6 = CurrentGroup.EmitCount ? 1 : 0;
                            int a7 = CurrentGroup.EmitLast ? 1 : 0;
                            int a8 = CurrentGroup.EmitRange ? 1 : 0;
                            int a9 = CurrentGroup.EmitList ? 1 : 0;

                            if (a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 != 1)
                            {
                                if (CurrentGroup.EmitMin) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Min", DataType = dataType });
                                if (CurrentGroup.EmitMax) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Max", DataType = dataType });
                                if (CurrentGroup.EmitRange) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Размах", DataType = "Double" });
                                if (CurrentGroup.EmitAverage) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Среднее", DataType = "Double" });
                                if (CurrentGroup.EmitSum) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Сумма", DataType = dataType });
                                if (CurrentGroup.EmitFirst) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Первое", DataType = dataType });
                                if (CurrentGroup.EmitLast) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Последнее", DataType = dataType });
                                if (CurrentGroup.EmitCount) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Число", DataType = "Int32" });
                                if (CurrentGroup.EmitList) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name + "_Список", DataType = "String" });
                            }
                            else
                            {
                                if (CurrentGroup.EmitList) Output.Fields.Add(new DataField() { Name = CurrentGroup.Name, DataType = "String" });
                                else Output.Fields.Add(new DataField() { Name = CurrentGroup.Name, DataType = dataType });
                            }
                            ProcessedFields = ProcessedFields.Concat(CurrentGroup.CombinedFields).ToList();
                        }
                    }
                }
                if (AllFieldsExisting)
                {
                    Output.DataItemType = SelectedTable.DataItemType;
                    Output.Name = SelectedTable.Name;
                    Output.TableName = SelectedTable.TableName;
                    Outputs = new List<DataItem>();
                    Outputs.Add(Output);
                }
            }
            else
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
