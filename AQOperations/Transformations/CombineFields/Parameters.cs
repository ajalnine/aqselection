﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_CombineFields
{
    public class Parameters
    {
        public ObservableCollection<FieldGroupData> Groups;
        public FieldGroupData UnfinishedGroup;
        public string TableName;
    }

    public class FieldGroupData : INotifyPropertyChanged
    {
        public ObservableCollection<string> CombinedFields { get; set; }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool emitfirst;
        public bool EmitFirst
        {
            get
            {
                return emitfirst;
            }
            set
            {
                emitfirst = value;
                NotifyPropertyChanged("EmitFirst");
            }
        }

        private bool emitlast;
        public bool EmitLast
        {
            get
            {
                return emitlast;
            }
            set
            {
                emitlast = value;
                NotifyPropertyChanged("EmitLast");
            }
        }

        private bool emitrange;
        public bool EmitRange
        {
            get
            {
                return emitrange;
            }
            set
            {
                emitrange = value;
                NotifyPropertyChanged("EmitRange");
            }
        }
        
        private bool emitsum;
        public bool EmitSum
        {
            get
            {
                return emitsum;
            }
            set
            {
                emitsum = value;
                NotifyPropertyChanged("EmitSum");
            }
        }

        private bool emitaverage;
        public bool EmitAverage
        {
            get
            {
                return emitaverage;
            }
            set
            {
                emitaverage = value;
                NotifyPropertyChanged("EmitAverage");
            }
        }

        private bool emitmin;
        public bool EmitMin
        {
            get
            {
                return emitmin;
            }
            set
            {
                emitmin = value;
                NotifyPropertyChanged("EmitMin");
            }
        }

        private bool emitmax;
        public bool EmitMax
        {
            get
            {
                return emitmax;
            }
            set
            {
                emitmax = value;
                NotifyPropertyChanged("EmitMax");
            }
        }

        private bool emitcount;
        public bool EmitCount
        {
            get
            {
                return emitcount;
            }
            set
            {
                emitcount = value;
                NotifyPropertyChanged("EmitCount");
            }
        }

        private bool emitlist;
        public bool EmitList
        {
            get
            {
                return emitlist;
            }
            set
            {
                emitlist = value;
                NotifyPropertyChanged("EmitList");
            }
        }

        private bool disableavg;
        public bool DisableAvg
        {
            get
            {
                return disableavg;
            }
            set
            {
                disableavg = value;
                NotifyPropertyChanged("DisableAvg");
            }
        }

        private bool disablesum;
        public bool DisableSum
        {
            get
            {
                return disablesum;
            }
            set
            {
                disablesum = value;
                NotifyPropertyChanged("DisableSum");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public static class FieldGroupDataHelper
    {
        public static FieldGroupData GetFieldGroup(string FieldName, Parameters CurrentParameters)
        {
            FieldGroupData fgd = null;
            foreach (var g in CurrentParameters.Groups)
            {
                if (g == null) continue;
                if (g.CombinedFields.Contains(FieldName))
                {
                    fgd = g;
                    break;
                }
            }
            return fgd;
        }
    }
}
