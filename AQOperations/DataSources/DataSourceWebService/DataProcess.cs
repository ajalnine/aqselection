﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceWebService
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var variablesTable = availableData.SingleOrDefault(a => a.Name == "Переменные");
            _inputs = new List<DataItem>();

            if (_currentParameters.VariableMapping.Count > 0 &&  variablesTable == null) DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            else
            {
                if (variablesTable == null)
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs {Error = InconsistenceState.LostInput});
                }
                else
                {
                    _inputs.Add(variablesTable);
                    var usedVars = _currentParameters.VariableMapping
                        .Select(b =>b.VariableName.StartsWith("@") ? b.VariableName.Substring(1, b.VariableName.Length-1) :  b.VariableName)
                        .ToList().Distinct();
                    var existingVars = variablesTable.Fields.Select(b => b.Name).ToList();
                    if (usedVars.Intersect(existingVars).Count() != usedVars.Count())
                    {
                        DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    }
                }
            }
            _outputs = new List<DataItem>();

            if (_currentParameters.OutputVariables.Any())
            {
                if (variablesTable == null)
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
                }
                var variables = new DataItem {Name = variablesTable.Name, TableName = variablesTable.TableName, DataItemType = variablesTable.DataItemType, Fields = new List<DataField>()};
                variables.Fields.AddRange(variablesTable.Fields);
                _outputs.Add(variables);
                foreach (var v in _currentParameters.OutputVariables)
                {
                    variables.Fields.Add(new DataField
                    {
                        Name = v.Alias.StartsWith("@") ? v.Alias.Substring(1, v.Alias.Length - 1) : v.Alias,
                        DataType = v.DataType
                    });
                }
            }

            if (_currentParameters.OutputTables.Any())
            {
                foreach (var t in _currentParameters.OutputTables)
                {
                    _outputs.Add(new DataItem {DataItemType = DataType.Selection, Name = t.Alias,
                        Fields = t.RowItems.Select(a=>new DataField {Name = a.Name, DataType = a.DataType}).ToList()});
                }
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
