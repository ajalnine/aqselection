﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.AQServiceReference;
using ApplicationCore;
using AQCalculationsLibrary;
using System.Xml.Linq;
using AQControlsLibrary.Annotations;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceWebService
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private XDocument _doc;
        private Parameters _currentParameters;
        private readonly AQService _as;
        private List<DataItem> _dataSources;
        private XNamespace _wsdlns, _xsdns;

        public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters();
            _as = Services.GetAQService();
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml != null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(Parameters));
                StringReader sr = new StringReader(parametersXml);
                _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
                try
                {
                    _doc = XDocument.Parse(_currentParameters.Wsdl);
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message);
                    return;
                }

            }
            ParametersToUI(availableData);
            SetOutputsVisibility();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters
            {
                UserName = string.Empty,
                Password = string.Empty,
                Wsdlurl = string.Empty,
                Wsdl = string.Empty,
                OutputVariables =  new List<VariableOutput>(),
                VariableMapping = new List<VariableMap>(),
                OutputTables = new List<TableOutput>(),
                Method = string.Empty,
                SOAPAction = string.Empty,
                Service = string.Empty,
                Operations = new List<string>()
            };
            ParametersToUI(availableData);
            SetOutputsVisibility();
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData;
            ParametersToUI();
            SetOutputsVisibility();
        }

        private void ParametersToUI()
        {
            ((VariableNamesHelper)Resources["Variables"]).VariableNames = _dataSources.Single(a => a.Name == "Переменные").Fields.Select(a => "@" + a.Name).ToList();
            UserName.Text = _currentParameters.UserName ?? string.Empty;
            WSDLURL.Text = _currentParameters.Wsdlurl ?? string.Empty;
            WSDL.Text = _currentParameters.Wsdl ?? string.Empty;
            Password.Password = _currentParameters.Password;
            InputFieldMapper.ItemsSource = _currentParameters.VariableMapping;
            OperationSelector.ItemsSource = _currentParameters.Operations;
            OperationSelector.SelectedItem = _currentParameters.Method;
            OperationSelector.UpdateLayout();
            ResponseVariables.ItemsSource = _currentParameters.OutputVariables;
            ResponseVariables.UpdateLayout();
            ResponseTables.ItemsSource = _currentParameters.OutputTables;
            ResponseTables.UpdateLayout();

        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            if (CheckOutput())ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private bool CheckOutput()
        {
            if (_currentParameters.OutputTables.Select(a => a.Alias).Intersect(_dataSources.Select(a=>a.Name)).Any())
            {
                ShowMessage("Таблица с указанным именем уже существует");
                return false;
            }
            if (_currentParameters.OutputVariables.Select(a => a.Alias.StartsWith("@") ? a.Alias.Substring(1,a.Alias.Length-1) : a.Alias )
                .Intersect(_dataSources.Single(a => a.TableName == "Переменные").Fields.Select(a => a.Name))
                .Any())
            {
                ShowMessage("Переменная с указанным именем уже существует");
                return false;
            }
            if (string.IsNullOrEmpty(_currentParameters.Service))
            {
                ShowMessage("Адрес сервиса не определен");
                return false;
            }
            return true;
        }

        private void UIToParameters()
        {
            _currentParameters.Password = Password?.Password;
            _currentParameters.UserName = UserName?.Text;
            _currentParameters.Wsdl = WSDL?.Text;
            _currentParameters.Method = OperationSelector.SelectedValue?.ToString();
            _currentParameters.Wsdlurl = WSDLURL.Text;
            _currentParameters.VariableMapping = InputFieldMapper.ItemsSource as List<VariableMap>;
            _currentParameters.Operations = OperationSelector.ItemsSource as List<string>;
            _currentParameters.OutputVariables = ResponseVariables.ItemsSource as List<VariableOutput>;
            _currentParameters.OutputTables = ResponseTables.ItemsSource as List<TableOutput>;
        }
        
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        private void GetWSDLButton_Click(object sender, RoutedEventArgs e)
        {
            ShowMessage(string.Empty);
            UIToParameters();
            _as.BeginGetStringFromURL(_currentParameters.Wsdlurl, _currentParameters.UserName,
                _currentParameters.Password, Callback, null);
        }

        private void Callback(IAsyncResult ar)
        {
            try
            {
                var result = _as.EndGetStringFromURL(ar);
                _currentParameters.Wsdl = result;
                Dispatcher.BeginInvoke(ParametersToUI);
            }
            catch (Exception e)
            {
                Dispatcher.BeginInvoke(() => { ShowMessage(e.Message); });
            }
        }

        private void ProcessWSDLButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ShowMessage(null);
            try
            {
                _doc = XDocument.Parse(_currentParameters.Wsdl);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                return;
            }
            if (_doc.Root == null) return;
            _wsdlns = _doc.Root.Name.Namespace;
            var soapns = _doc.Root.Elements(_wsdlns + "service").Descendants(_wsdlns + "port").Descendants().First().Name.Namespace;
            var operations = _doc.Root.Elements(_wsdlns + "binding").Descendants(_wsdlns + "operation").Select(a => a.Attribute("name")?.Value).ToList();
            _currentParameters.Service = _doc.Root.Elements(_wsdlns + "service").Descendants(_wsdlns + "port").Descendants(soapns + "address") .Select(a => a.Attribute("location")?.Value).SingleOrDefault();
            if (_currentParameters.Service == null)
            {
                ShowMessage("Методы не найдены");
                return;
            }
            OperationSelector.ItemsSource = null;
            OperationSelector.UpdateLayout();
            if (!operations.Any()) return;
            OperationSelector.ItemsSource = operations;
            OperationSelector.UpdateLayout();
            if (operations.Contains(_currentParameters.Method))
                OperationSelector.SelectedValue = _currentParameters.Method;
            else OperationSelector.SelectedIndex = 0;

            if (operations.Count==1)GetOperationIO(_currentParameters.Method);
        }

        private void OperationSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _currentParameters.Method = OperationSelector.SelectedValue?.ToString();
            GetOperationIO(_currentParameters.Method);
        }

        private void GetOperationIO(string operationName)
        {

            if (_doc == null || operationName == null) return;
            _wsdlns = _doc.Root?.Name.Namespace;
            var port = _doc?.Root?.Elements(_wsdlns + "portType").Descendants(_wsdlns + "operation").ToList();
            var io = port?.Where(a => a.Attribute("name")?.Value == operationName).ToList();
            var inputMessage = io?.Descendants(_wsdlns + "input").ToList();
            var inputMessageName = inputMessage.Select(a => a.Attribute("message")).SingleOrDefault()?.Value;

            var inputActionName = inputMessage.Attributes().Where(a=>a.Name.LocalName.ToLower()=="action").Select(a=>a.Value).SingleOrDefault();

            _currentParameters.SOAPAction = inputActionName;
            var outputMessageName = io?.Descendants(_wsdlns + "output").Select(a => a.Attribute("message")).SingleOrDefault()?.Value;


            if (inputMessageName!=null && inputMessageName.Contains(":")) inputMessageName = inputMessageName.Split(':')[1];
            if (outputMessageName!=null && outputMessageName.Contains(":")) outputMessageName = outputMessageName.Split(':')[1];

            var messages = _doc?.Root?.Elements(_wsdlns + "message").ToList();
            var inputElement = messages?.Where(a => a.Attribute("name")?.Value == inputMessageName).Select(a => a.Descendants(_wsdlns + "part").First().Attribute("element")?.Value).SingleOrDefault();
            var outputElement = messages?.Where(a => a.Attribute("name")?.Value == outputMessageName).Select(a => a.Descendants(_wsdlns + "part").First().Attribute("element")?.Value).SingleOrDefault();

            if (inputElement!=null && inputElement.Contains(":")) inputElement = inputElement.Split(':')[1];
            if (outputElement != null && outputElement.Contains(":"))
            {
                var sp = outputElement.Split(':');

                _currentParameters.MethodNamespace = _doc?.Root?.GetNamespaceOfPrefix(sp[0])?.NamespaceName;
                _currentParameters.MethodResponse = sp[1];
                outputElement = sp[1];
            }
            else
            {
                _currentParameters.MethodNamespace = null;
                _currentParameters.MethodResponse = outputElement;
            }

            var types = _doc?.Root?.Elements(_wsdlns + "types").ToList();
            _xsdns = types?.Descendants().First().Name.Namespace;

            var schema = types?.Descendants(_xsdns + "schema").First();

            _currentParameters.VariableMapping = new List<VariableMap>();
            _currentParameters.OutputTables = new List<TableOutput>();
            _currentParameters.OutputVariables = new List<VariableOutput>();


            var inputs = schema?.Descendants(_xsdns + "element").Single(a => a.Attribute("name")?.Value == inputElement);
            while (inputs?.Elements().Count() == 1 && inputs.Elements().First().Name != _xsdns + "element") inputs = inputs.Elements().First();
            var outputs = schema?.Descendants(_xsdns + "element").Single(a => a.Attribute("name")?.Value == outputElement);
            while (outputs.Descendants().Any() && outputs?.Elements().FirstOrDefault().Name != _xsdns + "element") outputs = outputs.Elements().First();

            var inputParameters = inputs.Elements(_xsdns + "element");
            var outputsParameters = outputs.Elements(_xsdns + "element");
            var variables = _dataSources.Single(a => a.Name == "Переменные").Fields.Select(a => "@" + a.Name).ToList();
            var defVar = variables.FirstOrDefault();
            var inputParametersList = inputParameters.Select(a => a.Attribute("name")?.Value.ToString()).Select(a=>new VariableMap {ParameterName = a, VariableName = defVar} ).ToList();
            InputFieldMapper.ItemsSource = inputParametersList;
            _currentParameters.VariableMapping = inputParametersList;
            var incompatibleResults = 0;

            foreach (var o in outputsParameters)
            {
                if (IsSimpleType(o))
                {
                    _currentParameters.OutputVariables.Add(
                        new VariableOutput
                        {
                            Name = o.Attribute("name")?.Value,
                            Alias = "@"+o.Attribute("name")?.Value,
                            DataType = GetCompatibleSimpleType(o)
                        });
                }
                else if (IsComplexType(o))
                {
                    var result = new List<TableOutput>();
                    GetTableOutputs(o, result, o.Attribute("name")?.Value, o.Name.LocalName);
                    _currentParameters.OutputTables.AddRange(result);
                }
                else incompatibleResults++;
            }
            ShowMessage(incompatibleResults > 0
                ? $"{incompatibleResults} результатов имеют несовместимый формат данных"
                : string.Empty);
            ResponseVariables.ItemsSource = _currentParameters.OutputVariables;
            ResponseVariables.UpdateLayout();
            ResponseTables.ItemsSource = _currentParameters.OutputTables;
            ResponseTables.UpdateLayout();

            SetOutputsVisibility();
        }

        private void SetOutputsVisibility()
        {

            Inputs.Visibility = _currentParameters.VariableMapping.Any()
                ? Visibility.Visible
                : Visibility.Collapsed;
            TableOutputs.Visibility = _currentParameters.OutputTables.Any()
                ? Visibility.Visible
                : Visibility.Collapsed;
            VariableOutputs.Visibility = _currentParameters.OutputVariables.Any()
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        private void GetTableOutputs(XElement o, List<TableOutput> result, string rowName, string elementName)
        {
            var tableRowFound = o.Descendants(_xsdns + "element").All(IsSimpleType) && o.Descendants().Any();
            if (tableRowFound)
            {
                result.Add(new TableOutput
                {
                    TableName = elementName,
                    Alias = elementName,
                    RowItems = GetChildSimpleElements(o),
                    RowName = rowName
                });
            }
            else
            {
                foreach (var nested in o.DescendantsAndSelf(_xsdns + "element"))
                {
                    var type = nested.Attribute("type")?.Value;
                    if (type == null) continue;
                    if (type.Contains(":")) type = type.Split(':')[1];
                    var types = _doc?.Root?.Elements(_wsdlns + "types").ToList();
                    var complexTypes = types?.Descendants().Where(a => a.Attribute("name")?.Value == type).ToList();
                    if (complexTypes == null) continue;
                    var complexType = complexTypes.Count == 1 
                        ? complexTypes.Single()
                        : types.Descendants(_xsdns+ "complexType").Single(a => a.Attribute("name")?.Value == type);
                    GetTableOutputs(complexType, result, nested.Attribute("name")?.Value, rowName);
                }
            }
        }

        private void ShowMessage(string p0)
        {
            ErrorMessage.Text = !string.IsNullOrEmpty(p0) ? p0 : string.Empty;
        }

        private bool IsComplexType(XElement el)
        {
            var t = el.Attribute("type")?.Value;
            if (t != null && t.Contains(":") && t.Split(':')[0] != el.GetPrefixOfNamespace(_xsdns)) return true;
            return el.Descendants(_xsdns + "element").Count() == 1
                    && el.Descendants(_xsdns + "complexType").Any()
                    && (el.Descendants(_xsdns + "all").Any()
                        || (el.Descendants(_xsdns + "sequence").Any()));
        }

        private List<FieldOutput> GetChildSimpleElements(XElement el)
        {
            return el.Descendants(_xsdns + "element").Select(field => new FieldOutput
            {
                Name = field.Attribute("name")?.Value, DataType = ConvertXSDToAQType(GetCompatibleSimpleType(field))
            }).ToList();
        }

        private string GetCompatibleSimpleType(XElement el)
        {
            var t = el.Attribute("type")?.Value;
            if (t != null && t.StartsWith(el.GetPrefixOfNamespace(_xsdns)+":")) return ConvertXSDToAQType(t.Split(':')[1]);
            if (t != null && t.Contains(":"))
            {
                var types = _doc?.Root?.Elements(_wsdlns + "types").ToList();
                var nested = types?.Descendants().Single(a => a.Attribute("name")?.Value == t.Split(':')[1]);
                if (nested != null)
                {
                    var els = nested.Descendants(_xsdns + "element").ToList();
                    if ( els.Count == 1) return ConvertXSDToAQType(GetCompatibleSimpleType(els.Single()));
                    var sti = nested.DescendantsAndSelf(_xsdns + "simpleType").SingleOrDefault();
                    var tti = sti?.Elements(_xsdns + "restriction").SingleOrDefault()?.Attribute("base")?.Value;
                    if (tti != null) return ConvertXSDToAQType(tti.Split(':')[1]);
                }
            }
            var st = el.Descendants(_xsdns + "simpleType").SingleOrDefault();
            var tt = st?.Elements(_xsdns + "restriction").SingleOrDefault()?.Attribute("base")?.Value;
            if (tt != null) return ConvertXSDToAQType(tt.Split(':')[1]);
            return null;
        }

        private string ConvertXSDToAQType(string s)
        {
            if (s == null) return null;
            switch (s.ToLower())
            {
               default:
                    return "String";

                case "boolean":
                    return "Boolean";

                case "byte":
                case "decimal":
                case "double":
                case "float":
                case "int":
                case "integer":
                case "long":
                case "negativeinteger":
                case "nonnegativeinteger":
                case "positiveinteger":
                case "nonpositiveinteger":
                case "short":
                case "unsignedbyte":
                case "unsignedint":
                case "unsignedlong":
                case "unsignedshort":
                    return "Double";

                case "date":
                case "datetime":
                    return "DateTime";

                case "time":
                case "duration":
                    return "TimeSpan";
            }
        }

        private bool IsSimpleType(XElement el)
        {
            var t = el.Attribute("type")?.Value;
            if (el.DescendantsAndSelf(_xsdns + "complexType").Any()) return false;
            if (t == null) return true;
            if (t.StartsWith(el.GetPrefixOfNamespace(_xsdns) + ":")) return true;
            var types = _doc?.Root?.Elements(_wsdlns + "types").ToList();
            var nestedElements = types?.Descendants().Where(a => a.Attribute("name")?.Value == t.Split(':')[1]).ToList();
            if (nestedElements == null) return true;
            return nestedElements.Count==1 && IsSimpleType(nestedElements.Single());
        }
    }

    public class VariableOutput : INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value; OnPropertyChanged(nameof(Name));
            }
        }

        private string _alias;
        public string Alias
        {
            get
            {
                return _alias;
            }
            set
            {
                _alias = value; OnPropertyChanged(nameof(Alias));
            }
        }

        private string _dataType;
        public string DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value; OnPropertyChanged(nameof(DataType));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class FieldOutput : INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value; OnPropertyChanged(nameof(Name));
            }
        }

        private string _dataType;
        public string DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value; OnPropertyChanged(nameof(DataType));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class TableOutput
    {
        public string TableName { get; set; }
        public string Alias { get; set; }
        public string RowName { get; set; }
        public List<FieldOutput> RowItems { get; set; }
    }

    public class VariableMap : INotifyPropertyChanged
    {
        private string _variableName;
        private string _parameterName;

        public string VariableName
        {
            get
            {
                return _variableName;

            }
            set
            {
                _variableName = value;
                OnPropertyChanged1(nameof(VariableName));
            }
        }

        public string ParameterName
        {
            get
            {
                return _parameterName;

            }
            set
            {
                _parameterName = value;
                OnPropertyChanged1(nameof(ParameterName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged1(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class VariableNamesHelper
    {
        public List<string> VariableNames { get; set; }
    }
}