﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_DataSourceWebService
{
    public class Parameters
    {
        public string Wsdl;
        public string Wsdlurl;
        public string UserName;
        public string Password;
        public string Method;
        public string Service;
        public string MethodResponse;
        public string MethodNamespace;
        public string SOAPAction;
        public List<string> Operations; 
        public List<VariableMap> VariableMapping;
        public List<VariableOutput> OutputVariables;
        public List<TableOutput> OutputTables;
    }
}