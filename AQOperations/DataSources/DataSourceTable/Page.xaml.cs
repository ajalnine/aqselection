﻿using System.Globalization;
using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Linq.Dynamic;
using System.Windows.Input;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;
using ApplicationCore;
using AQChartLibrary;

namespace SLCalc_DataSourceTable
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private Cell _editingCell;        
        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private int _lastEditingItem;
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
        private readonly CalculationService _cs;
        private string _lastdataType;

        #region Initialization

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            var tableNumber = availableData.Count(a => a.Name.ToLower().StartsWith("таблиц"));
            _dataSources = availableData;
            _currentParameters = new Parameters{TableName = string.Format("Таблица{0}", ((tableNumber>0) ? " "+(tableNumber+1) : string.Empty))};
            InitData();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _dataSources = availableData;
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            if ((_currentParameters.Data == null || _currentParameters.Data.Count==0) && !string.IsNullOrEmpty( _currentParameters.TSV)) SetDataByText(0, 0, 0, 0, 0, _currentParameters.TSV);
            _currentParameters.TSV = null;
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            if (_currentParameters != null && _currentParameters.TableName != null) TableName.Text = _currentParameters.TableName;
            _dataSources = availableData;
            UpdateTableEditor(true);
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            var ws = new XmlWriterSettings {NewLineHandling = NewLineHandling.Entitize};
            xs.Serialize(XmlWriter.Create(sb, ws), _currentParameters);
            return sb.ToString();
        }

        private void InitData()
        {
            _currentParameters.Data = new ObservableCollection<Row>();
            for (int i = 0; i < 10; i++)
            {
                var row = new Row {RowData = new ObservableCollection<Cell>()};
                for (var j = 0; j < 10; j++)
                {
                    var c = new Cell { Value = (i==0) ? "v" + j : String.Empty};
                    row.RowData.Add(c);
                }
                _currentParameters.Data.Add(row);
            }
        }

        #endregion

        #region Setup DataGrid

        private void UpdateTableEditor(bool needToUpdateColumnWidth)
        {
            if (_currentParameters == null) return;
            if (_currentParameters.Data == null || _currentParameters.Data.Count==0) SetDataByText(0, 0, 0, 0, 0, _currentParameters.TSV);
            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var selected = MainDataGrid.SelectedItem;

            var selectionString = "new (";

            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                if (i > 0) selectionString += ",";
                selectionString += string.Format("RowData[{0}] as c{0}", i);
            }
            selectionString += ")";
            if (!_currentParameters.Data[0].RowData.Any()) MainDataGrid.ItemsSource = null;
            else
            {
                var rowdata = _currentParameters.Data.AsQueryable();

                var data = rowdata.Select(selectionString);
                if (needToUpdateColumnWidth) UpdateColumnWidth();
                if (_currentParameters.ColumnWidth == null) _currentParameters.ColumnWidth = new List<double>();
                MainDataGrid.Columns.Clear();
                MainDataGrid.ItemsSource = data;
                UpdateDataTypes();
                MakeDataGridStructure();
            }
            MainDataGrid.RowHeight = 20;
            MainDataGrid.UpdateLayout();
            MainDataGrid.SelectedItem = selected;
            if (x >= 0) MainDataGrid.CurrentColumn = MainDataGrid.Columns[x];
            UpdateTableInfo();
        }

        private void MakeDataGridStructure()
        {
            
            for (var index = 0; index < _currentParameters.Data[0].RowData.Count; index++)
            {
                var width = (_currentParameters.ColumnWidth.Count > index) ? _currentParameters.ColumnWidth[index] : 50;
                var dgc = new DataGridTemplateColumn
                {
                        Header = GetColumnName(index),
                        HeaderStyle = new Style
                        {
                            TargetType = typeof(DataGridColumnHeader),
                            Setters =
                            {
                                new Setter{Property = OpacityProperty, Value = 0.4},
                                new Setter{Property = FontSizeProperty, Value = 8},
                                new Setter{Property = HorizontalContentAlignmentProperty, Value = HorizontalAlignment.Center},
                                new Setter{Property = VerticalContentAlignmentProperty, Value = VerticalAlignment.Center},
                                new Setter {Property = ToolTipService.ToolTipProperty, Value = SLDataReportingExtentions.GetFieldTypeName(_currentParameters?.DataTypes?[index]) }
                            }
                        },
                        CanUserSort = false,
                        Width = new DataGridLength(width)
                    };
                var bindingText = "{Binding Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, Path=c" + index + ".Value}";
                var bindingBG = "{Binding Path=c" + index + ".BG, Converter={StaticResource BGColorConverter" + index + "}}";
                var bindingFG = "{Binding Path=c" + index + ".Expression, Converter={StaticResource FGColorConverter" + index + "}}";
                var template =
                    String.Format(
                        @"<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'  xmlns:this='clr-namespace:AQAnalysisLibrary;assembly=AQAnalysisLibrary'>
                    <Grid>
                    <Grid.Resources>
                        <this:BGColorConverter x:Key='BGColorConverter{3}' />
                        <this:FGColorConverter x:Key='FGColorConverter{3}' />
                    </Grid.Resources>
                    <Rectangle Fill='{1}'>
                    </Rectangle><TextBlock Text='{0}' Foreground ='{2}' Margin='2' /></Grid></DataTemplate>",
                        bindingText, bindingBG, bindingFG, index);

                var editTemplate =
                 String.Format(
                     @"<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'  xmlns:this='clr-namespace:AQAnalysisLibrary;assembly=AQAnalysisLibrary'>
                    <Grid>
                    <Grid.Resources>
                        <this:BGColorConverter x:Key='BGColorConverter{2}' />
                    </Grid.Resources>
                    <Rectangle Opacity='0.5' Fill='{1}'>
                    </Rectangle><TextBox BorderThickness='0' Text='{0}' Margin='0' /></Grid></DataTemplate>",
                     bindingText, bindingBG, index);
                
                dgc.CellTemplate = (DataTemplate)XamlReader.Load(template);
                dgc.CellEditingTemplate = (DataTemplate)XamlReader.Load(editTemplate);
                MainDataGrid.Columns.Add(dgc);
            }
        }

        private void UpdateColumnWidth()
        {
            if (_currentParameters == null) return;
            _currentParameters.ColumnWidth = new List<double>();
            if (MainDataGrid == null || MainDataGrid.Columns == null || MainDataGrid.Columns.Count <= 0) return;
            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                _currentParameters.ColumnWidth.Add(MainDataGrid.Columns.Count > i
                    ? MainDataGrid.Columns[i].ActualWidth
                    : 50);
            }
        }

        private string GetColumnName(int number)
        {
            if (number > 65535) throw new Exception();
            if (number < 26) return Encoding.UTF8.GetString(new[] { (byte)(65 + number) }, 0, 1);
            var name = String.Empty;
            number++;
            do
            {
                var div = --number % 26;
                number /= 26;
                name = Encoding.UTF8.GetString(new[] { (byte)(65 + div) }, 0, 1) + name;
            } while (number > 0);
            return name;
        }

        private void UpdateTableInfo()
        {
            TableInfo.Text = $"n={_currentParameters.Data.Count - 1}, m={_currentParameters.Data[0].RowData.Count}";
        }
        #endregion

        #region Processing UI events

        private void MainDataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if (e.Row.GetIndex() == 0)
            {
                e.Row.FontWeight = FontWeights.Bold;
                e.Row.Header = String.Empty;
            }
            else
            {
                e.Row.Header = e.Row.GetIndex()+"  ";
            }
        }
        void MainDataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            MainDataGrid.BeginEdit();
        }

        private void MainDataGrid_OnPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs e)
        {
            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            if (x>=0 && y>=0)
            {
                var c = _currentParameters.Data[y].RowData[x] as Cell;
                if (c != null && !string.IsNullOrEmpty(c.Expression)) EditExpression(c);
            }
            var grid = e.EditingElement as Grid;
            if (grid == null ||grid.Children == null || grid.Children.Count <= 1) return;
            var textBox = grid.Children[1] as TextBox;
            if (textBox == null) return;
            textBox.SelectAll();
        }

        private void MainDataGrid_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var grid = e.EditingElement as Grid;
            if (grid == null || grid.Children == null || grid.Children.Count <= 1) return;
            var textBox = grid.Children[1] as TextBox;
            if (textBox == null) return;
            var columnIndex = MainDataGrid.Columns.IndexOf(e.Column);
            var text = textBox.Text;
            var binding = textBox.GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();
            /*if (_currentParameters == null || _currentParameters.DataTypes == null || !SLDataReportingExtentions.CheckDataType(_currentParameters?.DataTypes?[columnIndex], text))
            {
                UpdateDataTypes();
                UpdateTableEditor(false);
                MainDataGrid.ScrollIntoView(e.Row, e.Column);
            }*/
        }

        void TableEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.V && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                SetDataFromClipboard();
            }
            if (e.Key == Key.C && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                SetDataToClipboard(false);
            }
            if (e.Key == Key.X && ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                SetDataToClipboard(true);
            }
            if (e.PlatformKeyCode == 187)
            {
                var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
                var y = MainDataGrid.SelectedIndex;
                if (x >= 0 && y >= 1)
                {
                    var c = _currentParameters.Data[y].RowData[x] as Cell;
                    if (c != null && c.Value!=null && string.IsNullOrEmpty(c.Value.ToString())) StartEditExpression();
                }
            }
            _lastEditingItem = MainDataGrid.SelectedIndex;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            MainDataGrid.SelectedIndex = -1;
            MainDataGrid.UpdateLayout();
            UpdateParameters();
            if (!CheckIsOutputValid()) return;

            var raw = new StringBuilder();
            bool rowFirst = true, columnFirst;
            if (_currentParameters.ColoredColumns == null || _currentParameters.ColoredColumns.Count == 0)
            {
                foreach (var r in _currentParameters.Data)
                {
                    columnFirst = true;
                    if (!rowFirst) raw.Append("\n");
                    rowFirst = false;
                    foreach (var c in r.RowData)
                    {
                        if (!columnFirst) raw.Append("\t");
                        columnFirst = false;
                        raw.Append(c.Value);
                        if (c.BG!=null || !string.IsNullOrEmpty(c.Code))
                        {
                            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
                            return;
                        }
                    }
                }
                _currentParameters.TSV = raw.ToString();
                _currentParameters.Data = null;
            }
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        #endregion

        #region Table Validation
        private bool CheckIsOutputValid()
        {
            if (TableName.Text == string.Empty)
            {
                ShowTableNameError(true, "Не задано имя таблицы");
                return false;
            }

            if ((from v in _dataSources select v.Name).Contains(TableName.Text))
            {
                ShowTableNameError(true, "Имя уже используется");
                return false;
            }
            if (_currentParameters.Data.Count < 2)
            {
                ShowTableNameError(true, "Нет ни одной записи");
                return false;
            }
            if (_currentParameters.Data[0].RowData.Count == 0)
            {
                ShowTableNameError(true, "Таблица пуста");
                return false;
            }
            var empty = (from s in _currentParameters.Data[0].RowData where s.Value.ToString().Trim() == string.Empty select s).Any();
            if (empty)
            {
                ShowTableNameError(true, "Есть безымянные поля");
                return false;
            }
            var isDoubled = (from s in _currentParameters.Data[0].RowData group s by s.Value.ToString() into g where g.Count() > 1 select g).Any();
            if (isDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся поля");
                return false;
            }

            ShowTableNameError(false, string.Empty);
            return true;
        }

        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Text = message;
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
        }
        
        #endregion

        #region Setup column datatypes

        private void UpdateParameters()
        {
            _currentParameters.TableName = TableName.Text;
            UpdateDataTypes();
            UpdateColumnWidth();
            UpdateColumnNames();
            UpdateColoredColumns();
        }

        private void UpdateColoredColumns()
        {
            _currentParameters.ColoredColumns = new List<string>();
            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                var isColored = false;
                for (var j = 1; j < _currentParameters.Data.Count; j++)
                {
                    if (_currentParameters.Data[j].RowData[i].BG == null) continue;
                    isColored = true;
                    break;
                }
                if (!isColored) continue;
                _currentParameters.ColoredColumns.Add(_currentParameters.Data[0].RowData[i].Value.ToString());
            }
        }

        private void UpdateColumnNames()
        {
            _currentParameters.ColumnNames = new List<string>();
            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                _currentParameters.ColumnNames.Add(_currentParameters.Data[0].RowData[i].Value.ToString());
            }
        }

        private void UpdateDataTypes()
        {
            var currentXMax = _currentParameters.Data[0].RowData.Count;
            _currentParameters.DataTypes = new List<string>();
            for (var j = 0; j < currentXMax; j++)
            {
                _currentParameters.DataTypes.Add(GetDataTypeForColumn(j));
            }
        }

        private string GetDataTypeForColumn(int j)
        {
            var recordsNumber = _currentParameters.Data.Count;
            bool isDouble = true;
            bool isBoolean = true;
            bool isDateTime = true;
            bool isInt32 = true;
            bool isTimeSpan = true;
            bool hasValues = false;
            for (var i = 0; i < recordsNumber; i++)
            {
                var value = _currentParameters.Data[i].RowData[j].Value.ToString();
                var expression = _currentParameters.Data[i].RowData[j].Expression?.ToString();
                if (!string.IsNullOrEmpty(expression))
                {
                    var dataType = _currentParameters.Data[i].RowData[j].DataType?.ToString().ToLower();
                    isDouble = dataType == "double";
                    isBoolean = dataType == "boolean";
                    isDateTime = dataType == "datetime";
                    isInt32 = dataType == "int32";
                    isTimeSpan = dataType == "timespan";
                    hasValues = true;
                    continue;
                }
                if (i==0 || string.IsNullOrWhiteSpace(value)) continue;
                hasValues = true;
                if (isDouble)
                {
                    Double doubleResult;
                    isDouble = Double.TryParse(value, out doubleResult) || Double.TryParse(value.Replace('.', ','), out doubleResult) || Double.TryParse(value.Replace(',', '.'), out doubleResult);
                }
                if (isTimeSpan)
                {
                    TimeSpan timeSpanResult;
                    isTimeSpan = TimeSpan.TryParse(value, out timeSpanResult);
                }
                if (isDateTime)
                {
                    DateTime dateTimeResult;
                    isDateTime = DateTime.TryParseExact(value, "MMMM yyyy", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime |= DateTime.TryParseExact(value, "dd,MM,yy", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime |= DateTime.TryParseExact(value, "d,M,y", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime |= DateTime.TryParseExact(value, "dd,MM,yyyy", null, DateTimeStyles.AllowWhiteSpaces, out dateTimeResult);
                    isDateTime = !isDateTime && DateTime.TryParse(value, out dateTimeResult);
                }
                if (isInt32)
                {
                    Int32 int32Result;
                    isInt32 = Int32.TryParse(value, out int32Result);
                }
                if (isBoolean)
                {
                    Boolean booleanResult;
                    isBoolean = Boolean.TryParse(value, out booleanResult);
                }
            }
            if (!hasValues) return "String";
            if (isDouble) return "Double";
            if (isBoolean) return "Boolean";
            if (isTimeSpan) return "TimeSpan";
            if (isDateTime) return "DateTime";
            if (isInt32) return "Int32";
            return "String";
        }
        
        #endregion

        #region Edit Table
        private void NewEmptyData_Click(object sender, RoutedEventArgs e)
        {
            InitData();
        }

        private void MainDataGrid_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter || MainDataGrid.SelectedIndex != _currentParameters.Data.Count - 1 || _lastEditingItem != _currentParameters.Data.Count - 1) return;
            AddNewRow();
            UpdateTableEditor(true);
            MainDataGrid.SelectedIndex = _currentParameters.Data.Count - 1;
            MainDataGrid.UpdateLayout();
            MainDataGrid.ScrollIntoView(MainDataGrid.SelectedItem, MainDataGrid.CurrentColumn);
        }

        private void DeleteEmptyRows_OnClick(object sender, RoutedEventArgs e)
        {
            var listToDelete = new List<int>();
            for (var j = 1; j < _currentParameters.Data.Count; j++)
            {
                var isEmpty = true;
                for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
                {
                    var value = _currentParameters.Data[j].RowData[i].Value;
                    if (value == null) continue;
                    if (value is string && string.IsNullOrEmpty(value.ToString())) continue;
                    isEmpty = false;
                    break;
                }
                if (isEmpty)
                {
                    listToDelete.Add(j);
                }
            }
            MainDataGrid.Columns.Clear();
            var deleted = 0;
            foreach (var j in listToDelete)
            {
                _currentParameters.Data.RemoveAt(j - deleted++);
            }
            UpdateTableEditor(true);

            MainDataGrid.UpdateLayout();
        }

        private void DeleteEmptyColumns_OnClick(object sender, RoutedEventArgs e)
        {
            var listToDelete = new List<int>();
            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
            {
                var isEmpty = true;
                for (var j = 1; j < _currentParameters.Data.Count; j++)
                {
                    var value = _currentParameters.Data[j].RowData[i].Value;
                    if (value == null) continue;
                    if (value is string && string.IsNullOrEmpty(value.ToString())) continue;
                    isEmpty = false;
                    break;
                }
                if (isEmpty)
                {
                    listToDelete.Add(i);
                }
            }
            MainDataGrid.Columns.Clear();
            var deleted = 0;
            foreach (var i in listToDelete)
            {
                DeleteColumnAtIndex(i - deleted);
                deleted++;
            }
            UpdateTableEditor(true);

            MainDataGrid.UpdateLayout();
        }

        private void DeleteColumnAtIndex(int x)
        {
            if (x < 0) return;
            if (_currentParameters.ColumnWidth.Count - 1 > x) _currentParameters.ColumnWidth.RemoveAt(x);

            foreach (var row in _currentParameters.Data)
            {
                row.RowData.RemoveAt(x);
            }
        }
        
        private void NewColumn_Click(object sender, RoutedEventArgs e)
        {
            AddNewColumn();
            UpdateTableEditor(true);
            MainDataGrid.CurrentColumn = MainDataGrid.Columns[MainDataGrid.Columns.Count - 1];
            MainDataGrid.SelectedIndex = 0;
            MainDataGrid.UpdateLayout();
            MainDataGrid.ScrollIntoView(MainDataGrid.SelectedItem, MainDataGrid.CurrentColumn);
        }

        private void AddNewColumn()
        {
            bool isFirst = true;
            foreach (var row in _currentParameters.Data)
            {
                var c = new Cell { Value = (isFirst) ? "v" + _currentParameters.Data[0].RowData.Count : String.Empty };
                row.RowData.Add(c);
                isFirst = false;
            }
        }

        private void NewRow_Click(object sender, RoutedEventArgs e)
        {
            MainDataGrid.CurrentColumn = MainDataGrid?.Columns[0];
            AddNewRow();
            UpdateTableEditor(true);
            MainDataGrid.SelectedIndex = _currentParameters.Data.Count - 1;
            MainDataGrid.UpdateLayout();
            MainDataGrid.ScrollIntoView(MainDataGrid.SelectedItem, MainDataGrid.CurrentColumn);
        }

        private void AddNewRow()
        {
            var r = new Row {RowData = new ObservableCollection<Cell>()};
            if (_currentParameters.Data==null || _currentParameters.Data.Count==0)
            {
                for (var index = 0; index < _currentParameters.DataTypes.Count; index++)
                {
                    r.RowData.Add(new Cell { Value = String.Empty });
                }
            }
            else
            {
                for (var index = 0; index < _currentParameters.Data[0].RowData.Count; index++)
                {
                    r.RowData.Add(new Cell { Value = String.Empty });
                }
            }
            _currentParameters.Data.Add(r);
            UpdateTableInfo();
        }

        private void DeleteRow_Click(object sender, RoutedEventArgs e)
        {
            if (MainDataGrid.SelectedItems.Count > 1)
            {
                var all = MainDataGrid.ItemsSource.Cast<object>().ToList();

                var toDelete = (from object s in MainDataGrid.SelectedItems where all.IndexOf(s) != 0 select _currentParameters.Data[all.IndexOf(s)]).ToList();

                foreach (var t in toDelete)
                {
                    _currentParameters.Data.Remove(t);
                }
            }
            else if (MainDataGrid.SelectedIndex > 0) _currentParameters.Data.RemoveAt(MainDataGrid.SelectedIndex);

            UpdateTableEditor(true);
            MainDataGrid.SelectedItem = _currentParameters.Data.First();
            MainDataGrid.UpdateLayout();
            if (MainDataGrid.SelectedItem != null) MainDataGrid.ScrollIntoView(MainDataGrid.SelectedItem, MainDataGrid.CurrentColumn);
        }

        private void DeleteColumn_Click(object sender, RoutedEventArgs e)
        {
            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            if (x < 0) return;
            var selected = MainDataGrid.SelectedItem;

            UpdateColumnWidth();
            _currentParameters.ColumnWidth.RemoveAt(x);
            MainDataGrid.Columns.Clear();

            foreach (var row in _currentParameters.Data)
            {
                row.RowData.RemoveAt(x);
            }
            UpdateTableEditor(false);

            if (_currentParameters.Data[0].RowData.Count <= x) x--;
            if (x > 0) MainDataGrid.CurrentColumn = MainDataGrid.Columns[x];
            MainDataGrid.UpdateLayout();
            if (selected != null) MainDataGrid.ScrollIntoView(selected, MainDataGrid.CurrentColumn);
        }
        #endregion

        #region Clipboard operations

        private void PasteData_Click(object sender, RoutedEventArgs e)
        {
            SetDataFromClipboard();
        }

        private void CopyData_Click(object sender, RoutedEventArgs e)
        {
            SetDataToClipboard(false);
        }

        private void CutData_Click(object sender, RoutedEventArgs e)
        {
            SetDataToClipboard(true);
        }

        private void SetDataFromClipboard()
        {
            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            int currentXMax;
            int currentYMax;
            var cleanInsert = 0;
            if (x <= 0 && y <= 0)
            {
                _currentParameters.Data = new ObservableCollection<Row>();
                currentXMax = 0;
                currentYMax = 0;
                var row = new Row { RowData = new ObservableCollection<Cell>() };
                _currentParameters.Data.Add(row);
                x = 0;
                y = 0;
                cleanInsert++;
            }
            else
            {
                currentXMax = _currentParameters.Data[0].RowData.Count;
                currentYMax = _currentParameters.Data.Count;
            }

            if (!Clipboard.ContainsText()) return;

            var textData = Clipboard.GetText();
            SetDataByText(x, y, currentXMax, currentYMax, cleanInsert, textData);
            UpdateTableEditor(true);
        }

        private void SetDataByText(int x, int y, int currentXMax, int currentYMax, int cleanInsert, string textData)
        {
            var insertedRows = textData.Contains("\r\n") 
                ? textData.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList()
                : textData.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var height = insertedRows.Count;
            var width = insertedRows[0].Split(new[] { "\t" }, StringSplitOptions.None).Count();
            if (x + width > currentXMax)
            {
                for (var i = 0; i < width + x - currentXMax; i++) AddNewColumn();
            }
            if (y + height > currentYMax)
            {
                for (var i = 0; i < height + y - currentYMax - cleanInsert; i++) AddNewRow();
            }

            for (var i = 0; i < height; i++)
            {
                var row = insertedRows[i];
                var values = row.Split(new[] { "\t" }, StringSplitOptions.None);
                var w = values.Count();
                for (var j = 0; j < w; j++)
                {
                    if (j + x >= _currentParameters.Data[0].RowData.Count) continue;
                    _currentParameters.Data[i + y].RowData[j + x].Value = values[j];
                }
            }
        }

        private void SetDataToClipboard(bool clear)
        {
            var textData = string.Empty;
            var isFirst = true;
            foreach (var s in MainDataGrid.SelectedItems)
            {
                if (!isFirst) textData += "\r\n";
                for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
                {
                    if (i > 0) textData += "\t";
                    var c = (s.GetType().GetProperty("c" + i).GetValue(s, null) as Cell);
                    if (c == null) continue;
                    textData += c.Value.ToString();
                    if (clear) c.Value = string.Empty;
                }
                isFirst = false;
            }
            Clipboard.SetText(textData);
            UpdateTableEditor(true);
        }

        
        #endregion
        
        #region Color Operations
        private void MainColorSelector_OnColorChanged(object sender, ColorChangedEventArgs e)
        {
            if (e.TableMarkMode == TableMarkMode.Clear)
            {
                var mr = MessageBox.Show("Удалить цветовую разметку таблицы ?", "Подтверждение", MessageBoxButton.OKCancel);
                if (mr == MessageBoxResult.Cancel) return;
                foreach (var yt in _currentParameters.Data) foreach (var xt in yt.RowData) xt.BG = null;
                return;
            }

            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            if (x < 0 || y < 0 || _currentParameters == null || _currentParameters.Data == null || !_currentParameters.Data.Any()) return;

            if (MainDataGrid.SelectedItems != null && MainDataGrid.SelectedItems.Count > 1)
            {
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                    case TableMarkMode.Column:
                        if (y == 0) foreach (var t in _currentParameters.Data) t.RowData[x].BG = e.ColorString;
                        else
                        {
                            foreach (var s in MainDataGrid.SelectedItems)
                            {
                                var c = (s.GetType().GetProperty("c" + x).GetValue(s, null) as Cell);
                                if (c == null) continue;
                                c.BG = e.ColorString;
                            }
                        }
                        break;
                    case TableMarkMode.Row:
                        if (y < 1) return;
                        foreach (var s in MainDataGrid.SelectedItems)
                        {
                            for (var i = 0; i < _currentParameters.Data[0].RowData.Count; i++)
                            {
                                var c = (s.GetType().GetProperty("c" + i).GetValue(s, null) as Cell);
                                if (c == null) continue;
                                c.BG = e.ColorString;
                            }
                        }
                        break;
                }
            }
            else
            {
                switch (e.TableMarkMode)
                {
                    case TableMarkMode.Cell:
                        if (y > 0) _currentParameters.Data[y].RowData[x].BG = e.ColorString;
                        else foreach (var t in _currentParameters.Data) t.RowData[x].BG = e.ColorString;
                        break;
                    case TableMarkMode.Row:
                        if (y > 0) foreach (var t in _currentParameters.Data[y].RowData) t.BG = e.ColorString;
                        break;
                    case TableMarkMode.Column:
                        foreach (var t in _currentParameters.Data) t.RowData[x].BG = e.ColorString;
                        break;
                }
            }
        }
        #endregion

        #region Expression Operations

        private void InsertExpression_Click(object sender, RoutedEventArgs e)
        {
            StartEditExpression();
        }

        private void StartEditExpression()
        {
            var x = MainDataGrid.Columns.IndexOf(MainDataGrid.CurrentColumn);
            var y = MainDataGrid.SelectedIndex;
            if (x >= 0 && y >= 0)
            {
                var c = _currentParameters.Data[y].RowData[x] as Cell;
                if (c == null) return;
                _editingCell = c;
                ResetEditor();
                ExpressionEditorWindow.Visibility = Visibility.Visible;
            }
            MainDataGrid.IsEnabled = false;
        }

        private void EditExpression(Cell sourceCell)
        {
            _editingCell = sourceCell;
            ExpressionEditorWindow.Visibility = Visibility.Visible;
            MainExpressionBuilder.StartHelp();
            MainExpressionBuilder.SetCodeEditor(sourceCell.Code);
            MainExpressionBuilder.SetExpressionEditor(sourceCell.Expression);
            var _tableDataItem = GetDataTableFromParameters();
            MainExpressionBuilder.SetDataItems(_tableDataItem, _dataSources, null);
            if (_editingCell.DataType!=null)ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(_editingCell.DataType.ToLower());
            MainDataGrid.IsEnabled = false;
        }

        private void ExpressionEditorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ResetEditor();
            _editingCell = null;
            ExpressionEditorWindow.Visibility = Visibility.Collapsed;
        }

        private void ResetEditor()
        {
            MainExpressionBuilder.StartHelp();
            MainExpressionBuilder.SetCodeEditor(String.Empty);
            MainExpressionBuilder.SetExpressionEditor(String.Empty);
            var _tableDataItem = GetDataTableFromParameters();
            MainExpressionBuilder.SetDataItems(_tableDataItem, _dataSources, null);
            MainDataGrid.IsEnabled = true;
        }

        private DataItem GetDataTableFromParameters()
        {
            UpdateColumnNames();
            UpdateDataTypes();
            UpdateColoredColumns();
            var tableDataItem = new DataItem();
            tableDataItem.DataItemType = DataType.Selection;
            tableDataItem.Name = _currentParameters.TableName;
            tableDataItem.Fields = new List<DataField>();

            for (var i = 0; i < _currentParameters.ColumnNames.Count; i++)
            {
                var df = new DataField
                {
                    DataType = _currentParameters.DataTypes[i],
                    Name = _currentParameters.ColumnNames[i]
                };
                tableDataItem.Fields.Add(df);
                if (!_currentParameters.ColoredColumns.Contains(_currentParameters.ColumnNames[i])) continue;
                var dc = new DataField
                {
                    DataType = "String",
                    Name = "#" + _currentParameters.ColumnNames[i] + "_Цвет"
                };
                tableDataItem.Fields.Add(dc);
            }
            return tableDataItem;
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _cs.BeginCheckExpression(e.CSharpText, _dataSources.Union(new[] { GetDataTableFromParameters() }).ToList(), _currentParameters.TableName, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null")
                {
                    EnableExit(true);
                    var typeDescription = SLDataReportingExtentions.GetFieldTypeDescription(sr.Message.ToLower());
                    _lastdataType = sr.Message;
                    ExpressionFieldType.SelectedValue = typeDescription;
                }
                else
                {
                    if (!sr.Success) MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        private void EnableExit(bool enable)
        {
            ExpressionOKButton.IsEnabled = enable;
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            EnableExit(false);
        }

        private void ExpressionOKButton_Click(object sender, RoutedEventArgs e)
        {
            var y = MainDataGrid.SelectedIndex;
            _editingCell.Code = MainExpressionBuilder.GetCode();
            _editingCell.Expression = MainExpressionBuilder.GetExpression();
            _editingCell.DataType = _lastdataType;
            if (y!=0) _editingCell.Value = "=" + _editingCell.Expression;
            ExpressionEditorWindow.Visibility = Visibility.Collapsed;
            MainDataGrid.IsEnabled = true;
        }

        private void ExpressionCancelButton_Click(object sender, RoutedEventArgs e)
        {
            ResetEditor();
            _editingCell = null;
            ExpressionEditorWindow.Visibility = Visibility.Collapsed;
            MainDataGrid.IsEnabled = true;
        }

        private void ExpressionRemoveButton_Click(object sender, RoutedEventArgs e)
        {
            ResetEditor();
            _editingCell.Code = null;
            _editingCell.Expression = null;
            _editingCell.DataType = null;
            _editingCell.Value = string.Empty;
            ExpressionEditorWindow.Visibility = Visibility.Collapsed;
            MainDataGrid.IsEnabled = true;
        }

        private void ExpressionFieldType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _lastdataType = SLDataReportingExtentions.GetFieldType(ExpressionFieldType.SelectedValue.ToString());
        }
        #endregion
    }
}