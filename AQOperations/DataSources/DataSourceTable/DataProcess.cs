﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using System.Linq;

namespace SLCalc_DataSourceTable
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;
        
        private List<DataItem> _outputs, _inputs; 
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));

            if (parametersXml.Length > 100000)
            {
                var endofBasicPart = parametersXml.IndexOf("</RowData>", 0, System.StringComparison.Ordinal);
                if (endofBasicPart < 0)
                {
                    endofBasicPart = parametersXml.IndexOf("<TSV>", 0, System.StringComparison.Ordinal);
                    var basicPart = parametersXml.Substring(0, endofBasicPart) + "<TSV></TSV></Parameters>";
                    _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(new StringReader(basicPart)));
                }
                else
                {
                    var basicPart = parametersXml.Substring(0, endofBasicPart) + "</RowData></Row></Data></Parameters>";
                    var sr = new StringReader(basicPart);
                    _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
                    if (string.IsNullOrEmpty(_currentParameters.TableName)) // Устаревшие расчеты
                    {
                        _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(new StringReader(parametersXml)));
                    }
                }
            }
            else
            {
                _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(new StringReader(parametersXml)));
            }
            
            _outputs = new List<DataItem>();
            var output = new DataItem();
            _outputs.Add(output); 
            output.DataItemType = DataType.Selection;
            output.Name = _currentParameters.TableName;
            output.Fields = new List<DataField>();

            var resultingTokens = new List<Token>();
            
            foreach (var row in _currentParameters.Data)
            {
                foreach (var cellExpression in row.RowData.Where(c => !string.IsNullOrEmpty(c.Expression)).Select(e => e.Expression))
                {
                    resultingTokens.InsertRange(0, Tokens.ParseTokens(cellExpression, availableData.ToList(), output));
                }
            }

            if (Tokens.CheckTokens(resultingTokens))
            {
                _inputs = UsedDataItemsHelper.GetUsedTables(resultingTokens, null).ToList();
            }

            if ((_currentParameters.ColumnNames == null || _currentParameters.ColumnNames.Count == 0) && _currentParameters.Data.Count>0)
            {
                _currentParameters.ColumnNames = new List<string>();
                foreach (var t in _currentParameters.Data[0].RowData)
                {
                    _currentParameters.ColumnNames.Add(t.Value.ToString());
                }
            }

            for (var i = 0; i < _currentParameters.ColumnNames.Count; i++)
            {
                var df = new DataField
                {
                    DataType = _currentParameters.DataTypes[i],
                    Name = _currentParameters.ColumnNames[i]
                };
                output.Fields.Add(df);
                if (!_currentParameters.ColoredColumns.Contains(_currentParameters.ColumnNames[i]))continue;
                var dc = new DataField
                {
                    DataType = "String",
                    Name = "#" + _currentParameters.ColumnNames[i] + "_Цвет"
                };
                output.Fields.Add(dc);
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
