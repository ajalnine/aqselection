﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using AQControlsLibrary.Annotations;

namespace SLCalc_DataSourceTable
{
    public class Parameters
    {
        public List<string> DataTypes;
        public string TableName;
        public List<string> ColumnNames;
        public List<string> ColoredColumns;
        public List<Double> ColumnWidth;
        public ObservableCollection<Row> Data;
        public string TSV;
    }

    public class Row
    {
        public ObservableCollection<Cell> RowData;
    }

    public class Cell : INotifyPropertyChanged
    {
        private object _v;
        public object Value
        {
            get
            {
                return _v;
            }
            set
            {
                _v = value;
                OnPropertyChanged("Value");
            }
        }

        private string _expression;
        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
                OnPropertyChanged("Expression");
            }
        }

        private string _dataType;
        public string DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                _dataType = value;
                OnPropertyChanged("DataType");
            }
        }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                OnPropertyChanged("Code");
            }
        }
        
        private object _bg;
        public object BG
        {
            get
            {
                return _bg;
            }
            set
            {
                _bg = value;
                OnPropertyChanged("BG");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
