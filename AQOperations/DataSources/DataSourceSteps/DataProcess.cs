﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceSteps
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            
            var dfs = new DataFlowSimulator();
            dfs.DataFlowSimulationFinished += DfsOnDataFlowSimulationFinished;
            dfs.DataFlowSimulationError += DfsOnDataFlowSimulationError;
            dfs.SimulateDataFlow(_currentParameters.CalculationDescription, availableData);
        }

        private void DfsOnDataFlowSimulationError(object o, DataFlowSimulationErrorEventArgs e)
        {
            DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = e.Error });
        }

        private void DfsOnDataFlowSimulationFinished(object o, DataFlowSimulationEventArgs e)
        {
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs{ Inputs = e.UsedInputs, Outputs = e.Results, Deleted = e.Deleted });
        }
    }
}
