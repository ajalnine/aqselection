﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace SLCalc_DataSourceSteps
{
    public partial class Page
    {
        #region Data request

        public event DataRequestDelegate DataRequest;

        private Task _requestTask;
        private bool _showAnalysis = false;
        private void OperationCalculationView_OnRequestForData(object o, GenericEventArgs e)
        {
            var stepNames = OperationCalculationView.GetStepNames().Take((int)e.CustomData + 1);
            OperationCalculationView.ResetProgressInScheme();
            _requestTask = new Task(new[] {"Внешние данные"}.Union(stepNames).ToArray(), "Исполнение расчета", TaskPanel)
            {
                ProcessingItem = o as OperationView
            };
            _showAnalysis = false;
            DataRequest?.Invoke(this, new EventArgs());
        }

        public void DataRequestReadyCallback(string fileName)
        {
            _requestTask.CustomData = fileName;
            _requestTask.AdvanceProgress();
            var c = OperationCalculationView.GetCalculation();
            if (c?.StepData == null || c.StepData.Count == 0)
            {
                if (!_showAnalysis)Dispatcher.BeginInvoke(() => (_requestTask.ProcessingItem as OperationView)?.DataReceived(_requestTask.CustomData.ToString()));
                else ShowAnalysis();
            }
            else
            {
                ExecuteSteps(0, _requestTask, (t, s) =>
                {
                    if (!_showAnalysis)Dispatcher.BeginInvoke(() =>(t.ProcessingItem as OperationView)?.DataReceived(t.CustomData.ToString()));
                    else ShowAnalysis();
                });
            }
        }

        private void ShowAnalysis()
        {
            var taskGuid = _requestTask.CustomData.ToString();
            Dispatcher.BeginInvoke(
                () => _cs.BeginGetTablesSLCompatible(taskGuid, i =>
                {
                    var tables = _cs.EndGetTablesSLCompatible(i);
                    if (tables == null) return;
                    Dispatcher.BeginInvoke(() =>
                    {
                        var ca = new Dictionary<string, object>
                        {
                            {"SourceData", tables},
                            {"Calculation", OperationCalculationView.GetCalculation().StepData},
                            {"New", true},
                        };
                        OperationCalculationView.FinalizeProgressInScheme();
                        Signal.Send(null, Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                    });
                }, _requestTask));
        }

        private void ExecuteSteps(int number, Task task, Page.FinalizeJobDelegate finalizejob)
        {
            OperationCalculationView.ShowProgressInScheme(number, TaskState.Processing);
            _cs.BeginExecuteStep(task.CustomData.ToString(), OperationCalculationView.GetCalculation().StepData[number],
                iar =>
                {
                    try
                    {
                        var sr = _cs.EndExecuteStep(iar);
                        if (sr.Success)
                        {
                            task.AdvanceProgress();
                            var c = OperationCalculationView.GetCalculation();
                            OperationCalculationView.ShowProgressInScheme(number, TaskState.Ready);
                            if (number < c.StepData.Count - 1) ExecuteSteps(number + 1, task, finalizejob);
                            else finalizejob(task, sr);
                        }
                        else
                        {
                            task.SetState(task.CurrentStep, TaskState.Error);
                            task.SetMessage(sr.Message);
                            OperationCalculationView.ShowProgressInScheme(number, TaskState.Error);
                            _cs.BeginDropData(sr.TaskDataGuid, null, null);
                        }
                    }
                    catch
                    {
                        task.SetState(task.CurrentStep, TaskState.Error);
                        OperationCalculationView.ShowProgressInScheme(number, TaskState.Error);
                    }
                }, task);
        }

        #endregion

        #region Request for preview

        private void OperationCalculationView_OnRequestForPreview(object o, GenericEventArgs e)
        {
            var stepNames = OperationCalculationView.GetStepNames().Take((int)e.CustomData + 1);
            OperationCalculationView.ResetProgressInScheme();
            _requestTask = new Task(new[] { "Внешние данные" }.Union(stepNames).ToArray(), "Исполнение расчета", TaskPanel)
            {
                ProcessingItem = o as OperationView
            };
            _showAnalysis = true;
            DataRequest?.Invoke(this, new EventArgs());
        }

        #endregion
    }
}
