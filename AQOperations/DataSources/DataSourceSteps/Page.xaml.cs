﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;
using ApplicationCore;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceSteps
{
    
    public partial class Page : UserControl, IDataFlowUI
    {
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private delegate void FinalizeJobDelegate(Task task, StepResult sr);
        private Parameters _currentParameters;
        private List<DataItem> _availableData; 
        private readonly CalculationService _cs;
        
        public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters();
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            if (parametersXml != null)
            {
                var xs = new XmlSerializer(typeof(Parameters));
                var sr = new StringReader(parametersXml);
                _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            }
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _currentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            OperationCalculationView.InsertSteps(_currentParameters.CalculationDescription?.StepData, availableData, ()=>
            {
                CheckCalculationConsistency();
                ShowCalculationResults();
            });
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = true, ParametersXML = GetParametersXML() });
        }
        
        private void UpdateParameters()
        {
            _currentParameters.CalculationDescription = OperationCalculationView.GetCalculation();
        }

        private void OperationCalculationView_OnDataFlowChanged(object o, GenericEventArgs e)
        {
            Refresh();
        }

        private void ShowMessage(string p0)
        {
            ErrorMessage.Text = !string.IsNullOrEmpty(p0) ? p0 : string.Empty;
        }
        private void Refresh()
        {
            CheckCalculationConsistency();
            ShowCalculationResults();
        }

        private void CheckCalculationConsistency()
        {
            var message = OperationCalculationView.CheckOperations();
            ShowMessage(message);
            var noErrors = string.IsNullOrEmpty(message);
            OKButton.IsEnabled = noErrors;
        }

        private void ShowCalculationResults()
        {
            var results = OperationCalculationView.GetResults() ?? new List<DataItem>();
            AvailableSourcesView.ItemsSource = from r in results where r != null select r;
            InputSourcesView.ItemsSource = from r in _availableData where r != null select r;
        }

        
        public void StateChanged(OperationViewMode ovm) { }

        
    }
}
