﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQCatalogueLibrary;

namespace SLCalc_DataSourceCalculation
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;
        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataSources = AvailableData;
            CatalogueOperations.GetItem(CatalogueOperations.CreateDomainContext(), CurrentParameters.CurrentCalculationID, (e, state) => 
            {
                try
                {
                    CalculationDescription CD = CatalogueOperations.ItemToCalculationDescription(e.Item);
                    Outputs = new List<DataItem>();

                    foreach (var q in CD.Results.Where(r => r.DataItemType == DataType.Selection))
                    {
                        DataItem Output = new DataItem();
                        Output.DataItemType = q.DataItemType;
                        Output.Name = AliasHelper.GetAlias(q.Name, CurrentParameters);
                        Output.TableName = q.TableName;
                        Output.Fields = new List<DataField>();

                        foreach (var f in q.Fields)
                        {
                            Output.Fields.Add(new DataField() { Name = f.Name, DataType = f.DataType });
                        }
                        Outputs.Add(Output);
                    }
                    var Vars = CD.Results.Where(r => r.DataItemType == DataType.Variables).SingleOrDefault();

                    if (Vars != null)
                    {
                        DataItem Output = new DataItem();
                        Output.DataItemType = DataType.Variables;
                        Output.Name = "Переменные";
                        Output.TableName = "Переменные";
                        Output.Fields = new List<DataField>();

                        DataItem CurrentVars = DataSources.Where(d => d.DataItemType == DataType.Variables).SingleOrDefault();
                        if (CurrentVars != null)
                        {
                            foreach (var f in CurrentVars.Fields)
                            {
                                Output.Fields.Add(new DataField() { Name = f.Name, DataType = f.DataType });
                            }

                        }
                        foreach (var f in Vars.Fields)
                        {
                            if (!Output.Fields.Select(s => s.Name).Contains(f.Name))
                            {
                                Output.Fields.Add(new DataField() { Name = f.Name, DataType = f.DataType });
                            }
                        }
                        Outputs.Add(Output);
                    }
                }
                catch
                {
                    if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
                }
                if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
 
            }, null, true, null);
        }
    }
}
