﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_DataSourceCalculation
{
    public class Parameters
    {
        public int CurrentCalculationID;
        public ObservableCollection<Alias> TableAliases;
    }

    public class Alias : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private string aliasname;
        public string AliasName
        {
            get
            {
                return aliasname;
            }
            set
            {
                aliasname = value;
                NotifyPropertyChanged("AliasName");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    public static class AliasHelper
    {
        public static string GetAlias(string Name, Parameters CurrentParameters)
        {
            return (from s in CurrentParameters.TableAliases where s.Name == Name select s.AliasName).SingleOrDefault();
        }
    }
}
