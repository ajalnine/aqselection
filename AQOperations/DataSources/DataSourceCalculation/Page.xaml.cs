﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using OpenRiaServices.DomainServices.Client;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQCatalogueLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceCalculation
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private bool _updateAliases = true;
        private string _name = string.Empty;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters
            {
                CurrentCalculationID = -1,
                TableAliases = new ObservableCollection<Alias>()
            };
            _updateAliases = true;
            ParametersToUI(availableData);
            
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _updateAliases = false;
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData;
            if (_currentParameters.CurrentCalculationID != -1) QuerySelector.SelectItem(_currentParameters.CurrentCalculationID, false);
            else QuerySelector.RenewData();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        void Alias_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            EnableExit(CheckIsOutputValid());
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckIsOutputValid()) return;
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
            var name = string.IsNullOrEmpty(_name)
                ? $"@Расчет #{_currentParameters.CurrentCalculationID}"
                : _name;
            NameChanged?.Invoke(this, new NameChangedEventArgs { NewName = name });
        }

        private bool CheckIsOutputValid()
        {
            var isDoubled = (from o in _currentParameters.TableAliases group o by o.AliasName into g where g.Count() > 1 select g).Any();
            if (isDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся имена таблиц");
                return false;
            }

            var isOverride = (from o in _currentParameters.TableAliases select o.AliasName).Intersect(from d in _dataSources where d.DataItemType == DataType.Selection select d.Name).Any();
            if (isOverride)
            {
                ShowTableNameError(true, "Некоторые имена совпадают с имеющимися таблицами в расчете");
                return false;
            }
            ShowTableNameError(false, "");
            return true;
        }

        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = message;
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        private void QuerySelector_ItemSelected(object o, ItemOperationEventArgs e)
        {
            if (QuerySelector.SelectedItemID.HasValue)
            {
                _currentParameters.TableAliases = new ObservableCollection<Alias>();
                _updateAliases = _currentParameters.CurrentCalculationID != QuerySelector.SelectedItemID.Value;
                _currentParameters.CurrentCalculationID = QuerySelector.SelectedItemID.Value;
                if (_updateAliases || _currentParameters.TableAliases==null || _currentParameters.TableAliases.Count==0)
                {
                    var cdc = CatalogueOperations.CreateDomainContext();
                    CatalogueOperations.GetDocument(cdc, _currentParameters.CurrentCalculationID, (ev, state) =>
                    {
                        var res = ev.Result.XmlData;
                        var xs2 = new XmlSerializer(typeof(List<DataItem>));
                        var results = (List<DataItem>)xs2.Deserialize(XmlReader.Create(new StringReader(res)));
                        foreach (var s in results.Where(r => r.DataItemType != DataType.Variables))
                        {
                            var a = new Alias { AliasName = s.Name, Name = s.Name };
                            a.PropertyChanged += Alias_PropertyChanged;
                            _currentParameters.TableAliases.Add(a);
                        }
                        
                        cdc.Load(cdc.GetAQ_CatalogueItemQuery(_currentParameters.CurrentCalculationID), LoadBehavior.RefreshCurrent, o2 =>
                        {
                            var currentItem = o2.Entities.SingleOrDefault();
                            _name = currentItem?.Name;
                        }, null);

                    }, null, "Results", null);
                }
            }
            TableNameEditor.ItemsSource = _currentParameters.TableAliases;
            TableNameEditor.UpdateLayout();
            EnableExit(CheckIsOutputValid());
        }
    }
}
