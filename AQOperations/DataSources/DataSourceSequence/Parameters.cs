﻿
namespace SLCalc_DataSourceSequence
{
    public class Parameters
    {
        public string Expression;
        public string Code;
        public string SequenceName;
        public string TableName;
        public string SequenceType;
    }
}
