﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceSequence
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var result = new DataItem
            {
                Name = _currentParameters.TableName,
                TableName = _currentParameters.TableName,
                Fields = new List<DataField>
                {
                    new DataField {Name = _currentParameters.SequenceName, DataType = _currentParameters.SequenceType} 
                }
            };
            _outputs = new List<DataItem> { result };
            var resultingTokens = Tokens.ParseTokens(_currentParameters.Expression, availableData, result);

            if (Tokens.CheckTokens(resultingTokens))
            {
                _inputs = UsedDataItemsHelper.GetUsedTables(resultingTokens, null).ToList();
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
