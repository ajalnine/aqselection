﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQCalculationsLibrary;
using AQChartLibrary;

namespace SLCalc_DataSourceSequence
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private readonly CalculationService _cs;

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            var tableNumber = availableData.Count(a => a.Name.ToLower().StartsWith("таблиц"));
            _dataSources = availableData;
            _currentParameters = new Parameters
            {
                Code = string.Empty,
                Expression = string.Empty,
                TableName = $"Таблица{((tableNumber > 0) ? " " + (tableNumber + 1) : string.Empty)}",
                SequenceName = "Ряд",
                SequenceType = "Double"
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData;
            MainExpressionBuilder.SetDataItems(null, _dataSources, null);
            MainExpressionBuilder.SetExpressionEditor(_currentParameters.Expression);
            MainExpressionBuilder.SetCodeEditor(_currentParameters.Code);
            ExpressionFieldName.Text = _currentParameters.SequenceName;
            TableName.Text = _currentParameters.TableName;
            ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(_currentParameters.SequenceType);
            MainExpressionBuilder.StartHelp();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            var expr = (_currentParameters.Expression.Length > 10) ? _currentParameters.Expression.Substring(0, 10) + "..." : _currentParameters.Expression;
            var name = $"@{_currentParameters.SequenceName}={expr}";
            NameChanged?.Invoke(this, new NameChangedEventArgs { NewName = name });
            if (CheckResult())
            {
                ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
            }
        }

        private bool CheckResult()
        {
            if (TableName.Text.Trim() == string.Empty)
            {
                ShowTableNameError(true, "Не задано имя таблицы");
                return false;
            }

            if (ExpressionFieldName.Text.Trim() == string.Empty)
            {
                ShowTableNameError(true, "Не задано имя переменной");
                return false;
            }

            ShowTableNameError(false, string.Empty);
            return true;
        }

        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = message;
        }

        private void UpdateParameters()
        {
            _currentParameters.SequenceName = ExpressionFieldName.Text;
            _currentParameters.TableName = TableName.Text;
            _currentParameters.Expression = MainExpressionBuilder.GetExpression();
            _currentParameters.Code = MainExpressionBuilder.GetCode();
            _currentParameters.SequenceType = SLDataReportingExtentions.GetFieldType(ExpressionFieldType.SelectedValue.ToString());
        }

        public void DataRequestReadyCallback(string fileName) { }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _cs.BeginCheckExpression(e.CSharpText, _dataSources, "Переменные", CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null")
                {
                    EnableExit(true);
                    _currentParameters.SequenceType = sr.Message;
                    ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(sr.Message.ToLower());
                }
                else
                {
                    EnableExit(false);
                    if (!sr.Success) MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            EnableExit(false);
        }

        private void ExpressionFieldType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _currentParameters.SequenceType = SLDataReportingExtentions.GetFieldType(ExpressionFieldType.SelectedValue.ToString());
        }
    }
}