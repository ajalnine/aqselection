﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_DataSourceSQL
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;
        private Parameters _currentParameters;
        private CalculationService _cs;
        private bool _hasFieldsError;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var variablesTable = availableData.SingleOrDefault(a => a.Name == "Переменные");
            if (variablesTable == null) return;
            var variables = (from v in variablesTable.Fields select new InputDescription() { Name = v.Name, Type = v.DataType}).ToList();
            _inputs = new List<DataItem>();
            var inputVariables = new DataItem() { DataItemType = DataType.Variables, Name = "Переменные", TableName = "Переменные", Fields = new List<DataField>() };
            var exists = false;
            foreach (var v in variables)
            {
                if (_currentParameters.SQL.Contains("@"+v.Name))
                {
                    inputVariables.Fields.Add(new DataField(){ Name = v.Name, DataType = v.Type});
                    exists = true;
                }
            }
            if (exists) _inputs.Add(inputVariables);

            var m = Regex.Match(_currentParameters.SQL, @"@([a-zA-Z0-9а-яА-Я]*)");

            _hasFieldsError = false;
            foreach (var g in m.Groups)
            {
                if (!variablesTable.Fields.Select(a => a.Name).Contains(g.ToString().Replace("@", "")) && g.ToString().EndsWith("date1") && g.ToString().EndsWith("date2")) _hasFieldsError = true;
            }
            var replaces = Regex.Matches(_currentParameters.SQL, @"(\{@([a-zA-z0-9а-яА-Я]*)([^\{\}]*)\})");
            //Сделать виртуальную таблицу переменных с тестовыми значениями по типу

            _currentParameters.SQL = replaces.Cast<Match>().Aggregate(_currentParameters.SQL, (current, r) => current.Replace(r.Groups[0].Value, "(null)"));

            var sql = _currentParameters.SQL;
            var cisql = sql.ToLower();
            foreach (var dt in availableData)
            {
                if (!cisql.Contains(("[#" + dt.Name + "]").ToLower())) continue;
                _inputs.Add(dt);
                sql = CreateTableScript(dt) + "\r\n" + sql + "\r\n" + CreateDropTableScript(dt);
            }

            _cs = Services.GetCalculationService();
            _cs.BeginGetQueryResults(sql, _currentParameters.TableName, variables, _currentParameters.ConnectionString, GetQueryResultsDone, new object());
        }

        public static string CreateTableScript(DataItem dt)
        {
            var res = "\r\nCREATE TABLE [#" + dt.Name.ToLower() + "] (";
            var isFirst = true;
            foreach (var f in dt.Fields)
            {
                if (!isFirst) res += " , ";
                string datatype;
                switch (f.DataType.ToLower())
                {
                    case "int32":
                    case "system.int32":
                        datatype = " int ";
                        break;
                    case "double":
                    case "system.double":
                        datatype = " decimal ";
                        break;
                    case "boolean":
                    case "system.boolean":
                        datatype = " bit ";
                        break;
                    case "timespan":
                    case "system.timespan":
                    case "datetime":
                    case "system.datetime":
                        datatype = " datetime ";
                        break;
                    default:
                        datatype = " nvarchar ";
                        break;
                }

                res += "[" + f.Name + "] " + datatype;
                isFirst = false;
            }

            res += ")\r\n";
            return res;
        }

        public static string CreateDropTableScript(DataItem dt)
        {
            var res = "\r\n DROP TABLE [#" + dt.Name.ToLower() + "] \r\n";
            return res;
        }

        public void GetQueryResultsDone(IAsyncResult iar)
        {
            try
            {
                var res = _cs.EndGetQueryResults(iar);
                if (res.Success) _outputs = res.Fields;
                else throw new Exception();
            }
            catch
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (_hasFieldsError) DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });

            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
