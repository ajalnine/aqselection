﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_DataSourceSQL
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private CalculationService cs;
        private List<DataItem> QueryResult;
        private List<DataItem> DataSources;
        private List<InputDescription> Variables
        {
            get 
            {
                if (DataSources == null || DataSources.Count == 0) return new List<InputDescription>();
                var VariablesTable = DataSources.Where(a => a.Name == "Переменные").SingleOrDefault();
                if (VariablesTable == null) return new List<InputDescription>();
                var r = (from v in VariablesTable.Fields select new InputDescription(){ Name = v.Name, Type = v.DataType, Value = "''"}).ToList();
                return r;
            }
        }

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
            SQL.IsReadOnly = !Security.IsInRole("SQL");
            ConnectionString.IsEnabled = Security.IsInRole("SQL");
        }

        public void SetupUI(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml != null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(Parameters));
                StringReader sr = new StringReader(parametersXml);
                CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            }
            ParametersToUI(AvailableData);
        }

        public void SetupUI(List<DataItem> AvailableData)
        {
            CurrentParameters = new Parameters();
            CurrentParameters.SQL = string.Empty;
            CurrentParameters.TableName = string.Empty;
            ParametersToUI(AvailableData);
        }

        public void ParametersToUI(List<DataItem> AvailableData)
        {
            DataSources = AvailableData;
            TableName.Text = CurrentParameters.TableName;
            ConnectionString.Text = CurrentParameters.ConnectionString ?? Connection.ConnectionString;
            SQL.Text = CurrentParameters.SQL;
        }

        
        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            if (!CheckSQL()) return;
            cs = Services.GetCalculationService();
            cs.BeginGetQueryResults(CurrentParameters.SQL, CurrentParameters.TableName, Variables, CurrentParameters.ConnectionString, GetQueryResultsDone, false);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            if (!CheckSQL()) return;

            var sql = CurrentParameters.SQL.ToLower();
            foreach (var dt in DataSources)
            {
                var tempName = "[#" + dt.Name + "]";
                if (!sql.Contains(tempName.ToLower())) continue;
                sql = DataFlow.CreateTableScript(dt) + "\r\n" + sql + "\r\n" + DataFlow.CreateDropTableScript(dt);
            }
            
            cs = Services.GetCalculationService();
            cs.BeginGetQueryResults(sql, CurrentParameters.TableName, Variables, CurrentParameters.ConnectionString, GetQueryResultsDone, true);
        }

        private void GetQueryResultsDone(IAsyncResult iar)
        {
            try
            {
                var res = cs.EndGetQueryResults(iar);
                if (res.Success) QueryResult = res.Fields;

                Dispatcher.BeginInvoke(delegate
                {
                    if (!res.Success) ShowTableNameError(true, "Ошибка в запросе: " + res.Message);
                    else if (CheckIsOutputValid())
                    {
                        ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = (bool)iar.AsyncState, ParametersXML = GetParametersXML() });
                    }
                });
            }
            catch(Exception e)
            {
                ShowTableNameError(true, "Ошибка: " + e.Message);
            }
        }

        private void ShowTableNameError(bool Show, string Message)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
                ErrorLabel.Text = Message;
            });
        }

        private bool CheckIsOutputValid()
        {
            if (TableName.Text.Trim() == string.Empty)
            {
                ShowTableNameError(true, "Имя не задано");
                return false;
            }
            if (DataSources != null && (from v in DataSources select v.Name).Contains(TableName.Text))
            {
                ShowTableNameError(true, "Имя уже используется");
                return false;
            }

            ShowTableNameError(false, ""); return true;
        }

        private bool CheckSQL()
        {
            string[] ProhibitedKeywords = {"drop ", "delete " , "insert ", "update ", "truncate "};
            foreach(var KeyWord in ProhibitedKeywords)
            {
                if (CurrentParameters.SQL.ToLower().Contains(KeyWord))
                {
                    ShowTableNameError(true, "Запрос содержит недопустимое ключевое слово: " + KeyWord);
                    return false;
                }
            }
            return true;
        }

        private void UpdateParameters()
        {
            CurrentParameters.SQL = SQL.Text;
            CurrentParameters.TableName = TableName.Text;
            CurrentParameters.ConnectionString = ConnectionString.Text;
        }

        private void CopyData_Click(object sender, RoutedEventArgs e)
        {
            SetDataToClipboard();
        }

        private void SetDataToClipboard()
        {
            var sql = "set dateformat ymd\r\n";
            sql += "declare @date1 datetime\r\n";
            sql += "declare @date2 datetime\r\n";
            sql += $"set @date1='{DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd")}'\r\n";
            sql += $"set @date2='{DateTime.Now.ToString("yyyy-MM-dd")}'\r\n\r\n";
            foreach (var v in Variables)
            {
                if (CurrentParameters.SQL.Contains("@" + v.Name))
                {
                    switch (v.Type.ToLower())
                    {
                        case "double":
                            sql += $"declare @{v.Name} float\r\nset @{v.Name}=null\r\n";
                            break;
                        case "int":
                        case "int32":
                            sql += $"declare @{v.Name} integer\r\nset @{v.Name}=null\r\n";
                            break;
                        case "boolean":
                            sql += $"declare @{v.Name} bit\r\nset @{v.Name}=null\r\n";
                            break;
                        case "string":
                            sql += $"declare @{v.Name} nvarchar(MAX)\r\nset @{v.Name}=null\r\n";
                            break;
                        case "datetime":
                            sql += $"declare @{v.Name} datetime\r\nset @{v.Name}=null\r\n";
                            break;
                        case "timespan":
                            sql += $"declare @{v.Name} time\r\nset @{v.Name}=null\r\n";
                            break;
                    }
                }
            }
            sql += CurrentParameters.SQL;
            Clipboard.SetText(sql);
        }
        public void DataRequestReadyCallback(string FileName) { }
        public void StateChanged(OperationViewMode OVM) { }

        private void ShowCS_Click(object sender, RoutedEventArgs e)
        {
            var v = ConnectionPanel.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            ConnectionPanel.Visibility = v;
            DefaultCS.Visibility = v;
        }
        
        private void DefaultCS_Click(object sender, RoutedEventArgs e)
        {
            ConnectionString.Text = Connection.ConnectionString;
        }
    }
}
