﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using AQAnalysisLibrary;
using AQCalculationsLibrary;
using AQChartViewLibrary;
using AQClientStepLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_Report
{
    
    public partial class Page 
    {
        private int  _s, _insertionPoint, _queuePosition;
        private List<QueueElement> _queue;
        private List<Guid> _pureTablesAtStep, _pureChartsAtStep, _combinedOperationAtStep;

        private void StartRefresh()
        {
            _s = 0;
            _currentParameters.NewTables = new List<NewTableIndex>();

            MainSlidePanel.SlideReady += (o, er) =>
            {
                if ((er.ElementType & ReportElementType.DataTable) == ReportElementType.DataTable)
                {
                    if (_currentParameters.TableGeneratingOperations.Select(a => a.Guid).Contains(er.SAR.AnalysisGuid))
                    {
                        foreach (var i in er.SAR.InnerTables)
                        {
                            var tableToReplace = _data.SingleOrDefault(a => a.TableName == i.TableName);
                            if (tableToReplace != null)
                            {
                                _data.Remove(tableToReplace);
                            }
                            _data.Add(i);
                            _currentParameters.NewTables.Add(new NewTableIndex { NewTable = i.GetClone(), PositionOfOperation = _s });
                        }
                    }
                }

                _queuePosition++;
                if (_queuePosition < _queue.Count)ProcessQueuePosition();
                else DoStepOrFinalize();
            };

            CreateQueueForStep();
        }


        private void CreateQueueForStep()
        {
            _queue = new List<QueueElement>();

            _pureTablesAtStep = _currentParameters.TableGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Except(_currentParameters.SARs.Select(a => a.AnalysisGuid)).ToList();

            _pureChartsAtStep = _currentParameters.ChartGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Where(a => !_currentParameters.TableGeneratingOperations.Select(b => b.Guid).Contains(a)).ToList();

            _combinedOperationAtStep = _currentParameters.ChartGeneratingOperations
                .Where(a => a.PositionOfOperation == _s)
                .Select(a => a.Guid)
                .Where(a => _currentParameters.TableGeneratingOperations.Select(b => b.Guid).Contains(a)).ToList();

            _queue.AddRange(_pureTablesAtStep.Select(a=>new QueueElement {Guid = a}));
            foreach (var co in _combinedOperationAtStep)
            {
                var sars = _currentParameters.SARs.Where(a => co == a.AnalysisGuid);
                foreach (var sar in sars)
                {
                    var position = _currentParameters.SARs.IndexOf(sar);
                    _queue.Add(new QueueElement {Guid = co, SAR = sar, SARPosition = position});
                }
            }
            foreach (var co in _pureChartsAtStep)
            {
                var sars = _currentParameters.SARs.Where(a => co == a.AnalysisGuid);
                foreach (var sar in sars)
                {
                    var position = _currentParameters.SARs.IndexOf(sar);
                    _queue.Add(new QueueElement { Guid = co, SAR = sar, SARPosition = position });
                }
            }
            _queuePosition = 0;
            if (_queue.Count>0)ProcessQueuePosition();
            else DoStepOrFinalize();
        }

        private void ProcessQueuePosition()
        {
            var processingItem = _queue[_queuePosition];
            Dispatcher.BeginInvoke(() =>
            {
                if (_pureTablesAtStep.Contains(processingItem.Guid))
                {
                    var o = _currentParameters.TotalOperations.SingleOrDefault(a => processingItem.Guid == a.Guid);
                    if (o != null) MainSlidePanel.GetInternalTablesForSAR(_data, o.Operation, o.Guid);
                }
                else
                {
                    MainSlidePanel.RenderSAR(_data, processingItem.SAR, false, _insertionPoint + processingItem.SARPosition);
                }
            });
        }
        
        private void DoStepOrFinalize()
        {
            if (_s < _currentParameters.InternalSteps.Count)
            {
                ClientStepProcessor.ProcessStep(_data, _currentParameters.InternalSteps[_s]);
                _s++;
                CreateQueueForStep();
            }
            else
            {
                RefreshProgressIndicator.Visibility = Visibility.Collapsed;
                DeleteAll.IsEnabled = true;
                MainChartView.DoAnalysis(_data, null, _currentParameters.InternalSteps, null, null, null, null);
            }
        }
    }
}