﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQChartViewLibrary;
using AQClientStepLibrary;
using AQControlsLibrary;
using AQReportingLibrary;
using AQStepFactoryLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_Report
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private readonly CalculationService _cs;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<DataItem> _availableData;
        private List<SLDataTable> _data;
        private List<SLDataTable> _internalTables;
        private Task _autoFillTask;
        private DataLoader _dataSetLoader;


        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            SetDefaultParameters();

            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters
            {
                SARs = new List<SerializableAnalysisResult>(),
                InternalSteps = new List<Step>(),
                ChartGeneratingOperations = new List<OperationIndex>(),
                TableGeneratingOperations = new List<OperationIndex>(),
                NewTables = new List<NewTableIndex>(),
                TotalOperations = new List<GuidStepPair>()
            };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            if (_data == null) return;
            if (_currentParameters?.TableGeneratingOperations == null) MainTableSelector.CreateTableSelectors(_data, _data.Count > 0 ? _data[1] : _data[0]);
            else
            {
                var selectedTable = _data.SingleOrDefault(a => a.TableName == _currentParameters.SelectedTable);
                MainTableSelector.CreateTableSelectors(_data, selectedTable);
                 _currentParameters.SelectedTable = selectedTable?.TableName;
            }

            if (_currentParameters?.ChartGeneratingOperations == null || _currentParameters.ChartGeneratingOperations.Count==0) MainChartView.DoAnalysis(_data, null, _currentParameters?.InternalSteps, null, null, null, null, false);
            else
            {
                var last = _currentParameters?.ChartGeneratingOperations.Last().Guid;
                var lastAnalysis = _currentParameters?.TotalOperations.SingleOrDefault(a=>a.Guid==last);
                if (lastAnalysis != null)
                {
                    var selectedTable = _data.SingleOrDefault(a => a.TableName == _currentParameters.SelectedTable);
                    MainChartView.DoAnalysis(_data, selectedTable, _currentParameters.InternalSteps,  lastAnalysis.Operation, null, null, null);
                    _currentParameters.SelectedTable = selectedTable?.TableName;
                }
            }
            MainSlidePanel.Clear();

            if (_currentParameters?.SARs != null)
            {
                foreach (var c in _currentParameters.SARs)
                {
                    MainSlidePanel.InsertSerializableAnalysisResultElement(c);
                }
            }
            if (_currentParameters?.SARs != null) OKButton.IsEnabled = _currentParameters.SARs.Count > 0 || _currentParameters.NewTables.Count > 0;
        }

        private void UIToParameters()
        {
            _currentParameters.SARs = MainSlidePanel.GetSlides().ToList();
            _currentParameters.InternalSteps = MainChartView.GetAnalisysResult().InnerSteps;
            var requiredOperations = _currentParameters.SARs.Select(a => a.AnalysisGuid).Distinct().ToList();
            var existingOperations = _currentParameters.ChartGeneratingOperations.Select(a=>a.Guid).ToList();
            var todeleteGuids = existingOperations.Except(requiredOperations);
            var lastTableGuid = _currentParameters.TableGeneratingOperations.LastOrDefault()?.Guid;
            foreach (var d in todeleteGuids)
            {
                var toDel = _currentParameters.TotalOperations.Where(a => a.Guid == d).ToList();
                foreach (var item in toDel) _currentParameters.TotalOperations.Remove(item);
                var toDelOp = _currentParameters.ChartGeneratingOperations.Where(a => a.Guid == d).ToList();
                foreach (var item in toDelOp) _currentParameters.ChartGeneratingOperations.Remove(item);
                if (lastTableGuid == d) _currentParameters.LastOperationTableAdded = false;
            }
            var lastAnalysis = MainChartView.GetAnalisysResult();
            _currentParameters.DefaultColorScale = lastAnalysis.UsedColorScale;
            _currentParameters.IsInvertedPanel = lastAnalysis.IsInverted;
            _currentParameters.SelectedTable = MainChartView.GetSelectedTable().TableName;
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            var newName = $"Отчет:";
            if (_currentParameters.SARs.Count > 0) newName += $" слайдов: {_currentParameters.SARs.Count}, ";
            if (_currentParameters.NewTables.Count > 0) newName +=$" таблиц: {_currentParameters.NewTables.Count}, ";
            newName += $" операций: {(_currentParameters.InternalSteps.Count ==0 ? "нет" : _currentParameters.InternalSteps.Count.ToString())}.";
            NameChanged?.Invoke(this, new NameChangedEventArgs {NewName = newName});
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void AutoButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataRequest != null)
            {
                _autoFillTask = new Task(new[] { "Исполнение расчета", "Прием данных" }, "Данные для автозаполнения", TaskPanel);
                _autoFillTask.SetState(0, TaskState.Processing);
                AutoButton.IsEnabled = false;
                DataRequest.Invoke(this, new EventArgs());
            }
        }
        
        public void DataRequestReadyCallback(string fileName)
        {
            _autoFillTask.SetState(0, TaskState.Ready);
            _autoFillTask.SetState(1, TaskState.Processing);
            _dataSetLoader = new DataLoader();
            _dataSetLoader.DataProcessed += Data_DataProcessed;
            _dataSetLoader.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            _autoFillTask.SetState(0, TaskState.Ready);
            _autoFillTask.SetState(1, TaskState.Ready);
            Dispatcher.BeginInvoke(() =>
            {
                EmptyDataSet.Visibility = Visibility.Collapsed;
                AutoButton.Visibility = Visibility.Collapsed;
                _data = _dataSetLoader.GetTables().Select(a=>a.Clone()).ToList();
                for (var s = 0; s <= _currentParameters.InternalSteps.Count; s++)
                {
                    var tablesAtStep = _currentParameters.NewTables.Where(a => a.PositionOfOperation == s).Select(a=>a.NewTable).ToList();
                    _data.AddRange(tablesAtStep);
                    if (s < _currentParameters.InternalSteps.Count) ClientStepProcessor.ProcessStep(_data, _currentParameters.InternalSteps[s]);
                }

                TaskPanel.Children.Clear();
                if (_currentParameters.ChartGeneratingOperations.Count +
                    _currentParameters.TableGeneratingOperations.Count > 0)
                {
                    Refresh.Visibility = Visibility.Visible;
                    Refresh.IsEnabled = _currentParameters.ChartGeneratingOperations.Count + _currentParameters.TableGeneratingOperations.Count > 0;
                    DeleteAll.IsEnabled = true;
                }
                DeleteAll.Visibility = Visibility.Visible;
                ClearButton.Visibility = Visibility.Visible;
                DeleteButton.Visibility = Visibility.Visible;
                DeleteButton.IsEnabled = _data.Count > 0;
                ParametersToUI(_dataSources);
            });
        }

        private void TableSelector_OnSelectedTableChanged(object o, SelectedTableChangedEventArgs e)
        {
            MainChartView.DoAnalysis(e.AllTables, e.SelectedTable, _currentParameters.InternalSteps, null, null, null, null, false);
            _currentParameters.SelectedTable = e.SelectedTable.TableName;
            MainTableSelector.Focus();
        }

        private void MainChartView_OnInteractiveCommandProcessed(object o, InteractiveCommandProcessedEventArgs e)
        {
            if (e.Result.TablesCreated) MainTableSelector.CreateTableSelectors(_data, MainChartView.GetSelectedTable());
            if (e.Result.NewSteps.Any()) _currentParameters.InternalSteps = MainChartView.GetAnalisysResult().InnerSteps;
            Refresh.IsEnabled = false;
        }

        private void MainChartView_OnReportInteractiveRename(object o, ReportInteractiveRenameEventArgs e)
        {
            foreach (var ro in e.RenameOperations)
            {
                if (ro.RenameTarget == ReportRenameTarget.Table)
                {
                    MainTableSelector.ChangeTableName(ro.OldName, ro.NewName);
                    _currentParameters.SelectedTable = ro.NewName;
                }
            }
            Refresh.IsEnabled = false;
        }

        private void MainChartView_OnInternalTablesReadyForExport(object o, InternalTablesReadyForExportEventArgs e)
        {
            AddInternalTables.Visibility = Visibility.Visible;
            _internalTables = e.InternalTables;
        }

        private void MainChartView_OnAnalysisStarted(object o, AnalysisStartedEventArgs e)
        {
            AddInternalTables.Visibility = Visibility.Collapsed;
            NewSlideButton.Visibility = Visibility.Collapsed;
            _internalTables = null;
            _currentParameters.LastOperationTableAdded = false;
        }

        private void MainChartView_OnAnalysisFinished(object o, AnalysisReadyEventArgs e)
        {
            NewSlideButton.Visibility = Visibility.Visible;
        }

        private void MainChartView_OnSelectedTableChanged(object o, SelectedTableChangedEventArgs e)
        {
            MainTableSelector.CreateTableSelectors(_data, e.SelectedTable);
            _currentParameters.SelectedTable = e.SelectedTable.TableName;
        }

        private void AddInternalTables_Click(object sender, RoutedEventArgs e)
        {
            var existed = _data.Select(a => a.TableName).ToList();
            foreach (var t in _internalTables)
            {
                if (existed.Contains(t.TableName))
                {
                    MessageBox.Show($"Перед добавлением таблиц анализа необходимо переименовать существующую таблицу {t.TableName}. Переименовать таблицу можно с помощью изменения её имени в заголовке любого графика. При последующих построениях отчета переименование таблицы будет производиться автоматически.", "Совпадение имен таблиц", MessageBoxButton.OK);
                    return;
                }
            }

            _data.AddRange(_internalTables);
            var analysis = MainChartView.GetAnalisysResult();
            if (_currentParameters.TotalOperations.Select(a=>a.Guid).All(a => a != analysis.AnalysisGuid))
            {
                _currentParameters.TotalOperations.Add(new GuidStepPair {Guid = analysis.AnalysisGuid, Operation = analysis.AnalysisOperation});
            }
            _currentParameters.TableGeneratingOperations.Add(new OperationIndex { Guid = analysis.AnalysisGuid, PositionOfOperation = MainChartView.GetAnalisysResult().InnerSteps.Count });
            
            foreach (var it in _internalTables)
            {
                _currentParameters.NewTables.Add(new NewTableIndex { NewTable = it.Clone(), PositionOfOperation = _currentParameters.InternalSteps.Count});
            }
            
            MainTableSelector.CreateTableSelectors(_data, MainTableSelector.GetSelectedTable());
            _currentParameters.LastOperationTableAdded = true;
            OKButton.IsEnabled = true;
            Refresh.IsEnabled = false;
            DeleteButton.IsEnabled = _data.Count > 0;
        }

        private void NewSlideButton_Click(object sender, RoutedEventArgs e)
        {
            var analysis = MainChartView.GetAnalisysResult();
            MainSlidePanel.InsertAnalysisResultElement(analysis, false);
            if (_currentParameters.TotalOperations.Select(a => a.Guid).All(a => a != analysis.AnalysisGuid))
            {
                _currentParameters.TotalOperations.Add(new GuidStepPair { Guid = analysis.AnalysisGuid, Operation = analysis.AnalysisOperation });
            }
            _currentParameters.ChartGeneratingOperations.Add(new OperationIndex { Guid = analysis.AnalysisGuid, PositionOfOperation = MainChartView.GetAnalisysResult().InnerSteps.Count });
        }

        private void MainSlidePanel_OnSlideToAnalysis(object o, SlideToAnalysisEventArgs e)
        {
            MainChartView.DoAnalysis(_data, null, null, e.Operation, null, null, null);
        }

        private void MainSlidePanel_OnSlideCollectionChanged(object o, SlideCollectionChangedEventArgs e)
        {
            OKButton.IsEnabled = e.SlideCollection.Count > 0 || _currentParameters.NewTables.Count > 0;
            Refresh.IsEnabled &= e.SlideCollection.Count > 0 || _currentParameters.NewTables.Count > 0;
            DeleteAll.IsEnabled = e.SlideCollection.Count > 0;
        }
        
        private void RefreshSlides_OnClick(object sender, RoutedEventArgs e)
        {
            DeleteAll.IsEnabled = false;
            UIToParameters();
            Refresh.IsEnabled = false;
            _insertionPoint = MainSlidePanel.ResetInternalSlidesCollection(true, _currentParameters.SARs.Count);
            RefreshProgressIndicator.Visibility = Visibility.Visible;
            _data = _dataSetLoader.GetTables().Select(a => a.Clone()).ToList();
            StartRefresh();
        }
        
        private void DeleteAll_OnClick(object sender, RoutedEventArgs e)
        {
            _currentParameters.SARs = new List<SerializableAnalysisResult>();
            var requiredOperations = _currentParameters.TableGeneratingOperations.Select(a => a.Guid).Distinct().ToList();
            _currentParameters.ChartGeneratingOperations = new List<OperationIndex>();
            var todeleteOperations = _currentParameters.TotalOperations.Where(a=>!requiredOperations.Contains(a.Guid)).ToList();
            foreach (var d in todeleteOperations)
            {
                _currentParameters.TotalOperations.Remove(d);
            }
            ParametersToUI(_availableData);
            RefreshProgressIndicator.Visibility = Visibility.Collapsed;
            Refresh.IsEnabled = false;
            DeleteAll.IsEnabled = false;
        }
        
        private void MainSlidePanel_OnInternalTablesReady(object o, InternalTablesReadyForExportEventArgs e)
        {
            if (_currentParameters.TableGeneratingOperations.Select(a => a.Guid).Contains(e.AnalysisGuid))
            {
                foreach (var i in e.InternalTables)
                {
                    var tableToReplace = _data.SingleOrDefault(a => a.TableName == i.TableName);
                    if (tableToReplace != null)
                    {
                        _data.Remove(tableToReplace);
                        _data.Add(i);
                    }
                }
            }
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentParameters.SelectedTable == null) return;
            var selectedTable = _data.SingleOrDefault(a => a.TableName == _currentParameters.SelectedTable);
            if (selectedTable == null) return;
            var message = (selectedTable.TableName == "Переменные")
                ? "Удалить таблицу \"" + selectedTable.TableName + "\" ? Она может использоваться последующими операциями расчета."
                : "Удалить таблицу \"" + selectedTable.TableName + "\" ?";
            var mbr = MessageBox.Show(message,
                "Удаление", MessageBoxButton.OKCancel);
            if (mbr != MessageBoxResult.OK) return;
            _data.Remove(selectedTable);
            MainChartView.GetAnalisysResult().InnerSteps.AddRange(StepFactory.CreateDeleteTable(selectedTable.TableName));
            _currentParameters.InternalSteps = MainChartView.GetAnalisysResult().InnerSteps;
            MainTableSelector.CreateTableSelectors(_data, _data.Count > 1 ? _data[1] : _data[0]);
            MainChartView.DoAnalysis(_data, null, _currentParameters.InternalSteps, null, null, null, null, false);
            DeleteButton.IsEnabled = _data.Count > 0;
        }

        private void ClearButton_OnClick(object sender, RoutedEventArgs e)
        {
            var message = "Начать анализ заново? Будут удалены все таблицы, слайды и операции";
            var mbr = MessageBox.Show(message,
                "Удаление", MessageBoxButton.OKCancel);
            if (mbr != MessageBoxResult.OK) return;
            _data = _dataSetLoader.GetTables().Select(a => a.Clone()).ToList();
            SetupUI(_availableData);
        }
    }
}