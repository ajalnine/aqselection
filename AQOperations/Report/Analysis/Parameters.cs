﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;
using AQBasicControlsLibrary;
using AQChartViewLibrary;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace SLCalc_Report
{
    public class Parameters
    {
        public List<Step> InternalSteps;
        public List<GuidStepPair> TotalOperations; 
        public List<OperationIndex> TableGeneratingOperations;
        public List<OperationIndex> ChartGeneratingOperations;
        public List<SerializableAnalysisResult> SARs;
        public List<NewTableIndex> NewTables;
        public string SelectedTable;
        public bool IsInvertedPanel;
        public bool LastOperationTableAdded;
        public ColorScales DefaultColorScale;
    }
}
