﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQReportingLibrary;


namespace SLCalc_Report
{
    
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs = new List<DataItem>();
        private List<DataItem> _outputs = new List<DataItem>();
        private List<DataItem> _availableData;
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));


            var dfs = new DataFlowSimulator();
            dfs.DataFlowSimulationFinished += DfsOnDataFlowSimulationFinished;
            dfs.DataFlowSimulationError += DfsOnDataFlowSimulationError;
            dfs.SimulateDataFlow(new CalculationDescription { StepData = _currentParameters.InternalSteps }, availableData.ToList(), _currentParameters.NewTables);
        }

        private void DfsOnDataFlowSimulationError(object o, DataFlowSimulationErrorEventArgs e)
        {
            DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = e.Error });
        }

        private void DfsOnDataFlowSimulationFinished(object o, DataFlowSimulationEventArgs e)
        {
            var usedtablesInChart = _currentParameters.SARs.Select(a => a.UsedTable).Distinct().ToList();
            var usedtables = e.UsedInputs.Select(a => a.Name).Union(usedtablesInChart).Distinct().ToList();
            _inputs = usedtables.SelectMany(a => _availableData.Where(b => b.Name == a)).Distinct().ToList();
            foreach (var u in e.UsedInputs)
            {
                if (!_inputs.Select(a=>a.Name).Contains(u.Name))_inputs.Add(u);
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = e.Results, Deleted = e.Deleted });
        }
    }
}
