﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQCalculationsLibrary;

namespace SLCalc_CheckExpression
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;

        CalculationService cs;

        public Page()
        {
            InitializeComponent();
            cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DataSources = availableData;
            DataItem CurrentTable = DataSources.Where(s => s != null).Where(s => s.Name == CurrentParameters.TableName).SingleOrDefault();

            if (CurrentTable != null)
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources where di != null select di.Name);
                DataSourceSelector.SelectedItem = CurrentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                MainExpressionBuilder.SetDataItems(CurrentTable, DataSources, null);
                MainExpressionBuilder.SetExpressionEditor(CurrentParameters.Expression);
                MainExpressionBuilder.SetCodeEditor(CurrentParameters.Code);
            }
            else
            {
                DataSourceSelector.ItemsSource = (from di in DataSources where di != null select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            MainExpressionBuilder.StartHelp();
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            string Name = (CurrentParameters.Expression.Length > 20) ? CurrentParameters.Expression.Substring(0, 20) + "..." : CurrentParameters.Expression;
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs() { NewName = Name });
            if (CheckResult()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        private bool CheckResult()
        {
            ShowTableNameError(false, string.Empty);
            return true;
        }

        private void ShowTableNameError(bool Show, string Message)
        {
            ErrorLabel.Visibility = (Show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = Message;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            MainExpressionBuilder.SetDataItems(DataSources.Where(a=>a!=null).Where(s => s.Name == CurrentParameters.TableName).SingleOrDefault(), DataSources, null);
        }

        private void UpdateParameters()
        {
            CurrentParameters.Expression = MainExpressionBuilder.GetExpression();
            CurrentParameters.Code = MainExpressionBuilder.GetCode();
        }

        public void DataRequestReadyCallback(string fileName){}

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            cs.BeginCheckExpression(e.CSharpText, DataSources, CurrentParameters.TableName, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            this.Dispatcher.BeginInvoke(delegate()
            {
                StepResult sr = cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null" && sr.Message=="Boolean")
                {
                    EnableExit(true);
                }
                else
                {
                    if (!sr.Success)
                    {
                        EnableExit(false);
                        MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                    }
                    else if (sr.Message != "Boolean") MessageBox.Show("Результат должен быть двоичного типа Истина/Ложь", "Ошибка выражения", MessageBoxButton.OK);
                }
            });
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            EnableExit(false);
        }

        public void StateChanged(OperationViewMode OVM) 
        {
            if (OVM == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }
    }
}