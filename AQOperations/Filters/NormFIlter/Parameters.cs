﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_NormFilter
{
    public class Parameters
    {
        public string TableName;
        public bool Include;
        public bool Case;
        public bool All;
        public bool InRange;
        public string Field;
    }
}
