﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_NormFilter
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<DataItem> _availableData;
        private DataItem SelectedTable;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            CurrentParameters = new Parameters()
            {
                TableName = availableData.Where(s => s.DataItemType == DataType.Selection).First().TableName,
                Include = true,
                Case = true,
                All = false,
                InRange = false,
                Field = null,
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!DataSources.Select(a => a.Name).Contains(CurrentParameters.TableName)) UpdateDataSourceSelector(null);
            else UpdateDataSourceSelector(CurrentParameters.TableName);
            InRanges.IsChecked = CurrentParameters.InRange;
            OutRanges.IsChecked = !CurrentParameters.InRange;
            Include.IsChecked = CurrentParameters.Include;
            Case.IsChecked = CurrentParameters.Case;
            Exclude.IsChecked = !CurrentParameters.Include;
            Value.IsChecked = !CurrentParameters.Case;
            All.IsChecked = CurrentParameters.All;
            Every.IsChecked = !CurrentParameters.All;
            All.IsEnabled = CurrentParameters.Case;
            Every.IsEnabled = CurrentParameters.Case;
        }

        private bool UpdateDataSourceSelector(string SourceToSelect)
        {
            if (DataSources.Select(s => s.Name).Contains(SourceToSelect) && SourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = SourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                UpdateFieldsSource();
                return true;
            }

            else if (DataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            return false;
        }
        private void UpdateFieldsSource()
        {
            var currentTable = DataSources.Where(a => a != null).SingleOrDefault(s => s.Name == CurrentParameters.TableName);
            if (currentTable == null) return;
            var fields = new List<string> {"  Любое поле"}.Union(currentTable.Fields.Where(a => a != null && a.DataType.ToLower().EndsWith("double")).Select(s => s.Name)).ToList();
            FieldSelector.ItemsSource = fields;
            FieldSelector.UpdateLayout();
            if (!fields.Any()) return;
            if (CurrentParameters.Field != null && fields.Contains(CurrentParameters.Field)) FieldSelector.SelectedItem = CurrentParameters.Field;
            else if (CurrentParameters.Field == null) FieldSelector.SelectedItem = "  Любое поле";
            FieldSelector.UpdateLayout();
        }
        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }
        
        private void UIToParameters()
        {
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            CurrentParameters.Include = Include.IsChecked.Value;
            CurrentParameters.Case = Case.IsChecked.Value;
            CurrentParameters.All = All.IsChecked.Value;
            CurrentParameters.InRange = InRanges.IsChecked.Value;
            CurrentParameters.Field = FieldSelector.SelectedValue?.ToString() == "  Любое поле"
                                      ? null
                                      : FieldSelector.SelectedValue?.ToString();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void CheckResult()
        {
            EnableExit(true);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            SelectedTable = _availableData.SingleOrDefault(a => a.Name == CurrentParameters.TableName);
            UpdateFieldsSource();
            CheckResult();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters(); 
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }
        #endregion


        public void StateChanged(OperationViewMode OVM) { }

        private void Case_Checked(object sender, RoutedEventArgs e)
        {
            if (All != null) All.IsEnabled = true;
            if (Every != null) Every.IsEnabled = true;
        }

        private void Case_Unchecked(object sender, RoutedEventArgs e)
        {
            All.IsEnabled = false;
            Every.IsEnabled = false;
        }

        public void DataRequestReadyCallback(string fileName)
        {
            
        }
    }
}