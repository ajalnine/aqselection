﻿using System;
using System.Net;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_RangeFilter
{
    public class Parameters
    {
        public string TableName;
        public string GroupField;
        public string Groups;
        public bool Include;
        public bool Case;
        public bool All;
        public ObservableCollection<Range> Ranges;
    }

    public class Range : INotifyPropertyChanged
    {
        private double? _min;
        public double? Min
        {
            get
            {
                return _min;
            }
            set
            {
                _min = value;
                NotifyPropertyChanged("Min");
            }
        }

        private double? _max;
        public double? Max
        {
            get
            {
                return _max;
            }
            set
            {
                _max = value;
                NotifyPropertyChanged("Max");
            }
        }

        public string MinText
        {
            get
            {
                return _min.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    Min = null;
                }
                else
                {
                    double min;
                    if (!double.TryParse(value, out min)) throw (new InvalidCastException("Неверный формат числа"));
                    Min = min;
                }
                NotifyPropertyChanged("MinText");
            }
        }

        public string MaxText
        {
            get
            {
                return _max.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value)) Max = null;
                else
                {
                    double max;
                    if (!double.TryParse(value, out max)) throw (new InvalidCastException("Неверный формат числа"));
                    Max = max;
                }
                NotifyPropertyChanged("MaxText");
            }
        }

        private string _parameter;
        public string Parameter
        {
            get
            {
                return _parameter;
            }
            set
            {
                _parameter = value;
                NotifyPropertyChanged("Parameter");
            }
        }

        private string _group;
        public string Group
        {
            get
            {
                return _group;
            }
            set
            {
                _group = value;
                NotifyPropertyChanged("Group");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
