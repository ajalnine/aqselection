﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace SLCalc_Outliers
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            
            var currentTable = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable != null)
            {
                _inputs.Add(currentTable);
                _outputs.Add(currentTable);
                var report = new DataItem { DataItemType = DataType.Selection, Name = _currentParameters.ReportName, TableName = _currentParameters.ReportName, Fields = new List<DataField>() };
                foreach (var s in _currentParameters.IncludeInReport)
                {
                    var field = (from f in currentTable.Fields where f.Name==s select f).SingleOrDefault();
                    if (field == null)
                    {
                        DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    }
                    else
                    {
                        var df = new DataField { Name = field.Name, DataType = field.DataType };
                        report.Fields.Add(df);
                    }
                }
                var layers = (from f in currentTable.Fields where f.Name == _currentParameters.LayerField select f).SingleOrDefault();
                if (layers == null && _currentParameters.LayerField!=null)
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                }
                report.Fields.Add(new DataField { Name = "Поле выброса", DataType = "String"});
                report.Fields.Add(new DataField { Name = "Значение выброса", DataType = "Double" });
                report.Fields.Add(new DataField { Name = "Нижняя граница", DataType = "Double" });
                report.Fields.Add(new DataField { Name = "Верхняя граница", DataType = "Double" });
                report.Fields.Add(new DataField { Name = "Номер прохода", DataType = "Double" });
                report.Fields.Add(new DataField { Name = "Способ фильтрации", DataType = "String" });
                if (_currentParameters.GenerateReport) _outputs.Add(report);

                var names = from c in currentTable.Fields select c.Name;
                if (_currentParameters.Filtering.Count != _currentParameters.Filtering.Intersect(names).Count())
                {
                    DataFlowError?.Invoke(this,
                        new DataFlowErrorEventArgs {Error = InconsistenceState.LostInputFields});
                }
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs{ Error = InconsistenceState.LostInput });
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs{ Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
