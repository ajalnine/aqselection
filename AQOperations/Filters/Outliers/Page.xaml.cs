﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQMathClasses;
using static System.Double;
using static System.String;

// ReSharper disable once CheckNamespace
namespace SLCalc_Outliers
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<FilterTableData> _filteringFields;
        private List<FilterTableData> _includeInReport;
        private List<string> _layerFields; 

        public Page()
        {
            InitializeComponent();
            _currentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters() { Filtering = new List<string>(), K=1.5, KZ=3, IncludeInReport = new List<string>(), TableName = Empty, FilterMethod="AutoAD", GenerateReport = true, ReportName = "Выбросы", IsCyclic = true, RemoveValueOnly = true };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            CyclicButton.IsChecked = _currentParameters.IsCyclic;
            SingleButton.IsChecked = !_currentParameters.IsCyclic;
            DeleteValue.IsChecked = _currentParameters.RemoveValueOnly;
            DeleteRecord.IsChecked = !_currentParameters.RemoveValueOnly;
            SaveReport.IsChecked = _currentParameters.GenerateReport;
            IQRK.Text = _currentParameters.K.ToString(CultureInfo.InvariantCulture);
            K.Text = _currentParameters.KZ.ToString(CultureInfo.InvariantCulture);
            UpdatePercents();
            
            var rb = ModePanel.Children.Cast<RadioButton>().SingleOrDefault(a => a.Tag.ToString() == _currentParameters.FilterMethod);
            if (rb != null) rb.IsChecked = true;

            OutliersTableName.Text = _currentParameters.ReportName;

            _dataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!_dataSources.Select(a => a.Name).Contains(_currentParameters.TableName))
            {
                UpdateDataSourceSelector(null);
                _currentParameters.Filtering = new List<string>();
                return;
            }
            UpdateDataSourceSelector(_currentParameters.TableName);
            UpdateFields();
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void UpdateDataSourceSelector(string sourceToSelect)
        {
            if (_dataSources.Select(s => s.Name).Contains(sourceToSelect) && sourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = sourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                return;
            }
            if (_dataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
                
            OutliersTableName.Text = _currentParameters.TableName + "_Выбросы";
            UpdateFields();
        }

        private bool UpdateParameters()
        {
            ErrorMessage.Text = Empty;
            
            _currentParameters.Filtering = (from g in _filteringFields where g.IsFiltering select g.FieldName).ToList();
            _currentParameters.IncludeInReport = (from g in _includeInReport where g.IsFiltering select g.FieldName).ToList();
            _currentParameters.GenerateReport = SaveReport.IsChecked.Value;
            _currentParameters.IsCyclic = CyclicButton.IsChecked.Value;
            _currentParameters.RemoveValueOnly = DeleteValue.IsChecked.Value;
            _currentParameters.ReportName = OutliersTableName.Text;

            if (GroupSelector.SelectedItem != null && (string) GroupSelector.SelectedItem != "Нет")
            {
                _currentParameters.LayerField = GroupSelector.SelectedItem.ToString();
            }
            else _currentParameters.LayerField = null;

            if (!TryParse(K.Text, out _currentParameters.KZ))
            {
                ErrorMessage.Text = "Не указан коэффициент для случая нормального распределения";
                return false;
            }

            if (!TryParse(IQRK.Text, out _currentParameters.K))
            {
                ErrorMessage.Text = "Не указан коэффициент для расчета по квартильным размахам";
                return false;
            }

            if (IsNullOrWhiteSpace(_currentParameters.ReportName) && _currentParameters.GenerateReport)
            {
                ErrorMessage.Text = "Имя таблицы отчета о выбросах не может быть пустым";
                return false;
            }
            
            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.ReportName) && _currentParameters.GenerateReport)
            {
                ErrorMessage.Text = "Имя отчета о выбросах уже используется в расчете";
                return false;
            }

            if (_currentParameters.Filtering.Count==0)
            {
                ErrorMessage.Text = "Не выбраны поля для фильтрации";
                return false;
            }
            
            var rb = ModePanel.Children.Cast<RadioButton>().SingleOrDefault(a => a.IsChecked.Value);
            if (rb != null) _currentParameters.FilterMethod = rb.Tag.ToString();
            else return false;
            
            return true;
        }

        private void UpdateFields()
        {
            var Table = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);
            if (Table != null)
            {
                _filteringFields = (from f in Table.Fields
                    where f.DataType.ToLower() == "double"
                    select
                        new FilterTableData()
                        {
                            FieldName = f.Name,
                            IsFiltering = _currentParameters.Filtering.Contains(f.Name)
                        }).ToList();
                _includeInReport = (from f in Table.Fields
                    select
                        new FilterTableData()
                        {
                            FieldName = f.Name,
                            IsFiltering = _currentParameters.IncludeInReport.Contains(f.Name)
                        }).ToList();
                _layerFields = new[] {"Нет"}.Union(Table.Fields.Select(a => a.Name)).ToList();
            }
            FilterSelector.ItemsSource = _filteringFields;
            FilterSelector.UpdateLayout();
            IncludeInReportSelector.ItemsSource = _includeInReport;
            IncludeInReportSelector.UpdateLayout();

            GroupSelector.ItemsSource = _layerFields;
            GroupSelector.UpdateLayout();
            if (!IsNullOrEmpty(_currentParameters.LayerField) &&
                _layerFields.Contains(_currentParameters.LayerField))
                GroupSelector.SelectedItem = _currentParameters.LayerField;
            else if (_layerFields.Any()) GroupSelector.SelectedIndex = 0;
            GroupSelector.UpdateLayout();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (UpdateParameters()) ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void ModeButton_Checked(object sender, RoutedEventArgs e)
        {
            if (_currentParameters != null) _currentParameters.FilterMethod = ((RadioButton)sender).Tag.ToString();
        }

        private void CheckNone_Click(object sender, RoutedEventArgs e)
        {
            if (_filteringFields != null) foreach (var f in _filteringFields) f.IsFiltering = false;
        }

        private void CheckAll_Click(object sender, RoutedEventArgs e)
        {
            if (_filteringFields != null) foreach (var f in _filteringFields) f.IsFiltering = true;
        }
        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        private void SaveReport_Click(object sender, RoutedEventArgs e)
        {
            IncludeInReportSelector.IsEnabled = SaveReport.IsChecked.Value;
            OutliersTableName.IsEnabled = SaveReport.IsChecked.Value;
        }

        
        private void K_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!TryParse(K.Text, out _currentParameters.KZ))
            {
                K.Background = new SolidColorBrush(Colors.Red);
                K.Focus();
            }
            else
            {
                K.Background = new SolidColorBrush(Colors.Transparent);
                UpdatePercents();
            }
        }

        private void UpdatePercents()
        {
            Percent.Text = (Math.Round(100 * AQMath.NormalCDBetween(-_currentParameters.KZ, _currentParameters.KZ, 0, 1).Value, 5)).ToString(CultureInfo.InvariantCulture);
        }

        private void IQRK_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!TryParse(IQRK.Text, out _currentParameters.K))
            {
                IQRK.Background = new SolidColorBrush(Colors.Red);
                IQRK.Focus();
            }
            else
            {
                IQRK.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        private void K_GotFocus(object sender, RoutedEventArgs e)
        {
            sigma3.IsChecked = true;
        }

        private void IQRK_GotFocus(object sender, RoutedEventArgs e)
        {
            h15.IsChecked = true;
        }

        private void Percent_LostFocus(object sender, RoutedEventArgs e)
        {
            double p;
            if (!TryParse(Percent.Text, out p))
            {
                Percent.Background = new SolidColorBrush(Colors.Red);
                Percent.Focus();
            }
            else
            {
                Percent.Background = new SolidColorBrush(Colors.Transparent);
                K.Text = Math.Round(AQMath.NormalZbyP(1-(1-p/100)/2).Value, 5).ToString();
            }
        }
    }

    public class FilterTableData : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool _isfiltering;
        public bool IsFiltering
        {
            get
            {
                return _isfiltering;
            }
            set
            {
                _isfiltering = value;
                NotifyPropertyChanged("IsFiltering");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}