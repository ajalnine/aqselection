﻿using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_Outliers
{
    public class Parameters
    {
        public string TableName;
        
        public List<string> Filtering;
        public string FilterMethod;
        public string LayerField;
        public bool IsCyclic;
        public bool RemoveValueOnly;

        public double K;
        public double KZ;

        public bool GenerateReport;
        public string ReportName;
        public List<string> IncludeInReport;
    }
}
