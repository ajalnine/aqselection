﻿using System;
using System.Linq;
using System.Net;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SLCalc_LimitSize
{
    public class Parameters
    {
        public string TableName;
        public List<string> Grouping;
        public int Size;
        public bool DeleteSmallGroups;
        public string Mode;
        public string ZField;
    }
}
