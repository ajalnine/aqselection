﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_LimitSize
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<GroupTableData> GroupingFields;
        private DataItem SelectedTable;

        public Page()
        {
            InitializeComponent();
            CurrentParameters = new Parameters();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            CurrentParameters = new Parameters() { Grouping = new List<string>(), DeleteSmallGroups = true, Mode = "First", Size = 50, TableName = String.Empty };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            DeleteButton.IsChecked = CurrentParameters.DeleteSmallGroups;
            LeaveButton.IsChecked = !CurrentParameters.DeleteSmallGroups;
            var rb = ModePanel.Children.Cast<RadioButton>().Where(a=>a.Tag.ToString()==CurrentParameters.Mode).SingleOrDefault();
            if (rb!=null) rb.IsChecked = true;
            n.Text = CurrentParameters.Size.ToString();
            
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!DataSources.Select(a => a.Name).Contains(CurrentParameters.TableName))
            {
                UpdateDataSourceSelector(null);
                CurrentParameters.Grouping = new List<string>();
                return;
            }
            else UpdateDataSourceSelector(CurrentParameters.TableName);
            UpdateFields();
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }

        private bool UpdateDataSourceSelector(string SourceToSelect)
        {
            if (DataSources.Select(s => s.Name).Contains(SourceToSelect) && SourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = SourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                return true;
            }
            else if (DataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            return false;
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            UpdateFields();
        }

        private bool UpdateParameters()
        {
            CurrentParameters.Grouping = (from g in GroupingFields where g.IsGrouping select g.FieldName).ToList();
            CurrentParameters.DeleteSmallGroups = DeleteButton.IsChecked.Value;
            var rb = ModePanel.Children.Cast<RadioButton>().Where(a => a.IsChecked.Value).SingleOrDefault();
            if (rb != null) CurrentParameters.Mode = rb.Tag.ToString();
            int N = 50;
            CurrentParameters.ZField = (ZSelector.SelectedValue != null) ? ZSelector.SelectedValue.ToString() : String.Empty;
            if (!Int32.TryParse(n.Text, out N))
            {
                EnableExit(false);
                n.Background = new SolidColorBrush(Colors.Red);
                return false;
            }
            else
            {
                EnableExit(true);
                n.Background = new SolidColorBrush(Colors.White);
                CurrentParameters.Size = N;
                return true;
            }
        }

        private void UpdateFields()
        {
            var Table = DataSources.Where(a => a.Name == CurrentParameters.TableName).SingleOrDefault();
            if (Table != null)
            {
                GroupingFields = (from f in Table.Fields select new GroupTableData() { FieldName = f.Name, IsGrouping = CurrentParameters.Grouping.Contains(f.Name)}).ToList();
            }
            GroupSelector.ItemsSource = GroupingFields;
            GroupSelector.UpdateLayout();
            var Z = (from f in Table.Fields where f.DataType.ToLower()=="double" select f.Name ).ToList();
            ZButton.IsEnabled = Z.Count > 0;
            ZSelector.ItemsSource = Z;
            ZSelector.UpdateLayout();
            if (CurrentParameters.ZField != null && CurrentParameters.ZField != String.Empty) ZSelector.SelectedValue = CurrentParameters.ZField;
            else ZSelector.SelectedIndex = (Z.Count > 0)? 0 : -1;
            ZSelector.UpdateLayout();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (UpdateParameters()) this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }

        #endregion

        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode OVM) { }

        private void ModeButton_Checked(object sender, RoutedEventArgs e)
        {
            if (CurrentParameters!=null) CurrentParameters.Mode = ((RadioButton)sender).Tag.ToString();
        }

        private void n_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateParameters();
        }
    }
    public class GroupTableData : INotifyPropertyChanged
    {
        private string fieldname;
        public string FieldName
        {
            get
            {
                return fieldname;
            }
            set
            {
                fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool isgrouping;
        public bool IsGrouping
        {
            get
            {
                return isgrouping;
            }
            set
            {
                isgrouping = value;
                NotifyPropertyChanged("IsGrouping");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}