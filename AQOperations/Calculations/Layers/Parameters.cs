﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQReportingLibrary;

namespace SLCalc_Layers
{
    public class Parameters
    {
        public string TableName;
        public string LayerField;
        public string YField;
        public InteractiveOperations Operation;
        public object OptionalCommandArgument;
        public List<object> Layers;
        public ColorMode ColorMode;
        public Color Color;
    }

    public enum ColorMode
    {
        SetForCase, SetForValue, ResetFromCases, ResetFromValues
    }
}
