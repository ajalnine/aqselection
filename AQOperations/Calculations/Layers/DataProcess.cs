﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

namespace SLCalc_Layers
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private List<string> _deleted;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            
            var currentTable = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable != null)
            {
                _inputs.Add(currentTable);
                if (currentTable.Fields.All(a => a.Name != _currentParameters.LayerField))
                {
                    DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = null, Deleted = null });
                    return;
                }
                switch (_currentParameters.Operation)
                {
                    case InteractiveOperations.CutInNewTable:
                    case InteractiveOperations.CopyToNewTable:
                        var destination = DataItemHelper.GetCopy(currentTable);
                        destination.Name = _currentParameters.OptionalCommandArgument.ToString();
                        _outputs.Add(destination);
                        break;

                    case InteractiveOperations.SetColor:
                        _outputs.Add(currentTable);
                        if (_currentParameters.ColorMode == ColorMode.SetForValue)
                        {
                            if (!currentTable.Fields.Select(a => a.Name)
                                .Contains("#" + _currentParameters.LayerField + "_Цвет"))
                            {
                                currentTable.Fields.Add(new DataField { DataType = "String", Name = "#" + _currentParameters.LayerField + "_Цвет" });
                            }

                        }

                        if (_currentParameters.ColorMode == ColorMode.SetForCase)
                        {
                            foreach (var f in currentTable.Fields.ToList())
                            {
                                if (!currentTable.Fields.Select(a => a.Name)
                                    .Contains("#" + f.Name + "_Цвет"))
                                {
                                    currentTable.Fields.Add(new DataField { DataType = "String", Name = "#" + f.Name + "_Цвет" });
                                }
                            }
                        }
                        break;

                    case InteractiveOperations.SetLabel:
                        _outputs.Add(currentTable);
                        if (!currentTable.Fields.Select(a => a.Name)
                            .Contains("#" + _currentParameters.LayerField + "_Метка"))
                        {
                            currentTable.Fields.Add(new DataField { DataType = "String", Name = "#" + _currentParameters.LayerField + "_Метка" });
                        }
                        break;
                    default:
                        _outputs.Add(currentTable);
                        break;
                 }
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = _deleted });
        }
    }
}
