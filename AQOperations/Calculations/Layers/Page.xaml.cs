﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;
using AQChartLibrary;
using AQChartViewLibrary;
using AQChartViewLibrary.UI;
using AQControlsLibrary;
using AQReportingLibrary;

namespace SLCalc_Layers
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<DataItem> _availableData;
        private DataItem _selectedTable;
        private bool _layerValueError = false;
        Task _analize;
        DataLoader _data;
        private ObservableCollection<MarkedItem> _list = new ObservableCollection<MarkedItem>();

        public Page()
        {
            InitializeComponent();
            DeleteCase.Text = "Удалить наблюдения";
            DeleteValue.Text = "Удалить значения";
            CutInNewTable.Text = "Вырезать в новую таблицу";
            CopyToNewTable.Text = "Копировать в новую таблицу";
            SetColor.Text = "Установить цвета маркеров";
            SetLabel.Text = "Установить метки маркеров";
            CombineLayers.Text = "Замена нескольких значений одним";
            EmptyDataSet.Visibility = Visibility.Visible;
            ErrorTextBlock.Text = string.Empty;
            HideAllPanels();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            var table = availableData.FirstOrDefault(s => s.DataItemType == DataType.Selection);
            _currentParameters = new Parameters
            {
                Operation = InteractiveOperations.DeleteValue,
                ColorMode = ColorMode.SetForValue,
                TableName = table?.TableName,
                LayerField = table?.Fields?.FirstOrDefault()?.Name,
                YField = table?.Fields?.FirstOrDefault()?.Name,
                Layers = new List<object>(),
                OptionalCommandArgument = null
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            if (_data == null) return;
            _availableData = availableData;
            _dataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            SetInterfaceForCommand(_currentParameters.Operation);
            OptionsText.Text = _currentParameters.OptionalCommandArgument?.ToString() ?? string.Empty;
            ColorSelector.Color = _currentParameters.Color;
            ButtonsPanel.Children.OfType<TextImageRadioButtonBase>().Single(a => a.Name == _currentParameters.Operation.ToString()).IsChecked = true;

            switch (_currentParameters.ColorMode)
            {
                case ColorMode.SetForValue:
                    SetColorForValue.IsChecked = true;
                    break;
                case ColorMode.SetForCase:
                    SetColorForRow.IsChecked = true;
                    break;
                case ColorMode.ResetFromCases:
                    SetNoColorForRow.IsChecked = true;
                    break;
                case ColorMode.ResetFromValues:
                    SetNoColor.IsChecked = true;
                    break;

            }
            if (!_dataSources.Select(a => a.Name).Contains(_currentParameters.TableName)) UpdateDataSourceSelector(null);
            else UpdateDataSourceSelector(_currentParameters.TableName);
        }

        private bool UpdateDataSourceSelector(string sourceToSelect)
        {
            if (_dataSources.Select(s => s.Name).Contains(sourceToSelect) && sourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = sourceToSelect;
                return true;
            }
            else if (_dataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            return false;
        }
        
        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }
        
        private void UpdateFieldsSource()
        {
            DataItem currentTable = _dataSources.Where(a => a != null).SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable == null) return;
            var fields = currentTable.Fields.Where(a => a != null).Select(s => s.Name).ToList();
            LayersFieldSelector.ItemsSource = fields;
            LayersFieldSelector.UpdateLayout();
            YFieldSelector.ItemsSource = fields;
            YFieldSelector.UpdateLayout();
            if (fields.Count > 0)
            {
                if (_currentParameters.YField != null && fields.Contains(_currentParameters.YField))
                {
                    YFieldSelector.SelectedItem = _currentParameters.YField;
                }
                else YFieldSelector.SelectedIndex = 0;

                YFieldSelector.UpdateLayout();

                if (_currentParameters.LayerField != null && fields.Contains(_currentParameters.LayerField))
                {
                    LayersFieldSelector.SelectedItem = _currentParameters.LayerField;
                }
                else LayersFieldSelector.SelectedIndex = 0;
                
                LayersFieldSelector.UpdateLayout();
            }
        }

        private void UIToParameters()
        {
            _currentParameters.TableName = DataSourceSelector?.SelectedItem?.ToString();
            _currentParameters.LayerField = LayersFieldSelector?.SelectedItem?.ToString();
            _currentParameters.YField = YFieldSelector?.SelectedItem?.ToString();
            _currentParameters.Layers = _list.Where(a=>a.IsSelected).Select(a => a.Source).ToList();
            ColorModeToParameters();
            var commandName = ButtonsPanel.Children.OfType<TextImageRadioButtonBase>().Single(a => a.IsChecked !=null && a.IsChecked.Value).Name;
            _currentParameters.Operation = (InteractiveOperations)Enum.Parse(typeof(InteractiveOperations), commandName, true);
            _currentParameters.OptionalCommandArgument = OptionsText.Text;
            _currentParameters.Color = ColorSelector.Color;
            _layerValueError = false;
            if (_currentParameters.Operation == InteractiveOperations.CombineLayers) SetNewLayerValue();
            else _currentParameters.OptionalCommandArgument = OptionsText.Text;
        }

        private void SetNewLayerValue()
        {
            var dt = _selectedTable.Fields.Single(a => a.Name == _currentParameters.LayerField).DataType.Split('.')
                .LastOrDefault()?.ToLower();
            _layerValueError = true;
            if (string.IsNullOrEmpty(OptionsText.Text))
            {
                _layerValueError = false;
                return;
            }

            switch (dt)
            {
                case "double":
                    double v;
                    if (double.TryParse(OptionsText.Text, out v))
                    {
                        _layerValueError = false;
                        _currentParameters.OptionalCommandArgument = (double?) v;
                    }
                    else
                    {
                        OptionsText.Focus();
                    }

                    break;

                case "bool":
                    bool v4;
                    if (bool.TryParse(OptionsText.Text, out v4))
                    {
                        _layerValueError = false;
                        _currentParameters.OptionalCommandArgument = (bool?) v4;
                    }
                    else
                    {
                        OptionsText.Focus();
                    }

                    break;

                case "datetime":
                    DateTime v2;
                    if (DateTime.TryParse(OptionsText.Text, out v2))
                    {
                        _layerValueError = false;
                        _currentParameters.OptionalCommandArgument = (DateTime?) v2;
                    }
                    else
                    {
                        OptionsText.Focus();
                    }

                    break;

                case "timespan":
                    TimeSpan v3;
                    if (TimeSpan.TryParse(OptionsText.Text, out v3))
                    {
                        _layerValueError = false;
                        _currentParameters.OptionalCommandArgument = (TimeSpan?) v3;
                    }
                    else
                    {
                        OptionsText.Focus();
                    }

                    break;

                default:
                    _layerValueError = false;
                    _currentParameters.OptionalCommandArgument = OptionsText.Text;
                    break;
            }
        }

        private void ColorModeToParameters()
        {
            if (SetColorForValue != null && SetColorForValue.IsChecked != null && SetColorForValue.IsChecked.Value)
                _currentParameters.ColorMode = ColorMode.SetForValue;
            if (SetColorForRow != null && SetColorForRow.IsChecked != null && SetColorForRow.IsChecked.Value)
                _currentParameters.ColorMode = ColorMode.SetForCase;
            if (SetNoColor != null && SetNoColor.IsChecked != null && SetNoColor.IsChecked.Value)
                _currentParameters.ColorMode = ColorMode.ResetFromValues;
            if (SetNoColorForRow != null && SetNoColorForRow.IsChecked != null && SetNoColorForRow.IsChecked.Value)
                _currentParameters.ColorMode = ColorMode.ResetFromCases;
        }

        private void Command_Click(object sender, RoutedEventArgs e)
        {
            var b = sender as TextImageRadioButtonBase;
            if (b == null) return;
            var command = (InteractiveOperations)Enum.Parse(typeof(InteractiveOperations), b.Name, true);
            HideAllPanels();
            SetInterfaceForCommand(command);
        }

        private void SetInterfaceForCommand(InteractiveOperations command)
        {
            switch (command)
            {
                case InteractiveOperations.CopyToNewTable:
                    OptionsPanel.Visibility = Visibility.Visible;
                    OptionsText.Tip = "Имя таблицы";
                    DestinationFieldPanel.Visibility = Visibility.Collapsed;
                    Grid.SetRow(OptionsPanel, 4);
                    break;
                case InteractiveOperations.CutInNewTable:
                    OptionsPanel.Visibility = Visibility.Visible;
                    OptionsText.Tip = "Имя таблицы";
                    DestinationFieldPanel.Visibility = Visibility.Collapsed;
                    Grid.SetRow(OptionsPanel, 3);
                    break;
                case InteractiveOperations.DeleteCase:
                    OptionsText.Tip = string.Empty;
                    DestinationFieldPanel.Visibility = Visibility.Collapsed;
                    break;
                case InteractiveOperations.DeleteValue:
                    OptionsText.Tip = string.Empty;
                    DestinationFieldPanel.Visibility = Visibility.Visible;
                    break;
                case InteractiveOperations.SetColor:
                    OptionsText.Tip = string.Empty;
                    ColorOptionsPanel.Visibility = Visibility.Visible;
                    SetDestinationPanelForColorMode();
                    break;
                case InteractiveOperations.SetLabel:
                    OptionsPanel.Visibility = Visibility.Visible;
                    OptionsText.Tip = "Метка";
                    Grid.SetRow(OptionsPanel, 5);
                    DestinationFieldPanel.Visibility = Visibility.Visible;
                    break;

                case InteractiveOperations.CombineLayers:
                    OptionsPanel.Visibility = Visibility.Visible;
                    OptionsText.Tip = "Значение";
                    Grid.SetRow(OptionsPanel, 6);
                    DestinationFieldPanel.Visibility = Visibility.Visible;
                    break;
            }
        }

        private void HideAllPanels()
        {
            OptionsPanel.Visibility = Visibility.Collapsed;
            ColorOptionsPanel.Visibility = Visibility.Collapsed;
        }
        
        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _selectedTable = _availableData.SingleOrDefault(a => a.Name == _currentParameters.TableName);
            UpdateFieldsSource();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            if (_layerValueError)
            {
                ErrorTextBlock.Text = "Значение слоя не соответствует типу поля слоёв";
                return;
            }

            if (_currentParameters.Layers.Count == 0)
            {
                ErrorTextBlock.Text = "Слои не выбраны";
                return;
            }

            if (_currentParameters.Operation == InteractiveOperations.CutInNewTable ||
                _currentParameters.Operation == InteractiveOperations.CopyToNewTable)
            {
                if (_availableData.Select(a => a.Name)
                    .Contains(_currentParameters.OptionalCommandArgument.ToString().Trim()))
                {
                    ErrorTextBlock.Text = "Таблица уже существует";
                    return;
                }
            }
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }
        #endregion

        #region Автозаполнение категорий

        public void DataRequestReadyCallback(string fileName)
        {
            _analize.SetState(0, TaskState.Ready);
            _analize.SetState(1, TaskState.Processing);
            _data = new DataLoader();
            _data.DataProcessed += Data_DataProcessed;
            _data.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            _analize.SetState(0, TaskState.Ready);
            _analize.SetState(1, TaskState.Ready);
            _analize.SetState(2, TaskState.Processing);
            this.Dispatcher.BeginInvoke(delegate()
            {
                ParametersToUI(_availableData);
            });
        }

        private void SetLayerValues(SLDataTable sldt)
        {
            var z = sldt.GetLayerList(_currentParameters.LayerField).OrderBy(a=>a?.ToString() ?? "");
            _list = _currentParameters.Layers != null && _currentParameters.Layers.Count > 0 && LayersFieldSelector.SelectedItem?.ToString() == _currentParameters.LayerField
                    ? new ObservableCollection<MarkedItem>(z.Select(a => new MarkedItem
                    {
                        Field = string.IsNullOrEmpty(a?.ToString()) ? " Нет значения" : a.ToString(),
                        IsSelected = _currentParameters.Layers.Contains(a),
                        Source = a
                    }))
                    : new ObservableCollection<MarkedItem>(z.Select(a => new MarkedItem
                    {
                        Field = string.IsNullOrEmpty(a?.ToString()) ? " Нет значения" : a.ToString(),
                        IsSelected = !string.IsNullOrEmpty(a?.ToString()), Source = a
                    }));
            LayerSelector.ItemsSource = _list;
            _analize.SetState(2, TaskState.Ready);
            UIToParameters();
        }

        private void GetCurrentLayers_Click(object sender, RoutedEventArgs e)
        {
            if (DataRequest != null)
            {
                _analize = new Task(new string[] { "Исполнение расчета", "Прием данных", "Анализ данных" }, "Автоопределение параметров", TaskPanel);
                _analize.SetState(0, TaskState.Processing);
                EmptyDataSet.Visibility = Visibility.Collapsed;
                DataRequest.Invoke(this, new EventArgs());
            }
        }

        #endregion
        public void StateChanged(OperationViewMode ovm) { }

        private void LayersFieldSelectorSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_data == null) return;
            LayersFieldSelector.UpdateLayout();
            if (_currentParameters.LayerField != LayersFieldSelector.SelectedItem?.ToString())
            {
                _currentParameters.Layers = new List<object>();
                _currentParameters.LayerField = LayersFieldSelector.SelectedItem?.ToString();
            }
            SetLayerValues(_data.GetTable(_currentParameters.TableName));
        }
        
        private void ClearSelection_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var z in _list)
            {
                z.IsSelected = false;
            }
        }

        private void SelectAll_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var z in _list)
            {
                z.IsSelected = true;
            }
        }

        private void YFieldSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void ColorModeChanged_Click(object sender, RoutedEventArgs e)
        {
            ColorModeToParameters();
            SetDestinationPanelForColorMode();
        }

        private void SetDestinationPanelForColorMode()
        {
            switch (_currentParameters.ColorMode)
            {
                case ColorMode.SetForValue:
                    DestinationFieldPanel.Visibility = Visibility.Visible;
                    break;
                case ColorMode.SetForCase:
                    DestinationFieldPanel.Visibility = Visibility.Collapsed;
                    break;
                case ColorMode.ResetFromCases:
                    DestinationFieldPanel.Visibility = Visibility.Collapsed;
                    break;
                case ColorMode.ResetFromValues:
                    DestinationFieldPanel.Visibility = Visibility.Visible;
                    break;
            }
        }
    }
}