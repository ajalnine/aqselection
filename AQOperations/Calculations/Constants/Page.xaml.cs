﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_Constants
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private int _editingIndex = -1;
        private bool _datePickerHooked;
        private readonly Dictionary<String, String> _constantTypes = new Dictionary<String, String>
        {
            {"Double", "123"},
            {"String", "Abc"},
            {"TimeSpan", "000.06:45:00"},
            {"DateTime", "25.12.2015"},
            {"Boolean", "0/1"}
        };

        private readonly Dictionary<bool, String> _boolValues = new Dictionary<bool, String>
        {
            {true, "1"},
            {false, "0"}
        };

        public static readonly DependencyProperty NewConstantProperty =
        DependencyProperty.Register("NewConstant", typeof(ConstantDescription), typeof(Page), 
        new PropertyMetadata(null, null));

        public ConstantDescription NewConstant
        {
            get { return (ConstantDescription)GetValue(NewConstantProperty); }
            set { SetValue(NewConstantProperty, value); }
        }

        public static readonly DependencyProperty DoubleValueProperty =
        DependencyProperty.Register("DoubleValue", typeof(Double), typeof(Page), new PropertyMetadata(0.0d, null));

        public Double DoubleValue
        {
            get { return (Double)GetValue(DoubleValueProperty); }
            set { SetValue(DoubleValueProperty, value); }
        }

        public static readonly DependencyProperty StringValueProperty =
       DependencyProperty.Register("StringValue", typeof(String), typeof(Page), new PropertyMetadata(String.Empty, null));

        public String StringValue
        {
            get { return (String)GetValue(StringValueProperty); }
            set { SetValue(StringValueProperty, value); }
        }

        public static readonly DependencyProperty TimeSpanValueProperty =
       DependencyProperty.Register("TimeSpanValue", typeof(TimeSpan), typeof(Page), new PropertyMetadata(TimeSpan.FromHours(0), null));

        public TimeSpan TimeSpanValue
        {
            get { return (TimeSpan)GetValue(TimeSpanValueProperty); }
            set { SetValue(TimeSpanValueProperty, value); }
        }

        public static readonly DependencyProperty DateTimeValueProperty =
       DependencyProperty.Register("DateTimeValue", typeof(DateTime), typeof(Page), new PropertyMetadata(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day) , null));

        public DateTime DateTimeValue
        {
            get { return (DateTime)GetValue(DateTimeValueProperty); }
            set { SetValue(DateTimeValueProperty, value); }
        }

        public static readonly DependencyProperty BooleanValueProperty =
       DependencyProperty.Register("BooleanValue", typeof(Boolean), typeof(Page), new PropertyMetadata(true, null));

        private DataItem _variables;

        public Boolean BooleanValue
        {
            get { return (Boolean)GetValue(BooleanValueProperty); }
            set { SetValue(BooleanValueProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            ConstantType.ItemsSource = _constantTypes;
            BooleanComboBox.ItemsSource = _boolValues;
            SetupNewConstant();
        }

        private void SetupNewConstant()
        {
            NewConstant = new ConstantDescription
            {
                VariableType = "String",
                VariableName = "A",
                Value = String.Empty
            };
            NewConstant.PropertyChanged += NewConstant_PropertyChanged;
            NewConstantPanel.DataContext = NewConstant;
        }

        void NewConstant_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResult(); 
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _variables = availableData.SingleOrDefault(s => s.Name == "Переменные"); 
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { Constants = new ObservableCollection<ConstantDescription>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _variables = availableData.SingleOrDefault(s => s.Name == "Переменные"); 
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {
            ConstantsListBox.ItemsSource = _currentParameters.Constants;
            CheckResult();
        }

        private void UIToParameters()
        {
            
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            var enabled = true;
            var variable = NewConstant.VariableName;
            if (string.IsNullOrEmpty(variable)) enabled = false;
            if (_variables.Fields.Select(a => a.Name).Contains(variable)) enabled = false;
            if (_currentParameters.Constants.Select(a => a.VariableName).Contains(variable)) enabled = false;
            AddRuleButton.IsEnabled = enabled; 
            
            EnableExit(_currentParameters.Constants.Count() != 0);
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        public void DataRequestReadyCallback(string fileName) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as ConstantDescription;
            _currentParameters.Constants.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
        }
        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1) StoreRule();
            var toEdit = ((TextImageButtonBase)sender).Tag as ConstantDescription;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.Constants.IndexOf(toEdit);
            _currentParameters.Constants.Remove(toEdit);

            NewConstant = new ConstantDescription
            {
                VariableType = toEdit.VariableType,
                VariableName = toEdit.VariableName,
                Value = toEdit.Value
            };

            switch (NewConstant.VariableType)
            {
                case "Double":
                    DoubleValue = (Double)toEdit.Value;
                    break;
                case "String":
                    StringValue = (String)toEdit.Value;
                    break;
                case "TimeSpan":
                    TimeSpanValue = new TimeSpan(((DateTime)toEdit.Value).Ticks);
                    break;
                case "DateTime":
                    DateTimeValue = (DateTime)toEdit.Value;
                    break;
                case "Boolean":
                    BooleanValue = (Boolean)toEdit.Value;
                    break;
            }
            
            NewConstantPanel.DataContext = NewConstant;
            CheckResult();
        }

        public void StateChanged(OperationViewMode ovm){}

        private void MainDatePicker_GotFocus(object sender, RoutedEventArgs e)
        {
            if (_datePickerHooked) return;
            var c = VisualTreeHelper.GetChildrenCount(ValueDatePicker);
            if (c == 0) return;
            var a = VisualTreeHelper.GetChild(ValueDatePicker, 0);
            var b = VisualTreeHelper.GetChild(a, 0);
            var ft = b as DatePickerTextBox;
            if (ft != null) ft.KeyUp += ft_KeyUp;
            _datePickerHooked = true;
        }

        private void ft_KeyUp(object sender, KeyEventArgs e)
        {
            var d = sender as DatePickerTextBox;
            if (d != null)
            {
                d.UpdateLayout();
                var s = d.Text;
                DateTime f;
                if (DateTime.TryParse(s, out f))
                {
                    ValueDatePicker.SelectedDate = f;
                }
            }
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            StoreRule();
            CheckResult();
        }

        private void StoreRule()
        {
            var binding = BooleanComboBox.GetBindingExpression(Selector.SelectedValueProperty);
            if (null != binding ) binding.UpdateSource();
            var binding2 = DoubleTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding2 && !string.IsNullOrEmpty(DoubleTextBox.Text)) binding2.UpdateSource();
            var binding3 = TimeSpanTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding3 && !string.IsNullOrEmpty(TimeSpanTextBox.Text)) binding3.UpdateSource();
            var binding4 = StringTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding4 && !string.IsNullOrEmpty(StringTextBox.Text)) binding4.UpdateSource();
            var binding52 = ValueDatePicker.GetBindingExpression(DatePicker.TextProperty);
            if (null != binding52) binding52.UpdateSource();
            var binding5 = ValueDatePicker.GetBindingExpression(DatePicker.SelectedDateProperty);
            if (null != binding5) binding5.UpdateSource();
            var binding6 = VariableNameTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding6 && !string.IsNullOrEmpty(VariableNameTextBox.Text)) binding6.UpdateSource();
            
            
            object value = null;
            switch (NewConstant.VariableType)
            {
                case "Double":
                    value = DoubleValue;
                    break;
                case "String":
                    value = StringValue;
                    break;
                case "TimeSpan":
                    value = new DateTime(TimeSpanValue.Ticks);
                    break;
                case "DateTime":
                    value = DateTimeValue;
                    break;
                case "Boolean":
                    value = BooleanValue;
                    break;
            }

            var constantDescription = new ConstantDescription
            {
               VariableName = NewConstant.VariableName,
               VariableType = NewConstant.VariableType,
               Value = value
            };
            CheckResult();
            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.Constants.Add(constantDescription);
                    break;
                default:
                    _currentParameters.Constants.Insert(_editingIndex, constantDescription);
                    _editingIndex = -1;
                    break;
            }
        }
        
        private void ConstantType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dataType = ((KeyValuePair<string, string>)ConstantType.SelectedItem).Key;
            foreach (var uiElement in ValuePanel.Children)
            {
                var control = (Control) uiElement;
                control.Visibility = control.Tag.ToString() == dataType ? Visibility.Visible : Visibility.Collapsed;
            }
        }
    }

    public class TypedValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (ConstantDescription)value;
            var v = string.Empty;
            switch (p.VariableType)
            {
                case "Double":
                    v = p.Value.ToString();
                    break;
                case "String":
                    v = p.Value.ToString();
                    break;
                case "TimeSpan":
                    v = new TimeSpan(((DateTime)p.Value).Ticks).ToString();
                    break;
                case "DateTime":
                    v = ((DateTime)p.Value).ToString("dd.MM.yyyy");
                    break;
                case "Boolean":
                    v = ((bool)p.Value) ? "1":"0";
                    break;
            }
            var result = p.VariableName + "=" + v;
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}