﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_Constants
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var variableTable = availableData.Single(n => n.Name == "Переменные");
                
            var output = new DataItem
            {
                DataItemType = variableTable.DataItemType,
                Name = variableTable.Name,
                TableName = variableTable.Name,
                Fields = new List<DataField>()
            };
            output.Fields.AddRange(_currentParameters.Constants.Select(a=>new DataField { Name = a.VariableName, DataType = a.VariableType }));
            _inputs = new List<DataItem>();
            _outputs = new List<DataItem> {output};
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
