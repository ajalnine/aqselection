﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using AQControlsLibrary.Annotations;

namespace SLCalc_Constants
{
    public class Parameters
    {
        public ObservableCollection<ConstantDescription> Constants;
    }

    public class ConstantDescription : INotifyPropertyChanged
    {

        private string _variableName;

        public string VariableName
        {
            get { return _variableName; }
            set
            {
                _variableName = value;
                if (PropertyChanged!=null)OnPropertyChanged("VariableName");
            }
        }

        private string _variableType;
        public string VariableType
        {
            get { return _variableType; }
            set
            {
                _variableType = value;
                if (PropertyChanged != null) OnPropertyChanged("VariableType");
            }
        }
        public object Value { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
