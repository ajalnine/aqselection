﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_Aggregates
{
    public class Parameters
    {
        public string TableName;
        public string InsertAfterField;
        public ObservableCollection<AggregateFieldDescription> AggregateFields;
        public List<String> GroupingFields;
    }
    public class AggregateFieldDescription : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private string _expressionType;
        public string ExpressionType
        {
            get { return _expressionType; }
            set
            {
                _expressionType = value;
                NotifyPropertyChanged("ExpressionType");
            }
        }

        private string _expression;
        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
                NotifyPropertyChanged("Expression");
            }
        }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                NotifyPropertyChanged("Code");
            }
        }
        
       public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class GroupTableData : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool _isgroup;
        public bool IsGroup
        {
            get
            {
                return _isgroup;
            }
            set
            {
                _isgroup = value;
                NotifyPropertyChanged("IsGroup");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
