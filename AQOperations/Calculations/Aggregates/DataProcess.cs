﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_Aggregates
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs = new List<DataItem>();
        private readonly List<DataItem> _outputs = new List<DataItem>();
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var selectedTable = availableData.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            var resultingTokens = new List<Token>();

            if (selectedTable == null)
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }

            var output = new DataItem
            {
                TableName = selectedTable.TableName,
                DataItemType = selectedTable.DataItemType,
                Name = selectedTable.Name,
                Fields = new List<DataField>()
            };
            output.Fields.AddRange(selectedTable.Fields);
            var insertionIndex =
                selectedTable.Fields.IndexOf(
                    selectedTable.Fields.SingleOrDefault(a => a.Name == _currentParameters.InsertAfterField)) + 1;
            foreach (var rule in _currentParameters.AggregateFields)
            {
                var data = availableData.Except(new[] { selectedTable }).Union(new[] { output });
                resultingTokens.InsertRange(0, Tokens.ParseTokens(rule.Expression, data.ToList(), output));
                output.Fields.Insert(insertionIndex++, new DataField { Name = rule.FieldName, DataType = rule.ExpressionType });
            }

            if (Tokens.CheckTokens(resultingTokens))
            {
                _outputs.Add(output);
                var usedTableNames = UsedDataItemsHelper.GetUsedTables(resultingTokens, selectedTable).Select(a=>a.Name).Distinct().ToList();
                _inputs = availableData.Where(a=>usedTableNames.Contains(a.Name)).ToList();
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
