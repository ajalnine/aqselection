﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_Aggregates
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private readonly CalculationService _cs;
        private List<DataItem> _dataSources;
        private bool _ischecked;
        private Parameters _currentParameters;
        private List<DataItem> _availableData;
        private DataItem _selectedTable;
        private int _editingIndex = -1;
        private DataItem _availableFieldsTable;
        private ObservableCollection<GroupTableData> _groupData = new ObservableCollection<GroupTableData>();
        private readonly Dictionary<String, String> _expressionFieldTypes = new Dictionary<String, String>
        {
            {"Double", "123"},
            {"String", "Abc"},
            {"TimeSpan", "06:45:00"},
            {"DateTime", "25.12.2015"},
            {"Boolean", "0/1"}
        };

        public static readonly DependencyProperty NewAggregateFieldProperty =
        DependencyProperty.Register("NewAggregateField", typeof(AggregateFieldDescription), typeof(Page), new PropertyMetadata(null, null));

        public AggregateFieldDescription NewAggregateField
        {
            get { return (AggregateFieldDescription)GetValue(NewAggregateFieldProperty); }
            set { SetValue(NewAggregateFieldProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
            ExpressionFieldType.ItemsSource = _expressionFieldTypes;
            GetNewExpressionField();
        }

        private void GetNewExpressionField()
        {
            NewAggregateField = new AggregateFieldDescription
            {
                Expression = string.Empty,
                Code = string.Empty,
                FieldName = "Sum",
                ExpressionType = "Double"
            };
            NewAggregateField.PropertyChanged += NewVariable_PropertyChanged;
            NewVariablePanel.DataContext = NewAggregateField;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { AggregateFields = new ObservableCollection<AggregateFieldDescription>(), GroupingFields = new List<string>()};
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }

            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (newSource != null)
            {
                var fields = newSource.Fields.Where(a => !a.Name.StartsWith("#")).Select(a => a.Name).ToList();
                AfterFieldSelector.ItemsSource = fields;
                AfterFieldSelector.UpdateLayout();
                AfterFieldSelector.SelectedItem = string.IsNullOrEmpty(_currentParameters.InsertAfterField) 
                    ? AfterFieldSelector.ItemsSource.OfType<string>().LastOrDefault() 
                    : _currentParameters.InsertAfterField;
                AfterFieldSelector.UpdateLayout();

                _groupData = new ObservableCollection<GroupTableData>();
                foreach (var field in fields)
                {
                    var g = new GroupTableData
                    {
                        FieldName = field,
                        IsGroup = _currentParameters.GroupingFields.Contains(field)
                    };
                    g.PropertyChanged += g_PropertyChanged;
                    _groupData.Add(g);
                }
                GroupSelector.ItemsSource = _groupData;
            }
            ExpressionFieldsListBox.ItemsSource = _currentParameters.AggregateFields;

            SetupExpressionFieldsSources();
            MainExpressionBuilder.SetExpressionEditor(NewAggregateField.Expression);
            MainExpressionBuilder.SetCodeEditor(NewAggregateField.Code);
            MainExpressionBuilder.StartHelp();
            CheckResult();
        }

        void g_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void SetupExpressionFieldsSources()
        {
            _selectedTable = _availableData.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            var count = _editingIndex == -1 ? _currentParameters.AggregateFields.Count : _editingIndex;
            var existingFields = _currentParameters.AggregateFields.Select(a => new DataField { Name = a.FieldName, DataType = a.ExpressionType }).Take(count).ToList();
            if (_selectedTable == null) return;
            _availableFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = new List<DataField>()
            };
            _availableFieldsTable.Fields.AddRange(_selectedTable.Fields.Union(existingFields).ToList());
            var data = _availableData.ToList();
            data.Remove(_selectedTable);
            data.Add(_availableFieldsTable);
            ExpressionFieldsListBox.ItemsSource = _currentParameters.AggregateFields;
            MainExpressionBuilder.SetDataItems(_availableFieldsTable, data, null);
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null)
                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            if (AfterFieldSelector != null && AfterFieldSelector.SelectedItem != null)
                _currentParameters.InsertAfterField = AfterFieldSelector.SelectedItem.ToString();
            _currentParameters.GroupingFields = _groupData.Where(a => a.IsGroup).Select(a => a.FieldName).ToList();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            SetError(string.Empty);
            UIToParameters();
            var enabled = true;
            var fieldName = NewAggregateField.FieldName;
            if (string.IsNullOrEmpty(fieldName))
            {
                enabled = false;
                SetError("Пустое имя поля");
            }
            if (_selectedTable.Fields.Select(a => a.Name).Contains(fieldName))
            {
                enabled = false;
                SetError("Поле с указанным именем уже существует в таблице");
            }
            if (_currentParameters.AggregateFields.Select(a => a.FieldName).Contains(fieldName))
            {
                enabled = false;
            }
            if (string.IsNullOrEmpty(MainExpressionBuilder.GetExpression())) enabled = false;
            var exitEnabled = CheckFieldOrderAndExistance();

            AddRuleButton.IsEnabled = enabled && _ischecked;

            EnableExit(_currentParameters.AggregateFields.Count() != 0&& _currentParameters.GroupingFields.Count!=0 && exitEnabled);
        }

        private bool CheckFieldOrderAndExistance()
        {
            var existingFieldsList = _selectedTable.Fields.Select(a => new DataField { Name = a.Name, DataType = a.DataType }).ToList();
            var existingFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = existingFieldsList
            };
            var totalExistingFieldsList =
                _selectedTable.Fields.Select(a => new DataField { Name = a.Name, DataType = a.DataType }).ToList();
            var totalExistingFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = totalExistingFieldsList
            };
            foreach (var v in _currentParameters.AggregateFields)
            {
                totalExistingFieldsTable.Fields.Add(new DataField { Name = v.FieldName, DataType = v.ExpressionType });
            }

            var exitEnabled = true;
            foreach (var v in _currentParameters.AggregateFields)
            {
                if (Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] { _selectedTable }).Union(new[] { totalExistingFieldsTable }).ToList(),
                    totalExistingFieldsTable).Any(a => a.Type == TokenType.Unknown))
                {
                    exitEnabled = false;
                    SetError("Одно из выражений содержит неизвестные элементы");
                    break;
                }

                var usedFields = Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] { _selectedTable }).Union(new[] { totalExistingFieldsTable }).ToList(),
                    totalExistingFieldsTable).Where(a => a.Type == TokenType.CurrentField).ToList();
                foreach (var u in usedFields)
                {
                    if (existingFieldsList.Select(a => a.Name).Contains(u.UsedField.Field.Name)) continue;
                    exitEnabled = false;
                    SetError("Одно из выражений ссылается на поле таблицы, ранее не объявленное");
                    break;
                }
                if (!exitEnabled) break;
                existingFieldsTable.Fields.Add(new DataField { Name = v.FieldName, DataType = v.ExpressionType });
            }
            return exitEnabled;
        }

        private void SetError(string error)
        {
            ErrorTextBlock.Text = error;
            ErrorTextBlock.Visibility = string.IsNullOrEmpty(error) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }
        public void DataRequestReadyCallback(string fileName) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as AggregateFieldDescription;
            _currentParameters.AggregateFields.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
            SetupExpressionFieldsSources();
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1) StoreExpressionField();
            var toEdit = ((TextImageButtonBase)sender).Tag as AggregateFieldDescription;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.AggregateFields.IndexOf(toEdit);
            _currentParameters.AggregateFields.Remove(toEdit);
            NewAggregateField = toEdit;

            SetupExpressionFieldsSources();
            MainExpressionBuilder.SetExpressionEditor(NewAggregateField.Expression);
            MainExpressionBuilder.SetCodeEditor(NewAggregateField.Code);
            NewVariablePanel.DataContext = NewAggregateField;
            NewAggregateField.PropertyChanged += NewVariable_PropertyChanged;
            CheckResult();
        }

        void NewVariable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            AddRuleButton.IsEnabled = false;
            _ischecked = false;
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _ischecked = false;
            _cs.BeginCheckExpression(e.CSharpText
                , _availableData.Except(new[] { _selectedTable }).Union(new[] { _availableFieldsTable }).ToList()
                , _selectedTable.Name, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success)
                {
                    _ischecked = true;
                    CheckResult();
                    if (sr.Message != "null") NewAggregateField.ExpressionType = sr.Message;
                }
                else
                {
                    AddRuleButton.IsEnabled = false;
                    MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            StoreExpressionField();
            CheckResult();
            ParametersToUI();
        }

        private void StoreExpressionField()
        {
            var binding1 = ExpressionFieldTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding1 && !string.IsNullOrEmpty(ExpressionFieldTextBox.Text)) binding1.UpdateSource();
            var binding2 = ExpressionFieldType.GetBindingExpression(Selector.SelectedValueProperty);
            if (null != binding2) binding2.UpdateSource();

            var expressionFieldDescription = new AggregateFieldDescription
            {
                Expression = MainExpressionBuilder.GetExpression(),
                Code = MainExpressionBuilder.GetCode(),
                FieldName = NewAggregateField.FieldName,
                ExpressionType = NewAggregateField.ExpressionType
            };
            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.AggregateFields.Insert(_currentParameters.AggregateFields.Count, expressionFieldDescription);
                    break;
                default:
                    _currentParameters.AggregateFields.Insert(_editingIndex, expressionFieldDescription);
                    _editingIndex = -1;
                    break;
            }
        }

        private void UpExpressionField_Click(object sender, RoutedEventArgs e)
        {
            if (ExpressionFieldsListBox.SelectedItem == null) return;
            var selected = ExpressionFieldsListBox.SelectedItem as AggregateFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : ExpressionFieldsListBox.Items.Count - 1;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void DownExpressionField_Click(object sender, RoutedEventArgs e)
        {
            if (ExpressionFieldsListBox.SelectedItem == null) return;
            var selected = ExpressionFieldsListBox.SelectedItem as AggregateFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < ExpressionFieldsListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void TopExpressionField_Click(object sender, RoutedEventArgs e)
        {
            if (ExpressionFieldsListBox.SelectedItem == null) return;
            var selected = ExpressionFieldsListBox.SelectedItem as AggregateFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void BottomExpressionField_Click(object sender, RoutedEventArgs e)
        {
            if (ExpressionFieldsListBox.SelectedItem == null) return;
            var selected = ExpressionFieldsListBox.SelectedItem as AggregateFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = ExpressionFieldsListBox.Items.Count - 1;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void MoveExpressionField(int selectedIndex, int nextIndex)
        {
            var newItem = ExpressionFieldsListBox.SelectedItem as AggregateFieldDescription;
            _currentParameters.AggregateFields.RemoveAt(selectedIndex);
            _currentParameters.AggregateFields.Insert(nextIndex, newItem);
            ExpressionFieldsListBox.SelectedItem = newItem;
            ExpressionFieldsListBox.ScrollIntoView(ExpressionFieldsListBox.SelectedItem);
            ExpressionFieldsListBox.UpdateLayout();
            CheckResult();
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null) return;

            MainExpressionBuilder.SetDataItems(_dataSources.Where(a => a != null).SingleOrDefault(s => s.Name == _currentParameters.TableName), _availableData, null);
            if (DataSourceSelector.SelectedItem != null)

                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.AggregateFields.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            AddRuleButton.IsEnabled = false;
            ParametersToUI();
        }
    }
    public class VariableConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (AggregateFieldDescription)value;
            var result = string.Format("[{0}] = {1}", p.FieldName, p.Expression);
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}