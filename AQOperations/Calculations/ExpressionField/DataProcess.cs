﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_ExpressionField
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;

        public void DataProcess(List<DataItem> AvailableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));

            DataItem Current = AvailableData.Where(a => a != null).Where(n => n.Name == CurrentParameters.TableName).SingleOrDefault();
            if (Current != null)
            {
                List<Token> ResultingTokens = Tokens.ParseTokens(CurrentParameters.Expression, AvailableData, Current);
                if (Tokens.CheckTokens(ResultingTokens))
                {
                    DataField NewField = new DataField() { Name = CurrentParameters.FieldName, DataType = CurrentParameters.FieldType };
                    int InsertAfter = Current.Fields.Select(s => s.Name).ToList().IndexOf(CurrentParameters.InsertAfter);
                    Inputs = UsedDataItemsHelper.GetUsedTables(ResultingTokens, Current);
                    DataItem Output = new DataItem();
                    Output.DataItemType = Current.DataItemType;
                    Output.Name = Current.Name;
                    Output.TableName = Current.Name;
                    Output.Fields = Current.Fields.Take(InsertAfter + 1).ToList();
                    Output.Fields.Add(NewField);
                    Output.Fields = Output.Fields.Concat(Current.Fields.Skip(InsertAfter + 1).ToList()).ToList();
                    Outputs = new List<DataItem>();
                    Outputs.Add(Output);
                }
                else
                {
                    if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
                }
            }
            else
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInput });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
