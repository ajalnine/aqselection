﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using ApplicationCore;
using AQChartLibrary;

namespace SLCalc_ExpressionField
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private readonly CalculationService _cs;

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters
            {
                Code = string.Empty,
                Expression = string.Empty,
                FieldName = "X",
                FieldType = "Double"
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData;

            DataItem currentTable = _dataSources.Where(a => a != null).SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (currentTable != null)
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources where di != null select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                MainExpressionBuilder.SetDataItems(currentTable, _dataSources, null);
                MainExpressionBuilder.SetExpressionEditor(_currentParameters.Expression);
                MainExpressionBuilder.SetCodeEditor(_currentParameters.Code);
            }
            else
            {
                DataSourceSelector.ItemsSource = (from di in _dataSources where di != null select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            UpdateFieldsSource();
            ExpressionFieldName.Text = _currentParameters.FieldName;
            MainExpressionBuilder.StartHelp();
            ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(_currentParameters.FieldType.ToLower());
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            if (CheckResult()) ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void UpdateFieldsSource()
        {
            var currentTable = _dataSources.Where(a=>a!=null).SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable == null) return;
            var fields = currentTable.Fields.Where(a => a != null).Select(s => s.Name).ToList();
            FieldBeforeSelector.ItemsSource =fields;
            FieldBeforeSelector.UpdateLayout();
            if (!fields.Any()) return;
            if (_currentParameters.InsertAfter != null && fields.Contains(_currentParameters.InsertAfter)) FieldBeforeSelector.SelectedItem = _currentParameters.InsertAfter;
            else FieldBeforeSelector.SelectedIndex = fields.Count()-1;
            FieldBeforeSelector.UpdateLayout();
        }

        private bool CheckResult()
        {
            var currentTable = _dataSources.Where(a => a != null).SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (currentTable != null)
            {
                var resultingNames = currentTable.Fields.Select(s => s.Name).ToList();

                var isDoubled = (from s in resultingNames group s by s into g where g.Count() > 1 select g).Any();
                if (isDoubled)
                {
                    ShowTableNameError(true, "Есть повторяющиеся поля");
                    return false;
                }
            }


            if (ExpressionFieldName.Text.Trim()==string.Empty)
            {
                ShowTableNameError(true, "Не задано имя поля");
                return false;
            }

            ShowTableNameError(false, string.Empty);
            return true;
        }
        
        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = message;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            UpdateFieldsSource();
            MainExpressionBuilder.SetDataItems(_dataSources.Where(a=>a!=null).SingleOrDefault(s => s.Name == _currentParameters.TableName), _dataSources, null);
        }

        private void UpdateParameters()
        {
            _currentParameters.FieldName = ExpressionFieldName.Text;
            if (FieldBeforeSelector.SelectedItem != null) _currentParameters.InsertAfter = FieldBeforeSelector.SelectedItem.ToString();
            _currentParameters.Expression = MainExpressionBuilder.GetExpression();
            _currentParameters.Code = MainExpressionBuilder.GetCode();
        }
        
        public void DataRequestReadyCallback(string fileName){}

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _cs.BeginCheckExpression(e.CSharpText, _dataSources, _currentParameters.TableName, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null")
                {
                    var typeDescription = SLDataReportingExtentions.GetFieldTypeDescription(sr.Message.ToLower());

                    _currentParameters.FieldType = sr.Message;
                    ExpressionFieldType.SelectedValue = typeDescription;
                    EnableExit(true);
                }
                else
                {
                    if (!sr.Success) MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        private void FieldBeforeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_currentParameters != null && FieldBeforeSelector.SelectedItem!=null) _currentParameters.InsertAfter = FieldBeforeSelector.SelectedItem.ToString();
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            EnableExit(false);
        }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        private void ExpressionFieldType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _currentParameters.FieldType = SLDataReportingExtentions.GetFieldType(ExpressionFieldType.SelectedValue.ToString());
        }
    }
}