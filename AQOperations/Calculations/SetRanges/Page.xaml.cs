﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;
using AQControlsLibrary;

namespace SLCalc_SetRanges
{
    public partial class Page : UserControl, IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters CurrentParameters;
        private List<DataItem> DataSources;
        private List<DataItem> _availableData;
        private DataItem SelectedTable;
        private ObservableCollection<Range> _rangeView = new ObservableCollection<Range>();
        Task Analize;
        DataLoader Data;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            CurrentParameters = new Parameters()
            {
                Ranges = new ObservableCollection<Range>(),
                TableName = availableData.Where(s => s.DataItemType == DataType.Selection).First().TableName,
                GroupField = "Нет",
                Groups = string.Empty
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            DataSources = availableData.Where(s => s.DataItemType == DataType.Selection).ToList();
            if (!DataSources.Select(a => a.Name).Contains(CurrentParameters.TableName)) UpdateDataSourceSelector(null);
            else UpdateDataSourceSelector(CurrentParameters.TableName);
            UpdateFieldsSource();
            Groups.Text = CurrentParameters.Groups;
            CreateRangeView();
            RangeEditor.ItemsSource = _rangeView;
        }

        private bool UpdateDataSourceSelector(string SourceToSelect)
        {
            if (DataSources.Select(s => s.Name).Contains(SourceToSelect) && SourceToSelect != null)
            {
                DataSourceSelector.ItemsSource = null;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectedItem = SourceToSelect;
                DataSourceSelector.UpdateLayout();
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                return true;
            }

            else if (DataSources.Count > 0)
            {
                DataSourceSelector.ItemsSource = (from di in DataSources select di.Name);
                DataSourceSelector.SelectedIndex = 0;
            }
            return false;
        }
        
        private void CreateRangeView()
        {
            SelectedTable = _availableData.SingleOrDefault(a => a.Name == CurrentParameters.TableName);
            var fields = SelectedTable.Fields.Where(a => a.DataType == "Double").Select(a => a.Name).ToList();
            _rangeView = new ObservableCollection<Range>();
            if (CurrentParameters.GroupField == "Нет")
            {
                foreach (var f in fields)
                {
                    var existing = CurrentParameters.Ranges.FirstOrDefault(a => a.Parameter == f);
                    if (existing != null)
                    {
                        existing.Group = null;
                        _rangeView.Add(existing);
                    }
                    else
                    {
                        _rangeView.Add(new Range
                        {
                            Parameter = f,
                            Group = string.Empty,
                            Min = null,
                            Max = null
                        });
                    }
                }
            }
            else
            {
                var groups = Groups.Text.Split(new string[] { "\r", "\n", "\r\n" }, StringSplitOptions.None).Distinct();
                foreach (var f in fields)
                {
                    foreach (var g in groups.Where(a=>!string.IsNullOrWhiteSpace(a)))
                    {
                        var existing = CurrentParameters.Ranges.FirstOrDefault(a => a.Parameter == f && a.Group == g);
                        if (existing != null)
                        {
                            _rangeView.Add(existing);
                        }
                        else
                        {
                            _rangeView.Add(new Range
                            {
                                Parameter = f,
                                Group = g,
                                Min = null,
                                Max = null
                            });
                        }
                    }
                }
            }
        }

        public string GetParametersXML()
        {
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringBuilder sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), CurrentParameters);
            return sb.ToString();
        }
        
        private void UpdateFieldsSource()
        {
            DataItem CurrentTable = DataSources.Where(a => a != null).Where(s => s.Name == CurrentParameters.TableName).SingleOrDefault();
            if (CurrentTable == null) return;
            var Fields = (new[] {"Нет"}).Union(CurrentTable.Fields.Where(a => a != null).Select(s => s.Name)).ToList();
            GroupFieldSelector.SelectionChanged -= GroupFieldSelector_SelectionChanged;
            GroupFieldSelector.ItemsSource = Fields;
            GroupFieldSelector.UpdateLayout();
            if (Fields.Count() > 0)
            {
                if (CurrentParameters.GroupField != null && Fields.Contains(CurrentParameters.GroupField))
                {
                    GroupFieldSelector.SelectedItem = CurrentParameters.GroupField;
                    if (!string.IsNullOrWhiteSpace(CurrentParameters.Groups)) Groups.Text = CurrentParameters.Groups;
                }
                else GroupFieldSelector.SelectedIndex = 0;
                GroupFieldSelector.UpdateLayout();
            }
            GroupFieldSelector.SelectionChanged += GroupFieldSelector_SelectionChanged;
        }

        private void UIToParameters()
        {
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            CurrentParameters.GroupField = GroupFieldSelector.SelectedItem.ToString();
            CurrentParameters.Ranges= new ObservableCollection<Range>(_rangeView.Where(a=>a.Max!= null || a.Min!=null));
            CurrentParameters.Groups = Groups.Text;
        }

        void RangeEditor_CurrentCellChanged(object sender, EventArgs e)
        {
            RangeEditor.BeginEdit();
        }

        private void EnableExit(bool Enable)
        {
            OKButton.IsEnabled = Enable;
        }

        private void RangeEditor_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var grid = e.EditingElement as Grid;
            if (grid == null || grid.Children == null || grid.Children.Count <= 1) return;
            var textBox = grid.Children[1] as TextBox;
            if (textBox == null) return;
            var binding = textBox.GetBindingExpression(TextBox.TextProperty);
           
            binding.UpdateSource();
        }
        private void CheckResult()
        {
            EnableExit(true);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            CurrentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            SelectedTable = _availableData.SingleOrDefault(a => a.Name == CurrentParameters.TableName);
            UpdateFieldsSource();
            CheckResult();
        }

        #region Обработка событий от кнопок

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            RangeEditor.SelectedIndex = -1;
            RangeEditor.UpdateLayout();
            UIToParameters(); 
            this.ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs() { CloseUI = (bool)true, ParametersXML = GetParametersXML() });
        }
        #endregion

        #region Автозаполнение категорий

        public void DataRequestReadyCallback(string fileName)
        {
            UIToParameters();
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Processing);
            Data = new DataLoader();
            Data.DataProcessed += new DataLoader.DataProcessDelegate(Data_DataProcessed);
            Data.BeginLoadData(fileName, fileName);
        }

        private void Data_DataProcessed(object sender, TaskStateEventArgs e)
        {
            Analize.SetState(0, TaskState.Ready);
            Analize.SetState(1, TaskState.Ready);
            Analize.SetState(2, TaskState.Processing);
            this.Dispatcher.BeginInvoke(delegate()
            {
                AutoSetCategories(Data.GetTable(CurrentParameters.TableName));
            });
        }

        private void AutoSetCategories(SLDataTable sldt)
        {
            int i = sldt.ColumnNames.IndexOf(CurrentParameters.GroupField);

            var a = (from row in sldt.Table select row.Row[i].ToString()).Distinct();
            string Result = String.Empty;
            foreach (var s in a)
            {
                Result += s + "\r";
            }
            Groups.Text = Result;
            GetCurrentGroups.IsEnabled = true;
            Analize.SetState(2, TaskState.Ready);
            UIToParameters();
            CreateRangeView();
            RangeEditor.ItemsSource = _rangeView;
        }

        private void GetCurrentGroups_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentParameters.GroupField == "Нет") return;
            if (DataRequest != null)
            {
                Analize = new Task(new string[] { "Исполнение расчета", "Прием данных", "Анализ данных" }, "Автоопределение параметров", TaskPanel);
                Analize.SetState(0, TaskState.Processing);
                GetCurrentGroups.IsEnabled = false;
                DataRequest.Invoke(this, new EventArgs());
            }
        }

        #endregion
        public void StateChanged(OperationViewMode OVM) { }

        private void GroupFieldSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Groups.Text = string.Empty;
            UIToParameters();
            CreateRangeView();
            RangeEditor.ItemsSource = _rangeView;
        }
        
        private void Groups_OnLostFocus(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            CreateRangeView();
            RangeEditor.ItemsSource = _rangeView;
        }
    }
}