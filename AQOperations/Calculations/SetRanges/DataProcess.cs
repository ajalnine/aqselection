﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_SetRanges
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private List<string> _deleted;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            parametersXml = parametersXml.Replace("</Parameter></Range>", "</Parameter><Group /></Range>");
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _dataSources = availableData;

            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();
            
            var currentTable = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (currentTable != null)
            {
                _inputs.Add(currentTable);
                if (_currentParameters.GroupField!="Нет" && currentTable.Fields.All(a => a.Name != _currentParameters.GroupField))
                {
                    if (DataFlowError != null)DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = null, Deleted = null });
                    return;
                }

                if (_currentParameters.Ranges.Count!=0)
                {
                    var di = new DataItem { DataItemType = DataType.Selection };
                    di.Name = _currentParameters.TableName;
                    di.TableName = _currentParameters.TableName;
                    di.Fields = new List<DataField>();
                    foreach (var df in currentTable.Fields)
                    {
                        di.Fields.Add(new DataField { Name = df.Name, DataType = df.DataType });
                    }

                    foreach (var s in _currentParameters.Ranges)
                    {
                        var minExists = di.Fields.Any(a => (a.Name == ("#" + s.Parameter + "_НГ")));
                        var maxExists = di.Fields.Any(a => (a.Name == ("#" + s.Parameter + "_ВГ")));
                        if (s.Min.HasValue && !minExists) di.Fields.Add(new DataField { Name = "#" + s.Parameter + "_НГ", DataType = "Double" });
                        if (s.Max.HasValue && !maxExists) di.Fields.Add(new DataField { Name = "#" + s.Parameter + "_ВГ", DataType = "Double" });
                    }
                    _outputs.Add(di);
                }
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = _deleted });
        }
    }
}
