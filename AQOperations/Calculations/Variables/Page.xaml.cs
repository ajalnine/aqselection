﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_Variables
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private readonly CalculationService _cs;
        private bool _ischecked;
        private Parameters _currentParameters;
        private List<DataItem> _availableData;
        private DataItem _variables;
        private int _editingIndex = -1;
        private readonly Dictionary<String, String> _constantTypes = new Dictionary<String, String>
        {
            {"Double", "123"},
            {"String", "Abc"},
            {"TimeSpan", "06:45:00"},
            {"DateTime", "25.12.2015"},
            {"Boolean", "0/1"},
            {"List<Double>", "1,2,3"},
            {"List<String>", "A,b,c"},
            {"List<DateTime>", "2015, 2016"},
        };

        public static readonly DependencyProperty NewVariableProperty =
        DependencyProperty.Register("NewVariable", typeof(VariableDescription), typeof(Page), new PropertyMetadata(null,null));

        public VariableDescription NewVariable
        {
            get { return (VariableDescription)GetValue(NewVariableProperty); }
            set { SetValue(NewVariableProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
            VariableType.ItemsSource = _constantTypes;
            GetNewVariable();
        }

        private void GetNewVariable()
        {
            NewVariable = new VariableDescription
            {
                Expression = string.Empty,
                Code = string.Empty,
                VariableName = "Var",
                VariableType = "Double"
            };
            NewVariable.PropertyChanged += NewVariable_PropertyChanged;
            NewVariablePanel.DataContext = NewVariable;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _variables = availableData.SingleOrDefault(a => a.Name=="Переменные");
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { Variables = new ObservableCollection<VariableDescription>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _variables = availableData.SingleOrDefault(a => a.Name == "Переменные");
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {
            SetupVariableSources();
            MainExpressionBuilder.SetExpressionEditor(NewVariable.Expression);
            MainExpressionBuilder.SetCodeEditor(NewVariable.Code);
            MainExpressionBuilder.StartHelp(); 
        }

        private void SetupVariableSources()
        {
            _variables = _availableData.SingleOrDefault(s => s.Name == "Переменные");
            var count = _editingIndex == -1 ? _currentParameters.Variables.Count : _editingIndex;
            var existingVariables =
                _currentParameters.Variables.Select(a => new DataField { Name = a.VariableName, DataType = a.VariableType }).Take(count)
                    .ToList();
            if (_variables == null) return;
            var availableVariables = new DataItem
            {
                TableName = _variables.TableName,
                DataItemType = _variables.DataItemType,
                Name = _variables.Name,
                Fields = new List<DataField>()
            };
            availableVariables.Fields.AddRange(_variables.Fields.Union(existingVariables).ToList());
            var data = _availableData.ToList();
            data.Remove(_variables);
            data.Add(availableVariables);
            VariablesListBox.ItemsSource = _currentParameters.Variables;
            MainExpressionBuilder.SetDataItems(null, data, null);
        }

        private void UIToParameters()
        {
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            SetError(string.Empty); 
            UIToParameters();
            var enabled = true;
            var variable = NewVariable.VariableName;
            if (string.IsNullOrEmpty(variable))
            {
                enabled = false;
                SetError("Пустое имя переменной");
            }
            if (_variables.Fields.Select(a => a.Name).Contains(variable))
            {
                enabled = false;
                SetError("Переменная с указанным именем уже существует в расчете");
            }
            if (_currentParameters.Variables.Select(a => a.VariableName).Contains(variable))
            {
                enabled = false;
            }
            if (string.IsNullOrEmpty(MainExpressionBuilder.GetExpression())) enabled = false;

            var exitEnabled = CheckVariableOrderAndExistance();

            AddRuleButton.IsEnabled = enabled && _ischecked;

            EnableExit(_currentParameters.Variables.Count() != 0 && exitEnabled);
        }

        private bool CheckVariableOrderAndExistance()
        {
            var existingVariablesList =
                _variables.Fields.Select(a => new DataField {Name = a.Name, DataType = a.DataType}).ToList();
            var existingVariableTable = new DataItem
            {
                TableName = _variables.TableName,
                DataItemType = _variables.DataItemType,
                Name = _variables.Name,
                Fields = existingVariablesList
            };
            var totalExistingVariablesList =
                _variables.Fields.Select(a => new DataField {Name = a.Name, DataType = a.DataType}).ToList();
            var totalExistingVariableTable = new DataItem
            {
                TableName = _variables.TableName,
                DataItemType = _variables.DataItemType,
                Name = _variables.Name,
                Fields = totalExistingVariablesList
            };
            foreach (var v in _currentParameters.Variables)
            {
                totalExistingVariableTable.Fields.Add(new DataField {Name = v.VariableName, DataType = v.VariableType});
            }

            var exitEnabled = true;
            foreach (var v in _currentParameters.Variables)
            {
                if (Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] {_variables}).Union(new[] {totalExistingVariableTable}).ToList(),
                    totalExistingVariableTable).Any(a => a.Type == TokenType.Unknown))
                {
                    exitEnabled = false;
                    SetError("Одно из выражений содержит неизвестные элементы");
                    break;
                }

                var usedVariables = Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] {_variables}).Union(new[] {totalExistingVariableTable}).ToList(),
                    totalExistingVariableTable).Where(a => a.Type == TokenType.Variable).ToList();
                foreach (var u in usedVariables)
                {
                    if (existingVariablesList.Select(a => a.Name).Contains(u.UsedField.Field.Name)) continue;
                    exitEnabled = false;
                    SetError("Одно из выражений ссылается на переменную, ранее не объявленную");
                    break;
                }
                if (!exitEnabled) break;
                existingVariableTable.Fields.Add(new DataField {Name = v.VariableName, DataType = v.VariableType});
            }
            return exitEnabled;
        }

        private void SetError(string error)
        {
            ErrorTextBlock.Text = error;
            ErrorTextBlock.Visibility = string.IsNullOrEmpty(error) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }
        public void DataRequestReadyCallback(string fileName) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as VariableDescription;
            _currentParameters.Variables.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
            SetupVariableSources();
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1) StoreVariable();
            var toEdit = ((TextImageButtonBase)sender).Tag as VariableDescription;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.Variables.IndexOf(toEdit);
            _currentParameters.Variables.Remove(toEdit);
            NewVariable = toEdit;

            SetupVariableSources();
            MainExpressionBuilder.SetExpressionEditor(NewVariable.Expression);
            MainExpressionBuilder.SetCodeEditor(NewVariable.Code);
            NewVariablePanel.DataContext = NewVariable;
            NewVariable.PropertyChanged += NewVariable_PropertyChanged;
            CheckResult();
        }

        void NewVariable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            AddRuleButton.IsEnabled = false;
            _ischecked = false;
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _ischecked = false; 
            _cs.BeginCheckExpression(e.CSharpText, _availableData, "Переменные", CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success)
                {
                    _ischecked = true;
                    CheckResult();
                    if (sr.Message != "null") NewVariable.VariableType = sr.Message;
                }
                else
                {
                    AddRuleButton.IsEnabled = false;
                    MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            StoreVariable();
            CheckResult();
            ParametersToUI();
        }

        private void StoreVariable()
        {
            var binding1 = VariableNameTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding1 && !string.IsNullOrEmpty(VariableNameTextBox.Text)) binding1.UpdateSource();
            var binding2 = VariableType.GetBindingExpression(Selector.SelectedValueProperty);
            if (null != binding2) binding2.UpdateSource();

            var variableDescription = new VariableDescription
            {
                Expression = MainExpressionBuilder.GetExpression(),
                Code = MainExpressionBuilder.GetCode(),
                VariableName = NewVariable.VariableName, 
                VariableType = NewVariable.VariableType
            }; 
            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.Variables.Insert(_currentParameters.Variables.Count, variableDescription);
                    break;
                default:
                    _currentParameters.Variables.Insert(_editingIndex, variableDescription);
                    _editingIndex = -1;
                    break;
            }
        }

        private void UpVariable_Click(object sender, RoutedEventArgs e)
        {
            if (VariablesListBox.SelectedItem == null) return;
            var selected = VariablesListBox.SelectedItem as VariableDescription;
            if (selected == null) return;
            var selectedIndex = VariablesListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : VariablesListBox.Items.Count - 1;
            MoveVariable(selectedIndex, nextIndex);
        }

        private void DownVariable_Click(object sender, RoutedEventArgs e)
        {
            if (VariablesListBox.SelectedItem == null) return;
            var selected = VariablesListBox.SelectedItem as VariableDescription;
            if (selected == null) return;
            var selectedIndex = VariablesListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < VariablesListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveVariable(selectedIndex, nextIndex);
        }

        private void TopVariable_Click(object sender, RoutedEventArgs e)
        {
            if (VariablesListBox.SelectedItem == null) return;
            var selected = VariablesListBox.SelectedItem as VariableDescription;
            if (selected == null) return;
            var selectedIndex = VariablesListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveVariable(selectedIndex, nextIndex);
        }

        private void BottomVariable_Click(object sender, RoutedEventArgs e)
        {
            if (VariablesListBox.SelectedItem == null) return;
            var selected = VariablesListBox.SelectedItem as VariableDescription;
            if (selected == null) return;
            var selectedIndex = VariablesListBox.Items.IndexOf(selected);
            var nextIndex = VariablesListBox.Items.Count - 1;
            MoveVariable(selectedIndex, nextIndex);
        }

        private void MoveVariable(int selectedIndex, int nextIndex)
        {
            var newItem = VariablesListBox.SelectedItem as VariableDescription;
            _currentParameters.Variables.RemoveAt(selectedIndex);
            _currentParameters.Variables.Insert(nextIndex, newItem);
            VariablesListBox.SelectedItem = newItem;
            VariablesListBox.ScrollIntoView(VariablesListBox.SelectedItem);
            VariablesListBox.UpdateLayout();
            CheckResult();
        }
    }
    public class VariableConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (VariableDescription)value;
            var result = p.VariableName + " = " + p.Expression;
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}