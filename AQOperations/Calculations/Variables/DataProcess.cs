﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_Variables
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs = new List<DataItem>();
        private readonly List<DataItem> _outputs = new List<DataItem>();
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var variableTable = availableData.SingleOrDefault(s => s.Name == "Переменные");
            var resultingTokens = new List<Token>();
            
            if (variableTable == null)
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            
            var output = new DataItem
            {
                TableName = variableTable.TableName,
                DataItemType = variableTable.DataItemType,
                Name = variableTable.Name,
                Fields = new List<DataField>()
            };
            output.Fields.AddRange(variableTable.Fields);
            foreach (var rule in _currentParameters.Variables)
            {
                var data = availableData.Except(new[]{variableTable}).Union(new[]{output});
                resultingTokens.InsertRange(0, Tokens.ParseTokens(rule.Expression, data.ToList(), output));
                output.Fields.Add(new DataField { Name = rule.VariableName, DataType = rule.VariableType });
            }

            if (Tokens.CheckTokens(resultingTokens))
            {
                _outputs.Add(output);
                _inputs = UsedDataItemsHelper.GetUsedTables(resultingTokens, null).ToList();
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
