﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_Variables
{
    public class Parameters
    {
        public ObservableCollection<VariableDescription> Variables;
    }
    public class VariableDescription : INotifyPropertyChanged
    {
        private string _variablename;
        public string VariableName
        {
            get
            {
                return _variablename;
            }
            set
            {
                _variablename = value;
                NotifyPropertyChanged("SequenceName");
            }
        }

        private string _variableType;
        public string VariableType
        {
            get { return _variableType; }
            set
            {
                _variableType = value;
                NotifyPropertyChanged("SequenceType");
            }
        }

        private string _expression;
        public string Expression
        {
            get
            {
                return _expression;
            }
            set
            {
                _expression = value;
                NotifyPropertyChanged("Expression");
            }
        }

        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
                NotifyPropertyChanged("Code");
            }
        }
        
       public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
