﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_RecalcFields
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private readonly CalculationService _cs;
        private List<DataItem> _dataSources;
        private bool _ischecked;
        private Parameters _currentParameters;
        private List<DataItem> _availableData;
        private DataItem _selectedTable;
        private string _currentDataType, _targetType;
        private int _editingIndex = -1;
        private DataItem _availableFieldsTable;
        private readonly Dictionary<String, String> _expressionFieldTypes = new Dictionary<String, String>
        {
            {"Double", "123"},
            {"String", "Abc"},
            {"TimeSpan", "06:45:00"},
            {"DateTime", "25.12.2015"},
            {"Boolean", "0/1"}
        };
        public static readonly DependencyProperty RecalcFieldProperty =
        DependencyProperty.Register("RecalcField", typeof(RecalcFieldDescription), typeof(Page), new PropertyMetadata(null, null));

        public RecalcFieldDescription RecalcField
        {
            get { return (RecalcFieldDescription)GetValue(RecalcFieldProperty); }
            set { SetValue(RecalcFieldProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
            ExpressionFieldType.ItemsSource = _expressionFieldTypes;
            GetNewExpressionField();
        }

        private void GetNewExpressionField()
        {
            RecalcField = new RecalcFieldDescription
            {
                Expression = string.Empty,
                Code = string.Empty,
                ExpressionType = "String"
            };
            RecalcField.PropertyChanged += NewVariable_PropertyChanged;
            NewValuePanel.DataContext = RecalcField;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { ExpressionFields = new ObservableCollection<RecalcFieldDescription>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }
            FieldNameSelector.ItemsSource = _selectedTable.Fields.Select(a => a.Name);
            FieldNameSelector.UpdateLayout();
            if (string.IsNullOrEmpty(RecalcField.FieldName)) FieldNameSelector.SelectedItem = FieldNameSelector.ItemsSource.OfType<string>().FirstOrDefault();
            FieldNameSelector.UpdateLayout();
            ExpressionFieldsListBox.ItemsSource = _currentParameters.ExpressionFields;
            _currentDataType = _selectedTable.Fields.Where(b => b.Name == RecalcField.FieldName).Select(b => b.DataType).FirstOrDefault();
            SetupExpressionFieldsSources();
            if (string.IsNullOrEmpty(RecalcField.Expression)) SetExpressionTemplate();
            else  MainExpressionBuilder.SetExpressionEditor(RecalcField.Expression);
            MainExpressionBuilder.SetCodeEditor(RecalcField.Code);
            MainExpressionBuilder.StartHelp();
            CheckResult();
        }

        private void SetupExpressionFieldsSources()
        {
            _selectedTable = _availableData.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            var count = _editingIndex == -1 ? _currentParameters.ExpressionFields.Count : _editingIndex;
            
            var existingFields =_currentParameters.ExpressionFields.Select(a => new DataField { Name = a.FieldName, DataType = _selectedTable.Fields.Where(b=>b.Name==a.FieldName).Select(b=>b.DataType).FirstOrDefault() }).Take(count).ToList();
            if (_selectedTable == null) return;
            _availableFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = new List<DataField>()
            };
            _availableFieldsTable.Fields.AddRange(_selectedTable.Fields.Union(existingFields).ToList());
            var data = _availableData.ToList();
            data.Remove(_selectedTable);
            data.Add(_availableFieldsTable);
            ExpressionFieldsListBox.ItemsSource = _currentParameters.ExpressionFields;
            MainExpressionBuilder.SetDataItems(_availableFieldsTable, data, RecalcField.FieldName);
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null)
                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentDataType = _selectedTable.Fields.Where(b => b.Name == RecalcField.FieldName).Select(b => b.DataType).FirstOrDefault();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            SetError(string.Empty);
            UIToParameters();
            var enabled = true;
            var fieldName = RecalcField.FieldName;
            if (string.IsNullOrEmpty(MainExpressionBuilder.GetExpression())) enabled = false;

            var exitEnabled = CheckFieldOrderAndExistance();

            AddRuleButton.IsEnabled = enabled && _ischecked;

            EnableExit(_currentParameters.ExpressionFields.Count() != 0 && exitEnabled);
        }

        private bool CheckFieldOrderAndExistance()
        {
            var existingFieldsList = _selectedTable.Fields.Select(a => new DataField { Name = a.Name, DataType = a.DataType }).ToList();
            var existingFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = existingFieldsList
            };
            var totalExistingFieldsList =
                _selectedTable.Fields.Select(a => new DataField { Name = a.Name, DataType = a.DataType }).ToList();
            var totalExistingFieldsTable = new DataItem
            {
                TableName = _selectedTable.TableName,
                DataItemType = _selectedTable.DataItemType,
                Name = _selectedTable.Name,
                Fields = totalExistingFieldsList
            };
            foreach (var v in _currentParameters.ExpressionFields)
            {
                totalExistingFieldsTable.Fields.Add(new DataField { Name = v.FieldName, DataType = _selectedTable.Fields.Where(b => b.Name == v.FieldName).Select(b => b.DataType).FirstOrDefault() });
            }

            var exitEnabled = true;
            foreach (var v in _currentParameters.ExpressionFields)
            {
                if (Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] { _selectedTable }).Union(new[] { totalExistingFieldsTable }).ToList(),
                    totalExistingFieldsTable, v.FieldName).Any(a => a.Type == TokenType.Unknown))
                {
                    exitEnabled = false;
                    SetError("Одно из выражений содержит неизвестные элементы");
                    break;
                }

                var usedFields = Tokens.ParseTokens(v.Expression,
                    _availableData.Except(new[] { _selectedTable }).Union(new[] { totalExistingFieldsTable }).ToList(),
                    totalExistingFieldsTable, v.FieldName).Where(a => a.Type == TokenType.CurrentField).ToList();
                foreach (var u in usedFields)
                {
                    if (existingFieldsList.Select(a => a.Name).Contains(u.UsedField.Field.Name)) continue;
                    exitEnabled = false;
                    SetError("Одно из выражений ссылается на поле таблицы, ранее не объявленное");
                    break;
                }
                if (!exitEnabled) break;
            }
            return exitEnabled;
        }

        private void SetError(string error)
        {
            ErrorTextBlock.Text = error;
            ErrorTextBlock.Visibility = string.IsNullOrEmpty(error) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }
        public void DataRequestReadyCallback(string fileName) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as RecalcFieldDescription;
            _currentParameters.ExpressionFields.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
            SetupExpressionFieldsSources();
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1) StoreExpressionField();
            var toEdit = ((TextImageButtonBase)sender).Tag as RecalcFieldDescription;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.ExpressionFields.IndexOf(toEdit);
            _currentParameters.ExpressionFields.Remove(toEdit);
            RecalcField = toEdit;

            SetupExpressionFieldsSources();
            MainExpressionBuilder.SetExpressionEditor(RecalcField.Expression);
            MainExpressionBuilder.SetCodeEditor(RecalcField.Code);
            NewValuePanel.DataContext = RecalcField;
            RecalcField.PropertyChanged += NewVariable_PropertyChanged;
            CheckResult();
        }

        void NewVariable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void FieldSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_selectedTable == null || _selectedTable.Fields == null || FieldNameSelector == null || FieldNameSelector.SelectedValue == null) return;
            SetExpressionTemplate();
            var selectedField = _selectedTable.Fields.SingleOrDefault(a => a.Name == (string)FieldNameSelector.SelectedValue);
            ExpressionFieldType.SelectedValue = selectedField?.DataType;
            _currentDataType = selectedField?.DataType;
            AddRuleButton.IsEnabled = false;
        }

        private void SetExpressionTemplate()
        {
            if (_editingIndex > -1) return;
            FieldNameSelector.UpdateLayout();
            var selectedField = _selectedTable.Fields.SingleOrDefault(a => a.Name == (string)FieldNameSelector.SelectedValue);
            if (selectedField != null)
            {
                MainExpressionBuilder.SetDataItems(_selectedTable, _availableData, selectedField.Name);
                var type = selectedField.DataType;
                MainExpressionBuilder.SetExpressionEditor(GetTemplate());
            }
        }

        private string GetTemplate()
        {
            if (FilterTemplate.IsChecked.Value)
            {
                switch (_currentDataType)
                {
                    case "Double":
                    case "Int":
                        return "CheckInRange(v, 10,20)";
                    case "DateTime":
                        return
                            @"CheckInRange(v, 
ParseDateTimeFormatted(""01.01.2016"", ""dd.MM.yyyy""), 
ParseDateTimeFormatted(""01.07.2016"", ""dd.MM.yyyy""))";
                    case "TimeSpan":
                        return 
                            @"CheckInRange(v, 
ParseDateTimeFormat(""01:00:00"", ""HH.mm.ss""), 
ParseDateTimeFormat(""03:00:00"", ""HH.mm.ss""))";
                    case "String":
                        return
                            @"CheckList(v, ""a"", ""b"", ""c"")";
                }

            }
            else if (FormalTemplate.IsChecked.Value)
            {
                switch (_currentDataType)
                {
                    case "Double":
                    case "Int":
                        return "CeilingToList(v, 4, 8, 16, 32, 48)";

                    case "DateTime":
                        return @"RoundDateTimeToFloor(v, ""M"")";
                
                    case "String":
                        return @"ReplaceList(ReplaceList(v
, ""Группа 1"", ""a"", ""b"",""c"")
, ""Группа 2"", ""d"", ""e"",""f"")";

                }
            }
            else if (FillTemplate.IsChecked.Value)
            {
                return @"v = null ? 'v : v";
            }
            else if (ChangeTypeTemplate.IsChecked.Value)
            {
                switch (_currentDataType)
                {
                    case "Bool":
                        switch (_targetType)
                        {
                            case "Bool":
                                return @"v";
                            case "Double":
                                return "v? 1:0";
                            case "DateTime":
                                return @"";
                            case "String":
                                return "v ? \"Да\" : \"Нет\"";
                            case "TimeSpan":
                                return @"";
                        }
                        return "v";


                    case "Double":
                        switch (_targetType)
                        {
                            case "Bool":
                                return "v > 0";
                            case "Double":
                                return "v";
                            case "DateTime":
                                return @"";
                            case "String":
                                return @"Text(v)";
                            case "TimeSpan":
                                return @"";
                        }
                        return "v";

                    case "Int":
                        switch (_targetType)
                        {
                            case "Bool":
                                return "v > 0";
                            case "Double":
                                return "v";
                            case "DateTime":
                                return @"";
                            case "String":
                                return @"Text(v)";
                            case "TimeSpan":
                                return @"";
                        }
                        return "v";

                    case "DateTime":
                        switch (_targetType)
                        {
                            case "Bool":
                                return @"";
                            case "Double":
                                return @"";
                            case "DateTime":
                                return @"v";
                            case "String":
                                return @"Text(v)";
                            case "TimeSpan":
                                return @"v";
                        }
                        return "v";

                    case "String":
                        switch (_targetType)
                        {
                            case "Bool":
                                return @"";
                            case "Double":
                                return @"ParseNumber(v)";
                            case "DateTime":
                                return @"ParseDateTimeAuto(v)";
                            case "String":
                                return @"v";
                            case "TimeSpan":
                                return @"";
                        }
                        return "v";

                    case "TimeSpan":
                        switch (_targetType)
                        {
                            case "Bool":
                                return "";
                            case "Double":
                                return "";
                            case "DateTime":
                                return "";
                            case "String":
                                return @"Text(v)";
                            case "TimeSpan":
                                return @"v";
                        }
                        return "v";
                }
            }
            return "v";
        }




        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            AddRuleButton.IsEnabled = false;
            _ischecked = false;
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _ischecked = false;
            _cs.BeginCheckExpression(e.CSharpText
                , _availableData.Except(new[]{_selectedTable}).Union(new[]{_availableFieldsTable}).ToList()
                , _selectedTable.Name, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success)
                {
                    _ischecked = true;
                    CheckResult();

                    if (sr.Message != "null")
                    {
                        RecalcField.ExpressionType = sr.Message;
                        _currentDataType = sr.Message;
                    }
                }
                else
                {
                    AddRuleButton.IsEnabled = false;
                    MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            StoreExpressionField();
            CheckResult();
            ParametersToUI();
        }

        private void StoreExpressionField()
        {
            var binding2 = ExpressionFieldsListBox.GetBindingExpression(Selector.SelectedValueProperty);
            if (null != binding2) binding2.UpdateSource();

            var expressionFieldDescription = new RecalcFieldDescription
            {
                Expression = MainExpressionBuilder.GetExpression(),
                Code = MainExpressionBuilder.GetCode(),
                FieldName = RecalcField.FieldName,
                ExpressionType = ExpressionFieldType.SelectedValue.ToString()
            };

            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.ExpressionFields.Insert(_currentParameters.ExpressionFields.Count, expressionFieldDescription);
                    break;
                default:
                    _currentParameters.ExpressionFields.Insert(_editingIndex, expressionFieldDescription);
                    _editingIndex = -1;
                    break;
            }
        }

        private void UpExpressionField_Click(object sender, RoutedEventArgs e)
        {
            var selected = ExpressionFieldsListBox.SelectedItem as RecalcFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : ExpressionFieldsListBox.Items.Count - 1;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void DownExpressionField_Click(object sender, RoutedEventArgs e)
        {
            var selected = ExpressionFieldsListBox.SelectedItem as RecalcFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < ExpressionFieldsListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void TopExpressionField_Click(object sender, RoutedEventArgs e)
        {
            var selected = ExpressionFieldsListBox.SelectedItem as RecalcFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void BottomExpressionField_Click(object sender, RoutedEventArgs e)
        {
            var selected = ExpressionFieldsListBox.SelectedItem as RecalcFieldDescription;
            if (selected == null) return;
            var selectedIndex = ExpressionFieldsListBox.Items.IndexOf(selected);
            var nextIndex = ExpressionFieldsListBox.Items.Count - 1;
            MoveExpressionField(selectedIndex, nextIndex);
        }

        private void MoveExpressionField(int selectedIndex, int nextIndex)
        {
            var newItem = ExpressionFieldsListBox.SelectedItem as RecalcFieldDescription;
            _currentParameters.ExpressionFields.RemoveAt(selectedIndex);
            _currentParameters.ExpressionFields.Insert(nextIndex, newItem);
            ExpressionFieldsListBox.SelectedItem = newItem;
            ExpressionFieldsListBox.ScrollIntoView(ExpressionFieldsListBox.SelectedItem);
            ExpressionFieldsListBox.UpdateLayout();
            CheckResult();
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null) return;
            if (DataSourceSelector.SelectedItem != null) _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.ExpressionFields.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            AddRuleButton.IsEnabled = false;
            ParametersToUI();
        }

        private void Template_OnClick(object sender, RoutedEventArgs e)
        {
            MainExpressionBuilder.SetExpressionEditor(GetTemplate());
        }

        private void Type_OnClick(object sender, RoutedEventArgs e)
        {
            _targetType = ((TextImageRadioButtonBase)sender).Name;
            MainExpressionBuilder.SetExpressionEditor(GetTemplate());
        }

        private void ChangeTypeTemplate_OnUnchecked(object sender, RoutedEventArgs e)
        {
            TypePanel.Visibility = Visibility.Collapsed;
        }

        private void ChangeTypeTemplate_OnChecked(object sender, RoutedEventArgs e)
        {
            TypePanel.Visibility = Visibility.Visible;
        }

    }
    public class VariableConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = (RecalcFieldDescription)value;
            var result = $"[{p.FieldName}] = {p.Expression}";
            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}