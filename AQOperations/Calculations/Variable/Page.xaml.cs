﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore;
using AQCalculationsLibrary;
using AQChartLibrary;

namespace SLCalc_Variable
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        DataItem _currentTable;
        private readonly CalculationService _cs;

        public Page()
        {
            InitializeComponent();
            _cs = Services.GetCalculationService();
        }
        
        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters
            {
                Code = string.Empty,
                Expression = string.Empty,
                VariableName = "X",
                VariableType = "Double"
            };
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData;
            _currentTable = _dataSources.SingleOrDefault(s => s.Name == "Переменные");

            if (_currentTable != null)
            {
                MainExpressionBuilder.SetDataItems(_currentTable, _dataSources, null);
                MainExpressionBuilder.SetExpressionEditor(_currentParameters.Expression);
                MainExpressionBuilder.SetCodeEditor(_currentParameters.Code);
                ExpressionFieldName.Text = _currentParameters.VariableName;
                ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(_currentParameters.VariableType);
            }
            else
            {
                MainExpressionBuilder.SetDataItems(_currentTable, _dataSources, null);
            }
            MainExpressionBuilder.StartHelp(); 
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }
        
        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateParameters();
            var expr = (_currentParameters.Expression.Length > 10) ? _currentParameters.Expression.Substring(0, 10) + "..." : _currentParameters.Expression;
            var name = string.Format("@{0}={1}", _currentParameters.VariableName, expr);
            if (NameChanged != null) NameChanged.Invoke(this, new NameChangedEventArgs { NewName = name });
            if (CheckResult()) ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }
        
        private bool CheckResult()
        {
            List<string> resultingNames = _currentTable.Fields.Select(s => s.Name).ToList();

            bool isDoubled = (from s in resultingNames group s by s into g where g.Count() > 1 select g).Any();
            if (isDoubled)
            {
                ShowTableNameError(true, "Есть повторяющиеся имена");
                return false;
            }

            if (ExpressionFieldName.Text.Trim() == string.Empty)
            {
                ShowTableNameError(true, "Не задано имя переменной");
                return false;
            }

            ShowTableNameError(false, string.Empty);
            return true;
        }

        private void ShowTableNameError(bool show, string message)
        {
            ErrorLabel.Visibility = (show) ? Visibility.Visible : Visibility.Collapsed;
            ErrorLabel.Text = message;
        }

        private void UpdateParameters()
        {
            _currentParameters.VariableName = ExpressionFieldName.Text;
            _currentParameters.Expression = MainExpressionBuilder.GetExpression();
            _currentParameters.Code = MainExpressionBuilder.GetCode();
        }

        public void DataRequestReadyCallback(string fileName){ }

        public void StateChanged(OperationViewMode ovm) 
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }
        
        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _cs.BeginCheckExpression(e.CSharpText, _dataSources, "Переменные", CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(() =>
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null")
                {
                    EnableExit(true);
                    _currentParameters.VariableType = sr.Message;
                    ExpressionFieldType.SelectedValue = SLDataReportingExtentions.GetFieldTypeDescription(sr.Message.ToLower());
                }
                else
                {
                    EnableExit(false);
                    if (!sr.Success) MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                }
            });
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            EnableExit(false);
        }

        private void ExpressionFieldType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _currentParameters.VariableType = SLDataReportingExtentions.GetFieldType(ExpressionFieldType.SelectedValue.ToString());
        }
    }
}