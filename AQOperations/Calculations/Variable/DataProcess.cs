﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using AQCalculationsLibrary;
using ApplicationCore.CalculationServiceReference;

namespace SLCalc_Variable
{
    public class DataFlow : IDataFlow
    {
        private List<DataItem> Inputs;
        private List<DataItem> Outputs;

        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private Parameters CurrentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Parameters));
            StringReader sr = new StringReader(parametersXml);
            CurrentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            DataItem Current = availableData.Where(n => n.Name == "Переменные").Single();
            List<Token> ResultingTokens = Tokens.ParseTokens(CurrentParameters.Expression, availableData, Current);
                
            if (Tokens.CheckTokens(ResultingTokens))
            {
                DataItem Output = new DataItem();
                Output.DataItemType = Current.DataItemType;
                Output.Name = Current.Name;
                Output.TableName = Current.Name;
                DataField NewField = new DataField() { Name = CurrentParameters.VariableName, DataType = CurrentParameters.VariableType };
                Inputs = UsedDataItemsHelper.GetUsedTables(ResultingTokens, Current).Where(a=>a.DataItemType!= DataType.Variables).ToList();
                    
                Output.Fields = Current.Fields.ToList();
                Output.Fields.Add(NewField);

                Outputs = new List<DataItem>();
                Outputs.Add(Output);
            }
            else
            {
                if (this.DataFlowError != null) this.DataFlowError.Invoke(this, new DataFlowErrorEventArgs() { Error = InconsistenceState.LostInputFields });
            }
            if (this.DataFlowFinished != null) this.DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs() { Inputs = Inputs, Outputs = Outputs, Deleted = null });
        }
    }
}
