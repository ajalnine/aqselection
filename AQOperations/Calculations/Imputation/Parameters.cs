﻿using System.Collections.Generic;
using System.ComponentModel;

// ReSharper disable once CheckNamespace
namespace SLCalc_Imputation
{
    public class Parameters
    {
        public List<Imputation> ImputationFields;
        public string TableName;
    }

    public enum ImputationMethod
    {
        None, Avg, Med, First, Last, Zero, Int, HotDeck
    }

    public class Imputation : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private ImputationMethod _method;
        public ImputationMethod Method
        {
            get
            {
                return _method;
            }
            set
            {
                _method = value;
                NotifyPropertyChanged("Method");
            }
        }

        private bool _nan;
        public bool NAN
        {
            get
            {
                return _nan;
            }
            set
            {
                _nan = value;
                NotifyPropertyChanged("NAN");
            }
        }

        private bool _nand;
        public bool NAND
        {
            get
            {
                return _nand;
            }
            set
            {
                _nand = value;
                NotifyPropertyChanged("NAND");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
