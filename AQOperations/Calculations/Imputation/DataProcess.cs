﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_Imputation
{
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs;
        private List<DataItem> _outputs;
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            } 
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            _inputs = new List<DataItem>();
            _outputs = new List<DataItem>();

            var selectedTable = availableData.Where(a => a != null).LastOrDefault(s => s.Name == _currentParameters.TableName);
            if (selectedTable != null) 
            {
                _inputs.Add(selectedTable);
                _outputs.Add(selectedTable);
                foreach (var gf in _currentParameters.ImputationFields)
                {
                    var dataType = (from s in selectedTable.Fields where s.Name == gf.FieldName select s.DataType).LastOrDefault();
                    if (dataType == null)
                    {
                        DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
                    }
                }
            }
            else
            {
                DataFlowError?.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            DataFlowFinished?.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
