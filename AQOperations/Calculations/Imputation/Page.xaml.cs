﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

// ReSharper disable once CheckNamespace
namespace SLCalc_Imputation
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;

        private DataItem _selectedTable;

        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _currentParameters = new Parameters();
            ParametersToUI(availableData);
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            foreach (var g in _currentParameters.ImputationFields) g.PropertyChanged += gdt_PropertyChanged;
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection).ToList();

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
                var existingFields = (from s in _currentParameters.ImputationFields select s.FieldName).ToList();
                var newSource = _dataSources.Where(s => s.Name == _currentParameters.TableName).ToList();
                if (newSource.Any())
                {
                    var newFields = (from s in newSource.First().Fields select s.Name).ToList();
                    if (existingFields.Intersect(newFields).Count() != existingFields.Count)
                    {
                        UpdateFieldsSource();
                        CheckResult();
                        return;
                    }
                }
                UpdateFieldsSource();
                CheckResult();
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    DataSourceSelector.UpdateLayout();
                }
                UpdateFieldsSource();
                CheckResult();
            }
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ParametersAccepted?.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void DefaultButton_Click(object sender, RoutedEventArgs e)
        {
            _currentParameters.ImputationFields = null;
            UpdateFieldsSource();
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void UpdateFieldsSource()
        {
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName); ////////
            if (_currentParameters.ImputationFields == null)
            {
                _currentParameters.ImputationFields = new List<Imputation>();
                if (_selectedTable != null)
                {
                    foreach (var a in _selectedTable.Fields)
                    {
                        var ifd = new Imputation
                        {
                            FieldName = a.Name,
                            Method = ImputationMethod.None,
                            NAN = (a.DataType.ToLower() == "string" || a.DataType.ToLower() == "datetime"),
                            NAND = (a.DataType.ToLower() == "string")
                        };
                        ifd.PropertyChanged += gdt_PropertyChanged;
                        _currentParameters.ImputationFields.Add(ifd);
                    }
                }
            }
            else
            {
                if (_dataSources == null || _dataSources.Count == 0 || _selectedTable == null) return;
                var imputadedFields = from c in _currentParameters.ImputationFields select c.FieldName;
                var existingFields = (from s in _dataSources.Where(a => a != null).LastOrDefault(d => d.Name == _selectedTable.Name).Fields where s != null select s.Name).ToList();
                var unprocessedFields = existingFields.Except(imputadedFields).ToList();
                var toRemoveFields = imputadedFields.Except(existingFields).ToList();
                foreach (var u in unprocessedFields)
                {
                    var dt = _selectedTable.Fields.SingleOrDefault(f => f.Name == u).DataType.ToLower();
                    var gdt = new Imputation
                    {
                        FieldName = u,
                        Method = ImputationMethod.None,
                        NAN = (dt == "string" || dt == "datetime")
                    };
                    gdt.PropertyChanged += gdt_PropertyChanged;
                    _currentParameters.ImputationFields.Add(gdt);
                }
                foreach (var r in toRemoveFields)
                {
                    _currentParameters.ImputationFields.Remove(_currentParameters.ImputationFields.SingleOrDefault(a => a.FieldName == r));
                }
            }
            GroupSelector.ItemsSource = null;
            GroupSelector.UpdateLayout();
            GroupSelector.ItemsSource = _currentParameters.ImputationFields;
            GroupSelector.UpdateLayout();
        }

        void gdt_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CheckResult();
        }

        private void CheckResult()
        {
            EnableExit(_currentParameters.ImputationFields.Any(a => a.Method != ImputationMethod.None));
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            UpdateFieldsSource();
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }

    public class InverseBooleanConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return !((bool)value);
        }
    }
    
    public class ImputationMethodConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (ImputationMethod) value == (ImputationMethod) Enum.Parse(typeof(ImputationMethod), parameter.ToString(), true);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((bool) value)
                return (ImputationMethod) Enum.Parse(typeof (ImputationMethod), parameter.ToString(), true);
            return null;
        }
    }
}