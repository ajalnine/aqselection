﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_FormatBorders
{
    public class Parameters
    {
        public string TableName;
        public List<string> RightBordered;
        public List<string> LeftBordered;
        public List<string> HorizontalBordered;
        public List<string> ChangeBordered;
    }
    public class BorderItem : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool _right;
        public bool Right
        {
            get
            {
                return _right;
            }
            set
            {
                _right = value;
                NotifyPropertyChanged("Right");
            }
        }

        private bool _left;
        public bool Left
        {
            get
            {
                return _left;
            }
            set
            {
                _left = value;
                NotifyPropertyChanged("Left");
            }
        }

        private bool _horizontal;
        public bool Horizontal
        {
            get
            {
                return _horizontal;
            }
            set
            {
                _horizontal = value;
                NotifyPropertyChanged("Horizontal");
            }
        }
        private bool _change;
        public bool Change
        {
            get
            {
                return _change;
            }
            set
            {
                _change = value;
                NotifyPropertyChanged("Change");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
