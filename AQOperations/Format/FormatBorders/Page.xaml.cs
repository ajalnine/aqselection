﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_FormatBorders
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources; 
        private DataItem _selectedTable;
        private ObservableCollection<BorderItem> _columnsInfo;
        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { TableName = _selectedTable.Name, HorizontalBordered = new List<string>(), RightBordered = new List<string>(), LeftBordered = new List<string>(), ChangeBordered = new List<string>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {
            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    DataSourceSelector.UpdateLayout();
                }
            }

            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (newSource != null)
            {
                _columnsInfo = new ObservableCollection<BorderItem>();
                foreach (var f in newSource.Fields.Where(a=>!a.Name.StartsWith("#")))
                {
                    var ci = new BorderItem
                    {
                        FieldName = f.Name,
                        Left = _currentParameters.LeftBordered.Contains(f.Name),
                        Right = _currentParameters.RightBordered.Contains(f.Name),
                        Horizontal = _currentParameters.HorizontalBordered.Contains(f.Name),
                        Change = _currentParameters.ChangeBordered.Contains(f.Name)

                    };
                    ci.PropertyChanged += CiOnPropertyChanged;
                    _columnsInfo.Add(ci);

                }
                CombineSelector.ItemsSource = _columnsInfo;
            }
            CheckResult();
        }
        private void UIToParameters()
        {
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.RightBordered = _columnsInfo.Where(a => a.Right).Select(a => a.FieldName).ToList();
            _currentParameters.LeftBordered = _columnsInfo.Where(a => a.Left).Select(a => a.FieldName).ToList();
            _currentParameters.HorizontalBordered = _columnsInfo.Where(a => a.Horizontal).Select(a => a.FieldName).ToList();
            _currentParameters.ChangeBordered = _columnsInfo.Where(a => a.Change).Select(a => a.FieldName).ToList();
        }

        private void CiOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            CheckResult();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters(); 
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _currentParameters.LeftBordered.Clear();
            _currentParameters.RightBordered.Clear();
            _currentParameters.HorizontalBordered.Clear();
            _currentParameters.ChangeBordered.Clear();
            ParametersToUI();
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }


        private void CheckResult()
        {
            if (_currentParameters.ChangeBordered == null || _currentParameters.HorizontalBordered == null || _currentParameters.ChangeBordered == null ) return;
            UIToParameters();
            EnableExit(_currentParameters.RightBordered.Count() + _currentParameters.LeftBordered.Count() + _currentParameters.HorizontalBordered.Count() + _currentParameters.ChangeBordered.Count() != 0);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.RightBordered.Clear();
            _currentParameters.LeftBordered.Clear();
            _currentParameters.HorizontalBordered.Clear();
            _currentParameters.ChangeBordered.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            ParametersToUI();
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }
}