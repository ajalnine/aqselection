﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SLCalc_FormatCombine
{
    public class Parameters
    {
        public string TableName;
        public List<string> ColumnsToCombine;
        public bool EnableHierarchySubdivision;
    }
    public class CombinedItem : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private bool _isCombining;
        public bool IsCombining
        {
            get
            {
                return _isCombining;
            }
            set
            {
                _isCombining = value;
                NotifyPropertyChanged("IsCombining");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
