﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_FormatCombine
{
    
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private readonly List<DataItem> _inputs = new List<DataItem>();
        private readonly List<DataItem> _outputs = new List<DataItem>();
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var hasErrors = false;
            
            var selectedTable = availableData.Where(a => a != null).LastOrDefault(s => s.Name == _currentParameters.TableName);
            if (selectedTable == null) 
            {
                hasErrors = true;
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInput });
            }
            if (!hasErrors)
            {
                _inputs.Add(selectedTable);
                _outputs.Add(selectedTable);            
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
