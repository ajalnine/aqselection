﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_FormatCombine
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources; 
        private DataItem _selectedTable;
        private ObservableCollection<CombinedItem> _columnsInfo;
        public Page()
        {
            InitializeComponent();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters {TableName = _selectedTable.Name, ColumnsToCombine = new List<string>(), EnableHierarchySubdivision = true};
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {
            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    DataSourceSelector.UpdateLayout();
                }
            }

            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            if (newSource != null)
            {
                _columnsInfo = new ObservableCollection<CombinedItem>();
                foreach (var f in newSource.Fields.Where(a=>!a.Name.StartsWith("#")))
                {
                    var ci = new CombinedItem
                    {
                        FieldName = f.Name,
                        IsCombining = _currentParameters.ColumnsToCombine.Contains(f.Name)
                    };
                    ci.PropertyChanged += CiOnPropertyChanged;
                    _columnsInfo.Add(ci);

                }
                CombineSelector.ItemsSource = _columnsInfo;
            }
            EnableHierarchySubdivisionRadioButton.IsChecked = _currentParameters.EnableHierarchySubdivision;
            DisableHierarchySubdivisionRadioButton.IsChecked = !_currentParameters.EnableHierarchySubdivision;
            CheckResult();
        }
        private void UIToParameters()
        {
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.ColumnsToCombine =
                _columnsInfo.Where(a => a.IsCombining).Select(a => a.FieldName).ToList();
            if (EnableHierarchySubdivisionRadioButton.IsChecked != null)
                _currentParameters.EnableHierarchySubdivision = EnableHierarchySubdivisionRadioButton.IsChecked.Value;
        }

        private void CiOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            CheckResult();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void EnableExit(bool enable)
        {
            OKButton.IsEnabled = enable;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters(); 
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _currentParameters.ColumnsToCombine.Clear();
            ParametersToUI();
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }


        private void CheckResult()
        {
            if (_currentParameters.ColumnsToCombine == null ) return;
            UIToParameters();
            EnableExit(_currentParameters.ColumnsToCombine.Count() != 0);
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.ColumnsToCombine.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            ParametersToUI();
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }
    }
}