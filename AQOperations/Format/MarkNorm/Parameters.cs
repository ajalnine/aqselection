﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace SLCalc_MarkNorm
{
    public class Parameters
    {
        public string TableName;
        public Color ColorInRange;
        public Color ColorOutRange;
        public bool MarkInRange;
        public bool MarkOutRange;
        public bool AllRow;
    }
}
