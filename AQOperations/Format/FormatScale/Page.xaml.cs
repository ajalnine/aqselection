﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_FormatScale
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private DataItem _selectedTable;
        private int _editingIndex = -1;

        public static readonly DependencyProperty NewRuleProperty =
        DependencyProperty.Register("NewRule", typeof(ScaleRule), typeof(Page), new PropertyMetadata(new ScaleRule { Extended = false, Mode = ScaleRuleMode.MinMax, 
            Color1 = Color.FromArgb(0xff, 0xdf, 0x21, 0x10), 
            Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
            Color3 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10)
        }));

        public ScaleRule NewRule
        {
            get { return (ScaleRule)GetValue(NewRuleProperty); }
            set { SetValue(NewRuleProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            NewRulePanel.DataContext = NewRule;
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
             _currentParameters = new Parameters { TableName = _selectedTable.Name, Rules = new ObservableCollection<ScaleRule>()};
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {
            
            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName); 

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }
            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);
            
            if (newSource != null)
            {
                FieldSelector.ItemsSource = newSource.Fields.Where(a => !a.Name.StartsWith("#")).Select(a=>a.Name);
                FieldSelector.UpdateLayout(); 
                FieldSelector.SelectedIndex = 0;
                FieldSelector.UpdateLayout();
            }
            RuleListBox.ItemsSource = _currentParameters.Rules;
            CheckModesAvailability();
            CheckResult();
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null)
                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }
        
        private void CheckResult()
        {
            UIToParameters();
            EnableExit(_currentParameters.Rules.Count() != 0);
        }
        
        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null || DataSourceSelector.SelectedItem == null) return;
            _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.Rules.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            ParametersToUI();
        }
        public void DataRequestReadyCallback(string fileName) { }
        public void StateChanged(OperationViewMode ovm) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase) sender).Tag as ScaleRule;
            _currentParameters.Rules.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
        }
        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1) StoreRule();
            var toEdit = ((TextImageButtonBase)sender).Tag as ScaleRule;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.Rules.IndexOf(toEdit);
            _currentParameters.Rules.Remove(toEdit);

            NewRule = toEdit;

            NewRulePanel.DataContext = NewRule;
            CheckResult();
        }
        private void FieldSelector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_selectedTable ==null || _selectedTable.Fields == null || FieldSelector == null || FieldSelector.SelectedValue == null) return;
            CheckModesAvailability();
        }

        private void CheckModesAvailability()
        {
            var field = FieldSelector.SelectedValue;
            var fieldList = _selectedTable.Fields.Select(a => a.Name).ToList();
            if (fieldList.Contains(field + "_НГ") || fieldList.Contains(field + "_ВГ") ||
                fieldList.Contains("#" + field + "_НГ") || fieldList.Contains("#" + field + "_ВГ"))
            {
                NormRadioButton.IsEnabled = true;
            }
            else
            {
                NormRadioButton.IsEnabled = false;
                if (NewRule.Mode == ScaleRuleMode.Range) NewRule.Mode = ScaleRuleMode.MinMax;
            }

            var dataField = _selectedTable.Fields.SingleOrDefault(a=>a.Name == (string) field);
            if (dataField != null && !dataField.DataType.EndsWith("Double"))
            {
                NuberRadioButton.IsEnabled = false;
                if (NewRule.Mode == ScaleRuleMode.Number) NewRule.Mode = ScaleRuleMode.MinMax;
            }
            else
            {
                NuberRadioButton.IsEnabled = true; 
            }
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (StoreRule()) return;
            CheckResult();
        }

        private bool StoreRule()
        {
            var binding = MinTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding && !string.IsNullOrEmpty(MinTextBox.Text)) binding.UpdateSource();
            var binding2 = MaxTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding2 && !string.IsNullOrEmpty(MaxTextBox.Text)) binding2.UpdateSource();
            var binding3 = MedTextBox.GetBindingExpression(TextBox.TextProperty);
            if (null != binding3 && !string.IsNullOrEmpty(MedTextBox.Text)) binding3.UpdateSource();

            if (NewRule.Mode == ScaleRuleMode.Number &&
                (NewRule.Med < NewRule.Min || NewRule.Max < NewRule.Min || NewRule.Max < NewRule.Med))
            {
                MessageBox.Show("Интервал указан некорректно");
                return true;
            }
            var rule = new ScaleRule
            {
                Color1 = NewRule.Color1,
                Color2 = NewRule.Color2,
                Color3 = NewRule.Color3,
                Extended = NewRule.Extended,
                FieldName = NewRule.FieldName,
                Mode = NewRule.Mode,
                Min = NewRule.Min,
                Med = NewRule.Med,
                Max = NewRule.Max
            };


            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.Rules.Add(rule);
                    break;
                default:
                    _currentParameters.Rules.Insert(_editingIndex, rule);
                    _editingIndex = -1;
                    break;
            }
            return false;
        }

        private void ModeButton_OnClick(object sender, RoutedEventArgs e)
        {
            var p = ((RadioButton) sender).Tag.ToString();
            switch (p)
            {
                case "MinMax":
                    NumbersPanel.Visibility = Visibility.Collapsed;
                    ExtendedCheckBox.Visibility = Visibility.Collapsed;
                    return;

                case "Range":
                    NumbersPanel.Visibility = Visibility.Collapsed;
                    ExtendedCheckBox.Visibility = Visibility.Visible;
                    return;

                case "Number":
                    NumbersPanel.Visibility = Visibility.Visible;
                    ExtendedCheckBox.Visibility = Visibility.Visible;
                    return;
            }
        }

        private void UpRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as ScaleRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : RuleListBox.Items.Count - 1;
            MoveRule(selectedIndex, nextIndex);
        }

        private void DownRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as ScaleRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < RuleListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveRule(selectedIndex, nextIndex);
        }

        private void TopRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as ScaleRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveRule(selectedIndex, nextIndex);
        }

        private void BottomRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as ScaleRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = RuleListBox.Items.Count - 1;
            MoveRule(selectedIndex, nextIndex);
        }

        private void MoveRule(int selectedIndex, int nextIndex)
        {
            var newItem = RuleListBox.SelectedItem as ScaleRule;
            _currentParameters.Rules.RemoveAt(selectedIndex);
            _currentParameters.Rules.Insert(nextIndex, newItem);
            RuleListBox.SelectedItem = newItem;
            RuleListBox.ScrollIntoView(RuleListBox.SelectedItem);
            RuleListBox.UpdateLayout();
        }
    }
    public class ModeConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var  mode = (ScaleRuleMode)value;
            switch (mode)
            {
                case ScaleRuleMode.MinMax:
                    return "Min Max";
                case ScaleRuleMode.Number:
                    return "Интервал";
                case ScaleRuleMode.Range:
                    return "Нормы";
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class ModeButtonConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = parameter.ToString();
            switch (p)
            {
                case "MinMax":
                    return (ScaleRuleMode)value == ScaleRuleMode.MinMax;
                case "Number":
                    return (ScaleRuleMode)value == ScaleRuleMode.Number;
                case "Range":
                    return (ScaleRuleMode)value == ScaleRuleMode.Range;
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var p = parameter.ToString();
            switch (p)
            {
                case "MinMax":
                    return (bool)value ? (object)ScaleRuleMode.MinMax : null;
                case "Number":
                    return (bool)value ? (object)ScaleRuleMode.Number : null;
                case "Range":
                    return (bool)value ? (object)ScaleRuleMode.Range : null;
            }
            return null;
        }
    }

    public class ModeRangeVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((ScaleRuleMode)value == ScaleRuleMode.Number) ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

    public class ExtendedVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}