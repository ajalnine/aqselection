﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace SLCalc_FormatScale
{
    public class Parameters
    {
        public string TableName;
        public ObservableCollection<ScaleRule> Rules;
    }
    public class ScaleRule : INotifyPropertyChanged
    {
        private string _fieldname;
        public string FieldName
        {
            get
            {
                return _fieldname;
            }
            set
            {
                _fieldname = value;
                NotifyPropertyChanged("FieldName");
            }
        }

        private double? _min;
        public double? Min
        {
            get
            {
                return _min;
            }
            set
            {
                _min = value;
                NotifyPropertyChanged("Min");
            }
        }

        private double? _max;
        public double? Max
        {
            get
            {
                return _max;
            }
            set
            {
                _max = value;
                if (Max.HasValue && Med.HasValue && Max < Med) throw new Exception("Меньше среднего");
                if (Max.HasValue && Min.HasValue && Max < Min) throw new Exception("Меньше минимального");
                NotifyPropertyChanged("Max");
            }
        }

        private double? _med;
        public double? Med
        {
            get
            {
                return _med;
            }
            set
            {
                _med = value;
                if (Med.HasValue && Min.HasValue && Med<Min)throw new Exception("Меньше минимального");
                NotifyPropertyChanged("Med");
            }
        }

        private Color _color1;
        public Color Color1
        {
            get
            {
                return _color1;
            }
            set
            {
                _color1 = value;
                NotifyPropertyChanged("Color1");
            }
        }

        private Color _color2;
        public Color Color2
        {
            get
            {
                return _color2;
            }
            set
            {
                _color2 = value;
                NotifyPropertyChanged("Color2");
            }
        }

        private Color _color3;
        public Color Color3
        {
            get
            {
                return _color3;
            }
            set
            {
                _color3 = value;
                NotifyPropertyChanged("Color3");
            }
        }

        private ScaleRuleMode _mode;
        public ScaleRuleMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                NotifyPropertyChanged("Mode");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

       private bool _extended;

        public bool Extended
        {
            get
            {
                return _extended;
            }
            set
            {
                _extended = value;
                NotifyPropertyChanged("Extended");
            }
        }
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public enum ScaleRuleMode
    {
        MinMax, Range, Number
    }
}
