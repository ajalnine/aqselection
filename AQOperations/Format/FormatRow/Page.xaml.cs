﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using AQBasicControlsLibrary;
using AQCalculationsLibrary;

namespace SLCalc_FormatRow
{
    public partial class Page : IDataFlowUI
    {
        public event DataRequestDelegate DataRequest;
        public event NameChangedDelegate NameChanged;
        public event ParametersAcceptedDelegate ParametersAccepted;
        private readonly CalculationService _cs;

        private Parameters _currentParameters;
        private List<DataItem> _dataSources;
        private List<DataItem> _availableData;
        private DataItem _selectedTable;
        private int _editingIndex = -1;

        public static readonly DependencyProperty NewRuleProperty =
        DependencyProperty.Register("NewRule", typeof(RowFormatRule), typeof(Page), new PropertyMetadata(new RowFormatRule
        {
            Color = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
            Expression = string.Empty,
            Code = string.Empty
        }));

        public RowFormatRule NewRule
        {
            get { return (RowFormatRule)GetValue(NewRuleProperty); }
            set { SetValue(NewRuleProperty, value); }
        }

        public Page()
        {
            InitializeComponent();
            NewRulePanel.DataContext = NewRule;
            _cs = Services.GetCalculationService();
        }

        public void SetupUI(List<DataItem> availableData)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            _selectedTable = _dataSources.FirstOrDefault();
            SetDefaultParameters();
            ParametersToUI(availableData);
        }

        private void SetDefaultParameters()
        {
            _currentParameters = new Parameters { TableName = _selectedTable.Name, Rules = new ObservableCollection<RowFormatRule>() };
        }

        public void SetupUI(List<DataItem> availableData, string parametersXml)
        {
            _availableData = availableData;
            _dataSources = availableData.Where(a => a != null).Where(s => s.DataItemType == DataType.Selection && !s.Name.StartsWith("#")).ToList();
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            ParametersToUI(availableData);
        }

        public void ParametersToUI(List<DataItem> availableData)
        {
            ParametersToUI();
        }

        private void ParametersToUI()
        {

            if (_dataSources.Select(s => s.Name).Contains(_currentParameters.TableName))
            {
                DataSourceSelector.SelectionChanged -= DataSourceSelector_SelectionChanged;
                DataSourceSelector.ItemsSource = (from di in _dataSources select di.Name);
                DataSourceSelector.SelectedItem = _currentParameters.TableName;
                _selectedTable = _dataSources.SingleOrDefault(a => a.Name == _currentParameters.TableName);

                DataSourceSelector.SelectionChanged += DataSourceSelector_SelectionChanged;
            }
            else
            {
                var source = (from di in _dataSources select di.Name).ToList();
                if (source.Any())
                {
                    DataSourceSelector.ItemsSource = source;
                    DataSourceSelector.UpdateLayout();
                    DataSourceSelector.SelectedIndex = 0;
                    _selectedTable = _dataSources.First();
                    DataSourceSelector.UpdateLayout();
                }
            }

            var newSource = _dataSources.SingleOrDefault(s => s.Name == _currentParameters.TableName);

            if (newSource != null)
            {
                MainExpressionBuilder.SetDataItems(_selectedTable, _availableData, null);
                MainExpressionBuilder.SetExpressionEditor(NewRule.Expression);
                MainExpressionBuilder.SetCodeEditor(NewRule.Code);
            }
            RuleListBox.ItemsSource = _currentParameters.Rules;
            CheckResult();
            MainExpressionBuilder.StartHelp();
        }

        private void UIToParameters()
        {
            if (DataSourceSelector != null && DataSourceSelector.SelectedItem != null)
                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
        }

        public string GetParametersXML()
        {
            var xs = new XmlSerializer(typeof(Parameters));
            var sb = new StringBuilder();
            xs.Serialize(XmlWriter.Create(sb), _currentParameters);
            return sb.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UIToParameters();
            ParametersAccepted.Invoke(this, new ParametersAcceptedEventArgs { CloseUI = true, ParametersXML = GetParametersXML() });
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        private void CheckResult()
        {
            UIToParameters();
            EnableExit(_currentParameters.Rules.Count() != 0);
        }

        private void EnableExit(bool enable)
        {
            OkButton.IsEnabled = enable;
        }

        private void DataSourceSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataSourceSelector == null) return;

            MainExpressionBuilder.SetDataItems(_dataSources.Where(a => a != null).SingleOrDefault(s => s.Name == _currentParameters.TableName), _availableData, null);
            if (DataSourceSelector.SelectedItem != null)

                _currentParameters.TableName = DataSourceSelector.SelectedItem.ToString();
            _currentParameters.Rules.Clear();
            _selectedTable = _dataSources.LastOrDefault(a => a.Name == _currentParameters.TableName);
            AddRuleButton.IsEnabled = false;
            ParametersToUI();

        }
        public void DataRequestReadyCallback(string fileName) { }

        private void RemoveValueFromListButton_OnClick(object sender, RoutedEventArgs e)
        {
            var toDelete = ((TextImageButtonBase)sender).Tag as RowFormatRule;
            _currentParameters.Rules.Remove(toDelete);
            CheckResult();
            _editingIndex = -1;
        }
        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (_editingIndex > -1)StoreRule();
            var toEdit = ((TextImageButtonBase)sender).Tag as RowFormatRule;
            if (toEdit == null) return;
            _editingIndex = _currentParameters.Rules.IndexOf(toEdit);
            _currentParameters.Rules.Remove(toEdit);

            NewRule = toEdit;

            MainExpressionBuilder.SetExpressionEditor(NewRule.Expression);
            MainExpressionBuilder.SetCodeEditor(NewRule.Code);
            NewRulePanel.DataContext = NewRule;
            CheckResult();
        }

        private void MainExpressionBuilder_ExpressionChanged(object sender, EventArgs e)
        {
            AddRuleButton.IsEnabled = false;
        }

        public void MainExpressionBuilder_EditFinished(object o, EditFinishedEventArgs e)
        {
            ProgressIndicator.Visibility = Visibility.Visible;
            _cs.BeginCheckExpression(e.CSharpText, _dataSources, _currentParameters.TableName, CheckExpressionDone, new object());
        }

        private void CheckExpressionDone(IAsyncResult iar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                var sr = _cs.EndCheckExpression(iar);
                ProgressIndicator.Visibility = Visibility.Collapsed;
                if (sr.Success && sr.Message != "null" && sr.Message == "Boolean")
                {
                    AddRuleButton.IsEnabled = true;
                }
                else
                {
                    if (!sr.Success)
                    {
                        AddRuleButton.IsEnabled = false;
                        MessageBox.Show(sr.Message, "Ошибки компиляции", MessageBoxButton.OK);
                    }
                    else if (sr.Message != "Boolean") MessageBox.Show("Результат должен быть двоичного типа Истина/Ложь", "Ошибка выражения", MessageBoxButton.OK);
                }
            });
        }

        public void StateChanged(OperationViewMode ovm)
        {
            if (ovm == OperationViewMode.Edit) MainExpressionBuilder.StartHelp();
            else MainExpressionBuilder.StopHelp();
        }

        private void AddRuleButton_OnClick(object sender, RoutedEventArgs e)
        {
            StoreRule();
            CheckResult();
        }

        private void StoreRule()
        {
            var rule = new RowFormatRule
            {
                Color = NewRule.Color,
                Expression = MainExpressionBuilder.GetExpression(),
                Code = MainExpressionBuilder.GetCode(),
            };

            switch (_editingIndex)
            {
                case -1:
                    _currentParameters.Rules.Add(rule);
                    break;
                default:
                    _currentParameters.Rules.Insert(_editingIndex, rule);
                    _editingIndex = -1;
                    break;
            }
        }

        private void UpRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as RowFormatRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex > 0) ? selectedIndex - 1 : RuleListBox.Items.Count - 1;
            MoveRule(selectedIndex, nextIndex);
        }

        private void DownRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as RowFormatRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = (selectedIndex < RuleListBox.Items.Count - 1) ? selectedIndex + 1 : 0;
            MoveRule(selectedIndex, nextIndex);
        }

        private void TopRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as RowFormatRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            const int nextIndex = 0;
            MoveRule(selectedIndex, nextIndex);
        }

        private void BottomRule_Click(object sender, RoutedEventArgs e)
        {
            if (RuleListBox.SelectedItem == null) return;
            var selected = RuleListBox.SelectedItem as RowFormatRule;
            if (selected == null) return;
            var selectedIndex = RuleListBox.Items.IndexOf(selected);
            var nextIndex = RuleListBox.Items.Count - 1;
            MoveRule(selectedIndex, nextIndex);
        }

        private void MoveRule(int selectedIndex, int nextIndex)
        {
            var newItem = RuleListBox.SelectedItem as RowFormatRule;
            _currentParameters.Rules.RemoveAt(selectedIndex);
            _currentParameters.Rules.Insert(nextIndex, newItem);
            RuleListBox.SelectedItem = newItem;
            RuleListBox.ScrollIntoView(RuleListBox.SelectedItem);
            RuleListBox.UpdateLayout();
        }
    }
}