﻿using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQCalculationsLibrary;

namespace SLCalc_FormatRow
{
    
    public class DataFlow : IDataFlow
    {
        public event DataFlowFinishedDelegate DataFlowFinished;
        public event DataFlowErrorDelegate DataFlowError;

        private List<DataItem> _inputs = new List<DataItem>();
        private readonly List<DataItem> _outputs = new List<DataItem>();
        private Parameters _currentParameters;

        public void DataProcess(List<DataItem> availableData, string parametersXml)
        {
            if (parametersXml == null)
            {
                if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = null, Outputs = null, Deleted = null });
                return;
            }
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(parametersXml);
            _currentParameters = (Parameters)xs.Deserialize(XmlReader.Create(sr));
            var selectedTable = availableData.Where(a => a != null).LastOrDefault(s => s.Name == _currentParameters.TableName);
            var resultingTokens = new List<Token>();
            foreach (var rule in _currentParameters.Rules)
            {
                resultingTokens.InsertRange(0, Tokens.ParseTokens(rule.Expression, availableData, selectedTable));                
            }

            if (Tokens.CheckTokens(resultingTokens))
            {
                var output = selectedTable;
                _outputs.Add(output);
                _inputs = UsedDataItemsHelper.GetUsedTables(resultingTokens, selectedTable);
            }
            else
            {
                if (DataFlowError != null) DataFlowError.Invoke(this, new DataFlowErrorEventArgs { Error = InconsistenceState.LostInputFields });
            }
            if (DataFlowFinished != null) DataFlowFinished.Invoke(this, new DataFlowFinishedEventArgs { Inputs = _inputs, Outputs = _outputs, Deleted = null });
        }
    }
}
