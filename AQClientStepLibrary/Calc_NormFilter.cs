﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQReportingLibrary;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_NormFilter : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            var selectedTable = data.SingleOrDefault(a => a.TableName == p.TableName);
            if (p.Field!=null && !p.Include && !p.Case && !p.All && !p.InRange) selectedTable.RemoveOutOfRanges(p.Field);
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public bool Include;
            public bool Case;
            public bool All;
            public bool InRange;
            public string Field;
        }
    }
}
