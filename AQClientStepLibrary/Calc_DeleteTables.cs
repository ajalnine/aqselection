﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_DeleteTables : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            int i = 0;
            foreach (var tt in p.TablesTransformation)
            {
                var t = data.SingleOrDefault(a => a.TableName == tt.OldTableName);
                if (t==null)continue;
                t.TableName = tt.NewTableName;
                data.Remove(t);
                data.Insert(i, t);
                i++;
            }
            foreach (var td in p.TablesToDelete)
            {
                var t = data.SingleOrDefault(a => a.TableName == td);
                if (t!=null)data.Remove(t);
            }
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public ObservableCollection<string> TablesToDelete;
            public ObservableCollection<NewTableData> TablesTransformation;
        }

        public class NewTableData
        {
            public string OldTableName;
            public string NewTableName;
        }
    }
}
