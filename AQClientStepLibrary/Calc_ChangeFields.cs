﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_ChangeFields : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            var table = data.SingleOrDefault(a=>a.TableName==p.TableName);
            if (table == null) return;
            var index = data.IndexOf(table);
            foreach (var c in p.DeletedFields)
            {
                table.DeleteColumn(c);
            }

            foreach (var f in p.FieldTransformation)
            {
                table.RenameColumn(f.OldFieldName, f.NewFieldName);
            }

            var res = table.ReorderColumns(p.FieldTransformation.Select(a=>a.NewFieldName).ToList());
            data.RemoveAt(index);
            data.Insert(index, res);
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public ObservableCollection<string> DeletedFields;
            public ObservableCollection<NewTableData> FieldTransformation;
        }

        public class NewTableData
        {
            public string OldFieldName;
            public string NewFieldName;
        }
    }
}
