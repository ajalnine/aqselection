﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_SplitTable : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            var selectedTable = data.SingleOrDefault(a => a.TableName == p.TableName);
            if (selectedTable == null) return;

            SafeAppendNewData(data, selectedTable.SplitByLayers(p.CategoryFieldName, p.Categories.OfType<object>().ToList()));
        }

        private void SafeAppendNewData(List<SLDataTable> dataTables, List<SLDataTable> newTables)
        {
            if (newTables == null) return;
            var intersection = dataTables.Select(a => a.TableName).Intersect(newTables.Select(a => a.TableName)).ToList();
            if (!intersection.Any())
            {
                dataTables.InsertRange(dataTables.Count, newTables);
            }
            else
            {
                foreach (var doubleTable in intersection)
                {
                    var index = dataTables.IndexOf(dataTables.Single(a => a.TableName == doubleTable));
                    if (index >= 0)
                    {
                        dataTables.RemoveAt(index);
                        dataTables.Insert(index, newTables.Single(a => a.TableName == doubleTable));
                    }
                }
            }
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string CategoryFieldName;
            public string OutputTablePrefix;
            public ObservableCollection<string> Categories;
            public bool RemoveCategory;
            public bool RemoveTable;
        }
    }
}
