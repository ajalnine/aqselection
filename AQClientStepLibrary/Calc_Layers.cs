﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQChartLibrary;
using AQMathClasses;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_Layers : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            var table = data.SingleOrDefault(a => a.TableName == p.TableName);
            if (table == null) return;
            var cases = table.GetRowsForAllValuesFromLayers(p.LayerField, p.Layers);

            switch (p.Operation)
            {
                case InteractiveOperations.CutInNewTable:
                    var newTable1 = table.CutToNewTable(p.LayerField, cases);
                    var desc = p.LayerField + ": " + AQMath.Concatenate(p.Layers, ";");
                    newTable1.TableName += ", " + ((desc.Length < 50) ? desc : desc.Substring(0, 47) + "...");
                    data.Add(newTable1);
                    break;

                case InteractiveOperations.CopyToNewTable:
                    var newTable2 = table.CopyToNewTable(p.LayerField, cases);
                    var desc2 = p.LayerField + ": " + AQMath.Concatenate(p.Layers, ";");
                    newTable2.TableName += ", " + ((desc2.Length < 50) ? desc2 : desc2.Substring(0, 47) + "...");
                    data.Add(newTable2);
                    break;

                case InteractiveOperations.DeleteCase:
                    table.DeleteCases(p.LayerField, cases);
                    break;

                case InteractiveOperations.SetLabel:
                    table.SetLabel(p.LayerField, p.YField, cases, p.OptionalCommandArgument?.ToString());
                    break;

                case InteractiveOperations.CombineLayers:
                    table.SetFieldValue(p.YField, cases, p.OptionalCommandArgument);
                    break;

                case InteractiveOperations.SetColor:
                    
                    switch (p.ColorMode)
                    {
                        case ColorMode.SetForValue:
                            table.SetColor(p.LayerField , p.YField, cases, p.Color.ToString());
                            break;

                        case ColorMode.ResetFromValues:
                            table.SetColor(p.LayerField, p.YField, cases, null);
                            break;

                        case ColorMode.SetForCase:
                            table.SetColorForRow(cases, p.Color.ToString());
                            break;

                        case ColorMode.ResetFromCases:
                            table.SetColorForRow(cases, null);
                            break;
                    }
                    break;
                case InteractiveOperations.DeleteValue:
                    table.DeleteValues(p.YField, cases);
                    break;
            }
        }
        
        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string LayerField;
            public string YField;
            public InteractiveOperations Operation;
            public object OptionalCommandArgument;
            public List<object> Layers;
            public ColorMode ColorMode;
            public Color Color;
        }

        public enum ColorMode
        {
            SetForCase, SetForValue, ResetFromCases, ResetFromValues
        }

        [Flags]
        public enum InteractiveOperations
        {
            None = 0,
            DeleteValue = 1,
            DeleteCase = 2,
            CutInNewTable = 4,
            CopyToNewTable = 8,
            SetColor = 0x10,
            SetColorForRow = 0x20,
            SetLabel = 0x40,
            SplitLayers = 0x80,
            DeleteColumn = 0x100,
            SetFilter = 0x200,
            DeleteFilter = 0x400,
            RemoveFilters = 0x800,
            CombineLayers = 0x1000,
            Outliers = 0x2000,
            OutOfRange = 0x4000,
            All = 0xffff

        }
    }
}
