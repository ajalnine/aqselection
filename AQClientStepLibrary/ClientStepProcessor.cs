﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.CalculationServiceReference;

namespace AQClientStepLibrary
{
    public static class ClientStepProcessor
    {
        public static void ProcessStep(List<SLDataTable> data, Step step)
        {
            if (step.Group == "Параметры") return;
            var t = Type.GetType("AQClientStepLibrary." + step.Calculator);
            var calculator = Activator.CreateInstance(t) as IClientStepExecution;
            calculator?.Process(data, step.ParametersXML);
        }
    }

    public interface IClientStepExecution
    {
        void Process(List<SLDataTable> data, string parametersXML);
    }
}
