﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;
using ApplicationCore.CalculationServiceReference;
using AQReportingLibrary;

namespace AQClientStepLibrary
{
    // ReSharper disable once InconsistentNaming
    public class Calc_SetRanges : IClientStepExecution
    {
        public void Process(List<SLDataTable> data, string parametersXML)
        {
            var p = ParseParameters(parametersXML);
            var selectedTable = data.SingleOrDefault(a => a.TableName == p.TableName);
            if (selectedTable == null) return;
            foreach (var range in p.Ranges)
            {
                if (!(range.Min.HasValue&& double.IsNaN(range.Min.Value)))
                {
                    var value = range.Min;
                    var index = selectedTable.ColumnNames.IndexOf("#" + range.Parameter + "_НГ");
                    if (index == -1) index = selectedTable.ColumnNames.IndexOf(range.Parameter + "_НГ");
                    if (index == -1)
                    {
                        selectedTable.ColumnNames.Add("#" + range.Parameter + "_НГ");
                        selectedTable.ColumnTags?.Add(null);
                        selectedTable.DataTypes.Add("Double");
                        index = selectedTable.ColumnNames.Count - 1;
                        FillValue(selectedTable, p.GroupField, index, value, true, range.Group);
                    }
                    else FillValue(selectedTable, p.GroupField, index, value, false, range.Group);
                }
                if (!(range.Max.HasValue && double.IsNaN(range.Max.Value)))
                {
                    var value = range.Max;
                    var index = selectedTable.ColumnNames.IndexOf("#" + range.Parameter + "_ВГ");
                    if (index == -1) index = selectedTable.ColumnNames.IndexOf(range.Parameter + "_ВГ");
                    if (index == -1)
                    {
                        selectedTable.ColumnNames.Add("#" + range.Parameter + "_ВГ");
                        selectedTable.ColumnTags?.Add(null);
                        selectedTable.DataTypes.Add("Double");
                        index = selectedTable.ColumnNames.Count - 1;
                        FillValue(selectedTable, p.GroupField, index, value, true, range.Group);
                    }
                    else FillValue(selectedTable, p.GroupField, index, value, false, range.Group);
                }
            }
        }

        private void FillValue(SLDataTable table,  string groupField, int index, double? value, bool needAppend, string layer)
        {
            object toInsert = (value.HasValue ? (object)value.Value : null);
            if (groupField == "Нет")
            {
                foreach (var row in table.Table)
                {
                    if (needAppend) row.Row.Add(toInsert);
                    else row.Row[index] = toInsert;
                }
            }
            else
            {
                var layerIndex = table.ColumnNames.IndexOf(groupField);
                foreach (var row in table.Table)
                {
                    if (row.Row[layerIndex]?.ToString() != layer)
                    {
                        if (needAppend) row.Row.Add(null);
                        continue;
                    }
                    if (needAppend) row.Row.Add(toInsert);
                    else row.Row[index] = toInsert;
                }
            }
        }

        public Parameters ParseParameters(string xmlParameters)
        {
            if (xmlParameters == null) return null;
            var xs = new XmlSerializer(typeof(Parameters));
            var sr = new StringReader(xmlParameters);
            return (Parameters)xs.Deserialize(XmlReader.Create(sr));
        }

        public class Parameters
        {
            public string TableName;
            public string GroupField;
            public string Groups;
            public ObservableCollection<Range> Ranges;
        }

        public class Range
        {
            public double? Min;
            public double? Max;
            public string Parameter;
            public string Group;
        }
    }
}
