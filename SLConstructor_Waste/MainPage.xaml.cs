﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using AQControlsLibrary;
using AQConstructorsLibrary;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore.CalculationServiceReference;

namespace SLConstructor_Waste
{
    public partial class MainPage : IAQModule
    {
        #region Переменные
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;

        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;
        private readonly WasteParametersList _wasteParameters = new WasteParametersList();
        private readonly AQModuleDescription _aqmd;
        private List<Step> _steps;
        private List<DefectDescription> _defectList;
        private DefectSelectionType _selectionType;
        private DefectField _defectField;
        private Filters _filters;
        private bool _includeDeleted;
        private bool _includeDefectListField;
        private bool _formatted;
        private bool _colored;
        private string _dateField = "Дата списания";
        private string _sql = string.Empty;
        
        #endregion

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            ColumnsPresenter.SelectionParameters = _wasteParameters;
            InitializeCustomDateRangePanel();
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
        }

        private void InitializeCustomDateRangePanel()
        {
            var g = ColumnsPresenter.GetDateRangeCustomPanel();
            g.HorizontalAlignment = HorizontalAlignment.Center;
            var sp = Resources["DateRangeModeSelector"] as StackPanel;
            Resources.Remove("DateRangeModeSelector");
            g.Children.Add(sp);
        }

        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery();
        }

        private void RefreshQuery()
        {
            RefreshFlags();
            RefreshFilters();
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса"}, "Проверка условий", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _sql = ConstructSQL(true);
            _cs.BeginGetRecordCount(_sql, GetRecordCountDone, task);
        }

        private void GetRecordCountDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetRecordCount(iar);
            if (t > 0)
            {
                task.SetState(0, TaskState.Ready);
                task.SetMessage(string.Format("{0} записей", t));
                Dispatcher.BeginInvoke(() =>
                {
                    EnableSaveAndExport(true);
                    RefreshQueryButton.IsEnabled = true;
                    ColumnsPresenter.EnableFinishSelection(true);
                });
                return;
            }
            task.SetState(0, TaskState.Error);
            task.SetMessage("Нет данных");
            ColumnsPresenter.EnableFinishSelection(true);
        }

        private void RefreshFlags()
        {
            if (AddRemovedRecordsCheckBox.IsChecked.HasValue) _includeDeleted = AddRemovedRecordsCheckBox.IsChecked.Value;
            if (IncludeDefectList.IsChecked.HasValue) _includeDefectListField = IncludeDefectList.IsChecked.Value;
            _selectionType = (DefectSelectionType)Enum.Parse(typeof(DefectSelectionType), SelectionType.SelectedIndex.ToString(CultureInfo.InvariantCulture), true);

                 if (UnitIsPercentBilletButton.IsChecked != null && UnitIsPercentBilletButton.IsChecked.Value) _defectField = DefectField.PercentBillet;
            else if (UnitIsPercentIngotButton.IsChecked != null && UnitIsPercentIngotButton.IsChecked.Value)  _defectField = DefectField.PercentIngot;
            else if (UnitIsBilletWeightButton.IsChecked != null && UnitIsBilletWeightButton.IsChecked.Value)  _defectField = DefectField.Billet;
            else if (UnitIsIngotWeightButton.IsChecked != null && UnitIsIngotWeightButton.IsChecked.Value)   _defectField = DefectField.Ingot;
            if (NoFormatButton.IsChecked != null) _formatted = !NoFormatButton.IsChecked.Value;
            if (ScaleButton.IsChecked != null) _colored = ScaleButton.IsChecked.Value;
        }

        private void RefreshFilters()
        {
            if (ColumnsPresenter == null) return;
            _filters = ColumnsPresenter.GetFilters();
        }

        
        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery();
        }

        #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(() =>
            {
                XlsxExportButton.IsEnabled = allow;
                PreviewButton.IsEnabled = allow;
                SaveQueryButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
            });
        }

        #endregion

        #region Построение запроса

        private string ConstructSQL(bool checkOnly)
        {
            if (checkOnly) return WasteSelects.GetCheckSelect(_filters.WorkFilter);
            
            switch (_selectionType)
            {
                case DefectSelectionType.Details:
                    return WasteSelects.GetDetailedSelect(_filters.SaveFilter, _includeDeleted);
                case DefectSelectionType.MeltSum:
                    return WasteSelects.GetMeltSumSelect(_filters.SaveFilter, _includeDefectListField);
                case DefectSelectionType.Defects:
                    return WasteSelects.GetDefectsSelect(_filters.SaveFilter, _defectField, _defectList);
                default:
                    return "select null";
            }
        }

        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }

        private void RegisterDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _wasteParameters.DateField = "Waste_Notices.RegisterDate";
            _dateField = "Дата выплавки";
            if (ColumnsPresenter != null)
            {
                ColumnsPresenter.RefreshFirstFilter();
                RefreshFilters();
            }
        }

        private void DebitDateSelectionButton_Checked(object sender, RoutedEventArgs e)
        {
            _wasteParameters.DateField = "Waste_Notices.DebitDate";
            _dateField = "Дата списания";
            if (ColumnsPresenter != null)
            {
                ColumnsPresenter.RefreshFirstFilter();
                RefreshFilters();
            }
        }

    }
    public enum DefectSelectionType { Details = 0, MeltSum = 1, Defects = 2 };
    public enum DefectField { Billet = 0, Ingot = 1, PercentBillet = 2, PercentIngot = 3 };
}
