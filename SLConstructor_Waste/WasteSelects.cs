﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;

namespace SLConstructor_Waste
{
    public static class WasteSelects
    {
        public static string GetCheckSelect(string sqlFilter)
        {
            var s = @"SELECT Waste_Notices.SmeltNumber as 'Плавка'";
            s += @" FROM Waste_Notices where Waste_Notices.NoticeWasDeleted=0 and (" + sqlFilter + ")";
            return s;
        }

        public static string GetDetailedSelect(string sqlFilter, bool includeDeleted)
        {
            var s = @"SELECT Waste_Notices.SmeltNumber as 'Плавка'
               ,tw.WorkShop as 'Цех выплавки'
              ,Common_Mark.Mark as 'Марка'
              ,cn.Ntd as 'НТД'
              ,tbt.BottlingType as 'Разливка'
              ,r3s1.ShopName as 'Цех виновник'
              ,r3cp.CostPlaceName as 'МВЗ участка-виновника'
              ,r3s2.ShopName as 'Цех списания'
              ,r3wh.WareHouseName as 'Склад списания'
              ,Waste_Notices.DebitDate as 'Дата списания'
              ,wwt.WasteType as 'Вид брака в цехе списания'
              ,wdg.DefectGroup as 'Группа дефекта'
              ,wd.Defect as 'Дефект'
              ,wct.CancellationType as 'Тип списания'
              ,wp.Profile as 'Профиль'
              ,Waste_Notices.Size as 'Размер'
              ,CAST(Waste_Notices.BilletWeight as float) as 'Вес заготовки'
              ,Waste_Notices.Quantity as 'Количество'
              ,CAST(Waste_Notices.IngotWeight as float) as 'Вес слитка'
              ,Waste_Notices.SuitableTotal as 'Годное'
              ,Waste_Notices.Part as 'Партия'
              ,Waste_Notices.NoticeEmitDate 'Дата извещения'
              ,wmn.MaterialName as 'Материал'
              ,Waste_Notices.RegisterDate as 'Дата выплавки'";
            if (includeDeleted) s+= ", case when Waste_Notices.IsDebit=1 then 'Списание' else 'Удаление' end as 'Извещение' ";
            s += @" FROM Waste_Notices 
              inner join R3_Shops as r3s1 on Waste_Notices.CauserShopID=r3s1.R3_ShopCode
              inner join R3_Shops as r3s2 on Waste_Notices.DebitShopID=r3s2.R3_ShopCode
              inner join R3_CostPlaces as r3cp on Waste_Notices.CauserPlaceID = r3cp.R3_CostPlaceCode
              inner join R3_WareHouses as r3wh on Waste_Notices.DebitWareHouseID = r3wh.R3_WareHouseCode
              inner join Waste_WasteTypes as wwt on Waste_Notices.WasteTypeID = wwt.id
              inner join Waste_DefectGroups as wdg on Waste_Notices.DefectGroupID = wdg.id
              inner join Waste_Defects as wd on Waste_Notices.DefectID = wd.id
              inner join Waste_CancellationTypes as wct on Waste_Notices.CancellationTypeID=wct.id
              inner join Common_Mark on Waste_Notices.MarkID = Common_Mark.id
              left outer join Common_ntd as cn on Waste_Notices.NtdID = cn.id
              inner join Waste_MaterialNames as  wmn on Waste_Notices.MaterialNameID = wmn.id
              inner join Technology_Workshops as tw on Waste_Notices.WorkShopID=tw.id
              inner join Technology_BottlingTypes as tbt on Waste_Notices.BottlingTypeID = tbt.id
              inner join Waste_Profiles as wp on Waste_Notices.ProfileID = wp.id
              where Waste_Notices.NoticeWasDeleted=0 and " + (!includeDeleted ? "Waste_Notices.IsDebit=1 and " : String.Empty) + Security.GetMarkSQLCheck("Контроль") + "AND (" + sqlFilter + ") order by RegisterDate";
            return s;
        }

        public static string GetMeltSumSelect(string sqlFilter, bool includeDefectListField)
        {
            var s = @"SELECT  Waste_Notices.SmeltNumber as 'Плавка'
              ,tw.WorkShop as 'Цех выплавки'
              ,Common_Mark.Mark as 'Марка'
              ,cn.Ntd as 'НТД'
              ,tbt.BottlingType as 'Разливка'
              ,cast(sum(Waste_Notices.BilletWeight) as float) as 'Вес заготовки'
              ,cast(sum(Waste_Notices.IngotWeight) as float) as 'Вес слитка'
              ,cast(max(Waste_Notices.SuitableTotal) as float) as 'Годное'
              ,case when max(Waste_Notices.SuitableTotal) > 0 then cast( sum(Waste_Notices.BilletWeight) / max(Waste_Notices.SuitableTotal) * 100 as float) else null end as 'Процент заготовки от годного'
              ,case when max(Waste_Notices.SuitableTotal) > 0 then cast( sum(Waste_Notices.IngotWeight)  /  max(Waste_Notices.SuitableTotal) * 100 as float) else null end as 'Процент слитка от годного'
              ,Waste_Notices.RegisterDate as 'Дата выплавки'";
			  if (includeDefectListField) s+= ", dbo.Concatenate(wd.Defect) as 'Дефекты' ";
              s += @"FROM Waste_Notices 
              inner join Waste_Defects as wd on Waste_Notices.DefectID = wd.id
              inner join Common_Mark on Waste_Notices.MarkID = Common_Mark.id
              left outer join Common_ntd as cn on Waste_Notices.NtdID = cn.id
              inner join Technology_Workshops as tw on Waste_Notices.WorkShopID=tw.id
              inner join Technology_BottlingTypes as tbt on Waste_Notices.BottlingTypeID = tbt.id
              where Waste_Notices.NoticeWasDeleted=0 and Waste_Notices.IsDebit=1  and " + Security.GetMarkSQLCheck("Контроль") + " AND  (" + sqlFilter + @") 
              group by Waste_Notices.SmeltNumber, tw.WorkShop, Common_Mark.Mark,cn.Ntd ,tbt.BottlingType, RegisterDate
              order by Waste_Notices.SmeltNumber";
            return s;
        }

        public static string GetDefectsSelect(string sqlFilter, DefectField field, List<DefectDescription> defects)
        {
            var emitField = String.Empty;
            switch (field)
            {
                case DefectField.Billet:
                    emitField = "cast (Waste_Notices.BilletWeight as float)";
                    break;
                case DefectField.Ingot:
                    emitField = "cast (Waste_Notices.IngotWeight as float)";
                    break;
                case DefectField.PercentBillet:
                    emitField = "cast(case when Waste_Notices.SuitableTotal > 0 then Waste_Notices.BilletWeight / Waste_Notices.SuitableTotal * 100 else null end as float)";
                    break;
                case DefectField.PercentIngot:
                    emitField = "cast(case when Waste_Notices.SuitableTotal > 0 then Waste_Notices.IngotWeight  /  Waste_Notices.SuitableTotal * 100 else null end as float)";
                    break;
            }
            
            var s = defects.Aggregate(@"SELECT  Waste_Notices.SmeltNumber as 'Плавка'
              ,tw.WorkShop as 'Цех выплавки'
              ,Common_Mark.Mark as 'Марка'
              ,cn.Ntd as 'НТД'
              ,tbt.BottlingType as 'Разливка'
              ,max(Waste_Notices.SuitableTotal) as 'Годное'
              ,Waste_Notices.RegisterDate as 'Дата выплавки'", (current, d) => current + ("\r\n,sum(case when Waste_Notices.DefectGroupID=" + d.DefectGroupID + " and Waste_Notices.DefectID=" + d.DefectID + " then " + emitField + " else null end) as '" + d.DefectGroupName + ":" + d.DefectName + "'"));

            s += string.Format(@",wmn.MaterialName as 'Материал'
               FROM Waste_Notices 
              inner join Waste_Defects as wd on Waste_Notices.DefectID = wd.id
              inner join Common_Mark on Waste_Notices.MarkID = Common_Mark.id
              left outer join Common_ntd as cn on Waste_Notices.NtdID = cn.id
              inner join Waste_MaterialNames as  wmn on Waste_Notices.MaterialNameID = wmn.id
              inner join Technology_Workshops as tw on Waste_Notices.WorkShopID=tw.id
              inner join Technology_BottlingTypes as tbt on Waste_Notices.BottlingTypeID = tbt.id
              where Waste_Notices.NoticeWasDeleted=0 and Waste_Notices.IsDebit=1  and {0} AND ({1}) 
              group by Waste_Notices.SmeltNumber, wmn.MaterialName, tw.WorkShop, Common_Mark.Mark,cn.Ntd ,tbt.BottlingType, RegisterDate
              order by Waste_Notices.SmeltNumber", Security.GetMarkSQLCheck("Контроль"), sqlFilter);
            return s;
        }
    }
}
