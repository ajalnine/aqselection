﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using ApplicationCore.ConstructorServiceReference;
using AQStepFactoryLibrary;

namespace SLConstructor_Waste
{
    public static class WasteCalculationFactory
    {
        public static List<NewTableData> GetChangeFieldsFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<NewTableData>();

            switch (selectionType)
            {
                    case DefectSelectionType.Details:
                    result.AddRange(new List<NewTableData> 
                    { 
                        new NewTableData{OldFieldName = "Марка", NewFieldName = "Марка"}, 
                        new NewTableData{OldFieldName = "НТД", NewFieldName = "НТД"}, 
                        new NewTableData{OldFieldName = "Дата выплавки", NewFieldName = "Дата выплавки"}, 
                        new NewTableData{OldFieldName = "Плавка", NewFieldName = "Плавка"}, 
                        new NewTableData{OldFieldName = "Группа дефекта", NewFieldName = "Группа дефекта"},
                        new NewTableData{OldFieldName = "Дефект", NewFieldName = "Дефект"},
                        new NewTableData{OldFieldName = "Профиль", NewFieldName = "Профиль"},
                        new NewTableData{OldFieldName = "Вес заготовки", NewFieldName = "Вес заготовки"},
                        new NewTableData{OldFieldName = "Вес слитка", NewFieldName = "Вес слитка"},
                        new NewTableData{OldFieldName = "МВЗ участка-виновника", NewFieldName = "МВЗ участка-виновника"}
                    });
                    break;
                    case DefectSelectionType.MeltSum:
                    result.AddRange(new List<NewTableData>
                    {
                        new NewTableData {OldFieldName = "Марка", NewFieldName = "Марка"},
                        new NewTableData {OldFieldName = "НТД", NewFieldName = "НТД"},
                        new NewTableData {OldFieldName = "Дата выплавки", NewFieldName = "Дата выплавки"},
                        new NewTableData {OldFieldName = "Плавка", NewFieldName = "Плавка"},
                        new NewTableData {OldFieldName = "Годное", NewFieldName = "Годное"},
                        new NewTableData {OldFieldName = "Вес заготовки", NewFieldName = "Вес заготовки"},
                        new NewTableData {OldFieldName = "Вес слитка", NewFieldName = "Вес слитка"},
                        new NewTableData {OldFieldName = "Процент заготовки от годного", NewFieldName = "Процент заготовки от годного"},
                        new NewTableData {OldFieldName = "Процент слитка от годного", NewFieldName = "Процент слитка от годного"},
                    });
                    break;

                    case DefectSelectionType.Defects:
                    result.AddRange(new List<NewTableData>
                    {
                        new NewTableData {OldFieldName = "Марка", NewFieldName = "Марка"},
                        new NewTableData {OldFieldName = "НТД", NewFieldName = "НТД"},
                        new NewTableData {OldFieldName = "Дата выплавки", NewFieldName = "Дата выплавки"},
                        new NewTableData {OldFieldName = "Плавка", NewFieldName = "Плавка"},
                        new NewTableData {OldFieldName = "Годное", NewFieldName = "Годное"},
                    });
                    result.AddRange(defectList.Select(d => d.DefectGroupName + ":" + d.DefectName)
                        .Select(field => new NewTableData {OldFieldName = field, NewFieldName = field}));
                    result.Add(new NewTableData {OldFieldName = "Материал", NewFieldName = "Материал"});
                    break;
            }
            
            return result;
        }

        public static List<string> GetSortingFieldsFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();

            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта",
                        "Дефект"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                    });
                    break;
            } 
            return result;
        }

        public static List<string> GetCombineFieldsFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();
            
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                     result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта",
                        "Дефект"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                    });
                    break;
            }
            return result;
        }

        public static List<string> GetRightBordersFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();
            
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки"
                    });
                    break;
            }
            return result;
        }

        public static List<string> GetLeftBordersFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки"
                    });
                    var oldGroup = string.Empty;
                    foreach (var d in defectList)
                    {
                        if (d.DefectGroupName == oldGroup) continue;
                        oldGroup = d.DefectGroupName;
                        var field = d.DefectGroupName + ":" + d.DefectName;
                        result.Add(field);
                    }
                    break;
            }
            return result;
        }
        public static List<string> GetHorizontalBordersFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки"
                    });
                    break;
            }
            return result;
        }

        public static List<string> GetChangeBordersFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
            var result = new List<string>();
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                        "Плавка",
                        "Группа дефекта"
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки",
                    });
                    break;
                case DefectSelectionType.Defects:
                    result.AddRange(new List<string>
                    {
                        "Марка",
                        "НТД",
                        "Дата выплавки"
                    });
                    break;
            }
            return result;
        }

        public static List<ScaleRule> GetScaleRulesFormattedOutput(DefectSelectionType selectionType, bool includeDefectListField, List<DefectDescription> defectList)
        {
           var result = new List<ScaleRule>();
            switch (selectionType)
            {
                case DefectSelectionType.Details:
                    result.Add(new ScaleRule
                    {
                        FieldName = "Вес заготовки",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    result.Add(new ScaleRule
                    {
                        FieldName = "Вес слитка",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    break;
                case DefectSelectionType.MeltSum:
                    result.Add(new ScaleRule
                    {
                        FieldName = "Вес заготовки",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    result.Add(new ScaleRule
                    {
                        FieldName = "Вес слитка",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), 
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    result.Add(new ScaleRule
                    {
                        FieldName = "Процент заготовки от годного",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10),
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    result.Add(new ScaleRule
                    {
                        FieldName = "Процент слитка от годного",
                        Mode = ScaleRuleMode.MinMax,
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10),
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), 
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    });
                    break;

                case DefectSelectionType.Defects:
                    result.AddRange(defectList.Select(d => d.DefectGroupName + ":" + d.DefectName).Select(field => new ScaleRule
                    {
                        FieldName = field,
                        Mode = ScaleRuleMode.MinMax, 
                        Color3 = Color.FromArgb(0xff, 0xdf, 0x41, 0x10), 
                        Color2 = Color.FromArgb(0xff, 0xfe, 0xff, 0x10), 
                        Color1 = Color.FromArgb(0xff, 0x30, 0xc0, 0x10),
                    }));
                    break;
            }
            return result;
        }
    }
}
