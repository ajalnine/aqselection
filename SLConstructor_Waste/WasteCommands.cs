﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQBasicControlsLibrary;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Waste
{
    public partial class MainPage
    {
        private const string TableName = "Списание брака";

        #region Просмотр
        private void PreviewButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            PreviewButton.IsEnabled = false;
            if (_selectionType != DefectSelectionType.Defects)
            {
                var task = new Task(new[] { "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
                task.SetState(0, TaskState.Processing);
                SetupCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadDone, task);
            }
            else
            {
                var task = new Task(new[] { "Список дефектов", "Выполнение\r\nзапроса", "Прием данных" }, "Анализ", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _cs.BeginGetDefects(_filters.WorkFilter, GetDefectsDone, task);
            }
        }

        private void GetDefectsDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetDefects(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _defectList = t.DefectList;
                SetupCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void DataReadDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndExecuteSteps(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _cas.BeginGetTablesSLCompatible(t.TaskDataGuid, i =>
                {
                    var tables = _cas.EndGetTablesSLCompatible(i);
                    if (tables == null) return;
                    task.AdvanceProgress();
                    task.SetMessage("Данные готовы");
                    var ca = new Dictionary<string, object>
                    {
                        {"From", ColumnsPresenter.From}, 
                        {"To", ColumnsPresenter.To}, 
                        {"SourceData", tables},
                        {"Calculation", _steps},
                        {"Description", _filters.Description},
                        {"New", true},
                    };
                    Dispatcher.BeginInvoke(() =>
                    {
                        PreviewButton.IsEnabled = true;
                        Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertTablesToAnalysis, ca, null);
                    });
                }, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
                Dispatcher.BeginInvoke(() => PreviewButton.IsEnabled = true);
            }
        }

        #endregion

        #region Экспорт XLSX
        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            XlsxExportButton.IsEnabled = false;

            if (_selectionType != DefectSelectionType.Defects)
            {
                var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
                task.SetState(0, TaskState.Processing);
                task.CustomData = sender;
                SetupCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadForXlsxDone, task);
            }
            else
            {
                var task = new Task(new[] { "Список дефектов", "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт", TaskPanel);
                task.SetState(0, TaskState.Processing);
                task.CustomData = sender;
                _cs.BeginGetDefects(_filters.WorkFilter, GetDefectsDoneForXlsx, task);
            }
        }

        private void GetDefectsDoneForXlsx(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetDefects(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _defectList = t.DefectList;
                SetupCalculation();
                _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadForXlsxDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void DataReadForXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndExecuteSteps(iar);
            if (t.Success)
            {
                task.SetState(0 + ((_selectionType == DefectSelectionType.Defects) ? 1 : 0), TaskState.Ready);
                task.SetState(1 + ((_selectionType == DefectSelectionType.Defects) ? 1 : 0), TaskState.Processing);
                task.SetMessage(t.Message);
                _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, CreateXlsxDone, task);
            }
            else
            {
                _cas.BeginDropData(t.TaskDataGuid, null, null);
                task.SetState(0 + ((_selectionType == DefectSelectionType.Defects) ? 1 : 0), TaskState.Error);
                task.SetMessage(t.Message);
                Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
            }
        }

        private void CreateXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndGetAllResultsInXlsx(iar);
            task.SetState(1 + ((_selectionType == DefectSelectionType.Defects) ? 1 : 0), TaskState.Ready);
            task.SetMessage(t.Message);

            Dispatcher.BeginInvoke(() =>
            {
                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                    string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}"
                    , HttpUtility.UrlEncode(t.TaskDataGuid)
                    , HttpUtility.UrlEncode(string.Format("Отчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)))
                    , HttpUtility.UrlEncode("application/msexcel"))));
                XlsxExportButton.IsEnabled = true;
            });
        }
        #endregion

        #region Сохранение запроса
        private void SaveQueryButton_Click(object sender, RoutedEventArgs e)
        {
            var tibb = sender as TextImageButtonBase;
            if (tibb != null) tibb.Tag = "NewItem";

            RefreshFlags();
            SaveQueryButton.IsEnabled = false;
            if (_selectionType != DefectSelectionType.Defects)
            {
                RefreshFlags();
                SendSaveCommand();
            }
            else
            {
                var task = new Task(new[] { "Список дефектов" }, "Сохранение запроса", TaskPanel); task.SetState(0, TaskState.Processing);
                task.SetState(0, TaskState.Processing);
                task.CustomData = sender;
                _cs.BeginGetDefects(_filters.WorkFilter, GetDefectsDoneForSave, task);
            }
        }

        private void SendSaveCommand()
        {
            SetupCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", "Списание брака"},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation,
                signalArguments, null);
        }

        private void GetDefectsDoneForSave(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var t = _cs.EndGetDefects(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _defectList = t.DefectList;
                SendSaveCommand();
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        #endregion

        #region Начало расчета
        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshFlags();
            if (_selectionType != DefectSelectionType.Defects)
            {
                OpenCalculation();
            }
            else
            {
                var task = new Task(new[] { "Список дефектов" }, "Начало расчета", TaskPanel);
                task.SetState(0, TaskState.Processing);
                _cs.BeginGetDefects(_filters.WorkFilter, GetDefectsDoneForBeginCalculation, task);
            }
        }

        private void GetDefectsDoneForBeginCalculation(IAsyncResult iar)
        {
            var t = _cs.EndGetDefects(iar);
            var task = (Task)iar.AsyncState;
            if (t.Success)
            {
                _defectList = t.DefectList;
                task.SetState(0, TaskState.Ready);
                Dispatcher.BeginInvoke(OpenCalculation);
            }
            else task.SetState(0, TaskState.Ready);

        }

        private void OpenCalculation()
        {
            SetupCalculation();
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", "Списание брака"},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        #endregion

        private void SetupCalculation()
        {
            RefreshFilters();
            _sql = ConstructSQL(false);
            Services.GetAQService().BeginWriteLog("Calc: Waste, " + _filters.Description + ", " + _filters.PeriodDescription, null, null);
            _steps = StepFactory.CreateSQLQuery(_sql, TableName);
            AppendDescriptionStep();
            if (_formatted) AppendFormatSteps();
            if (_colored) AppendScales();
        }

        private void AppendDescriptionStep()
        {
            var defectFieldName = new Dictionary<DefectField, string>
            {
                {DefectField.Billet, "Вес слитка"},
                {DefectField.Ingot, "Вес заготовки"},
                {DefectField.PercentBillet, "% заготовок от годного"},
                {DefectField.PercentIngot, "% слитков от годного"},
            };
            
            var constants = new List<ConstantDescription>
            {
                new ConstantDescription
                {
                    VariableName = "Описание",
                    VariableType = "String",
                    Value = _filters.Description
                },
                new ConstantDescription {VariableName = "Автор", VariableType = "String", Value = Security.CurrentUser},
                new ConstantDescription {VariableName = "Поле даты", VariableType = "String", Value = _dateField},
                new ConstantDescription
                {
                    VariableName = "Фиксированный столбец",
                    VariableType = "Int32",
                    Value = _formatted ? (_selectionType == DefectSelectionType.Details ? 6: 4) : 1
                }
            };
            if (_selectionType == DefectSelectionType.Defects) constants.Add(new ConstantDescription { VariableName = "Расчет дефектов", VariableType = "String", Value = defectFieldName[_defectField]});

            _steps.Add(StepFactory.CreateConstants(constants));
        }
        
        private void AppendFormatSteps()
        {
            _steps.Add(StepFactory.CreateChangeFields(TableName, WasteCalculationFactory.GetChangeFieldsFormattedOutput(_selectionType, _includeDefectListField, _defectList)));
            _steps.Add(StepFactory.CreateSorting(TableName, WasteCalculationFactory.GetSortingFieldsFormattedOutput(_selectionType, _includeDefectListField, _defectList), true));
            _steps.Add(StepFactory.CreateFormatCombine(TableName, WasteCalculationFactory.GetCombineFieldsFormattedOutput(_selectionType, _includeDefectListField, _defectList), true));
            _steps.Add(StepFactory.CreateFormatBorders(TableName,
                WasteCalculationFactory.GetRightBordersFormattedOutput(_selectionType, _includeDefectListField, _defectList),
                WasteCalculationFactory.GetLeftBordersFormattedOutput(_selectionType, _includeDefectListField, _defectList),
                WasteCalculationFactory.GetHorizontalBordersFormattedOutput(_selectionType, _includeDefectListField, _defectList),
                WasteCalculationFactory.GetChangeBordersFormattedOutput(_selectionType, _includeDefectListField, _defectList)));
        }

        private void AppendScales()
        {
            _steps.Add(StepFactory.CreateFormatScale(TableName, WasteCalculationFactory.GetScaleRulesFormattedOutput(_selectionType, _includeDefectListField, _defectList)));
        }
    }
}
