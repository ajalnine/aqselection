﻿using System;
using System.Collections.Generic;
using AQConstructorsLibrary;
using ApplicationCore;

namespace SLConstructor_Waste
{
    public class WasteParametersList : SelectionParametersList
    {
        public WasteParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            
            DateField = " Waste_Notices.DebitDate";

            DetailsJoin = String.Empty;

            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark 
                                INNER JOIN 
                                (
	                                select distinct MarkID from Waste_Notices @PreviousFilter group by MarkID
                                ) as t1 on t1.MarkID=Common_Mark.id 
                                where " + Security.GetMarkSQLCheck("Контроль") + @" AND @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "Waste_Notices.MarkID",
                 LastOnly = false,
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                 select distinct NtdID from Waste_Notices @PreviousFilter group by NtdID
                                ) as t1 on t1.NtdID = G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "Waste_Notices.NtdID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Цех выплавки",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, WorkShop as name from Technology_Workshops AS G 
                                INNER JOIN 
                                (
	                                 select distinct WorkShopID from Waste_Notices @PreviousFilter group by WorkShopID
                                ) as t1 on t1.WorkShopID = G.id 
                                where @AdditionalFilter
                                order by WorkShop
                                ",
                Field = "WorkShop",
                ResultedFilterField = "Waste_Notices.WorkShopID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            }; 
            
            
            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Цех виновник",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select R3_ShopCode as id, ShopName as name from R3_Shops AS G 
                                INNER JOIN 
                                (
	                                 select distinct CauserShopID from Waste_Notices @PreviousFilter group by CauserShopID
                                ) as t1 on t1.CauserShopID = G.R3_ShopCode 
                                where @AdditionalFilter
                                order by ShopName
                                ",
                Field = "ShopName",
                ResultedFilterField = "Waste_Notices.CauserShopID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "МВЗ виновник",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select R3_CostPlaceCode as id, CostPlaceName as name from R3_CostPlaces AS G 
                                INNER JOIN 
                                (
	                                 select distinct CauserPlaceID from Waste_Notices @PreviousFilter group by CauserPlaceID
                                ) as t1 on t1.CauserPlaceID = G.R3_CostPlaceCode 
                                where @AdditionalFilter
                                order by CostPlaceName
                                ",
                Field = "CostPlaceName",
                ResultedFilterField = "Waste_Notices.CauserPlaceID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd6 = new SelectionParameterDescription
            {
                ID = 6,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Цех списания",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select R3_ShopCode as id, ShopName as name from R3_Shops AS G 
                                INNER JOIN 
                                (
	                                 select distinct DebitShopID from Waste_Notices @PreviousFilter group by DebitShopID
                                ) as t1 on t1.DebitShopID = G.R3_ShopCode 
                                where @AdditionalFilter
                                order by ShopName
                                ",
                Field = "ShopName",
                ResultedFilterField = "Waste_Notices.DebitShopID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd7 = new SelectionParameterDescription
            {
                ID = 7,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Склад списания",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select R3_WareHouseCode as id, WareHouseName as name from R3_WareHouses AS G 
                                INNER JOIN 
                                (
	                                 select distinct DebitWareHouseID from Waste_Notices @PreviousFilter group by DebitWareHouseID
                                ) as t1 on t1.DebitWareHouseID = G.R3_WareHouseCode 
                                where @AdditionalFilter
                                order by WareHouseName
                                ",
                Field = "WareHouseName",
                ResultedFilterField = "Waste_Notices.DebitWareHouseID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd8 = new SelectionParameterDescription
            {
                ID = 8,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Вид разливки",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, BottlingType as name from Technology_BottlingTypes AS G 
                                INNER JOIN 
                                (
	                                 select distinct BottlingTypeID from Waste_Notices @PreviousFilter group by BottlingTypeID
                                ) as t1 on t1.BottlingTYpeID = G.id 
                                where @AdditionalFilter
                                order by BottlingType
                                ",
                Field = "BottlingType",
                ResultedFilterField = "Waste_Notices.BottlingTypeID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            var spd9 = new SelectionParameterDescription
            {
                ID = 9,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Тип брака",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, WasteType as name from Waste_WasteTypes AS G 
                                INNER JOIN 
                                (
	                                 select distinct WasteTypeID from Waste_Notices @PreviousFilter group by WasteTypeID
                                ) as t1 on t1.WasteTypeID = G.id 
                                where @AdditionalFilter
                                order by WasteType
                                ",
                Field = "WasteType",
                ResultedFilterField = "Waste_Notices.WasteTypeID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd10 = new SelectionParameterDescription
            {
                ID = 10,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Группа дефектов",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, DefectGroup as name from Waste_DefectGroups AS G 
                                INNER JOIN 
                                (
	                                 select distinct DefectGroupID from Waste_Notices @PreviousFilter group by DefectGroupID
                                ) as t1 on t1.DefectGroupID = G.id 
                                where @AdditionalFilter
                                order by DefectGroup
                                ",
                Field = "DefectGroup",
                ResultedFilterField = "Waste_Notices.DefectGroupID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd11 = new SelectionParameterDescription
            {
                ID = 11,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Дефект",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, Defect as name from Waste_Defects AS G 
                                INNER JOIN 
                                (
	                                 select distinct DefectID from Waste_Notices @PreviousFilter group by DefectID
                                ) as t1 on t1.DefectID = G.id
                                where @AdditionalFilter
                                order by Defect
                                ",
                Field = "Defect",
                ResultedFilterField = "Waste_Notices.DefectID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            }; 
            
            var spd12 = new SelectionParameterDescription
            {
                ID = 12,
                Selector = ColumnTypes.MeltList,
                EnableDetailsJoin = false,
                Name = "Номер плавки",

                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 

                                create table #tquery
                                (number int IDENTITY(1,1),melt nvarchar(50), melttype nvarchar(50))

                                insert into #tquery 
                                select distinct SmeltNumber , max(WorkShop) as melttype from Waste_Notices
                                inner join Technology_Workshops on Waste_Notices.WorkshopID=Technology_Workshops.ID
                                @PreviousFilter @AdditionalFilter group by SmeltNumber having SmeltNumber is not null and max(WorkShop) is not null order by max(WorkShop), SmeltNumber

                                select t1.melt as name, t1.melttype, cast (case when t2.melttype=t1.melttype then 0 else 1 end as bit) as MeltTypeChanged from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 where t1.melt is not null

                                drop table #tquery
                                ",
                Field = "SmeltNumber",
                ResultedFilterField = "Waste_Notices.SmeltNumber",
                LastOnly = false,
                IsNumber = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd13 = new SelectionParameterDescription
            {
                ID = 13,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Тип списания",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, CancellationType as name from Waste_CancellationTypes AS G 
                                INNER JOIN 
                                (
	                                 select distinct CancellationTypeID from Waste_Notices @PreviousFilter group by CancellationTypeID
                                ) as t1 on t1.CancellationTypeID = G.id
                                where @AdditionalFilter
                                order by CancellationType
                                ",
                Field = "CancellationType",
                ResultedFilterField = "Waste_Notices.CancellationTypeID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd14 = new SelectionParameterDescription
            {
                ID = 14,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Профиль",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, Profile as name from Waste_Profiles AS G 
                                INNER JOIN 
                                (
	                                 select distinct ProfileID from Waste_Notices @PreviousFilter group by ProfileID
                                ) as t1 on t1.ProfileID = G.id
                                where @AdditionalFilter
                                order by Profile
                                ",
                Field = "Profile",
                ResultedFilterField = "Waste_Notices.ProfileID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };
            
            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd8);
            ParametersList.Add(spd12);
            ParametersList.Add(spd14); 
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
            ParametersList.Add(spd6);
            ParametersList.Add(spd7);
            ParametersList.Add(spd9);
            ParametersList.Add(spd10);
            ParametersList.Add(spd11);
            ParametersList.Add(spd13);
        }
    }
}
