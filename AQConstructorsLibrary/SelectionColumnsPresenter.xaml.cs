﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.ConstructorServiceReference;
using AQControlsLibrary;

namespace AQConstructorsLibrary
{
    public partial class SelectionColumnsPresenter : UserControl
    {
        private SelectionParametersList selectionParameters;
        
        public delegate void ConstructionFinishedDelegate(object o, ConstructionFinishedEventArgs e);
        public delegate void ConstructionNotCompletedDelegate(object o, ConstructionNotCompletedEventArgs e);

        public event ConstructionFinishedDelegate ConstructionFinished;
        public event ConstructionNotCompletedDelegate ConstructionNotCompleted;
        public bool IsFinished;
        public SelectionParametersList SelectionParameters
        {
            get { return selectionParameters; }
            set { selectionParameters = value; BeginWork(); }
        }

        public bool FinishEnabled = true;

        public string FirstFilter { get; set; }
        
        private string customFilter;

        public string CustomFilter { 
            get
            { 
                return customFilter;
            } 
            set
            {
                customFilter = value; RefreshFirstFilter();
            }
        }
        
        public List<int> UsedParameters;

        public DateTime From;
        
        public DateTime To;
        
        public SelectionColumnsPresenter()
        {
            InitializeComponent();
            UsedParameters = new List<int>();
        }

        private void BeginWork()
        {
            IsFinished = false;
            RefreshFirstFilter(); 
            SelectionColumnsPanel.Children.Clear();
            UsedParameters = new List<int>(); 
            MainDateRangePicker.Visibility = (SelectionParameters.DateField == null) ? Visibility.Collapsed : Visibility.Visible;
            AddColumn();
        }

        public void AddColumn()
        {
            IsFinished = false;
            var NewColumn = new SelectionColumn();
            NewColumn.ParentColumnsPresenter = this;
            SelectionColumnsPanel.Children.Add(NewColumn);
            NewColumn.SetSPL();
            NewColumn.SetPreviousFilter();
            NewColumn.State = ColumnStates.ParametersSelection;
            if (ConstructionNotCompleted != null) ConstructionNotCompleted(this, new ConstructionNotCompletedEventArgs());
        }

        public void RemoveColumn(SelectionColumn sc)
        {
            IsFinished = false;
            SelectionColumnsPanel.Children.Remove(sc);
            SelectionColumn LastColumn = ((SelectionColumn)SelectionColumnsPanel.Children.Last());
            LastColumn.SetSPL();
            LastColumn.SetPreviousFilter();
            if (ConstructionNotCompleted != null) ConstructionNotCompleted(this, new ConstructionNotCompletedEventArgs());
        }

        public void RemoveColumnAndFinish(SelectionColumn sc)
        {
            IsFinished = true;
            SelectionColumnsPanel.Children.Remove(sc);
            SelectionColumn LastColumn = ((SelectionColumn)SelectionColumnsPanel.Children.Last());
            LastColumn.SetSPL();
            LastColumn.SetPreviousFilter();
            if (ConstructionFinished != null)
            {
                ConstructionFinished(this, new ConstructionFinishedEventArgs(GetFilters()));
            }
        }
        public void EnableFinishSelection(bool Enable)
        {
            var source = from i in SelectionColumnsPanel.Children.AsQueryable().Cast<SelectionColumn>() select i;
            foreach (SelectionColumn s in source)
            {
                s.EnableFinish(Enable);
            }
            FinishEnabled = Enable;
        }

        public Filters GetFilters()
        {
            string Filter = string.Empty;
            string WorkFilter = string.Empty;
            string FilterForParameters = string.Empty;
            string ParametersFilter = string.Empty;
            string SaveFilter = string.Empty;
            string ChemistryWorkFilter = string.Empty;
            string ChemistrySaveFilter = string.Empty;
            string Description = string.Empty;
            string ValueFilter = string.Empty;
            string ChemistryFilter = string.Empty;
            List<ValueFilterInfo> GVFI = new List<ValueFilterInfo>();
            bool IsFirst = true;

            IQueryable source = null;

            source = from i in SelectionColumnsPanel.Children.AsQueryable().Cast<SelectionColumn>() select i;
            foreach (SelectionColumn s in source)
            {
                if (!IsFirst)
                {
                    Filter += " AND ";
                    FilterForParameters += " AND ";
                    Description += "\r\n";
                    ValueFilter += " AND ";
                }
                Description += s.FAD.Description;
                ValueFilter += s.FAD.ValueFilter;
                Filter += s.FAD.SQLFilter;
                FilterForParameters += s.FAD.ParameterSQLFilter;
                if (s.FAD.ValueFilterInfo != null && s.FAD.ValueFilterInfo.Count > 0)
                {
                    foreach (var v in s.FAD.ValueFilterInfo) GVFI.Add(v);
                }

                IsFirst = false;
            }

            IsFirst = true;
            source = from i in SelectionColumnsPanel.Children.AsQueryable().Cast<SelectionColumn>() where i.SelectedParameter!=null && !i.SelectedParameter.IgnoreFilterForChemistry select i;
            foreach (SelectionColumn s in source)
            {
                if (!IsFirst)
                {
                    ChemistryFilter += " AND ";
                }
                ChemistryFilter += s.FAD.SQLFilter;
                IsFirst = false;
            }

            string DateFilter = GetDateFilter(false, selectionParameters.DateField);
            string DateFilterSave = GetDateFilter(true, selectionParameters.DateField);
            string DateDescription = GetDateDescription();
            WorkFilter = DateFilter + ((Filter!=string.Empty)?" AND ":string.Empty) + Filter;
            ParametersFilter = DateFilter + ((FilterForParameters != string.Empty) ? " AND " : string.Empty) + FilterForParameters;
            SaveFilter = DateFilterSave + ((Filter != string.Empty) ? " AND " : string.Empty) + Filter;
            ChemistryWorkFilter = DateFilter + ((ChemistryFilter != string.Empty) ? " AND " : string.Empty) + ChemistryFilter;
            ChemistrySaveFilter = DateFilterSave + ((ChemistryFilter != string.Empty) ? " AND " : string.Empty) + ChemistryFilter;
            if (string.IsNullOrEmpty(CustomFilter))
                return new Filters(Filter, ChemistryFilter, WorkFilter, SaveFilter, ValueFilter, Description,
                    ChemistryWorkFilter, ChemistrySaveFilter, GVFI, ParametersFilter, DateDescription);
            if (WorkFilter != string.Empty) WorkFilter += " AND ";
            if (SaveFilter != string.Empty) SaveFilter += " AND ";
            if (ParametersFilter != string.Empty) ParametersFilter += " AND ";
            if (ChemistryWorkFilter != string.Empty) ChemistryWorkFilter += " AND ";
            if (ChemistrySaveFilter != string.Empty) ChemistrySaveFilter += " AND ";
            WorkFilter += CustomFilter;
            SaveFilter += CustomFilter;
            ParametersFilter += CustomFilter;
            ChemistryWorkFilter += CustomFilter;
            ChemistrySaveFilter += CustomFilter;
            return new Filters(Filter, ChemistryFilter, WorkFilter, SaveFilter, ValueFilter, Description, ChemistryWorkFilter, ChemistrySaveFilter, GVFI, ParametersFilter, DateDescription);
        }

        public Filters UpdateFilters(Filters OldFilter)
        {
            string Filter = OldFilter.Filter;
            string FilterForParameters = OldFilter.ParametersFilter;
            string Description = OldFilter.Description;
            string ValueFilter = OldFilter.ValueFilter;
            string ChemistryFilter = OldFilter.ChemistryFilter;
            List<ValueFilterInfo> GVFI = OldFilter.GenericValueFilterInfo;
            
            string DateFilter = GetDateFilter(false, selectionParameters.DateField);
            string DateFilterSave = GetDateFilter(true, selectionParameters.DateField);
            string DateDescription = GetDateDescription();
            string WorkFilter = DateFilter + ((Filter != string.Empty) ? " AND " : string.Empty) + Filter;
            string ParametersFilter = DateFilter + ((FilterForParameters != string.Empty) ? " AND " : string.Empty) + FilterForParameters;
            string SaveFilter = DateFilterSave + ((Filter != string.Empty) ? " AND " : string.Empty) + Filter;
            string ChemistryWorkFilter = DateFilter + ((ChemistryFilter != string.Empty) ? " AND " : string.Empty) + ChemistryFilter;
            string ChemistrySaveFilter = DateFilterSave + ((ChemistryFilter != string.Empty) ? " AND " : string.Empty) + ChemistryFilter;
            if (CustomFilter != string.Empty && CustomFilter != null)
            {
                if (WorkFilter != string.Empty) WorkFilter += " AND ";
                if (SaveFilter != string.Empty) SaveFilter += " AND ";
                if (ParametersFilter != string.Empty) ParametersFilter += " AND ";
                if (ChemistryWorkFilter != string.Empty) ChemistryWorkFilter += " AND ";
                if (ChemistrySaveFilter != string.Empty) ChemistrySaveFilter += " AND ";
                WorkFilter += CustomFilter;
                ParametersFilter += CustomFilter;
                SaveFilter += CustomFilter;
                ChemistryWorkFilter += CustomFilter;
                ChemistrySaveFilter += CustomFilter;
            }
            return new Filters(Filter, ChemistryFilter, WorkFilter, SaveFilter, ValueFilter, Description, ChemistryWorkFilter, ChemistrySaveFilter, GVFI, ParametersFilter, DateDescription);
        }

        private void DateRangePicker_DateRangeChanged(object sender, AQControlsLibrary.DateChangedEventArgs e)
        {
            RefreshFirstFilter();
        }

        private void DateRangePicker_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshFirstFilter();
        }

        public void RefreshFirstFilter()
        {
            if (SelectionParameters == null) return;
            if (SelectionParameters.DateField != null)
            {
                FirstFilter = GetDateFilter(false, SelectionParameters.DateField);
                if (CustomFilter != string.Empty && CustomFilter != null)
                {
                    FirstFilter += " and (" + CustomFilter + ") ";
                }
            }
            else
            {
                FirstFilter = " 1=1 ";
            }
        }

        private string GetDateDescription()
        {
            bool HasFrom = MainDateRangePicker.From.HasValue;
            bool HasTo = MainDateRangePicker.To.HasValue;

            var From = string.Empty;
            var To = string.Empty;
            if (HasFrom) From = MainDateRangePicker.From.Value.ToString("dd.MM.yyyy");
            if (HasTo) To = MainDateRangePicker.To.Value.ToString("dd.MM.yyyy");
            return "\r\nПериод: " +From + " - " + To;
        }

        private string GetDateFilter(bool IsCommon, string DateField)
        {
            bool HasFrom = MainDateRangePicker.From.HasValue;
            bool HasTo = MainDateRangePicker.To.HasValue; 
            
            From = DateTime.MinValue;
            To = DateTime.MaxValue;
            if (HasFrom) From = MainDateRangePicker.From.Value;
            if (HasTo) To = MainDateRangePicker.To.Value;

            string DateTimeFiltered = String.Empty;

            if (!IsCommon)
            {
                if (HasFrom && HasTo)
                {
                    DateTimeFiltered = " " +DateField + " >= '" + From.ToString("yyyy-MM-dd") + "' and " +DateField + " < '" + To.AddDays(1).ToString("yyyy-MM-dd") + "'";
                }
                else
                {
                    if (HasFrom && !HasTo)
                    {
                        DateTimeFiltered = " " + DateField + " >= '" + From.ToString("yyyy-MM-dd") + "'";
                    }
                    else
                    {
                        if (!HasFrom && HasTo)
                        {
                            DateTimeFiltered = " " + DateField + " < '" + To.AddDays(1).ToString("yyyy-MM-dd") + "'";
                        }
                        else
                        {
                            DateTimeFiltered = String.Empty;
                        }
                    }
                }
            }
            else
            {
                if (HasFrom && HasTo)
                {
                    DateTimeFiltered = " " + " " + DateField + " >= @date1 and   " + DateField + " < dateadd(d,1,@date2) ";
                }
                else
                {
                    if (HasFrom && !HasTo)
                    {
                        DateTimeFiltered = " " + DateField + " >= @date1";
                    }
                    else
                    {
                        if (!HasFrom && HasTo)
                        {
                            DateTimeFiltered = " " + DateField + " < dateadd(d,1,@date2) ";
                        }
                        else
                        {
                            DateTimeFiltered = String.Empty;
                        }
                    }
                }
            }
            return DateTimeFiltered;
        }

        public Grid GetDateRangeCustomPanel()
        {
            return MainDateRangePicker.GetCustomPanel();
        }

        public DateRangePicker GetDateRangePicker()
        {
            return MainDateRangePicker;
        }
    }

    public class ConstructionFinishedEventArgs : EventArgs
    {
        public Filters Filters;
        public ConstructionFinishedEventArgs(Filters filters)
        {
            Filters = filters;
        }
    }

    public class ConstructionNotCompletedEventArgs : EventArgs
    {
    }
    
    public class Filters
    {
        public string Filter;
        public string ChemistryFilter;
        public string WorkFilter;
        public string ParametersFilter;
        public string SaveFilter;
        public string ValueFilter;
        public string Description;
        public string ChemistryWorkFilter;
        public string ChemistrySaveFilter;
        public List<ValueFilterInfo> GenericValueFilterInfo;
        public string PeriodDescription;
        public Filters(string filter, string chemistryfilter, string workfilter, string savefilter, string valuefilter, string description, string chemistryworkfilter, string chemistrysavefilter, List<ValueFilterInfo> genericValueFilterInfo, string parametersFilter, string periodDescription)
        {
            ChemistryFilter = chemistryfilter;
            PeriodDescription = periodDescription;
            Filter = filter;
            WorkFilter = workfilter;
            SaveFilter = savefilter;
            ValueFilter = valuefilter;
            Description = description;
            ChemistryWorkFilter = chemistryworkfilter;
            ChemistrySaveFilter = chemistrysavefilter;
            GenericValueFilterInfo = genericValueFilterInfo;
            ParametersFilter = parametersFilter;
        }
    }
}
