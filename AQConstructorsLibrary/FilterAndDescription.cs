﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AQConstructorsLibrary
{
    public class FilterAndDescription
    {
        public string ParameterSQLFilter { get; set; }
        public string SQLFilter { get; set; }
        public string ValueFilter { get; set; }
        public string Description { get; set; }
        public string SelectedItems { get; set; }
        public List<ValueFilterInfo> ValueFilterInfo {get; set;}
    }

    public class ValueFilterInfo
    {
        public int ParameterID { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
    }
}
