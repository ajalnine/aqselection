﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.ConstructorServiceReference;

namespace AQConstructorsLibrary
{
    public partial class FieldCombineView : UserControl
    {
        public ParametersList PL 
        {
            get 
            { 
                return WorkParametersList.ParametersList; 
            } 
            set 
            { 
                WorkParametersList.ParametersList = value; 
            } 
        }

        public FieldListCombination FLC { get; set; }

        public FieldList NewFL { get; set; }

        public List<int> RestrictedList;

        public FieldCombineView()
        {
            InitializeComponent();
        }

        public void Refresh()
        {
            MainCombineView.ItemsSource = null;
            MainCombineView.UpdateLayout();
            MainCombineView.ItemsSource = FLC.FieldListCombined;
            MainCombineView.UpdateLayout();
            RefreshRestrictedList();
            var source = (from p in PL.AllParameters where !RestrictedList.Contains(p.ParamID) select new KeyValuePair<int, string>(p.ParamTypeID, p.ParamType)).Distinct();
            if (source.Count() > 0)
            {
                ShowGroups();
                NewGroup.ItemsSource = source;
                NewGroup.UpdateLayout();
                NewGroup.SelectedIndex = 0;
                NewGroup.UpdateLayout();
            }
            else HideGroups();
            AddListButton.Visibility = Visibility.Collapsed;
        }

        #region Редактирование новой группы слияния
        private void RefreshRestrictedList()
        {
            RestrictedList = new List<int>();
            foreach (FieldList fl in FLC.FieldListCombined)
            {
                RestrictedList.Add(fl.MainParameterID);
                foreach (int i in fl.Combined)
                {
                    RestrictedList.Add(i);
                }
            }
        }

        private void NewGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (NewGroup.SelectedItem != null)
            {
                int CurrentGroup = ((KeyValuePair<int, string>)NewGroup.SelectedItem).Key;
                RefreshRestrictedList();
                List<KeyValuePair<int, string>> Source = (from p in PL.AllParameters where !RestrictedList.Contains(p.ParamID) && p.ParamTypeID == CurrentGroup select new KeyValuePair<int, string>(p.ParamID, p.Param + " (" + p.ParamOld + ")")).ToList();
                if (Source.Count > 0)
                {
                    ShowParametersComboBoxes();
                    NewMainParameter.ItemsSource = Source;
                    NewMainParameter.UpdateLayout();
                    NewMainParameter.SelectedIndex = 0;
                    NewMainParameter.UpdateLayout();
                }
                else HideParametersComboBoxes();
                NewGroupName.DataContext = NewFL;
                NewGroupName.UpdateLayout();
            }
        }

        private void ShowParametersComboBoxes()
        {
            ParametersPanel.Visibility = Visibility.Visible;
        }

        private void ShowGroups()
        {
            GroupsPanel.Visibility = Visibility.Visible;
            CombineGroupPanel.Visibility = Visibility.Visible;
            ParametersPanel.Visibility = Visibility.Visible;
        }

        private void HideGroups()
        {
            GroupsPanel.Visibility = Visibility.Collapsed;
            CombineGroupPanel.Visibility = Visibility.Collapsed;
            ParametersPanel.Visibility = Visibility.Collapsed;
        }

        private void HideParametersComboBoxes()
        {
            ParametersPanel.Visibility = Visibility.Collapsed;
        }

        private void NewMainParameter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int CurrentGroup = ((KeyValuePair<int, string>)NewGroup.SelectedItem).Key;
            NewFL = new FieldList();
            NewFL.Combined = new List<int>();
            NewFL.FullCombined = new List<int>();
            NewFL.Enabled = true;
            NewFL.Hash = NewFL.GetHashCode();
            if (NewMainParameter.SelectedItem != null)
            {
                NewFL.MainParameterID = ((KeyValuePair<int, string>)NewMainParameter.SelectedItem).Key;
                NewFL.FullCombined.Add(NewFL.MainParameterID);
                var source = from p in PL.AllParameters where  p.ParamID != NewFL.MainParameterID && p.ParamTypeID == CurrentGroup && !NewFL.Combined.Contains(p.ParamID) && !RestrictedList.Contains(p.ParamID) select new KeyValuePair<int, string>(p.ParamID, p.Param + " (" + p.ParamOld + ")");
                NewCombined.ItemsSource = source;
                NewCombined.UpdateLayout();
                if (source.Count() > 0)
                {
                    NewCombined.SelectedIndex = 0;
                    NewCombined.UpdateLayout();
                }
                else HideParametersComboBoxes();
                AddListButton.Visibility = Visibility.Collapsed;
            }
            NewGroupListBox.ItemsSource = null;
            NewGroupListBox.UpdateLayout();
            NewGroupListBox.ItemsSource = NewFL.FullCombined;
            NewGroupListBox.UpdateLayout();
            NewMainName.DataContext = NewFL;
            NewMainName.UpdateLayout();
        }

        private void AddCombine_Click(object sender, RoutedEventArgs e)
        {
            int CurrentGroup = ((KeyValuePair<int, string>)NewGroup.SelectedItem).Key;
            int NewValue = ((KeyValuePair<int, string>)NewCombined.SelectedItem).Key;
            NewFL.Combined.Add(NewValue);
            NewFL.FullCombined.Add(NewValue);
            AddListButton.Visibility = Visibility.Visible;
            RefreshNewCombined();
        }

        private void RefreshNewCombined()
        {
            int CurrentGroup = ((KeyValuePair<int, string>)NewGroup.SelectedItem).Key;
            var source = from p in PL.AllParameters where p.ParamID != NewFL.MainParameterID && p.ParamTypeID == CurrentGroup && !NewFL.Combined.Contains(p.ParamID) && !RestrictedList.Contains(p.ParamID) select new KeyValuePair<int, string>(p.ParamID, p.Param + " (" + p.ParamOld + ")");
            if (source.Count() > 0)
            {
                NewCombined.ItemsSource = source;
                NewCombined.UpdateLayout();
                NewCombined.SelectedIndex = 0;
                NewCombined.UpdateLayout();
                ShowParametersComboBoxes();
            }
            else HideParametersComboBoxes();
            NewGroupListBox.ItemsSource = null;
            NewGroupListBox.UpdateLayout();
            NewGroupListBox.ItemsSource = NewFL.FullCombined;
            NewGroupListBox.UpdateLayout();
        }

        private void AddListButton_Click(object sender, RoutedEventArgs e)
        {
            FLC.FieldListCombined.Add(NewFL);
            Refresh();
        }
        #endregion

        #region Удаление групп и отдельных параметров
        private void RemoveFromNewListButton_Click(object sender, RoutedEventArgs e)
        {
            if (NewFL.Combined.Count == 0) return;
            int CurrentID = (int)((Control)sender).Tag;
            if (NewFL.MainParameterID != CurrentID)
            {
                NewFL.Combined.Remove(CurrentID);
                NewFL.FullCombined.Remove(CurrentID);
            }
            else
            {
                int NewMainID = NewFL.Combined.First();
                NewFL.Combined.Remove(NewMainID);
                NewFL.FullCombined.Remove(CurrentID);
                NewFL.MainParameterID = NewMainID;
                RefreshRestrictedList();
                
                NewMainParameter.SelectionChanged -= NewMainParameter_SelectionChanged;
                var source = (List<KeyValuePair<int, string>>)NewMainParameter.ItemsSource;
                int NewIndex = source.IndexOf((from a in source where a.Key==NewMainID select a).First());
                NewMainParameter.SelectedIndex = NewIndex;
                NewMainParameter.UpdateLayout();
                NewMainParameter.SelectionChanged += NewMainParameter_SelectionChanged;
                
                NewMainName.DataContext = NewFL;
                NewMainName.UpdateLayout();
                
            }
            if (NewFL.Combined.Count == 0) AddListButton.Visibility = Visibility.Collapsed;
            NewGroupListBox.ItemsSource = null;
            NewGroupListBox.UpdateLayout();
            NewGroupListBox.ItemsSource = NewFL.FullCombined;
            NewGroupListBox.UpdateLayout();
            RefreshRestrictedList();
            RefreshNewCombined();
        }

        private void RemoveValueFromListButton_Click(object sender, RoutedEventArgs e)
        {
            int CurrentID = (int)((Control)sender).Tag;
            FieldList CurrentItem = (from f in FLC.FieldListCombined where f.FullCombined.Contains(CurrentID) select f).First();
            if (CurrentItem.MainParameterID == CurrentID)
            {
                int NewMainID = CurrentItem.Combined.First();
                CurrentItem.Combined.Remove(NewMainID);
                CurrentItem.FullCombined.Remove(CurrentID);
                CurrentItem.MainParameterID = NewMainID;
            }
            else
            {
                CurrentItem.Combined.Remove(CurrentID);
                CurrentItem.FullCombined.Remove(CurrentID);
            }
            if (CurrentItem.FullCombined.Count < 2) FLC.FieldListCombined.Remove(CurrentItem);
            Refresh();
        }

        private void RemoveGroupFromListButton_Click(object sender, RoutedEventArgs e)
        {
            int CurrentID = (int)((Control)sender).Tag;
            FieldList CurrentItem = (from f in FLC.FieldListCombined where f.MainParameterID == CurrentID select f).First();
            FLC.FieldListCombined.Remove(CurrentItem);
            Refresh();
        }
        #endregion
    }
}
