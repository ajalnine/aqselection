﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore;
using ApplicationCore.ConstructorServiceReference;

namespace AQConstructorsLibrary
{
    public partial class SelectorMeltList : ISelectorBase
    {
        public SelectionColumn ParentColumn { get; set; }

        private readonly ConstructorService _cs;

        private List<MeltListData> _tempSourceData, _meltList;
        private int _selectedItemsCount, _allItemsCount;

        public SelectorMeltList(SelectionColumn parentColumn)
        {
            InitializeComponent();
            ParentColumn = parentColumn;
            _cs = Services.GetConstructorService();
        }

        public void Refresh()
        {
            ParentColumn.SetPreviousFilter();
            FilterChanged(true);
        }

        public void ReflectStateChange()
        {
            if (ParentColumn.SelectedParameter != null)
            {
                FilterPanel.Visibility = ParentColumn.SelectedParameter.NumericSelectionAvailable ? Visibility.Collapsed : Visibility.Visible;
            }

            if (ParentColumn.State != ColumnStates.ParametersSelected 
                || ColumnContent.ItemsSource != null 
                || (ParentColumn.GetSelfPosition() <= 1 && ParentColumn.SelectedParameter.SearchAvailable)) return;

            BeginGetMeltData();
        }

        private void BeginGetMeltData()
        {
            var joinSubstitution = ((ParentColumn.IsDetailJoinRequired())
                ? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin
                : string.Empty);
            var sql =
                ParentColumn.SelectedParameter.SQLTemplate
                    .Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ")
                    .Replace("@AdditionalFilter", string.Empty)
                    .Replace("@DetailsJoin", joinSubstitution);
            ParentColumn.StartProgressIndicator();

            _cs.BeginGetRows(sql, GetRowsDone, null);
        }

        protected void FilterChanged(bool preserveValue)
        {
            if (preserveValue && ColumnContent.ItemsSource != null)
                _tempSourceData =
                    (from a in ((List<MeltListData>) ColumnContent.ItemsSource) where a.IsChecked select a).ToList();
            else _tempSourceData = null;

            var sql = 
                ParentColumn.SelectedParameter.SQLTemplate
                    .Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ")
                    .Replace("@AdditionalFilter", string.IsNullOrEmpty(Filter.Text.Trim())
                        ? string.Empty
                        : ConvertStringFilterToSQLFilter(Filter.Text, false, ParentColumn.SelectedParameter.Field))
                    .Replace("@DetailsJoin", ((ParentColumn.IsDetailJoinRequired())
                        ? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin
                        : string.Empty));

            ParentColumn.StartProgressIndicator();
            _selectedItemsCount = 0;
            _allItemsCount = 0;
            _meltList = null;
            ColumnContent.Focus();
            ParentColumn.State = ColumnStates.ParametersSelected;
            ColumnContent.ItemsSource = null;
            ColumnContent.UpdateLayout();
            SelectionInfo.Text = string.Empty;
            _cs.BeginGetRows(sql, GetRowsDone, new object());
        }

        public void GetRowsDone(IAsyncResult iar)
        {
            var sourceData = _cs.EndGetRows(iar);

            ParentColumn.Dispatcher.BeginInvoke(delegate
            {
                ParentColumn.StopProgressIndicator();
                ParentColumn.UpdateLayout();
            });

            ColumnContent.Dispatcher.BeginInvoke(delegate
            {
                _meltList = GetMeltList(sourceData, ModeIsCompressedList.IsChecked != null && ModeIsCompressedList.IsChecked.Value);
                SetupColumnContentViewer();
            });
        }

        protected static string ConvertStringFilterToSQLFilter(string list, bool isFinalFilter, string field)
        {
            var and = (isFinalFilter) ? string.Empty : " and ";
            if (list.Contains('%') || list.Contains('_')) return and + " " + field + " like '" + list + "' ";

            var mc = Regex.Matches(list + ";", @"([\w]*?)([\d]*)(-([\d]*))?[^\w-\n]+");
            var oven = string.Empty;
            var number = string.Empty;

            var outputDiscreteFilter = " (" + field + " in (";
            var outputRangeFilter = "(";

            var isFirstDiscrete = true;
            var isFirstRange = true;

            foreach (Match m in mc)
            {
                var currentOven = m.Groups[1].Success ? m.Groups[1].Captures[0].Value : string.Empty;

                var currentNumber = m.Groups[2].Success ? m.Groups[2].Captures[0].Value : string.Empty;
                if (string.IsNullOrEmpty(currentOven) && currentNumber.Length == 6 && "123".Contains(currentNumber[0]))
                {
                    currentOven = currentNumber.StartsWith("19") ? "Э" : "К";
                }

                if (currentOven != string.Empty) oven = currentOven;

                if (currentNumber != string.Empty)
                {
                    if (number.Length >= currentNumber.Length && currentOven == string.Empty)
                    {
                        number = number.Substring(0, number.Length - currentNumber.Length) + currentNumber;
                    }
                    else number = currentNumber;
                }

                var currentEndRange = m.Groups[4].Success ? m.Groups[4].Captures[0].Value : string.Empty;
                string endRange;
                if (currentEndRange != string.Empty)
                {
                    if (number.Length >= currentEndRange.Length)
                    {
                        endRange = number.Substring(0, number.Length - currentEndRange.Length) + currentEndRange;
                    }
                    else endRange = currentEndRange;
                }
                else endRange = string.Empty;

                if (endRange != string.Empty)
                {
                    if (int.Parse(endRange) > int.Parse(number))
                    {
                        if (!isFirstRange) outputRangeFilter += " OR ";
                        var currentItemFilter = string.Format("((dbo.ToNumeric(substring({0},{1}, LEN({0}))) between {2} and {3}) and substring({0},1,{4})='{5}')", field, (oven.Length + 1), number, endRange, oven.Length, oven);
                        isFirstRange = false;
                        outputRangeFilter += currentItemFilter;
                    }
                }
                else
                {
                    if (!isFirstDiscrete) outputDiscreteFilter += ",";
                    isFirstDiscrete = false;
                    outputDiscreteFilter += "'" + oven + number + "'";
                }
            }
            outputRangeFilter += ")";
            outputDiscreteFilter += "))";
            and = (isFinalFilter && !isFirstDiscrete && !isFirstRange) ? string.Empty : " and ";
            if (isFirstRange)
            {
                if (isFirstDiscrete) return string.Empty;
                return and + outputDiscreteFilter;
            }
            if (isFirstDiscrete) return and + outputRangeFilter;
            return and + " (" + outputRangeFilter + " OR " + outputDiscreteFilter + ")";
        }

        private string ParseFilter(string stringFilter)
        {
            var mc = Regex.Matches(stringFilter + ";", @"([\w]*?)([\d]*)(-([\d]*))?[^\w-\n]+");
            var oven = string.Empty;
            var number = string.Empty;
            var outputSelectedItems = string.Empty;

            var isFirst = true;

            foreach (Match m in mc)
            {
                var currentOven = m.Groups[1].Success ? m.Groups[1].Captures[0].Value : string.Empty;
                if (currentOven != string.Empty) oven = currentOven;

                var currentNumber = m.Groups[2].Success ? m.Groups[2].Captures[0].Value : string.Empty;
                if (currentNumber != string.Empty)
                {
                    if (number.Length >= currentNumber.Length)
                    {
                        number = number.Substring(0, number.Length - currentNumber.Length) + currentNumber;
                    }
                    else number = currentNumber;
                }

                var currentEndRange = m.Groups[4].Success ? m.Groups[4].Captures[0].Value : string.Empty;
                string endRange;
                if (currentEndRange != string.Empty)
                {
                    if (number.Length >= currentEndRange.Length)
                    {
                        endRange = number.Substring(0, number.Length - currentEndRange.Length) + currentEndRange;
                    }
                    else endRange = currentEndRange;
                }
                else endRange = string.Empty;

                if (oven != string.Empty && number != string.Empty)
                {
                    if (endRange != string.Empty)
                    {
                        if (int.Parse(endRange) > int.Parse(number))
                        {
                            if (!isFirst) outputSelectedItems += "\r\n";
                            outputSelectedItems += oven + number + ".." + oven + endRange;
                            isFirst = false;
                        }
                    }
                    else
                    {
                        if (!isFirst) outputSelectedItems += "\r\n";
                        isFirst = false;
                        outputSelectedItems += oven + number;
                    }
                }
            }
            return outputSelectedItems;
        }

        private static List<MeltListData> GetMeltList(List<List<object>> source, bool compress)
        {
            var meltList = new List<MeltListData>();

            var oldNumber = string.Empty;
            var oldOven = string.Empty;
            var rangeStarted = false;
            var range = new MeltListData();
            MeltListData discrete = null;

            foreach (var i in source.Select(a=> new {IsGroup=(bool)a.ElementAt(2), Group = a.ElementAt(1)?.ToString(), Melt = a.ElementAt(0)?.ToString()}))
            {
                if (i.IsGroup)
                {
                    var header = new MeltListData
                    {
                        Key = i.Group,
                        Value = i.Group,
                        Min = string.Empty,
                        Max = string.Empty,
                        GroupID = i.Group,
                        IsRange = false,
                        Oven = string.Empty,
                        RowType = MeltRowType.Type
                    };

                    if (discrete != null) meltList.Add(discrete);
                    discrete = null;
                    meltList.Add(header);

                    rangeStarted = false;
                }
                var mc = Regex.Match(i.Melt, @"([\d]*)$");
                var currentNumber = mc.Success ? mc.Groups[1].Captures[0].Value : string.Empty;
                var currentOven = i.Melt.Substring(0, i.Melt.Length - currentNumber.Length);
                var cn = (currentNumber != string.Empty) ? int.Parse(currentNumber) : -1;
                var @on = (oldNumber != string.Empty) ? int.Parse(oldNumber) : -1;

                if (cn - @on != 1 || currentOven != oldOven || !compress)
                {
                    if (rangeStarted)
                    {
                        range.Max = oldNumber;
                        range.Value = range.Oven + range.Min + ".." + range.Oven + range.Max;
                        meltList.Add(range);
                        range = new MeltListData();
                    }

                    if (discrete != null) meltList.Add(discrete);
                    discrete = new MeltListData
                    {
                        Key = i.Melt,
                        Value = i.Melt,
                        Min = string.Empty,
                        Max = string.Empty,
                        GroupID = i.Group,
                        IsRange = false,
                        Oven = currentOven,
                        RowType = MeltRowType.Single
                    };

                    oldOven = currentOven;
                    oldNumber = currentNumber;
                    rangeStarted = false;
                }
                else
                {
                    if (!rangeStarted)
                    {
                        range.Min = oldNumber;
                    }
                    rangeStarted = true;
                    discrete = null;
                    range.Key = oldOven + oldNumber;
                    range.Value = oldOven + oldNumber;
                    range.Max = oldNumber;

                    range.GroupID = i.Group;
                    range.IsRange = true;
                    range.Oven = oldOven;
                    range.RowType = MeltRowType.Range;
                    oldOven = currentOven;
                    oldNumber = currentNumber;
                }
            }
            if (rangeStarted)
            {
                range.Max = oldNumber;
                range.Value = range.Oven + range.Min + ".." + range.Oven + range.Max;
                meltList.Add(range);
            }
            else
            {
                if (discrete != null) meltList.Add(discrete);
            }
            return meltList;
        }

        private void SetupColumnContentViewer()
        {
            ParentColumn.NoData.Visibility = (_meltList.Count == 0) ? Visibility.Visible : Visibility.Collapsed;
            if (_meltList.Count == 0) return;

            ColumnContent.ItemsSource = _meltList;
            ColumnContent.SelectedIndex = -1;
            if (_tempSourceData != null)
            {
                foreach (var i in _tempSourceData)
                {
                    var toRestore = (from t in _meltList where i.Key == t.Key select t);

                    var meltListDatas = toRestore as MeltListData[] ?? toRestore.ToArray();
                    if (meltListDatas.Length != 1) continue;
                    var n = meltListDatas.Single();
                    n.IsChecked = i.IsChecked;
                    n.Max = i.Max;
                    n.Min = i.Min;
                }
                _tempSourceData = null;
            }
            else
            {
                if (_meltList.Count <= 500)
                {
                    SetModeToList();
                    _selectedItemsCount = 0;
                    foreach (var i in _meltList)
                    {
                        i.IsChecked = true;
                        _selectedItemsCount++;
                    }
                    ModeIsList.IsChecked = true;
                }
                else
                {
                    if (WarningPanel != null) WarningPanel.Visibility = Visibility.Collapsed;
                    SetModeToFilter();
                    foreach (var i in _meltList) i.IsChecked = false;
                    _selectedItemsCount = 0;
                    ModeIsFilter.IsChecked = true;
                }
            }
            _selectedItemsCount = (from i in ColumnContent.ItemsSource.AsQueryable().Cast<MeltListData>()
                where i.IsChecked && i.RowType != MeltRowType.Type
                select i).Count();
            if (_selectedItemsCount > 0) ParentColumn.State = ColumnStates.ValueSelected;
            RefreshSelectionInfo();
            ColumnContent.UpdateLayout();
        }

        private MeltListData GetParamListData(string key)
        {
            return (from i in _meltList where i.Key == key select i).Single();
        }

        private IQueryable GetCheckedItems()
        {
            return from i in ColumnContent.ItemsSource.AsQueryable().Cast<MeltListData>()
                where i.IsChecked && i.RowType != MeltRowType.Type
                select i;
        }

        public void RefreshSelectionInfo()
        {
            if (SelectionInfo == null) return;
            if (ModeIsList?.IsChecked != null && ModeIsList.IsChecked.Value)
            {
                SelectionInfoPanel.Visibility = Visibility.Visible;
                SelectionInfo.Text =
                    $"Выбрано {_selectedItemsCount} из {_meltList.Count(a => a.RowType == MeltRowType.Single)}";
                CopyMeltList.IsEnabled = _selectedItemsCount > 0;
            }
            else
            {
                SelectionInfo.Text = string.Empty;
                SelectionInfoPanel.Visibility = Visibility.Collapsed;
            }
        }

        public FilterAndDescription GetFilterAndDescription()
        {
            var fad = new FilterAndDescription();

            var spd = ParentColumn.SelectedParameter;
            if (ModeIsFilter.IsChecked != null && ModeIsFilter.IsChecked.Value)
            {
                fad.SQLFilter = ConvertStringFilterToSQLFilter(Filter.Text, true, spd.Field);
                fad.ValueFilter = string.Empty;
                fad.SelectedItems = ParseFilter(Filter.Text);
                if (string.IsNullOrWhiteSpace(fad.SelectedItems)) fad.SelectedItems = "Без фильтрации";
                fad.Description = spd.Name + ": " + fad.SelectedItems.Replace("\r\n", ";");
            }
            else
            {
                var isFirstDescription = true;
                var isFirstGroup = true;
                var isFirstDiscrete = true;
                var isFirstRange = true;
                var oldGroupName = string.Empty;
                fad.Description = string.Empty;

                var outputDiscreteFilter = " (" + spd.Field + " in (";
                var outputRangeFilter = "(";
                foreach (MeltListData pld in GetCheckedItems())
                {
                    if (!isFirstDescription)
                    {
                        fad.SelectedItems += "\r\n";
                        fad.Description += ",";
                    }
                    isFirstDescription = false;
                    var meltGroupID = pld.GroupID;
                    if (meltGroupID != oldGroupName)
                    {
                        if (!isFirstGroup)
                        {
                            fad.SelectedItems += "\r\n";
                            fad.Description += ";";
                        }
                        isFirstGroup = false;
                        fad.SelectedItems += "● " + meltGroupID + ":\r\n";
                        fad.Description += meltGroupID + ":";
                        oldGroupName = meltGroupID;
                    }

                    switch (pld.RowType)
                    {
                        case MeltRowType.Range:
                            if (!isFirstRange) outputRangeFilter += " OR ";
                            var currentItemFilter = string.Format("((dbo.ToNumeric(substring({0},{1}, LEN({0}))) between {2} and {3}) and substring({0},1,{4})='{5}')", spd.Field, (pld.Oven.Length + 1), pld.Min, pld.Max, pld.Oven.Length, pld.Oven);
                            isFirstRange = false;
                            outputRangeFilter += currentItemFilter;
                            fad.Description += pld.Oven + pld.Min + ".." + pld.Oven + pld.Max;
                            fad.SelectedItems += "   • " + pld.Oven + pld.Min + ".." + pld.Oven + pld.Max;
                            break;

                        case MeltRowType.Single:
                            if (!isFirstDiscrete) outputDiscreteFilter += ",";
                            isFirstDiscrete = false;
                            outputDiscreteFilter += "'" + pld.Value + "'";
                            fad.Description += pld.Value;
                            fad.SelectedItems += "   • " + pld.Value;
                            break;
                    }
                }

                outputRangeFilter += ")";
                outputDiscreteFilter += "))";

                if (isFirstRange)
                {
                    fad.SQLFilter = !isFirstDiscrete ? outputDiscreteFilter : string.Empty;
                }
                else
                {
                    if (isFirstDiscrete) fad.SQLFilter = outputRangeFilter;
                    else fad.SQLFilter = " (" + outputRangeFilter + " OR " + outputDiscreteFilter + ")";
                }
            }

            fad.ParameterSQLFilter = fad.SQLFilter;
            return fad;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ParentColumn.SetPreviousFilter();
            FilterChanged(false);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var cb = (CheckBox) sender;
            var currentData = GetParamListData(cb.Tag.ToString());

            if (currentData.RowType != MeltRowType.Type)
            {
                var i =
                    (from a in ((List<MeltListData>)ColumnContent.ItemsSource)
                     where a.Key == ((CheckBox)sender).Tag.ToString()
                     select a).Single();
                if (cb.IsChecked != null && cb.IsChecked.Value)
                {
                    i.IsChecked = true;
                    CheckGroupName(currentData.GroupID, true);
                    _selectedItemsCount++;
                    ParentColumn.State = ColumnStates.ValueSelected;
                }
                else
                {
                    i.IsChecked = false;
                    i.Max = string.Empty;
                    i.Min = string.Empty;
                    if (!IsAnyItemChecked(currentData.GroupID)) CheckGroupName(currentData.GroupID, false);
                    _selectedItemsCount--;
                    if (_selectedItemsCount == 0) ParentColumn.State = ColumnStates.ParametersSelected;
                }
            }
            else
            {
                if (cb.IsChecked != null && cb.IsChecked.Value)
                {
                    var i = from a in _meltList where a.GroupID == currentData.GroupID select a;
                    foreach (var pld in i)
                    {
                        if (!pld.IsChecked) _selectedItemsCount++;
                        pld.IsChecked = true;
                    }
                    ParentColumn.State = ColumnStates.ValueSelected;
                }
                else
                {
                    var i = from a in _meltList where a.GroupID == currentData.GroupID select a;
                    foreach (var pld in i)
                    {
                        if (pld.IsChecked) _selectedItemsCount--;
                        pld.Max = string.Empty;
                        pld.Min = string.Empty;
                        pld.IsChecked = false;
                    }
                    if (_selectedItemsCount == 0) ParentColumn.State = ColumnStates.ParametersSelected;
                }
                ColumnContent.Focus();
            }
            SetWarningPanel();
            RefreshSelectionInfo();
        }

        private void SetWarningPanel()
        {
            if (WarningPanel == null) return;
            WarningPanel.Visibility = ModeIsFilter.IsChecked != null &&
                                      (_selectedItemsCount > 100 && !ModeIsFilter.IsChecked.Value)
                ? Visibility.Visible
                : Visibility.Collapsed;
        }

        private void ColumnContent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FilterPanel.InvalidateArrange();
            ParentColumn.SelectedValues.InvalidateArrange();
        }

        private void CheckGroupName(string groupID, bool check)
        {
            (from a in _meltList where a.GroupID == groupID && a.RowType == MeltRowType.Type select a).Single()
                .IsChecked = check;
        }

        private bool IsAnyItemChecked(string groupID)
        {
            return
                (from a in _meltList
                    where a.GroupID == groupID && a.RowType != MeltRowType.Type && a.IsChecked
                    select a).Any();
        }

        private void ModeIsFilter_Checked(object sender, RoutedEventArgs e)
        {
            SetModeToFilter();
            RefreshSelectionInfo();
        }

        private void ModeIsList_Checked(object sender, RoutedEventArgs e)
        {
            SetModeToList();
            RefreshSelectionInfo();
        }

        private void ModeIsCompressedList_Checked(object sender, RoutedEventArgs e)
        {
            SetModeToList();
            RefreshSelectionInfo();
        }

        private void SetModeToFilter()
        {
            var helper = Resources["FilterModeBindingHelper"] as BindingHelper;
            if (helper != null) helper.Value = false;
            SetWarningPanel();
        }

        private void SetModeToList()
        {
            var helper = Resources["FilterModeBindingHelper"] as BindingHelper;
            if (helper != null) helper.Value = true;
            SetWarningPanel();
        }

        private void CopyMeltList_OnClick(object sender, RoutedEventArgs e)
        {
            var textData = string.Empty;
            var isFirst = true;
            foreach (var s in _meltList.Where(a=>a.RowType == MeltRowType.Single && a.IsChecked))
            {
                if (!isFirst) textData += "\r\n";
                textData += s.Value;
                isFirst = false;
            }
            Clipboard.SetText(textData);
        }
    }

    public class MeltListData : ListData, INotifyPropertyChanged
    {
        private string _min;

        public string Min
        {
            get { return _min; }
            set
            {
                _min = value;
                NotifyPropertyChanged("Min");
            }
        }

        private string _max;

        public string Max
        {
            get { return _max; }
            set
            {
                _max = value;
                NotifyPropertyChanged("Max");
            }
        }

        private string _oven;

        public string Oven
        {
            get { return _oven; }
            set
            {
                _oven = value;
                NotifyPropertyChanged("Oven");
            }
        }

        private bool _ischecked;

        public new bool IsChecked
        {
            get { return _ischecked; }
            set
            {
                _ischecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        private string _groupid;

        public string GroupID
        {
            get { return _groupid; }
            set
            {
                _groupid = value;
                NotifyPropertyChanged("GroupID");
            }
        }

        private bool _isrange;

        public bool IsRange
        {
            get { return _isrange; }
            set
            {
                _isrange = value;
                NotifyPropertyChanged("IsRange");
            }
        }

        public MeltRowType RowType { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum MeltRowType
    {
        Single,
        Range,
        Type
    }
}
