﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Browser;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;
using AQControlsLibrary;
using AQBasicControlsLibrary;

namespace AQConstructorsLibrary
{
    public partial class SelectorParameters : UserControl, ISelectorBase
    {
         public SelectionColumn ParentColumn{get; set;}
        
        private ConstructorService cs;

        private List<List<object>> SourceData;
        private List<ParamListData> TempSourceData;
        private List<ParamListData> ColumnData;
        private int SelectedItemsCount = 0 ;

        public SelectorParameters(SelectionColumn parentColumn)
        {
            InitializeComponent();
            ParentColumn = parentColumn;
            cs = Services.GetConstructorService();
        }

        public void Refresh()
        {
            ParentColumn.SetPreviousFilter(); 
            FilterChanged(true);
        }

        public void ReflectStateChange()
        {
            if (ParentColumn.SelectedParameter != null)
            {
                if (ParentColumn.SelectedParameter.NumericSelectionAvailable)
                {
                    FilterPanel.Visibility = Visibility.Collapsed;
                }
                else
                {
                    FilterPanel.Visibility = Visibility.Visible;
                }
            }
            if (ParentColumn.State == ColumnStates.ParametersSelected)
            {
                if (ColumnContent.ItemsSource == null)
                {
                    if (ParentColumn.GetSelfPosition() > 1 || !ParentColumn.SelectedParameter.SearchAvailable)
                    {
                        
                        string JoinSubstitution = ((ParentColumn.IsDetailJoinRequired())? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin : "");
                        string SQL = ParentColumn.SelectedParameter.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", " 1=1 ").Replace("@DetailsJoin", JoinSubstitution);
                        ParentColumn.StartProgressIndicator();
                        cs.BeginGetRows(SQL, GetRowsDone, null);
                    }
                }
            }
        }

        protected void FilterChanged(bool PreserveValue)
        {
            SelectionParameterDescription spd = ParentColumn.SelectedParameter;
            string SQL;

            if (PreserveValue && ColumnContent.ItemsSource != null) TempSourceData = (from a in ((List<ParamListData>)ColumnContent.ItemsSource) where ((ParamListData)a).IsChecked select a).ToList();
            else TempSourceData = null;
            
            if (Filter.Text.Trim() == string.Empty || Filter.Text.Trim() == "Фильтр")
            {
                SQL = spd.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", " 1=1 ");
            }
            else
            {
                string[] Patterns = Filter.Text.Split('|');
                string AdditionalFilter = "(";

                bool IsFirst = true;
                foreach (string p in Patterns)
                {
                    if (!IsFirst) AdditionalFilter += " OR ";
                    AdditionalFilter += " " + spd.Field + " LIKE " + "('" + p + "')";
                    IsFirst = false;
                }
                AdditionalFilter += ")";
                SQL = spd.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", AdditionalFilter);
            }
            string JoinSubstitution = ((ParentColumn.IsDetailJoinRequired()) ? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin : "");
            SQL = SQL.Replace("@DetailsJoin", JoinSubstitution);
            ParentColumn.StartProgressIndicator();
            SelectedItemsCount = 0;
            SourceData = null;
            ColumnData = null;
            ColumnContent.Focus();
            ParentColumn.State = ColumnStates.ParametersSelected;
            ColumnContent.ItemsSource = null;
            ColumnContent.UpdateLayout();
            cs.BeginGetRows(SQL, GetRowsDone, null);
        }

        public void GetRowsDone(IAsyncResult iar)
        {
            SourceData = cs.EndGetRows(iar);

            ParentColumn.Dispatcher.BeginInvoke(delegate()
            {
                ParentColumn.StopProgressIndicator();
                ParentColumn.UpdateLayout();
            }); 
            
            ColumnContent.Dispatcher.BeginInvoke(delegate() 
            {
                ColumnData = new List<ParamListData>();
                foreach (var i in SourceData)
                {
                    if ((bool)i.ElementAt(5) == true)
                    {
                        ColumnData.Add(new ParamListData(i.ElementAt(2).ToString(), (string)i.ElementAt(3).ToString(), (int)i.ElementAt(2), false, false, string.Empty, string.Empty, ParametersRowType.Group));
                    }
                    if ((bool)i.ElementAt(6) == true)
                    {
                        ColumnData.Add(new ParamListData(i.ElementAt(1).ToString() + i.ElementAt(2).ToString() + i.ElementAt(4).ToString(), (string)i.ElementAt(1).ToString(), (int)i.ElementAt(2), (bool)i.ElementAt(4), false, string.Empty, string.Empty, ParametersRowType.Parameter));
                    }
                }

                ParentColumn.NoData.Visibility = (ColumnData.Count == 0)? Visibility.Visible: Visibility.Collapsed;
                if (ColumnData.Count == 0) return; 
                if (TempSourceData != null)
                {
                    foreach (var i in TempSourceData)
                    {
                        var ToRestore = (from t in ColumnData where i.Key == t.Key select t);

                        if (ToRestore.Count() == 1)
                        {
                            var n = ToRestore.Single();
                            n.IsChecked = i.IsChecked;
                            
                            n.Max = i.Max;
                            n.Min = i.Min;
                        }
                    }
                }
                TempSourceData = null;
                AutoSelectSmallCount();
                ColumnContent.ItemsSource = ColumnData;
                SelectedItemsCount = (from i in ColumnContent.ItemsSource.AsQueryable().Cast<ParamListData>() where i.IsChecked && i.RowType == ParametersRowType.Parameter select i).Count();
                if (SelectedItemsCount > 0) ParentColumn.State = ColumnStates.ValueSelected;
                ColumnContent.SelectedIndex = -1;
                ColumnContent.UpdateLayout();
            });
        }

        protected void AutoSelectSmallCount()
        {
            var Parameters = from a in ColumnData where a.RowType == ParametersRowType.Parameter select a;
            if (Parameters.Count() <= 1)
            {
                SelectedItemsCount = 0;
                foreach (var i in Parameters)
                {
                    i.IsChecked = true;
                    CheckGroupName(i.GroupID, true);
                    SelectedItemsCount++;
                }
                ParentColumn.State = ColumnStates.ValueSelected;
            }
        }

        List<int> GetParameterIDs(int GroupType, string DisplayParameterName)
        {
            return (from i in SourceData where (int)i.ElementAt(2) == GroupType && i.ElementAt(1).ToString() == DisplayParameterName select (int)i.ElementAt(7)).ToList<int>();
        }

        ParamListData GetParamListData(string Key)
        {
            return (from i in ColumnData where i.Key == Key select i).First<ParamListData>();
        }

        IQueryable GetCheckedItems()
        {
            return from i in ColumnContent.ItemsSource.AsQueryable().Cast<ParamListData>() where i.IsChecked && i.RowType == ParametersRowType.Parameter select i;
        }

        public FilterAndDescription GetFilterAndDescription()
        {
            
            string ResultedFilter = string.Empty;
            string ResultedParameterFilter = string.Empty;
            string ResultedValueFilter = string.Empty;
            string ResultedDescription = string.Empty;
            string ResultedSelectedItems = string.Empty;
            SelectionParameterDescription spd = ParentColumn.SelectedParameter;
            bool IsFirstDescription = true;
            bool IsFirstValueFilter = true;
            bool IsFirstGroup = true;
            string OldGroupName = string.Empty;
            ResultedFilter = " (case " + spd.ResultedFilterField;
            ResultedParameterFilter = " (case " + spd.ResultedFilterField;

            List<ValueFilterInfo> ResultedVFI = new List<ValueFilterInfo>();
            ResultedDescription = String.Empty;

            foreach (ParamListData pld in GetCheckedItems())
            {
                string ParameterGroupID = pld.GroupID.ToString();
                string GroupName = GetGroupNameById(pld.GroupID);
                if (GroupName != OldGroupName)
                {
                    if (!IsFirstGroup) ResultedSelectedItems += "\r\n";
                    IsFirstGroup = false;
                    ResultedSelectedItems += "● " + GroupName + ":\r\n";
                    OldGroupName = GroupName;
                }
                string ParameterName = pld.Value;
                var divider = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                string ValueMin = pld.Min.Replace(",", divider).Replace(".", divider); 
                string ValueMax = pld.Max.Replace(",", divider).Replace(".", divider);

                string ValueDescription = string.Empty;

                decimal From = decimal.MinValue;
                decimal To = decimal.MaxValue;
                bool HasMin = Decimal.TryParse(ValueMin, out From);
                bool HasMax = Decimal.TryParse(ValueMax, out To);

                if (!IsFirstDescription)
                {
                    ResultedDescription += "|";
                }

                IsFirstDescription = false;

                if (ParameterGroupID != string.Empty)
                {
                    foreach  (int PID in GetParameterIDs(pld.GroupID, pld.Value))
                    {
                        if (HasMin || HasMax)
                        {
                            ValueFilterInfo VFI = new ValueFilterInfo();
                            VFI.ParameterID = PID;
                            if (HasMin) VFI.Min = From;
                            if (HasMax) VFI.Max = To;
                            ResultedVFI.Add(VFI);
                        }
                        string ParameterID = PID.ToString();
                        string OptionalFieldFilter = " case when " + spd.ResultedFilterField + "=" + ParameterID + " then dbo.ToNumeric(" + spd.OptionalField + ") else null end ";

                        ResultedFilter += " when " + PID.ToString()  + " then ";
                        ResultedParameterFilter += " when " + PID.ToString() + " then ";
                        if (HasMin && !HasMax)
                        {
                            if (!IsFirstValueFilter)
                            {
                                ResultedValueFilter += "|";
                            }
                            ValueDescription = "[" + From.ToString() + ";∞)";
                            ResultedValueFilter += ParameterID + "@(MIN(" + OptionalFieldFilter + ")>='" + From.ToString().Replace(',', '.') + "')";
                            ResultedFilter += " (case when dbo.ToNumeric(" + spd.OptionalField + ")>=" + From.ToString().Replace(',', '.') + " then 1 else 0 end) ";
                            IsFirstValueFilter = false;
                        }
                        else
                        {
                            if (!HasMin && HasMax)
                            {
                                if (!IsFirstValueFilter)
                                {
                                    ResultedValueFilter += "|";
                                }
                                ValueDescription = "(-∞;" + To.ToString() + "]";
                                ResultedValueFilter += ParameterID + "@(MAX(" + OptionalFieldFilter + ")<='" + To.ToString().Replace(',', '.') + "')";
                                ResultedFilter += " (case when dbo.ToNumeric(" + spd.OptionalField + ")<=" + To.ToString().Replace(',', '.') + " then 1 else 0 end) ";
                                IsFirstValueFilter = false;
                            }
                            else
                            {
                                if (HasMin && HasMax)
                                {
                                    if (!IsFirstValueFilter)
                                    {
                                        ResultedValueFilter += "|";
                                    }
                                    ValueDescription = "[" + From.ToString() + ";" + To.ToString() + "]";
                                    ResultedValueFilter += ParameterID + "@(MIN(" + OptionalFieldFilter + ")>='" + From.ToString().Replace(',', '.') + "' and MAX(" + OptionalFieldFilter + ")<='" + To.ToString().Replace(',', '.') + "')";
                                    ResultedFilter += " (case when dbo.ToNumeric(" + spd.OptionalField + ")<=" + To.ToString().Replace(',', '.') + " and dbo.ToNumeric(" + spd.OptionalField + ")>=" + From.ToString().Replace(',', '.') + " then 1 else 0 end) ";
                                    IsFirstValueFilter = false;
                                }
                                else
                                {
                                    ResultedFilter += " 1 ";
                                }
                            }
                        }
                        ResultedParameterFilter += " 1 ";
                    }
                    ResultedDescription += ParameterName + ValueDescription;
                    ResultedSelectedItems += "   • " + ParameterName + ValueDescription + "\r\n";
                }
            }
            ResultedDescription.Replace("|", "\r\n");
            ResultedDescription = spd.Name + ": " + ResultedDescription.Replace("|", ";"); ;
            ResultedFilter += " else 0 end) =1 ";
            ResultedParameterFilter += " else 0 end) =1 ";

            FilterAndDescription fad = new FilterAndDescription();
            fad.Description = ResultedDescription;
            fad.SQLFilter = ResultedFilter;
            fad.ParameterSQLFilter = ResultedParameterFilter;
            fad.SelectedItems = ResultedSelectedItems;
            fad.ValueFilter = ResultedValueFilter;
            fad.ValueFilterInfo = ResultedVFI;
            return fad;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ParentColumn.SetPreviousFilter();
            FilterChanged(false);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            ParamListData currentData = GetParamListData(cb.Tag.ToString());

            if (currentData.RowType == ParametersRowType.Parameter)
            {
                if (cb.IsChecked.Value)
                {
                    var i = (from a in ((List<ParamListData>)ColumnContent.ItemsSource) where ((ParamListData)a).Key.ToString() == ((CheckBox)sender).Tag.ToString() select a).Single();
                    i.IsChecked = true;
                    CheckGroupName(currentData.GroupID, true);
                    SelectedItemsCount++;
                    ParentColumn.State = ColumnStates.ValueSelected;
                }
                else
                {
                    var i = (from a in ((List<ParamListData>)ColumnContent.ItemsSource) where ((ParamListData)a).Key.ToString() == ((CheckBox)sender).Tag.ToString() select a).Single();
                    i.IsChecked = false;
                    i.Max = string.Empty;
                    i.Min = string.Empty;
                    if (!IsAnyItemChecked(currentData.GroupID)) CheckGroupName(currentData.GroupID, false);
                    SelectedItemsCount--;
                    if (SelectedItemsCount == 0) ParentColumn.State = ColumnStates.ParametersSelected;
                }
            }
            else
            {
                if (cb.IsChecked.Value)
                {
                    var i = from a in ColumnData where a.GroupID.ToString() == currentData.GroupID.ToString() select a;
                    foreach (ParamListData pld in i)
                    {
                        if (!pld.IsChecked)SelectedItemsCount++;
                        pld.IsChecked = true;
                        
                    }
                ParentColumn.State = ColumnStates.ValueSelected;
                    ColumnContent.Focus();
                }
                else
                {
                    var i = from a in ColumnData where a.GroupID.ToString() == currentData.GroupID.ToString() select a;
                    foreach (ParamListData pld in i)
                    {
                        if (pld.IsChecked) SelectedItemsCount--;
                        pld.Max = string.Empty;
                        pld.Min = string.Empty;
                        pld.IsChecked = false;
                        
                    }
                    if (SelectedItemsCount == 0) ParentColumn.State = ColumnStates.ParametersSelected;
                    ColumnContent.Focus();
                }
            }
            WarningPanel.Visibility = (SelectedItemsCount > 100) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ColumnContent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FilterPanel.InvalidateArrange();
            ParentColumn.SelectedValues.InvalidateArrange();
        }

        private void MinTextBox_TextChanged(object o, EventArgs e)
        {
            ExtendedTextBox tb = (ExtendedTextBox)o;
            ParamListData currentData = GetParamListData(tb.Tag.ToString());
            if (tb.Text != string.Empty)
            {
                if (!currentData.IsChecked) SelectedItemsCount++; 
                currentData.IsChecked = true;
                CheckGroupName(currentData.GroupID, true);
                if (ParentColumn.State == ColumnStates.ParametersSelected)ParentColumn.State = ColumnStates.ValueSelected;
            }
        }

        private void MaxTextBox_TextChanged(object o, EventArgs e)
        {
            ExtendedTextBox tb = (ExtendedTextBox)o;
            ParamListData currentData = GetParamListData(tb.Tag.ToString());
            if (tb.Text != string.Empty)
            {
                if (!currentData.IsChecked) SelectedItemsCount++;
                currentData.IsChecked = true;
                CheckGroupName(currentData.GroupID, true);
                if (ParentColumn.State == ColumnStates.ParametersSelected) ParentColumn.State = ColumnStates.ValueSelected;
            }
        }

        private void CheckGroupName(int GroupID, bool Check)
        {
            (from a in ColumnData where a.GroupID == GroupID && a.RowType == ParametersRowType.Group select a).Single().IsChecked = Check;
        }

        private string GetGroupNameById(int GroupID)
        {
            return (from a in ColumnData where a.GroupID == GroupID && a.RowType == ParametersRowType.Group select a).Single().Value;
        }
        
        private bool IsAnyItemChecked(int GroupID)
        {
            return (from a in ColumnData where a.GroupID == GroupID && a.RowType == ParametersRowType.Parameter && a.IsChecked select a).Count()>0;
        }
    }

    public class ParamListData : ListData, INotifyPropertyChanged
    {
        private string min;
        public string Min 
        { 
            get
            {
                return min;
            } 
            set
            {
                min = value;
                NotifyPropertyChanged("Min");
            } 
        }

        private string max;
        public string Max
        {
            get
            {
                return max;
            }
            set
            {
                max = value;
                NotifyPropertyChanged("Max");
            }
        }

        private bool ischecked;
        public bool IsChecked 
        {
            get
            {
                return ischecked;
            }
            set
            {
                ischecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        private int groupid;
        public int GroupID 
        {
            get
            {
                return groupid;
            }
            set
            {
                groupid = value;
                NotifyPropertyChanged("GroupID");
            }
        }

        private bool isnumber;
        public bool IsNumber 
        {
            get
            {
                return isnumber;
            }
            set
            {
                isnumber = value;
                NotifyPropertyChanged("IsNumber");
            }
        }

        public ParametersRowType RowType { get; set; }
        
        public ParamListData(string key, string value, int groupID, bool isNumber, bool isChecked, string min, string max, ParametersRowType rowType)
            : base(key, value, isChecked)
        {
            Min = min;
            Max = max;
            GroupID = groupID;
            IsNumber = isNumber;
            RowType = rowType;
            IsChecked = isChecked;
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }  
    }

    public enum ParametersRowType {Parameter, Group }

   
}
