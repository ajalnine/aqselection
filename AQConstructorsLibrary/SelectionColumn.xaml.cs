﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using AQControlsLibrary;
using AQBasicControlsLibrary;

namespace AQConstructorsLibrary
{
    public partial class SelectionColumn : UserControl
    {
        private SelectionColumnsPresenter parentColumnsPresenter;
        public SelectionColumnsPresenter ParentColumnsPresenter
        {
            get { return parentColumnsPresenter; }
            set { parentColumnsPresenter = value; }
        }
        private ColumnStates state;
        public ColumnStates State { 
            get
            {
                return state;
            }
             
            set
            {
                state = value;
                ReflectStateChange();
            } 
        }

        public SelectionParameterDescription SelectedParameter { get; set; }

        public FilterAndDescription FAD = new FilterAndDescription();
        
        public string PreviousFilter { get; set; } 

        private IQueryable FilteredSPL;
        
        public SelectionColumn()
        {
            InitializeComponent();
        }

        public void SetSPL()
        {
            FilteredSPL = from i in ParentColumnsPresenter.SelectionParameters.ParametersList.AsQueryable() where !ParentColumnsPresenter.UsedParameters.Contains(i.ID) select i;
            ConditionsPanel.DataContext = FilteredSPL;
            //SetEndOfSelectionButtonVisibility();
        }

        public void SetPreviousFilter()
        {
            PreviousFilter = String.Empty;
            bool IsFirst = true;
            if (ParentColumnsPresenter.FirstFilter != string.Empty && ParentColumnsPresenter.FirstFilter != null)
            {
                PreviousFilter += "WHERE (" + ParentColumnsPresenter.FirstFilter + ")";
                IsFirst = false;
            }

            if (GetSelfPosition() != 0)
            {
                if (IsFirst) PreviousFilter = "WHERE ";
                var cp = ParentColumnsPresenter.SelectionColumnsPanel.Children;
                int CurrentIndex = cp.IndexOf(this);
                for (int i = 0; i < CurrentIndex; i++)
                {
                    string rf = ((SelectionColumn)cp[i]).FAD.SQLFilter;
                    if (!IsFirst && rf!=string.Empty) PreviousFilter += " AND ";
                    IsFirst = false;
                    PreviousFilter += rf;
                }
            }
        }

        public bool IsDetailJoinRequired()
        {
            bool Result = false;
            
            if (GetSelfPosition() != 0)
            {
                var cp = ParentColumnsPresenter.SelectionColumnsPanel.Children;
                int CurrentIndex = cp.IndexOf(this); 
                
                for (int i = 0; i < CurrentIndex; i++)
                {
                    Result |= ((SelectionColumn)cp[i]).SelectedParameter.EnableDetailsJoin;
                }
            }
            return Result;
        }

        private void ReflectStateChange()
        {
            int CurrentIndex = GetSelfPosition();
            EndOfSelection.IsEnabled = ParentColumnsPresenter.FinishEnabled;
            switch (State)
            {
                case ColumnStates.ParametersSelection:
                    HeaderLabel.Text = "Выбор условия";
                    ReselectValuesButton.Visibility = Visibility.Collapsed;
                    RefreshButton.Visibility = Visibility.Collapsed;
                    ReselectConditionButton.Visibility = Visibility.Collapsed;
                    RemoveFromListButton.Visibility = Visibility.Collapsed;
                    SelectedValues.Visibility = Visibility.Collapsed;
                    SelectorPlaceholder.Visibility = Visibility.Collapsed;
                    ConditionsPanel.Visibility = Visibility.Visible;
                    NoData.Visibility = Visibility.Collapsed;
                    SelectorPlaceholder.Children.Clear();
                    SetEndOfSelectionButtonVisibility();
                    break;

                case ColumnStates.ParametersSelected:
                    ReselectValuesButton.Visibility = Visibility.Collapsed;
                    RefreshButton.Visibility = Visibility.Visible;
                    ReselectConditionButton.Visibility = Visibility.Visible;
                    RemoveFromListButton.Visibility = Visibility.Collapsed;
                    
                    HeaderLabel.Text = SelectedParameter.Name;
                    SelectedValues.Visibility = Visibility.Collapsed;
                    SelectorPlaceholder.Visibility = Visibility.Visible;
                    ConditionsPanel.Visibility = Visibility.Collapsed;
                    if (SelectorPlaceholder.Children.Count == 0)
                    {
                        switch (SelectedParameter.Selector)
                        {
                            case ColumnTypes.Discrete:
                                SelectorPlaceholder.Children.Add(new SelectorDiscrete(this));
                                break;
                            case ColumnTypes.MeltList:
                                SelectorPlaceholder.Children.Add(new SelectorMeltList(this));
                                break;
                            case ColumnTypes.Parameters:
                                SelectorPlaceholder.Children.Add(new SelectorParameters(this));
                                break;
                            case ColumnTypes.Text:
                                SelectorPlaceholder.Children.Add(new SelectorText(this));
                                break;
                            default:
                                break;
                        }
                    }
                    SetClosedStateToPreviousColumn();
                    SetPreviousFilter();
                    EndOfSelection.Visibility = Visibility.Collapsed;
                    ClearColumnsAfterSelf();
                    break;

                case ColumnStates.ValueSelected:
                    ReselectValuesButton.Visibility = Visibility.Collapsed;
                    RefreshButton.Visibility = Visibility.Visible;
                    ReselectConditionButton.Visibility = Visibility.Visible;
                    RemoveFromListButton.Visibility = Visibility.Visible;
                    HeaderLabel.Text = SelectedParameter.Name;
                    SelectedValues.Visibility = Visibility.Collapsed;
                    SelectorPlaceholder.Visibility = Visibility.Visible;
                    ConditionsPanel.Visibility = Visibility.Collapsed;
                    SetOpenStateToNextColumn();
                    EndOfSelection.Visibility = Visibility.Collapsed;
                    break;

                case ColumnStates.ValueAccepted:
                    NoData.Visibility = Visibility.Collapsed; 
                    HeaderLabel.Text = SelectedParameter.Name;
                    ReselectValuesButton.Visibility = Visibility.Visible;
                    RefreshButton.Visibility = Visibility.Collapsed;
                    ReselectConditionButton.Visibility = Visibility.Visible;
                    RemoveFromListButton.Visibility = Visibility.Visible;
                    SelectedValues.Visibility = Visibility.Visible;
                    SelectorPlaceholder.Visibility = Visibility.Collapsed;
                    ConditionsPanel.Visibility = Visibility.Collapsed;
                    EndOfSelection.Visibility = Visibility.Collapsed;
                    FAD = ((ISelectorBase)SelectorPlaceholder.Children[0]).GetFilterAndDescription();
                    SelectedValues.Text = FAD.SelectedItems;
                    break;

                default:
                    break;
            }
            if (SelectorPlaceholder.Children.Count>0)((ISelectorBase)SelectorPlaceholder.Children[0]).ReflectStateChange();
        }

        private void SetEndOfSelectionButtonVisibility()
        {
            int CurrentIndex = GetSelfPosition(); 
            if (CurrentIndex > 0)
            {
                EndOfSelection.Visibility = Visibility.Visible;
            }
            else
            {
                EndOfSelection.Visibility = Visibility.Collapsed;
            }
        }

        public void EnableFinish(bool Enable)
        {
            EndOfSelection.IsEnabled = Enable;
        }
        
        private void ConditionButton_Click(object sender, RoutedEventArgs e)
        {
            int id = (int)((TextImageButtonBase)sender).Tag;
            SelectedParameter = (from i in ParentColumnsPresenter.SelectionParameters.ParametersList.AsQueryable() where i.ID == id select i).SingleOrDefault();
            ParentColumnsPresenter.UsedParameters.Add(id);
            State = ColumnStates.ParametersSelected;
        }


        private void ReselectValuesButton_Click(object sender, RoutedEventArgs e)
        {
            ((ISelectorBase)SelectorPlaceholder.Children[0]).Refresh();
//            State = ColumnStates.ValueSelected;
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            SetPreviousFilter();
            ISelectorBase selector = ((ISelectorBase)SelectorPlaceholder.Children[0]);
            selector.Refresh();
        }

        private void RemoveFromListButton_Click(object sender, RoutedEventArgs e)
        {
            ParentColumnsPresenter.UsedParameters.Remove(SelectedParameter.ID);
            SetSPL(); 
            if (ParentColumnsPresenter.SelectionColumnsPanel.Children.Count == 1)
            {
                ((SelectionColumn)ParentColumnsPresenter.SelectionColumnsPanel.Children[0]).State = ColumnStates.ParametersSelection;
                return;
            }
            ParentColumnsPresenter.RemoveColumn(this);

            SelectionColumn LastColumn = ((SelectionColumn)ParentColumnsPresenter.SelectionColumnsPanel.Children.Last());
            int Index = ParentColumnsPresenter.SelectionColumnsPanel.Children.Count - 2;
            SelectionColumn PreviousColumn = null;
            if (Index >= 0)
            {
                PreviousColumn = ((SelectionColumn)ParentColumnsPresenter.SelectionColumnsPanel.Children.ElementAt(Index));
            }
            switch (LastColumn.State)
            {
                case ColumnStates.ParametersSelected:
                    ((ISelectorBase)LastColumn.SelectorPlaceholder.Children.First()).Refresh();
                    break;
                case ColumnStates.ValueAccepted:
                    ((ISelectorBase)LastColumn.SelectorPlaceholder.Children.First()).Refresh();
                    break;
                case ColumnStates.ParametersSelection:
                    if (PreviousColumn!=null)  ((ISelectorBase)PreviousColumn.SelectorPlaceholder.Children.First()).Refresh();
                    break;
            }
        }

        private void ReselectConditionButton_Click(object sender, RoutedEventArgs e)
        {
            ClearColumnsAfterSelf();
            ParentColumnsPresenter.UsedParameters.Remove(SelectedParameter.ID); 
            State = ColumnStates.ParametersSelection;
            SetSPL();
        }

        private void SetClosedStateToPreviousColumn()
        {
            int CurrentIndex = GetSelfPosition();
            if (CurrentIndex > 0)
            {
                ((SelectionColumn)ParentColumnsPresenter.SelectionColumnsPanel.Children[CurrentIndex - 1]).State = ColumnStates.ValueAccepted;
            }
        }

        public int GetSelfPosition()
        {
            var cp = ParentColumnsPresenter.SelectionColumnsPanel.Children;
            return cp.IndexOf(this);
        }

        private void SetOpenStateToNextColumn()
        {
            ClearColumnsAfterSelf();
            ParentColumnsPresenter.AddColumn();
        }

        private void ClearColumnsAfterSelf()
        {
            var cp = ParentColumnsPresenter.SelectionColumnsPanel.Children;
            int CurrentIndex = cp.IndexOf(this);
            for (int i = cp.Count()-1; i > CurrentIndex; i--)
            {
                SelectionParameterDescription ToRemove = ((SelectionColumn)cp[i]).SelectedParameter;
                if (ToRemove!=null)  ParentColumnsPresenter.UsedParameters.Remove(ToRemove.ID);
                cp.RemoveAt(i);
            }
        }

        public void StartProgressIndicator()
        {
            ProgressIndicator.Visibility = Visibility.Visible;
        }

        public void StopProgressIndicator()
        {
            ProgressIndicator.Visibility = Visibility.Collapsed;
        }

        private void EndOfSelection_Click(object sender, RoutedEventArgs e)
        {
            SelectionColumn PreviousColumn = ((SelectionColumn)ParentColumnsPresenter.SelectionColumnsPanel.Children.ElementAt(ParentColumnsPresenter.SelectionColumnsPanel.Children.Count - 2));
            PreviousColumn.State = ColumnStates.ValueAccepted;
            ParentColumnsPresenter.RemoveColumnAndFinish(this);
        }
    }

    public enum ColumnStates {ParametersSelection, ParametersSelected, ValueSelected, ValueAccepted, Hidden}
}
