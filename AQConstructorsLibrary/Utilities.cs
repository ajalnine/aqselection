﻿using System;
using System.Net;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Linq;
using ApplicationCore.ConstructorServiceReference;

namespace AQConstructorsLibrary
{
    public static class FieldListOperations
    {
        public static bool IsCombined(IEnumerable<FieldList> CombinedFields, int i)
        {
            return (from FieldList fl in CombinedFields where fl.Combined.Contains(i) && fl.Enabled select fl).Any();
        }

        public static bool IsRequireToCombine(IEnumerable<FieldList> CombinedFields, int i)
        {
            return (from FieldList fl in CombinedFields where fl.MainParameterID == i && fl.Enabled select fl).Any();
        }

        public static string GetCombineString(List<FieldList> CombinedFields, int i)
        {
            var list = from FieldList fl in CombinedFields where fl.MainParameterID == i select fl;
            if (list.Any())
            {
                string Result = list.First().MainParameterID.ToString();

                foreach (int l in list.First().Combined)
                {
                    Result += "," + l.ToString();
                }
                return Result;
            }
            else return string.Empty;
        }
    }

    public static class ParametersUtility
    {
        public static bool IsDuplicatedParameter(ParametersList tpl, string value)
        {
            if (tpl.DuplicatedParameters == null) return false;
            return tpl.DuplicatedParameters.Contains(value.ToLower());
        }
    }

    public static class FilterUtility
    {
        public static string GetValueFilterString(string ValueFilter, int ParamID)
        {
            string[] ValueFilters = ValueFilter.Split('|');

            var l = (from string vf in ValueFilters where vf.Split('@')[0] == ParamID.ToString() || vf.Split('@')[0].Replace(" ", string.Empty).ToUpper() == "AND" + ParamID.ToString() select vf.Split('@')[1]);
            var enumerable = l as IList<string> ?? l.ToList();
            if (enumerable.Any()) return enumerable.First<string>();
            else return String.Empty;
        }
    }

    public class BindingHelper : INotifyPropertyChanged
    {
        private object _value;

        public object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
