﻿using System;

namespace AQConstructorsLibrary
{
    public interface ISelectorBase
    {
        SelectionColumn ParentColumn{get; set;}
        void ReflectStateChange();
        void Refresh();
        FilterAndDescription GetFilterAndDescription();
    }
}
