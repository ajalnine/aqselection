﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Browser;
using ApplicationCore.ConstructorServiceReference;
using AQBasicControlsLibrary;
using AQControlsLibrary;

namespace AQConstructorsLibrary
{
    public class ParametersRowTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            LinearGradientBrush lgb = new LinearGradientBrush();
            lgb.StartPoint = new Point(0, 0);
            lgb.EndPoint = new Point(0, 1);
            GradientStop gs1 = new GradientStop();
            GradientStop gs2 = new GradientStop();
            GradientStop gs3 = new GradientStop();

            if ((ParametersRowType)value != ParametersRowType.Parameter)
            {
                gs1.Color = Color.FromArgb(0x20, 0x26, 0x8f, 0x97);
                gs2.Color = Color.FromArgb(0x30, 0x26, 0x8f, 0x97);
                gs3.Color = Color.FromArgb(0x20, 0x26, 0x8f, 0x97);
            }
            else
            {
                gs1.Color = Color.FromArgb(0, 250, 250, 250);
                gs2.Color = Color.FromArgb(0, 255, 255, 255);
                gs3.Color = Color.FromArgb(0, 250, 250, 250);
            }
            gs1.Offset = 0;
            gs2.Offset = 0.5;
            gs3.Offset = 1;
            lgb.GradientStops.Add(gs1);
            lgb.GradientStops.Add(gs2);
            lgb.GradientStops.Add(gs3);
            return lgb;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class MeltRowTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            LinearGradientBrush lgb = new LinearGradientBrush();
            lgb.StartPoint = new Point(0, 0);
            lgb.EndPoint = new Point(0, 1);
            GradientStop gs1 = new GradientStop();
            GradientStop gs2 = new GradientStop();
            GradientStop gs3 = new GradientStop();

            if ((MeltRowType)value != MeltRowType.Type)
            {
                gs1.Color = Color.FromArgb(0, 250, 250, 250);
                gs2.Color = Color.FromArgb(0, 255, 255, 255);
                gs3.Color = Color.FromArgb(0, 250, 250, 250);
            }
            else
            {
                gs1.Color = Color.FromArgb(0x20, 0x26, 0x8f, 0x97);
                gs2.Color = Color.FromArgb(0x30, 0x26, 0x8f, 0x97);
                gs3.Color = Color.FromArgb(0x20, 0x26, 0x8f, 0x97);
            }
            gs1.Offset = 0;
            gs2.Offset = 0.5;
            gs3.Offset = 1;
            lgb.GradientStops.Add(gs1);
            lgb.GradientStops.Add(gs2);
            lgb.GradientStops.Add(gs3);
            return lgb;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotSupportedException();
        }
    }
   
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool visibility = (bool)value;
            return visibility ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;
            return (visibility == Visibility.Visible);
        }
    }

    public class IDToMainParameterNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = from pl in WorkParametersList.ParametersList.AllParameters where pl.ParamID == (int)value select pl.Param;
            if (source.Count() == 1) return source.Single();
            else return string.Empty;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotSupportedException();
        }
    }

    public class IDToCombinedParameterNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = from pl in WorkParametersList.ParametersList.AllParameters where pl.ParamID == (int)value select pl.ParamOld;
            if (source.Count() == 1) return source.Single();
            else return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotSupportedException();
        }
    }

    public class GroupToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = from pl in WorkParametersList.ParametersList.AllParameters where pl.ParamID == (int)value select pl.ParamType;
            if (source.Count() > 0) return source.First();
            else return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotSupportedException();
        }
    }
    
    public static class WorkParametersList
    {
        public static ParametersList ParametersList;
    }
}
