﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;
using AQControlsLibrary;

namespace AQConstructorsLibrary
{
    public partial class SelectorText : UserControl, ISelectorBase
    {
        public SelectionColumn ParentColumn{get; set;}
        
        private ConstructorService cs;

        private string CurrentFilter;

        public SelectorText(SelectionColumn parentColumn)
        {
            InitializeComponent();
            ParentColumn = parentColumn;
            cs = Services.GetConstructorService();
        }

        public void Refresh()
        {
            ParentColumn.SetPreviousFilter(); 
            FilterChanged();
        }

        public void ReflectStateChange()
        {
           
        }

        protected void FilterChanged()
        {
            if (Filter.Text.Trim() != string.Empty) 
            {
                ParentColumn.State = ColumnStates.ValueSelected;
                CurrentFilter = Filter.Text.Trim();
            }
            else 
            {
                ParentColumn.State = ColumnStates.ParametersSelected;
                CurrentFilter = null;
            }
            
        }

        public FilterAndDescription GetFilterAndDescription()
        {
            string ResultedFilter = string.Empty;
            string ResultedDescription = string.Empty;
            string ResultedSelectedItems = string.Empty;
            
            SelectionParameterDescription spd = ParentColumn.SelectedParameter;
            ResultedDescription = ParentColumn.SelectedParameter.Name + ": Поиcк по \"" + CurrentFilter + "\"";
            ResultedFilter = " " + spd.ResultedFilterField + " like '"+CurrentFilter + "' ";
            ResultedSelectedItems = CurrentFilter;

            
            FilterAndDescription fad = new FilterAndDescription();
            fad.Description = ResultedDescription;
            fad.SQLFilter = ResultedFilter;
            fad.SelectedItems = ResultedSelectedItems;
            fad.ValueFilter = string.Empty;
            return fad;
        }

        private void Filter_TextChanged(object o, EventArgs e)
        {
            FilterChanged();
        }
    }
}