﻿using System;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// Summary description for SelectionParameterDescription
/// </summary>
namespace AQConstructorsLibrary
{
    public class SelectionParameterDescription
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public bool SearchAvailable { get; set; }
        public bool NumericSelectionAvailable { get; set; }
        public bool LastOnly { get; set; }
        public bool IncludeInDescription { get; set; }
        public string SQLTemplate { get; set; }
        public string ResultedFilterField { get; set; }
        public string OptionalField { get; set; }
        public string Field { get; set; }
        public ColumnTypes Selector { get; set; }
        public bool IsNumber { get; set; }
        public bool IgnoreFilterForChemistry { get; set; }
        public bool EnableDetailsJoin { get; set; }

        public SelectionParameterDescription()
        {
            ID = -1;
            Name = String.Empty;
            Field = String.Empty;
            SQLTemplate = String.Empty;
            ResultedFilterField = String.Empty;
            OptionalField = String.Empty;
            SearchAvailable = false;
            NumericSelectionAvailable = false;
            LastOnly = false;
            IsNumber = false;
            IncludeInDescription = true;
            IgnoreFilterForChemistry = false;
            EnableDetailsJoin = false;
        }
    }
    public enum ColumnTypes { Discrete, MeltList, Parameters, Text }
}