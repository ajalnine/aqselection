﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore.ConstructorServiceReference;
using ApplicationCore;

namespace AQConstructorsLibrary
{
    public partial class SelectorDiscrete : UserControl, ISelectorBase
    {
        public SelectionColumn ParentColumn{get; set;}
        
        private ConstructorService cs;

        private List<List<object>> SourceData;
        private List<object> TempSourceData;
        private List<ListData> ColumnData;

        private int SelectedItemsCount = 0 ;
        
        public SelectorDiscrete(SelectionColumn parentColumn)
        {
            InitializeComponent();
            ParentColumn = parentColumn;
            cs = Services.GetConstructorService();
        }

        public void Refresh()
        {
            ParentColumn.SetPreviousFilter(); 
            FilterChanged(true);
        }

        public void ReflectStateChange()
        {
            if (ParentColumn.SelectedParameter != null)
            {
                if (ParentColumn.SelectedParameter.NumericSelectionAvailable)
                {
                    FilterPanel.Visibility = Visibility.Collapsed;
                    NumericFilterPanel.Visibility = Visibility.Visible;
                }
                else
                {
                    FilterPanel.Visibility = Visibility.Visible;
                    NumericFilterPanel.Visibility = Visibility.Collapsed;
                }
            }
            if (ParentColumn.State == ColumnStates.ParametersSelected)
            {
                if (ColumnContent.ItemsSource == null)
                {
                    if (ParentColumn.GetSelfPosition() > 1 || !ParentColumn.SelectedParameter.SearchAvailable)
                    {
                        
                        string JoinSubstitution = ((ParentColumn.IsDetailJoinRequired())? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin : "");
                        string SQL = ParentColumn.SelectedParameter.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", " 1=1 ").Replace("@DetailsJoin", JoinSubstitution);
                        ParentColumn.StartProgressIndicator();
                        cs.BeginGetRows(SQL, GetRowsDone, null);
                    }
                }
            }
        }

        protected void FilterChanged(bool PreserveValue)
        {
            SelectionParameterDescription spd = ParentColumn.SelectedParameter;
            string SQL;

            if (PreserveValue && ColumnContent.ItemsSource!=null) TempSourceData = (from a in ((List<ListData>)ColumnContent.ItemsSource) where ((ListData)a).IsChecked select (object)a.Key).ToList();
            else TempSourceData = null;
            
            if (Filter.Text.Trim() == string.Empty)
            {
                SQL = spd.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", " 1=1 ");
            }
            else
            {
                string[] Patterns = Filter.Text.Split('\r');
                string AdditionalFilter = "(";

                bool IsFirst = true;
                foreach (string p in Patterns)
                {
                    if (!IsFirst) AdditionalFilter += " OR ";
                    AdditionalFilter += " " + spd.Field + " LIKE " + "('" + p.Trim() + "')";
                    IsFirst = false;
                }
                AdditionalFilter += ")";
                SQL = spd.SQLTemplate.Replace("@PreviousFilter", " " + ParentColumn.PreviousFilter + " ").Replace("@AdditionalFilter", AdditionalFilter);
            }
            string JoinSubstitution = ((ParentColumn.IsDetailJoinRequired()) ? ParentColumn.ParentColumnsPresenter.SelectionParameters.DetailsJoin : "");
            SQL = SQL.Replace("@DetailsJoin", JoinSubstitution);
            ParentColumn.StartProgressIndicator();
            SelectedItemsCount = 0;
            SourceData = null;
            ColumnData = null;
            ColumnContent.Focus();
            ParentColumn.State = ColumnStates.ParametersSelected;
            ColumnContent.ItemsSource = null;
            ColumnContent.SelectedIndex = -1;
            ColumnContent.UpdateLayout();
            cs.BeginGetRows(SQL, GetRowsDone, null);
        }

        public void GetRowsDone(IAsyncResult iar)
        {
            SourceData = cs.EndGetRows(iar);

            ParentColumn.Dispatcher.BeginInvoke(delegate()
            {
                ParentColumn.StopProgressIndicator();
                ParentColumn.UpdateLayout();
            }); 
            
            ColumnContent.Dispatcher.BeginInvoke(delegate() 
            {
                
                ColumnData = (from i in SourceData select new ListData(i.First().ToString(), (string)i.Last().ToString(), false)).ToList();
                ParentColumn.NoData.Visibility = (ColumnData.Count == 0)? Visibility.Visible: Visibility.Collapsed;
                SelectedItemsCount = 0;
                if (ColumnData.Count == 0) return; 
                if (TempSourceData != null)
                {
                    foreach (var i in ColumnData)
                    {
                        if (TempSourceData.Contains((object)i.Key))
                        {
                            i.IsChecked = true;
                            SelectedItemsCount++;
                        }
                    }
                }
                TempSourceData = null;
                AutoSelectSmallCount();
                if (SelectedItemsCount > 0) ParentColumn.State = ColumnStates.ValueSelected;
                if (ColumnData.Count > 0) SelectValuesPanel.Visibility = Visibility.Visible;
                else SelectValuesPanel.Visibility = Visibility.Collapsed;
                ColumnContent.ItemsSource = ColumnData;
                ColumnContent.SelectedIndex = -1;
                ColumnContent.UpdateLayout();
            });
        }

        protected void AutoSelectSmallCount()
        {
            if (ColumnData.Count() <= 1)
            {
                foreach (var i in ColumnData)
                {
                    i.IsChecked = true;
                    SelectedItemsCount++;
                }
                ParentColumn.State = ColumnStates.ValueSelected;
            }
        }

        public FilterAndDescription GetFilterAndDescription()
        {
            string ResultedFilter = string.Empty;
            string ResultedDescription = string.Empty;
            string ResultedSelectedItems = string.Empty;
            var CheckedListValues = ColumnContent.ItemsSource.AsQueryable().Cast<ListData>().Where(x => x.IsChecked == true).Select<ListData, ListData>(y => y);
            
            SelectionParameterDescription spd = ParentColumn.SelectedParameter;
            switch (CheckedListValues.Count())
            {
                case 0:
                    if (spd != null)
                    {
                        string[] ResultedFilterFields = spd.ResultedFilterField.Split('|');

                        if (ResultedFilterFields.Count() == 1)
                        {
                            ResultedFilter = "(" + ResultedFilterFields[0] + " is null)";
                        }
                        else
                        {
                            bool IsFirst = true;
                            ResultedFilter = "(";
                            foreach (string s in ResultedFilterFields)
                            {
                                if (!IsFirst) ResultedFilter += " OR ";
                                ResultedFilter += "(" + s + " is null)";
                                IsFirst = false;
                            }
                            ResultedFilter += ")";

                        }
                        ResultedDescription = ParentColumn.SelectedParameter.Name + ": нет значения";
                        ResultedSelectedItems = "Нет значения";
                    }
                    break;

                case 1:
                    ResultedDescription = ParentColumn.SelectedParameter.Name + ": " + CheckedListValues.First().Value;
                    ResultedSelectedItems = CheckedListValues.First().Value;
                    string[] ResultedFilterFields2 = spd.ResultedFilterField.Split('|');
                    if (ResultedFilterFields2.Count() == 1)
                    {
                        if (spd.IsNumber) ResultedFilter = "(" + ResultedFilterFields2[0] + "='" + CheckedListValues.First().Value.Replace(',', '.') + "')";
                        else ResultedFilter = "(" + ResultedFilterFields2[0] + "='" + CheckedListValues.First().Key + "')";
                    }
                    else
                    {
                        bool IsFirst2 = true;
                        ResultedFilter = "(";
                        foreach (string s in ResultedFilterFields2)
                        {
                            if (!IsFirst2) ResultedFilter += " OR ";
                            if (spd.IsNumber) ResultedFilter += "(" + s + "='" + CheckedListValues.First().Value.Replace(',', '.') + "')";
                            else ResultedFilter += "(" + s + "='" + CheckedListValues.First().Key + "')";
                            IsFirst2 = false;
                        }
                        ResultedFilter += ")";
                    }
                    break;

                default:

                    string[] ResultedFilterFields3 = spd.ResultedFilterField.Split('|');
                    ResultedDescription = ParentColumn.SelectedParameter.Name + ": ";
                    if (ResultedFilterFields3.Count() == 1)
                    {
                        ResultedFilter = "(" + ResultedFilterFields3[0] + " in (";
                        bool IsFirst3 = true;

                        foreach (ListData l in CheckedListValues)
                        {
                            if (l.Value == " Пустые") continue;
                            if (!IsFirst3)
                            {
                                ResultedFilter += ",";
                                ResultedDescription += "; ";
                                ResultedSelectedItems += "\r\n";
                            }
                            IsFirst3 = false;
                            if (spd.IsNumber) ResultedFilter += "'" + l.Value.Replace(',', '.') + "'";
                            else ResultedFilter += "'" + l.Key + "'";
                            ResultedDescription += l.Value;
                            ResultedSelectedItems += l.Value;
                        }
                        ResultedFilter += ")";
                        if (CheckedListValues.Any(a => a.Value == " Пустые"))
                        {
                            ResultedFilter += " OR " + ResultedFilterFields3[0] + " is null";
                            ResultedDescription += ";Пустые";
                            ResultedSelectedItems += "\r\nПустые";
                        }
                        ResultedFilter += ")";
                    }
                    else
                    {
                        bool IsFirst4 = true;
                        foreach (ListData l in CheckedListValues)
                        {
                            if (!IsFirst4)
                            {
                                ResultedDescription += "; ";
                                ResultedSelectedItems += "\r\n";
                                
                            }
                            IsFirst4 = false;
                            ResultedDescription += l.Value;
                            ResultedSelectedItems += l.Value;
                        }

                        bool IsFirst6 = true;
                        ResultedFilter = "(";
                        foreach (string s in ResultedFilterFields3)
                        {
                            if (!IsFirst6) ResultedFilter += " OR ";

                            ResultedFilter += "(" + s + " in (";
                            bool IsFirst5 = true;
                            foreach (ListData l in CheckedListValues)
                            {
                                if (!IsFirst5)
                                {
                                    ResultedFilter += ",";
                                }
                                IsFirst5 = false;
                                if (spd.IsNumber) ResultedFilter += "'" + l.Value.Replace(',', '.') + "'";
                                else ResultedFilter += "'" + l.Key + "'";
                            }
                            ResultedFilter += "))";

                            IsFirst6 = false;
                        }
                        ResultedFilter += ")";
                    }
                    break;
            }
            FilterAndDescription fad = new FilterAndDescription();
            fad.Description = ResultedDescription;
            fad.SQLFilter = ResultedFilter;
            fad.SelectedItems = ResultedSelectedItems;
            fad.ValueFilter = string.Empty;
            fad.ParameterSQLFilter = fad.SQLFilter;
            return fad;

        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            ParentColumn.SetPreviousFilter();
            FilterChanged(false);
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.IsChecked.Value)
            {
                var i = (from a in ((List<ListData>)ColumnContent.ItemsSource) where ((ListData)a).Key.ToString() == ((CheckBox)sender).Tag.ToString() select a).Single();
                i.IsChecked = true;
                SelectedItemsCount++;
                ParentColumn.State = ColumnStates.ValueSelected;
            }
            else
            {
                var i = (from a in ((List<ListData>)ColumnContent.ItemsSource) where ((ListData)a).Key.ToString() == ((CheckBox)sender).Tag.ToString() select a).Single();
                i.IsChecked = false;
                SelectedItemsCount--;
                if (SelectedItemsCount == 0 && ParentColumn.State!=ColumnStates.ParametersSelected) ParentColumn.State = ColumnStates.ParametersSelected;
            }
            WarningPanel.Visibility = (SelectedItemsCount > 100) ? Visibility.Visible : Visibility.Collapsed;
            ColumnContent.Focus();
        }

        private void ColumnContent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FilterPanel.InvalidateArrange();
            NumericFilterPanel.InvalidateArrange();
            ParentColumn.SelectedValues.InvalidateArrange();
        }

        private void FilterButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            ColumnContent.Focus();
            SetChecked(true, true);
        }

        private void FilterButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            ColumnContent.Focus();
            SetChecked(false, true);
        }

        private void SetChecked(bool Checked, bool Range)
        {
            if (ColumnContent.ItemsSource == null) return;
            if (((List<ListData>)ColumnContent.ItemsSource).Count == 0) return;
            IQueryable ToCheck;
            if (Range)
            {
                float Min = float.NegativeInfinity;
                float Max = float.PositiveInfinity;
                if (!float.TryParse(FilterMin.Text, out Min)) Min = float.NegativeInfinity;
                if (!float.TryParse(FilterMax.Text, out Max)) Max = float.PositiveInfinity;
                ToCheck = (from a in ((List<ListData>)ColumnContent.ItemsSource) where float.Parse(((ListData)a).Key.ToString()) >= Min && float.Parse(((ListData)a).Key.ToString()) <= Max select a).AsQueryable();
            }
            else
            {
                ToCheck = (from a in ((List<ListData>)ColumnContent.ItemsSource)  select a).AsQueryable();
            }
            
            ColumnContent.ItemsSource = null;
            foreach (var item in ToCheck)
            {
                ((ListData)item).IsChecked = Checked;
            }
            ColumnContent.ItemsSource = ColumnData;
            ColumnContent.Focus();
            ColumnContent.UpdateLayout();
            SelectedItemsCount = (from a in ((List<ListData>)ColumnContent.ItemsSource) where ((ListData)a).IsChecked select a).Count();
            if (SelectedItemsCount == 0) ParentColumn.State = ColumnStates.ParametersSelected;
            else ParentColumn.State = ColumnStates.ValueSelected;
            WarningPanel.Visibility = (SelectedItemsCount > 100) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void FilterButtonAll_Click(object sender, RoutedEventArgs e)
        {
            ColumnContent.Focus();
            SetChecked(true, false);
        }

        private void FilterNoOne_Click(object sender, RoutedEventArgs e)
        {
            ColumnContent.Focus();
            SetChecked(false, false);
        }
    }

    public class ListData
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsChecked { get; set; }

        public ListData(string key, string value, bool isChecked)
        {
            Key = key;
            Value = value;
            IsChecked = isChecked;
        }

        public ListData()
        {
        }
    }
}