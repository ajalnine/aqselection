﻿using System;
using System.Collections.Generic;
namespace AQConstructorsLibrary
{
    public abstract class SelectionParametersList
    {
        public List<SelectionParameterDescription> ParametersList;
        public string DateField;
        public string DetailsJoin;

        public SelectionParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            DateField = string.Empty;
            DetailsJoin = string.Empty;
        }

        public virtual string GetSelectionSQL() { return String.Empty; }
    }
}
