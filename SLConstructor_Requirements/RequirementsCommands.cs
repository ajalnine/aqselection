﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using ApplicationCore;
using AQControlsLibrary;
using AQStepFactoryLibrary;

namespace SLConstructor_Requirements
{
    public partial class MainPage
    {

        #region Просмотр
        private void Preview(Task task)
        {
            task.AdvanceProgress();
            _sql = ConstructSQL(_filters.ChemistryFilter, false);
            _cas.BeginFillDataWithSQLNamed(_sql, ColumnsPresenter.From, ColumnsPresenter.To, "Требования", DataReadDone, task);
        }

        private void DataReadDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndFillDataWithSQLNamed(iar);
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                XMLPresenter.BeginLoadData(t.TaskDataGuid, StepFactory.CreateSQLQuery(_sql, "Требования"), "Требования: " + _filters.Description, ColumnsPresenter.From, ColumnsPresenter.To, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
        }

        private void DataTablePresenter_Loaded(object o, TaskStateEventArgs e)
        {
            ((Task)e.State).AdvanceProgress();
        }

        private void DataTablePresenter_Processed(object o, TaskStateEventArgs e)
        {
            ((Task)e.State).SetState(2, TaskState.Ready);
        }

        #endregion

        #region Экспорт XLSX

        private void XlsxExportButton_Click(object sender, RoutedEventArgs e)
        {
            XlsxExportButton.IsEnabled = false;
            var task = new Task(new[] { "Выполнение запроса", "Формирование\r\nфайла Xlsx" }, "Экспорт Xlsx", TaskPanel);
            task.SetState(0, TaskState.Processing);
            _sql = ConstructSQL(_filters.ChemistryFilter, false);
            task.CustomData = sender;
            _steps = StepFactory.CreateSQLQuery(_sql, "Требования");
            _cas.BeginExecuteSteps(ColumnsPresenter.From, ColumnsPresenter.To, null, _steps, DataReadForXlsxDone, task);
        }

        private void DataReadForXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndExecuteSteps(iar);
            if (t.Success)
            {
                task.SetState(0, TaskState.Ready);
                task.SetState(1, TaskState.Processing);
                task.SetMessage(t.Message);
                _cas.BeginGetAllResultsInXlsx(t.TaskDataGuid, CreateXlsxDone, task);
            }
            else
            {
                _cas.BeginDropData(t.TaskDataGuid, null, null);
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
                Dispatcher.BeginInvoke(() => XlsxExportButton.IsEnabled = true);
            }
        }

        private void CreateXlsxDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cas.EndGetAllResultsInXlsx(iar);
            task.SetState(1, TaskState.Ready);
            task.SetMessage(t.Message);

            Dispatcher.BeginInvoke(() =>
            {
                HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                    string.Format(@"GetFile.aspx?FileName={0}&FName={1}&ContentType={2}"
                    , HttpUtility.UrlEncode(t.TaskDataGuid)
                    , HttpUtility.UrlEncode(string.Format("Отчет {0}.xlsx", DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null)))
                    , HttpUtility.UrlEncode("application/msexcel"))));
                XlsxExportButton.IsEnabled = true;
            });
        }
        #endregion

        #region Сохранение запроса
        private void SaveQueryButton_Click(object sender, RoutedEventArgs e)
        {
            _sql = ConstructSQL(_filters.ChemistryFilter, false);
            _steps = StepFactory.CreateSQLQuery(_sql, "Требования");
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", "Технология"},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertAndSaveCalculation, signalArguments, null);
        }
        #endregion

        #region Начало расчета
        private void BeginCalculationButton_Click(object sender, RoutedEventArgs e)
        {
            _sql = ConstructSQL(_filters.ChemistryFilter, false);
            _steps = StepFactory.CreateSQLQuery(_sql, "Требования");
            var signalArguments = new Dictionary<string, object>
            {
                {"Name", "Технология"},
                {"From", ColumnsPresenter.From},
                {"To", ColumnsPresenter.To},
                {"Calculation", _steps}
            };
            Signal.Send(GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }
        #endregion
    }
}
