﻿using ApplicationCore.ConstructorServiceReference;

namespace SLConstructor_Requirements
{
    public static class RequirementsSelects
    {
        public static string GetCheckSelect(string sqlFilter)
        {
            string s = @"SELECT id FROM Requirements where Requirements.IsDeleted=0 and (" + sqlFilter + ")";
            return s;
        }

        public static string GetSelectForLines(string sqlFilter)
        {
            string s = @"select 
                Common_Mark.Mark as 'Марка',
                Common_NTD.Ntd as 'НТД',
                Common_ParameterTypes.ParamType as 'Группа параметров',
                Common_Parameters.DisplayParameterName as 'Параметр',
                Min,
                Max,
                Formula as 'Формула',
                Comments as 'Комментарии',
                case IsExtended when 1 then 'Расширенные' else 'Нормальные' end as 'Пределы'
                FROM Requirements INNER JOIN
                Common_Mark ON Requirements.MarkID = Common_Mark.id left outer JOIN
                Common_NTD ON Requirements.NtdID = Common_NTD.id INNER JOIN
                Common_ParameterTypes ON Requirements.ParamTypeID = Common_ParameterTypes.id INNER JOIN
                Common_Parameters ON Common_ParameterTypes.id = Common_Parameters.id_paramtype AND Requirements.ParamID = Common_Parameters.id 
                where " + sqlFilter + " order by Common_Mark.Mark, Common_NTD.Ntd, Common_ParameterTypes.Position, Common_Parameters.Position";
                
            return s;
        }

        public static string GetSelectForColumns(string sqlFilter, ParametersList aqpl)
        {
            string s = @"select 
                Common_Mark.Mark as 'Марка',
                Common_NTD.Ntd as 'НТД',
                case IsExtended when 1 then 'Расширенные' else 'Нормальные' end as 'Пределы'";

                foreach (ParamDescription k in aqpl.AllParameters)
                {
                    s += string.Format(",max(case when Common_Parameters.id='{0}' then dbo.ToNumeric(Min) else null end) as '{1} min'\r\n", k.ParamID, k.Param);
                    s += string.Format(",max(case when Common_Parameters.id='{0}' then dbo.ToNumeric(Max) else null end) as '{1} max'\r\n", k.ParamID, k.Param);
                }

                s += string.Format(@"
                FROM Requirements INNER JOIN
                Common_Mark ON Requirements.MarkID = Common_Mark.id left outer JOIN
                Common_NTD ON Requirements.NtdID = Common_NTD.id INNER JOIN
                Common_ParameterTypes ON Requirements.ParamTypeID = Common_ParameterTypes.id INNER JOIN
                Common_Parameters ON Common_ParameterTypes.id = Common_Parameters.id_paramtype AND Requirements.ParamID = Common_Parameters.id 
                where {0} " + "group by Common_Mark.Mark, Common_NTD.Ntd, IsExtended order by Common_Mark.Mark, Common_NTD.Ntd", sqlFilter);

            return s;
        }
    }
}