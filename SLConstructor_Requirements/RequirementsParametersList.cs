﻿using System.Collections.Generic;
using AQConstructorsLibrary;

namespace SLConstructor_Requirements
{
    public class RequirementsParametersList : SelectionParametersList
    {
        public RequirementsParametersList()
        {
            ParametersList = new List<SelectionParameterDescription>();
            DateField = null;
            var spd1 = new SelectionParameterDescription
            {
                ID = 1,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Марка",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, mark as name from Common_Mark AS M 
                                INNER JOIN 
                                (
	                                select distinct MarkID from Requirements @PreviousFilter group by MarkID
                                ) as t1 on t1.MarkID=M.id 
                                where @AdditionalFilter 
                                order by mark
                                ",
                 Field = "mark",
                 ResultedFilterField  = "Requirements.MarkID",
                 LastOnly = false,
                 IsNumber = false, 
                 NumericSelectionAvailable = false,
                 IncludeInDescription = true,
                 IgnoreFilterForChemistry = false
            };

            var spd2 = new SelectionParameterDescription
            {
                ID = 2,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "НТД",
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, ntd as name from Common_NTD AS G 
                                INNER JOIN 
                                (
	                                 select distinct NtdID from Requirements @PreviousFilter group by NtdID
                                ) as t1 on t1.NtdID = G.id 
                                where @AdditionalFilter
                                order by ntd
                                ",
                Field = "ntd",
                ResultedFilterField = "Requirements.NtdID",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd3 = new SelectionParameterDescription
            {
                ID = 3,
                Selector = ColumnTypes.Discrete,
                Name = "Группа параметров",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd select id, paramtype as name from tbl_paramtype AS p 
                                INNER JOIN 
                                (
	                                select distinct ParamtypeID from Requirements  @PreviousFilter group by ParamtypeID having ParamtypeID is not null 
                                ) as t1 on t1.ParamtypeID=p.id 
                                where @AdditionalFilter order by paramtype",
                Field = "paramtype",
                ResultedFilterField = "Requirements.ParamtypeID",
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd4 = new SelectionParameterDescription
            {
                ID = 4,
                Selector = ColumnTypes.Parameters,
                Name = "Параметры",
                EnableDetailsJoin = false,
                SearchAvailable = true,
                SQLTemplate = @"set dateformat ymd 
                                create table #tquery
                                (number int IDENTITY(1,1), pt_id int, name nvarchar(max), paramtype nvarchar(max), IsNumber bit, Parameter_ID int)

                                insert into #tquery 
                                select distinct Requirements.paramtypeID as pt_id, tbl_Parameters.DisplayParameterName,
                                tbl_Paramtype.Paramtype as Name, 0, tbl_parameters.id from Requirements 
                                inner join tbl_Paramtype on tbl_Paramtype.id = Requirements.paramtypeID
                                inner join tbl_Parameters on tbl_Parameters.id = Requirements.ParamID  @PreviousFilter and @AdditionalFilter order by tbl_Paramtype.Paramtype, tbl_Parameters.DisplayParameterName

                                select t1.number as id, t1.name as name, t1.pt_id, t1.paramtype, t1.IsNumber, cast (case when t2.paramtype=t1.paramtype then 0 else 1 end as bit) as GroupChanged, cast (case when t2.name=t1.name and t2.paramtype=t1.paramtype then 0 else 1 end as bit) as ParameterChanged, t1.Parameter_ID from #tquery as t1 
                                left outer join  #tquery  as t2 on t2.number=t1.number-1 

                                drop table #tquery
                                ",
                Field = "DisplayParameterName",
                ResultedFilterField = "Requirements.ParamID",
                OptionalField = null,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            var spd5 = new SelectionParameterDescription
            {
                ID = 5,
                Selector = ColumnTypes.Discrete,
                EnableDetailsJoin = false,
                Name = "Пределы",
                SearchAvailable = true,
                SQLTemplate = @" select distinct IsExtended as id, case IsExtended when 1 then 'Расширенные' else 'Нормальные' end as 'Name'
                 from Requirements  @PreviousFilter group by IsExtended",
                Field = "IsExtended",
                ResultedFilterField = "Requirements.IsExtended",
                LastOnly = false,
                IsNumber = false,
                NumericSelectionAvailable = false,
                IncludeInDescription = true,
                IgnoreFilterForChemistry = false
            };

            ParametersList.Add(spd1);
            ParametersList.Add(spd2);
            ParametersList.Add(spd3);
            ParametersList.Add(spd4);
            ParametersList.Add(spd5);
        }
    }
}
