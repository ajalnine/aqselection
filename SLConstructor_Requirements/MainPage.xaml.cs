﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ConstructorServiceReference;
using AQConstructorsLibrary;
using AQControlsLibrary;

namespace SLConstructor_Requirements
{
    public partial class MainPage : IAQModule
    {
        #region Переменные

        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;

        private readonly ConstructorService _cs;
        private readonly CalculationService _cas;

        private ParametersList _parametersList;
        private Filters _filters;
        private string _sql = string.Empty;
        private int _selectionType;
        private readonly RequirementsParametersList _requirementsParameters = new RequirementsParametersList();
        private List<Step> _steps;
        private readonly AQModuleDescription _aqmd;

        #endregion

        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name };
            ColumnsPresenter.SelectionParameters = _requirementsParameters;
            _cs = Services.GetConstructorService();
            _cas = Services.GetCalculationService();
        }

        #region Обновление запроса
        private void ColumnsPresenter_ConstructionFinished(object o, ConstructionFinishedEventArgs e)
        {
            RefreshQueryButton.IsEnabled = true;
            RefreshQuery();
        }

        private void RefreshQueryButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshQuery();
        }

        private void RefreshQuery()
        {
            RefreshFilters();
            ColumnsPresenter.EnableFinishSelection(false);
            EnableSaveAndExport(false);
            RefreshQueryButton.IsEnabled = false;

            switch (_selectionType)
            {
                case 0:
                    var task = new Task(new[] { "Выполнение запроса", "Прием данных", "Подготовка к отображению"}, "Запрос", TaskPanel);
                    task.StartTask();
                    _sql = ConstructSQL(_filters.ChemistryFilter, true);
                    _cs.BeginGetRecordCount(_sql, GetRecordCountDone, task);
                    break;
                case 1:
                    var task2 = new Task(new[] { "Формирование списка полей", "Выполнение запроса", "Прием данных", "Подготовка к отображению" }, "Запрос", TaskPanel);
                    task2.StartTask();
                    _cs.BeginGetRequirementsParametersList(_filters.ChemistryFilter, GetRequirementsParametersListDone, task2);
                    break;
            }
        }

        private void GetRequirementsParametersListDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;

            var t = _cs.EndGetRequirementsParametersList(iar);
            _parametersList = t.ParametersList;
            if (t.Success)
            {
                task.AdvanceProgress();
                task.SetMessage(t.Message);
                _sql = ConstructSQL(_filters.ChemistryFilter, true);
                _cs.BeginGetRecordCount(_sql, GetRecordCountDone, task);
            }
            else
            {
                task.SetState(0, TaskState.Error);
                task.SetMessage(t.Message);
            }
            Dispatcher.BeginInvoke(() => ColumnsPresenter.EnableFinishSelection(true));
        }


        private void GetRecordCountDone(IAsyncResult iar)
        {
            var task = (Task)iar.AsyncState;
            var taskNumber = _selectionType == 0 ? 0 : 1;
            var t = _cs.EndGetRecordCount(iar);
            if (t > 0)
            {
                task.SetState(taskNumber, TaskState.Ready);
                task.SetMessage(t + " записей");
                Dispatcher.BeginInvoke(() =>
                {
                    EnableSaveAndExport(true);
                    ColumnsPresenter.EnableFinishSelection(true);
                    Preview(task);
                });
                return;
            }
            task.SetState(taskNumber, TaskState.Error);
            task.SetMessage("Нет данных");
            ColumnsPresenter.EnableFinishSelection(true);
        }

        private void RefreshFilters()
        {
            if (ColumnsPresenter == null) return;
            _filters = ColumnsPresenter.GetFilters();
        }

        #endregion

        #region Установка состояний интерфейса
        private void EnableSaveAndExport(bool allow)
        {
            Dispatcher.BeginInvoke(() =>
            {
                XlsxExportButton.IsEnabled = allow;
                SaveQueryButton.IsEnabled = allow;
                RefreshQueryButton.IsEnabled = allow;
                BeginCalculationButton.IsEnabled = allow;
            });
        }

        #endregion

        #region Построение запроса

        private string ConstructSQL(string filter, bool checkOnly)
        {
            if (checkOnly) return RequirementsSelects.GetCheckSelect(filter);

            switch (SelectionType.SelectedIndex)
            {
                case 0:
                    return RequirementsSelects.GetSelectForLines(filter);

                case 1:
                    return RequirementsSelects.GetSelectForColumns(filter, _parametersList);

                default:
                    return RequirementsSelects.GetSelectForLines(filter);
            }
        }

        #endregion

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
        }

        private void ColumnsPresenter_ConstructionNotCompleted(object o, ConstructionNotCompletedEventArgs e)
        {
            XMLPresenter.Hide();
        }

        private void SelectionType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectionType == null) return;
            _selectionType = SelectionType.SelectedIndex;
            if (XMLPresenter!=null) XMLPresenter.SetFrozenColumnCount(_selectionType==0 ? 1:3 );
            if (!string.IsNullOrWhiteSpace(_sql))RefreshQuery();
        }
    }
}
