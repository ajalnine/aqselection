﻿using System.Windows;
using System.Windows.Media;
using AQBasicControlsLibrary;

namespace AQControlsLibrary
{
    public partial class TripleColorSelector
    {
        private int _colorNumber = 1;

        public static readonly DependencyProperty Color1Property =
            DependencyProperty.Register("Color1", typeof(Color), typeof(TripleColorSelector), new PropertyMetadata(Color.FromArgb(0xff, 0xdf, 0x21, 0x10), null));

        public Color Color1
        {
            get { return (Color)GetValue(Color1Property); }
            set { SetValue(Color1Property, value); }
        }

        public static readonly DependencyProperty Color2Property =
            DependencyProperty.Register("Color2", typeof(Color), typeof(TripleColorSelector), new PropertyMetadata(Color.FromArgb(0xff, 0xfe, 0xff, 0x10), null));

        public Color Color2
        {
            get { return (Color)GetValue(Color2Property); }
            set { SetValue(Color2Property, value); }
        }

        public static readonly DependencyProperty Color3Property =
        DependencyProperty.Register("Color3", typeof(Color), typeof(TripleColorSelector), new PropertyMetadata(Color.FromArgb(0xff, 0x30, 0xc0, 0x10), null));

        public Color Color3
        {
            get { return (Color)GetValue(Color3Property); }
            set { SetValue(Color3Property, value); }
        }


        public TripleColorSelector()
        {
            InitializeComponent();
        }

        private void ColorSelector_OnColorChanged(object sender, ColorChangedEventArgs e)
        {
            switch (_colorNumber)
            {
                case 1:
                    Color1 = e.Color;
                    break;
                case 2:
                    Color2 = e.Color;
                    break;
                case 3:
                    Color3 = e.Color;
                    break;
            }
            ColorPopup.Visibility = Visibility.Collapsed;
        }

        private void Color1Button_OnClick(object sender, RoutedEventArgs e)
        {
            ColorPopup.HorizontalOffset = -80;
            _colorNumber = 1;
            ColorPopup.Visibility = Visibility.Visible;
        }

        private void Color2Button_OnClick(object sender, RoutedEventArgs e)
        {
            ColorPopup.HorizontalOffset = 20;
            _colorNumber = 2;
            ColorPopup.Visibility = Visibility.Visible;
        }

        private void Color3Button_OnClick(object sender, RoutedEventArgs e)
        {
            ColorPopup.HorizontalOffset = 120;
            _colorNumber = 3;
            ColorPopup.Visibility = Visibility.Visible;
        }

        private void TripleColorSelector_OnLostFocus(object sender, RoutedEventArgs e)
        {
            //ColorPopup.Visibility = Visibility.Collapsed;
        }
    }
}
