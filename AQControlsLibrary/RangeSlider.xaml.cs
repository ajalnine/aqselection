﻿using System;
using System.Windows;
using System.Windows.Media;

namespace AQControlsLibrary
{
    public partial class RangeSlider
    {
        public delegate void RangeChangedDelegate(object o, RangeChangedEventArgs e);

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, MinValueChangedCallBack));

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, MaxValueChangedCallBack));

        public static readonly DependencyProperty UpValueProperty =
            DependencyProperty.Register("UpValue", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, UpValueChangedCallBack));

        public static readonly DependencyProperty DownValueProperty =
            DependencyProperty.Register("DownValue", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, DownValueChangedCallBack));

        public static readonly DependencyProperty StepProperty =
            DependencyProperty.Register("Step", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, StepChangedCallBack));

        public static readonly DependencyProperty GreenZoneStartProperty =
            DependencyProperty.Register("GreenZoneStart", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, GreenZoneStartChangedCallBack));

        public static readonly DependencyProperty GreenZoneEndProperty =
            DependencyProperty.Register("GreenZoneEnd", typeof (double), typeof (RangeSlider),
                                        new PropertyMetadata(0d, GreenZoneEndChangedCallBack));

        private bool _isInitialized;

        public RangeSlider()
        {
            InitializeComponent();
        }

        public double MinValue
        {
            get { return (double) GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public double MaxValue
        {
            get { return (double) GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public double UpValue
        {
            get { return (double) GetValue(UpValueProperty); }
            set { SetValue(UpValueProperty, value); }
        }

        public double DownValue
        {
            get { return (double) GetValue(DownValueProperty); }
            set { SetValue(DownValueProperty, value); }
        }

        public double Step
        {
            get { return (double) GetValue(StepProperty); }
            set { SetValue(StepProperty, value); }
        }

        public double GreenZoneStart
        {
            get { return (double) GetValue(GreenZoneStartProperty); }
            set { SetValue(GreenZoneStartProperty, value); }
        }

        public double GreenZoneEnd
        {
            get { return (double) GetValue(GreenZoneEndProperty); }
            set { SetValue(GreenZoneEndProperty, value); }
        }

        public event RangeChangedDelegate RangeChanged;

        private static void MinValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.UpValueSlider.Minimum = (double) e.NewValue;
            r.DownValueSlider.Minimum = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void MaxValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.UpValueSlider.Maximum = (double) e.NewValue;
            r.DownValueSlider.Maximum = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void UpValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.UpValueSlider.Value = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void DownValueChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.DownValueSlider.Value = (double) e.NewValue;
            r.RefreshControl();
        }

        private static void GreenZoneStartChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.RefreshControl();
        }

        private static void GreenZoneEndChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.RefreshControl();
        }

        private static void StepChangedCallBack(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var r = (RangeSlider) o;
            r.RefreshControl();
            r.UpValueSlider.SmallChange = (double) e.NewValue;
            r.DownValueSlider.SmallChange = (double) e.NewValue;
            r.UpValueSlider.LargeChange = 5*(double) e.NewValue;
            r.DownValueSlider.LargeChange = 5*(double) e.NewValue;
        }

        private void UpValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (UpValueSlider.Value < DownValueSlider.Value) DownValueSlider.Value = UpValueSlider.Value;
            UpValue = UpValueSlider.Value;
            RefreshControl();
            if (RangeChanged != null)
                RangeChanged(this,
                             new RangeChangedEventArgs
                                 {
                                     UpValue = UpValueSlider.Value,
                                     DownValue = DownValueSlider.Value
                                 });
        }

        private void DownValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (DownValueSlider.Value > UpValueSlider.Value) UpValueSlider.Value = DownValueSlider.Value;
            DownValue = DownValueSlider.Value;
            RefreshControl();
            if (RangeChanged != null)
                RangeChanged(this,
                             new RangeChangedEventArgs
                                 {
                                     UpValue = UpValueSlider.Value,
                                     DownValue = DownValueSlider.Value
                                 });
        }

        private void RefreshGreenZone()
        {
            RangeView.UpdateLayout();
            var actualWidth = RedZoneRectangle.ActualWidth;
            if (MaxValue - MinValue > 0)
            {
                var zoneNormedSize = actualWidth/(MaxValue - MinValue);
                var gs = (GreenZoneStart < MinValue)
                                ? MinValue
                                : (GreenZoneStart > MaxValue ? MaxValue : GreenZoneStart);
                var ge = (GreenZoneEnd > MaxValue) ? MaxValue : (GreenZoneEnd < MinValue ? MinValue : GreenZoneEnd);
                var offset = gs*zoneNormedSize;
                var width = (ge - gs)*zoneNormedSize;
                GreenZoneOffset.Width = offset;
                GreenZoneRectangle.Width = width;
            }
            else
            {
                GreenZoneOffset.Width = 0;
                GreenZoneRectangle.Width = (MinValue < GreenZoneStart || MaxValue > GreenZoneEnd) ? actualWidth : 0;
            }
        }

        private void RefreshGrayZone()
        {
            GrayZoneRectangle.Visibility = Math.Abs(MinValue - MaxValue) < 1e-8 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void RefreshSelectedZone()
        {
            RangeView.UpdateLayout();
            var actualWidth = RedZoneRectangle.ActualWidth;
            if (MaxValue - MinValue > 0)
            {
                var zoneNormedSize = actualWidth/(MaxValue - MinValue);
                var offset = Math.Min(DownValue, UpValue)*zoneNormedSize;
                var width = Math.Abs(UpValue - DownValue)*zoneNormedSize;
                SelectedZoneOffset.Width = offset;
                SelectedZoneRectangle.Width = width;
            }
            else
            {
                SelectedZoneOffset.Width = 0;
                SelectedZoneRectangle.Width = 0;
            }
        }

        private void RefreshSnap()
        {
            var actualWidth = RedZoneRectangle.ActualWidth;
            if (Math.Abs(MaxValue - MinValue) < 1e-8)
            {
                Ticks.Opacity = 0;
                SmallTicks.Visibility = Visibility.Collapsed;
                return;
            }
            var zoneNormedSize = Step*5*actualWidth/(MaxValue - MinValue);
            var dc = new DoubleCollection {1, zoneNormedSize - 1};
            if (zoneNormedSize > 12)
            {
                var dc2 = new DoubleCollection {1, zoneNormedSize/5 - 1};
                SmallTicks.StrokeDashArray = dc2;
                SmallTicks.StrokeDashOffset = zoneNormedSize + 1;
                SmallTicks.Visibility = Visibility.Visible;
            }
            else
            {
                SmallTicks.Visibility = Visibility.Collapsed;
            }

            Ticks.StrokeDashArray = dc;
            Ticks.StrokeDashOffset = zoneNormedSize + 1;
            Ticks.Opacity = 1;
        }

        private void RangeSlider_Loaded(object sender, RoutedEventArgs e)
        {
            _isInitialized = true;
            RefreshControl();
        }

        private void RefreshControl()
        {
            if (!_isInitialized) return;

            UpValueSlider.Minimum = MinValue;
            UpValueSlider.Maximum = MaxValue;
            DownValueSlider.Minimum = MinValue;
            DownValueSlider.Maximum = MaxValue;


            UpValueSlider.Value = UpValue;
            DownValueSlider.Value = DownValue;
            UpValueSlider.SmallChange = Step;
            DownValueSlider.SmallChange = Step;
            UpValueSlider.LargeChange = Step;
            DownValueSlider.LargeChange = Step;

            RefreshGreenZone();
            RefreshGrayZone();
            RefreshSelectedZone();
            RefreshSnap();
        }
    }
    public class RangeChangedEventArgs : EventArgs
    {
        public double UpValue { get; set; }
        public double DownValue { get; set; }
    }
}