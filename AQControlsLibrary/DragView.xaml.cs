﻿using System.Windows;
using System.Windows.Controls;

namespace AQControlsLibrary
{
    public partial class DragView : UserControl
    {
        public DragView()
        {
            InitializeComponent();
        }

        public void SetDragItem(UIElement whatToDrag)
        {
            DragItem.Child = whatToDrag;
            DragItem.Visibility = Visibility.Visible;
            DragSurface.Visibility = Visibility.Visible;
        }

        public void RemoveDragItem()
        {
            DragItem.Child = null;
            DragItem.Visibility = Visibility.Collapsed;
            DragSurface.Visibility = Visibility.Collapsed;
            SetDropPossibilityIcon(null);
        }

        public void SetDragItemPosition(Point pos)
        {
            Canvas.SetLeft(DragLabel, pos.X);
            Canvas.SetTop(DragLabel, pos.Y);
        }

        public void SetDropPossibilityIcon(bool? possibility)
        {
            if (!possibility.HasValue)
            {
                StopIcon.Visibility = Visibility.Collapsed;
                AddIcon.Visibility = Visibility.Collapsed;
            }
            else
            {
                StopIcon.Visibility = (possibility.Value) ? Visibility.Collapsed : Visibility.Visible;
                AddIcon.Visibility = (possibility.Value) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
    }
}