﻿using System;
using System.Collections.Generic;

namespace AQControlsLibrary
{
    public class StateDispatcher<T>
    {
        private T _currentState;

        private readonly Dictionary<T, Action> _stateProcessorsStore;

        public StateDispatcher()
        {
            _stateProcessorsStore = new Dictionary<T, Action>();
        }

        public void SetStateProcessor(T state, Action stateProcessor)
        {
            if (_stateProcessorsStore.ContainsKey(state)) _stateProcessorsStore[state] = stateProcessor;
            else _stateProcessorsStore.Add(state,stateProcessor);
        }

        public void RemoveStateProcessor(T state)
        {
            if (_stateProcessorsStore.ContainsKey(state)) _stateProcessorsStore.Remove(state);
        }

        public void GoState(T state)
        {
            if (EqualityComparer<T>.Default.Equals(_currentState, state))return;
            _currentState = state;
            if (_stateProcessorsStore.ContainsKey(state)) _stateProcessorsStore[state].Invoke();
        }

        public T GetCurrentState()
        {
            return _currentState;
        }
    }
}
