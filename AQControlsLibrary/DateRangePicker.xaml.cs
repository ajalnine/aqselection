﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace AQControlsLibrary
{
    public partial class DateRangePicker
    {
        public delegate void DateChangedDelegate(object sender, DateChangedEventArgs e);

        public static readonly DependencyProperty FromProperty =
            DependencyProperty.Register("fromDateTime", typeof (DateTime?), typeof (DateRangePicker),
                                        new PropertyMetadata(null, DateFromPropertyChange));

        public static readonly DependencyProperty ToProperty =
            DependencyProperty.Register("toDateTime", typeof (DateTime?), typeof (DateRangePicker),
                                        new PropertyMetadata(null, DateToPropertyChange));

        private bool _fromTyping;
        private bool _toTyping;

        public DateRangePicker()
        {
            InitializeComponent();
            FromDatePicker.SelectedDate = DateTime.Now.AddDays(-7);
            ToDatePicker.SelectedDate = DateTime.Now;
            FromDatePicker.SelectedDateChanged += FromDateChange;
            ToDatePicker.SelectedDateChanged += ToDateChange;
        }

        public DateTime? From
        {
            get { return (DateTime?) GetValue(FromProperty); }
            set { SetValue(FromProperty, value); }
        }

        public DateTime? To
        {
            get { return (DateTime?) GetValue(ToProperty); }
            set { SetValue(ToProperty, value); }
        }

        public event DateChangedDelegate DateRangeChanged;

        public void SetDate(DateTime? fromDateTime, DateTime? toDateTime)
        {
            if (fromDateTime.HasValue) FromDatePicker.SelectedDate = fromDateTime.Value;
            if (toDateTime.HasValue) ToDatePicker.SelectedDate = toDateTime.Value;
            UpdateLayout();
        }

        public void ResetView()
        {
            Panel p = GetCustomPanel();
            p.Children.Clear();
            p.Visibility = Visibility.Collapsed;
            ShowDateSelector(true);
        }

        private static void DateFromPropertyChange(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sender = (DateRangePicker) o;
            if (sender._fromTyping) return;
            sender.FromDatePicker.SelectedDate = sender.From;
        }

        private static void DateToPropertyChange(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sender = (DateRangePicker) o;
            if (sender._toTyping) return;
            sender.ToDatePicker.SelectedDate = sender.To;
        }

        private void FromDateChange(object sender, SelectionChangedEventArgs e)
        {
            From = FromDatePicker.SelectedDate;
            if (DateRangeChanged != null) DateRangeChanged(this, new DateChangedEventArgs(From));
        }

        private void ToDateChange(object sender, SelectionChangedEventArgs e)
        {
            To = ToDatePicker.SelectedDate;
            if (DateRangeChanged != null) DateRangeChanged(this, new DateChangedEventArgs(To));
        }

        public Grid GetCustomPanel()
        {
            return CustomPanel;
        }

        public void ShowDateSelector(bool show)
        {
            MainPanel.Visibility = show ? Visibility.Visible : Visibility.Collapsed;
        }

        private void FromDatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            DependencyObject a = VisualTreeHelper.GetChild(FromDatePicker, 0);
            DependencyObject b = VisualTreeHelper.GetChild(a, 0);
            var ft = b as DatePickerTextBox;
            if (ft == null) return;
            ft.KeyUp += ft_KeyUp;
            ft.GotFocus += ft_GotFocus;
            ft.LostFocus += ft_LostFocus;
        }

        private void ft_GotFocus(object sender, RoutedEventArgs e)
        {
            _fromTyping = true;
        }

        private void ft_LostFocus(object sender, RoutedEventArgs e)
        {
            _fromTyping = false;
            DateFromPropertyChange(this, new DependencyPropertyChangedEventArgs());
        }

        private void ft_KeyUp(object sender, KeyEventArgs e)
        {
            var d = sender as DatePickerTextBox;
            if (d == null) return;
            d.UpdateLayout();
            var s = d.Text;
            DateTime f;
            if (!DateTime.TryParse(s, out f)) return;
            From = f;
            if (DateRangeChanged != null) DateRangeChanged(this, new DateChangedEventArgs(From));
        }

        private void ToDatePicker_Loaded(object sender, RoutedEventArgs e)
        {
            var a = VisualTreeHelper.GetChild(ToDatePicker, 0);
            var b = VisualTreeHelper.GetChild(a, 0);
            var tt = b as DatePickerTextBox;
            if (tt == null) return;
            tt.KeyUp += tt_KeyUp;
            tt.GotFocus += tt_GotFocus;
            tt.LostFocus += tt_LostFocus;
        }

        private void tt_GotFocus(object sender, RoutedEventArgs e)
        {
            _toTyping = true;
        }

        private void tt_LostFocus(object sender, RoutedEventArgs e)
        {
            _toTyping = false;
            DateToPropertyChange(this, new DependencyPropertyChangedEventArgs());
        }

        private void tt_KeyUp(object sender, KeyEventArgs e)
        {
            var d = sender as DatePickerTextBox;
            if (d == null) return;
            d.UpdateLayout();
            var s = d.Text;
            DateTime T;
            if (!DateTime.TryParse(s, out T)) return;
            To = T;
            if (DateRangeChanged != null) DateRangeChanged(this, new DateChangedEventArgs(To));
        }
    }

    public class DateChangedEventArgs : EventArgs
    {
        public DateTime? Date;

        public DateChangedEventArgs(DateTime? date)
        {
            Date = date;
        }
    }
}