﻿using System;
using System.Linq;
using System.Windows.Controls;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public class Task
    {
        private readonly TaskStatePresenter _currentTaskStatePresenter;
        private readonly StackPanel _taskPanel;
        public int CurrentStep;
        public object CustomData;
        public int NumberOfSteps;
        public object ProcessingItem;
        public object SourceData;
        public bool TaskIsInterruptable = false;

        public Task(string[] steps, string name, StackPanel taskPanel)
        {
            _taskPanel = taskPanel;
            ClearOldTasks();
            _currentTaskStatePresenter = new TaskStatePresenter();
            NumberOfSteps = steps.Length;
            CurrentStep = 0;
            _currentTaskStatePresenter.SetTaskSteps(steps, name);
            _taskPanel.Children.Add(_currentTaskStatePresenter);
        }

        public Task(string[] steps, string name, StackPanel taskPanel, bool interruptable)
            : this(steps, name, taskPanel)
        {
            TaskIsInterruptable = interruptable;
        }

        public void AdvanceProgress()
        {
            _taskPanel.Dispatcher.BeginInvoke(delegate
                {
                    _currentTaskStatePresenter.SetTaskState(CurrentStep, TaskState.Ready);
                    CurrentStep++;
                    _currentTaskStatePresenter.SetTaskState(CurrentStep,
                                                           CurrentStep <= NumberOfSteps
                                                               ? TaskState.Processing
                                                               : TaskState.Ready);
                });
        }

        public void StartTask()
        {
            _taskPanel.Dispatcher.BeginInvoke(
                () => _currentTaskStatePresenter.SetTaskState(0, TaskState.Processing));
        }

        public void StopTask(string message)
        {
            _taskPanel.Dispatcher.BeginInvoke(delegate
                {
                    _currentTaskStatePresenter.SetTaskState(CurrentStep, TaskState.Error);
                    _currentTaskStatePresenter.SetTaskMessage(message);
                });
        }

        public void SetState(int number, TaskState state)
        {
            _taskPanel.Dispatcher.BeginInvoke(() => _currentTaskStatePresenter.SetTaskState(number, state));
        }

        public void ClearOldTasks()
        {
            var toDelete = _taskPanel.Children.Cast<TaskStatePresenter>().Where(tsp => tsp.IsFinished).ToList();
            foreach (TaskStatePresenter tsp in toDelete)
            {
                _taskPanel.Children.Remove(tsp);
            }
        }

        public void RemoveTask()
        {
            _taskPanel.Dispatcher.BeginInvoke(() => _taskPanel.Children.Remove(_currentTaskStatePresenter));
        }

        public void SetMessage(string message)
        {
            _taskPanel.Dispatcher.BeginInvoke(() => _currentTaskStatePresenter.SetTaskMessage(message));
        }
    }

    public enum TaskState
    {
        Inactive,
        Processing,
        Preprocessing,
        Ready,
        Error
    };
}