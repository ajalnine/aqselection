﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using AQBasicControlsLibrary;
using ApplicationCore;
using ApplicationCore.AQServiceReference;
using ApplicationCore.CalculationServiceReference;
using ApplicationCore.ReportingServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public static class CommonTasks
    {
        public static void SendPNGFile(UIElement sender, WriteableBitmap wb, StackPanel taskView, string prefix, Action callback)
        {
            var task = new Task(new[] {"Сжатие изображения", "Отправка PNG"}, "Экспорт в PNG", taskView);
            task.StartTask();

            var task2Guid = Guid.NewGuid();
            var rdsc = Services.GetReportingService();

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task2Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  task.AdvanceProgress();
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           @"GetFile.aspx?FileName=" +
                                                                           HttpUtility.UrlEncode(
                                                                               sr.ResultData.ToString()) + "&FName=" +
                                                                           HttpUtility.UrlEncode(prefix + " " +
                                                                                                 DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) +
                                                                                                 ".png") +
                                                                           "&ContentType=" +
                                                                           HttpUtility.UrlEncode("image/png")));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  task.StopTask(sr.Message);
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });

            var bw = new BackgroundWorker();
            bw.DoWork += (s, ebw) =>
                {
                    var png = PNG.WriteableBitmapToPNG(wb).ToList();
                    ebw.Result = png;
                };
            bw.RunWorkerCompleted += (s, ebw) =>
                {
                    task.AdvanceProgress();

                    rdsc.SendBinaryAsync(task2Guid, (List<byte>) ebw.Result);
                };
            bw.RunWorkerAsync();
        }

        public static void SendReadyPNGFile(UIElement sender, List<byte> png, string prefix, Action callback)
        {
            ReportingDuplexServiceClient rdsc = Services.GetReportingService();

            Guid task1Guid = Guid.NewGuid();

            rdsc.SendBinaryAsync(task1Guid, png);

            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                           @"GetFile.aspx?FileName=" +
                                                                           HttpUtility.UrlEncode(
                                                                               sr.ResultData.ToString()) + "&FName=" +
                                                                           HttpUtility.UrlEncode(prefix +
                                                                                                 DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss", null) +
                                                                                                 ".png") +
                                                                           "&ContentType=" +
                                                                           HttpUtility.UrlEncode("image/png")));
                                          if (callback != null) callback.Invoke();
                                      });
                              }
                              else
                              {
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });
        }

        public static void CreatePPTX(UIElement sender, PresentationDescription pd, string fileName, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();
            var slidesToSend = pd.Slides.Count;
            var packetSize = Math.Min(5, slidesToSend);
            var send = 0;
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                          {
                              OperationResult sr = x.EventArgs.or;
                              if (sr.Success)
                              {
                                  switch (sr.Message)
                                  {
                                      case "FileReady":
                                          sender.Dispatcher.BeginInvoke(() =>
                                          {
                                              HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                                               @"GetFile.aspx?FileName=" +
                                                                               HttpUtility.UrlEncode(
                                                                                   sr.ResultData.ToString()) + "&FName=" +
                                                                               HttpUtility.UrlEncode(fileName) +
                                                                               "&ContentType=" +
                                                                               HttpUtility.UrlEncode(
                                                                                   "application/mspowerpoint")));
                                              if (callback != null) callback.Invoke();
                                          });
                                          break;
                                      case "Sent":
                                          if (callback != null) callback.Invoke();
                                          break;
                                      case "Appended":
                                      case "Created":
                                          send += packetSize;
                                          slidesToSend -= packetSize;
                                          packetSize = Math.Min(5, slidesToSend);
                                          if (slidesToSend == 0) rdsc.GetPPTXAsync(task1Guid, (Guid)sr.ResultData);
                                          else rdsc.AppendPPTXAsync(task1Guid, (Guid)sr.ResultData, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
                                          break;
                                  }
                              }
                              else
                              {
                                  if (sr.ResultData != null) rdsc.DropDataAsync((Guid) sr.ResultData);
                                  if (callback != null) callback.Invoke();
                              }
                          });

            rdsc.CreatePPTXAsync(task1Guid, new PresentationDescription {Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor});
        }

        public static void SendPPTX(UIElement sender, PresentationDescription pd, string fileName, string subject, string recepients, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();
            var slidesToSend = pd.Slides.Count;
            var packetSize = Math.Min(5, slidesToSend);
            var send = 0;
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              switch (sr.Message)
                              {
                                  case "FileReady":
                                      sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          var aq = Services.GetAQService();
                                          aq.BeginSendFile((Guid) sr.ResultData, fileName, "application/mspowerpoint", subject, recepients,
                                              ar =>
                                              {
                                                  aq.EndSendFile(ar);
                                                  callback?.Invoke();
                                              }, null
                                          );
                                      });
                                      break;
                                  case "Sent":
                                      if (callback != null) callback.Invoke();
                                      break;
                                  case "Appended":
                                  case "Created":
                                      send += packetSize;
                                      slidesToSend -= packetSize;
                                      packetSize = Math.Min(5, slidesToSend);
                                      if (slidesToSend == 0) rdsc.GetPPTXAsync(task1Guid, (Guid)sr.ResultData);
                                      else rdsc.AppendPPTXAsync(task1Guid, (Guid)sr.ResultData, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
                                      break;
                              }
                          }
                          else
                          {
                              if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                              if (callback != null) callback.Invoke();
                          }
                      });

            rdsc.CreatePPTXAsync(task1Guid, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
        }


        public static void CreateXLSX(UIElement sender, List<SLDataTable> tables, string fileName, Action callback)
        {
            var cs = Services.GetCalculationService();

            sender.Dispatcher.BeginInvoke(() =>
            {
                cs.BeginFillDataWithSLTables(tables, iar =>
                    {
                        var sr = cs.EndFillDataWithSLTables(iar);
                        cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, iar2 =>
                        {
                            sr = cs.EndGetAllResultsInXlsx(iar2);
                            if (sr.Success)
                            {
                                sender.Dispatcher.BeginInvoke(() =>
                                {
                                    HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                        @"GetFile.aspx?FileName=" + HttpUtility.UrlEncode(sr.TaskDataGuid) + "&FName=" +
                                        HttpUtility.UrlEncode(fileName) + "&ContentType=" +
                                        HttpUtility.UrlEncode("application/msexcel")));
                                    if (callback != null) callback.Invoke();
                                });
                            }
                            else
                            {
                                sender.Dispatcher.BeginInvoke(() =>
                                {
                                    callback?.Invoke();
                                });
                            }
                        }, null);
                    }, null);
            });
        }

        public static void SendXLSX(UIElement sender, List<SLDataTable> tables, string fileName, string subject, string recepients, Action callback)
        {
            var cs = Services.GetCalculationService();

            sender.Dispatcher.BeginInvoke(() =>
            {
                cs.BeginFillDataWithSLTables(tables, iar =>
                {
                    var sr = cs.EndFillDataWithSLTables(iar);
                    cs.BeginGetAllResultsInXlsx(sr.TaskDataGuid, iar2 =>
                    {
                        sr = cs.EndGetAllResultsInXlsx(iar2);
                        if (sr.Success)
                        {
                            sender.Dispatcher.BeginInvoke(() =>
                            {
                                var aq = Services.GetAQService();
                                aq.BeginSendFile(Guid.Parse(sr.TaskDataGuid), fileName, "application/msexcel", subject, recepients,
                                    ar =>
                                    {
                                        aq.EndSendFile(ar);
                                        callback?.Invoke();
                                    }, null
                                );
                            });

                        }
                        else
                        {
                            sender.Dispatcher.BeginInvoke(() =>
                            {
                                if (callback != null) callback.Invoke();
                            });
                        }
                    }, null);
                }, null);
            });
        }

        public static void SendPPTXXLSX(UIElement sender, PresentationDescription pd, List<SLDataTable> tables, string pptxFileName, string xlsxFilename, string subject, string recepients, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();
            var slidesToSend = pd.Slides.Count;
            var packetSize = Math.Min(5, slidesToSend);
            var send = 0;
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              switch (sr.Message)
                              {
                                  case "FileReady":
                                      sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          var aq = Services.GetAQService();
                                          var cs = Services.GetCalculationService();

                                          sender.Dispatcher.BeginInvoke(() =>
                                          {
                                              cs.BeginFillDataWithSLTables(tables, iar =>
                                              {
                                                  var sr2 = cs.EndFillDataWithSLTables(iar);
                                                  cs.BeginGetAllResultsInXlsx(sr2.TaskDataGuid, iar2 =>
                                                  {
                                                      sr2 = cs.EndGetAllResultsInXlsx(iar2);
                                                      if (sr2.Success)
                                                      {
                                                          sender.Dispatcher.BeginInvoke(() =>
                                                          {
                                                              var toSend = new List<FileInfo>
                                                              {
                                                                  new FileInfo{
                                                                      OperationGuid = (Guid)sr.ResultData,
                                                                      FileName = pptxFileName,
                                                                      ContentType = "application/mspowerpoint",
                                                                  },
                                                                  new FileInfo{
                                                                      OperationGuid = Guid.Parse(sr2.TaskDataGuid),
                                                                      FileName = xlsxFilename,
                                                                      ContentType = "application/msexcel",
                                                                  }
                                                              };
                                                              aq.BeginSendFiles(toSend, subject, recepients,
                                                                  ar =>
                                                                  {
                                                                      aq.EndSendFiles(ar);
                                                                      callback?.Invoke();
                                                                  }, null
                                                              );
                                                          });
                                                      }
                                                      else
                                                      {
                                                          sender.Dispatcher.BeginInvoke(() =>
                                                          {
                                                              callback?.Invoke();
                                                          });
                                                      }
                                                  }, null);
                                              }, null);
                                          });

                                      });
                                      break;
                                  case "Sent":
                                      if (callback != null) callback.Invoke();
                                      break;
                                  case "Appended":
                                  case "Created":
                                      send += packetSize;
                                      slidesToSend -= packetSize;
                                      packetSize = Math.Min(5, slidesToSend);
                                      if (slidesToSend == 0) rdsc.GetPPTXAsync(task1Guid, (Guid)sr.ResultData);
                                      else rdsc.AppendPPTXAsync(task1Guid, (Guid)sr.ResultData, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
                                      break;
                              }
                          }
                          else
                          {
                              if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                              if (callback != null) callback.Invoke();
                          }
                      });

            rdsc.CreatePPTXAsync(task1Guid, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
        }

        public static void CreateDOCX(UIElement sender, PresentationDescription pd, string fileName, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();
            var slidesToSend = pd.Slides.Count;
            var packetSize = Math.Min(5, slidesToSend);
            var send = 0;
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              switch (sr.Message)
                              {
                                  case "FileReady":
                                      sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          sender.Dispatcher.BeginInvoke(() =>
                                          {
                                              HtmlPage.Window.Navigate(new Uri(HtmlPage.Document.DocumentUri,
                                                  $@"GetFile.aspx?FileName={
                                                      HttpUtility.UrlEncode(sr.ResultData.ToString())}&FName={
                                                      HttpUtility.UrlEncode(fileName)
                                                      }&ContentType={HttpUtility.UrlEncode("application/msword")}"));
                                              callback?.Invoke();
                                          });
                                      });
                                      break;
                                  case "Sent":
                                      if (callback != null) callback.Invoke();
                                      break;
                                  case "Appended":
                                  case "Created":
                                      send += packetSize;
                                      slidesToSend -= packetSize;
                                      packetSize = Math.Min(5, slidesToSend);
                                      if (slidesToSend == 0) rdsc.GetDOCXAsync(task1Guid, (Guid)sr.ResultData);
                                      else rdsc.AppendDOCXAsync(task1Guid, (Guid)sr.ResultData, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
                                      break;
                              }
                          }
                          else
                          {
                              if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                              if (callback != null) callback.Invoke();
                          }
                      });

            rdsc.CreateDOCXAsync(task1Guid, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
        }

        public static void SendDOCX(UIElement sender, PresentationDescription pd, string fileName, string subject, string recepients, Action callback)
        {
            var task1Guid = Guid.NewGuid();

            var rdsc = Services.GetReportingService();
            var slidesToSend = pd.Slides.Count;
            var packetSize = Math.Min(5, slidesToSend);
            var send = 0;
            Observable.FromEvent<ReceiveReceivedEventArgs>(rdsc, "ReceiveReceived")
                      .Where((o1, e1) => o1.EventArgs.OperationGuid == task1Guid)
                      .Subscribe(x =>
                      {
                          OperationResult sr = x.EventArgs.or;
                          if (sr.Success)
                          {
                              switch (sr.Message)
                              {
                                  case "FileReady":
                                      sender.Dispatcher.BeginInvoke(() =>
                                      {
                                          var aq = Services.GetAQService();
                                          aq.BeginSendFile((Guid)sr.ResultData, fileName, "application/msword", subject, recepients,
                                              ar =>
                                              {
                                                  aq.EndSendFile(ar);
                                                  callback?.Invoke();
                                              }, null
                                          );
                                      });
                                      break;
                                  case "Sent":
                                      if (callback != null) callback.Invoke();
                                      break;
                                  case "Appended":
                                  case "Created":
                                      send += packetSize;
                                      slidesToSend -= packetSize;
                                      packetSize = Math.Min(5, slidesToSend);
                                      if (slidesToSend == 0) rdsc.GetDOCXAsync(task1Guid, (Guid)sr.ResultData);
                                      else rdsc.AppendDOCXAsync(task1Guid, (Guid)sr.ResultData, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
                                      break;
                              }
                          }
                          else
                          {
                              if (sr.ResultData != null) rdsc.DropDataAsync((Guid)sr.ResultData);
                              if (callback != null) callback.Invoke();
                          }
                      });

            rdsc.CreateDOCXAsync(task1Guid, new PresentationDescription { Slides = pd.Slides.Skip(send).Take(packetSize).ToList(), BlackBackground = pd.BlackBackground, ForegroundColor = pd.ForegroundColor });
        }
    }
}