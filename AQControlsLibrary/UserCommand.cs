﻿namespace AQControlsLibrary
{
    public class UserCommand
    {
        public delegate void UserCommandDelegate(UserCommand source, object customArgs);

        private readonly UserCommandDelegate _work;
        private readonly UserCommandDelegate _sucess;
        private readonly UserCommandDelegate _failed;

        public UserCommand(UserCommandDelegate work, UserCommandDelegate success, UserCommandDelegate failed)
        {
            _work = work;
            _sucess = success;
            _failed = failed;
        }

        public void Success()
        {
            _sucess.Invoke(this, null);
        }

        public void Success(object customArgs)
        {
            _sucess.Invoke(this, customArgs);
        }

        public void Failed()
        {
            _failed.Invoke(this, null);
        }

        public void Failed(object customArgs)
        {
            _failed.Invoke(this, customArgs);
        }

        public void Execute()
        {
             _work.Invoke(this, null);
        }

        public void Execute(object customArgs)
        {
            _work.Invoke(this, customArgs);
        }
    }
}
