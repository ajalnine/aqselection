﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using AQBasicControlsLibrary;

namespace AQControlsLibrary
{
    public partial class SingleColorSelector : UserControl
    {
        public static readonly DependencyProperty ColorProperty =
        DependencyProperty.Register("Color", typeof(Color), typeof(SingleColorSelector), new PropertyMetadata(Color.FromArgb(0xff, 0x30, 0xc0, 0x10), null));

        public Color Color
        {
            get { return (Color)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty VerticalDirectionProperty =
        DependencyProperty.Register("VerticalDirection", typeof(VerticalAlignment), typeof(SingleColorSelector), new PropertyMetadata(System.Windows.VerticalAlignment.Top, null));

        public VerticalAlignment VerticalDirection
        {
            get { return (VerticalAlignment)GetValue(VerticalDirectionProperty); }
            set { SetValue(VerticalDirectionProperty, value); }
        }


        public SingleColorSelector()
        {
            InitializeComponent();
        }

        private void ColorSelector_OnColorChanged(object sender, ColorChangedEventArgs e)
        {
            Color = e.Color;
            ColorPopup.Visibility = Visibility.Collapsed;
        }

        private void ColorButton_OnClick(object sender, RoutedEventArgs e)
        {
            ColorPopup.VerticalOffset = (VerticalDirection == VerticalAlignment.Top)  ? - 25 : 50;
            ColorPopup.Visibility = Visibility.Visible;
        }
    }
}
