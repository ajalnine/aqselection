﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace AQControlsLibrary
{
    public partial class Pager 
    {
        public delegate void CurrentPageChangedDelegate(object o, EventArgs e);

        public static readonly DependencyProperty TotalRecordsProperty =
            DependencyProperty.Register("TotalRecords", typeof (int), typeof (Pager),
                                        new PropertyMetadata(0, TotalRecordsPropertyChanged));

        public static readonly DependencyProperty PageSizeProperty =
            DependencyProperty.Register("PageSize", typeof (int), typeof (Pager),
                                        new PropertyMetadata(0, PageSizePropertyChanged));

        public static readonly DependencyProperty CurrentPageProperty =
            DependencyProperty.Register("CurrentPage", typeof (int), typeof (Pager),
                                        new PropertyMetadata(0, CurrentPagePropertyChanged));

        public static readonly DependencyProperty GroupSizeProperty =
            DependencyProperty.Register("GroupSize", typeof (int), typeof (Pager),
                                        new PropertyMetadata(10, GroupSizePropertyChanged));

        private int _firstPageInCurrentGroup;
        private int _lastPageInCurrentGroup;
        private int _numberOfPages;

        public Pager()
        {
            InitializeComponent();
        }

        public int TotalRecords
        {
            get { return (int) GetValue(TotalRecordsProperty); }
            set { SetValue(TotalRecordsProperty, value); }
        }

        public int PageSize
        {
            get { return (int) GetValue(PageSizeProperty); }
            set { SetValue(PageSizeProperty, value); }
        }

        public int CurrentPage
        {
            get { return (int) GetValue(CurrentPageProperty); }
            set { SetValue(CurrentPageProperty, value); }
        }

        public int GroupSize
        {
            get { return (int) GetValue(GroupSizeProperty); }
            set { SetValue(GroupSizeProperty, value); }
        }

        public event CurrentPageChangedDelegate CurrentPageChanged;

        private static void TotalRecordsPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var This = (o as Pager);
            if (This == null) return;
            This.RecalcNumberOfPages();
            This.RefreshControlView();
        }

        private static void PageSizePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var This = (o as Pager);
            if (This == null) return;
            This.RecalcNumberOfPages();
            This.RefreshControlView();
        }

        private static void CurrentPagePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var This = (o as Pager);
            if (This == null) return;
            This.RefreshControlView();
            if (This.CurrentPageChanged != null) This.CurrentPageChanged.Invoke(This, new EventArgs());
        }

        private static void GroupSizePropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var This = (o as Pager);
            if (This != null) This.RefreshControlView();
        }

        private void RefreshControlView()
        {
            if (_numberOfPages == 1)
            {
                PageSelectorPanel.Visibility = Visibility.Collapsed;
                return;
            }
            PageSelectorPanel.Visibility = Visibility.Visible;
            _firstPageInCurrentGroup = (CurrentPage/GroupSize)*GroupSize;
            var endOfGroup = _firstPageInCurrentGroup + GroupSize - 1;
            _lastPageInCurrentGroup = (endOfGroup < _numberOfPages) ? endOfGroup : _numberOfPages - 1;

            PageSelectorPanel.Children.Clear();

            if (_firstPageInCurrentGroup > 0)
            {
                var previousButton = new Button {Style = Resources["MiniButton"] as Style};
                var tb = new TextBlock {Text = ("...")};
                previousButton.Content = tb;
                previousButton.Click += PreviousGroup_Click;
                PageSelectorPanel.Children.Add(previousButton);
            }

            for (int i = _firstPageInCurrentGroup; i <= _lastPageInCurrentGroup; i++)
            {
                var pi = new RadioButton {Style = Resources["RadioButtonBorderStyle"] as Style};
                var tb = new TextBlock
                {
                    Text = (i + 1).ToString(CultureInfo.InvariantCulture),
                    Margin = new Thickness(2, 0, 2, 0),
                    VerticalAlignment = VerticalAlignment.Center
                };
                if (i == CurrentPage) pi.IsChecked = true;
                pi.Tag = i;
                pi.Content = tb;
                pi.VerticalContentAlignment = VerticalAlignment.Center;
                pi.Checked += pi_Checked;
                PageSelectorPanel.Children.Add(pi);
            }

            if (_lastPageInCurrentGroup < _numberOfPages - 1)
            {
                var nextButton = new Button {Style = Resources["MiniButton"] as Style};
                var tb = new TextBlock {Text = ("...")};
                nextButton.Content = tb;
                nextButton.Click += NextGroup_Click;
                PageSelectorPanel.Children.Add(nextButton);
            }
        }

        private void pi_Checked(object sender, RoutedEventArgs e)
        {
            var pi = sender as RadioButton;
            if (pi != null) CurrentPage = (int) pi.Tag;
        }

        private void RecalcNumberOfPages()
        {
            _numberOfPages = (int) Math.Ceiling(TotalRecords/(double) PageSize);
        }

        private void PreviousGroup_Click(object sender, RoutedEventArgs e)
        {
            CurrentPage = (CurrentPage/GroupSize)*GroupSize - 1;
        }

        private void NextGroup_Click(object sender, RoutedEventArgs e)
        {
            CurrentPage = (CurrentPage/GroupSize)*GroupSize + GroupSize;
        }
    }
}