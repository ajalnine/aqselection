﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using AQBasicControlsLibrary;

namespace AQControlsLibrary
{
    public partial class ColorSelector
    {
        public delegate void ColorChangedDelegate(object sender, ColorChangedEventArgs e);

        private ColorScales _currentColorScale = ColorScales.HSV;
        private TableMarkMode _currentTableMarkMode = TableMarkMode.Cell;
        private Double? _colorParameter;

        public static readonly DependencyProperty TableModeProperty =
            DependencyProperty.Register("TableMode", typeof(bool), typeof(ColorSelector), new PropertyMetadata(true, PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == TableModeProperty)
                ((ColorSelector)o).TableModePanel.Visibility = ((bool)e.NewValue ? Visibility.Visible : Visibility.Collapsed);
        }

        public bool TableMode
        {
            get { return (bool)GetValue(TableModeProperty); }
            set { SetValue(TableModeProperty, value); }
        }

        public ColorSelector()
        {
            InitializeComponent();
            SetCurrentColorScale(ColorScales.HSV);
            SetCurrentColor(null);
            SetCurrentTableMarkMode(TableMarkMode.Cell);
            RefreshColorButtons();
        }

        private void RefreshColorButtons()
        {
            foreach (var b in ColorsPanel.Children.OfType<Button>())
            {
                var value =
                    Double.Parse(b.Tag.ToString());
                var color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(_currentColorScale, value));
                var rect = b.Content as Rectangle;
                if (rect == null) continue;
                rect.Fill = new SolidColorBrush(color);
            }
        }

        public event ColorChangedDelegate ColorChanged;

        protected virtual void OnColorChanged(ColorChangedEventArgs e)
        {
            var handler = ColorChanged;
            if (handler != null) handler(this, e);
        }

        public ColorScales GetCurrentColorScale()
        {
            return _currentColorScale;
        }

        public Double? GetCurrentColorParameter()
        {
            return _colorParameter;
        }

        public void SetCurrentColorScale(ColorScales cs)
        {
            var rb = (from r in ButtonPanel.Children
                      where (r is RadioButton) && (r as RadioButton).Name == cs.ToString() && (r as RadioButton).GroupName == "ScaleGroup"
                      select r as RadioButton).FirstOrDefault();
            if (rb == null) return;
            rb.IsChecked = true;
            _currentColorScale = (ColorScales)Enum.Parse(typeof(ColorScales), rb.Name, true);
        }

        public void SetCurrentTableMarkMode(TableMarkMode tmm)
        {
            var rb = (from r in ButtonPanel.Children
                      where (r is RadioButton) && (r as RadioButton).Name == tmm.ToString() && (r as RadioButton).GroupName == "TableMarkModeGroup"
                      select r as RadioButton).FirstOrDefault();
            if (rb == null) return;
            rb.IsChecked = true;
            _currentTableMarkMode = (TableMarkMode)Enum.Parse(typeof(TableMarkMode), rb.Name, true);
        }
        public void SetCurrentColor(Double? colorParameter)
        {
            _colorParameter = colorParameter;
        }

        public void RefreshUI()
        {
            SetCurrentColorScale(_currentColorScale);
            SetCurrentColor(_colorParameter);
            SetCurrentTableMarkMode(_currentTableMarkMode);
            RefreshColorButtons();
        }
        private void MarkerButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;

            if (rb.GroupName != "ScaleGroup") return;
            _currentColorScale = (ColorScales)Enum.Parse(typeof(ColorScales), rb.Name, true);
            RefreshUI();
        }

        private void ButtonPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            SetupButtonGradients();
        }

        private void SetupButtonGradients()
        {
            HSVGradient.GradientStops.Clear();
            BlueGradient.GradientStops.Clear();
            RedGradient.GradientStops.Clear();
            RedGreenGradient.GradientStops.Clear();
            MetalGradient.GradientStops.Clear();

            for (double a = 1; a >= 0; a -= 0.1)
            {
                HSVGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.HSV, a))
                });
                BlueGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.Blue, a))
                });
                RedGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.Red, a))
                });
                RedGreenGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.RedGreen, a))
                });
                MetalGradient.GradientStops.Add(new GradientStop
                {
                    Offset = a,
                    Color = ColorConvertor.ConvertStringToColor(ColorConvertor.GetColorStringForScale(ColorScales.Metal, a))
                });
            }
        }

        private void ColorButton_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null) return;
            _colorParameter = button.Name == "NoColor" ? (double?)null : Double.Parse(button.Tag.ToString());

            var ce = new ColorChangedEventArgs
            {
                ColorScale = _currentColorScale,
                ColorParameter = _colorParameter,
                TableMarkMode = _currentTableMarkMode
            };
            if (_colorParameter.HasValue)
            {
                ce.ColorString = ColorConvertor.GetColorStringForScale(_currentColorScale, _colorParameter.Value);
                ce.Color = ColorConvertor.ConvertStringToColor(ce.ColorString);
            }
            else
            {
                ce.Color = Colors.Transparent;
                ce.ColorString = null;
            }

            OnColorChanged(ce);
        }

        private void ColorsPanel_OnLoaded(object sender, RoutedEventArgs e)
        {
            ColorsPanel.Children.Clear();
            for (double i = 0; i <= 1; i += 0.1)
            {
                var b = new Button
                {
                    Tag = i.ToString(CultureInfo.CurrentCulture),
                    Width = 10,
                    Height = 10,
                    UseLayoutRounding = true,
                    Margin = new Thickness(0),
                    Style = Resources["ButtonBorderStyle"] as Style,
                    Content = new Rectangle
                    {
                        Width = 6,
                        Height = 6,
                        Stroke = new SolidColorBrush(Colors.Black),
                        StrokeThickness = 0.5,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    }
                };
                b.Click += ColorButton_Click;
                ColorsPanel.Children.Add(b);
            }
            RefreshColorButtons();
        }

        private void MarkModeButton_Click(object sender, RoutedEventArgs e)
        {
            var rb = (RadioButton)sender;
            if (rb.GroupName != "TableMarkModeGroup") return;
            _currentTableMarkMode = (TableMarkMode)Enum.Parse(typeof(TableMarkMode), rb.Name, true);
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            var ce = new ColorChangedEventArgs
            {
                ColorScale = _currentColorScale,
                ColorParameter = _colorParameter,
                TableMarkMode = TableMarkMode.Clear,
                Color = Colors.Transparent,
                ColorString = null
            };

            OnColorChanged(ce);
        }
    }

    public class ColorChangedEventArgs : EventArgs
    {
        public ColorScales ColorScale { get; set; }
        public double? ColorParameter { get; set; }

        public TableMarkMode TableMarkMode { get; set; }

        public Color Color { get; set; }

        public string ColorString { get; set; }
    }

    public enum TableMarkMode
    {
        Column,
        Row,
        Cell,
        Clear
    }
}