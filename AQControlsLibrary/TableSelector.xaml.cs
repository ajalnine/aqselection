﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using ApplicationCore.CalculationServiceReference;

// ReSharper disable once CheckNamespace
namespace AQControlsLibrary
{
    public partial class TableSelector
    {
        private TableOrderMode _currentTableOrderMode = TableOrderMode.Unordered;
        private readonly Dictionary<TableOrderMode, string> _tableModeLabels =
            new Dictionary<TableOrderMode, string> { { TableOrderMode.Unordered, String.Empty }, { TableOrderMode.Abc, "A..Z" }, { TableOrderMode.Reversed, "Z..A" } };
        private bool _isComboBox;
        private ComboBox _comboBoxSelector;
        private List<SLDataTable> _dataTables;
        private SLDataTable _selectedTable;

        public event SelectedTableChangedDelegate SelectedTableChanged;
        public TableSelector()
        {
            InitializeComponent();
            
        }

        public string CreateTableSelectors(List<SLDataTable> dataTables, SLDataTable defaultTable = null)
        {
            _dataTables = dataTables;
            ClearSelectorPanel();
            if (_dataTables.Count == 0) return null;
            OrderModeTextBlock.Text = _tableModeLabels[_currentTableOrderMode];

            var tableToSelect = (defaultTable ?? (_dataTables.Count > 1 ? _dataTables[1]: _dataTables[0]));
            var orderedNameSource = GetOrderedNames().ToList();

            if (orderedNameSource.Count > 10 || orderedNameSource.Sum(a => a.Length) > 300)
            {
                _isComboBox = true;
                _comboBoxSelector = new ComboBox
                {
                    ItemsSource = orderedNameSource,
                    Style = Resources["GenericComboBoxStyle"] as Style,
                    ItemContainerStyle = Resources["GenericComboBoxItemStyle"] as Style,
                    Background = new SolidColorBrush(Colors.Transparent),
                    Height = 24,
                    Width = 500,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(5, 0, 0, 0)
                };
                SelectorPanel.Children.Add(_comboBoxSelector);
                _comboBoxSelector.SelectedItem = tableToSelect.TableName;
                _selectedTable = tableToSelect;
                _comboBoxSelector.SelectionChanged += ComboBoxTableSelector_SelectionChanged;
            }
            else
            {
                _isComboBox = false;
                foreach (var s in orderedNameSource)
                {
                    var erb = GetTableSelectorButton(s);
                    erb.IsChecked = s == tableToSelect.TableName;
                    erb.Click += SelectorButton_Click;
                    SelectorPanel.Children.Add(erb);
                }
            }
            return tableToSelect.TableName;
        }

        private void ComboBoxTableSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            if (c == null) return;
            _selectedTable = _dataTables.Single(a=>a.TableName == c.SelectedItem?.ToString());
            SelectedTableChanged?.Invoke(this, new SelectedTableChangedEventArgs {AllTables = _dataTables, SelectedTable = _selectedTable });
        }

        public IEnumerable<string> GetOrderedNames()
        {
            var nameSource = (from d in _dataTables select d.TableName).Where(a => !a.StartsWith("#")).ToList();
            var orderedNameSource = nameSource;
            switch (_currentTableOrderMode)
            {
                case TableOrderMode.Abc:
                    orderedNameSource = nameSource.OrderBy(a => a).ToList();
                    break;
                case TableOrderMode.Reversed:
                    orderedNameSource = nameSource.OrderByDescending(a => a).ToList();
                    break;
            }
            return orderedNameSource;
        }

        public void ChangeTableName(string oldName, string newName)
        {
            if (!_isComboBox)
            {
                var renamingButton = (from s in SelectorPanel.Children
                                      let radioButton = s as RadioButton
                                      where Equals(radioButton.Tag, oldName)
                                      select radioButton).SingleOrDefault();

                var grid = renamingButton?.Content as Grid;

                var tb = grid?.Children[0] as TextBlock;
                if (tb == null || tb.Text.Trim().ToLower() != oldName.Trim().ToLower()) return;
                tb.Text = newName;
                renamingButton.Tag = newName;
            }
            else
            {
                var c = _comboBoxSelector.ItemsSource.OfType<string>().ToList();
                var i = c.IndexOf(oldName);
                if (i >= 0)
                {
                    c[i] = newName;
                    _comboBoxSelector.SelectionChanged -= ComboBoxTableSelector_SelectionChanged;
                    _comboBoxSelector.ItemsSource = c;
                    _comboBoxSelector.SelectedValue = newName;
                    _comboBoxSelector.SelectionChanged += ComboBoxTableSelector_SelectionChanged;
                }
            }
        }

        public void StartLoadTableProgress()
        {
            Dispatcher.BeginInvoke(delegate
            {
                SelectorPanel.IsHitTestVisible = false;
                SelectorPanel.Opacity = 0.8;
                var rb = GetCurrentSelectorButton();
                var grid = rb.Content as Grid;
                if (grid == null) return;
                var rectangle = grid.Children[1] as Rectangle;
                if (rectangle != null) rectangle.Visibility = Visibility.Visible;
            });
        }

        public void StopLoadTableProgress()
        {
            Dispatcher.BeginInvoke(delegate
            {
                SelectorPanel.IsHitTestVisible = true;
                SelectorPanel.Opacity = 1;
                var rb = GetCurrentSelectorButton();
                var grid = rb.Content as Grid;
                if (grid == null) return;
                var rectangle = grid.Children[1] as Rectangle;
                if (rectangle != null) rectangle.Visibility = Visibility.Collapsed;
            });
        }

        private RadioButton GetCurrentSelectorButton()
        {
            return (from s in SelectorPanel.Children
                    let radioButton = s as RadioButton
                    where radioButton.IsChecked != null && (radioButton != null && (s is RadioButton && radioButton.IsChecked.Value))
                    select s)
                    .SingleOrDefault() as RadioButton;
        }

        private RadioButton GetTableSelectorButton(string s)
        {
            var g = new Grid();
            g.Children.Add(new TextBlock
            {
                Text = XmlConvert.DecodeName(s),
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                Padding = new Thickness(3)
            });

            var r = new Rectangle
            {
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Stroke = new SolidColorBrush(Colors.Red),
                StrokeThickness = 2,
                Visibility = Visibility.Collapsed,
                StrokeDashArray = new DoubleCollection { 2, 2 }
            };

            var animation = new DoubleAnimation
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 2)),
                From = 200,
                To = 0,
                RepeatBehavior = RepeatBehavior.Forever
            };
            var sb = new Storyboard { Duration = new Duration(new TimeSpan(0, 0, 0, 2)) };
            sb.Children.Add(animation);
            Storyboard.SetTarget(animation, r);
            Storyboard.SetTargetProperty(animation, new PropertyPath("StrokeDashOffset"));
            r.Resources.Add(s, sb);
            g.Children.Add(r);
            sb.Begin();

            return new RadioButton
            {
                Style = Resources["RadioButtonBorderStyle"] as Style,
                Content = g,
                Background = new SolidColorBrush(Colors.Transparent),
                Tag = s,
                GroupName = "0"
                    ,
                Height = 24
                    ,
                VerticalAlignment = VerticalAlignment.Center
                    ,
                Margin = new Thickness(5, 0, 0, 0)
            };
        }

        private void SelectorButton_Click(object sender, RoutedEventArgs e)
        {
            var erb = (RadioButton)sender;
            _selectedTable = _dataTables.First(a => a.TableName == erb.Tag.ToString());
            SelectedTableChanged?.Invoke(this, new SelectedTableChangedEventArgs { AllTables = _dataTables, SelectedTable = _selectedTable });
        }

        public SLDataTable GetSelectedTable()
        { 
            return _selectedTable;
        }

        public TableOrderMode GetOrderMode()
        {
            return _currentTableOrderMode;
        }

        private void ClearSelectorPanel()
        {
            SelectorPanel.Children.Clear();
            SelectorPanel.Visibility = (_dataTables == null || _dataTables.Count == 1)
                                           ? Visibility.Collapsed
                                           : Visibility.Visible;
            ReorderTablesButton.Visibility = SelectorPanel.Visibility;
            OrderModeTextBlock.Visibility = SelectorPanel.Visibility;
        }
        private void ReorderTablesButton_OnClick(object sender, RoutedEventArgs e)
        {
            int mode = (int)_currentTableOrderMode + 1;
            if (mode > 2) mode = 0;
            _currentTableOrderMode = (TableOrderMode)mode;
            ClearSelectorPanel();
            CreateTableSelectors(_dataTables, _selectedTable);
        }

        private void TableSelector_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (SelectorPanel.Children.Any() && SelectorPanel.Children[0] is ComboBox)
                ((ComboBox) SelectorPanel.Children[0]).Focus();
        }

        private void TableSelector_OnKeyDown(object sender, KeyEventArgs e)
        {
            var tables = GetOrderedNames().ToList();
            var index = tables.IndexOf(_selectedTable.TableName);
            if (e.Key == Key.Down || e.Key == Key.Right)
            {
                index++;
                if (index == tables.Count) index = 0;
                ChangeTable(tables, index);
            }
            if (e.Key == Key.Up|| e.Key == Key.Left)
            {
                index--;
                if (index == -1) index = tables.Count-1;
                ChangeTable(tables, index);
            }
        }

        private void ChangeTable(List<string> tables, int index)
        {
            _selectedTable = _dataTables.Single(a => a.TableName == tables[index]);
            if (_isComboBox) ((ComboBox) SelectorPanel.Children[0]).SelectedValue = tables[index];
            else
                ((RadioButton)
                    SelectorPanel.Children.Single(a => ((RadioButton) a).Tag.ToString() == tables[index]))
                    .IsChecked = true;
            SelectedTableChanged?.Invoke(this,
                new SelectedTableChangedEventArgs {AllTables = _dataTables, SelectedTable = _selectedTable});
        }
    }
    public enum TableOrderMode
    {
        Unordered = 0, Abc = 1, Reversed = 2
    }

    public delegate void SelectedTableChangedDelegate(object o, SelectedTableChangedEventArgs e);

    public class SelectedTableChangedEventArgs
    {
        public List<SLDataTable> AllTables;
        public SLDataTable SelectedTable;
    }
}