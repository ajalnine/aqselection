﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;

namespace AQControlsLibrary
{
    public partial class InlineDataSetPresenter
    {
        public delegate void DataProcessDelegate(object sender, TaskStateEventArgs e);

        private readonly CalculationService _cas;
        private object _currentTaskState;
        private List<SLDataTable> _dataTables;
        private string _fileName;
        private int _frozenColumnCount = 1;
        public InlineDataSetPresenter()
        {
            InitializeComponent();
            if (_cas == null) _cas = Services.GetCalculationService();
// ReSharper disable once RedundantJumpStatement
            if (DesignerProperties.GetIsInDesignMode(this)) return;
        }

        public event DataProcessDelegate DataLoaded;
        public event DataProcessDelegate DataProcessed;

        public void BeginLoadData(string fileName, List<Step> sourceCalculation, string desc, DateTime? from,
                                  DateTime? to, object iar)
        {
            _currentTaskState = iar;
            _fileName = fileName;
            _cas.BeginGetTablesSLCompatible(_fileName, GetDataInXMLDone, _currentTaskState);
        }

        private void GetDataInXMLDone(IAsyncResult state)
        {
            _dataTables = _cas.EndGetTablesSLCompatible(state);
            if (DataLoaded != null) DataLoaded(this, new TaskStateEventArgs(_currentTaskState));
            ConstructSelectorPanel();
            if (DataProcessed != null) DataProcessed(this, new TaskStateEventArgs(_currentTaskState));
        }

        public void DirectLoadData(List<SLDataTable> sldt)
        {
            _dataTables = sldt;
            ConstructSelectorPanel();
        }

        private void SelectorButton_Checked(object sender, RoutedEventArgs e)
        {
            var erb = (RadioButton) sender;
            ShowTable(erb.Tag.ToString());
        }

        public void Hide()
        {
            SelectorPanel.Children.Clear();
            MainDataGrid.ItemsSource = null;
            DataGridPanel.Visibility = Visibility.Collapsed;
        }

        private void ConstructSelectorPanel()
        {
            SelectorPanel.Dispatcher.BeginInvoke(delegate
                {
                    if (_dataTables == null)
                    {
                        Hide();
                        return;
                    }
                    DataGridPanel.Visibility = Visibility.Visible;
                    SelectorPanel.Children.Clear();
                    if (_dataTables.Count > 1)
                    {
                        SelectorPanel.Visibility = Visibility.Visible;
                        bool isFirst = true;
                        foreach (var s in (from d in _dataTables select d.TableName))
                        {
                            var erb = new RadioButton
                            {
                                Style = Resources["RadioButtonBorderStyle"] as Style,
                                Content = new TextBlock
                                {
                                    Text = XmlConvert.DecodeName(s),
                                    VerticalAlignment = VerticalAlignment.Center,
                                    HorizontalAlignment = HorizontalAlignment.Center,
                                    Padding = new Thickness(3)
                                },
                                Background = new SolidColorBrush(Colors.Transparent),
                                Tag = s,
                                GroupName = "0",
                                Height = 24,
                                VerticalAlignment = VerticalAlignment.Center
                            };
                            erb.Checked += SelectorButton_Checked;
                            erb.Margin = new Thickness(5, 1, 1, 0);
                            SelectorPanel.Children.Add(erb);
                            if (!isFirst) continue;
                            isFirst = false;
                            erb.IsChecked = true;
                        }
                    }
                    else
                    {
                        SelectorPanel.Visibility = Visibility.Collapsed;
                        ShowTable(_dataTables[0].TableName);
                    }
                });
        }

        private void ShowTable(string tableName)
        {
            var sldt = (from d in _dataTables where d.TableName == tableName select d).First();

            DataGridPanel.Dispatcher.BeginInvoke(delegate
                {
                    MainDataGrid.Columns.Clear();
                    MainDataGrid.ItemsSource = sldt.Table;
                    var i = 0;
                    foreach (string s in sldt.ColumnNames)
                    {
                        var dgc = new DataGridTextColumn
                        {
                            Header = s,
                            Binding = new Binding("Row[" + i + "]"),
                            CanUserSort = true,
                            SortMemberPath = "Row[" + i + "]"
                        };
                        i++;
                        MainDataGrid.Columns.Add(dgc);
                    }
                    MainDataGrid.CanUserSortColumns = true;
                    MainDataGrid.FrozenColumnCount = _frozenColumnCount;
                    MainDataGrid.UpdateLayout();
                }
                );
        }

        public void SetFrozenColumnCount(int columnCount)
        {
            _frozenColumnCount = columnCount;
        }
    }
}