﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ApplicationCore;
using ApplicationCore.CalculationServiceReference;

namespace AQControlsLibrary
{
    public class DataLoader
    {
        public delegate void DataProcessDelegate(object sender, TaskStateEventArgs e);

        public static readonly DependencyProperty DisposeDataAfterClosingProperty =
            DependencyProperty.Register("DisposeDataAfterClosing", typeof (bool), typeof (DataLoader),
                                        new PropertyMetadata(true));

        private readonly CalculationService _cs;
        private List<SLDataTable> _dataTables;
        private string _fileName;
        private object _taskState;

        public DataLoader()
        {
            if (_cs == null) _cs = Services.GetCalculationService();
        }

        public event DataProcessDelegate DataLoaded;
        public event DataProcessDelegate DataProcessed;

        public void BeginLoadData(string fileName, object iar)
        {
            _taskState = iar;
            _fileName = fileName;
            _cs.BeginGetTablesSLCompatible(_fileName, GetDataInXMLDone, _taskState);
        }

        private void GetDataInXMLDone(IAsyncResult state)
        {
            _dataTables = _cs.EndGetTablesSLCompatible(state);
            if (DataLoaded != null) DataLoaded(this, new TaskStateEventArgs(_taskState));
            if (DataProcessed != null) DataProcessed(this, new TaskStateEventArgs(_taskState));
        }

        public SLDataTable GetTable(string tableName)
        {
            return (from d in _dataTables where d.TableName == tableName select d).First();
        }

        public List<SLDataTable> GetTables()
        {
            return _dataTables;
        }
    }
}