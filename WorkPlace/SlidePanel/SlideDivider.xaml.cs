﻿using System;
using System.Windows;

// ReSharper disable once CheckNamespace
namespace WorkPlace
{

    public enum SlideDividerMode { Empty, First, Generic, Last }
    public delegate void SlideDividerDelegate(object o, EventArgs e);
    
    public partial class SlideDivider
    {
        public SlideDividerMode DividerMode
        {
            get { return (SlideDividerMode)GetValue(DividerModeProperty); }
            set { SetValue(DividerModeProperty, value); }
        }

        public static readonly DependencyProperty DividerModeProperty =
            DependencyProperty.Register("DividerMode", typeof(SlideDividerMode), typeof(SlideDivider), new PropertyMetadata(SlideDividerMode.Empty, DividerModeChangedCallback));
        
        public bool NonBreakIsUnlocked
        {
            get { return (bool)GetValue(NonBreakIsUnlockedProperty); }
            set { SetValue(NonBreakIsUnlockedProperty, value); }
        }

        public static readonly DependencyProperty NonBreakIsUnlockedProperty =
            DependencyProperty.Register("NonBreakIsUnlocked", typeof(bool), typeof(SlideDivider), new PropertyMetadata(true));

        public bool IsContinuous
        {
            get { return (bool)GetValue(IsContinuousProperty); }
            set { SetValue(IsContinuousProperty, value); }
        }

        public static readonly DependencyProperty IsContinuousProperty =
            DependencyProperty.Register("IsContinuous", typeof(bool), typeof(SlideDivider), new PropertyMetadata(false));

        public bool InsertHere
        {
            get { return (bool)GetValue(InsertHereProperty); }
            set { SetValue(InsertHereProperty, value); }
        }

        public static readonly DependencyProperty InsertHereProperty =
            DependencyProperty.Register("InsertHere", typeof(bool), typeof(SlideDivider), new PropertyMetadata(false));

        public string GroupName
        {
            get { return (string)GetValue(GroupNameProperty); }
            set { SetValue(GroupNameProperty, value); }
        }

        public static readonly DependencyProperty GroupNameProperty =
            DependencyProperty.Register("GroupName", typeof(string), typeof(SlideDivider), new PropertyMetadata("GroupDivider1"));

        public event SlideDividerDelegate  MarkedAsInsertion;
        
        public SlideDivider()
        {
            InitializeComponent();
        }

        private static void DividerModeChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sd = o as SlideDivider;
            var sdm = (SlideDividerMode)e.NewValue;
            if (sd != null) sd.DivideMarker.Style = sd.Resources[sdm + "Divider"] as Style;
        }
        
        private void InsertMarker_Checked(object sender, RoutedEventArgs e)
        {
            InsertHere = true;
            MarkedAsInsertion?.Invoke(this, new EventArgs());
        }

        private void InsertMarker_Unchecked(object sender, RoutedEventArgs e)
        {
            InsertHere = false;
        }
    }
}
