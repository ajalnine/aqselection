﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Core;
using Core.ReportingServiceReference;

// ReSharper disable once CheckNamespace
namespace WorkPlace
{
    public delegate void SlideViewDelegate(object o, SlideViewEventArgs e);
    public delegate void SlideDragDelegate(object o, SlideDragEventArgs e);

    public partial class SlideView
    {
        public event SlideViewDelegate SlideAboutDelete;
        public event SlideDragDelegate DragStarted;
        public event SlideDragDelegate DragStopped;
        
        public ClientSlideElementDescription PresentedCsed
        {
            get { return (ClientSlideElementDescription)GetValue(PresentedCsedProperty); }
            set { SetValue(PresentedCsedProperty, value); }
        }

        public static readonly DependencyProperty PresentedCsedProperty =
            DependencyProperty.Register("PresentedCsed", typeof(ClientSlideElementDescription), typeof(SlideView), new PropertyMetadata(null, PresentedCsedChangedCallback));
        
        private List<byte> _png;
                
        private static void PresentedCsedChangedCallback(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var sv = o as SlideView;
            if (sv?.PresentedCsed.Thumbnail != null && sv.PresentedCsed.SED.SlideElementType == SlideElementTypes.Image)
            {
                if (sv.PresentedCsed.SED.SlideData == null)
                {
                    sv.GeneratePNG(sv);
                }
                else
                {
                    sv.Thumbnail.Source = sv.PresentedCsed.Thumbnail;
                    sv.ProgressIndicator.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                if (sv != null) sv.Thumbnail.Source = new BitmapImage {UriSource = new Uri($"/Resources;component/Images/SlideTypes/{sv.PresentedCsed.SED.SlideElementType}.png", UriKind.Relative)};
            }
            if (sv == null) return;
            sv.Description.Text = sv.PresentedCsed.SED.Description;
            sv.SetupButtonsState();
        }

        private void SetupButtonsState()
        {
            ExportPNGButton.Visibility = (_png == null) ? Visibility.Collapsed : Visibility.Visible;
            ReturnToPreview.Visibility = (PresentedCsed.SourceData == null) ? Visibility.Collapsed : Visibility.Visible;
            ExportCalcButton.Visibility = (PresentedCsed.SourceCalculation == null) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void GeneratePNG(SlideView sv)
        {
            throw new Exception();
            /*
            sv.Thumbnail.Source = sv.PresentedCsed.Thumbnail;
            var bw = new BackgroundWorker();
            sv.ProgressIndicator.Visibility = Visibility.Visible;
            sv.PresentedCsed.IsBusy = true;
            var wb = sv.PresentedCsed.Thumbnail;

            bw.DoWork += (a, b) =>
            {
                sv._png = PNG.WriteableBitmapToPNG(wb).ToList();
            };

            bw.RunWorkerCompleted += (a, b) => sv.Dispatcher.BeginInvoke(new Action(() =>
            {
                sv.PresentedCsed.SED.SlideData = sv._png;
                sv.ProgressIndicator.Visibility = Visibility.Collapsed;
                sv.PresentedCsed.IsBusy = false;
                sv.SetupButtonsState();
            }));

            bw.RunWorkerAsync();
            */
        }

        public SlideView()
        {
            InitializeComponent();
        }

        private void DeleteSlideButton_Click(object sender, RoutedEventArgs e)
        {
            SlideAboutDelete?.Invoke(this, new SlideViewEventArgs { CSED = PresentedCsed });
        }

        private void ExportPNGButton_Click(object sender, RoutedEventArgs e)
        {
            throw new Exception();
            //CommonTasks.SendReadyPNGFile(this, _png, "Слайд ", null);
        }

        private void ExportCalcButton_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object>();
            if (PresentedCsed.From.HasValue) signalArguments.Add("From", PresentedCsed.From.Value);
            if (PresentedCsed.To.HasValue) signalArguments.Add("To", PresentedCsed.To.Value);
            signalArguments.Add("Operation", PresentedCsed.Operation);
            signalArguments.Add("Calculation", PresentedCsed.SourceCalculation);
            signalArguments.Add("Description", PresentedCsed.SED.Description);
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.InsertCalculation, signalArguments, null);
        }

        private void ReturnToPreview_Click(object sender, RoutedEventArgs e)
        {
            var signalArguments = new Dictionary<string, object>();
            if (PresentedCsed.From.HasValue) signalArguments.Add("From", PresentedCsed.From.Value);
            if (PresentedCsed.To.HasValue) signalArguments.Add("To", PresentedCsed.To.Value);
            signalArguments.Add("Operation", PresentedCsed.Operation);
            signalArguments.Add("SourceData", PresentedCsed.SourceData);
            signalArguments.Add("Calculation", PresentedCsed.SourceCalculation);
            signalArguments.Add("Description", PresentedCsed.SED.Description);
            Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), Command.ReopenInAnalisys, signalArguments, null);
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            ButtonPanel.Visibility = Visibility.Visible;
            _mouseIsIn = true;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            ButtonPanel.Visibility = Visibility.Collapsed;
            _mouseIsIn = false;
            _mouseIsDown = false;
        }
        
        #region Drag&Drop

        private bool _mouseIsIn;
        private bool _mouseIsDown;
        private bool _dragStartIsInvoked;
        private Point _dragStartPosition;
        
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_mouseIsIn) return;
            _dragStartPosition = e.GetPosition(Parent as UIElement);
            _dragStartIsInvoked = false;
            _mouseIsDown = true;
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!_mouseIsIn) return;
            _mouseIsDown = false;
            _dragStartIsInvoked = false;
            DragStopped?.Invoke(this, new SlideDragEventArgs { CSED = PresentedCsed, Position = e.GetPosition(Parent as UIElement), Offset = e.GetPosition(this) });
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mouseIsDown || _dragStartIsInvoked) return;
            var currentPosition = e.GetPosition(Parent as UIElement);
            if (!(Math.Sqrt(Math.Pow(currentPosition.X - _dragStartPosition.X, 2) +
                            Math.Pow(currentPosition.Y - _dragStartPosition.Y, 2)) > 20)) return;
            if (DragStarted == null) return;
            DragStarted.Invoke(this, new SlideDragEventArgs { CSED = PresentedCsed, Position = currentPosition, Offset = e.GetPosition(this) });
            _dragStartIsInvoked = true;
        }
        
        #endregion
    }

    public class SlideViewEventArgs
    {
        public ClientSlideElementDescription CSED;
    }

    public class SlideDragEventArgs
    {
        public ClientSlideElementDescription CSED;
        public Point Position;
        public Point Offset;
    }
}
