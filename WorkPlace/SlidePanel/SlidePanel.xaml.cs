﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Core.CalculationServiceReference;
using Core.ReportingServiceReference;

// ReSharper disable once CheckNamespace
namespace WorkPlace
{
    public partial class SlidePanel
    {
        public ObservableCollection<ClientSlideElementDescription> InternalSlideCollection;
        private int _pngpc;
        private int ProcessCount
        {
            get
            {
                return _pngpc;
            }
            set
            {
                _pngpc = value;

                Dispatcher.BeginInvoke(value == 0
                    ? (() => EnableButtons(InternalSlideCollection.Count != 0))
                    : new Action(() => EnableButtons(false)));
            }
        }
        private bool _breakCalcInProcess;
        private int _insertionIndex;

        public SlidePanel()
        {
            InitializeComponent();
        }

        void InternalSlideCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            FirstDivider.DividerMode = (InternalSlideCollection.Count > 0) ? SlideDividerMode.First : SlideDividerMode.Empty;
            ResetButtonStates();

            for(var i=0; i<InternalSlideCollection.Count; i++)
            {
                InternalSlideCollection[i].DividerMode = (i!=InternalSlideCollection.Count-1) ? SlideDividerMode.Generic : SlideDividerMode.Last;
            }
            ShowCounters();
        }

        private void ShowCounters()
        {
            var slides = InternalSlideCollection.Count(t => !t.IsContinuous);
            CountersPanel.Visibility = slides > 0 ? Visibility.Visible : Visibility.Collapsed;
            var s = slides.ToString(CultureInfo.InvariantCulture).EndsWith("1") ? "слайде" : "слайдах";
            var e = InternalSlideCollection.Count.ToString(CultureInfo.InvariantCulture);
            var last = int.Parse(e.Substring(e.Length - 1));
            var elements = last == 1 ? "элемент" : last < 5 ? "элемента" : "элементов";
            SedNumber.Text = String.Format("На {0} {2} {1} {3}.", slides, e, s, elements);
        }


        public void InsertSlideElement(SlideElementDescription sed, List<Step> sourceCalculation, List<SLDataTable> sourceData, Guid sourceKey, Step operation, WriteableBitmap thumbnail, DateTime? @from, DateTime? to)
        {
            if (InternalSlideCollection == null) ResetInternalSlidesCollection();
            var csed = new ClientSlideElementDescription { SED = sed, Operation = operation, SourceCalculation = sourceCalculation, SourceKey = sourceKey, Thumbnail = thumbnail, DividerMode = SlideDividerMode.Generic, AllowNonBreak = true,  SourceData = sourceData, From = @from, To = to, IsInsertion = true };
            csed.PropertyChanged += csed_PropertyChanged;
            if (InternalSlideCollection == null) return;
            InternalSlideCollection.Insert(_insertionIndex, csed);
            UpdateLayout();
            _insertionIndex++;
            Dispatcher.BeginInvoke(new Action(() => 
            {
                if (InternalSlideCollection.Count > 1) SlideScroller.ScrollToVerticalOffset(SlideScroller.ScrollableHeight * (_insertionIndex - 1) / (InternalSlideCollection.Count - 1) + FirstDivider.ActualHeight);
                RecalcBreaks();
            }));
        }

        private void ResetInsertionPoint()
        {
            if (_insertionIndex == 0 )FirstDivider.InsertHere = true;
            else
            {
                InternalSlideCollection[_insertionIndex - 1].IsInsertion = true;
            }
        }

        private void RecalcBreaks()
        {
            var slideElementCounter = 0;
            foreach (var c in InternalSlideCollection)
            {
                if (c.IsContinuous)
                {
                    slideElementCounter++;
                    if (slideElementCounter == 4)
                    {
                        _breakCalcInProcess = true;

                        c.IsContinuous = false;
                        c.AllowNonBreak = false;
                        slideElementCounter = 0;

                        _breakCalcInProcess = false;
                    }
                    else c.AllowNonBreak = true;
                }
                else
                {
                    c.AllowNonBreak = slideElementCounter != 3;
                    slideElementCounter = 0;
                }
            }
        }

        void csed_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsContinuous":
                    if (!_breakCalcInProcess) RecalcBreaks();
                    break;

                case "IsBusy":
                    ProcessCount += ((ClientSlideElementDescription)sender).IsBusy ? 1 : -1;
                    break;
            }
            ShowCounters();
        }

        public PresentationDescription GetPresentation()
        {
            return null;
        }

        private void DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Удалить все слайды ?", "Предупреждение", MessageBoxButton.OKCancel);
            if (mbr == MessageBoxResult.Cancel) return;
            ResetInternalSlidesCollection();
        }

        public void Clear()
        {
            ResetInternalSlidesCollection();
        }

        private void ResetInternalSlidesCollection()
        {
            InternalSlideCollection = new ObservableCollection<ClientSlideElementDescription>();
            ProcessCount = 0;
            SlidePresenter.ItemsSource = InternalSlideCollection;
            InternalSlideCollection.CollectionChanged += InternalSlideCollection_CollectionChanged;
            FirstDivider.DividerMode = SlideDividerMode.Empty;
            _insertionIndex = 0;
            ResetInsertionPoint();
            ResetButtonStates();
            UpdateLayout();
            ShowCounters();
        }

        private void ResetButtonStates()
        {
            EnableButtons(InternalSlideCollection.Count != 0);
        }

        private void EnableButtons(bool enable)
        {
            DeleteAll.IsEnabled = enable;
            ExportPPTX.IsEnabled = enable;
            ExportDOCX.IsEnabled = enable;
            CreateCalc.IsEnabled = enable;
        }

        private void ExportPPTX_Click(object sender, RoutedEventArgs e)
        {
            var pd = CreatePresentationDescription();

            PPTXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            throw (new Exception());
/*            CommonTasks.CreatePPTX(this, pd, "Презентация ", () => Dispatcher.BeginInvoke(() =>
            {
                PPTXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));*/
        }

        private void ExportDOCX_Click(object sender, RoutedEventArgs e)
        {
            var pd = CreatePresentationDescription();

            DOCXProgressIndicator.Visibility = Visibility.Visible;
            EnableButtons(false);
            UpdateLayout();
            throw (new Exception());
            /*
            CommonTasks.CreateDOCX(this, pd, "Отчет ", () => Dispatcher.BeginInvoke(() =>
            {
                DOCXProgressIndicator.Visibility = Visibility.Collapsed;
                EnableButtons(true);
            }));*/
        }

        private PresentationDescription CreatePresentationDescription()
        {
            var pd = new PresentationDescription {Slides = new List<SlideDescription>()};

            var sd = new SlideDescription {SlideElements = new List<SlideElementDescription>()};
            foreach (var csed in InternalSlideCollection)
            {
                sd.SlideElements.Add(csed.SED);
                if (!csed.IsContinuous)
                {
                    pd.Slides.Add(sd);
                    sd = new SlideDescription { SlideElements = new List<SlideElementDescription>() };
                }
            }
            if (sd.SlideElements.Count>0)pd.Slides.Add(sd);
            return pd;
        }

        private void CreateCalc_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Позже будет реализовано формирование расчета для построения графиков автоматически");
        }

        private void FirstDivider_MarkedAsInsertion(object o, EventArgs e)
        {
            _insertionIndex = 0;
        }

        private void SlideView_SlideAboutDelete(object o, SlideViewEventArgs e)
        {
            var index = InternalSlideCollection.IndexOf(e.CSED);
            if (index >= 0)
            {
                InternalSlideCollection.Remove(e.CSED);
                if (index < _insertionIndex) _insertionIndex--;
                if (!InternalSlideCollection.Any())_insertionIndex = 0;
            }
            ResetInsertionPoint();
        }

        #region Drag&Drop

        private int _dragStartPosition = -1;
        private int _dragEndPosition = -1;
        private bool _dragStarted;
        private Point _offset;

        private void SlideView_DragStarted(object o, SlideDragEventArgs e)
        {
            var sv = o as SlideView;
            _dragStarted = true;
            _offset = e.Offset;
            _dragStartPosition = InternalSlideCollection.IndexOf(e.CSED);
            if (sv != null)
            {
                var i = new Image { Source = e.CSED.Thumbnail, Width = sv.ActualWidth, Height = sv.ActualHeight };
                SlideDragView.SetDragItem(i);
            }
            SlideDragView.SetDropPossibilityIcon(null);
        }

        private void SlideView_DragStopped(object o, SlideDragEventArgs e)
        {
            if (_dragStarted)
            {
                _dragEndPosition = InternalSlideCollection.IndexOf(e.CSED);
                if (_dragEndPosition != _dragStartPosition)
                {
                    ClientSlideElementDescription movingElement = InternalSlideCollection[_dragStartPosition];
                    InternalSlideCollection.Remove(movingElement);
                    InternalSlideCollection.Insert(_dragEndPosition, movingElement);
                    RecalcBreaks();
                    _insertionIndex = _dragEndPosition+1;
                }
            }
            StopDrag();
        }

        private void StopDrag()
        {
            _dragStartPosition = -1;
            _dragEndPosition = -1;
            _dragStarted = false;
            ResetInsertionPoint();
            SlideDragView.RemoveDragItem();
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            var currentPosition = e.GetPosition(SlidePresenter);
            SlideDragView.SetDragItemPosition(new Point(currentPosition.X - _offset.X, currentPosition.Y - _offset.Y));
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            StopDrag();
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            StopDrag();
        }

        #endregion

        private void SlideDivider_MarkedAsInsertion(object o, EventArgs e)
        {
            var slideDivider = o as SlideDivider;
            if (slideDivider != null) _insertionIndex = 1 + InternalSlideCollection.IndexOf(slideDivider.Tag as ClientSlideElementDescription);
        }
    }

    public class ClientSlideElementDescription : INotifyPropertyChanged
    {
        public SlideElementDescription SED { get; set; }

        public List<Step> SourceCalculation { get; set; }
        public List<SLDataTable> SourceData { get; set; }

        public DateTime? From, To;

        public Guid SourceKey { get; set; }
        public Step Operation { get; set; }
        public WriteableBitmap Thumbnail { get; set; }
        
        private SlideDividerMode _dividermode;
        public SlideDividerMode DividerMode 
        {
            get { return _dividermode; }
            set { _dividermode = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("DividerMode"));
            }
        }

        private bool _iscontinuous;
        public bool IsContinuous
        {
            get { return _iscontinuous; }
            set { _iscontinuous = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsContinuous"));
            }
        }

        private bool _isbusy;
        public bool IsBusy
        {
            get { return _isbusy; }
            set { _isbusy = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsBusy"));
            }
        }
        
        private bool _isinsertion;
        public bool IsInsertion
        {
            get { return _isinsertion; }
            set { _isinsertion = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsInsertion"));
            }
        }
        
        private bool _allownonbreak;
        public bool AllowNonBreak
        {
            get { return _allownonbreak; }
            set { _allownonbreak = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AllowNonBreak"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
