﻿using System;
using System.Linq;
using System.Windows;
using Core;
using AQSelection;

namespace WorkPlace
{
    public partial class App
    {
        public App()
        {
            var ip = System.Security.Principal.WindowsIdentity.GetCurrent()?.Name;

            var adc = new AdministrationDomainContext();

            var q = adc.GetAQ_UserQuery(ip);
            adc.AQ_Users.Load(q, a =>
            {
                var currentUser = a.Entities.SingleOrDefault();
                if (currentUser != null)
                {
                    Security.CurrentUser = currentUser.Login;
                    Security.CurrentUserName = currentUser.CachedUserName;
                    Security.CurrentTitle = currentUser.Title;
                    Security.CurrentWorkPlace = currentUser.Workplace;
                    Security.Roles = Security.DecryptRoleString(currentUser.Roles, currentUser.Login);
                }
            }, true);
        }
    }
}
