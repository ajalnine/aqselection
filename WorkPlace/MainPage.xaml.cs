﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BasicControlsLibrary;
using Core;
using Core.AQServiceReference;
using Core.CalculationServiceReference;
using Core.ReportingServiceReference;

namespace WorkPlace
{
    public partial class MainPage : IAQModule
    {
        public void InitSignaling(CommandCallBackDelegate ccbd) {}
        public event ReadyForAcceptSignalsDelegate ReadyForAcceptSignals;
        private bool _initialized = false;        
        private string _initName;
        private string _assemblyName;
        private readonly AQService _aqs;
        private readonly ImageSource _newCalculation;
        private readonly AQModuleDescription _aqmd;
        private readonly Dictionary<Guid, AQModuleDescription> _boundModules;
        public MainPage()
        {
            InitializeComponent();
            _aqmd = new AQModuleDescription { Instance = this, ModuleType = GetType(), Name = GetType().Name }; 
            
            _newCalculation = new BitmapImage {UriSource = new Uri("/Resources;component/Images/Catalogue/Calculation.png", UriKind.Relative)}; 
            _boundModules = new Dictionary<Guid, AQModuleDescription>();
            Signal.WorkPlace = this;
            _aqs = Services.GetAQService();
        }

        #region Загрузка модулей

        private void AddTask(string assemblyName, string toInstance, CommandCallBackDelegate commandCallBack)
        {
            _initName = toInstance;
            _assemblyName = assemblyName;
            var name = $"{ assemblyName + "."+toInstance}, {assemblyName}, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            var type = Type.GetType(name);
            if (type == null)
            {
                MessageBox.Show($"Модуль {assemblyName} не найден", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var loadedPage = (UserControl)Activator.CreateInstance(type);

            var ti = new TabItem
            {
                Style = Resources["TaskTabItemStyle"] as Style,
                Padding = new Thickness(0),
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Stretch
            };
            ti.MouseLeave += ti_MouseLeave;
            MainPanel.Items.Add(ti);
            MainPanel.SelectedItem = ti;

            var aqModule = loadedPage as IAQModule;
            if (aqModule != null)
            {
                var aqmd = aqModule.GetAQModuleDescription();
                var mti = GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]);
                loadedPage.HorizontalAlignment = HorizontalAlignment.Left;
                loadedPage.VerticalAlignment = VerticalAlignment.Top;
                loadedPage.Loaded += LoadedPage_Loaded;
                var signalledModule = ((IAQModule)loadedPage);
                if (commandCallBack != null) signalledModule.InitSignaling(commandCallBack as CommandCallBackDelegate);
                signalledModule.ReadyForAcceptSignals += MainPage_ReadyForAcceptSignals;

                Signal.AttachAQModule(aqmd);
                aqmd.Container = ti;
                var fill = new SolidColorBrush(Color.FromArgb(255,
                    (byte)(255.0d - (255.0d - mti.ColorMark.R) / 64.0d),
                    (byte)(255.0d - (255.0d - mti.ColorMark.G) / 64.0d),
                    (byte)(255.0d - (255.0d - mti.ColorMark.B) / 64.0d)));

                if (!mti.NoScroll)
                {
                    var internalSv = new ScrollViewer
                    {
                        HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                        VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                        Margin = new Thickness(0),
                        Padding = new Thickness(1),
                        Content = loadedPage,
                        Background = fill
                    };
                    ti.Content = internalSv;
                    Action a = () => internalSv.ScrollToVerticalOffset(0);
                    Dispatcher.BeginInvoke(a);
                }
                else
                {
                    ti.Content = loadedPage;
                    loadedPage.Background = fill;
                }

                ti.Tag = aqmd;
                ti.Background = new SolidColorBrush(mti.ColorMark);
                var tpi = new TaskPanelItem { HighLight = Colors.Transparent, Text = mti.Caption, HorizontalAlignment = HorizontalAlignment.Right, TabToClose = ti, Closeable = mti.Closeable };
                
                ti.Header = tpi;

                tpi.CloseClick += (o, e) =>
                {
                    var taskPanelItem = o as TaskPanelItem;
                    if (taskPanelItem == null) return;
                    var t = taskPanelItem.TabToClose;

                    var aqmdClosing = (t.Tag as AQModuleDescription);
                    Signal.DetachAQModule(aqmdClosing);
                    if (aqmdClosing != null &&
                        (_boundModules != null && _boundModules.ContainsValue(aqmdClosing)))
                    {
                        var toDelete = _boundModules.Where(b => b.Value == aqmdClosing).Select(b => b.Key).ToList();
                        foreach (var d in toDelete)
                        {
                            _boundModules.Remove(d);
                        }
                        _boundModules.Remove(aqmdClosing.UniqueID);
                    }
                    MainPanel.Items.Remove(t);
                };
            }

            _aqs.BeginWriteLog(new WriteLogRequest("Load:" + assemblyName), null, new object());
        }

        private void InitApplication()
        {
            LoadByUri("Title.MainPage", null);
            Loader.TabSizeChanged(this, new Size(MainPanel.ActualWidth, MainPanel.ActualHeight));
            _aqs.BeginWriteLog(new WriteLogRequest("WPF Version;IP=@IP"), null, new object());
        }

        private void LoadByUri(string uri, CommandCallBackDelegate commandCallBack)
        {
            var initValues = uri.Split('.');
            AddTask(initValues[0], initValues[1], commandCallBack);
        }

        private void MainMenu_XAPSelected(object o, AssemblySelectedEventArgs e)
        {
            _initName = e.ClassName;
            _assemblyName = e.Assembly;
            AddTask(e.Assembly, e.ClassName, null);
        }

        void ti_MouseLeave(object sender, MouseEventArgs e)
        {
            RefreshThumbnail();
        }

        void LoadedPage_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshThumbnail();
        }

        private void RefreshThumbnail()
        {
            var st = MainPanel.SelectedItem as TabItem;
            var aqmd = st?.Tag as AQModuleDescription;
            var ui = aqmd?.Instance as UIElement;
            if (ui == null) return;
            var wb = new RenderTargetBitmap((int)(ui.RenderSize.Width), (int)(ui.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            wb.Render(ui);
            aqmd.Thumbnail = wb;
            var i = new Image{ Source = aqmd.Thumbnail, Width = ui.RenderSize.Width * 0.3d, Height = ui.RenderSize.Height * 0.3d };
            var tt = new ToolTip { Style = Resources["ThumbnailStyle"] as Style, Content = i, HorizontalOffset = 0, VerticalOffset = 10 };
            var uiElement = st.Header as UIElement;
            uiElement?.SetValue(ToolTipService.ToolTipProperty, tt);
        }

        private MenuTreeItem GetMenuTreeItemByAssemblyName(string assemblyName)
        {
            var mtr = Resources["MenuTreeData"] as MenuTreeRoot;
            return mtr?.Children.Select(mti => FindMenuTreeItemFrom(mti, assemblyName)).FirstOrDefault(mt => mt != null);
        }

        private MenuTreeItem FindMenuTreeItemFrom(MenuTreeItem mti, string assemblyName)
        {
            return mti.Assembly == assemblyName 
                ? mti 
                : mti.Children.Select(mt => FindMenuTreeItemFrom(mt, assemblyName)).FirstOrDefault(m => m != null);
        }

        #endregion
        
        #region "Expose"

        private void ThumbnailsSelection(object o, RoutedEventArgs e)
        {
            ThumbView.Children.Clear();
            
            foreach (var i in MainPanel.Items)
            {
                var ti = i as TabItem;
                if (ti == null) continue;
                var aqmd = ti.Tag as AQModuleDescription;
                if (aqmd != null && aqmd.Thumbnail == null) return;
                if (aqmd == null) continue;
                var mti = GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]);
                var b = new TextImageButtonBase {Style=Resources["ThumbButton"] as Style, Text = mti.Caption, Width = aqmd.Thumbnail.PixelWidth + 20, Height = aqmd.Thumbnail.PixelHeight+20, ImageSource = aqmd.Thumbnail, Tag = ti, Margin = new Thickness(10), MaxHeight = 300.0d, MaxWidth = 300.0d * aqmd.Thumbnail.PixelWidth / (aqmd.Thumbnail.PixelHeight + 80.0d)};
                b.Click += b_Click;
                ThumbView.Children.Add(b);
            }
            ThumbPanel.Visibility = Visibility.Visible;
            ThumbView.UpdateLayout();
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null) MainPanel.SelectedItem = button.Tag as TabItem;
            HideTaskSelection();
        }

        private void HideTaskSelection()
        {
            ThumbView.Children.Clear();
            ThumbPanel.Visibility = Visibility.Collapsed;
            BackAQButton.IsChecked = false;
        }

        private void BackAQButton_Click(object sender, RoutedEventArgs e)
        {
            HideTaskSelection();
        }


        #endregion

        #region Обработка сигналов

        void MainPage_ReadyForAcceptSignals(object o, ReadyForAcceptSignalsEventArg e)
        {
            var ccbd = e.CommandCallBack;
            ccbd?.Invoke(Command.Load, null, o);
        }

        public AQModuleDescription GetAQModuleDescription()
        {
            return _aqmd;
        }

        public void ProcessSignal(AQModuleDescription senderDescription, Command commandType, object commandArgument, CommandCallBackDelegate commandCallBack)
        {
            switch (commandType)
            {
                case Command.Renew:

                    var tab = MainPanel.SelectedItem as TabItem;
                    if (tab != null)
                    {
                        Action a = () =>
                        {
                            var sv = (tab.Content as ScrollViewer);
                            if (sv == null) return;
                            sv.InvalidateScrollInfo();
                            sv.ScrollToVerticalOffset(0);
                            sv.UpdateLayout();
                        };
                        Dispatcher.BeginInvoke(a);
                    }
                    return;


                case Command.InsertSlideElement:

                    if (MainSlidePanel != null) SendSlideToSlidePanel(commandArgument);
                    return;

                case Command.Load:

                    var ca = commandArgument as Dictionary<string, object>;
                    var uri = string.Empty;
                    if (ca != null && ca.ContainsKey("URI")) uri = (String)ca["URI"];

                    LoadByUri(uri, (t, a, r) =>
                    {
                    });
                    return;


                case Command.InsertStoredCalculation:
                    LoadByUri("Constructor_Calculations.Page", (t, a, r) =>
                    {
                        var loadedCalculationConstructor = r as IAQModule;
                        if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), Command.InsertStoredCalculation, commandArgument, null);
                    });
                    return;

                case Command.PasteToAnalysis:
                    var c = commandArgument as Dictionary<string, object> ?? new Dictionary<string, object>();
                    c.Add("Text", Clipboard.GetText());
                    SendCommandToAnalisys(c, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                case Command.InsertTablesToAnalysis:
                case Command.ReopenInAnalisys:
                case Command.SendTableToAnalysis:
                    SendCommandToAnalisys(commandArgument, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    break;

                case Command.StartNewAnalysis:
                    LoadByUri("Analysis.MainPage", (t, a, r) =>
                    {
                        var loadedCalculationConstructor = r as IAQModule;
                        if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), commandType, commandArgument, null);
                    });
                    break;
                case Command.Clear:
                    CloseAllModules();
                    break;

                default:
                    SendCommandToCalculation(commandArgument, commandType, senderDescription);
                    if (commandCallBack != null) commandCallBack.Invoke(commandType, commandArgument, null);
                    return;
            }
        }
        private void CloseAllModules()
        {
            Signal.ClearAQModules();
            MainPanel.Items.Clear();
            MainSlidePanel.Clear();
            InitApplication();
        }

        private void SendSlideToSlidePanel(object commandArgument)
        {
            var ca = commandArgument as Dictionary<string, object>;
            Step operation = null;
            List<Step> sourceSteps = null;
            var sourceCalcGuid = Guid.NewGuid();
            WriteableBitmap wb = null;
            SlideElementDescription sed = null;
            List<SLDataTable> sourceData = null;

            DateTime? from = null, to = null;
            if (ca != null)
            {
                if (ca.ContainsKey("From")) from = (DateTime) ca["From"];
                if (ca.ContainsKey("To")) to = (DateTime) ca["To"];
                if (ca.ContainsKey("Operation")) operation = (Step) ca["Operation"];
                if (ca.ContainsKey("Calculation")) sourceSteps = (List<Step>) ca["Calculation"];
                if (ca.ContainsKey("SourceGuid")) sourceCalcGuid = (Guid) ca["SourceGuid"];
                if (ca.ContainsKey("Thumbnail")) wb = (WriteableBitmap) ca["Thumbnail"];
                if (ca.ContainsKey("SlideElement")) sed = (SlideElementDescription) ca["SlideElement"];
                if (ca.ContainsKey("SourceData")) sourceData = (List<SLDataTable>) ca["SourceData"];
            }
            OpenSlidePanel();
            MainSlidePanel.InsertSlideElement(sed, sourceSteps, sourceData, sourceCalcGuid, operation, wb, from, to);
        }

        private void SendCommandToCalculation(object commandArgument, Command c, AQModuleDescription senderDescription)
        {
            var calcs = (from t in MainPanel.Items.OfType<TabItem>() let aqmd = ((AQModuleDescription)((TabItem)t).Tag) where aqmd.UniqueID != senderDescription.UniqueID && GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]).Caption.Trim() == "Расчёты" select t).ToList();
            if (!calcs.Any())
            {
                LoadByUri("SLConstructor_Calculations.Page", (t, a, r) =>
                {
                    var loadedCalculationConstructor = r as IAQModule;
                    if (loadedCalculationConstructor != null) Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), c, commandArgument, null);
                });
            }
            else SelectModule(calcs.Cast<TabItem>(), commandArgument, c, "Новый расчет", "Выбор расчета для вставки запроса", "SLConstructor_Calculations.Page");
        }

        private void SendCommandToAnalisys(object commandArgument, Command c, AQModuleDescription senderDescription)
        {
            var loadedmodules = (from t in MainPanel.Items.OfType<TabItem>()
                let aqmd = ((AQModuleDescription) ((TabItem) t).Tag)
                where GetMenuTreeItemByAssemblyName(aqmd.ModuleType.Assembly.FullName.Split(',')[0]).Caption.Trim() == "Анализ"
                && aqmd.UniqueID!=senderDescription.UniqueID
                select t).ToList();

            var ca = commandArgument as Dictionary<string, object>;
            var onlyNew = false;
            if (ca!=null && ca.ContainsKey("New")) onlyNew = (bool)ca["New"];
            if (senderDescription!=null && _boundModules.ContainsKey(senderDescription.UniqueID))
            {
                Signal.Send(GetAQModuleDescription(), _boundModules[senderDescription.UniqueID], c, commandArgument, null);
                MainPanel.SelectedItem = _boundModules[senderDescription.UniqueID].Container; 
                return;
            }
            
            if (!loadedmodules.Any() || onlyNew)
            {
                LoadByUri("SLAnalysis.MainPage", (t, a, r) =>
                {
                    var loadedAnalysis = r as IAQModule;
                    if (loadedAnalysis == null) return;
                    if (onlyNew && senderDescription!=null)_boundModules.Add(senderDescription.UniqueID, ((IAQModule)r).GetAQModuleDescription());
                    Signal.Send(GetAQModuleDescription(), loadedAnalysis.GetAQModuleDescription(), c, commandArgument, null);
                });
            }
            else SelectModule(loadedmodules.Cast<TabItem>(), commandArgument, c, "Новый анализ", "Выбор анализа для вставки данных", "SLAnalysis.MainPage");
        }

        private void SelectModule(IEnumerable<TabItem> tis, object commandArgument, Command currentCommand, string nameOfNewModule, string title, string newModuleURI)
        {
            
            ModuleView.Children.Clear();
            SelectModuleWindow.Tag = commandArgument;
            SelectModuleWindow.Caption = title;
            var newModuleButton = new TextImageButtonBase { Style = Resources["ThumbButton"] as Style, Text = nameOfNewModule, Width = 100, Height = 100, ImageSource = _newCalculation, Margin = new Thickness(10) };
            newModuleButton.Click += modulebutton_Click; 
            newModuleButton.Tag = new AQModuleDescription { CustomData = currentCommand, Name = "NewModule", URL = newModuleURI}; 
            ModuleView.Children.Add(newModuleButton);
            
            foreach (var ti in tis)
            {
                var aqmd = ti.Tag as AQModuleDescription;
                if (aqmd != null && aqmd.Thumbnail == null) return;
                if (aqmd == null) continue;
                var calcbutton = new TextImageButtonBase { Style = Resources["ThumbButton"] as Style, Text = aqmd.ShortDescription, Width = aqmd.Thumbnail.PixelWidth + 10, Height = aqmd.Thumbnail.PixelHeight + 10, ImageSource = aqmd.Thumbnail, Tag = ti, Margin = new Thickness(10), MaxHeight = 400 };
                calcbutton.Tag = aqmd;
                aqmd.CustomData = currentCommand;
                calcbutton.Click += modulebutton_Click; 
                ModuleView.Children.Add(calcbutton);
            }
            ModuleView.UpdateLayout();
            SelectModuleWindow.Visibility = Visibility.Visible;
            
        }
        

        void modulebutton_Click(object sender, RoutedEventArgs e)
        {
            SelectModuleWindow.Visibility = Visibility.Collapsed;
            var aqmd = ((TextImageButtonBase)sender).Tag as AQModuleDescription;
            if (aqmd == null) return;
            if (aqmd.Name != "NewModule")
            {
                Signal.Send(GetAQModuleDescription(), aqmd, (Command)aqmd.CustomData, SelectModuleWindow.Tag, null);
                MainPanel.SelectedItem = aqmd.Container;
            }
            else
            {
                LoadByUri(aqmd.URL, (t, a, r) =>
                {
                    var loadedCalculationConstructor = r as IAQModule;
                    if (loadedCalculationConstructor == null) return;
                    Signal.Send(GetAQModuleDescription(), loadedCalculationConstructor.GetAQModuleDescription(), (Command)aqmd.CustomData, SelectModuleWindow.Tag, null);
                });
            }
        }
        
        private void SelectCalculationWindow_Closing(object sender, CancelEventArgs e)
        {
            ModuleView.Children.Clear();
            SelectModuleWindow.Visibility = Visibility.Collapsed;
            SelectModuleWindow.Tag = null;
        }
        
        #endregion

        #region Панель слайдов

        private void OpenSlidesPanelButton_Click(object sender, RoutedEventArgs e)
        {
            OpenSlidePanel();
        }

        private void OpenSlidePanel()
        {
            RightPanelHiddenView.Visibility = Visibility.Collapsed;
            RightPanelOpenView.Visibility = Visibility.Visible;
            if (RightPanel.ActualWidth < 130) SlidePanelColumnDefinition.Width = new GridLength(175);
            SlidePanel.UpdateLayout();
        }

        private void CloseSlidesPanelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseSlidePanel();
        }

        private void CloseSlidePanel()
        {
            RightPanelHiddenView.Visibility = Visibility.Visible;
            RightPanelOpenView.Visibility = Visibility.Collapsed; 
            SlidePanelColumnDefinition.Width = new GridLength(12);
            SlidePanel.UpdateLayout();
        }

        #endregion 

        private void RightPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Math.Abs(e.NewSize.Width - 12) < 1e-8)
            {
                RightPanelHiddenView.Visibility = Visibility.Visible;
                RightPanelOpenView.Visibility = Visibility.Collapsed; 
            }
            else
            {
                RightPanelHiddenView.Visibility = Visibility.Collapsed;
                RightPanelOpenView.Visibility = Visibility.Visible;
            }
        }

        private void MainPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Loader.TabSizeChanged(this, new Size(MainPanel.ActualWidth, MainPanel.ActualHeight));
        }

        private void FrameworkElement_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_initialized || sender == null || Math.Abs(((FrameworkElement) sender).ActualWidth) < 1e-8 || Math.Abs(((FrameworkElement) sender).ActualHeight) < 1e-8) return;

            InitApplication();
            _initialized = true;
        }
    }
}