﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Core;
using Core.AQServiceReference;
using System.Diagnostics;

// ReSharper disable once CheckNamespace
namespace WorkPlace
{
    public partial class Menu 
    {
        public delegate void AssemblySelectedDelegete(object o, AssemblySelectedEventArgs e);
        public event AssemblySelectedDelegete AssemblySelected;
        private readonly AQService _aqs;
        

        public MenuTreeRoot MenuRoot
        {
            get { return (MenuTreeRoot)GetValue(MenuRootProperty); }
            set { SetValue(MenuRootProperty, value); }
        }

        public MenuItem ActiveItem
        {
            get
            {
                return _activeItem;
            } 
            set
            {
                _activeItem = value;
                if (value == null) _closePopupTimer.Start();
                else _closePopupTimer.Stop();
            }
        }
        private MenuItem _activeItem;
        private readonly DispatcherTimer _closePopupTimer;

        public static readonly DependencyProperty MenuRootProperty =
            DependencyProperty.Register("MenuRoot", typeof(MenuTreeRoot), typeof(Menu), new PropertyMetadata(null, delegate(DependencyObject o, DependencyPropertyChangedEventArgs args) { ((Menu)o).RebuildMenu();}));

        public Menu()
        {
            InitializeComponent();
            _closePopupTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 400)};
            _closePopupTimer.Tick += ClosePopupTimer_Tick;
            _aqs = Services.GetAQService();
        }
        
        private void ClosePopupTimer_Tick(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action (HideAllPopups));
        }
        
        public void MenuAccepted(MenuItem mi)
        {
            if (!string.IsNullOrEmpty(mi.TreeDataItem.Signal))
            {
                Command command;
                Enum.TryParse(mi.TreeDataItem.Signal, true, out command);
                Signal.Send(Signal.WorkPlace.GetAQModuleDescription(), Signal.WorkPlace.GetAQModuleDescription(), command, null, null);
                return;
            }
            if (!string.IsNullOrEmpty(mi.TreeDataItem.NavigateURL))
            {
                _aqs.BeginWriteLog(new WriteLogRequest("Url:" + mi.TreeDataItem.NavigateURL), null, null); 
                Process.Start(mi.TreeDataItem.NavigateURL);
                return;
            }
            AssemblySelected?.Invoke(this, new AssemblySelectedEventArgs{ ClassName = mi.TreeDataItem.ToInstance, Assembly = mi.TreeDataItem.Assembly});
        }

        private void RebuildMenu()
        {
            RootItemContainer.Children.Clear();
            var itemStyle = Resources["MenuItemStyle"] as Style;
            var subItemStyle = Resources["MenuSubItemStyle"] as Style;

            foreach (var r in MenuRoot.Children)
            {
                var allRolesExists = true; 
                if (!string.IsNullOrEmpty(r.RequiredRoles))
                {
                    var roles = r.RequiredRoles.Split(';').ToList();
                    if (roles.Any(v => Security.Roles==null || !Security.Roles.Contains(v)))
                    {
                        allRolesExists = false;
                    }
                }

                if (!allRolesExists) continue;
                var menuItem = new MenuItem
                {
                    Style = itemStyle,
                    SubItemStyle = subItemStyle,
                    ParentItem = null,
                    TreeDataItem = r,
                    Level = 0,
                    RootMenu = this,
                    ColorMarkerBrush = new SolidColorBrush(r.ColorMark)
                };
                RootItemContainer.Children.Add(menuItem);
                UpdateLayout();
            }
        }

        public void HideAllPopups()
        {
            foreach (var m in RootItemContainer.Children) ((MenuItem)m).HideSelfAndChildPopups();
        }
    }

    public class AssemblySelectedEventArgs : EventArgs
    {
        public string Assembly{get; set; }
        public string ClassName { get; set; }
    }
}
